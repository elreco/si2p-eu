<?php

	//EDITION DES PRODUIT D'UN CLIENT
	
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");
	include("includes/connexion_fct.php");

	// $client correspond à l'ID du client consulter // différent de cpr_client qui est l'ID du client qui mémorise la base produit
	$client=0;
	if(!empty($_GET['client'])){
		$client=intval($_GET['client']);
	}
	if($client==0){
		echo("Erreur : client inconnu !");
		die();	
	}
	
	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}
	
	
	// SUR LE CLIENT 
	
	$req=$Conn->query("SELECT cli_categorie,cli_nom FROM Clients WHERE cli_id=" . $client . ";");
	$d_client=$req->fetch();
	if(empty($d_client)){
		echo("Impossible d'afficher la page !");
		die();	
	}else{
		if($d_client["cli_categorie"]==2){
			if(empty($_SESSION["acces"]["acc_droits"][8])){
				// c'est un gc et U n'a pas la gestion des GC
				echo("Impossible d'afficher la page !");
				die();
			}
		}elseif($d_client["cli_categorie"]==5){
			if(empty($_SESSION['acces']['acc_service'][1]) OR empty($_SESSION["acces"]["acc_droits"][15])){
				// c'est un client NATIONNAL et U ne faait pas partie du service com ou n'a pas le droit client réseau
				echo("Impossible d'afficher la page !");
				die();
			}
		}else{
			if(empty($_SESSION["acces"]["acc_droits"][15])){
				echo("Impossible d'afficher la page !");
				die();
			}
		}
	}
	

	$sql="SELECT soc_id,soc_nom,soc_agence,age_id,age_nom,cso_societe,cso_agence,cso_commercial FROM Societes 
	LEFT OUTER JOIN Agences ON (Societes.soc_id=Agences.age_societe)
	LEFT OUTER JOIN Clients_Societes ON (Societes.soc_id=Clients_Societes.cso_societe AND (Agences.age_id=Clients_Societes.cso_agence OR ISNULL(age_id)) AND Clients_Societes.cso_client=" . $client ." AND NOT cso_archive)";
	if($d_client["cli_categorie"]!=2 AND $_SESSION['acces']["acc_profil"]!=10){
		// si le client est un gc ou si U est responsable d'exploitation > on affiche toutes les societes (sauf archivé et LVDF)
		$sql.=" LEFT JOIN Utilisateurs_Societes ON (Societes.soc_id=Utilisateurs_Societes.uso_societe AND (Agences.age_id=Utilisateurs_Societes.uso_agence OR ISNULL(age_id) ) )";
	}
	$sql.=" WHERE NOT soc_archive";
	if($d_client["cli_categorie"]==2 OR $_SESSION['acces']["acc_profil"]==10){
		$sql.=" AND NOT soc_id IN (1,6,10)";
	}else{
		$sql.=" AND uso_utilisateur=" . $acc_utilisateur;
	}
	$sql.=" ORDER BY soc_nom,age_nom;";
	$req=$Conn->query($sql);
	$societes=$req->fetchAll(); 
	
	/*echo("<pre>");
		print_r($societes);
	echo("</pre>");
	die();*/
		

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" >
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	   
	   <!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="client_reseau_enr.php">
			<div>
				<input type="hidden" name="client" value="<?=$client?>" />
			</div>
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" class="" >
					<section id="content" class="animated fadeIn">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
							
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											
											<div class="content-header">
												<h2>Réseau du client <b class="text-primary"><?= $d_client['cli_nom'] ?></b></h2>											
											</div>
											<div class="col-md-10 col-md-offset-1">										 
												<div class="row section text-left">

											<?php 	$soc_id=0;
													foreach($societes as $s){ 
													
														$age_id=0;
														if(!empty($s['age_id'])){
															$age_id=$s['age_id'];
														}
													
														if($soc_id!=$s['soc_id']){
															
															if($soc_id>0){
																echo("</div>");
															};
															
															$soc_id=$s['soc_id']; 
															
															$ConnFct = connexion_fct($s['soc_id']);	 ?>
															
															<div class="col-md-6 section">
																
													<?php			if($age_id==0){ 
																		
																		$sql="SELECT * FROM Commerciaux WHERE com_archive = 0";
																		$sql.=" ORDER BY com_label_1,com_label_2;";
																		$req=$ConnFct->query($sql);
																		$commerciaux=$req->fetchAll(); 	?>
																		<div class="row">
																			<div class="col-md-1">
																				<label class="option option-info">
																					<input type="checkbox" class="checkbox2" name="cso_societe_<?=$s['soc_id']?>_<?=$age_id?>" <?php if(!empty($s['cso_societe'])) echo("checked") ?> />
																					<span class="checkbox"></span>
																				</label>
																			</div>
																			<div class="col-md-5 text-info"><?=$s['soc_nom']?></div>
																			<div class="col-md-6">
																				<div class="section">
																					<select name="cso_commercial_<?=$s['soc_id']?>_<?=$age_id?>" class="select2" id="cso_commercial_<?=$s['soc_id']?>_<?=$age_id?>" >
																						<option value="0">Commercial ...</option>
																			<?php 		foreach($commerciaux as $c){ ?>																				
																							<option value="<?= $c['com_id'] ?>"
																							<?php if($c['com_id'] == $s['cso_commercial']){ ?> 
																								selected
																							<?php }?>
																							><?= $c['com_label_1'] ?> <?= $c['com_label_2'] ?></option>
																			<?php 		}?>
																					</select>
																				</div>		
																			</div>
																		</div>
													<?php			}else{ ?>
																		<div class="row">
																			<div class="col-md-12 text-info "><?=$s['soc_nom']?></div>
																		</div>														
												<?php				}
														}
														if($age_id>0){
															$sql="SELECT * FROM Commerciaux WHERE com_archive = 0 AND com_agence=" . $age_id ;
															$sql.=" ORDER BY com_label_1,com_label_2;";
															$req=$ConnFct->query($sql);
															$commerciaux=$req->fetchAll(); 
															?>
															<div class="row">
																<div class="col-md-1">															
																	<label class="option option-success">
																		<input type="checkbox" class="checkbox2" name="cso_societe_<?=$s['soc_id']?>_<?=$age_id?>" <?php if(!empty($s['cso_agence'])) echo("checked") ?> />
																		<span class="checkbox"></span>
																	</label>
																</div>																															
																<div class="col-md-5 text-success"><?=$s['age_nom']?></div>
																<div class="col-md-6">
																	<div class="section">
																		<select name="cso_commercial_<?=$s['soc_id']?>_<?=$age_id?>" class="select2" >
																			<option value="0">Commercial ...</option>
																<?php 		foreach($commerciaux as $c){ ?>																				
																				<option value="<?= $c['com_id'] ?>"
																				<?php if($c['com_id'] == $s['cso_commercial']){ ?> 
																					selected
																				<?php }?>
																				><?= $c['com_label_1'] ?> <?= $c['com_label_2'] ?></option>
																<?php 		}?>
																		</select>
																	</div>		
																</div>
															</div>
											<?php		} 
													} ?>
														
												</div>
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>				
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="client_voir.php?client=<?=$client?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" ></div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>
		<script type="text/javascript" >
		
			var categorie=parseInt("<?=$d_client["cli_categorie"]?>");
		
		
				jQuery(document).ready(function (){	
					
					//*****************************************
					//	INTERACTION HEADER FAC
					//******************************************/
					
					$(".checkbox2").click(function(){
						if(!$(this).is(":checked") && categorie==2){
							if($(this).prop("name")=="cso_societe_4_4"){
								$(this).prop("checked",true);
								
							}	
						}
					});
				});	
						
		</script>			
	</body>
</html>
