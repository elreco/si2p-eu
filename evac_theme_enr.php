<?php

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

$erreur="";

if(!empty($_POST)){

	
	$eth_id=0;
	if(isset($_POST['eth_id'])){
		$eth_id=intval($_POST['eth_id']);
	}
	
	if($eth_id>0){
		$req = $Conn->prepare("UPDATE evacuations_themes SET eth_theme=:eth_theme,eth_libelle=:eth_libelle WHERE eth_id=:eth_id;");
		$req->bindParam("eth_theme",$_POST['eth_theme']);
		$req->bindParam("eth_libelle",$_POST['eth_libelle']);
		$req->bindParam("eth_id",$eth_id);
		$req->execute();

	}else{
		$req = $Conn->prepare("INSERT INTO evacuations_themes (eth_theme,eth_libelle) VALUES (:eth_theme,:eth_libelle);");
		$req->bindParam("eth_theme",$_POST['eth_theme']);
		$req->bindParam("eth_libelle",$_POST['eth_libelle']);
		$req->execute();

	}
		
}else{
	$erreur="Formulaire incomplet!";
}

if(!empty($erreur)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur
	);
}else{
	$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Thème ajouté"
	);
}

header("location : evac_theme_liste.php");
	
 ?>
