<?php
//////////////////// INCLUDES ///////////////////////
include "includes/controle_acces.inc.php";
include 'includes/connexion.php';
include 'modeles/mod_parametre.php';
include 'modeles/mod_contact.php';
include 'modeles/mod_famille.php';
include 'modeles/mod_sous_famille.php';
include 'modeles/mod_categorie.php';
include 'modeles/mod_utilisateur.php';
include 'modeles/mod_fournisseur.php';
include "modeles/mod_competence.php";
include "modeles/mod_diplome.php";

///////////////////// Contrôles des parametres ////////////////////

$fournisseur_id = 0;
if (!empty($_GET["fournisseur"])) {
    // si les get sont pas remplis
    $fournisseur_id = intval($_GET["fournisseur"]);
}
if ($fournisseur_id == 0) {
    echo ("Impossible d'afficher la page");
    die();
}
///////////////////// FIN Contrôles des parametres ////////////////////

//////////////////// Requêtes à la base de données /////////////////////

$acc_agence = 0;
if (isset($_SESSION['acces']["acc_agence"])) {
    $acc_agence = $_SESSION['acces']["acc_agence"];
}
$acc_societe = 0;
if (isset($_SESSION['acces']["acc_societe"])) {
    $acc_societe = $_SESSION['acces']["acc_societe"];
}

// rechercher le fournisseur
$non_accessible = 0;
$sql = "SELECT * FROM fournisseurs WHERE fou_id=:fournisseur_id;";
$req = $Conn->prepare($sql);
$req->bindParam(':fournisseur_id', $fournisseur_id);
$req->execute();
$fournisseur = $req->fetch();
if (empty($fournisseur)) {
    $non_accessible = 1;
}

// GROUPE
if ($fournisseur["fou_groupe"] == 1) {

    if ($fournisseur['fou_filiale_de'] == 0) {

        $maison_mere = $fournisseur;

    } else {
        // recherche maison mère

        $sql = "SELECT * FROM fournisseurs ";
        $sql .= " WHERE fou_groupe = 1 AND fou_id = :fou_filiale_de";
        $req = $Conn->prepare($sql);
        $req->bindParam(':fou_filiale_de', $fournisseur['fou_filiale_de']);
        $req->execute();
        $maison_mere = $req->fetch();
    }

    // recherche des filiales
    $sql = "SELECT * FROM fournisseurs WHERE fou_groupe = 1 AND fou_filiale_de = :fou_filiale_de";
    $req = $Conn->prepare($sql);
    $req->bindParam(':fou_filiale_de', $maison_mere['fou_id']);
    $req->execute();
    $filiales = $req->fetchAll();
    $arrayfil = array();
    foreach ($filiales as $f) {
        $arrayfil[$f['fou_niveau']][] = $f;
    }
}

// CONTACTS
$req = $Conn->prepare("SELECT * FROM fournisseurs_contacts LEFT JOIN Contacts_Fonctions ON (fournisseurs_contacts.fco_fonction=Contacts_Fonctions.cfo_id)
	WHERE fco_fournisseur = :fco_fournisseur ORDER BY fco_defaut DESC");
$req->bindParam(':fco_fournisseur', $fournisseur_id);
$req->execute();
$fournisseurs_contacts = $req->fetchAll();

// LES DES INTERVENANTS

$req = $Conn->prepare("SELECT * FROM fournisseurs_intervenants WHERE fin_fournisseur = :fin_fournisseur ORDER BY fin_archive,fin_nom,fin_prenom");
$req->bindParam(':fin_fournisseur', $fournisseur_id);
$req->execute();
$fournisseurs_intervenants = $req->fetchAll();

// adresse de $fournisseur_id
$req = $Conn->prepare("SELECT * FROM fournisseurs_adresses WHERE fad_fournisseur = :fad_fournisseur");
$req->bindParam(':fad_fournisseur', $fournisseur_id);
$req->execute();
$fournisseurs_adresse = $req->fetch();

// validations du fournisseur
$req = $Conn->prepare("SELECT * FROM fournisseurs_validations WHERE fva_fournisseur = :fva_fournisseur AND fva_societe = :fva_societe");
$req->bindParam(':fva_fournisseur', $fournisseur_id);
$req->bindParam(':fva_societe', $_SESSION['acces']['acc_societe']);
$req->execute();
$fournisseur_validation = $req->fetch();

// TYPES D'ACHAT

$req = $Conn->prepare("SELECT ffa_id, ffa_libelle, ffa_type,ffj_famille FROM fournisseurs_familles LEFT JOIN fournisseurs_familles_jointure
	ON (fournisseurs_familles.ffa_id=fournisseurs_familles_jointure.ffj_famille AND ffj_fournisseur=:fournisseur_id)
	ORDER BY ffa_libelle;");
$req->bindParam(":fournisseur_id", $fournisseur_id);
$req->execute();
$fournisseurs_familles = $req->fetchAll();
$afficher_tabs = 0;
/* echo("<pre>");
var_dump($fournisseurs_familles);
echo("</pre>");
die(); */
foreach($fournisseurs_familles as $ffa){
	// le fournisseur est de la famille Sous-traitance
	if($ffa['ffa_id'] == 2 && !empty($ffa['ffj_famille'])){
		$afficher_tabs = 1;
	}
}

// rechercher les familles de produit
$req = $Conn->prepare("SELECT pfa_id, pfa_libelle FROM produits_familles");
$req->execute();
$produits_familles = $req->fetchAll();

// recherche produits de $fournisseur_id
$req = $Conn->prepare("SELECT * FROM fournisseurs_produits WHERE fpr_fournisseur = :fpr_fournisseur");
$req->bindParam(':fpr_fournisseur', $fournisseur_id);
$req->execute();
$fournisseurs_produits = $req->fetchAll();

// fichiers de $fournisseur_id
$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_societe = :ffi_societe");
$req->bindParam(':ffi_fournisseur', $fournisseur_id);
$req->bindParam(':ffi_societe', $_SESSION['acces']['acc_societe']);
$req->execute();
$fournisseurs_fichiers = $req->fetchAll();

// DOCUMENTS FOURNISSEURS
if($fournisseur["fou_type"]!==2 AND $afficher_tabs==1){

	// fournisseur ST autre que INTERCO

	$sql_doc_fou="SELECT * FROM fournisseurs_documents_Param LEFT fournisseurs_documents 
	ON (fournisseurs_documents_Param.fdp_id=fournisseurs_documents.fdo_document AND fournisseurs_documents.fdo_fournisseur=:fournisseur)
	WHERE fdp_type = 1";
	if($fournisseur["fou_type"]==1){
		$sql_doc_fou.=" AND fdp_entreprise";
	}else{
		$sql_doc_fou.=" AND fdp_indiv";
	}
	$req_doc_fou = $Conn->prepare($sql_doc_fou);
	$req_doc_fou ->bindParam(":fournisseur",$fournisseur_id);
	$req->execute();
	$fournisseurs_documents = $req->fetchAll();

}


// tous les documents des fournisseurs avec le type 2
/*$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_type = 2");
$req->execute();
$fournisseurs_documents_intervenants = $req->fetchAll();*/

// tous les départements de $fournisseur_id
$req = $Conn->prepare("SELECT fin_departement FROM fournisseurs_interventions WHERE fin_fournisseur = :fin_fournisseur");
$req->bindParam(':fin_fournisseur', $fournisseur_id);
$req->execute();
$fournisseurs_interventions = $req->fetchAll();

// construis ma chaine de caracteres
$numItems = count($fournisseurs_interventions);
$fin_string = "";
foreach ($fournisseurs_interventions as $f) {

    $fin_string .= "'" . $f['fin_departement'] . "',";

    /*if(!empty($fin_string) && $i === $numItems) {
$fin_string =  $fin_string . ",'" . $f['fin_departement'] . "']";
}elseif(!empty($fin_string)){
$fin_string = $fin_string . ",'" . $f['fin_departement'] . "'";
}else{
$fin_string = "['" . $f['fin_departement'] . "'";
}*/
}

if (!empty($fin_string)) {
    $fin_string = substr($fin_string, 0, -1);
    $fin_string = '[' . $fin_string . ']';
    // virer le dernier caractère $fin_string = '[' $fin_string ']'
    $fournisseurs_interventions_json = $fin_string;
}

// GESTION DES ONGLETS ET BTN ASSOCIE
$style_general = "";
$classe_general = " active";

$style_zone = "style='display:none;'";
$classe_zone = "";

$style_produit = "style='display:none;'";
$classe_produit = "";

$style_intervenant = "style='display:none;'";
$classe_intervenant = "";

$style_doc = "style='display:none;'";
$classe_doc = "";

$style_groupe = "style='display:none;'";
$classe_groupe = "";

if (isset($_GET['tab'])) {
    switch ($_GET['tab']) {
        case 2:
            // produit
            $style_produit = "";
            $classe_produit = " active";

            $style_general = "style='display:none;'";
            $classe_general = "";
            break;
        case 3:
            // zone d'intervention
            $style_zone = "";
            $classe_zone = " active";

            $style_general = "style='display:none;'";
            $classe_general = "";
            break;
        case 4:
            // doc
            $style_doc = "";
            $classe_doc = " active";

            $style_general = "style='display:none;'";
            $classe_general = "";
            break;
        case 5:
            // intervenant
            $style_intervenant = "";
            $classe_intervenant = " active";

            $style_general = "style='display:none;'";
            $classe_general = "";
            break;
        case 6:
            // groupe
            $style_groupe = "";
            $classe_groupe = " active";

            $style_general = "style='display:none;'";
            $classe_general = "";
            break;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - <?=$fournisseur['fou_nom']?></title>
	<meta name="keywords" content=""/>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
<!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

	<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link href="vendor/plugins/nestable/nestable.css" rel="stylesheet" type="text/css">
	<link href="vendor/plugins/carte-france/dist/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/img/favicon.png">

	<style type="text/css">
		.select-famille{
			cursor: default!important;
		}

		#francemap {
			width:100%;
			height:100%;
		}
		.tooltip-inner {
		    white-space:pre-wrap;
		}

		.intervenant-liste.active{
			font-weight:bold;
		}

	</style>

	<!-- fin styles sur les checkbox famille -->
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

</head>
	<body class="sb-top sb-top-sm ">
		<div id="main">
<?php		include "includes/header_def.inc.php";?>
			<section id="content_wrapper">
				<h1><small>Fournisseur - </small><?=$fournisseur['fou_nom']?></h1>

				<section>
		<?php 		if ($non_accessible == 1) 
					{?>
						<div class="alert alert-danger">
							Le fournisseur n'est pas accessible sur cette société !
						</div>
		<?php 		}else
					{ ?>
						<div class="row">
							<div class="col-md-12">
								<div class="tab-block">
									<ul class="nav nav-tabs responsive">
							<?php 		if ($fournisseur['fou_groupe'] == 1 && $fournisseur['fou_type'] != 3){ ?>
											<li class="tab6 tab-clic<?=$classe_groupe?>" data-tab="6" >
												<a href="#tab6" data-toggle="tab" aria-expanded="false">Groupe</a>
											</li>
							<?php 		} ?>
										<li class="tab1 tab-clic<?=$classe_general?>" data-tab="1" >
											<a href="#tab1" data-toggle="tab" aria-expanded="false">Général</a>
										</li>										
										<li class="tab2 tab-clic<?=$classe_produit?>" data-tab="2" >
											<a href="#tab2" data-toggle="tab" aria-expanded="false">Produits et Services</a>
										</li>
							<?php 		if(!empty($afficher_tabs)){ ?>
											<li class="tab3 tab-clic<?=$classe_zone?>" data-tab="3" >
												<a href="#tab3" data-toggle="tab" aria-expanded="false">Zones d'interventions</a>
											</li>
							<?php		}
										if($afficher_tabs){ ?>
											<li class="tab4 tab-clic<?=$classe_doc?>" data-tab="4" >
												<a href="#tab4" data-toggle="tab" aria-expanded="false">Documents</a>
											</li>
							<?php		} ?>
										<li class="tab5 tab-clic<?=$classe_intervenant?>" data-tab="5" >
											<a href="#tab5" data-toggle="tab" aria-expanded="false">
								<?php		 	if ($fournisseur['fou_type'] == 3)
												{ 	?>
													Fiche intervenant
								<?php 			}else{?>
													Liste des intervenants
								<?php 			}	?>
											</a>
										</li>
									</ul>

									<div class="tab-content responsive p30" style="margin-bottom: 50px;">
										<!-- GROUPE -->
							<?php		 if ($fournisseur['fou_groupe'] == 1 && $fournisseur['fou_type'] != 3) {?>
											<div id="tab6" class="tab6 tab-pane<?=$classe_groupe?>" >
												<h2  style="margin-top:0px;margin-bottom:15px;">Groupe</h2>
												<div class="dd">
													<ol class="dd-list">
														<li class="dd-item" data-id="<?=$maison_mere['fou_id']?>" data-niveau="-1">
															<div class="dd-handle" <?php if ($fournisseur['fou_id'] == $maison_mere['fou_id']) echo ("style='color:#e31936;'"); ?> >
																<?=$maison_mere['fou_nom']?>
													<?php		if (!empty($maison_mere['fso_fournisseur'])) {?>
																	<a href="fournisseur_voir.php?fournisseur=<?=$maison_mere['fou_id']?>" class=" pull-right btn btn-xs btn-info" >
																		<i class="fa fa-eye" ></i> Voir
																	</a>
														<?php	}?>
															</div>
															<ol class="dd-list">
													<?php		if (!empty($arrayfil[0])) {
        															foreach ($arrayfil[0] as $a) {?>
																		<li class="dd-item" data-id="<?=$a['fou_id']?>" data-niveau="<?=$a['fou_niveau']?>">
																			<div class="dd-handle" <?php if ($fournisseur['fou_id'] == $a['fou_id']) {?>style="color:#e31936;"<?php }?> >
																				<?=$a['fou_nom']?>
																		<?php	if (!empty($a['fso_fournisseur'])) {?>
																					<a href="fournisseur_voir.php?fournisseur=<?=$a['fou_id']?>" class=" pull-right btn btn-xs btn-info" >
																						<i class="fa fa-eye" ></i> Voir
																					</a>
																		<?php	}?>
																			</div>
																			<ol class="dd-list">
																	<?php 		if (!empty($arrayfil[1])) {
																					foreach ($arrayfil[1] as $a1) {
																						if ($a1['fou_fil_de'] == $a['fou_id']) {?>
																							<li class="dd-item" data-id="<?=$a1['fou_id']?>" data-niveau="<?=$a1['fou_niveau']?>">
																								<div class="dd-handle" <?php if ($fournisseur['fou_id'] == $a1['fou_id']) echo("style='color:#e31936;'"); ?> >
																									<?=$a1['fou_nom']?>
																					<?php			if (!empty($a1['fso_fournisseur'])) {?>
																										<a href="fournisseur_voir.php?fournisseur=<?=$a1['fou_id']?>" >
																											Voir
																										</a>
																					<?php			}?>
																								</div>
																								<ol class="dd-list">
																					<?php 			if (!empty($arrayfil[2])) {
																										foreach ($arrayfil[2] as $a2) {
																											if ($a2['fou_fil_de'] == $a1['fou_id']) {?>
																												<li class="dd-item" data-id="<?=$a2['fou_id']?>" data-niveau="<?=$a2['fou_niveau']?>">
																													<div class="dd-handle" <?php if ($fournisseur['fou_id'] == $a2['fou_id']) echo("style='color:#e31936;'"); ?> >
																														<?=$a2['fou_nom']?>
																													</div>
																													<ol class="dd-list">
																					<?php								if (!empty($arrayfil[3])) {
																															foreach ($arrayfil[3] as $a3) {
																																if ($a3['fou_fil_de'] == $a2['fou_id']) { ?>
																																	<li class="dd-item" data-id="<?=$a3['fou_id']?>" data-niveau="<?=$a3['fou_niveau']?>">
																																		<div class="dd-handle" <?php if ($fournisseur['fou_id'] == $a3['fou_id']) echo("style='color:#e31936;'"); ?> >
																																			<?=$a3['fou_nom']?>
																																		</div>
																		 															</li>
																					<?php										}                       
																															}
                       																									} ?>
																													</ol>
																												</li>
																							<?php 			}
                   																						}
                																					} ?>
																								</ol>
																							</li>
																	<?php 				}
																					}
																				} ?>
																			</ol>
																		</li>
												<?php 				}
   																} ?>
															</ol>
														</li>
													</ol>
												</div>
											</div>
						<?php 			} ?>
										<!-- FIN GROUPE -->

										<!-- GENERAL -->
										<div id="tab1" class="tab1 tab-pane<?=$classe_general?>" >

											<!-- Archivé -->
								<?php		if ($fournisseur['fou_archive'] == 1) {?>
												<div class="panel bg-danger light of-h mb10">
													<div class="pn pl20 p5">
														<div class="icon-bg">
															<i class="fa fa-archive" aria-hidden="true"></i>
														</div>
														<h2 class="mt15 lh15">
															<b>Ce fournisseur est archivé</b>
														</h2>
														<h5 class="text-muted">Pour le désarchiver, modifiez cette fiche et décochez "Archivé"</h5>
													</div>
												</div>
								<?php 		} ?>

											<!-- Blacklisté -->
								<?php 		if ($fournisseur['fou_blacklist'] == 1) { ?>
												<div class="panel bg-dark light of-h mb10">
													<div class="pn pl20 p5" style="min-height: 100px;">
														<div class="icon-bg">
															<i class="fa fa-times" aria-hidden="true"></i>
														</div>
														<h2 class="mt15 lh15">
															<b>Ce fournisseur a été blacklisté</b>
														</h2>
														<?php $u = get_utilisateur($fournisseur['fou_blacklist_uti']);?>
														<h5 class="text-muted">Blacklisté par <?=$u['uti_prenom']?> <?=$u['uti_nom']?>, pour la raison suivante : <?=$fournisseur['fou_blacklist_txt']?></h5>
													</div>
												</div>
								<?php 		} ?> 

											<!-- Controle admin -->
								<?php 		if (!empty($fournisseur_validation['fva_admin_etat'])) {
												// si success
												$bg_panel = " bg-info";
												if ($fournisseur_validation['fva_admin_etat'] == 1) {
													$bg_panel = " bg-danger";
												} elseif ($fournisseur_validation['fva_admin_etat'] == 2) {
													$bg_panel = " bg-warning";
												} elseif ($fournisseur_validation['fva_admin_etat'] == 3) {
													$bg_panel = " bg-success";
												} ?>

												<div class="etat_admin_color panel<?=$bg_panel?> light of-h mb10">
													<div class="pn pl20 p5">
														<div class="icon-bg">
															<i class="fa fa-info-circle" aria-hidden="true"></i>
														</div>
														<h2 class="mt15 lh15">
															<b>État administratif </b>
														</h2>

												<?php 	// si success
    													if ($fournisseur_validation['fva_admin_etat'] == 1) {?>
															<h5 class="etat_admin text-muted">Le fournisseur n'est pas valide, il faut vérifier l'état des documents suivant :</h5>
												<?php 	}elseif ($fournisseur_validation['fva_admin_etat'] == 2) {?>
															<h5 class="etat_admin text-muted">Le fournisseur est valide cependant il faut vérifier l'état des documents suivant :</h5>
												<?php 	}elseif ($fournisseur_validation['fva_admin_etat'] == 3) {?>
															<h5 class="etat_admin text-muted">Le fournisseur est valide.</h5>
												<?php	}
    													// afficher les documents non valides

   														if (!empty($fournisseur_validation['fva_admin_txt'])) {?>
															<p class="etat_admin_txt"><?=$fournisseur_validation['fva_admin_txt'];?></p>
												<?php 	}else {?>
															<p class="etat_admin_txt"></p>
												<?php 	}?>
													</div>
												</div>
							<?php 			}?>

											<h2 style="margin-top:0px;margin-bottom:15px;">Général</h2>

											<!-- INFOS PRINCIPALES -->
											<div class="row">
												<div class="col-md-12">
													<div class="panel">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon"></span>
															<span class="panel-title"> Informations principales</span>
														</div>
														<div class="panel-body">
															<div class="row">
																<div class="col-md-3">
																	<strong>Code :</strong> <?=$fournisseur['fou_code'];?>
																</div>
																<div class="col-md-3">
																	<strong>Nom :</strong> <?=$fournisseur['fou_nom'];?>
																</div>
																<div class="col-md-3">
																	<strong>Catégorie :</strong>
																	<?php if (!empty($fournisseur['fou_type'])) {?>
																		<?php foreach ($base_type_fournisseur as $k => $b) {?>
																			<?php if ($k == $fournisseur['fou_type']) {?>
																				<?=$b;?>
																			<?php }?>
																		<?php }?>
																	<?php } else {?>
																	<i>Pas de type</i>
																	<?php }?>
																</div>
																<div class="col-md-3">
																	<strong>Site Internet :</strong>
																	<?php if (!empty($fournisseur['fou_site'])) {?>
																		<a href="<?=$fournisseur['fou_site'];?>" target="_blank">
																			<?=str_replace(array("http://", "https://"), "", $fournisseur['fou_site'])?>
																		</a>
																	<?php } else {?>
																		<i>Pas de Site Internet</i>
																	<?php }?>

																</div>
															</div>

														</div>
													</div>
												</div>
												<!-- FIN INFOS PRINCIPALES -->
											</div>

											<!-- ADRESSE ET CONTACT -->
											<div class="row">
												<!-- ADRESSE -->
												<div class="col-md-4">
													<div class="panel">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon"></span>
															<span class="panel-title"> Adresse </span>
														</div>
														<div class="panel-body pb5">
												<?php 		if (empty($fournisseurs_adresse)) {?>
																<div class="alert alert-warning">
																	Pas d'adresse pour ce fournisseur.
																</div>
												<?php 		}else{?>
																<h4><?=$fournisseurs_adresse['fad_nom'];?></h4>
																<hr class="short br-lighter">
																<p class="text-muted">
																	<?=$fournisseurs_adresse['fad_ad1']?>
															<?php 	if (!empty($fournisseurs_adresse['fad_ad2'])) {?>
																		<br> <?=$fournisseurs_adresse['fad_ad2']?>
															<?php 	}
    																if (!empty($fournisseurs_adresse['fad_ad3'])) { ?>
																		<br> <?=$fournisseurs_adresse['fad_ad3']?>
															<?php 	} ?>
																	<br> <?=$fournisseurs_adresse['fad_cp']?> <?=$fournisseurs_adresse['fad_ville']?>
																</p>
													<?php 	} ?>
														</div>
													</div>
												</div>
												<!-- FIN ADRESSE -->

												<!-- CONTACT -->
												<div class="col-md-8">
													<div class="panel">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon"></span>
															<span class="panel-title">
												<?php 			if ($fournisseur['fou_type'] == 3) {?>
																	Contact
												<?php 			}else {?>
																	Contacts
												<?php 			} ?>
															</span>
														</div>
														<div class="panel-body pn">
															<table class="table table-striped text-center table_contact">
																<thead>
																	<tr>
																		<th style="text-align:center!important;">Fonction</th>
																		<th style="text-align:center!important;">Prénom et Nom</th>
																		<th style="text-align:center!important;">Email</th>
																		<th style="text-align:center!important;">Téléphone</th>
																		<th style="text-align:center!important;">Portable</th>
																		<th style="text-align:center!important;">Fax</th>
																		<th style="text-align:center!important;">Action</th>
																	</tr>
																</thead>
																<tbody id="contact_body">
															<?php	foreach ($fournisseurs_contacts as $f) 
																	{
																		if (!empty($f['fco_nom'])) 
																		{
																			if (!empty($f['cfo_id']))
																			{
																				$fonction_nom = $f['cfo_libelle'];
																			} else
																			{
																				$fonction_nom = $f['fco_fonction_nom'];
																			} ?>
																			<!-- si c'est le contact par défaut -->
																			<tr  id="contact_<?=$f['fco_id']?>" data-contact="<?=$f['fco_id']?>" <?php if ($f['fco_defaut'] == 1) echo ("class='primary'"); ?> >
																				<td class="table_contact_fonction"><?=$fonction_nom?></td>
																				<td class="table_contact_prenom_nom">
																					<?=$f['fco_prenom']?> <?=$f['fco_nom']?>
																				</td>
																				<td class="table_contact_mail">
																					<?=$f['fco_mail']?>
																				</td>
																				<td class="table_contact_tel">
																					<?=$f['fco_tel']?>
																				</td>
																				<td class="table_contact_portable">
																					<?=$f['fco_portable']?>
																				</td>
																				<td class="table_contact_fax">
																					<?=$f['fco_fax']?>
																				</td>
																				<td class="table_contact_btn" >
																					<a href="#" data-id="<?=$f['fco_id']?>" class="btn btn-xs btn-warning btn-contact">
																						<i class="fa fa-pencil"></i>
																					</a>
																		<?php 		if ($f['fco_defaut'] == 0)
																					{ ?>
																						<a href="#" data-id="<?=$f['fco_id']?>" class="btn btn-xs btn-danger btn-contact-del">
																							<i class="fa fa-times"></i>
																						</a>
																		<?php 		} ?>
																				</td>
																			</tr>
																<?php 	}
																	} ?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<!-- FIN CONTACT -->
											</div>

											<div class="row">
												<!-- Types d'achat -->
												<div class="col-md-6">
													<div class="panel">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon"></span>
															<span class="panel-title"> Types d'achat</span>
														</div>
														<div class="panel-body pb5">
												<?php 		foreach ($fournisseurs_familles as $ffa)
															 {?>
																<div class="col-md-6">
																	<div class="checkbox-custom checkbox-default mb5">
																		<input type="checkbox" class="select-famille" disabled name="ffj_famille" id="checkboxFamille<?=$ffa['ffa_id']?>" <?php if (!empty($ffa['ffj_famille'])) echo ("checked"); ?> />
																		<label style="cursor:default" for="checkboxFamille<?=$ffa['ffa_id']?>"><?=$ffa['ffa_libelle'];?></label>
																	</div>
																</div>
												<?php 		}?>
														</div>
													</div>
												</div>
												<!-- FIN Types d'achat -->

												<!-- COMPTA -->
												<div class="col-md-6">
													<div class="panel">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon"></span>
															<span class="panel-title"> Compta</span>
														</div>
														<div class="panel-body">
															<div class="row">
													<?php 		if ($_SESSION['acces']['acc_droits'][9]) {?>
																	<div class="col-md-4">
																		<strong>Code sage : </strong>
													<?php 				if (!empty($fournisseur['fou_sage'])) {?>
																			<?=$fournisseur['fou_sage'];?>
													<?php 				} else {?>
																			<i>Pas de code sage</i>
													<?php 				}?>
																	</div>
													<?php 		}?>
																<div class="col-md-4">
																	<strong>Siret : </strong>
																	<?php if (!empty($fournisseur['fou_siren']) && !empty($fournisseur['fou_siret'])) {?>
																		<?=$fournisseur['fou_siren'];?> <?=$fournisseur['fou_siret'];?>
																	<?php } else {?>
																		<i>Pas de siret</i>
																	<?php }?>
																</div>
																<div class="col-md-4">
																	<strong>Identification TVA : </strong>
																	<?php if (!empty($fournisseur['fou_tva'])) {?>
																		<?=$fournisseur['fou_tva']?>
																	<?php } else {?>
																		<i>Pas d'identification TVA</i>
																	<?php }?>
																</div>
															</div>
															<hr class="alt short">
															<div class="row">
																<div class="col-md-4">
																	<strong>IBAN : </strong>
																	<?php if (!empty($fournisseur['fou_iban'])) {?>
																		<?=$fournisseur['fou_iban']?>
																	<?php } else {?>
																		<i>Pas d'IBAN</i>
																	<?php }?>
																</div>
																<div class="col-md-4">
																	<strong>Montant maximum : </strong>
																	<?php if (!empty($fournisseur['fou_montant_max'])) {?>
																		<?=$fournisseur['fou_montant_max']?> €
																	<?php } else {?>
																		<i>Pas de montant maximum</i>
																	<?php }?>
																</div>
																<div class="col-md-4">
																	<strong>Mode de réglement : </strong>
																	<?php if (empty($fournisseur['fou_reg_type'])) {?>
																		<i>Pas de mode de règlement</i>
																	<?php } else {?>
																		<?php foreach ($base_reglement as $k => $n) {?>
																			<?php if ($k > 0) {?>
																				<?php if ($fournisseur['fou_reg_type'] == $k) {?><?=$n?><?php }?>
																			<?php }?>
																		<?php }?>
																	<?php }?>
																</div>
															</div>
															<hr class="alt short">
															<div class="row">
																<div class="col-md-4">
																	<strong>Type de règlement :</strong>

																	<?php if ($fournisseur['fou_reg_formule'] == 1) {?>

																	Date + <?php if (!empty($fournisseur['fou_reg_formule'])) {?><?=$fournisseur['fou_reg_nb_jour']?><?php } else {?>0<?php }?> jours

																	<?php }?>
																	<?php if ($fournisseur['fou_reg_formule'] == 2) {?>
																	Date + 45j + fin de mois
																	<?php }?>
																	<?php if ($fournisseur['fou_reg_formule'] == 3) {?>
																	Date + fin de mois + 45j
																	<?php }?>
																	<?php if ($fournisseur['fou_reg_formule'] == 4) {?>
																	Date + <?php if (!empty($fournisseur['fou_reg_nb_jour'])) {?><?=$fournisseur['fou_reg_nb_jour']?><?php } else {?>0<?php }?> jours + fin de mois + <?php if (!empty($fournisseur['fou_reg_fdm'])) {?><?=$fournisseur['fou_reg_fdm']?><?php } else {?>0<?php }?> jour(s)
																	<?php }?>

																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- FIN COMPTA -->
											</div>
										</div>
										<!-- FIN GENERAL -->

										<!-- PRODUITS ET SERVICES -->
										<div id="tab2" class="tab2 tab-pane<?=$classe_produit?>" >
											<h2  style="margin-top:0px;margin-bottom:15px;">Produits et services</h2>
											<div class="row" style="margin-bottom:25px;">
												<div class="col-md-12">
												<!-- fpr_type Types d'achat (fournisseurs_produits) -->
													<div class="col-md-2">
														<label for="fpr_type">Types d'achat :</label>

														<select name="fpr_type" id="fpr_type" class="select2" >
															<option value="0">Types d'achat...</option>
															<?php foreach ($fournisseurs_familles as $f): ?>
																<?php if(!empty($f['ffj_famille'])){ ?>
							                                    	<option value="<?=$f['ffa_id']?>" data-revente="<?=$f['ffa_type'];?>"><?=$f['ffa_libelle']?></option>
																<?php } ?>
							                                <?php endforeach;?>

														</select>
													</div>
													<div class="col-md-2 fpr_categorie"  style="display:none;">
														<label for="fpr_categorie">Catégorie de produit :</label>

														<select name="fpr_categorie" id="fpr_categorie" class="select2" >
															<option value="0">Catégorie de produit...</option>


														</select>
													</div>
													<div class="col-md-2 pro_famille" style="display:none;">
							                        	<label for="pro_famille">Famille :</label>
							                            <div class="section">

							                                <select id="pro_famille" name="pro_famille" class="select2 select2-famille">
							                                  <option value="0">Famille...</option>
							                                    <?php foreach ($produits_familles as $pfa): ?>
							                                      <option value="<?=$pfa['pfa_id']?>"><?=$pfa['pfa_libelle'];?></option>
							                                    <?php endforeach;?>
							                                </select>

							                            </div>
							                        </div>
							                        <div class="col-md-2 pro_sous_famille" style="display:none;">
							                        	<label for="pro_sous_famille">Sous-famille :</label>
							                            <div class="section">

							                                <select id="pro_sous_famille" name="pro_sous_famille" class="select2 select2-famille-sous-famille">
							                                    <option value="0">Sous-famille...</option>
							                                </select>

							                            </div>
							                        </div>
							                        <div class="col-md-2 pro_sous_sous_famille" style="display:none;">
							                        	<label for="pro_sous_sous_famille">Sous-sous-famille :</label>
							                            <div class="section">

							                                <select id="pro_sous_sous_famille" name="pro_sous_sous_famille" class="select2 select2-famille-sous-sous-famille select2-famille-sous-sous-fam">
							                                    <option value="0">Sous-sous-famille...</option>
							                                </select>

							                            </div>
							                        </div>

													<div class="col-sm-1">
														<button type="button" name="search_produit" id="search_produit" class="btn btn-primary mt20">Filtrer</button>



													</div>
													<div class="col-sm-1">
														<button type="button" id="search_produit_zero" class="btn btn-info mt20" style="display:none;">Remettre à zéro</button>

													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="table-responsive">
														<table class="table table-striped table-hover text-center" >
															<thead>
																<tr class="dark">
																	<th style="text-align:center!important;">Type d'achat</th>
																	<th style="text-align:center!important;">Catégorie</th>
																	<th style="text-align:center!important;">Référence produit SI2P</th>
																	<th style="text-align:center!important;">Référence du fournisseur</th>
																	<th style="text-align:center!important;">Libellé</th>
																	<th style="text-align:center!important;">Descriptif</th>
																	<th style="text-align:center!important;">Tarif (€)</th>
																	<th style="text-align:center!important;">Actions</th>
																</tr>
															</thead>
															<tbody id="produit_body">
																
																<?php foreach ($fournisseurs_produits as $p) {?>
																	<tr data-id="<?=$p['fpr_id']?>" data-type="<?=$p['fpr_type']?>" data-categorie="<?=$p['fpr_categorie']?>" data-famille="<?=$p['fpr_pro_famille']?>" data-sousfamille="<?=$p['fpr_pro_sous_famille']?>" data-soussousfamille="<?=$p['fpr_pro_sous_sous_famille']?>">
																		<td><?=get_fournisseur_famille($p['fpr_type'])?></td>
																		<?php 
																				$req = $Conn->prepare("SELECT * FROM fournisseurs_familles WHERE ffa_id =" . $p['fpr_type']);
																				$req->execute();
																				$ffamille = $req->fetch();
																		?>
																		<td>
																		<?php if(!empty($p['fpr_categorie'])){ ?>
																			
																				<?=get_fournisseur_categorie($p['fpr_categorie'])?>
																			
																		<?php } ?>
																		</td>
																		<td><?=$p['fpr_pro_reference'];?></td>
																		<td><?=$p['fpr_reference'];?></td>
																		<td><?=$p['fpr_libelle'];?></td>
																		<td><?=$p['fpr_descriptif'];?></td>
																		<td><?=$p['fpr_tarif'];?></td>
																		<td>
																			<?php if($ffamille["ffa_droit"] == 0 or $_SESSION["acces"]["acc_droits"][$ffamille["ffa_droit"]]){ ?>
																			<a href="fournisseur_produit.php?fournisseur=<?=$fournisseur_id?>&edition=<?=$p['fpr_id'];?>"  class="btn btn-warning btn-sm">
																				<i class="fa fa-pencil"></i>
																			</a>
																			<button type="button" id="produit_supprimer_btn" data-id="<?=$p['fpr_id']?>" data-toggle="modal" data-target="#fournisseur_produit_suppr_modal" class="btn btn-danger btn-sm">
																				<i class="fa fa-times"></i>
																			</button>
																			<?php }?>
																		</td>
																	</tr>
																<?php }?>
															</tbody>
														</table>
													</div>

												</div>
											</div>
										</div>
										<!-- FIN PRODUITS ET SERVICES -->

										<!-- ZONES D'INTERVENTIONS -->
										<div id="tab3" class="tab3 tab-pane<?=$classe_zone?>" >
											<h2  style="margin-top:0px;margin-bottom:15px;">Zones d'interventions</h2>
											<div class="row">
												<div class="col-md-8 text-center" style="margin:0;padding:0;height:600px;">
													<div id="francemap"></div>
												</div>
												<div class="col-md-4">
													<div class="panel">
														<div class="panel-heading">
															<span class="panel-icon">
																<i class="fa fa-info-circle"></i>
															</span>
															<span class="panel-title"> Légende</span>
														</div>
														<div class="panel-body pb5">
															<div class="row">
																<div class="col-md-6">
																	<p><span class="fa fa-circle" style="font-size:18px;color:#EC0000;"></span> <strong style="font-size:13px;"> Départements sélectionnés</strong></p>

																</div>
																<div class="col-md-6">
																	<p><span class="fa fa-circle" style="font-size:18px;color:#f2ede8;"></span> <strong style="font-size:13px;"> Départements non sélectionnés</strong></p>

																</div>
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
										<!-- FIN ZONES D'INTERVENTIONS -->

										<!-- DOCUMENTS -->
										<div id="tab4" class="tab5 tab-pane<?=$classe_doc?>" >
											<h2  style="margin-top:0px;margin-bottom:15px;">Documents</h2>
											<div class="row" style="margin-bottom:25px;">
												<div class="col-md-12">

												</div>
											</div>
											<div class="row">
												<div class="col-md-10">



													<div class="table-responsive">
														<table class="table table-striped table-hover text-center" >
															<thead>
																<tr class="dark">
																	<th style="text-align:center!important;">Nom du document</th>
																	<th style="text-align:center!important;">Télécharger</th>
																	<th style="text-align:center!important;">Mis en ligne le</th>
																	<th style="text-align:center!important;">Fin de validité le</th>
																	<th style="text-align:center!important;">Actions</th>
																</tr>
															</thead>
															<tbody id="document_body">

													<?php 		if(!empty($fournisseurs_documents)){
																	foreach ($fournisseurs_documents as $f) {
    																	// tous les documents de $fournisseur_id
																		if ($f['fdo_une_seule_fois'] == 1) {
																			$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_fichier = :ffi_fichier AND ffi_societe=0");
																			$req->bindParam(':ffi_fournisseur', $fournisseur_id);
																			$req->bindParam(':ffi_fichier', $f['fdo_id']);
																			$req->execute();
																		} else {
																			$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_fichier = :ffi_fichier AND ffi_societe = :ffi_societe");
																			$req->bindParam(':ffi_fournisseur', $fournisseur_id);
																			$req->bindParam(':ffi_fichier', $f['fdo_id']);
																			$req->bindParam(':ffi_societe', $_SESSION['acces']['acc_societe']);
																			$req->execute();
																		}
																		$fichier = $req->fetch(); ?>
																		<tr data-id="<?=$f['fdo_id']?>"
																	<?php 	if ($f['fdo_obligatoire'] == 1) {?>
																				style="font-weight:bold;"
																	<?php 	} ?>
																	<?php 	if (($f['fdo_obligatoire'] == 1 && empty($fichier)) or ($f['fdo_periodicite_mois'] != 0 && date('Y-m-d') > date('Y-m-d', strtotime("+" . $f['fdo_periodicite_mois'] . " months", strtotime($fichier['ffi_date']))))) {?>
																				class="danger"
																	<?php 	}else{?>
																				class="success"
																	<?php 	}?>
																		>
																			<td><?=$f['fdo_libelle']?></td>
																	<?php 	if (!empty($fichier)) {
       																			$filepath = $fichier['ffi_fournisseur'] . "/" . $fichier['ffi_societe'] . "_" . $f['fdo_id'] . "_" . clean($fichier['ffi_nom']) . "." . $fichier['ffi_ext'];
        																		// nom du fichier ?>
																				<td class="document_dl_<?=$fichier['ffi_fichier']?>"><a href="documents/fournisseurs/<?=$filepath?>" download class="btn btn-sm btn-warning"><i class="fa fa-download"></i></a></td>
																	<?php 	}else{?>
																				<td><i>Pas de document</i></td>
																	<?php 	}
																			if (!empty($fichier)) {?>
																				<td class="document_date_<?=$fichier['ffi_fichier']?>"><?=convert_date_txt($fichier['ffi_date']);?></td>
																	<?php 	} else {?>
																				<td><i>Pas de document</i></td>
																	<?php 	}
																		 	if (!empty($fichier)) {
																				if ($f['fdo_periodicite_mois'] == 0) { // check la périodicité ?>
																					<td class="document_fin_<?=$fichier['ffi_fichier']?>">Jamais</td>
																	<?php 		} else {
      																				$fichier['ffi_date'] = date('Y-m-d', strtotime("+" . $f['fdo_periodicite_mois'] . " months", strtotime($fichier['ffi_date'])));?>
																					<td  class="document_fin_<?=$fichier['ffi_fichier']?>"><?=convert_date_txt($fichier['ffi_date']);?></td>
																	<?php 		}
																			}else {?>
																				<td><i>Pas de document</i></td>
																	<?php 	}
																			if (!empty($fichier)) {?>
																				<td class="document_actions_<?=$fichier['ffi_fichier']?>">
																					<a href="fournisseur_doc.php?fournisseur=<?=$fournisseur_id?>&id=<?=$f['fdo_id']?>&fichier"  class="btn btn-sm btn-warning" id="document_edition"><i class="fa fa-pencil"></i>
																					</a>
																					<a href="#" data-toggle="modal" data-target="#fournisseur_doc_suppr_modal" data-societe="<?=$fichier['ffi_societe']?>" data-id="<?=$f['fdo_id']?>" class="btn btn-sm btn-danger" id="document_suppr"><i class="fa fa-times"></i>
																					</a>
																				</td>
																	<?php 	}else{?>
																				<td><a href="fournisseur_doc.php?fournisseur=<?=$fournisseur_id?>&id=<?=$f['fdo_id']?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a></td>
																	<?php 	} ?>
																		</tr>
															<?php 	}
																} ?>
															</tbody>
													</table>
												</div>
											</div>
											<div class="col-md-2">
												<div class="panel">
													<div class="panel-heading">
														<span class="panel-icon">
															<i class="fa fa-info-circle"></i>
														</span>
														<span class="panel-title"> Légende</span>
													</div>
													<div class="panel-body pb5">
														<div class="row">
															<div class="col-md-12">
																<p><span class="fa fa-circle" style="color:#afe1a8;font-size:17px;"></span> <span style="font-size:13px;"> Document valide</span></p>
															</div>
															<div class="col-md-12">
																<p><span style="font-weight:bold;font-size:17px;">Gras : </span> <strong style="font-weight:bold;font-size:13px;"> Document obligatoire</strong></p>
															</div>
															<div class="col-md-12">
																<p><span class="fa fa-circle" style="color:#eea79c;font-size:17px;"></span> <span style="font-size:13px;"> Document manquant ou non valide</span></p>
															</div>
															<!-- <div class="col-md-12">
																<p><span class="fa fa-circle" style="color:#fadb9a;font-size:17px;"></span> <span style="font-size:13px;"> Document manquant ou non valide mais optionnel</span></p>
															</div> -->


														</div>
													</div>
												</div>
											</div>
										</div>

										</div>
										<!-- FIN DOCUMENTS -->

										<!-- INTERVENANTS EXTERIEURS -->
										<div id="tab5" class="tab5 tab-pane<?=$classe_intervenant?>" >
											<h2  style="margin-top:0px;margin-bottom:15px;">
									<?php	if ($fournisseur['fou_type'] == 3) {?>
													Fiche intervenant
									<?php } else {?>
													Intervenants exterieurs
									<?php }?>
											</h2>

											<section class="table-layout" >

												<!-- LISTE DES INTERVENANTS -->
												<aside class="tray tray-left" style="vertical-align:top;<?php if ($fournisseur['fou_type'] == 3) {
    echo ("display:none;");
}
    ?>" >
													<div id="nav-spy">
														<h4>Liste des intervenants</h4>
														<ul class="nav tray-nav affix-top" >
													<?php $int_aff = 0;
    foreach ($fournisseurs_intervenants as $i => $f) {
        $class = "";
        if (($i == 0 && !isset($_GET['intervenant'])) or (isset($_GET['intervenant']) && $_GET['intervenant'] == $f['fin_id'])) {
            $class = " active";
            $int_aff = $i;
        }
        $style_a = "";
        if ($f['fin_archive'] == 1) {
            $style_a = "class='bg-danger' style='color:#FFF;'";
        }
        ?>
																<li id="data_intervenant_<?=$f['fin_id']?>" class="intervenant-liste<?=$class?>" data-id="<?=$f['fin_id']?>" >
																	<a href="#p1" <?=$style_a?> >
																		<span id="fin_prenom"><?=$f['fin_prenom']?></span> <span id="fin_nom"><?=$f['fin_nom']?></span>
																	</a>
																</li>
													<?php }?>
														</ul>
													</div>
												</aside>
												<div class="tray tray-center p15" style="vertical-align:top;" >
										<?php foreach ($fournisseurs_intervenants as $i => $f) {

        // verification du statut de l'intervenant
        ////////////////////////////// MISE A JOUR COMPETENCES ////////////////////////////////
        // LISTE DES DIPLOMES
        $sql = "SELECT idi_date_deb, idi_date_fin,dip_libelle,idi_fichier,dip_id FROM diplomes
														LEFT JOIN intervenants_diplomes ON (diplomes.dip_id = intervenants_diplomes.idi_diplome)
														WHERE idi_ref = 2 AND idi_ref_id = " . $f['fin_id'] . " ORDER BY dip_libelle";
        $req = $Conn->query($sql);
        $diplomes = $req->fetchAll();

        // COMPETENCES VALIDEES
        $sql = "SELECT * FROM competences
														LEFT JOIN Intervenants_Competences ON(Competences.com_id=Intervenants_Competences.ico_competence)
														 WHERE  Intervenants_Competences.ico_ref=2 AND Intervenants_Competences.ico_ref_id=" . $f['fin_id'] . " ORDER BY com_libelle";
        $req = $Conn->query($sql);
        $competences_validees = $req->fetchAll();
        // COMPETENCES PAS ENCORE ACQUISES
        $sql = "SELECT * FROM competences
														LEFT OUTER JOIN Intervenants_Competences ON(Competences.com_id=Intervenants_Competences.ico_competence AND Intervenants_Competences.ico_ref=2 AND Intervenants_Competences.ico_ref_id=" . $f['fin_id'] . ")
														WHERE ico_competence IS NULL ORDER BY com_libelle";
        $req = $Conn->query($sql);
        $competences_non_validees = $req->fetchAll();

        // verification du statut de l'intervenant ?>

														<div id="intervenant_panels_<?=$f['fin_id']?>" class="intervenant-panels" <?php if ($int_aff != $i) {
            echo ("style='display:none;'");
        }
        ?> >
															<h2 <?php if ($fournisseur['fou_type'] == 3) {
            echo ("id='fin_ident'");
        }
        ?> ><?=$f['fin_prenom'] . " " . $f['fin_nom']?></h2>
															<div class="row">
																<div class="col-md-12">
																	<div class="panel">
																		<div class="panel-heading">
																			<span class="panel-icon">
																				<i class="fa fa-info-circle"></i>
																			</span>
																			<span class="panel-title"> Informations de l'intervenant</span>
																			<span class="pull-right"><a href="fournisseur_intervenant.php?fournisseur=<?=$fournisseur_id?>&id=<?=$f['fin_id']?>" data-toggle="tooltip" data-title="Modifier les informations de l'intervenant" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a></span>
																		</div>
																		<div class="panel-body">
																			<div class="row">
																				<div class="col-md-3 mb10 mt10">
																					<?=$f['fin_ad1']?><br>
																					<?php if (!empty($f['fin_ad2'])) {?>
																						<?=$f['fin_ad2']?><br>
																					<?php }?>
																					<?php if (!empty($f['fin_ad3'])) {?>
																						<?=$f['fin_ad3']?><br>
																					<?php }?>
																					<?=$f['fin_cp']?> - <?=$f['fin_ville']?>
																				</div>
																				<div class="col-md-2">
																					<div class="text-center" data-toggle="tooltip" data-placement="bottom" data-original-title="Téléphone">
																						<a href="#">
																							<span class="fa fa-phone fs35 text-muted"></span>
																						</a>
																						<p>
																							<span id="fin_tel"><?=$f['fin_tel']?><br></span>
																						</p>
																					</div>
																				</div>

																				<div class="col-md-2">
																					<div class="text-center" data-toggle="tooltip" data-placement="bottom" data-original-title="Portable">
																						<a href="#">
																							<span class="fa fa-mobile fs35 text-info"></span>
																						</a>
																						<p>
																							<span id="fin_portable"><?=$f['fin_portable']?></span>
																						</p>
																					</div>
																				</div>

																				<div class="col-md-2">
																					<div class="text-center" data-toggle="tooltip" data-placement="bottom" data-original-title="Fax">
																						<a href="#">
																							<span class="fa fa-fax fs35 text-system"></span>
																						</a>
																						<p>
																							<span id="fin_fax"><?=$f['fin_fax']?></span>
																						</p>
																					</div>
																				</div>
																				<div class="col-md-2">
																					<div class="text-center" data-toggle="tooltip" data-placement="bottom" data-original-title="Email">
																						<a href="#">
																							<span class="fa fa-envelope-square fs35 text-primary"></span>
																						</a>
																						<p>
																							<span id="fin_mail"><?=$f['fin_mail']?></span>
																						</p>
																					</div>
																				</div>

																			</div>

																		</div>
																	</div>
																	<!-- FIN INFOS DE L'INTERVENANTS -->
																</div>
															</div>


															<!-- DIPLOMES DE L'INTERVENANT -->
															<!--<div class="row">
																<div class="col-md-6">
																	<div class="panel">
																		<div class="panel-heading">
																			<span class="panel-icon">
																				<i class="fa fa-university" aria-hidden="true"></i>
																			</span>
																			<span class="panel-title"> Diplômes de l'intervenant</span>
																			<span class="pull-right"><a href="fournisseur_intervenant_diplome.php?fournisseur=<?=$fournisseur_id?>&intervenant=<?=$f['fin_id']?>" data-toggle="tooltip" data-title="Ajouter un intervenant" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a></span>
																		</div>
																		<div class="panel-body pn">
																			<div class="table-responsive">
																				<table class="table table-striped table-hover text-center" >
																					<thead>
																						<tr class="dark">
																							<th style="text-align:center!important;">Nom</th>
																							<th style="text-align:center!important;">Fin de validité</th>
																							<th style="text-align:center!important;">pdf</th>
																							<?php	if ($_SESSION['acces']['acc_droits'][3]) {?>
																								<th style="text-align:center!important;">Actions</th>
																							<?php	}?>
																						</tr>
																					</thead>
																					<tbody  id="diplome_body">
																						<?php foreach ($diplomes as $d) {
            $classe_doc = "";
            $classe_doc = "";
            $idi_date_fin = "&nbsp;";

            $class_perime = "";
            $style_perime = "";
            if (!empty($d["idi_date_fin"])) {
                $idi_date_fin = convert_date_txt($d["idi_date_fin"]);
                if ($d["idi_date_fin"] < date("Y-m-d")) {
                    $class_perime = "danger";
                    //$style_perime="style='color:#FFF;'";
                }
            }

            ?>
																						<tr class="text-center <?=$class_perime?>">
																							<td><?=$d["dip_libelle"]?></td>
																							<td class="text-center  <?=$style_perime?>" ><?=$idi_date_fin?></td>
																	<?php	if (!empty($d["idi_fichier"]) and file_exists("documents/intervenants/diplomes/" . $d["idi_fichier"])) {?>
																								<td>
																									<a href="documents/intervenants/diplomes/<?=$d["idi_fichier"]?>" download target="_blank" class="btn btn-success btn-sm">
																										<i class="fa fa-file-pdf-o"></i>
																									</a>
																								</td>
																	<?php	} else {
                echo ("<td>&nbsp;</td>");
            }
            ;
            if ($_SESSION['acces']['acc_droits'][3]) {?>
																								<td>
																									<a href="fournisseur_intervenant_diplome.php?ref=2&ref_id=<?=$f['fin_id']?>&diplome=<?=$d["dip_id"]?>&fournisseur=<?=$fournisseur_id?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom"  title="Modifier" >
																									   <i class="fa fa-pencil"></i>
																									</a>
																									<button type="button" class="btn btn-xs btn-danger diplome-supp" data-diplome="<?=$d["dip_id"]?>" data-intervenant="<?=$f['fin_id']?>" data-toggle="tooltip" title="Supprimer le diplôme" data-placement="bottom">
																										<i class="fa fa-times"></i>
																									</button>
																								</td>
																	<?php	}?>
																						</tr>
																	<?php	}?>
																					</tbody>
																				</table>
																			</div>
																		</div>
																	</div>
																</div>-->
																<!-- FIN DIPLOMES DE L'INTERVENANT -->

																<!-- DOCUMENTS DE L'INTERVENANT -->
																<div class="col-md-6">
																	<div class="panel">
																		<div class="panel-heading">
																			<span class="panel-icon">
																				<i class="fa fa-file-text" aria-hidden="true"></i>
																			</span>
																			<span class="panel-title"> Documents de l'intervenant</span>
																		</div>
																		<div class="panel-body pn">
																			<div class="table-responsive">
																				<table class="table table-striped table-hover text-center" >
																					<thead>
																						<tr class="dark">
																							<th style="text-align:center!important;">Nom</th>
																							<th style="text-align:center!important;">Validité</th>
																							<th style="text-align:center!important;">Actions</th>
																						</tr>
																					</thead>
																					<tbody id="document_intervenant_body">
																			<?php 		if(!empty($fournisseurs_documents_intervenants)){
																							foreach ($fournisseurs_documents_intervenants as $fdi) {
																								// tous les documents de l'intervenant
																								$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_intervenant = :ffi_intervenant AND ffi_fichier = :ffi_fichier");
																								$req->bindParam("ffi_fournisseur", $fournisseur_id);
																								$req->bindParam("ffi_intervenant", $f['fin_id']);
																								$req->bindParam("ffi_fichier", $fdi['fdo_id']);
																								$req->execute();
																								$doc_intervenant = $req->fetch(); ?>
																								<tr data-document="<?=$fdi['fdo_id']?>" data-intervenant="<?=$f['fin_id']?>"
																						<?php 		if ($fdi['fdo_obligatoire'] == 1) {?>
																										style="font-weight:bold;"
																						<?php		 }?>
																						<?php		if (($fdi['fdo_obligatoire'] == 1 && empty($doc_intervenant)) or ($fdi['fdo_periodicite_mois'] != 0 && date('Y-m-d') > date('Y-m-d', strtotime("+" . $fdi['fdo_periodicite_mois'] . " months", strtotime($doc_intervenant['ffi_date']))))) {?>
																										class="danger"
																						<?php 		}else {?>
																										class="success"
																						<?php 		}?>
																								>
																									<td><?=$fdi['fdo_libelle']?></td>
																						<?php		// verification de l'état du diplome
																									if ($fdi['fdo_periodicite_mois'] == 0) {
																										// si pas de validité
																										$date_validite = "";
																									} else {
																										// sinon rajouter la durée de validite
																										$date_validite = date('Y-m-d', strtotime("+" . $fdi['fdo_periodicite_mois'] . " months", strtotime($doc_intervenant['ffi_date'])));
																									}
																									if (!empty($doc_intervenant)) { 
																										// si il n'y a pas de document pour l'intervenant ?>
																										<td class="document_intervenants_date_<?=$doc_intervenant['ffi_fichier']?>_<?=$f['fin_id']?>"><?=convert_date_txt($date_validite);?></td>
																										<td class="document_intervenants_actions_<?=$doc_intervenant['ffi_fichier']?>_<?=$f['fin_id']?>">
																											<a href="documents/fournisseurs/<?=$fournisseur_id?>/intervenants/<?=$f['fin_id']?>/<?=$fdi['fdo_id']?>_<?=clean($doc_intervenant['ffi_nom'])?>.<?=$doc_intervenant['ffi_ext']?>" download="" class="btn btn-xs btn-warning">
																												<i class="fa fa-download"></i>
																											</a>
																											<a href="fournisseur_doc_intervenant.php?fournisseur=<?=$fournisseur_id?>&id=<?=$fdi['fdo_id']?>&intervenant=<?=$f['fin_id']?>"  class="btn btn-xs btn-warning" id="document_edition"><i class="fa fa-pencil"></i>
																											</a>
																											<a href="#" data-toggle="modal" data-target="#fournisseur_doc_intervenant_suppr_modal" data-id="<?=$fdi['fdo_id']?>" data-intervenant="<?=$f['fin_id']?>" class="btn btn-xs btn-danger" id="document_intervenant_suppr"><i class="fa fa-times"></i>
																											</a>
																										</td>
																							<?php 	}else {?>
																										<td>Pas de document</td>
																										<td>
																											<a href="fournisseur_doc_intervenant.php?fournisseur=<?=$fournisseur_id?>&id=<?=$fdi['fdo_id']?>&intervenant=<?=$f['fin_id']?>" class="btn btn-xs btn-success">
																												<i class="fa fa-plus"></i>
																											</a>
																										</td>
																							<?php 	} ?>
																								</tr>
																				<?php	  }
																						} ?>
																					</tbody>
																				</table>
																			</div>
																		</div>
																	</div>
																</div>
															</div>-->
															<!-- FIN DOCUMENTS DE L'INTERVENANT -->


															<!-- COMPETENCES DE L'INTERVENANT -->
															<!--<div class="row">
																<div class="col-md-4">
																	<form method="GET" action="fournisseur_intervenant_competence.php">
																		<div class="panel">
																			<div class="panel-heading">
																				<span class="panel-icon">
																					<i class="fa fa-trophy" aria-hidden="true"></i>
																				</span>
																				<span class="panel-title"> Consulter les diplômes liés à une compétence</span>
																			</div>
																			<div class="panel-body">
																				<input type="hidden" name="ref_id" value="<?=$f['fin_id']?>">
																				<input type="hidden" name="ref" value="2">
																				<input type="hidden" name="fournisseur" value="<?=$fournisseur_id?>">
																				<div class="section mb15">
																					<select id="uti_competence" class="select2" name="uti_competence" required>
																					<option value="">Choix de la compétence...</option>					<?php foreach ($competences_non_validees as $c) {?>
																							<option value="<?=$c['com_id']?>"><?=$c['com_libelle']?></option>
																						<?php }?>
																					</select>
																				</div>
																				<button type="submit" class="btn btn-sm btn-success">Afficher les diplômes</button>
																			</div>
																		</div>
																	</form>
																</div>
																<div class="col-md-8">
																	<div class="panel">
																		<div class="panel-heading">
																			<span class="panel-icon">
																				<i class="fa fa-trophy" aria-hidden="true"></i>
																			</span>
																			<span class="panel-title"> Compétences de l'intervenant</span>
																		</div>
																		<div class="panel-body">
																			<div class="row text-center">
																			<?php
if (!empty($competences_validees)) {
            $numOfCols = 6;
            $rowCount = 0;
            foreach ($competences_validees as $c) {?>
																					<div class="col-md-2">
																						<p title="Cliquez pour plus d'informations">
																							<a href="fournisseur_intervenant_competence.php?ref=2&ref_id=<?=$f['fin_id']?>&uti_competence=<?=$c['com_id']?>&fournisseur=<?=$fournisseur_id?>" id="check_comp_<?=$c["com_id"]?>">
																							<span id='competence_<?=$c["com_id"]?>' class='fa fa-circle text-success' style='font-size:25px;'></span>
																							</a>
																						</p>
																						<p><?=$c["com_libelle"]?></p>
																					</div>
																<?php	$rowCount++;
                if ($rowCount % $numOfCols == 0) {
                    echo '</div><div class="row text-center mb15">';
                }

            }
        }?>
																			</div>
																		</div>
																	</div>
																</div>
															</div>-->
															<!-- FIN COMPETENCES DE L'INTERVENANT -->
														</div>
											<?php }?>
												</div>
											</section>
										</div>
										<!-- FIN INTERVENANTS EXTERIEURS -->

									</div>
								</div>
							</div>
						</div>
		<?php }?>
				</section>
			</section>
		</div>

<?php // FOOTER
if ($non_accessible == 0) {?>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<?php if (!empty($_SESSION['retour'])) {?>
						<a href="<?=$_SESSION['retour'];?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
						<?php }?>

					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right">
						<!-- ZONES -->
						<a href="fournisseur_region.php?fournisseur=<?=$fournisseur_id?>&ajout=1" class="btn btn-success btn-sm btn-3" <?=$style_zone?> >
							<i class="fa fa-plus" aria-hidden="true"></i> Ajouter une région Si2P
						</a>
						<a href="fournisseur_region.php?fournisseur=<?=$fournisseur_id?>&ajout=0" class="btn btn-warning btn-sm btn-3" <?=$style_zone?> >
							<i class="fa fa-minus" aria-hidden="true"></i> Supprimer une région Si2P
						</a>

						<!-- PRODUITS -->
				<?php if (!empty($fournisseurs_familles)) {?>
							<a  class="btn btn-success btn-sm btn-2" href="fournisseur_produit.php?fournisseur=<?=$fournisseur_id?>" <?=$style_produit?> ><i class="fa fa-plus"></i> Produit</a>
				<?php	}?>

						<!-- INTERVENANTS EXTERIEURS -->
				<?php if ($fournisseur['fou_type'] != 3 or empty($fournisseurs_intervenants)) {?>
							<a href="fournisseur_intervenant.php?fournisseur=<?=$fournisseur_id?>" class="btn btn-success btn-sm btn-5" <?=$style_intervenant?> ><i class="fa fa-plus"></i> Intervenant</a>
				<?php }?>

						<!-- GENERAL -->
				<?php if ($fournisseur['fou_type'] != 3 or empty($fournisseurs_intervenants)) {?>
							<a href="#" data-id="0" class="btn btn-success btn-sm btn-1 btn-contact" <?=$style_general?> ><i class="fa fa-plus"></i> Contact</a>
				<?php }?>
						<a href="fournisseur_mod.php?id=<?=$fournisseur_id;?>" class="btn btn-warning btn-sm btn-1" <?=$style_general?> ><i class="fa fa-pencil"></i> Editer</a>

					</div>
				</div>
			</footer>
<?php }?>

		<div id="modal_confirmation_user" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" >
							<i class="fa fa-info" ></i> - <span>Confirmation</span>
						</h4>
					</div>
					<div class="modal-body" >

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
							<i class="fa fa-close" ></i>
							Annuler
						</button>
						<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
							<i class="fa fa-check" ></i>
							Confirmer
						</button>
					</div>
				</div>
			</div>
		</div>

		<!-- MODAL FOURNISSEURS CONTACTS -->
		<div id="fournisseurs_contacts_modal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<form id="form_contact" action="ajax/ajax_fournisseur_contact_set.php" method="post" class="admin-form form-inline form-inline-grid">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title contact-title">Contact</h4>
						</div>

						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">


								<input type="hidden" class="champ-contact" id="con_id" value="0" name="con_id">
								<input type="hidden" id="fournisseur" name="fournisseur" value="<?=$fournisseur_id?>">
									<label>Civilité</label>
									<select id="con_titre" name="con_titre" class="form-control champ-contact">

										<?php foreach ($base_civilite as $b => $c) {?>
											<?php if ($b > 0) {?>
												<option value="<?=$b?>"><?=$c?></option>
											<?php }?>
										<?php }?>
									</select>


								</div>

								<div class="col-md-6">

									<label>Nom</label>
									<label class="field">
										<input type="text" id="con_nom" name="con_nom" class="gui-input nom champ-contact" placeholder="Nom" required>
									</label>


								</div>

								<div class="col-md-6">
									<label>Prénom</label>
									<label class="field">
										<input type="text" id="con_prenom" name="con_prenom" class="gui-input prenom champ-contact" placeholder="Prénom" required>
									</label>


								</div>
							</div>
							<div class="row">
								<div class="col-md-6">


									<label>Fonction</label>
									<select id="con_fonction" name="con_fonction" class="form-control champ-contact" onchange="nomchange(this.value)">
										<option value="0">Sélectionner une fonction...</option>
										<?php if (isset($_GET['id'])) {?>
										<?=get_contact_fonction_select($contact['con_fonction']);?>
										<?php } else {?>
										<?=get_contact_fonction_select(0);?>
										<?php }?>
										<option value="autre">Autre...</option>
									</select>


								</div>
								<div class="col-md-6 con_fonction_nom_style">
									<label>Autre fonction</label>
									<div class="field prepend-icon">
										<input type="text" name="con_fonction_nom" id="con_fonction_nom" class="gui-input champ-contact" placeholder="Nouvelle fonction">
										<label for="con_fonction_nom" class="field-icon">
											<i class="fa fa-tag"></i>
										</label>
									</div>

								</div>

							</div>



							<div class="row">
								<div class="col-md-6">

									<label>Téléphone</label>
									<label class="field">
										<input type="text" id="con_tel" name="con_tel" class="gui-input telephone champ-contact" placeholder="Tél">
									</label>


								</div>
								<div class="col-md-6">

									<label>Portable</label>
									<label class="field">
										<input type="text" id="con_portable" name="con_portable" class="gui-input telephone champ-contact" placeholder="Portable">
									</label>


								</div>
								<div class="col-md-6">

									<label>Fax</label>
									<label class="field">
										<input type="text" id="con_fax" name="con_fax" class="gui-input telephone champ-contact" placeholder="Fax">
									</label>


								</div>
								<div class="col-md-6">

									<div class="section">
										<label>Mail</label>
										<label class="field">
											<input type="email" id="con_mail" name="con_mail" class="gui-input champ-contact" placeholder="Email">
										</label>
									</div>

								</div>
								<div class="col-md-12 fad_contact_edit">
									<div class="section">


										<label class="option block mn">
											<input type="checkbox" name="con_defaut" class="fad_contact_edit_check champ-contact" value="con_defaut">
											<span class="checkbox mn"></span> Contact par défaut
										</label>
									</div>
								</div>

							</div>



						</div>

						<div class="modal-footer">
							<button type="button" name="edition" class="btn btn-success btn-sm btn-contact-submit"> <i class="fa fa-floppy-o"></i> Enregistrer</button>
							<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
						</div>

					</div>
				</form>
			</div>
		</div>
		<!-- FIN MODAL CONTACT -->

		<!-- MODAL SUPPRESSION PRODUIT -->
		<div id="fournisseur_produit_suppr_modal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Supprimer un produit</h4>
					</div>

					<div class="modal-body">
						Êtes-vous sûr de vouloir supprimer ce produit ?

					</div>

					<div class="modal-footer">
						<button type="button" id="produit_supprimer" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</a>
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
					</div>
				</div>

			</div>
		</div>
		<!-- FIN MODAL SUPPRESSION PRODUIT -->


		<!-- MODAL SUPPRESSION DOCUMENT -->
		<div id="fournisseur_doc_suppr_modal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Supprimer un document</h4>
					</div>

					<div class="modal-body">
						Êtes-vous sûr de vouloir supprimer ce document ?

					</div>

					<div class="modal-footer">
						<button type="button" id="doc_supprimer" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</button>
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
					</div>
				</div>

			</div>
		</div>
		<!-- FIN MODAL SUPPRESSION DOCUMENT -->

		<!-- MODAL SUPPRESSION DOCUMENT INTERVENANT -->
		<div id="fournisseur_doc_intervenant_suppr_modal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Supprimer un document d'un intervenant</h4>
					</div>

					<div class="modal-body">
						Êtes-vous sûr de vouloir supprimer ce document ?

					</div>

					<div class="modal-footer">
						<button type="button" id="doc_supprimer_intervenant" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</button>
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
					</div>
				</div>

			</div>
		</div>
		<!-- FIN MODAL SUPPRESSION DOCUMENT INTERVENANT -->

		<!-- MODAL SUPPRESSION DOCUMENT -->
		<div id="fournisseur_contact_suppr_modal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Supprimer un contact</h4>
					</div>

					<div class="modal-body">
						Êtes-vous sûr de vouloir supprimer ce contact ?

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-success btn-sm btn-contact-del-submit"> <i class="fa fa-check"></i> Oui</a>
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
					</div>
				</div>

			</div>
		</div>
		<!-- FIN MODAL SUPPRESSION DOCUMENT -->

		<!-- MODAL SUPPRESSION DIPLOME -->
		<div id="fournisseur_diplome_suppr_modal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Supprimer un diplôme</h4>
					</div>

					<div class="modal-body">
						Êtes-vous sûr de vouloir supprimer ce diplôme ?

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-success btn-sm btn-diplome-del-submit"> <i class="fa fa-check"></i> Oui</a>
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
					</div>
				</div>

			</div>
		</div>
		<!-- FIN MODAL SUPPRESSION DIPLOME -->

		<!-- MODAL COMPETENCES VISU-->
		<div id="fournisseurs_competences_modal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Forcer la validation</h4>
					</div>

					<div class="modal-body pn">
						<div class="row">
							<div class="col-md-12 text-center">
								<div class="section">
									<h4>Forcer la validation de la compétence</h4>
									<div class="switch switch-info switch-lg switch-inline">

										<input id="force_validation" type="checkbox" name="force_validation">
										<label for="force_validation"></label>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Ok</button>
					</div>
				</div>

			</div>
		</div>
		<!-- FIN MODAL COMPETENCES VISU -->



<?php	include "includes/footer_script.inc.php";?>

		<!-- CARTE DE FRANCE -->
		<script src="vendor/plugins/carte-france/dist/jquery.vmap.js" type="text/javascript"></script>


		<script src="vendor/plugins/mask/jquery.mask.js"></script>

		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="vendor/plugins/pnotify/pnotify.js"></script>
		<script src="vendor/plugins/nestable/jquery.nestable.js"></script>

		<!-- Theme Javascript -->
		<script src="assets/js/custom.js"></script>

		<script src="assets/js/responsive-tabs.js"></script>
		<script src="vendor/plugins/carte-france/dist/maps/jquery.vmap.france.js" type="text/javascript"></script>
		<script src="vendor/plugins/carte-france/dist/jquery.vmap.colorsFrance.js" type="text/javascript"></script>
		<script src="vendor/plugins/summernote/summernote.min.js"></script>
		<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
		<script src="assets/js/orion.js"></script>

		<script type="text/javascript">
		////////////////////////////// FONCTIONS  //////////////////////////////
		var fournisseur="<?=$fournisseur_id?>";
		var html="";
		var map = "";
		function initFranceMap(){
			map = jQuery('#francemap').vectorMap({
				map: 'france_fr',
				hoverOpacity: 0.5,
				hoverColor: true,
				backgroundColor: "#ffffff",
				/*colors: couleurs,*/
				<?php if (!empty($fournisseurs_interventions_json)) {?>
					selectedRegions: <?=$fournisseurs_interventions_json?>,
				<?php }?>
					borderColor: "#000000",
					selectedColor: "#EC0000",
					enableZoom: true,
					showTooltip: true,
					showLabels: true,
					multiSelectRegion: true,
					onRegionSelect: function(event, code, region)
					{
						departement(map.selectedRegions);
					},
					onRegionDeselect: function(event, code, region)
					{

						departement(map.selectedRegions);
					},

			});
		}
		jQuery(document).ready(function (){

			// Init map
			if($(".tab3").hasClass("active")){
				initFranceMap();
			}
			// ONGLET
			$(".tab-clic").click(function(){
				//update resize map
				if(map == "" && $(this).hasClass("tab3")){
					setTimeout(	function()	{
						initFranceMap();
					}, 50);
				}
					
				onglet=$(this).data("tab");
				$('.footer-right .btn').each(function(){
					if($(this).hasClass("btn-" + onglet)){
						$(this).show();
					}else{
						$(this).hide();
					}
				});

			});

			// CONTACT

			// si on clique sur un des boutons contact (ajout ou edition)
			$(document).on("click", ".btn-contact", function () {

				if($(this).data("id")== 0){
					afficher_contact();
				}else{
					get_fournisseur_contact($(this).data("id"),afficher_contact);
				}

			});

			// lorsque on clique sur suppr le contact
			$(document).on("click", ".btn-contact-del", function () {
				$("#fournisseur_contact_suppr_modal").modal("show");
				$(".btn-contact-del-submit").data('id', $(this).data('id'));

			});

			// lorsque l'on clique sur submit le suppr contact
			$(document).on("click", ".btn-contact-del-submit", function () {

				del_fournisseur_contact($(this).data("id"), supprimer_contact);

			});

			// MODAL CONTACT

			// lorsque l'on clique sur submit contact
			$(".btn-contact-submit").click(function(){
				if($("#con_nom").val() != "" && $("#con_prenom").val() != ""){
					if($("#con_id").val() == 0){
						envoyer_form_ajax($('#form_contact'),ajouter_contact);
					}else{
						envoyer_form_ajax($('#form_contact'),actualiser_contact);
					}
				}else{
					new PNotify({
						title: "Impossible d'ajouter un contact",
						text: "Veuillez renseigner au moins le nom et prénom",
						type: "danger", // all contextuals available(info,system,warning,etc)
						hide: true,
						delay: 4000
					});
				}
			});


			// INTERVENANT

			$(".intervenant-liste").on('click', function(){
				var intervenant = $(this).data('id');

				$(".intervenant-liste.active").removeClass("active");
				$('.intervenant-panels').hide();

				if(intervenant>0){
					$("#data_intervenant_" + intervenant).addClass("active");
					$("#intervenant_panels_" + intervenant).show();
				}
			});



			/////// OK ////////




			// supprimer un diplome
			$(".diplome-supp").click(function(){
				var diplome=$(this).attr("data-diplome");
				var intervenant=$(this).attr("data-intervenant");
				modal_confirmation_user("Êtes-vous sûr de vouloir supprimer ce diplôme ?",supprimer_diplome,diplome,intervenant,"");
			});

			// ajout du document modal
			$(document).on("click", "#document_ajout", function () {
				doc = $(this).data('id');
				$("#form_doc").attr( "action", "fournisseur_doc_enr.php?fournisseur=<?=$fournisseur_id?>&doc=" + doc);
				$("#btn-doc").attr('name', "submitajout");
				$("#modal-title-doc").text("Ajouter un document");
			});
			//

			// edition du document modal
			$(document).on("click", "#document_edition", function () {
				doc = $(this).data('id');
				$("#form_doc").attr( "action", "fournisseur_doc_enr.php?fournisseur=<?=$fournisseur_id?>&doc=" + doc);
				$("#btn-doc").attr('name', "submitedition");
				$("#modal-title-doc").text("Editer un document");
			});
			//


			// passage du data id dans le modal
			$(document).on("click", "#produit_supprimer_btn", function () {
				$("#produit_supprimer").data('id', $(this).data('id'));
			});
			// fin passage data id dans le modal
			// suppression d'un produit modal
			$(document).on("click", "#produit_supprimer", function () {
				var fpr_id = $(this).data('id');
				del_produit("#produit_body", fpr_id);
			});
			// fin suppression d'un produit modal

			// suppression d'un document onlcik suppr
			$(document).on('click', '#document_suppr', function() {
				doc_id = $(this).data("id");
				doc_societe = $(this).data("societe");
				$("#doc_supprimer").data('id',doc_id);
				$("#doc_supprimer").data('societe',doc_societe);
			});
			// fin suppression onclick suppr

			// suppression d'un document modal
			$(document).on("click", "#doc_supprimer", function () {

				var doc_id = $(this).data('id');
				var doc_societe = $(this).data('societe');
				del_document("#document_body", doc_id, doc_societe);
				actualiser_etat_admin(<?=$fournisseur_id?>);
			});
			// fin suppression d'un document modal

			// suppression d'un document onlcik suppr
			$(document).on('click', '#document_intervenant_suppr', function() {
				doc_id = $(this).data("id");
				doc_intervenant = $(this).data("intervenant");
				$("#doc_supprimer_intervenant").data('id',doc_id);
				$("#doc_supprimer_intervenant").data('intervenant',doc_intervenant);
			});
			// fin suppression onclick suppr

			// suppression d'un document modal
			$(document).on("click", "#doc_supprimer_intervenant", function () {

				var doc_id = $(this).data('id');
				var doc_intervenant = $(this).data('intervenant');
				del_document_intervenant("#document_intervenant_body", doc_id, doc_intervenant);
				//actualiser_etat_admin(<?=$fournisseur_id?>);
			});
			// fin suppression d'un document modal


			// suppression d'un diplome onlcik suppr
			$(document).on('click', '#diplome_delete', function() {
				var intervenant = $(this).data("intervenant");
				var diplome = $(this).data("diplome");
				$(".btn-diplome-del-submit").data('intervenant',intervenant);
				$(".btn-diplome-del-submit").data('diplome',diplome);
			});
			// fin suppression onclick suppr

			// suppression d'un diplome modal
			$(document).on("click", ".btn-diplome-del-submit", function () {

				var intervenant = $(this).data("intervenant");
				var diplome = $(this).data("diplome");
				del_diplome("#diplome_body", diplome,intervenant);

			});
			// fin suppression d'un diplome modal

			// actualisation competence
			$(document).on("click", ".competence_clic", function () {

				if($(this).data("valide") == 1){
					$("#force_validation").attr('checked', true);
				}else{
					$("#force_validation").attr('checked', false);
				}

				$("#force_validation").data('intervenant', $(this).data("intervenant"));
				$("#force_validation").data('competence', $(this).data("competence"));

			});
			// fin actualisation competence

			// actualisation de l'input forcer validation
			$(document).on("change", "#force_validation", function () {
				var etat = 0;
				if($(this).is(":checked")){
					etat = 1;
				}else{
					etat = 0
				}
				actualiser_competence_validation( $(this).data("competence"),$(this).data("intervenant"), etat);
			});
			actualiser_select_produit($("#fpr_type").find(':selected').data("revente"));
			// actualisation des  select de recherche produit
			$(document).on("change", "#fpr_type", function () {
				var fpr_type = $(this).find(':selected').data("revente");
				actualiser_select_produit(fpr_type);
			});
			// fin actualisation des  select de recherche produit
			// actualisation du tableau produit
			$(document).on("click", "#search_produit", function () {
				fpr_type = $("#fpr_type").val();
				fpr_categorie = $("#fpr_categorie").val();
				pro_famille = $("#pro_famille").val();
				pro_sous_famille = $("#pro_sous_famille").val();
				pro_sous_sous_famille = $("#pro_sous_sous_famille").val();
				$("#search_produit_zero").show();
				actualiser_recherche_produit("#produit_body",fpr_type, fpr_categorie, pro_famille, pro_sous_famille, pro_sous_sous_famille);
			});

			$(document).on("click", "#search_produit_zero", function () {
				actualiser_recherche_produit_raz("#produit_body");
				$(this).hide();
			});
			// actualisation du tableau produit




			// actualisation des checkbox pour les societes
			/* $( ".select-societe" ).change(function() {
			// pour rafraichir la carte quand on a selectionné des agences
			societe = $(this).attr('id');
			if($(this).is(":checked")){
			$( ".select-agence-" + societe ).each(function() {
			//if($(this).attr("id") == societe + "_agence"){
			$( this ).prop( "checked", true );
			// }

			});
			}else{

			$( ".select-agence" ).each(function() {
			if($(this).attr("id") == societe + "_agence"){
			$( this ).prop( "checked", false );
			}

			});

			}

			});*/
			// fin actualisation checkbox pour les societes

			// actualisation dans la base de données des zones d'interventions
			/* $( ".select-societe" ).change(function() {
			societe = $(this).attr('id');
			societe_val = $(this).val();
			if($(this).is(":checked")){
			zone(societe_val, 0, 1);
			$( ".select-agence" ).each(function() {
			if($(this).attr("id") == societe + "_agence"){
			zone(societe_val, $(this).val(), 1);
			}
			});
			}else{
			zone(societe_val, 0, 0);
			$( ".select-agence" ).each(function() {
			if($(this).attr("id") == societe + "_agence"){
			zone(societe_val, $(this).val(), 0);
			}
			});
			}


			});*/
			// fin actualisation dans la base de données des zones d'interventions

			// actualisation des agences

			// fin actualisation des agences

			// plugin nestable
			/*$('.dd').nestable({

			}).on('change',".dd-item", function(e) {
				e.stopPropagation();

				var levelId = $(this).data('niveau');
				var id = $(this).data('id');
				var parentLevel = $(this).parents('.dd-item').data('niveau');
				var parentId = $(this).parents('.dd-item').data('id');

				if(parentLevel === undefined){
					alert("Attention: vous êtes en train de remplacer la maison mère");
					console.log(parentId);
				}


				if(parentLevel > 2){
					alert("Attention: vous ne pouvez pas faire plus de trois niveaux");
					document.location.href="fournisseur_voir.php?fournisseur=<?=$fournisseur_id?>&tab=6";
				}else{
					$.ajax({
						type:'POST',
						url: 'ajax/ajax_groupe_fournisseur.php',
						data : 'id=' + id + "&levelId=" + levelId + "&parentLevel=" + parentLevel + "&parentId=" + parentId,
						dataType: 'json',
						success: function(data)
						{


						}

					});
				}
			});*/
			// fin plugin nestable
			///////////////////////// FIN SCRIPTS NORMAUX  ////////////////////////

		});

		//////// FONCTION ///////////////

			// MODAL CONTACT

			// prep l'affichage du modal contact
			function afficher_contact(json){
				if(json){
					// si c'est une edition
					$("#con_id").val(json['fco_id']);
					$(".contact-title").text("Editer un contact");
					$("#con_nom").val(json['fco_nom']);
					$("#con_prenom").val(json['fco_prenom']);
					$("#con_mail").val(json['fco_mail']);
					$("#con_tel").val(json['fco_tel']);
					$("#con_portable").val(json['fco_portable']);
					$("#con_fax").val(json['fco_fax']);

					if(json['fco_defaut'] == 1){
						$(".fad_contact_edit").hide();
						$(".fad_contact_edit_check").attr("checked", "checked");
					}else{
						$(".fad_contact_edit").show();
						$(".fad_contact_edit_check").removeAttr("checked");
					}
					$("#con_titre").val(json['fco_titre']);

					if(json['fco_fonction']!=0){
						$("#con_fonction").val(json['fco_fonction']);
						$(".con_fonction_nom_style").hide();
					}else if(json['fco_fonction_nom']!=""){
						$("#con_fonction").val("autre");
						$(".con_fonction_nom_style").show();
						$("#con_fonction_nom").val(json['fco_fonction_nom']);
					}else{
						$("#con_fonction").val(0);
						$(".con_fonction_nom_style").hide();
					}

				}else{
					$(".champ-contact").val('');
					$(".contact-title").text("Ajouter un contact");
					<?php if (empty($fournisseurs_contacts)): ?>
						$(".fad_contact_edit").hide();
						$(".fad_contact_edit_check").attr("checked", "checked");
					<?php else: ?>
						$(".fad_contact_edit").show();
						$(".fad_contact_edit_check").removeAttr("checked");
					<?php endif;?>
					$('#con_fonction option').eq(0).prop('selected', true);
					$('#con_titre option').eq(0).prop('selected', true);

				}
				$("#fournisseurs_contacts_modal").modal("show");

			}

			function actualiser_contact(json){
				if(json){
					if(json.warning_txt!=""){
						 afficher_message("Attention","warning",json.warning_txt);
					}

					var contact_defaut=0;
			<?php if ($fournisseur['fou_type'] == 3) {?>
						$("#fin_ident").html(json['fco_prenom'] + " " + json['fco_nom']);
						$("#fin_fax").text(json['fco_fax']);
						$("#fin_tel").text(json['fco_tel']);
						$("#fin_mail").text(json['fco_mail']);
						$("#fin_prenom").text(json['fco_prenom']);
						$("#fin_nom").text(json['fco_nom']);
			<?php }?>

					if(json['cfo_libelle'] == null){
						json['cfo_libelle'] = "";
					}
					console.log("fco_fonction : " + json['fco_fonction']);
					console.log("fco_fonction_nom : " + json['fco_fonction_nom']);
					if(json['fco_fonction']!=0){
						$("#contact_" + json['fco_id'] + " .table_contact_fonction").text(json['cfo_libelle']);
					}else if(json['fco_fonction']==0){
						$("#contact_" + json['fco_id'] + " .table_contact_fonction").text(json['fco_fonction_nom']);
					}
					$("#contact_" + json['fco_id'] + " .table_contact_prenom_nom").text(json['fco_prenom'] + " " + json['fco_nom']);
					$("#contact_" + json['fco_id'] + " .table_contact_mail").text(json['fco_mail']);
					$("#contact_" + json['fco_id'] + " .table_contact_tel").text(json['fco_tel']);
					$("#contact_" + json['fco_id'] + " .table_contact_portable").text(json['fco_portable']);
					$("#contact_" + json['fco_id'] + " .table_contact_mail").text(json['fco_mail']);
					$("#contact_" + json['fco_id'] + " .table_contact_fax").text(json['fco_fax']);

					if(json['fco_defaut'] == 1){
						contact_defaut=$("#contact_body .primary").data("contact");
						if(contact_defaut!=json['fco_id']){
							// changement de contact par defaut

							// l'ancien contact devient supprimable
							$("#contact_" + contact_defaut).removeClass('primary');
							html="<a href='#' data-id='" + contact_defaut + "' class='btn btn-xs btn-danger btn-contact-del' >";
								html=html + "<i class='fa fa-times' ></i>";
							html=html + "</a>";
							$("#contact_" + contact_defaut + " .table_contact_btn").prepend(html);

							// le nouveau ne l'ai plus
							$("#contact_" + json['fco_id'] + " .btn-contact-del").remove();
							$("#contact_" + json['fco_id']).addClass('primary');
						}
					}
				}
				$("#fournisseurs_contacts_modal").modal("hide");

			}

			// ajouter le contact
			function ajouter_contact(json){
				if(json.warning_txt!=""){
					afficher_message("Attention","warning",json.warning_txt);
				}
				var fonction_nom="";
				if(json){
					if(json['cfo_libelle']!=""){
						fonction_nom=json['cfo_libelle'];
					}else{
						fonction_nom=json['fco_fonction_nom'];
					}
					if(json['fco_defaut'] == 1){
						$('#contact_body tr').removeClass('primary');
						html="<tr class='primary' id='contact_" + json['fco_id'] + "'>";
							html=html + "<td class='table_contact_fonction'>" + fonction_nom + "</td>";
							html=html + "<td class='table_contact_prenom_nom'>" + json['fco_prenom'] + " " + json['fco_nom'] + "</td>";
							html=html + "<td class='table_contact_mail'>" + json['fco_mail'] + "</td>";
							html=html + "<td class='table_contact_tel'>" + json['fco_tel'] + "</td>";
							html=html + "<td class='table_contact_portable'>" + json['fco_portable'] + "</td>";
							html=html + "<td class='table_contact_fax'>" + json['fco_fax'] + "</td>";
							html=html + "<td class='table_contact_btn' ><a href='#' data-id='"+ json['fco_id'] + "' class='btn btn-xs btn-warning btn-contact'><i class='fa fa-pencil'></i></a></td>";
						html=html + "</tr>";
						$('#contact_body').prepend(html);

					}else{
						html="<tr id='contact_" + json['fco_id'] + "'>";
							html=html + "<td class='table_contact_fonction'>" + fonction_nom + "</td>";
							html=html + "<td class='table_contact_prenom_nom'>" + json['fco_prenom'] + " " + json['fco_nom'] + "</td>";
							html=html + "<td class='table_contact_mail'>" + json['fco_mail'] + "</td>";
							html=html + "<td class='table_contact_tel'>" + json['fco_tel'] + "</td>";
							html=html + "<td class='table_contact_portable'>" + json['fco_portable'] + "</td>";
							html=html + "<td class='table_contact_fax'>" + json['fco_fax'] + "</td>";
							html=html + "<td class='table_contact_btn' >";
								html=html + "<a href='#' data-id='"+ json['fco_id'] + "' class='btn btn-xs btn-warning btn-contact'><i class='fa fa-pencil'></i></a>";
								html=html + "<a href='#' data-id='" + json['fco_id'] + "' class='btn btn-xs btn-danger btn-contact-del'><i class='fa fa-times'></i></a>";
							html=html + "</td>";
						html=html + "</tr>";
						$('#contact_body:last-child').append(html);

					}

				}
				$("#fournisseurs_contacts_modal").modal("hide");
			}

			// fonction contact
			function nomchange(selected){
				if(selected == "autre"){
					$(".con_fonction_nom_style").show(400);
				}else{
					$(".con_fonction_nom_style").hide(400);
					$("#con_fonction_nom").val("");
				}

			}



		////// OK //////




		// supprimer un diplome
		function supprimer_diplome(diplome,intervenant){

			document.location.href="fournisseur_intervenant_diplome_supp.php?ref=2&ref_id=" + intervenant + "&diplome=" + diplome + "&fournisseur=" + fournisseur;
		}
		// Actualisation sous_famille en fonction de famille
		function sous_famille(selected_id){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_sous_famille.php',
				data : 'field=' + selected_id,
				dataType: 'json',
				success: function(data)
				{
					if (data['0'] == undefined){

						$("#pro_sous_famille").find("option:gt(0)").remove();

					}else{
						$("#pro_sous_famille").find("option:gt(0)").remove();
						$.each(data, function(key, value) {
							$('#pro_sous_famille')
							.append($("<option></option>")
								.attr("value",value["psf_id"])
								.text(value["psf_libelle"]));
						});
					}
				}
			});
		}

		function sous_sous_famille(selected_id){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_sous_sous_famille.php',
				data : 'field=' + selected_id,
				dataType: 'json',
				success: function(data)
				{
					if (data['0'] == undefined){

						$("#pro_sous_sous_famille").find("option:gt(0)").remove();

					}else{
						$("#pro_sous_sous_famille").find("option:gt(0)").remove();
						$.each(data, function(key, value) {
							$('#pro_sous_sous_famille')
							.append($("<option></option>")
								.attr("value",value["pss_id"])
								.text(value["pss_libelle"]));
						});
					}
				}
			});
		}
		// Fin Actualisation sous_famille en fonction de famille


		// fonction zones d'interventions
		function zone(societe, agence, ajout){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_fournisseur_zone.php',
				data : 'societe=' + societe + "&agence=" + agence + "&fournisseur=<?=$fournisseur_id?>&ajout=" + ajout,
				dataType: 'json',
				success: function(data)
				{

				}
			});
		}
		// fin fonction zones d'interventions

		// fonction departement zones d'interventions
		function departement(departement){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_fournisseur_departement.php',
				data : 'departement=' + departement +"&fournisseur=<?=$fournisseur_id?>",
				dataType: 'json',
				success: function(data)
				{

				}
			});
		}
		// fin fonction departement zones d'interventions
		// fonction agence / societe
		function actu_select_agence(societe){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_agence.php',
				data : 'field=' + societe,
				dataType: 'json',
				success: function(data)
				{
					if (data['0'] == undefined){
						$('#agence').find("option:gt(0)").remove();
					}else{
						$("#agence").find("option:gt(0)").remove();
						$.each(data, function(key, value){
							$('#agence')
							.append($("<option></option>")
								.attr("value",value["age_id"])
								.text(value["age_nom"]));
						});
					}
				}
			});
		}
		// fin fonction agence / societe

		// supprimer le contact
		function supprimer_contact(json){

			if(json){
				$('#contact_' + json).remove();
			}
			$("#fournisseur_contact_suppr_modal").modal("hide");
		}
		// fin supprimer le contact

		// actualiser les select dans la recherche produits
		function actualiser_select_produit(type){
			// savoir si
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_fournisseur_traitement_produit.php',
				data : 'type=' + type,
				dataType: 'json',
				success: function(data)
				{
					if(data == 1){// si pas sous-traitance ni achat pour revente
						$(".fpr_categorie").hide();
						$(".pro_famille").hide();
						$(".pro_sous_famille").hide();
						$(".pro_sous_sous_famille").hide();
						$("#fpr_categorie").val(0);
						$("#fpr_categorie").trigger("change");
						$("#pro_famille").val(0);
						$("#pro_famille").trigger("change");
						$("#pro_sous_famille").val(0);
						$("#pro_sous_famille").trigger("change");
						$("#pro_sous_sous_famille").val(0);
						$("#pro_sous_sous_famille").trigger("change");
					}else{ // si achat pour revente OU sous_traitance
						// actualisation du select categorie
						$("#fpr_categorie").find("option:gt(0)").remove();
						$.each(data, function(key, value) {
							$('#fpr_categorie').append($("<option></option>").attr("value",value["pca_id"]).text(value["pca_libelle"]));
						});
						$(".fpr_categorie").show();
						$(".pro_famille").show();
						$(".pro_sous_famille").show();
						$(".pro_sous_sous_famille").show();

					}

				}

			});

		}
		// fin actualiser les select dans la recherche produits

		// actualiser la recherche produit
		function actualiser_recherche_produit(id_body,type,categorie,famille,sous_famille, sous_sous_famille){
			$( id_body + '  > tr').each(function() {
				// type
				if(($(this).data('type') == type || type == 0) && 
				($(this).data('categorie') == categorie || categorie == 0) && 
				($(this).data('famille') == famille || famille == 0) && 
				($(this).data('sousfamille') == sous_famille || sous_famille == 0) && 
				($(this).data('soussousfamille') == sous_sous_famille || sous_sous_famille == 0)){
					$(this).show();
				}else{
					$(this).hide();
				}
				/*// categorie
				if(categorie != 0 && $(this).data('categorie') != categorie){
					$(this).hide();
				}else{
					$(this).show();
				}
				// famille
				if(famille != 0 && $(this).data('famille') != famille){
					$(this).hide();
				}else{
					$(this).show();
				}
				// sous_famille
				if(sous_famille != 0 && $(this).data('sousfamille') != sous_famille){
					$(this).hide();
				}else{
					$(this).show();
				}*/

			});

		}
		function actualiser_recherche_produit_raz(id_body){
			$( id_body + '  > tr').each(function() {
				$(this).show();
				$("#fpr_type").val(0);
				$("#fpr_type").trigger("change");
				$("#fpr_categorie").val(0);
				$("#fpr_categorie").trigger("change");
				$("#pro_famille").val(0);
				$("#pro_famille").trigger("change");
				$("#pro_sous_famille").val(0);
				$("#pro_sous_famille").trigger("change");
				$("#pro_sous_sous_famille").val(0);
				$("#pro_sous_sous_famille").trigger("change");
			});
		}
		// fin actualiser la recherche produit

		// actualise controle intervenant
		/*function actualise_controle_intervenant(intervenant){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_fournisseur_actualise_controle.php',
				data : 'intervenant=' + intervenant + "&fournisseur=<?=$fournisseur_id?>",
				dataType: 'json',
				success: function(data)
				{



						if(data["ico_controle_dt"] == 1){
							$("#intervenant_controle_" + intervenant).css("color", "#70CA63");
						}else if(data["ico_controle_dt"] == 2){
							$("#intervenant_controle_" + intervenant).css("color", "#F6BB42");
						}else if(data["ico_controle_dt"] == 3){
							$("#intervenant_controle_" + intervenant).css("color", "#DF5640");
						}
						$("#data_intervenant_" + intervenant).attr("data-original-title", data['ico_controle_dt_txt']);


				}

			});

		}*/
		// fin actualise controle intervenant

		// supprimer un produit
		function del_produit(id_body, produit){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_del_fournisseur_produit.php',
				data : 'produit=' + produit,
				dataType: 'html',
				success: function(data)
				{
					if(data == "OK"){

						$( id_body + '  > tr').each(function() {

							if($(this).data('id') == produit){

								$(this).hide();
							}
						});
					}


				}

			});
			$("#fournisseur_produit_suppr_modal").modal("hide");
		}
		// fin supprimer un porduit

		// supprimer un document
		function del_document(id_body, doc, doc_societe){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_del_fournisseur_document.php',
				data : 'document=' + doc + "&fournisseur=<?=$fournisseur_id?>&societe=" + doc_societe,
				dataType: 'html',
				success: function(data)
				{
					$(".document_dl_" + doc).html("<i>Pas de document</i>");
					$(".document_date_" + doc).html("<i>Pas de document</i>");
					$(".document_fin_" + doc).html("<i>Pas de document</i>");
					$(".document_actions_" + doc).html("<a href='fournisseur_doc.php?fournisseur=<?=$fournisseur_id?>&id=" + doc + "' class='btn btn-sm btn-success'><i class='fa fa-plus'></i></a>");

					if(data == 1){
						$( id_body + '  > tr').each(function() {
							if($(this).data('id') == doc){
								$(this).removeClass().addClass("danger");
								$(this).css("font-weight","bold");
							}
						});


					}else{
						$( id_body + '  > tr').each(function() {
							if($(this).data('id') == doc){
								$(this).removeClass().addClass("danger");
								$(this).css("font-weight","normal");
							}
						});
					}


				}

			});
			$("#fournisseur_doc_suppr_modal").modal("hide");
		}
		// fin supprimer un document

		// supprimer un document d'un intervenant
		function del_document_intervenant(id_body, doc, doc_intervenant){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_del_fournisseur_document_intervenant.php',
				data : 'document=' + doc + "&fournisseur=<?=$fournisseur_id?>&intervenant=" + doc_intervenant,
				dataType: 'html',
				success: function(data)
				{
					$(".document_intervenants_date_" + doc + "_" + doc_intervenant).html("Pas de document");
					$(".document_intervenants_actions_" + doc + "_" + doc_intervenant).html("<a href='fournisseur_doc_intervenant.php?fournisseur=<?=$fournisseur_id?>&id=" + doc + "&intervenant=" + doc_intervenant + "' class='btn btn-xs btn-success'><i class='fa fa-plus'></i></a>");
					if(data == 1){
						$( id_body + '  > tr').each(function() {
							if($(this).data('document') == doc && $(this).data('intervenant') == doc_intervenant){
								$(this).removeClass().addClass("danger");
								$(this).css("font-weight","bold");
							}
						});


					}else{
						$( id_body + '  > tr').each(function() {
							if($(this).data('document') == doc && $(this).data('intervenant') == doc_intervenant){
								$(this).removeClass().addClass("danger");
								$(this).css("font-weight","normal");
							}
						});
					}


				}

			});
			/*actualise_controle_intervenant(doc_intervenant);*/
			$("#fournisseur_doc_intervenant_suppr_modal").modal("hide");
		}
		// fin supprimer un document d'un intervenant

		// supprimer un diplome
		function del_diplome(id_body, diplome, intervenant){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_del_fournisseur_diplome.php',
				data : 'diplome=' + diplome + "&fournisseur=<?=$fournisseur_id?>&intervenant=" + intervenant,
				dataType: 'json',
				success: function(data)
				{
					$(".diplome_obtention_" + diplome + "_" + intervenant).html("Pas de diplôme");
						$(".diplome_btn_" + diplome + "_" + intervenant).html("<a class='btn btn-success btn-xs' href='fournisseur_intervenant_diplome.php?fournisseur=<?=$fournisseur_id?>&intervenant=" + intervenant + "&diplome=" + diplome + "'><i class='fa fa-plus'></i></a>");


					$( id_body + '  > tr').each(function() {
						if($(this).data('intervenant') == intervenant && $(this).data('diplome') == diplome){
							$(this).removeClass().addClass("danger");
						}
					});

					$.each(data, function (index, value) {

						if(value["ico_controle_dt"] == 1){
							$("#competence_" + value['com_id'] + "_" + intervenant).removeClass().addClass("fa fa-circle fs35 text-success");
						}else if(value["ico_controle_dt"] == 2){
							$("#competence_" + value['com_id'] + "_" + intervenant).removeClass().addClass("fa fa-circle fs35 text-warning");
						}else if(value["ico_controle_dt"] == 3){
							$("#competence_" + value['com_id'] + "_" + intervenant).removeClass().addClass("fa fa-circle fs35 text-danger");
						}
						console.log(value['ico_controle_dt_txt']);

						$("#data_competence_" + value['com_id'] + "_" + intervenant).attr("data-title", value['ico_controle_dt_txt']);
						$("#data_competence_" + value['com_id'] + "_" + intervenant).attr("data-original-title", value['ico_controle_dt_txt']);
					});




				}

			});
			/*actualise_controle_intervenant(intervenant);*/
			$("#fournisseur_diplome_suppr_modal").modal("hide");
		}
		// fin supprimer un diplome

		// actualiser l'état administratif du fournisseur
		function actualiser_etat_admin(fou_id){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_fournisseur_etat_admin.php',
				data : 'fournisseur=' + fou_id,
				dataType: 'json',
				success: function(data)
				{

					if(data['fva_admin_etat'] == 1){
						$('.etat_admin_color').removeClass().addClass('etat_admin_color panel bg-danger light of-h mb10');
						$('.etat_admin').text("Le fournisseur n'est pas valide, il faut vérifier l'état des documents suivant :");
						$('.etat_admin_txt').html(data['fva_admin_txt']);

					}else if(data['fva_admin_etat'] == 2){
						$('.etat_admin_color').removeClass().addClass('etat_admin_color panel bg-warning light of-h mb10');
						$('.etat_admin').text("Le fournisseur est valide cependant il faut vérifier l'état des documents suivant :");
						$('.etat_admin_txt').html(data['fva_admin_txt']);
					}else if(data['fva_admin_etat'] == 3){
						$('.etat_admin_color').removeClass().addClass('etat_admin_color panel bg-success light of-h mb10');
						$('.etat_admin').text("Le fournisseur est valide.");
						$('.etat_admin_txt').html(data['fva_admin_txt']);


					}

				}

			});
		}
		// fin actualiser l'état administratif du fournisseur

		// actualiser l'état competence
		function actualiser_competence_validation(competence, intervenant, etat){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_fournisseur_competence_validation.php',
				data : 'competence=' + competence + "&intervenant=" + intervenant + "&etat=" + etat,
				dataType: 'html',
				success: function(data)
				{

					if(data == 1){
						$("#competence_" + competence + "_" + intervenant).removeClass().addClass("fa fa-circle fs35 text-success");
					}else if(data == 2){
						$("#competence_" + competence + "_" + intervenant).removeClass().addClass("fa fa-circle fs35 text-warning");
					}else if(data == 3){
						$("#competence_" + competence + "_" + intervenant).removeClass().addClass("fa fa-circle fs35 text-danger");
					}

				}

			});
		}
		// fin actualiser l'état compétence

		//////////////////////////// FIN FONCTIONS  ///////////////////////////

		// carte France
		
		// fin carte France
		</script>

	</body>
</html>
