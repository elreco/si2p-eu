<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';
include_once 'includes/connexion_fct.php';
include_once 'modeles/mod_parametre.php';
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
$erreur = "";
$for_rap_precedent = 0;
$for_rap_suivant = 0;
$for_action = 0;
$for_km_fin = 0;
$for_km_deb = 0;
$for_vehicule_id = 0;
if(!empty($_POST['for_vehicule_id'])){
	$for_vehicule_id = $_POST['for_vehicule_id'];
}
$for_nb_groupe = 0;
if(!empty($_POST['for_nb_groupe'])){
	$for_nb_groupe = $_POST['for_nb_groupe'];
}
$for_nb_h_groupe = 0;
if(!empty($_POST['for_nb_h_groupe'])){
	$for_nb_h_groupe = $_POST['for_nb_h_groupe'];
}
$for_total_h=0;
if(!empty($_POST['for_total_h'])){
	$for_total_h = floatval($_POST['for_total_h']);
}
$continue = true;
$date_rapport = new DateTime($_POST['for_date']);
if($date_rapport->format("N")== 7){
	$for_semaine = $date_rapport->format("W") + 1;
}else{
	$for_semaine = $date_rapport->format("W");
}
$for_annee = $date_rapport->format("Y");
if(empty($_POST['for_demi'])){
	$_POST['for_demi'] = 1.5;
}
if(!empty($_POST['for_km'])){
	$km = abs(intval($_POST['for_km']));
}else{
	$km = 0;
}
$prec_rapport_km_deb = 0;
$prec_rapport_km_fin = 0;
$for_rap_precedent= 0;
$for_rap_precedent_soc=0;
$for_rap_suivant= 0;
$for_rap_suivant_soc=0;
$acl_nb_sta_rap = array();

// EDITION

if (isset($_POST['for_id'])){
	// CHERCHER LE RAPPORT PRECEDENT
	$sql="SELECT * FROM Formations WHERE for_id =" . $_POST['for_id'];
	$req = $ConnSoc->query($sql);
	$d_formation=$req->fetch();
	if(!empty($_POST['for_vehicule_id'])){


		if(!empty($d_formation['for_vehicule_id']) && ($_POST['for_vehicule_id'] != $d_formation['for_vehicule_id']  OR empty($d_formation['for_km_deb']))){
			if(!empty($d_formation['for_rap_precedent'])){

		        $ConnFct = connexion_fct($d_formation['for_rap_precedent_soc']);
		        $sql="SELECT * FROM Formations  WHERE for_id =" . $d_formation['for_rap_precedent'];
		        $req = $ConnFct->prepare($sql);
		        $req->execute();
		        $d_formation_prec=$req->fetch();
		        if(!empty($d_formation['for_rap_suivant']) && !empty($d_formation_prec)){
		            $ConnFct = connexion_fct($d_formation['for_rap_suivant_soc']);
		            $sql="SELECT * FROM Formations  WHERE for_id =" . $d_formation['for_rap_suivant'];
		            $req = $ConnFct->prepare($sql);
		            $req->execute();
		            $d_formation_suivant=$req->fetch();

		            $ConnFct = connexion_fct($d_formation['for_rap_suivant_soc']);
		            $sql="UPDATE Formations  SET for_rap_precedent_soc=" . $d_formation['for_rap_precedent_soc'] . ", for_rap_precedent=" . $d_formation_prec['for_id'] . " WHERE for_id=" . $d_formation['for_rap_suivant'];
		            $req = $ConnFct->prepare($sql);
		            $req->execute();

		            $for_new_km = abs($d_formation_suivant['for_km_deb'] - $d_formation_prec['for_km_deb']);
		            $ConnFct = connexion_fct($d_formation['for_rap_precedent_soc']);
		            $sql="UPDATE Formations  SET for_km_fin= ".$d_formation_suivant['for_km_deb'].",for_km= ". $for_new_km . ", for_rap_suivant_soc=" . $d_formation['for_rap_suivant_soc'] . ", for_rap_suivant=" . $d_formation_suivant['for_id'] ." WHERE for_id=" . $d_formation_prec['for_id'];
		            $req = $ConnSoc->prepare($sql);
		            $req->execute();

		        }elseif(!empty($d_formation_prec)){
		            $ConnFct = connexion_fct($d_formation['for_rap_precedent_soc']);
		            $sql="UPDATE Formations  SET for_km_fin= 0,for_km= 0, for_rap_suivant_soc=0, for_rap_suivant=0 WHERE for_id=" . $d_formation_prec['for_id'];
		            $req = $ConnSoc->prepare($sql);
		            $req->execute();
		        }


		    }else{
				$for_date_prec="";
				$for_demi_prec = 0;
				// CHERCHER LE RAPPORT PRECEDENT
		        $sql="SELECT soc_id FROM Societes WHERE soc_archive = 0";
		        $req = $Conn->query($sql);
		        $d_societes=$req->fetchAll();
		        foreach($d_societes as $s){
		            $ConnFct = connexion_fct($s['soc_id']);
		            $sql="SELECT * FROM Formations  WHERE for_vehicule_id =" . $_POST['for_vehicule_id'] . " AND for_date <= '". $_POST['for_date'] . "'  AND for_id != " . $_POST['for_id'] . " ORDER BY for_date DESC, for_demi DESC";
		            $req = $ConnFct->prepare($sql);
		            $req->execute();
		            $d_formation_prec[$s['soc_id']]=$req->fetch();
		        }

				if(!empty($d_formation_prec)){

					foreach($d_formation_prec as $k=>$f){

			            if(isset($f['for_date']) && ($f['for_date']>$for_date_prec && $f['for_demi']>$for_demi_prec)){
			                $prec_rapport_km_deb =  $f['for_km_deb'];
			                $prec_rapport_km_fin = $f['for_km_fin'];
			                $for_rap_precedent = $f['for_id'];
			                $for_rap_precedent_soc = $k;
			            }

			        }

				}


		        if($prec_rapport_km_deb>$km){
		            //GOOD
		            $continue = false;
		            $km_message = "Le nbre de kilomètres ne peut pas être inférieur à " . $prec_rapport_km_deb;
		        }else{
					if(!empty($for_rap_precedent_soc)){
						$ConnFct = connexion_fct($for_rap_precedent_soc);
			            $for_new_km = abs($km-$prec_rapport_km_deb);
			            $sql="UPDATE Formations  SET for_km_fin= " . $km . ",for_km= " . $for_new_km . ", for_rap_suivant = " . $_POST['for_id'] . ",for_rap_suivant_soc=" . $acc_societe . "  WHERE for_id=" . $for_rap_precedent;
			            $req = $ConnFct->prepare($sql);
			            $req->execute();
						$sql="UPDATE Formations  SET for_km_deb= ". $km . ", for_rap_precedent_soc=" . $for_rap_precedent_soc . ", for_rap_precedent=" . $for_rap_precedent ." WHERE for_id=" . $_POST['for_id'];
						$req = $ConnSoc->prepare($sql);
						$req->execute();
					}
				}
			}



		}else{
			if(!empty($d_formation['for_rap_precedent'])){

				$ConnFct = connexion_fct($d_formation['for_rap_precedent_soc']);
				$sql="SELECT * FROM Formations  WHERE for_id =" . $d_formation['for_rap_precedent'];
				$req = $ConnFct->prepare($sql);
				$req->execute();
				$d_formation_prec=$req->fetch();

				$for_new_km = abs($km - $d_formation_prec['for_km_deb']);


				if(!empty($d_formation['for_rap_suivant'])){
					$ConnFct = connexion_fct($d_formation['for_rap_suivant_soc']);
					$sql="SELECT * FROM Formations  WHERE for_id =" . $d_formation['for_rap_suivant'];
					$req = $ConnFct->prepare($sql);
					$req->execute();
					$d_formation_suivant=$req->fetch();
					$for_km_deb_suivant = $d_formation_suivant['for_km_deb'];
				}else{
					$for_km_deb_suivant = 99999999999999;

				}

				if($d_formation_prec['for_km_deb']>$km OR $for_km_deb_suivant<$km){
		            //GOOD
		            $continue = false;
					if($for_km_deb_suivant == 99999999999999){

						$km_message = "Le nbre de kilomètres ne peut pas être inférieur à " . $d_formation_prec['for_km_deb'];

					}else{
						$km_message = "Le nbre de kilomètres ne peut pas être inférieur à " . $d_formation_prec['for_km_deb'] . " et supérieur à " . $for_km_deb_suivant;

					}
				}else{
					if(!empty($d_formation_suivant)){
						$for_new_km = abs($for_km_deb_suivant-$km);
						$sql="UPDATE Formations  SET for_km_deb= " . $km . ",for_km= " . $for_new_km . " WHERE for_id=" . $_POST['for_id'];
						$req = $ConnSoc->prepare($sql);
						$req->execute();
					}
				}

			}else{

				if(!empty($d_formation['for_rap_suivant'])){
					$ConnFct = connexion_fct($d_formation['for_rap_suivant_soc']);
					$sql="SELECT * FROM Formations  WHERE for_id =" . $d_formation['for_rap_suivant'];
					$req = $ConnFct->prepare($sql);
					$req->execute();
					$d_formation_suivant=$req->fetch();
					$for_km_deb_suivant = $d_formation_suivant['for_km_deb'];
					if($for_km_deb_suivant<$km){
			            //GOOD
			            $continue = false;
						$km_message = "Le nbre de kilomètres ne peut pas être supérieur à " . $for_km_deb_suivant;

			        }else{

						// UPDATE l'ANCIEN RAPPORT
						$for_new_km = abs($for_km_deb_suivant-$km);
						$sql="UPDATE Formations  SET for_km_deb= " . $km . ",for_km= " . $for_new_km . " WHERE for_id=" . $_POST['for_id'];
						$req = $ConnSoc->prepare($sql);
						$req->execute();

					}
				}else{
					$for_date_prec="";
					$for_demi_prec = 0;
					// CHERCHER LE RAPPORT PRECEDENT
			        $sql="SELECT soc_id FROM Societes WHERE soc_archive = 0";
			        $req = $Conn->query($sql);
			        $d_societes=$req->fetchAll();
			        foreach($d_societes as $s){
			            $ConnFct = connexion_fct($s['soc_id']);
			            $sql="SELECT * FROM Formations  WHERE for_vehicule_id =" . $_POST['for_vehicule_id'] . " AND for_date <= '". $_POST['for_date'] . "' AND for_id != " . $_POST['for_id'] . " ORDER BY for_date DESC, for_demi DESC";
			            $req = $ConnFct->prepare($sql);
			            $req->execute();
			            $d_formation_prec[$s['soc_id']]=$req->fetch();
			        }

					if(!empty($d_formation_prec)){

						foreach($d_formation_prec as $k=>$f){

				            if(isset($f['for_date']) && ($f['for_date']>$for_date_prec && $f['for_demi']>$for_demi_prec)){
				                $prec_rapport_km_deb =  $f['for_km_deb'];
				                $prec_rapport_km_fin = $f['for_km_fin'];
				                $for_rap_precedent = $f['for_id'];
				                $for_rap_precedent_soc = $k;
				            }

				        }

					}


			        if($prec_rapport_km_deb>$km){
			            //GOOD
			            $continue = false;
			            $km_message = "Le nbre de kilomètres ne peut pas être inférieur à " . $prec_rapport_km_deb;
			        }else{
						if(!empty($for_rap_precedent_soc)){
							$ConnFct = connexion_fct($for_rap_precedent_soc);
				            $for_new_km = abs($km-$prec_rapport_km_deb);
				            $sql="UPDATE Formations  SET for_km_fin= " . $km . ",for_km= " . $for_new_km . ", for_rap_suivant = " . $_POST['for_id'] . ",for_rap_suivant_soc=" . $acc_societe . "  WHERE for_id=" . $for_rap_precedent;
				            $req = $ConnFct->prepare($sql);
				            $req->execute();
							$sql="UPDATE Formations  SET for_km_deb= ". $km . ", for_rap_precedent_soc=" . $for_rap_precedent_soc . ", for_rap_precedent=" . $for_rap_precedent ." WHERE for_id=" . $_POST['for_id'];
							$req = $ConnSoc->prepare($sql);
							$req->execute();
						}



					}
				}




			}




		}

    }else{
		if(!empty($d_formation['for_rap_precedent']) && !empty($d_formation['for_rap_suivant'])){

			$ConnFct = connexion_fct($d_formation['for_rap_precedent_soc']);
			$sql="SELECT * FROM Formations  WHERE for_id =" . $d_formation['for_rap_precedent'];
			$req = $ConnFct->prepare($sql);
			$req->execute();
			$d_formation_prec=$req->fetch();

			$ConnFct = connexion_fct($d_formation['for_rap_suivant_soc']);
			$sql="SELECT * FROM Formations  WHERE for_id =" . $d_formation['for_rap_suivant'];
			$req = $ConnFct->prepare($sql);
			$req->execute();
			$d_formation_suivant=$req->fetch();
			$for_new_km = abs($d_formation_suivant['for_km_deb'] - $d_formation_prec['for_km_deb']);

			$ConnFct = connexion_fct($d_formation['for_rap_precedent_soc']);
			$sql="UPDATE Formations  SET for_km_fin= " . $d_formation_suivant['for_km_deb'] . ",for_km= " . $for_new_km . ", for_rap_suivant = " . $d_formation['for_rap_suivant'] . ", for_rap_suivant_soc = " . $d_formation['for_rap_suivant_soc'] . " WHERE for_id=" . $d_formation['for_rap_precedent'];
			$req = $ConnFct->prepare($sql);
			$req->execute();
			$ConnFct = connexion_fct($d_formation['for_rap_suivant_soc']);
			$sql="UPDATE Formations  SET for_rap_precedent = " . $d_formation['for_rap_precedent'] . ", for_rap_precedent_soc = " . $d_formation['for_rap_precedent_soc'] . " WHERE for_id=" . $d_formation['for_rap_suivant'];
			$req = $ConnFct->prepare($sql);
			$req->execute();
		}elseif(!empty($d_formation['for_rap_precedent']) && empty($d_formation['for_rap_suivant'])){
			$ConnFct = connexion_fct($d_formation['for_rap_precedent_soc']);
			$sql="UPDATE Formations  SET for_km_fin= 0,for_km= 0, for_rap_suivant = 0, for_rap_suivant_soc = 0 WHERE for_id=" . $d_formation['for_rap_precedent'];
			$req = $ConnFct->prepare($sql);
			$req->execute();

		}elseif(empty($d_formation['for_rap_precedent']) && !empty($d_formation['for_rap_suivant'])){
			$ConnFct = connexion_fct($d_formation['for_rap_suivant_soc']);
			$sql="UPDATE Formations  SET for_rap_precedent = 0, for_rap_precedent_soc = 0 WHERE for_id=" . $d_formation['for_rap_suivant'];
			$req = $ConnFct->prepare($sql);
			$req->execute();
		}
		$sql="UPDATE Formations  SET for_km_fin= 0,for_km= 0, for_rap_suivant = 0, for_rap_suivant_soc = 0,for_rap_precedent = 0, for_rap_precedent_soc = 0 WHERE for_id=" . $_POST['for_id'];
		$req = $ConnSoc->prepare($sql);
		$req->execute();

	}

	if($continue==true){

		if(!empty($_POST['for_vehicule_id'])){
			$for_km_deb = $km;
		}else{
			$for_km_deb = 0;
		}
		$for_km_fin = 0;
		$sql="UPDATE Formations SET for_annee=:for_annee, for_semaine=:for_semaine, for_intervenant=:for_intervenant, for_date=:for_date, for_demi=:for_demi
		, for_nb_demi=:for_nb_demi, for_nb_groupe=:for_nb_groupe, for_nb_h_groupe=:for_nb_h_groupe,
		for_total_h=:for_total_h, for_observations=:for_observations, for_vehicule_categorie=:for_vehicule_categorie, for_vehicule_id=:for_vehicule_id,
		 for_vehicule_numero=:for_vehicule_numero,for_km_deb=:for_km_deb, for_utilisation=:for_utilisation,
		for_hebergement=:for_hebergement, for_hebergement_texte=:for_hebergement_texte, for_km_deb=:for_km_deb, for_action=:for_action
		WHERE for_id = " . $_POST['for_id'];
		$req = $ConnSoc->prepare($sql);
		$req->bindValue(":for_annee",$for_annee);
		$req->bindValue(":for_semaine",$for_semaine);
		$req->bindValue(":for_intervenant",$_POST['for_intervenant']);
		$req->bindValue(":for_date",$_POST['for_date']);
		$req->bindValue(":for_demi",$_POST['for_demi']);
		$req->bindValue(":for_nb_demi",$_POST['for_nb_demi']);
		$req->bindValue(":for_nb_groupe",$for_nb_groupe);
		$req->bindValue(":for_nb_h_groupe",$for_nb_h_groupe);
		$req->bindValue(":for_total_h",$for_total_h);
		$req->bindValue(":for_observations",$_POST['for_observations']);
		$req->bindValue(":for_vehicule_categorie",$_POST['for_vehicule_categorie']);
		$req->bindValue(":for_vehicule_id",$for_vehicule_id);
		$req->bindValue(":for_vehicule_numero",$_POST['for_vehicule_numero']);
		$req->bindValue(":for_utilisation",$_POST['for_utilisation']);
		$req->bindValue(":for_hebergement",$_POST['for_hebergement']);
		$req->bindValue(":for_hebergement_texte",$_POST['for_hebergement_texte']);
		$req->bindValue(":for_action",$for_action);
		$req->bindValue(":for_km_deb",$for_km_deb);
		$req->execute();
		$for_id = $_POST['for_id'];
		$sql="DELETE FROM  formations_actions WHERE fac_formation = " . $for_id;
		$req = $ConnSoc->prepare($sql);
		$req->execute();
		if(!empty($_POST['for_nb_stagiaires'])){
			foreach($_POST['for_nb_stagiaires'] as $proid=>$l_clients){
				foreach($l_clients as $cliid=>$l){
					if(!empty($_POST['for_nb_stagiaires'][$proid][$cliid])){
						$nb_stas = $_POST['for_nb_stagiaires'][$proid][$cliid];
					}else{
						$nb_stas = 0;
					}
					$acl_action = $_POST['for_action'][$proid][$cliid];

					$sql="INSERT INTO formations_actions (fac_formation, fac_date, fac_demi, fac_action_client, fac_produit, fac_nb_stagiaires) VALUES (:fac_formation, :fac_date, :fac_demi, :fac_action_client, :fac_produit, :fac_nb_stagiaires)";
					$req = $ConnSoc->prepare($sql);
					$req->bindValue(":fac_formation",$for_id);
					$req->bindValue(":fac_date",$_POST['for_date']);
					$req->bindValue(":fac_demi",$_POST['for_demi']);
					$req->bindValue(":fac_action_client",$cliid);
					$req->bindValue(":fac_produit",$proid);
					$req->bindValue(":fac_nb_stagiaires",$nb_stas);
					$req->execute();

					$sql="SELECT act_gest_sta FROM Actions  WHERE act_id =" . $acl_action;
					$req = $ConnSoc->prepare($sql);
					$req->execute();
					$d_action=$req->fetch();
					if($d_action['act_gest_sta']==2){
						// INSCRIPTION SESSION ==> ON ADITIONNE TOUT
						if(empty($acl_nb_sta_rap[$cliid])){
							$acl_nb_sta_rap[$cliid] = $nb_stas;
						}else{
							$acl_nb_sta_rap[$cliid] = $acl_nb_sta_rap[$cliid] + $nb_stas;
						}

					}else{
						// INSCRIPTION FORMATION ==> ON PREND LA PLUS GRANDE VALEUR
						if(empty($acl_nb_sta_rap[$cliid]) OR (!empty($acl_nb_sta_rap[$cliid]) && $nb_stas > $acl_nb_sta_rap[$cliid])){
							$acl_nb_sta_rap[$cliid] = $nb_stas;
						}

					}


				}
			}

			foreach($acl_nb_sta_rap as $c=>$n){
				$sql="UPDATE Actions_Clients SET acl_nb_sta_rap = " . $n . "  WHERE acl_client = " .$c . " AND acl_action =" . $acl_action;
				$req = $ConnSoc->prepare($sql);
				$req->execute();
			}
			$sql="UPDATE Formations SET for_action = " . $acl_action . "  WHERE for_id = " .$for_id;
			$req = $ConnSoc->prepare($sql);
			$req->execute();

		}
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $km_message
		);

		header("location : " .$_SESSION['retourRapport']);
		die();
	}

}else{
    // CHECK IF continue

    if(!empty($_POST['for_vehicule_id'])){
		$for_date_prec="";
		$for_demi_prec = 0;
        // CHERCHER LE RAPPORT PRECEDENT
        $sql="SELECT soc_id FROM Societes WHERE soc_archive = 0";
        $req = $Conn->query($sql);
        $d_societes=$req->fetchAll();
        foreach($d_societes as $s){
            $ConnFct = connexion_fct($s['soc_id']);
            $sql="SELECT * FROM Formations  WHERE for_vehicule_id =" . $_POST['for_vehicule_id'] . " AND for_date <= '". $_POST['for_date'] . "' ORDER BY for_date DESC, for_demi DESC";
            $req = $ConnFct->prepare($sql);
            $req->execute();
            $d_formation_prec[$s['soc_id']]=$req->fetch();
        }

		if(!empty($d_formation_prec)){
			foreach($d_formation_prec as $k=>$f){
	            if(isset($f['for_date']) && ($f['for_date']>$for_date_prec && $f['for_demi']>$for_demi_prec)){
	                $prec_rapport_km_deb =  $f['for_km_deb'];
	                $prec_rapport_km_fin = $f['for_km_fin'];
	                $for_rap_precedent = $f['for_id'];
	                $for_rap_precedent_soc = $k;
	            }
	        }
		}

        if($prec_rapport_km_deb>$km){
            //GOOD
            $continue = false;
            $km_message = "Le nbre de kilomètres ne peut pas être inférieur à " . $prec_rapport_km_deb;
        }

    }

    if($continue==true){
		if(!empty($_POST['for_vehicule_id'])){
			$for_km_deb = $km;
		}else{
			$for_km_deb = 0;
		}
        $for_km_fin = 0;
        $sql="INSERT INTO Formations (for_annee, for_semaine, for_intervenant, for_date, for_demi, for_nb_demi, for_nb_groupe, for_nb_h_groupe,
        for_total_h, for_observations, for_vehicule_categorie, for_vehicule_id, for_vehicule_numero, for_utilisation,
        for_hebergement, for_hebergement_texte, for_rap_precedent, for_rap_precedent_soc, for_km_deb, for_action)
        VALUES (:for_annee, :for_semaine, :for_intervenant, :for_date, :for_demi, :for_nb_demi, :for_nb_groupe, :for_nb_h_groupe,
        :for_total_h, :for_observations, :for_vehicule_categorie, :for_vehicule_id, :for_vehicule_numero, :for_utilisation,
        :for_hebergement, :for_hebergement_texte, :for_rap_precedent, :for_rap_precedent_soc, :for_km_deb, :for_action)";
        $req = $ConnSoc->prepare($sql);
        $req->bindValue(":for_annee",$for_annee);
        $req->bindValue(":for_semaine",$for_semaine);
        $req->bindValue(":for_intervenant",$_POST['for_intervenant']);
        $req->bindValue(":for_date",$_POST['for_date']);
        $req->bindValue(":for_demi",$_POST['for_demi']);
        $req->bindValue(":for_nb_demi",$_POST['for_nb_demi']);
        $req->bindValue(":for_nb_groupe",$for_nb_groupe);
        $req->bindValue(":for_nb_h_groupe",$for_nb_h_groupe);
        $req->bindValue(":for_total_h",$for_total_h);
        $req->bindValue(":for_observations",$_POST['for_observations']);
        $req->bindValue(":for_vehicule_categorie",$_POST['for_vehicule_categorie']);
        $req->bindValue(":for_vehicule_id",$for_vehicule_id);
        $req->bindValue(":for_vehicule_numero",$_POST['for_vehicule_numero']);
        $req->bindValue(":for_utilisation",$_POST['for_utilisation']);
        $req->bindValue(":for_hebergement",$_POST['for_hebergement']);
        $req->bindValue(":for_hebergement_texte",$_POST['for_hebergement_texte']);
        $req->bindValue(":for_rap_precedent",$for_rap_precedent);
        $req->bindValue(":for_rap_precedent_soc",$for_rap_precedent_soc);
        $req->bindValue(":for_action",$for_action);
        $req->bindValue(":for_km_deb",$for_km_deb);
        $req->execute();

        $for_id = $ConnSoc->lastInsertId();

        if(!empty($_POST['for_vehicule_id']) && !empty($for_rap_precedent_soc)){
            // ALLER UPDATE L'ANCIEN RAPPORT
            $ConnFct = connexion_fct($for_rap_precedent_soc);
            $for_new_km = abs($for_km_deb-$prec_rapport_km_deb);
            $sql="UPDATE Formations  SET for_km_fin= " . $for_km_deb . ",for_km= " . $for_new_km . ", for_rap_suivant = " . $for_id . ",for_rap_suivant_soc=" . $acc_societe . "  WHERE for_id=" . $for_rap_precedent;
            $req = $ConnFct->prepare($sql);
            $req->execute();
        }

        if(!empty($_POST['for_nb_stagiaires'])){
            foreach($_POST['for_nb_stagiaires'] as $proid=>$l_clients){
                foreach($l_clients as $cliid=>$l){
                    if(!empty($_POST['for_nb_stagiaires'][$proid][$cliid])){
                        $nb_stas = $_POST['for_nb_stagiaires'][$proid][$cliid];
                    }else{
                        $nb_stas = 0;
                    }
                    $acl_action = $_POST['for_action'][$proid][$cliid];
                    $sql="INSERT INTO formations_actions (fac_formation, fac_date, fac_demi, fac_action_client, fac_produit, fac_nb_stagiaires) VALUES (:fac_formation, :fac_date, :fac_demi, :fac_action_client, :fac_produit, :fac_nb_stagiaires)";
                    $req = $ConnSoc->prepare($sql);
                    $req->bindValue(":fac_formation",$for_id);
                    $req->bindValue(":fac_date",$_POST['for_date']);
                    $req->bindValue(":fac_demi",$_POST['for_demi']);
                    $req->bindValue(":fac_action_client",$cliid);
                    $req->bindValue(":fac_produit",$proid);
                    $req->bindValue(":fac_nb_stagiaires",$nb_stas);
                    $req->execute();
					$sql="SELECT act_gest_sta FROM Actions  WHERE act_id =" . $acl_action;
		            $req = $ConnSoc->prepare($sql);
		            $req->execute();
		            $d_action=$req->fetch();
					if($d_action['act_gest_sta']==2){
		                // INSCRIPTION SESSION ==> ON ADITIONNE TOUT
						if(empty($acl_nb_sta_rap[$cliid])){
							$acl_nb_sta_rap[$cliid] = $nb_stas;
						}else{
							$acl_nb_sta_rap[$cliid] = $acl_nb_sta_rap[$cliid] + $nb_stas;
						}

		            }else{

		                // INSCRIPTION FORMATION ==> ON PREND LA PLUS GRANDE VALEUR
						if(empty($acl_nb_sta_rap[$cliid]) OR (!empty($acl_nb_sta_rap[$cliid]) && $nb_stas > $acl_nb_sta_rap[$cliid])){
							$acl_nb_sta_rap[$cliid] = $nb_stas;
						}

		            }


                }
            }
            foreach($acl_nb_sta_rap as $c=>$n){
				$sql="UPDATE Actions_Clients SET acl_nb_sta_rap = " . $n . "  WHERE acl_client = " .$c . " AND acl_action =" . $acl_action;
				$req = $ConnSoc->prepare($sql);
				$req->execute();
			}
			$sql="UPDATE Formations SET for_action = " . $acl_action . "  WHERE for_id = " .$for_id;
			$req = $ConnSoc->prepare($sql);
			$req->execute();
        }
    }else{

        $_SESSION['message'][] = array(
            "titre" => "Erreur",
            "type" => "danger",
            "message" => $km_message
        );
        header("location : " . $_SESSION['retourRapport']);
        die();
    }

    if(!empty($erreur)){
        $_SESSION['message'][] = array(
            "titre" => "Erreur",
            "type" => "danger",
            "message" => "Le rapport n'a pas pu être enregistré"
        );
        header("location : " . $_SESSION['retourRapport']);
        die();
    }else{
        $_SESSION['message'][] = array(
            "titre" => "Succès",
            "type" => "success",
            "message" =>"Rapport enregistré"
        );
        header("location : " . $_SESSION['retourRapportVoir']);
        die();
    }

}

$_SESSION['message'][] = array(
	"titre" => "Succès",
	"type" => "success",
	"message" =>"Rapport enregistré"
);
header("location : " . $_SESSION['retourRapportVoir']);
die();
