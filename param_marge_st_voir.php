<?php
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");

// DROITS D'ACCES
if($_SESSION['acces']["acc_profil"]!=13 AND $_SESSION['acces']["acc_service"][2]!=1){
    echo("Accès refusé!");
    die();
}

// FAMILLES
$req = $Conn->prepare("SELECT * FROM produits_familles,produits_sous_familles WHERE pfa_id=psf_pfa_id ORDER BY pfa_libelle,psf_libelle");
$req->execute();
$familles = $req->fetchAll();

// LES PROFILS DE DERO

/*$tab_dero_lib=array();
$req = $Conn->prepare("SELECT pro_id,pro_libelle FROM profils WHERE pro_id IN (15,10,8) ORDER BY pro_id");
$req->execute();
$d_profils = $req->fetchAll();
if(!empty($d_profils)){
	foreach($d_profils as $profil){
		switch ($profil["pro_id"]) {
			case 15:
				$tab_dero_lib[1]=$profil["pro_libelle"];
				break;
			case 10:
				$tab_dero_lib[2]=$profil["pro_libelle"];
				break;
			case 8:
				$tab_dero_lib[3]=$profil["pro_libelle"];
				break;
		}
	}
}*/


?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<div id="main">
<?php        include "includes/header_def.inc.php";?>
			<section id="content_wrapper">
                <section id="content" class="animated fadeIn">

                    <h1>Marges sous-traitance<h1>
					<h2>Paliers de dérogation par famille de produit</h2>

					<p class="alert alert-info" >
						L'accès à la validation de chaque niveau est géré par la grille de validation "Sous-traitance".
					</p>

					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="dark" >
									<th rowspan="2" >Famille</th>
									<th rowspan="2" >Sous-famille</th>
									<th colspan="2" class="text-center" >Niveau 0</th>
									<th colspan="2" class="text-center" >Niveau 1</th>
                                    <th colspan="2" class="text-center" >Niveau 2</th>
								</tr>
                                <tr class="dark" >									
									<th>Marge (M)</th>
									<th>Taux de marge (TM)</th>
                                    <th>Marge (M)</th>
									<th>Taux de marge (TM)</th>
                                    <th>Marge (M)</th>
									<th>Taux de marge (TM)</th>
								</tr>
							</thead>
							<tbody>
                        <?php   foreach ($familles as $f){ ?>
                                    <tr>
                                        <td><?= $f['pfa_libelle'] ?></td>
                                        <td><?= $f['psf_libelle'] ?></td>
                                        <td>M > <?=$f['psf_marge_montant_1'] ?> €</td>
                                        <td>TM > <?= $f['psf_marge_taux_1'] ?> %</td>
										<td><?=$f['psf_marge_montant_1'] ?> € >= M > <?=$f['psf_marge_montant_2'] ?> €</td>
                                        <td><?=$f['psf_marge_taux_1'] ?> % >= M > <?=$f['psf_marge_taux_2'] ?> %</td>
										<td><?=$f['psf_marge_montant_2'] ?> € >= M</td>
                                        <td>T<?=$f['psf_marge_taux_2'] ?> % >= TM</td>
                                    </tr>
                        <?php   } ?>
							</tbody>
						</table>
					</div>
				</section>
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-long-arrow-left"></span>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="param_marge_st.php" class="btn btn-warning btn-sm" role="button" >
						<span class="fa fa-pencil"></span>
						Modifier les valeurs
					</a>
				</div>
			</div>
		</footer>
<?php
		include "includes/footer_script.inc.php"; ?>	
		
	</body>
</html>
