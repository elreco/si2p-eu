 <?php
 
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");
	include_once("includes/connexion_fct.php");
	
	// LISTE DES DIPLOMES LIE A UNE ACTION

	$action_id=0;
	if(isset($_GET["action"])){
		if(!empty($_GET["action"])){
			$action_id=$_GET["action"];
		}	
	}
	if(empty($action_id)){
		echo("Impossible d'afficher la page");
		die();
	}
	
	$_SESSION["retourCaces"]="action_qualif_liste.php?action=" . $action_id . "&societ=" . $conn_soc_id;
	$_SESSION["retourSsiap"]="action_qualif_liste.php?action=" . $action_id . "&societ=" . $conn_soc_id;;
	$_SESSION["retourAttest"]="action_qualif_liste.php?action=" . $action_id . "&societ=" . $conn_soc_id;
	$_SESSION["retourQualif"]="action_qualif_liste.php?action=" . $action_id . "&societ=" . $conn_soc_id;
	
	// LES STAGIAIRES -> ne sont pas sur la meme base
		
	$liste_sta="";
	$sql="SELECT ast_stagiaire,ast_qualification,ast_diplome,ast_diplome_statut FROM Actions_Stagiaires WHERE ast_action=" . $action_id . ";";
	$req=$ConnSoc->query($sql);
	$d_diplomes=$req->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($d_diplomes)){
		$tab_sta=array_column ($d_diplomes,"ast_stagiaire");
		$liste_sta=implode($tab_sta,",");
		
		$qualification=$d_diplomes[0]["ast_qualification"];
	}
	
	// SUR LA QUALIF
	
	$sql="SELECT qua_caces,qua_ssiap FROM Qualifications WHERE qua_id=" . $qualification . ";";
	$req=$Conn->query($sql);
	$d_qualification=$req->fetch();
	
	// DONNEE ORION_0 DE CHAQUE STAGIAIRE
	
	if(!empty($d_diplomes)){
		
		if($d_qualification["qua_caces"]==1){
			
			$sql="SELECT sta_id,sta_nom,sta_prenom,sta_naissance,dca_diplome,dca_id FROM Stagiaires LEFT JOIN Diplomes_Caces ON (Stagiaires.sta_id=Diplomes_Caces.dca_stagiaire AND dca_id=:diplome)
			WHERE sta_id=:stagiaire;";
			$req=$Conn->prepare($sql);
			
			foreach($d_diplomes as $num => $dip){
				
				$req->bindParam("stagiaire",$dip["ast_stagiaire"]);
				$req->bindParam("diplome",$dip["ast_diplome"]);
				$req->execute();
				$result_sta=$req->fetch();
				if(!empty($result_sta)){
					$d_diplomes[$num]["sta_nom"]=$result_sta["sta_nom"];
					$d_diplomes[$num]["sta_prenom"]=$result_sta["sta_prenom"];
					$d_diplomes[$num]["sta_naissance"]=$result_sta["sta_naissance"];
					$d_diplomes[$num]["numero"]=$result_sta["dca_id"];
				}
			}
			
		}elseif($d_qualification["qua_ssiap"]==1){
		
			// DIPLOME SSIAP
			
			$sql="SELECT sta_id,sta_nom,sta_prenom,sta_naissance,dss_numero FROM Stagiaires LEFT JOIN Diplomes_Ssiap ON (Stagiaires.sta_id=Diplomes_Ssiap.dss_stagiaire AND dss_id=:diplome)
			WHERE sta_id=:stagiaire;";
			$req=$Conn->prepare($sql);
			
			foreach($d_diplomes as $num => $dip){
				
				$req->bindParam("stagiaire",$dip["ast_stagiaire"]);
				$req->bindParam("diplome",$dip["ast_diplome"]);
				$req->execute();
				$result_sta=$req->fetch();
				if(!empty($result_sta)){
					$d_diplomes[$num]["sta_nom"]=$result_sta["sta_nom"];
					$d_diplomes[$num]["sta_prenom"]=$result_sta["sta_prenom"];
					$d_diplomes[$num]["sta_naissance"]=$result_sta["sta_naissance"];
					$d_diplomes[$num]["numero"]=$result_sta["dss_numero"];
				}
			}
			
		}
		
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

		<!-- PERSO Si2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
	   
	   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="#" id="form_print" >
			<div>
				<input type="hidden" name="print" value="1" />
				<input type="hidden" name="action" value="<?=$action_id?>" />
				<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
			</div>
			<div id="main">
    <?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					

					<section id="content" class="animated">
					
				<?php	echo("<h1>Action N° " . $action_id . ": Liste des diplômes</h1>");
					
						if(!empty($d_diplomes)){ ?>
							<div class="admin-form theme-primary">
								
									<table class="table table-striped" >
										<thead>
											<tr class="dark" >
												<th>Stagiaires</th>
												<th colspan="2" >Dossiers</th>
												<th>Statut</th>
										<?php	if($d_qualification["qua_caces"]==1){ ?>
													<th class="text-center" >Certificats CACES®</th>
										<?php	}elseif($d_qualification["qua_ssiap"]==1){ ?>
													<th class="text-center" >Diplôme SSIAP</th>
										<?php	} ?>
												
												<th class="text-center" >Attestations</th>														
											</tr>
										</thead>
										<tbody id="tabRappelBody" >
						<?php				foreach($d_diplomes as $d){ ?>
												<tr>												
													<td><?=$d["sta_nom"] . " " . $d["sta_prenom"]?></td>																							
											<?php	if(!empty($d["ast_diplome"])){ ?>
														<td>
											<?php			echo("N° " . $d["numero"]); ?>
														</td>
														<td>
											<?php			if($d_qualification["qua_caces"]==1){ ?>	
																<a href="dip_caces_voir.php?dossier=<?=$d["ast_diplome"]?>&action=<?=$action_id?>&societ=<?=$conn_soc_id?>" class="btn btn-sm btn-info" >
																	<i class="fa fa-eye" ></i>
																</a>
											<?php			}elseif($d_qualification["qua_ssiap"]==1){ ?>
																<a href="dip_ssiap_voir.php?diplome=<?=$d["ast_diplome"]?>&action=<?=$action_id?>&societ=<?=$conn_soc_id?>" class="btn btn-sm btn-info" >
																	<i class="fa fa-eye" ></i>
																</a>
											<?php			} ?>
														</td> 
											<?php	}else{ ?>
														<td>Non renseigné</td>
														<td>
											<?php			if($d_qualification["qua_caces"]==1){ ?>	
																<a href="dip_caces.php?action=<?=$action_id?>&stagiaire=<?=$d["ast_stagiaire"]?>&qualification=<?=$d["ast_qualification"]?>&societ=<?=$conn_soc_id?>" class="btn btn-sm btn-success" >
																	<i class="fa fa-plus" ></i>
																</a>
											<?php			}elseif($d_qualification["qua_ssiap"]==1){ ?>
																<a href="dip_ssiap.php?action=<?=$action_id?>&stagiaire=<?=$d["ast_stagiaire"]?>&qualification=<?=$d["ast_qualification"]?>&societ=<?=$conn_soc_id?>" class="btn btn-sm btn-success" >
																	<i class="fa fa-plus" ></i>
																</a>
											<?php			} ?>
														</td> 
											<?php	} ?>
											<?php	if($d["ast_diplome_statut"]==1){ ?>
														<td class="text-success" >
															Validée
														</td>
														<td class="text-center" >
															<div class="option-group field">
																<label class="option option-dark">
																	<input type="checkbox" name="dossiers[]" value="<?=$d["ast_diplome"]?>" >	
																	<span class="checkbox"></span>
																</label>
															</div>
														</td>
											<?php	}elseif($d["ast_diplome_statut"]==2){ ?>
														<td class="text-danger" >
															Echec
														</td>
														<td>&nbsp;</td>
											<?php	}else{ ?>
														<td>
															Non renseigné
														</td>
														<td>&nbsp;</td>
											<?php	} ?>
													<td class="text-center" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" name="stagiaires[]" value="<?=$d["ast_stagiaire"]?>" >	
																<span class="checkbox"></span>
															</label>
														</div>
													</td>
												</tr>
						<?php				} ?>					
										</tbody>
										<tfoot>
											<tr>
												<td colspan="4" >&nbsp;</td>
												<td class="text-center" >
													<button type="button" class="btn btn-sm btn-info" id="print_certif"  >
														<i class="fa fa-print"></i>
													</button>	
												</td>
												<td class="text-center" >
													<button type="button" class="btn btn-sm btn-info" id="print_attest" >
														<i class="fa fa-print"></i>
													</button>	
												</td>
											</tr>
									</table>
								
							</div>
				<?php	}else{ ?>
							<p class="alert alert-warning" >
								Aucun diplôme n'est associé à cette formation.
							</p>
				<?php	} ?>			
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="action_voir_sta.php?action=<?=$action_id?>&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i> Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>

<?php	
		include "includes/footer_script.inc.php"; ?>		
		<script type="text/javascript">
		
			jQuery(document).ready(function (){
				
				$("#print_certif").click(function(){
					
					if($("input[name='dossiers[]']:checked").length>0){
				<?php	if($d_qualification["qua_caces"]==1){ ?>
							$("#form_print").prop("action","dip_caces_print.php");
				<?php	}elseif($d_qualification["qua_ssiap"]==1){ ?>
							$("#form_print").prop("action","dip_ssiap_voir.php");
				<?php	} ?>		
						$("#form_print").submit();						
					}else{
						modal_alerte_user("Vous n'avez selectionné aucun document","");
					}
				});
				
				$("#print_attest").click(function(){					
					if($("input[name='stagiaires[]']:checked").length>0){
						$("#form_print").prop("action","attestation_ind_voir.php");
						$("#form_print").submit();						
					}else{
						modal_alerte_user("Vous n'avez selectionné aucun document","");
					}
				});
				
				
				// ----- FIN DOC READY -----
			});
			
					
		</script>
	</body>
</html>
