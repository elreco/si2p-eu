<?php
	include('../../includes/connexion.php');
	include('../../includes/connexion_fct.php');
	
	$sql="SELECT soc_id FROM Societes ORDER BY soc_id;";
	$req=$Conn->query($sql);
	$d_societes=$req->fetchAll();
	if(!empty($d_societes)){
		foreach($d_societes as $soc){
			
			echo("societe " . $soc["soc_id"] . "<br/>");
			
			$ConnFct=connexion_fct($soc["soc_id"]);
			
			try{
				$ConnFct->query("ALTER TABLE `suspects` ADD `sus_opca_type` INT NULL DEFAULT '0' AFTER `sus_facture_opca`;");
			}Catch(Exception $e){
				echo("ADD sus_opca_type : " . $e->getMessage() . "<br/>"); 
			}
			
			try{
				$ConnFct->query("UPDATE Suspects SET sus_opca_type=5 WHERE sus_opca>0;");
			}Catch(Exception $e){
				echo("UPDATE sus_opca_type : " . $e->getMessage() . "<br/>"); 
			}
			
			try{
				$ConnFct->query("ALTER TABLE `factures` ADD `fac_opca_type` INT NULL DEFAULT '0' AFTER `fac_regle`;");
			}Catch(Exception $e){
				echo("ADD fac_opca_type : " . $e->getMessage() . "<br/>"); 
			}
			
			try{
				$ConnFct->query("UPDATE factures SET fac_opca_type=5 WHERE fac_opca>0;");
			}Catch(Exception $e){
				echo("UPDATE fac_opca_type : " . $e->getMessage() . "<br/>"); 
			}
			
			try{
				$ConnFct->query("ALTER TABLE `factures` ADD `fac_opca_compte` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Si fac_opca, ce champ contient le nom du client pour le compte duquel on facture l\'OPCA' AFTER `fac_cli_code`;");
			}Catch(Exception $e){
				echo("ADD fac_opca_compte : " . $e->getMessage() . "<br/>"); 
			}
			
			try{
				$ConnFct->query("ALTER TABLE `factures` CHANGE `fac_cli_code` `fac_cli_code` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'code du client que l\'on facture', CHANGE `fac_cli_nom` `fac_cli_nom` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'nom de facturation. nom du client ou nom du financeur si facturation à un financeur';");
			}Catch(Exception $e){
				echo("factures ADD commentaires : " . $e->getMessage() . "<br/>"); 
			}

		}
	}
	try{
		$Conn->query("ALTER TABLE `clients` ADD `cli_opca_type` INT NULL DEFAULT '0' AFTER `cli_opca`;");
	}Catch(Exception $e){
		echo("ADD cli_opca_type : " . $e->getMessage() . "<br/>"); 
	}
	
	try{
		$Conn->query("UPDATE clients SET cli_opca_type=5 WHERE cli_opca>0;");
	}Catch(Exception $e){
		echo("UPDATE cli_opca_type : " . $e->getMessage() . "<br/>"); 
	}

?>