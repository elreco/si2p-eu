<?php

include('../../includes/connexion.php');
include('../../includes/connexion_fct.php');

// TRAITEMENT SUR BASE 0


try{
	$req=$Conn->query("ALTER TABLE `banques` ADD `ban_archive` BOOLEAN NULL DEFAULT FALSE AFTER `ban_libelle`, ADD `ban_factor` BOOLEAN NULL DEFAULT FALSE AFTER `ban_archive`;");
}Catch(Exception $e){
	echo("ALTER BANQUE ban_archive : " . $e->getMessage() . "<br/>");
	die();
}
try{
	$req=$Conn->query("ALTER TABLE `banques` ADD `ban_code` VARCHAR(10) NULL AFTER `ban_id`;");
}Catch(Exception $e){
	echo("ALTER BANQUE ban_code: " . $e->getMessage() . "<br/>");
	die();
}



try{
	$req=$Conn->query("UPDATE Banques SET ban_archive=1 WHERE ban_id IN (1,2,4,5,6,8,9,10,12,13,14,15,16,17,18,19);");
}Catch(Exception $e){
	echo("ARCHIVE BANQUE : " . $e->getMessage() . "<br/>");
	die();
}

try{
	$req=$Conn->query("UPDATE Banques SET ban_code='BNP' WHERE ban_id=3;");
}Catch(Exception $e){
	echo("UPDATE BANQUE BNP : " . $e->getMessage() . "<br/>");
	die();
}
try{
	$req=$Conn->query("UPDATE Banques SET ban_code='HSBC' WHERE ban_id=7;");
}Catch(Exception $e){
	echo("UPDATE BANQUE HSBC : " . $e->getMessage() . "<br/>");
}
try{
	$req=$Conn->query("UPDATE Banques SET ban_code='CDN76' WHERE ban_id=11;");
}Catch(Exception $e){
	echo("UPDATE BANQUE CDN76 : " . $e->getMessage() . "<br/>");
	die();
}
try{
	$req=$Conn->query("UPDATE Banques SET ban_code='Autre' WHERE ban_id=20;");
}Catch(Exception $e){
	echo("UPDATE BANQUE Autre : " . $e->getMessage() . "<br/>");
	die();
}

try{
	$req=$Conn->query("INSERT INTO `banques` (`ban_id`, `ban_code`, `ban_libelle`, `ban_archive`, `ban_factor`) VALUES 
	(NULL, 'CE76', 'Caisse d\'épargne Normandie', '0', '0'),
	(NULL, 'CE49', 'Caisse d\'épargne Pays de Loire', '0', '0'),
	(NULL, 'CIC76', 'CIC NORD', '0', '0'),
	(NULL, 'CIC49', 'CIC OUEST', '0', '0'),
	(NULL, 'BPA49', 'BPA Maine et Loire', '0', '0'),
	(NULL, 'BPA68', 'BPA Alsace Lorraine Champagne', '0', '0'),
	(NULL, 'FACTOR', 'Compte BQ dédié encaissement affacturage', '0', '1');");
}Catch(Exception $e){
	echo("AJOUT BANQUE : " . $e->getMessage() . "<br/>");
	die();
}

try{
	$req=$Conn->query("ALTER TABLE `rib` ADD `rib_banque` INT NULL DEFAULT '0' AFTER `rib_affacturage`;");
}Catch(Exception $e){
	echo("ALTER RIB  rib_banque : " . $e->getMessage() . "<br/>");
	die();
}

// UPDATE DES BANQUES EN FONCTION DES RIB

// BNP
try{
	$req=$Conn->query("UPDATE `rib` SET `rib_banque` = '3' WHERE rib_id IN (1,35,26,44) ;");
}Catch(Exception $e){
	echo(" : " . $e->getMessage() . "<br/>");
	die();
}

//HSBC
try{
	$req=$Conn->query("UPDATE `rib` SET `rib_banque` = '7' WHERE rib_id IN (7,8,22,32,36,24) ;");
}Catch(Exception $e){
	echo(": " . $e->getMessage() . "<br/>");
	die();
}

//CE NORMANDIE
try{
	$req=$Conn->query("UPDATE `rib` SET `rib_banque` = '21' WHERE rib_id IN (20,39,40,38,21) ;");
}Catch(Exception $e){
	echo(" : " . $e->getMessage() . "<br/>");
	die();
}

//Caisse d'épargne Pays de Loire
try{
	$req=$Conn->query("UPDATE `rib` SET `rib_banque` = '22' WHERE rib_id IN (2,4,5,12) ;");
}Catch(Exception $e){
	echo(" : " . $e->getMessage() . "<br/>");
	die();
}

// CIC NORD
try{
	$req=$Conn->query("UPDATE `rib` SET `rib_banque` = '23' WHERE rib_id IN (16,27,28,29,30,31,33,34,18);");
}Catch(Exception $e){
	echo(" : " . $e->getMessage() . "<br/>");
	die();
}

// CIC OUEST
try{
	$req=$Conn->query("UPDATE `rib` SET `rib_banque` = '24' WHERE rib_id IN (3,9,10,11,13,14);");
}Catch(Exception $e){
	echo(" : " . $e->getMessage() . "<br/>");
	die();
}
// BPA Maine et Loire
try{
	$req=$Conn->query("UPDATE `rib` SET `rib_banque` = '25' WHERE rib_id IN (15);");
}Catch(Exception $e){
	echo(" : " . $e->getMessage() . "<br/>");
	die();
}
// BPA Alsace
try{
	$req=$Conn->query("UPDATE `rib` SET `rib_banque` = '26' WHERE rib_id IN (25);");
}Catch(Exception $e){
	echo(" : " . $e->getMessage() . "<br/>");
	die();
}
// BPA Alsace
try{
	$req=$Conn->query("UPDATE `rib` SET `rib_banque` = '26' WHERE rib_id IN (25);");
}Catch(Exception $e){
	echo(" : " . $e->getMessage() . "<br/>");
	die();
}
// BPA Alsace
try{
	$req=$Conn->query("UPDATE `rib` SET `rib_banque` = '27' WHERE rib_id IN (45,48,47,49);");
}Catch(Exception $e){
	echo(" : " . $e->getMessage() . "<br/>");
	die();
}

// DONNEE EN BASE N

$req=$Conn->query("SELECT soc_id FROM Societes ORDER BY soc_id;");
$d_societe=$req->fetchAll();
if(!empty($d_societe)){
	
	foreach($d_societe as $soc){
		
		echo("Societe : " . $soc["soc_id"] . "<br/>");
		
		$ConnFct=connexion_fct($soc["soc_id"]);
		
		try{
			$req=$ConnFct->query("ALTER TABLE `reglements` ADD `reg_rb` BOOLEAN NULL DEFAULT FALSE AFTER `reg_uti_enr`, ADD `reg_rb_date` DATE NULL DEFAULT NULL AFTER `reg_rb`;");
		}Catch(Exception $e){
			echo("ALTER reglements reg_rb : " . $e->getMessage() . "<br/>");
			die();
		}
		
		try{
			$req=$ConnFct->query("ALTER TABLE `factures` ADD `fac_aff_retro` BOOLEAN NULL DEFAULT FALSE AFTER `fac_aff_remise`, ADD `fac_aff_retro_date` DATE NULL DEFAULT NULL AFTER `fac_aff_retro`;");
		}Catch(Exception $e){
			echo(" ALTER FACTURES fac_aff_retro : " . $e->getMessage() . "<br/>");
			die();
		}
		
	
		if($soc["soc_id"]!=15 AND $soc["soc_id"]<17){
			
			// on passe les virement swicht Héraclès en virement ORION
			
			$sql="UPDATE Reglements SET reg_type=2 WHERE reg_date<'2019-04-01' AND reg_type=6;";
			try{
				$req=$ConnFct->query($sql);
			}Catch(Exception $e){
				echo("BASCULE VIR SWITCH en VIR : " . $e->getMessage() . "<br/>");
				die();
			}	
		}
		
		// on passe les especes Héraclès 8 en espece ORION 6
		$sql="UPDATE Reglements SET reg_type=6 WHERE reg_date<'2019-04-01' AND reg_type=8;";
		try{
			$req=$ConnFct->query($sql);
		}Catch(Exception $e){
			echo("BASCULE ESP HERACLES EN ESP ORION : " . $e->getMessage() . "<br/>");
			die();
		}
	}
}

// ON GERE LES TYPES DE REGMENTS EN CONSEQUENCE

try{
$req=$Conn->query("ALTER TABLE `reglements_types` ADD `rty_archive` BOOLEAN NULL DEFAULT FALSE AFTER `rty_code`;");
}Catch(Exception $e){
	echo("ADD rty_archive : " . $e->getMessage() . "<br/>");
	die();
}
try{
	$req=$Conn->query("UPDATE `reglements_types` SET rty_archive=1 WHERE rty_id IN (4,5);");
}Catch(Exception $e){
	echo("ARCHIVE reglements_types : " . $e->getMessage() . "<br/>");
	die();
}
try{
	$req=$Conn->query("UPDATE `reglements_types` SET `rty_libelle` = 'Traite' WHERE `reglements_types`.`rty_id` = 3;");
}Catch(Exception $e){
	echo("rename reg_type 3 : " . $e->getMessage() . "<br/>");
	die();
}
try{
	$req=$Conn->query("UPDATE `reglements_types` SET `rty_libelle` = 'Ajustement' WHERE `reglements_types`.`rty_id` = 7;");
}Catch(Exception $e){
	echo("rename reg_type 7 : " . $e->getMessage() . "<br/>");
	die();
}
try{
	$req=$Conn->query("UPDATE `reglements_types` SET `rty_libelle` = 'CB', `rty_code` = 'U' WHERE `reglements_types`.`rty_id` = 8;");
}Catch(Exception $e){
	echo("rename reg_type 8 : " . $e->getMessage() . "<br/>");
	die();
}
try{
	$req=$Conn->query("UPDATE `reglements_types` SET `rty_libelle` = 'Espèces', `rty_code` = '' WHERE `reglements_types`.`rty_id` = 6;");
}Catch(Exception $e){
	echo("rename reg_type 8 : " . $e->getMessage() . "<br/>");
	die();
}
	


echo("TERMINE");
?>