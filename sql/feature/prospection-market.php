<?php

include('../../includes/connexion.php');
include('../../includes/connexion_fct.php');

	
$sql="SELECT soc_id FROM Societes ORDER BY soc_id;";
$req=$Conn->query($sql);
$d_societe=$req->fetchAll();
if(!empty($d_societe)){
	
	foreach($d_societe as $soc){
		
		// TRAITEMENT SUR BASE N
		
		echo("SOCIETE : " . $soc["soc_id"] . "<br/>");
		
		$ConnFct=connexion_fct($soc["soc_id"]);
		
		$sql="ALTER TABLE `clients` ADD `cli_facture` BOOLEAN NULL DEFAULT FALSE AFTER `cli_affacturable`;";
		try{
			$req=$ConnFct->query($sql);
		}Catch(Exception $e){
			echo("clients add cli_facture : " . $e->getMessage() . "<br/>");
		}
		
		// INI DE LA VALEUR cli_facture
		
		$sql="SELECT cli_id FROM Clients,Clients_Societes WHERE cli_id=cso_client 
		AND NOT ISNULL(cli_first_facture) AND cli_first_facture>0 AND cso_societe=" . $soc["soc_id"] . ";";
		$req=$Conn->query($sql);
		$d_clients_facture=$req->fetchAll();
		if(!empty($d_clients_facture)){
			
			$sql_cli="UPDATE Clients SET cli_facture=1 WHERE cli_id=:client;";
			$req_cli=$ConnFct->prepare($sql_cli);
			
			foreach($d_clients_facture as $cli_fac){
				$req_cli->bindParam(":client",$cli_fac["cli_id"]);
				try{
					$req_cli->execute();
				}Catch(Exception $e){
					echo("INI cli_facture : " . $e->getMessage() . "<br/>");
					die();
				}
			}
		}
		
		$sql="ALTER TABLE `suspects` ADD `sus_tel` VARCHAR(20) NULL AFTER `sus_import`;";
		try{
			$req=$ConnFct->query($sql);
		}Catch(Exception $e){
			echo("Suspects add sus_tel : " . $e->getMessage() . "<br/>");
		}
		
	}
}

// TRAITEMENT SUR BASE 0

$sql="ALTER TABLE `clients` ADD `cli_tel` VARCHAR(20) NULL AFTER `cli_hors_concours`;";
try{
	$req=$Conn->query($sql);
}Catch(Exception $e){
	echo("clients add cli_tel : " . $e->getMessage() . "<br/>");
}


echo("TERMINE");
?>