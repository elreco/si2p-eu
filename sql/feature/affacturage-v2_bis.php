<?php

include('../../includes/connexion.php');
include('../../includes/connexion_fct.php');

// UTILISATION D'UN SCRIPT BIS CAR CELUI LA PEUT ETRE EXECUTE PLUSIEUR FOIS CONTRAIREMENT AU PREMIER


$sql_fac="SELECT fac_id,fac_numero,fac_date FROM Factures WHERE fac_rib_iban=:rib_iban AND fac_rib_bic=:rib_bic AND NOT fac_rib=:rib_id;";

$sql_up_fac="UPDATE Factures SET fac_rib=:rib_id WHERE fac_rib_iban=:rib_iban AND fac_rib_bic=:rib_bic AND NOT fac_rib=:rib_id;";

$req_rib=$Conn->prepare("SELECT rib_id,rib_iban,rib_bic,ban_code FROM Rib LEFT JOIN Banques ON (Rib.rib_banque=Banques.ban_id) 
WHERE NOT rib_iban='' AND NOT rib_bic='' AND rib_societe=:societe ORDER BY rib_id;");

$req=$Conn->query("SELECT soc_id FROM Societes ORDER BY soc_id;");
$d_societe=$req->fetchAll();
if(!empty($d_societe)){
	
	foreach($d_societe as $soc){
		
		echo("Societe : " . $soc["soc_id"] . "<br/>");
		
		$ConnFct=connexion_fct($soc["soc_id"]);
		
		// ON PARCOURS LES RIB DE LA SOCIETE
		
		//$req_fac=$ConnFct->prepare($sql_fac);
		$req_up_fac=$ConnFct->prepare($sql_up_fac);
		
		$req_rib->bindParam(":societe",$soc["soc_id"]);
		$req_rib->execute();
		$d_ribs=$req_rib->fetchAll();
		if(!empty($d_ribs)){
			
			foreach($d_ribs as $rib){
				
				// MODE ACTU
				$req_up_fac->bindParam("rib_id",$rib["rib_id"]);
				$req_up_fac->bindParam("rib_iban",$rib["rib_iban"]);
				$req_up_fac->bindParam("rib_bic",$rib["rib_bic"]);
				try{
					$req_up_fac->execute();				
				}Catch(Exception $e){
					echo("MAJ FAC RIB societe " . $soc["soc_id"] . " RIB " . $rib["rib_id"] . " : " . $e->getMessage() . "<br/>");
					die();
				}
				
				// MODE CONTROLE
				
				/*echo("RIB : " . $rib["rib_id"] . "<br/>");
				echo("IBAN : " . $rib["rib_iban"] . "<br/>");
				echo("BIC : " . $rib["rib_bic"] . "<br/>");
				echo("BANQUE : " . $rib["ban_code"] . "<br/>");
				
				
				$req_fac->bindParam("rib_id",$rib["rib_id"]);
				$req_fac->bindParam("rib_iban",$rib["rib_iban"]);
				$req_fac->bindParam("rib_bic",$rib["rib_bic"]);
				$req_fac->execute();
				$d_factures=$req_fac->fetchAll();
				if(!empty($d_factures)){
					
					foreach($d_factures as $fac){
						
						echo($fac["fac_numero"] . "<br/>");

					}
					
				}
				
				echo("<br/><br/>");*/
			}
		}else{
			echo("Pas de RIB<br/>");
		}
		
		
	}

}
echo("TERMINE");
?>