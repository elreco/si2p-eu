<?php

	include('../../includes/connexion.php');
    include('../../includes/connexion_fct.php');
    

    // liste des qualifications CACES

     $sql="SELECT qua_id,qua_libelle FROM Qualifications WHERE qua_caces ORDER BY qua_libelle;";
     $req=$Conn->query($sql);
     $src_qualif=$req->fetchAll(PDO::FETCH_ASSOC);
    if(!empty($src_qualif)){
        $tab_qualif=array_column ($src_qualif,"qua_id");
        $liste_qualif=implode($tab_qualif,",");
    }

    if(!empty($liste_qualif)){

        // recherche du diplome
        $sql_get_diplome="SELECT dca_id,dca_stagiaire
        ,dca_action_1,dca_action_client_1,dca_action_soc_1
        ,dca_action_2,dca_action_client_2,dca_action_soc_2
        ,dca_action_3,dca_action_client_3,dca_action_soc_3
        ,dca_action_4,dca_action_client_4,dca_action_soc_4
        ,dca_action_5,dca_action_client_5,dca_action_soc_5
        ,dca_action_6,dca_action_client_6,dca_action_soc_6
        FROM diplomes_caces WHERE dca_id=:diplome;";
        $req_get_diplome=$Conn->prepare($sql_get_diplome);

        //maj du diplome

        $sql_up_diplome="UPDATE diplomes_caces SET dca_action_client_1=:dca_action_client_1
        ,dca_action_client_2=:dca_action_client_2
        ,dca_action_client_3=:dca_action_client_3
        ,dca_action_client_4=:dca_action_client_4
        ,dca_action_client_5=:dca_action_client_5
        ,dca_action_client_6=:dca_action_client_6
        WHERE dca_id=:diplome;";
        $req_up_diplome=$Conn->prepare($sql_up_diplome);



        $req=$Conn->query("SELECT soc_id FROM Societes ORDER BY soc_id;");
        $d_societe=$req->fetchAll();
        if(!empty($d_societe)){
            
            foreach($d_societe as $soc){
                
                echo("Societe : " . $soc["soc_id"] . "<br/>");
                
                $ConnFct=connexion_fct($soc["soc_id"]);
                
                $sql_get_stagiaires="SELECT ast_stagiaire,ast_action,ast_action_client,ast_diplome FROM Actions_Stagiaires,Actions,Actions_Clients
                WHERE ast_action_client=acl_id AND acl_action=act_id AND NOT acl_archive AND NOT act_archive
                AND ast_qualification IN (" .  $liste_qualif . ") AND ast_diplome>0;";
                $req_get_stagiaires=$ConnFct->query($sql_get_stagiaires);
                $d_stagiaires=$req_get_stagiaires->fetchAll();
                if(!empty($d_stagiaires)){

                    foreach ($d_stagiaires as $sta){

                       
                       
                        $req_get_diplome->bindParam("diplome",$sta["ast_diplome"]);
                        $req_get_diplome->execute();
                        $d_diplome= $req_get_diplome->fetch();
                        if (!empty($d_diplome)) {

                        

                            if($d_diplome["dca_stagiaire"]==$sta["ast_stagiaire"]) {

                                $action_client=array(
                                    "1" => array(
                                        "action_client" => $d_diplome["dca_action_client_1"],
                                        "action" => $d_diplome["dca_action_1"],
                                        "societe" => $d_diplome["dca_action_soc_1"]
                                    ),
                                    "2" => array(
                                        "action_client" => $d_diplome["dca_action_client_2"],
                                        "action" => $d_diplome["dca_action_2"],
                                        "societe" => $d_diplome["dca_action_soc_2"]
                                    ), 
                                    "3" => array(
                                        "action_client" => $d_diplome["dca_action_client_3"],
                                        "action" => $d_diplome["dca_action_3"],
                                        "societe" => $d_diplome["dca_action_soc_3"]
                                    ),
                                    "4" => array(
                                        "action_client" => $d_diplome["dca_action_client_4"],
                                        "action" => $d_diplome["dca_action_4"],
                                        "societe" => $d_diplome["dca_action_soc_4"]
                                    ),
                                    "5" => array(
                                        "action_client" => $d_diplome["dca_action_client_5"],
                                        "action" => $d_diplome["dca_action_5"],
                                        "societe" => $d_diplome["dca_action_soc_5"]
                                    ),
                                    "6" => array(
                                        "action_client" => $d_diplome["dca_action_client_6"],
                                        "action" => $d_diplome["dca_action_6"],
                                        "societe" => $d_diplome["dca_action_soc_6"]
                                    )
                                );
                               

                                for ($bcl=1;$bcl<=6;$bcl++){
                                    
                                    if($d_diplome["dca_action_" . $bcl] == $sta["ast_action"] AND $d_diplome["dca_action_soc_" . $bcl] == $soc["soc_id"]) {

                                        if(!empty($action_client[$bcl]["action_client"]) AND $action_client[$bcl]["action_client"]!=$sta["ast_action_client"]){
                                            echo($sta["ast_action_client"]);
                                            echo("<br/>");
                                            echo($sta["ast_diplome"]);
                                            echo("delta action client");
                                            die();
                                        }else{
                                            
                                            $action_client[$bcl]["action_client"]=$sta["ast_action_client"];
                                            break;
                                        }
                                    }
                                }
          
                                $req_up_diplome->bindParam("diplome",$sta["ast_diplome"]);
                                $req_up_diplome->bindParam("dca_action_client_1", $action_client["1"]["action_client"]);
                                $req_up_diplome->bindParam("dca_action_client_2", $action_client["2"]["action_client"]);
                                $req_up_diplome->bindParam("dca_action_client_3", $action_client["3"]["action_client"]);
                                $req_up_diplome->bindParam("dca_action_client_4", $action_client["4"]["action_client"]);
                                $req_up_diplome->bindParam("dca_action_client_5", $action_client["5"]["action_client"]);
                                $req_up_diplome->bindParam("dca_action_client_6", $action_client["6"]["action_client"]);
                                $req_up_diplome->execute();
                                

                            } else {
                                echo("error");
                                die();
                            }
                                
                        }
        
                    } 
                } else {
                    echo("pas de stagiaire");
                   
                }

                echo("<br/>");
				
			}
			
        }


        
        // ajout des champs pour dupliquer les données actions dans diplomes_caces_cat

        try {
            $Conn->query("ALTER TABLE `diplomes_caces_cat` ADD `dcc_action` INT NOT NULL AFTER `dcc_numero`, ADD `dcc_action_client` INT NOT NULL AFTER `dcc_action`, ADD `dcc_action_soc` INT NOT NULL AFTER `dcc_action_client`;");
        } Catch(Exception $e) {
            echo("diplomes_caces_cat add donnee action : " .  $e->getMessage());
        }

        $sql_up_passage="UPDATE diplomes_caces_cat SET 
        dcc_action=:dcc_action,
        dcc_action_client=:dcc_action_client,
        dcc_action_soc=:dcc_action_soc
        WHERE dcc_diplome=:dcc_diplome AND dcc_passage=:passage;";
        $req_up_passage=$Conn->prepare($sql_up_passage);

        $sql_get_diplomes="SELECT dca_id
        ,dca_action_1,dca_action_client_1,dca_action_soc_1
        ,dca_action_2,dca_action_client_2,dca_action_soc_2
        ,dca_action_3,dca_action_client_3,dca_action_soc_3
        ,dca_action_4,dca_action_client_4,dca_action_soc_4
        ,dca_action_5,dca_action_client_5,dca_action_soc_5
        ,dca_action_6,dca_action_client_6,dca_action_soc_6
        FROM diplomes_caces ORDER BY dca_id;";
        $req_get_diplomes=$Conn->query($sql_get_diplomes);
        $d_diplomes=$req_get_diplomes->fetchAll();
        if (!empty($d_diplomes)) {

            foreach ($d_diplomes as $dip) {

                for ($bcl=1;$bcl<=6;$bcl++){

                    if (!empty($dip["dca_action_" . $bcl]) ) {

                        $req_up_passage->bindValue("dcc_action",$dip["dca_action_" . $bcl]);
                        $req_up_passage->bindValue("dcc_action_client",$dip["dca_action_client_" . $bcl]);
                        $req_up_passage->bindValue("dcc_action_soc",$dip["dca_action_soc_" . $bcl]);
                        $req_up_passage->bindValue("dcc_diplome",$dip["dca_id"]);
                        $req_up_passage->bindValue("passage",$bcl);
                        $req_up_passage->execute();

                    }
                }

            }


        }
		
	}
?>