<?php
include('../../includes/connexion.php');

try {
    $Conn->query("ALTER TABLE `conges_categories`
    ADD COLUMN `cca_user` TINYINT NULL DEFAULT '0' AFTER `cca_for_rapport`,
    ADD COLUMN `cca_resp` TINYINT NULL DEFAULT '0' AFTER `cca_user`,
    ADD COLUMN `cca_ra` TINYINT NULL DEFAULT '0' AFTER `cca_resp`;");
} catch (Exception $e) {
    echo($e->getMessage() . "<br/>");
}

try {
    $Conn->query("UPDATE `conges_categories` SET cca_validation = 1 WHERE cca_validation = 0;");
} catch (Exception $e) {
    echo($e->getMessage() . "<br/>");
}

