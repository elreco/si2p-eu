<?php
	include('../../includes/connexion.php');
	
	try{
		$Conn->query("ALTER TABLE `qualifications_categories` ADD `qca_nom` VARCHAR(255) NULL AFTER `qca_libelle`;");
	}Catch(Exception $e){
		echo("add qca_nom : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("ALTER TABLE `qualifications_categories` ADD `qca_archive` BOOLEAN NULL DEFAULT FALSE AFTER `qca_comp_opt_3`;");
	}Catch(Exception $e){
		echo("add qca_nom : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("UPDATE `qualifications` SET `qua_code` = 'R489' WHERE `qualifications`.`qua_id` = 7;");
	}Catch(Exception $e){
		echo("update qualif 7 : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("UPDATE `qualifications` SET `qua_code` = 'R482' WHERE `qualifications`.`qua_id` = 8;");
	}Catch(Exception $e){
		echo("update qualif 8 : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("UPDATE `qualifications` SET `qua_code` = 'R486', `qua_opt_1` = 'Porte-engins' WHERE `qualifications`.`qua_id` = 9;");
	}Catch(Exception $e){
		echo("update qualif 9 : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("UPDATE `qualifications` SET `qua_libelle` = 'Grue de chargement', `qua_code` = 'R490', `qua_opt_2` = '' WHERE `qualifications`.`qua_id` = 10;");
	}Catch(Exception $e){
		echo("update qualif 10 : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("UPDATE `qualifications` SET `qua_opt_1` = '', `qua_opt_2` = '' WHERE `qualifications`.`qua_id` = 12;");
	}Catch(Exception $e){
		echo("update qualif 12 : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("UPDATE `qualifications_categories` SET qca_archive=1 WHERE qca_qualification IN (7,8,9,10,12,13,18);");
	}Catch(Exception $e){
		echo("archive categorie old : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("INSERT INTO `qualifications_categories` (`qca_id`, `qca_qualification`, `qca_libelle`, `qca_nom`, `qca_competence`, `qca_ut`, `qca_opt_1`, `qca_ut_opt_1`, `qca_comp_opt_1`, `qca_opt_2`, `qca_ut_opt_2`, `qca_comp_opt_2`, `qca_opt_3`, `qca_ut_opt_3`, `qca_comp_opt_3`, `qca_archive`) VALUES (NULL, '7', '1A', 'Transpalette Préparateur haut<=1.2m', '0', '0.5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), (NULL, '7', '1B', 'Gerbeur haut >1.2m', '0', '0.75', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), (NULL, '7', '2A', 'Porteur de capacité <= 2 tonnes', '0', '0.5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), (NULL, '7', '2B', 'Tracteur de capacité <= 25 tonnes', '0', '0.5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), (NULL, '7', '3', 'Frontal de capacité <= 6 ', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), (NULL, '7', '4', 'Frontal de capacité > 6 tonnes', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), (NULL, '7', '5', 'A mât rétractable', '0', '0.75', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), (NULL, '7', '6', 'Poste de cond. élevable haut. > 1.2m', '0', '0.75', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), (NULL, '7', '7', 'Hors production', '0','0.75', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');");
	}Catch(Exception $e){
		echo("ADD categorie qualif 7 : " . $e->getMessage() . "<br/>");
	}	
	
	try{
		$Conn->query("INSERT INTO `qualifications_categories` (`qca_id`, `qca_qualification`, `qca_libelle`, `qca_nom`, `qca_competence`, `qca_ut`, `qca_opt_1`, `qca_ut_opt_1`, `qca_comp_opt_1`, `qca_opt_2`, `qca_ut_opt_2`, `qca_comp_opt_2`, `qca_opt_3`, `qca_ut_opt_3`, `qca_comp_opt_3`, `qca_archive`) VALUES 
		(NULL, '8', 'A', 'Engins compacts', '0', '1.5', '1', '0.5', '0', '1', '0', '0', '0', '0', '0', '0'), 
		(NULL, '8', 'B1', 'Déplacement séquenciel Pelles ...', '0', '1', '1', '0.5', '0', '1', '0.5', '0', '0', '0', '0', '0'),
		(NULL, '8', 'B2', 'Déplacement séquenciel Foreuse...', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
		(NULL, '8', 'B3', 'Déplacement séquenciel Rail-route', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
		(NULL, '8', 'C1', 'Déplacement alternatif Chargeuses...', '0', '1', '1', '0.5', '0', '1', '0.5', '0', '0', '0', '0', '0'),
		(NULL, '8', 'C2', 'Déplacement alternatif Bouteurs ...', '0', '1', '1', '0.5', '0', '1', '0.5', '0', '0', '0', '0', '0'),
		(NULL, '8', 'C3', 'Déplacement alternatif Niveleuses...', '0', '1', '1', '0.5', '0', '1', '0.5', '0', '0', '0', '0', '0'),
		(NULL, '8', 'D', 'Engins de compactage', '0', '1', '1', '0.5', '0', '1', '0.5', '0', '0', '0', '0', '0'),
		(NULL, '8', 'E', 'Engins de transport', '0', '1', '1', '0.5', '0', '1', '0.5', '0', '0', '0', '0', '0'),
		(NULL, '8', 'F', 'Chariots de manutention', '0', '1', '1', '0.5', '0', '1', '0.5', '0', '0', '0', '0', '0'),
		(NULL, '8', 'G', 'Utilisation hors production', '0', '1.2', '1', '0.5', '0', '1', '0', '0', '0', '0', '0', '0');");
	}Catch(Exception $e){
		echo("ADD categorie qualif 8 : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("INSERT INTO `qualifications_categories` (`qca_id`, `qca_qualification`, `qca_libelle`, `qca_nom`, `qca_competence`, `qca_ut`, `qca_opt_1`, `qca_ut_opt_1`, `qca_comp_opt_1`, `qca_opt_2`, `qca_ut_opt_2`, `qca_comp_opt_2`, `qca_opt_3`, `qca_ut_opt_3`, `qca_comp_opt_3`, `qca_archive`) VALUES 
		(NULL, '9', 'A', 'PEMP à élévation verticale', '0', '1', '1', '0.5', '0', '0', '0', '0', '0', '0', '0', '0'),
		(NULL, '9', 'B', 'PEMP à élévation multidirection.', '0', '1', '1', '0.5', '0', '0', '0', '0', '0', '0', '0', '0'),
		(NULL, '9', 'C', 'Hors production', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0');");
	}Catch(Exception $e){
		echo("ADD categorie qualif 9 : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("INSERT INTO `qualifications_categories` (`qca_id`, `qca_qualification`, `qca_libelle`, `qca_nom`, `qca_competence`, `qca_ut`, `qca_opt_1`, `qca_ut_opt_1`, `qca_comp_opt_1`, `qca_opt_2`, `qca_ut_opt_2`, `qca_comp_opt_2`, `qca_opt_3`, `qca_ut_opt_3`, `qca_comp_opt_3`, `qca_archive`) VALUES 
		(NULL, '10', 'R490', 'Grue de chargement', '0', '1', '1', '0.5', '0', '0', '0', '0', '0', '0', '0', '0');");
	}Catch(Exception $e){
		echo("ADD categorie qualif 10 : " . $e->getMessage() . "<br/>");
	}
	
	
	try{
		$Conn->query("INSERT INTO `qualifications_categories` (`qca_id`, `qca_qualification`, `qca_libelle`, `qca_nom`, `qca_competence`, `qca_ut`, `qca_opt_1`, `qca_ut_opt_1`, `qca_comp_opt_1`, `qca_opt_2`, `qca_ut_opt_2`, `qca_comp_opt_2`, `qca_opt_3`, `qca_ut_opt_3`, `qca_comp_opt_3`, `qca_archive`) VALUES 
		(NULL, '12', '1', 'Gerbeur h>1.20 m h<=2.5 m', '0', '0.75', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
		(NULL, '12', '2', 'Gerbeur haut >2.5 m', '0', '0.75', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');");
	}Catch(Exception $e){
		echo("ADD categorie qualif 12 : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("INSERT INTO `qualifications_categories` (`qca_id`, `qca_qualification`, `qca_libelle`, `qca_nom`, `qca_competence`, `qca_ut`, `qca_opt_1`, `qca_ut_opt_1`, `qca_comp_opt_1`, `qca_opt_2`, `qca_ut_opt_2`, `qca_comp_opt_2`, `qca_opt_3`, `qca_ut_opt_3`, `qca_comp_opt_3`, `qca_archive`) VALUES 
		(NULL, '13', '1', 'Pont roulant Portique Cde au sol', '0', '0.75', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
		(NULL, '13', '2', 'Pont roulant Portique Cde en cabine', '0', '0.75', '1', '0.5', '0', '0', '0', '0', '0', '0', '0', '0');");
	}Catch(Exception $e){
		echo("ADD categorie qualif 13 : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("INSERT INTO `qualifications_categories` (`qca_id`, `qca_qualification`, `qca_libelle`, `qca_nom`, `qca_competence`, `qca_ut`, `qca_opt_1`, `qca_ut_opt_1`, `qca_comp_opt_1`, `qca_opt_2`, `qca_ut_opt_2`, `qca_comp_opt_2`, `qca_opt_3`, `qca_ut_opt_3`, `qca_comp_opt_3`, `qca_archive`) VALUES 
		(NULL, '18', 'A', 'Grues mobiles à flèche treillis', '0', '1.5', '1', '0.5', '0', '1', '0.5', '0', '0', '0', '0', '0'),
		(NULL, '18', 'B', 'Grues mobiles à flèche télescopique', '0', '1.5', '1', '0.5', '0', '1', '0', '0', '0', '0', '0', '0');");
	}Catch(Exception $e){
		echo("ADD categorie qualif 18 : " . $e->getMessage() . "<br/>");
	}
	
	try{
		$Conn->query("UPDATE `societes` SET `soc_caces` = '1', `soc_inrs` = 'ICSCAC228' WHERE `societes`.`soc_id` = 17;");
	}Catch(Exception $e){
		echo("UPDATE SOC CACES : " . $e->getMessage() . "<br/>");
	}
	try{
		$Conn->query("UPDATE `societes` SET `soc_caces` = '1', `soc_inrs` = 'ICSCAC228' WHERE `societes`.`soc_id` = 19;");
	}Catch(Exception $e){
		echo("UPDATE SOC CACES : " . $e->getMessage() . "<br/>");
	}
	
	
	
