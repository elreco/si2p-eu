<?php

include('../../includes/connexion.php');
include('../../includes/connexion_fct.php');

// TRAITEMENT SUR BASE 0

$tab_req=array(
	"0" => array(
		"req" => "ALTER TABLE `fournisseurs_documents` DROP `fdo_optionnel`;",
		"table" => "fournisseurs_documents",
		"nom" => "DEL fdo_optionnel"
	),
	"1" => array(
		"req" => "ALTER TABLE `fournisseurs_documents` CHANGE `fdo_type` `fdo_type` INT(11) NULL DEFAULT '0' COMMENT '1 doc fournisseur / 2 doc intervenant';",
		"table" => "fournisseurs_documents",
		"nom" => "ADD com fdo_type"
	),
	"2" => array(
		"req" => "ALTER TABLE `fournisseurs_documents` ADD `fdo_entreprise` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'si type 1 demander au fournisseur societe' AFTER `fdo_type`, ADD `fdo_indiv` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'si type 1 demander au fournisseur autoentrepreneur' AFTER `fdo_entreprise`;",
		"table" => "fournisseurs_documents",
		"nom" => "ADD com fdo_type"
	),
	"3" => array(
		"req" => "RENAME TABLE `orion`.`fournisseurs_documents` TO `orion`.`fournisseurs_documents_param`;",
		"table" => "fournisseurs_documents",
		"nom" => "Rename fournisseurs_documents_param"
	),
	"4" => array(
		"req" => "ALTER TABLE `fournisseurs_documents_param` CHANGE `fdo_id` `fdp_id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `fdo_libelle` `fdp_libelle` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `fdo_type` `fdp_type` INT(11) NULL DEFAULT '0' COMMENT '1 doc fournisseur / 2 doc intervenant', CHANGE `fdo_entreprise` `fdp_entreprise` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'si type 1 demander au fournisseur societe', CHANGE `fdo_indiv` `fdp_indiv` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'si type 1 demander au fournisseur autoentrepreneur', CHANGE `fdo_obligatoire` `fdp_obligatoire` TINYINT(1) NULL DEFAULT '0', CHANGE `fdo_periodicite_mois` `fdp_periodicite_mois` INT(11) NULL DEFAULT '0', CHANGE `fdo_une_seule_fois` `fdp_une_seule_fois` TINYINT(1) NULL DEFAULT '0';",
		"table" => "fournisseurs_documents_param",
		"nom" => "Rename champ"
	),
	"5" => array(
		"req" => "CREATE TABLE `orion`.`Fournisseurs_Documents` ( `fdo_fournisseur` INT NOT NULL , `fdo_document` INT NOT NULL , `fdo_societe` INT NOT NULL , `fdo_date` DATE NOT NULL , `fdo_date_expire` DATE NOT NULL , `fdo_expire` BOOLEAN NOT NULL , `fdo_valide` BOOLEAN NOT NULL , `fdo_valide_uti` INT NOT NULL , `fdo_valide_date` DATE NOT NULL ) ENGINE = MyISAM;",
		"table" => "Fournisseurs_Documents",
		"nom" => "Create Fournisseurs_Documents"
	)

	,
	"6" => array(
		"req" => "ALTER TABLE `produits_sous_familles` ADD `psf_marge_montant_1` FLOAT NOT NULL AFTER `psf_alerte_jour`, ADD `psf_marge_taux_1` FLOAT NOT NULL AFTER `psf_marge_montant_1`, ADD `psf_marge_montant_2` FLOAT NOT NULL AFTER `psf_marge_taux_1`, ADD `psf_marge_taux_2` FLOAT NOT NULL AFTER `psf_marge_montant_2`, ADD `psf_marge_montant_3` FLOAT NOT NULL AFTER `psf_marge_taux_2`, ADD `psf_marge_taux_3` FLOAT NOT NULL AFTER `psf_marge_montant_3`;",
		"table" => "produits_sous_familles",
		"nom" => "Add pallier marge"
	),
	"7" => array(
		"req" => "ALTER TABLE `fournisseurs` CHANGE `fou_admin_etat` `fou_admin_etat` INT(11) NULL DEFAULT '0' COMMENT '1 ok 2 warning 3 danger';",
		"table" => "fournisseurs",
		"nom" => "fou_admin_etat Add commentaire"
	),
	"8" => array(
		"req" => "	ALTER TABLE `commandes_grilles` ADD `cgr_droit_1` INT NULL AFTER `cgr_utilisateur_4`, ADD `cgr_droit_2` INT NULL AFTER `cgr_droit_1`, ADD `cgr_droit_3` INT NULL AFTER `cgr_droit_2`, ADD `cgr_droit_4` INT NULL AFTER `cgr_droit_3`;",
		"table" => "commandes_grilles",
		"nom" => "Add  cgr_droit"
	),
	"9" => array(
		"req" => "ALTER TABLE `commandes_validations` ADD `cva_droit` INT NULL AFTER `cva_utilisateur`;",
		"table" => "commandes_validations",
		"nom" => "Add  cva_droit" 
	)
);

foreach($tab_req as $req){
	try{
		$req=$Conn->query($req["req"]);
	}Catch(Exception $e){
		echo($req["table"] . "-" . $req["nom"] . " : " . $e->getMessage() . "<br/>");
	}
}

/******************************************
 * REQUETE EN BASE N
*******************************************/

$tab_req_n=array(
	"0" => array(
		"req" => "ALTER TABLE `actions` ADD `act_verrou_admin` BOOLEAN NOT NULL DEFAULT FALSE AFTER `act_intra_multi`;",
		"table" => "actions",
		"nom" => "ADD act_verrou_admin"
	),
	"1" => array(
		"req" => "ALTER TABLE `plannings_dates` ADD `pda_alert` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Applique fond rouge sur le planning' AFTER `pda_sta_conf`;",
		"table" => "plannings_dates",
		"nom" => "ADD pda_alert"
	),
	"2" => array(
		"req" => "ALTER TABLE `actions` DROP ` acl_annule `;",
		"table" => "actions",
		"nom" => "DELETE acl_annule"
	),
	"3" => array(
		"req" => "ALTER TABLE `actions` ADD `act_verrou_marge` BOOLEAN NULL AFTER `act_verrou_admin`;",
		"table" => "actions",
		"nom" => "Add champ blocage marge"
	),
	"4" => array(
		"req" => "ALTER TABLE `actions` ADD `act_verrou_bc` BOOLEAN NOT NULL DEFAULT FALSE AFTER `act_verrou_admin`;",
		"table" => "actions",
		"nom" => "Add champ blocage marge"
	),
	"5" => array(
		"req" => "ALTER TABLE `actions` ADD `act_marge_ca` FLOAT NULL AFTER `act_verrou_bc`;",
		"table" => "actions",
		"nom" => "Add champ act_marge_ca"
	),
	
	
	
);

$sql_soc="SELECT soc_id FROM Societes ORDER BY soc_id;";
$req_soc=$Conn->query($sql_soc);
$d_societe=$req_soc->fetchAll();
if(!empty($d_societe)){
	
	foreach($d_societe as $soc){
		
		// TRAITEMENT SUR BASE N
		
		echo("SOCIETE : " . $soc["soc_id"] . "<br/>");
		
		$ConnFct=connexion_fct($soc["soc_id"]);

		foreach($tab_req_n as $req_n){
			try{
				$req=$ConnFct->query($req_n["req"]);
			}Catch(Exception $e){
				echo($req_n["table"] . "-" . $req_n["nom"] . " : " . $e->getMessage() . "<br/>");
			}
		}


		// on actualise int_ref_3

		$sql_up_int="UPDATE Intervenants SET int_ref_3=:fournisseur WHERE int_id=:intervenant;";
		$req_up_int=$ConnFct->prepare($sql_up_int);

		$sql="SELECT int_id,int_ref_1,int_ref_2 FROM Intervenants WHERE int_type=2 AND int_ref_1>0;";
		$req=$ConnFct->query($sql);
		$d_intervenants_interco=$req->fetchAll();
		if(!empty($d_intervenants_interco)){

			// pour chaque intervenant INTERCO
			foreach($d_intervenants_interco as $int_interco){

				$sql_fou="SELECT fou_id FROM Fournisseurs WHERE fou_interco_soc=" . $int_interco["int_ref_1"] . " AND NOT fou_archive";
				if(!empty($int_interco["int_ref_2"])){
					// si toutes les agences ont le même siret (SE) -> on peut utiliser un fournisseur unique
					$sql_fou.=" AND (fou_interco_age=" . $int_interco["int_ref_2"] . " OR fou_interco_age=0)";
				}else{
					$sql_fou.=" AND fou_interco_age=0";
				}
				$sql_fou.=";";
				$req_fou=$Conn->query($sql_fou);
				$d_fournisseur=$req_fou->fetchAll();
				if(!empty($d_fournisseur)){
					if(count($d_fournisseur)==1){

						$req_up_int->bindParam("fournisseur",$d_fournisseur[0]["fou_id"]);
						$req_up_int->bindParam("intervenant",$int_interco["int_id"]);
						$req_up_int->execute();

					}else{
						echo("plusieurs Fournisseur INTERCO " .  $int_interco["int_ref_1"] . "-" . $int_interco["int_ref_2"] . "<br/>");
					}
				}else{
					echo($int_interco["int_id"] . ": Fournisseur INTERCO " .  $int_interco["int_ref_1"] . "-" . $int_interco["int_ref_2"] . " introuvable<br/>");
				}


			}

		}
	}
}

echo("TERMINE");
?>