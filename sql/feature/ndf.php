<?php
include('../../includes/connexion.php');

try {
    $Conn->query("ALTER TABLE `ndf`
        CHANGE COLUMN `ndf_dr` `ndf_re` INT(11) NULL DEFAULT '0',
        ADD COLUMN `ndf_ra` INT(11) NULL DEFAULT '0',
        ADD COLUMN `ndf_ra_valide` DATE NULL DEFAULT NULL,
        ADD COLUMN `ndf_tranche_km` INT(11) NULL DEFAULT '0',
        ADD COLUMN `ndf_puissance` INT(11) NULL DEFAULT '0',
        ADD COLUMN `ndf_bareme` INT(11) NULL DEFAULT '0',
        CHANGE COLUMN `ndf_dr_valide` `ndf_re_valide` DATE NULL DEFAULT NULL;");
} catch (Exception $e) {
    echo("add ndf_dr : " . $e->getMessage() . "<br/>");
}

try {
    $Conn->query(" CREATE TABLE `ndf_invitations` (
                `nin_id` INT(11) NOT NULL AUTO_INCREMENT,
                `nin_qte` INT(11) NULL DEFAULT '0',
                `nin_ndf` INT(11) NULL DEFAULT '0',
                `nin_autre_client` VARCHAR(255) NULL DEFAULT NULL,
                `nin_ligne` INT(11) NULL DEFAULT '0',
                `nin_utilisateur_invite` INT(11) NULL DEFAULT '0',
                `nin_client` INT(11) NULL DEFAULT '0',
                `nin_date` DATE NULL DEFAULT NULL,
                PRIMARY KEY (`nin_id`)
        )");
} catch (Exception $e) {
    echo("add ndf_dr : " . $e->getMessage() . "<br/>");
}

try {
    $Conn->query(" ALTER TABLE `baremes_kilometres`
                CHANGE COLUMN `bki_date_fin` `bki_date_fin` DATE NULL DEFAULT NULL AFTER `bki_date_deb`;");
} catch (Exception $e) {
    echo("add ndf_dr : " . $e->getMessage() . "<br/>");
}

try {
    $Conn->query("INSERT INTO `baremes_kilometres` (`bki_id`, `bki_date_deb`, `bki_date_fin`) VALUES (1, '2020-11-11', '2029-11-17');");
} catch (Exception $e) {
    echo("add ndf_dr : " . $e->getMessage() . "<br/>");
}
try {
    $Conn->query("ALTER TABLE `ndf_categories`
                CHANGE COLUMN `nca_tva` `nca_tva` FLOAT NOT NULL DEFAULT 0 AFTER `nca_libelle`;");
} catch (Exception $e) {
    echo("add ndf_dr : " . $e->getMessage() . "<br/>");
}

try {
    $Conn->query("ALTER TABLE `ndf_lignes`
                ADD COLUMN `nli_vehicule` INT NULL DEFAULT '0' AFTER `nli_region`,
                ADD COLUMN `nli_vehicule_categorie` INT NULL DEFAULT '0' AFTER `nli_vehicule`,
                ADD COLUMN `nli_montant_tva` FLOAT NULL DEFAULT '0' AFTER `nli_vehicule`,
                CHANGE COLUMN `nli_depassement_valide` `nli_depassement_valide` FLOAT NULL DEFAULT NULL,
                ADD COLUMN `nli_refus` TINYINT NULL DEFAULT '0' AFTER `nli_vehicule_categorie`,
                ADD COLUMN `nli_compte` INT NULL DEFAULT '0' AFTER `nli_refus`;");
} catch (Exception $e) {
    echo("add ndf_dr : " . $e->getMessage() . "<br/>");
}

try {
    $Conn->query("ALTER TABLE `ndf_categories`
                ADD COLUMN `nca_vehicule` FLOAT NULL DEFAULT '0' AFTER `nca_tva_type`;");
} catch (Exception $e) {
    echo("add ndf_dr : " . $e->getMessage() . "<br/>");
}

for ($i = 1; $i <= 15; $i++) {
    try {
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 5.5, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 29, 2, 15, 15, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 31, 2, 15, 15, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 32, 2, 15, 15, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 3, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 4, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 5, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 12, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 7, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 8, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 8.5, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 10, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 15, 1, 55, 65, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 11, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 38, 2, 70, 80, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 13, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 14, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 16, 1, 55, 65, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 15, 2, 55, 65, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 16, 2, 55, 65, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 5.5, 2, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 7, 2, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 17, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 18, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 19, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 20, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 35, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 34, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 33, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 33, 2, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 34, 2, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 35, 2, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 28, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 32, 1, 15, 15, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 31, 1, 15, 15, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 21, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 22, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 23, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 24, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 26, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 27, 1, NULL, NULL, 0);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 25, 1, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 29, 1, 10, 10, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 30, 1, 15, 15, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 36, 1, 15, 15, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 37, 1, 15, 15, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 38, 1, 70, 80, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 18, 2, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 21, 2, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 22, 2, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 24, 2, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 26, 2, NULL, NULL, 1);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 27, 2, NULL, NULL, 0);");
        $Conn->query(" INSERT INTO `ndf_categories_profils` (`ncp_profil`, `ncp_categorie`, `ncp_type`, `ncp_plafond_1`, `ncp_plafond_2`, `ncp_acces`) VALUES ( " . $i . ", 25, 2, NULL, NULL, 1);");
    } catch (\Throwable $th) {
    }


}

try {
    $Conn->query("INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (3, 'Alcool ou gaz pour formation', 5.5, 0, 0, 0, 2, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (4, 'Autres Frais non listé', 0, 0, 1, 0, 3, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (5, '	Autres matières premières de formation', 20, 0, 52, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (6, 'Avion', 20, 0, 45, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (7, 'Carburants hors cartes total', 20, 0, 51, 0, 1, 1);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (8, 'Fournitures Administratives', 20, 0, 49, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (9, 'Fournitures/Petit Equipement pour Véhicules', 20, 0, 50, 0, 1, 1);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (10, 'Fournitures/Petits Equipements', 20, 0, 50, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (11, 'Frais Agence (café, sucre, papier toilette...)', 20, 0, 45, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (12, 'Frais Entretien Vehicules', 20, 0, 46, 0, 1, 1);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (13, 'Frais postaux', 0, 0, 43, 0, 3, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (14, 'Frais Réparation Vehicules', 20, 0, 46, 0, 1, 1);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (15, 'Hôtel', 8.5, 0, 45, 0, 5, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (16, 'Hôtel- invitation interne', 8.5, 0, 45, 0, 5, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (17, 'Indemnité forfaitaire déjeuner', 20, 0, 45, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (18, 'Indemnités kilométriques', 0, 0, 45, 0, 3, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (19, 'Lavage combinaisons - cagoules (pressing)', 20, 0, 74, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (20, 'Lavage hors cartes Total', 20, 0, 46, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (21, 'Location de Matériel et Outillage', 20, 0, 47, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (22, 'Locations de Véhicules', 20, 0, 48, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (23, 'Outillage', 20, 0, 50, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (24, 'Parking hors carte total', 8.5, 0, 45, 0, 5, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (25, 'Péage hors cartes total', 20, 0, 45, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (26, 'Petit Déjeuner Hors Hotel', 5.5, 0, 45, 0, 2, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (27, 'Plastification plans', 20, 0, 63, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (28, 'Réparation matériel pédagogique (MAF, ...)', 20, 0, 74, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (29, 'Repas du midi', 5.5, 0, 45, 0, 2, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (30, 'Repas du midi - Invitation Clients/Fournisseurs', 8.5, 0, 44, 0, 5, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (31, 'Repas du midi - Invitation Interne', 8.5, 0, 45, 0, 5, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (32, 'Repas du soir', 5.5, 0, 45, 0, 2, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (33, 'Taxi', 5.5, 0, 45, 0, 2, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (34, 'Train', 0, 0, 45, 0, 3, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (35, 'Vetements de travail', 20, 0, 53, 0, 1, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (36, 'Repas du soir- Invitation Clients/Fournisseurs', 0, 0, 44, 0, 3, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (37, 'Repas du soir- Invitation Interne', 0, 0, 45, 0, 3, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (38, 'Soirée étape', 0, 0, 45, 0, 3, 0);
    INSERT INTO `ndf_categories` (`nca_id`, `nca_libelle`, `nca_tva`, `nca_plafond`, `nca_compte`, `nca_type`, `nca_tva_type`, `nca_vehicule`) VALUES (39, 'Soirée étape - invitation interne', 20, 0, 45, 0, 1, 0);
    ");
} catch (\Throwable $th) {
}
