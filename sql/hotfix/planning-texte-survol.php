

<?php
	include('../../includes/connexion.php');
	include('../../includes/connexion_fct.php');
	
	$sql_act="SELECT act_id,acl_produit,acl_pro_reference,act_adr_ville,act_pro_type,act_produit FROM Actions,Actions_Clients WHERE act_id=acl_action
	AND acl_pro_inter=0 AND NOT acl_archive AND NOT act_archive AND act_date_deb>=NOW() AND MONTH(act_date_deb)=5;";
	
	$sql_up_date="UPDATE Plannings_Dates SET pda_survol=:texte_survol WHERE pda_type=1 AND pda_ref_1=:action;";
	
	$sql_up_action="UPDATE Actions SET act_produit=:act_produit,act_pro_type=1 WHERE act_id=:action;";
	
	
	$sql="SELECT soc_id FROM Societes WHERE soc_id=3 ORDER BY soc_id;";
	$req=$Conn->query($sql);
	$d_societes=$req->fetchAll();
	if(!empty($d_societes)){
		foreach($d_societes as $soc){
			
			echo("Societe : " . $soc["soc_id"] . "<br/>");
			
			$ConnFct=connexion_fct($soc["soc_id"]);
			
			$req_up_date=$ConnFct->prepare($sql_up_date);
			$req_up_action=$ConnFct->prepare($sql_up_action);
			
			$req_act=$ConnFct->query($sql_act);
			$d_actions=$req_act->fetchAll();
			if(!empty($d_actions)){
				foreach($d_actions as $action){
					
					if($action["act_pro_type"]!=1 OR $action["acl_produit"]!=$action["act_produit"]){
						
						$req_up_action->bindParam(":action",$action["act_id"]);
						$req_up_action->bindParam(":act_produit",$action["acl_produit"]);
						$req_up_action->execute();
						
					}
					
					$texte_survol=$action["acl_pro_reference"] . " " . $action["act_adr_ville"];
					
					$req_up_date->bindParam(":action",$action["act_id"]);
					$req_up_date->bindParam(":texte_survol",$texte_survol);
					$req_up_date->execute();
					
				}
			}
			
			echo("<br/><br/>");
			
		}
	}

	echo("TERMINE");
?>
