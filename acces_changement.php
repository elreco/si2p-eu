﻿<?php 

session_start(); 

$erreur=1;
if(isset($_SESSION["acces"])){
	if(!empty($_SESSION["acces"]["acc_ref"]) AND !empty($_SESSION["acces"]["acc_ref_id"])){
		$erreur=0;	
	}
}
if($erreur==1){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => "Changement de mot de passe impossible" 
	);
	header("location : index.php");
	die();
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" >
	<title>Si2P - Formations incendie, sécurité et prévention</title>
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.min.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/login.css">
	<link href='https://fonts.googleapis.com/css?family=Audiowide' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="main">
		<div class="logo">
			<img src="assets/img/login/logo.png" title="Si2P" alt="Si2P" />
		</div>
		
		<div class="formulaire">
		
			<h1>Changement<br/>de mot de passe</h1>
			
			<div class="formulaire-body" >
	
				<form method="post" action="acces_change_enr.php" id="formul" >
					<label>Mot de passe actuel</label>
					<input type="password" name="passe_old" id="passe_old" class="password" required />
					<label>Nouveau mot de passe</label>
					<input type="password" name="passe" id="passe"  required class="password" />
					
					<div class="text-left" id="crit_passe" style="width:340px;margin:auto;display:none;" >
						<span class="text-danger" >Mot de passe non valide!</span>
						<br/>
						<b>Politique de mot de passe : </b><br/>
						8 caractères minimum dont :
						<ul>	
							<li>-une lettre minuscule</li>
							<li>-une lettre majuscule</li>
							<li>-un chiffre</li>
							<li>-un caractère spécial autorisé (? @ . ; : , ! _ -)</li>
						</ul>
					</div>
					<label>Confirmer mot de passe</label>
					<input type="password" name="passe_confirm" id="passe_confirm" class="password" required />
					
					<div class="text-left text-danger" id="copie_passe" style="width:340px;margin:auto;display:none;" >
						Les mots de passe ne sont pas identiques!
					</div>
					
					<label>
						Afficher mot de passe :
						<input type="checkbox" name="affiche" id="affiche" />
					</label>
					
					<input type="submit" value="Me connecter">
					<hr style="margin-top:20px;margin-bottom:20px;">					
				</form>
				
			</div>
		</div>
	</div>
	
	<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
	<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
	
	<script src="vendor/plugins/pnotify/pnotify.js"></script>
	
	<script src="assets/js/utility/utility.js"></script>
	<script src="assets/js/main.js"></script>
	<script src="assets/js/orion.js"></script>
	
	<script>
		jQuery(document).ready(function(){	
			$("#affiche").click(function(){	
				if($(this).is(":checked")){
					$(".password").prop("type","text");
				}else{
					$(".password").prop("type","password");
				}
			});
			$("#passe").blur(function(){
				valide=testPasse($(this).val());
				if(!valide){
					$("#crit_passe").show();
					$(this).val("");
				}else{
					$("#crit_passe").hide();
					if($("#passe_confirm").val()!=""){
						comparer_passe();
					}
				}
				
				
			});
			$("#passe_confirm").blur(function(){
				if($("#passe").val()!=""){
					comparer_passe();
				}
			});			
		});
		
		function comparer_passe(){
			if($("#passe_confirm").val()!=$("#passe").val()){
				$("#copie_passe").show();
				$("#passe_confirm").val("");
			}else{
				$("#copie_passe").hide();
			}
		}
	</script>	
</body>
</html>