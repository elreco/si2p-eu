<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
$req = $Conn->prepare("SELECT * FROM societes WHERE soc_archive = 0");
$req->execute();
$d_societes = $req->fetchAll();


if (!empty($_GET)) {
    $req = $Conn->prepare("SELECT * FROM evacuations_alertes WHERE eal_ref = 1 AND eal_ref_id = " . $_GET['eal_ref_id'] . " AND eal_societe = " . $_GET['eal_societe'] . " AND eal_agence = " . $_GET['eal_agence']);
    $req->execute();
    $evacuations_alerte = $req->fetch();

    $req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id = " . $evacuations_alerte['eal_ref_id']);
    $req->execute();
    $utilisateur = $req->fetch();

    $req = $Conn->prepare("SELECT * FROM societes WHERE soc_id = " . $evacuations_alerte['eal_societe']);
    $req->execute();
    $societe = $req->fetch();
    if(!empty($societe)){
        $req = $Conn->prepare("SELECT * FROM agences WHERE age_societe = " . $evacuations_alerte['eal_societe'] . " AND age_archive =0");
        $req->execute();
        $agences = $req->fetchAll();
    }

}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Chrono</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->

        <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css">
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
<form action="evac_gestion_alerte_mod_enr.php" method="POST" id="admin-form">
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">

            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-left">

                                        <div class="content-header">
                                          <?php if(isset($_GET['eal_ref_id'])){ ?>
                                            <h2>Modifier le validant <b class="text-primary"><?= $utilisateur['uti_prenom'] ?> <?= $utilisateur['uti_nom'] ?></b></h2>
                                          <?php }else{ ?>
                                            <h2>Ajouter un <b class="text-primary">validant</b></h2>
                                          <?php } ?>

                                        </div>

                                        <div class="col-md-10 col-md-offset-1">


                                                <?php if (!empty($_GET['eal_ref_id'])): ?>
                                                    <input type="hidden" name="eal_ref_id" value="<?=$_GET['eal_ref_id']?>"></input>
                                                    <input type="hidden" name="eal_societe" value="<?=$_GET['eal_societe']?>"></input>
                                                    <input type="hidden" name="eal_agence" value="<?=$_GET['eal_agence']?>"></input>
                                                <?php endif;?>
                                                    <div class="row mt10" >
                                                            <div class="col-md-6" >
                                                                <div class="select2-sm" >
                                                                    <select name="eal_societe" id="societe" class="select2 select2-societe" data-com_archive="0" required >
                                                                        <option value="0" >Sélectionnez une société</option>
                                                                        <?php       if(!empty($d_societes)){
                                                                                foreach($d_societes as $s){
                                                                                    if(!empty($societe) && $societe['soc_id'] == $s['soc_id']){
                                                                                        echo("<option selected value='" . $s["soc_id"] . "' >" . $s["soc_nom"] . "</option>");
                                                                                    }else{
                                                                                        echo("<option value='" . $s["soc_id"] . "' >" . $s["soc_nom"] . "</option>");
                                                                                    }

                                                                                }
                                                                            } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" >
                                                                <div class="select2-sm" >
                                                                    <select name="eal_agence" id="agence" class="select2 select2-societe-agence" data-com_archive="0" >
                                                                        <option value="0" >Sélectionnez une agence</option>
                                                                        <?php if(!empty($agences)){
                                                                            foreach($agences as $a){
                                                                            ?>
                                                                            <?php if(!empty($evacuations_alerte['eal_agence']) && $a['age_id'] == $evacuations_alerte['eal_agence']){ ?>
                                                                                <option value="<?=  $a['age_id'] ?>" selected><?= $a['age_nom'] ?></option>
                                                                            <?php }else{ ?>
                                                                                <option value="<?= $a['age_id'] ?>"><?= $a['age_nom'] ?></option>
                                                                            <?php }?>
                                                                        <?php }
                                                                    }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="row mt10" >
                                                            <div class="col-md-12" >
                                                                <div class="select2-sm" >
                                                                    <select name="eal_utilisateur" class="select2 select2-utilisateur" data-com_archive="0" >

                                                                        <?php if(!empty($utilisateur)){ ?>
                                                                            <option value="<?= $utilisateur['uti_id'] ?>" selected><?= $utilisateur['uti_prenom'] ?> <?= $utilisateur['uti_nom'] ?></option>
                                                                        <?php }else{?>
                                                                            <option value="0" >Sélectionnez un utilisateur</option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                    </div>

                                          <div class="col-md-4 mt10">
                                                <div class="section">
                                                    <div class="option-group field">
                                                        <label class="option option-dark">
                                                            <input type="checkbox" name="eal_alerte" value="adr_defaut"
                                                            <?php if(!empty($evacuations_alerte['eal_alerte'])){ ?>checked <?php }?>>
                                                            <span class="checkbox"></span> Alerte
                                                        </label>
                                                    </div>
                                                </div>
                                          </div>
                                          <div class="col-md-4 mt10">
                                                <div class="section">
                                                    <div class="option-group field">
                                                        <label class="option option-dark">
                                                            <input type="checkbox" name="eal_valide" value="adr_defaut"
                                                            <?php if(!empty($evacuations_alerte['eal_valide'])){ ?>checked <?php }?>>
                                                            <span class="checkbox"></span> Validation
                                                        </label>
                                                    </div>
                                                </div>
                                          </div>
                                      </div>

                                  </div>
                              </div>


                          </div>
                      </div>

                  </div>
              </div>

          </div>
      </section>
      <!-- End: Content -->
  </section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="evac_gestion_alerte.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-save'></i> Enregistrer
            </button>
        </div>
    </div>
</footer>
</form>
<?php
	include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>

</body>
</html>
