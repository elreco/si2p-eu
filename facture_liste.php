<?php

// AFFICHE LA LISTE DES FACTURES

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');

// DONNEE UTILE AU PROGRAMME

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}

$aff_detail_opca=false;
if($_SESSION['acces']['acc_service'][2]==1 OR $_SESSION['acces']['acc_service'][3]==1){
	$aff_detail_opca=true;
}

$_SESSION['retour'] = "facture_liste.php";
$_SESSION["retourFacture"]="facture_liste.php";
$_SESSION["retourClient"]="facture_liste.php";
$_SESSION["retour_action"]="facture_liste.php";

	if (isset($_POST['search'])){

		// le formulaire a été soumit
		// on actualise les criteres de recherche

		$fac_date_deb = convert_date_sql($_POST['fac_date_deb']);
		$fac_date_fin = convert_date_sql($_POST['fac_date_fin']);

		$fac_agence=0;
		if(!empty($_POST['fac_agence'])){
			$fac_agence=intval($_POST['fac_agence']);
		}
		$fac_commercial=0;
		if(!empty($_POST['fac_commercial'])){
			$fac_commercial=intval($_POST['fac_commercial']);
		}
		$fac_chrono=0;
		if(!empty($_POST['fac_chrono'])){
			$fac_chrono=intval($_POST['fac_chrono']);
		}
		$fac_cli_categorie=0;
		if(!empty($_POST['fac_cli_categorie'])){
			$fac_cli_categorie=intval($_POST['fac_cli_categorie']);
		}
		$fac_cli_s_categorie=0;
		if(!empty($_POST['fac_cli_s_categorie'])){
			$fac_cli_s_categorie=intval($_POST['fac_cli_s_categorie']);
		}

		$fac_client=0;
		if(!empty($_POST['fac_client'])){
			$fac_client=intval($_POST['fac_client']);
		}

		$fac_filiale=false;
		if(!empty($_POST['fac_filiale'])){
			$fac_filiale=true;
		}

		$fac_echeance_deb = convert_date_sql($_POST['fac_echeance_deb']);
		$fac_echeance_fin = convert_date_sql($_POST['fac_echeance_fin']);

		$opt_recherche=0;
		$option_aff=0;
		if(!empty($_POST["option_recherche"])){
			$opt_recherche=intval($_POST["option_recherche"]);
		}elseif(!empty($_POST["option_aff"])){
			$option_aff=intval($_POST["option_aff"]);
		}


		// mémorisation des critere

		$_SESSION['fac_tri'] = array(
			"fac_date_deb" => $fac_date_deb,
			"fac_date_fin" => $fac_date_fin,
			"fac_agence" => $fac_agence,
			"fac_commercial" => $fac_commercial,
			"fac_chrono" => $fac_chrono,
			"fac_cli_categorie" => $fac_cli_categorie,		// valeur actuelle du client pas celle enregistre sur la fac
			"fac_cli_s_categorie" => $fac_cli_s_categorie,
			"fac_client"        => $fac_client,
			"fac_filiale" => $fac_filiale,
			"fac_echeance_deb" => $fac_echeance_deb,
			"fac_echeance_fin" => $fac_echeance_fin,
			"opt_recherche" => $opt_recherche,
			"option_aff" => $option_aff
		);
	}

	// ON CONSTRUIT LA REQUETE

	$critere=array();

	if($_SESSION['fac_tri']['opt_recherche']!=1 AND $_SESSION['fac_tri']['opt_recherche']!=7){

		// AUTRE QUE FACTURE A FAIRE

		$mil="";
		if(!empty($_SESSION['fac_tri']['fac_chrono'])){
			$mil.=" AND fac_chrono =:fac_chrono";
			$critere["fac_chrono"]=$_SESSION['fac_tri']['fac_chrono'];

		}else{

			if(!empty($_SESSION['fac_tri']['fac_date_deb'])){
				$mil.=" AND fac_date>=:fac_date_deb";
				$critere["fac_date_deb"]=$_SESSION['fac_tri']['fac_date_deb'];
			};
			if(!empty($_SESSION['fac_tri']['fac_date_fin'])){
				$mil.=" AND fac_date<=:fac_date_fin";
				$critere["fac_date_fin"]=$_SESSION['fac_tri']['fac_date_fin'];
			};
			if(!empty($_SESSION['fac_tri']['fac_agence'])){
				$mil.=" AND fac_agence=:fac_agence";
				$critere["fac_agence"]=$_SESSION['fac_tri']['fac_agence'];
			};
			if(!empty($_SESSION['fac_tri']['fac_commercial'])){
				$mil.=" AND fac_commercial=:fac_commercial";
				$critere["fac_commercial"]=$_SESSION['fac_tri']['fac_commercial'];
			};

			// maj suite echange Marion. Si le client change de categorie GC -> REG. Le CA doit rester GC
			if(!empty($_SESSION['fac_tri']['fac_cli_categorie'])){
				$mil.=" AND fac_cli_categorie =:fac_cli_categorie";
				$critere["fac_cli_categorie"]=$_SESSION['fac_tri']['fac_cli_categorie'];
			};
			if(!empty($_SESSION['fac_tri']['fac_cli_s_categorie'])){
				$mil.=" AND fac_cli_sous_categorie =:fac_cli_s_categorie";
				$critere["fac_cli_s_categorie"]=$_SESSION['fac_tri']['fac_cli_s_categorie'];
			};
			if(!empty($_SESSION['fac_tri']['fac_client'])){
				if($_SESSION['fac_tri']['fac_filiale']){
					$mil.=" AND (cli_id = :fac_client OR cli_filiale_de=:fac_client)";
					$critere["fac_client"]=$_SESSION['fac_tri']['fac_client'];
				}else{
					$mil.=" AND fac_client = :fac_client";
					$critere["fac_client"]=$_SESSION['fac_tri']['fac_client'];
				};
			}
			if(!empty($_SESSION['fac_tri']['fac_echeance_deb'])){
				$mil.=" AND fac_date_reg_prev>=:fac_echeance_deb";
				$critere["fac_echeance_deb"]=$_SESSION['fac_tri']['fac_echeance_deb'];
			};
			if(!empty($_SESSION['fac_tri']['fac_echeance_fin'])){
				$mil.=" AND fac_date_reg_prev<=:fac_echeance_fin";
				$critere["fac_echeance_fin"]=$_SESSION['fac_tri']['fac_echeance_fin'];
			};
			if(!empty($_SESSION['fac_tri']['opt_recherche'])){
				if($_SESSION['fac_tri']['opt_recherche']==2){
					// factures dues
					$mil.=" AND ( (fac_regle<fac_total_ttc AND fac_nature=1) OR (fac_regle>fac_total_ttc AND fac_nature=2) )";
				}elseif($_SESSION['fac_tri']['opt_recherche']==3){
					// factures echues
					$mil.=" AND fac_nature=1 AND ( (fac_regle<fac_total_ttc AND fac_nature=1) OR (fac_regle>fac_total_ttc AND fac_nature=2) ) AND fac_date_reg_prev<=NOW()";

				}elseif($_SESSION['fac_tri']['opt_recherche']==4){
					// poste client dû
					$mil.=" AND fac_nature=1 AND cli_interco_soc=0 AND fac_regle<fac_total_ttc";

				}elseif($_SESSION['fac_tri']['opt_recherche']==5){
					// poste client echu
					$mil.=" AND fac_nature=1 AND cli_interco_soc=0 AND fac_regle<fac_total_ttc AND fac_date_reg_prev<=NOW()";

				}elseif($_SESSION['fac_tri']['opt_recherche']==6){
					// avoirs non lettrés
					$mil.=" AND fac_nature=2 AND fac_regle>fac_total_ttc";
				}
			};
		}

		// fin critère form

		// critere lie au droit d'accès

		if($acc_agence>0){
			$mil.=" AND fac_agence=" . $acc_agence;
		}
		if(!$_SESSION['acces']["acc_droits"][6]){
			$mil.=" AND ( (com_type=1 AND com_ref_1=" . $_SESSION['acces']['acc_ref_id'] . ") OR cli_utilisateur=" . $_SESSION['acces']['acc_ref_id'] . ")";
		}

		// champ a selectionne
		$sql="SELECT fac_id,fac_chrono,fac_relance_stop,fac_numero,fac_date,fac_total_ht,fac_total_ttc,fac_regle,fac_date_reg_prev,fac_client,fac_opca,fac_aff_remise
		,cli_code,cli_nom,cli_affacturable,fac_cli_nom
		,com_label_1,com_label_2";
		if($_SESSION['fac_tri']['option_aff']!=1){	
			$sql.=",DATE_FORMAT(max_reg_date,'%d/%m/%Y') AS max_reg_date_fr";
		}else{
			$sql.=",fli_montant_ht,fli_code_produit,fli_libelle,fli_formation";
		}
		$sql.=" FROM Factures
		LEFT OUTER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)
		LEFT OUTER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";

		//FG 17/04/2020
		// affichage ligne est incompatible avec les réglements
		if($_SESSION['fac_tri']['option_aff']==1){
			
			$sql.="LEFT OUTER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)";
		}else{
			$sql.="LEFT OUTER JOIN (SELECT MAX(reg_date) AS max_reg_date,reg_facture FROM Reglements GROUP BY reg_facture) AS Reglements ON (Factures.fac_id=Reglements.reg_facture)";
		};
		
		if($mil!=""){
			$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		}
		$sql.=" ORDER BY fac_date,fac_chrono,fac_numero;";
		$req = $ConnSoc->prepare($sql);

		if(!empty($critere)){
			foreach($critere as $c => $u){
				$req->bindValue($c,$u);
			}
		};
		$req->execute();
		$factures = $req->fetchAll();

	}elseif($_SESSION['fac_tri']['opt_recherche']==1){


		//********************************
		// 	FACTURE A FAIRE
		//********************************

		// ACTION DE LA REGION

		$sql="SELECT act_id,act_adr_ville,act_date_deb";
		$sql.=",acl_id,acl_pro_inter,acl_creation_date,acl_ca,acl_pro_reference,acl_ca_unit,acl_confirme,acl_for_nb_sta_resa,acl_ca_nc";
		$sql.=",cli_code,cli_nom";
		$sql.=",com_label_1";
		$sql.=",COUNT(acd_date) AS nb_demi_j";
		$sql.=",int_label_1";
		$sql.=" FROM Actions LEFT OUTER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)
		LEFT OUTER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)
		LEFT OUTER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_client=Actions_Clients_Dates.acd_action_client)
		LEFT OUTER JOIN Intervenants ON (Actions.act_intervenant=Intervenants.int_id)
		LEFT OUTER JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)";

		$mil="";

		// facturation anticipé pour les codes annulations et les actions reporté de nov. et dec. 2020
		$mil=" AND acl_confirme AND NOT acl_archive AND NOT acl_facture AND NOT acl_non_fac 
		AND (
			act_date_deb<=NOW() 
			OR acl_produit IN (1429,1582,1331,1472,1480,1462,1363,1451,1454,1280,1281,1282,1351,1330,1467,1337,1475,1387,1301,1374,1285,1284,1342,1345,1379,1356,1315,1283,1425,1310,1384,1649,1650,1651,1652,1653,1654) 
			OR (acl_reporte AND (MONTH(act_date_deb)=11 OR MONTH(act_date_deb)=12) AND YEAR(act_date_deb)=2020) 
		) AND acl_facturation=:acl_facturation";


		$critere["acl_facturation"]=0;

		if(!empty($_SESSION['fac_tri']['fac_date_deb'])){
			$mil.=" AND act_date_deb>=:fac_date_deb";
			$critere["fac_date_deb"]=$_SESSION['fac_tri']['fac_date_deb'];
		};
		if(!empty($_SESSION['fac_tri']['fac_date_fin'])){
			$mil.=" AND act_date_deb<=:fac_date_fin";
			$critere["fac_date_fin"]=$_SESSION['fac_tri']['fac_date_fin'];
		};

		// gestion d'accès
		if($acc_agence>0){
			$mil.=" AND act_agence=:act_agence";
			$critere["act_agence"]=$acc_agence;
		}
		if(!$_SESSION['acces']["acc_droits"][6]){
			$mil.=" AND com_type=1 AND com_ref_1=:com_ref_1";
			$critere["com_ref_1"]=$acc_utilisateur;
		}

		if(!empty($mil)){
			$sql.=" WHERE " . substr($mil, 5);
		}
		$sql.=" GROUP BY act_id,act_adr_ville,act_date_deb";
		$sql.=",acl_id,acl_pro_inter,acl_creation_date,acl_ca,acl_pro_reference";
		$sql.=",cli_code,cli_nom";
		$sql.=",com_label_1";
		$sql.=",int_label_1";
		$sql.=" ORDER BY act_date_deb,act_intervenant,act_id,acl_id";
		$req = $ConnSoc->prepare($sql);
		if(!empty($critere)){
			foreach($critere as $c => $u){
				$req->bindValue($c,$u);
			}
		};
		$req->execute();
		$d_actions = $req->fetchAll();


		if($acc_societe!=4){

			// AUTRE QUE GFC -> ACTION A FACTURER A GFC

			$req->bindValue("acl_facturation",1);
			$req->execute();
			$d_actions_gfc = $req->fetchAll();

		}elseif($_SESSION["acces"]["acc_droits"][8] AND $acc_agence==4){

			// SUR GFC -> LES ACTIONS A REFACTURER AUX CLIENTS

			$critere=array();
			$d_refac_clients=array();

			$sql="SELECT act_id,act_adr_ville,act_date_deb";
			$sql.=",acl_id,acl_pro_inter,acl_creation_date,acl_ca,acl_pro_reference,acl_ca_gfc,acl_confirme,acl_for_nb_sta_resa,acl_ca_nc,acl_facture";
			$sql.=",cli_code,cli_nom";
			$sql.=",com_label_1";
			$sql.=",COUNT(acd_date) AS nb_demi_j";
			$sql.=",int_label_1";
			$sql.=" FROM Actions LEFT OUTER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)
			LEFT OUTER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)
			LEFT OUTER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_client=Actions_Clients_Dates.acd_action_client)
			LEFT OUTER JOIN Intervenants ON (Actions.act_intervenant=Intervenants.int_id)
			LEFT OUTER JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)";

			$mil=" AND acl_confirme AND NOT acl_archive AND NOT acl_facture_gfc AND NOT acl_non_fac_gfc"; 
			/*FG temporaire pour que josiane puisse avoir toutes les actions a facturer. 
			Pour pointage uniquement seule les annulées et les reportées doivent pouvoir etre facturées en anticipé
			annulé le 28/10 par FG*/
			$mil.=" AND (
			act_date_deb<=NOW() 
			OR acl_produit IN (1429,1582,1331,1472,1480,1462,1363,1451,1454,1280,1281,1282,1351,1330,1467,1337,1475,1387,1301,1374,1285,1284,1342,1345,1379,1356,1315,1283,1425,1310,1384,1649,1650,1651,1652,1653,1654) 
			OR (acl_reporte AND (MONTH(act_date_deb)=11  OR MONTH(act_date_deb)=12) AND YEAR(act_date_deb)=2020) 
			)";
			$mil.=" AND acl_facturation=1";
			// FG 28/11/2019
			// avec ce système seul les refact de GFC vers client peuvent etre groupe
			// on verra si besoin d'autres chose
			$mil.=" AND cli_fac_groupe=0";

			if(!empty($_SESSION['fac_tri']['fac_date_deb'])){
				$mil.=" AND act_date_deb>=:fac_date_deb";
				$critere["fac_date_deb"]=$_SESSION['fac_tri']['fac_date_deb'];
			};
			if(!empty($_SESSION['fac_tri']['fac_date_fin'])){
				$mil.=" AND act_date_deb<=:fac_date_fin";
				$critere["fac_date_fin"]=$_SESSION['fac_tri']['fac_date_fin'];
			};
			if(!empty($mil)){
				$sql.=" WHERE " . substr($mil, 5);
			}
			$sql.=" GROUP BY act_id,act_adr_ville,act_date_deb";
			$sql.=",acl_id,acl_pro_inter,acl_creation_date,acl_ca,acl_pro_reference";
			$sql.=",cli_code,cli_nom";
			$sql.=",com_label_1";
			$sql.=",int_label_1";
			$sql.=" ORDER BY act_date_deb,act_intervenant,act_id,acl_id";

			$sql_soc="SELECT soc_id,soc_nom FROM Societes WHERE NOT soc_id=4 AND NOT soc_archive ORDER BY soc_nom;";
			$req_soc = $Conn->query($sql_soc);
			$d_societes=$req_soc->fetchAll();
			if(!empty($d_societes)){
				foreach($d_societes as $soc){

					$ConnFct=connexion_fct($soc["soc_id"]);

					$req = $ConnFct->prepare($sql);
					if(!empty($critere)){
						foreach($critere as $c => $u){
							$req->bindValue($c,$u);
						}
					};
					$req->execute();
					$d_refac_clients[] = array(
						"soc_id" => $soc["soc_id"],
						"soc_nom" => $soc["soc_nom"],
						"actions" => $req->fetchAll()
					);

				}
			}
		}
	}else{

		// securité
		// la facturation groupée est réservé à CG
		if($acc_agence!=4 OR !$_SESSION["acces"]["acc_droits"][8]){
			echo("Accès réfusé!");
			die();
		}
		// FACTURATION GROUPE A FAIRE

		// impossible de faire INNER JOIN Clients AS Groupee ON (Clients.cli_fac_groupe=Groupee.cli_id) car le société de l'action n'a pas forcément accès au factureur

		$critere=array();
		$d_fac_groupee=array();

		$sql="SELECT cli_fac_groupe,COUNT(acl_id) AS nb_action,acl_id";
		$sql.=" FROM Actions
		INNER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)
		INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND Actions.act_agence=Clients.cli_agence)";
		$mil=" AND acl_confirme AND NOT acl_archive AND NOT act_archive AND NOT acl_facture_gfc AND NOT acl_non_fac_gfc";		
		$mil.=" AND(
			( act_date_deb<=NOW()";

			// FG : Il ne faut pas tenir compte de de la période selectionnée sur facture_tri puisque facture.php inclut toutes les actions <=NOW
			// si critère actif les actions hors période <= now ne seront pas comptabilisées.
			// DONC delta d'action entre liste fac groupée et facturation 
			
			/*if(!empty($_SESSION['fac_tri']['fac_date_deb'])){
				$mil.=" AND act_date_deb>=:fac_date_deb";
				$critere["fac_date_deb"]=$_SESSION['fac_tri']['fac_date_deb'];
			};
			if(!empty($_SESSION['fac_tri']['fac_date_fin'])){
				$mil.=" AND act_date_deb<=:fac_date_fin";
				$critere["fac_date_fin"]=$_SESSION['fac_tri']['fac_date_fin'];
			};*/
			$mil.=") 
			OR acl_produit IN (1429,1582,1331,1472,1480,1462,1363,1451,1454,1280,1281,1282,1351,1330,1467,1337,1475,1387,1301,1374,1285,1284,1342,1345,1379,1356,1315,1283,1425,1310,1384,1649,1650,1651,1652,1653,1654) 
			OR (acl_reporte AND (MONTH(act_date_deb)=11 OR MONTH(act_date_deb)=12) AND YEAR(act_date_deb)=2020 ) 
		)";
		$mil.=" AND acl_facturation=1 AND NOT acl_ca_nc";
		$mil.=" AND Clients.cli_fac_groupe>0";

		// fg 28/10 sans cette condition les actions reporté et déjà facturé dont le CA a été mis à 0 pour ne pas fausser le prévisionnel
		// ressortent dans la facturation groupée alors qu'elles sont exclus sur l'écran de facturation
		$mil.=" AND (acl_ca_gfc-acl_facture_gfc_ht)>0";
		

		// test 
		//$mil.=" AND (Clients.cli_filiale_de=10499 OR Clients.cli_id=10499)";

		
		if(!empty($mil)){
			$sql.=" WHERE " . substr($mil, 5);
		}
		$sql.=" GROUP BY cli_fac_groupe,acl_id";
		$sql.=" ORDER BY cli_fac_groupe";

		$sql_soc="SELECT soc_id,soc_nom FROM Societes WHERE NOT soc_archive ORDER BY soc_nom;";
		$req_soc = $Conn->query($sql_soc);
		$d_societes=$req_soc->fetchAll();
		if(!empty($d_societes)){
			foreach($d_societes as $soc){

				$ConnFct=connexion_fct($soc["soc_id"]);

				$req = $ConnFct->prepare($sql);
				if(!empty($critere)){
					foreach($critere as $c => $u){
						$req->bindValue($c,$u);
					}
				};
				$req->execute();
				$resultats=$req->fetchAll();
				if(!empty($resultats)){

					// test 
					/*echo("<pre>");
						print_r($resultats);
					echo("</pre>");*/

					foreach($resultats as $r){

						if(empty($d_fac_groupee[$r["cli_fac_groupe"]])){
							$d_fac_groupee[$r["cli_fac_groupe"]]=array(
								"cli_code" => "",
								"cli_nom" => "",
								"nb_action" => 0,
								"valide" => false
							);
						}
						$d_fac_groupee[$r["cli_fac_groupe"]]["nb_action"]=$d_fac_groupee[$r["cli_fac_groupe"]]["nb_action"] + $r["nb_action"];

					};

				}

			}
		}

		// test 
		//die();


		// On identifie les factureurs

		if(!empty($d_fac_groupee)){

			$sql_cli_fac="SELECT cli_code,cli_nom FROM Clients WHERE cli_id=:factureur;";
			$req_cli_fac = $Conn->prepare($sql_cli_fac);

			foreach($d_fac_groupee as $factureur => $val){

				$req_cli_fac->bindParam(":factureur",$factureur);
				$req_cli_fac->execute();
				$d_cli_fac=$req_cli_fac->fetch();
				if(!empty($d_cli_fac)){
					$d_fac_groupee[$factureur]["cli_code"]=$d_cli_fac["cli_code"];
					$d_fac_groupee[$factureur]["cli_nom"]=$d_cli_fac["cli_nom"];
					$d_fac_groupee[$factureur]["valide"]=true;
				}
			}
			asort($d_fac_groupee);
		}
	}
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">

		<?php			if($_SESSION['fac_tri']['opt_recherche']!=1 AND $_SESSION['fac_tri']['opt_recherche']!=7){ ?>

							<h1>Liste des factures</h1>

			<?php 			if(!empty($factures)){
								$total_ht=0;
								$total_ttc=0;
								$total_regle=0;
								$total_reste_du=0;

								$total_cols=5; ?>

								<div class="table-responsive">
									<table class="table table-striped table-hover" >
										<thead>
											<tr class="dark2" >
												<th>Chrono</th>
												<th>Client</th>
										<?php	if($aff_detail_opca){
													$total_cols++; ?>
													<th>Entité de facturation</th>
										<?php	} ?>
												<th>Commercial</th>
												<th>Facture</th>
												<th>Date</th>
												<th class="text-right" >H.T.</th>
										<?php	if($_SESSION['fac_tri']['option_aff']!=1){ ?>
													<th class="text-right" >T.T.C.</th>
													<th class="text-right" >Réglé</th>
													<th>Date Rglt</th>
													<th class="text-right" >Reste Dû</th>
										<?php	}else{ ?>
													<th>Produit</th>
													<th>Libéllé</th>
													<th>Désignation</th>
										<?php	} ?>
												<th class="text-center" >
													Date prévue de règlement
												</th>
											</tr>
										</thead>
										<tbody>
								<?php		$page=array();

											foreach($factures as $f){

												$page[]=$f["fac_id"];

												// style de la ligne

												$class_fac="";
												$style="";

												if($aff_detail_opca){
													if(!empty($f['fac_opca'])){
														$style="style='color:purple;'";
													}
												}
												if($f['fac_relance_stop'] == 1){
													$style="style='color:orange;font-weight:bold;'";
												}elseif($f['fac_relance_stop'] == 2){
													$style="style='color:green;font-weight:bold;'";
												}elseif($f['fac_relance_stop'] == 3){
													$style="style='color:red;font-weight:bold;'";
												}
												if($_SESSION['acces']['acc_service'][2]==1){

													if($f['fac_aff_remise']>0){
														$class_fac="class='system'";
													}elseif($f['cli_affacturable']!=1 OR $f['fac_opca']>0){
														$class_fac="class='warning'";
													}
												}

												$fac_date="";
												if(!empty($f['fac_date'])){
													$dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
													$fac_date=$dt_fac_date->format("d/m/Y");
												}
												$fac_date_reg_prev="";
												if(!empty($f['fac_date_reg_prev'])){
													$dt_fac_date_reg_prev=date_create_from_format("Y-m-d",$f['fac_date_reg_prev']);
													$fac_date_reg_prev=$dt_fac_date_reg_prev->format("d/m/Y");
												}
												if($_SESSION['fac_tri']['option_aff']!=1){		
													$reste_du=$f['fac_total_ttc']-$f['fac_regle'];

													$total_ht+=$f['fac_total_ht'];
													$total_ttc+=$f['fac_total_ttc'];
													$total_regle+=$f['fac_regle'];
													$total_reste_du+=$reste_du;
												}else{
													$total_ht+=$f['fli_montant_ht'];
												}
												?>
												<tr <?=$class_fac?> <?=$style?> >
													<td><?= $f['fac_chrono'] ?></td>
													<td>
												<?php	if($_SESSION['acces']['acc_service'][2]==1){ ?>
															<a href="client_voir.php?client=<?=$f['fac_client']?>" <?=$style?> >
																<?=$f['cli_code']?>
															</a>
												<?php	}else{
															echo($f['cli_code']);
														} ?>
													</td>
											<?php	if($aff_detail_opca){ ?>
														<td><?=$f['fac_cli_nom']?></td>
											<?php	} ?>
													<td><?= $f['com_label_1'] ?></td>
													<td>
														<a href="facture_voir.php?facture=<?=$f['fac_id']?>" <?=$style?> >
															<?= $f['fac_numero'] ?>
														</a>
													</td>
													<td><?=	$fac_date?></td>													
										<?php		if($_SESSION['fac_tri']['option_aff']!=1){ ?>
														<td class="text-right" ><?=number_format($f['fac_total_ht'], 2, ',', ' '); ?></td>
														<td class="text-right" >
											<?php			if($_SESSION["acces"]["acc_droits"][30]){ ?>
																<a href="reglement.php?facture=<?=$f['fac_id']?>" <?=$style?> >
																	<?=number_format($f['fac_total_ttc'], 2, ',', ' ');?>
																</a>
											<?php			}else{
																echo(number_format($f['fac_total_ttc'], 2, ',', ' '));
															} ?>
														</td>
														<td class="text-right" ><?= number_format($f['fac_regle'], 2, ',', ' ');?></td>
														<td><?=$f["max_reg_date_fr"]?></td>
														<td class="text-right" ><?=number_format($reste_du, 2, ',', ' ');?></td>														
										<?php		}else{ ?>
														<td class="text-right" ><?=number_format($f['fli_montant_ht'], 2, ',', ' '); ?></td>
														<td><?=$f['fli_code_produit']?></td>
														<td><?=$f['fli_libelle']?></td>
														<td><?=str_replace("<br/>",chr(10),$f['fli_formation'])?></td>
										<?php		} ?>
													<td class="text-center" >
													<?php if($_SESSION['acces']['acc_profil']==8 OR $_SESSION['acces']['acc_profil']==11  OR $_SESSION['acces']['acc_profil']==13){ ?>
													<a href="facture_relance_facture_mod.php?facture=<?= $f['fac_id'] ?>&societe=<?= $acc_societe ?>" <?=$style?> ><?=$fac_date_reg_prev?></a>
													<?php }else{?>
														<?=$fac_date_reg_prev?>
													<?php }?>
													</td>
												</tr>
					<?php					}
											$_SESSION['facture_tableau'] = $page; ?>
										</tbody>
										<tfoot>
											<tr>
												<th colspan="<?=$total_cols?>" class="text-right" >Total :</th>												
												<td class="text-right" ><?=number_format($total_ht, 2, ',', ' ');?></td>
										<?php	if($_SESSION['fac_tri']['option_aff']!=1){ ?>		
													<td class="text-right" ><?=number_format($total_ttc, 2, ',', ' ');?></td>
													<td class="text-right" ><?=number_format($total_regle, 2, ',', ' ');?></td>
													<td class="text-right" ><?=number_format($total_reste_du, 2, ',', ' ');?></td>												
													<td>&nbsp;</td>
										<?php	}else{ ?>
													<td colspan="3" >&nbsp;</td>
										<?php	} ?>
											</tr>
										</tfoot>
									</table>
								</div>
			<?php 			}else{ ?>
								<div class="col-md-12 text-center" style="padding:0;" >
									<div class="alert alert-warning" style="border-radius:0px;">
										Aucune facture correspondant à votre recherche.
									</div>
								</div>
			<?php			}
						}elseif($_SESSION['fac_tri']['opt_recherche']==1){

							// FACTURES A FAIRE

							// ACTIONS REGIONNALES

							echo("<h1>Liste des actions à facturer.</h1>");

							if(!empty($d_actions)){

								echo("<h2>" . count($d_actions) . " actions en facturation directe.</h2>"); ?>

								<div class="table-responsive" >
									<table class="table table-striped table-hover" id="tabAction">
										<thead id="tabRappelHead" >
											<tr class="dark" >
												<th>ID</th>
												<th>Code client</th>
												<th>Nom client</th>
												<th>Commercial</th>
												<th>Produits</th>
												<th>Lieu</th>
												<th>Intervenant</th>
												<th>Date de début</th>
												<th>Nb 1/2 j</th>
												<th class="text-center" >CA</th>
												<th>&nbsp;</th>
											</tr>
										</thead>
										<tbody id="tabRappelBody" >
						<?php				$total_ca=0;
											$total_ca_projet=0;

											foreach($d_actions as $a){
												$act_date_deb="";
												if(!empty($a["act_date_deb"])){
													$date_deb=new DateTime($a["act_date_deb"]);
													$act_date_deb=$date_deb->format("d/m/Y");
												}
												$acl_creation_date="";
												if(!empty($a["acl_creation_date"])){
													$creation_date=new DateTime($a["acl_creation_date"]);
													$acl_creation_date=$creation_date->format("d/m/Y");
												}

												$ca=0;
												$ca=$a["acl_ca"];
												$total_ca=$total_ca+$ca;

												?>
												<tr>
													<td><?=$a["act_id"] . "-" . $a["acl_id"]?></td>
											<?php	if(!empty($a["acl_id"])){ ?>
														<td><?=$a["cli_code"]?></td>
														<td><?=$a["cli_nom"]?></td>
														<td><?=$a["com_label_1"]?></td>
											<?php	}else{ ?>
														<td colspan="3" class="text-danger text-center" >Pas de client</td>
											<?php	} ?>
													<td><?=$a["acl_pro_reference"]?></td>
													<td><?=$a["act_adr_ville"]?></td>
													<td><?=$a["int_label_1"]?></td>
													<td><?=$act_date_deb?></td>
													<td><?=$a["nb_demi_j"]?></td>
											<?php	if($a["acl_ca_nc"]){ ?>
														<td>
															<div class="alert alert-danger p5 mn text-center" >Tarif non renseigné</div>
														</td>
											<?php	}else{ ?>
														<td class="text-right text-success" >
											<?php			if(!empty($ca)){
																echo(number_format($ca, 2, ',', ' ') . "&nbsp;€");
															} ?>
														</td>
											<?php	} ?>
													<td class="text-center" >
														<a href="action_voir.php?action=<?=$a["act_id"]?>&action_client=<?=$a["acl_id"]?>" class="btn btn-info btn-sm" >
															<i class="fa fa-eye" ></i>
														</a>
												<?php	if($_SESSION["acces"]["acc_droits"][29] AND !$a["acl_ca_nc"]){ ?>
															<a href="facture.php?action_client=<?=$a["acl_id"]?>" class="btn btn-info btn-sm" >
																<i class="fa fa-euro" ></i>
															</a>
												<?php	} ?>
													</td>
												</tr>
						<?php				} ?>
										</tbody>
										<tfoot>
											<tr>
												<th colspan="9" class="text-right" >Total :</th>
												<td class="text-right text-success" >
											<?php	if(!empty($total_ca)){
														echo("<b>" . number_format($total_ca, 2, ',', ' ') . "&nbsp;€</b>");
													} ?>
												</td>

												<td>&nbsp;</td>
											</tr>
										</tfoot>
									</table>
								</div>
					<?php	}else{ ?>
								<p class="alert alert-warning" >
									Aucune action en attente de facturation.
								</p>
					<?php	}

							// ACTIONS A FACTURER A GFC

							if(isset($d_actions_gfc)){

								if(!empty($d_actions_gfc)){

									echo("<h2>" . count($d_actions_gfc) . " actions en facturation GFC.</h2>"); ?>

									<div class="table-responsive" >
										<table class="table table-striped table-hover" id="tabAction">
											<thead id="tabRappelHead" >
												<tr class="dark" >
													<th>ID</th>
													<th>Code client</th>
													<th>Nom client</th>
													<th>Commercial</th>
													<th>Produits</th>
													<th>Lieu</th>
													<th>Intervenant</th>
													<th>Date de début</th>
													<th>Date de création</th>
													<th>Nb 1/2 j</th>
													<th>CA</th>
													<th>&nbsp;</th>
												</tr>
											</thead>
											<tbody id="tabRappelBody" >
							<?php				$total_ca=0;
												foreach($d_actions_gfc as $a){
													$act_date_deb="";
													if(!empty($a["act_date_deb"])){
														$date_deb=new DateTime($a["act_date_deb"]);
														$act_date_deb=$date_deb->format("d/m/Y");
													}
													$acl_creation_date="";
													if(!empty($a["acl_creation_date"])){
														$creation_date=new DateTime($a["acl_creation_date"]);
														$acl_creation_date=$creation_date->format("d/m/Y");
													}

													$ca=$a["acl_ca"];
													$total_ca=$total_ca+$ca;	?>
													<tr>
														<td><?=$a["act_id"] . "-" . $a["acl_id"]?></td>
												<?php	if(!empty($a["acl_id"])){ ?>
															<td><?=$a["cli_code"]?></td>
															<td><?=$a["cli_nom"]?></td>
															<td><?=$a["com_label_1"]?></td>
												<?php	}else{ ?>
															<td colspan="3" class="text-danger text-center" >Pas de client</td>
												<?php	} ?>
														<td><?=$a["acl_pro_reference"]?></td>
														<td><?=$a["act_adr_ville"]?></td>
														<td><?=$a["int_label_1"]?></td>
														<td><?=$act_date_deb?></td>
														<td><?=$acl_creation_date?></td>
														<td><?=$a["nb_demi_j"]?></td>
														<td class="text-right text-success" >
													<?php	if(!empty($ca)){
																echo(number_format($ca, 2, ',', ' ') . "&nbsp;€");
															} ?>
														</td>
														<td class="text-center" >
															<a href="action_voir.php?action=<?=$a["act_id"]?>&action_client=<?=$a["acl_id"]?>" class="btn btn-info btn-sm" >
																<i class="fa fa-eye" ></i>
															</a>
													<?php	if($_SESSION["acces"]["acc_droits"][29]){ ?>
																<a href="facture.php?action_client=<?=$a["acl_id"]?>" class="btn btn-info btn-sm" >
																	<i class="fa fa-euro" ></i>
																</a>
													<?php	} ?>
														</td>

													</tr>
							<?php				} ?>
											</tbody>
											<tfoot>
												<tr>
													<th colspan="10" class="text-right" >Total :</th>
													<td class="text-right text-success" >
												<?php	if(!empty($total_ca)){
															echo("<b>" . number_format($total_ca, 2, ',', ' ') . "&nbsp;€</b>");
														} ?>
													</td>
													<td>&nbsp;</td>
												</tr>
											</tfoot>
										</table>
									</div>
						<?php	}else{ ?>
									<h2>Facturation GFC.</h2>
									<p class="alert alert-warning" >
										Aucune action en attente de facturation à GFC.
									</p>
						<?php	}
							}elseif(isset($d_refac_clients)){

								// ACTIONS REGION A REFACTURER AUX CLIENTS

								if(!empty($d_refac_clients)){

									foreach($d_refac_clients as $data_soc){

										if(!empty($data_soc["actions"])){

											echo("<h2>" . count($data_soc["actions"]) . " actions " . $data_soc["soc_nom"] . " à refacturer aux clients.</h2>"); ?>

											<div class="table-responsive" >
												<table class="table table-striped table-hover" id="tabAction">
													<thead id="tabRappelHead" >
														<tr class="dark" >
															<th>ID</th>
															<th>Code client</th>
															<th>Nom client</th>
															<th>Commercial</th>
															<th>Produits</th>
															<th>Lieu</th>
															<th>Intervenant</th>
															<th>Date de début</th>
															<th>Nb 1/2 j</th>
															<th>CA</th>
															<th>Facturée à GFC</th>
															<th>&nbsp;</th>
														</tr>
													</thead>
													<tbody id="tabRappelBody" >
									<?php				$total_ca=0;
														foreach($data_soc["actions"] as $a){
															$act_date_deb="";
															if(!empty($a["act_date_deb"])){
																$date_deb=new DateTime($a["act_date_deb"]);
																$act_date_deb=$date_deb->format("d/m/Y");
															}
															$acl_creation_date="";
															if(!empty($a["acl_creation_date"])){
																$creation_date=new DateTime($a["acl_creation_date"]);
																$acl_creation_date=$creation_date->format("d/m/Y");
															}

															$ca=0;
															$ca=$a["acl_ca_gfc"];
															$total_ca=$total_ca+$ca; ?>
															<tr>
																<td><?=$a["act_id"] . "-" . $a["acl_id"]?></td>
														<?php	if(!empty($a["acl_id"])){ ?>
																	<td><?=$a["cli_code"]?></td>
																	<td><?=$a["cli_nom"]?></td>
																	<td><?=$a["com_label_1"]?></td>
														<?php	}else{ ?>
																	<td colspan="3" class="text-danger text-center" >Pas de client</td>
														<?php	} ?>
																<td><?=$a["acl_pro_reference"]?></td>
																<td><?=$a["act_adr_ville"]?></td>
																<td><?=$a["int_label_1"]?></td>
																<td><?=$act_date_deb?></td>
																<td><?=$a["nb_demi_j"]?></td>

														<?php	if($a["acl_ca_nc"]){ ?>
																	<td colspan="2" >
																		<div class="alert alert-danger p5 mn text-center" >Tarif non renseigné</div>
																	</td>
														<?php	}else{ ?>
																	<td class="text-right text-success" >
																<?php	if(!empty($ca)){
																			echo(number_format($ca, 2, ',', ' ') . "&nbsp;€");
																		} ?>
																	</td>
																	<td>
														<?php			if(!$a["acl_facture"]){
																			echo("<div class='alert alert-danger p5 mn text-center' >NON</div>");
																		} ?>
																	</td>
														<?php	} ?>
																<td class="text-center" >
																	<a href="action_voir.php?action=<?=$a["act_id"]?>&action_client=<?=$a["acl_id"]?>&societ=<?=$data_soc["soc_id"]?>" class="btn btn-info btn-sm" >
																		<i class="fa fa-eye" ></i>
																	</a>
															<?php	if($_SESSION["acces"]["acc_droits"][29] AND !$a["acl_ca_nc"]){ ?>
																		<a href="facture.php?action_client=<?=$a["acl_id"]?>&action_soc=<?=$data_soc["soc_id"]?>" class="btn btn-info btn-sm" >
																			<i class="fa fa-euro" ></i>
																		</a>
															<?php	} ?>
																</td>
															</tr>
									<?php				} ?>
													</tbody>
													<tfoot>
														<tr>
															<th colspan="9" class="text-right" >Total :</th>
															<td class="text-right text-success" >
														<?php	if(!empty($total_ca)){
																	echo("<b>" . number_format($total_ca, 2, ',', ' ') . "&nbsp;€</b>");
																} ?>
															</td>
															<td>&nbsp;</td>
														</tr>
													</tfoot>
												</table>
											</div>
						<?php			}
									}
								}else{ ?>
									<h2>Actions CN à refacturer aux clients.</h2>
									<p class="alert alert-warning" >
										Aucune action en attente de refacturation.
									</p>
						<?php	}
							}

						}else{

							// FACTURATION GROUPEE

							if(!empty($d_fac_groupee)){

								echo("<h2>Facturation groupée</h2>"); ?>

								<div class="table-responsive" >
									<table class="table table-striped table-hover" id="tabAction">
										<thead id="tabRappelHead" >
											<tr class="dark" >
												<th>Code client</th>
												<th>Nom client</th>
												<th>Nb. actions</th>
												<th>&nbsp;</th>
											</tr>
										</thead>
										<tbody id="tabRappelBody" >
						<?php				foreach($d_fac_groupee as $factureur => $fac_g){
												if($fac_g["valide"]){ ?>
													<tr>
														<td><?=$fac_g["cli_code"]?></td>
														<td><?=$fac_g["cli_nom"]?></td>
														<td><?=$fac_g["nb_action"]?></td>
														<td class="text-center" >
													<?php	if($_SESSION["acces"]["acc_droits"][29]){ ?>
																<a href="facture.php?client=<?=$factureur?>" class="btn btn-info btn-sm" >
																	<i class="fa fa-euro" ></i>
																</a>
													<?php	} ?>
														</td>
													</tr>
						<?php					}else{	 ?>
													<tr class="danger" >
														<td colspan="2" >Vous n'avez pas accès à la fiche N° <?=$factureur?>. Contacter votre SI.</td>
														<td><?=$fac_g["nb_action"]?></td>
														<td class="text-center" >&nbsp;</td>
													</tr>
						<?php					}
											} ?>
										</tbody>
									</table>
								</div>
					<?php	}else{ ?>
								<p class="alert alert-warning" >
									Aucune action en attente de facturation groupée.
								</p>
					<?php	}


						} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="facture_tri.php" class="btn btn-default btn-sm" role="button">
							<span class="fa fa-search"></span>
							<span class="hidden-xs">Nouvelle recherche</span>
						</a>
					</div>
					<div class="col-xs-6 footer-middle">
			<?php		if($_SESSION['acces']['acc_service'][2]==1 OR $aff_detail_opca){	?>
							<button type="button" class="btn btn-system btn-sm" data-toggle="modal" data-target="#modal_aide" >
								<i class="fa fa-info" ></i>
							</button>
			<?php		} ?>
					</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>


		<div id="modal_aide" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="mdl_doc_titre" >
							<i class="fa fa-info" ></i> - <span>Aide</span>
						</h4>
					</div>
					<div class="modal-body" >
						<table class="table" >
							<caption>Code couleur</caption>
							<tr class="warning" >
								<td>Client non affacturable</td>
							</tr>
							<tr class="system" >
								<td>Facture remise au factor</td>
							</tr>
							<tr style="color:purple;" >
								<td>Facturée à un OPCA</td>
							</tr>
							<tr style="color:red;" >
								<td>Client douteux</td>
							</tr>
							<tr style="color:orange;" >
								<td>Facture suspendue par l'agence</td>
							</tr>
							<tr style="color:green;" >
								<td>Facture suspendue par la compta</td>
							</tr>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
							<i class="fa fa-close" ></i>
							Fermer
						</button>
					</div>
				</div>
			</div>
		</div>

<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function (){


			});
		</script>
	</body>
</html>
