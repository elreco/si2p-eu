<?php

// AFFICHE LA LISTE DES FACTURES
include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');
include('modeles/mod_import_planning.php');

// DONNEE UTILE AU PROGRAMME
/* var_dump($_SESSION['fac_relance_tri']);
die(); */
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}

if(!empty($_GET['annee'])){
	$annee=$_GET['annee'];
}else{
    die("probleme");
}

if(!empty($_GET['semaine'])){
	$semaine=$_GET['semaine'];
}else{
    die("probleme");
}

if(!empty($_GET['intervenant'])){
	$intervenant=$_GET['intervenant'];
}else{
    die("probleme");
}
// ANNEE / INTERVENANT / ACTION

$date_get = getStartAndEndDate($semaine, $annee);
$period = new DatePeriod(
    new DateTime($date_get['week_start']),
    new DateInterval('P1D'),
    new DateTime($date_get['week_end'])
);
$i = 0;
foreach($period as $p){

	$import_formations[$i][0] = $p->format("Y-m-d");
	$import_formations[$i][1] = 0;
	$import_formations[$i][2] = 0;
	$import_formations[$i][3] = 0;
	$sql="SELECT * FROM formations WHERE for_date = '" . $import_formations[$i][0] . "' AND for_intervenant=" . $intervenant;
	$req = $ConnSoc->query($sql);
	$d_formations=$req->fetchAll();


	foreach($d_formations as $f){
		if($f['for_demi'] == 1){
			$import_formations[$i][1] = 1;
		}elseif($f['for_demi'] == 2){
			$import_formations[$i][2] = 1;
		}elseif($f['for_demi'] == 1.5){
			$import_formations[$i][3] = 1;
		}
	}
	$i++;
}

foreach($import_formations as $import){

	if($import[3] == 0){
		if($import[1] == 0 OR $import[2] == 0){
			// ALLER CHERCHER LES ACTIONS DE L'APREM POUR LES IMPORTER
			$planning_date=array();
			if($import[1] == 0){
				$planning_date=import_planning(1, $import[0], $intervenant, $acc_agence);
			}
			if($import[2] == 0){

			    import_planning(2, $import[0], $intervenant, $acc_agence, $planning_date);
			}
		}
	}



}
$_SESSION['message'][] = array(
	"titre" => "Succès",
	"type" => "success",
	"message" => "L'import a été effectué"
);
header("location : " . $_SESSION['retourRapportVoir']);
die();
