<?php
	include "includes/controle_acces.inc.php";
	include("includes/connexion.php");
	include("includes/connexion_soc.php");
	include("includes/connexion_fct.php");

	include("modeles/mod_get_tva_taux.php");


	$erreur_txt="";

	// PERSONNE CONNECTE
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}
	function periode_libelle($date_deb,$demi_deb,$date_fin,$demi_fin){

		$dtime = DateTime::createFromFormat("Y-m-d",$date_deb);
		$lib=$dtime->format("d/m/Y");

		if($date_deb==$date_fin){
			$periode="Le  " . $lib;
			if($demi_deb==$demi_fin){
				if($demi_fin==2){
					$periode.=" après-midi";
				}else{
					$periode.=" matin";
				}
			}
		}else{

			$periode="Du ";

			$periode.=$lib;
			if($demi_deb==2){
				$periode.=" après-midi";
			}
			$periode.=" au ";

			$dtime = DateTime::createFromFormat("Y-m-d",$date_fin);
			$lib=$dtime->format("d/m/Y");
			$periode.=$lib;
			if($demi_fin==1){
				$periode.=" matin";
			}
		}


		return  $periode;
	}

	$heracles_id=0;

	if(!empty($_GET)){

		// MODIF A PARTIR D'UN ACTION

		if(!empty($_GET["action"])){

			$com_action=intval($_GET["action"]);

			$com_fournisseur=0;
			if(!empty($_GET["fournisseur"])){
				$com_fournisseur=intval($_GET["fournisseur"]);
			}

			$commande_id=0;
			$com_ht=0;
			$com_ttc=0;

			if(empty($com_action) OR empty($com_fournisseur)){
				$erreur_txt="Err001 : Impossible de genéré le bon de commande";
			}

			// SUR LE FOURNISSEUR
			if(empty($erreur_txt)){
				// place ici car on doit identifier le fait que le fournisseur soit un INTERCO => du coup 1 req fou partie action et 1 req fou partie form
				$req = $Conn->prepare("SELECT fou_code,fou_nom,fou_type,fou_reg_type,fou_reg_nb_jour,fou_reg_formule,fou_reg_fdm,fou_interco_soc FROM fournisseurs WHERE fou_id = " . $com_fournisseur);
				$req->execute();
				$d_fournisseur = $req->fetch();
				if(empty($d_fournisseur)){
					$erreur_txt="Err 005 Impossible de genéré le bon de commande";
				}else{
					$com_fou_code=$d_fournisseur["fou_code"];
					$com_fou_nom=$d_fournisseur["fou_nom"];
					$com_reg_type=$d_fournisseur["fou_reg_type"];
					$com_reg_nb_jour=$d_fournisseur["fou_reg_nb_jour"];
					$com_reg_formule=$d_fournisseur["fou_reg_formule"];
					$com_reg_fdm=$d_fournisseur["fou_reg_fdm"];
				}
			}

			// SUR L'ACTION
			if(empty($erreur_txt)){

				$req = $ConnSoc->prepare("SELECT act_agence,act_adr_nom,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville,act_produit,act_pro_categorie,act_pro_famille,act_pro_sous_famille
				,act_pro_sous_sous_famille,act_adr_ref,act_adr_ref_id,act_adresse FROM Actions WHERE act_id = " . $com_action);
				$req->execute();
				$d_action = $req->fetch();
				if(empty($d_action)){
					$erreur_txt="Err002 : Impossible de genéré le bon de commande";
				}else{
					$com_agence=$d_action["act_agence"];
					if($d_action["act_adr_ref"]==1){
						$com_liv_type=3;
						$com_liv_ref_1=$d_action["act_adr_ref_id"];
						$com_liv_ref_2=$d_action["act_adresse"];

					}elseif($d_action["act_adr_ref"]==2){
						$com_liv_type=1;
						$com_liv_ref_1=0;
						$com_liv_ref_2=0;
					}else{
						$com_liv_type=0;
						$com_liv_ref_1=0;
						$com_liv_ref_2=0;
					}
					$com_liv_nom=$d_action["act_adr_nom"];
					$com_liv_adr1=$d_action["act_adr1"];
					$com_liv_adr2=$d_action["act_adr2"];
					$com_liv_adr3=$d_action["act_adr3"];
					$com_liv_cp=$d_action["act_adr_cp"];
					$com_liv_ville=$d_action["act_adr_ville"];

				}
			}

			// LIGNES DE COMMANDES
			// -> on genere une ligne par intervenant

			if(empty($erreur_txt)){

				// ON RECHERCHE SI DONNEE PRODUIT RENSEIGNE

				$nb_jour=0.5;

				if(!empty($d_action["act_produit"])){

					$req = $Conn->query("SELECT pro_code_produit,pro_libelle,pro_ht_intra,pro_nb_demi_jour,pro_tva FROM Produits WHERE pro_id=" . $d_action["act_produit"] . ";");
					$d_produit = $req->fetch();
					if(!empty($d_produit)){

						// au niveau de fournisseur gestion des tarifs à la journée.
						// recalcul des valeurs

						$nb_jour=$d_produit["pro_nb_demi_jour"]/2;

						if($d_fournisseur["fou_interco_soc"]>0){
							$d_fou_produit=array(
								"fpr_reference" => $d_produit["pro_code_produit"],
								"fpr_libelle" => $d_produit["pro_libelle"],
								"fpr_descriptif" => "",
								"fpr_tarif" => ($d_produit["pro_ht_intra"]/$nb_jour)*0.5,
								"fpr_type" => 1
							);
						}
					}

					if(empty($d_fournisseur["fou_interco_soc"])){
						$req = $Conn->query("SELECT fpr_reference,fpr_libelle,fpr_descriptif,fpr_tarif,fpr_type FROM Fournisseurs_Produits
						WHERE fpr_fournisseur=" . $com_fournisseur . " AND fpr_pro_id=" . $d_action["act_produit"] . ";");
						$d_fou_produit = $req->fetch();
					}
				}


				$lignes=array();

				// ON PARCOUR LES DATES LIE AU FOUNISSEURS
				$intervenant_id=0;
				$cle=-1;

				$sql="SELECT int_id,int_ref_2,pda_intervenant,pda_date,pda_demi FROM Plannings_Dates INNER JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)
				WHERE pda_type=1 AND pda_ref_1=" . $com_action;
				if($d_fournisseur["fou_interco_soc"]>0){
					$sql.=" AND int_type=2 AND int_ref_3=" . $com_fournisseur;
				}else{
					$sql.=" AND int_type=3 AND int_ref_1=" . $com_fournisseur;
				}
				$sql.=" ORDER BY pda_intervenant,pda_date,pda_demi;";
				$req = $ConnSoc->query($sql);
				$d_dates = $req->fetchAll();

				if(!empty($d_dates) && !empty($d_fou_produit["fpr_tarif"])){

					foreach($d_dates as $k => $d){

						if($d["int_id"]!=$intervenant_id){

							if(!empty($intervenant_id)){

								$lignes[$cle].=periode_libelle($date_deb,$demi_deb,$d_dates[$k-1]["pda_date"],$d_dates[$k-1]["pda_demi"]);
								$lignes[$cle]["livraison"]=$d_dates[$k-1]["pda_date"];

								if(!empty($nb_jour)){
									if($lignes[$cle]["qte"]<$nb_jour){
										$lignes[$cle]["qte"]=$nb_jour;
									}
								}
								$lignes[$cle]["ht"]=$lignes[$cle]["pu"]*$lignes[$cle]["qte"];

								$bc_tva=get_tva_taux($lignes[$cle]["livraison"],$lignes[$cle]["tva"]);
								$lignes[$cle]["tva_taux"]=$bc_tva[$lignes[$cle]["tva"]]["tpe_taux"];
								$lignes[$cle]["tva_periode"]=$bc_tva[$lignes[$cle]["tva"]]["tpe_id"];
								$lignes[$cle]["ttc"]=$lignes[$cle]["ht"]+(($lignes[$cle]["ht"]*$lignes[$cle]["tva_taux"])/100);

								$com_ht=$com_ht+$lignes[$cle]["ht"];
								$com_ttc=$com_ttc+$lignes[$cle]["ttc"];
							}

							$intervenant_id=$d["int_id"];
							$cle++;
							$lignes[$cle]=array(
								"id" => 0,
								"int_nom" => "",
								"int_id" => $intervenant_id,
								"pro_categorie" => $d_action["act_pro_categorie"],
								"pro_famille" => $d_action["act_pro_famille"],
								"pro_sous_famille" => $d_action["act_pro_sous_famille"],
								"pro_sous_sous_famille" => $d_action["act_pro_sous_sous_famille"],
								"pro_id" => $d_action["act_produit"],
								"famille" => 2,
								"reference" => $d_fou_produit["fpr_reference"],
								"libelle" => $d_fou_produit["fpr_libelle"],
								"descriptif" => $d_fou_produit["fpr_descriptif"],
								"pu" => $d_fou_produit["fpr_tarif"],
								"qte" => 0,
								"ht" => 0,
								"tva" => $d_produit["pro_tva"],
								"tva_taux" => 0,
								"tva_periode" => 0,
								"ttc" => 0,
								"livraison" => null,
								"dates" => ""
							);

							$date_deb=$d["pda_date"];
							$demi_deb=$d["pda_demi"];

						}elseif($d["pda_demi"]==2){

							if($d_dates[$k-1]["pda_date"]!=$d["pda_date"] OR $d_dates[$k-1]["pda_demi"]!=1){
								$periode=periode_libelle($date_deb ,$demi_deb,$d_dates[$k-1]["pda_date"],$d_dates[$k-1]["pda_demi"]);
								$lignes[$cle]["dates"].=$periode;

								$date_deb=$d["pda_date"];
								$demi_deb=$d["pda_demi"];
							}

						}else{
							// matin
							if($d_dates[$k-1]["pda_demi"]!=2){

								$periode=periode_libelle($date_deb ,$demi_deb,$d_dates[$k-1]["pda_date"],$d_dates[$k-1]["pda_demi"]);
								$lignes[$cle]["dates"].=$periode;

								$date_deb=$d["pda_date"];
								$demi_deb=$d["pda_demi"];
							}

						}

						$lignes[$cle]["qte"]+=0.5;

					}

					$periode=periode_libelle($date_deb,$demi_deb,$d_dates[$k]["pda_date"],$d_dates[$k]["pda_demi"]);
					$lignes[$cle]["dates"].=$periode;
					$lignes[$cle]["livraison"]=$d_dates[$k]["pda_date"];

					if(!empty($nb_jour)){
						if($lignes[$cle]["qte"]<$nb_jour){
							$lignes[$cle]["qte"]=$nb_jour;
						}
					}
					$lignes[$cle]["ht"]=$lignes[$cle]["pu"]*$lignes[$cle]["qte"];

					$bc_tva=get_tva_taux($lignes[$cle]["livraison"],$lignes[$cle]["tva"]);
					$lignes[$cle]["tva_taux"]=$bc_tva[$lignes[$cle]["tva"]]["tpe_taux"];
					$lignes[$cle]["tva_periode"]=$bc_tva[$lignes[$cle]["tva"]]["tpe_id"];
					$lignes[$cle]["ttc"]=$lignes[$cle]["ht"]+(($lignes[$cle]["ht"]*$lignes[$cle]["tva_taux"])/100);

					$com_ht=$com_ht+$lignes[$cle]["ht"];
					$com_ttc=$com_ttc+$lignes[$cle]["ttc"];
				}
			}

			/*echo("<pre>");
				print_r($lignes);
			echo("</pre>");
			die();*/
			// FIN CREATION A PARTIR D'UNE ACTION

		}else{
			$erreur_txt="Err003 Impossible de genéré le bon de commande";
		}

		// CONTROLE SYNCHRO HERACLES
		if(!empty($_GET["err_heracles"])){
			$erreur_txt="Echec de la synchronisation Héraclès!";
		}elseif(!empty($_GET["id_heracles"])){
			$heracles_id=intval($_GET["id_heracles"]);
		}


		if(empty($erreur_txt)){

			$com_societe=$acc_societe;
			$com_donneur_ordre=$acc_utilisateur;
			// contact par defaut
			$req = $Conn->query("SELECT * FROM fournisseurs_Contacts WHERE fco_fournisseur = " . $com_fournisseur . " AND fco_defaut=1");
			$d_contact = $req->fetch();
			if(!empty($d_contact)){
				$com_contact_nom=$d_contact["fco_nom"];
				$com_contact_prenom=$d_contact["fco_prenom"];
				$com_contact_tel=$d_contact["fco_tel"];
				$com_contact_fax=$d_contact["fco_fax"];
				$com_contact_portable=$d_contact["fco_portable"];
				$com_contact_mail=$d_contact["fco_mail"];
				if($d_contact["fco_fonction"] == "autre"){
					$com_contact_fonction=0;
				}else{
					$com_contact_fonction=$d_contact["fco_fonction"];
				}
				$com_contact_fonction_nom=$d_contact["fco_fonction_nom"];
				$com_contact_titre=$d_contact["fco_titre"];
			}else{
				$com_contact_nom="";
				$com_contact_prenom="";
				$com_contact_tel="";
				$com_contact_fax="";
				$com_contact_portable="";
				$com_contact_mail="";
				$com_contact_fonction=0;
				$com_contact_fonction_nom="";
				$com_contact_titre=0;
			}

		}

	}elseif(!empty($_POST)){


		// EDITION VIA LE FOURMULAIRE
		$commande_id=0;
		if(!empty($_POST["com_id"])){
			$commande_id=intval($_POST["com_id"]);
		}

		$com_fournisseur=0;
		if(!empty($_POST["com_fournisseur"])){
			$com_fournisseur=intval($_POST["com_fournisseur"]);
		}

		if(empty($com_fournisseur)){
			$erreur_txt="Err 004 Impossible de genéré le bon de commande";
		}


		if(empty($commande_id)){
			$heracles_id=0;
			if(!empty($_POST["id_heracles"])){
				$heracles_id=intval($_POST["id_heracles"]);
			}
			/*if(empty($heracles_id)){
				$erreur_txt=" A Erreur de synchronisation HERACLES";
			}*/
		}

		// CONTACT

		$com_contact=0;
		if(isset($_POST["com_contact"])){
			if(!empty($_POST["com_contact"])){
				$com_contact=intval($_POST["com_contact"]);
			}
		}else{
			if(!empty($_POST["com_contact_disabled"])){
				$com_contact=intval($_POST["com_contact_disabled"]);
			}
		}
		$com_contact_nom=$_POST["com_contact_nom"];
		$com_contact_prenom=$_POST["com_contact_prenom"];
		$com_contact_tel=$_POST["com_contact_tel"];
		$com_contact_fax=$_POST["com_contact_fax"];
		$com_contact_portable=$_POST["com_contact_portable"];
		$com_contact_mail=$_POST["com_contact_mail"];
		if($_POST["com_contact_fonction"] == "autre"){
			$com_contact_fonction=0;
		}else{
			$com_contact_fonction=$_POST["com_contact_fonction"];
		}

		$com_contact_fonction_nom=$_POST["com_contact_fonction_nom"];
		$com_contact_titre=0;
		if(!empty($_POST["com_contact_titre"])){
			$com_contact_titre=intval($_POST["com_contact_titre"]);
		}

		// LIVRAISON

		$com_liv_type=0;
		$com_liv_ref_1=0;
		$com_liv_ref_2=0;

		if(isset($_POST["com_liv_type"])){
			if(!empty($_POST["com_liv_type"])){
				$com_liv_type=intval($_POST["com_liv_type"]);
			}
			switch($com_liv_type){
				case 2:
					// autre agence
					if(!empty($_POST["com_societe_adresse"])){
						$com_liv_ref_1=intval($_POST["com_societe_adresse"]);
					}
					if(!empty($_POST["com_agence_adresse"])){
						$com_liv_ref_2=intval($_POST["com_agence_adresse"]);
					}
					break;
				case 3:
					// client
					if(!empty($_POST["com_client"])){
						$com_liv_ref_1=intval($_POST["com_client"]);
					}
					if(!empty($_POST["com_client_adresse"])){
						$com_liv_ref_2=intval($_POST["com_client_adresse"]);
					}
					$com_liv_nom=$_POST["com_liv_nom"];
					$com_liv_adr1=$_POST["com_liv_adr1"];
					$com_liv_adr2=$_POST["com_liv_adr2"];
					$com_liv_adr3=$_POST["com_liv_adr3"];
					$com_liv_cp=$_POST["com_liv_cp"];
					$com_liv_ville=$_POST["com_liv_ville"];
					break;
				case 5:
					// Fournisseur
					if(!empty($_POST["com_fournisseur_adresse"])){
						$com_liv_ref_1=intval($_POST["com_fournisseur_adresse"]);
					}
					$com_liv_nom=$_POST["com_liv_nom"];
					$com_liv_adr1=$_POST["com_liv_adr1"];
					$com_liv_adr2=$_POST["com_liv_adr2"];
					$com_liv_adr3=$_POST["com_liv_adr3"];
					$com_liv_cp=$_POST["com_liv_cp"];
					$com_liv_ville=$_POST["com_liv_ville"];
					break;
				default:
					$com_liv_nom=$_POST["com_liv_nom"];
					$com_liv_adr1=$_POST["com_liv_adr1"];
					$com_liv_adr2=$_POST["com_liv_adr2"];
					$com_liv_adr3=$_POST["com_liv_adr3"];
					$com_liv_cp=$_POST["com_liv_cp"];
					$com_liv_ville=$_POST["com_liv_ville"];
			}
		}

		// EDITION
		if(!empty($commande_id)){

			$sql="SELECT * FROM Commandes WHERE com_id=" . $commande_id . ";";
			$req = $Conn->query($sql);
			$d_commande=$req->fetch();
			if(!empty($d_commande)){

				// LA COMMANDE EST LIE A UNE ACTION
				$com_societe=$d_commande["com_societe"];
				$com_action=$d_commande["com_action"];
				$com_donneur_ordre=$d_commande["com_donneur_ordre"];
				$com_ht=$d_commande["com_ht"];
				$com_ttc=$d_commande["com_ttc"];

				if(!empty($com_action)){
					$com_fournisseur=$d_commande["com_fournisseur"];

					$com_liv_type=$d_commande["com_liv_type"];
					$com_liv_ref_1=$d_commande["com_liv_ref_1"];
					$com_liv_ref_2=$d_commande["com_liv_ref_2"];
					$com_liv_nom=$d_commande["com_liv_nom"];
					$com_liv_adr1=$d_commande["com_liv_adr1"];
					$com_liv_adr2=$d_commande["com_liv_adr2"];
					$com_liv_adr3=$d_commande["com_liv_adr3"];
					$com_liv_cp=$d_commande["com_liv_cp"];
					$com_liv_ville=$d_commande["com_liv_ville"];

					$com_action=$d_commande["com_action"];

					$com_agence=$d_commande["com_agence"];

				}else{

					if(isset($_POST["com_agence"])){
						$com_agence=$d_commande["com_agence"];
					}else{
						$com_agence=$d_commande["com_agence"];
					}
				}

			}else{
				$erreur_txt="B Impossible de charger la commande";
			}
		}else{

			$com_societe=$acc_societe;
			if(isset($_POST["com_agence"])){
				$com_agence=0;
				if(!empty($_POST["com_agence"])){
					$com_agence=intval($_POST["com_agence"]);
				}
			}else{
				$com_agence=$acc_agence;
			}
			$com_donneur_ordre=$acc_utilisateur;

			$com_action=0;
			$com_ht=0;
			$com_ttc=0;
		}


		// SUR LE FOURNNISSEUR

		if(empty($erreur_txt)){
			// place ici car les variables ci-dessous ne sont pas géré dans le formulaire
			$req = $Conn->prepare("SELECT fou_code,fou_nom,fou_type,fou_reg_type,fou_reg_nb_jour,fou_reg_formule,fou_reg_fdm FROM fournisseurs WHERE fou_id = " . $com_fournisseur);
			$req->execute();
			$d_fournisseur = $req->fetch();
			if(empty($d_fournisseur)){
				$erreur_txt="Err 005 Impossible de genéré le bon de commande";
			}else{
				$com_fou_code=$d_fournisseur["fou_code"];
				$com_fou_nom=$d_fournisseur["fou_nom"];
				$com_reg_type=$d_fournisseur["fou_reg_type"];
				$com_reg_nb_jour=$d_fournisseur["fou_reg_nb_jour"];
				$com_reg_formule=$d_fournisseur["fou_reg_formule"];
				$com_reg_fdm=$d_fournisseur["fou_reg_fdm"];

			}
		}

	}

	// DONNEE COMPLEMENTAIRE


	// ADRESSE DE FACTURATION

	if(empty($erreur_txt)){

		$req = $Conn->query("SELECT soc_nom,soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville FROM Societes WHERE soc_id = " . $com_societe);
		$d_societe= $req->fetch();
		if(!empty($d_societe)){
			$com_fac_nom=$d_societe["soc_nom"];
			$com_fac_adr1=$d_societe["soc_ad1"];
			$com_fac_adr2=$d_societe["soc_ad2"];
			$com_fac_adr3=$d_societe["soc_ad3"];
			$com_fac_cp=$d_societe["soc_cp"];
			$com_fac_ville=$d_societe["soc_ville"];
		}else{
			$erreur_txt="Impossible de charger l'adresse de facturation!";
		}
	}

	if(empty($erreur_txt)){

		if(!empty($com_agence)){

			$req = $Conn->query("SELECT age_nom,age_ad1,age_ad2,age_ad3,age_cp,age_ville FROM Agences WHERE age_id = " . $com_agence);
			$d_agence = $req->fetch();
			if(!empty($d_agence)){
				$com_fac_nom.=" " . $d_agence["age_nom"];
				$com_fac_adr1=$d_agence["age_ad1"];
				$com_fac_adr2=$d_agence["age_ad2"];
				$com_fac_adr3=$d_agence["age_ad3"];
				$com_fac_cp=$d_agence["age_cp"];
				$com_fac_ville=$d_agence["age_ville"];
			}else{
				$erreur_txt="Impossible de charger l'adresse de facturation!";
			}
		}
	}

	// ADRESSE DE LIVRASION

	if(empty($erreur_txt)){

		if(empty($com_action)){

			if($com_liv_type==1){

				$com_liv_nom=$com_fac_nom;
				$com_liv_adr1=$com_fac_adr1;
				$com_liv_adr2=$com_fac_adr2;
				$com_liv_adr3=$com_fac_adr3;
				$com_liv_cp=$com_fac_cp;
				$com_liv_ville=$com_fac_ville;

			}elseif($com_liv_type==2){

				// LIVRAISON DANS UNE AUTRE AGENCE

				$req = $Conn->query("SELECT soc_nom,soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville FROM Societes WHERE soc_id = " . $com_liv_ref_1);
				$d_societe= $req->fetch();
				if(!empty($d_societe)){
					$com_liv_nom=$d_societe["soc_nom"];
					$com_liv_adr1=$d_societe["soc_ad1"];
					$com_liv_adr2=$d_societe["soc_ad2"];
					$com_liv_adr3=$d_societe["soc_ad3"];
					$com_liv_cp=$d_societe["soc_cp"];
					$com_liv_ville=$d_societe["soc_ville"];
				}

				if(!empty($com_liv_ref_2)){

					$req = $Conn->query("SELECT age_nom,age_ad1,age_ad2,age_ad3,age_cp,age_ville FROM Agences WHERE age_id = " . $com_liv_ref_2);
					$d_agence = $req->fetch();
					if(!empty($d_agence)){
						$com_liv_nom.=" " . $d_agence["age_nom"];
						$com_liv_adr1=$d_agence["age_ad1"];
						$com_liv_adr2=$d_agence["age_ad2"];
						$com_liv_adr3=$d_agence["age_ad3"];
						$com_liv_cp=$d_agence["age_cp"];
						$com_liv_ville=$d_agence["age_ville"];
					}
				}
			}

		}
	}

	// CONTACT AUTO ENTREPRENEUR
	$maj_contact=false;
	if(!empty($d_fournisseur) && $d_fournisseur["fou_type"]==3){

		$sql="SELECT * FROM Fournisseurs_Contacts WHERE fco_fournisseur=" . $com_fournisseur . ";";
		$req=$Conn->query($sql);
		$d_contact = $req->fetchAll();
		if(!empty($d_contact)){
			if(count($d_contact)>1){
				$erreur_txt="L'utilisateur selectionné dispose de plusieurs contacts.";
			}elseif(empty($com_contact)){
				$com_contact=$d_contact[0]["fco_id"];
				$com_contact_nom=$d_contact[0]["fco_nom"];
				$com_contact_prenom=$d_contact[0]["fco_prenom"];
				$com_contact_tel=$d_contact[0]["fco_tel"];
				$com_contact_fax=$d_contact[0]["fco_fax"];
				$com_contact_portable=$d_contact[0]["fco_portable"];
				$com_contact_mail=$d_contact[0]["fco_mail"];
				if($com_contact_fonction == "autre"){
					$com_contact_fonction=0;
				}else{
					$com_contact_fonction=$d_contact[0]["fco_fonction"];
				}
				$com_contact_fonction_nom=$d_contact[0]["fco_fonction_nom"];
				$com_contact_titre=$d_contact[0]["fco_titre"];
			}else{

				// cas ou U à potentiellment modifier le contact -> declenche traitement à annexe
				$maj_contact=true;
			}
		}
	}

	// ENREGISTREMENT

	if(empty($erreur_txt)){

		if(empty($commande_id)){

			// enregistrer le bon de commande
			$sql="INSERT INTO commandes
			(com_societe,
			com_agence,
			com_date,
			com_donneur_ordre,
			com_fournisseur,
			com_fou_code,
			com_fou_nom,
			com_contact,
			com_contact_nom,
			com_contact_prenom,
			com_contact_tel,
			com_contact_fax,
			com_contact_mail,
			com_contact_portable,
			com_contact_fonction,
			com_contact_fonction_nom,
			com_contact_titre,
			com_liv_type,
			com_liv_ref_1,
			com_liv_ref_2,
			com_liv_nom,
			com_liv_adr1,
			com_liv_adr2,
			com_liv_adr3,
			com_liv_cp,
			com_liv_ville,
			com_fac_nom,
			com_fac_adr1,
			com_fac_adr2,
			com_fac_adr3,
			com_fac_cp,
			com_fac_ville,
			com_action,
			com_reg_type,
			com_reg_nb_jour,
			com_reg_formule,
			com_reg_fdm,
			com_ht,
			com_ttc";
			if(!empty($heracles_id)){
				$sql.=",com_id";
			}
			$sql.=") VALUES (
			:com_societe,
			:com_agence,
			NOW(),
			:com_donneur_ordre,
			:com_fournisseur,
			:com_fou_code,
			:com_fou_nom,
			:com_contact,
			:com_contact_nom,
			:com_contact_prenom,
			:com_contact_tel,
			:com_contact_fax,
			:com_contact_mail,
			:com_contact_portable,
			:com_contact_fonction,
			:com_contact_fonction_nom,
			:com_contact_titre,
			:com_liv_type,
			:com_liv_ref_1,
			:com_liv_ref_2,
			:com_liv_nom,
			:com_liv_adr1,
			:com_liv_adr2,
			:com_liv_adr3,
			:com_liv_cp,
			:com_liv_ville,
			:com_fac_nom,
			:com_fac_adr1,
			:com_fac_adr2,
			:com_fac_adr3,
			:com_fac_cp,
			:com_fac_ville,
			:com_action,
			:com_reg_type,
			:com_reg_nb_jour,
			:com_reg_formule,
			:com_reg_fdm,
			:com_ht,
			:com_ttc";
			if(!empty($heracles_id)){
				$sql.=",:com_id";
			}
			$sql.=")";
			$req = $Conn->prepare($sql);
	        $req->bindParam(':com_societe',$com_societe);
	        $req->bindParam(':com_agence', $com_agence);
	        $req->bindParam(':com_donneur_ordre', $com_donneur_ordre);
	        $req->bindParam(':com_fournisseur', $com_fournisseur);
	        $req->bindParam(':com_fou_code', $com_fou_code);
	        $req->bindParam(':com_fou_nom', $com_fou_nom);
			$req->bindParam(':com_contact', $com_contact);
	        $req->bindParam(':com_contact_nom', $com_contact_nom);
	        $req->bindParam(':com_contact_prenom', $com_contact_prenom);
	        $req->bindParam(':com_contact_tel', $com_contact_tel);
	        $req->bindParam(':com_contact_fax', $com_contact_fax);
	        $req->bindParam(':com_contact_portable', $com_contact_portable);
	        $req->bindParam(':com_contact_mail', $com_contact_mail);
	        $req->bindParam(':com_contact_fonction', $com_contact_fonction);
	        $req->bindParam(':com_contact_fonction_nom', $com_contact_fonction_nom);
	        $req->bindParam(':com_contact_titre', $com_contact_titre);
			$req->bindParam(':com_liv_type', $com_liv_type);
			$req->bindParam(':com_liv_ref_1', $com_liv_ref_1);
			$req->bindParam(':com_liv_ref_2', $com_liv_ref_2);
	        $req->bindParam(':com_liv_nom', $com_liv_nom);
	        $req->bindParam(':com_liv_adr1', $com_liv_adr1);
	        $req->bindParam(':com_liv_adr2', $com_liv_adr2);
	        $req->bindParam(':com_liv_adr3', $com_liv_adr3);
	        $req->bindParam(':com_liv_cp', $com_liv_cp);
	        $req->bindParam(':com_liv_ville', $com_liv_ville);
			$req->bindParam(':com_fac_nom', $com_fac_nom);
			$req->bindParam(':com_fac_adr1', $com_fac_adr1);
			$req->bindParam(':com_fac_adr2', $com_fac_adr2);
			$req->bindParam(':com_fac_adr3', $com_fac_adr3);
			$req->bindParam(':com_fac_cp', $com_fac_cp);
			$req->bindParam(':com_fac_ville', $com_fac_ville);
	        $req->bindParam(':com_action', $com_action);
	        $req->bindParam(':com_reg_type', $com_reg_type);
	        $req->bindParam(':com_reg_nb_jour', $com_reg_nb_jour);
	        $req->bindParam(':com_reg_formule', $com_reg_formule);
	        $req->bindParam(':com_reg_fdm', $com_reg_fdm);
			$req->bindParam(':com_ht', $com_ht);
			$req->bindParam(':com_ttc', $com_ttc);
			if(!empty($heracles_id)){
				 $req->bindParam(':com_id', $heracles_id);
			}
			try{
				$req->execute();
				$commande_id = $Conn->lastInsertId();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
			}

			if(empty($erreur_txt)){
				if(!empty($heracles_id)){
					$commande_id=$heracles_id;
				}
				$numero_bc = sprintf("%05s", $commande_id);
				$bc = "DA" . date("y") . "-" . $numero_bc;
				$req = $Conn->prepare("UPDATE commandes SET com_numero = '" . $bc . "' WHERE com_id = " . $commande_id);
				$req->execute();
			}

		}else{

			// MAJ D'UN BC
			$req = $Conn->prepare("SELECT * FROM commandes_lignes WHERE cli_commande = " . $commande_id);
			$req->execute();
			$commande_exist = $req->fetch();
			$req = $Conn->prepare("SELECT com_fournisseur FROM commandes WHERE com_id = " . $commande_id);
			$req->execute();
			$commande_actu = $req->fetch();
			if(!empty($commande_exist) && $commande_actu['com_fournisseur'] != $com_fournisseur){
				if(!empty($warning_txt)){
					$_SESSION['message'][] = array(
						"titre" => "Attention",
						"type" => "warning",
						"message" => "Impossible de modifier le fournisseur car il existe une ligne de commande"
					);
				}
				header("location : commande.php?id=" . $commande_id);
				die();
			}
			$req = $Conn->prepare("UPDATE commandes
				SET com_fournisseur = :com_fournisseur,
				com_fou_code = :com_fou_code,
				com_fou_nom = :com_fou_nom,
				com_contact = :com_contact,
				com_contact_nom = :com_contact_nom,
				com_contact_prenom = :com_contact_prenom,
				com_contact_tel = :com_contact_tel,
				com_contact_fax = :com_contact_fax,
				com_contact_mail = :com_contact_mail,
				com_contact_portable = :com_contact_portable,
				com_contact_fonction = :com_contact_fonction,
				com_contact_fonction_nom = :com_contact_fonction_nom,
				com_contact_titre = :com_contact_titre,
				com_liv_type= :com_liv_type,
				com_liv_ref_1= :com_liv_ref_1,
				com_liv_ref_2= :com_liv_ref_2,
				com_liv_nom = :com_liv_nom,
				com_liv_adr1 = :com_liv_adr1,
				com_liv_adr2 = :com_liv_adr2,
				com_liv_adr3 = :com_liv_adr3,
				com_liv_cp = :com_liv_cp,
				com_liv_ville = :com_liv_ville,
				com_action = :com_action,
				com_reg_type = :com_reg_type,
				com_reg_nb_jour = :com_reg_nb_jour,
				com_reg_formule = :com_reg_formule,
				com_reg_fdm = :com_reg_fdm WHERE com_id = :com_id"
			);

	        $req->bindParam(':com_fournisseur', $com_fournisseur);
	        $req->bindParam(':com_fou_code', $com_fou_code);
	        $req->bindParam(':com_fou_nom', $com_fou_nom);
			$req->bindParam(':com_contact', $com_contact);
	        $req->bindParam(':com_contact_nom', $com_contact_nom);
	        $req->bindParam(':com_contact_prenom', $com_contact_prenom);
	        $req->bindParam(':com_contact_tel', $com_contact_tel);
	        $req->bindParam(':com_contact_fax', $com_contact_fax);
	        $req->bindParam(':com_contact_portable', $com_contact_portable);
	        $req->bindParam(':com_contact_mail', $com_contact_mail);
	        $req->bindParam(':com_contact_fonction', $com_contact_fonction);
	        $req->bindParam(':com_contact_fonction_nom', $com_contact_fonction_nom);
	        $req->bindParam(':com_contact_titre', $com_contact_titre);
			$req->bindParam(':com_liv_type', $com_liv_type);
			$req->bindParam(':com_liv_ref_1', $com_liv_ref_1);
			$req->bindParam(':com_liv_ref_2', $com_liv_ref_2);
	        $req->bindParam(':com_liv_nom', $com_liv_nom);
	        $req->bindParam(':com_liv_adr1', $com_liv_adr1);
	        $req->bindParam(':com_liv_adr2', $com_liv_adr2);
	        $req->bindParam(':com_liv_adr3', $com_liv_adr3);
	        $req->bindParam(':com_liv_cp', $com_liv_cp);
	        $req->bindParam(':com_liv_ville', $com_liv_ville);
	        $req->bindParam(':com_action', $com_action);
	        $req->bindParam(':com_reg_type', $com_reg_type);
	        $req->bindParam(':com_reg_nb_jour', $com_reg_nb_jour);
	        $req->bindParam(':com_reg_formule', $com_reg_formule);
	        $req->bindParam(':com_reg_fdm', $com_reg_fdm);
	        $req->bindParam(':com_id', $commande_id);
			try{
				$req->execute();
			}Catch(Exception $e){
				$erreur_txt="Erreur maj BC " . $e->getMessage();
			}
		}
	}
	// ENREGISTREMENT DES LIGNES
	if(empty($erreur_txt)){

		if(!empty($lignes)){

			foreach($lignes as $l){

				if(empty($l["id"])){

					// AJOUT D'UNE LIGNE

					$sql="INSERT INTO Commandes_Lignes
					(cli_commande,
					cli_categorie_si2p,
					cli_famille_si2p,
					cli_sous_famille_si2p,
					cli_sous_sous_famille_si2p,
					cli_produit_si2p,
					cli_famille,
					cli_reference,
					cli_libelle,
					cli_descriptif,
					cli_pu,
					cli_qte,
					cli_ht,
					cli_tva,
					cli_tva_periode,
					cli_tva_taux,
					cli_ttc,
					cli_dates,
					cli_fou_int,
					cli_livraison
					) VALUES (
					:cli_commande,
					:cli_categorie_si2p,
					:cli_famille_si2p,
					:cli_sous_famille_si2p,
					:cli_sous_sous_famille_si2p,
					:cli_produit_si2p,
					:cli_famille,
					:cli_reference,
					:cli_libelle,
					:cli_descriptif,
					:cli_pu,
					:cli_qte,
					:cli_ht,
					:cli_tva,
					:cli_tva_periode,
					:cli_tva_taux,
					:cli_ttc,
					:cli_dates,
					:cli_fou_int,
					:cli_livraison
					);";
					$req = $Conn->prepare($sql);
					$req->bindParam(':cli_commande',$commande_id);
					$req->bindParam(':cli_categorie_si2p',$l["pro_categorie"]);
					$req->bindParam(':cli_famille_si2p',$l["pro_famille"]);
					$req->bindParam(':cli_sous_famille_si2p',$l["pro_sous_famille"]);
					$req->bindParam(':cli_sous_sous_famille_si2p',$l["pro_sous_sous_famille"]);
					$req->bindParam(':cli_produit_si2p',$l["pro_id"]);
					$req->bindParam(':cli_famille', $l["famille"]);
					$req->bindParam(':cli_reference', $l["reference"]);
					$req->bindParam(':cli_libelle', $l["libelle"]);
					$req->bindParam(':cli_descriptif', $l["descriptif"]);
					$req->bindParam(':cli_pu', $l["pu"]);
					$req->bindParam(':cli_qte', $l["qte"]);
					$req->bindParam(':cli_ht', $l["ht"]);
					$req->bindParam(':cli_tva', $l["tva"]);
					$req->bindParam(':cli_tva_periode', $l["tva_periode"]);
					$req->bindParam(':cli_tva_taux', $l["tva_taux"]);
					$req->bindParam(':cli_ttc', $l["ttc"]);
					$req->bindParam(':cli_dates', $l["dates"]);
					$req->bindParam(':cli_fou_int', $l["int_id"]);
					$req->bindParam(':cli_livraison', $l["livraison"]);
					try{
						$req->execute();
					}Catch(Exception $e){
						$warning_txt=$e->getMessage();
					}

				}else{

					// MAJ D'UN BC

					$req = $Conn->prepare("UPDATE Commandes_Lignes
					SET cli_categorie_si2p = :cli_categorie_si2p,
					cli_famille_si2p = :cli_famille_si2p,
					cli_sous_famille_si2p = :cli_sous_famille_si2p,
					cli_produit_si2p = :cli_produit_si2p,
					cli_famille = :cli_famille,
					cli_reference = :cli_reference,
					cli_libelle = :cli_libelle,
					cli_descriptif = :cli_descriptif,
					cli_pu = :cli_pu,
					cli_qte = :cli_qte,
					cli_tva = :cli_tva,
					cli_tva_periode = :cli_tva_periode,
					cli_tva_taux = :cli_tva_taux,
					cli_ttc = :cli_ttc,
					cli_dates = :cli_dates,
					cli_fou_int = :cli_fou_int,
					cli_livraison = :cli_livraison
					WHERE cli_id = :cli_id");

					$req->bindParam(':cli_id',$l["id"]);
					$req->bindParam(':cli_categorie_si2p',$l["pro_categorie"]);
					$req->bindParam(':cli_famille_si2p',$l["pro_famille"]);
					$req->bindParam(':cli_sous_famille_si2p',$l["pro_sous_famille"]);
					$req->bindParam(':cli_sous_sous_famille_si2p',$l["pro_sous_sous_famille"]);
					$req->bindParam(':cli_produit_si2p',$l["pro_id"]);
					$req->bindParam(':cli_famille', $l["famille"]);
					$req->bindParam(':cli_reference', $l["reference"]);
					$req->bindParam(':cli_libelle', $l["libelle"]);
					$req->bindParam(':cli_descriptif', $l["descriptif"]);
					$req->bindParam(':cli_pu', $l["ht"]);
					$req->bindParam(':cli_qte', $l["qte"]);
					$req->bindParam(':cli_tva', $l["tva"]);
					$req->bindParam(':cli_tva_periode', $l["tva_periode"]);
					$req->bindParam(':cli_tva_taux', $l["tva_taux"]);
					$req->bindParam(':cli_ttc', $l["sdf"]);
					$req->bindParam(':cli_dates', $l["dates"]);
					$req->bindParam(':cli_fou_int', $l["int_id"]);
					$req->bindParam(':cli_livraison', $l["livraison"]);
					try{
						$req->execute();
					}Catch(Exception $e){
						$warning_txt=$e->getMessage();
					}
				}
			}
		}
	}

	// MAJ CONTACT
	// -> Seulement si c'est un Auto-E

	if(empty($erreur_txt)){

		var_dump($maj_contact);
		echo("<br/>");

		if($maj_contact){

			$req = $Conn->prepare("UPDATE Fournisseurs_Contacts
			SET fco_fonction = :com_contact_fonction,
			fco_fonction_nom = :com_contact_fonction_nom,
			fco_titre = :com_contact_titre,
			fco_nom = :com_contact_nom,
			fco_prenom = :com_contact_prenom,
			fco_tel = :com_contact_tel,
			fco_portable = :com_contact_portable,
			fco_fax = :com_contact_fax,
			fco_mail = :com_contact_mail
			WHERE fco_id = :com_contact AND fco_fournisseur=:com_fournisseur");
			$req->bindParam(':com_contact',$com_contact);
			$req->bindParam(':com_fournisseur',$com_fournisseur);
			$req->bindParam(':com_contact_nom',$com_contact_nom);
			$req->bindParam(':com_contact_prenom',$com_contact_prenom);
			$req->bindParam(':com_contact_tel',$com_contact_tel);
			$req->bindParam(':com_contact_portable',$com_contact_portable);
			$req->bindParam(':com_contact_fax',$com_contact_fax);
			$req->bindParam(':com_contact_mail', $com_contact_mail);
			$req->bindParam(':com_contact_fonction_nom', $com_contact_fonction_nom);
			$req->bindParam(':com_contact_fonction', $com_contact_fonction);
			$req->bindParam(':com_contact_titre',$com_contact_titre);
			try{
				$req->execute();
			}Catch(Exception $e){
				$warning_txt="MAJ CONTACT : " . $e->getMessage();
			}

			// MAJ DE LA FICHE INTERVENANT
			if(empty($warning_txt)){

				$sql="SELECT fin_id,fin_nom,fin_prenom FROM Fournisseurs_Intervenants WHERE fin_fournisseur=" . $com_fournisseur . ";";
				$req = $Conn->query($sql);
				$d_intervenant=$req->fetchAll();
				if(!empty($d_intervenant)){
					if(count($d_intervenant)>1){
						$warning_txt="Cet auto-entrepreneur dispose de plusieurs fiches intervenant!";
					}
				}else{
					$warning_txt="Cet auto-entrepreneur n'a pas de fiche intervenant!";
				}
			}
			if(empty($warning_txt)){

				$sql="UPDATE Fournisseurs_Intervenants SET
				fin_nom=:com_contact_nom,
				fin_prenom=:com_contact_prenom,
				fin_tel=:com_contact_tel,
				fin_portable=:com_contact_portable,
				fin_fax=:com_contact_fax,
				fin_mail=:com_contact_mail
				WHERE fin_id=:intervenant;";
				$req = $Conn->prepare($sql);
				$req->bindParam(':intervenant',$d_intervenant[0]["fin_id"]);
				$req->bindParam(':com_contact_nom',$com_contact_nom);
				$req->bindParam(':com_contact_prenom',$com_contact_prenom);
				$req->bindParam(':com_contact_tel',$com_contact_tel);
				$req->bindParam(':com_contact_portable',$com_contact_portable);
				$req->bindParam(':com_contact_fax',$com_contact_fax);
				$req->bindParam(':com_contact_mail', $com_contact_mail);
				try{
					$req->execute();
				}Catch(Exception $e){
					$warning_txt="MAJ INTERVENANT : " . $e->getMessage();
				}
			}
			// MAJ DES FICHES PLANNINGS

			if(empty($warning_txt) AND ($d_intervenant[0]["fin_nom"]!=$com_contact_nom OR $d_intervenant[0]["fin_prenom"]!=$com_contact_prenom)){

				$sql="SELECT DISTINCT fso_societe FROM Fournisseurs_Societes WHERE fso_fournisseur=" . $com_fournisseur . ";";
				$req = $Conn->query($sql);
				$d_societe=$req->fetchAll();
				if(!empty($d_societe)){

					$int_label_1=$d_fournisseur["fou_code"] . "-" . $com_contact_nom;
					$int_label_2=$com_contact_prenom;

					foreach($d_societe as $soc){

						$ConnFct=connexion_fct($soc["fso_societe"]);

						$req = $ConnFct->prepare("UPDATE Intervenants SET int_label_1 = :int_label_1, int_label_2 = :int_label_2
						WHERE int_type=3 AND int_ref_1=:int_ref_1 AND int_ref_2=:int_ref_2;");
						$req->bindParam(':int_label_1', $int_label_1);
						$req->bindParam(':int_label_2', $int_label_2);
						$req->bindParam(':int_ref_1', $com_fournisseur);
						$req->bindParam(':int_ref_2', $d_intervenant[0]["fin_id"]);
						try{
							$req->execute();
						}Catch(Exception $e){
							$warning_txt="MAJ FICHE PLANNINH " . $e->getMessage();
						}
					}
				}

			}
		}
	}

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);

		if(!empty($com_action)){
			header("location : action_voir.php?action=" . $com_action);
		}elseif(!empty($commande_id)){
			header("location : commande_voir.php?commande=" . $commande_id);
		}else{
			header("location : commande.php");
		}


	}else{
		if(!empty($warning_txt)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "warning",
				"message" => $warning_txt
			);
		}
		if(empty($_POST["com_id"])){

			// creation

			if(!empty($lignes)){
				// Orion à créé la la ligne en auto -> on visualise le BC classique
				header("location : commande_voir.php?commande=" . $commande_id);
			}else{
				// Orion n'a pas identifié la ligne -> on passe l'action pour ouvrir le modal ligne et le pre-renseigner
				header("location : commande_voir.php?action=" . $com_action . "&commande=" . $commande_id);
			}
		}else{
			header("location : commande_voir.php?commande=" . $commande_id);
		}

	}
	die();

?>
