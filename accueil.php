<?php
	// a mettre sur toute les pages de l'application
	// contient le session_start();
include "includes/controle_acces.inc.php";
include "modeles/mod_parametre.php";
include "includes/connexion.php";
include "includes/connexion_soc.php";
include "includes/connexion_fct.php";

// SUR QUEL MENU NOUS SOMMES
if(!empty($_GET['menu'])){
	$mod_menu = $_GET['menu'];
	$menu_actif = $_GET['menu'] . "-0";

}else{
	$mod_menu = 0;
	$menu_actif = "0-0";
}

$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);
}

if(isset($_SESSION['retourClient'])){
	unset($_SESSION['retourClient']);
}
$_SESSION["retour"]="accueil.php?menu=" . $mod_menu;
$_SESSION["retourDevis"]="accueil.php?menu=" . $mod_menu;
$_SESSION["retour_action"]="accueil.php?menu=" . $mod_menu;



// comment test

// récupérer les modèles de la personne connectée
$reqmod = $Conn->prepare("SELECT * FROM Modeles WHERE mod_utilisateur = :mod_utilisateur AND mod_menu = :mod_menu");
$reqmod->bindParam("mod_utilisateur", $_SESSION['acces']['acc_ref_id']);
$reqmod->bindParam("mod_menu", $mod_menu);
$reqmod->execute();
$modele = $reqmod->fetch();



if(!empty($modele)){
	// fonction 1
	$req = $Conn->prepare("SELECT mfo_nom, mfo_include FROM Modeles_fonctions WHERE mfo_id = :mfo_id");
	$req->bindParam("mfo_id", $modele['mod_fct_1']);
	$req->execute();
	$modele["mod_inc_1"] = $req->fetch();
	// fonction 2
	$req = $Conn->prepare("SELECT mfo_nom, mfo_include FROM Modeles_fonctions WHERE mfo_id = :mfo_id");
	$req->bindParam("mfo_id", $modele['mod_fct_2']);
	$req->execute();
	$modele["mod_inc_2"] = $req->fetch();

	// fonction 3
	$req = $Conn->prepare("SELECT mfo_nom, mfo_include FROM Modeles_fonctions WHERE mfo_id = :mfo_id");
	$req->bindParam("mfo_id", $modele['mod_fct_3']);
	$req->execute();
	$modele["mod_inc_3"] = $req->fetch();

	// fonction 4
	$req = $Conn->prepare("SELECT mfo_nom, mfo_include FROM Modeles_fonctions WHERE mfo_id = :mfo_id");
	$req->bindParam("mfo_id", $modele['mod_fct_4']);
	$req->execute();
	$modele["mod_inc_4"] = $req->fetch();

	// fonction 5
	$req = $Conn->prepare("SELECT mfo_nom, mfo_include FROM Modeles_fonctions WHERE mfo_id = :mfo_id");
	$req->bindParam("mfo_id", $modele['mod_fct_5']);
	$req->execute();
	$modele["mod_inc_5"] = $req->fetch();

	// fonction 6
	$req = $Conn->prepare("SELECT mfo_nom, mfo_include FROM Modeles_fonctions WHERE mfo_id = :mfo_id");
	$req->bindParam("mfo_id", $modele['mod_fct_6']);
	$req->execute();
	$modele["mod_inc_6"] = $req->fetch();

}
// fin

// récupérer la liste des bugs
$req = $Conn->prepare("SELECT bug_id FROM bugs WHERE bug_statut = 2 AND bug_statut_date='" . date("Y-m-d") . "'");
$req->execute();
$nbBugsCorriges = $req->fetchAll();
$nbBugsCorriges = count($nbBugsCorriges);

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css?v2.3.8">

	<link rel="stylesheet" type="text/css" href="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

	<!-- CSS Si2P -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm no-scroll" >
		<div id="main" style="height:100%!important;">
<?php
			if($_SESSION["acces"]["acc_ref"]==3){
				include "includes/header_sta.inc.php";
			}elseif($_SESSION["acces"]["acc_ref"]==2){
				include "includes/header_cli.inc.php";
			}else{
				include "includes/header_def.inc.php";
			} ?>
			<section id="content_wrapper" style="height:100%!important;">
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn" style="height:100%!important;">
					<div class="row " style="height:100%!important;">

				<?php	$modele_exist=false;
						$num_fct=0;
						if(empty($modele)){
							// si le modèle n'existe pas, il faut en créer un?>
							<div class="col-md-2 col-md-offset-5"  style="height:100%!important;vertical-align: middle;">
								<a href="modele.php?menu=<?= $mod_menu ?>" class="btn btn-success btn-gradient dark btn-block" style="position: absolute;top: 50%;transform: translateY(-50%);">
									Créer un tableau de bord
								</a>
							</div>
				<?php 	}else{

							if(empty($modele["mod_fct_1"]) && empty($modele["mod_fct_2"]) && empty($modele["mod_fct_3"]) && empty($modele["mod_fct_4"]) && empty($modele["mod_fct_5"]) && empty($modele["mod_fct_6"])){ ?>
								<div class="col-md-2 col-md-offset-5"  style="height:100%!important;vertical-align: middle;">
									<a href="modele.php?menu=<?=$mod_menu?>" class="btn btn-success btn-gradient dark btn-block" style="position: absolute;top: 50%;transform: translateY(-50%);">
										Créer un tableau de bord
									</a>
								</div>
					<?php 	}else{

								$modele_exist=true;



								if($modele['mod_type'] == 1){
									//TYPE 1 : 2 fct ?>
									<div class="col-md-6" style="height:98%!important;">
					<?php 				if(!empty($modele["mod_inc_1"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_1"]['mfo_include']); ?>
					<?php 				} ?>
									</div>
									<div class="col-md-6" style="height:98%!important;">
					<?php 				if(!empty($modele["mod_inc_2"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_2"]['mfo_include']); ?>
					<?php 				} ?>
									</div>
					<?php 		}elseif($modele['mod_type'] == 2){ ?>

									<div class="col-md-6" style="height:50%!important;">
					<?php 				if(!empty($modele["mod_inc_1"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_1"]['mfo_include']); ?>
					<?php 				} ?>
									</div>
									<div class="col-md-6" style="height:50%!important;">
					<?php 				if(!empty($modele["mod_inc_2"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_2"]['mfo_include']); ?>
					<?php 				} ?>
									</div>
									<div class="col-md-6" style="height:50%!important;">
					<?php 				if(!empty($modele["mod_inc_3"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_3"]['mfo_include']); ?>
					<?php 				} ?>
									</div>
									<div class="col-md-6" style="height:50%!important;">
					<?php 				if(!empty($modele["mod_inc_4"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_4"]['mfo_include']); ?>
					<?php 				} ?>
									</div>

					<?php 		}elseif($modele['mod_type'] == 3){ 	?>

									<div class="col-md-4" style="height:50%!important;">

					<?php 				if(!empty($modele["mod_inc_1"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_1"]['mfo_include']); ?>
					<?php 				} ?>
									</div>
									<div class="col-md-4" style="height:50%!important;">


					<?php 				if(!empty($modele["mod_inc_2"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_2"]['mfo_include']); ?>
					<?php 				} ?>
									</div>

									<div class="col-md-4" style="height:50%!important;">
					<?php 				if(!empty($modele["mod_inc_3"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_3"]['mfo_include']); ?>
					<?php 				} ?>
									</div>
									<div class="col-md-4" style="height:50%!important;">
					<?php 				if(!empty($modele["mod_inc_4"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_4"]['mfo_include']); ?>
					<?php 				} ?>
									</div>
									<div class="col-md-4" style="height:50%!important;">
					<?php 				if(!empty($modele["mod_inc_5"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_5"]['mfo_include']); ?>
					<?php 				} ?>
									</div>
									<div class="col-md-4" style="height:50%!important;">
					<?php 				if(!empty($modele["mod_inc_6"])){
											$num_fct++;
											include("includes/modeles_fonctions/" . $modele["mod_inc_6"]['mfo_include']); ?>
					<?php 				} ?>
									</div>
					<?php 		}
							}
						}?>
					</div>

				</section>
			</section>
		</div>
		<footer id="content-footer" >
			<div class="row">
				<div class="col-xs-3 text-center"></div>
				<div class="col-xs-6 footer-middle text-center" style="">
						<p class="mt5">
					<?php	if(!empty($nbBugsCorriges)){ ?>
								<strong>
									<a href="bugs_liste.php?liste=1" ><?= $nbBugsCorriges ?> incident(s) traité(s) aujourd'hui</a>
								</strong>
								-
					<?php	} ?>
							<a href="bugs_liste.php">Voir tous les incidents</a>
						</p>
				</div>
				<div class="col-xs-3 footer-right">
		<?php 		if($modele_exist){ ?>
						<a href="modele.php?menu=<?= $mod_menu ?>" class="btn btn-sm btn-success">
							<i class="fa fa-pencil"></i>
							Paramétrer mon tableau de bord
						</a>
		<?php		} ?>
				</div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>

	

		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>

		<!-- Simple Circles Plugin -->
		<script type="text/javascript" src="vendor/plugins/circles/circles.js"></script>
		<!-- Sparkline Plugin-->
		<script src="vendor/plugins/sparkline/jquery.sparkline.min.js"></script>

		<!-- HighCharts Plugin -->
		<script src="vendor/plugins/highcharts/highcharts.js"></script>

		<!-- Simple Circles Plugin -->
		<script src="vendor/plugins/circles/circles.js"></script>

		<!-- Jvectormap JS -->
		<script src="vendor/plugins/jvectormap/jquery.jvectormap.min.js"></script>

		<!-- Jvectormap Map -->
		<script src="vendor/plugins/jvectormap/assets/jquery-jvectormap-us-lcc-en.js"></script>

		<!-- Theme Javascript -->
		
		<script src="assets/js/demo/demo.js"></script>
  		<!-- Demo Widget Javascript -->
  		<script src="assets/js/demo/widgets.js"></script>

		<!-- SCRIPT SPE -->
		<script type="text/javascript">

<?php 		// INCLUDES JS DES MODELES
			for ($i = 1; $i < 7; $i++) {
				if(!empty($modele['mod_fct_' . $i])){

					$fic_name=str_replace(".php",".js",$modele['mod_inc_' . $i]['mfo_include']);
					if(file_exists("includes/modeles_fonctions/" . $fic_name)){
						include("includes/modeles_fonctions/" . $fic_name);
					}
				}
			} ?>
			var calcDataTableHeight = function (elt_content, elt_head) {

				return $(elt_content).height() - $(elt_head).height() - 50;
			};
			var num_fct=parseInt("<?=$num_fct?>");
			var tableDefFix=new Array();

			$(document).ready(function (){
				<?php if(!empty($_GET['error'])) { ?>
					new PNotify({
							title: "Erreur",
							text: "<?= $_GET['error'] ?>",
							type: "danger", // all contextuals available(info,system,warning,etc)
							hide: true,
							mouse_reset: false,
							delay: 4000
						});
				<?php } ?>
				for(var bcl=1;bcl<=num_fct;bcl++){

					tri_col=$(".tableDefFix-" + bcl).data("tri_col");
					tri_sens=$(".tableDefFix-" + bcl).data("tri_sens");

					tableDefFix[bcl]=$(".tableDefFix-" + bcl).dataTable({
						"language": {
						"url": "/vendor/plugins/DataTables/media/js/French.json"
						},
						order:[tri_col,tri_sens],
						paging: false,
						searching: false,
						info: false,
						scrollY: calcDataTableHeight(".tableDefCont-" + bcl, ".tableDefHead-" + bcl),
						scrollCollapse: true
					});
				};

				$(window).resize(function () {

					for(var bcl=1;bcl<=num_fct;bcl++){
						var tableDefFixParam = tableDefFix[bcl].fnSettings();
						tableDefFixParam.oScroll.sY = calcDataTableHeight(".tableDefCont-" + bcl, ".tableDefHead-" + bcl);
						tableDefFix[bcl].fnDraw();
					}

				});
			});
		</script>
	</body>
</html>
