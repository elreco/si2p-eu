<?php

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include "modeles/mod_client.php";

$s = array();
if (isset($_GET['id'])) {
    $s = get_sous_classification($_GET['id']);
} else {
    $s = array();
}

$f = array();
$f = get_classifications();

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Paramètres</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">
            <header id="topbar" >
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li>
                            <span class="glyphicon glyphicon-cog"></span> <a href="parametre.php"> Paramètres</a>
                        </li>
                        <li>
                            <a href="sous_famille_liste.php">Sous-classifications</a>
                        </li>
                        <li class="crumb-trail">Nouvelle sous-classification</li>
                    </ol>
                </div>

            </header>
            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-center">

                                        <div class="content-header">
                                            <h2>Ajouter une <b class="text-primary">sous-classification</b></h2>

                                        </div>

                                        <div class="col-md-10 col-md-offset-1">
                                            <form action="sous_classification_enr.php" method="POST" id="admin-form">

                                                <?php if (isset($_GET['id'])): ?>
                                                    <input type="hidden" name="csc_id" value="<?=$s['csc_id']?>"></input>
                                                <?php endif;?>
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="section">
                                                      <div class="field select">
                                                        <select name="csc_classification" id="csc_classification" required="">
                                                            <option value="">Classification ...</option>
                                                            <?php foreach($f as $f1){ ?>
                                                            <option 
                                                            <?php if(isset($_GET['id'])): ?>
                                                                <?php if($s['csc_classification'] == $f1['ccl_id']): ?>
                                                                    selected
                                                                <?php endif; ?>
                                                            <?php endif; ?> 
                                                            value="<?= $f1['ccl_id'] ?>"><?= $f1['ccl_libelle'] ?></option>
                                                            <?php } ?>


                                                        </select>
                                                        <i class="arrow simple"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="section">
                                              <div class="field prepend-icon">
                                                <input type="text" name="csc_libelle" class="gui-input" id="csc_libelle" placeholder="Libellé" 

                                                <?php if(isset($_GET['id'])): ?>
                                                    value="<?=$s['csc_libelle']?>"
                                                <?php endif; ?>>
                                                <label for="csc_libelle" class="field-icon">
                                                  <i class="fa fa-book"></i>
                                              </label>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                          </div>
                      </div>


                  </div>
              </div>

          </div>
      </div>

  </div>
</section>
<!-- End: Content -->
</section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="sous_classification_liste.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-save'></i> Enregistrer
            </button>
        </div>
    </div>
</footer>
<?php
		include "includes/footer_script.inc.php"; ?>	
<!-- validation inputs -->

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->

<script src="assets/js/custom.js"></script>

</body>
</html>
