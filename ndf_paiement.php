<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');


$req = $Conn->prepare("SELECT * FROM ndf_lignes WHERE nli_id =" . $_GET['nli']);
$req->execute();
$nli = $req->fetch();

if (!empty($nli['nli_depassement_accorde'])) {
    $nli_depassement_valide = $nli['nli_depassement_valide'] + $nli['nli_depassement_accorde'];
} else {
    $nli_depassement_valide = $nli['nli_depassement_valide'];
}

$req = $Conn->prepare("UPDATE ndf_lignes SET nli_depassement_accorde = :nli_depassement_valide, nli_depassement_valide = 0 WHERE nli_id =" . $_GET['nli']);
$req->bindValue(':nli_depassement_valide', $nli_depassement_valide);
$req->execute();

$_SESSION['message'][] = array(
    "titre" => "Note de frais",
    "type" => "success",
    "message" => "Le dépassement a été validé."
);

Header("Location: ndf.php?id=" . $_GET['id']);
die();