<?php 

// AFFICHE LA LISTE DES FACTURES DEPUIS LE TABLEAU RESULTAT
setlocale(LC_TIME, "fr");

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');

// DONNEE UTILE AU PROGRAMME

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	} 
}

// PARAMETRE GET

//conso
$agence=0;
if(isset($_GET["agence"])){
	if(!empty($_GET["agence"])){
		$agence=intval($_GET["agence"]);
	}
}
$com_type=0;
if(isset($_GET["com_type"])){
	if(!empty($_GET["com_type"])){
		$com_type=intval($_GET["com_type"]);
	}
}
$commercial=0;
if(isset($_GET["commercial"])){
	if(!empty($_GET["commercial"])){
		$commercial=intval($_GET["commercial"]);
	}
}

// periode
$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}
$mois=0;
if(isset($_GET["mois"])){
	if(!empty($_GET["mois"])){
		$mois=intval($_GET["mois"]);
	}
}

// classification
$categorie="";
if(isset($_GET["categorie"])){
	if(!empty($_GET["categorie"])){
		$categorie=$_GET["categorie"];
	}
}
$famille=0;
if(isset($_GET["famille"])){
	if(!empty($_GET["famille"])){
		$famille=intval($_GET["famille"]);
	}
}
$s_famille=0;
if(isset($_GET["s_famille"])){
	if(!empty($_GET["s_famille"])){
		$s_famille=intval($_GET["s_famille"]);
	}
}
$s_s_famille=0;
if(isset($_GET["s_s_famille"])){
	if(!empty($_GET["s_s_famille"])){
		$s_s_famille=intval($_GET["s_s_famille"]);
	}
}

$previ_type=0;
if(isset($_GET["previ_type"])){
	if(!empty($_GET["previ_type"])){
		$previ_type=intval($_GET["previ_type"]); // 1 confirme 2 option
	}
}
$vu=0;
if(isset($_GET["vu"])){
	if(!empty($_GET["vu"])){
		$vu=intval($_GET["vu"]);
	}
}


// CONTROLE D'ACCESS

if(!$_SESSION["acces"]["acc_droits"][35] AND $commercial==0){
	header("location:deconnect.php");
	die();
}elseif($agence>0){
	if($acc_agence>0 AND $agence!=$acc_agence){
		header("location:deconnect.php");
		die();
	}
}elseif(empty($agence) AND empty($commercial)){
	if(!isset($_SESSION["acces"]["acc_liste_societe"][$acc_societe . "-0"])){
		header("location:deconnect.php");
		die();
	}
}

// DONNEE PARAMETRE

if($vu==1 AND !empty($mois)){
	
	if(!empty($agence)){
		
		$sql="SELECT urp_mois FROM Agences,Utilisateurs_Rem_Periode WHERE age_intra_inter=urp_type AND urp_exercice=:exercice AND urp_periode=:mois AND age_id=:agence;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":exercice",$exercice);
		$req->bindParam(":mois",$mois);
		$req->bindParam(":agence",$agence);
		$req->execute();
		$d_periodes_mois=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($d_periodes_mois)){
			$tab_mois=array_column($d_periodes_mois,"urp_mois");
			$mois_liste=implode($tab_mois,",");
		}

	}else{
		
		$sql="SELECT urp_mois FROM Societes,Utilisateurs_Rem_Periode WHERE soc_intra_inter=urp_type AND urp_exercice=:exercice AND urp_periode=:mois AND soc_id=:societe;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":exercice",$exercice);
		$req->bindParam(":mois",$mois);
		$req->bindParam(":societe",$acc_societe);
		$req->execute();
		$d_periodes_mois=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($d_periodes_mois)){
			$tab_mois=array_column($d_periodes_mois,"urp_mois");
			$mois_liste=implode($tab_mois,",");
		}
	}
	
}

// CATEGORIE
$d_categories=array();
$sql="SELECT pca_id,pca_libelle FROM Produits_Categories ORDER BY pca_id;";
$req=$Conn->query($sql);
$d_result_cat=$req->fetchAll();
if(!empty($d_result_cat)){
	foreach($d_result_cat as $cat){
		$d_categories[$cat["pca_id"]]=$cat["pca_libelle"];
	}	
}
// Famille
$d_familles=array();
$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles  ORDER BY pfa_id;";
$req=$Conn->query($sql);
$d_result_fam=$req->fetchAll();
if(!empty($d_result_fam)){
	foreach($d_result_fam as $fam){
		$d_familles[$fam["pfa_id"]]=$fam["pfa_libelle"];
	}	
}
// Sous-Famille
$d_s_familles=array();
$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles ORDER BY psf_id;";
$req=$Conn->query($sql);
$d_result_s_fam=$req->fetchAll();
if(!empty($d_result_s_fam)){
	foreach($d_result_s_fam as $s_fam){
		$d_s_familles[$s_fam["psf_id"]]=$s_fam["psf_libelle"];
	}	
}
// Sous-Sous-Famille
$d_s_s_familles=array();
$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles ORDER BY pss_id;";
$req=$Conn->query($sql);
$d_result_s_s_fam=$req->fetchAll();
if(!empty($d_result_s_s_fam)){
	foreach($d_result_s_s_fam as $s_s_fam){
		$d_s_s_familles[$s_s_fam["pss_id"]]=$s_s_fam["pss_libelle"];
	}	
}

	// CONTSRUCTION DU TITRE
	
	$titre="Résultat";
	if($previ_type==1){
		$titre.=" prévisionnel";
	}else{
		$titre.=" prévisionnel (non confirmé)";
	}
	if($commercial>0){
		
		$sql="SELECT com_label_1,com_label_2 FROM Commerciaux WHERE com_id=" . $commercial;
		if(!$_SESSION["acces"]["acc_droits"][35]){
			$sql.=" AND com_ref_1=" . $acc_utilisateur;
		}elseif($acc_agence>0){
			$sql.=" AND com_agence=" . $acc_agence;
		}
		$sql.=";";
		$req=$ConnSoc->query($sql);
		$d_commercial=$req->fetch();
		if(!empty($d_commercial)){
			$titre.=" de " . $d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"];
		}else{
			echo("impossible d'afficher la page!");
			die();
		}
	}else{
		
		if($com_type>0){

			if($com_type==1){
				$titre.=" Vendeurs";
				
			}elseif($com_type==2){
				$titre.=" Revendeurs"; 
				
			}elseif($com_type==3){
				$titre.=" Indivis"; 
				
			}
		}
		if($agence>0){
			$sql="SELECT age_nom,age_societe FROM Agences WHERE age_id=" . $agence . ";";
			$req=$Conn->query($sql);
			$d_agence=$req->fetch();
			if(!empty($d_agence)){
				$titre.=" " . $d_agence["age_nom"];
				if($d_agence["age_societe"]!=$acc_societe){
					header("location:deconnect.php");
					die();
				}
			}	
		}else{
			$sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
			$req=$Conn->query($sql);
			$d_societe=$req->fetch();
			if(!empty($d_societe)){
				$titre.=" " . $d_societe["soc_nom"]; 
			}	
		}
	}
	
	if($mois>0){
		if($vu==1){
			$titre.= " pour la période P" . $mois . " " . $exercice;
		}else{
			if($mois<=3){
				$titre.= " pour le mois de  " . utf8_encode(strftime ("%B %Y", mktime(0, 0, 0, $mois,1 ,$exercice+1)));
			}else{
				$titre.= " pour le mois de  " . utf8_encode(strftime ("%B %Y", mktime(0, 0, 0, $mois, 1,intval($exercice))));
			}
		}
	}else{
		$titre.= " pour l'exercice " . $exercice . "/" . intval($exercice+1); 
	}


// Famille a exclure
// exemple si on affiche famille formation, il ne faut pas juste filtrer fli_famille=fomration
// mais il faut exclure les sous-éléments qui disposent de leur propre objectifs
// on part du niv le plus bas et on remonte l'arbo
$mil="";
if($categorie!="total"){
	//categorie=999 on affiche tout
	//categorie=0 est reserve pour autre
	if($s_s_famille>0){
		
		$mil.=" AND act_pro_sous_sous_famille=" . $s_s_famille;
		
	}else{
		
		$sql="SELECT cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille 
		FROM Commerciaux 
		LEFT JOIN Commerciaux_Objectifs ON (Commerciaux.com_id=Commerciaux_Objectifs.cob_commercial)
		WHERE cob_exercice=" . $exercice;
		// critere d'accès
		if(!$_SESSION["acces"]["acc_droits"][35]){
			$sql.=" AND com_ref_1=" . $acc_utilisateur;
		}elseif($acc_agence>0){
			$sql.=" AND com_agence=" . $acc_agence;
		}
		// critère de tri
		if($commercial>0){
			$sql.=" AND com_id=" . $commercial;
		}elseif($com_type>0){
			$sql.=" AND com_type=" . $com_type;
		}elseif($agence>0){
			$sql.=" AND com_agence=" . $agence;
		}
		if($categorie=="conso"){
			
			$sql.=" AND NOT cob_autre";
			
		}else{
			if($s_famille>0){
				$mil.=" AND act_pro_sous_famille=" . $s_famille;
				$sql.=" AND cob_sous_famille=" . $s_famille . " AND cob_sous_sous_famille>0";
			}elseif($famille>0){
				$mil.=" AND act_pro_famille=" . $famille;
				$sql.=" AND cob_famille=" . $famille . " AND cob_sous_famille>0";
			}elseif($categorie>0){
				//$mil.=" AND act_pro_categorie=" . $categorie;
				$sql.=" AND cob_categorie=" . $categorie . " AND cob_famille>0";
			}
		}
		$sql.=";";
		$req=$ConnSoc->query($sql);
		$d_exclut=$req->fetchAll();
		if(!empty($d_exclut)){
			foreach($d_exclut as $exclut){
				if($exclut["cob_sous_sous_famille"]>0){
					$mil.=" AND NOT act_pro_sous_sous_famille=" . $exclut["cob_sous_sous_famille"];	
				}elseif($exclut["cob_sous_famille"]>0){
					$mil.=" AND NOT act_pro_sous_famille=" . $exclut["cob_sous_famille"];	
				}elseif($exclut["cob_famille"]>0){
					$mil.=" AND NOT act_pro_famille=" . $exclut["cob_famille"];	
				}elseif($exclut["cob_categorie"]>0){
					//$mil.=" AND NOT act_categorie=" . $exclut["cob_categorie"];	
				}
			}
		}
	}
}
	// champ a selectionne

	$sql="SELECT DISTINCT act_id,act_date_deb,DATE_FORMAT(act_date_deb,'%d/%m/%Y') as act_date_deb_fr,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille
	,acl_id,acl_ca,acl_facture_ht,acl_pro_reference
	,cli_code,cli_nom
	,com_label_1,com_label_2
	,DATE_FORMAT(pda_date,'%d/%m/%Y') as act_date_fin_fr
	FROM Actions_Clients 
	INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
	LEFT OUTER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)
	LEFT OUTER JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)
	INNER JOIN Actions_Clients_Dates ON (Actions_Clients_Dates.acd_action_client=acl_id)
	INNER JOIN plannings_dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
	
	INNER JOIN ( 
		SELECT acd_action_client, MAX(pda_date) AS max_date FROM Actions_Clients_Dates,plannings_dates WHERE acd_date=pda_id GROUP BY acd_action_client ) AS groupe 
	ON Actions_Clients.acl_id = groupe.acd_action_client AND plannings_dates.pda_date = groupe.max_date
	
	WHERE NOT acl_ca_nc AND NOT act_archive AND NOT acl_archive AND NOT acl_non_fac AND pda_date>='" . $exercice . "-04-01' AND pda_date<='" . intval($exercice+1) . "-03-31'
	AND NOT acl_facture";
	if(!$_SESSION["acces"]["acc_profil"]==3){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif($acc_agence>0){
		$sql.=" AND act_agence=" . $acc_agence;
	}
	
	if($commercial>0){
		$sql.=" AND acl_commercial=" . $commercial;
	}elseif($com_type>0){
		$sql.=" AND com_type=" . $com_type;
	}elseif($agence>0){
		$sql.=" AND act_agence=" . $agence;
	}
	if($mois>0){
		if($vu==1){
			$sql.=" AND MONTH(pda_date) IN (" . $mois_liste . ")";
		}else{
			$sql.=" AND MONTH(pda_date)=" . $mois;
		}
	}
	if(!empty($previ_type)){
		if($previ_type==1){
			$sql.=" AND acl_confirme";
		}else{
			$sql.=" AND NOT acl_confirme";
		}
	}
	if($mil!=""){
		$sql.=$mil;
	}
	$sql.=" ORDER BY act_date_deb,act_id,acl_id;";
	$req = $ConnSoc->prepare($sql);	
	$req->execute();
	$actions = $req->fetchAll();
	
	
	
	// CAS PARTICULIER CONSULTATION GC
	
	if($acc_societe==4 AND ($agence==0 OR $agence==4) ){
		
		$d_commerciaux=array();
		$listeCom="";
		// on recupere les commerciaux GC pour pouvoir les afficher comme commercial sur les actions régions

		$sql="SELECT com_id,com_label_1,com_label_2,com_agence FROM Commerciaux WHERE com_agence=4";
		if(!empty($commercial)){
			$sql.=" AND com_id=" . $commercial;
		}elseif(!empty($com_type)){
			$sql.=" AND com_type=" . $com_type;
		};
		$sql.= " ORDER BY com_id;";
		$req=$ConnSoc->query($sql);
		$d_result_comm=$req->fetchAll();
		if(!empty($d_result_comm)){
			foreach($d_result_comm as $r_comm){
				$d_commerciaux[$r_comm["com_id"]]=array(
					"com_label_1" => $r_comm["com_label_1"],
					"com_label_2" => $r_comm["com_label_2"]
				);
			}
			
			if(!empty($com_type)){
				$tab_com_gc=array_column ($d_result_comm,"com_id");
				$listeCom=implode($tab_com_gc,",");
			}
		}
	
		
		// on recupere ces client sur GFC
		// on memorise les comm des maison mère pour pouvoir les afficher dans la liste
	
		$d_com_holding=array();
		
		$sql="SELECT cli_id,cli_code,cli_nom,cso_commercial FROM Clients,Clients_Societes WHERE cli_id=cso_client 
		AND cso_agence=4 AND cli_categorie=2 AND NOT cli_sous_categorie=2 AND cli_filiale_de=0";
		if(!$_SESSION["acces"]["acc_droits"][35]){
			$sql.=" AND cso_utilisateur=" . $acc_utilisateur;
		}
		if($commercial>0){
			$sql.=" AND cso_commercial=" . $commercial;
		}elseif(!empty($listeCom)){
			$sql.=" AND cso_commercial IN (" . $listeCom . ")";
		}
		$sql.=" ORDER BY cli_code;";
		$req=$Conn->query($sql);
		$d_mm_gc=$req->fetchAll();
		if(!empty($d_mm_gc)){
			$tab_mm_gc=array_column ($d_mm_gc,"cli_id");
			$liste_mm_gc=implode($tab_mm_gc,",");
			foreach($d_mm_gc as $mm){
				$d_com_holding[$mm["cli_id"]]=$mm["cso_commercial"];
			}
		}
			
			
			
		if(isset($liste_mm_gc)){
			
			$d_actions_gc=array();
			
			$sql="SELECT soc_id,soc_nom FROM Societes WHERE NOT soc_id=4 AND NOT soc_archive ORDER BY soc_id;";
			$req=$Conn->query($sql);
			$d_societe_region=$req->fetchAll();
			if(!empty($d_societe_region)){
				
				foreach($d_societe_region as $region){
					
					$ConnFct=connexion_fct($region["soc_id"]);
					
					
					$sql="SELECT DISTINCT act_id,DATE_FORMAT(act_date_deb,'%d/%m/%Y') as act_date_deb_fr,act_date_deb,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille
					,acl_id,acl_ca_gfc,acl_facture_gfc_ht,acl_pro_reference
					,cli_code,cli_nom,cli_id,cli_filiale_de					
					,DATE_FORMAT(pda_date,'%d/%m/%Y') as act_date_fin_fr
					
					FROM Actions_Clients 
					INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
					INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)										
					INNER JOIN Actions_Clients_Dates ON (Actions_Clients_Dates.acd_action_client=acl_id)
					INNER JOIN plannings_dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
					
					INNER JOIN ( 
						SELECT acd_action_client, MAX(pda_date) AS max_date FROM Actions_Clients_Dates,plannings_dates WHERE acd_date=pda_id GROUP BY acd_action_client ) AS groupe 
					ON Actions_Clients.acl_id = groupe.acd_action_client AND plannings_dates.pda_date = groupe.max_date
					
					WHERE NOT acl_ca_nc AND NOT act_archive AND NOT acl_archive AND NOT acl_non_fac_gfc AND pda_date>='" . $exercice . "-04-01' AND pda_date<='" . intval($exercice+1) . "-03-31'						
					AND NOT acl_facture_gfc AND (cli_id IN (" . $liste_mm_gc . ") OR  cli_filiale_de IN (" . $liste_mm_gc . ")) AND acl_facturation=1";
					if(!$_SESSION["acces"]["acc_droits"][35]){
						$sql.=" AND com_ref_1=" . $acc_utilisateur;
					}
					if($mois>0){
						$sql.=" AND MONTH(pda_date)=" . $mois;
					}
					if(!empty($previ_type)){
						if($previ_type==1){
							$sql.=" AND acl_confirme";
						}else{
							$sql.=" AND NOT acl_confirme";
						}
					}
					if($mil!=""){
						$sql.=$mil;
					}
					$sql.=" ORDER BY act_date_deb,act_id,acl_id;";					
					$req = $ConnFct->query($sql);	
					$resultat_action_gc = $req->fetchAll();
					if(!empty($resultat_action_gc)){
						
						/*var_dump($liste_mm_gc);
						echo("<pre>");
							print_r($resultat_action_gc);
						echo("</pre>");
						die();*/

						foreach($resultat_action_gc as $gc){
							
							if(!empty($gc["cli_filiale_de"])){
								$com_mm_id=$d_com_holding[$gc["cli_filiale_de"]];
							}else{
								$com_mm_id=$d_com_holding[$gc["cli_id"]];
							}
							if(!empty($d_commerciaux[$com_mm_id])){
								$com_gc_label_1=$d_commerciaux[$com_mm_id]["com_label_1"];
								$com_gc_label_2=$d_commerciaux[$com_mm_id]["com_label_2"];
							}
							
							$actions[]=array(
								"act_id" => $gc["act_id"],
								"act_date_deb_fr" => $gc["act_date_deb_fr"],
								"act_pro_famille" => $gc["act_pro_famille"],
								"act_pro_sous_famille" => $gc["act_pro_sous_famille"],
								"act_pro_sous_sous_famille" => $gc["act_pro_sous_sous_famille"],
								"acl_id" => $gc["acl_id"],
								"acl_ca" => $gc["acl_ca_gfc"],
								"acl_facture_ht" => $gc["acl_facture_gfc_ht"],
								"acl_pro_reference" => $gc["acl_pro_reference"],
								"cli_code" => $gc["cli_code"],
								"cli_nom" => $gc["cli_nom"],
								"com_label_1" => $com_gc_label_1,
								"com_label_2" => $com_gc_label_2,								
								"act_date_fin_fr" => $gc["act_date_fin_fr"],
								"soc_id" => $region["soc_id"],
								"soc_nom" => $region["soc_nom"]
							);
						}							
					}
				}
			}
		}
	}

	/*echo("<pre>");
		print_r($factures);
	echo("</pre>");
	die();*/
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">

		<?php 			if(!empty($actions)){ 
							$total_ht=0; ?>
							<h1><?=$titre?></h1>
							<div class="table-responsive">
								<table class="table table-striped" >
									<thead>
										<tr class="dark2" >
									<?php	$tot_cols=9;
											if(($acc_societe==4 AND $agence==0) OR $agence==4){ 
												$tot_cols=10; ?>
												<th>Société</th>
									<?php	} ?>
											<th>Action</th> 
											<th>Client</th> 
											<th>Commercial</th>																						
											<th>Famille</th>
											<th>Sous-Famille</th>
											<th>Sous-Sous-Famille</th>
											<th>Produit</th>
											
											<th>Date de début</th>
											<th>Date de fin</th>
											<th>H.T.</th>																																						
										</tr>
									</thead>
									<tbody>
							<?php		foreach($actions as $f){
								
											$ca_previ=$f['acl_ca']-$f['acl_facture_ht'];

											$total_ht+=$ca_previ;
											
											?>
											<tr>
										<?php	if(($acc_societe==4 AND $agence==0) OR $agence==4){ ?>
													<td>
										<?php			if(!empty($f['soc_nom'])){
															echo($f['soc_nom']);
														}else{
															echo("GFC");
														} ?>
													</td>
										<?php	} ?>		
												<td><?= $f['act_id'] . "-" . $f['acl_id']?></td>
												<td><?= $f['cli_code']?></td>
												<td><?= $f['com_label_1'] ?></td>											
												
												<td>
										<?php		if(!empty($f['act_pro_famille'])){
														echo($d_familles[$f['act_pro_famille']]);
													}else{
														echo("&nbsp;");
													} ?>
												</td>
												<td>
										<?php		if(!empty($f['act_pro_sous_famille'])){
														echo($d_s_familles[$f['act_pro_sous_famille']]);
													}else{
														echo("&nbsp;");
													} ?>
												</td>
												<td>
										<?php		if(!empty($f['act_pro_sous_sous_famille'])){
														echo($d_s_s_familles[$f['act_pro_sous_sous_famille']]);
													}else{
														echo("&nbsp;");
													} ?>
												</td>
												<td><?= $f['acl_pro_reference'] ?></td>
												
												<td><?= $f['act_date_deb_fr'] ?></td>
												<td><?= $f['act_date_fin_fr'] ?></td>
												<td class="text-right" ><?= $ca_previ?></td>																		
											</tr>
				<?php					}  ?>
									</tbody>
									<tfoot>
										<tr>
											<th colspan="<?=$tot_cols?>" class="text-right" >Total :</th>
											<td class="text-right" ><?=$total_ht?></td>											
										</tr>
									</tfoot>
								</table>
							</div>
		<?php 			}else{ ?>
							<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-warning" style="border-radius:0px;">
									Aucune action correspondant à votre recherche.
								</div>
							</div>
		<?php			} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="commercial_result.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&com_type=<?=$com_type?>$agence=<?=$agence?>&exercice=<?=$exercice?>&vu=<?=$vu?>" class="btn btn-default btn-sm" role="button">
							<i class="fa fa-left"></i> Retour
						</a>	</div>
					<div class="col-xs-6 footer-middle">&nbsp;</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
			
			});						
		</script>
	</body>
</html>
