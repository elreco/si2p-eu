<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_upload.php');

$erreur="";
$erreur_import="";
$alert_import="";

$rapport_erreur="";
$rapport_alert="";

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
  $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
  $acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
	$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
}

// TRAITEMNT DU FORME

$nom="";
if(!empty($_POST["libelle"])){
  $nom=$_POST["libelle"];
}

if($acc_agence>0){
	$cli_agence=$acc_agence;
}elseif(!empty($_POST["agence"])){
	$cli_agence=intval($_POST["agence"]);
}else{
	$cli_agence=0;
}

$cli_commercial=0;
if(!empty($_POST["commercial"])){
	$cli_commercial=intval($_POST["commercial"]);
}

/*$cli_prescripteur=0;
if(!empty($_POST["prescripteur"])){
	$cli_prescripteur=intval($_POST["prescripteur"]);
}*/

// on cherche le prescripteur qui correspond à l'import de fichier

$sql="SELECT cpr_id FROM Clients_Prescripteurs WHERE cpr_import AND NOT cpr_archive;";
$req=$Conn->query($sql);
$d_prescripteur=$req->fetchAll();
if(!empty($d_prescripteur)){
	if(count($d_prescripteur)!=1){
		$erreur="La source n'a pas pu être identifiée. Merci de contacter l'administrateur.";
	}
}else{
	$erreur="La source n'a pas pu être identifiée. Merci de contacter l'administrateur.";
}

if(empty($erreur)){
	
	if($acc_societe>0 AND !empty($nom)){
		
		$dossier="suspects/" . $acc_societe . "/";
		if(!file_exists("documents/" . $dossier)){
			mkdir("documents/" . $dossier, 0777, true);
		}
		
		$extension=array("csv");
		
		$taille=2097152999999;
		
		$result_upload=upload("fichier",$dossier,$nom,$taille,$extension,1);
		if($result_upload==0){
			
			// LE FICHIER EST CHARGER
			
			$cli_utilisateur=0;
			
			
			if($cli_commercial>0){
				$sql="SELECT * FROM Commerciaux WHERE com_id = ". $cli_commercial;
				$req=$ConnSoc->query($sql);
				$d_commercial=$req->fetch();
				if(!empty($d_commercial)){	
					$cli_utilisateur=$d_commercial["com_ref_1"];
				}
			}
			$sql="SELECT sim_id FROM Suspects_Imports WHERE sim_libelle=:nom;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":nom",$nom);
			$req->execute();
			$d_import=$req->fetch();
			if(!empty($d_import)){		
				$import=$d_import["sim_id"];
			}else{
				
				$sql="INSERT INTO Suspects_Imports (sim_libelle, sim_utilisateur,sim_date) VALUES(:sim_libelle, :sim_utilisateur, NOW());";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":sim_libelle",$nom);
				$req->bindParam(":sim_utilisateur",$acc_utilisateur);
				$req->execute();
				$import=$ConnSoc->lastInsertId();
			}
			
			if($import>0){
			
				$row=0;
				
				$src=$_SERVER["DOCUMENT_ROOT"]. "/documents/suspects/" . $acc_societe . "/" . $nom . ".csv";
				//$src=$_SERVER["DOCUMENT_ROOT"]. "/documents/suspects/" . $acc_societe . "/a.csv";
				$handle = fopen($src, "r");
				if ($handle!== FALSE){
					
					
					// doublon contact
					$sql_con="SELECT con_id FROM Contacts WHERE con_mail=:con_mail;";
					$req_con=$Conn->prepare($sql_con);
					
					$sql_con_soc="SELECT sco_id FROM Suspects_Contacts WHERE sco_mail=:sco_mail;";
					$req_con_soc=$ConnSoc->prepare($sql_con_soc);
					
					//doublon siret
					
					$sql_siret="SELECT cli_id FROM Clients INNER JOIN Adresses ON (Clients.cli_id=Adresses.adr_ref_id AND adr_ref=1) WHERE cli_siren=:cli_siren AND adr_siret=:adr_siret;";
					$req_siret=$Conn->prepare($sql_siret);
					
					$sql_siret_soc="SELECT sus_id FROM Suspects INNER JOIN Suspects_Adresses ON (Suspects.sus_id=Suspects_Adresses.sad_ref_id AND sad_ref=1) WHERE sus_siren=:sus_siren AND sad_siret=:sad_siret;";
					$req_siret_soc=$ConnSoc->prepare($sql_siret_soc);
					
					////CODE APE
					$sql_get_ape="SELECT ape_id FROM Ape WHERE ape_code=:ape_code;";
					$req_get_ape=$Conn->prepare($sql_get_ape);
					
					// Add suspect
					
					$sql_add_sus="INSERT INTO Suspects (
					sus_code,sus_nom,sus_agence,sus_commercial,sus_utilisateur,sus_categorie,sus_sous_categorie,sus_date_creation,sus_uti_creation,sus_siren,sus_ident_tva
					,sus_classification,sus_classification_type,sus_classification_categorie,sus_ape,sus_soc_type,sus_capital,sus_immat_lieu,sus_import,sus_prescripteur,sus_prescripteur_info)
					VALUES (:sus_code,:sus_nom, :sus_agence, :sus_commercial, :sus_utilisateur, :sus_categorie, :sus_sous_categorie, :sus_date_creation, :sus_uti_creation, :sus_siren, :sus_ident_tva
					, :sus_classification, :sus_classification_type, :sus_classification_categorie, :sus_ape, :sus_soc_type, :sus_capital, :sus_immat_lieu,:sus_import,:sus_prescripteur,:sus_prescripteur_info);";
					$req_add_sus=$ConnSoc->prepare($sql_add_sus);
					
					// ADRESSE
					$sql_add_adr="INSERT INTO Suspects_Adresses (sad_ref,sad_ref_id,sad_type,sad_nom,sad_service,sad_ad1,sad_ad2,sad_ad3,sad_cp,sad_ville,sad_defaut,sad_siret,sad_geo)
					VALUES (1,:sad_ref_id,:sad_type,:sad_nom,:sad_service,:sad_ad1,:sad_ad2,:sad_ad3,:sad_cp,:sad_ville,:sad_defaut,:sad_siret,1);";
					$req_add_adr=$ConnSoc->prepare($sql_add_adr);
					
					// CONTACT
					$sql_add_con="INSERT INTO Suspects_Contacts (sco_ref,sco_ref_id,sco_fonction_nom,sco_titre,sco_prenom,sco_nom,sco_tel,sco_portable,sco_fax,sco_mail,sco_comment)
					VALUES (1,:sco_ref_id,:sco_fonction_nom,:sco_titre,:sco_prenom,:sco_nom,:sco_tel,:sco_portable,:sco_fax,:sco_mail,:sco_comment);";
					$req_add_con=$ConnSoc->prepare($sql_add_con);

					// CORRESPONDANCE
					$sql_add_cor="INSERT INTO Suspects_Correspondances (sco_date,sco_commentaire, sco_utilisateur, sco_suspect)
					VALUES (NOW(),:sco_commentaire, 0, :sco_suspect);";
					$req_add_cor=$ConnSoc->prepare($sql_add_cor);
					
					// ADRESSE CONTACTS
					
					$sql_con_adr="INSERT INTO Suspects_Adresses_Contacts (sac_adresse,sac_contact,sac_defaut) VALUES (:sac_adresse,:sac_contact,:sac_defaut);";
					$req_con_adr=$ConnSoc->prepare($sql_con_adr);
					
					// UPDATE SUSPECT
					$sql_up_sus="UPDATE Suspects SET 
					sus_adresse=:sus_adresse,
					sus_adr_nom=:sus_adr_nom,
					sus_adr_service=:sus_adr_service,
					sus_adr_ad1=:sus_adr_ad1,
					sus_adr_ad2=:sus_adr_ad2,
					sus_adr_ad3=:sus_adr_ad3,
					sus_adr_cp=:sus_adr_cp,
					sus_adr_ville=:sus_adr_ville 
					
					WHERE sus_id=:suspect;";
					
					/* sus_contact=:sus_contact,										
					sus_con_prenom=:sus_con_prenom,
					sus_con_nom=:sus_con_nom,											
					sus_con_fct_nom=:sus_con_fct_nom,
					sus_con_tel=:sus_con_tel,
					sus_con_mail=:sus_con_mail */
					$req_up_sus=$ConnSoc->prepare($sql_up_sus);
					// UPDATE SUSPECT
					$req_up_con="UPDATE Suspects SET 
					sus_contact=:sus_contact,										
					sus_con_prenom=:sus_con_prenom,
					sus_con_nom=:sus_con_nom,											
					sus_con_fct_nom=:sus_con_fct_nom,
					sus_con_tel=:sus_con_tel,
					sus_con_mail=:sus_con_mail 
					
					WHERE sus_id=:suspect;";
					
					/*  */
					$req_up_con=$ConnSoc->prepare($req_up_con);
								
					while (($data = fgetcsv($handle, 1000, ";")) !== FALSE){
						$erreur_import="";
						if(!empty($erreur)){					
							break;
						}else{

							$row++;
							
							if($row==1){
								
								$cle=array(
									"CODE" =>"",
									"NOM" =>"",	
									"AGENCE" =>"",	
									"CATEGORIE" =>"",
									"SOUS_CATEGORIE" =>"",	
									"INTERVENTION_SERVICE" =>"",	
									"INTERVENTION_AD1" =>"",	
									"INTERVENTION_AD2" =>"",	
									"INTERVENTION_AD3" =>"",	
									"INTERVENTION_CP" =>"",	
									"INTERVENTION_VILLE" =>"",	
									"FACTURATION_NOM" =>"",	
									"FACTURATION_SERVICE" =>"",	
									"FACTURATION_AD1" =>"",	
									"FACTURATION_AD2" =>"",	
									"FACTURATION_AD3" =>"",	
									"FACTURATION_CP" =>"",
									"FACTURATION_VILLE" =>"",	
									"FACTURATION_SIRET" =>"",	
									"CONTACT_FONCTION" =>"",	
									"CONTACT_NOM" =>"",	
									"CONTACT_PRENOM" =>"",
									"CONTACT_TEL" =>"",	
									"CONTACT_FAX" =>"",	
									"CONTACT_MOBILE" =>"",	
									"CONTACT_MAIL" =>"",
									"CONTACT_TITRE_2" => "",	
									"CONTACT_FONCTION_2" =>"",	
									"CONTACT_NOM_2" =>"",	
									"CONTACT_PRENOM_2" =>"",
									"CONTACT_TEL_2" =>"",	
									"CONTACT_FAX_2" =>"",	
									"CONTACT_MOBILE_2" =>"",	
									"CONTACT_MAIL_2" =>"",	
									"CONTACT_COM_2" =>"",
									"CONTACT_COM_2_BIS" =>"",
									"CLASSIFICATION" =>"",	
									"CLASSIFICATION_TYPE" =>"",						
									"CLASSIFICATION_CATEGORIE" =>"",	
									"CODE_APE" =>"",	
									"CAPITAL" =>"",	
									"FORME_JURIDIQUE" =>"",	
									"NUM_TVA" =>"",
									"LIEU_IMMAT" =>"",
									"SIREN" => "",
									"DATE_CREATION" => "",
									"CORRESPONDANCE_TEXTE" => ""
								);
								
								foreach($data as $key => $v){
									if(isset($cle[$v])){
										$cle[$v]=$key;
									}else{
										$erreur="Colonne " . $v . " incompatible";
										break;
									}
								}	
								
							}else{
								
								// CHAMP OBLIGATOIRE
								$cli_nom="";
								if($cle["NOM"]!==""){							
									if(!empty($data[$cle["NOM"]])){
										$cli_nom=utf8_encode($data[$cle["NOM"]]);
									}else{
										$erreur_import="Ligne " . $row . " : Le nom du client n'est pas renseigné<br/>";	
									}
								}

								// CONTROLE DOUBLON MAIL
								if(empty($erreur_import)){

									$contact_mail="";
									if($cle["CONTACT_MAIL"]!==""){
										if(!empty($data[$cle["CONTACT_MAIL"]])){
											if(filter_var($data[$cle["CONTACT_MAIL"]], FILTER_VALIDATE_EMAIL)){
												$contact_mail=$data[$cle["CONTACT_MAIL"]];
											}
										}
									}
									
									$contact_mail_2="";
									if($cle["CONTACT_MAIL_2"]!==""){
										if(!empty($data[$cle["CONTACT_MAIL_2"]])){
											if(filter_var($data[$cle["CONTACT_MAIL_2"]], FILTER_VALIDATE_EMAIL)){
												$contact_mail_2=$data[$cle["CONTACT_MAIL_2"]];
											}
										}
									}

									if(!empty($contact_mail)){
										
										$req_con->bindParam(":con_mail",$contact_mail);
										$req_con->execute();
										$d_contact_existe=$req_con->fetchAll();
										if(!empty($d_contact_existe)){
											$alert_import="Ligne " . $row . " : le mail " . $contact_mail . "existe déjà dans la base client<br/>";
											$contact_mail="";									
										}
									}
									if(!empty($contact_mail)){
										
										$req_con_soc->bindParam(":sco_mail",$contact_mail);
										$req_con_soc->execute();
										$d_contact_existe=$req_con_soc->fetchAll();
										if(!empty($d_contact_existe)){
											$alert_import="Ligne " . $row . " : le mail " . $contact_mail . "existe déjà dans la base suspect<br/>";
											$contact_mail="";									
										}
									}
									
									if(!empty($contact_mail_2)){
										$req_con->bindParam(":con_mail",$contact_mail_2);
										$req_con->execute();
										$d_contact_existe=$req_con->fetchAll();
										if(!empty($d_contact_existe)){
											$alert_import="Ligne " . $row . " : le mail " . $contact_mail_2 . "existe déjà dans la base client<br/>";
											$contact_mail_2="";									
										}
									}
									if(!empty($contact_mail_2)){
										
										$req_con_soc->bindParam(":sco_mail",$contact_mail_2);
										$req_con_soc->execute();
										$d_contact_existe=$req_con_soc->fetchAll();
										if(!empty($d_contact_existe)){
											$alert_import="Ligne " . $row . " : le mail " . $contact_mail_2 . "existe déjà dans la base suspect<br/>";
											$contact_mail_2="";									
										}
									}
									
									/* if(empty($contact_mail) AND empty($contact_mail_2)){
										$alert_import="";
										$erreur_import.="Ligne " . $row . " : Les contacts existe déjà.<br/>";				
									} */
									
								}
									
								// CONTROLE SIRET
								if(empty($erreur_import)){
									$cli_siret="";
									if($cle["FACTURATION_SIRET"]!==""){
										if(!empty($data[$cle["FACTURATION_SIRET"]])){
											$cli_siret=$data[$cle["FACTURATION_SIRET"]];
											$cli_siret=trim($cli_siret);
											$cli_siret=str_replace("-","",$cli_siret);
											$cli_siret=str_replace(".","",$cli_siret);
											$cli_siret=str_replace(" ","",$cli_siret);
											$cli_siret=str_replace("'","",$cli_siret);
											$cli_siret=str_replace('"',"",$cli_siret);
											
											
											if(strlen($cli_siret)!=14){
												$erreur_import.="Ligne " . $row . " : Format de siret incorrect.<br/>";		
											}else{
												
												$cli_siren=substr($cli_siret,0,9);
												$adr_siret=substr($cli_siret,9);					
												$req_siret->bindParam(":cli_siren",$cli_siren);
												$req_siret->bindParam(":adr_siret",$adr_siret);
												$req_siret->execute();
												$d_siret_existe=$req_siret->fetchAll();
												if(!empty($d_siret_existe)){
													$erreur_import="Ligne " . $row . " : le siret " . $cli_siret . "existe déjà dans la base client<br/>";																	
												}
												
												if(empty($erreur_import)){
													$req_siret_soc->bindParam(":sus_siren",$cli_siren);
													$req_siret_soc->bindParam(":sad_siret",$adr_siret);
													$req_siret_soc->execute();
													$d_siret_existe=$req_siret_soc->fetchAll();
													if(!empty($d_siret_existe)){
														$erreur_import="Ligne " . $row . " : le siret " . $cli_siret . "existe déjà dans la base suspect<br/>";																	
													}
												}
											}
										}
									}
								}
								
								// IMPORTATION
								
								if(empty($erreur_import)){
									
									// controle des champs avec format défini
									
									$cli_categorie=0;
									if($cle["CATEGORIE"]!==""){
										if(!empty($data[$cle["CATEGORIE"]])){
											switch ($data[$cle["CATEGORIE"]]) {
												case "REG":
													$cli_categorie=1;
													break;
												case "DO":
													$cli_categorie=4;
													break;
												case "P":
													$cli_categorie=3;
													break;
											}
											if($cli_categorie==0){
												$alert_import.="Ligne " . $row . " : Format CATEGORIE incorrect.<br/>";
											}
										}
									}
									
									$cli_sous_categorie=0;
									if($cle["SOUS_CATEGORIE"]!==""){
										if($cli_categorie==4){
											if(!empty($data[$cle["SOUS_CATEGORIE"]])){
												switch ($data[$cle["SOUS_CATEGORIE"]]) {
													case "INTERIM":
														$cli_sous_categorie=4;
														break;
													case "OPCA":
														$cli_sous_categorie=5;
														break;
													case "RLOC":
														$cli_sous_categorie=7;
														break;
													case "POLE":
														$cli_sous_categorie=6;
														break;
													case "ROF":
														$cli_sous_categorie=8;
														break;
													case "SECU":
														$cli_sous_categorie=9;
														break;
												}
												if($cli_categorie==0){
													$alert_import.="Ligne " . $row . " : Format CATEGORIE incorrect.<br/>";
												}
											}
										}
									}
									
								
									$cli_classification=0;
									if($cle["CLASSIFICATION"]!==""){
										if(!empty($data[$cle["CLASSIFICATION"]])){
											switch($data[$cle["CLASSIFICATION"]]){
												case "ERP":
													$cli_classification=1;
													break;
												case "ERT":
													$cli_classification=2;
													break;									
											}
											if($cli_classification==0){
												$alert_import.="Ligne " . $row . " : Format CLASSIFICATION incorrect.<br/>";
											}
										}
									}
										
									$cli_classification_type=0;
									$cli_classification_categorie=0;
									
									if($cli_classification==1){
										if($cle["CLASSIFICATION_TYPE"]!==""){								
											if(!empty($data[$cle["CLASSIFICATION_TYPE"]])){
												switch($data[$cle["CLASSIFICATION_TYPE"]]){
													case "J":
														$cli_classification_type=1;
														break;
													case "L":
														$cli_classification_type=2;
														break;
													case "N":
														$cli_classification_type=3;
														break;
													case "M":
														$cli_classification_type=4;
														break;
													case "O":
														$cli_classification_type=5;
														break;
													case "P":
														$cli_classification_type=6;
														break;
													case "R":
														$cli_classification_type=7;
														break;
													case "S":
														$cli_classification_type=8;
														break;
													case "T":
														$cli_classification_type=9;
														break;
													case "U":
														$cli_classification_type=10;
														break;
													case "V":
														$cli_classification_type=11;
														break;
													case "W":
														$cli_classification_type=12;
														break;
													case "X":
														$cli_classification_type=13;
														break;
													case "Y":
														$cli_classification_type=14;
														break;
													case "Z":
														$cli_classification_type=15;
														break;
													case "GA":
														$cli_classification_type=16;
														break;
													case "EF":
														$cli_sous_cacli_classification_typetegorie=17;
														break;
													case "SG":
														$cli_classification_type=18;
														break;
													case "PS":
														$cli_classification_type=19;
														break;
													case "OA":
														$cli_classification_type=20;
														break;
													case "CTS":
														$cli_classification_type=21;
														break;
													case "PA":
														$cli_classification_type=22;
														break;
													case "PRISON":
														$cli_classification_type=23;
														break;
												}
												if(empty($cli_classification_type)){
													$alert_import.="Ligne " . $row . " : Format CLASSIFICATION_TYPE incorrect.<br/>";
												}											
											}
										}

										if($cle["CLASSIFICATION_CATEGORIE"]!==""){	
											if(!empty($data[$cle["CLASSIFICATION_CATEGORIE"]])){									
												switch($data[$cle["CLASSIFICATION_CATEGORIE"]]){
													case "CAT1":
														$cli_classification_categorie=1;
														break;
													case "CAT2":
														$cli_classification_categorie=2;
														break;
													case "CAT3":
														$cli_classification_categorie=3;
														break;
													case "CAT4":
														$cli_classification_categorie=4;
														break;
													case "CAT5":
														$cli_classification_categorie=5;
														break;
												
												}
												if($cli_classification_categorie==0){
													$alert_import.="Ligne " . $row . " : Format CLASSIFICATION_CATEGORIE incorrect.<br/>";
												}
											}
										}
									}
									
									if($cli_classification==2){
										if($cle["CLASSIFICATION_CATEGORIE"]!==""){	
											if(!empty($data[$cle["CLASSIFICATION_CATEGORIE"]])){				
												switch($data[$cle["CLASSIFICATION_CATEGORIE"]]){
													case "50":
														$cli_classification_categorie=6;
														break;
													case "100":
														$cli_classification_categorie=7;
														break;
													case "500":
														$cli_classification_categorie=10;
														break;
													case "1000":
														$cli_classification_categorie=8;
													case "9999":
														$cli_classification_categorie=9;									
														break;
												}
												if($cli_classification_categorie==0){
													$alert_import.="Ligne " . $row . " : Format CLASSIFICATION_CATEGORIE incorrect.<br/>";
												}
											}
										}
									}
								

									// REQUETTE COMPLEMENTAIRE
									
									$cli_ape=0;
									if($cle["CODE_APE"]!==""){	
										if(!empty($data[$cle["CODE_APE"]])){								
											$req_get_ape->bindParam("ape_code",$data[$cle["CODE_APE"]]);
											$req_get_ape->execute();
											$d_ape=$req_get_ape->fetchAll();
											if(!empty($d_ape)){
												$cli_ape=$d_ape[0]["ape_id"];
											}else{
												$alert_import.="Ligne " . $row . " : Format APE incorrect.<br/>";
											}
										}
									}

									// autre variable -> on verifie que la cle existe
									
									$cli_code="";
									if($cle["CODE"]!==""){
										if(!empty($data[$cle["CODE"]])){		
											$cli_code=utf8_encode($data[$cle["CODE"]]);
										}
									}
									if(empty($cli_code)){
										$cli_code=$import . "-" . $row;
									}
									
									if($cle["NUM_TVA"]!==""){
										$cli_ident_tva="";
										if(!empty($data[$cle["NUM_TVA"]])){		
											$cli_ident_tva=$data[$cle["NUM_TVA"]];
										}
									}
									if($cle["FORME_JURIDIQUE"]!==""){
										$cli_soc_type="";
										if(!empty($data[$cle["FORME_JURIDIQUE"]])){		
											$cli_soc_type=utf8_encode($data[$cle["FORME_JURIDIQUE"]]);
										}
									}
									if($cle["CAPITAL"]!==""){
										$cli_capital="";
										if(!empty($data[$cle["CAPITAL"]])){		
											$cli_capital=$data[$cle["CAPITAL"]];
										}
									}
									if($cle["LIEU_IMMAT"]!==""){
										$cli_immat_lieu="";
										if(!empty($data[$cle["LIEU_IMMAT"]])){		
											$cli_immat_lieu=$data[$cle["LIEU_IMMAT"]];
										}
									}
									$cli_date_creation="";
									if($cle["DATE_CREATION"]!==""){
										if(!empty($data[$cle["DATE_CREATION"]])){
											$cli_date_creation= convert_date_sql($data[$cle["DATE_CREATION"]]);
										}else{
											$cli_date_creation=date("Y-m-d");
										}
									}else{
										$cli_date_creation=date("Y-m-d");
									}
									// JE REGARDE SI LE SUSPECT EXISTE DEJA DANS LA BOUCLE
									$nouveau = 0;
									if(empty($controle_code[$cli_code])){
										$req_add_sus=$ConnSoc->prepare($sql_add_sus);
										$req_add_sus->bindParam(":sus_code",$cli_code);
										$req_add_sus->bindParam(":sus_nom",$cli_nom);
										$req_add_sus->bindParam(":sus_agence",$cli_agence);
										$req_add_sus->bindParam(":sus_commercial",$cli_commercial);
										$req_add_sus->bindParam(":sus_utilisateur",$cli_utilisateur);
										$req_add_sus->bindParam(":sus_categorie",$cli_categorie);
										$req_add_sus->bindParam(":sus_sous_categorie",$cli_sous_categorie);									
										$req_add_sus->bindParam(":sus_uti_creation",$acc_utilisateur);
										$req_add_sus->bindParam(":sus_siren",$cli_siren);
										$req_add_sus->bindParam(":sus_ident_tva",$cli_ident_tva);
										$req_add_sus->bindParam(":sus_classification",$cli_classification);
										$req_add_sus->bindParam(":sus_classification_type",$cli_classification_type);
										$req_add_sus->bindParam(":sus_classification_categorie",$cli_classification_categorie);
										$req_add_sus->bindParam(":sus_ape",$cli_ape);
										$req_add_sus->bindParam(":sus_soc_type",$cli_soc_type);
										$req_add_sus->bindParam(":sus_capital",$cli_capital);
										$req_add_sus->bindParam(":sus_immat_lieu",$cli_immat_lieu);
										$req_add_sus->bindParam(":sus_import",$import);
										$req_add_sus->bindParam(":sus_date_creation",$cli_date_creation);
										$req_add_sus->bindParam(":sus_prescripteur",$d_prescripteur[0]["cpr_id"]);
										$req_add_sus->bindParam(":sus_prescripteur_info",$nom);
										$req_add_sus->execute();
										$suspect=$ConnSoc->lastInsertId();
										$controle_code[$cli_code] = $suspect;
										$nouveau = 1;
									}else{
										
										$suspect = $controle_code[$cli_code];
										
									}

									
									// NOUVEAU
									if($suspect>0 AND $nouveau == 1){
										
										$adr_id=array();
										
										
										$sad_nom="";

										$fac_adr_ok = 0;

										if($cle["FACTURATION_NOM"]!==""){
											if(!empty($data[$cle["FACTURATION_NOM"]])){		
												$sad_nom=utf8_encode($data[$cle["FACTURATION_NOM"]]);
											}
										}

										$sad_service="";
										if($cle["FACTURATION_SERVICE"]!==""){
											if(!empty($data[$cle["FACTURATION_SERVICE"]])){		
												$sad_service=utf8_encode($data[$cle["FACTURATION_SERVICE"]]);
											}
										}
											
										$sad_ad1="";
										if($cle["FACTURATION_AD1"]!==""){
											if(!empty($data[$cle["FACTURATION_AD1"]])){		
												$sad_ad1=utf8_encode($data[$cle["FACTURATION_AD1"]]);
											}
										}
										$sad_ad2="";
										if($cle["FACTURATION_AD2"]!==""){
											if(!empty($data[$cle["FACTURATION_AD2"]])){		
												$sad_ad2=utf8_encode($data[$cle["FACTURATION_AD2"]]);
											}
										}
										$sad_ad3="";
										if($cle["FACTURATION_AD3"]!==""){
											if(!empty($data[$cle["FACTURATION_AD3"]])){		
												$sad_ad3=utf8_encode($data[$cle["FACTURATION_AD3"]]);
											}
										}
										$sad_cp="";
										if($cle["FACTURATION_CP"]!==""){
											if(!empty($data[$cle["FACTURATION_CP"]])){		
												$sad_cp=utf8_encode($data[$cle["FACTURATION_CP"]]);
											}
										}
										$sad_ville="";
										if($cle["FACTURATION_VILLE"]!==""){
											if(!empty($data[$cle["FACTURATION_VILLE"]])){		
												$sad_ville=utf8_encode($data[$cle["FACTURATION_VILLE"]]);
											}
										}
										
										if(!empty($sad_cp) AND !empty($sad_ville)){
											$fac_adr_ok = 1;
											
											$req_add_adr->bindParam(":sad_ref_id",$suspect);
											$req_add_adr->bindValue(":sad_type",2);
											$req_add_adr->bindParam(":sad_nom",$sad_nom);
											$req_add_adr->bindParam(":sad_service",$sad_service);
											$req_add_adr->bindParam(":sad_ad1",$sad_ad1);
											$req_add_adr->bindParam(":sad_ad2",$sad_ad2);									
											$req_add_adr->bindParam(":sad_ad3",$sad_ad3);
											$req_add_adr->bindParam(":sad_cp",$sad_cp);
											$req_add_adr->bindParam(":sad_ville",$sad_ville);
											$req_add_adr->bindParam(":sad_siret",$adr_siret);	
											$req_add_adr->bindValue(":sad_defaut",1);										
											$req_add_adr->execute();
											$adresse=$ConnSoc->lastInsertId();
											
											if($adresse > 0){
												$adr_def = array(
													"cli_adresse" => $adresse,
													"cli_nom" => $cli_nom,
													"cli_service" => $sad_service,
													"cli_ad1" => $sad_ad1,
													"cli_ad2" => $sad_ad2,
													"cli_ad3" => $sad_ad3,
													"cli_cp" => $sad_cp,
													"cli_ville" => $sad_ville,
		
												);
											}
											
										}
										
										$sad_service="";
										if($cle["INTERVENTION_SERVICE"]!==""){
											if(!empty($data[$cle["INTERVENTION_SERVICE"]])){		
												$sad_service=utf8_encode($data[$cle["INTERVENTION_SERVICE"]]);
											}
										}
											
										$sad_ad1="";
										if($cle["INTERVENTION_AD1"]!==""){
											if(!empty($data[$cle["INTERVENTION_AD1"]])){		
												$sad_ad1=utf8_encode($data[$cle["INTERVENTION_AD1"]]);
											}
										}
										$sad_ad2="";
										if($cle["INTERVENTION_AD2"]!==""){
											if(!empty($data[$cle["INTERVENTION_AD2"]])){		
												$sad_ad2=utf8_encode($data[$cle["INTERVENTION_AD2"]]);
											}
										}
										$sad_ad3="";
										if($cle["INTERVENTION_AD3"]!==""){
											if(!empty($data[$cle["INTERVENTION_AD3"]])){		
												$sad_ad3=utf8_encode($data[$cle["INTERVENTION_AD3"]]);
											}
										}
										$sad_cp="";
										if($cle["INTERVENTION_CP"]!==""){
											if(!empty($data[$cle["INTERVENTION_CP"]])){		
												$sad_cp=$data[$cle["INTERVENTION_CP"]];
											}
										}
										$sad_ville="";
										if($cle["INTERVENTION_VILLE"]!==""){
											if(!empty($data[$cle["INTERVENTION_VILLE"]])){		
												$sad_ville=utf8_encode($data[$cle["INTERVENTION_VILLE"]]);
											}
										}
											
										
										if(!empty($sad_cp) AND !empty($sad_ville)){

											$sad_type = 1;

											if($cli_categorie == 3){
												if(empty($fac_adr_ok)){
													$sad_type = 2;
											
													$req_add_adr->bindParam(":sad_ref_id",$suspect);
													$req_add_adr->bindValue(":sad_type",$sad_type);
													$req_add_adr->bindParam(":sad_nom",$cli_nom);
													$req_add_adr->bindParam(":sad_service",$sad_service);
													$req_add_adr->bindParam(":sad_ad1",$sad_ad1);
													$req_add_adr->bindParam(":sad_ad2",$sad_ad2);									
													$req_add_adr->bindParam(":sad_ad3",$sad_ad3);
													$req_add_adr->bindParam(":sad_cp",$sad_cp);
													$req_add_adr->bindParam(":sad_ville",$sad_ville);
													$req_add_adr->bindValue(":sad_defaut",1);
													$req_add_adr->bindValue(":sad_siret","");											
													$req_add_adr->execute();
													$adresse=$ConnSoc->lastInsertId();
												}
											}else{
												$req_add_adr->bindParam(":sad_ref_id",$suspect);
												$req_add_adr->bindValue(":sad_type",$sad_type);
												$req_add_adr->bindParam(":sad_nom",$cli_nom);
												$req_add_adr->bindParam(":sad_service",$sad_service);
												$req_add_adr->bindParam(":sad_ad1",$sad_ad1);
												$req_add_adr->bindParam(":sad_ad2",$sad_ad2);									
												$req_add_adr->bindParam(":sad_ad3",$sad_ad3);
												$req_add_adr->bindParam(":sad_cp",$sad_cp);
												$req_add_adr->bindParam(":sad_ville",$sad_ville);
												$req_add_adr->bindValue(":sad_defaut",1);
												$req_add_adr->bindValue(":sad_siret","");											
												$req_add_adr->execute();
												$adresse=$ConnSoc->lastInsertId();

												if(empty($fac_adr_ok)){
													$sad_type = 2;
													$req_add_adr->bindValue(":sad_type",$sad_type);
													$req_add_adr->execute();
												}
												
											}

											if($adresse > 0){
												$adr_def = array(
													"cli_adresse" => $adresse,
													"cli_nom" => $cli_nom,
													"cli_service" => $sad_service,
													"cli_ad1" => $sad_ad1,
													"cli_ad2" => $sad_ad2,
													"cli_ad3" => $sad_ad3,
													"cli_cp" => $sad_cp,
													"cli_ville" => $sad_ville,
		
												);
											}
										
											
										}else{
											$alert_import="Ligne " . $row . " : impossible d'importer un contact sans adresse</br>";
										}
										// GESTION DU CONTACT GENERIQUE (identique sur toutes les lignes pour un même client)//
										$sco_titre=0;
										$sco_nom="";
										if($cle["CONTACT_NOM"]!==""){											
											if(!empty($data[$cle["CONTACT_NOM"]])){												
												$sco_nom=utf8_encode(trim($data[$cle["CONTACT_NOM"]]));
												if(substr($sco_nom,0,3)=="M. "){
													$sco_titre=1;
													$sco_nom=str_replace("M. ","",$sco_nom);
												}elseif(substr($sco_nom,0,9)=="Monsieur "){
													$sco_titre=1;
													$sco_nom=str_replace("Monsieur ","",$sco_nom);
												}elseif(substr($sco_nom,0,4)=="Mme "){
													$sco_titre=2;
													$sco_nom=str_replace("Mme ","",$sco_nom);
												}elseif(substr($sco_nom,0,7)=="Madame "){
													$sco_titre=2;
													$sco_nom=str_replace("Madame ","",$sco_nom);
												}
											}
										}
										$con_defaut=1;
										
										if(!empty($contact_mail) OR !empty($sco_nom)){
											
											$sco_fonction_nom="";
											if($cle["CONTACT_FONCTION"]!==""){
												if(!empty($data[$cle["CONTACT_FONCTION"]])){		
													$sco_fonction_nom=utf8_encode($data[$cle["CONTACT_FONCTION"]]);
												}
											}
											
											$sco_prenom="";
											if($cle["CONTACT_PRENOM"]!==""){
												if(!empty($data[$cle["CONTACT_PRENOM"]])){		
													$sco_prenom=utf8_encode($data[$cle["CONTACT_PRENOM"]]);
												}
											}
											
											
											$sco_tel="";
											if($cle["CONTACT_TEL"]!==""){
												if(!empty($data[$cle["CONTACT_TEL"]])){		
													$sco_tel=utf8_encode($data[$cle["CONTACT_TEL"]]);
													$sco_tel=str_replace("'","",$sco_tel);
													$sco_tel=str_replace('"',"",$sco_tel);
												}
											}
											$sco_portable="";
											if($cle["CONTACT_MOBILE"]!==""){
												if(!empty($data[$cle["CONTACT_MOBILE"]])){		
													$sco_portable=utf8_encode($data[$cle["CONTACT_MOBILE"]]);
													$sco_portable=str_replace("'","",$sco_portable);
													$sco_portable=str_replace('"',"",$sco_portable);
													
												}
											}
											$sco_fax="";
											if($cle["CONTACT_FAX"]!==""){
												if(!empty($data[$cle["CONTACT_FAX"]])){		
													$sco_fax=utf8_encode($data[$cle["CONTACT_FAX"]]);
													$sco_fax=str_replace("'","",$sco_fax);
													$sco_fax=str_replace('"',"",$sco_fax);
												}
											}
											
											
											$req_add_con->bindParam(":sco_ref_id",$suspect);
											$req_add_con->bindValue(":sco_fonction_nom",$sco_fonction_nom);
											$req_add_con->bindParam(":sco_titre",$sco_titre);
											$req_add_con->bindParam(":sco_prenom",$sco_prenom);
											$req_add_con->bindParam(":sco_nom",$sco_nom);
											$req_add_con->bindParam(":sco_tel",$sco_tel);
											$req_add_con->bindParam(":sco_portable",$sco_portable);									
											$req_add_con->bindParam(":sco_fax",$sco_fax);
											$req_add_con->bindParam(":sco_mail",$contact_mail);
											$req_add_con->bindParam(":sco_comment",$sco_comment);	
																									
											$req_add_con->execute();
											
											$contact=$ConnSoc->lastInsertId();
											
											// on lie le contact aux adresses
											
											if(!empty($adr_def)){
												$req_con_adr->bindParam(":sac_adresse",$adr_def['cli_adresse']);
												$req_con_adr->bindValue(":sac_contact",$contact);
												$req_con_adr->bindParam(":sac_defaut",$con_defaut);																							
												$req_con_adr->execute();
												if($con_defaut > 0 && !empty($contact)){
													$con_def = array(
														"sco_id" => $contact,
														"sco_nom" => $sco_nom,
														"sco_prenom" => $sco_prenom,
														"sco_fonction_nom" => $sco_fonction_nom,
														"sco_tel" => $sco_tel,
														"sco_mail" => $contact_mail,
													);
												}
												$con_defaut=0;											
											}
										}
										
										if(!empty($data[$cle['CORRESPONDANCE_TEXTE']])){
											$sco_comment=utf8_encode($data[$cle['CORRESPONDANCE_TEXTE']]);
											$req_add_cor->bindParam(":sco_commentaire",$sco_comment);
											$req_add_cor->bindParam(":sco_suspect",$suspect);																	
											$req_add_cor->execute();
										}

										$req_up_sus->bindParam("sus_adresse",$adr_def["cli_adresse"]);
										$req_up_sus->bindParam("sus_adr_nom",$adr_def["cli_nom"]);
										$req_up_sus->bindParam("sus_adr_service",$adr_def["cli_service"]);
										$req_up_sus->bindParam("sus_adr_ad1",$adr_def["cli_ad1"]);
										$req_up_sus->bindParam("sus_adr_ad2",$adr_def["cli_ad2"]);
										$req_up_sus->bindParam("sus_adr_ad3",$adr_def["cli_ad3"]);
										$req_up_sus->bindParam("sus_adr_cp",$adr_def["cli_cp"]);
										$req_up_sus->bindParam("sus_adr_ville",$adr_def["cli_ville"]);
										$req_up_sus->bindParam("suspect",$suspect);
										$req_up_sus->execute();

									}
									// FIN DE NOUVEAU CLIENT
										
									$sco_titre=0;
									$sco_nom="";
									if($cle["CONTACT_NOM_2"]!==""){
										if(!empty($data[$cle["CONTACT_NOM_2"]])){		
											$sco_nom=utf8_encode($data[$cle["CONTACT_NOM_2"]]);
											if(substr($sco_nom,0,3)=="M. "){
												$sco_titre=1;
												$sco_nom=str_replace("M. ","",$sco_nom);
											}elseif(substr($sco_nom,0,9)=="Monsieur "){
												$sco_titre=1;
												$sco_nom=str_replace("Monsieur ","",$sco_nom);
											}elseif(substr($sco_nom,0,4)=="Mme "){
												$sco_titre=2;
												$sco_nom=str_replace("Mme ","",$sco_nom);
											}elseif(substr($sco_nom,0,7)=="Madame "){
												$sco_titre=2;
												$sco_nom=str_replace("Madame ","",$sco_nom);
											}
										}
									}
									if(!empty($contact_mail_2) OR !empty($sco_nom)){

										$sco_comment=0;
										if($cle["CONTACT_COM_2"]!==""){
											if(!empty($data[$cle["CONTACT_COM_2_BIS"]])!==""){
												$sco_comment = utf8_encode($data[$cle["CONTACT_COM_2"]]) . " " . utf8_encode($data[$cle["CONTACT_COM_2_BIS"]]);
											}else{
												$sco_comment = utf8_encode($data[$cle["CONTACT_COM_2"]]);
											}
										}

										$sco_fonction_nom="";
										if($cle["CONTACT_FONCTION_2"]!==""){
											if(!empty($data[$cle["CONTACT_FONCTION_2"]])){		
												$sco_fonction_nom=utf8_encode($data[$cle["CONTACT_FONCTION_2"]]);
											}
										}
										$sco_prenom="";
										if($cle["CONTACT_PRENOM_2"]!==""){
											if(!empty($data[$cle["CONTACT_PRENOM_2"]])){		
												$sco_prenom=utf8_encode($data[$cle["CONTACT_PRENOM_2"]]);
											}
										}
										
										

										$sco_titre=0;
										if($cle["CONTACT_TITRE_2"]!==""){
											if(!empty($data[$cle["CONTACT_TITRE_2"]])){
												if($data[$cle["CONTACT_TITRE_2"]] == "M."){
													$sco_titre = 1;
												}elseif($data[$cle["CONTACT_TITRE_2"]] == "Mme"){
													$sco_titre = 2;
												}
											}
										}
										
										$sco_tel="";
										if($cle["CONTACT_TEL_2"]!==""){
											if(!empty($data[$cle["CONTACT_TEL_2"]])){		
												$sco_tel=utf8_encode($data[$cle["CONTACT_TEL_2"]]);
											}
										}
										
										$sco_portable="";
										if($cle["CONTACT_MOBILE_2"]!==""){
											if(!empty($data[$cle["CONTACT_MOBILE_2"]])){		
												$sco_portable=utf8_encode($data[$cle["CONTACT_MOBILE_2"]]);
											}
										}
										
										$sco_fax="";
										if($cle["CONTACT_FAX_2"]!==""){
											if(!empty($data[$cle["CONTACT_FAX_2"]])){		
												$sco_fax=utf8_encode($data[$cle["CONTACT_FAX_2"]]);
											}
										}
										
										$req_add_con->bindParam(":sco_ref_id",$suspect);
										$req_add_con->bindValue(":sco_fonction_nom",$sco_fonction_nom);
										$req_add_con->bindParam(":sco_titre",$sco_titre);
										$req_add_con->bindParam(":sco_prenom",$sco_prenom);
										$req_add_con->bindParam(":sco_nom",$sco_nom);
										$req_add_con->bindParam(":sco_tel",$sco_tel);
										$req_add_con->bindParam(":sco_portable",$sco_portable);									
										$req_add_con->bindParam(":sco_fax",$sco_fax);
										$req_add_con->bindParam(":sco_mail",$contact_mail_2);	
										$req_add_con->bindParam(":sco_comment",$sco_comment);
										$req_add_con->execute();
										$contact=$ConnSoc->lastInsertId();
										
										// on lie le contact aux adresses
										
										if(!empty($adr_def)){	
												$req_con_adr->bindParam(":sac_adresse",$adr_def['cli_adresse']);
												$req_con_adr->bindValue(":sac_contact",$contact);
												$req_con_adr->bindParam(":sac_defaut",$con_defaut);																							
												$req_con_adr->execute();
																				
										}
									}
									// ACTUA DE LA FICHE SUSPECT AVEC LES DONNEES ADRESSES CONTACTS
									
									if(!empty($con_def)){

										$req_up_con->bindParam("sus_contact",$con_def["sco_id"]);
										$req_up_con->bindParam("sus_con_nom",$con_def["sco_nom"]);
										$req_up_con->bindParam("sus_con_prenom",$con_def["sco_prenom"]);
										$req_up_con->bindParam("sus_con_fct_nom",$con_def["sco_fonction_nom"]);
										$req_up_con->bindParam("sus_con_tel",$con_def["sco_tel"]);
										$req_up_con->bindParam("sus_con_mail",$con_def["sco_mail"]);
										$req_up_con->bindParam("suspect",$suspect);
										$req_up_con->execute();

										$con_def = array();
										
									}
									
								}
							}
							// FIN IMPORT DU SUSPECT						
						}
					}
						
					$rapport_erreur.=$erreur_import;
					$rapport_alert.=$alert_import;
					$erreur_import="";
					$alert_import="";
				}
				// fin boucle ligne
				fclose($handle);
			}else{
				$erreur="Echec de l'enregistrement de l'import!";
			}
		}else{
			$erreur="Impossible de charger le fichier";
		}
	}else{
		$erreur="Formulaire incomplet!";
	} 
}?>
<!DOCTYPE html> 
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8"> 
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	  
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm">
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper" >
				<section id="content" class="" >
	<?php			if(!empty($erreur)){ ?>
						<p class="alert alert-danger" >
							<?=$erreur?>
							</br>Le fichier n'a pas été traité!
						</p>
	<?php			}elseif(!empty($rapport_erreur) OR !empty($rapport_alert)){ 	
						if(!empty($rapport_alert)){ ?>
							<div class="alert alert-warning">
								<h1>Lignes partiellements importées</h1>
								<p><?=$rapport_alert?></p>
							</div>
	<?php				}
						if(!empty($rapport_erreur)){ ?>
							<div class="alert alert-danger">
								<h1>Lignes non importées</h1>
								<p><?=$rapport_erreur?></p>
							</div>
	<?php				}
					}else{ ?>
						<p class="alert alert-success" >L'ensemble du fichier a été traité sans erreur!</p>
	<?php			} ?>
				</section>				
			</section>		
		</div>
	

		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="suspect_tri.php" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle"></div>
				<div class="col-xs-3 footer-right" >
					  <button type="button" id="cancel-btn" class="btn btn-danger btn-sm" style="display:none;">
						<i class='fa fa-times'></i> Annuler
					  </button>
				</div>
			</div>
		</footer>
<?php 	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
		</script>
	</body>
</html>
