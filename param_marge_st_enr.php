<?php
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");

// DROITS D'ACCES
if($_SESSION['acces']["acc_profil"]!=13 AND $_SESSION['acces']["acc_service"][2]!=1){
    header("location : deconnect.php");
    die();
}

if(!empty($_POST)){

    $req = $Conn->prepare("SELECT * FROM produits_familles,produits_sous_familles WHERE pfa_id=psf_pfa_id ORDER BY pfa_libelle,psf_libelle");
    $req->execute();
    $d_familles = $req->fetchAll();
    if(!empty($d_familles)){

        $sql_up="UPDATE produits_sous_familles SET 
        psf_marge_montant_1=:psf_marge_montant_1,psf_marge_taux_1=:psf_marge_taux_1,
        psf_marge_montant_2=:psf_marge_montant_2,psf_marge_taux_2=:psf_marge_taux_2,
        psf_marge_montant_3=:psf_marge_montant_3,psf_marge_taux_3=:psf_marge_taux_3
        WHERE psf_id=:psf_id";
        $req_up=$Conn->prepare($sql_up);

        foreach($d_familles as $f){

            $psf_marge_montant_1=0;
            if(!empty($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_montant_1"])){
                $psf_marge_montant_1=floatval($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_montant_1"]);
            }
            $psf_marge_taux_1=0;
            if(!empty($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_taux_1"])){
                $psf_marge_taux_1=floatval($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_taux_1"]);
            }
            $psf_marge_montant_2=0;
            if(!empty($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_montant_2"])){
                $psf_marge_montant_2=floatval($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_montant_2"]);
            }
            $psf_marge_taux_2=0;
            if(!empty($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_taux_2"])){
                $psf_marge_taux_2=floatval($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_taux_2"]);
            }
            $psf_marge_montant_3=0;
            if(!empty($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_montant_3"])){
                $psf_marge_montant_3=floatval($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_montant_3"]);
            }
            $psf_marge_taux_3=0;
            if(!empty($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_taux_3"])){
                $psf_marge_taux_3=floatval($_POST[$f['pfa_id'] . "_" . $f['psf_id'] . "_psf_marge_taux_3"]);
            }

            $req_up->bindParam("psf_marge_montant_1",$psf_marge_montant_1);
            $req_up->bindParam("psf_marge_taux_1",$psf_marge_taux_1);
            $req_up->bindParam("psf_marge_montant_2",$psf_marge_montant_2);
            $req_up->bindParam("psf_marge_taux_2",$psf_marge_taux_2);
            $req_up->bindParam("psf_marge_montant_3",$psf_marge_montant_3);
            $req_up->bindParam("psf_marge_taux_3",$psf_marge_taux_3);
            $req_up->bindParam("psf_id",$f['psf_id']);
            $req_up->execute();

        }

        $_SESSION['message'][] = array(
            "aff" => "",
            "titre" => "Enregistrement terminé",
            "type" => "success",
            "message" => "Les valeurs ont été enregistrées avec succès!" 
        );
    
    }

}


header("location : param_marge_st_voir.php");
die();
