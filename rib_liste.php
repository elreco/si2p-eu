<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_rib.php');
	include('modeles/mod_societe.php');
	include('modeles/mod_erreur.php');

	$filt_societe=0;
	if(isset($_POST["filt_societe"])){
		$filt_societe=$_POST["filt_societe"];
		$_SESSION['filt_societe']=$filt_societe;
	}elseif(!empty($_SESSION['filt_societe'])){
		$filt_societe=$_SESSION['filt_societe'];
	};
	
	// LES BANQUES DISPONIBLES
	
	$req=$Conn->query("SELECT ban_id,ban_code,ban_factor FROM Banques WHERE NOT ban_archive ORDER BY ban_code;");
	$d_banques=$req->fetchAll();
	
	$banque_affacturage=0;
	
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link href="assets/admin-tools/admin-forms/css/admin-forms-orion.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php	
				include "includes/header_def.inc.php";
			?>	
				
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
							
				<section id="content" class="animated fadeIn">
				
					<div class="row mb10">
						<div class="col-md-6 col-md-offset-3">
							<div class="admin-form theme-primary">
								<form action="rib_liste.php" method="post" id="admin-form">
									<div class="col-md-10">
										<label class="field select">
											<select name="filt_societe" id="filt_societe" >
												<option value="0">Filtrer par société...</option>
												<?=get_societe_select($filt_societe);?>											
											</select>
											<i class="arrow simple"></i>
										</label>
									</div>	
									
									<div class="col-md-2">
										<button type="submit" class="btn btn-primary btn-sm" >
											<i class="fa fa-search"></i>										
										</button>
									
									</div>
								</form>
							</div>
						</div>
					</div>
					
					<div class="row" >
						<div class="col-md-12" >
							<div class="table-responsive" >
								<table class="table table-hover table-striped" >
									<thead>
										<tr class="dark" >
											<th>ID</th>
											<th>Société</th>
											<th>Nom</th>
											<th>Code BQ</th>
											<th>IBAN</th>
											<th>BIC</th>											
											<th>&nbsp;</th>
											<th class="text-center" >Changement de RIB</th>
											<th class="text-center" >RIB d'affacturage</th>
											<th class="text-center" >PDF</th>
											<th>&nbsp;</th>
										</tr>															
									</thead>
									<tbody>
						<?php		$req_sql="SELECT rib_id,rib_nom,rib_iban,rib_bic,rib_defaut,rib_change,rib_affacturage,rib_societe,rib_banque
									,soc_nom,ban_code
									FROM Rib 
									LEFT JOIN Banques ON (Rib.rib_banque=Banques.ban_id)
									LEFT JOIN Societes ON (Rib.rib_societe=Societes.soc_id)";
									if($filt_societe>0){
										$req_sql=$req_sql . " WHERE rib_societe=" . $filt_societe;
									}
									$req_sql=$req_sql . " ORDER BY rib_nom";
									$req=$Conn->query($req_sql);
									$donnees = $req->fetchAll();
									
										$societe=get_societes();
										if(!empty($donnees)){										
											foreach($donnees as $value){ 
												/*$societe_nom="";
												if($value["rib_societe"]){											
													$cle_societe = array_search($value["rib_societe"], array_column($societe, 'soc_id'));												
													$societe_nom=$societe[$cle_societe]["soc_nom"];
												};*/ 
												
												$url_pdf="./documents/societes/rib/rib_". $value["rib_id"] . ".pdf"; 
											 ?>
												<tr>
													<td><?=$value["rib_id"]?></td>
													<td><?=$value["soc_nom"]?></td>
													<td><?=$value["rib_nom"]?></td>
													<td><?=$value["ban_code"]?></td>
													<td><?=$value["rib_iban"]?></td>
													<td><?=$value["rib_bic"]?></td>
											<?php	if($value["rib_defaut"]){
														echo("<td>RIB par défaut</td>");
													}else{
														echo("<td>&nbsp;</td>");
													}
													if($value["rib_change"]){
														echo("<td class='text-center' >OUI</td>");
													}else{
														echo("<td>&nbsp;</td>");
													} ?>													
													<td class="text-center" >
												<?php	if($value["rib_affacturage"]==1){
															echo("<i class='fa fa-check' ></i>");
														}else{
															echo("&nbsp;");
														} ?>
													</td>
													<td>
											<?php 		if(file_exists($url_pdf)){
															echo("<a href=" . $url_pdf . " target='_blank' >");
																echo("<i class='fa fa-file-pdf-o' ></i>");
															echo("</a>");											
														} ?>
													</td>																												
													<td class="text-center" >
														<span data-toggle="modal" data-target="#formEdit" >
															<a href="#" class="btn btn-warning btn-xs open-formEdit" data-id="<?=$value["rib_id"]?>" data-nom="<?=$value["rib_nom"]?>" data-societe="<?=$value["rib_societe"]?>" data-iban="<?=$value["rib_iban"]?>" data-bic="<?=$value["rib_bic"]?>" data-defaut="<?=$value["rib_defaut"]?>" data-change="<?=$value["rib_change"]?>" data-affacturage="<?=$value["rib_affacturage"]?>" data-banque="<?=$value["rib_banque"]?>" data-toggle="tooltip" data-placement="bottom" title="Modifier" >
																<i class="fa fa-pencil"></i>
															</a>
														</span>
													</td>									
												</tr>	
								<?php		
											};
										}; ?>																			
									</tbody>
								</table>
							</div>
						</div>
					</div>
	
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >
				</div>
				<div class="col-xs-3 footer-right" >
					<button class="btn btn-success btn-sm open-formEdit" data-toggle="modal" data-target="#formEdit" data-id="0" data-nom="" data-societe="0" data-iban="" data-bic="" data-defaut="" >
						<i class="fa fa-plus" ></i>
						Nouveau RIB
					</button>
				</div>
			</div>
		</footer>
									
		<div id="formEdit" class="modal fade" role="dialog">
			
			
			<div class="modal-dialog admin-form theme-primary">
				<div class="modal-content">
				
					<form method="post" action="rib_enr.php" enctype="multipart/form-data" >
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title" >Nouveau RIB</h4>
						</div>
						<div class="modal-body">
						
							<div>
								<input type="hidden" name="rib_id" id="rib_id" value="0" >
							</div>
							
							<div class="row" >
								<div class="col-md-12" >
								
									<div class="form-group">
										<label for="rib_nom" class="sr-only" >Nom</label>
										<div>
											<input type="text" class="form-control" name="rib_nom" id="rib_nom" placeholder="Nom" required >
										</div>
									</div>
								</div>	
							</div>
							<div class="row" >
								<div class="col-md-6" >								  
									<div class="form-group" >
										<label for="rib_banque">Banque</label>
										<select name="rib_banque" id="rib_banque" class="form-control" required >
											<option value="" ></option>
									<?php	if(!empty($d_banques)){
												foreach($d_banques as $d_b){
													if($d_b["ban_factor"]==1){
														$banque_affacturage=$d_b["ban_id"];
													}
													echo("<option value='" . $d_b["ban_id"] . "' >" . $d_b["ban_code"] . "</option>");
												}
											} ?>
										</select>
									</div>	
								</div>
								<div class="col-md-6" >								  
									<div class="form-group" >
										<label for="rib_societe">Société</label>
										<select name="rib_societe" id="rib_societe" class="form-control" required >
											<option value="" ></option>
											<?=get_societe_select(0);?>
										</select>
									</div>	
								</div>
							</div>
							<div class="row" >
								<div class="col-md-12 text-center" >
									<div class="form-group">
										<label for="rib_iban" class="sr-only" >IBAN</label>
										<div>
											<input type="text" class="form-control" name="rib_iban" id="rib_iban" placeholder="IBAN" required >
										</div>
									</div>
								</div>
							</div>
							
							<div class="row" >
								<div class="col-md-6" >
								
									<div class="form-group">
										<label for="rib_bic" class="sr-only" >BIC</label>
										<div>
											<input type="text" class="form-control" name="rib_bic" id="rib_bic" placeholder="BIC" required >
										</div>
									</div>
								</div>
								<div class="col-md-6 option-group field text-center" >
								
									<label class="option">
										<input type="checkbox" id="rib_defaut" name="rib_defaut" value="1" >
										<span class="checkbox"></span>Rib par défaut
									</label>

								</div>									
							</div>
							<div class="row text-center" >
								<div class="col-md-6 option-group field form-group" >			
									<label class="option">
										<input type="checkbox" id="rib_change" name="rib_change" value="1" >
										<span class="checkbox"></span>Changement de RIB
									</label>
								</div>	
								<div class="col-md-6 option-group field form-group" >			
									<label class="option">
										<input type="checkbox" id="rib_affacturage" name="rib_affacturage" value="1" >
										<span class="checkbox"></span>RIB Affacturage
									</label>
								</div>	
							</div>
							<div class="row" >							
								<div class="col-md-12" >
								
									<span class="prepend-icon file">
										<span class="button btn-primary">Choisir</span>
										<input type="file" class="gui-file" name="rib_fichier" id="rib_fichier" onchange="document.getElementById('uploader').value = this.value;" >
										<input type="text" class="gui-input" id="uploader" placeholder="Fichier PDF">
										<span class="field-icon">
											<i class="fa fa-upload"></i>
										</span>
									</span>
                              </div>
									
							</div>
										
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php
		include "includes/footer_script.inc.php"; ?>	
		<!-- PLUGIN -->
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<!-- Theme Javascript -->
		
		<script type="text/javascript">
		
			var $banque_affacturage=parseInt(<?=$banque_affacturage?>);
			jQuery(document).ready(function() {
				
				if($('#modal-error')){
					$('#modal-error').modal('show')	
				};
					
				$(document).on("click", ".open-formEdit", function () {
					var rib_id = $(this).data('id');
					var rib_nom = $(this).data('nom');
					var rib_societe = $(this).data('societe');
					var rib_iban = $(this).data('iban');
					var rib_bic = $(this).data('bic');
					var rib_defaut = $(this).data('defaut');
					var rib_change = $(this).data('change');
					var rib_affacturage = $(this).data('affacturage');
					var rib_banque = $(this).data('banque');
					
					$(".modal-body #rib_id").val( rib_id );
					$(".modal-body #rib_nom").val( rib_nom );						 
					$(".modal-body #rib_societe").val( rib_societe );					
					$(".modal-body #rib_iban").val( rib_iban );
					$(".modal-body #rib_bic").val( rib_bic );
					$(".modal-body #rib_banque").val( rib_banque );
					if(rib_defaut){
						$(".modal-body #rib_defaut").prop('checked', true);
						$(".modal-body #rib_defaut").prop('disabled', true);
					}else{
						$(".modal-body #rib_defaut").prop('checked', false);
						$(".modal-body #rib_defaut").prop('disabled', false);
					}
					if(rib_change){
						$(".modal-body #rib_change").prop('checked', true);
					}else{
						$(".modal-body #rib_change").prop('checked', false);
					}
					if(rib_affacturage){
						$(".modal-body #rib_affacturage").prop('checked', true);
					}else{
						$(".modal-body #rib_affacturage").prop('checked', false);
					}
					
					
					if(rib_id==0){
						$("#formEdit .modal-title").html('Nouveau rib');
						$(".modal-body #rib_societe").prop('disabled', false);
					}else{
						$("#formEdit .modal-title").html('Edition d\'un rib');
						$(".modal-body #rib_societe").prop('disabled', true);
					};
				});	

				/*$("#rib_societe").change(function(){
					if($(this).val()>0){
						get_rib_affacturage($(this).val(),actualiser_affacturage,"","");
					}else{
						actualiser_affacturage("");
					}
				});*/
				
				$("#rib_defaut").click(function(){
					if($(this).is(":checked")){
						$("#rib_affacturage").prop("checked",false);
						if($banque_affacturage!=0 && $("#rib_banque").val()==$banque_affacturage){
							$("#rib_banque").val("");
						}
					}
				});
				
				$("#rib_banque").change(function(){
					if($(this).val()!="" && $(this).val()==$banque_affacturage){
						$("#rib_affacturage").prop("checked",true);
						$("#rib_defaut").prop("checked",false);
					}else{
						$("#rib_affacturage").prop("checked",false);
					}
				});
					
					
				
				$("#rib_affacturage").click(function(){
					if($(this).is(":checked")){
						$("#rib_defaut").prop("checked",false);
						if($banque_affacturage!=0 && $("#rib_banque").val()!=$banque_affacturage){
							$("#rib_banque").val($banque_affacturage);
						}
					}else if($banque_affacturage!=0 && $("#rib_banque").val()==$banque_affacturage){
						$("#rib_banque").val("");
					}
				});
				
			});
			(jQuery);
			
			
			/*function get_rib_affacturage(societe,callback,param1,param2){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_rib_affacturage_get.php',
					data : "societe=" + societe,
					dataType: 'json',
					success: function(data){		
						callback(data,param1,param2);
					},
					error: function(data) {
						afficher_message("Erreur","danger",data.responseText);

					}			
				});
			}
			
			function actualiser_affacturage(json){
				alert(json);
			}*/
		
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
