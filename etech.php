<?php

$menu_actif = "4-1";
include "includes/controle_acces.inc.php";

include('includes/connexion.php');

include('modeles/mod_document.php');
include('modeles/mod_parametre.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_erreur.php');
include('modeles/mod_produit.php');
include('modeles/mod_client.php');
include('modeles/mod_droit.php');
include('modeles/mod_societe.php');

if(!isset($_SESSION['etech_couper_id'])){
  $_SESSION['etech_couper_id'] = 0;
}

// admin etech
if($_SESSION["acces"]["acc_droits"][5]==1){
	$droits_uti=1;
}

$auteur=false;
if($_SESSION["acces"]["acc_droits"][1]==1){
	$auteur=true;
}


	$dossier_id=0;
	$filtrer=FALSE;
	$filtre_critere=array(
		"doc_nom" =>"",
		"doc_date_1" =>"",
		"doc_date_2" =>"",
		"doc_produit" =>0,
		"doc_utilisateur" =>0,
		"doc_client" =>0,
		"doc_societe" =>0,
		"doc_archive" =>0
	);

	if(isset($_GET['raz'])){
		unset($_SESSION['filtre_etech']);
	}

	// AFFICHAGE DOSSIER COURANT

	if(isset($_GET['dos'])){
		if(intval($_GET['dos'])){
			$dossier_id=intval($_GET['dos']);
		}
	}

	if($dossier_id>0){

		// ON AFFICHE LE DOSSIER

		$dossier = get_document($_GET['dos']);
		$dossier_url= $dossier['doc_url'] . $dossier['doc_nom_aff'] . "/";
		if($dossier['doc_dossier'] != 1){
			header('Location: etech.php');
		}
	}else{

		if(isset($_POST['filtre'])){

			// UTI APPLI UN FILTRE

			if(!empty($_POST['doc_nom'])){
				$filtre_critere["doc_nom"]=$_POST['doc_nom'];
				$filtrer=TRUE;
			}
			if(!empty($_POST['doc_date_1'])){
				$filtre_critere["doc_date_1"]=$_POST['doc_date_1'];
				$filtrer=TRUE;
			}
			if(!empty($_POST['doc_date_2'])){
				$filtre_critere["doc_date_2"]=$_POST['doc_date_2'];
				$filtrer=TRUE;
			}
			if(!empty($_POST['doc_produit'])){
				$filtre_critere["doc_produit"]=$_POST['doc_produit'];
				$filtrer=TRUE;
			}
			if(!empty($_POST['doc_utilisateur'])){
				if(intval($_POST['doc_utilisateur'])){
					$filtre_critere["doc_utilisateur"]=intval($_POST['doc_utilisateur']);
					$filtrer=TRUE;
				}
			}
			if(!empty($_POST['doc_client'])){
				if(intval($_POST['doc_client'])){
					$filtre_critere["doc_client"]=intval($_POST['doc_client']);
					$filtrer=TRUE;
				}
			}
			if(!empty($_POST['doc_societe'])){
				if(intval($_POST['doc_societe'])){
					$filtre_critere["doc_societe"]=intval($_POST['doc_societe']);
					$filtrer=TRUE;
				}
			}

			if(!empty($_POST['doc_archive'])){
					$filtre_critere["doc_archive"]=1;
			}else{
				$filtre_critere["doc_archive"]=0;
			}

			if($filtrer){
				$_SESSION['filtre_etech']=$filtre_critere;
			}else{
				unset($_SESSION['filtre_etech']);
			}
			$dossier_url="";
		}elseif(isset($_SESSION['filtre_etech'])){

			// IL Y A UN FILTRE EN MEMOIRE

			$filtrer=TRUE;
			$filtre_critere=$_SESSION['filtre_etech'];
			$dossier_url="";

		}else{
			$dossier_url = "ETECH/";
		}
	}

	$mil="";
	$req_param=array();

	$sql="SELECT DISTINCT doc_id,doc_type,doc_dossier,doc_archive,doc_descriptif,doc_url,doc_nom,doc_ext,doc_nom_aff,doc_date_creation,doc_utilisateur, doc_nouveau
	FROM documents";
	if(!$_SESSION["acces"]["acc_droits"][5] && !$_SESSION["acces"]["acc_droits"][1]){

		$sql.=" LEFT JOIN documents_utilisateurs ON (documents.doc_id=documents_utilisateurs.dut_document)";
		if(!$_SESSION["acces"]["acc_droits"][1]){
			$mil .=" AND dut_utilisateur = " . $_SESSION['acces']['acc_ref_id'];
		}else{
			$mil .=" AND (dut_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " OR doc_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " )";
		}
	}

	if(!$filtrer){
		$mil.=" AND doc_archive = 0 AND doc_url LIKE :doc_url";
		$req_param["doc_url"]=$dossier_url;

	}else{

		// ON CONSTRUIT LA REQUETE EN FONCTION DU FILTRE

		if(!empty($filtre_critere["doc_nom"])){

			$mil.=" AND (";
			$mots_cle=explode(",", $filtre_critere["doc_nom"]);
			foreach($mots_cle as $num_mot => $mot){
				$mil.=" doc_nom LIKE :doc_nom_" . $num_mot . " OR doc_nom_aff LIKE :doc_nom_" . $num_mot . " OR doc_mot_cle LIKE :doc_nom_" . $num_mot . " OR doc_descriptif LIKE :doc_nom_" . $num_mot . " OR";
				$req_param["doc_nom_" . $num_mot]="%" . $mot . "%";
			}
			$mil=substr($mil, 0,-3);
			$mil.=" )";


			/*$mil.=" AND doc_nom LIKE :doc_nom OR doc_nom_aff LIKE :doc_nom";
			$req_param["doc_nom"]="%" . $filtre_critere["doc_nom"] . "%";*/
			$avance = 0;

		}
		if(!empty($filtre_critere["doc_date_1"])){
			$mil.=" AND doc_date_creation >= :doc_date_1";
			$req_param["doc_date_1"]=convert_date_sql($filtre_critere["doc_date_1"]);
			$avance = 1;
		}
		if(!empty($filtre_critere["doc_date_2"])){
			$mil.=" AND doc_date_creation <= :doc_date_2";
			$req_param["doc_date_2"]=convert_date_sql($filtre_critere["doc_date_2"]);
			$avance = 1;
		}
		if(!empty($filtre_critere["doc_produit"])){
			$sql.=" LEFT JOIN  documents_produits ON (documents.doc_id=documents_produits.dpr_document)";

				foreach($filtre_critere["doc_produit"] as $p){
					$first = array_values($filtre_critere["doc_produit"])[0];
					if($p == $first){
						$tableau_produit = $p;
					}elseif($p == end($filtre_critere["doc_produit"]) && $p != $first){
						$tableau_produit = $tableau_produit . "," . $p;
					}else{
						$tableau_produit = $tableau_produit . "," . $p . ",";
					}
				}


			$mil.=" AND dpr_produit IN (" . $tableau_produit . ")";
			$avance = 1;
		}
		if(!empty($filtre_critere["doc_utilisateur"])){
			$mil.=" AND doc_utilisateur = :doc_utilisateur";
			$req_param["doc_utilisateur"]=$filtre_critere["doc_utilisateur"];
			$avance = 1;
		}
		if(!empty($filtre_critere["doc_client"])){
			$mil.=" AND doc_client = :doc_client";
			$req_param["doc_client"]=$filtre_critere["doc_client"];
			$avance = 1;
		}
		if(!empty($filtre_critere['doc_societe'])){
			$sql.=" LEFT JOIN  Documents_Societes ON (documents.doc_id=Documents_Societes.dso_document)";
			$mil.=" AND dso_societe = :doc_societe";
			$req_param["doc_societe"]=$filtre_critere["doc_societe"];
			$avance = 1;
		}
		if(!empty($filtre_critere["doc_archive"])){
			$mil.=" AND doc_archive = 1";
			$avance = 1;
		}else{
			$mil.=" AND doc_archive = 0";
		}
		/*$mil .= " AND doc_dossier=0";*/
	}

	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY doc_dossier DESC, doc_nom_aff ASC";
	$req = $Conn->prepare($sql);
	if(!empty($req_param)){
		foreach($req_param as $cle => $value){
			$req->bindValue($cle,$value);
		}
	}
	/*var_dump($sql);
	var_dump($req_param);
	die();*/
	$req->execute();
	$docs = $req->fetchAll();


	// LES AUTEURS Etech

	$sql="SELECT DISTINCT uti_id,uti_nom,uti_prenom FROM Utilisateurs,Utilisateurs_Droits WHERE uti_id=udr_utilisateur AND udr_profil=0
	AND NOT uti_archive AND udr_droit IN (1,5) ORDER BY uti_nom,uti_prenom;";
	$req=$Conn->query($sql);
	$d_auteurs=$req->fetchAll();

	// CLIENT FILTRE POUR INI SELECT2

	if($filtre_critere["doc_client"]>0){

		$sql="SELECT cli_id,cli_nom FROM Clients WHERE cli_id=". $filtre_critere["doc_client"] . ";";
		$req=$Conn->query($sql);
		$d_client=$req->fetch();

	}

?>


<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Etech</title>
	<meta name="keywords" content=""/>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->

	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

	<link href="vendor/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css">
	<link href="vendor/plugins/select2/css/core.css" rel="stylesheet" type="text/css">

	<link href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" >
	<link href="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css" rel="stylesheet" type="text/css" >


	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
		.admin-etech{
			margin-top:35px;
		}
		.avance{
			color:#e31936!important;
			text-decoration:none!important;
			font-size:12px;
			font-weight: 300;
			padding:5px;
			border:1px solid #e31936;
			transition: background-color 0.2s;
		}
		.avance:hover{
			color:white!important;
			background-color:#e31936;
			transition: background-color 0.2s;
		}
		#avance{
			display:none;
		}

		.chosen-container:last-of-type .chosen-drop:last-of-type {
			border-bottom: 0;
			border-top: 1px solid #aaa;
			top: auto;
			bottom: 40px;
		}
		.link-etech, link-etech:active, link-etech:visited, link-etech:focus{
			text-decoration:none!important;
		}
		.link-etech:hover{
			color: #e31936!important;
			text-decoration:none!important;
			text-shadow:0px 0.1px 0.05px #e31936;
		}
		.link-etech::after{
			display: block;
		    content: attr(title);
		    font-weight: bold;
		    height: 0;
		    overflow: hidden;
		    visibility: hidden;
		}
		.no-sort::after { display: none!important; }

.no-sort { pointer-events: none!important; cursor: default!important; }
	</style>
</head>
<body class="sb-top sb-top-sm no-scroll" style="overflow-y: auto;">
	<div id="main">
<?php	include "includes/header_def.inc.php"; ?>
		<section id="content_wrapper" >
			<section class="animated fadeIn table-layout row100" style="height:100%;">
				<div class="tray tray-center" style="vertical-align:top;">
					<div class="panel-menu p12 admin-form theme-primary">
						<div class="row">
				<?php 		if($filtrer){ ?>
								<div class="col-md-4">
									<a href="etech.php?raz=1" class="btn btn-primary">
										<i class="fa fa-refresh"></i> Remettre à zéro le filtre
									</a>
								</div>
				<?php 		}else{ ?>

								<div class='col-md-12 text-right' >
				  <?php 			$new_url = explode("/", $dossier_url); ?>
									<h5>
										<i class="fa fa-search" aria-hidden="true"></i>
										Parcourir ETECH
										<i class="fa fa-long-arrow-right" aria-hidden="true"></i>

                <?php 					$url_i="ETECH/";
										$array_key = array_keys($new_url);
										$last_key = end($array_key);
										foreach($new_url as $k => $n){
											// echo("dossier : " . $n . "<br/>");
											if($n != "" && $k > 0){
												$doc_url_id = get_document_url($url_i,$n);
												if(!empty($doc_url_id['doc_id'])){ ?>
													<a href="etech.php?dos=<?=$doc_url_id['doc_id']?>" >
														<?=$n?>
													</a>
											<?php 	if($last_key==2){
														// sous dossier de etech
														// une seule boucle
														$dos_parent_id=0;

													}elseif($k == $last_key - 2){
														// element parent du dossier consulté
														$dos_parent_id=$doc_url_id['doc_id'];
														echo("/");
													}elseif($k!=$last_key-1){
														echo("/");
													}
													$url_i = $url_i . $n . "/";
												}
											}elseif($k==0){ ?>
												<a href="etech.php">ETECH</a> /
				<?php   					}
										} ?>
									</h5>
								</div>
				<?php 		} ?>
						</div>
					</div>
			<?php 	if(isset($_GET['succes']) && $_GET['succes'] == 1): ?>
						<div class="col-md-12" style="padding:0;">
							<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0px;border-radius: 0px;">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								Vous avez créé un <strong>dossier !</strong>
							</div>
						</div>
			<?php 	endif; ?>

					<div class="col-md-12" style="padding:0;display:none;" id="msg_couper2">
						<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom:0px;border-radius: 0px;">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							Impossible de coller  <strong>ici !</strong>
						</div>
					</div>

			<?php 	if(isset($_GET['succes']) && $_GET['succes'] == 2): ?>
					  <div class="col-md-12" style="padding:0;">
						<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0px;border-radius: 0px;">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  Vous avez modifié un <strong>dossier !</strong>
						</div>

					  </div>
			<?php 	endif; ?>

			<?php 	if(isset($_GET['succes']) && $_GET['succes'] == 3): ?>
					  <div class="col-md-12" style="padding:0;">
						<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0px;border-radius: 0px;">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  Vous avez modifié un <strong>fichier !</strong>
						</div>
					  </div>
			<?php 	endif; ?>

			<?php 	if(isset($_GET['succes']) && $_GET['succes'] == 4): ?>
					  <div class="col-md-12" style="padding:0;" style="display:none;">
						<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0px;border-radius: 0px;">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  Vous avez créé un <strong>fichier !</strong>
						</div>
					  </div>
			<?php 	endif; ?>

			<?php 	if(isset($_GET['succes']) && $_GET['succes'] == 5): ?>
					  <div class="col-md-12" style="padding:0;">
						<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0px;border-radius: 0px;">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  L'élément a été <strong>supprimé !</strong>
						</div>
					  </div>
			<?php	endif; ?>

			<?php 	if(isset($_GET['success']) && $_GET['success'] == 6): ?>
					  <div class="col-md-12" style="padding:0;">
						<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0px;border-radius: 0px;">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  L'élément a été <strong>collé !</strong>
						</div>

					  </div>
			<?php 	endif; ?>

			<?php 	if(isset($_GET['success']) && $_GET['success'] == 7): ?>
					  <div class="col-md-12" style="padding:0;">
						<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0px;border-radius: 0px;">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  Les éléments ont été <strong>collés !</strong>
						</div>
					  </div>
			<?php 	endif; ?>

			<?php 	if(isset($_GET['error']) && $_GET['error'] == 7): ?>
					  <div class="col-md-12" style="padding:0;">
						<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom:0px;border-radius: 0px;">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  Un ou plusieurs éléments n'ont pas pu être <strong>collé ici !</strong>
						</div>

					  </div>
			<?php 	endif; ?>

			<?php 	if(isset($_GET['search'])): ?>
					  <div class="col-md-12" style="padding:0;">
						<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:0px;border-radius: 0px;">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  Voici les résultats de votre <strong>recherche !</strong>
						</div>

					  </div>
			<?php 	endif; ?>

					<form action="etech_doc_enr.php?dos=<?=$dossier_id?>&deleteall=1" id="form_delete" method="post" >

						<input id="inputcoller" name="submit5" type="hidden" value="1">

						<table <?php if(!empty($docs)) echo("id='tab_doc'"); ?> class="table admin-form table-striped table-condensed table-hover text-center" style="margin:0px 0px 50px 0px!important;">
							<thead id="tab_doc_head" >
								<tr class="dark">
									<th class="no-sort">&nbsp;</th>
									<th class="text-center">Nom</th>
									<th class="text-center">Date de dépôt</th>
									<th class="text-center no-sort">Déposé par</th>
                                    <th class="text-center">Type</th>
							<?php
									if (!empty($droits_uti) OR $auteur){ ?>
										<th class="text-center no-sort">Actions</th>
							<?php 	}
									if(!empty($droits_uti)){ ?>
										<th class="text-center no-sort">
											<label class="option mn" data-toggle="tooltip" title="Sélectionner tout">
												<input type="checkbox" id="selectall"  name="mobileos" value="FR">
												<span class="checkbox mn"></span>
											</label>
										</th>
							<?php	}?>
								</tr>
							</thead>
					<?php	if(!empty($docs)){ ?>
								<tbody id="tab_doc_body" >
					<?php			foreach($docs as $d){  ?>
										<tr
							<?php 			if($d['doc_dossier'] == 1): ?>class="primary"<?php endif; ?>
							<?php 			if($d['doc_archive'] == 1): ?>class="danger"<?php endif; ?>
										>
											<td class="text-center ">
							<?php 				if($d['doc_dossier'] == 1){ ?>
													<a href="etech.php?dos=<?= $d['doc_id'] ?>" style="text-decoration: none; color:#4C4A49;">
														<i class="fa fa-folder-open"></i>
													</a>
							<?php 				}else{ ?>
													<a href="documents/etech/<?= $d['doc_nom'] ?>?<?= time(); ?>" style="text-decoration: none; color:#4C4A49;"  title="Fichier <?= $d['doc_ext'] ?>" class="telecharger-fichier" data-id="<?= $d['doc_id'] ?>" download="<?= $d['doc_nom'] ?>" >
													  <?php if($d['doc_ext'] == "pdf"): ?>
														<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
													  <?php elseif($d['doc_ext'] == "jpg" OR $d['doc_ext'] == "jpeg" OR $d['doc_ext'] == "png"): ?>
														<i class="fa fa-file-image-o" aria-hidden="true"></i>
													  <?php elseif($d['doc_ext'] == "rar" OR $d['doc_ext'] == "zip"): ?>
														<i class="fa fa-archive" aria-hidden="true"></i>
													  <?php elseif($d['doc_ext'] == "pptx" OR $d['doc_ext'] == "ppt"): ?>
														<i class="  fa fa-file-powerpoint-o" aria-hidden="true"></i>
													  <?php elseif($d['doc_ext'] == "doc" OR $d['doc_ext'] == "docx"): ?>
														<i class="fa fa-file-word-o" aria-hidden="true"></i>
													  <?php elseif($d['doc_ext'] == "txt"): ?>
														<i class="fa fa-file-text-o" aria-hidden="true"></i>
													  <?php elseif($d['doc_ext'] == "mp4" OR $d['doc_ext'] == "avi" OR $d['doc_ext'] == "mov" OR $d['doc_ext'] == "mpg" OR $d['doc_ext'] == "mpeg" OR $d['doc_ext'] == "vob"  OR $d['doc_ext'] == "VOB" OR $d['doc_ext'] == "flv" OR $d['doc_ext'] == "wmv" OR $d['doc_ext'] == "xvid"): ?>
													  	<i class="fa fa-video-camera" aria-hidden="true"></i>
													  <?php elseif($d['doc_ext'] == "mp3" OR $d['doc_ext'] == "wav"): ?>
													  	<i class="fa fa-headphones" aria-hidden="true"></i>
													  	 <?php elseif($d['doc_ext'] == "swf"): ?>
													  	<i class="fa fa-globe" aria-hidden="true"></i>
													  <?php else: ?>
														<i class="fa fa-file-o" aria-hidden="true"></i>
													  <?php endif; ?>
													  <?php if(!empty($d['doc_nouveau'])){ ?>
													  	 <i class="fa fa-certificate" aria-hidden="true" style="color:#27a0cc;"
													  	  data-toggle="tooltip" title="Nouveau document" data-placement="right"></i>
													  <?php } ?>
													</a>
							<?php 				} ?>
											</td>
											<td>

											  <?php if($d['doc_dossier'] == 1): ?>
												<a href="etech.php?dos=<?= $d['doc_id'] ?>" class="link-etech" style="color:#4C4A49;"><?= $d['doc_nom_aff'] ?></a>
											  <?php else: ?>
											  	<span title="<?= $d['doc_descriptif'] ?>" data-toggle="tooltip" data-placement="right">
													<a class="link-etech telecharger-fichier" data-id="<?= $d['doc_id'] ?>" style="color:#4C4A49;" href="documents/etech/<?= $d['doc_nom']?>?<?= time(); ?>"  download="<?= $d['doc_nom'] ?>" >
													  <?= $d['doc_nom_aff'] ?>
													</a>
												</span>
												<?php endif; ?>

											</td>
											<td>
											<span style="display:none;"><?= convert_date_datatable($d['doc_date_creation']) ?></span>
												<?= convert_date_txt($d['doc_date_creation']) ?>

											</td>
                            <?php

                            // chercher la dernière personne qui a fait la
                            $uti = get_utilisateur($d['doc_utilisateur']); ?>
											<td><?= $uti['uti_prenom'] ?> <?= $uti['uti_nom'] ?></td>
                                            <?php
                                            if(!empty($d['doc_type'])){
                                                $req = $Conn->prepare("SELECT dty_libelle FROM documents_types WHERE dty_id = ". $d['doc_type']);
                                    			$req->execute();
                                                $doc_type = $req->fetch();
                                            }else{
                                                $doc_type = "";
                                            } ?>
                                            <td>
                                                <?php if(!empty($doc_type)){ ?>
                                                    <?=$doc_type['dty_libelle'];?>
                                                <?php } ?>
                                            </td>
							<?php 			if(!empty($droits_uti) OR $auteur){ ?>
												<td>
													<?php if($d['doc_dossier'] ==0){ ?>
														<a href="etech_voir.php?id=<?= $d['doc_id']; ?>&dos=<?=$dossier_id?>" class="btn btn-info btn-sm" data-toggle="tooltip" title="Voir les droits">
															<i class="fa fa-eye" aria-hidden="true"></i>
														</a>
													<?php }?>
							<?php					if($d['doc_dossier'] == 1){
														if(!empty($droits_uti)){ ?>
															<a href="etech_dos_mod.php?id=<?= $d['doc_id']; ?>&dos=<?=$dossier_id?>" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Editer">
																<i class="fa fa-pencil" aria-hidden="true"></i>
															</a>
							<?php						}
													}else{

														if(!empty($droits_uti) OR ($auteur AND $d['doc_utilisateur']==$_SESSION['acces']['acc_ref_id']) )
														{ ?>
															<a href="etech_doc_mod.php?id=<?= $d['doc_id']; ?>&dos=<?=$dossier_id?>" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Editer">
																<i class="fa fa-pencil" aria-hidden="true"></i>
															</a>
															<a href="etech_doc.php?id=<?= $d['doc_id']; ?>&dos=<?=$dossier_id?>" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Mettre en ligne un nouveau fichier">
																<i class="fa fa-upload" aria-hidden="true"></i>
															</a>
							<?php						} ?>
														<span data-toggle="tooltip" title="Télécharger">
															<a href="documents/etech/<?= $d['doc_nom'] ?>?<?= time(); ?>" class="btn btn-warning btn-sm telecharger-fichier" data-id="<?= $d['doc_id'] ?>" download="<?= $d['doc_nom'] ?>" >
																<i class="fa fa-download" aria-hidden="true"></i>
															</a>
														</span>

							<?php						if(!empty($droits_uti) OR ($auteur AND $d['doc_utilisateur']==$_SESSION['acces']['acc_ref_id']) )
														{
															$pro_doc = get_document_produits($d['doc_id']);
															$nombre_pro_doc = count($pro_doc);
															$classe_btn="btn-warning";
															if(!empty($pro_doc)){
																$classe_btn="btn-success";
															} ?>
															<span data-toggle="tooltip" title="Il y a <?= $nombre_pro_doc ?> code(s) produit(s) pour ce document">
																<a href="#" class="btn <?=$classe_btn?> btn-sm openProduit" data-toggle="modal" data-target="#myModal10" data-id="<?= $d['doc_id'] ?>" >
																	<i class="fa fa-barcode" aria-hidden="true"></i>
																</a>
															</span>
									<?php 				}
													}
													if(!empty($droits_uti) OR ($auteur AND $d['doc_utilisateur']==$_SESSION['acces']['acc_ref_id']) ){ ?>
														<span data-toggle="tooltip" title="Couper">
															<a href="#" id="cut" data-id="<?= $d['doc_id'] ?>" class="btn btn-warning btn-sm openCouper">
																<i class="fa fa-scissors" aria-hidden="true"></i>
															</a>
														</span>
														<span data-toggle="tooltip" title="Supprimer">
															<a href="#" data-id="<?= $d['doc_id'] ?>" data-dossier="<?= $d['doc_dossier'] ?>" data-nom="<?= $d['doc_nom_aff'] ?>" data-url="<?= $d['doc_url'] ?>" data-toggle="modal" data-target="#myModal3" class="btn btn-danger btn-sm open-formDelete">
																<i class="fa fa-times" aria-hidden="true"></i>
															</a>
														</span>
									<?php			} ?>
												</td>
									<?php		if(!empty($droits_uti)){ ?>
													<td>
														<label class="option block mn">
															<input type="checkbox" class="checkbox1" name="check_list[]" value="<?= $d['doc_id'] ?>">
															<span class="checkbox mn"></span>
														</label>
													</td>
									<?php 		}
											} ?>
										</tr>
							<?php 	} ?>
								</tbody>
					<?php 	}else{
								$nbCols=4;
								if(!empty($droits_uti)){
									$nbCols=6;
								}elseif($auteur){
									$nbCols=5;
								} ?>
								<tbody>
									<tr>
										<td colspan="<?=$nbCols?>" >
											<div class="alert alert-warning" style="border-radius:0px;margin-bottom:0px;">
												Aucun dossier ou fichier.
											</div>
										</td>
									</tr>
								</tbody>
					<?php	} ?>
						</table>
					</form>

				</div>

				<aside class="tray tray-right tray290" style="vertical-align:top;height:100%!important;">

					<div class="admin-form sidebar-right-content nano-content p10" id="recherche-over" style="height:100%!important;" >
						<form action="etech.php" method="post" id="form_filtre" >
							<div>
								<input type="hidden" name="dos" value="<?=$dossier_id?>" />
							</div>
							<h4> Recherche</h4>
							<hr class="short">
							<div class="section row mb30 admin-etech">
								<div class="col-md-12" >
									<div class="section mb15">
										<span class="field prepend-icon">
											<input type="text" name="doc_nom" id="doc_nom" class="gui-input" placeholder="Nom" value="<?=$filtre_critere["doc_nom"]?>" />
											<span class="field-icon">
												<i class="fa fa-tag"></i>
											</span>
										</span>
									</div>


									<div id="avance" style="margin-bottom:40px;<?php if(!empty($avance)) echo("display:block;")?>">

										<div class="section row mb15">
											<div class="col-md-12">
												<span class="field prepend-icon">
													<input type="text" name="doc_date_1" id="doc_date_1" class="gui-input datepicker" placeholder="Entre le" value="<?=$filtre_critere["doc_date_1"]?>" />
													<span class="field-icon">
														<i class="fa fa-calendar"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="section row mb15">
											<div class="col-md-12">
												<span class="field prepend-icon">
													<input type="text" name="doc_date_2" id="doc_date_2" class="gui-input datepicker" placeholder="Et le" value="<?=$filtre_critere["doc_date_2"]?>" />
													<span class="field-icon">
														<i class="fa fa-calendar"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<h5><small>Recherche par Code produit</small></h5>
												<select name="doc_produit[]" multiple="multiple" class="select2" id="doc_produit" style="width:100%" >
									<?php 			$produits = get_produits();
													foreach($produits as $p){ ?>
														<option value="<?= $p['pro_id'] ?>"
									<?php 					if(!empty($filtre_critere['doc_produit'])){
																if(is_array($filtre_critere['doc_produit'])){
																	foreach($filtre_critere['doc_produit'] as $ep){
																		if($p['pro_id'] == $ep){
																			echo("selected");
																		}
																	}
																}else{
																	if($p['pro_id'] == $filtre_critere['doc_produit']){
																		echo("selected");
																	}
																}

															} ?> >
															<?= $p['pro_code_produit'] ?>
														</option>
									<?php			} ?>
												</select>
											</div>
										</div>
							<?php		if(!empty($d_auteurs)){ ?>
											<h5><small>Recherche par Auteur</small></h5>
											<div class="section mb15">
												<select id="doc_utilisateur" name="doc_utilisateur" class="select2" >
													<option value="0" >Auteur</option>
							<?php					foreach($d_auteurs as $a){
														if($a["uti_id"]==$filtre_critere['doc_utilisateur']){
															echo("<option value='" . $a["uti_id"] . "' selected >" . $a["uti_nom"] . " " . $a["uti_prenom"] . "</option>");
														}else{
															echo("<option value='" . $a["uti_id"] . "' >" . $a["uti_nom"] . " " . $a["uti_prenom"] . "</option>");
														}
													} ?>
												</select>
											</div>
							<?php		} ?>

										<h5><small>Recherche sur la société:</small></h5>
										<div class="section mb15">
											<select id="doc_societe" name="doc_societe" class="select2 select2-client-societe" >
												<option value="0" selected="selected">Société</option>
									<?php 		foreach($_SESSION['acces']["acc_liste_societe"] as $cle=>$value){
													$cle = substr($cle,0,1);
													if($filtre_critere['doc_societe'] == $cle){
														echo("<option value='" . $cle . "' selected >" . $value . "</option>");
													}else{
														echo("<option value='" . $cle . "' >" . $value . "</option>");
													}
												}?>
											</select>
										</div>

										<div class="text-center"  id="loader">
											<i class="fa fa-spinner fa-pulse fa-3x fa-fw text-center" style="font-size:12px;"></i>
										</div>

										<h5><small>Recherche par Client</small></h5>
										<div class="section mb15">
											<select name="doc_client" id="doc_client" class="select2 select2-client-n" style="width:100%;" >
									<?php  		if(!empty($d_client)){
													echo("<option value='" . $d_client["cli_id"] . "' >" . $d_client['cli_nom'] . "</option>");
												}else{
													echo("<option value='0' >&nbsp;</option>");
												} ?>
											</select>
										</div>

										<div class="col-md-12 section text-center" style="margin-top:10px;">
											<div class="option-group field">
												<label class="option">
													<input type="checkbox" name="doc_archive" id="doc_archive" value="doc_archive" <?php if($filtre_critere['doc_archive']==1) echo("checked"); ?> />
													<span class="checkbox" data-toggle="tooltip"></span>
													<label for="com_valide">Archivé</label>
												</label>
											</div>
										</div>

									</div>
									<hr class="short">
									<div class="section text-center">
										<button class="btn btn-default btn-sm ph25" type="submit" name="filtre">Rechercher</button>
									</div>
								</div>
							</div>
						</form>
						<?php if(!empty($avance)){ ?>
							<div class="col-md-12 text-center pb50" >
								<button type="button" class="avance btn btn-sm" >
									<i class="fa fa-minus recherche"></i> Recherche simple
								</a>
							</div>
						<?php }else{ ?>
							<div class="col-md-12 text-center pb50" >
								<button type="button" class="avance btn btn-sm" >
									<i class="fa fa-plus recherche"></i> Recherche avancée
								</a>
							</div>
						<?php } ?>

					</div>
				</aside>
			</section>
		</section>
	</div>
	<footer id="content-footer" class="affix">
		<div class="row">

			<div class="col-xs-12 footer-right text-center">
		<?php 	if (!empty($droits_uti) OR $auteur){ ?>
					<div class="col-md-2">
						<span data-toggle="tooltip" title="Ajouter un document">
							<a href="etech_doc.php?dos=<?=$dossier_id?>" class="btn btn-success btn-sm">
								<i class="fa fa-plus"></i>
							</a>
						</span>
					</div>
		<?php 	}
				if (!empty($droits_uti)){ ?>
					<div class="col-md-2">
						<span data-toggle="tooltip" title="Créer un dossier">
							<a href="etech_dos.php?dos=<?=$dossier_id?>" class="btn btn-success btn-sm" >
								<i class="fa fa-folder"></i>
							</a>
						</span>
					</div>

					<div class="col-md-2">
						<a href="ethec_sync.asp" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Synchroniser">
							<i class="fa fa-refresh"></i>
						</a>
					</div>
			<?php	// si recherche pas de repertoire connu
					// ondesactive le coller
					if(!empty($dossier_url)){ ?>
						<div class="col-md-2" >
							<button type="button" class="btn btn-warning btn-sm openCouper2" data-toggle="tooltip" title="Couper les éléments sélectionnés">
								<i class="fa fa-scissors" aria-hidden="true"></i>
							</button>
							<button data-toggle="tooltip" title="Coller" type="button" id="paste" class="btn btn-info btn-sm" style="display:none;">
								<i class="fa fa-clipboard"></i>
							</button>
							<button data-toggle="tooltip" title="Coller" type="button" id="paste2" class="btn btn-info btn-sm" style="display:none;">
								<i class="fa fa-clipboard"></i>
							</button>
						</div>
			<?php	} ?>
					<div class="col-md-2" id="annule_paste" style="display:none;">
						<button data-toggle="tooltip" id="annule_paste_btn" title="Annuler couper" type="button" class="btn btn-danger btn-sm">
							<i class="fa fa-clipboard"></i>
						</button>
					</div>
					<div class="col-md-2">
						<span data-toggle="tooltip" title="Supprimer les éléments sélectionnés">
							<a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal4" >
								<i class="fa fa-times" aria-hidden="true"></i>
							</a>
						</span>
					</div>
		<?php 	} ?>
			</div>
		</div>
	</footer>


		<div id="myModal3" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><span class="doc_url"></span><span class="doc_nom"></span></h4>
			  </div>
			  <div class="modal-body">
				<p>Êtes-vous sûr de vouloir supprimer ce <span class="doc_dossier"></span> ?</p>
			  </div>
			  <div class="modal-footer">
				<a href="" class="btn btn-success btn-sm doc_id" >Oui</a>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Non</button>
			  </div>
			</div>

		  </div>
		</div>

		<div id="myModal4" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">ETECH/</h4>
			  </div>
			  <div class="modal-body">
				<p>Êtes-vous sûr de vouloir supprimer les éléments selectionnés ?</p>
			  </div>
			  <div class="modal-footer">
				<button type="submit" name="submit5" id="submit5" class="btn btn-success btn-sm">Oui</button>

				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Non</button>
			  </div>
			</div>

		  </div>
		</div>


		<!------------------------------------- -------------------------------------->
		<div id="myModal5" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title doc-url"></h4>
			  </div>
			  <div class="modal-body">
				<p class="doc-descriptif"></p>
			  </div>
			  <div class="modal-footer">
				<a href="" class="btn btn-success btn-sm doc-nom" download><i class="fa fa-download" aria-hidden="true"></i> Télécharger le fichier</a>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
			  </div>
			</div>

		  </div>
		</div>

		<!------------------------------------- -------------------------------------->
		<div id="myModal10" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Ajouter ou supprimer des codes produits</h4>
			  </div>
			  <form class="formulaireProduit" action="" method="POST">
				<div class="modal-body">
				  <div class="row">
					<div class="col-md-12">
					  <select name="doc_produit[]" multiple class="chosen-select3" style="width:100%;">
						<?php $produits = get_produits_code(); ?>
						<?php foreach($produits as $pr): ?>
						  <option value="<?= $pr['pro_id'] ?>"><?= $pr['pro_code_produit'] ?></option>
						<?php endforeach; ?>
					  </select>
					</div>
				  </div>
				</div>
				<div class="modal-footer">
				  <button type="submit" class="btn btn-success btn-sm" download>Enregistrer</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
				  </div>
				</form>
			  </div>

			</div>
		  </div>

<?php
	include "includes/footer_script.inc.php"; ?>
	<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
	<script src="vendor/plugins/select2/js/select2.min.js"></script>

	<script src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
	<script src="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

	<script type="text/javascript" >


    jQuery(document).ready(function () {

		// ENTETE FIXE ET SCROLL TAB
		var calcDataTableHeight = function (elt_content, elt_head) {
			return $(elt_content).height() - $(elt_head).height() - 6;
		};

		var tableDefFix = $('#tab_doc').dataTable({
			"info":     false,
			"paging":   false,
			"searching": false,
			"order": []
		});

		$(window).resize(function () {
			var tableDefFixParam = tableDefFix.fnSettings();
			tableDefFixParam.oScroll.sY = calcDataTableHeight("#tab_doc_body", "#tab_doc_head");
			tableDefFix.fnDraw();
		});

		/*$(window).load(function (){
            setTimeout(function (){
				if($(".dataTables_scrollBody").lenght>0){
					$(".dataTables_scrollBody").mCustomScrollbar({
						theme: "dark"
					});
				}
            }, 100);
        });*/

		$('#loader').hide();

		$( "#submit5" ).click(function() {
			$("#form_delete").submit();
		});

		$(".avance").click(function(){
			$( "#avance" ).toggle( "slow", function() {
				if ($(this).is(':hidden')) {
					$(".avance").html('<i class="fa fa-plus"></i> Recherche avancée');
				}else {
					$(".avance").html('<i class="fa fa-minus"></i> Recherche simple');
				}
			});
		});

		// fonction couper multiple
		$( ".openCouper2" ).click(function(){

			var myCheckboxes = new Array();
			$("input[name='check_list[]']:checked").each(function() {
				myCheckboxes.push($(this).val());
			});
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_couper_all.php',
				data : {check_list: myCheckboxes},
				dataType: 'html',
				success: function(data)
				{	$("#paste2").show();
					$(".openCouper2").hide();
					$(".openCouper").attr('disabled', true);
				}
			});
		});

		// fonction couper unique
		$(".openCouper" ).click(function() {
			var selected_id = $(this).data('id');
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_couper.php',
				data : 'field=' + selected_id,
				dataType: 'json',
				success: function(data)
				{	$("#paste").show();
					$("#annule_paste").show();
					$(".openCouper2").hide();
					// desactive tous les boutons couper. Pour new couper, U doit annuler couper en cours
					$(".openCouper").attr('disabled', true);
				}
			});
		});

		// coller multiple N fichier
		$( "#paste2" ).click(function() {
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_coller_all.php',
				data : 'coller=<?= urlencode($dossier_url) ?>',
				dataType: 'html',
				success: function(data)
				{	console.log(data);
					if(data == 0){
						$("#paste2").hide();
						$(".openCouper2").show();
						$(".openCouper").attr('disabled', false);
						var url = window.location.href;
						if (url.indexOf('?') > -1){
							url += '&success=7'
						}else{
							url += '?success=7'
						}
						window.location.href = url;
					}else{
						$("#msg_couper2").show();
						$("#paste2").hide();
						$(".openCouper2").show();
						$(".openCouper").attr('disabled', true);
						var url = window.location.href;
						if (url.indexOf('?') > -1){
							url += '&error=7'
						}else{
							url += '?error=7'
						}
						window.location.href = url;
					}

				}
			});
		});

		// coller simple 1 fichier
		$( "#paste" ).click(function() {
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_coller.php',
				data : 'doc_url=<?= urlencode($dossier_url) ?>',
				dataType: 'html',
				success: function(data)
				{	console.log(data);
					if(data == 0){
						$("#paste").hide();
						$("#annule_paste").hide();
						$(".openCouper2").show();
						$(".openCouper").attr('disabled', false);
						var url = window.location.href;
						if (url.indexOf('?') > -1){
							url += '&success=6'
						}else{
							url += '?success=6'
						}
						window.location.href = url;
					}else{
						$("#msg_couper2").show();
						$("#paste").hide();
						$("#annule_paste").hide();
						$(".openCouper2").show();
						$(".openCouper").attr('disabled', false);
					}

				}
			});
		});
    // fin fonction couper

<?php 	if(isset($_SESSION['etech_couper_id']) && $_SESSION['etech_couper_id'] != 0): ?>
			$("#paste").show();
			$("#annule_paste").show();
			$(".openCouper").attr('disabled', true);
			$(".openCouper2").hide();
<?php	 elseif(!isset($_SESSION['etech_couper_id_all'])): ?>
			$("#paste").hide();
			$("#annule_paste").hide();
			$(".openCouper").attr('disabled', false);
			$(".openCouper2").show();
<?php 	endif; ?>

<?php 	if(isset($_SESSION['etech_couper_id_all']) && !empty($_SESSION['etech_couper_id_all'])): ?>
			$("#paste2").show();
			$("#annule_paste").show();
			$(".openCouper").attr('disabled', true);
			$(".openCouper2").hide();
<?php 	elseif(!isset($_SESSION['etech_couper_id'])): ?>
			$("#paste2").hide();
			$("#annule_paste").hide();
			$(".openCouper").attr('disabled', false);
			$(".openCouper2").show();
<?php 	endif; ?>








    $( "#annule_paste_btn" ).click(function() {
      $.ajax({
        type:'POST',
        url: 'ajax/ajax_annule_couper.php',
        data : "coller=<?= $dossier_url ?>",
        dataType: 'html',
        success: function(data)
        {
          $("#paste").hide();
          $("#paste2").hide();
          $("#annule_paste").hide();
          $(".openCouper2").show();
          $(".openCouper").attr('disabled', false);

        }


      });
    });


    $(".chosen-select3").select2();
    $( ".openProduit" ).click(function() {
      	$(".chosen-select3 option").each(function(){
        	$(this).attr("selected", false);
      	});
      	var selected_id = $(this).data('id');
     	<?php if(isset($_GET['dos'])): ?>
        	$('.formulaireProduit').attr('action', "etech_doc_produit.php?dos=<?= $_GET['dos'] ?>&doc=" + selected_id);
    	<?php else: ?>
    		$('.formulaireProduit').attr('action', "etech_doc_produit.php?doc=" + selected_id);
  		<?php endif; ?>
		$.ajax({
		    type:'POST',
		    url: 'ajax/ajax_doc_produit.php',
		    data : 'field=' + selected_id,
		    dataType: 'json',
		    success: function(data)
		    {

			    $.each(data, function(key, value) {

			        var selecteed = value[1];
			        $(".chosen-select3 option").each(function(){

				        if($(this).val() == selecteed){
				          	$(this).attr("selected", "selected");
				          	$(".chosen-select3").select2();
				        }
			      	});
			    });
		    }
		});

	});



    $( "#submitcoller" ).click(function() {
      $( "#inputcoller" ).attr("name", "coller");
      $( "#inputcoller" ).attr("value", $( "#collerselect option:selected" ).val());

      $( "#form_delete" ).submit();
    });
    $("#datepicker1, #datepicker2").datepicker({
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: false,
      onSelect: function( selectedDate ) {
        if(this.id == 'datepicker1'){
          var dateMin = $('#datepicker1').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); // Min Date = Selected + 1d

              $('#datepicker2').datepicker("option","minDate",rMin);

            }

          },
          beforeShow: function(input, inst) {
            var newclass = 'admin-form';
            var themeClass = $(this).parents('.admin-form').attr('class');
            var smartpikr = inst.dpDiv.parent();
            if (!smartpikr.hasClass(themeClass)) {
              inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
            }
          }
        });


    $(".telecharger-fichier").click(function() {
      	$.ajax({
        type:'POST',
        url: 'ajax/ajax_etech_utilisateur.php',
	      //the script to call to get data
	     data : 'field=' + $(this).data("id"),                    //you can insert url argumnets here to pass to api.php
      //for example "id=5&parent=6"

        //get name
        //--------------------------------------------------------------------
        // 3) Update html content
        //--------------------------------------------------------------------
        //Set output element html
        //recommend reading up on jquery selectors they are awesome
        // http://api.jquery.com/category/selectors/
     });
    });


    });
    $(document).on("click", ".openCouper", function () {

      var doc_id = $(this).data('id');

      $(".couper form").attr("action", "etech_couper.php?id=" + doc_id);

    });

    $(document).on("click", ".open-formDelete", function () {

      var doc_nom_aff = $(this).data('nom');
      var doc_dossier = $(this).data('dossier');
      if(doc_dossier == 1){
        $(".modal-body .doc_dossier").html("dossier");
      }else{
        $(".modal-body .doc_dossier").html("fichier");
      }
      var doc_id = $(this).data('id');
      var doc_url = $(this).data('url');

      $(".modal-title .doc_nom").html( doc_nom_aff );
      $(".modal-title .doc_url").html( doc_url );
      <?php if(isset($_GET['dos'])): ?>

      $(".modal-footer .doc_id").attr("href", "etech_doc_enr.php?dos=<?= $_GET['dos'] ?>&delete=" + doc_id);
    <?php else: ?>
    $(".modal-footer .doc_id").attr("href", "etech_doc_enr.php?delete=" + doc_id);
  <?php endif; ?>

});



    $('.modal-footer .doc-nom').click(function() {
      $('#myModal5').modal('hide');
    });



    $("#selectall").change(function(){
      $(".checkbox1").prop('checked', $(this).prop("checked"));
    });


/* SCRIPT VALIDATION DU FORM */

		function client(selected_id){
			$("#doc_client option").remove();
			  if(selected_id != 0){

				$.ajax({
				  type:'POST',
				  url: 'ajax/ajax_client_etech.php',
				  data : 'field=' + selected_id,
				  dataType: 'json',
				  success: function(data)
				  {
					$.each(data, function(key, value) {
					  $('#doc_client').append($("<option></option>").attr("value",key).text(value["cli_nom"]));
					});
				  }
				});
			  }
		}
	</script>
</body>
</html>
