<?php

// AFFICHE LA LISTE DES FACTURES
$menu_actif = "2-6";
include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');

// DONNEE UTILE AU PROGRAMME
/* var_dump($_SESSION['fac_relance_tri']);
die(); */
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
if(!empty($_GET['utilisateur'])){
	$_SESSION['retour'] = "rap_for_liste.php?utilisateur=" . $_GET['utilisateur'];
}else{
	$_SESSION['retour'] = "rap_for_liste.php";
}


// DONNEES POUR LA REQUETE
$drt_voir = false;
if($_SESSION['acces']['acc_profil'] == 10 OR $_SESSION['acces']['acc_profil'] == 2 OR $_SESSION['acces']['acc_profil'] == 4 OR $_SESSION['acces']['acc_profil'] == 15 OR $_SESSION['acces']['acc_profil'] == 5 OR $_SESSION['acces']['acc_profil'] == 9 OR $_SESSION['acces']['acc_profil'] == 13 OR $_SESSION['acces']['acc_profil'] == 8){
    $drt_voir = true;
}
if($acc_societe == 5){
	if(!empty($drt_voir)){
		$sql="SELECT * FROM Intervenants WHERE int_type = 1 AND int_archive = 0";
	}else{
		 $sql="SELECT * FROM Intervenants WHERE int_type = 1 AND int_ref_1 = " . $acc_utilisateur;
	}
}else{
	if(!empty($drt_voir)){

	    // selectionner tous les intervenants
		if(empty($acc_agence)){
			$sql="SELECT * FROM Intervenants WHERE int_type = 1 AND int_archive = 0";
		}else{
			$sql="SELECT * FROM Intervenants WHERE int_type = 1 AND int_archive = 0 AND int_agence = " . $acc_agence;

		}
	}else{
	    $sql="SELECT * FROM Intervenants WHERE int_type = 1 AND int_ref_1 = " . $acc_utilisateur;

	}
}

$req = $ConnSoc->query($sql);
$d_intervenants=$req->fetchAll();

//selection par défaut
if(!empty($_GET['utilisateur'])){
    $utilisateur_select = false;
    foreach($d_intervenants as $dinter){
        if($dinter['int_ref_1'] == $_GET['utilisateur']){
            $utilisateur_select = $_GET['utilisateur'];
        }
    }
}

?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">
						<?php if(!empty($d_intervenants)){ ?>
                        <div class="row">
                            <div class="col-md-6">
                                <h1>Rapports hebdomadaires de <strong class="text-primary intervName"></strong></h1>
                            </div>
                            <?php if($drt_voir){?>
                                <div class="col-md-6">
                                    <div class="section">
                                        <select name="filtre" id="filtre" class="form-control" >

                                                <option value="1">Les 6 dernières semaines</option>
                                                <?php for ($i=1; $i < 5; $i++) {  ?>
                                                    <option
													<?php if(!empty($_SESSION["rap_nb_semaines"]) && $_SESSION["rap_nb_semaines"] == date("Y") -  $i){ ?>
														selected
													<?php }?>
													value="<?= date("Y") -  $i ?>">Depuis <?= date("Y") -  $i ?></option>
                                                <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            <?php }?>

                        </div>

                        <?php if(!empty($d_intervenants)){ ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <span class="panel-title"> Intervenants</span>
                                    </div>
                                    <div class="panel-body pn text-center">


                                                <table class="table table-striped table-hover">
                                                    <tbody class="tbodyInterv">
                                                        <?php foreach($d_intervenants as $i){ ?>
                                                        <tr style="cursor: pointer;" class="selectInterv" id="interv_<?= $i['int_ref_1'] ?>" data-id="<?= $i['int_ref_1'] ?>">
                                                            <td id="getIntervName_<?= $i['int_ref_1'] ?>">
                                                                <?= $i['int_label_2'] ?> <?= $i['int_label_1'] ?>
                                                            </td>
                                                        </tr>

                                                        <?php } ?>
                                                    </tbody>
                                                </table>



                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">

                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr class="dark2" >
                                                    <th>Semaine</th>
                                                    <th>du Dimanche</th>
                                                    <th>au Samedi</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tableRap">

                                            </tbody>
                                        </table>
                                    </div>

                            </div>
                        </div>
                        <?php }?>

					<?php }else{?>
						<div class="alert alert-danger">
							Vous n'avez accès à aucun rapport hebdo.
						</div>
					<?php } ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<?php if(!empty($_SESSION["retourIntervListe"])){ ?>
                            <a
                        href="<?= $_SESSION["retourIntervListe"] ?>"
                        class="btn btn-default btn-sm" role="button">
							<span class="fa fa-long-arrow-left"></span>
							<span class="hidden-xs">Retour</span>
						</a>
                        <?php }?>
					</div>
					<div class="col-xs-6 footer-middle">&nbsp;</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function (){
                <?php if(!empty($utilisateur_select)){ ?>
                    get_semaines(<?= $utilisateur_select ?>, $("#filtre").val());
                <?php }elseif(!empty($d_intervenants[0]['int_ref_1'])){ ?>
                    get_semaines(<?= $d_intervenants[0]['int_ref_1'] ?>, $("#filtre").val());
                <?php }?>
                $(".selectInterv").click(function () {
                    get_semaines($(this).data("id"),  $("#filtre").val());
                });

                $("#filtre").change(function () {
                    get_semaines($(".tbodyInterv").find(".info").data("id"),  $(this).val());

                });
            });
            function get_semaines(utilisateur, filtre){
                $.ajax({
                    type: "get",
                    url: "ajax/ajax_get_semaines_rap.php",
                    data: "utilisateur=" + utilisateur + "&drt_voir=<?= $drt_voir ?>&nb_semaines=" + filtre,
                    dataType: "json",
                    success: function (data) {
                        if (data['0'] == undefined){

                            $("#tableRap").html("");

                        }else{
                            $("#tableRap").html("");
                            $.each(data, function (index, value) {
                                if(value['current']){
                                    classe="info";
                                }else{
                                    classe="";
                                }
                                var append = "<tr class='" + classe + "'>\
                                    <td>" + value['semaine']  + "</td>\
                                    <td><a href='rap_for_voir.php?annee="+ value['annee'] +"&utilisateur=" + utilisateur + "&semaine=" + value['semaine'] + "&retour_liste=1'>" + value['dimanche'] + "</a></td>\
                                    <td><a href='rap_for_voir.php?annee="+ value['annee'] +"&utilisateur=" + utilisateur + "&semaine=" + value['semaine'] + "&retour_liste=1'>" + value['samedi'] + "</a></td>\
                                </tr>\
                                ";

                                    $("#tableRap").append(append);
                            });
                            $(".selectInterv").removeClass("info");
                            $("#interv_" + utilisateur).addClass("info");
                            $(".intervName").html($("#getIntervName_" + utilisateur).html());
                        }
                    }
                });
            }
		</script>
	</body>
</html>
