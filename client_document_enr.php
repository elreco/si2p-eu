<?php
include "includes/controle_acces.inc.php";
include('includes/connexion_soc.php');
include("modeles/mod_client_2.php");
include("modeles/mod_upload.php");
	$req = $ConnSoc->prepare("SELECT cli_logo_1,cli_logo_2, cli_id FROM clients WHERE cli_id = :id");
	$req->bindParam(':id', $_GET['client']);
	$req->execute();
	$client = $req->fetch();

	$logo1 = $client['cli_logo_1'];
    $logo2 = $client['cli_logo_2'];

    $req = $ConnSoc->prepare("UPDATE clients SET cli_logo_1 =:cli_logo_1, cli_logo_2 = :cli_logo_2 WHERE cli_id = :cli_id;");
    if (!empty($_FILES['soc_logo_1']['name'])) {
        $path = $_FILES['soc_logo_1']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
    	$logo1=  "logo_1_" . $_GET['client'] . "_" . date('m-d-Y') . "." . $ext;

    }
     if (!empty($_FILES['soc_logo_2']['name'])) {
        $path = $_FILES['soc_logo_1']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
    	$logo2 =  "logo_2_" . $_GET['client'] . "_" . date('m-d-Y') . "." . $ext;

    
	}
	$req->bindParam(':cli_logo_1', $logo1);
	$req->bindParam(':cli_logo_2', $logo2);
	$req->bindParam(':cli_id', $_GET['client']);
	$req->execute();
	

    // upload_image(chemin vers le dossier, le tmp_name du fichier uploadé, le name du fichier uploadé, le size du fichier uploadé)
    if (!empty($_FILES['soc_logo_1']['name'])) { 
        $upload_erreur = upload('soc_logo_1', "clients/logos/","logo_1_" . $_GET['client'] . "_" . date('m-d-Y'), 0, array("png", "jpg", "gif"), 1); 
    }
    if (!empty($_FILES['soc_logo_2']['name'])) {
        $upload_erreur2 = upload('soc_logo_2', "clients/logos/","logo_2_" . $_GET['client'] . "_" . date('m-d-Y'), 0, array("png", "jpg", "gif"), 1);
    }
    if(!empty($upload_erreur)){
       
    	    Header('Location:client_voir.php?client=' . $_GET['client'] . '&tab8&error=6');

    }
    if(!empty($upload_erreur2)){
    	    Header('Location:client_voir.php?client=' . $_GET['client'] . '&tab8&error=6');
    }
    if(empty($upload_erreur2) && empty($upload_erreur)){
    	    Header('Location:client_voir.php?client=' . $_GET['client'] . '&tab8');
    }

