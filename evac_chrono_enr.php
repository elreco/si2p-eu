<?php

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

$erreur="";

if(!empty($_POST)){

	
	$ecp_id=0;
	if(isset($_POST['ecp_id'])){
		$ecp_id=intval($_POST['ecp_id']);
	}

	$position = abs($_POST['ecp_position']);
	
	if($ecp_id>0){

		// EDITION
		$req = $Conn->prepare("UPDATE evacuations_chronologies_param SET ecp_position=:ecp_position,ecp_libelle=:ecp_libelle WHERE ecp_id=:ecp_id;");
		$req->bindParam("ecp_position",$position);
		$req->bindParam("ecp_libelle",$_POST['ecp_libelle']);
		$req->bindParam("ecp_id",$ecp_id);
		$req->execute();

	}else{
		$req = $Conn->prepare("INSERT INTO evacuations_chronologies_param (ecp_position,ecp_libelle) VALUES (:ecp_position,:ecp_libelle);");
		$req->bindParam("ecp_position",$position);
		$req->bindParam("ecp_libelle",$_POST['ecp_libelle']);
		$req->execute();

	}
		
}else{
	$erreur="Formulaire incomplet!";
}

if(!empty($erreur)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur
	);
}else{
	$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Chrono ajoutée"
	);
}

header("location : evac_chrono_liste.php");
	
 ?>
