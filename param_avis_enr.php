<?php

	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");
	
	
	// CONTROLE

	if(isset($_POST)){
		
		$type=intval($_POST["type"]);
		$avis=intval($_POST["avis"]);
		
		if($type==1 OR $type==2 AND !empty($_POST["avi_nom"]) ){
			
			
			if($avis>0){
				
				// on verif que l'etat et le type de l'avis permette la modification
				$sql="SELECT avi_id FROM Avis WHERE avi_type=" . $type . " AND avi_id=" . $avis . " AND avi_statut=0";
				try{
					$req=$Conn->query($sql);
					$d_avis=$req->fetch();
					if(empty($d_avis)){
						$erreur_txt="Impossible de modifier l'avis selectionner";
					}
				}Catch(Exception $e){
					$erreur_txt=$e->getMessage();
				}
			}else{
				
				// nouvel avis on verif qu'il n'y en a pas déjà 1 en pre-prod
				$sql="SELECT avi_id FROM Avis WHERE avi_type=" . $type . " AND avi_statut=0";
				try{
					$req=$Conn->query($sql);
					$avis_pre_prod=$req->fetch();
					if(!empty($avis_pre_prod)){
						$erreur_txt="Vous ne pouvez avoir qu'un seul avis par type en pré-production";
					}
				}Catch(Exception $e){
					$erreur_txt=$e->getMessage();
				}	
			}
					
		}else{
			$erreur_txt="Impossible d'enregistrer l'avis de stage.";
		}
	}else{
		$erreur_txt="Impossible d'enregistrer l'avis de stage.";
	}
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur !",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : param_avis_liste.php");
		die();
	}
	
	// CONTROLE OK => MAJ BDD
	
	if($avis==0){
		
		// ADD
		
		$sql="INSERT INTO Avis (avi_nom,avi_type) VALUES (:avi_nom,:avi_type);";
		$req=$Conn->prepare($sql);
		$req->bindParam(":avi_nom",$_POST["avi_nom"]);
		$req->bindParam(":avi_type",$type);
		try{
			$req->execute();
			$avis=$Conn->lastInsertId();
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}

	}else{
		
		// UPDATE
		
		$sql="UPDATE Avis SET avi_nom=:avi_nom WHERE avi_id=:avis;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":avi_nom",$_POST["avi_nom"]);
		$req->bindParam(":avis",$avis);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}
	}
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur !",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : param_avis_liste.php");
		die();
	}
	
	// ENREGISTREMENT DES QUESTIONS
	
	if(empty($erreur_txt)){
		
		$sql_add="INSERT INTO Avis_Questionner (aqu_avis,aqu_question) VALUES (" . $avis . ",:question);";
		$req_add=$Conn->prepare($sql_add);
		
		$sql_del="DELETE FROM Avis_Questionner WHERE aqu_avis=" . $avis . " AND aqu_question=:question;";
		$req_del=$Conn->prepare($sql_del);
		
		
		$sql="SELECT aqu_id FROM Avis_Questions WHERE aqu_valide;";
		$req=$Conn->query($sql);
		while ($question = $req->fetch(PDO::FETCH_ASSOC))
		{
			if(!empty($_POST["question_" . $question["aqu_id"]])){
				// LA QUESTION EST SELECT 
				
				if(empty($_POST["question_enr_" . $question["aqu_id"]])){
					// elle n'etait pas deja enregistrée
					$req_add->bindParam(":question",$question["aqu_id"]);
					$req_add->execute();
				}
				
			}elseif(!empty($_POST["question_enr_" . $question["aqu_id"]])){
				// elle n'est plus enregistrée
				$req_del->bindParam(":question",$question["aqu_id"]);
				$req_del->execute();
				
			}
		}
		$req->closeCursor();
	}
	
	$_SESSION['message'][] = array(
		"titre" => "Enregistrement terminé !",
		"type" => "success",
		"message" => "L'avis a été enregistré avec succès!"
	);
	header("location : param_avis_liste.php");
?>