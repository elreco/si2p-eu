<?php 

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include 'modeles/mod_droit.php';

// resultat d'une recherche de devis

$erreur=0;

$_SESSION['retour'] = "devis_liste.php";
$_SESSION['retourDevis'] = "devis_liste.php";
$acc_utilisateur = $_SESSION['acces']['acc_ref_id'];

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}


	if(isset($_POST["search"])){
		
		// le formulaire a été soumit
		
		$critere_sql=array();	
		$critere_affiche=array();	
		
		if(!empty($_POST["chrono"])){
			$critere_sql["chrono"]=intval($_POST["chrono"]);
		}
		
		if(!empty($_POST["client_categorie"])){
			$critere_sql["client_categorie"]=intval($_POST["client_categorie"]);
		}
		
		if(!empty($_POST["client_sous_categorie"])){
			$critere_sql["client_sous_categorie"]=intval($_POST["client_sous_categorie"]);
		}
		
		if(!empty($_POST["client"])){
			$critere_sql["client"]=intval($_POST["client"]);
		}
		
		if(!empty($_POST["commercial"])){
			$critere_sql["commercial"]=intval($_POST["commercial"]);
		}
		
		if(!empty($_POST["date_deb"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["date_deb"]);
			if(!is_bool($DT_periode)){
				$critere_sql["date_deb"]=$DT_periode->format("Y-m-d");
			}
		}
		if(!empty($_POST["date_fin"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["date_fin"]);
			if(!is_bool($DT_periode)){
				$critere_sql["date_fin"]=$DT_periode->format("Y-m-d");
			}
		}
		
		$crit_esperance=-1;
		if(!empty($_POST["esperance"])){			
			if($_POST["esperance"]!=-1){
				$crit_esperance=$_POST["esperance"];
			}
		}
		if($crit_esperance!=-1){			
			$critere_sql["esperance"]=intval($_POST["esperance"]);
		}else{
			if(empty($_POST["sans_suite"])){
				$critere_sql["sans_suite"]=0;
			}
			if(empty($_POST["commande"])){
				$critere_sql["commande"]=100;
			}
		}
		
		// produit

		if(!empty($_POST["pro_categorie"])){
			$critere_sql["pro_categorie"]=intval($_POST["pro_categorie"]);
		}
		if(!empty($_POST["pro_famille"])){
			$critere_sql["pro_famille"]=intval($_POST["pro_famille"]);
		}
		if(!empty($_POST["pro_sous_famille"])){
			$critere_sql["pro_sous_famille"]=intval($_POST["pro_sous_famille"]);
		}
		if(!empty($_POST["pro_type"])){
			$critere_sql["pro_type"]=intval($_POST["pro_type"]);
		}
		if(!empty($_POST["produit"])){
			$critere_sql["produit"]=intval($_POST["produit"]);
		}
		
		// critere hors form
		
		if(!$_SESSION["acces"]["acc_droits"][6]){
			$critere_sql["utilisateur"]=$acc_utilisateur;
		}
		if(!empty($acc_agence)){
			$critere_sql["agence"]=$acc_agence;
			
		}
		
		// les crite lié à l'affichage
		
		$critere_affiche["affiche_ligne"]=0;
		if(!empty($_POST["affiche_ligne"])){
			$critere_affiche["affiche_ligne"]=1;
		}
		
		
		$_SESSION["critere_sql"]=$critere_sql;
		$_SESSION["critere_affiche"]=$critere_affiche;
		
	}elseif(isset($_SESSION["critere_sql"])){
		
		if(!empty($_SESSION["critere_sql"])){
			$critere_sql=$_SESSION["critere_sql"];
			$critere_affiche=$_SESSION["critere_affiche"];
		}else{
			$erreur=1;
		}
		
	}else{
		$erreur=1;
	}
	
	if($erreur==0){
	
		// contruction de la requetes
		
		$sql_devis="SELECT DISTINCT dev_id,dev_adr_ville,dev_numero,dev_date,DATE_FORMAT(dev_date,'%d/%m/%Y') AS dev_date_aff,dev_prospect,dev_esperance,dev_adr_ville,dev_total_ht,dev_bordereau
		,cli_id,cli_code,cli_nom,dev_chrono";
		if($critere_affiche["affiche_ligne"]==1){
			$sql_devis.=",dli_id,dli_reference,dli_montant_ht";
		}
		$sql_devis.=" FROM ( ( ( Devis LEFT JOIN Clients ON (Devis.dev_client=Clients.cli_id) )
		LEFT OUTER JOIN Devis_Lignes ON (Devis_Lignes.dli_devis=Devis.dev_id))
		LEFT OUTER JOIN Commerciaux ON (Devis.dev_commercial=Commerciaux.com_id))";
		
		$mil="";
		if(isset($critere_sql["utilisateur"])){
			$mil.=" AND (com_ref_1=:utilisateur OR cli_utilisateur=:utilisateur)";
		}
		if(isset($critere_sql["agence"])){
			$mil.=" AND dev_agence=:agence";
		}
		if(isset($critere_sql["chrono"])){
			$mil.=" AND dev_chrono=:chrono";
		}
		if(isset($critere_sql["client_categorie"])){
			$mil.=" AND cli_categorie=:client_categorie";
		}
		if(isset($critere_sql["client_sous_categorie"])){
			$mil.=" AND cli_sous_categorie=:client_sous_categorie";
		}
		if(isset($critere_sql["client"])){
			$mil.=" AND dev_client=:client";
		}
		if(isset($critere_sql["commercial"])){
			$mil.=" AND dev_commercial=:commercial";
		}
		if(isset($critere_sql["date_deb"])){
			$mil.=" AND dev_date>=:date_deb";
		}
		
		if(isset($critere_sql["date_fin"])){
			$mil.=" AND dev_date<=:date_fin";
		}
		
		if(isset($critere_sql["esperance"])){
			$mil.=" AND dev_esperance=:esperance";
		}else{
			if(isset($critere_sql["sans_suite"])){
				$mil.=" AND NOT dev_esperance=:sans_suite";
			}
			if(isset($critere_sql["commande"])){
				$mil.=" AND NOT dev_esperance=:commande";
			}
		}			
		
		// produit

		if(isset($critere_sql["pro_categorie"])){
			$mil.=" AND dli_categorie=:pro_categorie";
		}
		if(isset($critere_sql["pro_famille"])){
			$mil.=" AND dli_famille=:pro_famille";
		}
		if(isset($critere_sql["pro_sous_famille"])){
			$mil.=" AND dli_sous_famille=:pro_sous_famille";
		}
		if(isset($critere_sql["pro_type"])){
			$mil.=" AND dli_type=:pro_type";
		}
		if(isset($critere_sql["produit"])){
			$mil.=" AND dli_produit=:produit";
		}
		if(!empty($mil)){
			$mil = substr($mil,4);  
			$sql_devis.=" WHERE" . $mil;
		}
		$sql_devis.=" ORDER BY dev_date,dev_chrono;";
		$req = $ConnSoc->prepare($sql_devis);	
	
		if(!empty($critere_sql)){
			foreach($critere_sql as $c => $u){
				$req->bindValue($c,$u);
			}
		};
		$req->execute();
		$devis = $req->fetchAll();
	}	

	if($erreur==0){ ?>

		<!DOCTYPE html>
		<html>
			<head>
				<!-- Meta, title, CSS, favicons, etc. -->
				<meta charset="utf-8">
				<title>Si2P - ORION</title>
				<meta name="keywords" content=""/>
				<meta name="description" content="">
				<meta name="author" content="Si2P">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<!-- Theme CSS -->

				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
				<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
				<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

				<link rel="shortcut icon" href="assets/img/favicon.png">

				<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
				<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
			</head>
			<body class="sb-top sb-top-sm">
				<!-- Start: Main -->
				<div id="main">
			<?php	include "includes/header_def.inc.php"; ?>
					
					<section id="content_wrapper">
					
						<section id="content" class="animated fadeIn">
							<h1>Liste des devis</h1>
				<?php 		
							if(!empty($devis)){ ?>
								<div class="table-responsive">
									<table class="table table-striped table-hover" id="table_id">
										<thead>
											<tr class="dark" >
												<th rowspan="2" >ID</th>
												<th rowspan="2" >Code client</th>
												<th rowspan="2"  >Nom</th> 												
												<th rowspan="2" >Ville</th>
												<th rowspan="2" >Numéro</th>
												<th rowspan="2" >Date</th>
												<th colspan="2" >Montant HT</th>
										<?php	if($critere_affiche["affiche_ligne"]==1){ ?>	
													<th rowspan="2" >Produit</th>
										<?php	} ?>
												<th rowspan="2" >Etat fiabilité</th>												
											</tr>
											<tr class="dark" >
												<th>Client</th>
												<th>Prospect</th>
											</tr>
										</thead>
										<tbody>
										
									<?php	$total_ht_prospect=0;
											$total_ht_client=0;
											
											foreach($devis as $d){
												
												$ht_prospect=0;
												$ht_client=0;
												if(empty($d['dev_bordereau'])){
													if($critere_affiche["affiche_ligne"]==1){ 
														if(!empty($d['dev_prospect'])){
															$ht_prospect=$d['dli_montant_ht'];	
															$total_ht_prospect=$total_ht_prospect+$ht_prospect;
														}else{
															$ht_client=$d['dli_montant_ht'];	
															$total_ht_client=$total_ht_client+$ht_client;														
														}
													}else{
														if(!empty($d['dev_prospect'])){
															$ht_prospect=$d['dev_total_ht'];
															$total_ht_prospect=$total_ht_prospect+$ht_prospect;														
														}else{
															$ht_client=$d['dev_total_ht'];	
															$total_ht_client=$total_ht_client+$ht_client;														
														}
													}
												}
												
												
												?>										
												<tr>
													<td><?=$d['dev_id']?></td>
													<td><a href="client_voir.php?client=<?= $d['cli_id']?>"><?=$d['cli_code']?></a></td>
													<td><?=$d['cli_nom']?></td>
													<td><?=$d['dev_adr_ville']?></td>
													<td>
														<a href="devis_voir.php?devis=<?=$d['dev_id']?>" >
															<?=$d['dev_numero']?>
														</a>
													</td>
													<td><?=$d['dev_date_aff']?></td>
													<td class="text-right" ><?=number_format($ht_client,2,","," ")?></td>		
													<td class="text-right" ><?=number_format($ht_prospect,2,","," ")?></td>
										<?php		if($critere_affiche["affiche_ligne"]==1){ ?>	
														<td><?=$d['dli_reference']?></td>
										<?php		}
													switch ($d['dev_esperance']){
														case 0:
															echo("<td class='bg-dark' >Sans suite</td>");
															break;	
														case 30:
															echo("<td class='bg-danger' >30%</td>");
															break;
														case 50:
															echo("<td class='bg-warning' >50%</td>");
															break;	
														case 90:
															echo("<td class='bg-success' >90%</td>");
															break;		
														case 100:
															echo("<td class='bg-info' >Commandé</td>");
															break;
														default:
															echo("<td>&nbsp;</td>");
													}	?>
												</tr>
									<?php 	} 	?>
											<tr>
												<td colspan="6" class="text-right" >Total :</td>
												<td class="text-right" ><?=number_format($total_ht_client,2,","," ")?></td>
												<td class="text-right" ><?=number_format($total_ht_prospect,2,","," ")?></td>
												<td>&nbsp;</td>
											</tr>
												
										</tbody>
										
									</table>
								</div>
			<?php 			}else{ ?>
								<div class="col-md-12 text-center" style="padding:0;" >
									<div class="alert alert-warning" style="border-radius:0px;">
										Aucun devis correspondant à votre recherche.
									</div>
								</div>
			<?php 			} ?>
						</section>
						<!-- End: Content -->
					</section>
				</div>
				<!-- End: Main -->
				
				<footer id="content-footer" class="affix">
					<div class="row">
						<div class="col-xs-3 footer-left">
							<a href="devis_tri.php" class="btn btn-primary btn-sm" role="button">
								<span class="fa fa-search"></span>
								<span class="hidden-xs">Nouvelle recherche</span>
							</a>
						</div>
						<div class="col-xs-6 footer-middle">&nbsp;</div>
						<div class="col-xs-3 footer-right">
							<a href="devis.php" class="btn btn-success btn-sm" role="button">
								<span class="fa fa-plus"></span>
								<span class="hidden-xs">Nouveau devis</span>
							</a>
						</div>
					</div>
				</footer>
	<?php		include "includes/footer_script.inc.php"; ?>	
				<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
				<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
				<script type="text/javascript">
					jQuery(document).ready(function () {
						
					});
				</script>
			</body>
		</html>
<?php
	}else{
		
	} ?>