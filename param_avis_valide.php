<?php

	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");

	$erreur_txt="";
	
	$avis=0;
	if(isset($_GET["avis"])){
		if(!empty($_GET["avis"])){
			$avis=intval($_GET["avis"]);
		}
	}
	if($avis==0){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => "Impossible de valider l'avis de stage"
		);
		header("location : ../avis_liste.php");
		die();
	}
	
	// DONNEE POUR VALIDATION
	
	// l'avis à validé
	
	try{
		$req=$Conn->query("SELECT * FROM Avis WHERE avi_id=" . $avis . ";");
		$d_avis=$req->fetch();
		if(empty($d_avis)){
			$erreur_txt="Impossible de valider l'avis de stage.";
		}
	}Catch(Exception $e){
		$erreur_txt=$e->getMessage();
	}			
	
	// l'avis actuellement en prod
	if(empty($erreur_txt)){
		
		try{
			$req=$Conn->query("SELECT avi_id FROM Avis WHERE avi_type=" . $d_avis["avi_type"] . " AND avi_statut=1;");
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}
		if(empty($erreur_txt)){
			$d_avis_archive=$req->fetch();
			if(!empty($d_avis_archive)){
				
				try{
					$req=$Conn->query("UPDATE Avis SET avi_statut=2,avi_prod_fin=NOW() WHERE avi_id=" . $d_avis_archive["avi_id"] . ";");
				}Catch(Exception $e){
					$erreur_txt=$e->getMessage();
				}
				
			}		
		}
	}
	
	// passage de l'avis en prod
	if(empty($erreur_txt)){
		try{
			$req=$Conn->query("UPDATE Avis SET avi_statut=1,avi_prod_deb=NOW() WHERE avi_id=" . $d_avis["avi_id"] . ";");
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}
	}

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : ../avis_voir.php?avis=" . $avis);
		
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Enregistrement terminé",
			"type" => "success",
			"message" => "L'avis a été validé avec succès!" 
		);
		header("location : param_avis_liste.php");
	}
	

	
?>