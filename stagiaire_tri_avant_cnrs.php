<?php
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'connexion_soc.php';

$_SESSION['retour'] = "stagiaire_tri.php";

// efface la pagination des clients
if(isset($_SESSION['client_tableau'])){
	unset($_SESSION['client_tableau']);
}

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];    
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
    $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];  
}

	// CONSTRUCTION DU SELECT CLIENT SI GROUPE 
	if($_SESSION['acces']["acc_ref"]==2){
		
		// rechercher le client de la personne conectée
		
		$d_clients=array();
		
		$req = $Conn->prepare("SELECT cli_id,cli_groupe, cli_filiale_de,cli_reference,cli_code,cli_nom FROM clients WHERE cli_id = :cli_id AND cli_groupe=1");
		$req->bindParam(':cli_id', $_SESSION['acces']['acc_client']);
		$req->execute();
		$result = $req->fetch();
		if(!empty($client)){
			if($result['cli_filiale_de'] == 0){ 
			
				// maison mere
				
				$lib=$result['cli_nom'];
				if(!empty($result['cli_reference'])){
					$lib.=" (" . $result['cli_reference'] . ")";
				}
				
				$d_clients[]=array(
					"entite_id" => $result['cli_id'],
					"entite_libelle" => $lib
				);
				
				$req = $Conn->prepare("SELECT cli_id,cli_code, cli_nom, cli_reference FROM clients WHERE cli_groupe=1 AND cli_filiale_de = :cli_filiale_de");
				$req->bindParam(':cli_filiale_de', $_SESSION['acces']['acc_client']);
				$req->execute();
				$result_fil = $req->fetchAll(); 
				foreach($result_fil as $fil){
					$lib=$result['cli_nom'];
					if(!empty($result['cli_reference'])){
						$lib.=" (" . $result['cli_reference'] . ")";
					}
					$d_clients[]=array(
						"entite_id" => $result['cli_id'],
						"entite_libelle" => $lib
					);
				}
			
			}else{ 
			
				// chercher toutes les filiales à partir du client connecté

				// NON DEV
				/*
				 $req = $Conn->prepare("SELECT cli_code, cli_nom, cli_id FROM clients WHERE cli_filiale_de = :cli_filiale_de AND cli_fil_de = :cli_fil_de");
				$req->bindParam(':cli_filiale_de', $client['cli_filiale_de']);
				$req->bindParam(':cli_fil_de', $_SESSION['acces']['acc_client']);
				$req->execute();
				$clients = $req->fetchAll(); 

				$i = 0;
				foreach($clients as $c){
					if(!empty($client_str)){
						$client_str = $client_str . "," . $c['cli_id'];
					}else{
						$client_str = $c['cli_id'];
					}
					$i= $i+1;
					$req = $Conn->prepare("SELECT cli_code, cli_nom, cli_id FROM clients WHERE cli_filiale_de = :cli_filiale_de AND cli_fil_de = :cli_fil_de");
					$req->bindParam(':cli_filiale_de', $client['cli_filiale_de']);
					$req->bindParam(':cli_fil_de', $c['cli_id']);
					$req->execute();
					$fil1[$i] = $req->fetch();
				}
				$i = 0;
				if(!empty($fil1)){
					foreach($fil1 as $f){
						if(!empty($client_str)){
							$client_str = $client_str . "," . $f['cli_id'];
						}else{
							$client_str = $f['cli_id'];
						}
						$i= $i+1;
						$req = $Conn->prepare("SELECT cli_code, cli_nom, cli_id FROM clients WHERE cli_filiale_de = :cli_filiale_de AND cli_fil_de = :cli_fil_de");
						$req->bindParam(':cli_filiale_de', $client['cli_filiale_de']);
						$req->bindParam(':cli_fil_de', $f['cli_id']);
						$req->execute();
						$fil2[$i] = $req->fetch();

						if(!empty($fil2)){
							foreach($fil2 as $f2){
								if(!empty($client_str)){
									$client_str = $client_str . "," . $f2['cli_id'];
								}else{
									$client_str = $f2['cli_id'];
								}
							}
						}
					}
				}
				*/
				
			}
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Recherche stagiaire</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="stagiaire_liste.php" id="formulaire" >
			<div>
				<input type="hidden" class="select2-client-societe" value="<?=$acc_societe?>" />
			</div>
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											<div class="text-center">
												<div class="content-header">
													<h2>Recherche de <b class="text-primary title-suscli">stagiaire</b></h2>
												</div>												
												<div class="col-md-10 col-md-offset-1">		
													<!--DEBUT CONTENU -->
													<div class="row">
														<div class="col-md-4">
															<input type="text" name="sta_nom" class="gui-input nom" placeholder="Nom" />
														</div>
														<div class="col-md-4">
															<input type="text" name="sta_prenom" class="gui-input prenom" placeholder="Prénom" />
														</div>
														<div class="col-md-4">
															<label for="datepicker-from" class="field prepend-icon">
																<input type="text" name="sta_naissance" class="gui-input datepicker" placeholder="Date de naissance" />
																<label class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</label>
															</label>
														</div>
													</div>
											<?php	if($_SESSION['acces']["acc_ref"]==1){ 
														// VU SI2P ?>
														<div class="row mt15" >
															<div class="col-md-4">
																<select name="sta_client" class="select2 select2-client" >
																	<option value="0" >Client ...</option>
																</select>
															</div>
															<div class="col-md-4 pt10">
																<div class="option-group field">
																	<label class="option option-primary" >
																		<input type="checkbox" name="filiale" value="on" />
																		<span class="checkbox"></span>et filiale
																	</label>
																</div>
															</div>
															<div class="col-md-4 pt10">
																<div class="option-group field">
																	<label class="option option-primary" >
																		<input type="checkbox" name="employeur" value="on" checked />
																		<span class="checkbox"></span>Employeur actuel
																	</label>
																</div>
															</div>
														</div>
											<?php	}else{
														// VU CLIENT													
														if(!empty($d_clients)){ ?>
															<div class="row" >
																<div class="col-md-4">
																	<select name="sta_client" class="select2" >
																		<option value="0" >Entité ...</option>
															<?php		foreach($d_clients as $client){
																			echo("<option value='" . $client["entite_id"] . "' >" . $client["entite_libelle"] . "</option>");
																		} ?>
																	</select>
																</div>
																<div class="col-md-4 pt10">
																	<div class="option-group field">
																		<label class="option option-primary" >
																			<input type="checkbox" name="filiale" value="on" />
																			<span class="checkbox"></span>et filiale
																		</label>
																	</div>
																</div>
															</div>
									<?php				 	if(!empty($_SESSION['acces']['acc_client_ref'])){ 
																// construire les input des ref internes ?>
																<div class="row" >
									<?php							foreach($_SESSION['acces']['acc_client_ref'] as $k=>$ref){ ?>
																		<div class="col-md-4">
																			<label><?= $ref ?> :</label>
																			<input type="text" name="scl_ref_<?= $k ?>" class="gui-input" placeholder="<?= $ref ?>" />
																		</div>
									<?php							} ?>
																</div>
									<?php					} ?>
															<div class="col-md-1">
																<label class="option option-primary mt25">
																	<input type="checkbox"  name="scl_archive" value="1" <?php if(!empty($_POST['scl_archive'])){ ?> checked <?php } ?>>
																	<span class="checkbox mn" ></span> Archivé
																</label>
															</div>
											<?php		}
													} ?>
													<!-- FIN CONTENU -->										
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>		
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left"></div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right">
						<button type="submit" name="search" class="btn btn-primary btn-sm">
							<i class='fa fa-search'></i> Rechercher
						</button>

					</div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	
		<script src="assets/admin-tools/admin-forms/js/jquery-ui-datepicker.min.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="vendor/plugins/holder/holder.min.js"></script>
		<script src="assets/js/custom.js"></script>
		<script type="text/javascript">


			jQuery(document).ready(function (){
			
			});
		
			
		</script>
	</body>
</html>
