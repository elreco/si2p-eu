<?php 
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

require "modeles/mod_utilisateur.php";
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>SI2P - Orion - Paramètres</title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
    <div id="main">
      <?php
      include "includes/header_def.inc.php";
      ?>


      <!-- Start: Content-Wrapper -->
      <section id="content_wrapper" class="" style="margin-bottom:40px;">
        <header id="topbar" >
          <div class="topbar-left">
            <ol class="breadcrumb">
              <li>
                <span class="glyphicon glyphicon-cog"></span> <a href="parametres.php"> Paramètres</a>
              </li>
              <li>
                <a href="etech.php">Etech</a>
              </li>
              <li class="crumb-trail">Nouveau document</li>
            </ol>
          </div>

        </header>
        <section id="content" class="animated fadeIn">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="admin-form theme-primary ">
                <div class="panel heading-border panel-primary">
                  <div class="panel-body bg-light">
                    <div class="text-center">

                      <div class="content-header">
                        <h2>Ajouter un <b class="text-primary">document</b></h2>
                        <h3>Dans: ETECH/Documents/RH/2016</h3>

                      </div>

                      <div class="col-md-10 col-md-offset-1">
                        <form action="etech_doc_enr.php" method="POST" id="admin-form">

                          <div class="row">
                            <div class="col-md-12">
                              <div class="section">
                                <label class="field prepend-icon file">
                                  <span class="button btn-primary">Choisir</span>
                                  <input type="file" class="gui-file" name="doc_nom" id="doc_nom" onchange="document.getElementById('uploader2').value = this.value;">
                                  <input type="text" class="gui-input" id="uploader2" placeholder="Merci de sélectionner un fichier">
                                  <label class="field-icon">
                                    <i class="fa fa-upload"></i>
                                  </label>
                                </label>
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="section">
                                <div class="field prepend-icon">
                                  <input type="text" name="doc_nom_aff" id="doc_nom_aff" class="gui-input" placeholder="Nom d'affichage du fichier" required="">
                                  <label for="soc_code" class="field-icon">
                                    <i class="fa fa-tag"></i>
                                  </label>
                                </div>
                              </div>
                            </div>

                          </div>
                          <div class="row">
                           <div class="col-md-6">
                            <div class="section">
                              <div class="field select">
                                <select name="doc_type" id="doc_type">
                                  <option value="">Type ...</option>
                                  <option value="1">doc_type</option>


                                </select>
                                <i class="arrow simple"></i>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="section">
                              <div class="field select">
                                <select name="doc_client" id="doc_client">
                                  <option value="">Client ...</option>
                                  <option value="1">doc_type</option>


                                </select>
                                <i class="arrow simple"></i>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="section">
                              <div class="field select">
                                <select name="doc_service" id="doc_service">
                                  <option value="">Service ...</option>
                                  <option value="1">doc_service</option>


                                </select>
                                <i class="arrow simple"></i>
                              </div>
                            </div>

                            </div>
                            <div class="col-md-6">
                              <div class="section">
                                <div class="option-group field">
                                  
                                  <label class="option option-primary" >

                                    <input type="checkbox" name="sous_dossiers">

                                    <span class="checkbox"></span>Appliquer les droits aux sous-dossiers</label>
                                    
                                    
                                  </div>
                                </div>
                              </div>

                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="section">
                                <label class="field prepend-icon">
                                  <textarea class="gui-textarea" id="doc_mot_cle" name="doc_mot_cle" placeholder="Mots-clés"></textarea>
                                  <label for="doc_mot_cle" class="field-icon">
                                    <i class="fa fa-key"></i>
                                  </label>
                                  <span class="input-footer text-left">
                                    <strong>Note:</strong> Notez les mots-clés pour décrire le fichier.</span>
                                  </label>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="section">
                                  <label class="field prepend-icon">
                                    <textarea class="gui-textarea" id="doc_descriptif" name="doc_descriptif" placeholder="Descriptif"></textarea>
                                    <label for="doc_mot_cle" class="field-icon">
                                      <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </label>
                                    <span class="input-footer text-left">
                                      <strong>Note:</strong> Ecrivez un paragraphe de quelques lignes pour décrire le fichier.</span>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="section-divider mb40">
                                    <span>Droits par profils</span>
                                  </div>
                                </div>
                              </div>
                              <div class="row section">
                                <div class="col-md-3">
                                  <div class="option-group field">
                                    <label class="option option-success" >
                                      <input type="checkbox" name="checked" value="checked" >

                                      <span class="checkbox" data-toggle="tooltip" data-placement="top" title="Accès"></span></label>
                                      <label class="option option-primary" >

                                        <input type="checkbox" name="checked" value="checked" >

                                        <span class="checkbox" data-toggle="tooltip" data-placement="top" title="Consultation obligatoire"></span>Formateurs</label>


                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="option-group field">
                                        <label class="option option-primary">
                                          <input type="checkbox" name="checked" value="checked">
                                          <span class="checkbox"></span>Commerciaux</label>

                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="option-group field">
                                          <label class="option option-primary">
                                            <input type="checkbox" name="checked" value="checked">
                                            <span class="checkbox"></span>Directeurs de régions</label>

                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div class="option-group field">
                                            <label class="option option-primary">
                                              <input type="checkbox" name="checked" value="checked">
                                              <span class="checkbox"></span>Ingénieurs en informatique</label>

                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="section-divider mb40">
                                              <span>Droits par sociétés</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row section">
                                          <div class="col-md-3">
                                            <div class="option-group field">
                                              <label class="option option-primary">
                                                <input type="checkbox" name="checked" value="checked">
                                                <span class="checkbox"></span>SI2P GFC</label>

                                              </div>
                                            </div>
                                            <div class="col-md-3">
                                              <div class="option-group field">
                                                <label class="option option-primary">
                                                  <input type="checkbox" name="checked" value="checked">
                                                  <span class="checkbox"></span>SI2P GN</label>

                                                </div>
                                              </div>
                                              <div class="col-md-3">
                                                <div class="option-group field">
                                                  <label class="option option-primary">
                                                    <input type="checkbox" name="checked" value="checked">
                                                    <span class="checkbox"></span>SI2P SE</label>

                                                  </div>
                                                </div>
                                                <div class="col-md-3">
                                                  <div class="option-group field">
                                                    <label class="option option-primary">
                                                      <input type="checkbox" name="checked" value="checked">
                                                      <span class="checkbox"></span>MB Formation</label>

                                                    </div>
                                                  </div>
                                                </div>



                                              </div>
                                            </div>


                                          </div>
                                        </div>

                                      </div>
                                    </div>

                                  </div>
                                </section>
                                <!-- End: Content -->
                              </section>
                            </div>
                            <!-- End: Main -->
                            <footer id="content-footer" class="affix">
                              <div class="row">
                                <div class="col-xs-3 footer-left" >
                                  <a href="etech.php" class="btn btn-default btn-sm">
                                    <i class="fa fa-long-arrow-left"></i>
                                    Retour
                                  </a>
                                </div>
                                <div class="col-xs-6 footer-middle" ></div>
                                <div class="col-xs-3 footer-right" >
                                  <button type="submit" name="submit" class="btn btn-success btn-sm">
                                    <i class='fa fa-save'></i> Enregistrer
                                  </button>
                                </div>
                              </div>
                            </footer>
                          </form>
						  
				<?php
						include "includes/footer_script.inc.php"; ?>	
                         

						<!-- validation inputs -->
						<script src="vendor/plugins/mask/jquery.mask.js"></script>
						<!-- plugin pour les masques formulaires -->
						<script src="vendor/plugins/holder/holder.min.js"></script>
						<!-- pour mettre des images de tests -->
						
						<!-- Theme Javascript -->
						<script src="assets/js/custom.js"></script>

                         

    </body>
    </html>
