 <?php
 
	include "includes/controle_acces.inc.php";
	include "includes/connexion_soc.php";
	include "modeles/mod_set_stagiaire.php";
	include "modeles/mod_get_action_sessions.php";
	include "modeles/mod_del_action_stagiaire.php";

	
	// MODIFICATION DES DONNES STAGIARES ET DE SON INSCRIPTION A UNE ACTION
	
	$erreur=0;
	
	$stagiaire_id=0;
	if(isset($_POST["stagiaire"])){
		if(!empty($_POST["stagiaire"])){
			$stagiaire_id=intval($_POST["stagiaire"]);
		}	
	}
	
	$action_id=0;
	if(isset($_POST["action"])){
		if(!empty($_POST["action"])){
			$action_id=intval($_POST["action"]);
		}	
	}
	if($stagiaire_id==0 OR $action_id==0){
		$erreur==1;
	}	
	if($erreur==0){
		
		$maj=set_stagiaire($stagiaire_id,$_POST["titre"],$_POST["nom"],$_POST["prenom"],$_POST["ad1"],$_POST["ad2"],$_POST["ad3"],$_POST["cp"],$_POST["ville"],$_POST["tel"],$_POST["portable"],$_POST["mail_perso"],$_POST["naissance"],$_POST["cp_naiss"],$_POST["ville_naiss"],$_POST["mail"]);
		if($maj===FALSE){
			$erreur==1;
		}
		
		$sql="SELECT ast_action_client,ast_confirme FROM Actions_Stagiaires WHERE ast_action=:action AND ast_stagiaire=:stagiaire;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$action);
		$req->bindParam(":stagiaire",$stagiaire);
		$req->execute();
		$result=$req->fetch();
		if(!empty($result)){
			$action_client=$result["ast_action_client"];
			$stagiaire_confirme=$result["ast_confirme"];
		}
		
		$sql_verif="SELECT ass_stagiaire FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire AND ass_action=:action AND ass_session=:session;";
		$req_verif=$ConnSoc->prepare($sql_verif);
		
		$sql_add="INSERT INTO Actions_Stagiaires_Sessions (ass_stagiaire, ass_action, ass_session) VALUES (:stagiaire, :action, :session);";
		$req_add=$ConnSoc->prepare($sql_add);
		
		$sql_del="DELETE FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire AND ass_action=:action AND ass_session=:session;";
		$req_del=$ConnSoc->prepare($sql_del);
		
		$inscrit=false;
		
		$sessions=get_action_sessions($action_id,$stagiaire_id);
		if(!empty($sessions)){
			foreach($sessions as $d){
				
				//var_dump($d["ase_id"]);
				
				// on verifie l'etat d'inscription
				
				$req_verif->bindParam(":stagiaire",$stagiaire_id);
				$req_verif->bindParam(":action",$action_id);
				$req_verif->bindParam(":session",$d["ase_id"]);
				$req_verif->execute();			
				$inscription=$req_verif->fetch();
				
				if(!empty($inscription)){
					if(empty($_POST["session_" . $d["ase_id"]]) ){
						// le stagiaire n'est plus inscrit
						$req_del->bindParam(":stagiaire",$stagiaire_id);
						$req_del->bindParam(":action",$action_id);
						$req_del->bindParam(":session",$d["ase_id"]);
						$req_del->execute();
						echo("<br/>Suppression");
						
					}else{
						$inscrit=true;	
						//echo("<br/>Reste inscrit");
					}
				}elseif(!empty($_POST["session_" . $d["ase_id"]])){
					
					// le stagiaire doit être inscrit
					
					//echo("<br/>Inscription");
					
					$req_add->bindParam(":stagiaire",$stagiaire_id);
					$req_add->bindParam(":action",$action_id);
					$req_add->bindParam(":session",$d["ase_id"]);
					$req_add->execute();
					$inscrit=true;	
				}

				
			}
		}
		//var_dump($inscrit);
		//die();
		if(!$inscrit){
			$result=del_action_stagiaire($stagiaire_id,$action_id,0);
			if($result===false){
				$erreur=1;
			}
		}

		
	} 
	
	header("location:action_voir.php?action=" . $action_id . "&erreur=" . $erreur . "&onglet=1");
?>