<?php 
/////////////////// MENU ACTIF /////////////////////// 
$menu_actif = 5;
/////////////////// INCLUDES /////////////////////////

include "includes/controle_acces.inc.php";
include('includes/connexion.php');

$intervenant_id = 0;
if(!empty($_GET["id"])){
    $intervenant_id = intval($_GET["id"]);
}
$fournisseur_id = 0;
if(!empty($_GET["fournisseur"])){
    $fournisseur_id = intval($_GET["fournisseur"]);
}


if($fournisseur_id == 0){
	echo("Impossible d'afficher la page");
	die();
}

///////////////////////// REQUETES A LA BDD /////////////////
	
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}

	// LE FOURNISSEUR AVEC CONTROLE ACCES

	$sql="SELECT fou_nom FROM fournisseurs";
	$sql.=" WHERE fou_id=:fournisseur_id;";
	$req = $Conn->prepare($sql);
	$req->bindParam(':fournisseur_id', $fournisseur_id);
	$req->execute();
	$d_fournisseur = $req->fetch();
	if(empty($d_fournisseur)){
		echo("Impossible d'afficher la page");
		die();
	}

	// L'INTERVENANT
	if(!empty($intervenant_id)){
		
		// si c'est un dition, aller chercher l'intervenant
		$req = $Conn->prepare("SELECT * FROM fournisseurs_intervenants WHERE fin_id = " . $intervenant_id);
		$req->execute();
		$int = $req->fetch();
	}

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Fournisseurs</title>
	<meta name="keywords" content=""/>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body class="sb-top sb-top-sm ">

<form method="post" action="fournisseur_intervenant_enr.php" >
	<input type="hidden" name="fournisseur" value="<?=$fournisseur_id?>" >
	<input type="hidden" name="intervenant" value="<?=$intervenant_id?>" >
	<div id="main">
<?php 	include "includes/header_def.inc.php"; ?>
		<section id="content_wrapper" >
			<section id="content" class="animated fadeIn">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="admin-form theme-primary ">
							<div class="panel heading-border panel-primary">
								<div class="panel-body bg-light text-center">
									<div class="content-header">
										<h2>
									<?php	echo($d_fournisseur["fou_nom"]);
											if(!empty($intervenant_id)){
												echo(" Edition d'un ");
											}else{
												echo(" Nouvel ");
											} ?>
											<b class="text-primary">intervenant</b>
										</h2>            
									</div>
									<div class="col-md-10 col-md-offset-1">

										<div class="row">
											<div class="col-md-12">
												<div class="section-divider mb40">
													<span>Informations générales</span>
												</div>
											</div>
										</div>									
										<div class="col-md-4">
											<div class="section">
												<div class="field prepend-icon">
													<input type="text" name="fin_nom" id="fin_nom" class="gui-input nom" placeholder="Nom"
													<?php if(isset($_GET['id'])){ ?>
														value="<?= $int['fin_nom'] ?>"
													<?php } ?>
													>
													<label for="fou_code" class="field-icon">
														<i class="fa fa-user"></i>
													</label>
												</div>
											</div>
										</div>

										<div class="col-md-8">
											<div class="section">
												<div class="field prepend-icon">
													<input type="text" name="fin_prenom" id="fin_prenom" class="gui-input prenom" placeholder="Prénom"
														<?php if(isset($_GET['id'])){ ?>
															value="<?= $int['fin_prenom'] ?>"
														<?php } ?>
													>
													<label for="fin_prenom" class="field-icon">
														<i class="fa fa-user" aria-hidden="true"></i>
													</label>
												</div>
											</div>    
										</div>
									 
										<div class="col-md-12">
											<div class="section">
												<div class="field prepend-icon">
													<input type="text" name="fin_ad1" id="fin_ad1" class="gui-input" placeholder="Adresse"
														<?php if(isset($_GET['id'])){ ?>
															value="<?= $int['fin_ad1'] ?>"
														<?php } ?>
													>
													<label for="fin_ad1" class="field-icon">
														<i class="fa fa-map-marker"></i>
													</label>
												</div>
											</div>
										</div>
									
									
										<div class="col-md-12">
											<div class="section">
												<div class="field prepend-icon">
													<input type="text" name="fin_ad2" class="gui-input" id="fin_ad2" placeholder="Adresse (Complément 1)"
													<?php if(isset($_GET['id'])){ ?>
														value="<?= $int['fin_ad2'] ?>"
													<?php } ?>
													>
													<label for="fin_ad2" class="field-icon">
														<i class="fa fa-map-marker"></i>
													</label>
												</div>
											</div>
										</div>
									
									
										<div class="col-md-12">
											<div class="section">
												<div class="field prepend-icon">
													<input type="text" name="fin_ad3" id="fin_ad3" class="gui-input" placeholder="Adresse (Complément 2)"
													<?php if(isset($_GET['id'])){ ?>
														value="<?= $int['fin_ad3'] ?>"
													<?php } ?>
													>
													<label for="fin_ad3" class="field-icon">
														<i class="fa fa-map-marker"></i>
													</label>
												</div>
											</div>
										</div>
																	
										<div class="col-md-4">
											<div class="section">
												<div class="field prepend-icon">
													<input type="text" name="fin_cp" id="fin_cp" class="gui-input code-postal" placeholder="Code postal"
													<?php if(isset($_GET['id'])){ ?>
														value="<?= $int['fin_cp'] ?>"
													<?php } ?>
													>
													<label for="fin_cp" class="field-icon">
														<i class="fa fa-map-marker" aria-hidden="true"></i>
													</label>
												</div>
											</div>
										</div>
										<div class="col-md-8">
											<div class="section">
												<div class="field prepend-icon">
													<input type="text" name="fin_ville" id="fin_ville" class="gui-input nom" placeholder="Ville"
													<?php if(isset($_GET['id'])){ ?>
														value="<?= $int['fin_ville'] ?>"
													<?php } ?>
													>
													<label for="fin_ville" class="field-icon">
														<i class="fa fa-map-marker" aria-hidden="true"></i>
													</label>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
												<div class="section-divider mb40">
													<span>Contact</span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="section">
												<div class="field prepend-icon">
													<input type="text" name="fin_tel" id="fin_tel" class="gui-input telephone" placeholder="Téléphone"
													<?php if(isset($_GET['id'])){ ?>
														value="<?= $int['fin_tel'] ?>"
													<?php } ?>
													>
													<label for="fin_tel" class="field-icon">
														<i class="fa fa-phone" aria-hidden="true"></i>
													</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="section">
												<div class="field prepend-icon">
													<input type="text" name="fin_portable" id="fin_portable" class="gui-input telephone" placeholder="Portable"
													<?php if(isset($_GET['id'])){ ?>
														value="<?= $int['fin_portable'] ?>"
													<?php } ?>
													>
													<label for="fin_portable" class="field-icon">
														<i class="fa fa-phone" aria-hidden="true"></i> 
													</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="section"> 
												<div class="field prepend-icon">
													<input type="text" name="fin_fax" id="fin_fax" class="gui-input telephone" placeholder="Fax"
													<?php if(isset($_GET['id'])){ ?>
														value="<?= $int['fin_fax'] ?>"
													<?php } ?>
													>
													<label for="fin_fax" class="field-icon">
														<i class="fa fa-fax" aria-hidden="true"></i>
													</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="section">
												<div class="field prepend-icon">
													<input type="email" name="fin_mail" id="fin_mail" class="gui-input" placeholder="Email"
													<?php if(isset($_GET['id'])){ ?>
														value="<?= $int['fin_mail'] ?>"
													<?php } ?>
													>
													<label for="fin_mail" class="field-icon">
														<i class="fa fa-envelope" aria-hidden="true"></i>
													</label>
												</div>
											</div>
										</div>
							<?php 	if(!empty($intervenant_id)){ ?>
										<div class="row">
											<div class="col-md-12">
												<div class="section-divider mb40">
													<span>Autre</span>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="section">
													<div class="checkbox-custom checkbox-default mb5">
													  <input type="checkbox" name="fin_archive" value="1" id="fin_archive" <?php if($int["fin_archive"]) echo("checked"); ?> >
													  <label for="fin_archive">Archivé</label>
													</div>
												</div>
											</div>
										</div>
							<?php 	} ?>



										
									</div>
									<!-- FIN COL-MD-10 -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</section>
	</div>
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">
            	<a 
                <?php if(isset($_GET['intervenant'])){ ?>
                    href="fournisseur_voir.php?fournisseur=<?= $_GET['fournisseur'] ?>&tab=5&intervenant=<?= $_GET['intervenant'] ?>"
                <?php }else{ ?>
                    href="fournisseur_voir.php?fournisseur=<?= $_GET['fournisseur'] ?>&tab=5"
                <?php } ?>
                 class="btn btn-default btn-sm">
                    <i class='fa fa-long-arrow-left'></i> Retour
                </a>
            </div>
            <div class="col-xs-6 footer-middle">
 
            </div>
            <div class="col-xs-3 footer-right">
                <button type="submit" class="btn btn-success btn-sm">
                    <i class='fa fa-plus'></i> Enregistrer
                </button>
                
            </div>
        </div>
    </footer>
</form>

<?php 
	include "includes/footer_script.inc.php"; ?>  

	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script src="assets/js/custom.js"></script>
	<script type="text/javascript">
	
	</script>
</body>
</html>