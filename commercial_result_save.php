<?php
$menu_actif = 1;
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");;
include_once("includes/connexion_soc.php");
include_once("modeles/mod_parametre.php");

// PARAMETRE
$commercial=0;
if(isset($_GET["commercial"])){
	if(!empty($_GET["commercial"])){
		$commercial=intval($_GET["commercial"]);
	}
}
$com_type=0;
if(isset($_GET["com_type"])){
	if(!empty($_GET["com_type"])){
		$com_type=intval($_GET["com_type"]);
	}
}
$agence=0;
if(isset($_GET["agence"])){
	if(!empty($_GET["agence"])){
		$agence=intval($_GET["agence"]);
	}
}
$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}

// PERSONNE CONNECTE
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	} 
}

// CONTROLE D'ACCESS

if($_SESSION["acces"]["acc_profil"]==3 AND $commercial==0){
	header("location:deconnect.php");
	die();
}elseif($agence>0){
	if($acc_agence>0 AND $agence!=$acc_agence){
		header("location:deconnect.php");
		die();
	}
}elseif(empty($agence) AND empty($commercial)){
	if(!isset($_SESSION["acces"]["acc_liste_societe"][$acc_societe . "-0"])){
		header("location:deconnect.php");
		die();
	}
}

// VALEUR PAR DEFAUT

if($exercice==0){
	if(date("n")<4){
		$exercice=date("Y")-1;
	}else{
		$exercice=date("Y");
	}
}
$exercice_fin=$exercice+1;

// CATEGORIE
$d_categories=array(
	"0" => "Divers"
);
$sql="SELECT pca_id,pca_libelle FROM Produits_Categories ORDER BY pca_id;";
$req=$Conn->query($sql);
$d_result_cat=$req->fetchAll();
if(!empty($d_result_cat)){
	foreach($d_result_cat as $cat){
		$d_categories[$cat["pca_id"]]=$cat["pca_libelle"];
	}	
}
// Famille
$d_familles=array();
$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles  ORDER BY pfa_id;";
$req=$Conn->query($sql);
$d_result_fam=$req->fetchAll();
if(!empty($d_result_fam)){
	foreach($d_result_fam as $fam){
		$d_familles[$fam["pfa_id"]]=$fam["pfa_libelle"];
	}	
}
// Sous-Famille
$d_s_familles=array();
$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles ORDER BY psf_id;";
$req=$Conn->query($sql);
$d_result_s_fam=$req->fetchAll();
if(!empty($d_result_s_fam)){
	foreach($d_result_s_fam as $s_fam){
		$d_s_familles[$s_fam["psf_id"]]=$s_fam["psf_libelle"];
	}	
}
// Sous-Sous-Famille
$d_s_s_familles=array();
$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles ORDER BY pss_id;";
$req=$Conn->query($sql);
$d_result_s_s_fam=$req->fetchAll();
if(!empty($d_result_s_s_fam)){
	foreach($d_result_s_s_fam as $s_s_fam){
		$d_s_s_familles[$s_s_fam["pss_id"]]=$s_s_fam["pss_libelle"];
	}	
}


// LE COMMERCIAL
if($commercial>0){
	$sql="SELECT com_label_1,com_label_2,com_ref_1,com_agence FROM Commerciaux WHERE com_id=" . $commercial;
	if($_SESSION["acces"]["acc_profil"]==3){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif($acc_agence>0){
		$sql.=" AND com_agence=" . $acc_agence;
	}
	$sql.=";";
	$req=$ConnSoc->query($sql);
	$d_commercial=$req->fetch();
	if(!empty($d_commercial)){
		$titre="Résultat de " . $d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"] . " pour l'exercice " . $exercice . "/" . $exercice_fin; 
	}else{
		echo("impossible d'afficher la page!");
		die();
	}
}elseif($com_type>0){
	
	if($com_type==1){
		$titre="Résultat Vendeurs pour l'exercice " . $exercice . "/" . $exercice_fin; 
		
	}elseif($com_type==2){
		$titre="Résultat Revendeurs pour l'exercice " . $exercice . "/" . $exercice_fin; 
		
	}elseif($com_type==3){
		$titre="Résultat Indivis pour l'exercice " . $exercice . "/" . $exercice_fin; 
		
	}
	
}elseif($agence>0){
	$sql="SELECT age_nom FROM Agences WHERE age_id=" . $agence . ";";
	$req=$Conn->query($sql);
	$d_agence=$req->fetch();
	if(!empty($d_agence)){
		$titre="Résultat " . $d_agence["age_nom"] . " pour l'exercice " . $exercice . "/" . $exercice_fin; 
	}	
}else{
	$sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
	$req=$Conn->query($sql);
	$d_societe=$req->fetch();
	if(!empty($d_societe)){
		$titre="Résultat " . $d_societe["soc_nom"] . " pour l'exercice " . $exercice . "/" . $exercice_fin; 
	}	
}

// LES OBJECTIFS

// [0] clé divers cree ici au cas ou pas d'objectifs et du CA
$resultats=array(
	0 => array(
		0 => array(
			0 => array(
				0 => array(
					"objectif" => array(
						"mois_1" => 0,
						"mois_2" => 0,
						"mois_3" => 0,
						"mois_4" => 0,
						"mois_5" => 0,
						"mois_6" => 0,
						"mois_7" => 0,
						"mois_8" => 0,
						"mois_9" => 0,
						"mois_10" => 0,
						"mois_11" => 0,
						"mois_12" => 0,
						"total" => 0
					),
					"resultat" => array(
						"mois_1" => 0,
						"mois_2" => 0,
						"mois_3" => 0,
						"mois_4" => 0,
						"mois_5" => 0,
						"mois_6" => 0,
						"mois_7" => 0,
						"mois_8" => 0,
						"mois_9" => 0,
						"mois_10" => 0,
						"mois_11" => 0,
						"mois_12" => 0,
						"total" => 0
					),
					"libelle" => "Divers",
					"lib_detail" => "",
					"autre" => 1
				)
			)
		)
	),
	"total" => array(
		"objectif" => array(
			"mois_1" => 0,
			"mois_2" => 0,
			"mois_3" => 0,
			"mois_4" => 0,
			"mois_5" => 0,
			"mois_6" => 0,
			"mois_7" => 0,
			"mois_8" => 0,
			"mois_9" => 0,
			"mois_10" => 0,
			"mois_11" => 0,
			"mois_12" => 0,
			"total" => 0
		),
		"resultat" => array(
			"mois_1" => 0,
			"mois_2" => 0,
			"mois_3" => 0,
			"mois_4" => 0,
			"mois_5" => 0,
			"mois_6" => 0,
			"mois_7" => 0,
			"mois_8" => 0,
			"mois_9" => 0,
			"mois_10" => 0,
			"mois_11" => 0,
			"mois_12" => 0,
			"total" => 0
		),
		"libelle" => "Total",
		"lib_detail" => ""	
	),
	"conso" => array(
		"objectif" => array(
			"mois_1" => 0,
			"mois_2" => 0,
			"mois_3" => 0,
			"mois_4" => 0,
			"mois_5" => 0,
			"mois_6" => 0,
			"mois_7" => 0,
			"mois_8" => 0,
			"mois_9" => 0,
			"mois_10" => 0,
			"mois_11" => 0,
			"mois_12" => 0,
			"total" => 0
		),
		"resultat" => array(
			"mois_1" => 0,
			"mois_2" => 0,
			"mois_3" => 0,
			"mois_4" => 0,
			"mois_5" => 0,
			"mois_6" => 0,
			"mois_7" => 0,
			"mois_8" => 0,
			"mois_9" => 0,
			"mois_10" => 0,
			"mois_11" => 0,
			"mois_12" => 0,
			"total" => 0
		),
		"libelle" => "Autres",
		"lib_detail" => ""
	)
);

$cob_valide=false;
$cob_valide_date=null;
$cob_valide_utilisateur=0;

$sql="SELECT cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille,
SUM(cob_objectif_1) AS objectif_1,SUM(cob_objectif_2) AS objectif_2,SUM(cob_objectif_3) AS objectif_3,
SUM(cob_objectif_4) AS objectif_4,SUM(cob_objectif_5) AS objectif_5,SUM(cob_objectif_6) AS objectif_6,
SUM(cob_objectif_7) AS objectif_7,SUM(cob_objectif_8) AS objectif_8,SUM(cob_objectif_9) AS objectif_9,
SUM(cob_objectif_10) AS objectif_10,SUM(cob_objectif_11) AS objectif_11,SUM(cob_objectif_12) AS objectif_12,
SUM(cob_objectif_total) AS objectif_total,cob_valide,cob_valide_date,cob_valide_utilisateur,cob_autre
FROM Commerciaux 
LEFT JOIN Commerciaux_Objectifs ON (Commerciaux.com_id=Commerciaux_Objectifs.cob_commercial)
WHERE cob_exercice=" . $exercice;
// restriction d'accès 
if($_SESSION['acces']["acc_profil"]==3){
	$sql.=" AND com_ref_1=" . $acc_utilisateur;
}elseif($acc_agence>0){
	$sql.=" AND com_agence=" . $acc_agence;
}
// gestion des filtres
if($commercial>0){
	$sql.=" AND com_id=" . $commercial;
}elseif($com_type>0){
	$sql.=" AND com_type=" . $com_type;
}elseif($agence>0){
	$sql.=" AND com_agence=" . $agence;
}
$sql.=" GROUP BY cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille,cob_valide,cob_valide_date,cob_valide_utilisateur,cob_autre ORDER BY SUM(cob_objectif_total) DESC;";
$req=$ConnSoc->query($sql);
$d_objectifs=$req->fetchAll();
if(!empty($d_objectifs)){
	foreach($d_objectifs as $obj){
		
		$cob_valide=$obj["cob_valide"];
		$cob_valide_date=$obj["cob_valide_date"];
		$cob_valide_utilisateur=$obj["cob_valide_utilisateur"];
		
		$lib_detail="";
		$libelle="";
		if(!empty($obj["cob_sous_sous_famille"])){
			$lib_detail=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]] . "/" .$d_s_familles[$obj["cob_sous_famille"]] . "/" .$d_s_s_familles[$obj["cob_sous_sous_famille"]];	
			$libelle=$d_s_s_familles[$obj["cob_sous_sous_famille"]];
		}elseif(!empty($obj["cob_sous_famille"])){
			$lib_detail=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]] . "/" .$d_s_familles[$obj["cob_sous_famille"]];
			$libelle=$d_s_familles[$obj["cob_sous_famille"]];
		}elseif(!empty($obj["cob_famille"])){
			$lib_detail=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]];	
			$libelle=$d_familles[$obj["cob_famille"]];
		}else{
			$libelle=$d_categories[$obj["cob_categorie"]];		
		}
		
		$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]=array(
			"objectif" => array(
				"mois_1" => $obj["objectif_1"],
				"mois_2" => $obj["objectif_2"],
				"mois_3" => $obj["objectif_3"],
				"mois_4" => $obj["objectif_4"],
				"mois_5" => $obj["objectif_5"],
				"mois_6" => $obj["objectif_6"],
				"mois_7" => $obj["objectif_7"],
				"mois_8" => $obj["objectif_8"],
				"mois_9" => $obj["objectif_9"],
				"mois_10" => $obj["objectif_10"],
				"mois_11" => $obj["objectif_11"],
				"mois_12" => $obj["objectif_12"],
				"total" => $obj["objectif_total"],
			),
			"resultat" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"libelle" => $libelle,
			"lib_detail" => $lib_detail,
			"autre" => $obj["cob_autre"]
		);
		
		if($obj["cob_autre"]){
			
			// conso autre
			$resultats["conso"]["objectif"]["mois_1"]+=$obj["objectif_1"];
			$resultats["conso"]["objectif"]["mois_2"]+=$obj["objectif_2"];
			$resultats["conso"]["objectif"]["mois_3"]+=$obj["objectif_3"];
			$resultats["conso"]["objectif"]["mois_4"]+=$obj["objectif_4"];
			$resultats["conso"]["objectif"]["mois_5"]+=$obj["objectif_5"];
			$resultats["conso"]["objectif"]["mois_6"]+=$obj["objectif_6"];
			$resultats["conso"]["objectif"]["mois_7"]+=$obj["objectif_7"];
			$resultats["conso"]["objectif"]["mois_8"]+=$obj["objectif_8"];
			$resultats["conso"]["objectif"]["mois_9"]+=$obj["objectif_9"];
			$resultats["conso"]["objectif"]["mois_10"]+=$obj["objectif_10"];
			$resultats["conso"]["objectif"]["mois_11"]+=$obj["objectif_11"];
			$resultats["conso"]["objectif"]["mois_12"]+=$obj["objectif_12"];
			$resultats["conso"]["objectif"]["total"]+=$obj["objectif_total"];

		}
		// on totalise
		$resultats["total"]["objectif"]["mois_1"]+=$obj["objectif_1"];
		$resultats["total"]["objectif"]["mois_2"]+=$obj["objectif_2"];
		$resultats["total"]["objectif"]["mois_3"]+=$obj["objectif_3"];
		$resultats["total"]["objectif"]["mois_4"]+=$obj["objectif_4"];
		$resultats["total"]["objectif"]["mois_5"]+=$obj["objectif_5"];
		$resultats["total"]["objectif"]["mois_6"]+=$obj["objectif_6"];
		$resultats["total"]["objectif"]["mois_7"]+=$obj["objectif_7"];
		$resultats["total"]["objectif"]["mois_8"]+=$obj["objectif_8"];
		$resultats["total"]["objectif"]["mois_9"]+=$obj["objectif_9"];
		$resultats["total"]["objectif"]["mois_10"]+=$obj["objectif_10"];
		$resultats["total"]["objectif"]["mois_11"]+=$obj["objectif_11"];
		$resultats["total"]["objectif"]["mois_12"]+=$obj["objectif_12"];
		$resultats["total"]["objectif"]["total"]+=$obj["objectif_total"];

	} 
}

// LE FACTURES

$sql="SELECT fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,MONTH(fac_date) AS mois,fac_date,fac_id,
SUM(fli_montant_ht) AS montant_ht FROM Factures_Lignes
LEFT JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
WHERE fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'";
$sql.=" AND fli_categorie<4";
if($_SESSION['acces']["acc_profil"]==3){
	$sql.=" AND com_ref_1=" . $acc_utilisateur;
}elseif($acc_agence>0){
	$sql.=" AND com_agence=" . $acc_agence;
}
if($commercial>0){
	$sql.=" AND com_id=" . $commercial;
}elseif($com_type>0){
	$sql.=" AND com_type=" . $com_type;
}elseif($agence>0){
	$sql.=" AND fac_agence=" . $agence;
}
$sql.=" GROUP BY fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,MONTH(fac_date),fac_date,fac_id;";
$req=$ConnSoc->query($sql);
$d_realises=$req->fetchAll();
if(!empty($d_realises)){
	foreach($d_realises as $realise){
		
		$cat_id=0;
		if(!empty($realise["fli_categorie"])){
			$cat_id=$realise["fli_categorie"];
		}
		$fam_id=0;
		if(!empty($realise["fli_famille"])){
			$fam_id=$realise["fli_famille"];
		}
		$s_fam_id=0;
		if(!empty($realise["fli_sous_famille"])){
			$s_fam_id=$realise["fli_sous_famille"];
		}
		$s_s_fam_id=0;
		if(!empty($realise["fli_sous_sous_famille"])){
			$s_s_fam_id=$realise["fli_sous_sous_famille"];
		}
		$mois=0;
		if(!empty($realise["mois"])){
			$mois=$realise["mois"];
		}
		$montant_ht=0;
		if(!empty($realise["montant_ht"])){
			$montant_ht=$realise["montant_ht"];
		}
		
		if(!empty($resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["objectif"]["total"])){
			$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat"]["total"]+=$montant_ht;
			if($resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["autre"]){
				$resultats["conso"]["resultat"]["mois_" . $mois]+=$montant_ht;
				$resultats["conso"]["resultat"]["total"]+=$montant_ht;
			}
		}elseif(!empty($resultats[$cat_id][$fam_id][$s_fam_id][0]["objectif"]["total"])){
			$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat"]["total"]+=$montant_ht;
			if($resultats[$cat_id][$fam_id][$s_fam_id][0]["autre"]){
				$resultats["conso"]["resultat"]["mois_" . $mois]+=$montant_ht;
				$resultats["conso"]["resultat"]["total"]+=$montant_ht;
			}
			
		}elseif(!empty($resultats[$cat_id][$fam_id][0][0]["objectif"]["total"])){
			$resultats[$cat_id][$fam_id][0][0]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats[$cat_id][$fam_id][0][0]["resultat"]["total"]+=$montant_ht;
			if($resultats[$cat_id][$fam_id][0][0]["autre"]){
				$resultats["conso"]["resultat"]["mois_" . $mois]+=$montant_ht;
				$resultats["conso"]["resultat"]["total"]+=$montant_ht;
			}
		}elseif(!empty($resultats[$cat_id][0][0][0]["objectif"]["total"])){
			$resultats[$cat_id][0][0][0]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats[$cat_id][0][0][0]["resultat"]["total"]+=$montant_ht;
			if($resultats[$cat_id][0][0][0]["autre"]){
				$resultats["conso"]["resultat"]["mois_" . $mois]+=$montant_ht;
				$resultats["conso"]["resultat"]["total"]+=$montant_ht;
			}
		}else{
			// divers CA SANS OBJECTIFS
			$resultats[0][0][0][0]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats[0][0][0][0]["resultat"]["total"]+=$montant_ht;
			$resultats["conso"]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats["conso"]["resultat"]["total"]+=$montant_ht;
		}
		
		
		$resultats["total"]["resultat"]["mois_" . $mois]+=$montant_ht;
		$resultats["total"]["resultat"]["total"]+=$montant_ht;

	}
}

// VALIDATION 

if($cob_valide_utilisateur>0){
	$sql="SELECT uti_prenom,uti_nom FROM Utilisateurs WHERE uti_id=" . $cob_valide_utilisateur . ";";
	$req=$Conn->query($sql);
	$d_validateur=$req->fetch();
}
//die();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			.objectif{
				color:#AA00AA;
			}
			.resultat{
				font-weight:bold;
			}
			.resultat a{
				color:#000;
			}
			.ligne-strip{
				background-color:#FFF;
			}
		</style>
		<style type="text/css" >
			@page{
				size:A4 landscape;
				margin:1cm;
			}
			html{
				height:0px!important;
				min-height:0px!important;
				background-color:#FFF!important;
			}
			body{
				height:0px!important;
				min-height:0px!important;
				background-color:#FFF!important;
			}
			#main{
				height:0px!important;
				min-height:0px!important;
				background-color:#FFF!important;
			}
			#content_wrapper{
				background-color:#FFF!important;
				padding:0px!important;
			}
			#content{
				background-color:FFF!important;
			}
			.table{
				background-color:#eee!important;
			}
			.ligne-strip{
				background-color:#FFF;
			}
			.d-print-none{
				display:none;
			}
		</style>
	</head>

	<body class="sb-top sb-top-sm" >
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php		if(empty($d_objectifs)){ ?>
						<p class="alert alert-danger text-center" >
							Objectifs non disponible!
						</p>
		<?php		} ?>
						
						
					<form method="get" action="commercial_result.php" id="filtre"  class="d-print-none" >
						<div class="admin-form theme-primary mb25">
							<div class="row" >
								<div class="col-md-offset-3 col-md-6" >
									<input type="hidden" name="commercial" value="<?=$commercial?>" />
									<input type="hidden" name="com_type" value="<?=$com_type?>" />
									<input type="hidden" name="agence" value="<?=$agence?>" />					
									<label for="exercice" >Choix de l'exercice :</label>
									<select class="select" id="exercice" name="exercice" >
								<?php	for($i=2017;$i<=intval(date("Y")+1);$i++){
											if($i==$exercice){
												echo("<option value='" . $i . "' selected >" . $i . "/" . intval($i+1) . "</option>");
											}else{
												echo("<option value='" . $i . "' >" . $i . "/" . intval($i+1) . "</option>");	
											} 
										}?>
									</select>										
								</div>
							</div>
						</div>
					</form>

					<h1><?=$titre?></h1>
					<div class="table-responsive">
						<table>
							<thead>
								<tr class="dark2" >
									<th colspan="2" >&nbsp;</th>
									<th>Avril</th>
									<th>Mai</th>
									<th>Juin</th>
									<th>Juillet</th>
									<th>Août</th>
									<th>Septembre</th>
									<th>Octobre</th>
									<th>Novembre</th>
									<th>Décembre</th>
									<th>Janvier</th>
									<th>Fevrier</th>							
									<th>Mars</th>
									<th>Total</th>									
								</tr>
							</thead>					
				<?php		$numLigne=0;
							$aff_conso=false;
							if(!empty($resultats)){ ?>
								<tbody>
				<?php				foreach($resultats as $cat => $categorie){ 
										if($cat!="conso" AND $cat!="total" AND $cat!=0){
											foreach($categorie as $fam => $famille){
												foreach($famille as $s_fam => $s_famille){
													foreach($s_famille as $s_s_fam => $s_s_famille){
														
														if($s_s_famille["autre"]==0){
															if(!empty($s_s_famille["objectif"]["total"])){ 
																$numLigne++;
																$classLigne="";
																if($numLigne % 2 ==0){
																	$classLigne="ligne-strip";	
																} 
																if($s_s_famille["autre"]){
																	$classLigne.=" conso-detail hidden";	
																} ?>
																<tr class="<?=$classLigne?>" >
																	<th rowspan="2" >
															<?php		if(!empty($s_s_famille["lib_detail"])){ ?>
																			<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Classification : <?=$s_s_famille["lib_detail"]?>" >
																				<i class="fa fa-info" ></i>
																			</button>
															<?php		} ?>	
																		<?=$s_s_famille["libelle"]?>																												
																	</th>
																	<th class="objectif"  >Objectifs</th>
															<?php	for($i=4;$i<=12;$i++){ ?>
																		<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_" . $i]?></td>
															<?php	} ?>
																	<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_1"]?></td>
																	<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_2"]?></td>
																	<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_3"]?></td>
																	<td class="objectif text-right" ><?=$s_s_famille["objectif"]["total"]?></td>
																</tr>
																<tr class="resultat <?=$classLigne?>" >																
																	<th>Résultats</th>
															<?php	for($i=4;$i<=12;$i++){ ?>
																		<td class="resultat text-right" >
																			<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>" >
																				<?=$s_s_famille["resultat"]["mois_" . $i]?>
																			</a>
																		</td>
															<?php	} ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>" >
																			<?=$s_s_famille["resultat"]["mois_1"]?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>" >
																			<?=$s_s_famille["resultat"]["mois_2"]?>
																		</a>	
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>" >
																			<?=$s_s_famille["resultat"]["mois_3"]?>
																		</a>	
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>" >
																			<?=$s_s_famille["resultat"]["total"]?>
																		</a>	
																	</td>
																</tr>
													<?php	} 
														}
													}
												}
											}
										}
									}

									// LIGNE CONSO				
									$numLigne++;
									$classLigne="";
									if($numLigne % 2 ==0){
										$classLigne="ligne-strip";	
									} ?>
									<tr class="<?=$classLigne?>" >
										<th rowspan="2" >
											<button type="button" class="btn btn-default btn-sm" id="conso_plus"  >
												<i id="ico_conso" class="fa fa-plus" ></i>
											</button>
											<?=$resultats["conso"]["libelle"]?>
										</th>
										<th class="objectif"  >Objectifs</th>
								<?php	for($i=4;$i<=12;$i++){ ?>
											<td class="objectif text-right" ><?=$resultats["conso"]["objectif"]["mois_" . $i]?></td>
								<?php	} ?>
										<td class="objectif text-right" ><?=$resultats["conso"]["objectif"]["mois_1"]?></td>
										<td class="objectif text-right" ><?=$resultats["conso"]["objectif"]["mois_2"]?></td>
										<td class="objectif text-right" ><?=$resultats["conso"]["objectif"]["mois_3"]?></td>
										<td class="objectif text-right" ><?=$resultats["conso"]["objectif"]["total"]?></td>
									</tr>
									<tr class="resultat <?=$classLigne?>" >																
										<th>Résultats</th>
								<?php	for($i=4;$i<=12;$i++){ ?>
											<td class="resultat text-right" >
												<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=conso" >
													<?=$resultats["conso"]["resultat"]["mois_" . $i]?>
												</a>
											</td>
								<?php	} ?>
										<td class="resultat text-right" >
											<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=conso" >
												<?=$resultats["conso"]["resultat"]["mois_1"]?>
											</a>
										</td>
										<td class="resultat text-right" >
											<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=conso" >
												<?=$resultats["conso"]["resultat"]["mois_2"]?>
											</a>	
										</td>
										<td class="resultat text-right" >
											<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=conso" >
												<?=$resultats["conso"]["resultat"]["mois_3"]?>
											</a>	
										</td>
										<td class="resultat text-right" >
											<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=conso" >
												<?=$resultats["conso"]["resultat"]["total"]?>
											</a>	
										</td>
									</tr>
			<?php					// DETAIL CONSO
									foreach($resultats as $cat => $categorie){ 
										if($cat!="conso" AND $cat!="total" AND $cat!=0){
											foreach($categorie as $fam => $famille){
												foreach($famille as $s_fam => $s_famille){
													foreach($s_famille as $s_s_fam => $s_s_famille){														
														if($s_s_famille["autre"]==1){
															if(!empty($s_s_famille["objectif"]["total"])){ 
																$numLigne++;
																$classLigne="";
																if($numLigne % 2 ==0){
																	$classLigne="ligne-strip";	
																} 
																if($s_s_famille["autre"]){
																	$classLigne.=" conso-detail hidden";	
																} ?>
																<tr class="<?=$classLigne?>" >
																	<th rowspan="2" >
																<?php	if(!empty($s_s_famille["lib_detail"])){ ?>
																			<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Classification : <?=$s_s_famille["lib_detail"]?>" >
																				<i class="fa fa-info" ></i>
																			</button>
															<?php		} ?>
																		<?=$s_s_famille["libelle"]?>															
																	</th>
																	<th class="objectif"  >Objectifs</th>
															<?php	for($i=4;$i<=12;$i++){ ?>
																		<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_" . $i]?></td>
															<?php	} ?>
																	<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_1"]?></td>
																	<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_2"]?></td>
																	<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_3"]?></td>
																	<td class="objectif text-right" ><?=$s_s_famille["objectif"]["total"]?></td>
																</tr>
																<tr class="resultat <?=$classLigne?>" >																
																	<th>Résultats</th>
															<?php	for($i=4;$i<=12;$i++){ ?>
																		<td class="resultat text-right" >
																			<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>" >
																				<?=$s_s_famille["resultat"]["mois_" . $i]?>
																			</a>
																		</td>
															<?php	} ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>" >
																			<?=$s_s_famille["resultat"]["mois_1"]?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>" >
																			<?=$s_s_famille["resultat"]["mois_2"]?>
																		</a>	
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>" >
																			<?=$s_s_famille["resultat"]["mois_3"]?>
																		</a>	
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>" >
																			<?=$s_s_famille["resultat"]["total"]?>
																		</a>	
																	</td>
																</tr>
													<?php	} 
														}
													}
												}
											}
										}
									}
									// DIVERS
									if(!empty($resultats[0][0][0][0]["resultat"]["total"])){ 
										$numLigne++;
										$classLigne="conso-detail hidden";
										if($numLigne % 2 ==0){
											$classLigne.=" ligne-strip";	
										} ?>
										<tr class="<?=$classLigne?>" >
											<th rowspan="2" ><?=$resultats[0][0][0][0]["libelle"]?></th>
											<th class="objectif"  >Objectifs</th>
									<?php	for($i=4;$i<=12;$i++){ ?>
												<td class="objectif text-right" ><?=$resultats[0][0][0][0]["objectif"]["mois_" . $i]?></td>
									<?php	} ?>
											<td class="objectif text-right" ><?=$resultats[0][0][0][0]["objectif"]["mois_1"]?></td>
											<td class="objectif text-right" ><?=$resultats[0][0][0][0]["objectif"]["mois_2"]?></td>
											<td class="objectif text-right" ><?=$resultats[0][0][0][0]["objectif"]["mois_3"]?></td>
											<td class="objectif text-right" ><?=$resultats[0][0][0][0]["objectif"]["total"]?></td>
										</tr>
										<tr class="<?=$classLigne?>" >
											<th class="resultat" >Résultats</th>
									<?php	for($i=4;$i<=12;$i++){ ?>
												<td class="resultat text-right" >
													<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=0" >
														<?=$resultats[0][0][0][0]["resultat"]["mois_" . $i]?>
													</a>
												</td>
									<?php	} ?>
											<td class="resultat text-right" >
												<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=0" >
													<?=$resultats[0][0][0][0]["resultat"]["mois_1"]?>
												</a>
											</td>
											<td class="resultat text-right" >
												<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=0" >
													<?=$resultats[0][0][0][0]["resultat"]["mois_2"]?>
												</a>
											</td>
											<td class="resultat text-right" >
												<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=0" >
													<?=$resultats[0][0][0][0]["resultat"]["mois_3"]?>
												</a>
											</td>
											<td class="resultat text-right" >
												<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=0" >
													<?=$resultats[0][0][0][0]["resultat"]["total"]?>
												</a>
											</td>
										</tr>
				<?php				}
									$numLigne++;
									$classLigne="";
									if($numLigne % 2 ==0){
										$classLigne="ligne-strip";	
									} ?>
									<tr class="<?=$classLigne?>" >
										<th rowspan="2" >Total</th>
										<th class="objectif" >Objectifs</th>
								<?php	for($i=4;$i<=12;$i++){ ?>
											<td class="objectif text-right" ><?=$resultats["total"]["objectif"]["mois_" . $i]?></td>
								<?php	} ?>
										<td class="objectif text-right" ><?=$resultats["total"]["objectif"]["mois_1"]?></td>
										<td class="objectif text-right" ><?=$resultats["total"]["objectif"]["mois_2"]?></td>
										<td class="objectif text-right" ><?=$resultats["total"]["objectif"]["mois_3"]?></td>
										<td class="objectif text-right" ><?=$resultats["total"]["objectif"]["total"]?></td>
									</tr>
									<tr class="<?=$classLigne?>" >																
										<th>Résultats</th>
								<?php	for($i=4;$i<=12;$i++){ ?>
											<td class="resultat text-right" >
												<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=total" >
													<?=$resultats["total"]["resultat"]["mois_" . $i]?>
												</a>	
											</td>
								<?php	} ?>
										<td class="resultat text-right" >
											<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=total" >
												<?=$resultats["total"]["resultat"]["mois_1"]?>
											</a>
										</td>
										<td class="resultat text-right" >
											<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=total" >
												<?=$resultats["total"]["resultat"]["mois_2"]?>
											</a>
										</td>
										<td class="resultat text-right" >
											<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=total" >
												<?=$resultats["total"]["resultat"]["mois_3"]?>
											</a>
										</td>
										<td class="resultat text-right" >
											<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=total" >
												<?=$resultats["total"]["resultat"]["total"]?>
											</a>
										</td>
									</tr>
								</tbody>
				<?php		} ?>
						</table>							
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix d-print-none" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="commercial_liste.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-left"></span>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
	<?php			if($cob_valide){ ?>
						<p class="text-center" >
							<b>Objectifs validés le <?=convert_date_txt($cob_valide_date)?> par <?=$d_validateur["uti_prenom"] . " " . $d_validateur["uti_nom"]?>.</b>
						</p>	
	<?php			} ?>
				</div>
				<div class="col-xs-3 footer-right" >
		<?php		if($commercial>0 AND (($_SESSION['acces']["acc_profil"]==5 AND !$cob_valide) OR $_SESSION['acces']["acc_profil"]==10 OR $_SESSION['acces']["acc_profil"]==13)){ ?>
						<a href="commercial_objectif.php?exercice=<?=$exercice?>&commercial=<?=$commercial?>" class="btn btn-warning btn-sm" role="button" >
							<span class="fa fa-pencil"></span>
							Mise à jour des objectifs
						</a>
		<?php		} ?>
				</div>
			</div>
		</footer>
<?php
		//include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				$("#exercice").change(function(){
					$("#filtre").submit();
				});
				
				$("#conso_plus").click(function(){
					$sous_famille=$(this).data("sous_famille");
					if($("#ico_conso").prop("class")=="fa fa-minus"){
						// on masque
						$(".conso-detail").addClass("hidden");
						$("#ico_conso").prop("class","fa fa-plus")
					}else{
						$(".conso-detail").removeClass("hidden");
						$("#ico_conso").prop("class","fa fa-minus")
					}
				});
			});						
		</script>
	
	</body>
</html>
