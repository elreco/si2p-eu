<?php

	include "includes/controle_acces.inc.php";

	include('includes/connexion.php');
	include('includes/connexion_soc.php');
	include "modeles/mod_parametre.php";
	include "modeles/mod_affacturage_fichier.php";


	$erreur_txt="";

	$remise=0;
	if(!empty($_GET["remise"])){
		$remise=intval($_GET["remise"]);
	}

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	if(!empty($remise)){

		// LA REMISE
		$sql="SELECT aff_date FROM Affacturages WHERE aff_id=" . $remise . ";";
		$req=$ConnSoc->query($sql);
		$d_remise=$req->fetch();

		// LES FACTURES

		$sql="SELECT fac_id FROM Factures WHERE fac_aff_remise=" . $remise . ";";
		$req=$ConnSoc->query($sql);
		$d_factures=$req->fetchAll();



		$fichier=affacturage_fichier($remise,$d_remise["aff_date"],$d_factures);
		
		if(is_array($fichier)){
			$erreur_txt=$fichier["texte"];
		}
	}

	if(!empty($erreur_txt)){
		$_SESSION['message'] = array(
			"aff" => "modal",
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);

	}
	header("Location: affacturage_liste.php");
