<?php

	// STATISTIQUE CA PAR PRODUIT

	include "includes/controle_acces.inc.php";

	include "includes/connexion.php";
	include "includes/connexion_soc.php";


	// CONTROLE ACCES

	if($_SESSION['acces']['acc_service'][1]!=1 AND $_SESSION['acces']['acc_service'][2]!=1 AND $_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']['acc_service'][5]!=1){
		echo("Impossible d'afficher la page!");
		die();
	}elseif(!isset($_SESSION["crit_stat"])){
		echo("Formulaire incomplet");
		die();
	}elseif(empty($_SESSION["crit_stat"])){
		echo("Formulaire incomplet");
		die();
	}
	
	// CRITERE
	
	$crit_stat=$_SESSION["crit_stat"];
	
	if(empty($crit_stat["qualification"])){
		echo("Formulaire incomplet");
		die();
	}
	
	
	
	$DT_periode=date_create_from_format('Y-m-d',$crit_stat["periode_deb"]);
	if(!is_bool($DT_periode)){
		$periode_deb_fr=$DT_periode->format("d/m/Y");
	}
	
	$DT_periode=date_create_from_format('Y-m-d',$crit_stat["periode_fin"]);
	if(!is_bool($DT_periode)){
		$periode_fin_fr=$DT_periode->format("d/m/Y");
	}
	
	$client_id=0;
	if(!empty($_GET["client"])){
		$client_id=intval($_GET["client"]);
	}
	
	// RETOUR
	
	$_SESSION["retourFacture"]="stat_com_qualif_fac.php?client_id=" . $client_id;

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}
	
	// TRAITEMENT
	
	// ON LISTE LES PRODUITS CONCERNE PAR LA Statistiques

	$sql="SELECT pro_id FROM Produits WHERE pro_qualification=:qualification;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":qualification",$crit_stat["qualification"]);
	$req->execute();
	$d_results=$req->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($d_results)){
		$tab_result=array_column($d_results,"pro_id");
		$liste_produit=implode($tab_result,",");
	}
	
	if(!empty($liste_produit)){
		
		// LE CLIENT CONCERNE

		if(!empty($client_id)){

			$sql="SELECT cli_code FROM Clients WHERE cli_id=" . $client_id . ";";
			$req=$Conn->query($sql);
			$d_client=$req->fetch();
		}
	
		// LA QUALIFICATION
		$sql="SELECT qua_libelle FROM Qualifications WHERE qua_id=:qualification;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":qualification",$crit_stat["qualification"]);
		$req->execute();
		$d_qualification=$req->fetch(PDO::FETCH_ASSOC);
		if(empty($d_qualification)){
			echo("Impossible d'identifier la qualification");
			die();
		}

		$total_ca=0;
		$total_qte=0;
		$d_factures=array();


		$sql="SELECT fli_montant_ht,fli_qte,fli_code_produit,fli_intra_inter
		,fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date_fr,fac_commercial,fac_id
		,cli_code,cli_nom
		,com_label_1,com_label_2
		FROM Factures_Lignes INNER JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
		LEFT JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)
		LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";
		$sql.=" WHERE fac_date>='". $crit_stat["periode_deb"] . "' AND fac_date<='" . $crit_stat["periode_fin"] . "' AND fli_produit IN (" . $liste_produit . ")";
		if(!empty($client_id)){
			$sql.=" AND fac_client=" . $client_id;
		}
		if(!empty($acc_agence)){
			$sql.=" AND fac_agence=" . $acc_agence;
		}
		$sql.=" ORDER BY fac_chrono;";
		$req=$ConnSoc->query($sql);
		$d_factures=$req->fetchAll();
	}

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">

<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" >
					<div class="row ">

						<h1 class="text-center" >
					<?php	if(isset($d_client)){
								echo($d_client["cli_code"]);								
								echo("<br/>");
							}
							
							echo("CA " . $d_qualification["qua_libelle"]);
							echo(" du " . $periode_deb_fr . " au " . $periode_fin_fr); ?>
						</h1>

						<div class="table-responsive">
							<table class="table table-striped table-hover" >
								<thead>
									<tr class="dark">
										<th>Code</th>
										<th>Nom</th>
										<th>Facture</th>
										<th>Date</th>
										<th>Commercial</th>
										<th>Produit</th>
										<th>INTRA / INTER</th>
										<th>Qte</th>
										<th>CA</th>
									</tr>
								</thead>
								<tbody>
						<?php		if(!empty($d_factures)){
										foreach($d_factures as $f){ 
											$total_qte=$total_qte + $f["fli_qte"];
											$total_ca=$total_ca + $f["fli_montant_ht"];
											?>
											<tr>
												<td><?=$f["cli_code"]?></td>
												<td><?=$f["cli_nom"]?></td>
												<td>
													<a href="facture_voir.php?facture=<?=$f["fac_id"]?>" >
														<?=$f["fac_numero"]?>
													</a>
												</td>
												<td><?=$f["fac_date_fr"]?></td>
												<td><?=$f["com_label_2"] . " " . $f["com_label_1"]?></td>
												<td><?=$f["fli_code_produit"]?></td>
												<td>
											<?php	if($f["fli_intra_inter"]==1){
														echo("INTRA");
													}else{
														echo("INTER");
													}?>
												</td>
												<td class="text-right" ><?=$f["fli_qte"]?></td>
												<td class="text-right" ><?=$f["fli_montant_ht"]?></td>												
											</tr>
						<?php			} ?>
										<tr>
											<th colspan="7" >Total :</th>
											<td class="text-right" ><?=$total_qte?></td>
											<td class="text-right" ><?=number_format($total_ca,2,","," ")?></td>
										</tr>
						<?php		} ?>
								</tbody>
							</table>
						</div>

					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="stat_com_qualif.php" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){



			});
		</script>
	</body>
</html>
