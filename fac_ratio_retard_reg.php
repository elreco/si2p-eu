<?php

	// DETAIL DU RATION RETARD DE REGLEMENT

	include "includes/controle_acces.inc.php";

	include "includes/connexion.php";
	include "includes/connexion_soc.php";


	// CONTROLE ACCES


	

	$commercial=0;
	if(isset($_GET["commercial"])){
		if(!empty($_GET["commercial"])){
			$commercial=intval($_GET["commercial"]);
		}
	}

	$taux=0;
	if(isset($_GET["taux"])){
		if(!empty($_GET["taux"])){
			$taux=intval($_GET["taux"]);
		}
	}
	if(empty($taux)){

		$_SESSION['message'][] = array(
			"aff" => "",
			"titre" => "Accès impossible",
			"type" => "danger",
			"message" => "Impossible d'accéder à cette page!" 
		);
	
		header("location : " . $_SESSION["retour"]);
		die();

	}

	if(!$_SESSION["acces"]["acc_droits"][35]){
		$vu=2; // detail des factures
		$retour=$_SESSION["retour"];
	} elseif(!empty($commercial)) {
		$vu=2;
		$retour="fac_ratio_retard_reg.php?taux=" . $taux;
	}else{
		$vu=1;
		$retour=$_SESSION["retour"];
	}

	
	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}

    // TRAITEMENT
    
	$data=array();
	
	/*var_dump(date("Y-m-d"));
	echo("<br/>");*/

    $sql_fac="SELECT fac_total_ttc,fac_date_reg_prev,fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date,fac_relance_stop
	,com_label_1,com_label_2,com_id 
	,cli_code,cli_nom
	FROM Factures 
	LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
	LEFT JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)
	WHERE fac_nature=1";
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql_fac.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif(!empty($commercial)){
		if($commercial != -1){
			$sql_fac.=" AND fac_commercial=" . $commercial;
		}
	}
	if(!empty($acc_agence)){
		$sql_fac.=" AND fac_agence=" . $acc_agence;
	}

	if($taux == 1){
		// blocage agence
		// bloqué / échu
		$sql_fac.=" AND fac_date_reg_prev<=NOW()";
	}
	$sql_fac.=" AND fac_regle < fac_total_ttc";

	$req_fac=$ConnSoc->query($sql_fac);
	//echo($sql_fac);
	

	$d_factures=$req_fac->fetchAll(); 

	if($vu == 1) {
		if(!empty($d_factures)){
			/*echo("<pre>");
				print_r($d_factures);
			echo("</pre>");*/
		
			foreach($d_factures as $fac){

				if(!isset($data[$fac["com_id"]])) {
					$data[$fac["com_id"]]=array(
						"com_identite" => $fac["com_label_1"] . " " . $fac["com_label_2"],
						"dues" => 0,
						"echues" => 0,
						"stop" => 0
					);
				}

				$data[$fac["com_id"]]["dues"] = $data[$fac["com_id"]]["dues"] + $fac["fac_total_ttc"];
			
				if($fac["fac_date_reg_prev"]<=date("Y-m-d")){
					$data[$fac["com_id"]]["echues"] = $data[$fac["com_id"]]["echues"] + $fac["fac_total_ttc"];
				}
				if($fac["fac_relance_stop"] == 1){
					$data[$fac["com_id"]]["stop"] = $data[$fac["com_id"]]["stop"] + $fac["fac_total_ttc"];
				}
			}
		}
	}
	
	$titre="";

	if(!$_SESSION["acces"]["acc_droits"][35]){

		if($taux == 1) {
			$titre="Liste des factures échues";
		}else{
			$titre="Liste des factures dues";
		}
		

	}else{

		if(!empty($commercial) AND $commercial!= -1){

			$sql="SELECT com_label_1,com_label_2 FROM Commerciaux WHERE com_id=" . $commercial . ";";
			$req=$ConnSoc->query($sql);
			$d_commercial=$req->fetch();
			if(!empty($d_commercial)){
				if($taux == 1) {
					$titre="Factures échues de " . $d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"];
				}else{
					$titre="Factures dues de " . $d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"];
				}
			}

		}else{

			$sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
			$req=$Conn->query($sql);
			$d_societe=$req->fetch();

			if(!empty($acc_agence)) {

				$sql="SELECT soc_nom,age_nom FROM Agences,Societes WHERE soc_id=age_societe AND age_id=" . $acc_agence . ";";
				$req=$Conn->query($sql);
				$d_agence=$req->fetch();
				if(!empty($d_agence)){
					if($vu == 2) {
						// détail
						if($taux == 1) {
							$titre="Liste des factures échues " . $d_agence["soc_nom"] . " " . $d_agence["age_nom"];
						}else{
							$titre="Liste des factures dues " . $d_agence["soc_nom"] . " " . $d_agence["age_nom"];
						}
						
					}else{
						// synthèse
						if($taux == 1) {
							$titre="Taux de factures en blocage 'Agence'<br/>sur " . $d_agence["soc_nom"] . " " . $d_agence["age_nom"];
						}else{
							$titre="Taux de retard de règlements clients<br/>sur" . $d_agence["soc_nom"] . " " . $d_agence["age_nom"];
						}
						
					}
				}

			}else{

				$sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
				$req=$Conn->query($sql);
				$d_societe=$req->fetch();
				if(!empty($d_societe)){
					if($vu == 2) {
						if($taux == 1) {
							$titre="Liste des factures échues " . $d_societe["soc_nom"];
						}else{
							$titre="Liste des factures dues " . $d_societe["soc_nom"];
						}
					}else{
						if($taux == 1) {
							$titre="Taux de factures en blocage 'Agence'<br/>sur " . $d_societe["soc_nom"];
						}else{
							$titre="Taux de retard de règlements clients<br/>sur" . $d_societe["soc_nom"];
						}
					}
				}

			}
		}
	}
	
	

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">

<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" >
					<div class="row ">

						<h1 class="text-center" ><?=$titre?></h1>

						<div class="table-responsive">
				<?php		if( $vu == 1) { 
								// SYNTHESE ?>
								<table class="table table-striped table-hover" >
									<thead>
										<tr class="dark">
											<th>Commercial</th>
									<?php	if($taux == 1){ ?>											
												<th>Total TTC des factures échues</th>
												<th>Total TTC des factures bloquées</th>
												<th>Taux de factures en blocage "Agence" </th>
									<?php	}else{  ?>
												<th>Total TTC des factures dues</th>
												<th>Total TTC des factures échues</th>
												<th>Taux de retard de règlements clients</th>
									<?php	}  ?>
											
										</tr>
									</thead>
									<tbody>
							<?php		if(!empty($data)){
											$total_dues=0;
											$total_echues=0;
											$total_stop=0;
											foreach($data as $com_id => $d){

												$total_dues=$total_dues + $d["dues"];
												$total_echues=$total_echues + $d["echues"];
												$total_stop=$total_stop + $d["stop"];

												$ratio=0;
												if($taux == 1){
													if(!empty($d["echues"])){
														$ratio=($d["stop"]/$d["echues"])*100;
													}
												}else{
													if(!empty($d["dues"])){
														$ratio=($d["echues"]/$d["dues"])*100;
													}
												}
												?>
												<tr>
													<td>
														<a href="fac_ratio_retard_reg.php?commercial=<?=$com_id?>&taux=<?=$taux?>">
															<?=$d["com_identite"]?>
														</a>
													</td>
											<?php	if($taux == 1){ ?>
														<td class="text-right" ><?=number_format($d["echues"],2,","," ")?></td>
														<td class="text-right" ><?=number_format($d["stop"],2,","," ")?></td>
											<?php	}else{ ?>
														<td class="text-right" ><?=number_format($d["dues"],2,","," ")?></td>
														<td class="text-right" ><?=number_format($d["echues"],2,","," ")?></td>
											<?php	} ?>
													<td class="text-right" ><?=number_format($ratio,2,","," ")?></td>
												</tr>
							<?php			}

											$ratio=0;
											if($taux == 1){
												if(!empty($total_echues)){
													$ratio=($total_stop/$total_echues)*100;
												}
											}else{
												if(!empty($total_dues)){
													$ratio=($total_echues/$total_dues)*100;
												}
											}
											
											?>
											<tr>
												<th>
													<a href="fac_ratio_retard_reg.php?commercial=-1&taux=<?=$taux?>">
														Total :
													</a>
												</th>
										<?php	if($taux == 1){ ?>
													<td class="text-right" ><?=number_format($total_echues,2,","," ")?></td>
													<td class="text-right" ><?=number_format($total_stop,2,","," ")?></td>
										<?php	}else{ ?>
													<td class="text-right" ><?=number_format($total_dues,2,","," ")?></td>
													<td class="text-right" ><?=number_format($total_echues,2,","," ")?></td>d>
										<?php	} ?>
												
												<td class="text-right" ><?=number_format($ratio,2,","," ")?></td>
											</tr>
							<?php		} ?>
									</tbody>
								</table>
				<?php		}else { ?>
								<table class="table table-striped table-hover" >
									<thead>
										<tr class="dark">
											<th>Code client</th>
											<th>Nom</th>
											<th>Numéro de facture</th>
											<th>Date de facture</th>
											<th>Commercial</th>
											<th>Date d'échéance</th>
									<?php	if($taux == 1){ ?>
												<th>TTC échu</th>
												<th>TTC blocage "Agence"</th>
									<?php	}else{ ?>
												<th>TTC dû</th>
												<th>TTC échu</th>
									<?php	} ?>
											
										</tr>
									</thead>
									<tbody>
							<?php		if(!empty($d_factures)){
											$total_du=0;
											$total_echu=0;
											$total_stop=0;
											/*echo("<pre>");
												print_r($d_factures);
											echo("</pre>");*/
											foreach($d_factures as $d){



												$total_du=$total_du + $d["fac_total_ttc"];

												$echu=0;
												if($d["fac_date_reg_prev"]<=date("Y-m-d")){
													$total_echu=$total_echu + $d["fac_total_ttc"];
													$echu=$d["fac_total_ttc"];
												}

												$stop=0;
												if($d["fac_relance_stop"] == 1){
													$total_stop=$total_stop + $d["fac_total_ttc"];
													$stop=$d["fac_total_ttc"];
												}

												if(!empty($d["fac_date_reg_prev"])){
													$Dt_date_reg=DateTime::createFromFormat('Y-m-d',$d["fac_date_reg_prev"]);
													$date_reg=$Dt_date_reg->format("d/m/Y");
												}else{
													$date_reg="";
												} ?>
												<tr>
													<td><?=$d["cli_code"]?></td>
													<td><?=$d["cli_nom"]?></td>
													<td><?=$d["fac_numero"]?></td>
													<td><?=$d["fac_date"]?></td>
													<td><?=$d["com_label_1"] . " " . $d["com_label_2"]?></td>
													<td><?=$date_reg?></td>
											<?php	if($taux == 1){ ?>
														<td class="text-right" ><?=number_format($d["fac_total_ttc"],2,","," ")?></td>
														<td class="text-right" >
												<?php		if(!empty($stop)) {
																echo(number_format($stop,2,","," "));
															}?>
														</td>
											<?php	}else{ ?>
														<td class="text-right" ><?=number_format($d["fac_total_ttc"],2,","," ")?></td>
														<td class="text-right" >
												<?php		if(!empty($stop)) {
																echo(number_format($echu,2,","," "));
															} ?>
														</td>
											<?php	} ?>	
													
												</tr>
							<?php			}
											if($taux == 1){ ?>

												<tr>
													<th colspan="6" class="text-right" >Total :</th>
													<td class="text-right" ><?=number_format($total_echu,2,","," ")?></td>
													<td class="text-right" ><?=number_format($total_stop,2,","," ")?></td>
												</tr>
												<tr>
													<th colspan="6" class="text-right" >Taux de blacage "Agence" :</th>
													<td class="text-center" colspan="2" >
												<?php	if(!empty($total_echu)){
															echo(number_format(($total_stop/$total_echu)*100,2,","," "));
														} ?>
													</td>
												</tr>

									<?php	}else{ ?>
												<tr>
													<th colspan="6" class="text-right" >Total :</th>
													<td class="text-right" ><?=number_format($total_du,2,","," ")?></td>
													<td class="text-right" ><?=number_format($total_echu,2,","," ")?></td>
												</tr>
												<tr>
													<th colspan="6" class="text-right" >Taux de retard de règlements clients :</th>
													<td class="text-center" colspan="2" >
												<?php	if(!empty($total_du)){
															echo(number_format(($total_echu/$total_du)*100,2,","," "));
														} ?>
													</td>
												</tr>
									<?php	} 
										} ?>
									</tbody>
								</table> 
				<?php		} ?>
						</div>

					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="<?=$retour?>" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){



			});
		</script>
	</body>
</html>
