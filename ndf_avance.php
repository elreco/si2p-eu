<?php 
/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');
///////////////////// Contrôles des parametres ////////////////////
$param_commande = 0;

if(!empty($_GET['utilisateur'])){
    // si les get sont pas remplis

    $param_commande = intval($_GET['utilisateur']);

}
if($param_commande == 0){
 
    echo("Impossible d'afficher la page");
    die();
}
///////////////////// FIN Contrôles des parametres ////////////////////

/////////////// TRAITEMENTS BDD ///////////////////////

if(!empty($_GET['id'])){ // si la ndf existe
    // sélectionner la ndf
    $req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id =" . $_GET['id']);
    $req->execute();
    $ndf = $req->fetch();
}

// récupérer l'utilisateur
$req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id =" . $_GET['utilisateur']);
$req->execute();
$uti = $req->fetch();
// fin récupérer l'utilisateur
if(!empty($_GET['nav'])){
    $req = $Conn->prepare("SELECT * FROM ndf_avances WHERE nav_id =" . $_GET['nav']);
    $req->execute();
    $nav = $req->fetch();
}

?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Enregistrer une avance</title> 
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
<form method="post" action="ndf_avance_enr.php" enctype="multipart/form-data">
<!-- Start: Main -->
<div id="main">
<?php include "includes/header_def.inc.php"; ?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper" >
    <!--DEBUT CONTENT -->
    <section id="content" class="animated fadeIn">
        <!--DEBUT ROW -->
        <div class="row"> 
        <!--DEBUT COL-MD-10 --> 
            <div class="col-md-10 col-md-offset-1">
              <!--DEBUT ADMIN FORM -->
                <div class="admin-form theme-primary ">
                    <!--DEBUT PANEL HEADING -->
                    <div class="panel heading-border panel-primary"> 
                      <!--DEBUT PANEL BODY -->
                        <div class="panel-body bg-light">
                            <div class="content-header">
                                <h2>Enregistrer une avance pour la note de frais de <b class="text-primary"><?= $uti['uti_prenom'] ?> <?= $uti['uti_nom'] ?></b> du <b class="text-primary"><?= convert_date_txt($ndf['ndf_quinzaine_deb']) ?></b> au <b class="text-primary"><?= convert_date_txt($ndf['ndf_quinzaine_fin']) ?></b></h2>            
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row">
                                    <input type="hidden" name="utilisateur" value="<?= $_GET['utilisateur'] ?>">
                                    <?php if(!empty($_GET['nav'])){ ?>
                                        <input type="hidden" name="nav" value="<?= $_GET['nav'] ?>">
                                    <?php } ?>
                                    <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
                                    
                                  
                                <div class="row">
                                   
                                    <div class="col-md-6">
                                        <label for="cli_qte" class="text-left mb20" style="font-weight:bold;">Date :</label>
                                            <input type="text" min="0" name="nav_date" id="nav_date" class="form-control date datepicker" required placeholder="Date" 
                                            <?php if(!empty($nav)){ ?>
                                                value="<?= convert_date_txt($nav['nav_date']) ?>"
                                            <?php }else{?>
                                            value="<?= date("d-m-Y") ?>"
                                            <?php } ?>
                                            >
                                            
                                         
                                    </div>
                                    <div class="col-md-6">
                                        <label for="cli_qte" class="text-left mb20" style="font-weight:bold;">T.T.C (€) :</label>
                                            <input type="text" name="nav_ttc" id="nav_ttc" class="form-control number" placeholder="Total"
                                            <?php if(!empty($nav)){ ?>
                                                value="<?= $nav['nav_ttc'] ?>"
                                            <?php }?>
                                                >
                                            
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 mt20">
                                        <p class="text-left mb20" style="font-weight:bold;">Commentaire :</p>
                                        <textarea id="nav_commentaire" name="nav_commentaire" class="summernote">
                                            <?php if(!empty($nav)){ ?>
                                                <?= $nav['nav_commentaire'] ?>
                                            <?php }?>

                                        </textarea>
                                    </div>
                                </div>
                              
                               

                                
                            </div>
                        <!-- FIN COL-MD-10 -->
                        </div>
                    <!-- FIN PANEL BODY -->
                    </div>
                <!-- FIN PANEL HEADING -->
                </div>
            <!-- FIN ADMIN FORM -->
            </div>
        </div> 

        </section>

    </section>

</div>
    <!-- End: Main -->
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">
                <a href="ndf_liste.php" class="btn btn-default btn-sm">

                    <i class='fa fa-long-arrow-left'></i> Retour
                
                </a>
            </div>
            <div class="col-xs-6 footer-middle">

            </div>
            <div class="col-xs-3 footer-right">
                <button type="submit" name="search" class="btn btn-success btn-sm">
                    <i class='fa fa-floppy-o'></i> Enregistrer
                </button>
            </div>
        </div>
    </footer>
</form>
<?php include "includes/footer_script.inc.php"; ?>  

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->
<script src="vendor/plugins/summernote/summernote.min.js"></script>
<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
<script src="assets/js/custom.js"></script>
<!-- SCRIPT SELECT2 -->
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
    ////////////// FONCTIONS ///////////////




 
    ////////////// FIN FONCTIONS ///////////////


    <!-- -->
    //////// EVENEMENTS UTILISATEURS //////////
    jQuery(document).ready(function () {

    });
   
        
    //////// FIN ÉVENEMENTS UTILISATEURS //////
</script>

</body>
</html>