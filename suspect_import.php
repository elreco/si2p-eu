<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");


// sur la personne connecte

$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"]; 
	}
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
  $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
  $acc_agence=$_SESSION['acces']["acc_agence"];  
}

// SUR LA SOCIETE / AGENCES

if($acc_agence==0){
	$req=$Conn->query("SELECT age_id,age_nom FROM Societes INNER JOIN Agences ON (Societes.soc_id=Agences.age_societe) WHERE soc_id=" . $acc_societe . " AND soc_agence=1;");
	$d_agences=$req->fetchAll();
}



if(empty($d_agences)){
	
	$sql="SELECT com_id,com_label_1 FROM Commerciaux WHERE NOT com_archive";
	if($acc_agence>0){
		$sql.=" AND com_agence=" . $acc_agence;
	}
	if(!$_SESSION["acces"]["acc_droits"][6]){
		$sql.=" AND com_utilisateur=" . $acc_utilisateur;
	}
	$sql.=" ORDER BY com_label_1,com_label_2;";
	$req=$ConnSoc->query($sql);
	$d_commerciaux=$req->fetchAll();
}


$sql="SELECT cpr_id,cpr_libelle FROM Clients_prescripteurs ORDER BY cpr_libelle;";
$req=$Conn->query($sql);
$d_prescripteurs=$req->fetchAll();

?>
<!DOCTYPE html> 
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8"> 
		<title>SI2P - Orion - Paramètres</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	   
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	  
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>


	<body class="sb-top sb-top-sm">
	
		<form action="suspect_import_result.php" enctype="multipart/form-data" method="POST" >
			<div id="main">
		<?php	include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" >
					<section id="content" class="" >
					
						<div class="admin-form theme-primary ">
							<div class="panel heading-border panel-primary">
								<div class="panel-body bg-light">
												
									<div class="content-header">
										<h2><?=$_SESSION["acces"]["acc_liste_societe"][$acc_societe . "-" . $acc_agence]?> - <b class="text-primary">Import suspects</b></h2>
									</div>
									
									<div class="row">
							<?php		if(!empty($d_agences)){ ?>
											<div class="col-md-4">
												<label for="agence" >Agence :</label>
												<select class="select2 select2-com-agence" id="agence" name="agence" required="required" data-com_archive="0" >
													<option value="" >Agence ...</option>
							<?php					foreach($d_agences as $age){
														echo("<option value='" . $age["age_id"] . "' >" . $age["age_nom"] . "</option>");
													} ?>
												</select>
											</div>
							<?php		} ?>
										<div class="col-md-4">
											<label for="commercial" >Commercial :</label>
											<select class="select2 select2-commercial" id="commercial" name="commercial" >
												<option value="" >Commercial ...</option>
						<?php					if(!empty($d_commerciaux)){
													foreach($d_commerciaux as $commercial){
														echo("<option value='" . $commercial["com_id"] . "' >" . $commercial["com_label_1"] . "</option>");
													}
												} ?>
											</select>
										</div>
									<!--
										<div class="col-md-4">
											<label for="commercial" >Prescripteur :</label>
											<select class="select2" id="prescripteur" name="prescripteur" >
												<option value="" >Prescripteur ...</option>
						<?php					if(!empty($d_prescripteurs)){
													foreach($d_prescripteurs as $prescripteur){
														echo("<option value='" . $prescripteur["cpr_id"] . "' >" . $prescripteur["cpr_libelle"] . "</option>");
													}
												} ?>
											</select>
										</div>-->
									</div>

									
									<div class="row mt15">
										<div class="col-md-12">																			
											<label for="" >Nom de l'import :</label>
											<input type="text" name="libelle" class="gui-input" id="" placeholder="Libellé de l'import" required="required" />
											
										</div>
									</div>
									<div class="row mt15" >
										<div class="col-md-12">
											<label class="field prepend-icon file">
												<span class="button btn-primary">Choisir</span>
												<input type="file" class="gui-file" name="fichier" id="fichier" />
												<input type="text" class="gui-input" id="fichier_champ" placeholder="Importez votre document" />
												<label class="field-icon">
													<i class="fa fa-upload"></i>
												</label>
											</label>
										</div>
									</div>
									
								</div>
							</div>
						</div>

					</section>				
				</section>		
			</div>
		

			<footer id="content-footer" class="affix">
				<div class="row">				
					<div class="col-xs-3 footer-left" >
						<a href="suspect_tri.php" class="btn btn-default btn-sm" >
							<i class="fa fa-arrow-left" ></i> Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right" >
						  <button type="submit" name="submit" id="upload-btn" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						  </button>
						  <button type="button" id="cancel-btn" class="btn btn-danger btn-sm" style="display:none;">
							<i class='fa fa-times'></i> Annuler
						  </button>
					</div>
				</div>
			</footer>
		</form>

<?php 	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
			});		
		</script>
	</body>
</html>
