<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'modeles/mod_upload.php';
include_once "modeles/mod_agence.php";
include_once "modeles/mod_societe.php";
include_once "modeles/mod_utilisateur.php";
$agences = get_agences();
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			
			<section id="content_wrapper">
				
				<section id="content" class="animated fadeIn">
				
					<h1>Liste des agences</h1>
					
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="dark" >
									<th>Nom de l'agence</th>
									<th>Société</th>
									<th>Nom du responsable</th>
									<th>Siret</th>
									<th>Tel</th>
									<th>Fax</th>
									<th>Mail</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($agences as $a): ?>
								<tr>
									<td><?= $a['age_nom'] ?></td>
									<?php $societe = get_societe($a['age_societe']); ?>
									<td><?= $societe['soc_nom']; ?></td>
									<?php $responsable = get_utilisateur($a['age_responsable']); ?>
									<td><?= $responsable['uti_prenom'] ?> <?= $responsable['uti_nom'] ?> </td>
									<td><?= $a['age_siret'] ?></td>
									<td><?= $a['age_tel'] ?></td>
									<td><?= $a['age_fax'] ?></td>
									<td><?= $a['age_mail'] ?></td>
									<td class="text-center" >
										<a href="agence.php?id=<?= $a['age_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier cette agence">
											<span class="fa fa-pencil"></span>
										</a>
									</td>
								</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-long-arrow-left"></span>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center">
		<?php		if($_SESSION['acces']['acc_profil']==13){ ?>
						<a href="import/sync_agence.php" class="btn btn-default btn-sm" >
							<i class="fa fa-refresh"></i>
							Importer
						</a>		
		<?php		} ?>	
				</div>
				<div class="col-xs-3 footer-right" >
					<a href="agence.php" class="btn btn-success btn-sm" role="button" >
						<span class="fa fa-plus"></span>
						Nouvelle agence
					</a>
				</div>
			</div>
		</footer>
<?php
		include "includes/footer_script.inc.php"; ?>	
	</body>
</html>
