<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_presence_data.php');
include('modeles/mod_get_juridique.php');




// VISUALISATION D'UNE CONVOCATIONS

$erreur_txt="";

$action_id=0;
if(!empty($_GET["action"])){
	$action_id=intval($_GET["action"]);
}

$action_client_id=0;
if(!empty($_GET["action_client"])){
	$action_client_id=intval($_GET["action_client"]);
}

if(empty($action_id)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();

}else{

	// personne connecte
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	if($acc_societe!=$conn_soc_id){
		$acc_societe=$conn_soc_id;
	}

	$data=presence_data($action_id,$action_client_id);

	if(empty($data)){
		$erreur_txt="impossible d'afficher la page";
		echo($erreur_txt);
		die();
	}
}
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#container_print{
				width:21cm;
			}

			#page_print section{
				padding-top:5px;
			}

			.ligne-col-1{
				width:33%;
				padding-left:5px;
			}
			.ligne-col-2{
				width:34%;
			}
			.signature{
				height:45px;
			}
			.table-div > div{
				vertical-align:middle;
			}
			#form_signature{
				margin-bottom:100px;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm">

		<div id="zone_print" ></div>


		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php 		if(empty($erreur_txt)){ ?>

							<div class="row" >

								<!-- zone d'affichage du devis -->

								<div class="col-md-8"  >

									<div id="container_print" >

										<div id="page_print" >

								<?php
											foreach($data["pages"] as $p => $page){

												?>

												<div class="page" id="header_<?=$p?>" data-page="<?=$p?>" >
													<header>
														<table>
															<tr>
																<td class="w-30" >
														<?php		echo($page["juridique"]["nom"]);
																	if(!empty($page["juridique"]["agence"])){
																		echo("<br/>" . $page["juridique"]["agence"]);
																	};
																	if(!empty($page["juridique"]["ad1"])){
																		echo("<br/>" . $page["juridique"]["ad1"]);
																	};
																	if(!empty($page["juridique"]["ad2"])){
																		echo("<br/>" . $page["juridique"]["ad2"]);
																	};
																	if(!empty($page["juridique"]["ad3"])){
																		echo("<br/>" . $page["juridique"]["ad3"]);
																	};
																	echo("<br/>" . $page["juridique"]["cp"] . " " . $page["juridique"]["ville"]); ?>
																</td>
																<td class="w-40" >
																	<h1 class="text-center" >FEUILLE D'EMARGEMENT</h1>
																</td>
																<td class="w-30 text-right" >
														<?php		if(!empty($page["juridique"]["logo_1"])){ ?>
																		<img src="<?=$page["juridique"]["logo_1"]?>" />
														<?php		}else{
																		echo("&nbsp;");
																	} ?>
																</td>

															</tr>
														</table>
													</header>
												</div>

												<div id="head_<?=$p?>" class="head" >
													<section>
														<table class="tab-border" >
															<tr>
																<td class="w-50" >
															<?php	echo("<b/>Lieu de formation :</b><br/>" . $data["act_adr_nom"]);
																	if(!empty($data["act_adr_service"])){
																		echo("<br/>" . $data["act_adr_service"]);
																	}
																	if(!empty($data["act_adr1"])){
																		echo("<br/>" . $data["act_adr1"]);
																	}
																	if(!empty($data["act_adr2"])){
																		echo(", " . $data["act_adr2"]);
																	}
																	if(!empty($data["act_adr3"])){
																		echo(", " . $data["act_adr3"]);
																	}
																	echo("<br/>" . $data["act_adr_cp"] . " " . $data["act_adr_ville"]); ?>
																</td>
																<td class="w-50" >
														<?php		if(!empty($data["act_con_nom"])){
																		echo("Contact : " . $data["act_con_prenom"] . " " . $data["act_con_nom"]);
																	}
																	if(!empty($data["act_con_tel"])){
																		echo("<br/>Tél. : " . $data["act_con_tel"]);
																	}
																	if(!empty($data["act_con_portable"])){
																		echo("<br/>Port. : " . $data["act_con_portable"]);
																	} ?>
																</td>
															</tr>
													<?php	if(!empty($page["date"])){ ?>
																<tr>
																	<td>
																<?php	echo("Date : Le " . utf8_encode(strftime("%d %B %Y",$page["date"]->getTimestamp())) ); ?>
																	</td>
																	<td>
																		<?php if(!empty($page["date_alternate"])){ ?>
																<?php	echo("Durée :" . $page["date_alternate"]->format("%Hh%I")); ?>
																		<?php } ?>
																	</td>
																</tr>
													<?php	} ?>
															<tr>
																<td>
													<?php			if(!empty($page["ase_h_deb"]) AND !empty($page["ase_h_fin"]) ){
																		echo("Horaires : de " . $page["ase_h_deb"] . " à " . $page["ase_h_fin"]);

																	} ?>
																</td>
																<td>
															<?php	if($page["pda_categorie"]==2){
																		echo("Testeur : ");
																	}else{
																		echo("Formateur : ");
																	}
																	echo($page["int_identite"])
															?>
																</td>
															</tr>
															<tr>
																<td>
													<?php			echo($page["acl_pro_libelle"]); ?>
																</td>
																<td>
													<?php			echo("Client :<br/>" . $page["cli_nom"]); ?>
																</td>
															</tr>
														</table>
													</section>
												</div>

												<div id="ligne_header_<?=$p?>" >
													<section>
														<div class="table-div" >
															<div class="table-div-th ligne-col-1 text-center" >Nom et Prénom <sup>(1)</sup></div>
															<div class="table-div-th ligne-col-1 text-center" >Etablissement / Service</div>
															<div class="table-div-th ligne-col-2 text-center" >Signature</div>
														</div>
													</section>
												</div>
												<?php if(!empty($page["ase_annule"]) && !empty($page["acl_annule"])){ ?>
													<h1 class="text-center" style="color:#e31936;position:absolute; opacity:0.5;left:30%;font-size:50px;margin-top:150px;">SESSION ANNULÉE</h1>
												<?php } ?>
										<?php	if(!empty($page["stagiaires"])){

													asort($page["stagiaires"]);

													foreach($page["stagiaires"] as $sta){

														?>

														<div class="table-div ligne-body-<?=$p?> signature" >
															<div class="ligne-col-1" ><?=$sta["sta_nom"] . " " . $sta["sta_prenom"]?></div>
															<div class="ligne-col-1 text-center" >&nbsp;</div>
															<div class="ligne-col-2" >
													<?php	if(!empty($sta["sta_signature"])){ ?>
																	<div style="height:44px;text-align:center;max-width:100%;" >
																		<img src="data:image/png;base64,<?=$sta["sta_signature"]?>" style="max-height:100%!important;max-width:100%;" >
																	</div>
													<?php		} ?>
															</div>
														</div>
										<?php		}
												}
												// on complete avec des ligne vide
												if(empty($page["stagiaires"])){
													$bcl=0;
												}elseif(count($page["stagiaires"])<10){
													$bcl=count($page["stagiaires"]);
												}elseif(count($page["stagiaires"])>10){
													$bcl=count($page["stagiaires"]) % 10;
												}else{
													$bcl=11;
												}
												for($bcl;$bcl<10;$bcl++){ ?>
													<div class="table-div ligne-body-<?=$p?> signature" >
														<div class="ligne-col-1" >&nbsp;</div>
														<div class="ligne-col-1 text-center" >&nbsp;</div>
														<div class="ligne-col-2" >&nbsp;</div>
													</div>
									<?php		} ?>
												<div class="ligne-body-<?=$p?>" >

													<section>
														<table class="tab-border" >
															<caption class="text-left" >Régistre de sécurité :</caption>
															<tr>
																<td class="w-25 text-right" ><b>existant :</b></td>
																<td class="w-25 text-center" >
																	<span><img src="assets/img/unchecked.png" /> OUI</span>
																	<span class="pl15" ><img src="assets/img/unchecked.png" /> NON</span>
																</td>
																<td class="w-25 text-right"  ><b>signé :</b></td>
																<td class="w-25 text-center" >
																	<span><img src="assets/img/unchecked.png" /> OUI</span>
																	<span class="pl15" ><img src="assets/img/unchecked.png" /> NON</span>
																</td>
															</tr>
														</table>
													</section>

												</div>

												<div id="foot_<?=$p?>" >
													<table id="form_signature" >
														<tr>
															<td class="w-67" >&nbsp;</td>
															<td>
														<?php	if($page["pda_categorie"]==2){
																	echo("Le testeur");
																}else{
																	echo("Le formateur");
																}
																echo(" <sup>(2)</sup><br/>");
																echo($page["int_identite"]);

																$marge_left=rand(-25,25);
																$marge_top=rand(-20,0); ?>
																<div style="height:44px;text-align:center;max-width:100%;" >
																	<img src="data:image/png;base64,<?=$page["int_signature"]?>" style="max-height:100%!important;max-width:100%;" >
																</div>

															</td>
														</tr>
													</table>
												</div>

												<div id="footer_<?=$p?>" <?php if($p<count($data["pages"])-1) echo("class='saut-page'"); ?> >
													<footer>
														<p class="text-left" >
															(1) - Par ma signature, j'atteste avoir reçu la formation ci-dessus référencée et il m'a été remis en main propre une attestation de formation.<br/>
															(2) - Par ma signature, j'atteste avoir dispensé la formation ci-dessous.
														</p>
														<hr/>
														<p class="text-center" >
															<?=$page["juridique"]["nom"]?> <span class="footer-bar" >|</span>
													<?php	if(!empty($page["juridique"]["agence"])){ ?>
																<?=$page["juridique"]["agence"]?> <span class="footer-bar" >|</span>
													<?php	} ?>
															<?=$page["juridique"]["ad1"] . " " .$page["juridique"]["ad2"] . " " . $page["juridique"]["ad3"]?> <span class="footer-bar" >|</span>
															<?=$page["juridique"]["cp"] . " " . $page["juridique"]["ville"]?> <span class="footer-bar" >|</span>
															TEL. <?=$page["juridique"]["tel"]?> <span class="footer-bar" >|</span>
															FAX. <?=$page["juridique"]["fax"]?>
														</p>
													</footer>
												</div>

							<?php			} ?>
										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->

								<div class="col-md-4" >
									<!-- DEBUT ANNEXE -->

									<!-- FIN ANNEXE -->
								</div>

							</div>


		<?php		}else{ ?>

						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>

		<?php		} ?>

				</section>

				<!-- End: Content -->
			</section>


		</div>
		<!-- End: Main -->

		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="action_voir.php?action=<?=$action_id?>&onglet=3&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
					<button type="button" class="btn btn-sm btn-info ml15" id="print" >
						<i class="fa fa-print"></i> Imprimer
					</button>
				</div>
				<div class="col-xs-3 footer-right" ></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="assets/js/print.js"></script>
		<script type="text/javascript">
			var l_page=21;
			var h_page=27;
		</script>
	</body>
</html>
