<?php 

// LISTE DES DEVIS ASSOCIE A UN CLIENT
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');


$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}

$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
}

$client=0;
if(!empty($_GET["client"])){
	$client=intval($_GET["client"]);
}
$exercice=0;
if(!empty($_GET["exercice"])){
	$exercice=intval($_GET["exercice"]);
}
$erreur_txt="";
if(empty($client) OR empty($exercice)){
	$erreur_txt="Impossible d'afficher cette page!";
}else{
	
	// ON VERIF QU'ON A ACCES AU CLIENT
	
	$sql="SELECT cli_id,cli_nom,cli_code FROM Clients WHERE cli_id = " . $client;
	if($acc_agence>0){
		$sql.=" AND cli_agence=" . $acc_agence;
	}
	if(!$_SESSION['acces']["acc_droits"][6]){
		$sql.=" AND cli_utilisateur=" . $acc_utilisateur;
	}
	$sql.=";";
	$req=$ConnSoc->query($sql);
	$d_client=$req->fetch();
	if(empty($d_client)){
		$erreur_txt="Impossible d'afficher cette page!";
	}
}

if(empty($erreur_txt)){
	
	$_SESSION["retourDevis"]="client_devis.php?client=" . $client . "&exercice=" . $exercice;

	$sql_devis="SELECT DISTINCT dev_id,dev_adr_ville,dev_numero,DATE_FORMAT(dev_date,'%d/%m/%Y') AS dev_date_aff,dev_prospect,dev_esperance,dev_adr_ville,dev_total_ht,dev_bordereau,dev_date
	,cli_id,cli_code,cli_nom,dev_chrono,dli_id,dli_reference,dli_montant_ht 
	FROM ( ( ( Devis LEFT JOIN Clients ON (Devis.dev_client=Clients.cli_id) )
	LEFT OUTER JOIN Devis_Lignes ON (Devis_Lignes.dli_devis=Devis.dev_id))
	LEFT OUTER JOIN Commerciaux ON (Devis.dev_commercial=Commerciaux.com_id))
	WHERE dev_client=" . $client . " AND dev_date>='" . intval($exercice) . "-04-01' AND dev_date<='" . intval($exercice+1) . "-03-31' AND NOT dev_bordereau";
	if($acc_agence>0){
		$sql_devis.=" AND dev_agence=" . $acc_agence;
	}
	$sql_devis.=" ORDER BY dev_date,dev_chrono;";
	$req = $ConnSoc->query($sql_devis);	
	$devis = $req->fetchAll();
}	?>

		<!DOCTYPE html>
		<html>
			<head>
				<!-- Meta, title, CSS, favicons, etc. -->
				<meta charset="utf-8">
				<title>Si2P - ORION</title>
				<meta name="keywords" content=""/>
				<meta name="description" content="">
				<meta name="author" content="Si2P">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<!-- Theme CSS -->

				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
				<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
				<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

				<link rel="shortcut icon" href="assets/img/favicon.png">

				<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
				<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
			</head>
			<body class="sb-top sb-top-sm">
				<!-- Start: Main -->
				<div id="main">
			<?php	include "includes/header_def.inc.php"; ?>
					
					<section id="content_wrapper">
					
						<section id="content" class="animated fadeIn">					
				<?php		if(!empty($erreur_txt)){ ?>
								<p class="alert alert-danger" ><?=$erreur_txt?></p>
				<?php		}else{ ?>
									
								<h1><?=$d_client["cli_nom"] . " [" . $d_client["cli_code"] . "]"?></h1>
								<h1>Liste des devis <?=$exercice . "/" . intval($exercice+1)?></h1>
					<?php 		
								if(!empty($devis)){ ?>
									<div class="table-responsive">
										<table class="table table-striped table-hover" id="table_id">
											<thead>
												<tr class="dark" >
													<th>ID</th>													
													<th>Numéro</th>
													<th>Date</th>
													<th>Montant HT</th>											
													<th>Produit</th>					
													<th>Etat fiabilité</th>												
												</tr>
											</thead>
											<tbody>
											
										<?php	$total_ht=0;
												foreach($devis as $d){
													
													$total_ht=$total_ht+$d['dli_montant_ht'];	?>										
													<tr>
														<td><?=$d['dev_id']?></td>														
														<td>
															<a href="devis_voir.php?devis=<?=$d['dev_id']?>" >
																<?=$d['dev_numero']?>
															</a>
														</td>
														<td><?=$d['dev_date_aff']?></td>
														<td class="text-right" ><?=number_format($d['dli_montant_ht'],2,","," ")?></td>
														<td><?=$d['dli_reference']?></td>
											<?php		switch ($d['dev_esperance']){
															case 0:
																echo("<td class='bg-dark' >Sans suite</td>");
																break;	
															case 30:
																echo("<td class='bg-danger' >30%</td>");
																break;
															case 50:
																echo("<td class='bg-warning' >50%</td>");
																break;	
															case 90:
																echo("<td class='bg-success' >90%</td>");
																break;		
															case 100:
																echo("<td class='bg-info' >Commandé</td>");
																break;
															default:
																echo("<td>&nbsp;</td>");
														}	?>
													</tr>
										<?php 	} 	?>
												<tr>
													<td colspan="3" class="text-right" >Total :</td>
													<td class="text-right" ><?=number_format($total_ht,2,","," ")?></td>													
													<td colspan="2" >&nbsp;</td>
												</tr>												
											</tbody>
											
										</table>
									</div>
				<?php 			}else{ ?>
									<div class="col-md-12 text-center" style="padding:0;" >
										<div class="alert alert-warning" style="border-radius:0px;">
											Aucun devis correspondant à votre recherche.
										</div>
									</div>
				<?php 			}
							} ?>
						</section>
						<!-- End: Content -->
					</section>
				</div>
				<!-- End: Main -->
				
				<footer id="content-footer" class="affix">
					<div class="row">
						<div class="col-xs-3 footer-left">
							<a href="client_voir.php?client=<?=$client?>&tab2" class="btn btn-default btn-sm" >
								<i class="fa fa-left-arrow"></i> Retour
							</a>
						</div>
						<div class="col-xs-6 footer-middle">&nbsp;</div>
						<div class="col-xs-3 footer-right"></div>
					</div>
				</footer>
	<?php		include "includes/footer_script.inc.php"; ?>	
				<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
				<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
				<script type="text/javascript">
					jQuery(document).ready(function () {
						
					});
				</script>
			</body>
		</html>