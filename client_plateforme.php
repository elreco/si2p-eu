<?php

include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'modeles/mod_platform.php';
include('modeles/mod_get_groupe.php');

function client_array() {
    return [
        'cli_id' => '',
        'cli_code' => '',
        'cli_nom' => '',
        'cli_siret' => '',
        'cli_holding' => '',
        'cli_subholding' => '',
        'cli_groupe' => 1
    ];
}
$req=$Conn->prepare("SELECT * FROM Clients
WHERE cli_id = :cli_id;");
$req->bindParam(":cli_id",$_GET['client']);
$req->execute();
$client=$req->fetch();

$sql="SELECT adr_siret FROM Adresses WHERE adr_ref_id=" . $client['cli_id'] . " AND adr_defaut AND adr_type=2;";
$req = $Conn->query($sql);
$adresse_fac=$req->fetch();

$clients[0]['cli_id'] = $client['cli_id'];
$clients[0]['cli_code'] = $client['cli_code'];
$clients[0]['cli_nom'] = $client['cli_nom'];
$clients[0]['cli_siret'] = $client['cli_siren'] . $adresse_fac['adr_siret'];
$clients[0]['cli_holding'] = 0;
$clients[0]['cli_subholding'] = 0;
$erreur = "";

if (!empty($client['cli_groupe'])) {
    $clients[0]['cli_holding'] = 0;
    $clients[0]['cli_groupe'] = 1;
    if (empty($client['cli_filiale_de'])) {

        $req=$Conn->prepare("SELECT * FROM Clients
        WHERE cli_groupe = 1 AND cli_filiale_de = :cli_id AND (cli_fil_de IS NULL OR cli_fil_de = 0 OR cli_fil_de = :cli_id) AND cli_id != :cli_id;");
        $req->bindParam(":cli_id", $client['cli_id']);
        $req->execute();
        $_clients=$req->fetchAll();
        // clients de niveau 1
        $i = 0;
        foreach ($_clients as $c) {
            $i = $i + 1;
            $clients[$i] = client_array();
            $clients[$i]['cli_id'] = $c['cli_id'];
            $clients[$i]['cli_code'] = $c['cli_code'];
            $clients[$i]['cli_nom'] = $c['cli_nom'];
            $sql="SELECT adr_siret FROM Adresses WHERE adr_ref_id=" . $c['cli_id'] . " AND adr_defaut AND adr_type=2;";
            $req = $Conn->query($sql);
            $adresse_fac=$req->fetch();
            $clients[$i]['cli_siret'] = $c['cli_siren'] . $adresse_fac['adr_siret'];
            $clients[$i]['cli_holding'] = $client['cli_id'];
            $clients[$i]['cli_subholding'] = 0;

            $req=$Conn->prepare("SELECT * FROM Clients
            WHERE cli_groupe = 1 AND cli_filiale_de = :cli_id AND cli_fil_de != 0 AND cli_fil_de != :cli_maison_mere AND cli_id != :cli_id;");
            $req->bindParam(":cli_maison_mere", $client['cli_id']);
            $req->bindParam(":cli_id", $c['cli_id']);
            $req->execute();
            $_clients=$req->fetchAll();
            $k = 0;
            foreach ($_clients as $fil_de) {
                $k = $k + 1;
                $clients[$i + $k] = client_array();
                $clients[$i + $k]['cli_id'] = $fil_de['cli_id'];
                $clients[$i + $k]['cli_code'] = $fil_de['cli_code'];
                $clients[$i + $k]['cli_nom'] = $fil_de['cli_nom'];
                $sql="SELECT adr_siret FROM Adresses WHERE adr_ref_id=" . $fil_de['cli_id'] . " AND adr_defaut AND adr_type=2;";
                $req = $Conn->query($sql);
                $adresse_fac=$req->fetch();
                $clients[$i + $k]['cli_siret'] = $fil_de['cli_siren'] . $adresse_fac['adr_siret'];
                $clients[$i + $k]['cli_holding'] = $c['cli_id'];
                $clients[$i + $k]['cli_subholding'] = $fil_de['cli_id'];
            }
        }
    }
}

foreach ($clients as $client) {
    $array = [
        'code' => $client['cli_code'],
        'name' => $client['cli_nom'],
        'siret' => preg_replace('/\s+/', '', $client['cli_siret'])
    ];

    if (!empty($client['cli_groupe'])) {
        $array['group'] = 1;
    }

    if (!empty($client['cli_holding'])) {
        $tableSource = plateformCall($array, '/table-sources/1/Clients/' . $client['cli_holding'], 'GET');

        if (!empty($tableSource['data'])) {
            $array['holding'] = $tableSource['data']['tableid'];
        }
    }

    if (!empty($client['cli_subholding'])) {
        $tableSource = plateformCall($array, '/table-sources/1/Clients/' . $client['cli_subholding'], 'GET');
        if (!empty($tableSource['data'])) {
            $array['subholding'] = $tableSource['data']['tableid'];
        }
    }

    $client_plateforme = plateformCall($array, '/customers', 'POST');

    if (!empty($client_plateforme['data'])) {

        $array = [
            'source_id' => 1,
            'tablename' => 'customers',
            'tableid' => $client_plateforme['data']['id'],
            'extname' => 'Clients',
            'extid' => $client['cli_id'],
        ];

        $tableSource = plateformCall($array, '/table-sources', 'POST');


    } elseif (!empty($client_plateforme['errors']) && !empty($client_plateforme['errors']['code'])) {

        $tableSource = plateformCall($array, '/table-sources/1/Clients/' . $client['cli_id'], 'GET');

        if (!empty($tableSource['data'])) {

            $client_plateforme = plateformCall($array, '/customers/' . $tableSource['data']['tableid'], 'GET');

            if (!empty($client_plateforme['data'])) {
                $update = plateformCall($array, '/customers/' . $client_plateforme['data']['id'], 'PATCH');
            }
        }

    } else {
        if (empty($erreur)) {
            $erreur = $array['code'];
        } else {
            $erreur .= ", " . $array['code'];
        }

    }
}

if ($erreur) {
    $_SESSION['message'][] = array(
        "titre" => "Attention",
        "type" => "warning",
        "message" => "Certains clients n'ont pas été transférés : " . $erreur
    );
} else {
    $_SESSION['message'][] = array(
        "titre" => "Succès",
        "type" => "success",
        "message" => "Le client a bien été transféré/actualisé vers la plateforme"
    );
}

$adresse="client_voir.php?client=" . $_GET['client'];
header("location : " . $adresse);
