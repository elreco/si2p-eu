<?php
include "includes/controle_acces.inc.php";
error_reporting( error_reporting() & ~E_NOTICE );
include "includes/connexion.php";
include "modeles/mod_upload.php";
include "modeles/mod_societe.php";
include "modeles/mod_utilisateur.php";
include "modeles/mod_agence.php";

$s = array();

$soc_compta=0;
$soc_compta_age=0;

if (isset($_GET['id'])) {
	
	$s = get_societe($_GET['id']);
	if(!empty($s["soc_compta"])){
		$soc_compta=$s["soc_compta"];
	}
	if(!empty($s["soc_compta_age"])){
		$soc_compta_age=$s["soc_compta_age"];
	}
}else{
  $s = array();
}

// LISTE DES SOCIETES 
$req=$Conn->query("SELECT soc_id,soc_code FROM Societes ORDER BY soc_code;");
$d_societes=$req->fetchAll();


// AGENCES
$d_agences=array();
if(!empty($soc_compta)){
	$req=$Conn->query("SELECT age_id,age_code FROM Agences WHERE age_societe=" . $soc_compta . " ORDER BY age_code;");
	$d_agences=$req->fetchAll();

}

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Paramètres</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<!--
		<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
		-->
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">	
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<!-- Favicon -->
		<link rel="icon" type="image/png" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
	
  <form method="post" action="societe_enr.php" enctype="multipart/form-data">
    <!-- Start: Main -->
    <div id="main">
      <?php
      include "includes/header_def.inc.php";
      ?>
      <!-- Start: Content-Wrapper -->
      <section id="content_wrapper">
       <section id="content" class="animated fadeIn">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="admin-form theme-primary ">
              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">
                  <div class="text-center">
                    <div class="content-header">
                      <?php	if(isset($_GET['id'])){
                       echo("<h2>Édition d'une <b class='text-primary'>société</b></h2>");
                     }else{
                       echo("<h2>Ajouter une <b class='text-primary'>société</b></h2>");
                     } ?>						
                   </div>
                   <?php if(isset($_GET['error']) && $_GET['error'] == 1): ?>
                    <div class="alert alert-danger">
                      Il y a eu un problème pour mettre en ligne un de vos logos. Veuillez réessayer.
                    </div>
                   <?php endif; ?>
                   <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="section-divider mb40">
                          <span>Informations générales</span>
                        </div>
                      </div>
                    </div>
                    <?php if (isset($_GET['id'])): ?>
                      <input type="hidden" name="soc_id" id="soc_id" value="<?=$_GET['id']?>">
                    <?php endif;?>
                    <div class="row">
                      <div class="col-md-8">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="text" name="soc_nom" id="soc_nom" class="gui-input input-sm nom" placeholder="Nom" required="" value="<?=$s['soc_nom']?>">
                            <label for="soc_nom" class="field-icon">
                              <i class="fa fa-building-o"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="text" name="soc_code" id="soc_code" class="gui-input nom" placeholder="Code" required="" value="<?=$s['soc_code']?>">
                            <label for="soc_code" class="field-icon">
                              <i class="fa fa-code"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="text" name="soc_ad1" id="soc_ad1" class="gui-input" placeholder="Adresse" required="" value="<?=$s['soc_ad1']?>">
                            <label for="soc_ad1" class="field-icon">
                              <i class="fa fa-map-marker"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="text" name="soc_ad2" class="gui-input" id="soc_ad2" placeholder="Adresse (Complément 1)" value="<?=$s['soc_ad2']?>">
                            <label for="soc_ad2" class="field-icon">
                              <i class="fa fa-map-marker"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="text" name="soc_ad3" id="soc_ad3" class="gui-input" placeholder="Adresse (Complément 2)" value="<?=$s['soc_ad3']?>">
                            <label for="soc_ad3" class="field-icon">
                              <i class="fa fa-map-marker"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="text" id="soc_cp" name="soc_cp" class="gui-input code-postal" placeholder="Code postal" required="" value="<?=$s['soc_cp']?>">
                            <label for="soc_cp" class="field-icon">
                              <i class="fa fa fa-certificate"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-9">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="text" name="soc_ville" id="soc_ville" class="gui-input nom" placeholder="Ville" required="" value="<?=$s['soc_ville']?>">
                            <label for="soc_ville" class="field-icon">
                              <i class="fa fa fa-building"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input name="soc_tel" id="soc_tel" type="tel" class="gui-input telephone" placeholder="Numéro de téléphone" value="<?=$s['soc_tel']?>">
                            <label for="soc_tel" class="field-icon">
                              <i class="fa fa fa-phone"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input name="soc_fax" id="soc_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax" value="<?=$s['soc_fax']?>">
                            <label for="soc_fax" class="field-icon">
                              <i class="fa fa-phone-square"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="email" name="soc_mail" id="soc_mail" class="gui-input" placeholder="Adresse Email" value="<?=$s['soc_mail']?>">
                            <label for="soc_mail" class="field-icon">
                              <i class="fa fa-envelope"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="url" name="soc_site" id="soc_site" class="gui-input" placeholder="Site Internet" value="<?=$s['soc_site']?>">
                            <label for="soc_site" class="field-icon">
                              <i class="fa fa-globe"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
	<?php			if (isset($_GET['id'])){ ?>
						<div class="row text-left">
							<div class="col-md-12">
								<label class="option">
									<input type="checkbox" name="soc_archive" value="1"  <?php if($s['soc_archive']==1) echo("checked"); ?> />
									<span class="checkbox"></span>Archivé
								</label>
							</div>
						</div>
	<?php			} ?>
							
                    <div class="row">

                      <div class="col-md-12">
                        <div class="section-divider mb40" >
                          <span>Informations juridiques</span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="section">
                          <div class="field select">
                            <select name="soc_responsable" id="soc_responsable">
                              <option value="">Responsable ...</option>
                              <?php if (isset($_GET['id'])):
                              echo get_utilisateur_select(0,0,"","",$s['soc_responsable']);
                              else:
                                echo get_utilisateur_select(0,0,"","",0);
                              endif;?>


                            </select>
                            <i class="arrow simple"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="text" name="soc_siren" id="soc_siren" class="gui-input siren" placeholder="Siren" required="" value="<?=$s['soc_siren']?>">
                            <label for="soc_siren" class="field-icon">
                              <i class="fa fa-barcode"></i>
                            </label>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="text" name="soc_siret" id="soc_siret" class="gui-input siretlong" placeholder="Siret" required="" value="<?=$s['soc_siret']?>">
                            <label for="soc_siret" class="field-icon">
                              <i class="fa fa-barcode"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                      
                    </div>
                    <div class="row">

                      <div class="col-md-6">
                        <div class="section">
                          <label for="tva" class="field">
                            <input type="text" name="soc_tva" id="tva" class="gui-input" placeholder="TVA Intracommune" required="" value="<?=$s['soc_tva']?>">
                          </label>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="section">
                          <label for="soc_tva_type" class="field">
                            <input type="text" name="soc_tva_type" id="soc_tva_type"  class="gui-input" placeholder="Type de TVA" value="<?=$s['soc_tva_type']?>">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="section">
                          <div class="field prepend-icon">
                            <input type="text" name="soc_num_existence" id="num_existence" class="gui-input" placeholder="Numéro d'existence" required="" value="<?=$s['soc_num_existence']?>">
                            <label for="num_existence" class="field-icon">
                              <i class="fa fa-barcode"></i>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="section">
                          <label class="field">
                            <input type="text" name="soc_ape" id="soc_ape" class="gui-input" placeholder="APE" required="" value="<?=$s['soc_ape']?>">
                          </label>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="section">
                          <label class="field">
                            <input type="text" name="soc_rcs" id="soc_rcs" class="gui-input" placeholder="RCS" required="" value="<?=$s['soc_rcs']?>">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                      <div class="section">
                          <label class="field">
                            <input type="text" name="soc_region_nom" id="soc_region_nom" class="gui-input" placeholder="Nom de la region" value="<?=$s['soc_region_nom']?>">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="section">
                          <span class="field prepend-icon">
                            <input type="text" name="soc_type" id="soc_type" class="gui-input" placeholder="Type" required="" value="<?=$s['soc_type']?>">
                            <span class="field-icon">
                              <i class="fa fa-gavel"></i>
                            </span>
                          </span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="section">
                          <div class="smart-widget sm-right smr-80">
                            <span class="prepend-icon">
                              <input type="text" name="soc_capital" id="soc_capital" class="gui-input input-int" placeholder="Capital" required="" value="<?=$s['soc_capital']?>">
                              <span class="field-icon">
                                <i class="fa fa-university"></i>
                              </span>
                            </span>
                            <span class="button">€</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr class="alt short">
                    <div class="section">
                      <p> Les horaires sont à entrer sous la forme: <strong>XXhXX</strong></p>
                    </div>
                    <div class="row">

                      <h4 class="text-left">Matin</h4>

                      <div class="col-md-6">
                        <div class="section">
                          <span class="prepend-icon">
                            <input type="text" id="timepicker1" name="soc_h_deb_matin" class="gui-input heure" placeholder="Heure de début du matin" value="<?=$s['soc_h_deb_matin']?>">
                            <span class="field-icon">
                              <i class="fa fa-clock-o"></i>
                            </span>
                          </span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="section">
                          <span class="prepend-icon">
                            <input type="text" id="timepicker2" name="soc_h_fin_matin" class="gui-input heure" placeholder="Heure de fin du matin" value="<?=$s['soc_h_fin_matin']?>">
                            <span class="field-icon">
                              <i class="fa fa-clock-o"></i>
                            </span>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <h4 class="text-left">Après-midi</h4>
                      <div class="col-md-6">
                        <div class="section">
                          <span class="prepend-icon">
                            <input type="text" id="timepicker3" name="soc_h_deb_am" class="gui-input heure" placeholder="Heure de début de l'après-midi" value="<?=$s['soc_h_deb_am']?>">
                            <span class="field-icon">
                              <i class="fa fa-clock-o"></i>
                            </span>
                          </span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="section">
                          <span class="prepend-icon">
                            <input type="text" id="timepicker4" name="soc_h_fin_am" class="gui-input heure" placeholder="Heure de fin de l'après-midi" value="<?=$s['soc_h_fin_am']?>">
                            <span class="field-icon">
                              <i class="fa fa-clock-o"></i>
                            </span>
                          </span>
                        </div>
                      </div>
                    </div>
                    <hr class="alt short">
                    <div class="row">
                      <div class="col-md-12 text-left">
                        <div class="section">
                          <div class="option-group field section">
                            <label for="soc_agence" class="block mt15 option option-primary">
                              <input type="radio" name="soc_agence" id="soc_agence" value="1" <?php if($s['soc_agence']==1) echo("checked"); ?> >
                              <span class="radio"></span>Cette société possède une agence
                            </label>
                            <label for="soc_no_agence" class="block mt15 option option-primary">
                              <input type="radio" name="soc_agence" id="soc_no_agence" value="0" <?php if($s['soc_agence']!=1) echo("checked"); ?> >
                              <span class="radio"></span>
                              Cette société ne possède pas d'agence
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>


                    <!-- end: .col-md-4 -->
                    <div class="row">

                      <div class="col-md-12">
                        <div class="section-divider mb40">
                          <span>Logos</span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 col-md-offset-1">
                        
                        <div class="col-md-12">
                          <div class="image-preview image-350x140" >
                            <img <?php if (isset($_GET['id']) && !empty($s['soc_logo_1'])): ?> src="documents/societes/logos/<?=$s['soc_logo_1']?>" <?php else: ?>src="assets/img/400x140.jpg"<?php endif;?> alt="" id="image_preview1" />
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="section">
                            <span class="prepend-icon file">
                              <span class="button btn-primary">Choisir</span>
                              <input type="file" class="gui-file" name="soc_logo_1" id="inputFile1" onchange="document.getElementById('uploader0').value = this.value;" >
                              <input type="text" class="gui-input" id="uploader0" placeholder="Logo 1">
                              <span class="field-icon">
                                <i class="fa fa-upload"></i>
                              </span>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-md-offset-2">

                        <div class="image-preview image-350x140">
                          <img <?php if (isset($_GET['id']) && !empty($s['soc_logo_2'])): ?>src="documents/societes/logos/<?=$s['soc_logo_2']?>"<?php else: ?>src="assets/img/400x140.jpg"<?php endif;?> id="image_preview2">
                        </div>
                        <div class="col-md-12">
                          <div class="section">
                            <span class="field prepend-icon file">
                              <span class="button btn-primary">Choisir</span>
                              <input type="file" class="gui-file" name="soc_logo_2" onchange="document.getElementById('uploader1').value = this.value;" id="inputFile2">
                              <input type="text" class="gui-input" id="uploader1" placeholder="Logo 2">
                              <span class="field-icon">
                                <i class="fa fa-upload"></i>
                              </span>
                            </span>
                          </div>
                        </div>

                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 col-md-offset-1">

                        <div class="col-md-12">
                          <div class="image-preview image-350x140" >
                            <img <?php if (isset($_GET['id']) && !empty($s['soc_qualite_1'])): ?>src="documents/societes/logos/<?=$s['soc_qualite_1']?>"<?php else: ?>src="assets/img/400x140.jpg"<?php endif;?> alt="" id="image_preview3" />
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="section">
                            <span class="prepend-icon file">
                              <span class="button btn-primary">Choisir</span>
                              <input type="file" class="gui-file" name="soc_qualite_1" id="inputFile3" onchange="document.getElementById('uploader2').value = this.value;" >
                              <input type="text" class="gui-input" id="uploader2" placeholder="Qualité 1">
                              <span class="field-icon">
                                <i class="fa fa-upload"></i>
                              </span>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-md-offset-2">

                        <div class="image-preview image-350x140">
                          <img <?php if (isset($_GET['id']) && !empty($s['soc_qualite_2'])): ?>src="documents/societes/logos/<?=$s['soc_qualite_2']?>"<?php else: ?>src="assets/img/400x140.jpg"<?php endif;?> id="image_preview4">
                        </div>
                        <div class="col-md-12">
                          <div class="section">
                            <span class="field prepend-icon file">
                              <span class="button btn-primary">Choisir</span>
                              <input type="file" class="gui-file" name="soc_qualite_2" onchange="document.getElementById('uploader3').value = this.value;" id="inputFile4">
                              <input type="text" class="gui-input" id="uploader3" placeholder="Qualité 2">
                              <span class="field-icon">
                                <i class="fa fa-upload"></i>
                              </span>
                            </span>
                          </div>
                        </div>

                      </div>
                    </div>
						<div class="row">
							<div class="col-md-12">
								<div class="section-divider mb40">
									<span>Comptabilité</span>
								</div>
							</div>
						</div>
						
						<div class="row mb10" >
                            <div class="col-md-6" >
								<select name="soc_compta" id="" class="select2 select2-societe" required >
									<option value="0" >Sélectionnez une société</option>
							<?php	if(!empty($d_societes)){
										foreach($d_societes as $d_soc){
											if($d_soc["soc_id"]==$soc_compta){
												echo("<option value='" . $d_soc["soc_id"] . "' selected >" . $d_soc["soc_code"] . "</option>");
											}else{
												echo("<option value='" . $d_soc["soc_id"] . "' >" . $d_soc["soc_code"] . "</option>");
											}
										}
									} ?>
                                </select>
                            </div>
                            <div class="col-md-6" >
								<select name="soc_compta_age" id="agence" class="select2 select2-societe-agence" >
									<option value="0" >Sélectionnez une agence</option>
							<?php	if(!empty($d_agences)){
										foreach($d_agences as $d_age){
											if($d_age["age_id"]==$soc_compta_age){
												echo("<option value='" . $d_age["age_id"] . "' selected >" . $d_age["age_code"] . "</option>");
											}else{
												echo("<option value='" . $d_age["age_id"] . "' >" . $d_age["age_code"] . "</option>");
											}
										}
									} ?>
                                </select>
                            </div>
                        </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="section">
                      <span class="prepend-icon">
                        <input type="email" name="soc_compta_mail" id="soc_compta_mail" class="gui-input" placeholder="Adresse Email" value="<?=$s['soc_compta_mail']?>">
                        <span class="field-icon">
                          <i class="fa fa-envelope"></i>
                        </span>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="section">
                      <input type="checkbox" name="soc_tva_exo" id="soc_tva_exo" value="1" <?php if(!empty($s['soc_tva_exo'])): ?> checked <?php endif; ?>>
                      <label for="pro_recurrent">Exonération TVA</label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="section">
                      <span class="prepend-icon">
                        <input name="soc_compta_tel" id="soc_compta_tel" type="tel" class="gui-input telephone" placeholder="Numéro de téléphone" value="<?=$s['soc_compta_tel']?>">
                        <span class="field-icon">
                          <i class="fa fa fa-phone"></i>
                        </span>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="section">
                      <span class="prepend-icon">
                        <input name="soc_compta_fax" id="soc_compta_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax" value="<?=$s['soc_compta_fax']?>">
                        <span class="field-icon">
                          <i class="fa fa-phone-square"></i>
                        </span>
                      </span>
                    </div>
                  </div>
                </div>
				
					<div class="row">
						<div class="col-md-6">
							<div class="section">
								<div class="smart-widget sm-right smr-80">
									<input type="text" name="soc_interco_intra" id="soc_interco_intra" class="gui-input input-int" placeholder="Pourcentage Interco INTRA" min="0" max="100" value="<?=$s['soc_interco_intra']?>">
									<span class="button">%</span>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="section">
								<div class="smart-widget sm-right smr-80">
									<input type="text" name="soc_interco_inter" id="soc_interco_inter" class="gui-input input-int" placeholder="Pourcentage Interco INTER" min="0" max="100" value="<?=$s['soc_interco_inter']?>">
									<span class="button">%</span>
								</div>
							</div>
						</div>
					</div>
		  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End: Content -->
</section>
<!-- End: Content WRAPPER -->
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
  <div class="row">
    <div class="col-xs-3 footer-left" >
      <a href="societe_liste.php" class="btn btn-default btn-sm">
        <i class="fa fa-long-arrow-left"></i>
        Retour
      </a>
    </div>
    <div class="col-xs-6 footer-middle"></div>
    <div class="col-xs-3 footer-right" >
      <button type="submit" name="submit" class="btn btn-success btn-sm">
        <i class='fa fa-save'></i> Enregistrer
      </button>
    </div>
  </div>
</footer>
</form>
<?php
		include "includes/footer_script.inc.php"; ?>	
		
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>
			
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
				$("#inputFile1").change(function () {
				  readURL1(this);
				});
				$("#inputFile2").change(function () {
					readURL2(this);
				});
				$("#inputFile3").change(function () {
					readURL3(this);
				});				
				$("#inputFile4").change(function () {
					readURL4(this);
				});
				
			});

			// changer l'image
			function readURL1(input) {

			  if (input.files && input.files[0]) {

				var reader = new FileReader();

				reader.onload = function (e) {
				  $('#image_preview1').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			  }
			}
			// changer l'image
			function readURL2(input) {

			  if (input.files && input.files[0]) {

				var reader = new FileReader();

				reader.onload = function (e) {
				  $('#image_preview2').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			  }
			}
			// changer l'image
			function readURL3(input) {

			  if (input.files && input.files[0]) {

				var reader = new FileReader();

				reader.onload = function (e) {
				  $('#image_preview3').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			  }
			}
			// changer l'image
			function readURL4(input) {

			  if (input.files && input.files[0]) {

				var reader = new FileReader();

				reader.onload = function (e) {
				  $('#image_preview4').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			  }
			}

			   
		</script>
</body>
</html>
