<?php

	include "includes/controle_acces.inc.php";

	include('includes/connexion.php');
	
	include('modeles/mod_parametre.php');
	include('modeles/mod_tva.php');
	
	$erreur=0;
	
	$tpe_id=0;
	if(!empty($_POST["tpe_id"])){
		$tpe_id=$_POST["tpe_id"];
	}
	$tpe_tva=0;
	if(!empty($_POST["tpe_tva"])){
		$tpe_tva=$_POST["tpe_tva"];
	}else{
		$erreur=1;
	}
	
	$tpe_taux=0;
	if(!empty($_POST["tpe_taux"])){
		$tpe_taux=$_POST["tpe_taux"];
	}
	
	$tpe_date_deb=null;
	if(!empty($_POST["tpe_date_deb"])){
		$tpe_date_deb=$_POST["tpe_date_deb"];
	}else{
		$erreur=0;
	}
	$tpe_cpt_ven=0;
	if(!empty($_POST["tpe_cpt_ven"])){
		$tpe_cpt_ven=$_POST["tpe_cpt_ven"];
	}
	$tpe_cpt_ach=0;
	if(!empty($_POST["tpe_cpt_ach"])){
		$tpe_cpt_ach=$_POST["tpe_cpt_ach"];
	}
	
	if($erreur==0){
		
		$date_deb=convert_date_sql($tpe_date_deb);
		
		if($tpe_id==0){
			
			insert_tva_periode($tpe_tva,$tpe_taux,$date_deb,$tpe_cpt_ven,$tpe_cpt_ach);
					
		}else{
			
			update_tva_periode($tpe_id,$tpe_tva,$tpe_taux,$date_deb,$tpe_cpt_ven,$tpe_cpt_ach);
		}
		
		foreach($base_tva as $cle => $value){
			
			if($cle>0){
				
				$histo_tva=get_tvas_periode($cle);
				
				if(!empty($histo_tva)){		
				
					$nbPeriode=count($histo_tva);
					
					foreach($histo_tva as $cle => $value){
						
						$tva_periode=$value["tpe_id"];
								
						$tpe_date_fin=null;				
						if($cle<$nbPeriode-1){	
							$tpe_date_fin=$histo_tva[$cle+1]["tpe_date_deb"];
							$tpe_date_fin=ajout_date($tpe_date_fin,"d",-1);	
												
						}
						
						update_tva_periode_fin($tva_periode,$tpe_date_fin);
					}
				}
			}
		}
	
	};
	
	header('Location: param_tva_liste.php?erreur='. $erreur);     
	
?>