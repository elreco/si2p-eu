<?php 
	include "includes/controle_acces.inc.php";

/* 	LISTE DES CLIENTS (LES SUSPECTS SONT GERE SUR suspect_liste.php)
	Tous les clients sont stockés sur la base ORION 

	NOTE

	$_SESSION['acces']["acc_droits"][37] : ne pas accorder ce droit à un user sans mon accord (FG)
*/

	include('includes/connexion.php');

	include('modeles/mod_orion_cli_categories.php');
	include('modeles/mod_orion_cli_sous_categories.php');

	include('modeles/mod_parametre.php');
	require "modeles/mod_contact.php";
	require "modeles/mod_erreur.php";


	// DONNEE UTILE AU PROGRAMME

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}
	$_SESSION['retour'] = "client_liste.php";

	$d_client_categorie=orion_cli_categories();
	$d_client_sous_categorie=orion_cli_sous_categories();

	if (isset($_POST['search'])){
		
		// le formulaire a été soumit
		// on actualise les criteres de recherche
		
		
		// critere sur le client
		$cli_categorie=0;
		if(!empty($_POST['cli_categorie'])){
			$cli_categorie=intval($_POST['cli_categorie']);
		}
		$cli_sous_categorie=0;
		if(!empty($_POST['cli_sous_categorie'])){
			$cli_sous_categorie=intval($_POST['cli_sous_categorie']);
		}
		$cli_id=0;
		if(!empty($_POST['cli_id'])){
			$cli_id=intval($_POST['cli_id']);
		}
		$cli_groupe=0;
		if(!empty($_POST['cli_groupe'])){
			$cli_groupe=intval($_POST['cli_groupe']); // 1 mm 2 groupe 3 entreprise
		}
	
		$date1 = convert_date_sql($_POST['cli_uti_dat_1']);
		$date2 = convert_date_sql($_POST['cli_uti_dat_2']);
		
		$cli_prescripteur=0;
		if(!empty($_POST['cli_prescripteur'])){
			$cli_prescripteur=intval($_POST['cli_prescripteur']);
		}
		$cli_ape=0;
		if(!empty($_POST['cli_ape'])){
			$cli_ape=intval($_POST['cli_ape']);
		}
		$cli_classification=0;
		if(!empty($_POST['cli_classification'])){
			$cli_classification=intval($_POST['cli_classification']);
		}
		$cli_classification_categorie=0;
		if(!empty($_POST['cli_classification_categorie'])){
			$cli_classification_categorie=intval($_POST['cli_classification_categorie']);
		}
		$cli_classification_type=0;
		if(!empty($_POST['cli_classification_type'])){
			$cli_classification_type=intval($_POST['cli_classification_type']);
		}
		
		$cli_siren="";
		$adr_siret="";
		if(!empty($_POST['cli_siren'])){
			$cli_siren=$_POST['cli_siren'];
			if(!empty($_POST['cli_siret']) AND strlen($cli_siren)==11){
				$adr_siret=$_POST['cli_siret']; 
			}
		}
		
		// autre info
		
		$cli_important=0;
		if(isset($_POST['cli_important'])){
			$cli_important=1;
		}
		
		$cli_affacturage=0;
		if(isset($_POST['cli_affacturage'])){
			$cli_affacturage=intval($_POST['cli_affacturage']);
		}
		
		$cli_import=0;
		if(!empty($_POST['cli_import'])){
			$cli_import=intval($_POST['cli_import']);		
		}
		
		$cli_reseau=0;
		if($_SESSION['acces']["acc_profil"]==13){
			if(isset($_POST['cli_reseau'])){
				if(!empty($_POST['cli_reseau'])){
					$cli_reseau=1;
				}
			}
		}
		
		
		// adresse
		
		$adr_type=0;
		if(!empty($adr_siret)){
			$adr_type=2; // facturation
		}elseif(!empty($_POST['cli_type_adresse'])){
			$adr_type=intval($_POST['cli_type_adresse']);
		}
		$adr_defaut=0;
		if(isset($_POST['adr_defaut'])){
			$adr_defaut=1;
		}
		
		// contact
		$con_mail=0; // 1 renseigne 2 non renseigné
		if(!empty($_POST['con_mail'])){
			$con_mail=intval($_POST['con_mail']);
		}
		$con_fonction=0; // 1 renseigne 2 non renseigné
		if(!empty($_POST['con_fonction'])){
			$con_fonction=intval($_POST['con_fonction']);
		}
		
		// correspondance
		
		$cor_date1 = convert_date_sql($_POST['cli_cor_dat_1']);
		$cor_date2 = convert_date_sql($_POST['cli_cor_dat_2']);
		
		$correspondance=0;
		if(!empty($_POST['cli_correspondance'])){
			$correspondance=intval($_POST['cli_correspondance']); // 1 avec //2 sans
		}
		
		$cli_depuis = convert_date_sql($_POST['cli_depuis']);
		
		// critere d'affichage
		
		$aff_cor=0; // 0 rappel 1 dernier corresp
		if(!empty($_POST['aff_cor'])){
			$aff_cor=intval($_POST['aff_cor']);
		}
		
		// critère client societe
		
		$cso_archive=0;
		if(isset($_POST['cli_archive'])){
			$cso_archive=1;
		}
		$cso_commercial=0;
		if(!empty($_POST['cli_commercial'])){
			$cso_commercial=intval($_POST['cli_commercial']);
		}
		
		// mémorisation des critere

		$_SESSION['cli_tri'] = array(
			"cli_code"         => $_POST['cli_code'],
			"cli_nom"          => $_POST['cli_nom'],
			"cli_date1"        => $date1,
			"cli_date2"        => $date2,
			"cli_categorie"    => $cli_categorie,
			"cli_sous_categorie"    => $cli_sous_categorie,
			"cli_id"           => $cli_id,
			"cli_prescripteur" => $cli_prescripteur,
			"cli_ape" => $cli_ape,
			"cli_classification" => $cli_classification,
			"cli_classification_type" => $cli_classification_type,
			"cli_classification_categorie" => $cli_classification_categorie,
			"cli_groupe" => $cli_groupe,
			"cli_siren" => $cli_siren,
			"cli_reference" => $_POST['cli_reference'],
			"cli_important" => $cli_important,
			"cli_affacturage" => $cli_affacturage,
			"cli_reseau" => $cli_reseau,
			"cli_import" => $cli_import,
						
			// adresse
			
			"adr_type" => $adr_type,
			"adr_siret" => $adr_siret, // ne contient que les 5 dernier chiffre
			"adr_defaut" => $adr_defaut,	
			"adr_cp" => $_POST['adr_cp'],
			"adr_ville" => $_POST['adr_ville'],
			
			
			//contact
			"con_nom"          	=> $_POST['con_nom'],
			"con_prenom"       	=> $_POST['con_prenom'],
			"con_mail" 			=> $con_mail,
			"con_fonction" 		=> $con_fonction,
			"con_tel"       	=> $_POST['con_tel'],

			//correspondance
			"cor_date1"        => $cor_date1,
			"cor_date2"        => $cor_date2,
			"correspondance" => $correspondance,
			"cli_depuis" => $cli_depuis,
			
			//affichage
			"aff_cor" => $aff_cor,
			
			// client societe
			"cso_archive" => $cso_archive,
			"cso_commercial"   => $cso_commercial
			
			
		);
	}

	// ON CONSTRUIT LA REQUETE

	$critere=array();
	
	$mil="";
	
	$rechercheParAdresse=false;
	$rechercheParContact=false;
	$rechercheParCorrespondance=false;
	
	if(!empty($_SESSION['cli_tri']['cli_id'])){
		$mil.=" AND cli_id =:cli_id"; 
		$critere["cli_id"]=$_SESSION['cli_tri']['cli_id'];
	}else{
		
		if(!empty($_SESSION['cli_tri']['cli_code'])){
			$mil.=" AND cli_code LIKE :cli_code"; 
			$critere["cli_code"]="%" . $_SESSION['cli_tri']['cli_code']."%";
		};
		if(!empty($_SESSION['cli_tri']['cli_nom'])){
			$mil.=" AND cli_nom LIKE :cli_nom"; 
			$critere["cli_nom"]="%" . $_SESSION['cli_tri']['cli_nom'] ."%";
		};
		if(!empty($_SESSION['cli_tri']['cli_date1'])){
			$mil.=" AND cli_date_creation >=:cli_date1"; 
			$critere["cli_date1"]=$_SESSION['cli_tri']['cli_date1'];
		};
		if(!empty($_SESSION['cli_tri']['cli_date2'])){
			$mil.=" AND cli_date_creation <=:cli_date2"; 
			$critere["cli_date2"]=$_SESSION['cli_tri']['cli_date2'];
		};
		if(!empty($_SESSION['cli_tri']['cli_categorie'])){
			$mil.=" AND cli_categorie = :cli_categorie"; 
			$critere["cli_categorie"]=$_SESSION['cli_tri']['cli_categorie'];
		};
		if(!empty($_SESSION['cli_tri']['cli_sous_categorie'])){
			$mil.=" AND cli_sous_categorie = :cli_sous_categorie";
			$critere["cli_sous_categorie"]=$_SESSION['cli_tri']['cli_sous_categorie'];
		};
		
		if(!empty($_SESSION['cli_tri']['cli_prescripteur'])){
			$mil.=" AND cli_prescripteur =:cli_prescripteur"; 
			$critere["cli_prescripteur"]=$_SESSION['cli_tri']['cli_prescripteur'];
		};
		if(!empty($_SESSION['cli_tri']['cli_ape'])){
			$mil.=" AND cli_ape =:cli_ape"; 
			$critere["cli_ape"]=$_SESSION['cli_tri']['cli_ape'];
		};
		if(!empty($_SESSION['cli_tri']['cli_classification'])){
			$mil.=" AND cli_classification =:cli_classification"; 
			$critere["cli_classification"]=$_SESSION['cli_tri']['cli_classification'];
		};
		if(!empty($_SESSION['cli_tri']['cli_classification_categorie'])){
			$mil.=" AND cli_classification_categorie =:cli_classification_categorie"; 
			$critere["cli_classification_categorie"]=$_SESSION['cli_tri']['cli_classification_categorie'];
		};
		if(!empty($_SESSION['cli_tri']['cli_classification_type'])){
			$mil.=" AND cli_classification_type =:cli_classification_type"; 
			$critere["cli_classification_type"]=$_SESSION['cli_tri']['cli_classification_type'];
		};
		
		
		
		if(!empty($_SESSION['cli_tri']['cli_groupe'])){
			if($_SESSION['cli_tri']['cli_groupe']==1){
				$mil.=" AND cli_groupe = 1 AND cli_filiale_de = 0"; 
			}else if($_SESSION['cli_tri']['cli_groupe']==2){
				$mil.=" AND cli_groupe != 0"; 
			}else if($_SESSION['cli_tri']['cli_groupe']==3){
				$mil.=" AND cli_groupe = 0"; 
			}
		};
		if(!empty($_SESSION['cli_tri']['cli_siren'])){
			$mil.=" AND cli_siren LIKE :cli_siren"; 
			$critere["cli_siren"]=$_SESSION['cli_tri']['cli_siren'];
		};
		if(!empty($_SESSION['cli_tri']['cli_reference'])){
			$mil.=" AND cli_reference LIKE :cli_reference"; 
			$critere["cli_reference"]="%" . $_SESSION['cli_tri']['cli_reference'] . "%";
		};
		
		if($_SESSION['cli_tri']['cli_important'] == 1){
			$mil.=" AND cli_important = 1"; 
		};
		
		if($_SESSION['cli_tri']['cli_affacturage']==1){
			// client non affacturable
			$mil.=" AND NOT cli_affacturable=1";
		}elseif($_SESSION['cli_tri']['cli_affacturage'] == 2){
			// clients affacturés
			$mil.=" AND cli_affacturage"; 
		}elseif($_SESSION['cli_tri']['cli_affacturage']==3){
			// clients non affacturés
			$mil.=" AND cli_affacturable=1 AND NOT cli_affacturage";
		};
		
		
		if(!empty($_SESSION['cli_tri']['cli_import'])){
			$mil.=" AND cli_import_suspect=:cli_import AND cli_soc_suspect=" . $acc_societe; 
			$critere["cli_import"]=$_SESSION['cli_tri']['cli_import'];
		};
		
		
		

		// critère sur les adresses	
		
		if($_SESSION['cli_tri']['adr_type']==1 AND $_SESSION['cli_tri']['adr_defaut']==1){			
			// revient a chercher l'adresse qui est en table client
			// adr_siret adr_type a ete force a 2 si condition vrai adr_siret=""
			if(!empty($_SESSION['cli_tri']['adr_cp'])){
				$mil.=" AND cli_adr_cp LIKE :adr_cp"; 
				$critere["adr_cp"]="%" . $_SESSION['cli_tri']['adr_cp'] ."%";
			};
			if(!empty($_SESSION['cli_tri']['adr_ville'])){
				$mil.=" AND cli_adr_ville LIKE :adr_ville"; 
				$critere["adr_ville"]="%" . $_SESSION['cli_tri']['adr_ville'] ."%";
			};
		}else{
			
			if(!empty($_SESSION['cli_tri']['adr_type'])){
				$mil.=" AND adr_type=:adr_type" ; 
				$critere["adr_type"]=$_SESSION['cli_tri']['adr_type'];
				$rechercheParAdresse=true;
			};
			if(!empty($_SESSION['cli_tri']['adr_siret'])){
				$mil.=" AND adr_siret LIKE :adr_siret"; 
				$critere["adr_siret"]=$_SESSION['cli_tri']['adr_siret'];
				$rechercheParAdresse=true;
			};
			if($_SESSION['cli_tri']['adr_defaut']==1){
				$mil.=" AND adr_defaut=1";
				$rechercheParAdresse=true;
			};
			if(!empty($_SESSION['cli_tri']['adr_cp'])){
				$mil.=" AND adr_cp LIKE :adr_cp"; 
				$critere["adr_cp"]="%" . $_SESSION['cli_tri']['adr_cp'] ."%";
				$rechercheParAdresse=true;
			};
			if(!empty($_SESSION['cli_tri']['adr_ville'])){
				$mil.=" AND adr_ville LIKE :adr_ville"; 
				$critere["adr_ville"]="%" . $_SESSION['cli_tri']['adr_ville'] ."%";
				$rechercheParAdresse=true;
			};
		}

		// critère sur les contacts
		
		if(!empty($_SESSION['cli_tri']['con_nom'])){
			$mil.=" AND con_nom LIKE :con_nom"; 
			$critere["con_nom"]="%" . $_SESSION['cli_tri']['con_nom'] ."%";
			$rechercheParContact=true;
		};
		if(!empty($_SESSION['cli_tri']['con_prenom'])){
			$mil.=" AND con_prenom LIKE :con_prenom"; 
			$critere["con_prenom"]="%" . $_SESSION['cli_tri']['con_prenom'] ."%";
			$rechercheParContact=true;
		};

		if(!empty($_SESSION['cli_tri']['con_mail'])){
			if($_SESSION['cli_tri']['con_mail'] == 1){
				$mil.=" AND con_mail IS NOT NULL AND con_mail!=''"; 
			}else{
				$mil.=" AND (con_mail IS NULL OR con_mail='')"; 
			}
			$rechercheParContact=true;
		};
		if(!empty($_SESSION['cli_tri']['con_fonction'])){
			$mil.=" AND con_fonction =:con_fonction"; 
			$critere["con_fonction"]=$_SESSION['cli_tri']['con_fonction'];
			$rechercheParContact=true;
		};

		if(!empty($_SESSION['cli_tri']['con_tel'])){
			$mil.=" AND (con_tel =:con_tel OR con_portable =:con_tel OR con_fax =:con_tel)"; 
			$critere["con_tel"]="%" . $_SESSION['cli_tri']['con_tel'] . "%";
			$rechercheParContact=true;
		};

		// sur les correspondances
		
		if(!empty($_SESSION['cli_tri']['cor_date1'])){
			$mil.=" AND NOT cor_rappel AND cor_rappeler_le>=:cor_date1"; 
			$critere["cor_date1"]=$_SESSION['cli_tri']['cor_date1'];
			$rechercheParCorrespondance=true;
		};
		if(!empty($_SESSION['cli_tri']['cor_date2'])){
			$mil.=" AND NOT cor_rappel AND cor_rappeler_le<=:cor_date2"; 
			$critere["cor_date2"]=$_SESSION['cli_tri']['cor_date2'];
			$rechercheParCorrespondance=true;
		};
		if(!empty($_SESSION['cli_tri']['correspondance'])){
			if($_SESSION['cli_tri']['correspondance']>0){
				$rechercheParCorrespondance=true;
				if($_SESSION['cli_tri']['correspondance']==1){				
						// avec
					if(!empty($_SESSION['cli_tri']['cli_depuis'])){
						$mil.=" AND cor_date>=:cli_depuis";
						$critere["cli_depuis"]=$_SESSION['cli_tri']['cli_depuis'];
					}else{
						$mil.=" AND NOT ISNUll(cor_date)";
					}
				}else{
						// sans
					$mil.=" AND (ISNUll(cor_date)";
					if(!empty($_SESSION['cli_tri']['cli_depuis'])){
						$mil.=" OR cor_date<=:cli_depuis";
						$critere["cli_depuis"]=$_SESSION['cli_tri']['cli_depuis'];
					}
					$mil.=")";
				}
			}
		}
		
		// critère client societe
		if($_SESSION['cli_tri']['cso_archive'] == 1){
			$mil.=" AND cso_archive = 1"; 
		}else{
			$mil.=" AND cso_archive = 0"; 
		};
		
		if(!empty($_SESSION['cli_tri']['cso_commercial'])){
			$mil.=" AND cso_commercial =:cso_commercial";
			$critere["cso_commercial"]=$_SESSION['cli_tri']['cso_commercial'];
		};
	}
	// fin critère form
	
	// critere auto
	
	//accès à la fiche
	if($_SESSION['cli_tri']['cli_reseau']!==1){
		$mil.= " AND cso_societe = " . $acc_societe;
		if(!empty($acc_agence)){
			$mil.= " AND cso_agence = " . $acc_agence;
		}
		if(!$_SESSION['acces']["acc_droits"][6]){
			$mil.=" AND cso_utilisateur=" . $acc_utilisateur;
		}
	}
	$mil.=" AND NOT ISNULL(cli_first_facture) AND cli_first_facture>0";

	// champ a selectionne
	$sql="SELECT DISTINCT cli_id,cli_code,cli_nom,cli_groupe,cli_filiale_de,cli_categorie,cli_sous_categorie,cli_blackliste,cli_first_facture,cli_siren";
	if($rechercheParAdresse){
		$sql.=",adr_id,adr_type,adr_service AS ad_service,adr_ad1 AS ad1, adr_ad2 AS ad2, adr_ad3 AS ad3 ,adr_cp AS ad_cp ,adr_ville AS ad_ville";
	}else{
		$sql.=",cli_adr_service AS ad_service,cli_adr_ad1 AS ad1, cli_adr_ad2 AS ad2, cli_adr_ad3 AS ad3, cli_adr_cp AS ad_cp, cli_adr_ville AS ad_ville";
	}
	if($rechercheParContact){
		$sql.=",con_id,con_prenom AS prenom, con_nom AS nom, con_fonction AS fonction, con_fonction_nom AS fonction_nom ,con_tel AS tel, con_mail AS mail";
	}else{
		$sql.=",cli_con_prenom AS prenom, cli_con_nom AS nom, cli_con_fct AS fonction, cli_con_fct_nom AS fonction_nom, cli_con_tel AS tel, cli_con_mail AS mail";
	}
	if($_SESSION['cli_tri']['aff_cor']==1){
		$sql.=",cli_cor_date AS corresp_date";
	}else{
		$sql.=",cli_cor_rappel AS corresp_date";
	}
	if($_SESSION['cli_tri']['cli_reseau']==1){
		$sql.=",cso_societe";
	}
	// rechercheParCorrespondance n'affecte pas les champs a select
	
	// table
	$sql.=" FROM Clients";
	if($rechercheParAdresse){
		$sql.=" LEFT JOIN Adresses ON (Clients.cli_id = Adresses.adr_ref_id)";
	}
	if($rechercheParContact){
		$sql.=" LEFT JOIN Contacts ON (Clients.cli_id = Contacts.con_ref_id)";
	}
	if($rechercheParCorrespondance){
		$sql.=" LEFT JOIN Correspondances ON (Clients.cli_id = Correspondances.cor_client)";
	}
	$sql.=" LEFT JOIN Clients_Societes ON (Clients_Societes.cso_client = Clients.cli_id)";
	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY cli_code,cli_nom";
	/*echo($sql);
	die();*/
	$req = $Conn->prepare($sql);	
	if(!empty($critere)){
		foreach($critere as $c => $u){
			if($c == "debut"){
				$req->bindValue($c,$u, PDO::PARAM_INT);
			}else{
				
				$req->bindValue($c,$u);
			}
		}
	};
	$req->execute();
	$clients = $req->fetchAll();
	
	
?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<!-- <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css"> -->
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
	
		<!-- Start: Main -->
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
<?php 				if(isset($_GET['succes']) && $_GET['succes'] == 2){ ?>
						<div class="row">   
							<div class="col-md-12">
								<div class="alert alert-success">
									Le suspect a été passé en client
								</div>
							</div>
						</div> 
<?php 				}
					if(!empty($clients)){ ?>	
						
						<h1><?=count($clients)?> client(s)</h1>
						<div class="table-responsive">
							<table class="table" id="table_id">
								<thead>
									<tr class="info" >
										<th>ID</th>
								<?php	if($_SESSION['cli_tri']['cli_reseau']==1){ ?>
											<th>Code Société</th> 
								<?php	} ?>
										<th>Code</th> 
										<th>Nom</th>
										<th>Type d'adresse</th>
										<th>Adresse</th>
										<th>CP</th>
										<th>Ville</th>
							<?php		if($_SESSION['acces']["acc_droits"][37]){ ?>
											<th>Contact</th>
											<th>Tél.</th>
											<th>Mail</th>
							<?php		}
										if($_SESSION['acces']["acc_profil"]==13){ ?>
											<th>SIREN</th>
							<?php		}
										if($_SESSION['cli_tri']['aff_cor']==1){ ?>
											<th>Date corresp</th>
							<?php		}else{ ?>	
											<th>Date rappel</th>
							<?php		} ?>
									</tr>
								</thead>
								<tbody>
						<?php		foreach($clients as $c){
							
										$page[]=$c["cli_id"] . "-2";
										
										// style de la ligne
										$style="";
										$style_a="";									
										if(!empty($c['cli_blackliste'])){
											$style="background-color:#000;color:#FFF;";
										}elseif(empty($c['cli_first_facture'])){
											$style="background-color:#CFC;";
										}elseif(!empty($c['cli_sous_categorie'])){
											if(!empty($d_client_sous_categorie[$c['cli_sous_categorie']])){
												if(!empty($d_client_sous_categorie[$c['cli_sous_categorie']]["csc_couleur"])){
													$style="background-color:" . $d_client_sous_categorie[$c['cli_sous_categorie']]["csc_couleur"] . ";color:#FFF;";
													$style_a="color:#FFF;";
												}
											}
										}elseif(!empty($c['cli_categorie'])){
											if(!empty($d_client_categorie[$c['cli_categorie']])){
												if(!empty($d_client_categorie[$c['cli_categorie']]["cca_couleur"])){
													$style="background-color:" . $d_client_categorie[$c['cli_categorie']]["cca_couleur"] . ";";
												}
											}
										}
										
										if($c['cli_groupe']==1 AND $c['cli_filiale_de']==0){
											$style.="font-weight:bold;";	
										}
										
										
										$adresse_type="Intervention";
										if(!empty($c['adr_type'])){
											if($c['adr_type']==2){
												$adresse_type="Facturation";
											}elseif($c['adr_type']==3){
												$adresse_type="Envoi de facture";
											}
										}
										$adresse_lib="";
										if(!empty($c['ad_service'])){
											$adresse_lib=$c['ad_service'];
										}
										if(!empty($c['ad1'])){
											if(!empty($adresse_lib)){
												$adresse_lib.="<br/>";
											}
											$adresse_lib.=$c['ad1'];
										}
										if(!empty($c['ad2'])){
											if(!empty($adresse_lib)){
												$adresse_lib.="<br/>";
											}
											$adresse_lib.=$c['ad2'];
										}
										if(!empty($c['ad3'])){
											if(!empty($adresse_lib)){
												$adresse_lib.="<br/>";
											}
											$adresse_lib.=$c['ad3'];
										} 
										
										// fonction
										if(!empty($fonction)){
											
										}else{
											$fonction_nom=$c['fonction_nom'];
										}
										$corresp_date="";
										if(!empty($c['corresp_date'])){
											$dt_corresp_date=date_create_from_format("Y-m-d",$c['corresp_date']);
											$corresp_date=$dt_corresp_date->format("d/m/Y");
										}
										
										?>
										<tr <?php if(!empty($style)) echo("style='" . $style . "'"); ?> >
											<td><?=$c['cli_id'] ?></td>
									<?php	if($_SESSION['cli_tri']['cli_reseau']==1){ ?>
												<td><?=$c['cso_societe']?></td>
								<?php		} ?>
											<td>
												<a href="client_voir.php?client=<?=$c['cli_id']?>" style="<?=$style_a?>" >
													<?=$c['cli_code']?>
												</a>
											</td>
											<td><?=$c['cli_nom']?></td>
											<td><?=$adresse_type?></td>										
											<td><?=$adresse_lib?></td>
											<td><?= $c['ad_cp'] ?></td>
											<td><?= $c['ad_ville'] ?></td>
									<?php	if($_SESSION['acces']["acc_droits"][37]){ ?>
												<td><?= $c['prenom'] . " " . $c['nom']?></td>
												<td><?= $c['tel']?></td>
												<td><?= $c['mail']?></td>
									<?php	}
											if($_SESSION['acces']["acc_profil"]==13){ ?>
												<td><?= $c['cli_siren'] ?></td>
									<?php	} ?>		
											<td><?=$corresp_date?></td>											
										</tr>
							<?php	} 
									$_SESSION['client_tableau'] = $page; ?>
								</tbody>
							</table>
						</div>
		<?php 		}else{ ?>
						<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-warning" style="border-radius:0px;">
								Aucun client/suspect correspondant à votre recherche.
							</div>
						</div>
		<?php 		} ?>
				</section>
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="client_tri.php?cli" class="btn btn-primary btn-sm">
						<span class="fa fa-search"></span>
						<span class="hidden-xs">Nouvelle recherche</span>
					</a>					
				</div>
				<div class="col-xs-6 footer-middle">&nbsp;</div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				$('#table_id').DataTable( {
					"language": {
						"url": "vendor/plugins/DataTables/media/js/French.json"
					},
					"paging": false,
					"searching": false,
					"info": false,
					"order": [[ 1, "asc" ]]
				} );
	<?php		/*FG 
				20/05/2020 rebloqué à la demande de JP
				13/01/2021 activé à la demande de Maud (elle doit etre la seul à avoir le droit 37)
				*/
				if(!$_SESSION['acces']["acc_droits"][37]){ ?>	
					 //Disable full page
					$('body').bind('cut copy paste', function (e) {
						e.preventDefault();
					});
					//Disable mouse right click
					$("body").on("contextmenu",function(e){
						return false;
					});
	<?php		} ?>
			});
		</script>
	</body>
</html>
