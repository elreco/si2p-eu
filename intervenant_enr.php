<?php
	include "includes/controle_acces.inc.php";
	
	include_once("includes/connexion.php");
	include_once("includes/connexion_soc.php");
	
	include 'modeles/mod_parametre.php';
	include 'modeles/mod_add_utilisateur_etech.php';
	// add_utilisateur_etech fait appel à set_affichage_dossier() present dans mod_document
	include 'modeles/mod_document.php';
	include 'modeles/mod_droit.php';
	include 'modeles/mod_add_notification.php';

	// ENREGISTREMENT D'UN NOUVELLE INTERVENANT = LIGNE DE PLANNING

	// PERSONNE CONNECTE
	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];	
	}
	/*if($acc_societe!=3){
		$acc_agence=0;
	}*/

	// TRAITEMENT DES PARAMETRES

	$erreur_txt="";
	
	$intervenant=0;
	if(isset($_POST["intervenant"])){
		$intervenant=$_POST["intervenant"];
	}

	$int_type=0;
	if(isset($_POST["int_type"])){
		$int_type=$_POST["int_type"];
	}

	$periode_deb=null;
	if(isset($_POST["periode_deb"])){
		if(!empty($_POST["periode_deb"])){
			$periode_deb=$_POST["periode_deb"];
		}
	}
	
	$periode_fin=null;
	if(isset($_POST["periode_fin"])){
		if(!empty($_POST["periode_fin"])){
			$periode_fin=$_POST["periode_fin"];
		}
	}

	if(!empty($_POST["utilisateur_add"])){
		if(empty($_POST["uti_nom"])){
			$erreur_txt="Le nom de l'utilisateur doit être renseigné";
		}else{
			$int_label_1=$_POST["uti_nom"];
		}	
		if(empty($_POST["uti_prenom"])){
			$erreur_txt="Le prénom de l'utilisateur doit être renseigné";
		}else{
			$int_label_2=$_POST["uti_prenom"];
		}
	}
	
	$int_archive=0;
	if(!empty($_POST["int_archive"])){
		$int_archive=1;	
	}
	$int_ref_3=0;
	$int_option=0;
	
	if(empty($erreur_txt)){
		switch ($int_type){
			
			case "1":	
			
				// FORMATEUR INTERNE
				
				$int_ref_2=0;
				$int_place=0;
				
				if(!empty($_POST["utilisateur"])){
					
						// UTILISATEUR EXISTANT
					
					$int_ref_1=$_POST["utilisateur"];
					
					// on verifie qu'il n'est pas déjà au planning
					$sql="SELECT int_id FROM Intervenants WHERE int_type=1 AND int_ref_1=:int_ref_1 AND NOT int_id=:intervenant;";
					$req=$ConnSoc->prepare($sql);
					$req->bindParam(":int_ref_1",$int_ref_1);
					$req->bindParam(":intervenant",$intervenant);
					$req->execute();
					$doublon=$req->fetch();
					if(!empty($doublon)){
						$erreur_txt="L'utilisateur selectionné dispose déjà d'une fiche intervenant";
					}
					
					// l'uti est libre on recupere les info qui seront utilise dans la fiche intervenant
					if(empty($erreur_txt)){
					
						$sql="SELECT uti_nom,uti_prenom,uti_agence FROM Utilisateurs WHERE uti_id=:utilisateur;";
						$req=$Conn->prepare($sql);
						$req->bindParam("utilisateur",$_POST["utilisateur"]);
						$req->execute();
						$info_uti=$req->fetch();
						if(!empty($info_uti)){
							$int_agence=$info_uti["uti_agence"];
							$int_label_1=$info_uti["uti_nom"];
							$int_label_2=$info_uti["uti_prenom"];
						}
					}
					
				}elseif(!empty($_POST["utilisateur_add"])){
					
					$int_agence=0;
					if(!empty($acc_agence)){
						$int_agence=$acc_agence;
					}else{
						if(isset($_POST["uti_agence"])){
							if(!empty($_POST["uti_agence"])){
								$int_agence=intval($_POST["uti_agence"]);
							}
						}
					}
					
					// AJOUT d'UN UTILISATEUR
					
					// controle pour eviter un doublon
					$sql="SELECT uti_id FROM Utilisateurs WHERE uti_nom LIKE :nom AND uti_prenom LIKE :prenom;";
					$req=$Conn->prepare($sql);
					$req->bindParam("nom",$int_label_1);
					$req->bindParam("prenom",$int_label_2);
					$req->execute();
					$doublon=$req->fetch();
					if(!empty($doublon)){
						$erreur_txt="L'utilisateur " . $int_label_2 . " " . $int_label_1 . " existe déjà";
					};
					
					if(empty($erreur_txt)){
					
						$uti_titre=0;
						if(!empty($_POST["uti_titre"])){
							$uti_titre=$_POST["uti_titre"];	
						}
						$uti_profil=1;

						$uti_responsable=0;
						$sql="SELECT uti_id FROM Utilisateurs WHERE uti_societe=:societe AND uti_agence=:agence AND uti_profil=5;";
						$req=$Conn->prepare($sql);
						$req->bindParam("societe",$acc_societe);
						$req->bindParam("agence",$int_agence);
						$req->execute();
						$responsable=$req->fetch();
						if(!empty($responsable)){
							$uti_responsable=$responsable["uti_id"];	
						};
						
						// ON ENREGISTRE LA NOUVELLE FICHE UTILISATEUR
						
						$sql="INSERT INTO Utilisateurs (uti_titre,uti_nom,uti_prenom,uti_ad1,uti_ad2,uti_ad3,uti_cp,uti_ville,uti_tel,uti_fax,uti_mobile,uti_mail,uti_societe,uti_agence,uti_profil,uti_fonction,uti_responsable)";
						$sql.=" VALUE (:uti_titre, :uti_nom, :uti_prenom, :uti_ad1, :uti_ad2, :uti_ad3, :uti_cp, :uti_ville, :uti_tel, :uti_fax, :uti_mobile, :uti_mail, :uti_societe, :uti_agence, :uti_profil, :uti_fonction, :uti_responsable)";
						$req = $Conn->prepare($sql);
						$req ->bindParam(":uti_titre",$uti_titre);
						$req ->bindParam(":uti_nom",$int_label_1);
						$req ->bindParam(":uti_prenom",$int_label_2);
						$req ->bindParam(":uti_ad1",$_POST["uti_ad1"]);
						$req ->bindParam(":uti_ad2",$_POST["uti_ad2"]);
						$req ->bindParam(":uti_ad3",$_POST["uti_ad3"]);
						$req ->bindParam(":uti_cp",$_POST["uti_cp"]);
						$req ->bindParam(":uti_ville",$_POST["uti_ville"]);					
						$req ->bindParam(":uti_tel",$_POST["uti_tel"]);
						$req ->bindParam(":uti_fax",$_POST["uti_fax"]);
						$req ->bindParam(":uti_mobile",$_POST["uti_mobile"]);
						$req ->bindParam(":uti_mail",$_POST["uti_mail"]);
						$req ->bindParam(":uti_societe",$acc_societe);
						$req ->bindParam(":uti_agence",$int_agence);
						$req ->bindParam(":uti_profil",$uti_profil);
						$req ->bindValue(":uti_fonction","Formateur");
						$req ->bindParam(":uti_responsable",$uti_responsable);
						try{						
							$req->execute();
							$int_ref_1=$Conn->lastInsertId();
						}Catch(Exception $e){
							$erreur_txt="L'utilisateur n'a pas été crée." . $e->getMessage();
						}
						
					}
					
					if(empty($erreur_txt)){
						
						// ON NOTIFIE LES UTILISATEURS CONCERNEE
						$texte_notif=$_SESSION["acces"]["acc_prenom"] . " " . $_SESSION["acces"]["acc_nom"] . " vient d'ajouter l'utilisateur " . $int_label_1 . " " . $int_label_2 . " en tant que nouveau formateur.";
						$url_notif="utilisateur_voir.php?utilisateur=" . $int_ref_1;

						add_notifications("<i class='fa fa-user'></i>",$texte_notif,"bg-success",$url_notif,"7,12,8,11,13","21","",0,0,1);
						
						// ON AFFECT LES AGENCES
						
						$tab_acces=array();
						
						if(!empty($int_agence)){
							$tab_acces[]=array(
								"societe" => $acc_societe,
								"agence" => $int_agence
							);
						}else{
							// Si pas d'agence -> uti a accès à toutes les agences
							
							$tab_acces[]=array(
								"societe" => $acc_societe,
								"agence" => 0
							);
								
							$sql="SELECT age_id FROM Agences WHERE age_societe=:acc_societe;";
							$req = $Conn->prepare($sql);
							$req ->bindParam(":acc_societe",$acc_societe);
							$req ->execute();
							$d_agences=$req->fetchAll();
							if(!empty($d_agences)){
								foreach($d_agences as $d_age){
									$tab_acces[]=array(
										"societe" => $acc_societe,
										"agence" => $d_age["age_id"]
									);
								}
							}
						}
						
						$req = $Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur,uso_societe,uso_agence)
						VALUES (:uso_utilisateur, :uso_societe, :uso_agence)");						
						$req->bindParam(':uso_utilisateur', $int_ref_1);
						foreach($tab_acces as $acces){						
							$req->bindParam(':uso_societe', $acces["societe"]);
							$req->bindParam(':uso_agence', $acces["agence"]);
							$req->execute();
						}

						// ON AFFECT LES DROITS PAR DEFAUT
						$droit_par_defaut=get_liste_droit_profil($uti_profil);
						if(!empty($droit_par_defaut)){						
							foreach($droit_par_defaut as $d){								
								if(!empty($d["udr_droit"])){
									insert_droit_utilisateur($d["udr_droit"],$int_ref_1,$d["udr_reaffecte"]);
								}
							}
						}
						
						// ON AFFECT LES DOCUMENTS ETECH						
						add_utilisateur_etech($int_ref_1,$uti_profil, $acc_societe, $int_agence);
						
						
						
					}
				}else{
					$erreur_txt="Un formateur interne doit obligatoirement être lié à un compte utilisateur.";
				}
				
				break;
				
			case "2": 
				
				// INTERCO
				
				$int_agence=0;
				if(!empty($acc_agence)){
					$int_agence=$acc_agence;
				}else{
					if(isset($_POST["interco_agence"])){
						if(!empty($_POST["interco_agence"])){
							$int_agence=intval($_POST["interco_agence"]);
						}
					}
				}

				if(!empty($_POST["societe"])){
					$int_ref_1=$_POST["societe"];
				}else{
					$erreur_txt="Une fiche INTERCO doit-être associée à une société!";
				}
				$int_ref_2=0;
				if(!empty($_POST["agence"])){
					$int_ref_2=$_POST["agence"];
				}
				if(!empty($_POST["interco_label"])){
					$int_label_1=$_POST["interco_label"];
				}else{
					$erreur_txt="Une fiche INTERCO doit avoir un libellé!";
				}
				if(empty($erreur_txt)){
					$sql="SELECT fou_id FROM Fournisseurs WHERE fou_interco_soc=" . $int_ref_1 . " AND (fou_interco_age=" . $int_ref_2 . " OR fou_interco_age=0) AND NOT fou_archive;";
					$req = $Conn->query($sql);
					$d_fournisseur=$req->fetchAll();
					if(!empty($d_fournisseur)){
						if(count($d_fournisseur)>1){
							$erreur_txt="Plusieurs fournisseurs correspondent à l'agence selectionnée! Contactez le service informatique.";
						}else{
							$int_ref_3=$d_fournisseur[0]["fou_id"];
						}
					}else{
						$erreur_txt="Aucun fournisseur ne correspond à l'agence selectionnée! Contactez le service informatique.";
					}
				}
				
				
				
				break;
				
			case "3":
				
				// ST
				
				$int_agence=0;
				if(!empty($acc_agence)){
					$int_agence=$acc_agence;
				}else{
					if(isset($_POST["st_agence"])){
						if(!empty($_POST["st_agence"])){
							$int_agence=intval($_POST["st_agence"]);
						}
					}
				}
				
				if(!empty($_POST["fournisseur"])){
					$int_ref_1=$_POST["fournisseur"];
				}else{
					$erreur_txt="Une fiche sous-traitant doit-être associée à un fournisseur!";
				}

				$int_ref_2=0;
				if(!empty($_POST["fou_int"])){
					$int_ref_2=$_POST["fou_int"];
					// les fournisseur sont accéssible a l'ensemble du réseau -> donc a toutes les agences d'une societe
					$int_agence=0;
				}
				
				$int_label_1="";
				$int_label_2="";
				if(!empty($_POST["st_label"])){
					$int_label_1=$_POST["st_label"];
				}
				
				if(empty($erreur_txt)){
				
					// ON RECUPERE LES DONNES DU FOURNISSEUR
					
					$sql="SELECT fou_code,fou_type FROM Fournisseurs WHERE fou_id=" . $int_ref_1 . ";";
					$req = $Conn->query($sql);
					$d_fournisseur=$req->fetch();
					if(empty($d_fournisseur)){
						$erreur_txt="Une fiche sous-traitant doit-être associée à un fournisseur!";
					}
				}
				
				if(empty($erreur_txt)){
					
					if(!empty($int_ref_2)){			
						
						$sql="SELECT fin_id,fin_nom,fin_prenom FROM Fournisseurs_Intervenants WHERE fin_fournisseur=" . $int_ref_1 . " AND fin_id=" . $int_ref_2 . ";";
						$req = $Conn->query($sql);
						$d_fou_intervenant=$req->fetch();
						if(empty($d_fou_intervenant)){
							$erreur_txt="Impossible d'identifier l'intervenant extérieur.";
						}else{
							$int_label_1=$d_fournisseur["fou_code"] . "-" . $d_fou_intervenant["fin_nom"];
							$int_label_2=$d_fou_intervenant["fin_prenom"];
						}
					
						
					}elseif($d_fournisseur["fou_type"]==3){
						
						// AUTO-ENTREPRENEUR
						
						$sql="SELECT fin_id,fin_nom,fin_prenom FROM Fournisseurs_Intervenants WHERE fin_fournisseur=" . $int_ref_1 . ";";
						$req = $Conn->query($sql);
						$d_fou_intervenant=$req->fetchAll();
						if(empty($d_fou_intervenant)){
							$erreur_txt="Impossible d'identifier l'intervenant extérieur.";
						}elseif(count($d_fou_intervenant)>1){
							$erreur_txt="Erreur : un auto-entrepreneur ne peut pas disposer de plusieurs intervenants.";
						}else{	
							$int_ref_2=$d_fou_intervenant[0]["fin_id"];
							$int_label_1=$d_fournisseur["fou_code"] . "-" . $d_fou_intervenant[0]["fin_nom"];
							$int_label_2=$d_fou_intervenant[0]["fin_prenom"];
						}
						
						
					}else{
						
						if(empty($int_label_1)){
							$int_label_1=$d_fournisseur["fou_code"];
						}
					}
					
					// SI LE ST EST LIE A UNE PERSONNE PHYSIQUE -> une seule inscription comme pour les utilisateurs	
					
					if(!empty($int_ref_2)){
						
						$sql="SELECT * FROM Intervenants WHERE int_type=3 AND int_ref_2=:int_ref_2 AND NOT int_id=:intervenant;";
						$req = $ConnSoc->prepare($sql);
						$req ->bindParam(":int_ref_2",$int_ref_2);
						$req ->bindParam(":intervenant",$intervenant);
						$d_intervenant=$req->fetchAll();
						if(!empty($d_intervenant)){
							if(count($d_intervenant)>1){
								$erreur_txt="Les intervenants extérieurs ne peuvent pas être liés à plusieurs lignes de planning";
							}else{	
								
								// il n'y a qu'une seule fiche intervenant on la recupère 
								$intervenant=$d_intervenant["int_id"];
								$int_label_1=$d_intervenant["int_label_1"];
								$int_label_2=$d_intervenant["int_label_2"];
								if(!empty($d_intervenant["int_agence"]) AND $d_intervenant["int_agence"]!=$int_agence){
									// l'intervenant est associé à une autre agence -> il mpasse multi agence
									$int_agence=0;
								}
							}
						}
						
						
					}
				}
				
				break;
				
				
			case "0":
				
				// AUTRES	

				$int_ref_1=0;
				$int_ref_2=0;
				if(!empty($_POST["autre_label"])){
					$int_label_1=$_POST["autre_label"];
				}
				$int_label_2="";
				
				$int_agence=0;
				if(!empty($acc_agence)){
					$int_agence=$acc_agence;
				}else{
					if(isset($_POST["autre_agence"])){
						if(!empty($_POST["autre_agence"])){
							$int_agence=intval($_POST["autre_agence"]);
						}
					}
				}

				if(!empty($_POST["autre_annul"])){
					$int_option=$_POST["autre_annul"];
				}
				
				break;
		}
		
		// FIN GESTION Type
		
		
		if(empty($erreur_txt)){
			
			if($intervenant==0){
				
				// ON AJOUTE L'INTERVENANT
				
				$sql="INSERT INTO Intervenants (int_type,int_ref_1,int_ref_2,int_ref_3,int_agence,int_label_1,int_label_2,int_option)";
				$sql.=" VALUE (:int_type, :int_ref_1, :int_ref_2, :int_ref_3, :int_agence, :int_label_1, :int_label_2,:int_option)";
				$req = $ConnSoc->prepare($sql);
				$req ->bindParam(":int_type",$int_type);
				$req ->bindParam(":int_ref_1",$int_ref_1);
				$req ->bindParam(":int_ref_2",$int_ref_2);
				$req ->bindParam(":int_ref_3",$int_ref_3); 
				$req ->bindParam(":int_agence",$int_agence);
				$req ->bindParam(":int_label_1",$int_label_1);
				$req ->bindParam(":int_label_2",$int_label_2);
				$req ->bindParam(":int_option",$int_option);
				$req->execute();
				$int_id=$ConnSoc->lastInsertId();

				// ON INSCRIT L'INTERVENANT AU PLANNING

				if(!empty($periode_deb) AND !empty($periode_fin)){
					
					// si la societe gère des agences -> l'inscription est forecement attaché à une agence
					// on ne peut pas utiliser $d_agences -> car calculer uniquement si ajout d'un utilisateur via la creation d'une fiche intervenant
					if(empty($int_agence)){
						$sql="SELECT age_id FROM Agences WHERE age_societe=:acc_societe;";
						$req = $Conn->prepare($sql);
						$req ->bindParam(":acc_societe",$acc_societe);
						$req ->execute();
						$d_agences=$req->fetchAll();
						if(empty($d_agences) OR $acc_societe!=3){
							$d_agences=array(
								"0" => array(
									"age_id" => 0
								)
							);
						}
					}else{
						$d_agences=array(
							"0" => array(
								"age_id" => $int_agence
							)
						);
					}


					$sql_place="SELECT MAX(pin_place) FROM Plannings_Intervenants,Intervenants WHERE int_id=pin_intervenant";
					$sql_place.=" AND int_type=:int_type AND pin_semaine=:semaine AND pin_annee=:annee AND pin_agence=:agence;";		
					$req_place = $ConnSoc->prepare($sql_place);
					$req_place ->bindParam(":int_type",$int_type);
					
					$sql="INSERT INTO Plannings_Intervenants (pin_intervenant,pin_semaine,pin_annee,pin_agence,pin_place)";
					$sql.=" VALUE (:pin_intervenant, :pin_semaine, :pin_annee, :pin_agence, :pin_place)";
					$req = $ConnSoc->prepare($sql);
					$req ->bindParam(":pin_intervenant",$int_id);
					
					
					$date_deb = new DateTime(convert_date_sql($periode_deb));
					$date_fin = new DateTime(convert_date_sql($periode_fin));
					
					WHILE($date_deb<$date_fin){

						$semaine=$date_deb->format("W");
						$annee=$date_deb->format("Y");		

						foreach($d_agences as $d_age_plan){
							
							$place=0;
							$req_place ->bindParam(":semaine",$semaine);
							$req_place ->bindParam(":annee",$annee);
							$req_place ->bindParam(":agence",$d_age_plan["age_id"]);						
							$req_place->execute();
							$p=$req_place->fetch();
							if(!empty($p)){
								$place=$p[0];
							}
							$place++;
							
							
							$req ->bindParam(":pin_semaine",$semaine);
							$req ->bindParam(":pin_annee",$annee);
							$req ->bindParam(":pin_agence",$d_age_plan["age_id"]);
							$req ->bindParam(":pin_place",$place);
							$req->execute();
						}
						$date_deb->add(new DateInterval('P7D'));	
					}
				}
			}else{
				
				// ON MET A JOUR L'INTERVENANT

				$sql="UPDATE Intervenants SET 
				int_type=:int_type,
				int_ref_1=:int_ref_1,
				int_ref_2=:int_ref_2,
				int_ref_3=:int_ref_3,
				int_agence=:int_agence,
				int_label_1=:int_label_1,
				int_label_2=:int_label_2,
				int_archive=:int_archive,
				int_option=:int_option
				WHERE int_id=:intervenant;";
				
				$req = $ConnSoc->prepare($sql);
				$req ->bindParam(":int_type",$int_type);
				$req ->bindParam(":int_ref_1",$int_ref_1);
				$req ->bindParam(":int_ref_2",$int_ref_2);
				$req ->bindParam(":int_ref_3",$int_ref_3);
				$req ->bindParam(":int_agence",$int_agence);
				$req ->bindParam(":int_label_1",$int_label_1);
				$req ->bindParam(":int_label_2",$int_label_2);
				$req ->bindParam(":int_archive",$int_archive);
				$req ->bindParam(":int_option",$int_option);
				$req ->bindParam(":intervenant",$intervenant);
				$req->execute();

			}
		
		}
	}
	
	if(!empty($erreur_txt)){
		$_SESSION['message'] = array(
			"aff" => "modal",
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);

	}else{
		$_SESSION['message'][] = array(
			"titre" => "Enregistrement terminé",
			"type" => "success",
			"message" => "Vos modifications ont bien été enregistrées."
		);
	}

	if(!empty($_SESSION["retour"])){
		header("Location: " . $_SESSION["retour"]);	
	}else{
		header('Location: accueil.php');
	}
	
?>