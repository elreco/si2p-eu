 <?php
 
	include "includes/controle_acces.inc.php";
	
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");
	
	// VISUALISATION D'UN DOSSIER CACES
	
	$erreur_txt="";
	
	$dossier_id=0;
	if(isset($_GET["dossier"])){
		if(!empty($_GET["dossier"])){
			$dossier_id=intval($_GET["dossier"]);
		}	
	}
	
	$passage=0;
	if(isset($_GET["passage"])){
		if(!empty($_GET["passage"])){
			$passage=intval($_GET["passage"]);
		}	
	}
	
	$action_id=0;
	if(isset($_GET["action"])){
		if(!empty($_GET["action"])){
			$action_id=intval($_GET["action"]);
		}	
	}

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}
	if($acc_societe!=$conn_soc_id){
		$acc_societe=$conn_soc_id;
	}

	if($dossier_id>0 AND ( !empty($passage) OR (!empty($action_id) AND !empty($acc_societe)) ) ){
		
		// LE DIPLOME
		
		$sql="SELECT * FROM Diplomes_Caces WHERE dca_id=:dca_id;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":dca_id",$dossier_id);
		$req->execute();
		$d_dossier=$req->fetch();
		if(!empty($d_dossier)){

			if(empty($passage)){
				for($bcl=1;$bcl<=6;$bcl++){
					if($d_dossier["dca_action_" . $bcl]==$action_id AND $d_dossier["dca_action_soc_" . $bcl]==$acc_societe){
						$passage=$bcl;
						break;
					}
				}
			}

			$dca_sta_naissance="";
			if(!empty($d_dossier["dca_sta_naissance"])){
				$DT=date_create_from_format('Y-m-d',$d_dossier["dca_sta_naissance"]);
				if(!is_bool($DT)){
					$dca_sta_naissance=$DT->format("d/m/Y");
				}
			}
			
			$dca_date="";
			if(!empty($d_dossier["dca_date"])){
				$DT=date_create_from_format('Y-m-d',$d_dossier["dca_date"]);
				if(!is_bool($DT)){
					$dca_date=$DT->format("d/m/Y");
				}
			}
			$dca_date_validite="";
			if(!empty($d_dossier["dca_date_validite"])){
				$DT=date_create_from_format('Y-m-d',$d_dossier["dca_date_validite"]);
				if(!is_bool($DT)){
					$dca_date_validite=$DT->format("d/m/Y");
				}
			}
			$dca_date_th="";
			if(!empty($d_dossier["dca_date_th"])){
				$DT=date_create_from_format('Y-m-d',$d_dossier["dca_date_th"]);
				if(!is_bool($DT)){
					$dca_date_th=$DT->format("d/m/Y");
				}
			}
			$dca_date_pra="";
			if(!empty($d_dossier["dca_date_pra"])){
				$DT=date_create_from_format('Y-m-d',$d_dossier["dca_date_pra"]);
				if(!is_bool($DT)){
					$dca_date_pra=$DT->format("d/m/Y");
				}
			}
			$dca_aipr_date="";
			if(!empty($d_dossier["dca_aipr_date"])){
				$DT=date_create_from_format('Y-m-d',$d_dossier["dca_aipr_date"]);
				if(!is_bool($DT)){
					$dca_aipr_date=$DT->format("d/m/Y");
				}
			}
	
		}else{
			echo("Impossible d'afficher la page!");
			die();
		}

		// RECAP DES CATEGORIES
		
		$d_categories=array(
			"validees" => array()
		);

		$sql="SELECT dcc_passage,dcc_date,DATE_FORMAT(dcc_date,'%d/%m/%Y') AS dcc_date_fr,dcc_testeur,dcc_valide,dcc_opt_1,dcc_opt_1_valide,dcc_opt_2,dcc_opt_2_valide,dcc_opt_3,dcc_opt_3_valide,dcc_numero,dcc_testeur_identite
		,qca_libelle
		,qua_opt_1,qua_opt_2,qua_opt_3
		FROM Diplomes_Caces_Cat 
		LEFT JOIN Qualifications_Categories ON (Diplomes_Caces_Cat.dcc_categorie=Qualifications_Categories.qca_id)
		LEFT JOIN Qualifications ON (Qualifications_Categories.qca_qualification=Qualifications.qua_id)
		WHERE dcc_diplome=:dca_id ORDER BY qca_libelle;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":dca_id",$dossier_id);
		$req->execute();
		$d_dossier_cat=$req->fetchAll();
		if(!empty($d_dossier_cat)){
			foreach($d_dossier_cat as $cat){
				
				if($cat["dcc_valide"]==1){
					$d_categories["validees"][]=$cat;	
				}
				if(!isset($d_categories[$cat["dcc_passage"]])){
					$d_categories[$cat["dcc_passage"]]=array();	
				}
				$d_categories[$cat["dcc_passage"]][]=$cat;
			}
			
		}
		/*echo("<pre>");
			print_r($d_categories);
		echo("</pre>");
		die();*/
		
		$_SESSION["retourCaces"]="dip_caces_voir.php?dossier=" . $dossier_id . "&passage=" . $passage . "&societ=" . $conn_soc_id;
		$_SESSION["retourAttest"]="dip_caces_voir.php?dossier=" . $dossier_id . "&passage=" . $passage . "&societ=" . $conn_soc_id;;
		
	}else{
		echo("Impossible d'afficher la page!");
		die();
	} ?>
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<title>SI2P - Orion - Recherche client</title>
			<meta name="keywords" content=""/>
			<meta name="description" content="">
			<meta name="author" content="">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<!-- CSS THEME -->
			<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
			
			
			<!-- CSS Si2P -->
			<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
			
			<!-- Favicon -->
			<link rel="shortcut icon" href="assets/img/favicon.png">
		   
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
			<![endif]-->
			
			<style type="text/css" >
				.photo{
					margin-left:15px;
					width:3.5cm;
					height:4.5cm;
					border:1px solid black;
					text-align:center;
				}
				.photo>img{
					max-width:100%;
					max-height:100%;
				}
			</style>
		</head>
		<body class="sb-top sb-top-sm ">			
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated" >
			<?php		echo("<h2>");
							echo("Dossier N° " . $dossier_id . " : ");
							echo("<b class='text-primary'>");
								if($d_dossier["dca_diplome"]==2){
									if($d_dossier["dca_qualification"]==16){
										echo("Certificat CAUSPR");
									}else{
										echo("Certificat CACES®");
									}
								}else{
									echo("Attestation de compétence");
								}
							echo("</b>");
						echo("</h2>"); ?>
						
						<div class="panel" >
							<div class="panel-heading panel-head-sm">Synthèse</div>
							<div class="panel-body">
								<div class="row" >
									<div class="row" >
										<div class="col-md-2" >
											<div class="photo" >
									<?php		if(file_exists("documents/Stagiaires/sta_phid_". $d_dossier["dca_stagiaire"] . ".jpg")){ ?>
													<img src="documents/Stagiaires/sta_phid_<?=$d_dossier["dca_stagiaire"]?>.jpg" alt="" id="sta_photo_preview" />
								<?php			} ?>
											</div>
										</div>
										<div class="col-md-10" >
											<div class="row" >
												<div class="col-md-2 text-right" >Nom :</div>
												<div class="col-md-2" ><b><?=$d_dossier["dca_sta_nom"]?></b></div>
												<div class="col-md-2 text-right" >Prénom :</div>
												<div class="col-md-2" ><b><?=$d_dossier["dca_sta_prenom"]?></b></div>
												<div class="col-md-2 text-right" >Date de naissance :</div>
												<div class="col-md-2" ><b><?=$dca_sta_naissance?></b></div>
											</div>
										</div>
									</div>
								<div class="row mt15">
									<div class="col-md-2 text-right">
										Type de diplôme : 
									</div>
									<div class="col-md-2">
								<?php	if($d_dossier["dca_diplome"]==2){
											if($d_dossier["dca_qualification"]==16){
												echo("<b>Certificat CAUSPR</b>");
											}else{
												echo("<b>Certificat CACES®</b>");
											}
										}else{
											echo("<b>Attestation de compétence</b>");
										} ?>
									</div>
									<div class="col-md-2 text-right">
										Date d'obtention :
									</div>
									<div class="col-md-2"><b><?=$dca_date?></b></div>
									<div class="col-md-2 text-right">
										Fin de validité : 
									</div>
									<div class="col-md-2">
										<b><?=$dca_date_validite?></b>
									</div>
								</div>					
								<div class="row mt15">
							<?php	if(!empty($dca_date_th)){ ?>
										<div class="col-md-2 text-right p5">Théorie :</div>
										<div class="col-md-2 alert alert-success p5"><b>validée le <?=$dca_date_th?></b></div>
							<?php	}elseif($d_dossier["dca_th_" . $passage]==2){ ?>
										<div class="col-md-2 text-right p5">Théorie :</div>
										<div class="col-md-2 alert alert-danger p5"><b>échec</b></div>
							<?php	}
									if(!empty($dca_date_pra)){ ?>			
										<div class="col-md-2 text-right p5">Pratique :</div>
										<div class="col-md-2 alert alert-success p5"><b>validée le <?=$dca_date_pra?></b></div>	
							<?php	} 
									//	 AIPR
									if(!empty($d_dossier["dca_aipr"])){ ?>			
										<div class="col-md-2 text-right p5">AIPR :</div>
										<div class="col-md-2 alert alert-success p5"><b>validée le <?=$dca_aipr_date?></b></div>
							<?php	} ?>
								</div>
								<h3>Catégories validées</h3>
					<?php		if(!empty($d_categories["validees"])){ ?>
									<table class="table" >
										<tr>
											<th>Catégorie</th>
											<th>Numéro</th>
											<th>Testeur</th>
											<th>Obtention</th>
											<th>&Eacute;chéance</th>
										</tr>										
					<?php				foreach($d_categories["validees"] as $valide){ ?>
											<tr>
												<td><?=$valide["qca_libelle"]?></td>
												<td><?=$valide["dcc_numero"]?></td>
												<td><?=$valide["dcc_testeur_identite"]?></td>										
												<td>
												
										<?php		if($d_dossier["dca_date"]>$valide["dcc_date"]){
														echo($dca_date);
													}else{
														echo($valide["dcc_date_fr"]);
													} ?>
												</td>
												<td><?=$dca_date_validite?></td>
											</tr>
					<?php				} ?>
									</table>
					<?php		}else{ ?>
									<p class="alert alert-danger" >
										Pas de catégorie pratique validée.
									</p>					
					<?php		}  ?>
							</div>
						</div>
			<?php		if($d_dossier["dca_clos_" . $passage]){ ?>
							<div class="row mt15">
								<div class="col-md-3">&nbsp;</div>
			<?php				if($d_dossier["dca_diplome"]==2){ ?>								
									<div class="col-md-2 text-center">
										<a href="dip_caces_print.php?dossier=<?=$d_dossier["dca_id"]?>" class="btn btn-info btn-xl" <?php if(empty($d_dossier["dca_date"])) echo("disabled") ?> >
											<h2>
												<i class="fa fa-print" aria-hidden="true"  data-toggle="tooltip" data-placement="bottom" title="Imprimer"></i><br/>
			<?php								if($d_dossier["dca_qualification"]==16){
													echo("Certificat CAUSPR");
												}else{
													echo("Certificat CACES®");
												} ?>
											</h2>
										</a>
									</div>
			<?php				} ?>
								<div class="col-md-2 text-center">
									<a href="attestation_ind_voir.php?action=<?=$d_dossier["dca_action_" . $passage]?>&stagiaire=<?=$d_dossier["dca_stagiaire"]?>&societ=<?=$conn_soc_id?>" class="btn btn-info btn-xl" >
										<h2>
											<i class="fa fa-print" aria-hidden="true"  data-toggle="tooltip" data-placement="bottom" title="Imprimer"></i><br/>
											Attestation
										</h2>
									</a>
								</div>
							</div>
			<?php		} ?>
							
						
						
						<h2>Détails par passage</h2>
				<?php	for($bcl_pass=1;$bcl_pass<=6;$bcl_pass++){
							if(!empty($d_dossier["dca_action_" . $bcl_pass])){ 
							
								$date_passage="";
								if(!empty($d_dossier["dca_date_" . $bcl_pass])){
									$DT_date_passage=date_create_from_format('Y-m-d',$d_dossier["dca_date_" . $bcl_pass]);
									if(!is_bool($DT_date_passage)){
										$date_passage=$DT_date_passage->format("d/m/Y");
									}
								}	?>
								<div class="panel" >
									<div class="panel-heading panel-head-sm">
										<span class="panel-title">Passage N° <?=$bcl_pass?> du <?=$date_passage?></span>
										<span class="pull-right" >
											<button class="btn btn-default btn-sm btn-aff-mas" data-cible="aff_passage_<?=$bcl_pass?>" >
										<?php	if($bcl_pass!=$passage){
													echo("<i class='fa fa-plus' ></i>");
												}else{
													echo("<i class='fa fa-minus' ></i>");
												} ?>
											</button>
										</span>
									</div>
									<div class="panel-body" id="aff_passage_<?=$bcl_pass?>" <?php if($bcl_pass!=$passage) echo("style='display:none;'"); ?> >
								<?php	if(!empty($d_dossier["dca_th_" . $bcl_pass])){ ?>
											<div class="row" >
												<div class="col-md-6 text-right p5" >Théorie :</div>
										<?php	if(!empty($d_dossier["dca_th_" . $bcl_pass]==1)){ ?>
													<div class="col-md-6 alert alert-success p5" >
														Validée
													</div>
										<?php	}else{ ?>
													<div class="col-md-6 alert alert-danger p5" >
														&Eacute;chec
													</div>
										<?php	} ?>
											</div>
								<?php	}
										if(isset($d_categories[$bcl_pass])){
											
											if(!empty($d_categories[$bcl_pass])){ ?>
												<table class="table" >
													<tr>
														<th>Catégorie</th>
														<th>Numéro</th>
														<th>Testeur</th>
														<th>Obtention</th>
														<th>&Eacute;chéance</th>
													</tr>										
								<?php				foreach($d_categories[$bcl_pass] as $cat_passage){ ?>
														<tr>
															<td><?=$cat_passage["qca_libelle"]?></td>
															<td><?=$cat_passage["dcc_numero"]?></td>
															<td><?=$cat_passage["dcc_testeur_identite"]?></td>
													<?php	if($cat_passage["dcc_valide"]){ ?>
																<td>
													<?php			echo($cat_passage["dcc_date_fr"]); ?>
																</td>
																<td><?=$dca_date_validite?></td>
													<?php	}else{ ?>
																<td class="text-danger" >Échec</td>
																<td>&nbsp;</td>
													<?php	} ?>															
														</tr>
								<?php				} ?>
												</table>
								<?php		}
										} else{ ?>
											<p class="alert alert-danger" >
												Pas de partie pratique
											</p>					
							<?php		} ?>							
									</div>
								</div>
				<?php		}
						} ?>
						
						
						
					</section>						
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
				<?php	if(isset($_SESSION["retourQualif"])){ ?>
							<a href="<?=$_SESSION["retourQualif"]?>" class="btn btn-default btn-sm" >
								Retour
							</a>
				<?php	}else{ ?>
							<a href="action_voir_sta.php?action=<?=$d_dossier["dca_action_" . $passage]?>" class="btn btn-default btn-sm" >
								Retour
							</a>
				<?php	} ?>
					</div>
					<div class="col-xs-6 footer-middle text-center">
			<?php		if(!$d_dossier["dca_clos_" . $passage]){ ?>
							<button type="button" class="btn btn-success btn-sm" id="valide_dossier" >
								<i class="fa fa-check" ></i> Clôturer ce dossier
							</button>
			<?php		} ?>
					</div>
					<div class="col-xs-3 footer-right">
				<?php	if(!$d_dossier["dca_clos_" . $passage]){ ?>							
							<a href="dip_caces.php?action=<?=$d_dossier["dca_action_" . $passage]?>&stagiaire=<?=$d_dossier["dca_stagiaire"]?>&qualification=<?=$d_dossier["dca_qualification"]?>&societ=<?=$conn_soc_id?>" class="btn btn-sm btn-warning" >
								<i class="fa fa-pencil" ></i> Modifier
							</a>
				<?php	} ?>		
					
					</div>
				</div>
			</footer>
			
			<div id="modal_confirmation_user" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title">Confirmer ?</h4>
						</div>
						<div class="modal-body"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								Annuler
							</button>
							<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
								Confirmer
							</button>
						</div>
					</div>
				</div>
			</div>

			
			
	<?php	include "includes/footer_script.inc.php"; ?>
			<script type="text/javascript" >

				jQuery(document).ready(function(){
					
					$("#valide_dossier").click(function(){
						modal_confirmation_user("Vous êtes sur le point de valider ce dossier. Une fois validé, vous ne pourrez plus le modifier.",valider_dossier,"","","")
					});
					
					
				});
				
				function valider_dossier(){
					document.location="dip_caces_valide.php?dossier=<?=$d_dossier["dca_id"]?>&societ=<?=$conn_soc_id?>";
				}

			</script>
		</body>
	</html>