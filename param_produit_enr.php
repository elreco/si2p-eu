<?php

	include "includes/controle_acces.inc.php";
	include_once 'includes/connexion.php';

	// ENREGISTREMENT DES DONNEES COMPTABLES

	$erreur_txt="";
	
	if($_SESSION['acces']['acc_service'][2]!=1){
		$erreur_txt="Impossible d'enregistrer les modifications!";
	}
	
	if(empty($erreur_txt)){
		
	
		$pro_categorie=0;
		$pro_famille=0;
		$pro_sous_famille=0;
		$pro_sous_sous_famille=0;
		
		if(!empty($_POST)){

		
			if(!empty($_POST['pro_categorie'])){
				$pro_categorie=intval($_POST['pro_categorie']);
			}

			
			if(!empty($_POST['pro_famille'])){
				$pro_famille=intval($_POST['pro_famille']);
			}

			
			if(!empty($_POST['pro_sous_famille'])){
				$pro_sous_famille=intval($_POST['pro_sous_famille']);
			}
			
			
			if(!empty($_POST['pro_sous_sous_famille'])){
				$pro_sous_sous_famille=intval($_POST['pro_sous_sous_famille']);
			}
		
		}
		
		// prep requete update
		
		$sql_up="UPDATE produits SET pro_deductible=:pro_deductible, pro_ven_cpt=:pro_ven_cpt, pro_ven_cpt_si2p=:pro_ven_cpt_si2p
		, pro_ven_cpt_exo=:pro_ven_cpt_exo, pro_ven_cpt_si2p_exo=:pro_ven_cpt_si2p_exo 
		WHERE pro_id=:pro_id";
		$req_up=$Conn->prepare($sql_up);
		
		
		$sql="SELECT pro_id FROM produits";
		$mil="";
		if(!empty($pro_categorie)){
			$mil.=" AND pro_categorie=" . $pro_categorie;
		}
		if(!empty($pro_famille)){
			$mil.=" AND pro_famille=" . $pro_famille;
		}
		if(!empty($pro_sous_famille)){
			$mil.=" AND pro_sous_famille=" . $pro_sous_famille;
		}
		if(!empty($pro_sous_sous_famille)){
			$mil.=" AND pro_sous_sous_famille=" . $pro_sous_sous_famille;
		}
		if($mil!=""){
			$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		}
		$sql.=" ORDER BY pro_code_produit";
		$req = $Conn->query($sql);
		$d_produits = $req->fetchAll();
		if(!empty($d_produits)){
			foreach($d_produits as $produit){
				
				if(!empty($_POST["produit_" . $produit["pro_id"]])){
					
					$pro_deductible=0;
					if(!empty($_POST["pro_deductible_" . $produit["pro_id"]])){
						$pro_deductible=1;
					}
					
					$pro_ven_cpt=0;
					if(!empty($_POST["pro_ven_cpt_" . $produit["pro_id"]])){
						$pro_ven_cpt=intval($_POST["pro_ven_cpt_" . $produit["pro_id"]]);
					}
					
					$pro_ven_cpt_si2p=0;
					if(!empty($_POST["pro_ven_cpt_si2p_" . $produit["pro_id"]])){
						$pro_ven_cpt_si2p=intval($_POST["pro_ven_cpt_si2p_" . $produit["pro_id"]]);
					}
					
					$pro_ven_cpt_exo=0;
					if(!empty($_POST["pro_ven_cpt_exo_" . $produit["pro_id"]])){
						$pro_ven_cpt_exo=intval($_POST["pro_ven_cpt_exo_" . $produit["pro_id"]]);
					}
					
					$pro_ven_cpt_si2p_exo=0;
					if(!empty($_POST["pro_ven_cpt_si2p_exo_" . $produit["pro_id"]])){
						$pro_ven_cpt_si2p_exo=intval($_POST["pro_ven_cpt_si2p_exo_" . $produit["pro_id"]]);
					}
					
					// maj du produit
					$req_up->bindParam(":pro_id",$produit["pro_id"]);
					$req_up->bindParam(":pro_deductible",$pro_deductible);
					$req_up->bindParam(":pro_ven_cpt",$pro_ven_cpt);
					$req_up->bindParam(":pro_ven_cpt_si2p",$pro_ven_cpt_si2p);
					$req_up->bindParam(":pro_ven_cpt_exo",$pro_ven_cpt_exo);
					$req_up->bindParam(":pro_ven_cpt_si2p_exo",$pro_ven_cpt_si2p_exo);
					$req_up->execute();
				}
				
			}
		}else{
			$erreur_txt="Impossible de charger la base produit";
		}
	}
	
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);

	}else{
		$_SESSION['message'][] = array(
			"titre" => "Enregistrement terminé",
			"type" => "success",
			"message" => "Les modifications ont bien été prise en coompte" 
		);

	}
	header("location : param_produit_liste.php");
	
?>