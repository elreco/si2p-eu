<?php
$menu_actif = 1;
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");

include 'modeles/mod_parametre.php';


	// SI LA PERSONNE N'A PAS LE DROIT COMMERCIAL :
	if($_SESSION["acces"]["acc_droits"][14]!=1){
		Header("Location: index.php");
		die();
	}
	
	// PARAMETRE
	$commercial=0;
	if(!empty($_GET['commercial'])){
		$commercial=intval($_GET['commercial']);
	}
	
	// PERSONNE CONNECTE
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];	
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref_id"])){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];	 
	}
	
	
	// AGENCE DISPO
	
	if($acc_agence==0){
		
		$sql="SELECT age_id,age_nom FROM Agences LEFT JOIN Societes ON (Agences.age_societe=Societes.soc_id)
		WHERE soc_id=:societe AND soc_agence ORDER BY age_nom;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":societe",$acc_societe);
		$req->execute();
		$d_agences=$req->fetchAll();
		
	}

	// C'EST UNE EDITION
	
	if(!empty($commercial)){
		$sql="SELECT * FROM Commerciaux WHERE com_id=" . $commercial;
		$req=$ConnSoc->prepare($sql);
		$req->execute();
		$d_commercial=$req->fetch();
		
	}else{
		$d_commercial=array(
			"com_id" => 0,
			"com_type" => 1,
			"com_ref_1" => 0,
			"com_label_1" => "",
			"com_label_2" => "",
			"com_ad1" => "",
			"com_ad2" => "",
			"com_ad3" => "",
			"com_cp" => "",
			"com_ville" => "",
			"com_tel" => "",
			"com_portable" => "",
			"com_fax" => "",
			"com_mail" => ""
		);
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<form method="post" action="commercial_enr.php" id="form_com" >
<?php 		if(!empty($d_commercial)){ ?>
				<input type="hidden" name="com_id" value="<?= $d_commercial['com_id'] ?>" />
<?php 		} ?>
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">		
						<div class="panel" > 
							<div class="panel-heading panel-head-sm" >
								<span class="panel-title">Type</span>
							</div>
							<div class="panel-body">
								<div class="admin-form" >
									<div class="row mb15" >
										<div class="col-md-3 text-center" >
											<label class="option">
												<input type="radio" name="com_type" value="1" 
												<?php if(empty($d_commercial) OR $d_commercial['com_type'] == 1){ ?>
												checked 
												<?php } ?>
												>
												<span class="radio"></span>Vendeur
											</label>
										</div>
										<div class="col-md-3 text-center" >
											<label class="option">
												<input type="radio" name="com_type" value="2" 
												<?php if(!empty($d_commercial) && $d_commercial['com_type'] == 2){ ?>
													checked 
												<?php } ?>
												>
												<span class="radio"></span>Revendeur
											</label>
										</div>
										<div class="col-md-3 text-center" >
											<label class="option">
												<input type="radio" name="com_type" value="3"  
												<?php if(!empty($d_commercial) && $d_commercial['com_type'] == 3){ ?>
													checked 
												<?php } ?>
												>
												<span class="radio"></span>Indivis
											</label>
										</div>
										<div class="col-md-3 text-center">
											<label class="option">
												<input type="radio" name="com_type" value="0"  
												<?php if(!empty($d_commercial) && $d_commercial['com_type'] == 0){ ?>
													checked 
												<?php } ?>
												>
												<span class="radio"></span>Autre
											</label>
										</div>																
									</div>
								</div>
							</div>
						</div>
						
						<!--FORMATEUR INTERNE -->	
						<div class="panel" id="vendeur" > 
							<div class="panel-heading" >
								<span class="panel-title">Vendeur</span>
							</div>
							<div class="panel-body">
								<div class="admin-form" >
									<div class="row" >
							<?php		if(isset($d_agences)){
											if(!empty($d_agences)){ ?>										
												<div class="col-md-4" >
													<div class="section">
														<label>Sélectionner une agence</label>
														<label class="field select">
															<select name="com_agence" id="com_agence" >
													<?php		foreach($d_agences as $a){
																	if(!empty($d_commercial['com_agence']) && $d_commercial['com_agence'] == $a['age_id']){
																		echo("<option selected value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																	}else{
																		echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																	}
																};?>																																	
															</select>
															<i class="arrow simple"></i>
														</label>
														<small class="text-danger texte_required" id="com_agence_required">
															Merci de selectionner une agence
														</small>
													</div>
												</div>									
								<?php		}
										} ?>
										<div class="col-md-5" >

											<label>Sélectionner un utilisateur</label>
											<select id="utilisateur" name="utilisateur" placeholder="Compte utilisateur existants" class="select2" >
												<option value="0" >&nbsp;</option>
								<?php			if(!empty($d_commercial) AND $d_commercial['com_ref_1']>0){ ?>
													<option value="<?=$d_commercial['com_ref_1']?>" selected="selected"  ><?=$d_commercial['com_label_1'] . " " . $d_commercial['com_label_2']?></option>
								<?php			} ?>
											</select>
											<small class="text-danger texte_required" id="utilisateur_required">
												Chaque fiche "Vendeur" doit-être associé à un compte utilisateur!
											</small>
										</div>
										
									</div>
									
								</div>
								
							</div>
						</div>
						<!--FIN DE FORMATEUR INTERNE -->

						<!--REVENDEUR -->	
						<div class="panel" id="revendeur" style="display:none;" > 
							<div class="panel-heading" >
								<span class="panel-title">Revendeur</span>
							</div>
							<div class="admin-form theme-primary ">
								<div class="panel-body bg-light">
									<div class="row mb15" >
							<?php		if(isset($d_agences)){
											if(!empty($d_agences)){ ?>										
												<div class="col-md-4" >
													<div class="section">
														<label>Sélectionner une agence</label>
														<label class="field select">
															<select name="revendeur_agence" id="revendeur_agence" >
													<?php		foreach($d_agences as $a){
																	if(!empty($d_commercial['com_agence']) && $d_commercial['com_agence'] == $a['age_id']){
																		echo("<option selected value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																	}else{
																		echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																	}
																};?>																																	
															</select>
															<i class="arrow simple"></i>
														</label>
													</div>
												</div>									
							<?php			}
										} ?>									
										<div class="col-md-4">
											<label>Nom du commercial</label>
											<input type="text" name="revendeur_label_1" class="gui-input nom" id="revendeur_label_1" placeholder="Nom" value="<?= $d_commercial['com_label_1'] ?>" />	
											<small class="text-danger texte_required" id="revendeur_label_1_required">
												Merci de renseigner un nom pour cette fiche "revendeur"
											</small>

										</div>	
										<div class="col-md-4">
											<label>Prénom du commercial</label>
											<input type="text" name="revendeur_label_2" class="gui-input prenom" id="revendeur_label_2" placeholder="Prénom" value="<?= $d_commercial['com_label_2'] ?>" />																			
										</div>	
									</div>		
									
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span style="background: white;">Adresse</span>
													</div>
												</div>
											</div>
											<div class="row mb15">
												<div class="col-md-12 mb15">
													<label>Adresse du commercial</label>
													<input type="text" name="revendeur_ad1" class="gui-input" placeholder="Adresse" value="<?= $d_commercial['com_ad1'] ?>" />											
												</div>
												<div class="col-md-12 mb15">
													<label>Complément 1</label>
													<input type="text" name="revendeur_ad2" class="gui-input" placeholder="Adresse (complément 1)" value="<?= $d_commercial['com_ad2'] ?>" />											
												</div>
												<div class="col-md-12">
													<label>Complément 2</label>
													<input type="text" name="revendeur_ad3" class="gui-input" placeholder="Adresse (complément 2)" value="<?= $d_commercial['com_ad3'] ?>" />																							
												</div>
											</div>	
											<div class="row">
												<div class="col-md-4">
													<label>Code postal</label>
													<input type="text" name="revendeur_cp" class="gui-input code-postal" placeholder="Code postal" value="<?= $d_commercial['com_cp'] ?>" />											
												</div>
												<div class="col-md-8">
													<label>Ville</label>
													<input type="text" name="revendeur_ville" class="gui-input nom" placeholder="Ville" value="<?= $d_commercial['com_ville'] ?>" />											
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span style="background: white;">Contact</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 mb15">
													<label>Téléphone</label>
													<input type="text" name="revendeur_tel" class="gui-input telephone" placeholder="Téléphone" value="<?= $d_commercial['com_tel'] ?>" />											
												</div>
												<div class="col-md-12 mb15">
													<label>Portable</label>
													<input type="text" name="revendeur_portable" class="gui-input telephone" placeholder="Portable" value="<?= $d_commercial['com_portable'] ?>" />											
												</div>
												<div class="col-md-12 mb15">
													<label>Fax</label>
													<input type="text" name="revendeur_fax" class="gui-input telephone" placeholder="Fax" value="<?= $d_commercial['com_fax'] ?>" />											
												</div>
												<div class="col-md-12 mb15">
													<label>Mail</label>
													<input type="email" name="revendeur_mail" class="gui-input" placeholder="Adresse email" value="<?= $d_commercial['com_mail'] ?>" />											
												</div>
											</div>
										</div>
									</div> 
										
											
													
								</div>
								
							</div>
						</div>
						<!--FIN DE REVENDEUR -->
						
						<!--INDIVIS -->	
						<div class="panel" id="indivi" style="display:none;" > 
							<div class="panel-heading" >
								<span class="panel-title">Indivi</span>
							</div>
							<div class="admin-form theme-primary" >
								<div class="panel-body">
									<div class="row mb15" >
										<div class="col-md-6 pt25" >
											<div class="option-group field">
												<label class="option option-dark">
													<input type="checkbox" name="indivi_uti" id="indivi_uti" <?php if(!empty($d_commercial['com_ref_1'])) echo("checked"); ?> >
													<span class="checkbox"></span>fiche commerciale liée à un compte utilisateur
												</label>
											</div>
										</div>
									</div>
									<div class="row mb15" id="bloc_indivi_uti" <?php if(empty($d_commercial['com_ref_1'])) echo("style='display:none;'"); ?> >
							<?php		if(isset($d_agences)){
											if(!empty($d_agences)){ ?>										
												<div class="col-md-6" id="cont_indivi_agence">
													<div class="section">
														<label>Sélectionner une agence</label>
														<label class="field select">
															<select name="indivi_agence" id="indivi_agence" >
													<?php		foreach($d_agences as $a){
																	if(!empty($d_commercial['com_agence']) && $d_commercial['com_agence'] == $a['age_id']){
																		echo("<option selected value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																	}else{
																		echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																	}
																};?>																																	
															</select>
															<i class="arrow simple"></i>
														</label>
													</div>
												</div>									
							<?php			}
										} ?>
										<div class="col-md-6" >
											<label>Sélectionner un utilisateur</label>
											<select id="indivi_utilisateur" name="indivi_utilisateur" placeholder="Compte utilisateur existants" class="select2" >
												<option value="0" >&nbsp;</option>
								<?php			if(!empty($d_commercial) AND $d_commercial['com_ref_1']>0){ ?>
													<option value="<?=$d_commercial['com_ref_1']?>" selected="selected"  ><?=$d_commercial['com_label_1'] . " " . $d_commercial['com_label_2']?></option>
								<?php			} ?>
											</select>
											<small class="text-danger texte_required" id="indivi_utilisateur_required">
												Merci de selectionner l'utilisateur que vous souhaitez associer à cette fiche "indivi".
											</small>

										</div>											
									</div>								
									
									
									<div class="row mb15" >
										<div class="col-md-6">
											<label>Nom du commercial</label>
											<input type="text" name="indivi_label_1" class="gui-input nom" id="indivi_label_1" placeholder="Nom" value="<?= $d_commercial['com_label_1'] ?>" />
											<small class="text-danger texte_required" id="indivi_label_1_required">
												Merci de renseigner un nom pour cette fiche "indivi"
											</small>

										</div>	
										<div class="col-md-6">
											<label>Prénom du commercial</label>
											<input type="text" name="indivi_label_2" class="gui-input prenom" id="indivi_label_2" placeholder="Prénom" value="<?= $d_commercial['com_label_2'] ?>" />											
										</div>
																				
									</div>
									<div id="bloc_indivi_autre" <?php if(!empty($d_commercial['com_ref_1'])) echo("style='display:none;'"); ?> >	
										<div class="row">
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span style="background: white;">Adresse</span>
														</div>
													</div>
												</div>
												<div class="row mb15">
													<div class="col-md-12 mb15">
														<label>Adresse du commercial</label>
														<input type="text" name="indivi_ad1" class="gui-input" placeholder="Adresse" value="<?= $d_commercial['com_ad1'] ?>" />											
													</div>
													<div class="col-md-12 mb15">
														<label>Complément 1</label>
														<input type="text" name="indivi_ad2" class="gui-input" placeholder="Adresse (complément 1)" value="<?= $d_commercial['com_ad2'] ?>" />											
													</div>
													<div class="col-md-12">
														<label>Complément 2</label>
														<input type="text" name="indivi_ad3" class="gui-input" placeholder="Adresse (complément 2)" value="<?= $d_commercial['com_ad3'] ?>" />											
													</div>
												</div>	
												<div class="row">
													<div class="col-md-4">
														<label>Code postal</label>
														<input type="text" name="indivi_cp" class="gui-input code-postal" placeholder="Code postal" value="<?= $d_commercial['com_cp'] ?>" />																								
													</div>
													<div class="col-md-8">
														<label>Ville</label>
														<input type="text" name="indivi_ville" class="gui-input nom" placeholder="Ville" value="<?= $d_commercial['com_ville'] ?>" />																					
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span style="background: white;">Contact</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12 mb15">
														<label>Téléphone</label>
														<input type="text" name="indivi_tel" class="gui-input telephone" placeholder="Téléphone" value="<?= $d_commercial['com_tel'] ?>" />											
													</div>
													<div class="col-md-12 mb15">
														<label>Portable</label>
														<input type="text" name="indivi_portable" class="gui-input telephone" placeholder="Portable" value="<?= $d_commercial['com_portable'] ?>" />																						
													</div>
													<div class="col-md-12 mb15">
														<label>Fax</label>
														<input type="text" name="indivi_fax" class="gui-input telephone" placeholder="Fax" value="<?= $d_commercial['com_fax'] ?>" />																					
													</div>
													<div class="col-md-12 mb15">
														<label>Mail</label>
														<input type="email" name="indivi_mail" class="gui-input" placeholder="Adresse email" value="<?= $d_commercial['com_mail'] ?>" />																				
													</div>
												</div>
											</div>
										</div>                                           
															
									</div>									
								</div>
								
							</div>

							
						</div>
						<!--FIN DE INDIVIS -->
						
						<!--AUTRE -->	
						<div class="panel" id="autre" > 
							<div class="panel-heading" >
								<span class="panel-title">Autre</span>
							</div>
							<div class="admin-form theme-primary" >
								<div class="panel-body">
									<div class="row mb15" >
							<?php		if(isset($d_agences)){
											if(!empty($d_agences)){ ?>										
												<div class="col-md-4" id="cont_autre_agence">
													<div class="section">
														<label>Sélectionner une agence</label>
														<label class="field select">
															<select name="autre_agence" id="autre_agence" >
													<?php		foreach($d_agences as $a){
																	if(!empty($d_commercial['com_agence']) && $d_commercial['com_agence'] == $a['age_id']){
																		echo("<option selected value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																	}else{
																		echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																	}
																};?>																																	
															</select>
															<i class="arrow simple"></i>
														</label>
													</div>
												</div>									
							<?php			}
										} ?>
										<div class="col-md-4">
											<label>Nom du commercial</label>
											<input type="text" name="autre_label_1" class="gui-input nom" id="autre_label_1" placeholder="Nom" value="<?= $d_commercial['com_label_1'] ?>" />
											<small class="text-danger texte_required" id="autre_label_1_required">
												Merci de renseigner un nom pour cette fiche.
											</small>											
										</div>	
										<div class="col-md-4">
											<label>Prénom du commercial</label>
											<input type="text" name="autre_label_2" class="gui-input prenom" id="autre_label_2" placeholder="Prénom" value="<?= $d_commercial['com_label_2'] ?>" />
										</div>
										
										
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span style="background: white;">Adresse</span>
													</div>
												</div>
											</div>
											<div class="row mb15">
												<div class="col-md-12 mb15">
													<label>Adresse du commercial</label>
													<input type="text" name="autre_ad1" class="gui-input" placeholder="Adresse" value="<?= $d_commercial['com_ad1'] ?>" />											
												</div>
												<div class="col-md-12 mb15">
													<label>Complément 1</label>
													<input type="text" name="autre_ad2" class="gui-input" placeholder="Adresse (complément 1)" value="<?= $d_commercial['com_ad2'] ?>" />																				
												</div>
												<div class="col-md-12">
													<label>Complément 2</label>
													<input type="text" name="autre_ad3" class="gui-input" placeholder="Adresse (complément 2)" value="<?= $d_commercial['com_ad3'] ?>" />																						
												</div>
											</div>	
											<div class="row">
												<div class="col-md-4">
													<label>Code postal</label>
													<input type="text" name="autre_cp" class="gui-input code-postal" placeholder="Code postal" value="<?= $d_commercial['com_cp'] ?>" />																					
												</div>
												<div class="col-md-8">
													<label>Ville</label>
													<input type="text" name="autre_ville" class="gui-input nom" placeholder="Ville"  value="<?= $d_commercial['com_ville'] ?>" />																	
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span style="background: white;">Contact</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 mb15">
													<label>Téléphone</label>
													<input type="text" name="autre_tel" class="gui-input telephone" placeholder="Téléphone" value="<?=$d_commercial['com_tel'] ?>" />																					
												</div>
												<div class="col-md-12 mb15">
													<label>Portable</label>
													<input type="text" name="autre_portable" class="gui-input telephone" placeholder="Portable" value="<?= $d_commercial['com_portable'] ?>" />																					
												</div>
												<div class="col-md-12 mb15">
													<label>Fax</label>
													<input type="text" name="autre_fax" class="gui-input telephone" placeholder="Fax" value="<?= $d_commercial['com_fax'] ?>" />																						
												</div>
												<div class="col-md-12 mb15">
													<label>Mail</label>
													<input type="email" name="autre_mail" class="gui-input" placeholder="Adresse email" value="<?= $d_commercial['com_mail'] ?>" />																					
												</div>
											</div>
										</div>
									</div>                                           
															
								</div>									
							</div>
								
						</div>
					
				<?php	if(!empty($d_commercial)){ ?>
							<div class="panel" id="autre" > 
								<div class="panel-heading" >
									<span class="panel-title">Archivage</span>
								</div>
								<div class="admin-form theme-primary" >
									<div class="panel-body">
										<div class="row" >
											<div class="col-md-2">
												<div class="section">
													<label class="option option-dark">
														<input type="checkbox" name="com_archive" id="com_archive" value="oui" <?php if(!empty($d_commercial['com_archive'])){ ?>checked<?php } ?>>
														<span class="checkbox"></span>
														<label for="fou_blacklist">Archivé </label>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
				<?php 	} ?>	
					</section>
					<!-- End: Content -->
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="commercial_liste.php" class="btn btn-default btn-sm" role="button" >
							<span class="fa fa-long-arrow-left"></span>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" >&nbsp;</div>
					<div class="col-xs-3 footer-right" >
						<button type="button" class="btn btn-success btn-sm" id="btn_form_com"  >
							<span class="fa fa-save"></span>
							Enregistrer
						</a>
					</div>
				</div>
			</footer>
		</form>
		<?php
		include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>
		<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script type="text/javascript" >
		
			var age_id=0;
<?php		if(isset($d_agences)){
				if(empty($d_agences)){ ?>
					age_id="<?=$acc_agence?>";
<?php			}
			}else{ ?>
				age_id="<?=$acc_agence?>";
<?php		} ?>

			var com_ref_1="<?=$d_commercial["com_ref_1"]?>";
			var com_label_1="<?=$d_commercial["com_label_1"]?>";
			var com_label_2="<?=$d_commercial["com_label_2"]?>";
	
			jQuery(document).ready(function(){	
			
				// AFFICHAGE
				$('input[name=com_type]').click(function(){	
					actualiser_affichage();
				});

				// BLOC VENDEUR
				$("#com_agence").change(function(){					
					get_vendeurs();
				});
				
				// BLOC INDIVI
				
				$("#indivi_uti").click(function(){
					if($(this).is(":checked")){
						$("#bloc_indivi_uti").show();
						$("#bloc_indivi_autre").hide();
					}else{
						$("#bloc_indivi_uti").hide();
						$("#indivi_utilisateur").select2("val",0);
						$("#bloc_indivi_autre").show();
					}
				});
								
				$("#indivi_agence").change(function(){					
					get_indivis()
				});
				
				$("#indivi_utilisateur").change(function(){
					if($(this).val()>0){
						get_utilisateur($(this).val(),afficher_utilisateur,$(this).val(),"");
					}
					
				});
				
				
				// INI
				//on initialise les blocs et les valuers des champs
				
				actualiser_affichage();
				get_vendeurs();
				get_indivis();
						
				// VALIDATION DU FORMULAIRE
				
				$("#btn_form_com").click(function(){
					
					$("#com_agence").prop("required",false);
					$("#utilisateur").prop("required",false);	
					
					$("#revendeur_agence").prop("required",false);
					$("#revendeur_label_1").prop("required",false);
					
					$("#indivi_agence").prop("required",true);		
					$("#indivi_label_1").prop("required",false);
					$("#indivi_utilisateur").prop("required",false);

					$("#autre_agence").prop("required",true);		
					$("#autre_label_1").prop("required",false);						
					
					switch($('input[name=com_type]:checked').val()) {
						
						case "1":					
							if($("#com_agence").lenth>0){
								$("#com_agence").prop("required",true);							
							}
							if(!$("#com_archive").is(":checked")){
								$("#utilisateur").prop("required",true);	
							}
							
						break;
							
						
						case "2":
							if($("#revendeur_agence").lenth>0){
								$("#revendeur_agence").prop("required",true);							
							}
							$("#revendeur_label_1").prop("required",true);						
						break;
						
						case "3":
							if($("#indivi_agence").lenth>0){
								$("#indivi_agence").prop("required",true);							
							}
							$("#indivi_label_1").prop("required",true);			
							if($("#indivi_uti").is(":checked")){
								$("#indivi_utilisateur").prop("required",true);							
							}			
						break;
											
						default:			
							if($("#autre_agence").lenth>0){
								$("#autre_agence").prop("required",true);							
							}
							$("#autre_label_1").prop("required",true);									
						break;
					}
						
					if(valider_form("#form_com")){
						// si validation de la question on demande confirmation
						$("#form_com").submit();					
					}			

				});
				
			});	
			
			function select_utilisateur(agence, defaut, selecteur){
				$.ajax({
					type:'GET',
					url: 'ajax/ajax_utilisateur_commercial.php',
					data : 'agence=' + agence + "&defaut=" + defaut,
					dataType: 'json',
					success: function(data)         
					{	actualiser_select2(data,selecteur,defaut);
					}
				});
			}				
			
			function afficher_utilisateur(json,utilisateur){
				if(json!=undefined){
					if(utilisateur==com_ref_1 && com_label_1!=""){
						$("#indivi_label_1").val(com_label_1);
						$("#indivi_label_2").val(com_label_2);
					}else{
						$("#indivi_label_1").val(json.uti_nom);
						$("#indivi_label_2").val(json.uti_prenom);
					}
				}else{
					$("#indivi_label_1").val("");
					$("#indivi_label_2").val("");
				}
				
			}
			
			function get_vendeurs(){
				console.log($("#com_agence"));
				console.log($("#com_agence").length);
				if($("#com_agence").length>0){
					var com_agence=$("#com_agence").val();
					console.log("com_agence : " + com_agence);
				}else{
					console.log("J'UTILISE age_id");
					var com_agence=age_id;	
				}
				select_utilisateur(com_agence,$("#utilisateur").val(),"#utilisateur");
			}
			
			function get_indivis(){
				if($("#indivi_agence").length>0){
					var com_agence=$("#indivi_agence").val();
				}else{
					var com_agence=age_id;	
				}
				select_utilisateur(com_agence,$("#indivi_utilisateur").val(),"#indivi_utilisateur");
			}
			
			function actualiser_affichage(){
				switch($('input[name=com_type]:checked').val()) {
					case "1":
						$('#vendeur').show();
						$('#revendeur').hide();
						$('#indivi').hide();
						$('#autre').hide();
						break;
					case "2":
						$('#vendeur').hide();   
						$('#revendeur').show();
						$('#indivi').hide();
						$('#autre').hide();
						break;
					case "3":
						$('#vendeur').hide(); 
						$('#revendeur').hide();
						$('#indivi').show();
						$('#autre').hide();
						break;
					case "0":
						$('#vendeur').hide();    
						$('#revendeur').hide();
						$('#indivi').hide();	
						$('#autre').show();						
				}
			}
		</script>
	</body>
</html>