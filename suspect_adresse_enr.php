<?php
include "includes/controle_acces.inc.php";

include("includes/connexion.php");
include("includes/connexion_soc.php");
include("modeles/mod_check_siret.php");

// MAJ D'UNE ADRESSE SUSPECT
/* Un particulier n'a pas accès à ce programme -> un seul contact une seule adresse maj dans suspect_mod*/

$suspect=0;
if(!empty($_POST["suspect"])){
	$suspect=intval($_POST["suspect"]);	
}
$adresse=0;
if(!empty($_POST["adresse"])){
	$adresse=intval($_POST["adresse"]);	
}

if(empty($suspect)){
	echo("Erreur!");
	die();
}

$erreur_txt="";

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);	
}


if(empty($_POST['sad_cp']) OR empty($_POST['sad_ville'])){
	
	$erreur_txt="Impossible de mettre à jour l'adresse";
	
}else{
	
	
	// TRAITEMENT DU FORM
	// champ du form particulier dont la valuer peut être forcé selon le context
	
	
	$sad_type=1;
	if(!empty($_POST["sad_type"])){
		$sad_type=intval($_POST["sad_type"]);	
	}
	
	$sad_nom="";
	if(!empty($_POST['sad_nom'])){
		$sad_nom=$_POST['sad_nom'];
	}
	
	$sad_ville="";
	if(!empty($_POST['sad_ville'])){
		$sad_ville=$_POST['sad_ville'];
	}
	
	$sad_defaut=0;
	if(!empty($_POST["sad_defaut"])){
		$sad_defaut=1;	
	}
	
	$sad_libelle="";
	if(!empty($_POST["sad_libelle"])){
		$sad_libelle=$_POST["sad_libelle"];	
	}
	
	$sad_geo=1;
	if(!empty($_POST["sad_geo"])){
		$sad_geo=intval($_POST["sad_geo"]);	
	}
	
	$sad_siret="";
	if(!empty($_POST["sad_siret"])){
		$sad_siret=$_POST["sad_siret"];	
	}

	// siren
	/* utilise seulement s'il n'est pas encore renseigné dans la fiche suspect
		s'il est connu on priorise le siren de la fiche suspect
	*/
	$sus_siren="";
	if(!empty($_POST["sus_siren"])){
		$sus_siren=$_POST["sus_siren"];	
	}
	
	$contact_defaut = 0;
	if(isset($_POST['sac_defaut'])){
		if(!empty($_POST['sac_defaut'])){
			$contact_defaut = $_POST['sac_defaut'];
		}
	}
	
	// FIN TRAITEMENT FORM
	
	
	/**************
		INFO DB POUR ENREGISTREMENT ET CONTROLE
	***************/
	
	// le suspect
	$sql="SELECT sus_categorie,sus_nom,sus_agence,sus_siren,sus_ident_tva FROM suspects WHERE sus_id=" . $suspect . ";";
	$req=$ConnSoc->query($sql);
	$d_suspect=$req->fetch();
	if(!empty($d_suspect)){
		
		if(empty($sad_nom)){
			$sad_nom=$d_suspect["sus_nom"];
		}
		
		
		if(!empty($d_suspect["sus_siren"])){
			$sus_siren=$d_suspect["sus_siren"];
		}

	}else{
		$erreur_txt="L'adresse n'a pas été mise à jour!";
	}
	
	if(empty($erreur_txt)){
	
		if($adresse>0){
			// modification d'adresse
			// on va lire les données non modifiable -> securité
			$sql="SELECT sad_defaut,sad_type FROM Suspects_Adresses WHERE sad_id=" . $adresse . ";";
			$req=$ConnSoc->query($sql);
			$d_adresse=$req->fetch();
			if(!empty($d_adresse)){
				
				$sad_type=$d_adresse["sad_type"];
				if($d_adresse["sad_defaut"]==1){
					$sad_defaut=1;
				}
				
				
			}else{
				$erreur_txt="L'adresse n'a pas été mise à jour!";
			}
		}else{
			
			// création
			// on verifie qu'il y a une adresse par defaut -> sinon on force la nouvelle adresse en adr defaut
			$sql="SELECT sad_id FROM Suspects_Adresses WHERE sad_type=" . $sad_type . " AND sad_ref_id=" . $suspect . " AND sad_defaut;";
			$req=$ConnSoc->query($sql);
			$d_adresse=$req->fetch();
			if(empty($d_adresse)){
				// pas d'adresse par defaut 
				// on force la nouvelle adresse en adresse defaut
				$sad_defaut=1;
		
			}
		}
	}
	
	// adresse de facturation
	
	if(empty($erreur_txt)){
	
		if($sad_type==2){
			
			// on controle que le siret n'est pas déjà utilisé dans la base client 
			// si existe dans suspect pas grave il sera bloqué au moment du transfert
			
			if(!empty($d_suspect["sus_siren"])){
				// LE SIREN EST DEJA CONNU -> on test uniquement le siret saisie les siret des autre adresse ont déjà été testé
				if(!empty($sus_siren) && !empty($sad_siret)){
					
					$siret_existe=check_siret($sus_siren,$sad_siret,$adresse);
					if(!empty($siret_existe)){
						$erreur_txt="Le siret " . $sus_siren . " " . $sad_siret . " est déjà utilisé dans la base client";
					}
				}
			}elseif(!empty($sus_siren)){
				
				// on saisie le siren depuis l'adresse de facturation -> il va impacter les autres adresses.
				
				$req=$ConnSoc->query("SELECT sad_siret FROM Suspects_Adresses WHERE sad_ref_id=" . $suspect . " AND sad_type=2;");
				$d_adresse_fac=$req->fetchAll();
				if(!empty($d_adresse_fac)){
					foreach($d_adresse_fac as $ad_fac){
						$siret_existe=check_siret($sus_siren,$ad_fac["sad_siret"],0);
						if(!empty($siret_existe)){
							$erreur_txt.="Le siret " . $sus_siren . " " . $ad_fac["sad_siret"] . " est déjà utilisé dans la base client<br/>";
						}
					}
				}
				
			}
		}else{
			$sad_siret="";
		}
		
	}

	/****************************
		ENREGISTREMENT
	****************************/
	
	IF(empty($erreur_txt)){
		
		if(empty($sad_libelle)){
			$sad_libelle=$sad_nom . " " . $sad_ville;	
		}


		if($adresse>0){
			
			// MODIF
			
			$sql="UPDATE Suspects_Adresses SET 
			sad_nom=:sad_nom,
			sad_service=:sad_service,
			sad_ad1=:sad_ad1,
			sad_ad2=:sad_ad2,
			sad_ad3=:sad_ad3,
			sad_cp=:sad_cp,
			sad_ville=:sad_ville,
			sad_defaut=:sad_defaut,
			sad_libelle=:sad_libelle,
			sad_siret=:sad_siret,
			sad_geo=:sad_geo
			WHERE sad_id=" . $adresse . " AND sad_ref_id=" . $suspect . ";";
			$req = $ConnSoc->prepare($sql);
			$req->bindParam("sad_nom",$sad_nom);
			$req->bindParam("sad_service",$_POST['sad_service']);
			$req->bindParam("sad_ad1",$_POST["sad_ad1"]);
			$req->bindParam("sad_ad2",$_POST["sad_ad2"]);
			$req->bindParam("sad_ad3",$_POST["sad_ad3"]);
			$req->bindParam("sad_cp",$_POST["sad_cp"]);
			$req->bindParam("sad_ville",$sad_ville);
			$req->bindParam("sad_defaut",$sad_defaut);
			$req->bindParam("sad_libelle",$sad_libelle);
			$req->bindParam("sad_siret",$sad_siret);
			$req->bindParam("sad_geo",$sad_geo);
			try{
				$req->execute();
			}catch( PDOException $Exception ){
				$erreur_txt=$Exception->getMessage();
				$erreur_txt.="Erreur, l'adresse n'a pas été mis à jour!";
			}

			// INSERTION SUSPECTS ADRESSES CONTACTS
			$req = $ConnSoc->prepare("DELETE FROM Suspects_Adresses_Contacts WHERE sac_adresse = :sac_adresse;");
			$req->bindParam("sac_adresse",$adresse);
			$req->execute();
			
			if(!empty($_POST['sac_contact'])){
	
				foreach($_POST['sac_contact'] as $c){
					if(!empty($c)){
						
						$sac_defaut=0;
						if($contact_defaut==$c){
							$sac_defaut=1;
						}

						$req = $ConnSoc->prepare("INSERT INTO Suspects_Adresses_Contacts (
						sac_adresse, sac_contact, sac_defaut ) VALUES (
						:sac_adresse, :sac_contact, :sac_defaut);");
						$req->bindParam("sac_adresse",$adresse);
						$req->bindParam("sac_contact",$c);
						$req->bindParam("sac_defaut",$sac_defaut);
						try {
							$req->execute();
						}catch( PDOException $Exception ){
							$erreur_txt=$Exception->getMessage();
						}
					}
					
				}
			}
		
		}else{
		
			// CREATION
			
			$req = $ConnSoc->prepare("INSERT INTO Suspects_Adresses (
			sad_ref, sad_ref_id, sad_type, sad_nom, sad_service, sad_ad1, sad_ad2, sad_ad3, sad_cp, sad_ville, sad_defaut, sad_libelle, sad_siret
			,sad_geo ) VALUES (
			1, :sad_ref_id, :sad_type, :sad_nom, :sad_service, :sad_ad1, :sad_ad2, :sad_ad3, :sad_cp, :sad_ville, :sad_defaut, :sad_libelle, :sad_siret
			,:sad_geo);");
			$req->bindParam("sad_ref_id",$suspect);
			$req->bindParam("sad_type",$sad_type);
			$req->bindParam("sad_nom",$sad_nom);
			$req->bindParam("sad_service",$_POST['sad_service']);
			$req->bindParam("sad_ad1",$_POST["sad_ad1"]);
			$req->bindParam("sad_ad2",$_POST["sad_ad2"]);
			$req->bindParam("sad_ad3",$_POST["sad_ad3"]);
			$req->bindParam("sad_cp",$_POST["sad_cp"]);
			$req->bindParam("sad_ville",$sad_ville);
			$req->bindParam("sad_defaut",$sad_defaut);
			$req->bindParam("sad_libelle",$sad_libelle);
			$req->bindParam("sad_siret",$sad_siret);
			$req->bindParam("sad_geo",$sad_geo);
			try {
				$req->execute();
				$adresse = $ConnSoc->lastInsertId();
			}catch( PDOException $Exception ){
				$erreur_txt=$Exception->getMessage();
			}
			// INSERTION SUSPECTS ADRESSES CONTACTS
			if(!empty($_POST['sac_contact'])){
				
				
				foreach($_POST['sac_contact'] as $k => $c){
					if(!empty($c)){
						
						$sac_defaut=0;
						if($c==$contact_defaut){
							$sac_defaut=1;
						}
						$req = $ConnSoc->prepare("INSERT INTO Suspects_Adresses_Contacts (
						sac_adresse, sac_contact, sac_defaut ) VALUES (
						:sac_adresse, :sac_contact, :sac_defaut);");
						$req->bindParam("sac_adresse",$adresse);
						$req->bindParam("sac_contact",$c);
						$req->bindParam("sac_defaut",$sac_defaut);
						try {
							$req->execute();
						}catch( PDOException $Exception ){
							$erreur_txt=$Exception->getMessage();
						}
					}
				}
			}

			
			
		}
	}
	
	/****************************
		TRAITEMENT ANNEXE
	****************************/
	
	// adresse par défaut
	if($sad_defaut==1){
		
		// ON SUPP L'ANCIENNE ADRESSE PAR DEFAUT
		$sql="UPDATE Suspects_Adresses SET sad_defaut=0 WHERE sad_type=" . $sad_type . " AND sad_ref_id=" . $suspect . " AND NOT sad_id=" . $adresse . ";";
		$req = $ConnSoc->query($sql);
		
		IF($sad_type==1){
			
			// il s'agit de l'adresse d'intervention par défaut du suspect => mise à jour de la fiche
			
			// on recherche le contact par defaut associé à cette adresse
			$sql="SELECT * FROM Suspects_Contacts 
			LEFT JOIN suspects_adresses_contacts ON (suspects_adresses_contacts.sac_contact = suspects_contacts.sco_id)
			 WHERE sac_adresse=" . $adresse . " AND sac_defaut;";
			$req = $ConnSoc->query($sql);
			$d_contact=$req->fetch();
			if(empty($d_contact)){
				$d_contact["sco_id"]=0;
				$d_contact["sco_fonction"]=0;
				$d_contact["sco_fonction_nom"]="";
				$d_contact["sco_titre"]=0;
				$d_contact["sco_nom"]="";
				$d_contact["sco_prenom"]="";
				$d_contact["sco_tel"]="";
				$d_contact["sco_mail"]="";			
			}
			
			
			$sql="UPDATE suspects SET 
			sus_adresse=:sus_adresse,
			sus_adr_nom=:sus_adr_nom,
			sus_adr_service=:sus_adr_service,
			sus_adr_ad1=:sus_adr_ad1,
			sus_adr_ad2=:sus_adr_ad2,
			sus_adr_ad3=:sus_adr_ad3,
			sus_adr_cp=:sus_adr_cp,
			sus_adr_ville=:sus_adr_ville,
			sus_contact=:sus_contact,
			sus_con_fct=:sus_con_fct,
			sus_con_fct_nom=:sus_con_fct_nom,
			sus_con_titre=:sus_con_titre,
			sus_con_nom=:sus_con_nom,
			sus_con_prenom=:sus_con_prenom,
			sus_con_tel=:sus_con_tel,
			sus_con_mail=:sus_con_mail
			WHERE sus_id=" . $suspect . ";";
			$req = $ConnSoc->prepare($sql);
			$req->bindParam("sus_adresse",$adresse);
			$req->bindParam("sus_adr_nom",$_POST['sad_nom']);
			$req->bindParam("sus_adr_service",$_POST['sad_service']);
			$req->bindParam("sus_adr_ad1",$_POST["sad_ad1"]);
			$req->bindParam("sus_adr_ad2",$_POST["sad_ad2"]);
			$req->bindParam("sus_adr_ad3",$_POST["sad_ad3"]);
			$req->bindParam("sus_adr_cp",$_POST["sad_cp"]);
			$req->bindParam("sus_adr_ville",$sad_ville);
			$req->bindParam("sus_contact",$d_contact["sco_id"]);
			$req->bindParam("sus_con_fct",$d_contact["sco_fonction"]);
			$req->bindParam("sus_con_fct_nom",$d_contact["sco_fonction_nom"]);
			$req->bindParam("sus_con_titre",$d_contact["sco_titre"]);
			$req->bindParam("sus_con_nom",$d_contact["sco_nom"]);
			$req->bindParam("sus_con_prenom",$d_contact["sco_prenom"]);
			$req->bindParam("sus_con_tel",$d_contact["sco_tel"]);
			$req->bindParam("sus_con_mail",$d_contact["sco_mail"]);			
			try{
				$req->execute();
			}catch( PDOException $Exception ){
				$erreur_txt=$Exception->getMessage();
				$erreur_txt.="<br/>Erreur, l'adresse par défaut n'a pas été mise à jour sur la fiche suspect.";
			}
			
			// pas besoin de dubliquer en base N
			
		}else{
			
			// ADRESSE DE FAC PAR DEFAUT
			
			//$sus_siret="";
			/*if(!empty($sus_siren) AND !empty($sad_siret)){
				$sus_siret=$sus_siren . " " . $sad_siret;
			}*/
			
			$sql="UPDATE suspects SET sus_siren=:sus_siren";
			//if(!empty($sus_siret)){
			//	$sql.=",sus_siret=:sus_siret";
			//}
			$sql.=" WHERE sus_id=" . $suspect . ";";
			$req = $ConnSoc->prepare($sql);
			$req->bindParam("sus_siren",$sus_siren);
			//if(!empty($sus_siret)){
			//	$req->bindParam("sus_siret",$sus_siret);
			//}
			try{
				$req->execute();
			}catch( PDOException $Exception ){
				$erreur_txt=$Exception->getMessage();
				$erreur_txt.="Erreur, le siren n'a pas été enregistré sur la fiche suspect.";
			}
		}
		
	}
}

if(!empty($_SESSION['retourAdresse'])){
	$retour=$_SESSION['retourAdresse'];
	if(!empty($erreur_txt)){
		$retour.="&erreur=" . $erreur_txt; 
	}
}
$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Votre adresse a été enregistrée"
	);
header("location : " . $retour);
