<?php

include "includes/controle_acces.inc.php";
include("../includes/connexion.php");

$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fichier = :ffi_fichier");
$req->bindParam(':ffi_fichier', $_GET['id']);
$req->execute();
$existe = $req->fetch();

if(!empty($existe)){
	Header('Location: fournisseur_document_liste.php?error=1');
}else{
	$req = $Conn->prepare("DELETE FROM fournisseurs_documents WHERE fdo_id = :fdo_id");

	$req->bindParam(':fdo_id', $_GET['id']);
	$req->execute();

	Header('Location: fournisseur_document_liste.php?succes=1');
}