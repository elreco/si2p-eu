<?php


	include "includes/controle_acces.inc.php";
	include_once 'includes/connexion.php';
	include_once 'includes/connexion_soc.php';


	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}


?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />


		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php
			include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<section id="content" class="animated fadeIn">

					<div class="admin-form theme-primary ">
						<div class="panel heading-border panel-primary">
							<div class="panel-body bg-light">

								<div class="content-header">
									<h2>Affacturage : Demandes de garanties (Carte 107)</h2>
								</div>

								<!--<form method="post" action="param_produit_liste.php" id="form" >

										<div class="col-md-3 text-center">
											<button type="submit" class="btn btn-sm btn-info" name="search" >
												<i class="fa fa-search" ></i> Afficher les produits
											</button>
										</div>
									</div>
									-->



										<div class="table-responsive mt15">
											<table class="table table-striped table-hover">
												<thead>
													<tr class="dark" >
														<th colspan="2" >Fichier</th>
													</tr>
												</thead>
												<tbody>
														<tr>
															<td class="text-center" >
														<?php	if(file_exists("documents/Societes/" . $acc_societe . "/garanties.txt")){ ?>

																	<a href="documents/Societes/<?=$acc_societe?>/garanties.txt?<?= time() ?>" download target="_blank" class="btn btn-sm btn-info" >
																		<i class="fa fa-download" ></i> Télécharger le fichier
																	</a>

														<?php	}else{
																	echo("&nbsp;");
																}?>
															</td>
														</tr>

															<?php if(!empty($_SESSION['garanties']['erreur'])){ ?>
																<tr>
																	<td  colspan="2" class="warning">
																<?= $_SESSION['garanties']['erreur'] ?>
																</td>
																</tr>
															<?php
																unset($_SESSION['garanties']['erreur']);
														}?>

												</tbody>
											</table>
										</div>
								</form>
							</div>
						</div>
					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
			<?php	if(!empty($_SESSION["retour"])){ ?>
						<a href="<?=$_SESSION["retour"]?>" class="btn btn-default btn-sm" >
							<span class="fa fa-long-arrow-left"></span>
							Retour
						</a>
			<?php	} ?>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="demandes_de_garanties_enr.php" class="btn btn-success btn-sm" >
						<span class="fa fa-refresh"></span>
						Actualiser le fichier
					</a>
				</div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>


		<script type="text/javascript" >

			jQuery(document).ready(function (){

				$("#pro_categorie").change(function(){
					if($(this).val()==1){
						$("#bloc_famille").show();
					}else{
						$("#pro_famille").val(0);
						$("#pro_sous_famille").val("");
						$("#pro_sous_sous_famille").val("");
						$("#bloc_famille").hide();
					}
				});

				$("#sub_produit").click(function(){
					$("#form").prop("action","param_produit_enr.php");
					$("#form").submit();
				});


			});
		</script>

	</body>
</html>
