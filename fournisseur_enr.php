<?php 
include "includes/controle_acces.inc.php";
// error_reporting( error_reporting() & ~E_NOTICE );
include "includes/connexion.php";
include "includes/connexion_soc.php";


$erreur_text="";
if(isset($_POST)){
    // verif code nom siren siret

    $code=$_POST['fou_code'];
    $siret=substr($_POST['fou_siret'], 12, 6);
    $siren=substr($_POST['fou_siret'], 0, 11);
    $ident=$_POST['fou_tva'];
    $sql="SELECT * FROM fournisseurs";

    $mil="";
    if($code!=""){
        $mil.="  OR fou_code LIKE :code"; 
    };
    if($siren!=""){
        $mil.= " OR (fou_siren = :siren";
        if($siret != ""){
            $mil.= " AND fou_siret = :siret";   
        }
    };
    $mil.= ")";
    if($ident!=""){
        $mil.="  OR fou_tva LIKE :ident"; 
    };

    if($mil!=""){
        $sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
    }
    $req = $Conn->prepare($sql);
    
    /////////////// bind params
    if($code!=""){
        $req->bindParam("code",$code);
    };
    if($siren!=""){
        $req->bindParam("siren",$siren);
    };
    if($siret!=""){
        $req->bindParam("siret",$siret);
    };
    if($ident!=""){
        $req->bindParam("ident",$ident);
    };
    ///////////////
    $req->execute();
    $fournisseur_existe = $req->fetch();
    var_dump($fournisseur_existe);
    if(empty($fournisseur_existe)){
             
        if(isset($_POST['fou_groupe']) && $_POST['fou_type'] !=2){

            if(isset($_POST['fou_maison_mere']) && $_POST['fou_maison_mere'] == "oui"){
                $fou_groupe = 1;
                $fou_filiale_de = 0; 
            }else if(isset($_POST['fou_maison_mere']) && $_POST['fou_maison_mere'] == "non"){
                if($_POST['fou_filiale_de'] != 0){
                    $fou_groupe = 1;
                    $fou_filiale_de = $_POST['fou_filiale_de'];
                }else{
                    $fou_groupe = 1;
                    $fou_filiale_de = 0;
                }
            }
            
        }else{
            $fou_groupe = 0;
            $fou_filiale_de = 0;
        }
        if(empty($_POST['fou_montant_max'])){
            $_POST['fou_montant_max'] = 0;
        }

        if(isset($_POST['fou_depassement_autorise']) && $_POST['fou_depassement_autorise'] == "oui"){
            $fou_depassement = 1;
        }else{
            $fou_depassement = 0;
        }
        // modalités
        $reg_type=0;
        if(!empty($_POST['fou_reg_type'])){
            $reg_type=intval($_POST['fou_reg_type']);
        }
        
        $reg_formule=0;
        if(!empty($_POST['reg_formule'])){
            $reg_formule=intval($_POST['reg_formule']);
        }
        $reg_nb_jour=0;
        if(!empty($_POST['reg_nb_jour_' . $reg_formule])){
            $reg_nb_jour=intval($_POST['reg_nb_jour_' . $reg_formule]);
        }
        $reg_fdm=0;
        if(!empty($_POST['reg_fdm_' . $reg_formule])){
            $reg_fdm=intval($_POST['reg_fdm_' . $reg_formule]);
        }
        // fin modalités
		
		// personne connecte
		
		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=intval($_SESSION['acces']["acc_agence"]);  
		}
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
		}

        
        $req = $Conn->prepare("INSERT INTO fournisseurs (fou_code, fou_nom, fou_type, fou_site, fou_sage, fou_groupe, fou_filiale_de, fou_siren, fou_siret, fou_tva, fou_reg_type, fou_reg_nb_jour, fou_reg_formule, fou_reg_fdm, fou_iban, fou_montant_max, fou_bic, fou_depassement_autorise) VALUES (:fou_code, :fou_nom, :fou_type, :fou_site, :fou_sage, :fou_groupe, :fou_filiale_de, :fou_siren, :fou_siret, :fou_tva, :fou_reg_type, :fou_reg_nb_jour, :fou_reg_formule, :fou_reg_fdm, :fou_iban, :fou_montant_max, :fou_bic, :fou_depassement)");
        $req->bindParam(':fou_code', $_POST['fou_code']);
        $req->bindParam(':fou_nom', $_POST['fou_nom']);
        $req->bindParam(':fou_type', $_POST['fou_type']);
        $req->bindParam(':fou_site', $_POST['fou_site']);
        $req->bindParam(':fou_sage', $_POST['fou_sage']);
        $req->bindParam(':fou_filiale_de', $fou_filiale_de);
        $req->bindParam(':fou_groupe', $fou_groupe);
        $req->bindParam(':fou_siren', $siren);
        $req->bindParam(':fou_siret', $siret);
        $req->bindParam(':fou_tva', $_POST['fou_tva']);
        $req->bindParam(':fou_reg_type', $reg_type);
        $req->bindParam(':fou_reg_nb_jour', $reg_nb_jour);
        $req->bindParam(':fou_reg_formule', $reg_formule);
        $req->bindParam(':fou_reg_fdm', $reg_fdm);
        $req->bindParam(':fou_bic', $_POST['fou_bic']);
        $req->bindParam(':fou_iban', $_POST['fou_iban']);
        $req->bindParam(':fou_montant_max', $_POST['fou_montant_max']);
        $req->bindParam(':fou_depassement', $fou_depassement);
		try{
			$req->execute();
			$id = $Conn->lastInsertId();
		}Catch(Exception $e){
			$erreur_txt="Le fournisseur n'a pas été enregistré! " . $e->getMessage();
		}
		
		if(empty($erreur_txt)){
			
			// ZONE D'INTERVENTION
			
			// -> par defaut on affecte le fournisseur à la société connecté
			
			
		/*	$sql="SELECT sde_societe,sde_agence FROM Societes_Departements WHERE sde_departement=:departement";					
			$req_dep=$Conn->prepare($sql);
			
			$req_add_dep = $Conn->prepare("INSERT INTO fournisseurs_interventions (fin_fournisseur, fin_departement) VALUES (:fin_fournisseur, :fin_departement)");           
			
			// on recupere les departements.
			
			$tab_societe=array();
			$tab_societe_cle=array();
			
			$sql="SELECT DISTINCT sde_departement FROM Societes_Departements WHERE sde_societe=:acc_societe";
			if($acc_agence>0){
				$sql.=" AND sde_agence=:acc_agence";
			}
			$req=$Conn->prepare($sql);
			$req->bindParam(':acc_societe',$acc_societe);
			if($acc_agence>0){
				$req->bindParam(':acc_agence',$acc_agence);
			}
			$req->execute();
			$d_departements=$req->fetchAll();
			if(!empty($d_departements)){
				foreach($d_departements as $dep){
					
					$req_dep->bindParam(':departement',$dep["sde_departement"]);
					$req_dep->execute();
					$d_soc=$req_dep->fetchAll();
					foreach($d_soc as $s){
						
						if(empty($tab_societe_cle[$s["sde_societe"]][$s["sde_agence"]])){
							$tab_societe_cle[$s["sde_societe"]][$s["sde_agence"]]=1;
							$tab_societe[]=array(
								"societe" => $s["sde_societe"],
								"agence" => $s["sde_agence"]
							);
						}
					}
					
					// on ajoute le departement a la zone d'intervention du fournisseur
					$req_add_dep->bindParam(':fin_fournisseur', $id);
					$req_add_dep->bindParam(':fin_departement', $dep['sde_departement']);
					$req_add_dep->execute();
					
				}
			}
			
			// ON AJOUTE LES SOCIETES / AGENCES EN FONCTIONS DES DEPARTEMENTS D'INTERVENETIONS
			
			$req_add_soc = $Conn->prepare("INSERT INTO fournisseurs_societes (fso_fournisseur, fso_societe, fso_agence) VALUES (:fso_fournisseur, :fso_societe, :fso_agence)");
			$req_add_soc->bindParam(':fso_fournisseur', $id);
			
			foreach($tab_societe as $t_soc){				 
				$req_add_soc->bindParam(':fso_societe', $t_soc["societe"]);
				$req_add_soc->bindParam(':fso_agence', $t_soc["agence"]);
				$req_add_soc->execute();
			}
			*/
			// FIN ZONE D'INTERVENETION
			
			
			
			if(isset($_POST['ffj_famille'])){
				// si il y a au moins une famille
				if(!empty($_POST['ffj_famille'])){
					foreach($_POST['ffj_famille'] as $f){
						$req = $Conn->prepare("SELECT * FROM fournisseurs_familles WHERE ffa_id =" . $f);
						$req->execute();
						$famille_droit = $req->fetch();
						$peut_ajouter_famille = 0;
						if(!empty($famille_droit['ffa_droit'])){
							
							if($_SESSION["acces"]["acc_droits"][$famille_droit['ffa_droit']]){
								$peut_ajouter_famille = 1;
							}
						}else{
							$peut_ajouter_famille = 1;
						}
						if($peut_ajouter_famille){
							$req = $Conn->prepare("INSERT INTO fournisseurs_familles_jointure (ffj_famille, ffj_fournisseur) VALUES (:ffj_famille, :ffj_fournisseur)");
							$req->bindParam(':ffj_famille', $f);
							$req->bindParam(':ffj_fournisseur', $id);
							$req->execute();
						}else{
							$_SESSION['message'][] = array(
								"titre" => "Attention",
								"type" => "warning",
								"message" => "Vous ne pouvez pas cocher la famille " . $famille_droit['ffa_libelle'] . " car vous n'avez pas le droit"
							);
		
							Header("Location: fournisseur.php");
							die();
						}
					}
				}
			}

			if($_POST['fco_fonction'] == "autre"){ 
				$_POST['fco_fonction'] = 0;
			}
			
			if(!empty($_POST['fco_nom'])){
				$req = $Conn->prepare("INSERT INTO fournisseurs_contacts (fco_fournisseur, fco_fonction, fco_fonction_nom, fco_titre, fco_nom, fco_prenom, fco_tel, fco_fax, fco_mail,fco_portable, fco_defaut) VALUES (:fco_fournisseur, :fco_fonction, :fco_fonction_nom, :fco_titre, :fco_nom, :fco_prenom, :fco_tel, :fco_fax, :fco_mail,:fco_portable, 1)");

				$req->bindParam(':fco_fournisseur', $id);
				$req->bindParam(':fco_fonction', $_POST['fco_fonction']);
				$req->bindParam(':fco_fonction_nom', $_POST['fco_fonction_nom']);
				$req->bindParam(':fco_titre', $_POST['fco_titre']);
				$req->bindParam(':fco_nom', $_POST['fco_nom']);
				$req->bindParam(':fco_prenom', $_POST['fco_prenom']);
				$req->bindParam(':fco_tel', $_POST['fco_tel']);
				$req->bindParam(':fco_fax', $_POST['fco_fax']);
				$req->bindParam(':fco_mail', $_POST['fco_mail']);
				$req->bindParam(':fco_portable', $_POST['fco_portable']);
				$req->execute();
				if($_POST['fou_type'] == 3){
					// création d'un intervenant
					$req = $Conn->prepare("INSERT INTO fournisseurs_intervenants (fin_nom, fin_prenom, fin_ad1, fin_ad2, fin_ad3, fin_cp, fin_ville, fin_tel, fin_fax, fin_mail, fin_fournisseur, fin_archive) VALUES (:fin_nom,:fin_prenom,:fin_ad1,:fin_ad2,:fin_ad3,:fin_cp,:fin_ville,:fin_tel,:fin_fax,:fin_mail, :fin_fournisseur,0) ");
					$req->bindParam(':fin_prenom', $_POST['fco_prenom']);
					$req->bindParam(':fin_nom', $_POST['fco_nom']);
					$req->bindParam(':fin_ad1', $_POST['fad_ad1']);
					$req->bindParam(':fin_ad2', $_POST['fad_ad2']);
					$req->bindParam(':fin_ad3', $_POST['fad_ad3']);
					$req->bindParam(':fin_cp', $_POST['fad_cp']);
					$req->bindParam(':fin_ville', $_POST['fad_ville']);
					$req->bindParam(':fin_fax', $_POST['fco_fax']);
					$req->bindParam(':fin_mail', $_POST['fco_mail']);
					$req->bindParam(':fin_tel', $_POST['fco_tel']);
					$req->bindParam(':fin_fournisseur', $id);
					$req->execute();
				}

			}
			
			if(!empty($_POST['fad_ville']) && !empty($_POST['fad_cp'])){
				$req = $Conn->prepare("INSERT INTO fournisseurs_adresses (fad_fournisseur, fad_nom, fad_ad1, fad_ad2, fad_ad3, fad_cp, fad_ville) VALUES (:fad_fournisseur, :fad_nom, :fad_ad1, :fad_ad2, :fad_ad3, :fad_cp, :fad_ville)");

				$req->bindParam(':fad_fournisseur', $id);
				$req->bindParam(':fad_nom', $_POST['fad_nom']);
				$req->bindParam(':fad_ad1', $_POST['fad_ad1']);
				$req->bindParam(':fad_ad2', $_POST['fad_ad2']);
				$req->bindParam(':fad_ad3', $_POST['fad_ad3']);
				$req->bindParam(':fad_cp', $_POST['fad_cp']);
				$req->bindParam(':fad_ville', $_POST['fad_ville']);
				$req->execute();

			}

			///////////////////// MISE A JOUR ETAT DOCUMENTS FOURNISSEUR ///////////////////
			// mise a jour de l'état
			$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id=" . $id);
			$req->execute();
			$fournisseur = $req->fetch();

			$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_type = 1"); // documents du fournisseur
			$req->execute();
			$documents = $req->fetchAll();
			
			$txt = "";
			// verif etat du document 1 par 1
			foreach($documents as $d){
				$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur=" . $id . " AND ffi_fichier = " . $d['fdo_id']);
				$req->execute();
				$fichier = $req->fetch();

				if(($d['fdo_obligatoire'] == 1 && empty($fichier)) OR (!empty($fichier) && $d['fdo_periodicite_mois'] != 0 && date('Y-m-d') > date('Y-m-d', strtotime("+" . $d['fdo_periodicite_mois'] . " months", strtotime($fichier['ffi_date']))))){
					 $statut = 1; // danger
				

				}elseif(($d['fdo_optionnel'] == 1 && empty($fichier))){
					$statut = 2; // warning
				}else{
					$statut = 3; // succes
				}
				if($statut == 1 OR $statut == 2){
					if(empty($txt)){
						$txt = $d['fdo_libelle'] . "<br>";
					}else{
						$txt .= $d['fdo_libelle'] . "<br>";
					}
				}else{
					$txt = "";
				}
				

			}

			if(!empty($statut)){
					$req = $Conn->prepare("SELECT * FROM fournisseurs_validations WHERE fva_fournisseur = :fva_fournisseur AND fva_societe = :fva_societe");
				$req->bindParam(':fva_fournisseur', $id);
				$req->bindParam(':fva_societe', $_SESSION['acces']['acc_societe']);
				$req->execute();
				$fou_valide = $req->fetch();

				if(empty($fou_valide)){
					 $req = $Conn->prepare("INSERT INTO fournisseurs_validations (fva_admin_etat, fva_admin_txt, fva_fournisseur, fva_societe) VALUES (:fva_admin_etat, :fva_admin_txt, :fva_fournisseur, :fva_societe)");
					$req->bindParam(':fva_fournisseur', $id);
					$req->bindParam(':fva_societe', $_SESSION['acces']['acc_societe']);
					$req->bindParam(':fva_admin_etat', $statut);
					$req->bindParam(':fva_admin_txt', $txt);
					$req->execute();
				}else{
					 $req = $Conn->prepare("UPDATE fournisseurs_validations SET fva_admin_etat = :fva_admin_etat, fva_admin_txt = :fva_admin_txt  WHERE fva_fournisseur = :fva_fournisseur AND fva_societe = :fva_societe");
					$req->bindParam(':fva_fournisseur', $id);
					$req->bindParam(':fva_societe', $_SESSION['acces']['acc_societe']);
					$req->bindParam(':fva_admin_etat', $statut);
					$req->bindParam(':fva_admin_txt', $txt);
					$req->execute();
				}
			   
			}
			///////////////////// FIN MISE A JOUR ETAT DOCUMENTS FOURNISSEUR ///////////////////
			
		}   
    }else{
		$erreur_text="Le fournisseur existe déjà.";
    }
}else{
	$erreur_text="Formulaire incomplet!.";		
}

if(!empty($erreur_text)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur_text
	);
	 Header("Location: fournisseur.php");
}else{
	 Header('Location: fournisseur_voir.php?fournisseur=' . $id);
}
die();
    