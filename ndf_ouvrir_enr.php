<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');

// INSERER LA NDF
$format = "Y-m-d";

// le premier jour du mois
$date_ref = new DateTime($_POST['ndf_annee'] . "-" . $_POST['ndf_mois'] . "-" . 1);
$premier_jour = $date_ref->format($format);
// le milieu 15
$milieu_1 = new DateTime($_POST['ndf_annee'] . "-" . $_POST['ndf_mois'] . "-" . 15);
$milieu_1 = $milieu_1->format($format);
// le milieu 16
$milieu_2 = new DateTime($_POST['ndf_annee'] . "-" . $_POST['ndf_mois'] . "-" . 16);
$milieu_2 = $milieu_2->format($format);
// dernier jour
$dernier_jour = $date_ref->modify('last day of this month');
$dernier_jour = $dernier_jour->format($format);

// aller chercher l'utilisateur concerné par la ndf
$req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id = :uti_id");
$req->bindValue(':uti_id', $_POST['ndf_utilisateur']);
$req->execute();
$utilisateur = $req->fetch();
// fin aller chercher l'utilisateur concerné par la ndf
// déterminer la quinzaine
/*si l'utilisateur a une carte affaire :
il faut que ce soit découpé sur le mois et pas en quinzaine */
if ($utilisateur['uti_carte_affaire'] == 1) {
    $date1 = $premier_jour;
    $date2 = $dernier_jour;
} else {
if ($_POST['ndf_quinzaine'] && $_POST['ndf_quinzaine'] == 2) {
    /////////////// DATE 1 ==> DEBUT /////////////////
    /////////////// DATE 2 ==> FIN ///////////////////
    $date1 = $milieu_2;
    $date2 = $dernier_jour;
} else {
    /////////////// DATE 1 ==> DEBUT /////////////////
    /////////////// DATE 2 ==> FIN ///////////////////
    $date1 = $premier_jour;
    $date2 = $milieu_1;
}

}
/* if( $date1 > date('Y-m-d')) {
    $_SESSION['message'][] = array(
        "titre" => "Note de frais",
        "type" => "warning",
        "message" => "Vous ne pouvez pas ouvrir une note supérieure à la date du jour."
    );

    Header("Location: ndf_ouvrir.php");
    die();
} */
// LE CA
$sql = "SELECT * FROM utilisateurs
LEFT JOIN utilisateurs_societes ON (utilisateurs_societes.uso_utilisateur = utilisateurs.uti_id)
    WHERE uso_agence = " . $utilisateur['uti_agence'] . " AND uso_societe = " . $utilisateur['uti_societe'] . " AND uti_profil = 15 AND uti_archive = 0";
$req = $Conn->query($sql);
$chef_agence = $req->fetch();

$ra = 0;
if (!empty($chef_agence)) {
    $ra = $chef_agence['uti_id'];
}
$req = $Conn->prepare("SELECT * FROM
        ndf WHERE ndf_quinzaine_deb = :ndf_quinzaine_deb AND ndf_quinzaine_fin = :ndf_quinzaine_fin AND ndf_utilisateur = :ndf_utilisateur AND ndf_societe = :ndf_societe AND ndf_agence = :ndf_agence");
$req->bindValue(':ndf_quinzaine_deb', $date1);
$req->bindValue(':ndf_quinzaine_fin', $date2);
$req->bindValue(':ndf_utilisateur', $_POST['ndf_utilisateur']);
$req->bindValue(':ndf_societe', $utilisateur['uti_societe']);
$req->bindValue(':ndf_agence', $utilisateur['uti_agence']);
$req->execute();
$ndf_exists = $req->fetch();

if(!empty($ndf_exists)) {
    $_SESSION['message'][] = array(
        "titre" => "Note de frais",
        "type" => "warning",
        "message" => "La note de frais existe déjà pour cette période."
    );

    Header("Location: ndf_ouvrir.php");
    die();
}else{
    $mois_ref = new DateTime($date1);
    $mois = $mois_ref->format('n');

    $annee_ref = new DateTime($date1);
    $annee = $annee_ref->format('Y');

     $req = $Conn->prepare("SELECT * FROM baremes_kilometres WHERE bki_date_deb <= :bki_date_deb AND bki_date_fin >= :bki_date_deb");
     $req->bindValue(':bki_date_deb', date("Y-m-d"));
     $req->execute();
     $bareme_kilometre = $req->fetch();

    $req = $Conn->prepare("INSERT INTO
        ndf (ndf_quinzaine_deb, ndf_quinzaine_fin, ndf_utilisateur, ndf_devise, ndf_date, ndf_re, ndf_ra, ndf_societe, ndf_agence, ndf_mois, ndf_annee, ndf_bareme, ndf_puissance, ndf_tranche_km)
        VALUES (:ndf_quinzaine_deb, :ndf_quinzaine_fin, :ndf_utilisateur, 'EUR', :ndf_date, :ndf_re, :ndf_ra, :ndf_societe, :ndf_agence, :ndf_mois, :ndf_annee, :ndf_bareme, :ndf_puissance, :ndf_tranche_km)");
    $req->bindValue(':ndf_quinzaine_deb', $date1);
    $req->bindValue(':ndf_quinzaine_fin', $date2);
    $req->bindValue(':ndf_utilisateur', $_POST['ndf_utilisateur']);
    $req->bindValue(':ndf_date', $date1);
    $req->bindValue(':ndf_re', $utilisateur['uti_responsable']);
    $req->bindValue(':ndf_ra', $ra);
    $req->bindValue(':ndf_societe', $utilisateur['uti_societe']);
    $req->bindValue(':ndf_agence', $utilisateur['uti_agence']);
    $req->bindValue(':ndf_mois', $mois);
    $req->bindValue(':ndf_annee', $annee);
    $req->bindValue(':ndf_bareme', $bareme_kilometre['bki_id']);
    $req->bindValue(':ndf_puissance', $utilisateur['uti_veh_cv']);
    $req->bindValue(':ndf_tranche_km', $utilisateur['uti_tranche_km']);
    $req->execute();

    $ndf_id = $Conn->lastInsertId();

    // CALCUL DE L'AVANCE
    $req = $Conn->prepare("SELECT * FROM ndf WHERE
    ndf_societe = " . $utilisateur['uti_societe'] . " AND
    ndf_agence = " . $utilisateur['uti_agence'] . " AND
    ndf_utilisateur = " . $_POST['ndf_utilisateur'] . "
    AND (ndf_statut = 5 OR ndf_statut = 6 OR ndf_statut = 7)
    AND ndf_id =
    (SELECT ndf_id FROM ndf
    WHERE ndf_societe = " . $utilisateur['uti_societe'] . "
    AND ndf_agence = " . $utilisateur['uti_agence'] . "
    AND ndf_utilisateur = " . $_POST['ndf_utilisateur'] . "
    AND ndf_quinzaine_fin < '" . $date1 . "'
     ORDER BY ndf_quinzaine_fin DESC LIMIT 1)
     AND ndf_id != " . $ndf_id);
	$req->execute();
    $ndf_precedente = $req->fetch();

    if (!empty($ndf_precedente)) {
        // JE VAIS CHERCHER LES AVANCES DE LA NOTE PRECEDENTE
        $req = $Conn->prepare("SELECT nav_ttc FROM ndf_avances WHERE nav_ndf = " . $ndf_precedente['ndf_id']);
        $req->execute();
        $ndf_avances = $req->fetch();
        // SI LE TOTAL TTC EST INFERIEUR A L'AVANCE, JE L'AJOUTE A LA NOTE ACTUELLE
        if ($ndf_precedente['ndf_ttc'] < $ndf_avances['nav_ttc']) {
            $nav_date = date('Y-m-d');
            $diff = $ndf_avances['nav_ttc'] - $ndf_precedente['ndf_ttc'];
            if ($diff > 0) {
                $req = $Conn->prepare("INSERT INTO ndf_avances (nav_ndf, nav_utilisateur, nav_date, nav_ttc, nav_commentaire) VALUES (:nav_ndf, :nav_utilisateur, :nav_date, :nav_ttc, :nav_commentaire)");
                $req->bindValue(':nav_ndf', $ndf_id);
                $req->bindValue(':nav_utilisateur', $_POST['ndf_utilisateur']);
                $req->bindValue(':nav_date', $nav_date);
                $req->bindValue(':nav_ttc', $diff);
                $req->bindValue(':nav_commentaire', "Reste de l'avance de la note précédente");
                $req->execute();

                $req = $Conn->prepare("UPDATE ndf_avances SET nav_ttc = :nav_ttc WHERE nav_ndf = " . $ndf_precedente['ndf_id']);
                $req->bindValue(':nav_ttc', $ndf_precedente['ndf_ttc']);
                $req->bindValue(':nav_commentaire', "Avance initiale de " . $ndf_avances['nav_ttc'] . " €");
                $req->execute();
            }
        }
    }
    // FIN CALCUL DE L'AVANCE

    $_SESSION['message'][] = array(
        "titre" => "Note de frais",
        "type" => "success",
        "message" => "La note de frais a été ouverte."
    );

    Header("Location: ndf_liste.php");
    die();
}


