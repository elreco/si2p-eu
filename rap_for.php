<?php 

// AFFICHE LA LISTE DES FACTURES
include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');

// DONNEE UTILE AU PROGRAMME
/* var_dump($_SESSION['fac_relance_tri']);
die(); */
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}

if(!empty($_GET['annee'])){
	$annee=$_GET['annee'];  
}else{
    die("probleme"); 
}

if(!empty($_GET['semaine'])){
	$semaine=$_GET['semaine'];  
}else{
    die("probleme"); 
}

if(!empty($_GET['utilisateur'])){
	$utilisateur=$_GET['utilisateur'];  
}else{
    die("probleme"); 
}
// selectionner tous les intervenants
$sql="SELECT * FROM Intervenants WHERE int_type = 1 AND int_ref_1 = " . $utilisateur;
$req = $ConnSoc->query($sql);
$d_intervenant=$req->fetch();
if(empty($d_intervenant)){
    die("probleme");
}
// selectionner tous les intervenants
$sql="SELECT uti_prenom, uti_nom FROM Utilisateurs WHERE uti_id = " . $utilisateur;
$req = $Conn->query($sql);
$d_utilisateur=$req->fetch();

$_SESSION['retourRapport'] = "rap_for.php?utilisateur=" . $utilisateur ."&semaine=" . $semaine . "&annee=" . $annee;

// DONNEES POUR LA REQUETE

$sql="SELECT * FROM formations WHERE for_annee = " . $annee . " AND for_semaine=" . $semaine . " AND for_intervenant=" . $d_intervenant['int_id'];
$req = $ConnSoc->query($sql);
$rapport=$req->fetchAll();

?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">
					
					
                        <h1>Rapports hebdomadaires de <?= $d_utilisateur['uti_prenom'] ?> <?= $d_utilisateur['uti_nom'] ?></h1>
                        <?php if(!empty($d_intervenants)){ ?>
                        <div class="row">
                            
                            <div class="col-md-12">

                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                
                                                    <th colspan="2" >Date</th>
                                                    <th>Type</th>
                                                    <th>Client</th>
                                                    <th>Nombre de<br/>1/2 jours</th>
                                                    <th>Nombre de<br/>groupes</th>
                                                    <th>Nombre heures<br/>par groupe</th>
                                                    <th>Total<br/>heures</th>
                                                    <th>Nombre de<br/>stagiaires</th>  
                                                    <th>Observations</th>
                                                    <th>Catégorie véhicule</th>
                                                    <th>Véhicule</th>
                                                    <th>Immatriculation</th>
                                                    <th>Km</th>
                                                    <th>Hébergement Hôtel</th>
                                                    <th>Hébergement Autres</th>
                                                    <th>Bureau</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tableRap">
                                                
                                            </tbody>
                                        </table>
                                    </div>

                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="alert alert-warning">
                            Aucun rapport hebdomadaire.
                        </div>
                        <?php }?>	
			
			
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
                        <a href="<?= $_SESSION['retour'] ?>" class="btn btn-default btn-sm">
                            <i class="fa fa-long-arrow-left"></i>
                            Retour
                        </a>
					</div>
					<div class="col-xs-6 footer-middle text-center">
                        <a href="rap_import.php" class="btn btn-info btn-sm">
                            Importer depuis le planning
                        </a>
                        
                    </div>
					<div class="col-xs-3 footer-right">
                        <a href="rap_import.php" class="btn btn-success btn-sm">
                            Ajouter une formation
                        </a>
                    </div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>	
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function (){
                
            });		
            				
		</script>
	</body>
</html>