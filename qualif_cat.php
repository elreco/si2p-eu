<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

include "modeles/mod_qualification.php";

$cat_id=0;
if(!empty($_GET["cat"])){
	$cat_id=intval($_GET["cat"]);
}

$qualification=0;
if(!empty($_GET["qualification"])){
	$qualification=intval($_GET["qualification"]);
}

if(!empty($cat_id)){
	
	$sql="SELECT * FROM Qualifications_Categories WHERE qca_id=" . $cat_id . ";";
	$req=$Conn->query($sql);
	$d_categorie=$req->fetch();
	
	// LA QUALIF 
	
	$sql="SELECT * FROM Qualifications WHERE qua_id=" . $d_categorie["qca_qualification"] . ";";
	$req=$Conn->query($sql);
	$d_qualification=$req->fetch();
	
	
}else{
	$d_categorie=array(
		"qca_libelle" =>"",
		"qca_nom" =>"",
		"qca_ut" =>0,
		"qca_competence" => 0,
		"qca_opt_1" => 0,
		"qca_ut_opt_1" => 0,
		"qca_comp_opt_1" => 0,
		"qca_opt_2" => 0,
		"qca_ut_opt_2" => 0,
		"qca_comp_opt_2" => 0,
		"qca_opt_3" => 0,
		"qca_ut_opt_3" => 0,
		"qca_comp_opt_3" => 0
	);
	
	// LES QUALIF POUR FILTRE
	$qualifications = get_qualifications();
	
}

$lib_option=array(
	"0" => array(
		"1" => "",
		"2" => "",
		"3" => ""
	)
);

// LISTE DES COMPETENCES

$sql="SELECT * FROM Competences ORDER BY com_libelle;";
$req=$Conn->query($sql);
$d_competences=$req->fetchAll();

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Paramètres</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form action="qualif_cat_enr.php" method="POST" id="form_cat">
			<div>
				<input type="hidden" name="qca_id" value="<?=$cat_id?>" />
			</div>
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" class="">
					<section id="content" class="animated fadeIn">
					
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											
											<div class="content-header">
									<?php		if(!empty($cat_id)){ ?>
													<h2>Modification d'une <b class="text-primary">catégorie</b></h2>
									<?php		}else{ ?>
													<h2>Nouvelle <b class="text-primary">catégorie</b></h2>
									<?php		} ?>
											</div>
											<div class="col-md-10 col-md-offset-1">										
								<?php			if(empty($cat_id)){ ?>	
													<div class="row">
														<div class="col-md-12">
															<div class="section">
																<label for="qca_qualification" >Qualification</label>
																<span class="field select">
																	<select id="qca_qualification" name="qca_qualification" required >
																		<option value="">Qualification...</option>
																<?php 	foreach($qualifications as $q){
																			$lib_option[$q["qua_id"]]=array(
																				"1" => $q["qua_opt_1"],
																				"2" => $q["qua_opt_2"],
																				"3" => $q["qua_opt_3"]
																			);
																	
																			if($q["qua_id"]==$qualification){
																				$d_qualification=$q;
																				echo("<option value='" . $q["qua_id"] . "' selected >" . $q["qua_libelle"] . "</option>");
																			}else{
																				echo("<option value='" . $q["qua_id"] . "' >" . $q["qua_libelle"] . "</option>");
																			} 
																		} ?>
																	</select>
																	<i class="arrow simple"></i>
																</span>
															</div>
														</div>
													</div>
								<?php			} ?>
								
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Catégorie</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<div class="section">
															<label for="qca_libelle" >Libellé de la catégorie</label>
															<input type="text" name="qca_libelle" class="gui-input" id="qca_libelle" placeholder="Libellé" required value="<?=$d_categorie['qca_libelle']?>" />
														</div>
													</div>
													<div class="col-md-8">
														<div class="section">
															<label for="qca_nom" >Descriptif</label>
															<input type="text" name="qca_nom" class="gui-input" id="qca_nom" placeholder="Descriptif" required value="<?=$d_categorie['qca_nom']?>" />
														</div>
													</div>
												</div>
												<div class="row">												
													<div class="col-md-4">
														<div class="section">
															<label for="qca_ut" >Unité de temps</label>
															<input type="text" name="qca_ut" class="gui-input input-float" id="qca_ut" placeholder="UT" value="<?=$d_categorie['qca_ut']?>" />
														</div>
													</div>
													<div class="col-md-8">
														<div class="section">
															<label for="qca_competence" >Compétence requise</label>															
															<select id="qca_competence" name="qca_competence" class="select2" >
																<option value="">Compétence...</option>
														<?php 	foreach($d_competences as $comp){
																	if($comp["com_id"]==$d_categorie['qca_competence']){
																		echo("<option value='" . $comp["com_id"] . "' selected >" . $comp["com_libelle"] . "</option>");
																	}else{
																		echo("<option value='" . $comp["com_id"] . "' >" . $comp["com_libelle"] . "</option>");
																	} 
																} ?>
															</select>
														</div>
													</div>
												</div>
										<?php	if(!empty($d_qualification)){ 
													for($bcl=1;$bcl<=2;$bcl++){ 
														$style="";
														if(empty($d_qualification["qua_opt_" . $bcl])){
															$style="style='display:none;'";
														}?>										
														<div class="row opt-<?=$bcl?>" <?=$style?> >
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span id="lib_option_<?=$bcl?>" >Option "<?=$d_qualification["qua_opt_" . $bcl]?>"</span>
																</div>
															</div>
														</div>
														<div class="row opt-<?=$bcl?>" <?=$style?> >
															<div class="col-md-1">
																<div class="radio-custom mt25">
																	<input id="qca_opt_<?=$bcl?>_non" class="opt-non" name="qca_opt_<?=$bcl?>" type="radio" value="0" data-option="<?=$bcl?>" <?php if($d_categorie["qca_opt_" . $bcl]==0) echo("checked"); ?> >
																	<label for="qca_opt_<?=$bcl?>_non">NON</label>
																</div>
															</div>
															<div class="col-md-1">
																<div class="radio-custom mt25">
																	<input id="qca_opt_<?=$bcl?>_oui" class="opt-oui" name="qca_opt_<?=$bcl?>" type="radio" value="1" data-option="<?=$bcl?>" <?php if($d_categorie["qca_opt_" . $bcl]==1) echo("checked"); ?> >
																	<label for="qca_opt_<?=$bcl?>_oui">OUI</label>
																</div>
															</div>
															<div class="col-md-5">
																<div class="section">
																	<label for="qca_ut" >Unité de temps</label>
																	<input type="text" name="qca_ut_opt_<?=$bcl?>" class="gui-input input-float" id="ut_opt_<?=$bcl?>" placeholder="UT option" value="<?=$d_categorie['qca_ut_opt_' . $bcl]?>" <?php if($d_categorie["qca_opt_" . $bcl]==0) echo("disabled"); ?> />
																</div>
															</div>
															<div class="col-md-5">
																<div class="section">
																	<label for="qca_comp_opt_<?=$bcl?>" >Compétence requise</label>							
																	<select id="qca_comp_opt_<?=$bcl?>" name="qca_comp_opt_<?=$bcl?>" class="select2" <?php if($d_categorie["qca_opt_" . $bcl]==0) echo("disabled"); ?> >
																		<option value="">Compétence...</option>
																<?php 	foreach($d_competences as $comp){
																			if($comp["com_id"]==$d_categorie['qca_comp_opt_' . $bcl]){
																				echo("<option value='" . $comp["com_id"] . "' selected >" . $comp["com_libelle"] . "</option>");
																			}else{
																				echo("<option value='" . $comp["com_id"] . "' >" . $comp["com_libelle"] . "</option>");
																			} 
																		} ?>
																	</select>
																</div>
															</div>
														</div>
											<?php	}
												}
												if(!empty($cat_id)){ ?>
													<div class="row" >
														<div class="col-md-12">
															<div class="option-group field">
																<label class="option option-dark">
																	<input type="checkbox" name="qca_archive" value="on" <?php if($d_categorie['qca_archive']) echo("checked") ?> />
																	<span class="checkbox"></span>Archivé
																</label>
															</div>
														</div>
													</div>
										<?php	} ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="qualif_cat_liste.php" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" ></div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" >	
			
			var lib_option=JSON.parse('<?=json_encode($lib_option)?>');
			
			jQuery(document).ready(function(){
				$(".opt-non").click(function(){
					option=$(this).data("option");
					$("#ut_opt_" + option).val(0);
					$("#ut_opt_" + option).prop("disabled",true);
					$("#qca_comp_opt_" + option).select2("val",0);
					$("#qca_comp_opt_" + option).prop("disabled",true);
				});
				$(".opt-oui").click(function(){
					option=$(this).data("option");
					$("#ut_opt_" + option).prop("disabled",false);
					$("#qca_comp_opt_" + option).prop("disabled",false);
				});
				
				$("#qca_qualification").change(function(){
					
					if($(this).val()==""){
						qualification=0;
					}else{
						qualification=$(this).val();
					}
					$.each(lib_option[qualification],function(i,val){
						
						if(val!="" && val!=null){
							$("#lib_option_" + i).html('Option "' + val + '"');
							$(".opt-" + i).show();
						}else{
							console.log("MASQUE");
							$("#qca_opt_" + i + "_non").prop("checked",true);
							console.log(".opt-" + i);
							$(".opt-" + i).hide();
						}
						
					
						
						
					});
					
				});
			});
		</script>
	</body>
</html>
