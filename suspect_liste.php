<?php

// AFFICHE LA LISTE DES SUSPECTS

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_orion_cli_categories.php');
include('modeles/mod_orion_cli_sous_categories.php');
include('modeles/mod_orion_con_fonctions.php');
include('modeles/mod_parametre.php');
//require "modeles/mod_droit.php";
//require "modeles/mod_contact.php";
//require "modeles/mod_get_commerciaux.php";
//require "modeles/mod_erreur.php";

if(!$_SESSION["acces"]["acc_droits"][23]){
	header("location :deconnect.php");
	die();
}
// DONNEE UTILE AU PROGRAMME

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}

$d_client_categorie=orion_cli_categories();
$d_client_sous_categorie=orion_cli_sous_categories();
$d_contact_fonctions=orion_con_fonctions();

$_SESSION['retour'] = "suspect_liste.php";

	if (isset($_POST['search'])){

		// le formulaire a été soumit
		// on actualise les criteres de recherche
		// name en cli_ -> car form de recherche commun avec client


		// critere sur le client
		$cli_categorie=0;
		if(!empty($_POST['cli_categorie'])){
			$cli_categorie=intval($_POST['cli_categorie']);
		}
		$cli_sous_categorie=0;
		if(!empty($_POST['cli_sous_categorie'])){
			$cli_sous_categorie=intval($_POST['cli_sous_categorie']);
		}
		$cli_id=0;
		if(!empty($_POST['cli_id'])){
			$cli_id=intval($_POST['cli_id']);
		}
		$cli_commercial=0;
		if(!empty($_POST['cli_commercial'])){
			$cli_commercial=intval($_POST['cli_commercial']);
		}

		$date1 = convert_date_sql($_POST['cli_uti_dat_1']);
		$date2 = convert_date_sql($_POST['cli_uti_dat_2']);

		$cli_prescripteur=0;
		if(!empty($_POST['cli_prescripteur'])){
			$cli_prescripteur=intval($_POST['cli_prescripteur']);
		}

		$cli_ape=0;
		if(!empty($_POST['cli_ape'])){
			$cli_ape=intval($_POST['cli_ape']);
		}

		$cli_classification=0;
		if(!empty($_POST['cli_classification'])){
			$cli_classification=intval($_POST['cli_classification']);
		}
		$cli_classification_categorie=0;
		if(!empty($_POST['cli_classification_categorie'])){
			$cli_classification_categorie=intval($_POST['cli_classification_categorie']);
		}
		$cli_classification_type=0;
		if(!empty($_POST['cli_classification_type'])){
			$cli_classification_type=intval($_POST['cli_classification_type']);
		}

		$cli_siren="";
		$adr_siret="";
		if(!empty($_POST['cli_siren'])){
			$cli_siren=$_POST['cli_siren'];
			if(!empty($_POST['cli_siret']) AND strlen($cli_siren)==11){
				$adr_siret=$_POST['cli_siret'];
			}
		}

		$cli_import=0;
		if(!empty($_POST['cli_import'])){
			$cli_import=intval($_POST['cli_import']);
		}

		// adresse

		$adr_type=0;
		if(!empty($adr_siret)){
			$adr_type=2; // facturation
		}elseif(!empty($_POST['cli_type_adresse'])){
			$adr_type=intval($_POST['cli_type_adresse']);
		}
		$adr_defaut=0;
		if(isset($_POST['adr_defaut'])){
			$adr_defaut=1;
		}

		// contact
		$con_mail=0; // 1 renseigne 2 non renseigné
		if(!empty($_POST['con_mail'])){
			$con_mail=intval($_POST['con_mail']);
		}
		$con_fonction=0; // 1 renseigne 2 non renseigné
		if(!empty($_POST['con_fonction'])){
			$con_fonction=intval($_POST['con_fonction']);
		}

		// correspondance

		/*$cor_date1 = convert_date_sql($_POST['cli_cor_dat_1']);
		$cor_date2 = convert_date_sql($_POST['cli_cor_dat_2']);

		$correspondance=0;
		if(!empty($_POST['cli_correspondance'])){
			$correspondance=intval($_POST['cli_correspondance']); // 1 avec //2 sans
		}

		$cli_depuis = convert_date_sql($_POST['cli_depuis']);*/

		// critere d'affichage

		/*$aff_cor=0; // 0 rappel 1 dernier corresp
		if(!empty($_POST['aff_cor'])){
			$aff_cor=intval($_POST['aff_cor']);
		}*/


		// mémorisation des critere

		$_SESSION['cli_tri'] = array(
			"cli_categorie" => $cli_categorie,
			"cli_sous_categorie" => $cli_sous_categorie,
			"cli_id" => $cli_id,
			"cli_code" => $_POST['cli_code'],
			"cli_nom" => $_POST['cli_nom'],
			"cli_commercial"   => $cli_commercial,
			"cli_date1"        => $date1,
			"cli_date2"        => $date2,
			"cli_prescripteur" => $cli_prescripteur,
			"cli_ape" => $cli_ape,
			"cli_classification" => $cli_classification,
			"cli_classification_type" => $cli_classification_type,
			"cli_classification_categorie" => $cli_classification_categorie,
			"cli_siren" => $cli_siren,
			"cli_import" => $cli_import,

			// adresse

			"adr_type" => $adr_type,
			"adr_siret" => $adr_siret, // ne contient que les 5 dernier chiffre
			"adr_defaut" => $adr_defaut,
			"adr_cp" => $_POST['adr_cp'],
			"adr_ville" => $_POST['adr_ville'],


			//contact
			"con_nom"          	=> $_POST['con_nom'],
			"con_prenom"       	=> $_POST['con_prenom'],
			"con_mail" 			=> $con_mail,
			"con_fonction" 		=> $con_fonction,
			"con_tel"       	=> $_POST['con_tel']
		);
	}

	// ON CONSTRUIT LA REQUETE

	$critere=array();

	$mil="";

	$rechercheParAdresse=false;
	$rechercheParContact=false;
	$rechercheParCorrespondance=false;

	if(!empty($_SESSION['cli_tri']['cli_id'])){
		$mil.=" AND sus_id =:cli_id";
		$critere["cli_id"]=$_SESSION['cli_tri']['cli_id'];
	}else{

		if(!empty($_SESSION['cli_tri']['cli_code'])){
			$mil.=" AND sus_code LIKE :cli_code";
			$critere["cli_code"]=$_SESSION['cli_tri']['cli_code']."%";
		};
		if(!empty($_SESSION['cli_tri']['cli_nom'])){
			$mil.=" AND sus_nom LIKE :cli_nom";
			$critere["cli_nom"]="%" . $_SESSION['cli_tri']['cli_nom'] ."%";
		};
		if(!empty($_SESSION['cli_tri']['cli_commercial'])){
			$mil.=" AND sus_commercial =:cli_commercial";
			$critere["cli_commercial"]=$_SESSION['cli_tri']['cli_commercial'];
		};
		if(!empty($_SESSION['cli_tri']['cli_date1'])){
			$mil.=" AND sus_date_creation >=:cli_date1";
			$critere["cli_date1"]=$_SESSION['cli_tri']['cli_date1'];
		};
		if(!empty($_SESSION['cli_tri']['cli_date2'])){
			$mil.=" AND sus_date_creation <=:cli_date2";
			$critere["cli_date2"]=$_SESSION['cli_tri']['cli_date2'];
		};
		if(!empty($_SESSION['cli_tri']['cli_categorie'])){
			$mil.=" AND sus_categorie = :cli_categorie";
			$critere["cli_categorie"]=$_SESSION['cli_tri']['cli_categorie'];
		};
		if(!empty($_SESSION['cli_tri']['cli_sous_categorie'])){
			$mil.=" AND sus_sous_categorie = :cli_sous_categorie";
			$critere["cli_sous_categorie"]=$_SESSION['cli_tri']['cli_sous_categorie'];
		};
		if(!empty($_SESSION['cli_tri']['cli_prescripteur'])){
			$mil.=" AND sus_prescripteur =:cli_prescripteur";
			$critere["cli_prescripteur"]=$_SESSION['cli_tri']['cli_prescripteur'];
		};
		if(!empty($_SESSION['cli_tri']['cli_ape'])){
			$mil.=" AND sus_ape =:cli_ape";
			$critere["cli_ape"]=$_SESSION['cli_tri']['cli_ape'];
		};
		if(!empty($_SESSION['cli_tri']['cli_classification'])){
			$mil.=" AND sus_classification =:cli_classification";
			$critere["cli_classification"]=$_SESSION['cli_tri']['cli_classification'];
		};
		if(!empty($_SESSION['cli_tri']['cli_classification_categorie'])){
			$mil.=" AND sus_classification_categorie =:cli_classification_categorie";
			$critere["cli_classification_categorie"]=$_SESSION['cli_tri']['cli_classification_categorie'];
		};
		if(!empty($_SESSION['cli_tri']['cli_classification_type'])){
			$mil.=" AND sus_classification_type =:cli_classification_type";
			$critere["cli_classification_type"]=$_SESSION['cli_tri']['cli_classification_type'];
		};

		if(!empty($_SESSION['cli_tri']['cli_siren'])){
			$mil.=" AND sus_siren LIKE :cli_siren";
			$critere["cli_siren"]=$_SESSION['cli_tri']['cli_siren'];
		};

		if(!empty($_SESSION['cli_tri']['cli_import'])){
			$mil.=" AND sus_import=:cli_import";
			$critere["cli_import"]=$_SESSION['cli_tri']['cli_import'];
		};

		// critère sur les adresses

		if($_SESSION['cli_tri']['adr_type']==1 AND $_SESSION['cli_tri']['adr_defaut']==1){
			// revient a chercher l'adresse qui est en table client
			// adr_siret adr_type a ete force a 2 si condition vrai adr_siret=""
			if(!empty($_SESSION['cli_tri']['adr_cp'])){
				$mil.=" AND sus_adr_cp LIKE :adr_cp";
				$critere["adr_cp"]=$_SESSION['cli_tri']['adr_cp'] ."%";
			};
			if(!empty($_SESSION['cli_tri']['adr_ville'])){
				$mil.=" AND sus_adr_ville LIKE :adr_ville";
				$critere["adr_ville"]="%" . $_SESSION['cli_tri']['adr_ville'] ."%";
			};
		}else{

			if(!empty($_SESSION['cli_tri']['adr_type'])){
				$mil.=" AND sad_type=:adr_type" ;
				$critere["adr_type"]=$_SESSION['cli_tri']['adr_type'];
				$rechercheParAdresse=true;
			};
			if(!empty($_SESSION['cli_tri']['adr_siret'])){
				$mil.=" AND sad_siret LIKE :adr_siret";
				$critere["adr_siret"]=$_SESSION['cli_tri']['adr_siret'];
				$rechercheParAdresse=true;
			};
			if($_SESSION['cli_tri']['adr_defaut']==1){
				$mil.=" AND sad_defaut=1";
				$rechercheParAdresse=true;
			};
			if(!empty($_SESSION['cli_tri']['adr_cp'])){
				$mil.=" AND sad_cp LIKE :adr_cp";
				$critere["adr_cp"]=$_SESSION['cli_tri']['adr_cp'] ."%";
				$rechercheParAdresse=true;
			};
			if(!empty($_SESSION['cli_tri']['adr_ville'])){
				$mil.=" AND sad_ville LIKE :adr_ville";
				$critere["adr_ville"]="%" . $_SESSION['cli_tri']['adr_ville'] ."%";
				$rechercheParAdresse=true;
			};
		}

		// critère sur les contacts
		if(!empty($_SESSION['cli_tri']['con_nom'])){
			$mil.=" AND sco_nom LIKE :con_nom";
			$critere["con_nom"]="%" . $_SESSION['cli_tri']['con_nom'] ."%";
			$rechercheParContact=true;
		};
		if(!empty($_SESSION['cli_tri']['con_prenom'])){
			$mil.=" AND sco_prenom LIKE :con_prenom";
			$critere["con_prenom"]="%" . $_SESSION['cli_tri']['con_prenom'] ."%";
			$rechercheParContact=true;
		};

		if(!empty($_SESSION['cli_tri']['con_mail'])){
			if($_SESSION['cli_tri']['con_mail'] == 1){
				$mil.=" AND sco_mail IS NOT NULL AND sco_mail!=''";
			}else{
				$mil.=" AND (sco_mail IS NULL OR sco_mail='')";
			}
			$rechercheParContact=true;
		};
		if(!empty($_SESSION['cli_tri']['con_fonction'])){
			$mil.=" AND sco_fonction =:con_fonction";
			$critere["con_fonction"]=$_SESSION['cli_tri']['con_fonction'];
			$rechercheParContact=true;
		};
		if(!empty($_SESSION['cli_tri']['con_tel'])){
			$mil.=" AND (sco_tel =:con_tel OR sco_portable =:con_tel OR sco_fax =:con_tel)";
			$critere["con_tel"]=$_SESSION['cli_tri']['con_tel'];
			$rechercheParContact=true;
		};

		// sur les correspondances
	/*	if(!empty($_SESSION['cli_tri']['cor_date1'])){
			$mil.=" AND NOT sco_rappel AND sco_rappeler_le>=:cor_date1";
			$critere["cor_date1"]=$_SESSION['cli_tri']['cor_date1'];
			$rechercheParCorrespondance=true;
		};
		if(!empty($_SESSION['cli_tri']['cor_date2'])){
			$mil.=" AND NOT sco_rappel AND sco_rappeler_le<=:cor_date2";
			$critere["cor_date2"]=$_SESSION['cli_tri']['cor_date2'];
			$rechercheParCorrespondance=true;
		};
		if(!empty($_SESSION['cli_tri']['correspondance'])){
			if($_SESSION['cli_tri']['correspondance']>0){
				$rechercheParCorrespondance=true;
				if($_SESSION['cli_tri']['correspondance']==1){
						// avec
					if(!empty($_SESSION['cli_tri']['cli_depuis'])){
						$mil.=" AND sco_date>=:cli_depuis";
						$critere["cli_depuis"]=$_SESSION['cli_tri']['cli_depuis'];
					}else{
						$mil.=" AND NOT ISNUll(sco_date)";
					}
				}else{
						// sans
					$mil.=" AND (ISNUll(sco_date)";
					if(!empty($_SESSION['cli_tri']['cli_depuis'])){
						$mil.=" OR sco_date<:cli_depuis";
						$critere["cli_depuis"]=$_SESSION['cli_tri']['cli_depuis'];
					}
					$mil.=")";
				}
			}
		}*/
	}

	// fin critère form

	// critere lie au droit d'accès aux fiches

	if($acc_agence>0){
		$mil.=" AND sus_agence=" . $acc_agence;
	}
	if(!$_SESSION['acces']["acc_droits"][6]){
		$mil.=" AND sus_utilisateur=" . $_SESSION['acces']['acc_ref_id'];
	}
	$mil.=" AND ISNULL(Suspects_Correspondances.sco_id)";

	// champ a selectionne
	$sql="SELECT DISTINCT sus_id,sus_code,sus_nom,sus_categorie,sus_sous_categorie";
	if($rechercheParAdresse){
		$sql.=",sad_id,sad_type,sad_service AS ad_service,sad_ad1 AS ad1, sad_ad2 AS ad2, sad_ad3 AS ad3 ,sad_cp AS ad_cp ,sad_ville AS ad_ville";
	}else{
		$sql.=",sus_adr_service AS ad_service,sus_adr_ad1 AS ad1, sus_adr_ad2 AS ad2, sus_adr_ad3 AS ad3, sus_adr_cp AS ad_cp, sus_adr_ville AS ad_ville";
	}
	if($rechercheParContact){
		$sql.=",sco_prenom AS prenom, sco_nom AS nom, sco_fonction AS fonction, sco_fonction_nom AS fonction_nom ,sco_tel AS tel, sco_mail AS mail";
	}else{
		$sql.=",sus_con_prenom AS prenom, sus_con_nom AS nom, sus_con_fct AS fonction, sus_con_fct_nom AS fonction_nom, sus_con_tel AS tel, sus_con_mail AS mail";
	}
	/*if($_SESSION['cli_tri']['aff_cor']==1){
		$sql.=",sus_cor_date AS corresp_date";
	}else{
		$sql.=",sus_cor_rappel AS corresp_date";
	}*/

	// pour la condition isnull()
	//$sql.=",sco_id";

	// table
	$sql.=" FROM Suspects LEFT JOIN Suspects_Correspondances ON (Suspects.sus_id = Suspects_Correspondances.sco_suspect)";

	if($rechercheParAdresse){
		$sql.=" LEFT JOIN Suspects_Adresses ON (Suspects.sus_id = Suspects_Adresses.sad_ref_id)";
		if(!$rechercheParContact){
			$sql.=" LEFT JOIN Suspects_Adresses_Contacts ON (Suspects_Adresses_Contacts.sac_contact = Suspects_Adresses_Contacts.sac_adresse AND sac_defaut=1)";
		}
	}
	if($rechercheParContact){
		$sql.=" LEFT JOIN Suspects_Contacts ON (Suspects.sus_id = Suspects_Contacts.sco_ref_id)";
	}
	/*if($rechercheParCorrespondance){
		$sql.=" LEFT JOIN Suspects_Correspondances ON (Suspects.sus_id = Suspects_Correspondances.sco_suspect)";
	}*/
	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY sus_code,sus_nom";
	$req = $ConnSoc->prepare($sql);
	if(!empty($critere)){
		foreach($critere as $c => $u){
			if($c == "debut"){
				$req->bindValue($c,$u, PDO::PARAM_INT);
			}else{
				$req->bindValue($c,$u);
			}
		}
	};
	$req->execute();
	$suspects=$req->fetchAll();
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">

		<form action="suspect_suppr.php" method="POST" id="form_delete"  >
			<div>
				<input type="hidden" name="suspect" id="suspect" value="0" />
			</div>
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">

		<?php 			if(!empty($suspects)){ ?>

							<h1>Liste des suspects</h1>
							<div class="table-responsive admin-form">
								<table class="table" id="table_id">
									<thead>
										<tr class="dark2" >
											<th>ID</th>
											<th>Code</th>
											<th>Nom</th>
											<th>Type d'adresse</th>
											<th>Adresse</th>
											<th>CP</th>
											<th>Ville</th>
											<th>Contact</th>
											<th>Fonction</th>
											<th>Tel</th>
											<th>Mail</th>
								<?php		/*if($_SESSION['cli_tri']['aff_cor']==1){ ?>
												<th>Date corresp</th>
								<?php		}else{ ?>
												<th>Date rappel</th>
								<?php		}*/ ?>
											<th class="no-sort">Supprimer</th>
											<th class="text-center no-sort">
												<label class="option mn" data-toggle="tooltip" title="Sélectionner tout">
													<input type="checkbox" id="selectall"  name="mobileos" value="FR">
													<span class="checkbox mn"></span>
												</label>
											</th>
										</tr>
									</thead>
									<tbody>
							<?php		$page=array();

										foreach($suspects as $c){

											$page[]=$c["sus_id"] . "-1";



											// style de la ligne
											$style="";
											if(!empty($c['sus_sous_categorie'])){
												if(!empty($d_client_sous_categorie[$c['sus_sous_categorie']])){
													$style="background-color:" . $d_client_sous_categorie[$c['sus_sous_categorie']]["csc_couleur"] . ";color:#FFF;";
													$style_a="color:#FFF;";
												}
											}elseif(!empty($c['sus_categorie'])){
												if(!empty($d_client_categorie[$c['sus_categorie']])){
													$style="background-color:" . $d_client_categorie[$c['sus_categorie']]["cca_couleur"] . ";";
												}
											}


											$adr_type_lib="Intervention";
											if(!empty($c['sad_type'])){
												if($c['sad_type'] == 1){
													$adr_type_lib="Intervention";
												}elseif($c['sad_type'] == 2){
													$adr_type_lib="Facturation";
												}elseif($c['sad_type'] == 3){
													$adr_type_lib="Envoi de facture";
												}
											}
											$adresse_lib="";
											if(!empty($c['ad_service'])){
												$adresse_lib=$c['ad_service'];
											}
											if(!empty($c['ad1'])){
												if(!empty($adresse_lib)){
													$adresse_lib.="<br/>";
												}
												$adresse_lib.=$c['ad1'];
											}
											if(!empty($c['ad2'])){
												if(!empty($adresse_lib)){
													$adresse_lib.="<br/>";
												}
												$adresse_lib.=$c['ad2'];
											}
											if(!empty($c['ad3'])){
												if(!empty($adresse_lib)){
													$adresse_lib.="<br/>";
												}
												$adresse_lib.=$c['ad3'];
											}

											// fonction
											if(!empty($c['fonction'])){
												$fonction_nom=$d_contact_fonctions[$c['fonction']]["cfo_libelle"];
											}else{
												$fonction_nom=$c['fonction_nom'];
											}

											$corresp_date="";
											/*if(!empty($c['corresp_date'])){
												$dt_corresp_date=date_create_from_format("Y-m-d",$c['corresp_date']);
												$corresp_date=$dt_corresp_date->format("d/m/Y");
											}*/?>
											<tr>
												<td><?= $c['sus_id'] ?></td>
												<td><a href="suspect_voir.php?suspect=<?= $c['sus_id'] ?>"><?= $c['sus_code'] ?></a></td>
												<td><?= $c['sus_nom'] ?></td>
												<td><?=$adr_type_lib?></td>
												<td><?=$adresse_lib?></td>
												<td><?= $c['ad_cp'] ?></td>
												<td><?= $c['ad_ville'] ?></td>
												<td>
												<?php if(!empty($c['nom']) && !empty($c['prenom'])): ?>
													<?= $c['prenom'] . " " .$c['nom'] ?>
												<?php endif; ?>
												</td>
												<td><?=$fonction_nom?></td>
												<td><?= $c['tel'] ?></td>
												<td><?= $c['mail'] ?></td>
												<!--<td><?=$corresp_date?></td>-->
												<td class="text-center">
													<a href="#" data-suspect="<?= $c['sus_id']; ?>" class="btn btn-danger btn-sm suspect-del" >
														<i class="fa fa-times"></i>
													</a>
												</td>
												<td class="text-center">
													<label class="option block mn">
														<input type="checkbox" class="checkbox1" name="check_list[]" value="<?= $c['sus_id'] ?>">
														<span class="checkbox mn"></span>
													</label>
												 </td>
											</tr>
				<?php					}
										$_SESSION['client_tableau'] = $page; ?>
									</tbody>
								</table>
							</div>
		<?php 			}else{ ?>
							<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-warning" style="border-radius:0px;">
									Aucun suspect correspondant à votre recherche.
								</div>
							</div>
		<?php			} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="suspect_tri.php" class="btn btn-default btn-sm" role="button">
							<span class="fa fa-search"></span>
							<span class="hidden-xs">Nouvelle recherche</span>
						</a>
					</div>
					<div class="col-xs-3 footer-middle">&nbsp;</div>
					<div class="col-xs-6 footer-right">
						<a href="suspect.php" class="btn btn-success btn-sm" role="button">
							<span class="fa fa-plus"></span>
							<span class="hidden-xs">Nouvelle fiche</span>
						</a>
						<a href="#" class="btn btn-danger btn-sm suspect-del" data-suspect="0" >
							<span class="fa fa-times"></span>
							<span class="hidden-xs">Supprimer</span>
						</a>
			<?php		//if($_SESSION["acces"]["acc_profil"]==13){ ?>
							<a href="#" class="btn btn-default btn-sm suspect-pas" data-suspect="0" >
								<span class="hidden-xs">Passer en prospect</span>
							</a>
			<?php		//} ?>
					</div>
				</div>
			</footer>
		</form>

		<div id="modal_confirmation_user" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Confirmer ?</h4>
					</div>
					<div class="modal-body">
						<p>Message</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							Annuler
						</button>
						<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
							Confirmer
						</button>
					</div>
				</div>
			</div>
		</div>

<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function (){

				$(".suspect-del").click(function(){

					suspect=$(this).data("suspect");
					console.log("SUSPECT : " + suspect);
					if(suspect>0){
						texte_supp="Êtes-vous sur de vouloir supprimer ce suspect?";
					}else{
						texte_supp="Êtes-vous sur de vouloir supprimer tous les suspects sélectionnés?";
					}
					modal_confirmation_user(texte_supp,supprimer_suspect,suspect,"","");

				});
				$(".suspect-pas").click(function(){

					suspect=$(this).data("suspect");
					console.log("SUSPECT : " + suspect);
					if(suspect>0){
						texte_supp="Êtes-vous sur de vouloir passer ce suspect en prospect ?";
					}else{
						texte_supp="Êtes-vous sur de vouloir passer tous les suspects sélectionnés en prospects ?";
					}
					modal_confirmation_user(texte_supp,passer_suspect,suspect,"","");

				});

				$('#table_id').DataTable( {
					"language": {
						"url": "vendor/plugins/DataTables/media/js/French.json"
					},
					"paging": false,
					"searching": false,
					"info": false,
					"order": [[ 1, "asc" ]]
				} );

				$("#selectall").change(function(){
					$(".checkbox1").prop('checked', $(this).prop("checked"));
				});
			});

			function supprimer_suspect(suspect){
				$("#suspect").val(suspect);
				$("#form_delete").attr("action", "suspect_suppr.php");
				$("#form_delete").submit();
			}
			function passer_suspect(suspect){
				$("#suspect").val(suspect);
				$("#form_delete").attr("action", "suspect_transfert.php");
				$("#form_delete").submit();
			}

		</script>
	</body>
</html>
