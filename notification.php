<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'modeles/mod_parametre.php';

    $reqmod = $Conn->prepare("SELECT * FROM Societes
        LEFT JOIN
        utilisateurs_societes ON (utilisateurs_societes.uso_societe = societes.soc_id)
        WHERE soc_archive = 0  AND  uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " ORDER BY soc_nom");
    $reqmod->execute();
    $societes = $reqmod->fetchAll();



?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Notification</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
    <form action="notification_enr.php" method="POST" id="admin-form">
        <div id="main">
            <?php
            include "includes/header_def.inc.php";
            ?>


            <!-- Start: Content-Wrapper -->
            <section id="content_wrapper" class="">

                <section id="content" class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="admin-form theme-primary ">
                                <div class="panel heading-border panel-primary">
                                    <div class="panel-body bg-light">
                                        <div class="text-left">

                                            <div class="content-header">
                                                <h2>Créer une <b class="text-primary">notification</b></h2>

                                            </div>

                                            <div class="col-md-10 col-md-offset-1">
                                                <div class="row">
                                                    <div class="col-md-6 mb20">
                                                        <label for="">Type : </label>
                                                        <select name="not_type" id="not_type" class="form-control" >
                                                            <option value="0">Normale</option>
                                                            <option value="1">Popup sur la page d'accueil</option>
                                                        </select>
                                                        <div class="section mt20">
                                                            <input type="text" name="texte" class="gui-input" id="texte" placeholder="Texte">
                                                        </div>
                                                        <div class="section mt20">
                                                            <input type="url" name="url" class="gui-input" id="texte" placeholder="Url">

                                                        </div>
                                                        <h2>Fonctionnalités avancées</h2>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="section">
                                                                    <input type="text" name="icone" class="gui-input" id="icone" placeholder="Icone (fa fa-***)" value="info">
                                                                    <small>Vous trouverez la liste des icones disponibles ici : <a target="_blank" href="https://fontawesome.com/v4.7.0/icons/">FontAwesome</a></small>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="section">
                                                                    <input type="text" name="classe" class="gui-input" id="texte" value="bg-info" placeholder="Classe (bg-danger, bg-success, bg-warning, bg-info)">
                                                                    <small>bg-danger : rouge, bg-success : vert, bg-warning : orange, bg-info : bleu</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb10">
                                                        <label class="option option-dark mt20 mb10">
                                                            <input type="checkbox" id="tout_cocher" value="oui">
                                                            <span class="checkbox"></span>
                                                            <label for="tout_cocher">Tout cocher</label>
                                                        </label>
                                                        <div class="row">

                                                            <?php
                                                            foreach($societes as $s){

                                                            ?>
                                                            <div class="col-md-12">
                                                                <?php if(empty($s['uso_agence'])){ ?>
                                                                <label class="option option-dark mb20">
                                                                    <input type="checkbox" name="not_societe[<?= $s['soc_id'] ?>]" id="not_societe" value="<?= $s['soc_id'] ?>">

                                                                    <span class="checkbox"></span>
                                                                    <label for="not_societe" style="font-weight:bold"
                                                                    >
                                                                        <?= $s['soc_nom'] ?>
                                                                    </label>
                                                                </label>
                                                                <?php }?>
                                                                <?php
                                                                $agence = [];
                                                                if(!empty($s['uso_agence'])){
                                                                    $reqmod = $Conn->prepare("SELECT * FROM
                                                                        agences
                                                                        WHERE age_archive = 0 AND age_id = " . $s['uso_agence'] . " ORDER BY age_nom");
                                                                    $reqmod->execute();
                                                                    $agence = $reqmod->fetch();

                                                                }
                                                                if(!empty($agence)){
                                                                ?>
                                                                <div class="col-md-12">
                                                                    <label class="option option-dark mb20 ml10">
                                                                        <input type="checkbox" name="not_agence[<?= $agence['age_id'] ?>]" id="not_agence" value="<?= $s['soc_id'] ?>">

                                                                        <span class="checkbox"></span>
                                                                        <label for="not_societe"
                                                                        >
                                                                            <?= $agence['age_nom'] ?>
                                                                        </label>
                                                                    </label>

                                                                </div>
                                                                <?php } ?>

                                                                </div>
                                                            <?php } ?>

                                                        </div>
                                                    </div>
                                                </div>

                                  </div>


                              </div>
                          </div>

                      </div>
                  </div>

              </div>
          </section>
          <!-- End: Content -->
      </section>
    </div>
    <!-- End: Main -->
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left" >
                <a href="parametre.php" class="btn btn-default btn-sm">
                    <i class="fa fa-long-arrow-left"></i>
                    Retour
                </a>
            </div>
            <div class="col-xs-6 footer-middle" ></div>
            <div class="col-xs-3 footer-right" >
                <button type="submit" class="btn btn-success btn-sm">
                    <i class='fa fa-save'></i> Enregistrer
                </button>
            </div>
        </div>
    </footer>
    </form>
    <?php
    	include "includes/footer_script.inc.php"; ?>

    <script src="vendor/plugins/mask/jquery.mask.js"></script>
    <!-- plugin pour les masques formulaires -->

    <script src="vendor/plugins/holder/holder.min.js"></script>
    <!-- pour mettre des images de tests -->
    <!-- Theme Javascript -->

    <script src="assets/js/custom.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
         // Stuff to do as soon as the DOM is ready
         $("#tout_cocher").click(function(){
             $('input:checkbox').not(this).prop('checked', this.checked);
         });
    });


    </script>
</body>
</html>
