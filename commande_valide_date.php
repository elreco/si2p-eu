<?php
include "includes/controle_acces.inc.php";
include("includes/connexion.php");
include("includes/connexion_fct.php");

include("modeles/mod_add_notification.php");
include("modeles/mod_parametre.php");
include("modeles/mod_tva.php");


// PARAMETRE (sécurisation des valeurs)

$commande_id=0;
if (!empty($_GET['commande'])) {
	$commande_id=intval($_GET['commande']);
}

if (empty($commande_id)) {
	echo("Paramètres absents. Impossible d'afficher la page!");
	die();
}

$profil_id=0;
if (!empty($_GET['profil'])) {
	$profil_id=intval($_GET['profil']);
}

$droit_id=0;
if (!empty($_GET['droit'])) {
	$droit_id=intval($_GET['droit']);
}

if ( (!empty($profil_id) AND $_SESSION['acces']['acc_profil']!=$profil_id) OR (!empty($droit_id) AND !$_SESSION['acces']['acc_droits'][$droit_id]) ) {

	$_SESSION['message'] = array(
		"aff" => "modal",
		"titre" => "Accès refusé!",
		"type" => "danger",
		"message" => "Vous n'êtes pas autorisé à valider cette DA"
	);
	Header('Location: commande_voir.php?commande=' . $commande_id);
	die();
}

// la personne connecté

$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
$nom_prenom=$_SESSION['acces']['acc_prenom'] . " " . $_SESSION['acces']['acc_nom'];

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
}


if (!empty($profil_id)) {

	// validation par profil

	$req = $Conn->prepare("UPDATE commandes_validations SET cva_date = NOW(), cva_nom_prenom = :cva_nom_prenom
	WHERE cva_profil = :cva_profil AND cva_commande = :cva_commande");
	$req->bindParam("cva_nom_prenom",$nom_prenom);
	$req->bindParam("cva_profil",$_SESSION['acces']['acc_profil']);
	$req->bindParam("cva_commande",$commande_id);
	$req->execute();

}elseif (!empty($droit_id)) {

	// validation par droit

	$req = $Conn->prepare("UPDATE commandes_validations SET cva_date = NOW(), cva_nom_prenom = :cva_nom_prenom
	WHERE cva_droit = :cva_droit AND cva_commande = :cva_commande");
	$req->bindParam("cva_nom_prenom",$nom_prenom);
	$req->bindParam("cva_droit",$droit_id);
	$req->bindParam("cva_commande",$commande_id);
	$req->execute();

} else {

	// validation par user

	$req = $Conn->prepare("UPDATE commandes_validations SET cva_date = NOW(), cva_nom_prenom = :cva_nom_prenom
	WHERE cva_utilisateur = :cva_utilisateur AND cva_commande = :cva_commande");
	$req->bindParam("cva_nom_prenom",$nom_prenom);
	$req->bindParam("cva_utilisateur",$acc_utilisateur);
	$req->bindParam("cva_commande",$commande_id);
	$req->execute();

}

// On regarde s'il reste des validations

$actualise = 1;
$req = $Conn->prepare("SELECT * FROM commandes_validations WHERE cva_commande = " . $_GET['commande'] . " AND ISNULL(cva_date);");
$req->execute();
$validations = $req->fetchAll();
if(!empty($validations)){
	$actualise = 0;
}

if($actualise == 1){ 

	// IL NE RESTE PLUS DE VALIDATIONS = LA DA devient BC

	$warning_text="";
	
	// met a jour la commande a validé

	$req = $Conn->prepare("SELECT com_id,com_donneur_ordre,com_numero,com_action,com_societe,com_agence FROM commandes WHERE com_id = " . $_GET['commande']);
	$req->execute();
	$commande = $req->fetch();
	
	$commande["com_numero"]= str_replace("DA","BC",$commande["com_numero"]);

	$req = $Conn->prepare("UPDATE commandes SET com_etat = 2, com_numero='" . $commande["com_numero"] . "' WHERE com_id = " . $_GET['commande']);
	$req->execute();


	/*$charge=0;
	$fournisseurs=array();
	$demi_j_total=0;
	$demi_j_st=0;
	$verrou_auto=false;*/

	if (!empty($commande["com_action"])){

		// LE BC est lié à une action

		if (!empty($commande["com_societe"])) {

			$ConnFct=connexion_fct($commande["com_societe"]);

			/***************************************************
			 *  ON VERIFIE QUE CHAQUE ST a un BC
			 ***************************************************/

			// requetes preparées

			$sql_get_bc="SELECT com_id,com_ht FROM Commandes WHERE com_action=:action AND com_societe=:societe AND com_fournisseur=:fournisseur AND com_etat>1 AND NOT com_annule;";
			$req_get_bc=$Conn->prepare($sql_get_bc);
			$req_get_bc->bindParam(":action",$commande["com_action"]);
			$req_get_bc->bindParam(":societe",$commande["com_societe"]);

			$sql_get_four_interco = "SELECT fou_id FROM Fournisseurs WHERE fou_interco_soc=:interco_soc AND fou_interco_age=:interco_age AND NOT fou_archive;";
			$req_get_four_interco = $Conn->prepare($sql_get_four_interco);	

			// recheche des différents intervenants sous-traitants

			$verrou_auto=0;
			$act_charge=0;

			$sql="SELECT DISTINCT int_type,int_ref_1,int_ref_2 FROM Plannings_Dates,Intervenants 
			WHERE pda_intervenant=int_id AND pda_type=1 AND pda_ref_1=:action AND int_type IN (2,3)";
			$req=$ConnFct->prepare($sql);
			$req->bindParam(":action",$commande["com_action"]);
			$req->execute();
			$d_intervenants=$req->fetchAll();
			if (!empty($d_intervenants)) {
					
				foreach($d_intervenants as $int){

					if ($int["int_type"]==3) {

						// sous-traitant

						$req_get_bc->bindParam(":fournisseur",$int["int_ref_1"]);
						$req_get_bc->execute();
						$d_bc=$req_get_bc->fetch();
						if (empty($d_bc)) {
							$verrou_auto=1;
							
						}else{
							$act_charge=$act_charge + $d_bc["com_ht"];
						}

					} elseif ($int["int_type"]==2) {

						// interco

						$demi_j_st++;

						if(empty($d["int_ref_1"])){

							// int non lié à un fournisseur
							$verrou_auto=1;
						

						}else {	

							$req_get_four_interco -> bindParam("interco_soc",$int["int_ref_1"]);
							$req_get_four_interco -> bindParam("interco_age",$int["int_ref_2"]);
							$req_get_four_interco ->execute();
							$d_fou_interco=$req_get_four_interco -> fetchAll();
							if (!empty($d_fou_interco)) {

								if(count($d_fou_interco) > 1 ){

									$verrou_auto=1;
								

								}else {

									$req_get_bc->bindParam(":fournisseur",$d_fou_interco["fou_id"]);
									$req_get_bc->execute();
									$d_bc=$req_get_bc->fetch();
									if (empty($d_bc)) {
										$verrou_auto=1;										
									}else{
										$act_charge=$act_charge + $d_bc["com_ht"];
									}
									
								} 
							}else{
								$verrou_auto=1;						
							}

						}

					}
				}
			}

			$act_marge_ca=0;	// CA validé pour l'action au moment de la validation de la marge (via validation des BC)
								// si le CA tombe sous cette valeur, l'action repasse en blocage administratif et nécéssite une dérogation compta

			if ($verrou_auto==0) {

				// tous les ST disposent d'un BC
				//

				// on ne peut pas utiliser act_ca car = au CA facturé. A ce stade pas encore de facturation
				$sql="SELECT SUM(acl_ca) AS ca FROM Actions_Clients WHERE acl_action=" . $commande["com_action"] . " AND NOT acl_archive AND acl_confirme;";
				$req=$ConnFct->query($sql);
				$d_action_clients=$req->fetch();
				if(!empty($d_action_clients)){
					$act_marge_ca=$d_action_clients["ca"];
				}

			}

			// MAJ DE L'ACTION

			$sql="UPDATE Actions SET act_verrou_admin=:act_verrou_admin, act_charge=:act_charge, act_marge_ca=:act_marge_ca, act_verrou_bc=:act_verrou_bc
			WHERE act_id=:action;";
			$req=$ConnFct->prepare($sql);
			$req->bindParam(":action",$commande["com_action"]);
			$req->bindParam(":act_verrou_admin",$verrou_auto);
			$req->bindParam(":act_verrou_bc",$verrou_auto);
			$req->bindParam(":act_charge",$act_charge);
			$req->bindParam(":act_marge_ca",$act_marge_ca);
			$req->execute();

			// MAJ DES DATES

			$sql="UPDATE Plannings_Dates SET pda_alert=:pda_alert WHERE pda_type=1 AND pda_ref_1=:action;";
			$req=$ConnFct->prepare($sql);
			$req->bindParam(":action",$commande["com_action"]);
			$req->bindParam(":pda_alert",$verrou_auto);
			$req->execute();

		}else{
			$warning_text="La DA a été validée mais l'action associée n'a pas pu être identifiée. Le blocage administratif n'a pas été actualisé!";
		}
	}
	// fin !empty com_action

	// notifications
	add_notifications("<i class='fa fa-truck'></i>","Votre bon de commande " . $commande['com_numero'] . " est validé","bg-info","commande_voir.php?commande=" . $_GET['commande'],"","",$commande['com_donneur_ordre'],0,0,0);
	// fin notifications

	// générer un pdf
	include('modeles/mod_commande_pdf.php');
	// fin générer un pdf


}

if(!empty($warning_text)){
	$_SESSION['message'] = array(
		"aff" => "modal",
		"titre" => "Avertissement",
		"type" => "warning",
		"message" => $warning_text
	);
}else{

	if($actualise==1){
		$success="La DA a été validée!";
	}else{
		$success="Votre validation a bien été enregistrée.";
	}
	$_SESSION['message'][] = array(
		"aff" => "",
		"titre" => "Enregistrement terminé",
		"type" => "success",
		"message" => $success
	);

}
//die();
Header('Location: commande_voir.php?commande=' . $_GET['commande'] . '&succes=5');

?>
