<?php
$commerce_li = true;

$menu_actif = "1-1";

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include "modeles/mod_categorie.php";
include "modeles/mod_famille.php";
include "modeles/mod_sous_famille.php";
include "modeles/mod_qualification.php";


$_SESSION["retour"] = "produit_tri.php";
unset($_SESSION['pro_tri']);

?>
<!DOCTYPE html>
<html>
<!-- Mirrored from admindesigns.com/demos/absolute/1.1/layout_horizontal-sm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 09:52:52 GMT -->
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>SI2P - Orion - Recherche produit</title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!--
       <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
       <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
     -->
     <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
     <!-- Favicon -->
     <link rel="shortcut icon" href="assets/img/favicon.png">

     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

  </head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="produit_liste.php">
			<div id="main">
<?php	 		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											<div class="text-center">
												<div class="content-header">
													<h2>Recherche de <b class="text-primary title-suscli">produits</b></h2>
												</div>
												<div class="col-md-10 col-md-offset-1">
													<div class="row">
														
														<div class="col-md-6">
														  <div class="section">
															<div class="field prepend-icon">
															  <input type="text" name="pro_code_produit" id="pro_code_produit"
															  class="gui-input" placeholder="Code produit"
															  >
															  <label for="soc_ad1" class="field-icon">
																<i class="fa fa-barcode"></i>
															  </label>
															</div>
														  </div>
														</div>
														<div class="col-md-6">
														  <div class="section">
															<div class="field prepend-icon">
															  <input type="text" name="pro_libelle" id="pro_libelle"
															  class="gui-input" placeholder="Libellé"
															  >
															  <label for="pro_libelle" class="field-icon">
																<i class="fa fa-barcode"></i>
															  </label>
															</div>
														  </div>
														</div>
														
													</div>
													
													<div class="row">
														<div class="col-md-6">
															<div class="field select">
																<select name="pro_categorie" id="pro_categorie">
																	<option value="">Catégorie ...</option>
															<?php 	if (isset($_SESSION['pro_tri'])):
																		echo get_categorie_select($_SESSION['pro_tri']['pro_categorie']);
																	else:
																		echo get_categorie_select(0);
																	endif;?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
													 
													<div class="row mt20" id="bloc_famille" style="display:none;" >						
														<div class="col-md-4">
															<div class="field select">
																<select name="pro_famille" id="pro_famille" onchange="sous_famille(this.value)">
																	<option value="0">Famille ...</option>
															<?php 	if (isset($_SESSION['pro_tri'])):
																		echo get_famille_select($_SESSION['pro_tri']['pro_famille']);
																	else:
																		echo get_famille_select(0);
																	endif;?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>                      
														<div class="col-md-4">
															<div class="field select">
																<select name="pro_sous_famille" id="pro_sous_famille" onchange="sous_sous_famille(this.value)">
																	<option value="">Sous-famille ...</option>
															<?php 	if (isset($_SESSION['pro_tri'])):
																		echo get_sous_famille_select($_SESSION['pro_tri']['pro_famille'], $_SESSION['pro_tri']['pro_sous_famille']);
																	endif; ?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
														<div class="col-md-4">
															<div class="field select">
																<select name="pro_sous_sous_famille" id="pro_sous_sous_famille">
																	<option value="">Sous-Sous-famille ...</option>
															<?php 	if (isset($_SESSION['pro_tri'])):
																		echo get_sous_sous_famille_select($_SESSION['pro_tri']['pro_sous_famille'], $_SESSION['pro_tri']['pro_sous_sous_famille']);
																	endif; ?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>								
													</div>
													
													
													
													<div class="row mt20">                           							
														<div class="col-md-6">
															<div class="field select">
																<select name="pro_inter_intra" id="pro_inter_intra">
																	<option value="0">Intra / Inter</option>
																	<option value="1">Inter</option>
																	<option value="2">Intra</option>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
														<div class="col-md-6"> 
															<div class="field select">
																<select name="pro_qualification" id="pro_qualification">
																	<option value="">Qualification ...</option>
															<?php 	if (isset($_SESSION['pro_tri'])):
																		echo get_qualification_select($_SESSION['pro_tri']['pro_qualification']);
																	else:
																		echo get_qualification_select(0);
																	endif; ?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
													
													<div class="row mt20">
														<div class="col-md-6">
															<label class="option">
																<input type="checkbox" name="pro_recurrent" id="pro_recurrent" value="pro_recurrent" <?php if(!empty($_SESSION['pro_tri']['pro_recurrent'])): ?> checked <?php endif; ?>>
																<span class="checkbox"></span>
																<label for="pro_recurrent">Récurrent</label>
															</label>
														</div>
														<div class="col-md-6">
															<label class="option">
																<input type="checkbox" name="pro_deductible" id="pro_deductible" value="pro_deductible" <?php if(!empty($_SESSION['pro_tri']['pro_deductible'])): ?> checked <?php endif; ?>>
																<span class="checkbox"></span>
																<label for="pro_deductible">Déductible</label>
															</label>
														</div>
													</div>
													
													<div class="row mt20">						
														<div class="col-md-6">                          
															<label class="option">
																<input type="checkbox" name="pro_archive" id="pro_archive" value="pro_archive" <?php if(!empty($_SESSION['pro_tri']['pro_archive'])): ?> checked <?php endif; ?>>
																<span class="checkbox"></span>
																<label for="pro_archive">Archivé</label>
															</label>
														</div>	
										<?php			if($_SESSION['acces']["acc_droits"][2]){ ?>		
															<div class="col-md-6">                          
																<label class="option">
																	<input type="checkbox" name="pro_challenge" id="pro_challenge" value="on" />
																	<span class="checkbox"></span>
																	<label for="pro_challenge">Produit "challenge"</label>
																</label>
															</div>	
										<?php			} ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>				
					</section>
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix">
				<div class="row">			
					<div class="col-xs-3 footer-left">
				
					</div>
					<div class="col-xs-6 footer-middle text-center">
			<?php		if($_SESSION['acces']["acc_profil"]==13){ ?>
							<a href="import/sync_produit.php" class="btn btn-default btn-sm" role="button" >
							  <span class="fa fa-refresh"></span>
							  Import local
							</a>
							<a href="import/sync_produit.php?srv=1" class="btn btn-default btn-sm" role="button" >
							  <span class="fa fa-refresh"></span>
							  Import srv
							</a>
			<?php		} ?>
					</div>
					<div class="col-xs-3 footer-right">
			<?php		if($_SESSION['acces']["acc_droits"][2]){ ?>
							<a href="produit.php" class="btn btn-success btn-sm" role="button" >
							  <span class="fa fa-plus"></span>
							  Nouveau produit
							</a>
			<?php		} ?>
						<button type="submit" name="search" class="btn btn-primary btn-sm">
						  <i class='fa fa-search'></i> Rechercher
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery-ui-datepicker.min.js"></script>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="vendor/plugins/holder/holder.min.js"></script>
		<script src="assets/js/custom.js"></script>
		<script type="text/javascript" >
		
			jQuery(document).ready(function (){
				
				$("#pro_categorie").change(function(){
					if($(this).val()==1){
						$("#bloc_famille").show();
					}else{
						$("#pro_famille").val(0);
						$("#pro_sous_famille").val("");
						$("#pro_sous_sous_famille").val("");
						$("#bloc_famille").hide();
					}
				});
				
			});
		
			function sous_famille(selected_id){
				//-----------------------------------------------------------------------
				// 2) Send a http request with AJAX http://api.jquery.com/jQuery.ajax/
				//-----------------------------------------------------------------------

				$.ajax({
				  type:'POST',
				  url: 'ajax/ajax_sous_famille.php',
				  //the script to call to get data
				  data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
				  //for example "id=5&parent=6"

				  dataType: 'json',                //data format
				  success: function(data)          //on recieve of reply
				  {

					if (data['0'] == undefined){

					  $("#pro_sous_famille").find("option:gt(0)").remove();

					}else{
					  $("#pro_sous_famille").find("option:gt(0)").remove();
					  $.each(data, function(key, value) {
						$('#pro_sous_famille')
						.append($("<option></option>")
						  .attr("value",value["psf_id"])
						  .text(value["psf_libelle"]));
					  });
					}
				  }
				});
			}
			function sous_sous_famille(selected_id){
				//-----------------------------------------------------------------------
				// 2) Send a http request with AJAX http://api.jquery.com/jQuery.ajax/
				//-----------------------------------------------------------------------

				$.ajax({
				  type:'POST',
				  url: 'ajax/ajax_sous_sous_famille.php',
				  //the script to call to get data
				  data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
				  //for example "id=5&parent=6"

				  dataType: 'json',                //data format
				  success: function(data)          //on recieve of reply
				  {

					if (data['0'] == undefined){

					  $("#pro_sous_sous_famille").find("option:gt(0)").remove();

					}else{
					  $("#pro_sous_sous_famille").find("option:gt(0)").remove();
					  $.each(data, function(key, value) {
						$('#pro_sous_sous_famille')
						.append($("<option></option>")
						  .attr("value",value["pss_id"])
						  .text(value["pss_libelle"]));
					  });
					}
				  }
				});
			}
		</script>
	</body>
</html>
