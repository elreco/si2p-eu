<?php
include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	
	include('modeles/mod_service.php');
	include('modeles/mod_erreur.php');

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">		

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php	
				include "includes/header_def.inc.php";
			?>			
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">	

				
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn">		
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="dark" >
									<th>ID</th>
									<th>Service</th>									
									<th>&nbsp;</th>									
								</tr>
							</thead>
							<tbody>
						
						<?php
								$donnees=get_services();
								if(!empty($donnees)){
									foreach($donnees as $value){ ?>
										<tr>
											<td><?=$value["ser_id"]?></td>
											<td><?=$value["ser_libelle"]?></td>
											<td class="text-center" >
												<span data-toggle="modal" data-target="#formService" >
													<a href="#" class="btn btn-warning btn-xs open-MajService" data-id="<?=$value["ser_id"]?>" data-libelle="<?=$value["ser_libelle"]?>" data-toggle="tooltip" data-placement="bottom" title="Modifier" >
														<i class="fa fa-pencil"></i>
													</a>
												</span>
											</td>									
										</tr>	
						<?php		
									};	
								}else{ ?>
									<tr>
										<td colspan="3" class="text-center" >
											Aucun service n'est enregistré.
										</td>
									</tr>
						<?php
									
								}; ?>
							
								
							</tbody>
						</table>
					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-long-arrow-left"></span>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="#" class="btn btn-success btn-sm open-MajService" data-id="0" data-libelle="" role="button" data-toggle="modal" data-target="#formService" >
						<span class="fa fa-plus"></span>
						Nouveau service
					</a>
				</div>
			</div>
		</footer>
		
		<div id="formService" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<form method="post" action="param_service_enr.php" >
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title" id="titreModal" >Nouveau service</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input type="hidden" name="ser_id" id="ser_id" value="0" />
							</div>
							<div class="form-group">
								<label for="ser_libelle" class="sr-only" >Service</label>
								<div>
									<input type="text" class="form-control" name="ser_libelle" id="ser_libelle" placeholder="Nom du service" required >
								</div>
							</div>			
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	<?php
		include "includes/footer_script.inc.php"; ?>	
		
		<!-- BEGIN: PAGE SCRIPTS -->

		<!-- jQuery JS -->	
		<script type="text/javascript">
			jQuery(document).ready(function(){				
				

				$(document).on("click", ".open-MajService", function () {
					 var ser_id = $(this).data('id');
					 var ser_libelle = $(this).data('libelle');						 
					 $(".modal-body #ser_id").val( ser_id );					  
					 $(".modal-body #ser_libelle").val( ser_libelle );					
					if(ser_id==0){
						$("#titreModal").html('Nouveau service');
					}else{
						$("#titreModal").html('Edition d\'un service');
					};
				});				
			});
			(jQuery);
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
