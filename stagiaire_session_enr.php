<?php
	session_start();
	include("includes/connexion.php");
	include("modeles/mod_parametre.php");
include("modeles/mod_upload.php");
	if(isset($_POST)){

			// regarder si le stagiaire existe déjà
			$req = $Conn->prepare("SELECT * FROM stagiaires WHERE sta_nom LIKE '" . $_POST['sta_nom'] . "' AND sta_prenom LIKE '" . $_POST['sta_prenom'] . "'");

	        $req->execute();

	        $stagiaire = $req->fetch();
	        
	        	$ses_date = convert_date_sql($_POST['ses_date']);
		        // voir si la session existe
		        $req = $Conn->prepare("SELECT * FROM sessions WHERE ses_date = '" . $ses_date . "' AND ses_h_deb = '" . $_POST['ses_h_deb'] . "'");
		        $req->execute();
		        $session = $req->fetch();

		        if(empty($session)){
		        	// insérer la session
			        $req = $Conn->prepare("INSERT INTO sessions (ses_date, ses_h_deb, ses_h_fin) VALUES (:ses_date, :ses_h_deb, :ses_h_fin)");

			        $req->bindParam(':ses_date', $ses_date);
		        	$req->bindParam(':ses_h_deb', $_POST['ses_h_deb']);
		        	$req->bindParam(':ses_h_fin', $_POST['ses_h_fin']);
			        $req->execute();

			        $last_session = $Conn->lastInsertId();
		        }

		    if(empty($stagiaire)){ 

				// enregistrer le stagiaire
				$req = $Conn->prepare("INSERT INTO stagiaires 
					(sta_nom, 
					sta_prenom, 
					sta_naissance, 
					sta_mail,
					sta_ad1, 
					sta_ad2, 
					sta_ad3, 
					sta_cp, 
					sta_ville, 
					sta_tel, 
					sta_ville_naiss, 
					sta_cp_naiss, 
					sta_portable, 
					sta_mail_perso,
					sta_titre 
					) 
					VALUES 
					(:sta_nom, 
					:sta_prenom, 
					:sta_naissance, 
					:sta_mail,
					:sta_ad1, 
					:sta_ad2, 
					:sta_ad3, 
					:sta_cp, 
					:sta_ville, 
					:sta_tel, 
					:sta_ville_naiss, 
					:sta_cp_naiss, 
					:sta_portable, 
					:sta_mail_perso,
					:sta_titre)"
				);
				$sta_naissance = convert_date_sql($_POST['sta_naissance']);

		        $req->bindParam(':sta_nom', $_POST['sta_nom']);
		        $req->bindParam(':sta_prenom', $_POST['sta_prenom']);
		        $req->bindParam(':sta_naissance', $sta_naissance);
		        $req->bindParam(':sta_mail', $_POST['sta_mail']);
		        $req->bindParam(':sta_ad1', $_POST['sta_ad1']);
		  		$req->bindParam(':sta_ad2', $_POST['sta_ad2']);
		  		$req->bindParam(':sta_ad3', $_POST['sta_ad3']);
		        $req->bindParam(':sta_cp', $_POST['sta_cp']);
				$req->bindParam(':sta_ville', $_POST['sta_ville']);
				$req->bindParam(':sta_tel', $_POST['sta_tel']);
				$req->bindParam(':sta_ville_naiss', $_POST['sta_ville_naiss']);
				$req->bindParam(':sta_cp_naiss', $_POST['sta_cp_naiss']);
				$req->bindParam(':sta_portable', $_POST['sta_portable']);
				$req->bindParam(':sta_mail_perso', $_POST['sta_mail_perso']);
				$req->bindParam(':sta_titre', $_POST['sta_titre']);

				$req->execute();
				$last = $Conn->lastInsertId();
	        }
	        // enregistrer le fichier
	        $nom = pathinfo($_FILES['doc']['name']); // nom du document (infos)

		
			// construction du nom du  document
			if(!empty($last_session)){
				$nom_session = $last_session;
			}else{
				$nom_session = $session['ses_id'];
			}

			if(!empty($last)){
				$nom_stagiaire = $last;
				$nom_sta = $_POST['sta_nom'];
				
			}else{
				$nom_stagiaire = $stagiaire['sta_id'];
				$nom_sta = $stagiaire['sta_nom'];
			}

			$nom_fichier = $nom_stagiaire . "_" . $nom_session . "_" . clean($nom_sta);

			// upload $erreur != 0 si erreur 
			
			$erreur = upload('doc', 'sessions_stagiaires/',$nom_fichier, 0, 0, 1);

			$req = $Conn->prepare("INSERT INTO sessions_stagiaires (sst_stagiaire, sst_session, sst_fichier, sst_ext) VALUES (:sst_stagiaire, :sst_session, :sst_fichier, :sst_ext)");
			
				$req->bindParam(':sst_stagiaire', $nom_stagiaire);
				$req->bindParam(':sst_session', $nom_session);
				$req->bindParam(':sst_fichier', $nom_fichier);
				$req->bindParam(':sst_ext', $nom['extension']);
				$req->execute();


	        Header("Location: stagiaire_session_liste.php?succes=1");
	}


?>

?>