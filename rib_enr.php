<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_rib.php');
	include('modeles/mod_upload.php');

	
	$erreur_txt="";
	$warning_txt="";
	
	$rib_id=0;
	if(!empty($_POST["rib_id"])){
		$rib_id=$_POST["rib_id"];
	};
	
	$rib_nom="";
	if(!empty($_POST["rib_nom"])){
		$rib_nom=$_POST["rib_nom"];
	};

	if(!empty($_POST["rib_societe"])){
		$rib_societe=$_POST["rib_societe"];
	};
		
	if(!empty($_POST["rib_iban"])){
		$rib_iban=$_POST["rib_iban"];
	};
	
	if(!empty($_POST["rib_bic"])){
		$rib_bic=$_POST["rib_bic"];
	};
	
	$rib_defaut=0;
	if(!empty($_POST["rib_defaut"])){
		if($_POST["rib_defaut"]=="1"){
			$rib_defaut=1;
		};
	};
	
	$rib_change=0;
	if(!empty($_POST["rib_change"])){
		if($_POST["rib_change"]=="1"){
			$rib_change=1;
		};
	};
	
	$rib_affacturage=0;
	if(!empty($_POST["rib_affacturage"])){
		if($_POST["rib_affacturage"]=="1"){
			$rib_affacturage=1;
		};
	};
	
	$rib_banque=0;
	if(!empty($_POST["rib_banque"])){
		$rib_banque=intval($_POST["rib_banque"]);
	};

	// ENREGISTREMENT DU RIB

	if($rib_id>0){
		
		// MISE A JOUR D'UN RIB
		if(empty($rib_nom) OR empty($rib_iban) OR empty($rib_bic) ){
			$erreur_txt="Formulaire incomplet!";			
		}else{
			
			// donnee du rib avant modif
			$donnee_rib=get_rib($rib_id);				
			$rib_societe=$donnee_rib["0"]["rib_societe"];
			if($donnee_rib["0"]["rib_defaut"]==1){
				// FG le post est vide car checkbox disabled 
				// en maj d'un rib par défaut on conserve forcément le rib defaut
				$rib_defaut=1;
				// securité rib par defaut ne peut pas etre decoché
				//$erreur_txt="Chaque société doit obligatoirement avoir un RIB par défaut!";
			}
		}
		if(empty($erreur_txt)){
			
			// EDITION DU RIB PAR DEFAUT
			if($rib_defaut==1){
				
				// le rib par defaut ne peux pas etre le rib d'affacturage
				$rib_affacturage=0;
				
				// il devient le rib par defaut
				if($donnee_rib["0"]["rib_defaut"]==0){
					// on recupere l'ID de l'ancien rib par défaut et on le passe à 0
					$d_rib_defaut=get_rib_defaut($rib_societe);			
					update_rib_defaut($d_rib_defaut["rib_id"],0);
				}
			}elseif($rib_affacturage==1 AND $donnee_rib["0"]["rib_affacturage"]==0){
			
				// le rib devient rib d'affacturage
				$req = $Conn->prepare("UPDATE rib SET rib_affacturage=0 WHERE rib_societe=:rib_societe AND NOT rib_id=:rib_id;");
				$req->bindParam(':rib_id', $rib_id);
				$req->bindParam(':rib_societe', $rib_societe);
				$req->execute();
			}
			
			update_rib($rib_id,$rib_nom,$rib_iban,$rib_bic,$rib_defaut,$rib_change,$rib_affacturage,$rib_banque);
			
		}
		
	}else{
		
			// AJOUT D'UN RIB
		if($rib_societe>0 AND $rib_nom!="" AND $rib_iban!="" AND $rib_bic!=""){
			
			$rib_par_defaut=get_rib_defaut($rib_societe);			
			
			$rib_defaut_id=$rib_par_defaut["rib_id"];
			
			if($rib_defaut_id==0){
				
				// premier rib pour cette sociéte il est déclaré en defaut
				$rib_defaut=1;
				
			}else if($rib_defaut==1){
				
				// rib par defaut l'ancien est mis a false
				update_rib_defaut($rib_defaut_id,0);
			};
			
			if($rib_defaut==0){
				
				if($rib_affacturage>0){
					
					// le nouveau rib est un rib d'affacturage et efface l'ancien rib d'affacturage
					$req = $Conn->prepare("UPDATE rib SET rib_affacturage=0 WHERE rib_societe=:rib_societe;");
					$req->bindParam(':rib_societe', $rib_societe);
					$req->execute();
				}
				
			}else{
				$rib_affacturage=0;
			}
			
			
			$rib_id=insert_rib($rib_nom,$rib_societe,$rib_iban,$rib_bic,$rib_defaut,$rib_change,$rib_affacturage,$rib_banque);
			if($rib_id==0){
				$erreur_txt="Le RIB n'a pas été enregistré!";	
			};
			
		}else{
			$erreur_txt="Formulaire incomplet!";		
		};	
	};	
	
	// CHARGEMENT DU FICHIER PDF
	
	if(empty($erreur_txt)){
		if (!empty($_FILES['rib_fichier']['name'])) {
			$fichier_nom="rib_" . $rib_id;
			$fichier_ext=array("pdf");
			$erreur = upload("rib_fichier","societes/rib/",$fichier_nom,0,$fichier_ext,0);
			if(!empty($erreur)){
				$warning_txt="Le fichier PDF n'a pas été enregistré.";	
			}
		};
	};
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"aff" => "",
			"titre" => "Erreur !",
			"type" => "danger",
			"message" => $erreur_txt 
		);
	}elseif(!empty($warning_txt)){
		$_SESSION['message'][] = array(
			"aff" => "",
			"titre" => "Attention !",
			"type" => "warning",
			"message" => $warning_txt 
		);
	}
	
	header('Location: rib_liste.php');      
	
?>

