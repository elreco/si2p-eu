<?php 

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_convention_data.php');

include('modeles/mod_get_juridique.php');


// VISUALISATION D'UNE CONVENTION

$erreur_txt="";

$convention_id=0;
if(!empty($_GET["convention"])){
	$convention_id=intval($_GET["convention"]);
}

if(empty($convention_id)){
	$erreur_txt="impossible d'afficher la page";
	
}else{

	$data=convention_data($convention_id);
	if(empty($data)){
		$erreur_txt="impossible d'afficher la page";	
	}
}

	if(empty($erreur_txt)){
	
		// DONNEE POUR MODIFICATION

		// adresse de facturation du client
		
		$sql="SELECT cli_nom,adr_id,adr_libelle,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_geo 
		FROM Clients,Adresses WHERE cli_id=adr_ref_id AND adr_ref=1 AND cli_id=" . $data["con_client"] . "
		AND ( (NOT cli_categorie=3 AND adr_type=1) OR (cli_categorie=3 AND adr_type=2) ) ORDER BY adr_libelle,cli_nom,adr_ville;";
		$req=$Conn->query($sql);
		$d_adresses=$req->fetchAll();
		
		// LIEU D'INTERVENTION
		
		// donnee enregistré dans l'action
		$sql="SELECT act_adr_ref,act_adresse,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville,act_con_nom,act_con_prenom,act_con_tel,act_con_portable,act_adr_ref_id 
		FROM Actions WHERE act_id=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$data["con_action"]);
		$req->execute();
		$action=$req->fetch();
		
		// LES clientS qui participe à la formation
		$sql="SELECT DISTINCT cli_id,cli_code,cli_nom FROM Clients,Actions_Clients WHERE cli_id=acl_client AND acl_action=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$data["con_action"]);
		$req->execute();
		$d_action_client=$req->fetchAll();
		
		// LES UNITE DE FACTURATIONS
		
		$unite_fac=array(
			0 => 1,
			1 => 1,
			2 => 0,
			3 => 0,
			4 => 0,
			5 => count($data["liste_stagiaires"])
		);
		
		// DATE DE FORMATION
		$sql="SELECT COUNT(DISTINCT pda_id) as nb_demi,COUNT(DISTINCT ase_id) as nb_session FROM Plannings_Dates,Actions_Sessions,Actions_Clients_Dates WHERE pda_id=ase_date AND pda_id=acd_date
		AND pda_type=1 AND pda_ref_1=:action AND acd_action_client=:action_client AND not pda_archive;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$data["con_action"]);
		$req->bindParam(":action_client",$data["con_action_client"]);
		$req->execute();
		$d_dates=$req->fetch();
		if(!empty($d_dates)){
			$unite_fac[2]=$d_dates["nb_demi"]/2;
			$unite_fac[3]=$d_dates["nb_demi"];
			$unite_fac[4]=$d_dates["nb_session"];
		}
	}
	/*echo("<pre>");
		print_r($data);
	echo("<pre>");
	die();*/
 ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title><?=$data["con_numero"]?></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >



		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		
		<style type="text/css" >
			#container_print{
				width:21cm;
			}
			header table td{
				padding-bottom:30px;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm">
	
		<div id="zone_print" ></div>
		
	
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">	
		<?php 		if(empty($erreur_txt)){ ?>
		
							<div class="row" >
							
								<!-- zone d'affichage du devis -->
								
								<div class="col-md-8"  >	
								
									<div id="container_print" >
								
										<div id="page_print" >

											<div class="page" id="header_0" data-page="0" >
												<header>							
													<table>
														<tr>
															<td class="w-25" >
													<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
																	<img src="<?=$data["juridique"]["logo_1"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td>&nbsp;</td>
															<td class="w-25 text-right" >
													<?php		if(!empty($data["juridique"]["logo_2"])){ ?>
																	<img src="<?=$data["juridique"]["logo_2"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
														</tr>													
													</table>
												</header>
											</div>
											
											<div class="ligne-body-0" >
												<h1 class="text-center" >CONVENTION DE FORMATION PROFESSIONNELLE</h1>
												<h2 class="text-center" ><?=$data["con_numero"]?></h2>
											</div>
											
											<div class="ligne-body-0" >
												<table>
													<tr>
														<td class="text-right w-33 tab-label" ><strong>Entre les soussignés :</strong></td>
														<td class="w-67" >													
													<?php	echo("<b>" . $data["juridique"]["nom"] . "</b>");
															if(!empty($data["juridique"]["agence"])){
																echo("<br/><b>" . $data["juridique"]["agence"] . "</b>");
															}
															if(!empty($data["juridique"]["ad1"])){
																echo("<br/><b>" . $data["juridique"]["ad1"] . "</b>");
															}
															if(!empty($data["juridique"]["ad2"])){
																echo("<br/><b>" . $data["juridique"]["ad2"] . "</b>");
															}
															if(!empty($data["juridique"]["ad3"])){
																echo("<br/><b>" . $data["juridique"]["ad3"] . "</b>");
															}
															echo("<br/><b>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"] . "</b>");
															echo("<br/>SIRET " . $data["juridique"]["siret"]);
															echo("<br/>APE " . $data["juridique"]["ape"]);
															/*echo("<pre>");
																print_r($data["juridique"]);
															echo("</pre>");*/ ?>
														</td>
													</td>
												</table>
											</div>
											
											<div class="ligne-body-0" >
												<p>
													N° de déclaration d’existence <?=$data["juridique"]["num_existence"]?> enregistré auprès de la Préfecture de la Région <?=$data["juridique"]["region_nom"]?>. Ci-après désigné <b>« l’Organisme de Formation »</b>
												</p>
											</div>
											
											<div class="ligne-body-0" >
												<table>
													<tr>
														<td class="w-33 text-right tab-label" >
															<button class="btn btn-xs btn-warning pull-left" id="btn_edit_adr" >
																<i class="fa fa-pencil" ></i>
															</button>
															<strong>Et :</strong>
														</td>
														<td class="w-67 text-strong" id="con_adresse_txt" ><?=$data["adresse_client"]?></td>
													</tr>
												</table>
											</div>
											
											<div class="ligne-body-0" >
												<p>
													Ci-après désigné <b>« l’Entreprise »</b>
												</p>
											</div>											
											
											<div class="ligne-body-0" >
												<p>
													est conclue la convention suivante, en application des dispositions de la sixième partie du Code du Travail portant organisation de la formation professionnelle tout au long de la vie. 
												</p>
											</div>										
											
											<div class="ligne-body-0" >
												<strong>ARTICLE 1 : Objet de la Convention</strong>
												<p>En exécution de la présente convention, l’organisme de formation <?= $data["juridique"]["nom"] ?> s’engage à organiser l’action de formation suivante : </p>
											</div>
											
											<div class="ligne-body-0" >
												<table>
													<tr>
														<td class="w-33 text-right tab-label" >
															<button class="btn btn-xs btn-warning pull-left" id="btn_edit_lib" >
																<i class="fa fa-pencil" ></i>
															</button>
															<strong>Intitulé du stage :</strong>
														</td>																								
														<td class="w-67 text-strong" id="con_libelle_txt" ><?=$data["libelle"]?></td>
													</tr>
												</table>
											</div>
											
											<div class="ligne-body-0" >
												<p>
													Le programme, les objectifs, les méthodes et moyens pédagogiques sont annexés à la présente convention. 
												</p>
											</div>
											
											<div class="ligne-body-0" >
												<table>
													<tr>
														<td class="w-33 text-right tab-label" >
															<button class="btn btn-xs btn-warning pull-left" id="btn_edit_dates" >
																<i class="fa fa-pencil" ></i>
															</button>
															<strong>Date(s) :</strong>
														</td>
														<td class="w-67 text-strong con_form_dates" id="con_form_dates" >
													<?php	if(!empty($data["con_form_dates"])){
																echo(str_replace("\r\n","<br>",$data["con_form_dates"]));
															} ?>
														</td>
													</tr>
												</table>
											</div>
											
											<div class="ligne-body-0" >
												<table>
													<tr>
														<td class="w-33 text-right" ><strong>Durée :</strong></td>
														<td class="w-67 text-strong" id="con_form_duree" ><?=$data["con_form_duree"]?></td>
													</tr>
												</table>
											</div>
											
											<div class="ligne-body-0" >
												<table>
													<tr>
														<td class="w-33 text-right tab-label" >
															<button class="btn btn-xs btn-warning pull-left" id="btn_edit_lieu" >
																<i class="fa fa-pencil" ></i>
															</button>
															<strong>Lieu(x) de formation :</strong>
														</td>
														<td class="w-67 text-strong" id="con_lieu" ><?=$data["lieu"]?></td>
													</tr>
												</table>
											</div>
											
											<div class="ligne-body-0" >
												<table>
													<tr>
														<td class="w-33 text-right tab-label" >
															<button class="btn btn-xs btn-warning pull-left" id="btn_edit_eff" >
																<i class="fa fa-pencil" ></i>
															</button>
															<strong>Effectif formé :</strong>													
														</td>
														<td class="w-67 text-strong" id="con_form_effectif_text" >												
													<?php	if($data["con_form_effectif"]==1){
																echo("cf. annexe");
															}elseif($data["con_form_effectif"]==3){
																echo("cf. feuille de présence");
															}else{
																echo(str_replace("\r\n","<br>",$data["con_form_effectif_text"]));
															} ?>
														</td>
													</tr>
												</table>
											</div>
											
											<div class="ligne-body-0" >
												<p>
													<b>Modalités de suivi :</b> fiches de présence émargées  
												</p>
											</div>
											
											<div class="ligne-body-0" >
												<p>
													<b>Appréciation des résultats :</b> évaluation en fin de stage qui permet au formateur de déterminer si les stagiaires ont acquis les connaissances et les gestes professionnels, dont la maîtrise constitue l'objectif initial de l'action de formation. 
												</p>
											</div>
											
											<div class="ligne-body-0" >
												<strong>ARTICLE 2 : Dispositions financières</strong>
												<p>
													a) Le client, en contrepartie des actions de formation réalisées, s’engage à verser à l’organisme de formation, une somme correspondant :<br/>
													• aux frais de formation :
												</p>
											</div>											
											
											<div class="ligne-body-0" >
												<table class="tab-border" >
													<tr>
														<th>
															<button class="btn btn-xs btn-warning pull-left" id="btn_edit_fac" >
																<i class="fa fa-pencil" ></i>
															</button>
														</th>
														<th>Dates</th>
														<th>Quantité</th>
														<th>€uros</th>
													</tr>
													<tr>
														<td>&nbsp;</th>
														<td class="text-strong con_form_dates" >
													<?php	if(!empty($data["con_form_dates"])){
																echo(str_replace("\r\n","<br>",$data["con_form_dates"]));
															} ?>
														</td>
														<td id="con_qte" ><?=$data["con_qte"] . " " . $data["con_unite_txt"]?></td>
														<td class="con_ht text-right" ><?=$data["con_ht"]?> €</td>
													</tr>
													<tr>
														<td class="text-right" >Coût Total € HT :</th>
														<td colspan="2" >&nbsp;</td>
														<td class="con_ht text-right" ><?=$data["con_ht"]?> €</td>														
													</tr>											
													<tr id="bloc_tva" <?php	if($data["con_tva_id"]==3) echo("style='display:none;'"); ?> >												
														<td class="text-right" id="con_tva_taux" >TVA à <?=$data["con_tva_taux"]?>% :</th>
														<td colspan="2" >&nbsp;</td>
														<td id="con_tva" class="text-right" ><?=$data["con_tva"]?> €</td>														
													</tr>										
													<tr>
														<td class="text-right" >Coût TTC :</th>
														<td colspan="2" >&nbsp;</td>
														<td id="con_ttc" class="text-right" ><?=$data["con_ttc"]?> €</td>														
													</tr>
												</table>
											</div>
											
											<div class="ligne-body-0" id="con_comment" >
										<?php	if(!empty($data["con_comment"])){
													echo(str_replace("\r\n","<br>",$data["con_comment"]));
												} ?>
											</div>
											
											<div class="ligne-body-0" >
												<p>
													L’organisme de formation, en contrepartie des sommes reçues, s’engage à réaliser toutes les actions prévues dans le cadre de la présente convention ainsi qu’à fournir tout document et pièce de nature à justifier la réalité et la validité des dépenses de formation engagées à ce titre. 
												</p>
											</div>
											
											<div class="ligne-body-0" >
												<p>
													b) Modalités de règlement : La facture devra être réglée à 30 jours par chèque ou virement. Aucun escompte ne sera accordé quelle que soit la date de règlement. Les intérêts de retard seront calculés sur la base de 1,5 fois le taux d'intérêt légal. 
												</p>
											</div>
											
											<div class="ligne-body-0" >
												<strong>ARTICLE 3 : Dédit ou abandon</strong>
												<p>
													<!--
														a) En cas de résiliation de la présente convention par le client à moins de 10 jours francs avant le début d’une des actions mentionnées, l’organisme retiendra sur le coût total un pourcentage de 50 %, au titre de dédommagement. Toute annulation sur place ou le jour de la formation fera l'objet d'une facturation équivalente à la totalité du montant de la prestation. 
													-->
													a) En cas de résiliation de la présente convention par le client à moins de 10 jours ouvrés avant le début d’une des actions mentionnées, l’organisme se réserve le droit de réclamer une indemnité égale :													
													<ul>
														<li>
															à 50% du montant de la formation, si la demande d’annulation parvient entre le 9ème et le 3ème jour ouvrés avec le début de
													la session de Formation ;
														</li>
														<li>
															à 100% du montant de la formation, si la demande d’annulation parvient moins de 3 jours ouvrés avec le début de la session
													de formation
														</li>
													</ul>
													En cas d’absence à la formation, de retard, de participation partielle, d’abandon ou de cessation anticipée pour tout autre raison
													que la force majeure dûment reconnue, le client sera redevable de l’intégralité du montant de la formation.

												</p>
											</div>
											
											<!--<div class="ligne-body-0" >
												<p>
													b) En cas de réalisation partielle de l’action du fait du client, seule sera facturée au client la partie effectivement réalisée de l’action, selon le prorata suivant : nombre de stagiaires présents/nombre de stagiaires prévus, ou nombre d’heures réalisées / nombre d’heures prévues. En outre, l’organisme retiendra sur le coût correspondant à la partie non-réalisée de l’action un pourcentage de 50 %, ou la totalité du montant pour toute annulation sur place ou le jour de la formation, au titre de dédommagement. 
												</p>
											</div>-->
											
											<div class="ligne-body-0" >
												<p>
													b) Les montants versés par le client au titre de dédommagement ne pourront pas être imputés par le client sur son obligation définie à l’article L6331-1 du Code du Travail ni faire l’objet d’une demande de remboursement ou de prise en charge par un OPCA. 
												</p>
											</div>
											
											<div class="ligne-body-0" >
												<p>
													c) Dans le cas ou <?= $data["juridique"]["nom"] ?> serait amené à annuler l'action objet de la présente convention, cette dernière serait considérée comme caduque. L'entreprise sera avertie dans les meilleurs délais par lettre recommandée. Les sommes éventuellement perçues lui seront intégralement reversées. 
												</p>
											</div>
											
											<div class="ligne-body-0" >
												<strong>ARTICLE 4 : Date d’effet et durée de la convention</strong>
												<p>
													La présente convention prend effet à compter de sa signature par le client, pour s’achever au terme de l'action de formation visé à l'article 1 de la présente convention. 
												</p>
											</div>
											
											<div class="ligne-body-0" >
												<strong>ARTICLE 5 : Différends éventuels</strong>
												<p>
													Si une contestation ou un différend ne peuvent être réglés à l’amiable, le Tribunal d'Angers sera seul compétent pour se prononcer sur le litige. 
												</p>
											</div>
											
											<div class="ligne-body-0" >
												<strong>ARTICLE 6 : Données personnelles et confidentialité</strong>
												<p>
													L’Organisme de Formation gère toutes les données personnelles reçues ou collectées dans le cadre de la Convention (collectivement les «Données personnelles») conformément aux lois et règlements applicables en matière de protection des données personnelles. En particulier, l’Organisme de Formation veillera à ce que des mesures de sécurité techniques et organisationnelles adéquates soient mises en œuvre et maintenues pour protéger les Données personnelles contre toute destruction ou perte accidentelle ou illégale, toute altération, divulgation ou accès non-autorisé, et toute autres formes illégales de traitement. Toutes les données et informations divulguées par l’Entreprise et acquises ou traitées par l’Organisme de Formation, dans le cadre de la Convention doivent être traitées comme strictement confidentielles. 
												</p>
											</div>
											
											<div id="foot_0" class="foot" >									
												
												<p>Fait en double exemplaire, à <?=$data["juridique"]["ville"]?>, le <span class="text-strong" ><?=$data["con_date"]?></span></p>	
												
												<table class="table" >
													<tr>
														<td class="w-50 tab-label" >
															Pour le client,
															(nom et qualité du signataire) 
														</td>
														<td class="w-50" style="padding-bottom:100px;" >
																Pour <?=$data["juridique"]["nom"]?>,
																(nom et qualité du signataire)
														<?php	if(!empty($data["juridique"]["resp_ident"])){
																	echo("<br/>" . $data["juridique"]["resp_ident"]);
																}
																if(!empty($data["juridique"]["resp_fonction"])){
																	echo("<br/>" . $data["juridique"]["resp_fonction"]);
																}
																
																if(!empty($data["juridique"]["resp_signature_tampon"])){ ?>
																	<img src="<?=$data["juridique"]["resp_signature_tampon"]?>" class="img-responsive" />
																	<img src="<?=$data["juridique"]["resp_signature"]?>" class="img-responsive" style="margin-top:-100px;"/>
														<?php	}elseif(!empty($data["juridique"]["resp_signature"])){ ?>
																	<img src="<?=$data["juridique"]["resp_signature"]?>" class="img-responsive" />
														<?php	}?>
														</td>
													</tr>
												</table>
											</div>
											
											<div id="footer_0" >
												<footer>
													<table>
														<tr>
															<td class="w-10"  >
													<?php		if(!empty($data["juridique"]["qualite_1"])){ ?>
																	<img src="<?=$data["juridique"]["qualite_1"]?>" class="img-responsive" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td class="text-center" >
														<?php	echo($data["juridique"]["nom"] . " - ");
																if(!empty($data["juridique"]["ad1"])){
																	echo($data["juridique"]["ad1"] . " - ");
																}
																if(!empty($data["juridique"]["ad2"])){
																	echo($data["juridique"]["ad2"] . " - ");
																}
																if(!empty($data["juridique"]["ad3"])){
																	echo($data["juridique"]["ad3"] . " - ");
																}
																echo($data["juridique"]["cp"] . " " . $data["juridique"]["ville"]);
																
																if(!empty($data["juridique"]["tel"])){
																	echo(" - TEL. " . $data["juridique"]["tel"]);
																}
																if(!empty($data["juridique"]["fax"])){
																	echo(" - FAX. " . $data["juridique"]["fax"]);
																}
																echo("<br/>");
																echo($data["juridique"]["footer"]);
																?>
															</td>
															<td class="w-10"  >
													<?php		if(!empty($data["juridique"]["qualite_2"])){ ?>
																	<img src="<?=$data["juridique"]["qualite_2"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
														</tr>
													</table>
												</footer>
											</div>
								
								
											<!--ANNEXE -->
											<div class="annexe" data-annexe="1" id="annexe_1" style="page-break-before:always;<?php if($data["con_form_effectif"]!=1) echo("display:none;"); ?>" >
												<header id="h_annexe_1" >							
													<table>
														<tr>
															<td class="print-logo" >
													<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
																	<img src="<?=$data["juridique"]["logo_1"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td class="print-logo text-right" >
													<?php		if(!empty($data["juridique"]["logo_2"])){ ?>
																	<img src="<?=$data["juridique"]["logo_2"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
														</tr>													
													</table>
												</header>
												<section id="s_annexe_1" >
													<h1>CONVENTION DE FORMATION PROFESSIONNELLE</h1>
													<h2>Annexe : Effectif formé</h2>
													
													<p id="annexe_stagiaire" >
												<?php	$html_sta="";
														if(!empty($data["liste_stagiaires"])){															
															echo("<ul>");
																foreach($data["liste_stagiaires"] as $sta){
																	echo("<li>" . $sta["sta_nom"] . " " . $sta["sta_prenom"] . "</li>");
																	$html_sta.=$sta["sta_nom"] . " " . $sta["sta_prenom"] . "<br/>";
																}
															echo("</ul>");	
														} ?>													
													</p>
												</section>												
												<footer id="f_annexe_1" >
													<table>
														<tr>
															<td class="print-logo" >
													<?php		if(!empty($data["juridique"]["qualite_1"])){ ?>
																	<img src="<?=$data["juridique"]["qualite_1"]?>" class="img-responsive" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td class="text-center" ><?=$data["juridique"]["footer"]?></td>
															<td class="text-right print-logo" >
													<?php		if(!empty($data["juridique"]["qualite_2"])){ ?>
																	<img src="<?=$data["juridique"]["qualite_2"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
														</tr>
													</table>
												</footer>												
											</div>
								
										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->
								
								<div class="col-md-4" >
									<!-- DEBUT ANNEXE -->
									
									<!-- FIN ANNEXE -->
								</div>
								
							</div>
						

		<?php		}else{ ?>
						
						<p class="alert alert-danger text-center" >
							<?=$erreur_txt?>
						</p>
					
		<?php		} ?>
									
				</section>
				
				<!-- End: Content -->
			</section>
			
			
		</div>
	
		<!-- End: Main -->
		
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
			<?php	if(!empty($data["con_action"])){ ?>	
						<a href="action_voir.php?action=<?=$data["con_action"]?>&onglet=3&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
							Retour
						</a>	
			<?php	}else{ ?>
						<a href="accueil.php" class="btn btn-default btn-sm" >
							Retour
						</a>
			<?php	} ?>		
				</div>
				<div class="col-xs-6 footer-middle text-center" >
		<?php		if(empty($erreur_txt)){ ?>
						<button type="button" class="btn btn-sm btn-info ml15" id="print" >
							<i class="fa fa-print"></i> Imprimer
						</button>
		<?php		} ?>
				</div>
				<div class="col-xs-3 footer-right" ></div>
			</div>
		</footer>
<?php	if(empty($erreur_txt)){	?>
			<!-- MODAL -->
			
			<!-- MODAL ADRESSE -->
			<div id="modal_ad_client" class="modal fade" role="dialog" >
				<div class="modal-dialog">
					<div class="modal-content">
						<form method="post" action="ajax/ajax_convention_adresse_set.php" id="form_ad_client" >
							<div>
								<input type="hidden" name="convention" value="<?=$convention_id?>" />
								<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
							</div>				
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title">Adresse du client</h4>
							</div>
							<div class="modal-body">

								<div class="admin-form theme-primary ">
								
									<div class="row" >
										<div class="col-md-12" >							
											<div class="select2-sm" >										
												<select name="con_cli_adresse" id="mod_cli_adresse" class="select2" required >
													<option value="" >Adresse ...</option>
											<?php	if(!empty($d_adresses)){
														foreach($d_adresses as $d_adresse){
															if(!empty($d_adresse["adr_libelle"])){
																$lib=$d_adresse["adr_libelle"];
															}else{
																$lib=$d_adresse["cli_nom"] . " " . $d_adresse["adr_ville"];
															}
															
															if($d_adresse["adr_id"]==$data["con_cli_adresse"]){
																echo("<option value='" . $d_adresse["adr_id"] . "' selected >" . $lib . "</option>");
															}else{
																echo("<option value='" . $d_adresse["adr_id"] . "' >" . $lib . "</option>");	
															}
														}
													} ?>
												</select>
												<small class="text-danger texte_required" id="mod_cli_adresse_required">
													Merci de selectionner une adresse
												</small>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="section-divider mb40">
												<span>Adresse du client</span>
											</div>
										</div>
									</div>

									<div class="row" >
										<div class="col-md-12" id="mod_adresse_txt" >
												
										</div>
									</div>
									
									
								</div>

							</div>
							<div class="modal-footer">						
								<button type="button" class="btn btn-success btn-sm" id="btn_ad_client" >
									<i class="fa fa-save" ></i> Enregistrer
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<!-- MODAL INTITULE FORMATION -->
			<div id="modal_form_lib" class="modal fade" role="dialog" >
				<div class="modal-dialog">
					<div class="modal-content">
					
						<form method="post" action="ajax/ajax_action_client_chp.php" id="form_form_lib" >
							<div>
								<input type="hidden" name="acl_id" value="<?=$data["con_action_client"]?>" />
								<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
							</div>				
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title">Intitulé de la formation</h4>
							</div>
							<div class="modal-body">

								<div class="admin-form theme-primary ">
								
									<div class="row" >
										<div class="col-md-12" >
											<input type="text" name="acl_pro_libelle" class="gui-input" id="mod_libelle_txt" placeholder="Intitulé de la formation" required />	
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">						
								<button type="button" class="btn btn-success btn-sm" id="btn_form_lib" >
									<i class="fa fa-save" ></i> Enregistrer
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<!-- MODAL DATE -->
			<div id="modal_form_dates" class="modal fade" role="dialog" >
				<div class="modal-dialog">
					<div class="modal-content">
					
						<form method="post" action="ajax/ajax_convention_chp.php" id="form_form_dates" >
							<div>
								<input type="hidden" name="con_id" value="<?=$convention_id?>" />
								<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
							</div>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title">Planning</h4>
							</div>
							<div class="modal-body">

								<div class="admin-form theme-primary ">
									<div class="row" >
										<div class="col-md-12" >
											<label for="mod_form_dates" >Dates :</label>
											<textarea class="gui-textarea" id="mod_form_dates" name="con_form_dates" ></textarea>
										</div>
									</div>
									<div class="row mt10" >
										<div class="col-md-12" >
											<label for="mod_form_dates" >Durée :</label>
											<input type="text" name="con_form_duree" class="gui-input" id="mod_form_duree" placeholder="durée de la formation" required />	
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">						
								<button type="button" class="btn btn-success btn-sm" id="btn_form_dates" >
									<i class="fa fa-save" ></i> Enregistrer
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<!-- MODAL LIEU D'INTERVENTION -->
			<div id="modal_form_lieu" class="modal fade" role="dialog" >
				<div class="modal-dialog">
					<div class="modal-content">			
						<form method="post" action="ajax/ajax_action_adresse_set.php" id="form_form_lieu" >
							<div>
								<input type="hidden" name="action" value="<?=$data["con_action"]?>" />
								<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
							</div>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title">Lieu d'intervention</h4>
							</div>
							<div class="modal-body">
								<div class="admin-form theme-primary ">
								
									<div class="row" >
										<div class="col-md-4" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr-ref" name="adr_ref" value="1" id="adr_client" <?php if($action["act_adr_ref"]==1) echo("checked");?> />
													<span class="radio"></span>Client
												</label>
											</div>
										</div>
										<div class="col-md-4" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr-ref" name="adr_ref" value="2" id="adr_si2p" <?php if($action["act_adr_ref"]==2) echo("checked");?> />
													<span class="radio"></span>Agence 
												</label>
											</div>
										</div>
										<div class="col-md-4" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr-ref" name="adr_ref" value="3" id="adr_autre" <?php if($action["act_adr_ref"]==3) echo("checked");?> />
													<span class="radio"></span>Autres 
												</label>
											</div>
										</div>
									</div>	
									
									<div class="row cont-adr-client" <?php if($action["act_adr_ref"]!=1) echo("style='display:none;'"); ?> >
										<div class="col-md-12" >	
											<div class="section" >
												<label for="client" >Client :</label>
												<select name="client" id="client" class="select2 get-client" data-adr_type="1"  data-adr_contact="0" >
													<option value="0" >Clients ... </option>
										<?php		if(!empty($d_action_client)){
														foreach($d_action_client as $cli){																						
															if($action["act_adr_ref_id"]==$cli["cli_id"]){													
																echo("<option value='" . $cli["cli_id"] . "' selected >" . $cli["cli_code"] . "-" . $cli["cli_nom"] . "</option>");
															}else{
																echo("<option value='" . $cli["cli_id"] . "' >" . $cli["cli_code"] . "-" . $cli["cli_nom"] . "</option>");
															}
														} 
													} ?>	
												</select>
												<small class="text-danger texte_required" id="client_required">
													Merci de selectionner un client
												</small>

											</div>
										</div>
									</div>
									<div class="row cont-adr-client" <?php if($action["act_adr_ref"]!=1) echo("style='display:none;'"); ?> >
										<div class="col-md-12" >	
											<div class="section" >
												<label for="adresse" >Adresse :</label>
												<select name="adresse" id="adresse" class="select2" >
													<option value="0" >Selectionnez une adresse</option>
										<?php		if(!empty($d_adresses)){
														foreach($d_adresses as $ad){		
															$adr_nom="";
															if(!empty($ad["adr_nom"])){
																$adr_nom=$ad["adr_nom"];
															}else{
																$adr_nom=$ad["cli_nom"];
															}
															$adr_nom.=$ad["adr_cp"] . " " . $ad["adr_ville"];
															if($action["act_adresse"]==$ad["adr_id"]){													
																echo("<option value='" . $ad["adr_id"] . "' selected >" . $adr_nom . "</option>");
															}else{
																echo("<option value='" . $ad["adr_id"] . "' >" . $adr_nom . "</option>");
															}
														} 
													}?>	
												</select>
												<small class="text-danger texte_required" id="adresse_required">
													Merci de selectionner une adresse
												</small>
											</div>
										</div>
									</div>
									<div class="cont-adresse" <?php if($action["act_adr_ref"]==2) echo("style='display:none;'"); ?> >
										<div class="row">
											<div class="col-md-12" >	
												<input type="text" name="adr_nom" id="adr_nom" class="gui-input nom champ-adresse adr-nom" placeholder="Nom" value="<?=$action["act_adr_nom"]?>" />	
												<input type="text" name="adr_service" id="adr_service" class="gui-input champ-adresse adr-service" placeholder="Service" value="<?=$action["act_adr_service"]?>" />		
												<input type="text" name="adr1" id="adr1" class="gui-input champ-adresse adr1" placeholder="Adresse" value="<?=$action["act_adr1"]?>" />		
												<input type="text" name="adr2" id="adr2" class="gui-input champ-adresse adr2" placeholder="Complément 1" value="<?=$action["act_adr2"]?>" />		
												<input type="text" name="adr3" id="adr3" class="gui-input champ-adresse adr3" placeholder="Complément 2" value="<?=$action["act_adr3"]?>" />		
											</div>
										</div>
										<div class="row">
											<div class="col-md-3" >	
												<input type="text" name="adr_cp" id="adr_cp" class="gui-input code-postal champ-adresse adr-cp" placeholder="CP" value="<?=$action["act_adr_cp"]?>" />		
											</div>
											<div class="col-md-9" >	
												<input type="text" name="adr_ville" id="adr_ville" class="gui-input nom champ-adresse adr-ville" placeholder="Ville" value="<?=$action["act_adr_ville"]?>" />		
											</div>										
										</div>
										<div class="row">
											<div class="col-md-3" >	
												<small class="text-danger texte_required" id="adr_cp_required">
													Merci d'indiquer le CP
												</small>
											</div>
											<div class="col-md-9" >	
												<small class="text-danger texte_required" id="adr_ville_required">
													Merci d'indiquer la ville
												</small>
											</div>
										</div>
									</div>

									<!-- CONTACT -->
									<div class="cont-contact"  <?php if($action["act_adr_ref"]==2) echo("style='display:none;'"); ?> >
										<div class="section-divider" >
											<span>Contact</span>
										</div>								
										<div class="row cont-contact-liste" >
											<div class="col-md-8" >	
												<div class="section" >
													<label for="contact" >Contact :</label>
													<select name="contact" id="contact" class="select2 contact" >
														<option value="0" >Selectionnez un contact client</option>
											<?php		if(!empty($d_contacts)){
															foreach($d_contacts as $c){
																if($action["act_contact"]==$c["con_id"]){													
																	echo("<option value='" . $c["con_id"] . "' selected >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
																}else{
																	echo("<option value='" . $c["con_id"] . "' >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
																}
															} 
														}?>																					
													</select>
												</div>
											</div>
											
										</div>								
										<div class="row">
											<div class="col-md-6" >	
												<input type="text" name="con_nom" id="con_nom" class="gui-input nom champ-contact" placeholder="Nom" value="<?=$action["act_con_nom"]?>" />	
											</div>
											<div class="col-md-6" >	
												<input type="text" name="con_prenom" id="con_prenom" class="gui-input prenom champ-contact" placeholder="Prénom" value="<?=$action["act_con_prenom"]?>" />	
											</div>
										</div>
										<div class="row">
											<div class="col-md-6" >	
												<input type="text" name="con_tel" id="con_tel" class="gui-input telephone champ-contact" placeholder="Téléphone" value="<?=$action["act_con_tel"]?>" />		
											</div>
											<div class="col-md-6" >	
												<input type="text" name="con_portable" id="con_portable" class="gui-input telephone champ-contact" placeholder="Portable" value="<?=$action["act_con_portable"]?>" />		
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">						
								<button type="button" class="btn btn-success btn-sm" id="btn_form_lieu" >
									<i class="fa fa-save" ></i> Enregistrer
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<!-- MODAL EFFECTIF -->
			<div id="modal_form_eff" class="modal fade" role="dialog" >
				<div class="modal-dialog">
					<div class="modal-content">
					
						<form method="post" action="ajax/ajax_convention_chp.php" id="form_form_eff" >
							<div>
								<input type="hidden" name="con_id" value="<?=$convention_id?>" />
								<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
							</div>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title">Effectif</h4>
							</div>
							<div class="modal-body">

								<div class="admin-form theme-primary ">
									
									<div class="row" >
										<div class="col-md-12" >
											<label for="mod_form_effectif" >Effectifs :</label>
											<span class="field select">
												<select id="mod_form_effectif" name="con_form_effectif" >
													<option value="0" >Saisie libre</option>
													<option value="1" <?php if($data["con_form_effectif"]==1) echo("selected"); ?> >Afficher la liste des stagiaires (annexe)</option>
													<option value="2" <?php if($data["con_form_effectif"]==2) echo("selected"); ?> >Afficher la liste des stagiaires (zone de saisie)</option>
													<option value="3" <?php if($data["con_form_effectif"]==3) echo("selected"); ?> >cf. feuille de présence</option>
												</select>
												<i class="arrow simple"></i>
											</span>
										</div>
									</div>
									
									<div class="row mt10" id="bloc_effectif_text" <?php if($data["con_form_effectif"]==1 OR $data["con_form_effectif"]==3) echo("style='display:none;'"); ?>>
										<div class="col-md-12" >
											<textarea class="gui-textarea" id="mod_form_effectif_text" name="con_form_effectif_text" ><?=$data["con_form_effectif_text"]?></textarea>
										</div>
									</div>
									
								</div>
							</div>
							<div class="modal-footer">						
								<button type="button" class="btn btn-success btn-sm" id="btn_form_eff" >
									<i class="fa fa-save" ></i> Enregistrer
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<!-- MODAL FACTURATION -->
			<div id="modal_fac" class="modal fade" role="dialog" >
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
					
						<form method="post" action="ajax/ajax_convention_fac_set.php" id="form_fac" >
							<div>
								<input type="hidden" name="convention" value="<?=$convention_id?>" />
								<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
							</div>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title">Facturation</h4>
							</div>
							<div class="modal-body">

								<div class="admin-form theme-primary ">
								
									<div class="row" >
										<div class="col-md-3" >
											<label for="mod_unite" >Unité de facturation</label>
											<span class="field select">
												<select id="mod_unite" name="con_unite" >
													<option value="0"></option>
													<option value="1" <?php if($data["con_unite"]==1) echo("selected"); ?> >par formation</option>
													<option value="2" <?php if($data["con_unite"]==2) echo("selected"); ?> >par jour</option>
													<option value="3" <?php if($data["con_unite"]==3) echo("selected"); ?> >par demi-journée</option>
													<option value="4" <?php if($data["con_unite"]==4) echo("selected"); ?> >par session</option>
													<option value="5" <?php if($data["con_unite"]==5) echo("selected"); ?> >par stagiaire</option>
												</select>
												<i class="arrow simple"></i>
											</span>
										</div>						
										<div class="col-md-3" >
											<label for="mod_qte" >Quantité</label>
											<input type="text" name="con_qte" class="gui-input" id="mod_qte" placeholder="Quantité" value="<?=$data["con_qte"]?>" />
										</div>
										<div class="col-md-3" >		
											<label for="mod_unite_txt" >&nbsp;</label>
											<input type="text" name="con_unite_txt" class="gui-input" id="mod_unite_txt" placeholder="" value="<?=$data["con_unite_txt"]?>" />
										</div>
										<div class="col-md-3" >	
											<label for="mod_qte" >Montant HT</label>
											<input type="text" name="con_ht" class="gui-input text-right br-n" id="mod_ht" placeholder="HT" value="<?=$data["con_ht"]?> €" readonly  />
										</div>
									</div>
									<div class="row mt15" >
										<div class="col-md-12" >
											<label for="mod_qte" >Commentaire</label>
											<textarea class="gui-textarea" id="mod_comment" name="con_comment" ><?=$data["con_comment"]?></textarea>
										</div>
									</div>
									
									
								</div>
							</div>
							<div class="modal-footer">						
								<button type="button" class="btn btn-success btn-sm" id="btn_fac" >
									<i class="fa fa-save" ></i> Enregistrer
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			
<?php	}
		include "includes/footer_script.inc.php"; ?>	
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script type="text/javascript" src="assets/js/custom.js"></script>
		<script type="text/javascript" src="assets/js/print.js"></script>	
<?php	if(empty($erreur_txt)){	?>		
			<script type="text/javascript">
			
				var l_page=21;
				var h_page=28;

				var cli_adresse=0;
				
				var tab_adresse=new Array();
				tab_adresse[0]=new Array();
				tab_adresse[0]["texte"]="";	
		<?php	foreach($d_adresses as $d_adresse){ ?>
					tab_adresse[<?=$d_adresse["adr_id"]?>]=new Array();
		
		<?php		$ad_txt="";
					if(!empty($d_adresse["adr_nom"])){
						$ad_txt=$d_adresse["adr_nom"];
					}else{
						$ad_txt=$d_adresse["cli_nom"];
					} ?>
					tab_adresse[<?=$d_adresse["adr_id"]?>]["nom"]="<?=$ad_txt?>";
					
		<?php		if(!empty($d_adresse["adr_service"])){
						$ad_txt.="<br/>" . $d_adresse["adr_service"];
					}
					if(!empty($d_adresse["adr_ad1"])){
						$ad_txt.="<br/>" . $d_adresse["adr_ad1"];
					}
					if(!empty($d_adresse["adr_ad2"])){
						$ad_txt.="<br/>" . $d_adresse["adr_ad2"];
					}
					if(!empty($d_adresse["adr_ad3"])){
						$ad_txt.="<br/>" . $d_adresse["adr_ad3"];
					}
					$ad_txt.="<br/>" . $d_adresse["adr_cp"] . " " . $d_adresse["adr_ville"]; ?>
					
					tab_adresse[<?=$d_adresse["adr_id"]?>]["texte"]="<?=$ad_txt?>";
					tab_adresse[<?=$d_adresse["adr_id"]?>]["service"]="<?=$d_adresse["adr_service"]?>";	
					tab_adresse[<?=$d_adresse["adr_id"]?>]["ad1"]="<?=$d_adresse["adr_ad1"]?>";	
					tab_adresse[<?=$d_adresse["adr_id"]?>]["ad2"]="<?=$d_adresse["adr_ad2"]?>";	
					tab_adresse[<?=$d_adresse["adr_id"]?>]["ad3"]="<?=$d_adresse["adr_ad3"]?>";	
					tab_adresse[<?=$d_adresse["adr_id"]?>]["cp"]="<?=$d_adresse["adr_cp"]?>";	
					tab_adresse[<?=$d_adresse["adr_id"]?>]["ville"]="<?=$d_adresse["adr_ville"]?>";	
		<?php	} ?>
		
				var stagiaire="<?=$html_sta?>";
				
				var tab_qte=new Array();
		<?php	foreach($unite_fac as $k => $v){ ?>	
					tab_qte[<?=$k?>]="<?=$v?>";		
		<?php	} ?>
				
				jQuery(document).ready(function () {				
				
					// MAJ ADRESSE CLIENT POUR LA CONVENTION
					
					// ini l'ouverture du modal de choix
					$("#btn_edit_adr").click(function(){
						cli_adresse=$("#mod_cli_adresse").val();
						afficher_ad_client("",cli_adresse,0);
					});
					
					// affiche l'adresse sélectionnée
					$("#mod_cli_adresse").change(function(){
						cli_adresse=$("#mod_cli_adresse").val();
						afficher_ad_client("",cli_adresse,0);
					});
					
					// enregistrement du choix et fermeture du modal
					$("#btn_ad_client").click(function(){	
						if(valider_form("#form_ad_client")){
							cli_adresse=$("#mod_cli_adresse").val();
							envoyer_form_ajax($("#form_ad_client"),afficher_ad_client,cli_adresse,1);
							$("#modal_ad_client").modal("hide");
						}
					});	
					
					// MAJ LIBELLE
					$("#btn_edit_lib").click(function(){
						$("#mod_libelle_txt").val($("#con_libelle_txt").html());
						$("#modal_form_lib").modal("show");
						
					});
					
					$("#btn_form_lib").click(function(){
						envoyer_form_ajax($("#form_form_lib"),afficher_form_lib,"","");
						$("#modal_form_lib").modal("hide");
						
					});
				
					// MAJ DATES
					$("#btn_edit_dates").click(function(){
						$("#mod_form_dates").val($("#con_form_dates").html().trim().replace(new RegExp('<br>', 'g'),"\r\n"));
						$("#mod_form_duree").val($("#con_form_duree").html());
						$("#modal_form_dates").modal("show");
						
					});
					
					$("#btn_form_dates").click(function(){
						
						envoyer_form_ajax($("#form_form_dates"),afficher_form_dates,"","");
						$("#modal_form_dates").modal("hide");
						
					});
				
					// MAJ LIEU D'INTERVENTION
					$("#btn_edit_lieu").click(function(){					
						$("#modal_form_lieu").modal("show");					
					});
					
					$(".adr-ref").change(function(){
						var type_adresse=$('input[name=adr_ref]:checked').val();
						affichage_adresse(type_adresse);
					});
				
					$("#adresse").change(function(){
						adresse=$(this).val();
						client=$("#client").val();
						afficher_adresse(adresse);
						if(client>0){
							// si adresse on prend contact associé sinon tous les contacts
							get_contacts(1,client,adresse,0,actualiser_contacts,"","");
						}
					
					});
					
					$("#contact").change(function(){					
						afficher_contact($(this).val());					
					});
					
					$("#btn_form_lieu").click(function(){
						
						$("#client").prop("required",false);
						$("#adresse").prop("required",false);
						$("#adr_cp").prop("required",false);
						$("#adr_ville").prop("required",false);
						
						var type_adresse=$('input[name=adr_ref]:checked').val();
						if(type_adresse==1){
							$("#client").prop("required",true);
							$("#adresse").prop("required",true);
							$("#adr_cp").prop("required",true);
							$("#adr_ville").prop("required",true);
						}else if(type_adresse==3){
							$("#adr_cp").prop("required",true);
							$("#adr_ville").prop("required",true);
						}
						if(valider_form("#form_form_lieu")){
							envoyer_form_ajax($("#form_form_lieu"),afficher_form_lieu,"","");
							$("#modal_form_lieu").modal("hide");
						}
					});

					// MAJ EFFECTIF
					$("#btn_edit_eff").click(function(){					
						$("#modal_form_eff").modal("show");					
					});
					
					$("#mod_form_effectif").change(function(){	
						if($(this).val()==1 || $(this).val()==3){
							$("#mod_form_effectif_text").val("");
							$("#bloc_effectif_text").hide();
						}else{
							if($(this).val()==2){
								$("#mod_form_effectif_text").val(stagiaire.replace(new RegExp('<br/>', 'g'),"\r\n"));
							}
							$("#bloc_effectif_text").show();					
						}
					});
					
					$("#btn_form_eff").click(function(){
						envoyer_form_ajax($("#form_form_eff"),afficher_form_eff,"","");
						$("#modal_form_eff").modal("hide");
					});
				
					// MAJ FAC 
					$("#btn_edit_fac").click(function(){	
						$("#modal_fac").modal("show");				
					});
					
					$("#mod_unite").change(function(){	
						qte=tab_qte[$(this).val()];
						qte_lib="";
						switch($(this).val()){
							case "1":
								qte_lib="formation"
								break;
							case "2":
								qte_lib="jour"
								break;
							case "3":
								qte_lib="demi-journée"
								break;
							case "4":
								qte_lib="session"
								break;
							case "5":
								qte_lib="stagiaire"
								break;
						} 
						if(qte_lib!="" && qte>1){
							qte_lib=qte_lib + "s";
						}
						$("#mod_qte").val(qte);
						$("#mod_unite_txt").val(qte_lib);
					});
					
					$("#btn_fac").click(function(){			
						envoyer_form_ajax($("#form_fac"),afficher_fac,"","");
						$("#modal_fac").modal("hide");		
					});
				
				});
				
				
				function afficher_ad_client(json,adresse,zone){
					if(adresse==""){
						adresse=0;
					}
					
					if(zone==0){
							
						$("#mod_adresse_txt").html(tab_adresse[adresse]["texte"]);
						$("#modal_ad_client").modal("show");
					}else{
						// ecriture du resultat
						$("#con_adresse_txt").html(tab_adresse[adresse]["texte"]);
						
						// le changement d'adresse peut engendrer un changement de tva (ex adr FR => adr EU)
						if(json){
							if(json.actu_tva==1){
								if(json.con_tva_id==3){
									$("#bloc_tva").hide();
								}else{
									$("#bloc_tva").show();
									$("#con_tva_taux").html("TVA à " + json.con_tva_taux + "% :");
									$("#con_tva").html(json.con_tva + " €");
								}
								$("#con_ttc").html(json.con_ttc + " €");
							}
						}
						mise_en_page();
					}
					
				}
				
				function afficher_form_lib(json,param1,param1){
					$("#con_libelle_txt").html(json.acl_pro_libelle);
					mise_en_page();
				}
				
				// MAJ DATES
				function afficher_form_dates(json,param1,param1){
					$(".con_form_dates").html(json.con_form_dates.replace(new RegExp('\r\n', 'g'),"<br>"));
					$("#con_form_duree").html(json.con_form_duree);
					mise_en_page();
					
				}
			
				// MAJ LIEU
				function affichage_adresse(type_adresse){
					switch(type_adresse){
						case "1":
							// client
							$(".cont-adr-client").show();
							$(".cont-adresse").show();	
							$(".cont-contact").show();			
							$(".cont-contact-liste").show();								
							break;
						case "2":
							// agence
							$(".cont-adr-client").hide();
							$(".cont-adresse").hide();		
							$(".cont-contact").hide();			
							$(".cont-contact-liste").hide();								
							break;
							
						default:
							// autre
							$(".cont-adr-client").hide();
							$(".cont-adresse").show();	
							$(".cont-contact").show();			
							$(".cont-contact-liste").hide();								
							break;
					}
				}
			
				function actualiser_get_client(json){
					if(json){
						if(json.erreur_txt!=""){
							afficher_message("Erreur","danger",json.erreur_txt);
						}else{	
							
							// on memorie les adresses
							if(json["adr_int"]!=""){
								$.each(json["adr_int"], function(i, ad){
									tab_adresse[ad.id]=new Array(8);
									tab_adresse[ad.id]["nom"]=ad.nom;
									tab_adresse[ad.id]["service"]=ad.service;
									tab_adresse[ad.id]["ad1"]=ad.ad1;
									tab_adresse[ad.id]["ad2"]=ad.ad2;
									tab_adresse[ad.id]["ad3"]=ad.ad3;
									tab_adresse[ad.id]["cp"]=ad.cp;
									tab_adresse[ad.id]["ville"]=ad.ville;
								});
							}
							actualiser_select2(json.adr_int,"#adresse",json.int_defaut);
						}
					}else{
						// PAS DE CLIENT
						actualiser_select2("","#adresse",0);
						$("#adresse").val("").trigger("change");
						actualiser_select2("","#contact",0);
						$("#contact").val("").trigger("change");
						
					}
				}
				
				function afficher_adresse(adresse){
					if(adresse>0){						
						$("#adr_nom").val(tab_adresse[adresse]["nom"]);
						$("#adr_service").val(tab_adresse[adresse]["service"]);
						$("#adr1").val(tab_adresse[adresse]["ad1"]);
						$("#adr2").val(tab_adresse[adresse]["ad2"]);
						$("#adr3").val(tab_adresse[adresse]["ad3"]);
						$("#adr_cp").val(tab_adresse[adresse]["cp"]);
						$("#adr_ville").val(tab_adresse[adresse]["ville"]);							
					}else{
						$(".champ-adresse").val("");						
					}
				}
			
				// traitement suite à la recuperation des contacts liés à l'adresse 
				function actualiser_contacts(json){					
					if(json){						
						// on memorie les conatcts						
						var con_defaut=0;
						tab_contact=new Array();
						$.each(json, function(i, con){
							tab_contact[con.id]=new Array();
							tab_contact[con.id]["nom"]=con.nom;
							tab_contact[con.id]["prenom"]=con.prenom;
							tab_contact[con.id]["tel"]=con.tel;
							tab_contact[con.id]["portable"]=con.portable;
							tab_contact[con.id]["mail"]=con.mail;
							if(con.defaut==1){
								 con_defaut=con.id;
							}
						});	
						// actualisation du select contact
						actualiser_select2(json,"#contact",con_defaut);
						if(con_defaut==0){
							// securite si 0 pas #contact.change
							$("#contact").val(0).trigger("change");
						}
					}else{
						actualiser_select2("","#contact",0);
						$("#contact").val(0).trigger("change");
					}
				}
			
				function afficher_contact(contact){
					if(contact>0){
						$("#con_nom").val(tab_contact[contact]["nom"]);
						$("#con_prenom").val(tab_contact[contact]["prenom"]);
						$("#con_tel").val(tab_contact[contact]["tel"]);
						$("#con_portable").val(tab_contact[contact]["portable"]);						
					}else{
						$(".champ-contact").val("");							
					}
				}
			
				function afficher_form_lieu(json){
					html="";
					if(json){
						html=json.adr_nom;
						if(json.adr_service){
							html=html + "<br/>" + json.adr_service;
						}
						if(json.adr1){
							html=html + "<br/>" + json.adr1;
						}
						if(json.adr2){
							html=html + "<br/>" + json.adr2;
						}
						if(json.adr3){
							html=html + "<br/>" + json.adr3;
						}
						html=html + "<br/>" + json.adr_cp + " " + json.adr_ville;
					}
					$("#con_lieu").html(html);
					
				}
			
				// MAJ EFFECTIF
				function afficher_form_eff(json){
					
					if(json){
						if(json.con_form_effectif==1){
							$("#con_form_effectif_text").html("cf. annexe");
							$("#annexe_1").show();
						}else{
							$("#annexe_1").hide();
							if(json.con_form_effectif==3){
								$("#con_form_effectif_text").html("cf. feuille de présence");
							}else{
								
								$("#con_form_effectif_text").html(json.con_form_effectif_text.replace(new RegExp('\r\n', 'g'),"<br>"));
							}
						}
					}else{
						$("#con_form_effectif_text").html("");
						$("#annexe_1").hide();	
					}
					mise_en_page();
				}
			
				// MAJ FACTURATION 
				function afficher_fac(json){
					if(json){
						$("#con_qte").html(json.con_qte + " " + json.con_unite_txt);
						$(".con_ht").html(json.con_ht + " €");
						if(json.con_tva_id==3){
							$("#bloc_tva").hide();
						}else{
							$("#bloc_tva").show();
							$("#con_tva_taux").html("TVA à " + json.con_tva_taux + "% :");
							$("#con_tva").html(json.con_tva + " €");
						}
						$("#con_ttc").html(json.con_ttc + " €");
						$("#con_comment").html(json.con_comment.replace(new RegExp('\r\n', 'g'),"<br>"));
					}
				}
			
			</script>
<?php	} ?>
	</body>
</html>
