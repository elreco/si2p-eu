<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';
include_once 'modeles/mod_parametre.php';
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
// EDITION
if (isset($_GET['for_id'])){
    $sql="SELECT * FROM formations WHERE for_id=" . $_GET['for_id'];
    $req = $ConnSoc->query($sql);
    $d_formation=$req->fetch();
	$intervenant = $d_formation['for_intervenant'];
    $date = new DateTime($d_formation['for_date']);
	if($date->format("N") == 7){
		$currentweek = $date->format("W")+1;
	}else{
		$currentweek = $date->format("W");
	}

    $year = $date->format("Y");
    $date_get = getStartAndEndDate($currentweek, $year);

	$_SESSION['retourRapport'] = "rap.php?for_id=" . $_GET['for_id'];
}else{
    $intervenant = $_GET['intervenant'];
    $semaine = $_GET['semaine'];
    $year = $_GET['annee'];

    $date_get = getStartAndEndDate($semaine, $year);
	$_SESSION['retourRapport'] = "rap.php?annee=". $year ."&intervenant=" . $intervenant . "&semaine=" . $semaine;
}
$period = new DatePeriod(
    new DateTime($date_get['week_start']),
    new DateInterval('P1D'),
    new DateTime($date_get['week_end'])
);
$sql="SELECT * FROM formations_utilisations ORDER BY fut_libelle ASC";
$req = $Conn->query($sql);
$d_types=$req->fetchAll();

$sql="SELECT * FROM vehicules_categories";
$req = $Conn->query($sql);
$d_vehicules_categories=$req->fetchAll();

if(!empty($d_formation['for_vehicule_categorie'])){
	$sql="SELECT veh_id, veh_libelle, veh_immat FROM vehicules WHERE veh_categorie = " . $d_formation['for_vehicule_categorie'];
	$req = $Conn->query($sql);
	$d_vehicules=$req->fetchAll();
}elseif(isset($_GET['for_id'])){

	$sql="UPDATE formations SET for_vehicule_categorie = 1 WHERE for_id =" . $_GET['for_id'];
	$req = $ConnSoc->query($sql);
	$sql="SELECT veh_id, veh_libelle, veh_immat FROM vehicules WHERE veh_categorie = " . $d_formation['for_vehicule_categorie'];
	$req = $Conn->query($sql);
	$d_vehicules=$req->fetchAll();
}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Ajouter un rapport</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
<form action="rap_enr.php" method="POST" id="admin-form">
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">

            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-left">

                                        <div class="content-header">
                                            <?php if(!empty($_GET['for_id'])){ ?>

                                                <h2>Modifier le rapport <b class="text-primary"><?= $_GET['for_id'] ?></b></h2>
                                            <?php }else{ ?>
                                                <h2>Nouveau rapport </h2>
                                            <?php } ?>
                                        </div>

                                        <div class="col-md-10 col-md-offset-1">


                                                <?php if (isset($_GET['for_id'])): ?>
                                                    <input type="hidden" name="for_id" value="<?=$_GET['for_id']?>">
                                                <?php endif;?>
												<input type="hidden" name="for_intervenant" value="<?=$intervenant?>">
                                          <div class="row">

                                                <div class="col-md-2">
                                                    <div class="section">
														<label>Utilisation</label>
                                                        <select id="for_utilisation" name="for_utilisation" class="form-control">
                                                            <?php foreach($d_types as $t){ ?>

                                                                <option value="<?= $t['fut_id'] ?>"
																	<?php if(!empty($_GET['for_id']) && $t['fut_id'] == $d_formation['for_utilisation']){ ?>
																		selected
																	<?php }?>
																	><?= $t['fut_libelle'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="section">
														<label>Date</label>
                                                        <select id="for_date" name="for_date" class="form-control">
                                                            <?php foreach ($period as $key => $value) {?>
                                                                <option value="<?= $value->format('Y-m-d') ?>"
																	<?php if(!empty($_GET['for_id']) && $value->format('Y-m-d') == $d_formation['for_date']){ ?>
																		selected
																	<?php }?>
																	><?= $value->format('d/m/Y') ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="section">
														<label>Demi-journée</label>
                                                        <select id="for_demi" name="for_demi" class="form-control">
                                                            <option value="1"
															<?php if(!empty($_GET['for_id']) && $d_formation['for_demi'] == 1){ ?>
																selected
															<?php }?>
															>Matin</option>
															<option value="1.5"
															<?php if(!empty($_GET['for_id']) && $d_formation['for_demi'] == 1.5){ ?>
																selected
															<?php }?>
															>Toute la journée</option>
                                                            <option value="2"
															<?php if(!empty($_GET['for_id']) && $d_formation['for_demi'] == 2){ ?>
																selected
															<?php }?>
															>Après-midi</option>
                                                        </select>
                                                    </div>
                                                </div>
												<div class="col-md-6">
													<table class="table">
														<thead>
															<th>
																Produit
															</th>
															<th>
																Clients
															</th>
															<th>
																Nb Stagiaires
															</th>
														</thead>
														<tbody class="tableProduits">


														</tbody>
													</table>
												</div>
                                          </div>
										  <hr>
                                          <div class="row pt15">
                                                <div class="col-md-3">
                                                    <div class="section">
                                                        <label>Nombre de 1/2 jours : </label>
                                                        <input type="text" readonly id="for_nb_demi" name="for_nb_demi" class="form-control"
														<?php if(!empty($d_formation['for_nb_demi'])){ ?>
															value="<?= $d_formation['for_nb_demi'] ?>"
														<?php }?>
														 >
                                                    </div>

                                                </div>
                                                <div class="col-md-3">
                                                    <div class="section">
                                                        <label>Nombre de groupes :</label>
                                                        <input type="number" id="for_nb_groupe" name="for_nb_groupe" class="form-control"
														<?php if(!empty($d_formation['for_nb_groupe'])){ ?>
															value="<?= $d_formation['for_nb_groupe'] ?>"
														<?php }?>
														placeholder="Nombre de groupes">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="section">
                                                        <label>Nombre d'heures par groupe :</label>
                                                        <input type="text" id="for_nb_h_groupe" name="for_nb_h_groupe" class="form-control input-float"
														<?php if(!empty($d_formation['for_nb_h_groupe'])){ ?>
															value="<?= $d_formation['for_nb_h_groupe'] ?>"
														<?php }?>
														placeholder="Nombre d'heures par groupe">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="section">
                                                        <label>Total heures : </label>
                                                        <input type="text" readonly id="for_total_h" name="for_total_h" class="form-control input-float"
														<?php if(!empty($d_formation['for_total_h'])){ ?>
															value="<?= $d_formation['for_total_h'] ?>"
														<?php }?>
														>
                                                    </div>

                                                </div>

                                          </div>
                                          <div class="row pt15">
                                                <div class="col-md-12">
													<label>
														Observations
													</label>
                                                    <textarea name="for_observations" id="" cols="30" rows="10" class="form-control"><?php if(!empty($d_formation['for_observations'])){ ?><?= $d_formation['for_observations'] ?><?php }?></textarea>
                                                </div>
                                          </div>
										  <div class="row pt15 hebergementBloc"
										  <?php if(empty($d_formation['for_hebergement'])){ ?>
											  style="display:none;"
										  <?php } ?>
										  >
											  	<div class="col-md-4">
												  	<div class="section">
														<label>Hébergement</label>
													  	<select id="for_hebergement" name="for_hebergement" class="form-control">
															<option value="0">Hébergement...</option>
														  	<option value="1"
															<?php if(!empty($_GET['for_id']) && $d_formation['for_hebergement'] == 1){ ?>
																selected
															<?php }?>
															>Hôtel</option>
														  	<option value="2"
															<?php if(!empty($_GET['for_id']) && $d_formation['for_hebergement'] == 2){ ?>
																selected
															<?php }?>
															>Autre</option>
													  	</select>
												  	</div>
											  	</div>
                                                <div class="col-md-8">
													<label>Précisions</label>
													<input type="text" id="for_hebergement_texte" name="for_hebergement_texte" class="form-control" placeholder="Précisions"
													<?php if(!empty($d_formation['for_hebergement_texte'])){ ?>
														value="<?= $d_formation['for_hebergement_texte'] ?>"
													<?php }?>
													>
                                                </div>
                                          </div>
                                          <div class="row pt15">
                                                <div class="col-md-3">
                                                <label>Catégorie du véhicule</label>
                                                    <select id="for_vehicule_categorie" name="for_vehicule_categorie" class="select2 select2-categorie-vehicule" placeholder="Catégorie du véhicule">
														<option value="0">Sélectionnez une catégorie</option>
														<?php foreach($d_vehicules_categories as $c){ ?>
                                                            <option value="<?= $c['vca_id'] ?>"
																<?php if(!empty($_GET['for_id']) && $d_formation['for_vehicule_categorie'] == $c['vca_id']){ ?>
																	selected
																<?php }?>
																><?= $c['vca_libelle'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                <label>Véhicule</label>

                                                    <select id="for_vehicule_id" name="for_vehicule_id" class="select2 select2-vehicule"

													>
														<option value="0">Sélectionnez un véhicule</option>
														<?php if(!empty($d_formation['for_vehicule_id'])){
														foreach($d_vehicules as $v){ ?>
															<option value="<?= $v['veh_id'] ?>"
																<?php if($v['veh_id'] == $d_formation['for_vehicule_id']){ ?>selected<?php }?>
																><?= $v['veh_libelle'] ?></option>
														<?php }
															}?>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="section">
                                                        <label>Immatriculation : </label>
                                                        <input type="text" id="for_vehicule_numero" name="for_vehicule_numero" class="form-control" placeholder="Immatriculation" <?php if(!empty($d_formation['for_hebergement_texte'])){ ?>
															value="<?= $d_formation['for_vehicule_numero'] ?>"
														<?php }?>>
                                                    </div>

                                                </div>
                                                <div class="col-md-3">
                                                    <div class="section">
                                                        <label>Kilométrage à la prise du véhicule : </label>
                                                        <input type="number" id="for_km_deb" name="for_km" class="form-control" placeholder="Kilométrage à la prise du véhicule" <?php if(!empty($d_formation['for_km_deb'])){ ?>
															value="<?= $d_formation['for_km_deb'] ?>"
														<?php }?>>
                                                    </div>
                                                </div>
                                          </div>

                                      </div>

                                  </div>
                              </div>


                          </div>
                      </div>

                  </div>
              </div>

          </div>
      </section>
      <!-- End: Content -->
  </section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="<?= $_SESSION['retourRapportVoir']?>" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-save'></i> Enregistrer
            </button>
        </div>
    </div>
</footer>
</form>
<?php
	include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
<script>
    $(document).ready(function () {
        choixVal();
        calcul();
		controleHotel();
		/* var selecteur=".select2-vehicule";
		get_vehicules($(".select2-categorie-vehicule").val(),actualiser_select2,selecteur,0); */
		$(".select2-vehicule").select2();
		if($(".select2-vehicule").val()!=0){
			$("#for_km_deb").attr("required",true);
		}else{
			$("#for_km_deb").attr("required",false);
		}
		if($("#for_utilisation").val() == 1){
			<?php if(!empty($_GET['for_id'])){ ?>
				get_clients($("#for_demi").val(), $("#for_date").val(), <?= $_GET['for_id'] ?>);
			<?php }else{ ?>
				get_clients($("#for_demi").val(), $("#for_date").val(), 0);
			<?php }?>
		}else{
			$(".tableProduits").html("");
		}
        $("#for_utilisation").change(function () {
            choixVal();
			controleHotel();
			if($("#for_utilisation").val() == 1){
					get_clients($("#for_demi").val(), $("#for_date").val(), 0);
			}else{
				$(".tableProduits").html("");
			}
       });
       $("#for_demi").change(function () {
            choixVal();
			controleHotel();

			if($("#for_utilisation").val() == 1){
				get_clients($("#for_demi").val(), $("#for_date").val(), 0);
			}else{
				$(".tableProduits").html("");
			}
       });
       $("#for_date").change(function () {
            choixVal();
			if($("#for_utilisation").val() == 1){
					get_clients($("#for_demi").val(), $("#for_date").val(), 0);
			}else{
				$(".tableProduits").html("");
			}

       });
		$("#for_nb_groupe").change(function () {
            calcul();
        });
        $("#for_nb_h_groupe").change(function () {
            calcul();
        });
		$(".select2-vehicule").change(function () {
            get_immat($(this).val());
			if($(this).val()!=0){
				$("#for_km_deb").attr("required",true);
			}else{
				$("#for_km_deb").attr("required",false);
				$("#for_vehicule_numero").val("");
			}
        });
		if($(".select2-categorie-vehicule").val() != 0){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_vehicules_get.php',
				data : 'categorie=' + $(".select2-categorie-vehicule").val(),
				dataType: 'json',
				success: function(data){
					actualiser_select2(data);
				},
				error: function(data){

				}
			});
		}

		$(".select2-categorie-vehicule").change(function () {
			if($(".select2-vehicule").val()!=0){
				get_immat($(".select2-vehicule").val());
			}
			if($(this).val()==0){
				$("#for_km_deb").attr("required",false);
				$("#for_vehicule_numero").val("");
			}

        });
		<?php if(!empty($_GET['for_id'])){ ?>
			get_immat($(".select2-vehicule").val());
		<?php } ?>

    });
    function choixVal(){

        choix=$("#for_demi").val();
        utilisation=$("#for_utilisation").val();

        if(utilisation==10)
        {
            nb_demi=2;
            $("#for_demi").attr("disabled", "disabled");
        }else{
            $("#for_demi").removeAttr("disabled");
            if(choix==1||choix==2)
                nb_demi=1
            else
                nb_demi=2;
        };

		if($("#for_utilisation").val() != 1){
			$("#for_nb_groupe").attr("readonly", true);
			$("#for_nb_groupe").val(0);
			$("#for_nb_demi").val(nb_demi);
			$("#for_nb_h_groupe").attr("readonly", true);
			$("#for_nb_h_groupe").val(0);
			$("#for_total_h").val(0);
		}else{
			$("#for_nb_groupe").attr("readonly", false);
			$("#for_nb_h_groupe").attr("readonly", false);
			$("#for_nb_demi").val(nb_demi);
		}
    }
	function get_immat(vehicule){

		$.ajax({
            type: "get",
            url: "ajax/ajax_get_immat.php",
            data: "vehicule=" + vehicule,
            dataType: "html",
            success: function (data) {
                $("#for_vehicule_numero").val(data.replace(/\s/g, ''));
            }
        });
	}
	function get_clients(demi, date, formation){
		$(".tableProduits").html("");
		$.ajax({
            type: "get",
            url: "ajax/ajax_get_clients_rapports.php",
            data: "date=" + date + "&demi=" + demi + "&intervenant=<?= $intervenant ?>&formation=" + formation,
            dataType: "json",
            success: function (data) {
				$(".tableProduits").html("");
				console.log(data);
				if (data != 1 && data != 2){
					var html = "";
					//
					$.each(data, function (index, value) {
						// BOUCLE PRODUIT

						$.each(value["clients"], function (index2, value2) {
							// BOUCLE CLIENT LIE AU PRODUIT PRO_ID
							html = html + "<tr>";
							if(index2==0){
								html = html + "<td style='width:33%' rowspan='"+ value["clients"].length + "'>" + value["pro_code_produit"]  + "</td>";
							}
							html = html +"<td style='width:50%;'>" + value2['cli_code'] + "</td>\
							<td>\
								<input type='text' name='for_nb_stagiaires[" + value['pro_id'] + "][" + value2['acl_id'] + "]' size='4' maxlength='4' class='form-control' value='" + value2['fac_nb_stagiaires'] + "'>\
								<input type='hidden' name='for_produit[" + value['pro_id'] + "][" + value2['acl_id'] + "]' value='" + value['pro_id'] + "'>\
								<input type='hidden' name='for_action[" + value['pro_id'] + "][" + value2['acl_id'] + "]' value='" + value['acl_action'] + "'>\
							</td>";
							html = html + "</tr>";
						});

					});

					$(".tableProduits").append(html);
				}else if(data == 1){
					$("#for_demi").val(1);
					get_clients(1, date, formation);
				}

            }
        });
		// <tr>
		// 	<td style="width:33%;"></td>
		// 	<td>
		// 		<table class="table">
		//
		// 			<tbody class="tableClients_">
		//
		// 				<tr>
		// 					<td style="width:50%;">RIBIERE</td>
		// 					<td>
		// 						<input type="text" name="for_nb_stagiaires_1" size="4" maxlength="4" class="form-control" value="0">
		// 						<input type="hidden" name="for_id_1" value="283713">
		// 						<input type="hidden" name="for_produit_1" value="1128">
		// 						<input type="hidden" name="for_action_1" value="56352">
		// 						<input type="hidden" name="for_client_1" value="10733">
		// 					</td>
		// 				</tr>
		// 			</tbody>
		// 		</table>
		// 	</td>
		// </tr>

	}
    function calcul(){
        var nbg=document.getElementById("for_nb_groupe").value.toString().replace(",",".");
        var nbh=document.getElementById("for_nb_h_groupe").value.toString().replace(",",".");
        nbg=parseFloat(nbg);
        nbh=parseFloat(nbh);
        var total=nbg*nbh;
        total=Math.round(100*total)/100;
        if($("#for_nb_groupe").val() && $("#for_nb_h_groupe").val()){
            document.getElementById("for_total_h").value=total;
        }
    }
	function controleHotel(){
		if(($("#for_demi").val()==2)||($("#for_demi").val()==1.5)){
			if(($("#for_utilisation").val()<=4)||($("#for_utilisation").val()==8)||($("#for_utilisation").val()==11)||($("#for_utilisation").val()==12)||($("#for_utilisation").val()==13)){
				$(".hebergementBloc").show();
			}else{
				$(".hebergementBloc").hide();
			};
		}else{
			$(".hebergementBloc").hide();
		}
	}
</script>
</body>
</html>
