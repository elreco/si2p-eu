<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");

include "modeles/mod_parametre.php";
include "modeles/mod_droit.php";
include "modeles/mod_erreur.php";

include "modeles/mod_get_cli_categories.php";

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
/*if($acc_societe!=3){
	$acc_agence=0;
}*/
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
$acc_profil=0;
if(isset($_SESSION['acces']["acc_profil"])){
	$acc_profil=intval($_SESSION['acces']["acc_profil"]);
}
$acc_drt_base_client=false;
if($_SESSION['acces']['acc_ref']==1){
	if(!empty(get_droit_utilisateur(6,$_SESSION['acces']['acc_ref_id']))){
		$acc_drt_base_client=true;
	}
}

// GESTION DU MENU
	if(isset($_GET["menu_actif"])){
		if(!empty($_GET["menu_actif"])){
			$menu_actif="2-3";
		}
	}

$_SESSION["retour_action"]="planning.php";
$_SESSION["retour"]="planning.php";

/*------------------------------
 GESTION DE L'AFFICHAGE
------------------------------*/

$mois=0;
$annee=0;
$drt_edit_planning=true;
if($acc_profil==1){
	$_SESSION["pla_affichage"]=1;
	if(!empty($_SESSION['acces']['acc_droits'][32])){
		$drt_edit_planning=true;
	}else{
		$drt_edit_planning=false;
	}
}else{
	if(!isset($_SESSION["pla_affichage"])){
		$_SESSION["pla_affichage"]=0;
	}
}


// L'UTILISATEUR A FILTRE L'AFFICHAGE

if(isset($_POST["filt_aff"])){
	if(!empty($_POST["periode_mois"])){
		// l'utilisateur affiche un mois entier

		$tab_mois=explode("/",$_POST["periode_mois"]);
		$mois=$tab_mois[0];
		$annee=$tab_mois[1];

		$jour_deb=date("N", mktime(0, 0, 0, $mois, 1, $annee));
		$deb_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1), $annee);
		$date_deb=date_create(date("Y-m-d",$deb_mk));

		$jour_fin=date("N", mktime(0, 0, 0, $mois+1, 0, $annee));
		$fin_mk=mktime(0, 0, 0, $mois+1, 5-$jour_fin, $annee);
		$date_fin=date_create(date("Y-m-d",$fin_mk));

		$_SESSION["pla_affichage"]=0;
		$_SESSION["pla_mois"]=$mois;
		$_SESSION["pla_annee"]=$annee;
		$_SESSION["pla_periode_deb"]=$date_deb->format("d/m/Y");
		$_SESSION["pla_periode_fin"]=$date_fin->format("d/m/Y");


	}elseif(!empty($_POST["periode_deb"])){

		// l'utilisateur affiche une periode perso

		$c_date_deb=convert_date_sql($_POST["periode_deb"]);
		$date_deb = new DateTime($c_date_deb);

		$c_date_fin=convert_date_sql($_POST["periode_fin"]);
		$date_fin = new DateTime($c_date_fin);

		$_SESSION["pla_affichage"]=1;
		$_SESSION["pla_mois"]=0;
		$_SESSION["pla_annee"]=0;
		$_SESSION["pla_periode_deb"]=$_POST["periode_deb"];
		$_SESSION["pla_periode_fin"]=$_POST["periode_fin"];


	}

}else{

	// PAS DE FILTRE

	if($_SESSION["pla_affichage"]==1){

		// periode perso

		if(!empty($_GET["periode_deb"])){

			$c_date_deb=convert_date_sql($_GET["periode_deb"]);
			$_SESSION["pla_periode_deb"]=$_GET["periode_deb"];

			$c_date_fin=convert_date_sql($_GET["periode_fin"]);
			$_SESSION["pla_periode_fin"]=$_GET["periode_fin"];

		}elseif(!empty($_SESSION["pla_periode_deb"])){


			//echo("SESSION periode " . $_SESSION["pla_periode_deb"] . "<br/>");

			$c_date_deb=convert_date_sql($_SESSION["pla_periode_deb"]);
			$c_date_fin=convert_date_sql($_SESSION["pla_periode_fin"]);

		}else{

			if($acc_profil==1){


				//echo("Période formateur<br/>");

				$jour_deb=date("N");

				$deb_mk=mktime(0, 0, 0, date("m"), date("d")-($jour_deb-1)-7, date("y"));
				$c_date_deb=date("Y-m-d",$deb_mk);

				$fin_mk=mktime(0, 0, 0, date("m"), date("d")-($jour_deb-1)+18, date("y"));
				$c_date_fin=date("Y-m-d",$fin_mk);

			}else{

				$jour_deb=date("N");
				$deb_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1), $annee);
				$c_date_deb=date("Y-m-d",$deb_mk);
				//$_SESSION["pla_periode_deb"]=$c_date_deb;

				$fin_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1)+6, $annee);
				$c_date_fin=date("Y-m-d",$fin_mk);
				//$_SESSION["pla_periode_fin"]=$c_date_fin;
			}

			// session stock format fr
			$_SESSION["pla_periode_deb"]=convert_date_txt($c_date_deb);
			$_SESSION["pla_periode_fin"]=convert_date_txt($c_date_fin);
		}

		$date_deb = new DateTime(date("Y-m-d",strtotime($c_date_deb)));
		$date_fin = new DateTime(date("Y-m-d",strtotime($c_date_fin)));


	}else{

		// mois entier

		if(isset($_GET["mois"]) AND isset($_GET["annee"])){

			$mois=$_GET["mois"];
			$annee=$_GET["annee"];

			$_SESSION["pla_mois"]=$mois;
			$_SESSION["pla_annee"]=$annee;


		}elseif(!isset($_SESSION["pla_mois"])){

			$mois=date("n");
			$annee=date("Y");

			$_SESSION["pla_mois"]=$mois;
			$_SESSION["pla_annee"]=$annee;

		}else{

			$mois=$_SESSION["pla_mois"];
			$annee=$_SESSION["pla_annee"];

		}

		$jour_deb=date("N", mktime(0, 0, 0, $mois, 1, $annee));
		$deb_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1), $annee);
		$date_deb=date_create(date("Y-m-d",$deb_mk));

		$_SESSION["pla_periode_deb"]=date("d/m/Y",$deb_mk);

		//var_dump($_SESSION["pla_periode_deb"]);

		$jour_fin=date("N", mktime(0, 0, 0, $mois+1, 0, $annee));
		$fin_mk=mktime(0, 0, 0, $mois+1, 5-$jour_fin, $annee);
		$date_fin=date_create(date("Y-m-d",$fin_mk));

		$_SESSION["pla_periode_fin"]=date("d/m/Y",$fin_mk);

	}
}

// GESTION DE LA PAGINATION
if($_SESSION["pla_affichage"]==1){

	$interval_date = $date_deb->diff($date_fin);

	$prec_fin = new DateTime($date_deb->format("Y-m-d"));
	$prec_fin->sub(new DateInterval('P2D'));

	$prec_deb=new DateTime($prec_fin->format("Y-m-d"));
	$prec_deb->sub(new DateInterval('P'. $interval_date->format('%a') .'D'));

	$suiv_deb=new DateTime($date_fin->format("Y-m-d"));
	$suiv_deb->add(new DateInterval('P2D'));

	$suiv_fin=new DateTime($suiv_deb->format("Y-m-d"));
	$suiv_fin->add(new DateInterval('P'. $interval_date->format('%a') .'D'));

}else{
	$time_mois_suiv=mktime(0, 0, 0, $mois+1, 1, $annee);
	$mois_suiv=date("n",$time_mois_suiv);
	$annee_suiv=date("Y",$time_mois_suiv);

	$time_mois_prec=mktime(0, 0, 0, $mois-1, 1, $annee);
	$mois_prec=date("n",$time_mois_prec);
	$annee_prec=date("Y",$time_mois_prec);
}

	//------------------------------
	//	DONNEE NECESSAIRE
	//------------------------------

		//SOCIETES / AGENCES

	$sql="SELECT soc_agence FROM Societes WHERE soc_id=:acc_societe;";
	$req = $Conn->prepare($sql);
	$req->bindParam(":acc_societe",$acc_societe);
	$req->execute();
	$societe = $req->fetch();


	// FAMILLES DE PRODUITS
	$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles ORDER BY pfa_libelle;";
	$req = $Conn->query($sql);
	$d_familles = $req->fetchAll();

	// LES CODES AGENCES

	$d_agence=array();
	if(!empty($societe["soc_agence"])){
		$sql="SELECT age_id,age_code FROM Agences WHERE age_societe=:acc_societe";
		if($acc_agence>0){
			$sql.=" AND age_id=" . $acc_agence;
		}
		$sql.=" ORDER BY age_code;";
		$req = $Conn->prepare($sql);
		$req->bindParam(":acc_societe",$acc_societe);
		$req->execute();
		$d_result = $req->fetchAll();
		if(!empty($d_result)){
			foreach($d_result as $d_r){
				if($acc_agence>0){
					$d_agence[$d_r["age_id"]]="";
				}else{
					$d_agence[$d_r["age_id"]]=$d_r["age_code"];
				}
			}
		}
	}else{
		$d_agence[0]="";
	}



	/*if($acc_agence==0){
		$horaire_agence=array(
			0 => $societe["soc_h_deb_matin"],
			1 => $societe["soc_h_fin_matin"],
			2 => $societe["soc_h_deb_am"],
			3 => $societe["soc_h_fin_am"]
		);
	}*/

	/*if($acc_agence>0){
		$sql="SELECT age_h_deb_matin,age_h_fin_matin,age_h_deb_am,age_h_fin_am FROM Agences WHERE age_id=:acc_agence;";
		$req = $Conn->prepare($sql);
		$req->bindParam(":acc_agence",$acc_agence);
		$req->execute();
		$agence = $req->fetch();
		$horaire_agence=array(
			0 => $agence["age_h_deb_matin"],
			1 => $agence["age_h_fin_matin"],
			2 => $agence["age_h_deb_am"],
			3 => $agence["age_h_fin_am"]
		);
	}*/
		// LES COMMERCIAUX

	/*$sql="SELECT com_id,com_label_1,com_label_2 FROM Commerciaux";
	if($acc_agence>0){
		$sql.=" WHERE com_agence=:acc_agence";
	}
	$sql.=" ORDER BY com_label_1,com_label_2";
	$req = $ConnSoc->prepare($sql);
	if($acc_agence>0){
			$req->bindParam(":acc_agence",$acc_agence);
	}
	$req->execute();
	$commerciaux = $req->fetchAll();*/

		// LES VEHICULES

	/*$sql="SELECT veh_id,veh_libelle FROM Vehicules WHERE veh_societe=:acc_societe";
	if($acc_agence>0){
		$sql.=" AND veh_agence=:acc_agence";
	}
	$sql.=" ORDER BY veh_libelle,veh_libelle";
	$req = $Conn->prepare($sql);
	$req->bindParam(":acc_societe",$acc_societe);
	if($acc_agence>0){
			$req->bindParam(":acc_agence",$acc_agence);
	}
	$req->execute();
	$vehicules = $req->fetchAll();*/

	// LES CATEGORIES DE CLIENTS

	//$client_categories=get_cli_categories();

/********************************************
		FONCTION
*********************************************/

// RETOURNE L'ENTETE D'UNE Semaine

Function w_planning_head($semaine,$date_deb){

	$f_date=new DateTime($date_deb->format('Y-m-d'));
	$date_f_fr=$f_date->getTimestamp();


	echo("<div class='row mt15 ml15 mr15 bg-dark'  >");
		echo("<div class='col-xs-1 text-center' >");
			echo(utf8_encode(strftime("%B %Y",$date_f_fr)));
			echo("<br/>Semaine " . $semaine);

		echo("</div>");
		echo("<div class='col-xs-11' >");
			echo("<div class='row' >");
				$f_date->sub(new DateInterval('P1D'));
				for($j=0;$j<=5;$j++){
					$f_date->add(new DateInterval('P1D'));
					$f_date->setTimezone(new DateTimeZone('Europe/Paris'));
					echo("<div class='col-xs-2 text-center' >");
						$date_f_fr=$f_date->getTimestamp();
						echo(strftime("%A %d",$date_f_fr));
					echo("</div>");
				}
			echo("</div>");

			echo("<div class='row' >");
			for ($m = 0; $m <= 5; $m++){
					echo("<div class='col-xs-1 text-center' >");
						echo("Matin");
					echo("</div>");
					echo("<div class='col-xs-1 text-center' >");
						echo("Après-Midi");
					echo("</div>");
			};
			echo("</div>");
		echo("</div>");
	echo("</div>");
}

function w_planning_ligne($int_id,$int_agence,$int_label,$int_type,$date_deb,$planning,$semaine,$int_option){

	global $drt_edit_planning;

	$f_date=new DateTime($date_deb->format('Y-m-d'));
	$f_semaine=$f_date->format("W");
	$f_annee=$f_date->format("Y");

	$class_int="";
	if($int_type==2){
		$class_int=" text-info";
	}elseif($int_type==3){
		$class_int=" text-danger";
	} elseif ($int_type==0 AND $int_option>0) {
		$class_int=" text-warning";
	}
	// INTERCO
	$check_interco = 0;
	if($int_type == 2){
		$check_interco = 1;
	}
	if(empty($int_agence)){
		if(!empty($_SESSION['acces']['acc_agence'])){
			$int_agence = $_SESSION['acces']['acc_agence'];
		}

	}
	echo("<div class='row ml15 mr15' id='int_" . $int_id . "_" . $f_annee . "_" . $f_semaine . "_" . $int_agence . "' >");
		if($drt_edit_planning){
			echo("<div class='col-xs-1 planning_intervenant context-case intervenant" . $class_int . "' data-intervenant='" . $int_id . "' data-date_deb='" . $f_date->format("Y-m-d") . "' data-agence='" . $int_agence . "' data-annee='" . $f_annee . "' data-semaine='" . $f_semaine . "' >");
				echo($int_label);
			echo("</div>");
		}else{
			echo("<div class='col-xs-1 planning_intervenant' >");
				echo($int_label);
			echo("</div>");
		}

		echo("<div class='col-xs-11' >");
			echo("<div class='row' >");

				for($j=0;$j<=5;$j++){

					$date_lib=$f_date->format('Y-m-d');
					$date_lib_txt=$f_date->format('d/m/Y');

					for($d=1;$d<=2;$d++){

						$case_id=$int_id . "_" . $date_lib . "_" . $d;

						if(!empty($planning[$date_lib][$d])){
							$case=$planning[$date_lib][$d];

							$class_case="";
							$class_contenu="";
							$data_contenu="";
							if($drt_edit_planning){
								switch ($case["type"]) {
									case 0:
										$class_case="";
										break;
									case 1:
										$class_case=" action context-case";
										$class_contenu=" action-" . $case["ref"];
										$data_contenu=" data-action='" . $case["ref"] . "'";
										if($_SESSION["acces"]["acc_droits"][32] && !isMobile()){
											$class_contenu.=" drag";
										}

										break;
									case 2:
										$class_case=" conges";
										$class_contenu="";
										$data_contenu=" data-conges='" . $case["ref"] . "'";
										break;
									case 3:
										if($_SESSION["acces"]["acc_droits"][32] && !isMobile()){
											$class_contenu=" drag";
										}
										$class_case=" case_info click context-case";
										break;
								}
								echo("<div class='col-xs-1 planning_case case_demi_". $d . " " . $class_case . " case_" . $case_id . "' data-id='" . $case_id . "' >");
									echo("<div class='contenu_" . $case_id . $class_contenu . "' style='" . $case["style"] . "' " . $data_contenu . " data-date='" . $date_lib_txt . "' data-id='" . $case_id . "' data-interco='" . $check_interco . "' data-toggle='tooltip' data-placement='top' title='" . $case["survol"] . "' >"); // id='contenu_" .  . "'
										echo($case["texte"]);
									echo("</div>");
								echo("</div>");

							}else{

								$a_url="";
								switch ($case["type"]) {
									case 1:
										$a_url="action_voir.php?action=" . $case["ref"];
										break;
								}

								echo("<div class='col-xs-1 planning_case' >");
									if(!empty($a_url)){
										echo("<a href='" . $a_url . "' style='text-decoration:none;' >");
											echo("<div style='" . $case["style"] . "' " . $data_contenu . " data-date='" . $date_lib_txt . "' data-interco='" . $check_interco . "' data-id='" . $case_id . "' data-toggle='tooltip' data-placement='top' title='" . $case["survol"] . "' >");
												echo($case["texte"]);
											echo("</div>");
										echo("</a>");
									}else{
										echo("<div style='" . $case["style"] . "' " . $data_contenu . " data-date='" . $date_lib_txt . "' data-interco='" . $check_interco . "' data-id='" . $case_id . "' data-toggle='tooltip' data-placement='top' title='" . $case["survol"] . "' >");
											echo($case["texte"]);
										echo("</div>");
									}
								echo("</div>");
							}
						}else{
							// construction case libre
							if($drt_edit_planning){
								echo("<div class='col-xs-1 context-case planning_case case_demi_". $d . " libre click case_" . $case_id . "' data-agence='" . $int_agence . "' data-id='" . $case_id . "' data-date='" . $date_lib_txt . "'>&nbsp;</div>");	// id='case_" . $case_id . "'
							}else{
								echo("<div class='col-xs-1 planning_case' >&nbsp;</div>");
							}
						}
					}
					$f_date->add(new DateInterval('P1D'));
				}
			echo("</div>");
		echo("</div>");
	echo("</div>");
};
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >

		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">



		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

		<style type="text/css" >
			.planning_case{
				font-size:9pt;
				white-space: nowrap;
			}
			.planning_intervenant{
				font-size:9pt;
			}
		</style>

	</head>

	<body class="sb-top sb-top-sm body-planning" >
		<!-- Start: Main -->
		<div id="main">

			<?php
				include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<h1 class="text-center" >
		<?php		if($_SESSION["pla_affichage"]==1){
						echo("Période du " . utf8_encode(strftime("%d %b %Y",$date_deb->getTimestamp())) . " au " . utf8_encode(strftime("%d %b %Y",$date_fin->getTimestamp())));
					}else{
						echo(utf8_encode(strftime("%B %Y",mktime(0, 0, 0, $mois, 1, $annee))));
					} ?>
				</h1>
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn content-planning">


				<?php

					foreach($d_agence as $age_id => $age_nom){

						$planning=array();

						$bcl_date_deb = new DateTime($date_deb->format('Y-m-d'));


						if(!empty($age_nom)){
							echo("<h2>Planning " . $age_nom . "</h2>");
						}

						while ($bcl_date_deb<$date_fin){


							$bcl_date_fin = new DateTime($bcl_date_deb->format('Y-m-d'));
							$bcl_date_fin->add(new DateInterval('P5D'));

							$semaine=$bcl_date_deb->format("W");
							$annee=$bcl_date_deb->format("Y");

							w_planning_head($semaine,$bcl_date_deb);

							$sql="SELECT int_id,int_label_1,int_label_2,int_type,int_agence,int_option
							,pin_agence
							,pda_date,pda_demi,pda_confirme,pda_vehicule,pda_type,pda_texte,pda_ref_1,pda_survol,pda_style_bg,pda_style_txt,pda_alert
							FROM ((";

							$sql.="Intervenants LEFT JOIN plannings_intervenants ON (Intervenants.int_id=plannings_intervenants.pin_intervenant)";
							$sql.=")";

							$sql.=" LEFT OUTER JOIN plannings_dates ON (Intervenants.int_id=plannings_dates.pda_intervenant";
								$sql.=" AND plannings_dates.pda_date>='" . $bcl_date_deb->format('Y-m-d') . "' AND plannings_dates.pda_date<='" . $bcl_date_fin->format('Y-m-d') . "' AND NOT pda_archive";
							$sql.="))";

							$sql.=" WHERE pin_semaine=" . $semaine . " AND pin_annee=" . $annee;
							//if($acc_agence>0){
								$sql.=" AND pin_agence=" . $age_id;
							//}
							if($acc_profil==1){
								$sql.=" AND int_type=1 AND int_ref_1=" . $acc_utilisateur;
							}
							$sql.=" ORDER BY pin_agence,pin_place,int_id,pda_date,pda_demi";
							$req = $ConnSoc->query($sql);
							$result = $req->fetchAll(PDO::FETCH_ASSOC);

							if(!empty($result)){
								$int_id=0;
								$pin_agence=0;
								foreach($result as $r)
								{
									if($int_id!=$r["int_id"] OR $pin_agence!=$r["pin_agence"]){

										if($int_id>0){
											w_planning_ligne($int_id,$pin_agence,$int_label,$int_type,$bcl_date_deb,$planning,$semaine,$int_option);
										}
										$int_id=$r["int_id"];
										$int_label=$r["int_label_1"] . " ". $r["int_label_2"];
										$pin_agence=$r["pin_agence"];
										$int_type=$r["int_type"];
										$int_option=$r["int_option"];

										// intervenant toutes agences -> on on ajoute le code de l'agence concerné pour la ligne
										if($societe["soc_agence"] AND empty($r["int_agence"])){
											if(!empty($d_agence[$pin_agence])){
												$int_label.=" (" . $d_agence[$pin_agence] . ")";
											}

										}

										$planning=array();

									}
									if(!empty($r["pda_date"])){

										$style="";

										if($r["pda_alert"]){
											$style="background-color:#FFF;color:#F00;";
										}else{

											if($r["pda_type"]<3){
												if(empty($r["pda_confirme"])){
													$style="color:#FFF;";
												}else{
													$style="color:#333;";
												}
											}else{
												if(!empty($r["pda_style_txt"])){
													$style="color:" . $r["pda_style_txt"] . ";";
												}else{
													$style="color:#333;";
												}
											}
											if(!empty($r['pda_vehicule'])){
												$req = $Conn->prepare("SELECT veh_couleur FROM vehicules WHERE veh_id=" . $r['pda_vehicule']);
												$req->execute();
												$veh_couleur = $req->fetch();
												if(!empty($veh_couleur['veh_couleur'])){
													$style.="background-color:" . $veh_couleur['veh_couleur'] . ";";
												}elseif(!empty($r["pda_style_bg"])){
													$style.="background-color:" . $r["pda_style_bg"] . ";";
												}

											}elseif(!empty($r["pda_style_bg"])){
												$style.="background-color:" . $r["pda_style_bg"] . ";";
											}
										}

										if(empty($r["pda_texte"])){
											$r["pda_texte"] = "&nbsp;";
										}

										$planning[$r["pda_date"]][$r["pda_demi"]]=array(
											"type" => $r["pda_type"],
											"ref" => $r["pda_ref_1"],
											"texte" => $r["pda_texte"],
											"style" => $style,
											"survol" => $r["pda_survol"]

										);
									}
								}
								w_planning_ligne($int_id,$pin_agence,$int_label,$int_type,$bcl_date_deb,$planning,$semaine,$int_option);
							};
							$bcl_date_deb->add(new DateInterval('P7D'));
						}
						// FIN WHILE

					} ?>


				</section>

				<!-- End: Content -->
			</section>
		</div>


		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-4 col-md-3 footer-left" >
			<?php	if($_SESSION["acces"]["acc_droits"][32]){ ?>
						<a href="intervenant.php" class="btn btn-success btn-sm" role="button" title="Ajouter une ligne de planning" data-placement="top" data-toggle="tooltip" >
							<i class="fa fa-user" ></i>
						</a>

				<?php	if($acc_agence==0 AND $societe["soc_agence"]==1){ ?>
							<span title="Connectez-vous sur une agence pour organiser le planning" data-placement="top" data-toggle="tooltip" >
								<button class="btn btn-warning btn-sm disabled" >
									<i class="fa fa-user" ></i>
								</button>
							</span>
				<?php	}else{ ?>
							<a href="planning_intervenant.php" class="btn btn-warning btn-sm" role="button" title="Organiser les lignes" data-placement="top" data-toggle="tooltip" >
								<i class="fa fa-user" ></i>
							</a>
				<?php	}
					} ?>

				</div>

				<div class="col-xs-4 col-md-6 footer-middle center" >
					<div id="footer_dialog" >
				<?php	if(isset($_SESSION["action_coupe"])){ ?>
							<div class='text-center' >
								Presse-Papier : <b>Action N° <?=$_SESSION["action_coupe"]["action"]?></b> <?=$_SESSION["action_coupe"]["texte"]. " " . $_SESSION["action_coupe"]["survol"] . " " . $_SESSION["action_coupe"]["nb_demi_j"] . " demi-joournée(s)"?>.
								<div class='pull-right' >
									<button type='button' class='btn btn-sm btn-danger' data-toggle='tooltip' title='Annuler' id='btn_annul_coupe' >
										<i class='fa fa-times' ></i>
									</button>
								</div>
							</div>
			<?php		} ?>
					</div>
				</div>

				<div class="col-xs-4 col-md-3 footer-right" >
			<?php	if($drt_edit_planning){
						if($_SESSION["pla_affichage"]==1){ ?>
							<a href="planning.php?periode_deb=<?=$prec_deb->format("d/m/Y")?>&periode_fin=<?=$prec_fin->format("d/m/Y")?>" class="btn btn-default btn-sm" >
								<i class="fa fa-arrow-left"></i>
								<span class="hidden-xs">Période précédente</span>
							</a>
				<?php	}else{ ?>
							<a href="planning.php?mois=<?=$mois_prec?>&annee=<?=$annee_prec?>" class="btn btn-default btn-sm" >
								<i class="fa fa-arrow-left"></i>
								<span class="hidden-xs"><?=utf8_encode(strftime("%B %Y",$time_mois_prec))?></span>
							</a>
				<?php	} ?>

						<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-affiche" >
							<i class="fa fa-calendar" aria-hidden="true"></i>
						</button>
				<?php	if($_SESSION["pla_affichage"]==1){ ?>
							<a href="planning.php?periode_deb=<?=$suiv_deb->format("d/m/Y")?>&periode_fin=<?=$suiv_fin->format("d/m/Y")?>" class="btn btn-default btn-sm" >
								<span class="hidden-xs">Période suivante</span>
								<i class="fa fa-arrow-right"></i>
							</a>
				<?php	}else{ ?>
							<a href="planning.php?mois=<?=$mois_suiv?>&annee=<?=$annee_suiv?>" class="btn btn-default btn-sm" >
								<span class="hidden-xs"><?=utf8_encode(strftime("%B %Y",$time_mois_suiv))?></span>
								<i class="fa fa-arrow-right"></i>
							</a>
				<?php	}
					} ?>
				</div>

			</div>
		</footer>
		<style media="screen">
			.overheight{
				max-height: 300px;
				overflow-y: scroll;
			}
		</style>
		<!-- ******************************	-->
		<!--	MENU CONTEXTUEL  			-->
		<!-- ******************************	-->

		<div id="context_add" class="context-menu">
			<ul class="dropdown-menu">
	<?php		if($_SESSION["acces"]["acc_droits"][32]){ ?>
					<li class="ctx-add" >
						<a href="javascript:void(0)" id="ctx_add_action" >Action</a>
					</li>
					<li class='ctx-coller' >
						<a href="javascript:void(0)" id="ctx_act_coller" >Coller</a>
					</li>
					<li class="dropdown-submenu ctx-add">
						<a tabindex="-1" href="javascript:void(0)">Congés</a>
						<ul class="dropdown-menu" style="max-height:500px;overflow-y:scroll;" >
			<?php
						$sql="SELECT udr_droit FROM utilisateurs_droits WHERE udr_utilisateur=" . $_SESSION['acces']['acc_ref_id'];
						$req = $Conn->query($sql);
						$droits = $req->fetchAll();

						if(!empty($droits)){
							foreach($droits as $d){
								$list_droits[] = $d['udr_droit'];
							}
							$query_droits = implode(',', $list_droits);
						}else{
							$query_droits = 0;
						}

						$req=$Conn->query("SELECT cca_id, cca_libelle, cca_couleur_bg FROM conges_categories WHERE cca_droit = 0 OR cca_droit IN(" . $query_droits . ")");
						$d_conges_cat = $req->fetchAll();
						if(!empty($d_conges_cat)){
							foreach($d_conges_cat as $con_cat){
								echo("<li>");
									echo("<a href='javascript:void(0)' class='add-conges' data-conges='" . $con_cat["cca_id"] . "' >");
										echo ("<span class='badge' style='background-color:" . $con_cat["cca_couleur_bg"] . "' >&nbsp;</span>");
										echo ($con_cat["cca_libelle"]);
									echo("</a>");
								echo("</li>");
							}
						} ?>
						</ul>
					</li>
	<?php		} ?>
				<li class="dropdown-submenu ctx-add">
					<a href="javascript:void(0)">Autres</a>
					<ul class="dropdown-menu overheight">
			<?php		$req=$Conn -> query("SELECT pca_id,pca_libelle,pca_couleur FROM Plannings_Cases WHERE pca_type=0 AND pca_archive=0 ORDER BY pca_libelle;");
						$case = $req->fetchAll();
						if(!empty($case)){
							foreach($case as $c){
								echo("<li>");
									echo("<a href='javascript:void(0)' class='add-case' data-case='" . $c["pca_id"] . "' >");
										echo ("<span class='badge' style='background-color:" . $c["pca_couleur"] . "' >&nbsp;</span>");
										echo ($c["pca_libelle"]);
									echo("</a>");
								echo("</li>");
							}
						} ?>
						<li>
							<a href="javascript:void(0)" class="add-case" data-case="0" >
								<span class='badge' style='background-color:#000;' >&nbsp;</span>
								Saisi libre
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>

		<div id="context-info" class="context-menu" >
			<ul class="dropdown-menu" >
				<li>
					<a href='javascript:void(0)' class='ctx-supp-date' >
						<i class="fa fa-times" ></i>
						Supprimer
					</a>
				</li>
		<?php	if(!empty($case)){
					foreach($case as $c){
						echo("<li>");
							echo("<a href='javascript:void(0)' class='add-case' data-case='" . $c["pca_id"] . "' >");
								echo ("<span class='badge' style='background-color:" . $c["pca_couleur"] . "' >&nbsp;</span>");
								echo ($c["pca_libelle"]);
							echo("</a>");
						echo("</li>");
					}
				} ?>
			</ul>
		</div>

		<!--menu contextuel pour une action sans autre action pre-selectionné ni case selection -->
		<div id="context_action" class="context-menu" >
			<ul class="dropdown-menu" >
				<li class="dropdown-submenu" >
					<a tabindex="-1" href="javascript:void(0)">Action</a>
					<ul class="dropdown-menu" >
					<!--	<li>
							<a href='#' class="no-select ctx-act-select" >
								<i class="fa fa-add" ></i>
								Selectionner cette action
							</a>
						</li>-->
						<!--<li>
							<a href='#' class="no-select ctx-act-comment" >
								<i class="fa fa-add" ></i>
								Modifier Commentaire
							</a>
						</li>
						<li>
							<a href='#' class="no-select ctx-act-adr" >
								<i class="fa fa-add" ></i>
								Modifier l'adresse et le contact
							</a>
						</li>
						<li>
							<a href='#' class="no-select ctx-act-heure" >
								<i class="fa fa-add" ></i>
								Modifier les horaires
							</a>
						</li>-->
						<li>
							<a href='javascript:void(0)' class="select-required ctx-act-add-case" >
								<i class="fa fa-add" ></i>
								Ajouter les dates
							</a>
						</li>
						<li>
							<a href='javascript:void(0)' class="no-select ctx-act-couper" >
								<i class="fa fa-add" ></i>
								Couper
							</a>
						</li>
				<?php	if($_SESSION['acces']["acc_droits"][10]){ ?>
							<li>
								<a href='javascript:void(0)' class="no-select ctx-act-archive" >
									<i class="fa fa-add" ></i>
									Archiver la formation
								</a>
							</li>
				<?php 	} ?>
					</ul>
				</li>
				<li class="dropdown-submenu" >
					<a tabindex="-1" href="javascript:void(0)">Clients</a>
					<ul class="dropdown-menu" id="ctx_act_cli" >

					</ul>
				</li>
			</ul>
		</div>

		<!--menu contextuel pour une action avec une autre action selectionne A DEV -->
		<div id="context_action_select" class="context-menu" >
			<ul class="dropdown-menu" >
				<li class="select-required" >
					<a href='javascript:void(0)' id="ctx_act_add_date" >
						<i class="fa fa-add" ></i>
						Ajouter la sélection
					</a>
				</li>
				<li class="select-required" >
					<a href='javascript:void(0)' id="ctx_act_move_cli" >
						<i class="fa fa-add" ></i>
						Transférer client
					</a>
				</li>
			</ul>
		</div>

		<!--menu contextuel pour L'ACTION selectionnee
		.no-select : option disponible si pas de selection en cours
		.select_required : option disponible si selection en cours
		.selected : option disponible si le clic qdroi a eu lieu sur l'action selected -->
		<div id="context_action_selected" class="context-menu" >
			<ul class="dropdown-menu" >
				<li>
					<a href='javascript:void(0)' class="selected ctx-act-select" >
						<i class="fa fa-add" ></i>
						Désélectionner action
					</a>
				</li>
				<li>
					<a href='javascript:void(0)' class="select-required ctx-supp-date" >
						<i class="fa fa-add" ></i>
						Supprimer la selection
					</a>
				</li>
		<!--
				<li>
					<a href='#' class="no-select" id="ctx_act_aff_date" >
						<i class="fa fa-add" ></i>
						Afficher / Masquer les dates
					</a>
				</li>

				<li class="select_required" >
					<a href='#' id="ctx_act_add_date" >
						<i class="fa fa-add" ></i>
						Ajouter la sélection
					</a>
				</li>
				<li class="select_required" >
					<a href='#' id="ctx_act_move_cli" >
						<i class="fa fa-add" ></i>
						Transférer client
					</a>
				</li>
-->
			</ul>
		</div>

		<!-- clic droit sur intervenant -->
<?php	if($_SESSION["acces"]["acc_droits"][32]){ ?>
			<div id="context_int" class="context-menu" >
				<ul class="dropdown-menu" >
					<li>
						<a href="javascript:void(0)" id="ctx_int_select" >Selectionner la ligne</a>
					</li>
					<li>
						<a href="javascript:void(0)" id="ctx_int_supp" >Supprimer la ligne</a>
					</li>
				</ul>
			</div>
<?php	} ?>



		<!-- ******************************	-->
		<!--			MODAL 				-->
		<!-- ******************************	-->

		<!-- gestion de la periode d'affichage -->
		<div id="modal-affiche" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Affichage</h4>
					</div>
					<form method="post" action="planning.php" >
						<div>
							<input type="hidden" name="filt_aff" value="filt" />
						</div>
						<div class="modal-body admin-form">
							<div class="row">
								<div class="col-md-12">
									<div class="section-divider mb40">
										<span>Affichage par mois</span>
									</div>
								</div>
							</div>
							<div class="row" >
								<div class="col-md-6">
									<div class="field prepend-icon">
							<?php		if($_SESSION["pla_affichage"]==1){ ?>
											<input type="text" id="periode_mois" name="periode_mois" class="gui-input champ_date" placeholder="Selectionner un mois" />
							<?php		}else{ ?>
											<input type="text" id="periode_mois" name="periode_mois" class="gui-input champ_date" placeholder="Selectionner un mois" value="<?=$mois."/".$annee?>" />
							<?php		} ?>
										<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
									</div>
								</div>
							</div>


							<div class="row">
								<div class="col-md-12">
									<div class="section-divider mb40">
										<span>Affichage personalisé</span>
									</div>
								</div>
							</div>
							<div class="row" >
								<div class="col-md-6">
									Du
									<div class="field prepend-icon">
							<?php		if($_SESSION["pla_affichage"]==1){ ?>
											<input type="text" id="periode_deb" name="periode_deb" class="gui-input" placeholder="Selectionner une date" value="<?=$date_deb->format("d/m/Y")?>" />
							<?php		}else{ ?>
											<input type="text" id="periode_deb" name="periode_deb" class="gui-input" placeholder="Selectionner une date" />
							<?php		} ?>
										<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
									</div>
								</div>
								<div class="col-md-6">
									Au
									<div class="field prepend-icon">

							<?php		if($_SESSION["pla_affichage"]==1){ ?>
											<input type="text" id="periode_fin" name="periode_fin" class="gui-input" placeholder="Selectionner une date" value="<?=$date_fin->format("d/m/Y")?>" />
							<?php		}else{ ?>
											<input type="text" id="periode_fin" name="periode_fin" class="gui-input" placeholder="Selectionner une date" />
							<?php		} ?>
										<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-view" ></i>
								Afficher
							</button>
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div id="modal-confirme" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
						<p id="modal-text" ></p>
						<p>Etes-vous sur de vouloir continuer ?</p>
					</div>
					<div class="modal-footer">
						<button type="button" id="confirme_oui" class="btn btn-success btn-sm" data-dismiss="modal" data-confirmation=""  >Oui</button>
						<button type="button" id="confirme_non" class="btn btn-default btn-sm" data-dismiss="modal">Non</button>
					</div>
				</div>
			</div>
		</div>

		<!-- CREATION D'ACTION --->
		<div id="modal_action" class="modal fade" role="dialog" >
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Nouvelle action</h4>
					</div>

					<form method="POST" action="ajax/ajax_action_cree.php" id="form_modal_action" >
						<div>
							<!-- determine l'agence d'apres le formateur pour la recherche client -->
							<input type="hidden" id="id_heracles" name="id_heracles" value="0" />

							<input type="hidden" id="agence" name="agence" value="<?= $acc_agence ?>" class="get-client-agence client-n-agence" />

							<input type="hidden" id="liste_case" name="liste_case" value="" />
							<input type="hidden" id="action" name="action" value="0" />
							<input type="hidden" id="modal_type" value="0" />
							<input type="hidden" id="ca_min" value="0" />
						</div>
						<div class="modal-body admin-form theme-primary">

							<div class="row" id="cont_client_search" >
								<div class="row mt10" >
									<div class="col-md-11" >
										<label>Client :</label>
										<div class="select2-sm" >
											<select id="client" class="select2 select2-client-n select2-produit-client get-client" required name="client" data-archive="0" data-blackliste="0" data-adr_type="1" >
												<option value='0' selected='selected' >&nbsp;</option>
											</select>
										</div>
										<small class="text-danger texte_required" id="client_required">
											Merci de selectionner un client
										</small>
									</div>
									<div class="col-md-1 pt15" >
										<button type="button" class="btn btn-danger btn-sm" id="btn_client_supp" >
											<i class="fa fa-times" ></i>
										</button>
									</div>
								</div>
							</div>
							<div class="row" >
								<!-- colonne 1 -->
								<div id="modal_act_col_1" class="col-md-6" >
									<!-- ADRESSE -->
									<div class="section-divider mb40" >
										<span>Lieu d'intervention</span>
									</div>
									<div class="row" >
										<div class="col-md-4" >
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr-ref" name="act_adr_ref" value="1" checked id="adresse_client" >
													<span class="radio"></span>Client
												</label>
											</div>
										</div>
										<div class="col-md-4" >
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr-ref" name="act_adr_ref" value="2" id="adresse_agence" >
													<span class="radio"></span>Agence
												</label>
											</div>
										</div>
										<div class="col-md-4" >
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr-ref" name="act_adr_ref" value="3" id="adresse_autre" >
													<span class="radio"></span>Autres
												</label>
											</div>
										</div>
									</div>
									<div class="row bloc-adresse-client" >
										<div class="col-md-12" >
											<div class="section" >
												<div class="select2-sm" >
													<select class="select2" id="adresse" name="adresse" >
														<option value="" >Selectionnez une adresse</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row bloc-adresse">
										<div class="col-md-12" >
											<input type="text" id="adr_nom" name="act_adr_nom" class="gui-input gui-input-sm champ-adresse" placeholder="Nom" />
											<input type="text" id="adr_service" name="act_adr_service" class="gui-input gui-input-sm champ-adresse" placeholder="Service" />
											<input type="text" id="adr1" name="act_adr1" class="gui-input gui-input-sm champ-adresse" placeholder="Adresse" />
											<input type="text" id="adr2" name="act_adr2" class="gui-input gui-input-sm champ-adresse" placeholder="Complément 1" />
											<input type="text" id="adr3" name="act_adr3" class="gui-input gui-input-sm champ-adresse" placeholder="Complément 2" />
										</div>
									</div>
									<div class="row bloc-adresse">
										<div class="col-md-3" >
											<input type="text" id="adr_cp" name="act_adr_cp" class="gui-input gui-input-sm code-postal champ-adresse" placeholder="CP" />
										</div>
										<div class="col-md-9" >
											<input type="text" id="adr_ville" name="act_adr_ville" class="gui-input gui-input-sm nom champ-adresse" placeholder="Ville" />
										</div>
									</div>
									<small class="text-danger texte_required" id="adr_cp_ville_required">
										Merci d'indiquer le lieu de formation
									</small>
									<div class="row mt5 bloc-adresse-client">
										<div class="col-md-12 text-center">
											Zone géographique
											<div class="section">
												<label for="adr_geo1" class="mt15 option option-info">
													<input type="radio" id="adr_geo1" class="adr-geo" name="adr_geo" value="1" />
													<span class="radio"></span> France
												</label>
												<label for="adr_geo2" class=" mt15 option option-info">
													<input type="radio" id="adr_geo2" class="adr-geo" name="adr_geo" value="2" />
													<span class="radio"></span> UE
												</label>
												<label for="adr_geo3" class=" mt15 option option-info">
													<input type="radio" id="adr_geo3" class="adr-geo" name="adr_geo" value="3" />
													<span class="radio"></span> Autre
												</label>
											</div>
										</div>
									</div>
									<div class="row bloc-adresse-client">
										<div class="col-md-12">
											<div class="option-group field" id="bloc_adr_add" style="display:none;" >
												<label class="option option-dark">
													<input type="checkbox" name="adr_add" value="1">
													<span class="checkbox"></span>Enregistrer comme nouvelle adresse
												</label>
											</div>
											<div class="option-group field" id="bloc_adr_maj" style="display:none;" >
												<label class="option option-dark">
													<input type="checkbox" name="adr_maj" value="1">
													<span class="checkbox"></span>Mettre à jour l'adresse
												</label>
											</div>

										</div>
									</div>
									<!-- CONTACT -->

									<div class="section-divider mb40 bloc-contact" >
										<span>Contact</span>
									</div>
									<div class="row bloc-contact-client" >
										<div class="col-md-12" >
											<div class="section" >
												<div class="select2-sm" >
													<select class="select2" id="contact" name="act_contact" >
														<option value="" >Selectionnez un contact</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row bloc-contact">
										<div class="col-md-6" >
											<input type="text" name="act_con_nom" id="con_nom" class="gui-input gui-input-sm nom champ-contact" placeholder="Nom" />
										</div>
										<div class="col-md-6" >
											<input type="text" name="act_con_prenom" id="con_prenom" class="gui-input gui-input-sm prenom champ-contact" placeholder="Prénom" />
										</div>
									</div>
									<div class="row bloc-contact">
										<div class="col-md-6" >
											<input type="text" name="act_con_tel" id="con_tel" class="gui-input gui-input-sm telephone champ-contact" placeholder="Téléphone" />
										</div>
										<div class="col-md-6" >
											<input type="text" name="act_con_portable" id="con_portable" class="gui-input gui-input-sm telephone champ-contact" placeholder="Portable" />
										</div>
									</div>
									<div class="row mt5 bloc-contact-client" >
										<div class="col-md-12" >
											<input type="text" id="con_mail" name="con_mail"  class="gui-input gui-input-sm champ-contact" placeholder="Mail" value="" />
										</div>
									</div>
									<div class="row mt5 bloc-contact-client" >
										<div class="col-md-12" >
											<div class="option-group field" id="bloc_con_add" style="display:none;" >
												<label class="option option-dark">
													<input type="checkbox" name="con_add" value="1">
													<span class="checkbox"></span>Enregistrer comme nouveau contact
												</label>
											</div>
											<div class="option-group field" id="bloc_con_maj" style="display:none;" >
												<label class="option option-dark">
													<input type="checkbox" name="con_maj" value="1" >
													<span class="checkbox"></span>Mettre à jour le contact
												</label>
											</div>
										</div>
									</div>

									<!-- COMMENTAIRE -->
									<div id="cont_commentaire" >
										<div class="section-divider mb40" >
											<span>Commentaire</span>
										</div>

										<textarea class="gui-textarea" name="act_commentaire" placeholder="Commentaire sur la formation"></textarea>
									</div>
								</div>
								<!-- fin colonne 1 -->

								<!-- colonne 2 -->
								<div class="col-md-6" id="modal_act_col_2" >
									<!--FORMATION -->
									<div class="section-divider mb40" >
										<span>Formation</span>
									</div>
									<div class="row" >
										<div class="col-md-6" >
											<div class="section" >
												 <label class="option">
													<input type="radio" id="produit_intra" name="produit_type" class="select2-produit-intra produit-type" checked value="1" >
													<span class="radio"></span>Produit intra
												</label>
											</div>
										</div>

										<div class="col-md-6" >
											<div class="section" >
												 <label class="option">
													<input type="radio" id="produit_inter" name="produit_type" class="select2-produit-inter produit-type" value="2" >
													<span class="radio"></span>Produit inter
												</label>
											</div>
										</div>
									</div>
									<div class="row" >
										<div class="col-md-12" >
											<div class="section" >
												<label for="produit_famille" >Famille : </label>
												<select class="select2 select2-produit-fam" name="act_pro_famille" id="produit_famille" required="required" >
													<option value="" >Famille de produit</option>
										<?php		if(!empty($d_familles)){
														foreach($d_familles as $d_fam){
															echo("<option value='" . $d_fam["pfa_id"] . "' >" . $d_fam["pfa_libelle"] . "</option>");
														}
													} ?>
												</select>
												<small class="text-danger texte_required" id="produit_famille_required">
													Merci de selectionner la famille de produit
												</small>
											</div>
										</div>
									</div>
									<div class="row" id="bloc_s_famille" style="display:none;"  >
										<div class="col-md-12" >
											<div class="section" >
												<label for="produit_s_famille" >Sous-Famille : </label>
												<select class="select2 select2-produit-sous-fam" name="act_pro_sous_famille" id="produit_s_famille" >
													<option value="" >Sous-Famille de produit</option>
												</select>
												<small class="text-danger texte_required" id="produit_s_famille_required">
													Merci de selectionner la sous-famille de produit
												</small>
											</div>
										</div>
									</div>
									<div class="row" id="bloc_s_s_famille" style="display:none;" >
										<div class="col-md-12" >
											<div class="section" >
												<label for="produit_s_s_famille" >Sous Sous-Famille : </label>
												<select class="select2 select2-produit-sous-sous-fam" id="produit_s_s_famille" name="act_pro_sous_sous_famille" >
													<option value="" >Sous Sous-Famille de produit</option>
												</select>
												<small class="text-danger texte_required" id="produit_s_s_famille_required">
													Merci de selectionner la sous sous-famille de produit
												</small>
											</div>
										</div>
									</div>
									<div class="row bloc-produit" style="display:none;" >
										<div class="col-md-12" >
											<div class="section" >
												<div class="select2-sm" >
													<label for="produit" >Produit :</label>
													<select id="produit" class="select2" name="produit" >
														<option value="" >Selectionnez un produit ...</option>
													</select>
												</div>
												<small class="text-danger texte_required" id="produit_required">
													Merci de selectionner un produit
												</small>
											</div>
										</div>
									</div>
									<div class="row bloc-produit" style="display:none;" >
										<div class="col-md-12" >
											<div class="section" >
												<label for="produit_libelle" >Libellé de la formation :</label>
												<input type="text" class="gui-input gui-input-sm" id="produit_libelle" name="produit_libelle" value="" />
											</div>
										</div>
									</div>
									<div class="row bloc-produit" style="display:none;" >
										<div class="col-md-12" >
											<div class="section" >
												<label for="attestation" >Attestation : </label>
												<select class="select2" id="attestation" name="acl_attestation" >
													<option value="" >Attestation de formation</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row bloc-produit" style="display:none;" >
										<div class="col-md-12" >
											<div class="section" >
												<label class="option">
													<input type="checkbox" name="acl_confirme" value="on" />
													<span class="checkbox"></span>Action confirmée
												</label>
											</div>
										</div>
									</div>

									<div id="modal_act_session" >
										<div class="row" >
											<div class="col-md-10" >
												<h4 class="text-right">Nombre de session / demi-journée :</h4>
											</div>
											<div class="col-md-2" >
												<input type="number" id="nb_session" class="gui-input gui-input-sm" value="1"  min="1" step="1" max="4" name="nb_session" />
											</div>
										</div>

										<div class="row" >
											<div class="col-md-6">
												<h4 class="text-left">Matin</h4>
											</div>
											<div class="col-md-6">
												<h4 class="text-left">Après-Midi</h4>
											</div>
										</div>
										<div class="row session-1" >
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" id="h_deb_matin_1" name="h_deb_1_1" class="gui-input heure session-am" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" id="h_fin_matin_1" name="h_fin_1_1" class="gui-input heure session-am" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" id="h_deb_ap_1" name="h_deb_2_1" class="gui-input heure session-pm" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" id="h_fin_ap_1" name="h_fin_2_1" class="gui-input heure session-pm" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
										</div>
										<div class="row session-2" style="display:none;" >
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_deb_1_2" class="gui-input heure session-am" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_fin_1_2" class="gui-input heure session-am" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_deb_2_2" class="gui-input heure session-pm" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_fin_2_2" class="gui-input heure session-pm" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
										</div>
										<div class="row session-3" style="display:none;" >
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_deb_1_3" class="gui-input heure session-am" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_fin_1_3" class="gui-input heure session-am" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_deb_2_3" class="gui-input heure session-pm" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_fin_2_3" class="gui-input heure session-pm" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
										</div>
										<div class="row session-4" style="display:none;" >
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_deb_1_4" class="gui-input heure session-am" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_fin_1_4" class="gui-input heure session-am" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_deb_2_4" class="gui-input heure session-pm" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon prepend-icon-sm">
														<input type="text" name="h_fin_2_4" class="gui-input heure session-pm" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="row" id="modal_act_vehicule"  >
										<div class="col-md-12" >
											<div class="section" >
												<div class="select2-sm" >
													<select name="vehicule" id="vehicule" class="select2" >
													<option value="0" >Selectionnez un vehicule ...</option>
												</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row" id="modal_act_salle"  >
										<div class="col-md-12" >
											<div class="section" >
												<div class="select2-sm" >
													<select name="salle" id="salle" class="select2" >
													<option value="0" >Selectionnez une salle ...</option>
												</select>
												</div>
											</div>
										</div>
									</div>

									<!--COMMERCE -->
									<div class="section-divider mb40 bloc-client" style="display:none;" >
										<span>Commerce</span>
									</div>
									<div class="row bloc-client" style="display:none;" >
										<div class="col-md-12" >
											<div class="section" >
												<div class="select2-sm" >
													<select id="commercial" class="select2" name="commercial" required >
														<option value="" >Selectionnez un commercial ...</option>
													</select>
												</div>
												<small class="text-danger texte_required" id="commercial_required">
													Merci de selectionner un commercial
												</small>
											</div>
										</div>
									</div>
									<div class="row bloc-client" style="display:none;" >
										<div class="col-md-12" >
											<div class="section" >
												<div class="option-group field">
													<label class="option option-dark">
														<input type="checkbox" id="ca_nc" name="acl_ca_nc" value="on">
														<span class="checkbox"></span>Tarif inconnu
													</label>
												</div>
											</div>
										</div>
									</div>
									<!-- CA DE L'ACTION -->
									<div class="row bloc-client" style="display:none;" >
										<div class="section" id="cont_ca_intra" >
											<div class="col-md-12" >
												<label>CA Formation</label>
												<div class="field prepend-icon prepend-icon-sm">
													<input id="ca_intra" name="ca_intra" class="gui-input gui-input-sm numerique" type="number" value="0" min="0.00" step="0.01">
													<label class="field-icon" for="ca_intra">
														 <i class="fa fa-euro" ></i>
													</label>
												</div>
												<small class="text-danger texte_required" id="alert_min_intra"></small>
											</div>
										</div>
										<div class="section" id="cont_ca_inter" style="display:none;"  >
											<div class="col-md-6" >
												<label>CA / stagiaire</label>
												<div class="field prepend-icon prepend-icon-sm">
													<input id="ca_inter" name="ca_inter" class="gui-input gui-input-sm numerique" type="number" value="0" min="0.00" step="0.01">
													<label class="field-icon" for="ca_inter">
														 <i class="fa fa-euro" ></i>
													</label>
												</div>
												<small class="text-danger texte_required" id="alert_min_inter"></small>
											</div>
											<div class="col-md-6" >
												<label>Nb. Stagiaire(s)</label>
												<div class="field prepend-icon prepend-icon-sm">
													<input id="for_nb_sta" name="for_nb_sta" class="gui-input gui-input-sm numerique" type="number" value="1" min="1" step="1">
													<label class="field-icon" for="for_nb_sta">
														 <i class="fa fa-user" ></i>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- fin colonne 2 -->
							</div>
						</div>
						<div class="modal-footer">
							<div class="row" >
								<div class="col-md-12" >
									<button type="button" class="btn btn-success btn-sm" id="form_action_submit" >
										<i class="fa fa-save" ></i>
										Enregistrer
									</button>
									<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal" >
										<i class="fa fa-close" ></i>
										Annuler
									</button>
								</div>
							</div>
							<div class="row" >
								<div class="col-md-12" >
									<div id="modal_action_alert" style="display:none;" >
										<p class="text-center text-danger" ><strong></strong></p>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- MODAL CLIENT : AJOUT OU MOD DONNEE actions_clients -->
		<div id="modal_client" class="modal fade" role="dialog" >
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<form method="post" action="ajax/ajax_action_client.php" id="form_modal_client" >
						<input type="hidden" name="action" id="mc_action" value="0" />
						<input type="hidden" name="action_client" id="mc_action_client" value="0" />
						<input type="hidden" name="id_heracles" id="mc_id_heracles" value="0" />
						<div class="modal-body admin-form" >

							<div class="row" >
								<div class="row mt10" >
									<div class="col-md-11" >
										<label>Client :</label>
										<div class="select2-sm" >
											<select id="mc_client" class="select2 select2-client-n select2-produit-client" name="client" data-archive="0" data-blackliste="0" data-adr_type="1" >
												<option value='0' selected='selected' >&nbsp;</option>
											</select>
										</div>
										<small class="text-danger texte_required" id="mc_client_required">
											Merci de selectionner un client
										</small>
									</div>
									<div class="col-md-1 pt15" >
										<button type="button" class="btn btn-danger btn-sm" id="mc_btn_client_supp" >
											<i class="fa fa-times" ></i>
										</button>
									</div>
								</div>
							</div>

							<div class="row" >

								<!-- COLONNE GAUCHE -->
								<div class="col-md-6" >

									<div class="section-divider mb40" >
										<span>Lieu d'intervention</span>
									</div>

									<div class="row"  >
										<div class="col-md-12" >
											<div class="option-group field">
												<label class="option option-dark">
													<input type="checkbox" id="mc_intervention" name="act_adr_ref" value="1">
													<span class="checkbox"></span>Utiliser comme lieu d'intervention
												</label>
											</div>
										</div>
									</div>
									<div class="row mc-client-int"  >
										<div class="col-md-12" >
											<div class="section" >
												<select name="act_adresse" id="mc_adresse" class="select2" >
													<option value="0" >Selectionnez une adresse</option>
												</select>
											</div>
										</div>
									</div>
									<div class="mc-client-int" >
										<div class="row">
											<div class="col-md-12" >
												<input type="text" name="act_adr_nom" id="mc_adr_nom" class="gui-input nom champ_adresse" placeholder="Nom" />
												<input type="text" name="act_adr_service" id="mc_adr_service" class="gui-input champ_adresse" placeholder="Service" />
												<input type="text" name="act_adr1" id="mc_adr1" class="gui-input champ_adresse" placeholder="Adresse" />
												<input type="text" name="act_adr2" id="mc_adr2" class="gui-input champ_adresse" placeholder="Complément 1" />
												<input type="text" name="act_adr3" id="mc_adr3" class="gui-input champ_adresse" placeholder="Complément 2" />
											</div>
										</div>
										<div class="row">
											<div class="col-md-3" >
												<input type="text" name="act_adr_cp" id="mc_adr_cp" class="gui-input code-postal champ_adresse" placeholder="CP" />
											</div>
											<div class="col-md-9" >
												<input type="text" name="act_adr_ville" id="mc_adr_ville" class="gui-input nom champ_adresse" placeholder="Ville" />
											</div>
										</div>
									</div>
									<div class="mc-client-int" >
										<div class="section-divider mb40" >
											<span>Contact</span>
										</div>
										<div class="row mc-client-int" >
											<div class="col-md-12" >
												<div class="section" >
													<select name="act_contact" id="mc_contact" class="select2 contact" >
														<option value="0" >Selectionnez un contact</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row mc-client-int">
											<div class="col-md-6" >
												<input type="text" name="act_con_nom" id="mc_con_nom" class="gui-input nom champ-contact" placeholder="Nom" />
											</div>
											<div class="col-md-6" >
												<input type="text" name="act_con_prenom" id="mc_con_prenom" class="gui-input prenom champ-contact" placeholder="Prénom" />
											</div>
										</div>
										<div class="row mc-client-int">
											<div class="col-md-6" >
												<input type="text" name="act_con_tel" id="mc_con_tel" class="gui-input telephone champ-contact" placeholder="Téléphone" />
											</div>
											<div class="col-md-6" >
												<input type="text" name="act_con_portable" id="mc_con_portable" class="gui-input telephone champ-contact" placeholder="Portable" />
											</div>
										</div>
									</div>
								</div>


								<!-- COLONNE DROITE -->
								<div class="col-md-6" >

									<div class="section-divider mb40" >
										<span>Formation</span>
									</div>
									<div class="row" >
										<div class="col-md-6" >
											<div class="section" >
												 <label class="option">
													<input type="radio" id="mc_produit_intra" name="acl_pro_inter" class="select2-produit-intra produit-type" checked value="0" >
													<span class="radio"></span>Produit intra
												</label>
											</div>
										</div>

										<div class="col-md-6" >
											<div class="section" >
												 <label class="option">
													<input type="radio" id="mc_produit_inter" name="acl_pro_inter" class="select2-produit-inter produit-type" value="1" >
													<span class="radio"></span>Produit inter
												</label>
											</div>
										</div>
									</div>
									<div class="row" >
										<div class="col-md-12" >
											<div class="section" >
												<div class="select2-sm" >
													<label for="mc_produit" >Produit :</label>
													<select id="mc_produit" class="select2" name="acl_produit" data-famille="0" data-sous_famille="0" data-sous_sous_famille="0" >
														<option value="" >Selectionnez un produit ...</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row" >
										<div class="col-md-12" >
											<div class="section" >
												<label for="mc_pro_libelle" >Libellé de la formation :</label>
												<input type="text" class="gui-input gui-input-sm" id="mc_pro_libelle" name="acl_pro_libelle" value="" />
											</div>
										</div>
									</div>
									<div class="row" >
										<div class="col-md-12" >
											<div class="section" >
												<label class="option">
													<input type="checkbox" name="acl_confirme" value="on" />
													<span class="checkbox"></span>Action confirmée
												</label>
											</div>
										</div>
									</div>
									<div class="section-divider mb40" >
										<span>Commerce</span>
									</div>
									<div class="row" >
										<div class="col-md-12" >
											<div class="section" >
												<div class="select2-sm" >
													<select id="mc_commercial" class="select2" name="acl_commercial" >
														<option value="" >Selectionnez un commercial ...</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row" >
										<div class="col-md-12" >
											<div class="section" id="mc_cont_ca_intra" >
												<label>CA Formation</label>
												<div class="field prepend-icon prepend-icon-sm">
													<input id="mc_ca_intra" name="ca_intra" class="gui-input gui-input-sm numerique" type="number" value="0" min="0.00" step="0.01">
													<label class="field-icon" for="mc_ca_intra">
														 <i class="fa fa-euro" ></i>
													</label>
												</div>
											</div>
											<div class="section" id="mc_cont_ca_inter" style="display:none;"  >
												<label>CA / stagiaire</label>
												<div class="field prepend-icon prepend-icon-sm">
													<input id="mc_ca_inter" name="ca_inter" class="gui-input gui-input-sm numerique" type="number" value="0" min="0.00" step="0.01">
													<label class="field-icon" for="mc_ca_inter">
														 <i class="fa fa-euro" ></i>
													</label>
												</div>
											</div>
										</div>
									</div>


								</div>
								<!-- FIN COLONNE DE DROITE -->
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success btn-sm" id="mc_btn_submit" >
								<i class="fa fa-long-arrow-right" ></i>
								Enregistrer
							</button>
							<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div id="modal_action_comment" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Commentaire</h4>
					</div>
					<form method="post" action="ajax/ajax_set_action_comment.php" id="form_modal_action_comment" >
						<div>
							<input type="hidden" name="action" id="mdl_action_comment_id" value="0" />
						</div>
						<div class="modal-body admin-form">
							<div class="row" >
								<div class="col-md-4 text-right" >Texte planning :</div>
							</div>
							<div class="row" >
								<div class="col-md-12" >
									<input type="text" maxlength="50" id="mdl_action_comment_pla_txt" name="act_planning_txt" class="gui-input gui-input-sm" value="" />
								</div>
							</div>
							<div class="row mt10" >
								<div class="col-md-4 text-right" >Commentaire :</div>
							</div>
							<div class="row" >
								<div class="col-md-12 text-center" >
									<textarea name="act_commentaire" id="mdl_action_comment_comment" rows="6" cols="50" ></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
							<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal" >
								<i class="fa fa-close" ></i>
								Annuler
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div id="modal_horaire" class="modal fade" role="dialog" >
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form method="post" action="ajax/ajax_set_action_horaire.php" id="form_modal_horaire" >
						<div>
							<input type="hidden" name="action" value="0" />
						</div>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">horaires</h4>
						</div>
						<div class="modal-body">
							<div class="admin-form" >

								<table id="horaire" class="table" >
									<thead>
										<tr>
											<th class="text-center" >Intervenant</th>
											<th class="text-center" >Date</th>
											<th class="text-center" >Début</th>
											<th class="text-center" >Fin</th>
											<th class="text-center" >Test</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody id="horaire_body" >

									</tbody>
								</table>
							</div>

						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-success btn-sm"  >Enregistrer</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	<!-- modal maj adresse et contact -->
		<!--
		<div id="modal_action_adresse" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Lieu d'intervention et contact</h4>
					</div>
					<form method="post" action="ajax/ajax_set_action_adresse.php" id="form_modal_action_adresse" >
						<div>
							<input type="hidden" name="action" value="0" />
						</div>
						<div class="modal-body admin-form">


								<div class="row" >
									<div class="col-md-4" >
										<div class="section" >
											 <label class="option">
												<input type="radio" class="adr-ref adr-client" name="act_adr_ref" value="1" >
												<span class="radio"></span>Client
											</label>
										</div>
									</div>
									<div class="col-md-4" >
										<div class="section" >
											 <label class="option">
												<input type="radio" class="adr-ref adr-si2p" name="act_adr_ref" value="2" >
												<span class="radio"></span>Agence
											</label>
										</div>
									</div>
									<div class="col-md-4" >
										<div class="section" >
											 <label class="option">
												<input type="radio" class="adr-ref adr-autre" name="act_adr_ref" value="3" >
												<span class="radio"></span>Autres
											</label>
										</div>
									</div>
								</div>
								<div class="row cont-adr-client"  >
									<div class="col-md-12" >
										<div class="section" >
											<select name="act_adresse" class="select2 adresse-client-multi" >
												<option value="0" >Selectionnez une adresse</option>
											</select>
										</div>
									</div>
								</div>
							<!--
								<div class="cont-adresse" >
									<div class="row">
										<div class="col-md-12" >
											<input type="text" name="act_adr_nom" class="gui-input nom adr-nom" id="adr_nom" placeholder="Nom" />
											<input type="text" name="act_adr_service" class="gui-input adr-service" placeholder="Service" />
											<input type="text" name="act_adr1" class="gui-input adr1" placeholder="Adresse" />
											<input type="text" name="act_adr2" class="gui-input adr2" placeholder="Complément 1" />
											<input type="text" name="act_adr3" class="gui-input adr3" placeholder="Complément 2" />
										</div>
									</div>
									<div class="row">
										<div class="col-md-3" >
											<input type="text" name="act_adr_cp" class="gui-input code-postal champ_adresse adr-cp" placeholder="CP" />
										</div>
										<div class="col-md-9" >
											<input type="text" name="act_adr_ville" class="gui-input nom champ_adresse adr-ville" placeholder="Ville" />
										</div>
									</div>
								</div>


								<div class="cont-contact" >
									<div class="section-divider mb40" >
										<span>Contact</span>
									</div>
									<div class="row cont-contact-liste" >
										<div class="col-md-9" >
											<div class="section" >
												<select name="act_contact" class="select2 contact" >
													<option value="0" >Selectionnez un contact</option>
												</select>
											</div>
										</div>
										<div class="col-md-3" >
											<div class="section mt10" >
												 <label class="option">
													<input type="checkbox" name="act_contact_autre" class="contact-autre" >
													<span class="checkbox"></span>Autres
												</label>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6" >
											<input type="text" name="act_con_nom" class="gui-input nom champ-contact con-nom" placeholder="Nom" />
										</div>
										<div class="col-md-6" >
											<input type="text" name="act_con_prenom" class="gui-input prenom champ-contact con-prenom" placeholder="Prénom" />
										</div>
									</div>
									<div class="row">
										<div class="col-md-6" >
											<input type="text" name="act_con_tel" class="gui-input telephone champ-contact con-tel" placeholder="Téléphone" />
										</div>
										<div class="col-md-6" >
											<input type="text" name="act_con_portable" class="gui-input telephone champ-contact con-portable" placeholder="Portable" />
										</div>
									</div>
								</div>

						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
							<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal" >
								<i class="fa fa-close" ></i>
								Annuler
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>-->

		<div id="modal_confirmation_user" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Confirmer ?</h4>
					</div>
					<div class="modal-body">
						<p>Message</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							Annuler
						</button>
						<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
							Confirmer
						</button>
					</div>
				</div>
			</div>
		</div>
		<div id="confirmer_deplace" class="modal fade" role="dialog" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close annule_deplace_btn" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Confirmer ?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Êtes-vous sûr(e) de vouloir déplacer l'action au <span id="confirmer_deplace_date"></span> ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm annule_deplace_btn" data-dismiss="modal">
                            Annuler
                        </button>
                        <button type="button" class="btn btn-success btn-sm" id="confirmation_deplace_btn">
                            Confirmer
                        </button>
                    </div>
                </div>
            </div>
        </div>
		<div id="interco_attention" class="modal fade" role="dialog" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close annule_deplace_btn" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Attention</h4>
                    </div>
                    <div class="modal-body">
                        <p>Vous ne pouvez pas déplacer une action liée à une interco</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">
                            Ok
                        </button>
                    </div>
                </div>
            </div>
        </div>
		<!-- MODAL CASES -->
		<div id="modal_case" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Case libre</h4>
					</div>
					<div class="modal-body admin-form theme-primary">
						<div class="row" >
							<div class="col-md-12" >
								<label>Type de case :</label>
								<select id="mdl_case_id" class="select2" required >
									<option value="" selected='selected' >Type de case</option>
							<?php	if(!empty($case)){
										foreach($case as $p_c){
											echo("<option value='" . $p_c["pca_id"] . "' >" . $p_c["pca_libelle"] . "</option>");
										}
									} ?>
								</select>
								<small class="text-danger texte_required" id="mdl_case_id_required">
									Merci de selectionner un type de case.
								</small>
							</div>
						</div>
						<div class="row mt15">
							<div class="col-md-12" >
								<label>Libellé de la case :</label>
								<input type="text" id="mdl_case_txt" class="gui-input" placeholder="Libelle de la case" />
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row" >
							<div class="col-md-12" >
								<button type="button" class="btn btn-success btn-sm" id="sub_modal_case" >
									<i class="fa fa-save" ></i>
									Enregistrer
								</button>
								<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal" >
									<i class="fa fa-close" ></i>
									Annuler
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


<?php 	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<script type="text/javascript" src="assets/admin-tools/admin-forms/js/jquery-ui-monthpicker.min.js"></script>

		<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>

		<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
		<script type="text/javascript" src="/assets/js/custom.js"></script>
		<script type="text/javascript" src="/assets/js/sync_heracles.js"></script>

		<script type="text/javascript" >

			// VARIABLE GLOBAL
			var elt_source;
			var id_cible;
			var nb_demi_j=0; // nb de demi jour selectionné au moment ou on affiche le modal action
			var type_case=0;		// utiliser pour la recuperation de la selection utilisateur

			var case_click_drt;		// on memorise la case qui ouvre le m ctx pour les fonctions de traitement.
			var contenu_click_drt;	// on memorise le contenu equivalant.
			var action=0;			// id de l'action de référence determiné par le click droit
			var action_select=0;	// id de l'action selectionne via ctx_act_aff_date // verrouille certaine op et permet des traitements en 2 temps
	<?php	if(isset($_SESSION["action_coupe"])){ ?>
				var action_coupe=parseInt("<?=$_SESSION["action_coupe"]["action"]?>");
	<?php	}else{ ?>
				var action_coupe=0;
	<?php	} ?>


			var modal_actif_id=""	// certaine fonction peuvent être utilise sur des modals ou champ difféfrent
									// cette variable permet de rendre les fonction générique
			var modal_type=0;
			var tab_adresse=new Array(); // memorise les adresses dispo pour les utiliser sans repasser par une requete ajax
			var adresse_defaut=0;		// adresse par defaut du client selectionner
			var type_adresse=1;
			var tab_contact=new Array();
			var contact_defaut=0;		// contact par defaut du client selectionnee
			var client=0;
			var produit=0;
			var type_produit=0;
			var famille_produit=0;
			var categorie_produit=0;
			var int_agence=0;
			var client_categorie=0;
			var contenu_top=0;
			var contenu_left=0;
			var semaine=0;
			var annee=0;
			var id_source_global = "";
			var id_cible_global = "";
			var contenu_top_global = "";
			var contenu_left_global="";

			// memo parametre du produit selectionnée
			var ca_intra=0;
			var ca_inter_unit=0;
			var produit_type=0;
			var dragCheck = false;
			var h_agence=new Array();
			h_agence[0]="";
			h_agence[1]="";
			h_agence[2]="";
			h_agence[3]="";

			var tab_adresse=new Array();
			var tab_contact=new Array();



	<?php	/*foreach($horaire_agence as $k => $v){ ?>
				//h_agence[<?=$k?>]="<?=$v?>";
	<?php	}*/ ?>


			determineDropDirection();
			$(window).on('load resize scroll', determineDropDirection);

			$(document).ready( function() {

				// code requis pour select2 dans modal petit ecran
				$.fn.modal.Constructor.prototype.enforceFocus = function() {};
				// initialisation des event en fonction des classes affectées au case
				case_ini();

				// PERIODE D'AFFICHAGE PERSO
				$('#periode_mois').monthpicker();
				$('#periode_mois').change(function(){
					$("#periode_deb").val("");
					$("#periode_fin").val("");
				});
				$("#periode_deb").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=1) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},
					onSelect: function (input, inst){
						$("#periode_mois").val("");
						var date_fin=$('#periode_deb').datepicker( "getDate" );
						date_fin.setDate(date_fin.getDate() + 5);
						var max_fin=$('#periode_deb').datepicker( "getDate" );
						max_fin.setDate(max_fin.getDate() + 84);
						$("#periode_fin").datepicker( "setDate",date_fin);
						$("#periode_fin").datepicker( "option", "minDate", date_fin);
						$("#periode_fin").datepicker( "option", "maxDate", max_fin);
					}
				});
				$("#periode_fin").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=6) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},

				});

				//-------------------------------------
				// 	GESTION DES MENU CONTEXTUEL
				//------------------------------------

				// AFFICHAGE DES MENU CONTEXTUEL

				// zone planning
				$(document).on("contextmenu",".planning_case",function(e){


					$(".context-menu").hide();
					if(e.pageX/$(document).width()>0.75){
						$(".dropdown-submenu").addClass( "pull-left");
					}else{
						$(".dropdown-submenu").removeClass( "pull-left");
					}

					// on memorise la case qui ouvre le m ctx pour les fonctions de traitement.
					case_click_drt=$(this);
					// on memorise le contenu equivalant.
					contenu_click_drt=$(this).find("div");

					action=0; // on efface action à chaque ouverture de de m ctx il sera calculé si necessaire

					if($(this).hasClass("libre")&&$(this).hasClass("case_select")){
						if(action_coupe>0){
							$(".ctx-add").hide();
							$(".ctx-coller").show();
						}else{
							$(".ctx-add").show();
							$(".ctx-coller").hide();
						}
						$("#context_add").css({top:e.pageY,left:e.pageX}).show();

					}else if($(this).hasClass( "case_info" )&&$(this).hasClass("case_select")){

						$("#context-info").css({top:e.pageY,left:e.pageX}).show();

					}else if($(this).hasClass("action")){

						// clique droit sur une action
				<?php		if($_SESSION["acces"]["acc_droits"][32]){
							// NECESSITE LE DROIT ¨PLANNIFACTION ?>

							// on identifie l'action associé au clique droit
							if(contenu_click_drt.data("action")){
								action=contenu_click_drt.data("action");
							}

							if(action>0){
								// securite au cas ou l'id action ne pourrait pas etre recuperé
								if(action_select>0){
									// il y a un action selectionne

									if(action_select==action){
										// clique droit sur l'action selectionnée
										$("context_action_selected .selected").show();
									}else{
										$("context_action_selected .selected").hide();
									}
									if($(".case_select").length>0){
										$(".select-required").show();
										$(".no-select").hide();
									}else{
										$(".select-required").hide();
										$(".no-select").show();
									}
									$("#context_action_selected").css({top:e.pageY,left:e.pageX}).show();

								}else{

									if($(".case_select").length>0){
										$(".select-required").show();
										$(".no-select").hide();
									}else{
										$(".select-required").hide();
										$(".no-select").show();
									}

									// on positionne le menu contextuel sans l'affichacher

									$("#context_action").css({top:e.pageY,left:e.pageX});
									get_action_clients(action,0,construire_ctx_action,"","");
								}
							}
				<?php		} ?>

					}
					// desactive le comportement par défaut du clique droit
					e.preventDefault();
					determineDropDirection();
					return false;
				});

				// zone intervenant
				<?php	if($_SESSION["acces"]["acc_droits"][32]){	?>
					$(document).on("contextmenu",".intervenant",function(e){
						$(".context-menu").hide();
						if(e.pageX/$(document).width()>0.75){
							$(".dropdown-submenu").addClass( "pull-left");
						}else{
							$(".dropdown-submenu").removeClass( "pull-left");
						}
						action=0; // on efface action à chaque ouverture de de m ctx il sera calculé si necessaire
						intervenant=$(this).data("intervenant");
						date_deb=$(this).data("date_deb");
						int_agence=$(this).data("agence");
						semaine=$(this).data("semaine");
						annee=$(this).data("annee");
						$("#context_int").css({top:e.pageY,left:e.pageX}).show();
						e.preventDefault();
						return false;
					});
				<?php	} ?>


				// fermeture si clic hors zone
				$(document).on("contextmenu click","*:not(.context-case)",function(e){
					$(".context-menu").hide();
				});

				// INTERACTION CONTEXTUELLE

				//--- #context_add ---

				// ajout d'une nouvelle action
				$("#ctx_add_action").click(function(){
					categorie_produit=0;
					famille_produit=0;
					client=0;
					int_agence=0;
					// dates selectionner pour tester ressources
					selection=recuperer_case_select();
					get_action_ressources(selection,0,int_agence,afficher_modal_action,1,selection);
				});

				$(".add-case").click(function(){
					type_case=$(this).data("case");
					if(type_case==0){
						$("#modal_case").modal("show");
					}else{
						enregistrer_case(type_case,"");
					}
					$(".context-menu").hide();
				});

				// ajout des conges
				$(".add-conges").click(function(){
					type_conges=$(this).data("conges");
					enregistrer_conges(type_conges);
					$(".context-menu").hide();
				});

				// colle le contenu du presse papier sur le planning
				$("#ctx_act_coller").click(function(){
					action_coller();
					$(".context-menu").hide();
				});

				//--- #context_action ---
				// ATTENTION : les interations contextuelles du sous-menu client sont gerées de façon dynamique dans contruire_ctx_action()

				// .ctx-act-select Selection / deselect action pour operation en 2 tps + mise en evidence des dates
				/*$(".ctx-act-select").click(function(){
					$(".action-" + action).each(function(){
						if($(".action-" + action).hasClass("action-select")){
							$(".action-" + action).removeClass("action-select");
							action_select=0;
						}else{
							$(".action-" + action).addClass("action-select");
							action_select=action;
						}
						if(action_select==0){
							$(".case_select").removeClass("case_select");
						}
					});
					$(".context-menu").hide();
				});*/

				/*$(".ctx-act-comment").click(function(){
					get_action(action,afficher_modal_comment);
				});*/

				/*$(".ctx-act-adr").click(function(){
					get_action(action,afficher_modal_action,3);
				});*/

				/*$(".ctx-act-heure").click(function(){
					get_action_sessions(action,0,afficher_modal_horaire);
				});*/

				$(".ctx-act-archive").click(function(){
					modal_confirmation_user("Êtes-vous sûr de vouloir archiver cette formation ? Les inscriptions des clients seront également archivées.",archiver_action,0);
				});

				$(".ctx-act-add-case").click(function(){
					ajouter_date_action(action);
					$(".context-menu").hide();

				});

				// declenche la mise en presse-papier d'une action
				$(".ctx-act-couper").click(function(){
					couper_action(action);
					$(".context-menu").hide();
				});

				//--- #context_action_select ---

				// ajout de date sur une action existante
				$("#ctx_act_add_date").click(function(){
					ajouter_date_action(action);
					$(".context-menu").hide();
				});

				// transfert de client de l'action de ref vers nouvelle action
				$("#ctx_act_move_cli").click(function(){
					get_action_clients(action,0,ini_modal_client,"","");
					$(".context-menu").hide();
				});

				//--- #context-info #context_action_select ---
				$(".ctx-supp-date").click(function(){

					$("#modal-confirme #modal-text").text("Vous êtes sur le point d'éffacer les dates sélectionnées");
					$('#modal-confirme').modal('show');

					//$("#confirme_oui").off() si plusieur utilisation du modal confirme
					$("#confirme_oui").click(function(){
						var selection=recuperer_case_select();
						supprimer_dates(selection);
					});

					$(".context-menu").hide();
				});

				//--- #context_int ---

				// suppression d'un intervenant
				$("#ctx_int_supp").click(function(){
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer cette ligne de planning ?",del_planning_ligne,"");
					$(".context-menu").hide();
				});

				// selection
				$("#ctx_int_select").click(function(){
					select_planning_ligne();
					$(".context-menu").hide();
				});

				// *** FIN MENU CONTEXTUEL ***

				//*************************************
				//		INTERACTION PLANNNING (AUTRE QUE CONTEXT OU MODAL)
				//**************************************/

				// click sur un case action
				$(document).on("click",".action",function(e){
					if(dragCheck == false){
						// do the click action here...
						if(!$(this).find("div").hasClass("action-select")){
							// permet de desactiver la redirection lorque l'action est selectionnee
							action=$(this).find("div").data("action");
							document.location.href="action_voir.php?action=" + action;
						}else{
							selectionner_case($(this));
						}
					}
				});

				$(document).on("click",".click",function(e){
					selectionner_case($(this));
				});

				$(document).on("click",".conges",function(e){
					conges=$(this).find("div").data("conges");
					document.location.href="conges_voir.php?conge=" + conges;
				});

				if($("#btn_annul_coupe").length>0){
					$("#btn_annul_coupe").click(function(){
						couper_action(0);
					});

				}


				//*************************************
				//		INTERACTION MODAL ACTION
				//**************************************/

				$("#btn_client_supp").click(function(){
					$("#client").val(0).trigger("change");
				});

				// traitement en plus des actions déclenché par .get-client
				$("#client").change(function(){
					console.log("#client -> change");
					afficher_bloc_produit();
					afficher_bloc_client();
				});

				$(".adr-ref").change(function(){

					adr_ref=$('input[name=act_adr_ref]:checked').val();
					$(".champ-adresse").val("");
					$(".champ-contact").val("");

					if(adr_ref==1){
						// lieu = client
						if($("#adresse").val()>0){
							afficher_adresse($("#adresse").val());
						}
						if($("#contact").val()>0){
							afficher_contact($("#contact").val());
						}
						$(".bloc-adresse-client").show();
						$(".bloc-adresse").show();
						$(".bloc-contact-client").show();
						$(".bloc-contact").show();

					}else if(adr_ref==2){
						// agence
						$(".bloc-adresse-client").hide();
						$(".bloc-adresse").hide();
						$(".bloc-contact-client").hide();
						$(".bloc-contact").hide();


					}else if(adr_ref==3){
						// autre
						$(".bloc-adresse-client").hide();
						$(".bloc-adresse").show();
						$(".bloc-contact-client").hide();
						$(".bloc-contact").show();
					}

				});

				$("#adresse").change(function(){
					adresse=$(this).val();
					client=$("#client").val();
					afficher_adresse(adresse);
					if(client>0){
						// si adresse on prend contact associé sinon tous les contacts
						get_contacts(1,client,adresse,0,actualiser_contacts,"","");
					}

				});

				$("#contact").change(function(){
					afficher_contact($(this).val());
				});

				$(".produit-type").change(function(){
					afficher_bloc_produit();
				});

				$("#produit_famille").change(function(){
					get_sous_familles($(this).val(),afficher_sous_famille,$(this).val(),"");
				});

				$("#produit_s_famille").change(function(){
					get_sous_sous_familles($(this).val(),afficher_sous_sous_famille,$(this).val(),"");
				});

				$("#produit_s_s_famille").change(function(){
					afficher_bloc_produit();
				});

				$("#produit").change(function(){
					console.log("#produit -> change");
					produit=0;
					if($(this).val()!=null){
						produit=$(this).val();
					}
					client=$("#client").val();
					if(produit>0 && client>0){
						//client_categorie=$("#client_categorie").val();
						get_produit(client,produit,1,0,afficher_produit_planning,"","");
					}else{
						afficher_produit_planning();
					}

				});

				$("#nb_session").change(function(){
					if($(this).val()>4){
						$(this).val(4)
					}else if($("#produit").val()=="" || $("#produit").val()==0){
						$(this).val(1);
					}
					afficher_sessions($(this).val());
				});

				$("#nb_session").blur(function(){
					if($(this).val()>4){
						$(this).val(4)
					}else if($("#produit").val()=="" || $("#produit").val()==0){
						$(this).val(1);
					}
					afficher_sessions($(this).val());
				});

				$(".produit-type").click(function(){

					produit_type=$('input[name=produit_type]:checked').val();
					if(produit_type==1){
						// lieu = client
						$("#cont_ca_intra").show();
						$("#cont_ca_inter").hide();
					}else{
						$("#cont_ca_intra").hide();
						$("#cont_ca_inter").show();
					}
				});

				$("#ca_nc").click(function(){
					if($(this).is(":checked")){
						$("#ca_intra").val(0);
						$("#ca_intra").prop("disabled",true);
						$("#ca_inter").val(0);
						$("#ca_inter").prop("disabled",true);
					}else{
						$("#ca_intra").prop("disabled",false);
						$("#ca_inter").prop("disabled",false);
						if(produit_type==1){
							$("#ca_intra").val(ca_intra);
						}else{
							$("#ca_inter").val(ca_inter_unit);
						}
					}
				});

				$("#ca_intra").blur(function(){
					if($("#ca_min").val()>0){
						ca_min=parseFloat($("#ca_min").val());
						ca=parseFloat($("#ca_intra").val());
						if(ca<ca_min){
							$("#ca_intra").val(ca_min);
							$("#alert_min_intra").html("Tarif min à " + ca_min + "€");
							$("#alert_min_intra").show().fadeOut(5000);
						}
					}
				});

				$("#ca_inter").blur(function(){
					if($("#ca_min").val()>0){
						ca_min=parseFloat($("#ca_min").val());
						ca=parseFloat($("#ca_inter").val());
						if(ca<ca_min){
							$("#ca_inter").val(ca_min);
							$("#alert_min_inter").html("Tarif min à " + ca_min + "€");
							$("#alert_min_inter").show().fadeOut(5000);
						}
					}
				});

				// envoie du form de creation d'action
				$('#form_action_submit').click(function(){
					// on masque les alertes d'un envoie précédent
					$('#form_modal_action .texte_required').hide();

					/* desactive tant que synchro heracles pour cohérence synchro
					if($("#adresse_client").is(":checked") || $("#produit_intra").is(":checked") ){
						$("#client").prop("required",true);
					}else{
						$("#client").prop("required",false);
					}*/

					if($("#produit_s_s_famille option").size()>1){
						$("#produit_s_s_famille").prop("required",true);
						$("#produit_s_famille").prop("required",true);
					}else if($("#produit_s_famille option").size()>1){
						$("#produit_s_s_famille").prop("required",false);
						$("#produit_s_famille").prop("required",true);
					}else{
						$("#produit_s_s_famille").prop("required",false);
						$("#produit_s_famille").prop("required",false);
					}

					if($("#adresse_client").is(":checked") || $("#adresse_autre").is(":checked") ){
						$("#adr_cp").prop("required",true);
						$("#adr_ville").prop("required",true);
						if($("#adr_cp").val()=="" || $("#adr_ville").val()==""){
							$("#adr_cp_ville_required").show();
						}

					}else{
						$("#adr_cp").prop("required",false);
						$("#adr_ville").prop("required",false);
					}
					if(produit_type==1 || $("#client").val()>0 ){
						$("#produit").prop("required",true);
					}else{
						$("#produit").prop("required",false);
					}



					if(valider_form("#form_modal_action")){
						if($("#client").val()>0){
							action_sync("<?=$acc_societe?>","","#id_heracles",envoyer_form_action);
						}else{
							envoyer_form_ajax($("#form_modal_action"),ecrire_case);
							$('#modal_action').modal('hide');
						}

					}else{
						//alert("NON VALIDE");
					}
				});

				//*************************************
				//		INTERACTION MODAL_CLIENT
				//**************************************/

				$("#mc_btn_client_supp").click(function(){
					$("#mc_client").val(0).trigger("change");
				});

				$("#mc_client").change(function(){
					var client=$(this).val();
					if(client>0){
						get_client(client,"<?=$acc_societe?>",int_agence,1,1,actualiser_action_client,"","");
					}else{
						actualiser_action_client();
					}
				});

				$("#mc_intervention").click(function(){
					if($(this).is(":checked")){
						$(".mc-client-int").show();
					}else{
						$(".mc-client-int").hide();
					}
				});

				$("#mc_adresse").change(function(){
					adresse=$(this).val();
					client=$("#mc_client").val();
					afficher_mc_adresse(adresse);
					if(client>0){
						// si adresse on prend contact associé sinon tous les contacts
						get_contacts(1,client,adresse,0,actualiser_mc_contacts,"","");
					}

				});

				$("#mc_contact").change(function(){
					afficher_mc_contact($(this).val());
				});

				$("#mc_produit_inter").click(function(){
					if($(this).is(":checked")){
						$("#mc_cont_ca_intra").hide();
						$("#mc_cont_ca_inter").show();
					}
				});

				$("#mc_produit_intra").click(function(){
					if($(this).is(":checked")){
						$("#mc_cont_ca_intra").show();
						$("#mc_cont_ca_inter").hide();
					}
				});

				$("#mc_produit").change(function(){
					get_produit($("#mc_client").val(),$(this).val(),1,0,afficher_mc_produit,"","");
				});

				$("#mc_btn_submit").click(function(){
					if($("#mc_client").val()>0){
						if($("#mc_action_client").val()==0){
							//c'est une creation d'action client
							action_sync(<?=$acc_societe?>,"","#mc_id_heracles",envoyer_form_client,"","");
						}else{
							envoyer_form_ajax($("#form_modal_client"));
							$('#modal_client').modal('hide');
						}
					}
				});


				//*************************************
				//		INTERACTION MODAL CASE
				//**************************************/

				$("#sub_modal_case").click(function(){
					if($("#mdl_case_id").val()!=""){
						type_case=$("#mdl_case_id").val();
						lib_case=$("#mdl_case_txt").val();
						enregistrer_case(type_case,lib_case);
						$("#modal_case").modal("hide");
					}else{
						$("#mdl_case_id_required").show();
					}
				});

				//*************************************
				//		INTERACTION MODAL CONFIRMATION
				//**************************************/
				// annuler déplacer une action
				$(".annule_deplace_btn").click(function () {
					/* recharger(); */
					id_source_global.css("top",contenu_top);
					id_source_global.css("left",contenu_left);
				});
				$('#confirmer_deplace').on('hidden.bs.modal', function () {
					id_source_global.css("top",contenu_top);
					id_source_global.css("left",contenu_left);
				});
				// confirmer déplacer une action
				$("#confirmation_deplace_btn").click(function () {
					deplacer_case(id_source_global, id_cible);
					$("#confirmer_deplace").modal("hide");
				});
				//*************************************
				//		INTERACTION COMMENTAIRE (plus utiliser)
				//**************************************/

				/*$('#form_modal_action_comment').on('submit', function(e){
					e.preventDefault();
					envoyer_form_ajax($(this),maj_texte_survol);
					$("#modal_action_comment").modal("hide");

				});*/

				//*************************************
				//		INTERACTION ADRESSE (plus utiliser)
				//**************************************/
				// MODAL ACTION adresse
				/*$('#form_modal_action_adresse').on('submit', function(e){
					e.preventDefault();
					$('#form_modal_action_adresse input[name=action]').val(action);
					envoyer_form_ajax($(this),afficher_action_resultat);
					$("#modal_action_adresse").modal("hide");

				});*/

				//*************************************
				//		INTERACTION HORAIRES (plus utiliser)
				//**************************************/
				/*
				$('#form_modal_horaire').on('submit', function(e){
					e.preventDefault();
					$('#form_modal_horaire input[name=action]').val(action);
					envoyer_form_ajax($(this),afficher_action_resultat);
					$("#modal_horaire").modal("hide");

				});*/

			});
			//--------- FIN DE DOCUMENT READY ------------------


			//	-------------------------------------
			// 		FONCTION
			//	------------------------------------
			//	A - FONCTION INTERACTION PLANNING
			//	B - FONCTION PLANNING
			//	C - FONCTION MULTI MODAL
			//	D - FONCTION MODAL ACTION
			//	E - FONCTION MODAL CLIENT
			//	F- FONCTION GENERIQUE
			//	G- FONCTION MODAL ACTION COMMENT
			//	H- FONCTION MODAL ACTION ADRESSE */

			//************************************
			// 		A - FONCTION INTERACTION PLANNING
			//*************************************/

			 //reinitialise les event lié au case en fonction des classes (evite de recharger)

			function case_ini(){

				$(".libre").droppable({
					drop : function(event,ui){
						var id_source=elt_source;
                        id_cible=$(this).data("id");
						/* var id_source=elt_source;

						 */
						id_source_global = elt_source;
						id_cible=$(this).data("id");
						// INTERCO

						// ACTION DE REF
						if(elt_source.data("interco")){
							// peut etre déplacée
							var can_deplace = null;
							can_deplace = check_interco(id_cible);
							if(can_deplace == 1){
								$("#confirmer_deplace_date").text($(this).data("date"));
								$("#confirmer_deplace").modal();
							}else{

								$("#interco_attention").modal();
								id_source_global.css("top",contenu_top);
								id_source_global.css("left",contenu_left);
							}
						}else{
							$("#confirmer_deplace_date").text($(this).data("date"));
							$("#confirmer_deplace").modal();
						}


						/* deplacer_case(id_source,id_cible); */
					}
				});
				$('.drag').draggable({
					snap : ".libre",
					revert : 'invalid', // renvoi les bloc s'il ne sont pas depose en zone drop
					cursor : "move",
					zIndex: 9999,
					start : function(){
						elt_source=$(this);
						contenu_top=$(this).css("top");
						contenu_left=$(this).css("left");
					},
					drag: function(){
						// On drag set that flag to true
						dragCheck = true;
					},
					stop: function(){
						// On stop of dragging reset the flag back to false
						dragCheck = false;
					}
				});
			}


			// select ou deselection d'une case
			function selectionner_case(elt){
				if(action_select==0 || elt.find("div").hasClass("action-" + action_select) ){
					if(elt.hasClass( "case_select" )){
						// annule select
						elt.removeClass("case_select");
						if($(".case_select").length==0){
							case_ini();
						}
					}else{
						// select
						if($(".case_select").length==0){
							if(elt.hasClass( "case_info" )){
								$(".libre").off(); // .libre ne sont plus select
							}else{
								$(".case_info").off(); // .info ne sont plus select
							}
						}
						elt.addClass("case_select");
					}
				}
			}

			// retourne la liste des case selectionnées
			function recuperer_case_select(){
				var liste_case="";
				var ident_case="";
				nb_demi_j=$(".case_select").length;
				$(".case_select").each(function(){
					ident_case=$(this).data("id").replace("case_", "");
					liste_case=liste_case +  ident_case + ",";
					if($(this).data("agence")){
						int_agence=$(this).data("agence");
					}
				});
				return liste_case;
			}

			// ajouts des cases sur le planning suite à une requete ajax
			function ecrire_case(json){
				console.log("ecrire_case()");
				if(json.erreur_type==1){
					// erreur type danger -> pas de création
					afficher_message("Erreur","danger",json.erreur_text);
					if(json.erreur_code==2){
						// les dates ne sont plus libre -> on recharge le planning
						setTimeout(recharger,3000);
					}
				}else{

					if(json.erreur_type==2){
						modal_alerte_user(json.erreur_text);
					}
					if(json.erreur_code==2){
						setTimeout(recharger,3000);
					}

					data_case=json.cases;
					if(data_case.date.length>0){
						var classe="";
						var data_contenu="";
						var class_contenu="";
						data_case["type"] = parseInt(data_case["type"]);
						switch(data_case["type"]){
							case 0:
								classe="";
								break;
							case 1:
								classe=" action";
								class_contenu="drag action-" + data_case["ref_1"];
								data_contenu=" data-action='" + data_case["ref_1"] + "'";
								break;
							case 2:
								classe=" conges";
								break;
							case 3:
								classe=" case_info click";
								class_contenu = " drag";
								break;
						}

						for(i=0;i<data_case.date.length;i++){

							var contenu="";
							var case_id=data_case["date"][i];
							var case_style=data_case["style"];
							var up_style_act=false;
							if(data_case["up_style_act"]){
								up_style_act=data_case["up_style_act"];
							}
							

							if(data_case["type"]==2){
								if(data_case["spe_ref_1"][i]!=""){
									data_contenu=" data-conges='" + data_case["spe_ref_1"][i] + "'";
								}
								if(data_case["spe_style"][i]!=""){
									case_style=data_case["spe_style"][i];
								}
							};
							case_contenu="<div class='drag " + class_contenu + " contenu_" + case_id + "' style='" + case_style + "' " + data_contenu + " data-id='" + case_id + "' data-toggle='tooltip' data-placement='top' title='" + data_case["survol"] + "'>";
							case_contenu+=data_case["texte"];
							case_contenu+="</div>";

							if(data_case["type"]==1){
								$(".case_" + case_id).removeClass("click");
							}
							$(".case_" + case_id).removeClass("case_select");	// la case n'est plus select
							$(".case_" + case_id).removeClass("libre"); 		// la case n'est plus libre
							$(".case_" + case_id).off();						// on supprime les ecouteurs = interaction possible

							$(".case_" + case_id).html(case_contenu);
							$(".case_" + case_id).addClass(classe);
							$(".contenu_" + case_id).tooltip();

							if(up_style_act){
								$(".action-" + data_case["ref_1"]).prop("style",case_style);
							}
						}
						case_ini();
					}else{
						//alert("PAS DE DATE");
					}
				}
			}

			// vide une case pour la rendre disponible
			function case_vide(case_id){
				$(".contenu_" + case_id).remove();
				$(".case_" + case_id).html("&nbsp;");
				$(".case_" + case_id).removeClass("action");
				// la case devient cliquable
				$(".case_" + case_id).addClass("libre");
				$(".case_" + case_id).addClass("click");
			}

			// déplace une case
			function deplacer_case(id_source,id_cible){
				// id_source = object jquery


				$.ajax({
					type:'GET',
					url: 'ajax/ajax_move_case.php',
					data : 'id_source=' + id_source.data("id") + "&id_cible=" + id_cible,
					dataType: 'json',
					success: function(data)
					{

						if(data.erreur_type==1){
							afficher_message("Erreur","danger",data.erreur_text);
							if(data.erreur_code==2)	{
								setTimeout(recharger,3000);
							}
							id_source.css("top",contenu_top);
							id_source.css("left",contenu_left);
						}else{

							case_vide(id_source.data("id"));
							ecrire_case(data);
							case_ini();

						}
					},
					error: function(data){
						afficher_message("Erreur","danger",data.responseText);
					}
				});
			}

			// Suppression d'une ligne de planning
			function del_planning_ligne(){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_del_planning_ligne.php',
					data : 'intervenant=' + intervenant + "&date_deb=" + date_deb + "&agence=" + int_agence,
					dataType: 'json',
					success: function(data)
					{	if(data){
							$("#int_" + data.intervenant + "_" + data.annee + "_" + data.semaine + "_" + data.agence).remove();
						}
					},
					error: function(data) {
						if(data){
							modal_alerte_user(data.responseText);
						}

					}
				});
			}

			function select_planning_ligne(){
				$("#int_" + intervenant + "_" + annee + "_" + semaine + "_" + int_agence + " .libre").each(function( i ){
					selectionner_case($(this));
				});

			}

			// requete ajax pour enregistrer les dates selectionnés par l'utilisateur
			function enregistrer_case(type_case,lib_case){
				var liste_case=recuperer_case_select();
				$.ajax({
					type:'GET',
					url: 'ajax/ajax_add_case.php',
					data : "type_case=" + type_case + "&liste_case=" + liste_case + "&lib_case=" + lib_case,
					dataType: 'json',
					success: function(data)
					{	ecrire_case(data);
						case_ini();
					},
					error: function() {

					}
				});
			};

			// requete ajax pour enregistrer des demandes de congés
			function enregistrer_conges(type_conges){
				var liste_case=recuperer_case_select();
				$.ajax({
					type:'GET',
					url: 'ajax/ajax_add_conges.php',
					data : "type_conges=" + type_conges + "&liste_case=" + liste_case,
					dataType: 'json',
					success: function(data)
					{	ecrire_case(data);
						case_ini();
					},
					error: function(){

					}
				});
			};

			// check interco

			function check_interco(id){
				$.ajax({
					type:'GET',
					async: false,
					url: 'ajax/ajax_check_interco.php',
					data : "id=" + id,
					dataType: 'html',
					success: function(data)
					{
						can_deplace = data;

					},
					error: function(){

					}
				});
				return can_deplace;
			};

			// supprimer des dates du plannings
			// liste_date au format int_date-sql_demi
			function supprimer_dates(liste_case){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_del_planning_dates.php',
					data : 'liste_case=' + liste_case,
					dataType: 'JSON',
					success: function(json)
					{	if(json.erreur_type==1){
							// erreur type danger -> pas de création
							afficher_message("Erreur","danger",json.erreur_text);
							if(json.erreur_code==2){
								// l'erreur nécéssite un rechargement du planning
								setTimeout(recharger,3000);
							}
						}else{
							if(json.erreur_type==2){
								afficher_message("Attention","warning",json.erreur_text);
							}
							if(json.cases.length>0){
								for(i=0;i<json.cases.length;i++){
									case_id=json["cases"][i];
									$(".case_" + case_id).removeClass("case_select");
									$(".case_" + case_id).removeClass("case_info");
									case_vide(case_id);
								}
								case_ini();
							}
						}
					},
					error: function() {
						afficher_message("Erreur","danger",json.responseText);
					}
				});
			}

			// ajouter des dates à une action de reference
			function ajouter_date_action(action){
				var liste_case=recuperer_case_select();
				if(action>0 && liste_case!=""){
					$.ajax({
						type:'POST',
						url: 'ajax/ajax_action_date_add.php',
						data : 'action=' + action + "&liste_case=" + liste_case,
						dataType: 'json',
						success: function(json)
						{	ecrire_case(json);
							action=0;
							case_ini();
						},
						error: function(data){
							afficher_message("Erreur","danger",data.responseText);
						}
					});
				}
			}

			// contruction du menu contextuel suite clic droit action
			function construire_ctx_action(json){

				var liste_client="";

				$.each(json, function(key, val)
				{	liste_client+="<li class='dropdown-submenu' >";
						liste_client+="<a href='javascript:void(0)' >";
							liste_client+=val.code;
						liste_client+="</a>";
						liste_client+="<ul class='dropdown-menu' style='max-height:300px;overflow-y:scroll'>";
							// liste_client+="<li><a href='#' class='ctx-act-mod-cli' data-action_client='" + val.action_client + "' >Modifier</a></li>";
					<?php	if($_SESSION['acces']["acc_droits"][10]){ ?>
								liste_client+="<li><a href='javascript:void(0)' class='ctx-act-arch-cli' data-action_client='" + val.action_client + "' >Archiver</a></li>";
					<?php	} ?>
							liste_client+="<li><a href='client_voir.php?client=" + val.client + "' >Voir la fiche</a></li>";
						liste_client+="</ul>";
					liste_client+="</li>";

				});

				liste_client+="<li>";
					liste_client+="<a href='javascript:void(0)' id='ctx_act_add_cli' >";
						liste_client+="<i class='fa fa-add' ></i>";
						liste_client+="Ajouter un client";
					liste_client+="</a>";
				liste_client+="</li>";

				$("#ctx_act_cli").html(liste_client);

				// ajout des events lié aux éléments ajouter après le chargement du document
				// $(".ctx-act-mod-cli").click(function(){
				// 	alert("CHAT");
				// PAS GERE
				// });
				$(".ctx-act-arch-cli").click(function(){
					action_client=$(this).data("action_client");
					modal_confirmation_user("Êtes-vous sûr de vouloir archiver l'action de ce client ?",archiver_action_client,action_client);

				});

				// ajout d'un nouveau client
				$("#ctx_act_add_cli").click(function(){
					action_client=0;
					if(action>0){
						get_action(action,ini_modal_client);
						$(".context-menu").hide();
					}
				});

				// affichage du menu contextuel
				$("#context_action").show();

			}

			function recharger(){
				location.reload();
			}

			// archive une action
			function archiver_action(){
				if(action>0){
					$.ajax({
						type:'POST',
						url: 'ajax/ajax_action_archive_add.php',
						data : "action=" + action,
						dataType: 'json',
						success: function(data){
							effacer_action(data);
						},
						error: function(data) {
							modal_alerte_user(data.responseText);
						}
					});
				}
			}

			// traite le retour suite a un archivage pour effacer les dates du planning
			function effacer_action(json){
				if(json.action>0){
					$(".action-" + json.action).each(function(){
						case_id=$(this).data("id");
						case_vide(case_id);
					});
					if(json.notif){
						afficher_message("Enregistrement terminé","success",json.notif);
					}
				}
			}

			function archiver_action_client(action_client){
				if(action>0&&action_client>0){
					$.ajax({
						type:'POST',
						url: 'ajax/ajax_action_client_archive_add.php',
						data : "action=" + action + "&action_client=" + action_client,
						dataType: 'json',
						success: function(data){
							afficher_message("Enregistrement terminé","success",data.notif);
						},
						error: function(data) {
							modal_alerte_user(data.responseText);
						}
					});
				}
			}

			// mise en cache de l'action devant être coupé
			function couper_action(action){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_action_couper.php',
					data : "action=" + action,
					dataType: 'json',
					success: function(data){
						if(action>0){

							action_coupe=action;

							// on affiche l'action dans le presse papier
							footer_dialog="<div class='text-center' >";
								footer_dialog=footer_dialog + "Presse-Papier : <b>Action N° " + data.action + "</b> " + data.texte + " " + data.survol + " " + data.nb_demi_j + " demi-journée(s).";
								footer_dialog=footer_dialog + "<div class='pull-right' >";
									footer_dialog=footer_dialog + "<button type='button' class='btn btn-sm btn-danger' data-toggle='tooltip' title='Annuler' id='btn_annul_coupe' >";
										footer_dialog=footer_dialog + "<i class='fa fa-times' ></i>";
									footer_dialog=footer_dialog + "</button>";
								footer_dialog=footer_dialog + "</div>";
							footer_dialog=footer_dialog + "</div>";
							$("#footer_dialog").html(footer_dialog);

							afficher_message("","success","L'action a bien été couper");

							$("#btn_annul_coupe").click(function(){
								couper_action(0);
							});
						}else{
							action_coupe=0;
							$("#footer_dialog").html("");
							afficher_message("","success","Opération annulée");

						}

					},
					error: function(data) {
						modal_alerte_user(data.responseText);
					}
				});

			}

			function action_coller(){
				liste_case=recuperer_case_select();
				if(liste_case!=""){
					$.ajax({
						type:'POST',
						url: 'ajax/ajax_action_coller.php',
						data: "liste_case=" + liste_case,
						dataType: 'json',
						success: function(data){

							if(data.erreur_type==1){								
								if(data.erreur_code==2){
									afficher_message("Erreur","danger",data.erreur_text);
									setTimeout(recharger,3000);
								}else{
									modal_alerte_user(data.erreur_text);
								}
							}else{

								// on efface l'action
								if($(".action-" + data["cases"]["ref_1"]).length>0){
									$(".action-" + data["cases"]["ref_1"]).each(function(){
										case_id=$(this).data("id");
										case_vide(case_id);
									});
								};
								// on efface le presse papier
								$("#footer_dialog").html("");
								action_coupe=0;

								// on ecrit les nouvelles dates
								ecrire_case(data);
							}

						},
						error: function(data) {
							modal_alerte_user(data.responseText);
						}
					});
				}else{
					modal_alerte_user("Aucune date selectionnée.");
				}
			};


			//----- OK ----

			// retourne l'id du premier intervenant
			function recuperer_intervenant_select(){
				date_deb=null;
				demi_deb=1;
				intervenant=0;
				$(".case_select").each(function(){

					tab_case=(this).id.split("_");
					tab_intervenant=tab_case[1];
					tab_date_deb=tab_case[2].split("-");
					date_compare=new Date(tab_date_deb[0],tab_date_deb[1]-1,tab_date_deb[2], 0, 0, 0, 0);
					tab_demi=tab_case[3];

					if(date_compare<date_deb || date_deb==null ){
						date_deb=date_compare;
						demi_deb=tab_demi;
						intervenant=tab_intervenant;
					}else if(date_compare.getTime()===date_deb.getTime()){
						date_deb=date_compare;
						demi_deb=tab_demi;
						intervenant=tab_intervenant;
					}
				});
				return intervenant;
			}




			//************************************
			// 		D - FONCTION MODAL ACTION
			//*************************************/

			// D-1 FONCTION SPE PLANNING

			// traite les données de ajax_get_client dé
			function actualiser_get_client(json){
				if(json){
					if(json.erreur_txt!=""){
						afficher_message("Erreur","danger",json.erreur_txt);
					}else{

						// on memorie les adresses
						if(json["adr_int"]!=""){
							$.each(json["adr_int"], function(i, ad){
								tab_adresse[ad.id]=new Array(8);
								tab_adresse[ad.id]["nom"]=ad.nom;
								tab_adresse[ad.id]["service"]=ad.service;
								tab_adresse[ad.id]["ad1"]=ad.ad1;
								tab_adresse[ad.id]["ad2"]=ad.ad2;
								tab_adresse[ad.id]["ad3"]=ad.ad3;
								tab_adresse[ad.id]["cp"]=ad.cp;
								tab_adresse[ad.id]["ville"]=ad.ville;
								tab_adresse[ad.id]["geo"]=ad.geo;
							});
						}
						if($("#adresse_client").is(":checked")){
							// on selectionne l'adresse par defaut
							actualiser_select2(json.adr_int,"#adresse",json.int_defaut);
						}else{
							actualiser_select2(json.adr_int,"#adresse",0);
							$("#adresse").val("").trigger("change");
						}
						$("#commercial").val(json.commercial).trigger("change");
						client_categorie=json.categorie;
					}
				}else{
					// PAS DE CLIENT
					actualiser_select2("","#adresse",0);
					$("#adresse").val("").trigger("change");
					actualiser_select2("","#contact",0);
					$("#contact").val("").trigger("change");

				}
			}

			// initialise les champ du modal selon le context et affiche
			function afficher_modal_action(json,param_modal_type,liste_case){
				console.log("afficher_modal_action()");
				console.log("param_modal_type : " + param_modal_type);
				/* JSON defini = action connu
					modal_type -> type d'action réalisé par le modal
					modal_type= 1 : ajout d'un client
					modal_type= 2 : mise à jour de l'adresse d'inter et contact
					modal_type= 3 : modification de l'adresse
				*/
				if(param_modal_type==1){

					if(json.erreur_num){
						// la requete a detecté une erreur nécéssitant un action sur le planning

						if(json.erreur_num=="1"){
							afficher_message("Dates non disponibles","danger",json.erreur_txt);
							setInterval(recharger, 5000);
						}

					}else{

						// NOUVELLE ACTION

						// ini la valeur de type produit
						produit_type=$('input[name=produit_type]:checked').val();

						// on memorise les dates selectionnées dans le formulaire
						$("#liste_case").val(liste_case);
						$("#agence").val(int_agence);

						// on affecte les vehicules dispo
						if(json.vehicules!=""){
							actualiser_select2(json.vehicules,"#vehicule",0);
							$("#modal_act_vehicule").show();
						}else{
							$("#vehicule").val(0);
							$("#modal_act_vehicule").hide();
						}
						// on affecte les salles dispo
						if(json.salles!=""){
							actualiser_select2(json.salles,"#salle",0);
							$("#modal_act_salle").show();
						}else{
							$("#salle").val(0);
							$("#modal_act_salle").hide();
						}

						// on affect les commerciaux
						actualiser_select2(json.commerciaux,"#commercial",0);

						// on affect l'agence pour la recherche de client
						$("#agence").val(json.agence);

						// on memorise les horaires par défaut
						h_agence[0]=json.horaires[0];
						h_agence[1]=json.horaires[1];
						h_agence[2]=json.horaires[2];
						h_agence[3]=json.horaires[3];
						afficher_horaire_agence();

						$(".select2-produit-fam").val(0);

						$("#cont_client_search").show();

						$("#modal_action .modal-dialog").addClass("modal-lg");
						$("#modal_act_col_1").show();
						$("#modal_act_col_1").removeClass("col-md-12");
						$("#modal_act_col_1").addClass("col-md-6");

						$("#modal_act_col_2").show();
						$("#modal_act_col_2").removeClass("col-md-12");
						$("#modal_act_col_2").addClass("col-md-6");
						$("#modal_act_vehicule").show();
						if($(".case_select.case_demi_1").length>0){
							$(".session-am").prop("disabled",false);
						}else{
							$(".session-am").prop("disabled",true);
						}
						if($(".case_select.case_demi_2").length>0){
							$(".session-pm").prop("disabled",false);
						}else{
							$(".session-pm").prop("disabled",true);
						}
						$("#modal_act_session").show();


						$("#modal_action .modal-title").html("Nouvelle action");

						$(".context-menu").hide();

						$("#cont_commentaire").show();

						$('#modal_action').modal('show');
					}

				}else if(param_modal_type==2){

						// ajout d'un client

						// restriction lors de la recherche du produit sur la famille de l'action
						$(".select2-produit-fam").val(json.act_pro_famille);

						$("#modal_action .modal-dialog").removeClass("modal-lg");
						$("#cont_client_search").show();
						$("#modal_act_col_1").hide();

						$("#modal_act_col_2").removeClass("col-md-6");
						$("#modal_act_col_2").addClass("col-md-12");
						$("#modal_act_vehicule").hide();
						$("#modal_act_session").hide();
						$("#modal_act_col_2").show();

						$("#modal_action .modal-title").html("Nouveau client");
						$(".context-menu").hide();
						$('#modal_action').modal('show');

				}else if(param_modal_type==3){

					// MAJ ADRESSES ET CONTACT

					if(json.adresses){
						actualiser_adresses(json.adresses,"");
						if(json.act_adr_ref==1){
							actualiser_contacts(json.contacts,"");
						}
					}

					// INI INPUT

					if(json.act_adr_ref==1){
						$("#adresse_client").prop("checked",true);
						$("#adresse").val(json.act_adresse);
					}else if(json.act_adr_ref==2){
						$("#adresse_agence").prop("checked",true);
						$("#adresse").val("");
					}if(json.act_adr_ref==3){
						$("#adresse_autre").prop("checked",true);
						$("#adresse").val("")
					}

					$(".adr-nom").val(json.act_adr_nom);
					$(".adr-service").val(json.act_adr_service);
					$(".adr-ad1").val(json.act_adr1);
					$(".adr-ad2").val(json.act_adr2);
					$(".adr-ad3").val(json.act_adr3);
					$(".adr-cp").val(json.act_adr_cp);
					$(".adr-ville").val(json.act_adr_ville);

					$(".con-nom").val(json.act_con_nom);
					$(".con-prenom").val(json.act_con_prenom);
					$(".con-tel").val(json.act_con_tel);
					$(".con-portable").val(json.act_con_portable);


					// aff / mas des blocs
					$("#modal_action .modal-dialog").removeClass("modal-lg");
					$("#cont_client_search").hide();
					$("#modal_act_col_1").show();
					$("#modal_act_col_1").removeClass("col-md-6");
					$("#modal_act_col_1").addClass("col-md-12");
					$("#modal_act_col_2").hide();
					$("#modal_action .modal-title").html("Lieu d'intervention et contact");

					$(".context-menu").hide();
					$('#modal_type').val(3);

					$("#cont_commentaire").hide();

					$('#modal_action').modal('show');

				}

			}


			function afficher_produit_planning(json){
				console.log("afficher_produit_planning()");

				if(json){
					// CA
					if(produit_type==1){

						coeff_j=0;
						if(json.nb_demi_jour!=0){
							coeff_j=nb_demi_j/json.nb_demi_jour;
						}
							// intra
						ca_intra=json.ht_intra*coeff_j;
						$("#ca_intra").val(json.ht_intra*coeff_j);
						$("#ca_min").val(json.cpr_min_intra*coeff_j);
						$("#ca_inter").val(0);
						$("#nb_sta_inter").val(0);
						if(json.ca_verrou_intra){
							$("#ca_intra").prop("readonly",true);
							$("#ca_nc").prop("checked",false);
							$("#ca_nc").prop("disabled",true);
						}else{
							$("#ca_intra").prop("readonly",false);
							$("#ca_nc").prop("disabled",false);
						}
					}else{
							// inter
						ca_inter_unit=json.ht_inter;
						$("#ca_inter").val(ca_inter_unit);
						$("#ca_min").val(json.cpr_min_inter);
						$("#nb_sta_inter").val(1);
						$("#ca_intra").val(0);

						if(json.ca_verrou_inter){
							$("#ca_inter").prop("readonly",true);
							$("#ca_nc").prop("checked",false);
							$("#ca_nc").prop("disabled",true);
						}else{
							$("#ca_inter").prop("readonly",false);
							$("#ca_nc").prop("disabled",false);
						}
					}

					//if(json.nb_demi_jour>1){
					//	$("#nb_session").val(1).trigger("change");
					//	$("#nb_session").prop("readonly",true);
					//}else{
					//	$("#nb_session").prop("readonly",false);
					//}

					$("#produit_libelle").val(json.libelle);
					$("#facturation").val(json.facturation);
				}else{

					$("#ca_intra").val(0);
					$("#ca_inter").val(0);
					$("#produit_libelle").val("");
					$("#facturation").val(0);
					$("#ca_nc").prop("checked",false);
					$("#ca_nc").prop("disabled",false);
					$("#ca_inter").prop("disabled",false);
					$("#ca_intra").prop("disabled",false);
					ca_intra=0;
					ca_inter_unit=0;
				}
			}

			// affiche les zones de saisie en fonction du nombre de session
			function afficher_sessions(nb_session){
				for(i=1;i<=4;i++){
					if(i<=nb_session){
						$(".session-" + i).show();
					}else{
						$(".session-" + i).hide();
					}
				}
				if(nb_session==1 && $("#adresse_agence").is(":checked")){
					afficher_horaire_agence();
				}
			}

			function afficher_horaire_agence(){
				if($("#nb_session").val()==1){
					$("#h_deb_matin_1").val(h_agence[0]);
					$("#h_fin_matin_1").val(h_agence[1]);
					$("#h_deb_ap_1").val(h_agence[2])
					$("#h_fin_ap_1").val(h_agence[3])
				}
			}

			// mise a jour des case du planning suite à l'ajout d'un client
			function maj_case_action_client(json){


			}

			function afficher_adresse(adresse){
				if(adresse>0){
					$("#adr_nom").val(tab_adresse[adresse]["nom"]);
					$("#adr_service").val(tab_adresse[adresse]["service"]);
					$("#adr1").val(tab_adresse[adresse]["ad1"]);
					$("#adr2").val(tab_adresse[adresse]["ad2"]);
					$("#adr3").val(tab_adresse[adresse]["ad3"]);
					$("#adr_cp").val(tab_adresse[adresse]["cp"]);
					$("#adr_ville").val(tab_adresse[adresse]["ville"]);
					if(tab_adresse[adresse]["geo"]>0){
						$("#adr_geo" + tab_adresse[adresse]["geo"]).prop("checked",true);
					}

					$("#bloc_adr_maj").show();

					$("#adr_add").prop("checked",false);
					$("#bloc_adr_add").hide();

				}else{
					$(".champ-adresse").val("");
					$("#adr_maj").prop("checked",false);
					$("#bloc_adr_maj").hide();

					$("#bloc_adr_add").show();
				}
			}

			// traitement suite à la recuperation des contacts liés à l'adresse
			function actualiser_contacts(json){
				if(json){
					// on memorie les conatcts
					var con_defaut=0;
					tab_contact=new Array();
					$.each(json, function(i, con){
						tab_contact[con.id]=new Array();
						tab_contact[con.id]["nom"]=con.nom;
						tab_contact[con.id]["prenom"]=con.prenom;
						tab_contact[con.id]["tel"]=con.tel;
						tab_contact[con.id]["portable"]=con.portable;
						tab_contact[con.id]["mail"]=con.mail;
						if(con.defaut==1){
							 con_defaut=con.id;
						}
					});
					// actualisation du select contact
					actualiser_select2(json,"#contact",con_defaut);
					if(con_defaut==0){
						// securite si 0 pas #contact.change
						$("#contact").val(0).trigger("change");
					}
				}else{
					actualiser_select2("","#contact",0);
					$("#contact").val(0).trigger("change");
					$("#contact").prop("disabled",true);
				}
				if(client_categorie==3){
					$("#contact").prop("disabled",true);
				}else{
					$("#contact").prop("disabled",false);
				}
			}

			function afficher_contact(contact){
				if(contact>0){
					$("#con_nom").val(tab_contact[contact]["nom"]);
					$("#con_prenom").val(tab_contact[contact]["prenom"]);
					$("#con_tel").val(tab_contact[contact]["tel"]);
					$("#con_portable").val(tab_contact[contact]["portable"]);
					$("#con_mail").val(tab_contact[contact]["mail"]);

					$("#bloc_con_maj").show();

					$("#con_add").prop("checked",false);
					$("#bloc_con_add").hide();

				}else{
					$(".champ-contact").val("");
					$("#con_maj").prop("checked",false);
					$("#bloc_con_maj").hide();
					$("#bloc_con_add").show();
				}
			}

			function afficher_sous_famille(json,famille){
				$("#produit_s_famille option[value!='0'][value!='']").remove();
				if(json!=""){
					//il y a des sous famille
					$("#produit_s_famille").select2({
						data: json,
						closeOnSelect: true
					});

					$("#bloc_s_famille").show();
				}else{
					$("#bloc_s_famille").hide();
				}
				$("#produit_s_famille").trigger("change");


			}

			function afficher_sous_sous_famille(json,sous_famille){
					$("#produit_s_s_famille option[value!='0'][value!='']").remove();
					if(json!=""){
						//il y a des sous sous famille
						$("#produit_s_s_famille").select2({
							data: json,
							closeOnSelect: true
						});
						$("#bloc_s_s_famille").show();
					}else{
						$("#bloc_s_s_famille").hide();
					}
					$("#produit_s_s_famille").trigger("change");
				}

			// on verifie si le système dispose des infos pour afficher les données produits
			function afficher_bloc_produit(){
				console.log("afficher_bloc_produit()");
				if($("#client").val()!="" && $("#client").val()!=0 && $("#produit_famille").val()!="" && $("#produit_s_famille").val()!="" && ($("#produit_s_s_famille").val()!="" || $("#produit_s_s_famille option").size()==1) ){

					var client=$(".select2-produit-client").val();
					var categorie=$(".select2-produit-cat").val();
					var famille=$(".select2-produit-fam").val();
					var sous_famille=$(".select2-produit-sous-fam").val();
					var sous_sous_famille=$(".select2-produit-sous-sous-fam").val();
					var intra=$(".select2-produit-intra").is(":checked");
					var inter=$(".select2-produit-inter").is(":checked");


					get_produits(client,categorie,famille,sous_famille,sous_sous_famille,intra,inter,actualiser_bloc_produit,"","");
				}else{
					$("#produit option[value!='0'][value!='']").remove();
					$(".bloc-produit").hide();
				}
			}

			// on actualise les données produits d'apres le retour ajax
			function actualiser_bloc_produit(json){
				console.log("actualiser_bloc_produit()");
				$("#produit option[value!='0'][value!='']").remove();
				$("#produit").select2({
					data: json,
					closeOnSelect: true
				});
				$("#produit").val("").trigger("change");
				$(".bloc-produit").show();
				get_attestations($("#produit_famille").val(),$("#produit_s_famille").val(),$("#produit_s_s_famille").val(),actualiser_select2,"#attestation","");
			}

			// gere l'affiche de données spécifique en fonction de la présence ou non d'un client
			function afficher_bloc_client(){
				if($("#client").val()!="" && $("#client").val()!=0){
					$(".bloc-client").show();
				}else{
					$(".bloc-client").hide();
				}

			}

			function envoyer_form_action(){
				envoyer_form_ajax($("#form_modal_action"),ecrire_case);
				$('#modal_action').modal('hide');
			}

			// D-2 FONCTION REUTILISABLE

			// selection des ressources disponible pour la création d'action
			function get_action_ressources(dates,action,agence,callback,param1,param2){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_get_action_ressources.php',
					data : 'dates=' + dates + "&agence=" + agence,
					dataType: 'JSON',
					success: function(data){
						callback(data,param1,param2);
					},
					error: function(data){
						afficher_message("Erreur","danger",data.responseText);

					}
				});
			}


			//*****************************************
			// 		E - FONCTION MODAL CLIENT
			//******************************************/

			function ini_modal_client(json){

				$("#modal_client .modal-title").html("Nouveau client");


				if(json.act_adr_ref!=1){
					$("#mc_intervention").prop("checked",false);
					$(".mc-client-int").hide();
				}else{
					$("#mc_intervention").prop("checked",true);
					$(".mc-client-int").hide();
				}

				$("#mc_action").val(json.act_id);


				$("#mc_produit").data("famille",json.act_pro_famille);
				$("#mc_produit").data("sous_famille",json.act_pro_sous_famille);
				$("#mc_produit").data("sous_sous_famille",json.act_pro_sous_sous_famille);

				//alert("json.act_pro_famille" + json.act_pro_famille);
				//alert("json.act_pro_sous_famille" + json.act_pro_sous_famille);
				//alert("json.act_pro_sous_famille" + json.act_pro_sous_sous_famille);

				get_commerciaux(0,json.act_agence,"",actualiser_mc_commerciaux,"","");


			}

			function actualiser_mc_commerciaux(json){
				$("#mc_commercial option[value!='0'][value!='']").remove();
				$("#mc_commercial").select2({
					data: json,
					closeOnSelect: true
				});
				afficher_modal_client();
			}

			function afficher_modal_client(json){
				$("#modal_client").modal("show");
			}

			function actualiser_action_client(json){
				if(json){
					if(json.erreur_txt!=""){
						afficher_message("Erreur","danger",json.erreur_txt);
					}else{

						// on memorie les adresses
						if(json["adr_int"]!=""){
							$.each(json["adr_int"], function(i, ad){
								tab_adresse[ad.id]=new Array(8);
								tab_adresse[ad.id]["nom"]=ad.nom;
								tab_adresse[ad.id]["service"]=ad.service;
								tab_adresse[ad.id]["ad1"]=ad.ad1;
								tab_adresse[ad.id]["ad2"]=ad.ad2;
								tab_adresse[ad.id]["ad3"]=ad.ad3;
								tab_adresse[ad.id]["cp"]=ad.cp;
								tab_adresse[ad.id]["ville"]=ad.ville;
								tab_adresse[ad.id]["geo"]=ad.geo;
							});
						}
						// on selectionne l'adresse par defaut
						actualiser_select2(json.adr_int,"#mc_adresse",json.int_defaut);
						$("#mc_commercial").val(json.commercial).trigger("change");

						// utiliser pour la partie contact
						client_categorie=json.categorie;

						// on va chercher la liste des produits
						recuperer_mc_produit();
					}
				}else{
					// PAS DE CLIENT
					actualiser_select2("","#mc_adresse",0);
					$("#mc_adresse").val("").trigger("change");
					actualiser_select2("","#mc_contact",0);
					$("#mc_contact").val("").trigger("change");

				}
			}

			function afficher_mc_adresse(adresse){
				if(adresse>0){
					$("#mc_adr_nom").val(tab_adresse[adresse]["nom"]);
					$("#mc_adr_service").val(tab_adresse[adresse]["service"]);
					$("#mc_adr1").val(tab_adresse[adresse]["ad1"]);
					$("#mc_adr2").val(tab_adresse[adresse]["ad2"]);
					$("#mc_adr3").val(tab_adresse[adresse]["ad3"]);
					$("#mc_adr_cp").val(tab_adresse[adresse]["cp"]);
					$("#mc_adr_ville").val(tab_adresse[adresse]["ville"]);
				}else{
					$(".champ-adresse").val("");
				}
			}

			function actualiser_mc_contacts(json){
				if(json){
					// on memorie les conatcts
					var con_defaut=0;
					tab_contact=new Array();
					$.each(json, function(i, con){
						tab_contact[con.id]=new Array();
						tab_contact[con.id]["nom"]=con.nom;
						tab_contact[con.id]["prenom"]=con.prenom;
						tab_contact[con.id]["tel"]=con.tel;
						tab_contact[con.id]["portable"]=con.portable;
						tab_contact[con.id]["mail"]=con.mail;
						if(con.defaut==1){
							 con_defaut=con.id;
						}
					});
					// actualisation du select contact
					actualiser_select2(json,"#mc_contact",con_defaut);
					if(con_defaut==0){
						// securite si 0 pas #contact.change
						$("#mc_contact").val(0).trigger("change");
					}
				}else{
					actualiser_select2("","#mc_contact",0);
					$("#mc_contact").val(0).trigger("change");
					$("#mc_contact").prop("disabled",true);
				}
				if(client_categorie==3){
					$("#mc_contact").prop("disabled",true);
				}else{
					$("#mc_contact").prop("disabled",false);
				}
			}

			function afficher_mc_contact(contact){
				if(contact>0){
					$("#mc_con_nom").val(tab_contact[contact]["nom"]);
					$("#mc_con_prenom").val(tab_contact[contact]["prenom"]);
					$("#mc_con_tel").val(tab_contact[contact]["tel"]);
					$("#mc_con_portable").val(tab_contact[contact]["portable"]);
					$("#mc_con_mail").val(tab_contact[contact]["mail"]);
				}else{
					$(".champ-contact").val("");
				}
			}

			function recuperer_mc_produit(){
				var client=$("#mc_client").val();
				var famille=$("#mc_produit").data("famille");
				var sous_famille=$("#mc_produit").data("sous_famille");
				var sous_sous_famille=$("#mc_produit").data("sous_sous_famille");
				var intra=$("#mc_produit_intra").is(":checked");
				var inter=$("#mc_produit_intra").is(":checked");
				get_produits(client,1,famille,sous_famille,sous_sous_famille,intra,inter,actualiser_mc_produits,"","");
			}

			function actualiser_mc_produits(json){
				$("#mc_produit option[value!='0'][value!='']").remove();
				$("#mc_produit").select2({
					data: json,
					closeOnSelect: true
				});
			}

			function afficher_mc_produit(json){

				produit_type=$('input[name=acl_pro_inter]:checked').val()

				if(json){
					// CA
					if(produit_type==0){
							// intra
						$("#mc_ca_intra").val(json.ht_intra);
						$("#mc_ca_inter").val(0);
						if(json.ca_verrou_intra){
							$("#mc_ca_intra").prop("readonly",true);
						}else{
							$("#mc_ca_intra").prop("readonly",false);
						}
					}else{
							// inter
						ca_inter_unit=json.ht_inter;
						$("#mc_ca_inter").val(ca_inter_unit);
						$("#mc_ca_intra").val(0);

						if(json.ca_verrou_inter){
							$("#mc_ca_inter").prop("readonly",true);
						}else{
							$("#mc_ca_inter").prop("readonly",false);
						}
					}
					$("#mc_pro_libelle").val(json.libelle);
				}else{
					$("#mc_ca_intra").val(0);
					$("#mc_ca_inter").val(0);
					$("#mc_pro_libelle").val("");
				}
			}

			function envoyer_form_client(){
				envoyer_form_ajax($("#form_modal_client"));
				$('#modal_client').modal('hide');
			}


			/*
			function ini_modal_client(json_clients){

				prefixe_modal="mdl_cli_";
				var contenu="";
				var champ="";
				var nb_client=0;
				$.each(json_clients, function(key, val){
					champ="";
					champ="<div class='row' >";
						champ+="<div class='col-md-1' >";
							if(val.intervention==1){
								champ+="<div class='section text-center pt5' >";
									champ+="<i class='fa fa-map-marker' data-toggle='tooltip' data-placement='top' title='Site d&apos;intervention' ></i>";
								champ+="</div>";
							}else{
								nb_client++;
								champ+="<div class='section' >";
									champ+="<label class='option' >";
										champ+="<input type='checkbox' id='mdl_cli_client_num_" + nb_client + "' name='mdl_cli_client_num_" + nb_client + "' value='" + val.action_client + "' class='client-checkbox' />";
										champ+="<span class='checkbox' ></span>";
									champ+="</label>";
									champ+="<input type='hidden' id='mdl_cli_client_" + nb_client + "' name='mdl_cli_client_" + nb_client + "' value='" + val.client + "' />";
								champ+="</div>";

							}
						champ+="</div>";
						champ+="<div class='col-md-1 pt5' >";
							champ+=val.action_client;
						champ+="</div>";
						champ+="<div class='col-md-2 pt5' >";
							champ+=val.code;
						champ+="</div>";
						champ+="<div class='col-md-5 pt5' >";
							champ+=val.nom;
						champ+="</div>";
						champ+="<div class='col-md-3 pt5' >";
							champ+=val.pro_reference;
						champ+="</div>";

					champ+="</div>";
					contenu+=champ;
				});
				$("#mdl_cli_input").html(contenu);
				$('#mdl_cli_nb_client').val(nb_client);
				$('#mdl_cli_action').val(action);

				// selection ou deselection des clients inscrits à une action
				// place ici car les element sont ajouter apres doc.ready
				$(".client-checkbox").on("click",function(){
					recuperer_client_check();
				});
				$('#modal_client').modal('show');
			}

			function transferer_client(e){
			//	var $this = e; // L'objet jQuery du formulaire

				var liste_case=recuperer_case_select();

				var nb_client=$("#" + prefixe_modal + "nb_client").val();

				var liste_ac_check="";
				for(i=1;i<=nb_client;i++){
					if($("#" + prefixe_modal + "client_num_" + i).is(":checked")){
						liste_ac_check=liste_ac_check + $("#" + prefixe_modal + "client_num_" + i).val() + ",";
					}
				}

				if( liste_case === '') {
					alert('Les champs doivent êtres remplis');
				}else{
					$('#mdl_cli_liste_case').val(liste_case);
					$('#mdl_cli_action_client').val(liste_ac_check);
					$.ajax({
						url: "ajax/ajax_action_transfert.php", // Le nom du fichier indiqué dans le formulaire
						type: "POST", // La méthode indiquée dans le formulaire (get ou post)
						data: $("#form_modal_client").serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
						dataType: 'json',
						success: function(json) { // Je récupère la réponse du fichier PHP
							if(json.erreur==0){
								ecrire_case(json);
								$(".case_select").each(function(){
									ident_case=(this).id.replace("case_", "");
									case_vide(ident_case);
								});
								$('#modal_client').modal('hide');
							}
						}
					});
				}
			}

			*/
			//-------------------------------------
			// 		F- FONCTION GENERIQUE
			//------------------------------------

			// affiche un message selon le resultat d'une requete ajax
			function afficher_action_resultat(erreur){
				if(erreur==""){
					$("#footer_dialog").prop("class","text-center text-success");
					$("#footer_dialog").html("L'action a été mise à jour");
					$("#footer_dialog").fadeIn('slow').delay(2000).fadeOut('slow');
				}else{
					$("#footer_dialog").prop("class","text-center text-danger");
					$("#footer_dialog").html("Erreur! L'action n'a pas été mise à jour");
					$("#footer_dialog").fadeIn('slow').delay(2000).fadeOut('slow');
				}
			}



			// 		FIN FONCTION GENERIQUE

			//-------------------------------------
			// 		G- FONCTION MODAL ACTION COMMENT
			//------------------------------------
			function afficher_modal_comment(json_action){
				$("#mdl_action_comment_id").val(action);
				$("#mdl_action_comment_pla_txt").val(json_action.act_planning_txt);
				$("#mdl_action_comment_comment").val(json_action.act_commentaire);
				$("#modal_action_comment").modal("show");
				$(".context-menu").hide();
			}

			function maj_texte_survol(json){
				if(json){
					$(".action-" + json.action).prop("title",json.survol);
					afficher_action_resultat("");
				}else{
					afficher_action_resultat("erreur");
				}
			}

			//-------------------------------------
			// 		H- FONCTION MODAL ACTION ADRESSE
			//------------------------------------
			/*function afficher_modal_adresse(json,appel){

				console.log("afficher_modal_adresse(json," + appel + ")");

				if(appel==0){
					// premier appel : on recupere les donnees de l'action
					get_action(action,afficher_modal_adresse,1)
				}else if(appel==1 && json!=""){
					// second appel on memorise les info action et on va chercher les adresses
					json_action=json;
					$("#" + modal_actif_id + " .adresse-client").val(json.act_adresse);
					$("#" + modal_actif_id + " .contact").val(json.act_contact);
					get_adresses("",1,1,action,afficher_modal_adresse,2);
				}else if(appel==2 && json!=""){
					json_adresse=json;
					get_contacts(json_action.act_adr_ref_id,1,afficher_modal_adresse,3);
				}else if(appel==3){
					json_contact=json;

					actualiser_adresses(json_adresse);
					actualiser_contacts(json_contact);
					/*
					$("#" + modal_actif_id + " .adr-nom").val(json_action.act_adr_nom);
					$("#" + modal_actif_id + " .adr-service").val(json_action.act_adr_service);
					$("#" + modal_actif_id + " .adr1").val(json_action.act_adr1);
					$("#" + modal_actif_id + " .adr2").val(json_action.act_adr2);
					$("#" + modal_actif_id + " .adr3").val(json_action.act_adr3);
					$("#" + modal_actif_id + " .adr-cp").val(json_action.act_adr_cp);
					$("#" + modal_actif_id + " .adr-ville").val(json_action.act_adr_ville);

					if(json_action.act_contact==0){
						$("#" + modal_actif_id + " .contact-autre").prop("checked",true);
					}else{
						$("#" + modal_actif_id + " .contact-autre").prop("checked",false);
					}
					$("#" + modal_actif_id + " .con-nom").val(json_action.act_con_nom);
					$("#" + modal_actif_id + " .con-prenom").val(json_action.act_con_prenom);
					$("#" + modal_actif_id + " .con-tel").val(json_action.act_con_tel);
					$("#" + modal_actif_id + " .con-portable").val(json_action.act_con_portable);

					$("#modal_action_adresse").modal("show");
				}
			}*/

			//-------------------------------------
			// 		C - FONCTION MULTI MODAL
			//------------------------------------

			// retourne les id des clients select sous form de liste
			function recuperer_client_check(){

				var nb_client=$("#" + prefixe_modal + "nb_client").val();

				var liste_client_check="";
				for(i=1;i<=nb_client;i++){
					if($("#" + prefixe_modal + "client_num_" + i).is(":checked")){
						liste_client_check=liste_client_check + $("#mdl_cli_client_" + i).val() + ",";
					}
				}
				//get_adresses(liste_client_check,1,1,0,actualiser_adresses);
			}


			//-------------------------------------
			// 		C - FONCTION horaire
			//------------------------------------
			function afficher_modal_horaire(json){

				$("#horaire tbody tr").remove();
				if(json){

					multi_session=json.multi_session

					pda_id=0;
					pda_test=0;

					for(var i=0;i<json.horaires.length; i++){
						obj = json.horaires[i];
						if(pda_id!=obj["pda_id"]){
							pda_id=obj["pda_id"];
							nb_session=1
							ligne="<tr class='session-date session-date-" + pda_id + "' data-session_date='" + pda_id + "' >";
								ligne=ligne + "<td>";
									ligne=ligne + obj["int_label_1"];
									ligne=ligne + "<input type='hidden' id='nb_session_" + pda_id + "' name='nb_session_" + pda_id + "' value='0' />";
								ligne=ligne + "</td>";
								ligne=ligne + "<td>";
									ligne=ligne + obj["pda_date"];
								ligne=ligne + "</td>";
								ligne=ligne + "<td>";
									ligne=ligne + "<input class='gui-input heure' name='h_deb_" + pda_id + "_" + nb_session + "' value='" + obj["ase_h_deb"] + "' type='text'  />";
									ligne=ligne + "<input type='hidden' name='session_" + pda_id + "_" + nb_session + "' value='" + obj["ase_id"] + "' />";
								ligne=ligne + "</td>";
								ligne=ligne + "<td>";
									ligne=ligne + "<input class='gui-input heure' name='h_fin_" + pda_id + "_" + nb_session + "' value='" + obj["ase_h_fin"] + "'  type='text'  />";
								ligne=ligne + "</td>";
								ligne=ligne + "<td class='text-center' >";
									 ligne=ligne + "<label class='option' >";
										if(obj["pda_test"]==1){
											ligne=ligne + "<input type='checkbox' value='on' checked name='test_" + pda_id + "' >";
										}else{
											ligne=ligne + "<input type='checkbox' value='on' name='test_" + pda_id + "' >";
										}
										ligne=ligne + "<span class='checkbox' ></span>";
									ligne=ligne + "</label>";
								ligne=ligne + "</td>";
								if(multi_session==1){
									ligne=ligne + "<td class='text-center' >";
										ligne=ligne + "<button type='button' class='btn btn-sm btn-success add-session-date' id='session_btn_" + pda_id + "' data-session_date='" + pda_id + "' >";
											ligne=ligne + "<i class='fa fa-plus' ></i>";
										ligne=ligne + "</button>";
									ligne=ligne + "</td>";
								}else{
									ligne=ligne + "<td>&nbsp;</td>";
								}
							ligne=ligne + "</tr>";
						}else{
							nb_session=nb_session+1
							ligne="<tr class='session-date-" + pda_id + "' >";
								ligne=ligne + "<td colspan='2' >&nbsp;</td>";
								ligne=ligne + "<td>";
									ligne=ligne + "<input class='gui-input heure' name='h_deb_" + pda_id + "_" + nb_session + "' value='" + obj["ase_h_deb"] + "' type='text' />";
									ligne=ligne + "<input type='hidden' name='session_" + pda_id + "_" + nb_session + "' value='" + obj["ase_id"] + "' />";
								ligne=ligne + "</td>";
								ligne=ligne + "<td>";
									ligne=ligne + "<input class='gui-input heure' name='h_fin_" + pda_id + "_" + nb_session + "' value='" + obj["ase_h_fin"] + "' type='text'  />";
								ligne=ligne + "</td>";
								ligne=ligne + "<td colspan='2' >&nbsp;</td>";
							ligne=ligne + "</tr>";
						}
						if($('#horaire_body tr').length>0){
							$('#horaire tr:last').after(ligne);
						}else{
							$("#horaire tbody").append(ligne);
						}
					}

					$(".session-date").each(function(){
						session_date=$(this).data("session_date");
						nb_session=$(".session-date-" + session_date).length;
						$("#nb_session_" + session_date).val(nb_session);
						if(nb_session>3){
							$("#session_btn_" + session_date).prop("disabled",true);
						}
					});
					$('.add-session-date').click(function(){
						date=$(this).data("session_date");
						ajouter_date_session(date);
					});

					$('.heure').mask('12h34',{
						'translation': {
							1: {pattern: /[0-2]/},
							2: {pattern: /[0-9]/},
							3: {pattern: /[0-5]/},
							4: {pattern: /[0-9]/}
						},
					});
				}

				$("#modal_horaire").modal("show");
				$(".context-menu").hide();
			}

			function ajouter_date_session(date){

				nb_session=$("#nb_session_" + date).val();
				nb_session=1*nb_session+1;

				ligne="<tr class='session-date-" + date + "' >";
					ligne=ligne + "<td colspan='2' >&nbsp;</td>";
					ligne=ligne + "<td>";
						ligne=ligne + "<input type='text' name='h_deb_" + date + "_" + nb_session + "' class='gui-input heure' value='' />";
					ligne=ligne + "</td>";
					ligne=ligne + "<td>";
						ligne=ligne + "<input type='text' name='h_fin_" + date + "_" + nb_session + "' class='gui-input heure' value='' />";
					ligne=ligne + "</td>";
					ligne=ligne + "<td colspan='2' >&nbsp;</td>";
				ligne=ligne + "</tr>";

				$('.heure').mask('12h34',{
					'translation': {
						1: {pattern: /[0-2]/},
						2: {pattern: /[0-9]/},
						3: {pattern: /[0-5]/},
						4: {pattern: /[0-9]/}
					},
				});

				$("#horaire tr.session-date-" + date).last().after(ligne);
				$("#nb_session_" + date).val(nb_session);

				if(nb_session>3){
					$("#session_btn_" + date).prop("disabled",true);
				}

			}

			//DETERMINER LE SCROLL
			function determineDropDirection(){
				$(".dropdown-menu").each( function(){
					// Invisibly expand the dropdown menu so its true height can be calculated
					$(this).css({
						visibility: "hidden",
						display: "block",
					});
					// Necessary to remove class each time so we don't unwantedly use dropup's offset top
					$(this).parent().removeClass("dropup");
					// Determine whether bottom of menu will be below window at current scroll position
					if ($(this).offset().top + $(this).outerHeight() > $(window).innerHeight() + $(window).scrollTop()){
						$(this).parent().addClass("dropup");
					}
					// Return dropdown menu to fully hidden state
					$(this).removeAttr("style");

				});
			}
		</script>
	</body>
</html>
