<?php

	include "includes/controle_acces.inc.php";

	include('includes/connexion.php');
	include('modeles/mod_parametre.php');
	include('modeles/mod_tva.php');
	include('modeles/mod_compte.php');
	include('modeles/mod_erreur.php');
	
	$filt_tva=0;
	if(isset($_POST["filt_tva"])){
		$filt_tva=$_POST["filt_tva"];
		$_SESSION['filt_tva']=$filt_tva;
	}elseif(!empty($_SESSION['filt_tva'])){
		$filt_tva=$_SESSION['filt_tva'];
	};
	
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
		
			<?php	
				include "includes/header_def.inc.php";
			?>	
			
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
							
				<section id="content" class="animated fadeIn">	
					<div class="row mb10">
						<div class="col-md-6 col-md-offset-3">
							<div class="admin-form theme-primary">
								<form action="param_tva_liste.php" method="post" id="admin-form">
							
									<div class="col-md-10">
										<label class="field select">
											<select name="filt_tva" id="filt_tva" >
												<option value="0">Filtrer par TVA...</option>									
										<?php	foreach($base_tva as $cle => $value){
													if($cle>0){																							
														if($cle==$filt_tva){
															echo("<option value='" . $cle . "' selected >" . $value . "</option>");
														}else{
															echo("<option value='" . $cle . "'  >" . $value . "</option>");
														};
													};
												}?>											
											</select>
											<i class="arrow simple"></i>
										</label>
									</div>	
									
									<div class="col-md-2">
										<button type="submit" class="btn btn-primary btn-sm" >
											<i class="fa fa-search"></i>										
										</button>
									
									</div>
								</form>
							</div>
						</div>
					</div>
					
					<div class="row" >
						<div class="col-md-12" >
							<div class="table-responsive" >
								<table class="table table-hover table-striped" >
									<thead>
										<tr class="dark" >
											<th>ID</th>
											<th>TVA</th>
											<th>TAUX</th>
											<th>Date d'effet</th>
											<th>Date de fin</th>
											<th>Compte de vente</th>
											<th>Compte d'achat</th>											
											<th>&nbsp;</th>
										</tr>															
									</thead>
									<tbody>
								<?php	$comptes=get_comptes();
										$donnees=get_tvas_periode($filt_tva);
										
										if(!empty($donnees)){										
											foreach($donnees as $value){
												
												$tva_id=$value["tpe_tva"];
												
												$cpt_ven="&nbsp;";
												if($value["tpe_cpt_ven"]>0){
													$cle_cpt_ven = array_search($value["tpe_cpt_ven"], array_column($comptes, 'cpt_id'));												
													$cpt_ven=$comptes[$cle_cpt_ven]["cpt_numero"];
												};
												$cpt_ach="&nbsp;";
												if($value["tpe_cpt_ach"]>0){
													$cle_cpt_ach = array_search($value["tpe_cpt_ach"], array_column($comptes, 'cpt_id'));												
													$cpt_ach=$comptes[$cle_cpt_ach]["cpt_numero"];
												};
												
												
												?>
												<tr>
													<td><?=$value["tpe_id"]?></td>
													<td><?=$base_tva[$tva_id]?></td>
													<td><?=$value["tpe_taux"]?></td>
													<td><?=convert_date_txt($value["tpe_date_deb"])?></td>
													<td><?=convert_date_txt($value["tpe_date_fin"])?></td>
													<td><?=$cpt_ven?></td>
													<td><?=$cpt_ach?></td>																																				
													<td class="text-center" >
														<a href="param_tva.php?tva_periode=<?=$value["tpe_id"]?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier" >
															<i class="fa fa-pencil"></i>
														</a>
														
													</td>									
												</tr>	
								<?php		
											};
										}; 

										?>																			
									</tbody>
								</table>
							
							</div>
						</div>
					</div>
	
				</section>
				<!-- End: Content -->
			</section>
			
		</div>
		<!-- End: Main -->
		
		<footer id="content-footer" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >
				</div>
				<div class="col-xs-3 footer-right" >
					<a href="param_tva.php" class="btn btn-success btn-sm">
						<i class="fa fa-plus" ></i>
						Nouvelle TVA
					</a>
				</div>
			</div>
		</footer>
									
		
	<?php
		include "includes/footer_script.inc.php"; ?>	
		<script src="vendor/plugins/mask/jquery.mask.js" ></script>
		
		<!-- PLUGIN -->
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		
		
		
		<script type="text/javascript">
			jQuery(document).ready(function() {
				
				$('#tpe_taux').mask('00.00');
			});
			(jQuery);
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
