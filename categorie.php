<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include "modeles/mod_categorie.php";

$s = array();
if (isset($_GET['id'])) {
    $c = get_categorie($_GET['id']);
} else {
    $c = array();
}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Paramètres</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">

	<form action="categorie_enr.php" method="POST" id="admin-form">
<?php 	if (isset($_GET['id'])): ?>
			<input type="hidden" name="pca_id" value="<?=$c['pca_id']?>"></input>
<?php 	endif;?>	
		<!-- Start: Main -->
		<div id="main">
			<?php
			include "includes/header_def.inc.php";
			?>


			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper" class="">
				
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary ">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="text-center">
											<div class="content-header">
												<h2>Ajouter une <b class="text-primary">catégorie</b></h2>
											</div>
											<div class="col-md-10 col-md-offset-1">										 
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="pca_libelle" class="gui-input" id="pca_libelle" placeholder="Libellé" 
																<?php if(isset($_GET['id'])): ?>
																	value="<?=$c['pca_libelle']?>"
																<?php endif; ?>>
																<label for="pca_libelle" class="field-icon">
																	<i class="fa fa-book"></i>
																</label>
															</div>													  
														</div>
													</div>
													<div class="col-md-6">
														<div class="section mt10">
															<label class="option">
													<?php 		if(isset($_GET['id'])){ ?>
																	<input type="checkbox" name="pca_planning" value="1" <?php if($c['pca_planning']==1) echo("checked"); ?> >
													<?php		}else{ ?>
																	<input type="checkbox" name="pca_planning" value="1" >
													<?php		} ?>		
																<span class="checkbox"></span>Planning
															</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>

					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="categorie_liste.php" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
					<button type="submit" class="btn btn-success btn-sm">
						<i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php
		include "includes/footer_script.inc.php"; ?>	

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->

<script src="assets/js/custom.js"></script>

</body>
</html>
