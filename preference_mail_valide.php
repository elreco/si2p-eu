<?php

// MISE à JOUR PREFERENCE MARKET

?>
<!DOCTYPE html>
<html lang="fr">  
	<head>
		<meta charset="utf-8">
		<title>SI2P</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			.img_rgpd{
				width:600px;
			}
			.logo{
				width:300px;
			}
			.juridique{
				font-size:8pt;
				text-align:justify;
				padding:10px;
			}
		</style>
	</head>
	<body class="sb-l-c">

		<div id="main">
			<section id="content_wrapper" class="">
				<section id="content" class="">	

					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="panel heading-border panel-primary">
								<div class="panel-body bg-light">
									<div class="text-center" >
										<img src="https://www.si2p.eu/assets/img/rgpd_rep1.jpg" alt="RGPD" title="Règlement Général de la Protection des Données" class="logo" />
									</div>
							<?php	if(isset($_GET["err"])){ ?>
										
										<p class="alert alert-danger" >
											Une erreur c'est produit! Vos choix n'ont pas été enregistrés!
										</p>
								
							<?php	}else{ ?> 
										
										<p>Merci d‘avoir répondu à ce formulaire. Vos choix ont bien été enregistrés.<p>
										<p>Votre code promo à rappeler est le suivant : <b>RGPDRESEAUSI2P</b>
										<p>
											Valable une seule fois à partir du 1er juin ce code promo vous offre 5% pour toute nouvelle formation positionnée en juillet ou en août 2018. 
											« Cet été, laissez-vous former ! » Appelez vite votre interlocuteur ou le 
											<div style="text-align:center;" >
												<img src="https://www.si2p.eu/assets/img/indigo.jpg" />											
											</div>
										</p>				

										<p>
											Cette nouvelle réglementation s’inscrit pleinement dans notre philosophie, nous souhaitons communiquer avec vous en toute transparence et en toute confiance pour mieux vous servir.
											À tout moment vous pourrez revenir sur votre choix et corriger ou compléter les données qui vous concernent en contactant notre délégué à la protection des données personnelles à l'adresse <a href="mailto:dpo@si2p.fr" >dpo@si2p.fr</a> ou par courrier  : 
											Si2P GFC, Délégué à la Protection des Données, ZA des Hautes Perches, Chemin du Bois, 49610 Saint Melaine Sur Aubance FRANCE. Votre demande devra être accompagnée d'une copie de votre justificatif d'identité.
										</p>
							<?php	} ?>
								</div>
							</div>
						</div>
					</div>
				</section>
				
			</section>
			
		</div>
	

		<script src="/vendor/jquery/jquery-2.0.0.min.js"></script>
		<script src="/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
		<script src="/assets/js/utility/utility.js"></script>
		<script src="/assets/js/main.js"></script>
		<script type="text/javascript">		
			jQuery(document).ready(function(){
				"use strict";
				// Init Theme Core    
				Core.init();
				
			});
			
		</script>
	</body>
</html>
