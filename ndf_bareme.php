<?php
/////////////////// MENU ACTIF ///////////////////////
/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');
///////////////////// Contrôles des parametres ////////////////////

///////////////////// FIN Contrôles des parametres ////////////////////

/////////////// TRAITEMENTS BDD ///////////////////////
$req = $Conn->prepare("SELECT * FROM baremes_kilometres WHERE bki_date_deb <= :bki_date_deb AND bki_date_fin >= :bki_date_deb");
$req->bindValue(':bki_date_deb', date("Y-m-d"));
$req->execute();
$bareme_kilometre = $req->fetch();

$req = $Conn->prepare("SELECT * FROM
baremes_kilometres_lignes
 WHERE bkl_bareme = :bkl_bareme AND bkl_puissance = :bkl_puissance AND bkl_tranche_km = :bkl_tranche_km");
$req->bindParam(':bkl_bareme', $_GET['bki_id']);
$req->bindParam(':bkl_puissance', $_GET['bkl_puissance']);
$req->bindParam(':bkl_tranche_km', $_GET['bkl_tranche_km']);
$req->execute();
$bkl = $req->fetch();

?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Enregistrer un barème</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
<form method="post" action="ndf_bareme_enr.php" enctype="multipart/form-data">
<!-- Start: Main -->
<div id="main">
<?php include "includes/header_def.inc.php"; ?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper" >
    <!--DEBUT CONTENT -->
    <section id="content" class="animated fadeIn">
        <!--DEBUT ROW -->
        <div class="row">
        <!--DEBUT COL-MD-10 -->
            <div class="col-md-10 col-md-offset-1">
              <!--DEBUT ADMIN FORM -->
                <div class="admin-form theme-primary ">
                    <!--DEBUT PANEL HEADING -->
                    <div class="panel heading-border panel-primary">
                      <!--DEBUT PANEL BODY -->
                        <div class="panel-body bg-light">
                            <div class="content-header">
                                <h2>Barème note de frais</h2>
                            </div>
                            <input type="text" name="edition" value="1" hidden>
                            <input type="text" name="bkl_bareme" value="<?= $_GET['bki_id'] ?>" hidden>
                            <input type="text" name="bkl_puissance_before" value="<?= $_GET['bkl_puissance'] ?>" hidden>
                            <input type="text" name="bkl_tranche_km_before" value="<?= $_GET['bkl_tranche_km'] ?>" hidden>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-3 text-right">
                                        <strong>KM annuel :</strong>
                                    </div>

                                    <div class="col-md-3" >
                                        <label class="option">
                                            <input type="radio" name="bkl_tranche_km" value="1"
                                            <?php if ($bkl['bkl_tranche_km'] == 1) { ?>
                                                checked
                                            <?php } ?>
                                            >
                                            <span class="radio mr5"></span> &lt;= 5000 Km
                                        </label>
                                    </div>
                                    <div class="col-md-3" >
                                        <label class="option">
                                            <input type="radio" name="bkl_tranche_km" value="2"
                                            <?php if ($bkl['bkl_tranche_km'] == 2) { ?>
                                                checked
                                            <?php } ?>>
                                            <span class="radio mr5"></span> &lt;= 20000 Km
                                        </label>
                                    </div>
                                    <div class="col-md-3" >
                                        <label class="option">
                                            <input type="radio" name="bkl_tranche_km" value="3"
                                            <?php if ($bkl['bkl_tranche_km'] == 3) { ?>
                                                checked
                                            <?php } ?>
                                                />
                                            <span class="radio mr5"></span> &gt; 20000 Km
                                        </label>
                                    </div>
                                </div>
                                <div class="row mt20">
                                    <div class="col-md-6" >
                                        <label for="uti_veh_cv" >Puissance du véhicule :</label>
                                        <div class="input-group">
                                            <input type="text" name="bkl_puissance" id="bkl_puissance" min="0" max="20"
                                            class="gui-input input-int" placeholder="Puissance"

                                                value="<?= $bkl['bkl_puissance'] ?>"
                                            />
                                            <span class="input-group-addon" >CV</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt20">
                                    <div class="col-md-6">
                                        <label for="bkl_coeff" class="text-left" style="font-weight:bold;">Coeff :</label>
                                        <input type="text" value="<?= $bkl['bkl_coeff'] ?>" name="bkl_coeff" id="bkl_coeff" required class="form-control input-float" placeholder="Coeff">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="bkl_bonus" class="text-left" style="font-weight:bold;">Bonus :</label>
                                        <input type="text" value="<?= $bkl['bkl_bonus'] ?>" name="bkl_bonus" id="bkl_bonus" class="form-control input-float" placeholder="Bonus">
                                    </div>
                                </div>
                            </div>
                        <!-- FIN COL-MD-10 -->
                        </div>
                    <!-- FIN PANEL BODY -->
                    </div>
                <!-- FIN PANEL HEADING -->
                </div>
            <!-- FIN ADMIN FORM -->
            </div>
        </div>

        </section>

    </section>

</div>
    <!-- End: Main -->
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">
                <a href="parametre.php" class="btn btn-default btn-sm">

                    <i class='fa fa-long-arrow-left'></i> Retour

                </a>
            </div>
            <div class="col-xs-6 footer-middle">

            </div>
            <div class="col-xs-3 footer-right">
                <button type="submit" name="search" class="btn btn-success btn-sm">
                    <i class='fa fa-floppy-o'></i> Enregistrer
                </button>
            </div>
        </div>
    </footer>
</form>
<?php include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->
<script src="vendor/plugins/summernote/summernote.min.js"></script>
<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
<script src="assets/js/custom.js"></script>
<!-- SCRIPT SELECT2 -->
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
    ////////////// FONCTIONS ///////////////





    ////////////// FIN FONCTIONS ///////////////
    //////// EVENEMENTS UTILISATEURS //////////
    jQuery(document).ready(function () {

    });


    //////// FIN ÉVENEMENTS UTILISATEURS //////
</script>

</body>
</html>