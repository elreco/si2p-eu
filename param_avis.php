<?php
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");
	
	// ECRAN D'EDITION D'UN AVIS
	
	$type=0;
	if(isset($_GET["type"])){
		$type=intval($_GET["type"]);
	}
		
	$avis=0;
	if(isset($_GET["avis"])){
		$avis=intval($_GET["avis"]);
		try{
			$req=$Conn->query("SELECT avi_nom,avi_type FROM Avis WHERE avi_id=" . $avis . ";");
			$d_avis=$req->fetch();
			$type=$d_avis["avi_type"];
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}

	}else{
		$d_avis=array(
			"avi_nom" => "",
			"avi_type" => $type
		);
	}
	
	// CONTROLE
	if($type==1 OR $type==2){
		try{
			$req=$Conn->query("SELECT avi_id FROM Avis WHERE avi_type=" . $type . " AND avi_statut=0 AND NOT avi_id=" . $avis . ";");
			$avis_pre_prod=$req->fetch();
			if(!empty($avis_pre_prod)){
				$erreur_txt="Vous ne pouvez avoir qu'un seul avis par type en pré-production";
			}
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}			
	}else{
		$erreur_txt="Impossible de créer un avis de stage.";
	}
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur !",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : " . $_SESSION["retour"]);
		die();
	}
	
	// LES Questions
	
	$sql="SELECT * FROM Avis_Rubriques LEFT JOIN Avis_Questions ON (Avis_Rubriques.aru_id=Avis_Questions.aqu_rubrique)
	LEFT JOIN Avis_Questionner ON (Avis_Questions.aqu_id=Avis_Questionner.aqu_question AND Avis_Questionner.aqu_avis=" . $avis . ")
	WHERE aqu_valide ORDER BY aru_place,aqu_place,aqu_id;";
	$req=$Conn->query($sql);
	$d_questions=$req->fetchAll();

	
	
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	   
	   <!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="param_avis_enr.php" >
			<input type="hidden" name="avis" value="<?=$avis?>" />
			<input type="hidden" name="type" value="<?=$type?>" />
			
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" class="" >
					<section id="content" class="animated fadeIn">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
							
								<div class="content-header">
							<?php	if($avis>0){ ?>
										<h2>Modification d'un <b class="text-primary" > avis </b></h2>
							<?php	}else{ ?>
										<h2>Ajout d'un <b class="text-primary" > avis </b></h2>	
							<?php	} ?>
								</div>
								
								<div class="row" >
									<div class="col-md-offset-1 col-md-10" >
											
										<div class="panel heading-border panel-primary">
											<div class="panel-body bg-light">
												<div class="admin-form theme-primary">
													<div class="row">
														<div class="col-md-6">													
															<label for="avi_nom" >Nom :</label>												
															<span class="field">
																<input type="text" name="avi_nom" id="avi_nom" class="gui-input" placeholder="Nom de l'avis" required value="<?=$d_avis["avi_nom"]?>" />
															</span>
														</div>
														<div class="col-md-6 pt25 text-center">
															Type :
													<?php	if($type==1){
																echo("<strong>INTRA</strong>");
															}else{
																echo("<strong>INTER</strong>");
															} ?>
														</div>
													</div>
													
													<div class="section-divider mb20">
														<span>Questions</span>
													</div>
													
												</div>
												
												<div class="row">
													<div class="col-md-6 text-center" >
														<p>Utilisez l'icône <i class="fa fa-check" ></i> pour ajouter une question de l'avis de stage.</p>
													</div>
													<div class="col-md-6 text-center" >
														<p>Utilisez l'icône <i class="fa fa-times" ></i> pour supprimer une question de l'avis de stage.</p>
													</div>
												</div>
													
										<?php	$rubrique=0;
												if(!empty($d_questions)){ 
															
													$place=0;
													foreach($d_questions as $q){ 
													
														$place++; 
														
														if($rubrique!=$q["aru_id"]){
															$rubrique=$q["aru_id"];
															echo("<h4>" . $q["aru_libelle"] . "</h4>");
														}
														
														$class_panel=" panel-default";
														$class_control="fa fa-check text-success";
														$val_select=0;
														if(!empty($q["aqu_avis"])){ 
															$class_panel=" panel-success";
															$class_control="fa fa-times";
															$val_select=1;
														} ?>
														
														
														<div class="panel<?=$class_panel?> panel-question" data-question="<?=$q["aqu_id"]?>" id="panel_question_<?=$q["aqu_id"]?>" >
															<div class="panel-heading ">
																<span class="panel-title">Question <?=$q["aqu_id"]?></span>		
																<span class="panel-controls" >
																	<a href="#" class="panel-controls-active" >
																		<i class="<?=$class_control?>" id="panel_active_<?=$q["aqu_id"]?>" ></i>
																	</a>
																	<input type="hidden" id="question_<?=$q["aqu_id"]?>" name="question_<?=$q["aqu_id"]?>" value="<?=$val_select?>" />	
																	<input type="hidden" name="question_enr_<?=$q["aqu_id"]?>" value="<?=$q["aqu_avis"]?>" />
																</span>
															</div>
															<div class="panel-body">
															
																<?=$q["aqu_texte"]?>
																<div class="row" >
																	<div class="col-md-4 text-center" >
																		<?=$q["aqu_min_lib"]?>
																	</div>
																	<div class="col-md-offset-4 col-md-4 text-center" >
																		<?=$q["aqu_max_lib"]?>
																	</div>
																</div>
																<div class="row" >
																	<div class="col-md-4 text-center" >
																		<?=$q["aqu_min_val"]?>
																	</div>
																	<div class="col-md-offset-4 col-md-4 text-center" >
																		<?=$q["aqu_max_val"]?>
																	</div>
																</div>
															</div>
														</div>
															
														
													
									<?php			}
												}else{ ?>
													<div class="alert alert-danger text-center" >Vous n'avez validé aucune question.</div>
										<?php	} ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- FIN DU CONTENU -->
				</section>
			</div>
			
			
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="param_avis_liste.php" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" ></div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
			
			
			
		</form>

		<!-- BEGIN: PAGE SCRIPTS -->

		<!-- jQuery -->
		<!--<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
		<script src="vendor/jquery/jquery_ui/jquery-ui.js"></script>
		<script src="theme/vendor/plugins/select2/select2.min.js"></script>-->
<?php	include "includes/footer_script.inc.php"; ?>

		<script type="text/javascript">
		
			var question_depart;
			var drop_place=0;
			
			jQuery(document).ready(function() {

				$( ".panel-question" ).each(function() {
					initier_panel($(this).data("question"));
				});
			});
			function activer_panel(question){
				if($("#question_" + question).val()==0){
					$("#panel_question_" + question).removeClass("panel-default");
					$("#panel_question_" + question).addClass("panel-success");
					$("#panel_active_" + question).removeClass("fa-check");
					$("#panel_active_" + question).removeClass("text-success");
					$("#panel_active_" + question).addClass("fa-times");
					$("#question_" + question).val(1);						
				}else{
					$("#panel_question_" + question).removeClass("panel-success");
					$("#panel_question_" + question).addClass("panel-default");
					$("#panel_active_" + question).removeClass("fa-times");
					$("#panel_active_" + question).addClass("text-success");
					$("#panel_active_" + question).addClass("fa-check");
					$("#question_" + question).val(0);		
				}
			}
			
			function initier_panel(question){

				console.log("question " + question);
				$("#panel_question_" + question + " .panel-controls-active").click(function(e){
					e.preventDefault();
					activer_panel(question);
				});
			}
		</script>
	</body>
</html>