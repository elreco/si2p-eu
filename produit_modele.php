<?php
/* variable pour la nav (active) */
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
/* include pour chaque fichier */
include_once 'modeles/mod_produit.php';
$modeles = get_produit_modeles();
include_once 'modeles/mod_famille.php';
include_once 'modeles/mod_sous_famille.php';
include_once 'modeles/mod_qualification.php';
?>
<!DOCTYPE html>
<html>
<!-- Mirrored from admindesigns.com/demos/absolute/1.1/layout_horizontal-sm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 09:52:52 GMT -->
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Modèles de produits</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!--
       <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
       <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
   -->
   <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
   <!-- Favicon -->
   <link rel="shortcut icon" href="assets/img/favicon.png">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<?php
include "includes/header_def.inc.php";
?>
<body class="sb-top sb-top-sm">
    <section id="content_wrapper">
        
<section id="content" class="animated fadeIn">
    <div class="row">
        <div class="col-md-5">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr class="dark">
                            <th>Nom</th>
                            <th>Edition</th> 
                            <th>Famille</th>
                            <th>Sous-famille</th>
                            <th>Qualification</th>
                            <th>URL</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($modeles as $k => $v): ?>
                            <tr class="tr-<?= $v['pmo_id'] ?>">
                                <td><?= $v['pmo_nom'] ?></td>
                                <td><?php if(empty($v['pmo_edition'])): ?><i>Pas d'édition</i><?php endif; ?></td>
                                <?php $famille = get_famille($v['pmo_famille']); ?>
                                <td><?= $famille['pfa_libelle'] ?></td>
                                <?php $sous_famille = get_sous_famille($v['pmo_sous_famille']); ?>
                                <td><?php if(empty($v['pmo_sous_famille'])): ?><i>Pas de sous-famille</i><?php else: ?><?= $sous_famille['sfa_libelle'] ?><?php endif; ?></td>
                                <?php $qualif = get_qualification($v['pmo_qualification']); ?>
                                <td><?= $qualif['qua_libelle'] ?></td>
                                <td><?php if(empty($v['pmo_url'])): ?><i>Pas d'url</i><?php else: ?><?= $v['pmo_url'] ?><?php endif; ?></td>
                                <td>
                                    <a href="#" class="btn btn-info btn-sm voir" data-id="<?= $v['pmo_edition'] ?>" data-idp="<?= $v['pmo_id'] ?>"><i class="fa fa-eye"></i></a> 
                                    <a href="produit_modele_cree.php?id=<?= $v['pmo_id'] ?>" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php if(empty($modeles)): ?>
                    <div class="col-md-12 text-center" style="padding:0;" >
                        <div class="alert alert-warning" style="border-radius:0px;">
                            Aucun modèle.
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-100">
                <div class="panel-heading panel-md">
                    <span class="panel-title ped-id">Aucun modèle sélectionné</span>

                </div>
                <div class="panel-body panel-scroller scroller-overlay" >
                    <div class="col-md-12 text-center message hidden" style="padding:0;" >
                        <div class="alert alert-warning" style="border-radius:0px;margin:0px;">
                            Aucune édition pour le modèle <strong class="ped-id-s"></strong>.
                        </div>
                    </div>
                    <table class="table table-edition mbn tc-med-1 tc-bold-2 table-hover pad-right-td hidden">
                        <thead>
                            <tr>
                                <th>Convention</th>
                                <th>Convocation</th>
                                <th>Avis de passage</th>
                                <th>Feuille de présence</th>
                                <th>Attestation</th>
                                <th>Diplôme</th>
                                <th>Editer</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                </div>
            </div>


        </div>
    </div>
</div>
</section>
</section>


<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left">
            <a href="parametre.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle"></div>
        <div class="col-xs-3 footer-right">

            <a href="produit_modele_cree.php" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Ajouter un modèle</a>
            <a href="" data-toggle="tooltip" title="" data-placement="top" class="btn btn-success btn-sm btn-edition hidden"><i class="fa fa-plus"></i> Ajouter une édition</a>
        </div>
    </div>
</footer>

<?php
include "includes/footer_script.inc.php"; 
?>	
<script src="assets/js/custom.js"></script>
<script type="text/javascript">
    $( ".voir" ).click(function() {

        var selected_id = $(this).data("id");
        var selected_idp = $(this).data("idp");

        $(".tr-" + selected_idp).addClass("info");

$.ajax({
          type:'POST',
          url: '/ajax/ajax_produit_edition_tout.php',
      dataType: 'json',                //data format
      success: function(data)          //on recieve of reply
      {
        
        


    }
});

        $.ajax({
          type:'POST',
          url: '/ajax/ajax_produit_edition.php',
      //the script to call to get data
      data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
      //for example "id=5&parent=6"

      dataType: 'json',                //data format
      success: function(data)          //on recieve of reply
      {
        if(data['ped_id'] == undefined){
            $(".table-edition").addClass("hidden");
            $(".message").removeClass("hidden");
            $(".btn-edition").removeClass("hidden");
            $(".ped-id-s").text(data['ped_id']);
        }else{
            $(".ped-id").html("Editions du modèle <strong>" + data['ped_id'] + "</strong>");
            $(".table-edition").removeClass("hidden");
            $(".message").addClass("hidden");
        }
        


    }
});
    });
</script>
</body>
</html>
