<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" >
	<title>Si2P - Formations incendie, sécurité et prévention</title>
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.min.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/login.css">
	<link href='https://fonts.googleapis.com/css?family=Audiowide' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="main">
		<div class="logo">
			<img src="assets/img/login/logo.png" style="margin-bottom:15px;"/>
		</div>
		
		<div class="formulaire">
			<h1>Mot de passe oublié</h1>
			<div class="formulaire-body" >
			
				<form method="post" action="oublie_enr.php" required id="formul" >
					<label>Adresse mail liée à votre compte</label>
					<input type="email" name="mail" >
					
					<input type="submit" value="Envoyer">
					<hr style="margin-top:20px;margin-bottom:20px;">
					<div class="mdp" >
						<a href="index.php" >Annuler</a>
					</div>
				</form>
				
			</div>
		</div>
	</div>


</body>
</html>