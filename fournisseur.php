<?php  
////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_droit.php');

// session retour
if(isset($_GET['menu_retour'])){
	unset($_SESSION['retour']);
}
// fin session retour
  
////////////////// TRAITEMENTS SERVEUR ///////////////////
// sélectionner les fournisseurs pour construire le GROUPE
$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_groupe = 1 AND fou_type =1 AND fou_filiale_de = 0");
$req->execute();
$fournisseurs = $req->fetchAll();
// familles des fournisseurs
$req = $Conn->prepare("SELECT * FROM fournisseurs_familles ORDER BY ffa_id");
$req->execute();
$fournisseurs_familles = $req->fetchAll();
// verif droit compta 
if($_SESSION['acces']['acc_ref']==1){
	$acc_utilisateur=$_SESSION['acces']['acc_ref_id'];
	if(!empty(get_droit_utilisateur(9,$_SESSION['acces']['acc_ref_id']))){
		$acc_drt_compta=true;
	}else{
		$acc_drt_compta = false;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>SI2P - Orion - Fournisseurs</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
 
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
	<form method="post" action="fournisseur_enr.php" enctype="multipart/form-data">
		<!-- Start: Main -->
		<div id="main">
			<?php include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper" >
				

				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary ">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="text-left">
											<div class="content-header">
												<h2>Ajouter un <b class='text-primary'>fournisseur</b></h2>					
											</div>
											<div class="col-md-10 col-md-offset-1">
												<?php if(isset($_GET['error']) && $_GET['error'] == 1){ ?>
						                            <div class="col-md-12">
							                            <div class="alert alert-danger">
							                                Le Fournisseur existe déjà dans notre base de données.
							                            </div>
						                            </div>
						                        <?php } ?>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Informations générales</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="fou_code" id="fou_code" class="gui-input nom" placeholder="Code" required="" value="">
																<label for="fou_code" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</label>
															</div>
														</div>
													</div>
													<div class="col-md-8">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="fou_nom" id="fou_nom" class="gui-input" placeholder="Nom" required="" value="">
																<label for="fou_nom" class="field-icon">
																	<i class="fa fa-bookmark"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<!-- <div class="col-md-8" id="source2">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="fou_siren" id="fou_siren" class="gui-input siren" placeholder="Siren" required>
																<label for="fou_siren" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</label>
															</div>
														</div>
													</div> -->
													<div class="col-md-12" id="source3">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="fou_siret" id="fou_siret" class="gui-input siretlong" placeholder="Siret" required>
																<label for="fou_siret" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</label>
															</div>
														</div>
													</div>

												</div>
												<!-- espace vérification (boutons et messages) -->
												<div class="row verification">

													<div class="col-md-12 text-center">
														<div class="section" id="loader">
															<i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="font-size:12px;display:none;"></i>
														</div>

														<div class="section">
															<button type="button" id="verif" class="btn btn-success btn-sm">Vérifier</button>
														</div>

													</div>
													<div class="col-md-12" id="msg1" style="display:none;">
														<div class="section">
															<div class="alert alert-success">Il n'y a pas de fournisseur associé à ces informations.</div>
														</div>
													</div>
													<div class="col-md-12" id="msg2" style="display:none;">
														<div class="section">
															<div class="alert alert-danger">Le siret est libre mais un fournisseur existe déjà avec ce code, merci de le changer.</div>
														</div>
													</div>
													<div class="col-md-12" id="msg3" style="display:none;">
														<div class="section">
															<div class="alert alert-danger">Veuillez renseigner le champ code.</div>
														</div>
													</div>
													<div class="col-md-12" id="msg4" style="display:none;">
														<div class="section">
															<div class="alert alert-danger">Veuillez renseigner le champ siret.</div>
														</div>
													</div>

												</div>
												<!-- fin espace vérification (boutons et messages) -->

												<!-- si la vérification est bonne, afficher le reste du formulaire -->
												<div class="rest-form" style="display:none;">
													<div class="row">
														<div class="col-md-6">
															<div class="section">
																<span class="select">
																	<select id="fou_type" name="fou_type">
																		<!-- aller chercher les TYPES de fournisseurs dans mod_parametre.php -->
																		<?php foreach($base_type_fournisseur as $k => $n){ ?>
																			<?php if($k > 0){ ?>
																				<option value="<?= $k ?>"><?= $n ?></option>
																			<?php } ?>
																		<?php } ?>
																	</select>
																	<i class="arrow"></i>
																</span>
															</div>
														</div>
														<div class="col-md-6">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="url" name="fou_site" id="fou_site" class="gui-input" placeholder="Site Internet" value="">
																	<label for="fou_site" class="field-icon">
																		<i class="fa fa-globe"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>	
													
													<div class="row groupe">
														<div class="col-md-6">
															<div class="section">
																<label class="option option-dark">
																<input type="checkbox" name="fou_groupe" id="fou_groupe" value="oui">
																	<span class="checkbox"></span>
																	<label for="fou_groupe">Groupe</label>
																</label>
															</div>
														</div>

														<div class="col-md-6 maisonm">
							                              	<div class="section">
								                                <label for="fou_maison_mere" class=" mt15 option option-dark">
									                                <input type="radio" id="fou_maison_mere" name="fou_maison_mere" value="oui" checked>
									                                <span class="radio"></span> Ce fournisseur est la maison mère
								                                </label>
								                              	<label for="fou_maison_mere_non" class=" mt15 option option-dark">
								                                    <input type="radio" id="fou_maison_mere_non" name="fou_maison_mere" value="non">
								                                    <span class="radio"></span> Ce fournisseur est une filiale
								                                </label>
							                              	</div>
							                              
							                            </div>
													</div>

													<div class="row filiale">
														<div class="col-md-6 col-md-offset-6">
 
								                            <h3>Choisir la maison mère :</h3>
								                                
								                                
							                                <select id="fou_filiale_de"  name="fou_filiale_de" class="chosenselect">
										                        <option value="0" selected>C'est la maison mère...</option>
										                            <?php if(!empty($fournisseurs)){ ?>
										                                <?php foreach($fournisseurs as $f){ ?>
																					
																			<option value="<?= $f['fou_id'] ?>"><?= $f['fou_code'] ?></option>
																					
																		<?php } ?>
																	<?php } ?>
							                                </select>

								                            
								                              
								                        </div>
													</div>
													
													<div class="row">

														<div class="col-md-6">
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Adresse</span>
																	</div>
																</div>
															</div>
															<div class="row">

																<div class="col-md-12">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="fad_nom" id="fad_nom" class="gui-input" placeholder="Nom">
																			<label for="fad_nom" class="field-icon">
																				<i class="fa fa-tag"></i>
																			</label>
																		</div>
																	</div>
																</div>
 
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="fad_ad1" id="fad_ad1" class="gui-input" placeholder="Adresse">
																			<label for="fad_ad1" class="field-icon">
																				<i class="fa fa-map-marker"></i>
																			</label>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="fad_ad2" class="gui-input" id="fad_ad2" placeholder="Adresse (Complément 1)">
																			<label for="fad_ad2" class="field-icon">
																				<i class="fa fa-map-marker"></i>
																			</label>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="fad_ad3" id="fad_ad3" class="gui-input" placeholder="Adresse (Complément 2)">
																			<label for="fad_ad3" class="field-icon">
																				<i class="fa fa-map-marker"></i>
																			</label>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-3">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" id="fad_cp" name="fad_cp" class="gui-input code-postal" placeholder="Code postal"  >
																			<label for="fad_cp" class="field-icon">
																				<i class="fa fa fa-certificate"></i>
																			</label>
																		</div>
																	</div>
																</div>
																<div class="col-md-9">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="fad_ville" id="fad_ville" class="gui-input nom" placeholder="Ville">
																			<label for="fad_ville" class="field-icon">
																				<i class="fa fa fa-building"></i>
																			</label>
																		</div>
																	</div>
																</div>


															</div>
														</div>
														<div class="col-md-6">
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Contact</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<div class="field select">
																			<select name="fco_fonction" id="fco_fonction" onchange="nomchange(this.value)">
																				<option value="0">Sélectionner une fonction...</option>
																				<?php if (isset($_GET['id'])){ ?>
																					<?=get_contact_fonction_select($s['fco_fonction']);?>
																				<?php }else{ ?>
																					<?=get_contact_fonction_select(0);?>
																				<?php } ?>
																				<option value="autre">Autre fonction...</option>
																			</select>
																			<i class="arrow simple"></i>
																		</div>
																	</div>
																</div>
																<div class="col-md-12 fco_fonction_nom_style" style="display:none;">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="fco_fonction_nom" id="fco_fonction_nom" class="gui-input" placeholder="Autre fonction"  >
																			<label for="fco_fonction_nom" class="field-icon">
																				<i class="fa fa-tag"></i>
																			</label>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<div class="field select">
																			<select name="fco_titre" id="fco_titre" >
																				<option value="0">Civilité...</option>
																				<?php foreach($base_civilite as $b => $c){ ?>
																					<?php if($b > 0){ ?>
																						<option value="<?= $b ?>"><?= $c ?></option>
																					<?php } ?>
																				<?php } ?>


																			</select>
																			<i class="arrow simple"></i>
																		</div>
																	</div>
																</div>
															</div>

															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="fco_nom" id="fco_nom" class="gui-input nom" placeholder="Nom">
																			<label for="fco_nom" class="field-icon">
																				<i class="fa fa-user"></i>
																			</label>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="fco_prenom" id="fco_prenom" class="gui-input prenom" placeholder="Prénom">
																			<label for="fco_prenom" class="field-icon">
																				<i class="fa fa-tag"></i>
																			</label>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input name="fco_tel" id="fco_tel" type="tel" class="gui-input telephone" placeholder="Numéro de téléphone">
																			<label for="fco_tel" class="field-icon">
																				<i class="fa fa fa-phone"></i>
																			</label>
																		</div>
																	</div>
																</div>

																<div class="col-md-6">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input name="fco_fax" id="fco_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax">
																			<label for="fco_fax" class="field-icon">
																				<i class="fa fa-phone-square"></i>
																			</label>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input name="fco_portable" id="fco_portable" type="tel" class="gui-input telephone" placeholder="Numéro de portable">
																			<label for="fco_portable" class="field-icon">
																				<i class="fa fa fa-mobile"></i>
																			</label>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="email" name="fco_mail" id="fco_mail" class="gui-input" placeholder="Adresse Email">
																			<label for="fco_mail" class="field-icon">
																				<i class="fa fa-envelope"></i>
																			</label>
																		</div>
																	</div>
																</div>
															</div>

														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Facturation</span>
																	</div>
																</div>
															</div>
															<div class="row section">
																<div class="col-md-12">
																	<div class="section">
																		<span class="select">

																			<select id="fou_reg_type" name="fou_reg_type">
																				<option value="0">Mode de règlement...</option>
																				<?php foreach($base_reglement as $k => $n){ ?>
																					<?php if($k > 0){ ?>
																					<option value="<?= $k ?>"><?= $n ?></option>
																					<?php } ?>
																				<?php } ?>
																			</select>
																			<i class="arrow"></i>
																		</span>
																	</div>
																</div>
																	<div class="col-md-12 mt5">
																
																		<label class="option">
																			<input type="radio" id="reg_formule_1" class="reg-formule" name="reg_formule" value="1" checked />
																			<span class="radio"></span>
																		</label>
																		Date + <input type="number" max="60" size="2" name="reg_nb_jour_1" value="0" class="champ-reg-formule reg-formule-1"/> jours
																
																	</div>
																	<div class="col-md-12 mt5">
																		<label class="option">
																			<input type="radio" id="reg_formule_2" class="reg-formule" name="reg_formule" value="2">
																			<span class="radio"></span>
																		</label>
																		Date + 45j + fin de mois
																	</div>
																	<div class="col-md-12 mt5">
																		<label class="option">
																			<input type="radio" id="reg_formule_3" class="reg-formule" name="reg_formule" value="3">
																			<span class="radio"></span>
																		</label>
																		Date + Fin de mois + 45j
																	</div>
																	<div class="col-md-12 mt5">
																
																			<label class="option">
																				<input type="radio" id="reg_formule_4" class="reg-formule" name="reg_formule" value="4" />
																				<span class="radio"></span>
																			</label>
																			Date + <input type="number" max="30" size="2" maxlength="2"name="reg_nb_jour_4" value="0" disabled class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
																			+ Fin de mois +  <input type="number" size="2" maxlength="2" max="20" name="reg_fdm_4" value="0" disabled class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours	
																	</div>
																</div>

															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="fou_tva" id="fou_itva" class="gui-input" placeholder="Identification TVA">
																			<label for="fou_tva" class="field-icon">
																				<i class="fa fa-barcode"></i>
																			</label>
																		</div>
																	</div>
																</div>
															</div>
															<?php if($acc_drt_compta == true){ ?>
															<div class="row">

																
																<div class="col-md-6">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="number" name="fou_montant_max" id="fou_montant_max" class="gui-input" placeholder="Montant maximum d'achat" >
																			<label for="fou_montant_max" class="field-icon">
																				<i class="fa fa-euro"></i>
																			</label>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
									                              	<div class="section">
										                                <div class="checkbox-custom checkbox-default mb10 mt10">
										                                  <input type="checkbox"
										                                   name="fou_depassement_autorise" value="oui" id="fou_depassement_autorise">
										                                  <label for="fou_depassement_autorise">Dépassement autorisé</label>
										                                </div>
									                                </div>
									                            </div>
															</div>
															<?php }?>
															<div class="row">
																<div class="col-md-8">
																	<div class="section">
																		<label for="fou_iban">IBAN</label>
																		<div class="field prepend-icon">
																			<input type="text" name="fou_iban" id="fou_iban" class="gui-input iban" placeholder="IBAN" >
																			<label for="fou_iban" class="field-icon">
																				<i class="fa fa-barcode"></i>
																			</label>
																		</div>
																	</div>
																</div>
																<div class="col-md-4">
																	<div class="section">
																		<label for="fou_iban">BIC</label>
																		<div class="field prepend-icon">
																			<input type="text" name="fou_bic" id="fou_bic" class="gui-input" placeholder="BIC" >
																			<label for="fou_bic" class="field-icon">
																				<i class="fa fa-barcode"></i>
																			</label>
																		</div>
																	</div>
																</div>
																<?php if($acc_drt_compta == true){ ?>
																<div class="col-md-12">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="fou_sage" id="fou_sage" class="gui-input" placeholder="Code Sage" >
																			<label for="fou_sage" class="field-icon">
																				<i class="fa fa-barcode"></i>
																			</label>
																		</div>
																	</div>
																</div>
																<?php } ?>

															</div>

														</div>
														<div class="col-md-6">
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Familles</span>
																	</div>
																</div>
															</div>
															<div class="row text-left">
																<?php foreach($fournisseurs_familles as $ffa){ ?>
									                              <div class="col-md-6">
									                              	<div class="section">
										                                <div class="checkbox-custom checkbox-default mb5">
										                                  <input type="checkbox"
										                                   name="ffj_famille[]" value="<?= $ffa['ffa_id'] ?>" id="checkboxFamille<?= $ffa['ffa_id'] ?>">
										                                  <label for="checkboxFamille<?= $ffa['ffa_id'] ?>"><?= $ffa['ffa_libelle']; ?></label>
										                                </div>
									                                </div>
									                              </div>
									                            <?php } ?>
								                            </div>
														</div>
													</div>

												</div>


											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</section>

		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<?php if(!empty($_SESSION['retour'])){ ?>
				        <a href="<?= $_SESSION['retour']; ?>" class="btn btn-default btn-sm">
				          <i class="fa fa-long-arrow-left"></i>
				          Retour
				        </a>
				    <?php } ?>
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
					<button type="submit" class="btn btn-success btn-sm" id="fou_submit" style="display:none;">
						<i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php
include "includes/footer_script.inc.php"; ?>	

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->
<script src="assets/js/custom.js"></script>
<script src="vendor/plugins/holder/holder.min.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->


<script>
// DOCUMENT READY //
jQuery(document).ready(function () {
///////////////// INTERRACTIONS UTILISATEUR ///////////////////
	////////// INITIALISATIONS ////////////////
	$('#fou_submit').click(function() {
	      checked = $("input[type=checkbox]:checked").length;

	      if(!checked) {
	        alert("Vous devez au moins sélectionner une famille.");
	        return false;
	      }

    });
	// select2
	$(".chosenselect").select2();
	// groupe, maisons mères et filiales
	$(".maisonm").hide();
	$(".filiale").hide();
	// masque sur l'iban
	$('.iban').mask('SS00 0000 0000 0000 0000 00', {
		placeholder: '____ ____ ____ ____ ____ __'
	});
	// masque sur siret long
	$('.siretlong').mask('000 000 000 000 00');
	////////// FIN INITIALISATIONS ////////////
	// code du fournisseur == code sage
	$( "#fou_code" ).focusout(function() {
		$("#fou_sage").val($(this).val());
	});
	// fin code du fournisseur == code sage

	// lorsqu'on change le type, on actualise le groupe
	$( "#fou_type" ).change(function() {
		// si le type est un auto entrepreneur, cacher le groupe
		// actualisation des filiales lorsque le type change
		selected_id = $(this).val();
    	$.ajax({
		    type:'POST',
		    url: 'ajax/ajax_fournisseur_filiale.php',
		    data : 'field=' + selected_id,
		    dataType: 'json',
		    success: function(data)         
		    {
		        if (data['0'] == undefined){

		          $("#fou_filiale_de").find("option:gt(0)").remove();

		        }else{
		          $("#fou_filiale_de").find("option:gt(0)").remove();
		            $.each(data, function(key, value) {
			            $('#fou_filiale_de')
			            .append($("<option></option>")
			              .attr("value",value["fou_id"])
			              .text(value["fou_code"]));
		            });
		        }
		    }
		});

    	// fin actualisation des filiales lorsque le type change
		if($(this).val() == 3){
			$('.groupe').hide();
			$(".maisonm").hide();
			$(".filiale").hide();
		}else{
			$('.groupe').show();
			$('#fou_groupe').attr("checked", false);
			$(".maisonm").hide();
			$(".filiale").hide();
		}
	  
	});
	// fin lorsqu'on change le type, on actualise le groupe

	// actualisation du groupe avec la checkbox groupe
	$('#fou_groupe').click(function() {
        if($("#fou_groupe").is(':checked')){

	        $(".maisonm").show();
			if($("#fou_maison_mere").is(':checked')){
	            $(".filiale").hide();
	        }else if($("#fou_maison_mere_non").is(':checked')){
	       		$(".filiale").show();
	      	}
        
	    }else{
	      	$(".maisonm").hide();
			$(".filiale").hide();
	    }
    });
    // fin actualisation du groupe avec la checkbox groupe

    // actualisation des filiales et maisons mères
    $('#fou_maison_mere').click(function() {
		if($("#fou_maison_mere").is(':checked')){
            $(".filiale").hide();
        }else if($("#fou_maison_mere_non").is(':checked')){
        	
       		$(".filiale").show();
      	}
    });
	$('#fou_maison_mere_non').click(function() {
		if($("#fou_maison_mere").is(':checked')){
            $(".filiale").hide();
        }else if($("#fou_maison_mere_non").is(':checked')){
        	
       		$(".filiale").show();
      	}
    });
    // fin actualisation des filiales et maisons mères



    // VERIFICATION DES INFORMATIONS
	$( "#verif" ).click(function() {
		
		$('#loader').show();
		var code= $('#fou_code').val();
		var nom= $('#fou_nom').val();
		var siret= $('#fou_siret').val();
		var siren= $('#fou_siren').val();
		// initialisations des messages
		$('#msg3').hide();
		$('#msg4').hide();
		
		if($('#fou_code').val() == ""){
			$('#msg3').show(400);
		}else if($('#fou_siren').val() == "" || $('#fou_siret').val() == ""){
			$('#msg4').show(400);
		}else{
			// encodage du nom
			nom = encodeURIComponent(nom);
			// requête ajax pour verifier si le fournisseur existe ou pas
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_fournisseur_search.php',
		        // on envoie les données en post
		        data : {"code":code, "nom":nom, "siret":siret, "siren" : siren},
		        /*"code=" + code + "&nom=" + nom + "&siret=" + siret + "&siren=" + siren,*/
		     	dataType: 'html',                  
	      		success: function(data){
			      	if (data == "OK"){
			      		$( "#verif" ).hide();
			      		$('#msg1').show(400);
			      		$('#msg2').hide(400);
			      		$('#msg4').hide(400);
			      		$('.rest-form').show(400);
			      		$('#fou_submit').show();
			      	}else if(data == "CODE"){
			      		$('#msg1').hide(400);
			      		$('#msg2').show(400);
			      		$('#msg4').hide(400);
			      		$('#fou_submit').hide();
			      	}else{
			      		// rediriger vers la fiche fournisseur
			      		window.location.replace("fournisseur_voir.php?fournisseur=" + data + "&error=2");
			      		/*$('#msg1').hide(400);
			      		$('#msg2').show(400);
			      		$('#msg4').hide(400);
			      		$('#fou_submit').hide();*/
			      	}
				},
      			complete: function(){
      				$('#loader').hide();
      			}
  			});

		}
	});

	// FIN VERIFICATION DES INFORMATIONS
	// GESTION DES MODALITES DE REGLEMENTS						
	$("input[name=reg_formule]").click(function (){
		reg_formule=$("input[name=reg_formule]:checked").val();
		$('.champ-reg-formule').each(function(){
			if($(this).hasClass("reg-formule-" + reg_formule)){
				$(this).prop("disabled",false);	
			}else{
				$(this).val(0);	
				$(this).prop("disabled",true);	
			}
		});							
	});
	$(".champ-reg-formule").blur(function(){
		//console.log($(this).val());
		//console.log($(this).prop("max"));
		if( ($(this).val()*1)>($(this).prop("max")*1) ){
			$(this).val($(this).prop("max"));
		}
	});
	// FIN GESTION DES MODALITES DE REGLEMENTS

////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
});

///////////// FONCTIONS //////////////
function nomchange(selected){

	if(selected == "autre"){
		$(".fco_fonction_nom_style").show(400);
	}else{
		$(".fco_fonction_nom_style").hide(400);
	}

}
//////////// FIN FONCTIONS //////////


</script>

</body>
</html>