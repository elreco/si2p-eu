<?php

	// LISTES DES AVIS DE STAGES
	
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");
	
	include_once("modeles/mod_parametre.php");

	$_SESSION["retour"]="param_avis_liste.php";
	
	// AVIS INTRA
	$req=$Conn->query("SELECT * FROM Avis WHERE avi_type=1 ORDER BY avi_statut;");
	$d_avis_intra=$req->fetchAll();
	
	// AVIS INTER
	// 2 req pour savoir si intra ou inter empty
	$req=$Conn->query("SELECT * FROM Avis WHERE avi_type=2 ORDER BY avi_statut;");
	$d_avis_inter=$req->fetchAll();
	
	$avis_statut=array(
		"0" => array(
			"nom" => "Production",
			"classe" => "text-info"
		),
		"1" => array(
			"nom" => "Application",
			"classe" => "text-success"
		),
		"2" => array(
			"nom" => "Archivé",
			"classe" => "text-danger"
		)
	);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<!--<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">-->
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	   
	   <!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper" class="" >
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
						
							<div class="admin-form theme-primary ">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="text-center">
											<div class="content-header">
												<h2>Avis de <b class="text-primary">stage</b></h2>
											</div>
											

											<div class="row" >
												<div class="col-md-6" >			
													<div class="panel">
														<div class="panel-heading">
															<span class="panel-title">Avis de stage INTRA</span>
														</div>
														<div class="panel-body">
													<?php	$add_avis=true;
															if(!empty($d_avis_intra)){ ?>
																<table class="table" >
																	<thead>
																		<tr>
																			<th>Nom</th>
																			<th>Statut</th>
																			<th>Mis en application le</th>
																			<th>Archivé le</th>
																			<th>&nbsp;</th>
																		</tr>
																	</thead>
																	<tbody>
																<?php	foreach($d_avis_intra as $avis){ 
																			if($avis["avi_statut"]==0){
																				$add_avis=false;
																			} ?>
																			<tr class="<?=$avis_statut[$avis["avi_statut"]]["classe"]?>" >
																				<td><?=$avis["avi_nom"]?></td>
																				<td><?=$avis_statut[$avis["avi_statut"]]["nom"]?></td>
																				<td><?=convert_date_txt($avis["avi_prod_deb"])?></td>
																				<td><?=convert_date_txt($avis["avi_prod_fin"])?></td>
																				<td>
																		<?php		if($avis["avi_statut"]==0){ ?>
																						<a href="param_avis.php?avis=<?=$avis["avi_id"]?>" class="btn btn-sm btn-warning"  data-toggle="tooltip" title="Modifier l'avis">
																							<i class="fa fa-pencil" ></i>
																						</a>															
																			<?php	} ?>														
																					<a href="param_avis_voir.php?avis=<?=$avis["avi_id"]?>" class="btn btn-sm btn-info" data-toggle="tooltip" title="Voir l'avis">
																						<i class="fa fa-eye" ></i>
																					</a>																										
																			
																				</td>	
																			</tr>
															<?php		} ?>
																	</tbody>
																</table>
													<?php	} ?>		
															
														</div>
												<?php	if($add_avis){ ?>
															<div class="panel-footer">
																<a href="param_avis.php?type=1" class="btn btn-sm btn-success" >
																	<i class="fa fa-plus" ></i>  Nouvelle avis Intra
																</a>
															</div>
												<?php	} ?>
													</div>
												
												</div>
												<div class="col-md-6" >	
													<div class="panel">
														<div class="panel-heading">
															<span class="panel-title">Avis de stage INTER</span>
														</div>
														<div class="panel-body">
													<?php	$add_avis=true;
															if(!empty($d_avis_inter)){ ?>
																<table class="table" >
																	<thead>
																		<tr>
																			<th>Nom</th>
																			<th>Statut</th>
																			<th>Mis en application le</th>
																			<th>Archivé le</th>
																			<th>&nbsp;</th>
																		</tr>
																	</thead>
																	<tbody>
																<?php	foreach($d_avis_inter as $avis){ 
																			if($avis["avi_statut"]==0){
																				$add_avis=false;
																			} ?>
																			<tr class="<?=$avis_statut[$avis["avi_statut"]]["classe"]?>" >
																				<td><?=$avis["avi_nom"]?></td>
																				<td><?=$avis_statut[$avis["avi_statut"]]["nom"]?></td>
																				<td><?=convert_date_txt($avis["avi_prod_deb"])?></td>
																				<td><?=convert_date_txt($avis["avi_prod_fin"])?></td>
																				<td>
																		<?php		if($avis["avi_statut"]==0){ ?>
																						<a href="param_avis.php?avis=<?=$avis["avi_id"]?>" class="btn btn-sm btn-warning"  data-toggle="tooltip" title="Modifier l'avis">
																							<i class="fa fa-pencil" ></i>
																						</a>															
																			<?php	} ?>														
																					<a href="param_avis_voir.php?avis=<?=$avis["avi_id"]?>" class="btn btn-sm btn-info"  data-toggle="tooltip" title="Voir l'avis">
																						<i class="fa fa-eye" ></i>
																					</a>																										
																			
																				</td>	
																			</tr>
																<?php	} ?>
																	</tbody>
																</table>
													<?php	} ?>		
															
														</div>
												<?php	if($add_avis){ ?>
															<div class="panel-footer">
																<a href="param_avis.php?type=2" class="btn btn-sm btn-success" >
																	<i class="fa fa-plus" ></i>  Nouvelle avis Inter
																</a>
															</div>
												<?php	} ?>
													</div>
												</div>
											</div>
											
									
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</section>				
			</section>
		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
				<!--	<button type="submit" class="btn btn-success btn-sm">
						<i class='fa fa-save'></i> Enregistrer
					</button>-->
				</div>
			</div>
		</footer>
<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" >
			jQuery(document).ready(function (){
				
			});
		</script>			
	</body>
</html>
