<?php 
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_client.php');

Global $ConnSoc;
// selection du suspect
$req = $ConnSoc->prepare("SELECT * FROM suspects WHERE sus_id=" . $_GET['client']);
$req->execute();
$suspect = $req->fetch();

// les adresses du suspect
$req2 = $ConnSoc->prepare("SELECT * FROM suspects_adresses WHERE sad_ref_id=" . $_GET['client']);
$req2->execute();
$suspect_adresses = $req2->fetchAll();

// les contacts du suspect
$req3 = $ConnSoc->prepare("SELECT * FROM suspects_contacts WHERE sco_ref_id=" . $_GET['client']);
$req3->execute();
$suspect_contacts = $req3->fetchAll();

// les correspondances du suspect
$req8 = $ConnSoc->prepare("SELECT * FROM correspondances WHERE cor_client=" . $_GET['client']);
$req8->execute();
$suspect_correspondances = $req8->fetchAll();


$erreur = 0;
if($suspect['sus_categorie'] != 3){
	foreach($suspect_adresses as $sad){
		if(!empty($sad['sad_siret']) && !empty($suspect['sus_siren'])){
			$erreur = 0;
		}else{
			$erreur = 1;	
		}

	}
}
if(empty($suspect_contacts)){
	$erreur = 2;
}
if(empty($suspect_adresses)){

	$erreur = 3;
}


if($erreur == 0){

	foreach($suspect_adresses as $sad){
		if($sad['sad_type'] == 1){
			$erreur = 0;
			break;
		}else{
			$erreur = 3;
		}
		
	}
}
if($erreur == 0){
	foreach($suspect_adresses as $sad){

		if($sad['sad_type'] == 2){
			$erreur = 0;
			break;
		}else{
			$erreur = 4;
		}
	}
}
if(empty($suspect['sus_nom'])){

	$erreur = 5;
}
if(empty($suspect['sus_code'])){

	$erreur = 6;
}

if($erreur == 0){

	// insertion d'un nouveau client
	if($suspect['sus_filiale_de_type'] == 1){
		$suspect['sus_filiale_de_type'] = 0;
	}
	if($suspect['sus_groupe'] == 1 && $suspect['sus_filiale_de'] == 0){
		$suspect['sus_groupe'] = 1;
		$suspect['sus_filiale_de'] = 0;
	}


	foreach($suspect_adresses as $sad){
		if($sad['sad_defaut'] == 1){
				$departement = substr($sad['sad_cp'],0,2);
				$req8 = $Conn->prepare("SELECT * FROM societes_departements WHERE sde_departement=" . $departement . " AND sde_gc = 1");
				$req8->execute();
				$dep = $req->fetch();

		}
	}

	$req4 = $Conn->prepare("INSERT INTO clients ( cli_code, cli_nom, cli_agence,cli_societe, cli_commercial, cli_categorie, cli_sous_categorie,cli_stagiaire_type, cli_secteur, cli_sous_secteur, cli_classification, cli_sous_classification, cli_date_creation, cli_reg_type, cli_siren, cli_ape, cli_uti_creation, cli_opca, cli_facture_opca, cli_reg_formule, cli_reg_nb_jour, cli_reg_fdm, cli_ident_tva, cli_groupe, cli_filiale_de, cli_filiale_de_type, cli_releve, cli_reference, cli_region_soc, cli_region_age)
	 VALUES 
	 ( :cli_code, :cli_nom, :cli_agence, :cli_societe, :cli_commercial,  :cli_categorie,:cli_sous_categorie,:cli_stagiaire_type, :cli_secteur, :cli_sous_secteur, :cli_classification, :cli_sous_classification, NOW(), :cli_reg_type, :cli_siren, :cli_ape, :creator, :cli_opca, :cli_facture_opca, :cli_reg_formule, :cli_reg_nb_jour, :cli_reg_fdm, :cli_ident_tva, :cli_groupe, :cli_filiale_de, :cli_filiale_de_type, :cli_releve, :cli_reference, :cli_region_soc, :cli_region_age)");
	$req4->bindParam("cli_code",$suspect['sus_code']); 
	$req4->bindParam("cli_nom",$suspect['sus_nom']);
	$req4->bindParam("cli_societe",$_SESSION['acces']['acc_societe']);
	$req4->bindParam("cli_agence",$suspect['sus_agence']);
	$req4->bindParam("cli_commercial",$suspect['sus_commercial']);
	$req4->bindParam("cli_stagiaire_type",$suspect['sus_stagiaire_type']);
	$req4->bindParam("cli_categorie",$suspect['sus_categorie']);
	$req4->bindParam("cli_sous_categorie",$suspect['sus_sous_categorie']);
	$req4->bindParam("cli_secteur",$suspect['sus_secteur']);
	$req4->bindParam("cli_sous_secteur",$suspect['sus_sous_secteur']);
	$req4->bindParam("cli_classification",$suspect['sus_classification']);
	$req4->bindParam("cli_reg_type",$suspect['sus_reg_type']);
	$req4->bindParam("cli_siren",$suspect['sus_siren']);
	$req4->bindParam("cli_ape",$suspect['sus_ape']);
	$req4->bindParam("cli_opca",$suspect['sus_opca']);
	$req4->bindParam("cli_facture_opca",$suspect['sus_facture_opca']);
	$req4->bindParam("cli_reg_formule",$suspect['sus_reg_formule']);
	$req4->bindParam("cli_reg_nb_jour",$suspect['sus_reg_nb_jour']); 
	$req4->bindParam("cli_reg_fdm",$suspect['sus_reg_fdm']);
	$req4->bindParam("cli_sous_classification",$suspect['sus_sous_classification']);
	$req4->bindParam("creator",$suspect['sus_uti_creation']);
	$req4->bindParam("cli_ident_tva",$suspect['sus_ident_tva']);
	$req4->bindParam("cli_groupe",$suspect['sus_groupe']);
	$req4->bindParam("cli_filiale_de",$suspect['sus_filiale_de']);
	$req4->bindParam("cli_filiale_de_type",$suspect['sus_filiale_de_type']);
	$req4->bindParam("cli_releve",$suspect['sus_releve']);
	$req4->bindParam("cli_reference",$suspect['sus_reference']);
	$req4->bindParam("cli_region_soc",$dep['sde_societe']);
	$req4->bindParam("cli_region_age",$dep['sde_agence']);

	$req4->execute();
	$last = $cli_id;

	$req4 = $ConnSoc->prepare("INSERT INTO clients (cli_id, cli_code, cli_nom, cli_agence, cli_commercial, cli_categorie, cli_sous_categorie)
	 VALUES 
	 (:cli_id, :cli_code, :cli_nom, :cli_agence, :cli_societe, :cli_commercial , :cli_categorie,:cli_sous_categorie)");
	$req4->bindParam("cli_id",$last);
	$req4->bindParam("cli_code",$suspect['sus_code']); 
	$req4->bindParam("cli_nom",$suspect['sus_nom']);
	$req4->bindParam("cli_agence",$suspect['sus_agence']);
	$req4->bindParam("cli_commercial",$suspect['sus_commercial']);
	$req4->bindParam("cli_categorie",$suspect['sus_categorie']);
	$req4->bindParam("cli_sous_categorie",$suspect['sus_sous_categorie']);

	$req4->execute();
	// 
	$req41 = $ConnSoc->prepare("DELETE FROM suspects WHERE sus_id=" . $_GET['client']);
	$req41->execute();

	// les adresses

	foreach($suspect_contacts as $sco){

		$req5 = $Conn->prepare("INSERT INTO contacts (con_ref, con_ref_id, con_fonction, con_fonction_nom, con_titre, con_nom, con_prenom, con_tel, con_portable, con_fax, con_mail, con_defaut, con_compta) VALUES (:con_ref, :con_ref_id, :con_fonction, :con_fonction_nom, :con_titre, :con_nom, :con_prenom, :con_tel, :con_portable, :con_fax, :con_mail, :con_defaut, :con_compta);");

		$req5->bindParam("con_ref",$sco['sco_ref']);
		$req5->bindParam("con_ref_id",$last);
		$req5->bindParam("con_fonction",$sco['sco_fonction']);
		$req5->bindParam("con_fonction_nom",$sco['sco_fonction_nom']);
		$req5->bindParam("con_titre",$sco['sco_titre']); 
		$req5->bindParam("con_nom",$sco['sco_nom']);
		$req5->bindParam("con_prenom",$sco['sco_prenom']);
		$req5->bindParam("con_tel",$sco['sco_tel']);
		$req5->bindParam("con_portable",$sco['sco_portable']);
		$req5->bindParam("con_fax",$sco['sco_fax']);
		$req5->bindParam("con_mail",$sco['sco_mail']);
		$req5->bindParam("con_defaut",$sco['sco_defaut']);
		$req5->bindParam("con_compta",$sco['sco_compta']);

		$req5->execute();

		$cli_last_con_id = $Conn->lastInsertId();
		if($sco['sco_defaut'] == 1){
			$req9 = $Conn->prepare("UPDATE clients SET cli_con_defaut = " . $cli_last_con_id . " WHERE cli_id = " . $last);
			$req9->execute();
		}

		$req42 = $ConnSoc->prepare("DELETE FROM suspects_contacts WHERE sco_id=" . $sco['sco_id']);
		$req42->execute();
	}
	// les contacts

	foreach($suspect_adresses as $sad){

		if($sad['sad_defaut'] == 1){
			$departement = substr($sad['sad_cp'],0,2);
			$req8 = $Conn->prepare("SELECT * FROM societes_departements WHERE sde_departement=" . $departement . " AND sde_gc = 1");
			$req8->execute();
			$dep = $req8->fetch();
		}
        

		$req6 = $Conn->prepare("INSERT INTO adresses (adr_ref, adr_ref_id, adr_type, adr_nom, adr_service, adr_ad1, adr_ad2, adr_ad3, adr_cp, adr_ville, adr_defaut, adr_libelle, adr_geo, adr_siret) VALUES (:adr_ref, :adr_ref_id, :adr_type, :adr_nom, :adr_service, :adr_ad1, :adr_ad2, :adr_ad3, :adr_cp, :adr_ville, :adr_defaut, :adr_libelle, :adr_geo, :adr_siret);");
		
		$req6->bindParam("adr_ref",$sad['sad_ref']);
		$req6->bindParam("adr_ref_id",$last);
		$req6->bindParam("adr_type",$sad['sad_type']);
		$req6->bindParam("adr_nom",$sad['sad_nom']);
		$req6->bindParam("adr_service",$sad['sad_service']);
		$req6->bindParam("adr_ad1",$sad['sad_ad1']);
		$req6->bindParam("adr_ad2",$sad['sad_ad2']);
		$req6->bindParam("adr_ad3",$sad['sad_ad3']);
		$req6->bindParam("adr_cp",$sad['sad_cp']);
		$req6->bindParam("adr_ville",$sad['sad_ville']);
		$req6->bindParam("adr_defaut",$sad['sad_defaut']);
		$req6->bindParam("adr_libelle",$sad['sad_libelle']);
		$req6->bindParam("adr_geo",$sad['sad_geo']);
		$req6->bindParam("adr_siret",$sad['sad_siret']);
		$req6->execute();

		$cli_last_adr_id = $Conn->lastInsertId();
		if($sad['sad_defaut'] == 1 && $sad['sad_type'] == 1){
			$req9 = $Conn->prepare("UPDATE clients SET cli_adr_int_defaut = " . $cli_last_adr_id . " WHERE cli_id = " . $last);
			$req9->execute();
		}elseif($sad['sad_defaut'] == 1 && $sad['sad_type'] == 2){
			$req9 = $Conn->prepare("UPDATE clients SET cli_adr_fac_defaut = " . $cli_last_adr_id . " WHERE cli_id = " . $last);
			$req9->execute();
		}
		


		$req61 = $ConnSoc->prepare("DELETE FROM suspects_adresses WHERE sad_id=" . $sad['sad_id']);
		$req61->execute();
		

	}

	foreach($suspect_correspondances as $cor){
		$req9 = $Conn->prepare("INSERT INTO correspondances (cor_date, cor_utilisateur, cor_commentaire, cor_contact_tel, cor_contact, cor_contact_nom, cor_rappeler_le, cor_client) VALUES (:sco_date, :utilisateur, :commentaire, :tel, :contact, :nom, :rappeler_le, :suspect);");

		$req9->bindParam("sco_date",$cor['sco_date']);
		$req9->bindParam("utilisateur",$cor['sco_utilisateur']);
		$req9->bindParam("commentaire",$cor['sco_commentaire']);
		$req9->bindParam("tel",$cor['sco_contact_tel']);
		$req9->bindParam("contact",$cor['sco_contact']);
		$req9->bindParam("nom",$cor['sco_contact_nom']);
		$req9->bindParam("rappeler_le",$cor['sco_rappeler_le']);
		$req9->bindParam("suspect",$last);
		

		$req91 = $ConnSoc->prepare("DELETE FROM suspects_correspondances WHERE sco_id=" . $cor['sco_id']);
		$req91->execute();

	}
	Header("Location: client_voir.php?client=" . $last);
}elseif($erreur == 1){
	Header("Location: suspect_voir.php?client=" . $_GET['client'] . "&error=1");
}elseif($erreur == 2){
	Header("Location: suspect_voir.php?client=" . $_GET['client'] . "&error=2");
}elseif($erreur == 3){
	Header("Location: suspect_voir.php?client=" . $_GET['client'] . "&error=3");
}elseif($erreur == 4){
	Header("Location: suspect_voir.php?client=" . $_GET['client'] . "&error=4");
}


?>
