<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	
	$question=0;
	if(!empty($_GET["question"])){
		$question=intval($_GET["question"]);	
	}
	
	// LISTE DES RUBRIQUES
	$sql="SELECT * FROM Avis_Rubriques aru_libelle;";
	$req=$Conn->query($sql);
	$d_rubriques=$req->fetchAll();
	
	// LA QUESTION
	
	if($question>0){
		$sql="SELECT * FROM Avis_Questions WHERE aqu_id=" . $question . ";";
		$req=$Conn->query($sql);
		$d_question=$req->fetch();	
	}else{
		$d_question=array(
			"aqu_texte" => "",
			"aqu_min_lib" => "",
			"aqu_min_val" => 0,
			"aqu_max_lib" => "",
			"aqu_max_val" => "1",
			"aqu_rubrique" => 0
		);
	}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Si2P</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

			<!-- Theme CSS PAR DEFAUT -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<!-- plugin -->
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >

		
		<!-- Personalisation du theme -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<!-- Favicon -->
		<link rel="shortcut icon" href="theme/assets/img/favicon.ico" >

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm" >
	
		<form method="post" action="param_avis_question_enr.php" id="form" >
			<div>
				<input type="hidden" name="question" value="<?=$question?>" />
			</div>
			<!-- Start: Main -->
			<div id="main" >
				
		<?php	include "includes/header_def.inc.php"; ?>
				
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper" >
					<!-- DEBUT DU CONTENU -->
					<section id="content" class=""  >

						<div class="content-header">
					<?php	if($question>0){ ?>
								<h2>&Eacute;dition d'une <b class="text-primary" >question</b></h2>
					<?php	}else{ ?>
								<h2>Nouvelle <b class="text-primary" >question</b></h2>
					<?php	}?>
						</div>
						
						<div class="row" >
							<div class="col-md-offset-1 col-md-10" >
							
								<div class="admin-form theme-primary">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
										
											<!-- debut du contenu du form -->
											<div class="row">
												<div class="col-md-6">													
													<label for="aqu_rubrique" >Rubrique :</label>	
													<select class="select2" name="aqu_rubrique" >
												<?php	if(!empty($d_rubriques)){
															foreach($d_rubriques as $d_rubrique){
																if($d_rubrique["aru_id"]==$d_question["aqu_rubrique"]){
																	echo("<option value='" . $d_rubrique["aru_id"] . "' selected >" . $d_rubrique["aru_libelle"] . "</option>");
																}else{
																	echo("<option value='" . $d_rubrique["aru_id"] . "'  >" . $d_rubrique["aru_libelle"] . "</option>");
																}
															}								
														} ?>
													</select>
												</div>
											</div>
											
											<div class="row mt15">
												<div class="col-md-12">													
													<label for="aqu_texte" >Question :</label>												
													<textarea class="summernote required" id="aqu_texte" placeholder="Question..." name="aqu_texte" required ><?=$d_question["aqu_texte"]?></textarea>
													<small class="text-danger texte_required" id="aqu_texte_required" >Merci de saisir votre question</small>													
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="section-divider mb20">
														<span>Minimum</span>
													</div>
												</div>
												<div class="col-md-6">
													<div class="section-divider mb20">
														<span>Maximum</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">													
													<label for="aqu_min_lib" >Libellé min :</label>												
													<span class="field">
														<input type="text" name="aqu_min_lib" id="aqu_min_lib" class="gui-input" value="<?=$d_question["aqu_min_lib"]?>" placeholder="Libellé de la valeur minimum" required />
														<small class="text-danger texte_required" id="aqu_min_lib_required" ></small>
													</span>
												</div>
												<div class="col-md-6">													
													<label for="aqu_max_lib" >Libellé max :</label>												
													<span class="field">
														<input type="text" name="aqu_max_lib" id="aqu_max_lib" class="gui-input" value="<?=$d_question["aqu_max_lib"]?>" placeholder="Libellé de la valeur maximum" required />
														<small class="text-danger texte_required" id="aqu_max_lib_required" ></small>
													</span>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">													
													<label for="aqu_min_val" >Valeur min :</label>												
													<span class="field">
														<input type="number" min="0" step="1" name="aqu_min_val" id="aqu_min_val" class="gui-input" value="<?=$d_question["aqu_min_val"]?>" placeholder="Valeur minimum" required />
														<small class="text-danger texte_required" id="aqu_min_val_required" ></small>
													</span>
												</div>
												<div class="col-md-6">													
													<label for="aqu_max_val" >Valeur max :</label>												
													<span class="field">
														<input type="number" min="1" step="1" name="aqu_max_val" id="aqu_max_val" class="gui-input" value="<?=$d_question["aqu_max_val"]?>" placeholder="Valeur maximum" required />
														<small class="text-danger texte_required" id="aqu_max_val_required" ></small>
													</span>
												</div>
											</div>
											
											<div class="section-divider mb20">
												<span>Validation</span>
											</div>
											<div class="row">
												<div class="col-md-12 pt25">																							
													<label class="option">
														<input type="checkbox" id="aqu_valide" name="aqu_valide" value="on" />
														<span class="checkbox"></span>Valider cette question
													</label>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>

					</section>
					<!-- FIN DU CONTENU -->
				</section>
	
			</div>
			<!-- End: Main -->
			
			<!-- DEBUT DU FOOTER -->
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="<?=$_SESSION["retour"]?>" class="btn btn-default btn-sm" >
							<i class='fa fa-left'></i> Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" >
					</div>
					<div class="col-xs-3 footer-right" >
						<button type="button" class="btn btn-success btn-sm" id="sub_form" >
							<i class='fa fa-save'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
			<!-- FIN DE FOOTER  -->
			
		</form>
		
		<div id="modal_confirmation_user" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Confirmer ?</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							Annuler
						</button>
						<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
							Confirmer
						</button>
					</div>
				</div>
			</div>
		</div>

		
<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script> 
		<!--<script src="theme/vendor/plugins/select2/select2.min.js"></script>
		<script src="theme/assets/admin-tools/admin-forms/jquery-ui-datepicker.min.js"></script>
		<script src="theme/vendor/plugins/summernote/summernote.min.js"></script>
		<script src="theme/vendor/plugins/summernote/summernote-fr-FR.js"></script>-->

		<script type="text/javascript">
			jQuery(document).ready(function() {
				$("#sub_form").click(function(e){
					//if(valider_form($("#form"))){
						// si validation de la question on demande confirmation
						if($("#aqu_valide").prop("checked")==true){
							modal_confirmation_user("Vous êtes sur le point de valider cette question.<br/>Une fois la question validée, vous ne pourrez plus la re-modifier.<br/>Êtes-vous sûr de vouloir continuer ?",enregistrer_question,"","");
						}else{
							enregistrer_question();
						}
					//}			
				});	
				
				$("#aqu_max_val").change(function(){
					max_val=1*$("#aqu_min_val").val()+1*1;
					if($.isNumeric($("#aqu_max_val").val())){
						if($("#aqu_max_val").val()>0 && $("#aqu_max_val").val()>$("#aqu_min_val").val()){
							max_val=$("#aqu_max_val").val();		
						}
					}
					$("#aqu_max_val").val(max_val);
				});
				
				$("#aqu_min_val").change(function(){
					min_val=0;
					if($.isNumeric($("#aqu_min_val").val())){
						if($("#aqu_min_val").val()>=0 && $("#aqu_min_val").val()<$("#aqu_max_val").val()){
							min_val=$("#aqu_min_val").val();		
						}
					}
					$("#aqu_min_val").val(min_val);
				});
				
			});
			
			function enregistrer_question(){
				$("#form").submit();
			}
			
		</script>
	</body>
</html>