<?php
include "includes/controle_acces.inc.php";

if(!$_SESSION["acces"]["acc_droits"][24]){
	Header('Location: deconnect.php');
	die();
}

	include_once 'includes/connexion.php';
	
	$erreur="";
	if(!empty($_POST)){
	
		$produit=0;
		if(isset($_POST['produit'])){
			$produit=intval($_POST['produit']);	
		}
		
		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=intval($_SESSION['acces']["acc_agence"]);	
		}
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);	
		}

		if($produit>0){
			
			$sql_select="SELECT pso_produit FROM Produits_Societes 
			WHERE pso_produit=:produit AND pso_societe=:acc_societe AND pso_agence=:acc_agence;";
			$req_select=$Conn->prepare($sql_select);
			$req_select->bindParam(":produit",$produit);
			$req_select->bindParam(':acc_societe', $acc_societe);
			$req_select->bindParam(':acc_agence',$acc_agence);
			$req_select->execute();
			$existe=$req_select->fetch();
			if(!empty($existe)){
				
				if(empty($_POST['pso_devis_txt']) AND empty($_POST['pso_reference_sta'])){
					// DELETE
					$req = $Conn->prepare("DELETE FROM Produits_Societes WHERE pso_produit=:produit AND pso_societe=:acc_societe AND pso_agence=:acc_agence");
					$req->bindParam(':produit', $produit);
					$req->bindParam(':acc_societe', $acc_societe);
					$req->bindParam(':acc_agence',$acc_agence);
					$req->execute();
					
				}else{
					$req = $Conn->prepare("UPDATE Produits_Societes SET pso_devis_txt=:pso_devis_txt,pso_reference_sta = :pso_reference_sta
					WHERE pso_produit=:produit AND pso_societe=:acc_societe AND pso_agence=:acc_agence");
					$req->bindParam(':produit', $produit);
					$req->bindParam(':acc_societe', $acc_societe);
					$req->bindParam(':acc_agence',$acc_agence);
					$req->bindParam(':pso_devis_txt', $_POST['pso_devis_txt']);
					$req->bindParam(':pso_reference_sta', $_POST['pso_reference_sta']);
					$req->execute();
				}
			}elseif(!empty($_POST['pso_devis_txt']) OR !empty($_POST['pso_reference_sta'])){
				// INSERT
				$req = $Conn->prepare("INSERT INTO Produits_Societes 
				(pso_produit,pso_societe,pso_agence,pso_devis_txt,pso_reference_sta) 
				VALUES (:produit,:acc_societe,:acc_agence,:pso_devis_txt,:pso_reference_sta);");
				$req->bindParam(':produit', $produit);
				$req->bindParam(':acc_societe', $acc_societe);
				$req->bindParam(':acc_agence',$acc_agence);
				$req->bindParam(':pso_devis_txt', $_POST['pso_devis_txt']);
				$req->bindParam(':pso_reference_sta', $_POST['pso_reference_sta']);
				$req->execute();
				
			}
		}else{
			$erreur="Mise à jour impossible!";
		}
	}else{
		$erreur="Mise à jour impossible!";
	}
	
	if(!empty($erreur)){
		$_SESSION['message'][] = array(
			"titre" => "Attention",
			"type" => "danger",
			"message" => $erreur
		);
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Enregistrement terminé",
			"type" => "success",
			"message" => "Les données produits ont été mises à jours!"
		);
	}
	header("Location: produit_liste.php");
?>
