 <?php
 
	include "includes/controle_acces.inc.php";
	
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");
	
	// VISUALISATION D'UN DOSSIER CACES
	
	$erreur_txt="";
	
	$dossier_id=0;
	if(isset($_GET["dossier"])){
		if(!empty($_GET["dossier"])){
			$dossier_id=intval($_GET["dossier"]);
		}	
	}
	
	if($dossier_id>0){
		
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
		}
		if($acc_societe!=$conn_soc_id){
			$acc_societe=$conn_soc_id;
		}
		
		
		// LE DIPLOME
		
		$sql="SELECT * FROM Diplomes_Caces WHERE dca_id=:dca_id;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":dca_id",$dossier_id);
		$req->execute();
		$d_dossier=$req->fetch();
		if(!empty($d_dossier)){

			// on identifie le premier passage en attente de validation			
			for($bcl=1;$bcl<=6;$bcl++){
				if($d_dossier["dca_clos_" . $bcl]==0){
					$passage=$bcl;
					if($d_dossier["dca_action_soc_" . $bcl]!=$acc_societe){
						$erreur_txt="Vous ne pouvez pas valider un passage qui a été enregistré sur une autre agence du réseau.";
					}
					break;
				}
			}

		}
		
		if(empty($erreur_txt)){
			
			// L'ATTESTATION ASSOCIE
			
			$sql_get_sta="SELECT ast_attestation FROM Actions_Stagiaires WHERE ast_stagiaire=:stagiaire AND ast_action=:action;";
			$req_get_sta=$ConnSoc->prepare($sql_get_sta);
			$req_get_sta->bindParam(":stagiaire",$d_dossier["dca_stagiaire"]);
			$req_get_sta->bindParam(":action",$d_dossier["dca_action_" . $passage]);
			$req_get_sta->execute();
			$d_action_sta=$req_get_sta->fetch();
			if(empty($d_action_sta)){
				$erreur_txt="Impossible de charger les données d'inscription du stagiaire";
			}
		}
		
		// ON VERIFIE QUE CHAQUE CATEGORIE DEMANDE A ETE TESTE
		if(empty($erreur_txt)){
		
			$sql="SELECT dcc_categorie FROM Diplomes_Caces_Cat WHERE dcc_diplome=:dossier AND dcc_passage=:passage
			AND (ISNULL(dcc_date) OR dcc_testeur=0);";
			$req=$Conn->prepare($sql);
			$req->bindParam(":dossier",$dossier_id);
			$req->bindParam(":passage",$passage);
			$req->execute();
			$d_cat_non_teste=$req->fetchAll();
			if(!empty($d_cat_non_teste)){
				$erreur_txt="Certaines categories demandées n'ont pas été testées.";
			}
		}
		
		if(empty($erreur_txt)){
			
			// SI CERTIFICAT CACES VALIDE
			if($d_dossier["dca_diplome"]==2 AND !empty($d_dossier["dca_date"])) {
				
				// LES CODE RECO
			
				$caces=array(
					"7" => "489",
					"8" => "482",
					"9" => "486",
					"10" => "490",
					"11" => "",
					"12" => "485",
					"13" => "484",
					"14" => "",
					"15" => "",
					"16" => "30",
					"17" => "",
					"18" => "483"
				);
				
				/*$ini_chrono=array(
					"17" => array(
						"1" => 63713,
						"2" => 63030,
						"3" => 63714,
						"4" => 63403,
						"5" => 63715,
						"7" => 62953,
						"8" => 62954,
						"10" => 62955,
						"15" => 61162,
						"19" => 63016,
						"21" => 63106
					),
					"19" => array(
						"1" => 17904,
						"2" => 17834,
						"3" => 18099,
						"4" => 18059,
						"5" => 17895,
						"7" => 17673,
						"8" => 17674,
						"10" => 17675,
						"13" => 17103,
						"14" => 17778,
						"15" => 18004,
						"16" => 17558,
						"17" => 18019,
						"18" => 17875,
						"19" => 18062,
						"20" => 17854,
						"21" => 18063,
						"26" => 18125
					)
					
				);*/
				
				if(isset($caces[$d_dossier["dca_qualification"]])){
					if(empty($caces[$d_dossier["dca_qualification"]])){
						$erreur_txt="Impossible d'identifier la recommandation CACES!";
					}
				}else{
					$erreur_txt="Impossible d'identifier la recommandation CACES!";
				}
			
				// LA REF CACES DE LA SOC POUR LE CHRONO CACES
				if(empty($erreur_txt)){
					
					$sql="SELECT soc_caces FROM Societes WHERE soc_id=:acc_societe AND soc_caces>0;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":acc_societe",$acc_societe);
					$req->execute();
					$d_societe=$req->fetch();
					if(empty($d_societe)){
						$erreur_txt="Les paramètres de votre société ne permettent pas l'édition de CACES.";
					}
				}
				
				if(empty($erreur_txt)){
					
					$sql_up_cat="UPDATE Diplomes_Caces_Cat SET dcc_chrono=:dcc_chrono, dcc_numero=:dcc_numero
					WHERE dcc_diplome=:diplome AND dcc_passage=:passage AND dcc_categorie=:categorie;";
					$req_up_cat=$Conn->prepare($sql_up_cat);
					$req_up_cat->bindParam(":diplome",$dossier_id);
				
					// ON RECUPERE LES CATEGORIE VALIDE SANS CHRONO (inclut les passage précédent)
					
					$sql="SELECT dcc_categorie,dcc_date,dcc_passage,qca_libelle FROM Diplomes_Caces_Cat,Qualifications_Categories 
					WHERE dcc_categorie=qca_id AND dcc_diplome=:diplome AND dcc_valide AND dcc_chrono=0;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":diplome",$dossier_id);
					$req->execute();
					$d_categories=$req->fetchAll();
					if(!empty($d_categories)){
						foreach($d_categories as $cat){
							
							// on recupere le dernier chrono
							
							$dcc_chrono=0;
							$sql="SELECT MAX(dcc_chrono) FROM Diplomes_Caces_Cat,Diplomes_Caces WHERE dcc_diplome=dca_id AND dca_caces=:dca_caces
							AND dcc_categorie=:categorie;";
							$req=$Conn->prepare($sql);
							$req->bindParam(":dca_caces",$d_societe["soc_caces"]);
							$req->bindParam(":categorie",$cat["dcc_categorie"]);
							$req->execute();							
							$d_resultat=$req->fetch();
							if(!empty($d_resultat)){
								if(!empty($d_resultat[0])){
									$dcc_chrono=$d_resultat[0];
								}
							}
							/*if(empty($dcc_chrono)){				
								if(isset($ini_chrono[$acc_societe])){
									if(isset($ini_chrono[$acc_societe][$cat["dcc_categorie"]])){
										$dcc_chrono=$ini_chrono[$acc_societe][$cat["dcc_categorie"]];
									}
								}								
							}*/						
							$dcc_chrono++;

							// on genere un numero de CACES
							$dcc_numero="";
							if(!empty($cat["dcc_date"])){
								$DT_periode=date_create_from_format('Y-m-d',$cat["dcc_date"]);
								if(!is_bool($DT_periode)){
									$dcc_numero=$DT_periode->format("Y") . "." . $DT_periode->format("m") . "." . $caces[$d_dossier["dca_qualification"]] . "." . $cat["qca_libelle"] . "." . $dcc_chrono;
								}
							}
							
							if(!empty($dcc_numero)){
								
								$req_up_cat->bindParam(":dcc_chrono",$dcc_chrono);
								$req_up_cat->bindParam(":dcc_numero",$dcc_numero);
								$req_up_cat->bindParam(":passage",$cat["dcc_passage"]);
								$req_up_cat->bindParam(":categorie",$cat["dcc_categorie"]);
								try{
									$req_up_cat->execute();
								}Catch(Exception $e){
									$erreur_txt.=$e->getMessage();
									
								}							
							}else{
								$erreur_txt="Une ou plusieurs catégories n'ont pas pu être validées.";
							}
						}
						
					}
				}
			}
			// FIN TRAITEMENT CACES
			

			// ON MAJ LE STATUT DU DIPLOME
			if(empty($erreur_txt)){			
				
				$ast_diplome_statut=2;
				$ast_attest_ok=0;
				if(!empty($d_dossier["dca_date"])){
					$ast_diplome_statut=1;
					$ast_attest_ok=1;
				}
				
				$sql_up_sta="UPDATE Actions_Stagiaires SET ast_diplome_statut=:ast_diplome_statut,ast_attest_ok=:ast_attest_ok WHERE ast_stagiaire=:stagiaire AND ast_action=:action;";
				$req_up_sta=$ConnSoc->prepare($sql_up_sta);
				$req_up_sta->bindParam(":stagiaire",$d_dossier["dca_stagiaire"]);
				$req_up_sta->bindParam(":action",$d_dossier["dca_action_" . $passage]);
				$req_up_sta->bindParam(":ast_diplome_statut",$ast_diplome_statut);
				$req_up_sta->bindParam(":ast_attest_ok",$ast_attest_ok);
				try{
					$req_up_sta->execute();
				}Catch(Exception $e){
					$erreur_txt=$e->getMessage();
				}	
			}
			
			// attestation en fonction du diplome
			if(empty($erreur_txt)){	
			
				if(!empty($d_action_sta["ast_attestation"])){
					
					if($ast_attest_ok==1){
						// l'attestation est ok on delete les competences
						$sql_del_comp="DELETE FROM Stagiaires_Attestations_Competences WHERE sac_stagiaire=:stagiaire AND sac_action=:action;";
						$req_del_comp=$ConnSoc->prepare($sql_del_comp);
						$req_del_comp->bindParam(":stagiaire",$d_dossier["dca_stagiaire"]);
						$req_del_comp->bindParam(":action",$d_dossier["dca_action_" . $passage]);
						$req_del_comp->execute();
						
					}else{
						
						echo("caht");
						
						// l'attesation est en echec on add toutes les competences en echec
						$sql_add_comp="INSERT INTO Stagiaires_Attestations_Competences (sac_stagiaire,sac_action,sac_attest_competence,sac_acquise) VALUES (:sac_stagiaire,:sac_action,:sac_attest_competence,:sac_acquise);";
						$req_add_comp=$ConnSoc->prepare($sql_add_comp);
						$req_add_comp->bindParam(":sac_stagiaire",$d_dossier["dca_stagiaire"]);
						$req_add_comp->bindParam(":sac_action",$d_dossier["dca_action_" . $passage]);						
						$req_add_comp->bindValue(":sac_acquise",0);
						
						$sql_get_comp="SELECT aco_id FROM Attestations_competences WHERE aco_attestation=:attestation;";
						$req_get_comp=$Conn->prepare($sql_get_comp);
						$req_get_comp->bindParam(":attestation",$d_action_sta["ast_attestation"]);
						$req_get_comp->execute();
						$d_comp=$req_get_comp->fetchAll();
						if(!empty($d_comp)){
							foreach($d_comp as $comp){							
								$req_add_comp->bindParam(":sac_attest_competence",$comp["aco_id"]);
								$req_add_comp->execute();								
							}
						}
						
					}
				}
			}
			
			// ON CLOS LE PASSAGE 
			if(empty($erreur_txt)){	
				$sql_up_dossier="UPDATE Diplomes_Caces SET dca_clos_" . $passage . "=1 WHERE dca_id=:diplome;";
				$req_up_dossier=$Conn->prepare($sql_up_dossier);
				$req_up_dossier->bindParam(":diplome",$dossier_id);
				try{
					$req_up_dossier->execute();					
				}Catch(Exception $e){
					$erreur_txt=$e->getMessage();
				}	
			}

		}
		
	}else{
		$erreur_txt="Formulaire incomplet";
	}
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Echec de l'enregistrement",
			"type" => "danger",
			"message" => $erreur_txt
		);
	}
	if(!empty($erreur_txt)){
		if(!empty($dossier_id) AND !empty($passage)){
			header("location : dip_caces_voir.php?dossier=" . $dossier_id . "&passage=" . $passage);
		}else{
			header("location : accueil.php");
		}
	}else{
		header("location : dip_caces_voir.php?dossier=" . $dossier_id . "&passage=" . $passage);
	}
	die();

?>
	