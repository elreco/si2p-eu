<?php
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");

// PARAMETRE
$utilisateur=0;
if(isset($_GET["utilisateur"])){
	if(!empty($_GET["utilisateur"])){
		$utilisateur=intval($_GET["utilisateur"]);
	}
}
$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}

$erreur_txt="";
if($utilisateur==0 OR $exercice==0 OR !$_SESSION["acces"]["acc_droits"][39]){	
	echo("Accès refusé! Vous n'êtes pas autorisé à afficher cette page.");
	die();
}

$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}



$sql="SELECT uti_nom,uti_prenom,
urp_prime_produit,urp_prime_mois,urp_prime_trim,urp_prime_annee,urp_obj_prospect,urp_prime_prospect,urp_prct_interco,upr_rem_type,urp_prime_autre,urp_prct_no_obj
FROM Utilisateurs LEFT OUTER JOIN Utilisateurs_Rem_Param 
ON (Utilisateurs.uti_id=Utilisateurs_Rem_Param.urp_utilisateur AND urp_exercice=" . $exercice . ")
WHERE uti_id=" . $utilisateur;
if($_SESSION['acces']["acc_profil"]==10){
	$sql.=" AND NOT uti_profil=10";
	if($acc_utilisateur!=3){
		$sql.=" AND NOT uti_societe=4";
	}
}
$sql.=";";
$req=$Conn->query($sql);
$d_utilisateur=$req->fetch();
if(empty($d_utilisateur)){
	$erreur_txt="Impossible d'afficher la page!";
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			.objectif{
				color:#AA00AA;
			}
			.resultat{
				font-weight:bold;
			}
			.resultat a{
				color:#000;
			}
			.ligne-strip{
				background-color:#FFF;
			}
		</style>
	</head>

	<body class="sb-top sb-top-sm" >

		<form method="post" action="utilisateur_rem_param_enr.php" >
			<div>
				<input type="hidden" name="utilisateur" value="<?=$utilisateur?>" />
				<input type="hidden" name="exercice" value="<?=$exercice?>" />
			</div>
			<div id="main">
		<?php	include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">
			<?php		if(!empty($erreur_txt)){ ?>
							<p class="alert alert-danger text-center" >
								<?=$erreur_txt?>
							</p>
			<?php		}else{ ?>
							<h1><?=$d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"]?></h1>
							
							<div class="row">
								<div class="col-md-10 col-md-offset-1">
									<div class="admin-form theme-primary ">
									
										<div class="panel heading-border panel-primary">
											<div class="panel-body bg-light">											
												<div class="content-header">
													<h2>Paramètre de rémunération <b class="text-primary"><?=$exercice . "/" . intval($exercice+1)?></b></h2>
												</div>

												<div class="col-md-10 col-md-offset-1">

													<div class="row">
														<div class="col-md-6">	
															<label for="urp_prime_produit" >Prime produits :</label>
															<div class="field prepend-icon">
																<input type="text" name="urp_prime_produit" class="gui-input" id="urp_prime_produit" placeholder="Prime produits" value="<?=$d_utilisateur["urp_prime_produit"]?>" />
																<span class="field-icon">
																	<i class="fa fa-eur"></i>
																</span>
															</div>	
															<small>Montant divisé par le nombre de familles</small>
														</div>
														<div class="col-md-6">	
															<label for="urp_prime_mois" >Prime mensuelle :</label>
															<div class="field prepend-icon">
																<input type="text" name="urp_prime_mois" class="gui-input" id="urp_prime_mois" placeholder="Prime mensuelle" value="<?=$d_utilisateur["urp_prime_mois"]?>" />
																<span class="field-icon">
																	<i class="fa fa-eur"></i>
																</span>
															</div>															
														</div>	
													</div>
													<div class="row mt15">		
														<div class="col-md-6">	
															<label for="urp_prime_autre" >Prime de lancement :</label>
															<div class="field prepend-icon">
																<input type="text" name="urp_prime_autre" class="gui-input" id="urp_prime_autre" placeholder="Prime de lancement" value="<?=$d_utilisateur["urp_prime_autre"]?>" />
																<span class="field-icon">
																	<i class="fa fa-eur"></i>
																</span>
															</div>	
															<small>Pour les familles "autre INCENDIE" et "autre SANTE" uniquement</small>
														</div>		
														<div class="col-md-6">	
															<label for="urp_prct_no_obj" >Rémunération hors objectifs :</label>
															<div class="field prepend-icon">
																<input type="text" name="urp_prct_no_obj" class="gui-input" id="urp_prct_no_obj" placeholder="Rémunération hors objectifs" value="<?=$d_utilisateur["urp_prct_no_obj"]?>" />
																<span class="field-icon">
																	<i>%</i>
																</span>
															</div>	
															<small>% de rémunération des familles autres sans objectif</small>
														</div>	
													</div>
													
													<div class="row mt15">
														<div class="col-md-6">	
															<label for="urp_prime_trim" >Prime période :</label>
															<div class="field prepend-icon">
																<input type="text" name="urp_prime_trim" class="gui-input" id="urp_prime_trim" placeholder="Prime période" value="<?=$d_utilisateur["urp_prime_trim"]?>" />
																<span class="field-icon">
																	<i class="fa fa-eur"></i>
																</span>
															</div>	
														<small>Montant de la prime pour chaque période</small>															
														</div>
														<div class="col-md-6">	
															<label for="urp_prime_annee" >Prime annuelle :</label>
															<div class="field prepend-icon">
																<input type="text" name="urp_prime_annee" class="gui-input" id="urp_prime_annee" placeholder="Prime annuelle" value="<?=$d_utilisateur["urp_prime_annee"]?>" />
																<span class="field-icon">
																	<i class="fa fa-eur"></i>
																</span>
															</div>															
														</div>
													</div>
													<div class="row mt15">
														<div class="col-md-6">	
															<label for="urp_obj_prospect" >Objectif prospection :</label>
															<input type="text" name="urp_obj_prospect" class="gui-input" id="urp_obj_prospect" placeholder="Objectif prospection" value="<?=$d_utilisateur["urp_obj_prospect"]?>" />															
														</div>
														<div class="col-md-6">	
															<label for="urp_prime_produit" >Prime prospection :</label>
															<div class="field prepend-icon">
																<input type="text" name="urp_prime_prospect" class="gui-input" id="urp_prime_prospect" placeholder="Prime prospection" value="<?=$d_utilisateur["urp_prime_prospect"]?>" />
																<span class="field-icon">
																	<i class="fa fa-eur"></i>
																</span>
															</div>															
														</div>
													</div>
													
													<div class="row mt15">
														<div class="col-md-6">	
															<label for="urp_prct_interco" >Pourcentage interco :</label>
															<input type="text" name="urp_prct_interco" class="gui-input" id="urp_prct_interco" placeholder="% de rémunération interco" value="<?=$d_utilisateur["urp_prct_interco"]?>" />															
														</div>	
														<div class="col-md-6">	
															
															<label for="upr_rem_type" >Type de REM :</label>
															<span class="field select">
																<select name="upr_rem_type" id="upr_rem_type" required >
																	<option value="" >Type de rémuneration</option>
																	<option value="1" <?php if($d_utilisateur["upr_rem_type"]==1) echo("selected"); ?> >CA Commercial</option>
																	<option value="2" <?php if($d_utilisateur["upr_rem_type"]==2) echo("selected"); ?> >CA Agence</option>
																</select>
																<i class="arrow simple"></i>
															</span>														
														</div>															
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
				<?php	} ?>				
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="utilisateur_rem.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>" class="btn btn-default btn-sm" role="button" >
							<span class="fa fa-left"></span>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle text-center" >
		
					</div>
					<div class="col-xs-3 footer-right" >
			<?php		if(empty($erreur_txt)){ ?>
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i> Enregistrer
							</button>
			<?php		} ?>		
					</div>
				</div>
			</footer>
		</form>
<?php
		include "includes/footer_script.inc.php"; ?>	
	</body>
</html>
