<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_contact.php');
include('modeles/mod_parametre.php');

//////////////////// Requêtes à la base de données /////////////////////
// chercher le fournisseur
$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id = :fou_id");
$req->bindParam(':fou_id', $_GET['fournisseur']);
$req->execute();
$fournisseur = $req->fetch();
// chercher les societes
$req = $Conn->prepare("SELECT soc_id, soc_nom FROM societes WHERE soc_archive != 1");
$req->execute();
$societes = $req->fetchAll();

?>
<!DOCTYPE html>
<html>  
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - <?= $fournisseur['fou_nom'] ?></title>
	<meta name="keywords" content=""/>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
<!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
	<form method="post" action="fournisseur_region_enr.php">

		<!-- Start: Main -->
		<div id="main">
			<?php
			include "includes/header_def.inc.php"; 
			?>

			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">


				<section id="content">

					<!-- Begin .page-heading -->

					<!-- MESSAGES ERREUR -->

					<div class="row">

						<div class="col-md-12">
							<div class="panel">
								<div class="panel-heading  panel-head-sm">
									<i class="fa fa-barcode"></i> Ajouter une région pour le fournisseur <strong><?= $fournisseur['fou_nom'] ?></strong>
								</div>




								<div class="panel-body">
									<input type="hidden" name="fournisseur" value="<?= $_GET['fournisseur'] ?>">
									<input type="hidden" name="ajout" value="<?= $_GET['ajout'] ?>">
									<div class="row">
										<div class="col-md-6">

											<select id="societe" name="societe" class="select2 select2-societe" >
												<option value="0">Sélectionner une société...</option>
												<?php foreach($societes as $s){ ?>
												<option value="<?= $s['soc_id'] ?>"><?= $s['soc_nom'] ?></option>
												<?php } ?>
											</select>

										</div>
										<div class="col-md-6">

											<select id="agence" name="agence" class="select2 select2-societe-agence">

											</select>

										</div>
									</div>

								</div>





							</div>
						</div>
					</div>

				</section>
			</div>

			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="fournisseur_voir.php?fournisseur=<?= $_GET['fournisseur'] ?>&tab=3" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right">

						<button type="submit" id="submit" name="submit" class="btn btn-success btn-sm">
							<i class='fa fa-floppy-o'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>

		<?php
		include "includes/footer_script.inc.php"; ?>     
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="assets/js/custom.js"></script>

		<script src="assets/js/responsive-tabs.js"></script>
		<script type="text/javascript">
		///////////////// FONCTIONS //////////////////

		</script>
</form>
</body>
</html>
