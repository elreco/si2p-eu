 <?php
 
	include "includes/controle_acces.inc.php";
	
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");
	
	// EDITION D'UN DIPLOME CACES
	
	$erreur_txt="";
	
	$action_id=0;
	if(isset($_GET["action"])){
		if(!empty($_GET["action"])){
			$action_id=intval($_GET["action"]);
		}	
	}
	
	$stagiaire_id=0;
	if(isset($_GET["stagiaire"])){
		if(!empty($_GET["stagiaire"])){
			$stagiaire_id=intval($_GET["stagiaire"]);
		}	
	}
	
	$qualification=0;
	if(isset($_GET["qualification"])){
		if(!empty($_GET["qualification"])){
			$qualification=intval($_GET["qualification"]);
		}	
	}
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}
	if($acc_societe!=$conn_soc_id){
		$acc_societe=$conn_soc_id;
		
	}

	$dates_test=array();
	$d_qualif_cat=array();
	$testeurs=array();
	
	if($action_id==0 OR $stagiaire_id==0){
		$erreur_txt="Impossible d'afficher la page!";
	}
	
	if(empty($erreur_txt)){
		// en cas de recup dossier dca_id>0 mais retour doit se faire sur l'action
		$maj_dossier=false;
		// SUR LA SOCIETE
		$sql="SELECT soc_caces FROM Societes WHERE soc_id=:acc_societe AND soc_caces>0;";
		$req=$Conn->prepare($sql);
		$req->bindParam("acc_societe",$acc_societe);
		$req->execute();
		$d_societe=$req->fetch();
		if(empty($d_societe)){
			$erreur_txt="Le paramétrage actuel ne permet de réaliser l'opération demandée.";
		}
	}
	
	if(empty($erreur_txt)){	
		// SUR L'INSCRIPTION STAGIAIRE
		
		$sql="SELECT acl_pro_reference,act_date_deb FROM Actions_Stagiaires,Actions_Clients,Actions WHERE ast_action_client=acl_id AND acl_action=act_id AND ast_action=:action AND ast_stagiaire=:stagiaire;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("action",$action_id);
		$req->bindParam("stagiaire",$stagiaire_id);
		$req->execute();
		$d_action_client=$req->fetch();
		if(empty($d_action_client)){
			$erreur_txt="Impossible de charger les données relatives au code produit.";
		}else{
			if(substr($d_action_client["acl_pro_reference"],0,2)=="A-"){
				$dca_diplome=1;
			}else{
				$dca_diplome=2;
			};
		
			$DT_ouverture=date_create_from_format('Y-m-d',$d_action_client["act_date_deb"]);
			$DT_ouverture->sub(new DateInterval('P12M'));
			$date_fermeture=$DT_ouverture->format("Y-m-d");
			if($date_fermeture<'2020-01-01'){
				$date_fermeture="2020-01-01";
			}
			
		}
	}
	
	if(empty($erreur_txt)){	
		
		// RECHERCHE DU DIPLOME CACES
		
		/*$time_ref=mktime(date("H"),date("i"),date("s") , date("m")-6,date("d"),date("Y"));
		$date_fermeture=date("Y-m-d",$time_ref);*/
		
		$sql="SELECT dca_id,dca_qualification,dca_stagiaire,dca_diplome,dca_date,dca_date_validite,dca_sta_nom,dca_sta_prenom,DATE_FORMAT(dca_sta_naissance,'%d/%m/%Y') AS dca_sta_naissance
		,dca_action_1,dca_action_soc_1,dca_date_1,dca_th_1
		,dca_action_2,dca_action_soc_2,dca_date_2,dca_th_2
		,dca_action_3,dca_action_soc_3,dca_date_3,dca_th_3
		,dca_action_4,dca_action_soc_4,dca_date_4,dca_th_4
		,dca_action_5,dca_action_soc_5,dca_date_5,dca_th_5
		,dca_action_6,dca_action_soc_6,dca_date_6,dca_th_6
		,dca_date_th,dca_passage_th,dca_date_pra,dca_passage_pra,dca_aipr,DATE_FORMAT(dca_aipr_date,'%d/%m/%Y') AS dca_aipr_date_fr
		FROM Diplomes_Caces WHERE (
		(
			(dca_action_1=:action AND dca_action_soc_1=:societe) OR (dca_action_2=:action AND dca_action_soc_2=:societe) OR (dca_action_3=:action AND dca_action_soc_3=:societe) OR 
			(dca_action_4=:action AND dca_action_soc_4=:societe) OR (dca_action_5=:action AND dca_action_soc_5=:societe) OR (dca_action_6=:action AND dca_action_soc_6=:societe) 
		) OR 
			(dca_diplome=2 AND (dca_date_pra>=:date_ref OR dca_date_th>=:date_ref) )
		) 
		AND dca_stagiaire=:stagiaire AND dca_diplome=:diplome AND dca_qualification=:qualification";
		$req=$Conn->prepare($sql);
		$req->bindParam(":action",$action_id);
		$req->bindParam(":societe",$acc_societe);
		$req->bindParam(":stagiaire",$stagiaire_id);
		$req->bindParam(":date_ref",$date_fermeture);
		$req->bindParam(":diplome",$dca_diplome);
		$req->bindParam(":qualification",$qualification);
		$req->execute();
		$d_dossier=$req->fetch();
		if(!empty($d_dossier)){
			
			// DOSSIER EXISTANT

			$dernier_passage=0;
			$passage=0;
			for($bcl=1;$bcl<=6;$bcl++){
				if(!empty($d_dossier["dca_action_" . $bcl])){
					$dernier_passage=$bcl;
				}
				if($d_dossier["dca_action_" . $bcl]==$action_id AND $d_dossier["dca_action_soc_" . $bcl]==$acc_societe){
					$maj_dossier=true;
					$passage=$bcl;
					break;
				}
			}
			
			if(empty($passage) AND $dernier_passage<=5){
				$passage=$dernier_passage+1;
			}
			
			$qualification=$d_dossier["dca_qualification"];
			
		}else{
			
			// NOUVEAU DOSSIER
			
			$passage=1;
			
			$d_dossier=array(
				"dca_id" => 0,
				"dca_diplome" => $dca_diplome,
				"dca_stagiaire" => $stagiaire_id,
				"dca_passage_th" => 0,
				"dca_date_1" => "",
				"dca_th_1" => 0,
				"dca_aipr" => 0,
				"dca_aipr_date_fr" => null
			);
				
			$sql="SELECT sta_nom,sta_prenom,DATE_FORMAT(sta_naissance,'%d/%m/%Y') AS sta_naissance FROM Stagiaires WHERE sta_id=" . $stagiaire_id . ";";
			$req=$Conn->query($sql);
			$d_stagiaire=$req->fetch();
			if(!empty($d_stagiaire)){
				$d_dossier["dca_sta_nom"]=$d_stagiaire["sta_nom"];
				$d_dossier["dca_sta_prenom"]=$d_stagiaire["sta_prenom"];
				$d_dossier["dca_sta_naissance"]=$d_stagiaire["sta_naissance"];
			}else{
				$erreur_txt="Impossible de charger les données du stagiaire";
			}
			
		}
		
		if(empty($erreur_txt)){

			// SUR LA QUALIFICATION
			$nbOpt=0;
			$sql="SELECT qua_libelle,qua_opt_1,qua_opt_2,qua_opt_3 FROM Qualifications WHERE qua_id=" . $qualification . ";";
			$req=$Conn->query($sql);
			$d_qualification=$req->fetch();
			if(!empty($d_qualification)){
				if(!empty($d_qualification["qua_opt_3"])){
					$nbOpt=3;
				}elseif(!empty($d_qualification["qua_opt_2"])){
					$nbOpt=2;
				}elseif(!empty($d_qualification["qua_opt_1"])){
					$nbOpt=1;
				}
			}
		
			
			
			// LES CATEGORIES DE QUALIFICATION ET LES RESULTATS ASSOCIES POUR LE CACES EN COURS
			
			$d_qualif_cat=array();
			$sql="SELECT qca_id,qca_libelle,qca_competence,qca_ut
			,qca_opt_1,qca_ut_opt_1,qca_comp_opt_1
			,qca_opt_2,qca_ut_opt_2,qca_comp_opt_2
			,qca_opt_3,qca_ut_opt_3,qca_comp_opt_3
			,dcc_categorie,dcc_date,dcc_testeur,dcc_testeur,dcc_valide,dcc_opt_1,dcc_opt_1_valide,dcc_opt_2,dcc_opt_2_valide
			FROM Qualifications_Categories LEFT JOIN Diplomes_Caces_Cat 
			ON (Qualifications_Categories.qca_id=Diplomes_Caces_Cat.dcc_categorie AND dcc_diplome=" . $d_dossier["dca_id"];
				$sql.=" AND (dcc_passage=" . $passage . " OR dcc_valide)"; 
			$sql.=")";
			$sql.=" WHERE qca_qualification=" . $qualification . " AND NOT qca_archive";
			$sql.=" AND (dcc_passage=" . $passage . " OR ISNULL(dcc_categorie) )";
			$sql.=" ORDER BY qca_libelle;";
			
			$req=$Conn->query($sql);
			$d_dossier_cat=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($d_dossier_cat)){
				// on isole les parametre liés a la categorie de qualification
				foreach($d_dossier_cat as $cle_cat => $dqc){
					
					
					$d_qualif_cat[$dqc["qca_id"]]=array(
						"qca_competence" => $dqc["qca_competence"],
						"qca_ut" => $dqc["qca_ut"],
						"qca_opt_1" => $dqc["qca_opt_1"],
						"qca_ut_opt_1" => $dqc["qca_ut_opt_1"],
						"qca_comp_opt_1" => $dqc["qca_comp_opt_1"],
						"qca_opt_2" => $dqc["qca_opt_2"],
						"qca_ut_opt_2" => $dqc["qca_ut_opt_2"],
						"qca_comp_opt_2" => $dqc["qca_comp_opt_2"],
						"qca_opt_3" => $dqc["qca_opt_3"],
						"qca_ut_opt_3" => $dqc["qca_ut_opt_3"],
						"qca_comp_opt_3" => $dqc["qca_comp_opt_3"]
					);	
					
					// calcul des ut necessaire
					$ut_cat=0;
					if(!empty($dqc["dcc_categorie"])){
						$ut_cat=$dqc["qca_ut"];
						if(!empty($dqc["dcc_opt_1"])){
							$ut_cat=$ut_cat+$dqc["qca_ut_opt_1"];						
						}
						if(!empty($dqc["dcc_opt_2"])){
							$ut_cat=$ut_cat+$dqc["qca_ut_opt_2"];						
						}
						if(!empty($dqc["dcc_opt_3"])){
							$ut_cat=$ut_cat+$dqc["qca_ut_opt_3"];						
						}
					}
					$d_dossier_cat[$cle_cat]["ut_cat"]=$ut_cat;
					
					
				}
			}
			// ON RECHERCHE LES CATEGORIES VALIDEES SUR LES ANCIENS PASSAGE POUR LES UPP DU FORMULAIRE
			/*$sql="SELECT dcc_categorie FROM Diplomes_Caces_Cat WHERE dcc_diplome=" . $d_dossier["dca_id"] . " AND NOT dcc_passage=" . $passage . " 
			AND dcc_valide ORDER BY dcc_categorie;";
			$req=$Conn->query($sql);
			$d_dossier_cat_valide=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($d_dossier_cat_valide)){	
				foreach($d_dossier_cat_valide as $cat_valide){
					unset($d_qualif_cat[$cat_valide["dcc_categorie"]]);
				}
			}*/
			/*var_dump($sql);
			echo("<pre>");
				print_r($d_dossier_cat_valide);
			echo("</pre>");
			die();*/
			
			// DATE DE TEST ET TESTEUR
			
			$dates_test=array();
			$testeurs=array();

			$sql="SELECT pda_date,pda_intervenant,DATE_FORMAT(pda_date,'%d/%m/%Y') AS date_aff
			,int_id,int_label_1,int_type,int_ref_1,int_ref_2
			FROM Plannings_Dates,Intervenants WHERE pda_intervenant=int_id AND pda_type=1 AND pda_ref_1=" . $action_id . " AND NOT pda_archive";
			if($dca_diplome==2){
				$sql.=" AND pda_categorie=2";
			}
			$sql.=" ORDER BY pda_date,int_label_1;";
			$req=$ConnSoc->query($sql);
			$d_resultats=$req->fetchAll();
			if(!empty($d_resultats)){
				foreach($d_resultats as $r){
					
					if(!isset($dates_test[$r["pda_date"]])){
						$dates_test[$r["pda_date"]]=array(
							"date_aff" => $r["date_aff"],
							"testeurs" => array()
						);
					}
					if(!isset($dates_test[$r["pda_date"]]["testeurs"][$r["int_id"]])){
						$dates_test[$r["pda_date"]]["testeurs"][$r["int_id"]]=array(
							"int_label" => $r["int_label_1"],
							"ut" => 0
						);
					}
					
					if(!isset($testeurs[$r["int_id"]])){
						$testeurs[$r["int_id"]]=array(
							"int_label" => $r["int_label_1"],
							"int_type" => $r["int_type"],
							"int_ref_1" => $r["int_ref_1"],
							"int_ref_2" => $r["int_ref_2"],
							"dates" => array()
						);
					}
					if(!isset($testeurs[$r["int_id"]]["dates"][$r["pda_date"]])){
						$testeurs[$r["int_id"]]["dates"][$r["pda_date"]]=$r["date_aff"];
					}
					
				}
			}
			
			// UT DISPO PAR TESTEUR
			
			$sql="SELECT dcc_categorie,dcc_opt_1,dcc_opt_2,dcc_opt_3 FROM Diplomes_Caces_Cat,Diplomes_Caces WHERE dcc_diplome=dca_id 
			AND (dca_action_soc_1=:societe OR dca_action_soc_2=:societe OR dca_action_soc_3=:societe OR dca_action_soc_4=:societe OR dca_action_soc_5=:societe OR dca_action_soc_6=:societe) 
			AND NOT dcc_diplome=:diplome
			AND dcc_testeur=:testeur AND dcc_date=:date;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":diplome",$d_dossier["dca_id"]);
			$req->bindParam(":societe",$acc_societe);
					
			foreach($dates_test as $date_req => $date){
				
				foreach($date["testeurs"] as $testeur_id => $testeur){
					
					$ut=0;
					$req->bindParam(":testeur",$testeur_id);
					$req->bindParam(":date",$date_req);
					$req->execute();
					$result=$req->fetchAll();
					if(!empty($result)){
						foreach($result as $r){
							if(!empty($d_qualif_cat[$r["dcc_categorie"]])){
								
								$ut=$ut+$d_qualif_cat[$r["dcc_categorie"]]["qca_ut"];
							
								if(!empty($r["dcc_opt_1"])){
									$ut=$ut+$d_qualif_cat[$r["dcc_categorie"]]["qca_ut_opt_1"];
								}
								if(!empty($r["dcc_opt_2"])){
									$ut=$ut+$d_qualif_cat[$r["dcc_categorie"]]["qca_ut_opt_2"];
								}
								if(!empty($r["dcc_opt_3"])){
									$ut=$ut+$d_qualif_cat[$r["dcc_categorie"]]["qca_ut_opt_3"];
								}
							}
							
						}
						
						$dates_test[$date_req]["testeurs"][$testeur_id]["ut"]=$ut;
						
					}
				}
			}
		}
	}
	
	if(!empty($erreur_txt)){
		$d_dossier=array(
			"dca_id" => 0,
			"dca_diplome" => 0
		);
	} ?>
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<title>SI2P - Orion - Recherche client</title>
			<meta name="keywords" content=""/>
			<meta name="description" content="">
			<meta name="author" content="">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<!-- CSS THEME -->
			<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
			<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />

			<!-- CSS PLUGINS -->
			<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
			<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
			
			<!-- CSS Si2P -->
			<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
			
			<!-- Favicon -->
			<link rel="shortcut icon" href="assets/img/favicon.png">
		   
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
			<![endif]-->
		</head>
		<body class="sb-top sb-top-sm ">
			<form method="post" action="dip_caces_enr.php" id="form" enctype="multipart/form-data" >
				<div id="main">
		<?php		include "includes/header_def.inc.php"; ?>
					<section id="content_wrapper">
						<section id="content" class="animated" >
					<?php	if(empty($erreur_txt)){	?>	 
					
								<div>
									<input type="hidden" name="action" value="<?=$action_id?>" />
									<input type="hidden" name="stagiaire" value="<?=$stagiaire_id?>" />
									<input type="hidden" name="dossier" value="<?=$d_dossier["dca_id"]?>" />
									<input type="hidden" name="qualification" value="<?=$qualification?>" />
									<input type="hidden" name="dca_diplome" value="<?=$dca_diplome?>" />	
									<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />									
								</div>
				
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
										
											<div class="content-header">
												<h2>
											<?php	if($dca_diplome==1){
														echo("Attestation de compétence");
													}else{
														echo("Certificat CACES®");
													} ?>
													<b class="text-primary"><?=$d_qualification["qua_libelle"]?></b>
												</h2>
											</div>
											
											<div class="row" >
												<div class="col-md-3" >
													<div class="row" >
														<div class="col-md-12 text-center" >
															<div class="image-preview image-350x140" >
													<?php		if(file_exists("documents/Stagiaires/sta_phid_". $stagiaire_id . ".jpg")){ ?>
																	<img src="documents/Stagiaires/sta_phid_<?=$stagiaire_id?>.jpg" alt="" id="sta_photo_preview" />
												<?php			}else{ ?>
																	<img src="assets/img/400x140.jpg" alt="" id="sta_photo_preview" />
												<?php			} ?>
															</div>
														</div>
													</div>
													<div class="row" >
														<div class="col-md-8" >
															<span class="prepend-icon file">
																<span class="button btn-primary">Choisir</span>
																<input type="file" class="gui-file" name="sta_photo" id="sta_photo" />
																<input type="text" class="gui-input" id="sta_photo_champ" placeholder="Photo" >
																<span class="field-icon">
																	<i class="fa fa-upload"></i>
																</span>
															</span>
														</div>
														<div class="col-md-4 text-center" >
															<button type="button" class="btn btn-sm btn-danger img-preview-supp" data-id_img="sta_photo" data-url_img="Stagiaires/sta_phid_<?=$stagiaire_id?>.jpg" >
																<i class="fa fa-times" ></i>
															</button>
														</div>
													</div>
												</div>

												<div class="col-md-3" >
													<label for="sta_nom" >Nom :</label>
													<input type="text" name="dca_sta_nom" class="gui-input nom" id="sta_nom" placeholder="Nom" required value="<?=$d_dossier["dca_sta_nom"]?>" />
												</div>
												<div class="col-md-3" >
													<label for="sta_prenom" >Prénom :</label>
													<input type="text" name="dca_sta_prenom" class="gui-input prenom" id="sta_prenom" placeholder="Prénom" required value="<?=$d_dossier["dca_sta_prenom"]?>" />
												</div>
												<div class="col-md-3" >
													<label for="sta_naissance" >Date de naissance :</label>
													<input type="text" name="dca_sta_naissance" class="gui-input datepicker" id="sta_naissance" placeholder="Date de naissance" required value="<?=$d_dossier["dca_sta_naissance"]?>" />
												</div>
											</div>

									<?php	if($d_dossier["dca_passage_th"]==0 OR $d_dossier["dca_passage_th"]==$passage){ ?>	
												
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Théorie</span>
														</div>
													</div>
												</div>
											
												<div class="row" >
													<div class="col-md-4" >
													
														<label for="date_th" >Date du test théorique</label>
														<span class="field select">
													
															<select id="date_th" name="dca_date" <?php if(!empty($d_dossier["dca_th_" . $passage])) echo("required"); ?> >
																<option value="">Date du test...</option>
								<?php							foreach($dates_test as $date_req => $date){
																	if($d_dossier["dca_date_" . $passage]==$date_req){
																		echo("<option value='" . $date_req . "' selected >" . $date["date_aff"] . "</option>");	
																	}else{
																		echo("<option value='" . $date_req . "' >" . $date["date_aff"] . "</option>");	
																	}
																} ?>	
															</select>
															<i class="arrow simple"></i>
														</span>														
													</div>
													<div class="col-md-4 text-center" >
														<div class="option-group field mt25">
															<label class="option option-dark">
																<input type="checkbox" name="th_valide" id="th_valide" value="on" <?php if($d_dossier["dca_th_" . $passage]==1) echo("checked"); ?> />
																<span class="checkbox"></span>Validé
															</label>
														</div>
													</div>
													<div class="col-md-4 text-center" >
														<div class="option-group field mt25">
															<label class="option option-dark">
																<input type="checkbox" name="th_echec" id="th_echec" value="on" <?php if($d_dossier["dca_th_" . $passage]==2) echo("checked"); ?> />
																<span class="checkbox"></span>Echec
															</label>
														</div>
													</div>
													
												</div>
									<?php	} ?>
									
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span>AIPR</span>
													</div>
												</div>
											</div>
										
											<div class="row" >										
												<div class="col-md-4 text-center" >
													<div class="radio-custom mb5">
														<input id="aipr_oui" name="dca_aipr" type="radio" value="1" <?php if($d_dossier["dca_aipr"]) echo("checked"); ?> >
														<label for="aipr_oui">OUI</label>
													</div>
												</div>
												<div class="col-md-4 text-center" >
													<div class="radio-custom mb5">
														<input id="aipr_non" name="dca_aipr" type="radio" value="0" <?php if(!$d_dossier["dca_aipr"]) echo("checked"); ?>>
														<label for="aipr_non">NON</label>
													</div>
												</div>
												<div class="col-md-4" >
													<label for="aipr_date" >Date de réussite du QCM</label>
													<input type="text" name="dca_aipr_date" class="gui-input datepicker" id="aipr_date" placeholder="Date de réussite du QCM" value="<?=$d_dossier["dca_aipr_date_fr"]?>" />
												</div>
											</div>
									
								<?php		if(!empty($d_dossier_cat)){ ?>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Pratique</span>
														</div>
													</div>
												</div>
												
												<table class="table" >
													<tr>
														<th class='text-center' >Catégorie</th>
														<th class='text-center' >Demandé</th>
												<?php	for($bcl=1;$bcl<=$nbOpt;$bcl++){?>
															<th><?=$d_qualification["qua_opt_" . $bcl]?></th>
												<?php	} ?>
														<th class='text-center' >UT</th>
														<th class='text-center' >Date</th>
														<th class='text-center' >Testeur</th>
														<th class='text-center' >Cat. validée</th>
												<?php	for($bcl=1;$bcl<=$nbOpt;$bcl++){ ?>
															<th class='text-center' >Opt. <?=$d_qualification["qua_opt_" . $bcl]?> validée</th>
												<?php	} ?>
													</tr>
										<?php		foreach($d_dossier_cat as $cat){ 
														$att_checked="";
														$att_disabled="disabled";
														if(!empty($cat["dcc_categorie"])){
															$att_checked="checked";
															$att_disabled="";
														}
														
														?>
														<tr>
															
															<td>
															<?=$cat["qca_libelle"]?></td>
															<td class='text-center' >
																<div class="option-group field">
																	<label class="option option-dark">
																		<input type="checkbox" class="cat-check" id="cat_<?=$cat["qca_id"]?>" data-categorie="<?=$cat["qca_id"]?>" name="cat_<?=$cat["qca_id"]?>" value="on" <?=$att_checked?> >
																		<span class="checkbox"></span>
																	</label>
																</div>
															</td>
										<?php				for($bcl=1;$bcl<=$nbOpt;$bcl++){ ?>
																<td class='text-center' >
										<?php						if(!empty($cat["qca_opt_" . $bcl ]) AND !empty($cat["qca_ut_opt_" . $bcl])){ ?>
																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" class="cat-<?=$cat["qca_id"]?> cat-opt cat-opt-<?=$cat["qca_id"]?>" data-categorie="<?=$cat["qca_id"]?>" data-option="<?=$bcl?>" id="opt_<?=$cat["qca_id"]?>_<?=$bcl?>" name="opt_<?=$cat["qca_id"]?>_<?=$bcl?>" value="on" <?=$att_disabled?> <?php if(!empty($cat["dcc_opt_" . $bcl])) echo("checked"); ?> >
																				<span class="checkbox"></span>
																			</label>
																		</div>
										<?php						}else{ 
																		echo("&nbsp;");
																	} ?>
																</td>
										<?php				} ?>
															<td>
																<input type="text" readonly class="gui-input cat-<?=$cat["qca_id"]?>" id="ut_<?=$cat["qca_id"]?>" value="<?=$cat["ut_cat"]?>" size="4" <?=$att_disabled?> />
															</td>
															<td>
																<span class="field select">
																	<select class="cat-<?=$cat["qca_id"]?> date-test" id="date_<?=$cat["qca_id"]?>" name="date_<?=$cat["qca_id"]?>" <?=$att_disabled?> data-categorie="<?=$cat["qca_id"]?>" <?php if(!empty($cat["dcc_valide"])) echo("required"); ?> >
																		<option value="">Date du test...</option>
										<?php							foreach($dates_test as $date_req => $date){
																			if($cat["dcc_date"]==$date_req){
																				echo("<option value='" . $date_req . "' selected >" . $date["date_aff"] . "</option>");	
																			}else{
																				echo("<option value='" . $date_req . "' >" . $date["date_aff"] . "</option>");	
																			}
																		} ?>	
																	</select>
																	<i class="arrow simple"></i>
																</span>
															</td>
															<td>
																<span class="field select">
																	<select class="cat-<?=$cat["qca_id"]?> testeur" id="testeur_<?=$cat["qca_id"]?>" data-categorie="<?=$cat["qca_id"]?>" name="testeur_<?=$cat["qca_id"]?>" <?=$att_disabled?> <?php if(!empty($cat["dcc_valide"])) echo("required"); ?> >
																		<option value="">Testeur ...</option>
															<?php		if(!empty($dates_test[$cat["dcc_date"]]["testeurs"])){
																			foreach($dates_test[$cat["dcc_date"]]["testeurs"] as $int_id => $int_info){
																				if($cat["dcc_testeur"]==$int_id){
																					echo("<option value='" . $int_id . "' selected >" . $int_info["int_label"] . "</option>");	
																				}else{
																					echo("<option value='" . $int_id . "' >" . $int_info["int_label"] . "</option>");	
																				}
																			}
																
																		} ?>
																	</select>
																	<i class="arrow simple"></i>
																</span>
															</td>
															<td class='text-center' >
																<div class="option-group field">
																	<label class="option option-dark">
																		<input type="checkbox" class="cat-<?=$cat["qca_id"]?> cat-valide" data-cat="<?=$cat["qca_id"]?>" name="cat_valide_<?=$cat["qca_id"]?>" value="on" <?=$att_disabled?> <?php if(!empty($cat["dcc_valide"])) echo("checked"); ?> >
																		<span class="checkbox"></span>
																	</label>
																</div>
															</td>
										<?php				for($bcl=1;$bcl<=$nbOpt;$bcl++){ ?>
																<td class='text-center' >
									<?php							if(!empty($cat["qca_opt_" . $bcl]) AND !empty($cat["qca_ut_opt_" . $bcl])){ ?>
																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" class="cat-<?=$cat["qca_id"]?>" name="opt_<?=$cat["qca_id"]?>_<?=$bcl?>_valide" value="on" <?=$att_disabled?> <?php if(!empty($cat["dcc_opt_" . $bcl . "_valide"])) echo("checked"); ?> >
																				<span class="checkbox"></span>
																			</label>
																		</div>
									<?php							}else{ 
																		echo("&nbsp;");
																	}  ?>
																</td>
										<?php				} ?>
														</tr>
										<?php		} ?>
												</table>
									<?php	}	?>	
										</div>
									</div>
								</div>
					<?php	}else{ ?>
								<p class="alert alert-danger" >
									<?=$erreur_txt?>
								</p>
					<?php	} ?>	
						</section>						
					</section>
				</div>
				<footer id="content-footer" class="affix">
					<div class="row">
						<div class="col-xs-3 footer-left">
					<?php	if($maj_dossier){ ?>
								<a href="dip_caces_voir.php?dossier=<?=$d_dossier["dca_id"]?>&passage=<?=$passage?>" class="btn btn-default btn-sm" >
									Retour
								</a>
					<?php	}elseif(isset($_SESSION["retourCaces"])){ ?>
								<a href="<?=$_SESSION["retourCaces"]?>" class="btn btn-default btn-sm" >
									Retour
								</a>
					<?php	}else{ ?>
								<a href="action_voir_sta.php?action=<?=$action_id?>&societ=<?=$Conn_soc_id?>" class="btn btn-default btn-sm" >
									Retour
								</a>
					<?php	} ?>
						</div>
						<div class="col-xs-6 footer-middle text-center"></div>
						<div class="col-xs-3 footer-right">
				<?php		if(empty($erreur_txt)){ ?>
								<button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Enregistrer le diplôme" id="sub_form" >
									<i class="fa fa-save" ></i> Enregistrer
								</button>
				<?php		} ?>
						</div>
					</div>
				</footer>
			</form>
			
			
	<?php	include "includes/footer_script.inc.php"; ?>
				
			<script src="/vendor/plugins/select2/js/select2.min.js"></script>
			<script src="/vendor/plugins/mask/jquery.mask.js"></script>
			<script src="/assets/js/custom.js"></script>
			<script type="text/javascript" >
			
				var date_test=JSON.parse('<?=json_encode($dates_test)?>');
				var qualif_cat=JSON.parse('<?=json_encode($d_qualif_cat)?>');
				var testeurs=JSON.parse('<?=json_encode($testeurs)?>');
				var ut_sta=new Array();
				var dca_diplome=parseInt("<?=$d_dossier["dca_diplome"]?>");

				jQuery(document).ready(function(){

					/*$('input[name=dca_diplome]').change(function(){
						dca_diplome=$('input[name=dca_diplome]:checked').val();
						if(dca_diplome==2){
							check_ut_stagiaire(null,4,0);
						}
						
					});*/
					
					// resultat theorique
					
					$("#th_valide").click(function(){
						if($(this).is(":checked")){						
							$("#th_echec").prop("checked",false);
							$("#date_th").prop("required",true);
						}else if($("#th_echec").is(":checked")){
							$("#date_th").prop("required",true);
						}else{
							$("#date_th").prop("required",false);
						}
					});
					
					$("#th_echec").click(function(){
						if($(this).is(":checked")){
							$("#th_valide").prop("checked",false);
							$("#date_th").prop("required",true);
						}else if($("#th_valide").is(":checked")){
							$("#date_th").prop("required",true);
						}else{
							$("#date_th").prop("required",false);
						}
					});

				
					// selection d'une categorie
					$(".cat-check").click(function(){
						cat=$(this).data("categorie");
						if($(this).is(":checked")){
							$(".cat-" + cat).prop("disabled",false);						
							calcul_ut_cat(cat,1,0);			
						}else{
							$(".cat-opt-" + cat).prop("checked",false);
							$("#date_" + cat).val("");
							$("#date_" + cat).prop("required",false);							
							$("#testeur_" + cat).val("");
							$("#testeur_" + cat).prop("required",false);
							$(".cat-" + cat).prop("checked",false);
							$(".cat-" + cat).prop("disabled",true);
							
						}
					});

					// Selection d'une option
					$(".cat-opt").click(function(){						
						cat=$(this).data("categorie");
						option=$(this).data("option");
						calcul_ut_cat(cat,2,option);
					});
										
					// changement de la date du test
					$(".date-test").change(function(){						
						cat=$(this).data("categorie");
						testeur=$("#testeur_" + cat).val();
						option_def="";
						
						$("#testeur_" + cat + " option[value!='0'][value!='']").remove();
						
						// on met a jour la liste des testeurs
						$.each(date_test[$(this).val()]["testeurs"],function(i,val){
							
							$("#testeur_" + cat).append(new Option(val.int_label,i));
							if(i==testeur){
								option_def=testeur;
							}
						});
						$("#testeur_" + cat).val(option_def);
						if(dca_diplome==2){	
							// on verifie les ut du stagiaire
							 check_ut_stagiaire(cat,3,0);
						}
						
					});
			
				
					// changement de testeur				
					$(".testeur").change(function(){
						int_type=testeurs[$(this).val()]["int_type"];
						if($(this).val()!="" && dca_diplome==2 && int_type==1){
							cat_select=$(this).data("categorie");
							check_ut_testeur(cat_select,$(this).val());
						}
						
					});
				
					// validation d'une categorie
					$(".cat-valide").click(function(){
						cat=$(this).data("cat");
						if($(this).is(":checked")){
							$("#date_" + cat).prop("required",true);
							$("#testeur_" + cat).prop("required",true);
						}else{
							$("#date_" + cat).prop("required",false);
							$("#testeur_" + cat).prop("required",false);
						}
					});
					
				});
				
				// calcul des UT d'une categorie
				function calcul_ut_cat(cat_select,origine,option){
					console.log("calcul_ut_cat(" + cat_select + "," + origine + "," + option + ")");					
					ut=0;
					if($("#cat_" + cat_select).is(":checked")){
						ut=1*qualif_cat[cat_select]["qca_ut"];
					}
					$(".cat-opt-" + cat_select).each(function(i,val){
						if($(this).is(":checked")){
							ut=ut + 1*qualif_cat[cat_select]["qca_ut_opt_1"];
						}
					
					});
					$("#ut_" + cat_select).val(ut);
					
					if($("#date_" + cat_select).val()!="" && dca_diplome==2){
						check_ut_stagiaire(cat_select,origine,option);
					}
					
				}
				
				function check_ut_stagiaire(cat_select,origine,option){
					// origine
					//2 : selection d'une option
					
					// cat_select = categorie qui a declenché l'actu
					//console.log("check_ut_stagiaire(" + cat_select + "," + origine + "," + option + ")");	
					
					ut_sta=new Array();
					var err_ut=false;
					$(".date-test:enabled").each(function(index,obj){
						date=$(this).val();
						if(date!=""){
							cat=$(this).data("categorie");
							ut=parseFloat($("#ut_" + cat).val());
							console.log("ut : " + ut);
							if(!ut_sta[date]){
								ut_sta[date]=0;
							}
							
							if(ut_sta[date]+ut>6){
								err_ut=true;
								if(cat_select==null){
									$("#date_" + cat).val("");
								}
							}else{
								ut_sta[date]=ut_sta[date]+ut;
							}
							console.log("ut_sta[" + date + "] : " + ut_sta[date]);
						}
					});
					if(err_ut){	
						afficher_message("Erreur UT","warning","Le stagiaire ne peut pas dépasser les 6 UT / jour.");
						if(cat_select>0){
							if(origine==2){
								$("#opt_" + cat_select + "_" + option).prop("checked",false);
								calcul_ut_cat(cat_select,origine,option);
							}else{							
								$("#date_" + cat_select).val("");
							}
						}
						
					}
					if(origine==2){
						//selection d'une option
						testeur=$("#testeur_" + cat_select).val();
						check_ut_testeur(cat_select,testeur);
					}
					
				}
				
				function check_ut_testeur(cat_select,testeur){
					// cat_select = categorie qui a declenché l'actu
					var err_ut=false;
					date=$("#date_" + cat_select).val();
					if(date!=""){					
						deja_pris=parseFloat(date_test[date]["testeurs"][testeur]["ut"]);
						
						libre=6-deja_pris;
						
						// FG
						//marche pas car le $this pointe sur option alors que le data est sur le select
						//$(".date-test:enabled option[value='" + date + "']").each(function(index,obj){		
						$(".date-test:enabled").each(function(index,obj){
							if($(this).val()==date){
								console.log($(this));
								cat=$(this).data("categorie");					
								if($("#testeur_" + cat).val()==testeur){
									ut=parseFloat($("#ut_" + cat).val());
									if(libre-ut<0){
										err_ut=true;
									}else{
										libre=libre-ut;
									}
								}
							}
						});
						if(err_ut){	
							afficher_message("Erreur UT","warning","Le testeur ne peut pas dépasser les 6 UT / jour.");
							$("#testeur_" + cat_select).val("");
						}
					}
					if(!err_ut){
						check_testeur_competence(cat_select,testeur);
					}
					
				}
				
				function check_testeur_competence(cat_select,testeur){
				
					ref=0;
					if(testeurs[testeur]["int_type"]==1){
						ref=1;
						ref_id=testeurs[testeur]["int_ref_1"];
					}else if(testeurs[testeur]["int_type"]==3){
						ref=2;
						ref_id=testeurs[testeur]["int_ref_2"];
					}
					
					if(ref>0){
						
						if(ref_id!=0){
						
							liste_comp="";
							if(qualif_cat[cat_select]["qca_competence"]!=""){
								liste_comp=qualif_cat[cat_select]["qca_competence"] + ",";
							};								
							for(i=1;i<=3;i++){
								if($("#opt_" + cat_select + "_" + i).is(":checked")){
									if(qualif_cat[cat_select]["qca_comp_opt_" + i]!=""){
										liste_comp=liste_comp + qualif_cat[cat_select]["qca_comp_opt_" + i] + ",";
									};	
									
								}
								
							}
					
							$.ajax({			
								type:'POST',
								url: 'ajax/ajax_intervenant_competences_check.php',
								data : 'ref=' + ref + "&ref_id=" + ref_id + "&liste_comp=" + liste_comp, 
								dataType: 'JSON',                
								success: function(data){
									if(data.statut==2){
										alert_txt="Le testeur doit disposer des compétence(s) suivante(s) :";
										alert_txt=alert_txt + "<ul>";
										$.each(data.non_valides, function(i, obj) {
											alert_txt=alert_txt + "<li>" + obj + "</li>";
										});
										alert_txt=alert_txt + "</ul>";
										modal_alerte_user(alert_txt);
										$("#testeur_" + cat_select).val("");
									}
								},
								error: function(data){
									modal_alerte_user(data.responseText);
									$("#testeur_" + cat_select).val("");
								}
							});
							
						}else{
							modal_alerte_user("Impossible de contrôler les compétences de ce formateur");
							$("#testeur_" + cat_select).val("");
						}
					}

				}
				
				
				
					
			</script>
		</body>
	</html>