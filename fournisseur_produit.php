<?php
include "includes/controle_acces.inc.php";
include 'includes/connexion.php';
include 'includes/connexion_soc.php';
include 'modeles/mod_contact.php';
include 'modeles/mod_parametre.php';

$fournisseur_id = 0;
if (!empty($_GET['fournisseur'])) {
    $fournisseur_id = intval($_GET['fournisseur']);
}
if (empty($fournisseur_id)) {
    echo ("Impossible d'afficher la page");
    die();
}

//////////////////// Requêtes à la base de données /////////////////////

// rechercher le fournisseur
$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id = :fou_id");
$req->bindParam(':fou_id', $_GET['fournisseur']);
$req->execute();
$fournisseur = $req->fetch();

// TYPE D'ACHAT
$req = $Conn->prepare("SELECT ffa_id,ffa_libelle,ffa_type,ffa_droit FROM fournisseurs_familles INNER JOIN fournisseurs_familles_jointure
ON (fournisseurs_familles.ffa_id=fournisseurs_familles_jointure.ffj_famille)
WHERE ffj_fournisseur=:fournisseur_id");
$req->bindParam("fournisseur_id", $fournisseur_id);
$req->execute();
$fournisseurs_familles = $req->fetchAll();


// rechercher les familles de produit
$req = $Conn->prepare("SELECT pfa_id, pfa_libelle FROM produits_familles");
$req->execute();
$produits_familles = $req->fetchAll();

// si edition, recherche le produit
if (isset($_GET['edition'])) {
    $req = $Conn->prepare("SELECT * FROM fournisseurs_produits WHERE fpr_id = :fpr_id");
    $req->bindParam(':fpr_id', $_GET['edition']);
    $req->execute();
    $produit = $req->fetch();
    if ($produit['fpr_pro_id'] != 0) {
        // recherche le produit SI2P
        $req = $Conn->prepare("SELECT pro_id,pro_code_produit, pro_famille,pro_sous_famille, pro_sous_sous_famille FROM produits WHERE pro_id = :fpr_pro_id");
        $req->bindParam(':fpr_pro_id', $produit['fpr_pro_id']);
        $req->execute();
        $produit_si2p = $req->fetch();

        // recherche le produit SI2P
        $req = $Conn->prepare("SELECT psf_id, psf_pfa_id, psf_libelle  FROM produits_sous_familles WHERE psf_pfa_id = :psf_pfa_id");
        $req->bindParam(':psf_pfa_id', $produit_si2p['pro_famille']);
        $req->execute();
        $produits_sous_familles = $req->fetchAll();
        $req = $Conn->prepare("SELECT pss_id, pss_psf_id, pss_libelle  FROM produits_sous_sous_familles WHERE pss_psf_id = :pss_psf_id");
        $req->bindParam(':pss_psf_id', $produit_si2p['pro_sous_famille']);
        $req->execute();
        $produits_sous_sous_familles = $req->fetchAll();
    }
}
 // rechercher les catégories

if (isset($_GET['edition'])) {
   
    if(empty($produit['fpr_pro_sous_sous_famille'])){
        $produit['fpr_pro_sous_sous_famille'] = 0;
    }
    // si c'est un achat pour revente
    if ($produit['fpr_type'] == 2) {
        // rechercher les catégories de produits
        $req = $Conn->prepare("SELECT pca_id, pca_libelle  FROM produits_categories WHERE pca_revente = 1 AND pca_sous_traitance = 0");
        $req->execute();
        $produits_categories = $req->fetchAll();
        // rechercher les produits SI2P
        $req = $Conn->prepare("SELECT pro_id, pro_code_produit  FROM produits WHERE pro_categorie = :fpr_categorie AND  pro_famille = :pro_famille AND pro_sous_famille = :pro_sous_famille AND pro_sous_sous_famille = :pro_sous_sous_famille");
        $req->bindParam(':fpr_categorie', $produit['fpr_categorie']);
        $req->bindParam(':pro_famille', $produit['fpr_pro_famille']);
        $req->bindParam(':pro_sous_famille', $produit['fpr_pro_sous_famille']);
        $req->bindParam(':pro_sous_sous_famille', $produit['fpr_pro_sous_sous_famille']);
        $req->execute();
        $produits_si2p = $req->fetchAll();
    } elseif ($produit['fpr_type'] == 1) {
        // rechercher les catégories de produits
        $req = $Conn->prepare("SELECT pca_id, pca_libelle  FROM produits_categories WHERE pca_revente = 0 AND pca_sous_traitance = 1");
        $req->execute();
        $produits_categories = $req->fetchAll();
        // rechercher les produits SI2P
        $req = $Conn->prepare("SELECT pro_id, pro_code_produit  FROM produits WHERE pro_categorie = :fpr_categorie AND  pro_famille = :pro_famille AND pro_sous_famille = :pro_sous_famille AND pro_sous_sous_famille = :pro_sous_sous_famille");
        $req->bindParam(':fpr_categorie', $produit['fpr_categorie']);
        $req->bindParam(':pro_famille', $produit['fpr_pro_famille']);
        $req->bindParam(':pro_sous_famille', $produit['fpr_pro_sous_famille']);
        $req->bindParam(':pro_sous_sous_famille', $produit['fpr_pro_sous_sous_famille']);
        $req->execute();
        $produits_si2p = $req->fetchAll();

    }

}

?>
<!DOCTYPE html>
<html>
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>SI2P - Orion - <?=$fournisseur['fou_nom']?></title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
  <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
  <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
  <link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

</head>
<body class="sb-top sb-top-sm ">

	<form id="form" action="fournisseur_produit_enr.php" method="post" class="admin-form form-inline form-inline-grid">

		<div id="main">
	<?php	include "includes/header_def.inc.php";?>
			<section id="content_wrapper" class="">
				<section id="content" class="">

					<div class="admin-form theme-primary ">
						<div class="panel heading-border panel-primary">
							<div class="panel-body bg-light">

								<div class="content-header">
									<h2><?=$fournisseur['fou_nom']?> <b class="text-primary"><?php if (isset($_GET['edition'])) {
    echo ("Modifier un produit");
}else{
    echo ("Nouveau produit");
}
?></b></h2>
								</div>

								<input type="hidden" name="produit" <?php if (isset($_GET['edition'])) {
    echo ("value='" . $_GET['edition'] . "'");
}
?> >
								<input type="hidden" name="fournisseur" value="<?=$_GET['fournisseur']?>" >

								<div class="row">
									<div class="col-md-6">
										<label for="pro_famille">Type d'achat :</label>
										<div class="section">
											<select id="fpr_type" name="pro_type" class="select2">
												<!-- <option value="0" data-type="0" >Type d'achat...</option> -->
									        <?php foreach ($fournisseurs_familles as $f) {
                                                if ($f["ffa_droit"] == 0 or $_SESSION["acces"]["acc_droits"][$f["ffa_droit"]]) {?>
														<option value="<?=$f['ffa_id']?>" data-revente="<?=$f['ffa_type']?>"
                                                            <?php if (isset($_GET['edition'])) {
                                                                if ($produit['fpr_type'] == $f['ffa_id']) {
                                                                echo ("selected");
                                                                } 
                                                            }?> >
															<?=$f['ffa_libelle']?>
														</option>
                                                <?php }
                                                }?>
											</select>
										</div>
									</div>
									<div class="col-md-6 fpr_categorie">
										<label for="fpr_categorie">Catégorie de produit :</label>

										<select name="fpr_categorie" id="fpr_categorie" class="select2 select2-produit-cat">
											<!-- <option value="0">Catégorie de produit...</option> -->
											<?php if (isset($produits_categories)) {
                                                foreach ($produits_categories as $p) {?> 
                                                    <option value="<?=$p['pca_id']?>"
                                                <?php if ($p['pca_id'] == $produit['fpr_categorie']) {
                                                    echo ('selected');
                                                }?>
                                                ><?=$p['pca_libelle']?></option>
                                                <?php }
                                            }?>
										</select>
									</div>
								</div>
								<div class="si2p">
									<div class="row">

											<div class="col-md-4 pro_famille">
											<label for="pro_famille">Famille :</label>
												  <div class="section">

													  <select id="pro_famille" name="pro_famille" class="select2 select2-famille select2-produit-fam">
													  <option value="0">Famille...</option>
														<?php foreach ($produits_familles as $pfa): ?>
														  <option value="<?=$pfa['pfa_id']?>"
														  <?php if (!empty($produit_si2p)) {?>

															<?php if ($produit_si2p['pro_famille'] == $pfa['pfa_id']): ?>
															  selected
															<?php endif;?>
														  <?php }?>
														  ><?=$pfa['pfa_libelle'];?></option>
														<?php endforeach;?>
													  </select>

												  </div>
											</div>

											<div class="col-md-4 pro_sous_famille">
											<label for="pro_sous_famille">Sous-famille :</label>
												  <div class="section">

													  <select id="pro_sous_famille" name="pro_sous_famille" class="select2 select2-famille-sous-famille select2-produit-sous-fam">
														  <option value="0">Sous-famille...</option>
														  <?php if (isset($produits_sous_familles)): ?>
															<?php foreach ($produits_sous_familles as $s): ?>
															  <option value="<?=$s['psf_id']?>" <?php if (isset($produit) && $produit['fpr_pro_sous_famille'] == $s['psf_id']): ?>selected<?php endif;?>><?=$s['psf_libelle'];?></option>
															<?php endforeach;?>
														  <?php endif;?>

													  </select>

												  </div>
											</div>
											<div class="col-md-4 pro_sous_sous_famille">
												<label for="pro_sous_famille">Sous-sous-famille :</label>
												  <div class="section">

													  <select id="pro_sous_sous_famille" name="pro_sous_sous_famille" class="select2 select2-famille-sous-sous-famille select2-produit-sous-sous-fam" >
														  <option value="0">Sous-sous-famille...</option>
														  <?php if (isset($produits_sous_sous_familles)): ?>
															<?php foreach ($produits_sous_sous_familles as $s): ?>
															  <option value="<?=$s['pss_id']?>" <?php if (isset($produit) && $produit['pro_sous_sous_famille'] == $s['pss_id']): ?>selected<?php endif;?>><?=$s['pss_libelle'];?></option>
															<?php endforeach;?>
														  <?php endif;?>

													  </select>

												  </div>
											</div>
									</div>
									<div class="row produit_si2p">

											<div class="col-md-12">
											<label for="fpr_pro_id">Produit SI2P :</label>
                                           
											  <div class="section">

													<select name="fpr_pro_id" class="select2-produit" data-produit_type="1" >
														<option value="0">Produit SI2P...</option>
															<?php
                                                            if (isset($_GET['edition']) && !empty($produit_si2p)) {
                                                                foreach ($produits_si2p as $p) {
                                                                    ?>
															<option
															<?php if ($p['pro_id'] == $produit['fpr_pro_id']) {?>
																selected
															<?php }?>
															value="<?=$p['pro_id']?>"><?=$p['pro_code_produit']?></option>
															<?php }?>
														<?php }?>

													</select>

											  </div>
											</div>
											<!-- // référence produit si2P -->
											<input type="hidden" name="fpr_pro_reference" id="fpr_pro_reference" <?php if (!empty($produit_si2p)) {?>value="<?=$produit_si2p['pro_code_produit']?>"<?php }?>>

									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<label for="fpr_reference">Référence fournisseur:</label>
										<div class="section">
											<label class="field">
												<input type="text" name="fpr_reference" id="fpr_reference" class="gui-input nom" placeholder="Référence" <?php if (isset($produit)): ?>value="<?=$produit['fpr_reference']?>"<?php endif;?>>
											</label>
										</div>
									</div>
									<div class="col-md-6">
										<label for="fpr_reference">Libellé :</label>
										<div class="section">
											<label class="field">
												<input type="text" name="fpr_libelle" id="fpr_libelle" class="gui-input" placeholder="Libellé" <?php if (isset($produit)): ?>value="<?=$produit['fpr_libelle']?>"<?php endif;?>>
											</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<label for="fpr_tarif">Tarif :</label>
										<div class="section">
											<div class="field prepend-icon">
												<input type="text" name="fpr_tarif" id="fpr_tarif" class="gui-input input-float" placeholder="Tarif" <?php if (isset($produit)): ?>value="<?=$produit['fpr_tarif']?>"<?php endif;?>>
												<label for="fpr_tarif_fournisseur" class="field-icon">
													<i class="fa fa-euro"></i>
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label for="fpr_descriptif">Description :</label>
										<textarea class="gui-textarea summernote" id="fpr_descriptif" name="fpr_descriptif" placeholder="Description"><?php if (isset($produit)): ?><?=$produit['fpr_descriptif']?><?php endif;?></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>

		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="fournisseur_voir.php?fournisseur=<?=$_GET['fournisseur']?>&tab=2" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle"></div>
				<div class="col-xs-3 footer-right">
					<button type="submit" id="submit" name="submit" class="btn btn-success btn-sm">
						<i class='fa fa-floppy-o'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
    <style>
      .content-header {
        margin-bottom: 8px !important;
      }

      .ui-priority-secondary {
        display: none !important;
      }
    </style>
    <?php
include "includes/footer_script.inc.php";?>
    <script src="vendor/plugins/mask/jquery.mask.js"></script>
    <script src="vendor/plugins/select2/js/select2.min.js"></script>
    <script src="vendor/plugins/summernote/summernote.min.js"></script>
    <script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
    <script src="assets/js/custom.js"></script>

    <script src="assets/js/responsive-tabs.js"></script>
    <script type="text/javascript">


   ///////////////// FONCTIONS //////////////////
    // actualiser les select dans la recherche produits
    function actualiser_select_produit(type){
        // savoir si
        $.ajax({
            type:'POST',
            url: 'ajax/ajax_fournisseur_traitement_produit.php',
            data : 'type=' + type,
            dataType: 'json',
            success: function(data)
            {
                if(data == 1){// si pas sous-traitance ni achat pour revente
                    $(".fpr_categorie").hide();
                    $(".pro_famille").hide();
                    $(".pro_sous_famille").hide();
                    $(".produit_si2p").hide();
                    $(".pro_sous_sous_famille").hide();
                    $("#fpr_pro_id").val(0);
                    $("#fpr_pro_id").trigger("change");
                    $("#fpr_categorie").val(0);
                    $("#fpr_categorie").trigger("change");
                    $("#pro_famille").val(0);
                    $("#pro_famille").trigger("change");
                    $("#pro_sous_famille").val(0);
                    $("#pro_sous_famille").trigger("change");

                }else{ // si achat pour revente OU sous_traitance

                    // actualisation du select categorie
                    $("#fpr_categorie").find("option").remove();
                    $.each(data, function(key, value) {
                        $('#fpr_categorie').append($("<option></option>").attr("value",value["pca_id"]).text(value["pca_libelle"]));
                    });
                    /* if(type == 1){
                        $('#fpr_categorie').val(3).attr("selected");
                    }else if(type == 2){

                        $('#fpr_categorie').val(1).attr("selected");
                    } */
                    $(".fpr_categorie").show();
                    $(".pro_famille").show();
                    $(".pro_sous_famille").show();
                    $(".pro_sous_sous_famille").show();
                    $(".produit_si2p").show();

                }

            }

        });

    }
    // fin actualiser les select dans la recherche produits
    // produits SI2P en fonction de la catégorie
    /* function produits(){
        famille=$("#pro_famille").val();
        sous_famille=$("#pro_sous_famille").val();
        sous_sous_famille=$("#pro_sous_sous_famille").val();
        type=$("#pro_type").val();
        categorie=$("#fpr_categorie").val();
        <?php if (!empty($produit_si2p)): ?>
            selected = <?=$produit_si2p['pro_id'];?>;
        <?php else: ?>
            selected = 0;
        <?php endif;?>
        $.ajax({
          type:'POST',
          url: 'ajax/ajax_fournisseur_produit.php',
          data : 'categorie=' + categorie + "&famille=" + famille + "&sous_famille=" + sous_famille +"&sous_sous_famille=" + sous_sous_famille + "&type=" + type,
          dataType: 'json',
          success: function(data)
          {
              console.log(data);
            if (data['0'] == undefined){

                $("#fpr_pro_id").find("option:gt(0)").remove();
                $("#fpr_pro_reference").val("");
            }else{
              $("#fpr_pro_id").find("option:gt(0)").remove();
              // on recupere pro_reference intra

                $.each(data, function(key, value) {
                    if(value['pro_id'] == selected){

                      $('#fpr_pro_id')
                      .append($("<option></option>")
                      .attr("value",value["pro_id"])
                      .text(value["pro_code_produit"])
                      .attr("selected",true));

                    }else{
                      $('#fpr_pro_id')
                      .append($("<option></option>")
                      .attr("value",value["pro_id"])
                      .text(value["pro_code_produit"]));
                    }
                });
            }
          }
        });
      } */
    /////////////// FIN FONCTIONS /////////////////

 /////////////// SCRIPTS NORMAUX ////////////////
    // scripts normaux //

    jQuery(document).ready(function () {
        actualiser_select_produit($("#fpr_type").find(':selected').data("revente"));
		$("#fpr_type").change(function(event) {
			/* Act on the event */
			actualiser_select_produit($(this).find(':selected').data("revente"));
		});




      // actualisation produits
     /* produits();*/
      /* $( "#pro_famille" ).change(function() {
        produits();
      });
      $( "#pro_sous_famille" ).change(function() {
        produits();
      });
      $( "#fpr_type" ).change(function() {
        produits();
      }); */
      // fin actualisation produits

      $(".select2-produit").change(function() {
            $("#fpr_pro_reference").val($( ".select2-produit option:selected" ).text());
      });



      // fin actualisation reference produit

      // cacher les champs en fonction du radio

         $('#fpr_type').change(function() {
          if( $('#fpr_type').find('option:selected').attr('data-revente') == 1 || $('#fpr_type').find('option:selected').attr('data-revente') == 2){

            $(".autre").hide();
            $(".si2p").show();

          }else{

            $(".autre").show();
            $(".si2p").hide();

          }

         });

         if($('#fpr_type').find('option:selected').attr('data-revente') == 0){

            $(".autre").show();
            $(".si2p").hide();

          }else{

            $(".autre").hide();
            $(".si2p").show();

          }

      // fin cacher les champs en fonction du radio

    });

    ///////////// FIN SCRIPTS NORMAUX /////////////
    </script>

  </body>
  </html>
