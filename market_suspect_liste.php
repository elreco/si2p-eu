<?php
	include "includes/controle_acces.inc.php";

/* 	LISTE DES CONTACT CLIENTS POUVANT ÊTRE UTILISE POUR LE MARKETING*/

	include('includes/connexion.php');
	include('includes/connexion_soc.php');

	// DONNEE UTILE AU PROGRAMME

	// personne connecté

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}
	$_SESSION['retour'] = "market_client_liste.php";


	if (isset($_POST['search'])){



		$market_action=0;
		if(!empty($_POST['market_action'])){
			$market_action=intval($_POST['market_action']);
		}

		$cli_categorie=0;
		if(!empty($_POST['cli_categorie'])){
			$cli_categorie=intval($_POST['cli_categorie']);
		}
		$cli_sous_categorie=0;
		if(!empty($_POST['cli_sous_categorie'])){
			$cli_sous_categorie=intval($_POST['cli_sous_categorie']);
		}
		$cli_prescripteur=0;
		if(!empty($_POST['cli_prescripteur'])){
			$cli_prescripteur=intval($_POST['cli_prescripteur']);
		}

		$cli_ape="";
		if(!empty($_POST['cli_ape'])){
			if(is_array($_POST['cli_ape'])){
				$cli_ape=implode(",",$_POST['cli_ape']);
			}
		}
		$cli_classification=0;
		if(!empty($_POST['cli_classification'])){
			$cli_classification=intval($_POST['cli_classification']);
		}
		$cli_classification_categorie=0;
		if(!empty($_POST['cli_classification_categorie'])){
			$cli_classification_categorie=intval($_POST['cli_classification_categorie']);
		}
		$cli_classification_type=0;
		if(!empty($_POST['cli_classification_type'])){
			$cli_classification_type=intval($_POST['cli_classification_type']);
		}

		$cso_commercial=0;
		if(!empty($_POST['cli_commercial'])){
			$cso_commercial=intval($_POST['cli_commercial']);
		}

		$con_fonction=0;
		if(!empty($_POST['con_fonction'])){
			$con_fonction=intval($_POST['con_fonction']);
		}
		$con_mail=0;
		if(!empty($_POST['con_mail'])){
			$con_mail=1;
		}

		$cli_import=0;
		if(!empty($_POST['cli_import'])){
			$cli_import=intval($_POST['cli_import']);
		}

		$cli_dep=null;
		if(!empty($_POST['cli_dep'])){
			if(is_array($_POST['cli_dep'])){
				$cli_dep=$_POST['cli_dep'];
			}
		}


		// mémorisation des critere

		$_SESSION['sus_tri'] = array(
			"market_action" => $market_action,
			"cli_categorie" => $cli_categorie,
			"cli_sous_categorie" => $cli_sous_categorie,
			"cli_prescripteur" => $cli_prescripteur,
			"cli_ape" => $cli_ape,
			"cli_classification" => $cli_classification,
			"cli_classification_categorie" => $cli_classification_categorie,
			"cli_classification_type" => $cli_classification_type,
			"cso_commercial" => $cso_commercial,
			"cli_dep" => $cli_dep,
			"con_fonction" => $con_fonction,
			"con_mail" => $con_mail,
			"cli_import" => $cli_import
		);
	}

	// CRITERE DE RECHERCHE

	$critere=array();

	$mil="";

	if(!empty($_SESSION['sus_tri']['market_action'])){
		$critere["market_action"]=$_SESSION['sus_tri']['market_action'];
	}
	if(!empty($_SESSION['sus_tri']['cli_categorie'])){
		$mil.=" AND sus_categorie =:cli_categorie";
		$critere["cli_categorie"]=$_SESSION['sus_tri']['cli_categorie'];
	}
	if(!empty($_SESSION['sus_tri']['cli_sous_categorie'])){
		$mil.=" AND sus_sous_categorie =:cli_sous_categorie";
		$critere["cli_sous_categorie"]=$_SESSION['sus_tri']['cli_sous_categorie'];
	}
	if(!empty($_SESSION['sus_tri']['cli_prescripteur'])){
		$mil.=" AND sus_prescripteur =:cli_prescripteur";
		$critere["cli_prescripteur"]=$_SESSION['sus_tri']['cli_prescripteur'];
	}
	if(!empty($_SESSION['sus_tri']['cli_ape'])){
		$mil.=" AND sus_ape IN (" . $_SESSION['sus_tri']['cli_ape'] . ")";

	}
	if(!empty($_SESSION['sus_tri']['cli_classification'])){
		$mil.=" AND sus_classification =:cli_classification";
		$critere["cli_classification"]=$_SESSION['sus_tri']['cli_classification'];
	}
	if(!empty($_SESSION['sus_tri']['cli_classification_categorie'])){
		$mil.=" AND sus_classification_categorie =:cli_classification_categorie";
		$critere["cli_classification_categorie"]=$_SESSION['sus_tri']['cli_classification_categorie'];
	}
	if(!empty($_SESSION['sus_tri']['cli_classification_type'])){
		$mil.=" AND sus_classification_type =:cli_classification_type";
		$critere["cli_classification_type"]=$_SESSION['sus_tri']['cli_classification_type'];
	}
	if(!empty($_SESSION['sus_tri']['cli_import'])){
		$mil.=" AND sus_import =:cli_import";
		$critere["cli_import"]=$_SESSION['sus_tri']['cli_import'];
	}
	if(!empty($_SESSION['sus_tri']['cso_commercial'])){
		$mil.=" AND sus_commercial=:cso_commercial";
		$critere["cso_commercial"]=$_SESSION['sus_tri']['cso_commercial'];
	};

	// adresse d'inter par defaut (lu sur tab client)
	if(!empty($_SESSION['sus_tri']['cli_dep'])){
		$mil.=" AND (";
		foreach($_SESSION['sus_tri']['cli_dep'] as $k => $cp){
			if($k==0){
				$mil.="sus_adr_cp LIKE '" . $cp . "%'";
			}else{
				$mil.=" OR sus_adr_cp LIKE '" . $cp . "%'";
			}
		}
		$mil.=" )";
	};

	// contact
	if(!empty($_SESSION['sus_tri']['con_fonction'])){
		$mil.=" AND sco_fonction=:con_fonction";
		$critere["con_fonction"]=$_SESSION['sus_tri']['con_fonction'];
	};
	if(!empty($_SESSION['sus_tri']['con_mail'])){
		$mil.=" AND NOT ISNULL(sco_mail) AND NOT sco_mail=''";
	};

	// champ a selectionne
	$sql="SELECT DISTINCT sus_id,sus_code,sus_nom,sus_classification_categorie, sus_classification_type, Suspects_Contacts.sco_id,sco_nom,sco_prenom,sco_tel,sco_portable,sco_mail,sco_market_all,scm_market,sco_market_partiel,sco_market_no,sus_adr_cp
	,Suspects_Correspondances.sco_suspect
	FROM Suspects INNER JOIN Suspects_Contacts ON (Suspects.sus_id=Suspects_Contacts.sco_ref_id AND Suspects_Contacts.sco_ref=1)
	LEFT OUTER JOIN Suspects_Correspondances ON (Suspects.sus_id=Suspects_Correspondances.sco_suspect)
	LEFT OUTER JOIN Suspects_Contacts_Marketing ON (Suspects_Contacts.sco_id=Suspects_Contacts_Marketing.scm_contact AND scm_market=:market_action)";

	// critere auto
	$sql.=" WHERE ISNULL(Suspects_Correspondances.sco_suspect)";
	if(!empty($acc_agence)){
		$sql.=" AND sus_agence=" . $acc_agence;
	}
	if(!$_SESSION['acces']["acc_droits"][6]){
		$sql.=" AND sus_utilisateur=" . $acc_utilisateur;
	}
	$sql.=" AND ISNULL(Suspects_Correspondances.sco_suspect)";
	if($mil!=""){
		$sql.=$mil;
	}
	$sql.=" ORDER BY sco_nom,sco_prenom,sus_code,sus_nom,sus_id;";

	$req = $ConnSoc->prepare($sql);

	if(!empty($critere)){
		foreach($critere as $c => $u){
			//echo($c . "=>" . $u . "<br/>");
			$req->bindValue($c,$u);
		}
	};
	$req->execute();
	$clients = $req->fetchAll();


?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">

		<!-- Start: Main -->
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php		if(!empty($clients)){ ?>
						<h1>Liste des contacts</h1>
						<div class="table-responsive">
							<table class="table" id="table_id">
								<thead>
									<tr class="dark" >
										<th>ID</th>
										<th>Nom</th>
										<th>Prenom</th>
										<th>Téléphone</th>
										<th>Portable</th>
										<th>Mail</th>
										<th>Code client</th>
										<th>Nom client</th>
										<th>Consentement</th>
										<th>Catégorie de classification</th>
										<th>Type de classification</th>
										<th>Dpt</th>
									</tr>
								</thead>
								<tbody>
						<?php		foreach($clients as $c){ ?>
										<tr>
											<td><?=$c['sco_id'] ?></td>
											<td><?=$c['sco_nom'] ?></td>
											<td><?=$c['sco_prenom'] ?></td>
											<td><?=$c['sco_tel']?></td>
											<td><?=$c['sco_portable']?></td>
											<td><?=$c['sco_mail']?></td>
											<td><?=$c['sus_code']?></td>
											<td><?=$c['sus_nom']?></td>
											<td>
									<?php		if($c['sco_market_all'] OR ($c['sco_market_partiel'] AND !empty($c['scm_market'])) ){
													echo("OK");
												}elseif($c['sco_market_no'] OR ($c['sco_market_partiel'] AND empty($c['scm_market'])) ){
													echo("REFUS");
												}else{
													echo("PAS DE REPONSE");
												} ?>
											</td>
											<?php
												if(!empty($c["sus_classification_categorie"])){
													$sql="SELECT ccc_libelle FROM Clients_classifications_categories WHERE ccc_id=" . $c["sus_classification_categorie"] . ";";
													$req=$Conn->query($sql);
													$d_classification_categorie=$req->fetch();
												}

												if(!empty($c["sus_classification_type"])){
													$sql="SELECT cct_libelle FROM Clients_classifications_types WHERE cct_id=" . $c["sus_classification_type"] . ";";
													$req=$Conn->query($sql);
													$d_classification_type=$req->fetch();
												}
											?>
											<td>
												<?php if(!empty($d_classification_categorie)){ ?>
													<?=$d_classification_categorie['ccc_libelle']?>
												<?php }?>
											</td>
											<td>
												<?php if(!empty($d_classification_type)){ ?>
													<?=$d_classification_type['cct_libelle']?>
												<?php }?>
											</td>
											<td><?=substr($c['sus_adr_cp'], 0, 2)?></td>
										</tr>
							<?php	} ?>
								</tbody>
							</table>
						</div>
		<?php 		}else{ ?>
						<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-warning" style="border-radius:0px;">
								Aucun contact correspondant à votre recherche.
							</div>
						</div>
		<?php 		} ?>
				</section>
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="market_suspect_tri.php" class="btn btn-primary btn-sm">
						<span class="fa fa-search"></span>
						<span class="hidden-xs">Nouvelle recherche</span>
					</a>
				</div>
				<div class="col-xs-6 footer-middle">&nbsp;</div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>
<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				$('#table_id').DataTable( {
					"language": {
						"url": "vendor/plugins/DataTables/media/js/French.json"
					},
					"paging": false,
					"searching": false,
					"info": false,
					"order": [[ 1, "asc" ]]
				} );
				 //Disable full page
			    /*$('body').bind('cut copy paste', function (e) {
			        e.preventDefault();
			    });*/
			    //Disable mouse right click
			   /* $("body").on("contextmenu",function(e){
			        return false;
			    });*/
			});
		</script>
	</body>
</html>
