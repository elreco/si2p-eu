<?php 
$menu_actif = 4;
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

require "modeles/mod_document.php";
include('modeles/mod_droit.php');
include('modeles/mod_utilisateur.php');
if (!empty($droits_uti)){
  Header("Location: etech.php");
}
$droits_uti = get_droit_utilisateur(1, $_SESSION['acces']['acc_ref_id']);
if(isset($_GET['id'])){
  $d = get_document($_GET['id']);
}

if(!empty($_GET['dos'])){
  $dossier = get_document($_GET['dos']);
  $dossier_url= $dossier['doc_url'] . $dossier['doc_nom_aff'] . "/";
  
}else{
  $dossier_url = "ETECH/";
}
?>
<!DOCTYPE html>
<html> 
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>SI2P - Orion - Etech</title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

  </head>
  
	<body class="sb-top sb-top-sm ">
		
		<form 
			<?php if(isset($_GET['id'])): ?>
			  action="etech_doc_enr.php?replace=1&id=<?= $_GET['id'] ?>"
			<?php else: ?>
			  action="etech_doc_enr.php"
			<?php endif; ?> method="POST" id="admin-form" enctype="multipart/form-data">
			
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>

				<section id="content_wrapper" class="" style="margin-bottom:40px;">
					<section id="content" class="animated fadeIn">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											
											<div class="text-center">
												<div class="content-header">
													<h2><?php if(isset($_GET['id'])): ?>Editer le document <b class="text-primary"><?= $d['doc_nom_aff'] ?></b><?php else: ?>Ajouter un <b class="text-primary">document</b><?php endif; ?></h2>
													<h3>Dans: <?= $dossier_url; ?></h3>
												</div>

										<?php 	if(isset($_GET['id'])): ?>
													<div class="col-md-4 col-md-offset-4">
													  <div class="panel panel-tile text-center br-a br-grey">
														<div class="panel-body">
														  <h1 class="fs30 mt5 mbn">Document actuel</h1>
														  <h6 class="text-system" style="font-size:50px;"><a href="documents/etech/<?= $d['doc_nom'] ?>" download><i class="fa fa-download" aria-hidden="true"  data-toggle="tooltip" data-placement="bottom" title="Voir le document"></i></a></h6>
														</div>
														<div class="panel-footer br-t p12">
														  <span class="fs11">
															<?php $document = pathinfo($d['doc_nom']); ?>
															<?= $document['filename'] ?><b>.<?= $d['doc_ext'] ?></b>
														  </span>
														</div>
													  </div>
													</div>
										<?php 	endif; ?>

												<div class="col-md-10 col-md-offset-1">

												
													<input type="hidden" name="url" value="<?= $dossier_url ?>">
													<div class="row">
														<div class="col-md-12">
														  <div class="section">

															<label class="field prepend-icon file">

															  <span class="button btn-primary">Choisir</span>
															  <input id="fileupload" type="file" required="" name="doc" class="gui-file" onchange="document.getElementById('uploader2').value = this.value;">
															  <input type="text" class="gui-input" id="uploader2" <?php if(isset($_GET['id'])): ?>placeholder="Remplacer le fichier actuel"<?php else: ?>placeholder="Merci de sélectionner un fichier"<?php endif; ?>>
															  <label class="field-icon">
																<i class="fa fa-upload"></i>
															  </label>
															</label>
														  </div>
														</div>
														<div id="status" class="text-primary"></div>
													</div>
													<div class="row">   
														<div class="col-md-12 mb10">
															<label class="option option-success">
															  <input type="checkbox" id="doc_nouveau"  name="doc_nouveau" value="1" 

															  <?php if(empty($_GET['id'])){ ?>
																	checked
															  <?php }elseif(!empty($d['doc_nouveau'])){ ?>
																	checked
															  <?php } ?>>
															  <span class="checkbox mn"></span> Afficher comme nouveau document
														  </label> 
														</div>
													</div> 
                          
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
										
			<footer id="content-footer" class="affix">
				<div class="row">
				  <div class="col-xs-3 footer-left" >
					<a <?php if(isset($_GET['dos'])): ?>href="etech.php?dos=<?= $_GET['dos'] ?>"<?php else: ?>href="etech.php"<?php endif; ?>  class="btn btn-default btn-sm">
					  <i class="fa fa-long-arrow-left"></i>
					  Retour
					</a>
				  </div>
				  <div class="col-xs-6 footer-middle" >
				   <div class="progress" style="margin-bottom:0; margin-top:7px;">

					<div class="progress-bar progress-bar-success progress-bar-striped bar" role="progressbar" aria-valuemin="0" aria-valuemax="100">
					  <span class="percent text-center"></span>
					</div>
				  </div>

				  
				</div>
				<div class="col-xs-3 footer-right" >
				  <button type="submit" name="submit" id="upload-btn" class="btn btn-success btn-sm">
					<i class='fa fa-save'></i> Enregistrer
				  </button>
				  <button type="button" id="cancel-btn" class="btn btn-danger btn-sm" style="display:none;">
					<i class='fa fa-times'></i> Annuler
				  </button>
				</div>
			  </div>
			</footer>
		</form>
		
<?php 	include "includes/footer_script.inc.php"; ?>
		<!-- validation inputs -->
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<!-- plugin pour les masques formulaires -->
		<script src="vendor/plugins/holder/holder.min.js"></script>
		<!-- pour mettre des images de tests -->
		<!-- Theme Javascript -->
		<script src="assets/js/custom.js"></script>
		<script src="vendor/plugins/upload/jquery.form.js"></script>
		<script type="text/javascript">
		
			// surclasse val dans orion.js (en ms)
			// pour permettre la fin du téléchargement.
			//delai_deconnect_alert=86400000 //24H
			delai_deconnect=86400000;
			




			// formulaire agence, agence / société


        /* @custom validation method (smartCaptcha)
        ------------------------------------------------------------------ */
        //setInterval(reconnect, 3000);
        
		$(document).ready(function() { 
          $(function() {
            
            var bar = $('.bar');
            var percent = $('.percent');
            var status = $('#status');
            $(".progress").hide();
           

            $('form').ajaxForm({

              timeout: 10000000000000,

              beforeSend: function(xhr) {

                status.empty();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.text(percentVal);
                $( "#cancel-btn" ).show();
                $( "#upload-btn" ).hide();
                $( "#cancel-btn" ).click(function() {
                  xhr.abort();
                  $( "#cancel-btn" ).hide();
                  $( "#upload-btn" ).show();
                });

              },
              uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.text(percentVal);
                $(".progress").show();
                $( "#cancel-btn" ).show();
                $( "#upload-btn" ).hide();
                
              },
              complete: function(xhr) {
                $( "#cancel-btn" ).hide();
                $( "#upload-btn" ).show();
                
                status.html(xhr.responseText);
                console.log(status.text());
                if ($.isNumeric(status.text())) {
                  $(".progress").show();
                  var delay = 500; //Your delay in milliseconds

                  setTimeout(function(){ 
                          <?php if(isset($_GET['dos'])): ?>
                            <?php if(isset($_GET['id'])): ?>
                                window.location = "etech.php?dos=<?= $_GET['dos'] ?>&succes=3"; 
                            <?php else: ?>
                              window.location = "etech_doc_mod.php?id=" + status.text() + "&upload=1&dos=<?= $_GET['dos'] ?>"; 
                            <?php endif; ?>
                          <?php else: ?>
                            <?php if(isset($_GET['id'])): ?>
                              window.location = "etech.php?succes=3"; 
                            <?php else: ?>
                              window.location = "etech_doc_mod.php?id=" + status.text() + "&upload=1"; 
                            <?php endif; ?>
                          <?php endif; ?>
                  }, delay);
                  status.hide();

                }else{
                  $(".progress").hide();
                }
              }

            });
             
              
          }); 
        });

      </script>

    </body>
</html>
