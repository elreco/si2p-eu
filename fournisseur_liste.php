<?php
//////////////// MENU ACTIF ////////////////////
$menu_actif = 5;
//////////////// INCLUDES /////////////////////
include "includes/controle_acces.inc.php";
include 'includes/connexion.php';
include 'includes/connexion_soc.php';
include 'modeles/mod_parametre.php';
require "modeles/mod_droit.php";
require "modeles/mod_contact.php";
require "modeles/mod_get_commerciaux.php";
require "modeles/mod_erreur.php";
//////////////// REQUETES VERS LE SERVEUR //////////////////
$_SESSION['retour'] = "fournisseur_liste.php";

$acc_agence = 0;
if (isset($_SESSION['acces']["acc_agence"])) {
    $acc_agence = $_SESSION['acces']["acc_agence"];
}
// droit compta
if ($_SESSION['acces']['acc_ref'] == 1) {
    $acc_utilisateur = $_SESSION['acces']['acc_ref_id'];
    if (!empty(get_droit_utilisateur(9, $_SESSION['acces']['acc_ref_id']))) {
        $acc_drt_compta = true;
    } else {
        $acc_drt_compta = false;
    }
}
// initialisation des variables pour la requete recherche
$fou_blacklist = 0;
$fou_famille_non = 0;
$fou_archive = 0;
// si la recherche est activée
if (isset($_POST['search'])) {

    if (isset($_POST['fou_blacklist'])) {
        $fou_blacklist = 1;
    }
    if (isset($_POST['fou_archive'])) {
        $fou_archive = 1;
    }

    if (isset($_POST['fou_famille_non'])) {
        $fou_famille_non = 1;
    }
    // si la famille sélectionnée n'est pas achats pour revente ou sous traitance
    if ($_POST['fou_famille'] != 1 or $_POST['fou_famille'] != 7) {
        $_POST['pro_famille'] = 0;
        $_POST['pro_sous_famille'] = 0;
    }
    $fou_admin_etat = array();
    if (isset($_POST['fou_admin_etat'])) {

        foreach ($_POST['fou_admin_etat'] as $a) {

            $fou_admin_etat[] = $a;

        }
        $fou_admin_etat = implode(',', $fou_admin_etat);
    }

    $_SESSION['fou_tri'] = array(
        "fou_code" => $_POST['fou_code'],
        "fou_nom" => $_POST['fou_nom'],
        "fou_blacklist" => $fou_blacklist,
        "fou_archive" => $fou_archive,
        "fad_cp" => $_POST['fad_cp'],
        "fad_ville" => $_POST['fad_ville'],
        "ffj_famille" => $_POST['fou_famille'],
        "fou_famille_non" => $fou_famille_non,
        "fco_nom" => $_POST['fco_nom'],
        "fco_prenom" => $_POST['fco_prenom'],
        "fco_mail" => $_POST['fco_mail'],
        "fco_fonction" => $_POST['fco_fonction'],
        "fco_fonction_nom" => $_POST['fco_fonction_nom'],
        "fin_nom" => $_POST['fin_nom'],
        "fin_prenom" => $_POST['fin_prenom'],
        "fic_competence" => $_POST['fic_competence'],
        "fou_siren" => $_POST['fou_siren'],
        "fou_siret" => $_POST['fou_siret'],
        "pro_famille" => $_POST['pro_famille'],
        "pro_sous_famille" => $_POST['pro_sous_famille'],
        "fou_admin_etat" => $fou_admin_etat,
    );
}

// ON CONSTRUIT LA REQUETE

if (isset($_SESSION['fou_tri'])) {

    ////////////// FOURNISSEURS /////////////
    /////////////////////////////
    // VARIABLES DE RECHERCHES //
    //  rechercheParAdresse ==> recherche sur les adresses //
    //  rechercheParProduit ==> recherche sur les produits //
    //  rechercheParContact ==> recherche sur les contacts //
    //  rechercheParFamille ==> recherche sur les familles //
    //  rechercheParIntervenant ==> recherche sur les intervenants //
    //  rechercheParCompetence ==> recherche sur les compétences liée aux intervenants //
    // FIN VARIABLES DE RECHERCHES //
    /////////////////////////////////
    // recherche sur les fournisseurs
    $critere = array();
    $mil = "";
    // critère sur les adresses
    $rechercheParAdresse = false;

    if (!empty($_SESSION['fou_tri']['fad_cp'])) {
        $mil .= " AND fad_cp LIKE :fad_cp";
        $critere["fad_cp"] = "%" . str_replace(' ', '', $_SESSION['fou_tri']['fad_cp']) . "%";
        $rechercheParAdresse = true;
    }
    ;

    if (!empty($_SESSION['fou_tri']['fad_ville'])) {
        $mil .= " AND fad_ville LIKE :fad_ville";
        $critere["fad_ville"] = "%" . $_SESSION['fou_tri']['fad_ville'] . "%";
        $rechercheParAdresse = true;
    }
    ;

    // critère sur les produits
    $rechercheParProduit = false;

    if (!empty($_SESSION['fou_tri']['pro_famille'])) {
        $mil .= " AND fpr_famille = :fpr_famille";
        $critere["fpr_famille"] = $_SESSION['fou_tri']['fpr_famille'];
        $rechercheParProduit = true;
    }
    ;
    if (!empty($_SESSION['fou_tri']['pro_sous_famille'])) {
        $mil .= " AND fpr_sous_famille = :fpr_sous_famille";
        $critere["fpr_sous_famille"] = $_SESSION['fou_tri']['fpr_sous_famille'];
        $rechercheParProduit = true;
    }
    ;

    // critère sur les contacts

    $rechercheParContact = false;
    if (!empty($_SESSION['fou_tri']['fco_nom'])) {
        $mil .= " AND fco_nom LIKE :fco_nom";
        $critere["fco_nom"] = "%" . $_SESSION['fou_tri']['fco_nom'] . "%";
        $rechercheParContact = true;
    }
    ;
    if (!empty($_SESSION['fou_tri']['fco_prenom'])) {
        $mil .= " AND fco_prenom LIKE :fco_prenom";
        $critere["fco_prenom"] = "%" . $_SESSION['fou_tri']['fco_prenom'] . "%";
        $rechercheParContact = true;
    }
    ;

    if (!empty($_SESSION['fou_tri']['fco_mail'])) {
        if ($_SESSION['fou_tri']['fco_mail'] == 1) {
            $mil .= " AND (fco_mail IS NOT NULL AND fco_mail != '')";
        } else {
            $mil .= " AND (fco_mail IS NULL OR fco_mail = '')";
        }
        $rechercheParContact = true;
    }
    ;

    if (!empty($_SESSION['fou_tri']['fco_fonction'])) {
        $mil .= " AND fco_fonction =:fco_fonction";
        $critere["fco_fonction"] = $_SESSION['fou_tri']['fco_fonction'];
        $rechercheParContact = true;
    }
    ;
    if (!empty($_SESSION['fou_tri']['fco_fonction_nom'])) {
        $mil .= " AND fco_fonction_nom LIKE :fco_fonction_nom";
        $critere["fco_fonction_nom"] = "%" . $_SESSION['fou_tri']['fco_fonction_nom'] . "%";
        $rechercheParContact = true;
    }
    ;
    // fin recherche par contact

    // sur le fournisseur
    if (!empty($_SESSION['fou_tri']['fou_code'])) {
        $mil .= " AND fou_code LIKE :fou_code";
        $critere["fou_code"] = "%" . $_SESSION['fou_tri']['fou_code'] . "%";
    }
    ;
    if (!empty($_SESSION['fou_tri']['fou_nom'])) {
        $mil .= " AND fou_nom LIKE :fou_nom";
        $critere["fou_nom"] = "%" . $_SESSION['fou_tri']['fou_nom'] . "%";
    }
    ;

    if ($_SESSION['fou_tri']['fou_blacklist'] == 1) {
        $mil .= " AND fou_blacklist = 1";
    }else{
        $mil .= " AND (fou_blacklist = 0 OR fou_blacklist IS NULL)";
    }
    if ($_SESSION['fou_tri']['fou_archive'] == 1) {
        $mil .= " AND fou_archive = 1";
    } else {
        $mil .= " AND NOT fou_archive";
    }
    ;
    if (!empty($_SESSION['fou_tri']['fou_siren'])) {
        $mil .= " AND fou_siren LIKE :fou_siren";
        $critere["fou_siren"] = str_replace(' ', '', $_SESSION['fou_tri']['fou_siren']);
    }
    ;
    if (!empty($_SESSION['fou_tri']['fou_siret'])) {
        $mil .= " AND fou_siret LIKE :fou_siret";
        $critere["fou_siret"] = str_replace(' ', '', $_SESSION['fou_tri']['fou_siret']);
    }
    ;
    if (!empty($_SESSION['fou_tri']['fou_admin_etat'])) {
        $mil .= " AND fva_admin_etat IN(" . $_SESSION['fou_tri']['fou_admin_etat'] . ")";
        /*$critere["fou_admin_etat"]=$_SESSION['fou_tri']['fou_admin_etat'];*/
    }
    ;

    // critère sur la famille
    $rechercheParFamille = false;
    if (!empty($_SESSION['fou_tri']['fou_famille_non'])) {
        $rechercheParFamille = true;
        $mil .= " AND ISNULL(ffj_famille)";
    }
    ;

    if (!empty($_SESSION['fou_tri']['ffj_famille'])) {
        $mil .= " AND ffj_famille = :ffj_famille";
        $critere["ffj_famille"] = $_SESSION['fou_tri']['ffj_famille'];
        $rechercheParFamille = true;
    }
    ;

    // critère sur les intervenants
    $rechercheParIntervenant = false;
    if (!empty($_SESSION['fou_tri']['fin_prenom'])) {
        $mil .= " AND fin_prenom LIKE :fin_prenom";
        $critere["fin_prenom"] = "%" . $_SESSION['fou_tri']['fin_prenom'] . "%";
        $rechercheParIntervenant = true;
    }
    ;

    if (!empty($_SESSION['fou_tri']['fin_nom'])) {
        $mil .= " AND fin_nom LIKE :fin_nom";
        $critere["fin_nom"] = "%" . $_SESSION['fou_tri']['fin_nom'] . "%";
        $rechercheParIntervenant = true;
    }
    ;
    $rechercheParCompetence = false;
    if (!empty($_SESSION['fou_tri']['fic_competence'])) {
        $mil .= " AND ico_competence = :ico_competence";
        $critere["ico_competence"] = $_SESSION['fou_tri']['fic_competence'];
        $rechercheParIntervenant = true;
        $rechercheParCompetence = true;
    }

    $sql = "SELECT DISTINCT fou_id,fou_code,fou_nom
	,fad_ad1,fad_ad2,fad_ad3,fad_cp,fad_ville
	,fco_fonction,fco_fonction_nom,fco_nom,fco_prenom,fco_tel,fco_mail";
    $sql .= " FROM Fournisseurs";
    $sql = $sql . " LEFT JOIN Fournisseurs_adresses ON (Fournisseurs.fou_id = Fournisseurs_adresses.fad_fournisseur)";
    $sql = $sql . " LEFT JOIN fournisseurs_contacts ON (Fournisseurs.fou_id = Fournisseurs_Contacts.fco_fournisseur";
    if (empty($rechercheParContact)) {
        $sql = $sql . " AND Fournisseurs_Contacts.fco_defaut";
    }
    $sql = $sql . " )";

    if (!empty($rechercheParIntervenant)) {
        $sql = $sql . " LEFT JOIN Fournisseurs_Intervenants ON Fournisseurs.fou_id = Fournisseurs_Intervenants.fin_fournisseur";
    }
    if (!empty($rechercheParCompetence)) {
        $sql = $sql . " LEFT JOIN Intervenants_competences ON Fournisseurs_Intervenants.fin_id = Intervenants_competences.ico_ref_id";
    }
    if (!empty($rechercheParFamille)) {
        $sql = $sql . " LEFT JOIN Fournisseurs_Familles_Jointure ON Fournisseurs.fou_id = Fournisseurs_Familles_Jointure.ffj_fournisseur";
    }
    if (!empty($_SESSION['fou_tri']['fou_admin_etat'])) {
        $sql = $sql . " LEFT JOIN Fournisseurs_validations ON Fournisseurs.fou_id = Fournisseurs_validations.fva_fournisseur";
    }

    if (!empty($rechercheParProduit)) {
        $sql = $sql . " LEFT JOIN Fournisseurs_Produits ON Fournisseurs.fou_id = Fournisseurs_Produits.fpr_fournisseur))";
    }

    /*  if(!$rechercheParCorrespondance){
    $sql=$sql ." GROUP BY fou_id,fou_code,fou_nom";
    $sql.=",fad_ad1,fad_ad2,fad_ad3,fad_cp,fad_ville";
    $sql.=",fco_prenom,fco_nom,fco_fonction,fco_fonction_nom,fco_tel,fco_mail";
    }*/
    if ($mil != "") {
        $sql .= " WHERE " . substr($mil, 5, strlen($mil) - 5);
    }
    $sql .= " ORDER BY fou_id LIMIT 8000";

}
$req = $Conn->prepare($sql);
if (!empty($critere)) {
    foreach ($critere as $c => $v) {
        $req->bindValue($c, $v);
        //echo($c . " : " . $v . "<br/>");
    }
}
;
//die(var_dump($critere));
$req->execute();
$fournisseurs = $req->fetchAll();
?>

<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Si2P - ORION</title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="Si2P">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->

    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
    <style type="text/css">
        /* Paste this css to your style sheet file or under head tag */
        /* This only works with JavaScript,
		if it's not present, don't show loader */

        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(assets/img/loader.gif) center no-repeat #fff;
        }
    </style>
</head>

<body class="sb-top sb-top-sm">

    <!-- loder page -->
    <div class="se-pre-con"></div>

    <div id="main">
        <?php	include "includes/header_def.inc.php";?>
        <section id="content_wrapper">
            <section id="content" class="animated fadeIn">
                <h1>Liste des fournisseurs</h1>

                <?php
if (isset($_GET['succes']) && $_GET['succes'] == 1): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            Le fournisseur a été créé
                        </div>
                    </div>
                </div>
                <?php endif;?>
                <?php
if (isset($_GET['succes']) && $_GET['succes'] == 1): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            Le fournisseur a été supprimé
                        </div>
                    </div>
                </div>
                <?php endif;?>
                <?php
if (isset($_GET['succes']) && $_GET['succes'] == 3): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            Le(s) fournisseur(s) a/ont bien été supprimé(s).
                        </div>
                    </div>
                </div>
                <?php endif;?>


                <?php if (!empty($fournisseurs)) {?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="table_id">
                        <thead>
                            <tr class="dark">
                                <th>ID</th>
                                <th>Code</th>
                                <th>Nom</th>
                                <th>Adresse</th>
                                <th>CP</th>
                                <th>Ville</th>
                                <th>Contact</th>
                                <th>Fonction</th>
                                <th>Tel</th>
                                <th>Mail</th>
                                <?php if ($acc_drt_compta == true) {?>
                                <th class="no-sort">Archiver</th>
                                <th class="text-center no-sort">
                                    <input type="checkbox" id="selectall" name="archiver" value="oui">
                                </th>
                                <?php	}?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($fournisseurs as $c) {?>
                            <tr data-id="<?=$c['fou_id'];?>">
                                <td>
                                    <?=$c['fou_id']?>
                                </td>
                                <td><a href="fournisseur_voir.php?fournisseur=<?=$c['fou_id']?>">
                                        <?=$c['fou_code']?></a></td>
                                <td>
                                    <?=$c['fou_nom']?>
                                </td>
                                <td>
                                    <?=$c['fad_ad1']?>
                                    <?php if (!empty($c['fad_ad2'])): ?><br>
                                    <?php endif;?>
                                    <?=$c['fad_ad2']?>
                                    <?php if (!empty($c['fad_ad3'])): ?><br>
                                    <?php endif;?>
                                    <?=$c['fad_ad3']?>
                                </td>
                                <td>
                                    <?=$c['fad_cp']?>
                                </td>
                                <td>
                                    <?=$c['fad_ville']?>
                                </td>
                                <td>
                                    <?=$c['fco_prenom']?>
                                    <?=$c['fco_nom']?>
                                </td>
                                <td>
                                    <?php if (empty($c['fco_fonction'])) {?>
                                    <?=$c['fco_fonction_nom']?>
                                    <?php } else {?>
                                    <?php $fonction_nom = get_fonction($c['fco_fonction']);?>
                                    <?=$fonction_nom['cfo_libelle'];?>
                                    <?php }?>
                                </td>
                                <td>
                                    <?=$c['fco_tel']?>
                                </td>
                                <td>
                                    <?=$c['fco_mail']?>
                                </td>
                                <?php if ($acc_drt_compta == true): ?>

                                <?php if ($fou_archive == 1) {?>
                                <!-- SI la recherche est sur les fournisseurs archivés -->
                                <td class="text-center">
                                    <a href="#" data-id="<?=$c['fou_id'];?>" class="btn btn-success btn-sm submit_delete"
                                        data-target="#modal-suppr" data-toggle="modal">
                                        <i class="fa fa-archive" aria-hidden="true"></i>
                                    </a>
                                </td>
                                <?php } else {?>
                                <td class="text-center">
                                    <a href="#" data-id="<?=$c['fou_id'];?>" class="btn btn-warning btn-sm submit_delete"
                                        data-target="#modal-suppr" data-toggle="modal">
                                        <i class="fa fa-archive" aria-hidden="true"></i>
                                    </a>
                                </td>
                                <?php }?>

                                <td class="text-center">
                                    <div class="checkbox-custom checkbox-default mb5">
                                        <input type="checkbox" id="checkbox<?=$c['fou_id']?>" class="checkbox1" name="fournisseur[]"
                                            value="<?=$c['fou_id']?>">
                                        <label for="checkbox<?=$c['fou_id']?>"></label>
                                    </div>
                                </td>
                                <?php endif;?>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
                <?php } else {?>
                <div class="col-md-12 text-center" style="padding:0;">
                    <div class="alert alert-warning" style="border-radius:0px;">
                        Aucun fournisseur correspondant à votre recherche.
                    </div>
                </div>
                <?php }?>
            </section>
        </section>
    </div>

    <!-- Modal -->

    <div id="modal-suppr" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title modal-delete">Archiver un fournisseur</h4>
                </div>
                <div class="modal-body">
                    <p>Êtes-vous sûr de vouloir archiver ce fournisseur ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" id="submit_delete_btn">Oui</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
                </div>
            </div>

        </div>
    </div>
    <div id="myModal4" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title modal-delete-multi">Archivage de fournisseurs</h4>
                </div>
                <div class="modal-body">
                    <p>Êtes-vous sûr de vouloir archiver les éléments selectionnés ?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="submit5" id="submit5" class="btn btn-success btn-sm">Oui</button>

                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Non</button>
                </div>
            </div>

        </div>
    </div>
    <!-- End: Main -->
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">

                <a href="fournisseur_tri.php" class="btn btn-primary btn-sm" role="button">
                    <span class="fa fa-search"></span>
                    <span class="hidden-xs">Nouvelle recherche</span>
                </a>

            </div>
            <div class="col-xs-6 footer-middle">&nbsp;</div>
            <div class="col-xs-3 footer-right">

                <a href="fournisseur.php?retour=liste" class="btn btn-success btn-sm" role="button">
                    <span class="fa fa-plus"></span>
                    <span class="hidden-xs">Nouveau fournisseur</span>
                </a>
                <?php if ($acc_drt_compta == true): ?>
                <?php if ($fou_archive == 0) {?>
                <a href="#" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#myModal4">
                    <span class="fa fa-archive"></span>
                    <span class="hidden-xs">Archiver</span>
                </a>
                <?php } else {?>
                <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal4">
                    <span class="fa fa-archive"></span>
                    <span class="hidden-xs">Désarchiver</span>
                </a>
                <?php }?>
                <?php endif;?>


            </div>
        </div>
    </footer>
    <?php
include "includes/footer_script.inc.php";?>
    <script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            //paste this code under the head tag or in a separate js file.
            // Wait for window load
            $(window).load(function () {
                // Animate loader off screen
                $(".se-pre-con").fadeOut("slow");;
            });
            $("#selectall").change(function () {
                $(".checkbox1").prop('checked', $(this).prop("checked"));
            });
            $("#submit5").click(function () {
                $('#myModal4').modal('toggle');
                $('.checkbox1:checked').each(function () {
                    $('[data-id=' + $(this).val() + ']').hide();
                });
                $.ajax({
                    <?php if ($fou_archive == 1) {?>
                    url: "ajax/ajax_fournisseur_ajout_multi.php",
                    <?php } else {?>
                    url: "ajax/ajax_fournisseur_suppr_multi.php",
                    <?php }?>
                    type: "post",
                    data: $('.checkbox1:checked').serialize(),
                    success: function (data) {

                    }
                });

            });

            // archivage d'un fournisseur
            $(".submit_delete").click(function () {
                var fournisseur = $(this).data('id');
                $('#submit_delete_btn').data('id', fournisseur);
            });
            $("#submit_delete_btn").click(function () {
                var fournisseur = $(this).data('id');
                $('[data-id=' + fournisseur + ']').hide();
                $('#modal-suppr').modal('toggle');
                $.ajax({
                    type: 'POST',
                    <?php if ($fou_archive == 1) {?>
                    url: "ajax/ajax_fournisseur_ajout.php",
                    <?php } else {?>
                    url: "ajax/ajax_fournisseur_suppr.php",
                    <?php }?>
                    data: 'id=' + fournisseur,
                    dataType: 'json',
                    success: function (data) {

                    }

                });
            });


            $('#table_id').DataTable({
                "language": {
                    "url": "vendor/plugins/DataTables/media/js/French.json"
                },
                "paging": false,
                "searching": false,
                "info": false,
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }]
            });
        });
    </script>
</body>

</html>