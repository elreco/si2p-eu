 <?php
 
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");
	
	include 'modeles/mod_droit.php';
	include 'modeles/mod_get_client_produits.php';
	include 'modeles/mod_get_commerciaux.php';
	
	
	// INSCRIPTION D'UN STAGIAIRE A UNE FORMATION
	
	$erreur_txt="";
	
	$action_id=0;
	if(isset($_GET["action"])){
		if(!empty($_GET["action"])){
			$action_id=intval($_GET["action"]);
		}	
	}
	
	if(empty($action_id)){
		echo("Formulaire incomplet!");
		die();
	}
	
	// CONTROLE
	
	if(empty($erreur_txt)){
		
		// info sur l'action
		
		$sql="SELECT act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,act_produit,act_agence,act_gest_sta FROM Actions WHERE act_id=:action_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$d_action=$req->fetch();
		if(empty($d_action)){
			$erreur_txt="Impossible de charger les données";
		}else{
			
		}
	}
	
	if(empty($erreur_txt)){
		
		$action_client_id=0;
		if(isset($_GET["action_client"])){
			if(!empty($_GET["action_client"])){
				$action_client_id=intval($_GET["action_client"]);
			}	
		}
		
		$session_id=0;
		if(isset($_GET["session"])){
			if(!empty($_GET["session"])){
				$session_id=intval($_GET["session"]);
			}	
		}

		// sur la personne connecte
		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];	
		}
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];	
		}

		// ATTESTATIONS
		
		$sql="SELECT att_id,att_titre FROM Attestations WHERE att_statut=1 AND att_famille=" . $d_action["act_pro_famille"] . " AND att_sous_famille=" . $d_action["act_pro_sous_famille"] . "
		AND att_sous_sous_famille=" . $d_action["act_pro_sous_sous_famille"] . " ORDER BY att_titre;";
		$req=$Conn->query($sql);
		$d_attestations=$req->fetchAll();
		
		// LES SESSIONS ET CLIENTS
		
		
		
		if($d_action["act_gest_sta"]==2){
			
			$sql="SELECT DATE_FORMAT(pda_date,'%d/%m/%Y') AS pda_date,pda_demi,ase_id,ase_h_deb,ase_h_fin,int_label_1
			FROM Plannings_Dates INNER JOIN Actions_Sessions ON (Plannings_Dates.pda_id=ase_date)
			INNER JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)
			WHERE pda_type=1 AND pda_ref_1=:action;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action",$action_id);
			$req->execute();
			$d_sessions=$req->fetchAll();
			
			
			if(!empty($session_id)){
				
				// les client qui participe à la session
				
				$sql="SELECT acl_id,acl_attestation,cli_id,cli_code,cli_nom 
				FROM Clients INNER JOIN Actions_Clients ON (Clients.cli_id=Actions_Clients.acl_client)
				INNER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
				INNER JOIN Actions_Sessions ON (Actions_Sessions.ase_date=Actions_Clients_Dates.acd_date)
				WHERE ase_id=:session_id;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":session_id",$session_id);
				$req->execute();
				$d_clients=$req->fetchAll();
				
			}
		}else{
			
			// les client qui participe à la formation
				
			$sql="SELECT acl_id,acl_attestation,cli_id,cli_code,cli_nom 
			FROM Clients INNER JOIN Actions_Clients ON (Clients.cli_id=Actions_Clients.acl_client)
			WHERE acl_action=:action_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_id",$action_id);
			$d_clients=$req->fetchAll();
			
		}
		
		

	}

	if(empty($erreur_txt)){
?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<title>SI2P - Orion</title>
				<meta name="keywords" content=""/>
				<meta name="description" content="">
				<meta name="author" content="">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<!-- CSS THEME -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
				<link href="assets/admin-tools/admin-forms/css/admin-forms.css" rel="stylesheet" type="text/css">

				<!-- CSS PLUGINS -->
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
				
				<!-- CSS Si2P -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
				
				<!-- Favicon -->
				<link rel="shortcut icon" href="assets/img/favicon.png">
			   
				<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
				<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
			</head>
			<body class="sb-top sb-top-sm ">
				<form method="post" action="action_sta_cree_enr.php" id="form" >
					<div>
						<input type="hidden" name="action" value="<?=$action_id?>" />
					</div>
					<div id="main">
			<?php		include "includes/header_def.inc.php"; ?>
						<section id="content_wrapper">
							

							<section id="content" class="animated">
							
								<div class="row" >
							
									<div class="col-md-10 col-md-offset-1">
										
										<div class="admin-form theme-primary ">												
											
											<div class="panel heading-border panel-primary">
											
												<div class="panel-body bg-light">

													<div class="content-header mtn mbn">
												<?php	if($action_client_id>0){
															echo("<h2>Action N°". $action_id . " - <b class='text-primary' >Modification client</b></h2>");
														}else{
															echo("<h2>Action N°". $action_id . " - <b class='text-primary' >Nouveau client</b></h2>");
														} ?>														
													</div>
											<?php	if(isset($d_sessions)){ ?>
														<div class="row" >
															<div class="col-md-12" >
																<label for="sta_session" >Session :</label>
																<select name="sta_session" id="sta_session" class="select2" >
																	<option value="0">Session ...</option>
															<?php	foreach($d_sessions as $session){
																		$horaire="";
																		if(!empty($session["ase_h_deb"])){
																			$horaire=$session["ase_h_deb"] . " / " . $session["ase_h_fin"];
																		}elseif($session["pda_demi"]==1){
																			$horaire="matin";
																		}else{
																			$horaire="après-midi";
																		}
																		
																		if($session["ase_id"]==$session_id){
																			echo("<option value='" . $session["ase_id"] . "' selected >" . $session["int_label_1"] . " " . $session["pda_date"] . " " . $horaire . "</option>");
																		}else{
																			echo("<option value='" . $session["ase_id"] . "' >" . $session["int_label_1"] . " " . $session["pda_date"] . " " . $horaire . "</option>");
																		}
																	} ?>
																</select>
															</div>
														</div>
											<?php	} ?>
													<div class="row mt15" >
														<div class="col-md-12" >
															<label for="sta_session" >Client :</label>
															<select name="sta_action_client" id="sta_action_client" class="select2" >
																<option value="0">Client ...</option>
														<?php	$client_id=0;
																$attestation_id=0;
																if(isset($d_clients)){
																	foreach($d_clients as $cli){
																		if($cli["acl_id"]==$action_client_id){
																			$client_id=$cli["cli_id"];
																			$attestation_id=$cli["acl_attestation"];
																			echo("<option value='" . $cli["acl_id"] . "' selected >" . $cli["cli_code"] . " " . $cli["cli_nom"] . "</option>");
																		}else{
																			echo("<option value='" . $cli["acl_id"] . "' >" . $cli["cli_code"] . " " . $cli["cli_nom"] . "</option>");
																		}
																	}
																}	?>
															</select>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="section-divider mb40">
																<span>Stagiaire déjà enregistré</span>
															</div>
														</div>
													</div>

													<div class="row" >
														<div class="col-md-12" >
															<label for="sta_id" class="sr-only" >Stagiaires déjà enregistrés dans la base:</label>
															<select name="sta_id" id="sta_id" class="select2-stagiaire" data-client="<?=$client_id?>" >
																<option value="0">Stagiaire ...</option>													
															</select>
														</div>
													</div>
													
													<div class="row">
														<div class="col-md-12">
															<div class="section-divider mb40">
																<span>Nouveau Stagiaire</span>
															</div>
														</div>
													</div>
													<div class="row" >
														<div class="col-md-4" >
															<label for="sta_nom" >Nom :</label>
															<input type="text" name="sta_nom" class="gui-input nom" id="sta_nom" placeholder="Nom" />
														</div>
														<div class="col-md-4" >
															<label for="sta_prenom" >Prénom :</label>
															<input type="text" name="sta_prenom" class="gui-input prenom" id="sta_prenom" placeholder="Prénom" />
														</div>
														<div class="col-md-4" >
															<label for="sta_naissance" >Date de naissance</label>
															<span  class="field prepend-icon">
																<input type="text" id="sta_naissance" name="sta_naissance" class="gui-input datepicker" placeholder="Date de naissance" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>
													
													<div class="row mt15" >
														<div class="col-md-12" >
															<label for="sta_attestation" >Attestation :</label>
															<select name="sta_attestation" id="sta_attestation" class="select2" >
																<option value="0">Attestation ...</option>
														<?php	if(!empty($d_attestations)){
																	foreach($d_attestations as $att){
																		if($att["att_id"]==$attestation_id){
																			echo("<option value='" . $att["att_id"] . "' selected >" . $att["att_titre"] . "</option>");
																		}else{
																			echo("<option value='" . $att["att_id"] . "' >" . $att["att_titre"] . "</option>");
																		}
																	}
																}	?>	
															</select>
														</div>
													</div>
																									
												
												
												</div>
												<!-- fin panel body -->
											</div>
											<!-- fin panel -->
										</div>
										<!-- fin admin form -->	
									</div>
									
								</div>
									
							</section>
							
						</section>
					</div>
					<footer id="content-footer" class="affix">
						<div class="row">
							<div class="col-xs-3 footer-left">
								<a href="action_voir.php?action=<?=$action_id?>&action_client=<?=$action_client_id?>" class="btn btn-default btn-sm" >
									Retour
								</a>							
							</div>
							<div class="col-xs-6 footer-middle"></div>
							<div class="col-xs-3 footer-right">
								<button type="submit" class="btn btn-sm btn-success" >
									<i class="fa fa-plus" ></i> Enregistrer
								</button>
							</div>
						</div>
					</footer>
				</form>	
			
		<?php	
				include "includes/footer_script.inc.php"; ?>
				<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>	
				<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
				<script type="text/javascript" src="/assets/js/custom.js"></script>

				<script type="text/javascript" >
					var action="<?=$action_id?>";
					var action_client;
					jQuery(document).ready(function() {
						$("#sta_session").change(function(){
							action_client=$("#sta_action_client").val();
							if($(this).val()>0){
								get_action_clients(action,$(this).val(),actualiser_select2,"#sta_action_client",action_client);
							}else{
								actualiser_select2("#sta_action_client","");
							}							
						});
						$("#sta_action_client").change(function(){
							action_client=$(this).select2("val");
							if(action_client>0){
								alert("action_client " + action_client);
								get_action_client(action_client,actualiser_info_client,"","");
							}else{
								actualiser_info_client("");
							}
						});
						
					});
					(jQuery);
					
					function actualiser_info_client(json){
						if(json){
							$("#sta_id").data("client",json.acl_client);
							$("#sta_attestation").val(json.acl_acl_attestation);
						}else{
							$("#sta_id").data("client",0);
							$("#sta_attestation").val(0);
						}
						
					}
					
				</script>
			</body>
		</html>
<?php
	}else{
		echo($erreur_txt);
	}	?>