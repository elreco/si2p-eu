<?php 

// AFFICHE LES FACTURES D'UN CLIENT SUR L'ENSEMBLE DU RESEAU
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


// DONNEE UTILE AU PROGRAMME

// personne connecté
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	} 
}
$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}
$n=0;
if(isset($_GET["n"])){
	if(!empty($_GET["n"])){
		$n=intval($_GET["n"]);
	}
}
$utilisateur=0;
if(isset($_GET["utilisateur"])){
	if(!empty($_GET["utilisateur"])){
		$utilisateur=intval($_GET["utilisateur"]);
	}
}
// CONTROLE ACCES

if( ($_SESSION['acces']["acc_profil"]!=10 AND $_SESSION['acces']["acc_profil"]!=13 AND $acc_utilisateur!=372) OR empty($exercice) OR empty($utilisateur) ){
	$erreur_txt="Impossible d'afficher la page!";
	
}else{
	
	$sql="SELECT uti_nom,uti_prenom,uti_societe FROM Utilisateurs WHERE uti_id=" . $utilisateur . ";";
	$req=$Conn->query($sql);
	$d_utilisateur=$req->fetch();
	
	$an_deb=intval($exercice-$n);
	$an_fin=intval($exercice-$n+1);
	
	
	
	// CLIENTS FACTURES SUR LA SOC DE L'UTILISATEUR
	
	$ConnFct=connexion_fct($d_utilisateur["uti_societe"]);
	
	$sql="SELECT cli_code,cli_nom,cli_categorie,cli_interco_soc,fac_client,SUM(fli_montant_ht) AS ht 
	FROM factures 
	INNER JOIN Factures_Lignes ON (factures.fac_id=Factures_Lignes.fli_facture)
	INNER JOIN Commerciaux ON (factures.fac_commercial=Commerciaux.com_id)
	LEFT JOIN Clients ON (factures.fac_client=clients.cli_id AND factures.fac_agence=clients.cli_agence)
	WHERE com_type=1 AND com_ref_1=" . $utilisateur . " AND fac_date>='" . $an_deb . "-04-01' AND fac_date<='" . $an_fin . "-03-31'
	AND fli_categorie<4
	GROUP BY fac_client,cli_code,cli_nom,fac_client,cli_categorie,cli_interco_soc ORDER BY cli_code,cli_nom,fac_client,cli_categorie,cli_interco_soc;";
	$req=$ConnFct->query($sql);
	$d_clients=$req->fetchAll();
}
?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">

		<?php 			if(!empty($erreur_txt)){ ?>
							<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-danger" style="border-radius:0px;"><?=$erreur_txt?></div>
							</div>
		<?php			}else{

							if(!empty($d_clients)){ 
								echo("<h1>" . count($d_clients) . " clients facturés par " . $d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"] . "</h1>");
								echo("<h2>Sur l'exercice " . $an_deb . "/" . $an_fin . "</h2>");
								
								?> 
								<div class="table-responsive">
									<table class="table table-striped" >
										<thead>
											<tr class="dark2" >	
												<th>Code</th> 
												<th>Nom</th>
												<th>Catégorie</th>
												<th>Montant facturé</th>																		
											</tr>
										</thead>
										<tbody>
								<?php		$total=0;
											foreach($d_clients as $d_cli){	
												$total=$total + $d_cli['ht']; 
												$cat_lib="";
												if($d_cli["cli_categorie"]==2){
													$cat_lib="GC";
												}elseif($d_cli["fac_client"]==8891){
													$cat_lib="GFC";
												}elseif(!empty($d_cli["cli_interco_soc"])){
													$cat_lib="INTERCO";
												}else{
													$cat_lib="REGION";
												}
												
												?>
												<tr>		
													<td><?= $d_cli['cli_code']?></td>
													<td><?= $d_cli['cli_nom']?></td>		
													<td><?=$cat_lib?></td>	
													<td class="text-right" ><?=	$d_cli['ht']?></td>															
												</tr>
					<?php					}  ?>
											<tr>
												<th class="text-right" colspan="3" >Total :</th>
												<td class="text-right" ><?=	$total?></td>
											</tr>	
										</tbody>										
									</table>
								</div>
			<?php			} 
						}?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="utilisateur_rem.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>" class="btn btn-default btn-sm" role="button">
							<i class="fa fa-left"></i> Retour
						</a>	</div>
					<div class="col-xs-6 footer-middle">&nbsp;</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
			
			});						
		</script>
	</body>
</html>
