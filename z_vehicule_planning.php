<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");

include "modeles/mod_droit.php";

setlocale(LC_TIME, "fr");

	// sur la personne connecte
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];	
	}	

	// LES PARAMETRES
	$vehicule=0;
	if(isset($_GET["vehicule"])){
		$vehicule=$_GET["vehicule"];	
	}	

	if($vehicule==0){
		Header("Location: vehicule_liste.php");
		exit();
	}
	$mois=0;				
	if(isset($_GET["mois"])){
		$mois=$_GET["mois"];
	}else{
		$mois=date("n");
	}
	if(isset($_GET["annee"])){
		$annee=$_GET["annee"];
	}else{
		$annee=date("Y");
	}
	

	$jour_deb=date("N", mktime(0, 0, 0, $mois, 1, $annee));	
	$deb_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1), $annee);
	$date_deb=date_create(date("Y-m-d",$deb_mk));						
	$semaine_deb=$date_deb->format('W');
	
	
	$jour_fin=date("N", mktime(0, 0, 0, $mois+1, 0, $annee));	
	$fin_mk=mktime(0, 0, 0, $mois+1, 5-$jour_fin, $annee);
	$date_fin=date_create(date("Y-m-d",$fin_mk));
	$semaine_fin=$date_fin->format('W');
	
	echo($deb_mk . "<br/>");
	echo($fin_mk . "<br/>");
	
	$time_mois_suiv=mktime(0, 0, 0, $mois+1, 1, $annee);		
	$mois_suiv=date("n",$time_mois_suiv);
	$annee_suiv=date("Y",$time_mois_suiv);
	
	$time_mois_prec=mktime(0, 0, 0, $mois-1, 1, $annee);	
	$mois_prec=date("n",$time_mois_prec);
	$annee_prec=date("Y",$time_mois_prec);
	
	// ON IDENTIFIE LES DATES OU LE VEHICULE EST UITILISE PAR LA SOCIETE PROPRIETAIRE
	
	
		
	
	$planning=array();
	$ligne=array();
	$ligne_soc=0;
	$ligne_age=0;
	$num_ligne=0;
	
	$sql="SELECT soc_id,soc_code,age_id,age_code FROM ";
	
	$sql.="(";
	$sql.="(";
	
	$sql.="Societes LEFT OUTER JOIN Agences ON (Societes.soc_id=Agences.age_societe)";
	$sql.=")";
	
	$sql.=" LEFT OUTER JOIN vehicules_plannings";
	$sql.=" ON (";
		$sql.="vehicules_plannings.vpa_societe=Societes.soc_id AND vehicules_plannings.vpa_agence=Agences.age_id";
		$sql.=" AND vehicules_plannings.vpa_vehicule=" . $vehicule;
		$sql.=" AND vehicules_plannings.vpa_date>=" . $date_deb->format('Y-m-d') . " AND vehicules_plannings.vpa_date<=" . $date_fin->format('Y-m-d');
	$sql.=")";
	$sql.=")";
	$sql.=" ORDER BY soc_code,age_code";
	//echo($sql);
	$req = $Conn->query($sql);
	$result = $req->fetchAll();
	if(!empty($result)){
		foreach($result as $r)
		{	
			$societe=$r["soc_id"];
			$label=$r["soc_code"];
			
			$agence=0;
			if(!empty($r["age_id"])){
				$agence=$r["age_id"];
				$label.=" / " .	$r["age_code"];			
			}
			
			if($ligne_soc=$societe OR $ligne_age=$agence)
			{ 	
				$ligne_soc=$societe;
				$ligne_age=$agence;
				$num_ligne++;
				$ligne[$num_ligne]=array(
					"societe" => $ligne_soc,
					"agence" => $ligne_age,
					"label" => $label
				);
			}
			if(!empty($r["pda_date"])){
				$planning[$societe][$agence][$r["vpa_date"]][$r["vpa_demi"]]=array(
					"type" => 0,
					"ref" => $r["vpa_ref"],
					"texte" => $r["vpa_texte"],
					"style" => $r["vpa_style"]
					
				);
			}
		}										
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" >
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		
	</head>

	<body class="sb-top sb-top-sm" >
		<!-- Start: Main -->
		<div id="main">
			<?php
				include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

			
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn">
				
					<div class="row" >
						<div class="col-md-2 text-center" >
							<a href="planning.php?mois=<?=$mois_prec?>&annee=<?=$annee_prec?>" class="btn btn-default btn-sm" role="button" >
								<i class="fa fa-arrow-left"></i>					
								<?=strftime("%B %Y",$time_mois_prec)?>
							</a>
						</div>
						<div class="col-md-8 text-center" >
							<h1><?=strftime("%B %Y",mktime(0, 0, 0, $mois, 1, $annee))?></h1>
						</div>
						<div class="col-md-2 text-center" >
							<a href="planning.php?mois=<?=$mois_suiv?>&annee=<?=$annee_suiv?>" class="btn btn-default btn-sm" role="button" >											
								<?=strftime("%B %Y",$time_mois_suiv)?>
								<i class="fa fa-arrow-right"></i>	
							</a>
						</div>
					</div>
						
				<?php	
					for($bcl_semaine=$semaine_deb; $bcl_semaine<=$semaine_fin; $bcl_semaine++){ ?>
						<div class="row mt15 ml15 mr15 bg-dark"  >						
							<div class="col-sm-1" >
								Semaine <?=$bcl_semaine?>
							</div>
							<div class="col-sm-11" >						
								<div class="row" >								
						<?php		$date_deb->sub(new DateInterval('P1D'));
									for($j=0;$j<=5;$j++){
										$date_deb=$date_deb->add(new DateInterval('P1D')); 
										$date_deb->setTimezone(new DateTimeZone('Europe/Paris')); ?>
										<div class="col-sm-2 text-center" >
											<?php
												$date_f_fr=$date_deb->getTimestamp();
												echo(strftime("%A %d",$date_f_fr)); ?>
										</div>
						<?php		} ?>									
								</div>	
							
								<div class="row " >	
						<?php		for ($m = 0; $m <= 5; $m++){ ?>
										<div class="col-sm-1 text-center" >
											Matin
										</div>
										<div class="col-sm-1 text-center" >
											Après-Midi
										</div>
						<?php		}; ?>								
								</div>
							</div>
						</div>
			<?php		for($nbligne=1;$nbligne<=$num_ligne;$nbligne++){
							$date_deb->sub(new DateInterval('P6D')); 
							$societe=$ligne[$num_ligne]["societe"]; 
							$agence=$ligne[$num_ligne]["agence"]; ?>
							
							<div class="row ml15 mr15" >					
								<div class="col-sm-1 planning_intervenant" >
									<?=$ligne[$nbligne]["label"]?>
								</div> 
								<div class="col-sm-11" >
									<div class="row" >
							<?php		
										
										for($j=0;$j<=5;$j++){
											
											$date_deb=$date_deb->add(new DateInterval('P1D'));
											$date_lib=$date_deb->format('Y-m-d');
											
											
											for($d=1;$d<=2;$d++){
												
												$case_id=$societe . "_" . $agence . "_" . $date_lib . "_" . $d;
												
												if(!empty($planning[$societe][$agence][$date_lib][$d])){
													$case=$planning[$societe][$agence][$date_lib][$d];
													$class="";
													$class_case="";
													switch ($case["type"]) {
														case 0:
															$class="";
															break;
														case 1:
															$class=" action action_" . $bcl_semaine . "_" . $int_id . "_" . $case["ref"];
															break;
														case 3:
															$class_case=" case_info click";
															break;
													} ?>
													<div class="col-sm-1 planning_case<?=$class_case?>" id="case_<?=$case_id?>" >
													
														<div class="drag<?=$class?>" style="<?=$case["style"]?>" id="contenu_<?=$case_id?>" >
															<?=$case["texte"]?>
														</div>
														
													</div>
								<?php			}else{ ?>
													<div class="col-sm-1 planning_case libre click" id="case_<?=$case_id?>" >&nbsp;</div>										
								<?php			}
											}
										}?>
												
									</div>
								</div>
							</div>						
		<?php				// fin boucle intervenant
						}
						$date_deb->add(new DateInterval('P2D')); 
					} ?>
				</section>
				<!-- End: Content -->
			</section>
			
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<!--<a href="#" class="btn btn-success btn-sm" role="button" >
						<span class="fa fa-plus"></span>
						Nouvelle action
					</a>-->
				</div>
			</div>
		</footer>
		

<?php
		$req=$Conn -> query("SELECT pca_id,pca_libelle,pca_couleur FROM Plannings_Cases WHERE pca_type=1 ORDER BY pca_libelle;");
		$case = $req->fetchAll();
		if(!empty($case)){ ?>
			<div id="context-add" class="context-menu" >		
				<ul class="dropdown-menu " >
<?php				foreach($case as $c){ 
						echo("<li>");
							echo("<a href='#' class='add-case' data-case='" . $c["pca_id"] . "' >");
								echo ("<span class='badge' style='background-color:" . $c["pca_couleur"] . "' >&nbsp;</span>");
								echo ($c["pca_libelle"]);									
							echo("</a>");
						echo("</li>");							
					} ?>
				</ul>
			</div>						
<?php	} ?>						
				
		<div id="modal-confirme" class="modal fade" role="dialog" >
			<div class="modal-dialog">			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
						<p id="modal-text" ></p>
						<p>Etes-vous sur de vouloir continuer ?</p>
					</div>
					<div class="modal-footer">
						<button type="button" id="confirme_oui" class="btn btn-success btn-sm" data-dismiss="modal" data-confirmation=""  >Oui</button>
						<button type="button" id="confirme_non" class="btn btn-default btn-sm" data-dismiss="modal">Non</button>
					</div>
				</div>
			</div>
		</div>
		

		<?php
		include "includes/footer_script.inc.php"; ?>	
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" >
		
		var elt_source;
		
		$(document).ready( function() {
			// MENU CONTEXTUEL
			
			// gestion du clique droit et affichage du menu contextuel associé		
			$(document).on("contextmenu",".planning_case",function(e){
				
				if(e.pageX/$(document).width()>0.75){
					$(".dropdown-submenu").addClass( "pull-left");
				}else{
					$(".dropdown-submenu").removeClass( "pull-left");
				}
				
				if($(this).hasClass("libre")&&$(this).hasClass("case_select")){
					
					$("#context-add").css({top:e.pageY,left:e.pageX}).show();
					
				}else if($(this).hasClass( "case_info" )&&$(this).hasClass("case_select")){
					
					$("#context-info").css({top:e.pageY,left:e.pageX}).show();
				}
				// desactive le comportement par défaut du clique droit
				e.preventDefault();
				return false;
			});
			
			// masque les menucontextuels
			$(document).on("contextmenu click","*:not(.planning_case)",function(e){
				$(".context-menu").hide();
				//e.preventDefault();
				//return false;
			});
			
			
			// ini des fonctions de traitement liées au menu contextuel		
			$(".add-case").click(function(){
				ajouter_case($(this));
				$(".context-menu").hide();	
			});				
			$(".supp-info").click(function(){
				
				$("#modal-confirme #modal-text").text("Vous êtes sur le point d'éffacer le contenu des cases sélectionnées");
				$('#modal-confirme').modal('show');
				
				//$("#confirme_oui").off() si plusieur utilisation du modal confirme			
				$("#confirme_oui").click(function(){
					supprimer_info();
				});
				
				$(".context-menu").hide();	
			});
			// FIN MENU CONTEXTUEL
			
			// initialisation des event en fonction des classes affectées au case
			case_ini();
			
		});
		
		// OPERATION SUR LES CASES DU PLANNING	
		// vide une case pour la rendre disponible		
		function case_vide(case_id){
			$("#contenu_" + case_id).remove();
			$("#case_" + case_id).html("&nbsp;");
			// la case devient cliquable
			$("#case_" + case_id).addClass("libre");
			$("#case_" + case_id).addClass("click");			
			
		}
		// insertion de donnee dans une case du planning
		function case_donnee(case_id,case_contenu,case_type){
			console.log("case_type : " + case_type);
			$("#case_" + case_id).html(case_contenu);
			$("#case_" + case_id).removeClass("case_select");
			$("#case_" + case_id).removeClass("libre");
			if(case_type==3){
				$("#case_" + case_id).addClass("case_info");
				$("#case_" + case_id).addClass("click");
			}			
			$("#case_" + case_id).off(); // annule les events affecte
		}		
		// reinitialise les event lié au case en fonction des classes (evite de recharger)
		function case_ini(){	
		
			$(".click").off();	
			$(".click").click(function(){
				selectionner_case($(this));
			});		
			$(".libre").droppable({
				drop : function(event,ui){							
					var id_source=elt_source.replace("contenu_","");
					var id_cible=(this).id.replace("case_","");
					deplacer_case(id_source,id_cible);
				}
			});	
			$('.drag').draggable({
				snap : ".drag",
				revert : 'invalid', // renvoi les bloc s'il ne sont pas depose en zone drop
				cursor : "move",
				zIndex: 9999,
				start : function(){
					elt_source=(this).id;		
				}
			});
		}
		//****************************************
	
		// TRAITEMENT DES INTERACTION UTILISATEUR
		
		// select ou deselection d'une case
		function selectionner_case(elt){	
		
			console.log("selectionner_case()");
		
			if(elt.hasClass( "case_select" )){
				// annule select
				elt.removeClass("case_select");	
				if($(".case_select").length==0){
					case_ini();
				}							
			}else{
				// select
				if($(".case_select").length==0){
					if(elt.hasClass( "case_info" )){
						$(".libre").off(); // .libre ne sont plus select	
					}else{
						$(".case_info").off(); // .info ne sont plus select	
					}
				}
				elt.addClass("case_select");	
			}
		}
		
		// ajouter une ou plusieur case
		function ajouter_case(e){			
			var type_case=e.data("case");
			var liste_case="";
			var ident_case="";
			$(".case_select").each(function(){ 
				ident_case=(this).id.replace("case_", "");
				liste_case=liste_case +  ident_case + ",";
			});			
			$.ajax({			
				type:'GET',
				url: 'ajax/ajax_add_case.php',
				data : 'type_case=' + type_case + "&liste_case=" + liste_case, 
				dataType: 'json',                
				success: function(data)
				{	if(data.length<3){
						console.log("ERREUR DATA");
					}else{
						var case_texte=data[0];		
						var case_couleur=data[1];	
						var case_html="";
						var case_id="";
						var i=0;
						for(i==3;i<=data.length;i++){
							case_id=data[i];
							if($("#case_" + case_id).hasClass("case_select")){
								
								case_contenu="<div class='drag' style='background-color:" + case_couleur + ";' id='contenu_" + case_id + "' >";
								case_contenu+=case_texte;
								case_contenu+="</div>";	
								
								case_donnee(case_id,case_contenu,3);															
							}						
						}
						case_ini();		
					}
					
				},
				error: function() {
					console.log("ERREUR AJAX");		
				}
			});
		};
		
		// déplace une case
		function deplacer_case(id_source,id_cible){		
			var case_texte=$("#contenu_" + id_source).text();
			var case_bg=$("#contenu_" + id_source).css("background-color");
			
			var case_type=0;
			if($("#case_" + id_source).hasClass( "case_info" )){
				case_type=3;					
			}
			
			$.ajax({			
				type:'GET',
				url: 'ajax/ajax_move_case.php',
				data : 'id_source=' + id_source + "&id_cible=" + id_cible, 
				dataType: 'html',                
				success: function()
				{	
					case_contenu="<div class='drag' style='background-color:" + case_bg + ";' id='contenu_" + id_cible + "' >";
					case_contenu+=case_texte;
					case_contenu+="</div>";	
					
					case_vide(id_source);	
					case_donnee(id_cible,case_contenu,case_type);
					case_ini();
										
				},
				error: function() {
					console.log("ERREUR");		
				}
			});
		}
		
		function supprimer_info(){
			console.log("supprimer_info()");
			var liste_case="";
			var ident_case="";
			$(".case_select").each(function(){ 
				ident_case=(this).id.replace("case_", "");
				liste_case=liste_case +  ident_case + ",";
			});			
			$.ajax({			
				type:'GET',
				url: 'ajax/ajax_supp_case.php',
				data : "&liste_case=" + liste_case, 
				dataType: 'html',                
				success: function()
				{	$(".case_select").each(function(){ 
						ident_case=(this).id.replace("case_", "");
						$(this).removeClass("case_select");
						$(this).removeClass("case_info");	
						case_vide(ident_case);	
											
					});
					case_ini();					
				},
				error: function() {
					console.log("ERREUR AJAX");		
				}
			});			
		}
  
		function changer_produit(){
			
			console.log("changer_produit()");
			
			var client=$("#client").val();
			var type=0;
			if($("#produit_intra").is(':checked')){
				type=1;	
			}else if($("#produit_inter").is(':checked')){
				type=2;	
			};
			$('#produit option').remove();
					
			$.ajax({			
				type:'GET',
				url: 'ajax/ajax_produit.php',
				data : 'client=' + client + "&type=" + type, 
				dataType: 'json',                
				success: function(data)
				{	if(data.length>0){
						$.each(data, function(key, value){ 
							$('#produit').append($("<option></option>").attr("value",value[0]).text(value[1]));
						});
					}
					//$("#produit").select2();
				},
				error: function() {
					console.log("ERREUR AJAX");		
				}
			});
		}
		</script>
	</body>
</html>