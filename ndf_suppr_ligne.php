<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');

//////////////////////////////////////////////////////////////
///////////// SI C'EST LE DEMANDEUR QUI VALIDE ///////////////
//////////////////////////////////////////////////////////////

if(isset($_GET['id'])){


    // changer le statut de la ndf et mettre à jour la date de validation
    $req = $Conn->prepare("SELECT nli_ndf FROM ndf_lignes WHERE nli_id = :nli_id");
    $req->bindParam(':nli_id', $_GET['id']);
    $req->execute();
    $nli = $req->fetch();

    $req = $Conn->prepare("DELETE FROM ndf_lignes WHERE nli_id = :nli_id");
    $req->bindParam(':nli_id', $_GET['id']);
    $req->execute();

    $req = $Conn->prepare("SELECT SUM(nli_ttc) FROM ndf_lignes WHERE nli_ndf = " . $nli['nli_ndf']);
    $req->execute();
    $ndf_ttc = $req->fetch();

    $req = $Conn->prepare("UPDATE ndf SET ndf_ttc = :ndf_ttc WHERE ndf_id = :ndf_id");
    $req->bindValue(':ndf_ttc', $ndf_ttc['SUM(nli_ttc)']);
    $req->bindValue(':ndf_id',$nli['nli_ndf']);
    $req->execute();



    $_SESSION['message'][] = array(
		"titre" => "Note de frais",
		"type" => "success",
		"message" => "La ligne a été supprimée."
	);

	// fin changer le statut de la ndf et mettre à jour la date de validation

    Header("Location: ndf.php?id=" . $_GET['ndf']);
    die();
}