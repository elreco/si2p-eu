<?php

// SELECTION DES FACTURES

$menu_actif = "1-7";
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';

if(isset($_SESSION['cli_tri'])){ 
    unset($_SESSION['cli_tri']);
}
// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];    
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
    $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];  
}

if(isset($_SESSION['facture_tableau'])){
	unset($_SESSION['facture_tableau']);
}
$_SESSION["retourFacture"]="facture_tri.php";

// LES AGENCES
if($acc_agence==0){
	$sql="SELECT age_id,age_code FROM Agences LEFT JOIN Societes ON (Agences.age_societe=Societes.soc_id) 
	WHERE soc_id=" . $acc_societe. " AND soc_agence ORDER BY age_code";
	$req = $Conn->query($sql);
	$d_agences=$req->fetchAll();
	if(empty($d_agences)){
		unset($d_agences);
	}
}

// LES COMMERCIAUX
$sql="SELECT com_id,com_label_1,com_label_2 FROM commerciaux";
$mil="";
if($_SESSION['acces']['acc_profil'] == 3 && !$_SESSION['acces']["acc_droits"][6]){
	$mil=" AND com_ref_1 = " . $_SESSION['acces']['acc_ref_id'];
}
if($acc_agence>0){
	$mil.=" AND com_agence= " . $acc_agence;
}
if($mil!=""){
	$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
}
$sql.=" ORDER BY com_label_1,com_label_2";
$req = $ConnSoc->query($sql);
$d_commercial=$req->fetchAll();

// LES CATEGORIES DE CLIENTS
$sql="SELECT cca_id,cca_libelle FROM Clients_Categories ORDER BY cca_libelle;";
$req = $Conn->query($sql);
$d_categories=$req->fetchAll();

// PERIODE PAR DEFAUT
$date = new DateTime();
$dateDeb = $date -> format('01/m/Y');
$dateFin = $date -> format('t/m/Y');

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Recherche factures</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="facture_liste.php" id="formulaire" >			
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">											
											<div class="content-header">
												<h2>Recherche de <b class="text-primary title-suscli">Factures</b></h2>
											</div>							
											<div class="col-md-10 col-md-offset-1">											
												
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="fac_date_deb" >Facturé entre le</label>
															<span  class="field prepend-icon">
																<input type="text" id="fac_date_deb" name="fac_date_deb" class="gui-input datepicker" placeholder="Facturé entre le" value="<?=$dateDeb?>" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">															
															<label for="fac_date_fin" >et le</label>
															<span  class="field prepend-icon">
																<input type="text" id="fac_date_fin" name="fac_date_fin" class="gui-input datepicker" placeholder="Et le" value="<?=$dateFin?>" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>
												</div>
												<div class="row">
											<?php	if(isset($d_agences)){ ?>
														<div class="col-md-6">
															<div class="section">
																<label for="fac_agence" >Agence</label>
																<select name="fac_agence" id="fac_agence" class="select2" >
																	<option value="">Agence...</option>
														<?php 		if(!empty($d_agences)){
																		foreach($d_agences as $age){
																			echo("<option value='" . $age["age_id"] . "' >" . $age["age_code"] . "</option>");
																		}
																	} ?>
																</select>																
															</div>														
														</div>
											<?php	} ?>
													
													<div class="col-md-6">
											<?php 		if(count($d_commercial)>1){ ?>
															<div class="section">
																<label for="fac_commercial" >Commercial</label>
																<select name="fac_commercial" id="fac_commercial" class="select2" >
																	<option value="0">Commercial...</option>
														<?php 		if(!empty($d_commercial)){
																		foreach($d_commercial as $com){
																			echo("<option value='" . $com["com_id"] . "' >" . $com["com_label_1"] . " "  . $com["com_label_2"] . "</option>");
																		}
																	} ?>
																</select>																
															</div>														
												<?php 	} ?>
													</div>
													<div class="col-md-6">
														<div class="section">
															<label for="fac_chrono" >Numéro de chrono</label>
															<input type="text" name="fac_chrono" id="fac_chrono" class="gui-input" placeholder="Numéro de chrono" />
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Client</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="fac_cli_categorie" >Catégorie</label>
															<select name="fac_cli_categorie" id="fac_cli_categorie" class="select2" >
																<option value="0">Catégorie ...</option>
													<?php 		if(!empty($d_categories)){
																	foreach($d_categories as $cat){
																		echo("<option value='" . $cat["cca_id"] . "' >" . $cat["cca_libelle"] . "</option>");
																	}
																} ?>
															</select>
														</div>
													</div>
													<div class="col-md-6" id="sous_categorie" style="display:none;" >
														<div class="section">
															<label for="fac_cli_s_categorie" >Sous-catégorie</label>
															<select name="fac_cli_s_categorie" id="fac_cli_s_categorie" class="select2" >
																<option value="0">Sous-catégorie ...</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="fac_client" >Client</label>
															<select name="fac_client" id="fac_client" class="select2-client-n" >
																<option value="0">Client...</option>
															</select>
														</div>
													</div>
													<div class="col-md-6 pt25 text-center">
														<div class="checkbox-custom">
															<input type="checkbox" name="fac_filiale" id="fac_filiale" value="on" />
															<label for="fac_filiale">et filiale</label>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Règlement</span>
														</div>
													</div>
												</div>				
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="fac_echeance_deb" >Echéance entre le</label>
															<span  class="field prepend-icon">
																<input type="text" id="fac_echeance_deb" name="fac_echeance_deb" class="gui-input datepicker" placeholder="Echéance entre le" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">															
															<label for="fac_echeance_fin" >et le</label>
															<span  class="field prepend-icon">
																<input type="text" id="fac_echeance_fin" name="fac_echeance_fin" class="gui-input datepicker" placeholder="Et le" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>
												</div>
												<div class="row">
											<?php	if($_SESSION["acces"]["acc_droits"][29]){ ?>												
														<div class="col-md-3 mt15" >															
															<div class="option-group field">
																<label class="option option-dark">
																	<input type="checkbox" class="option_recherche opt1" name="option_recherche" value="1">
																	<span class="checkbox"></span>Facture à faire
																</label>
															</div>
															
														</div>
											<?php		if($acc_societe==4 AND $acc_agence==4 AND $_SESSION["acces"]["acc_droits"][8]){ ?>
															<div class="col-md-3 mt15" >
																<div class="option-group field">
																	<label class="option option-dark">
																		<input type="checkbox" class="option_recherche opt7" name="option_recherche" value="7">
																		<span class="checkbox"></span>Facture groupée à faire
																	</label>
																</div>	
															</div>
											<?php		}
													} ?>
													<div class="col-md-3 mt15">
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox"  class="option_recherche opt2" name="option_recherche" value="2" />
																<span class="checkbox" ></span> Factures dues
															</label>
														</div>
													</div>
													<div class="col-md-3 mt15">
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox"  class="option_recherche opt3" name="option_recherche" value="3" />
																<span class="checkbox" ></span> Factures échues
															</label>
														</div>
													</div>
											<?php	if($_SESSION["acces"]["acc_droits"][9] OR $acc_utilisateur==182){ ?>
														<div class="col-md-3 mt15">
															<div class="option-group field">
																<label class="option option-dark">
																	<input type="checkbox"  class="option_recherche opt4" name="option_recherche" value="4" />
																	<span class="checkbox" ></span> Poste client dû
																</label>
															</div>
														</div>
														<div class="col-md-3 mt15">
															<div class="option-group field">
																<label class="option option-dark">
																	<input type="checkbox"  class="option_recherche opt5" name="option_recherche" value="5" />
																	<span class="checkbox" ></span> Poste client échu
																</label>
															</div>
														</div>
														<div class="col-md-3 mt15">
															<div class="option-group field">
																<label class="option option-dark">
																	<input type="checkbox"  class="option_recherche opt6" name="option_recherche" value="6" />
																	<span class="checkbox" ></span> Avoirs non-lettrés 
																</label>
															</div>
														</div>
											
											<?php	} 
													if($_SESSION["acces"]["acc_service"][2]==1 OR $_SESSION["acces"]["acc_service"][5]==1 OR $_SESSION["acces"]["acc_service"][1]==1 OR $acc_utilisateur==182){ ?>
											
											
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Affichage</span>
																</div>
															</div>
														</div>	
														
														<div class="col-md-3 mt15">
															<div class="option-group field">
																<label class="option option-dark">
																	<input type="checkbox"  class="option_aff opt1" name="option_aff" value="1" />
																	<span class="checkbox" ></span> Afficher les lignes de factures 
																</label>
															</div>
														</div>

											<?php	} ?>		
												</div>												
											</div>																						
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left"></div>
					<div class="col-xs-6 footer-middle text-center">
			<?php		if($_SESSION["acces"]["acc_profil"]==13){ ?>				
							<a href="import/sync_facture.php" class="btn btn-defaut btn-sm" >
								<i class='fa fa-refresh'></i> Import local
							</a>
							<a href="import/sync_facture.php?srv=1" class="btn btn-defaut btn-sm" >
								<i class='fa fa-refresh'></i> Import srv
							</a>
			<?php		} ?>
					</div>
					<div class="col-xs-3 footer-right">
			<?php		if($_SESSION["acces"]["acc_droits"][29]){ ?>
							<a href="facture.php" class="btn btn-sm btn-success" >
								<i class='fa fa-plus'></i> Nouvelle facture
							</a>
			<?php		} ?>
						<button type="submit" name="search" class="btn btn-primary btn-sm">
							<i class='fa fa-search'></i> Rechercher
						</button>
					</div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function (){
				$(".option_recherche").click(function(){
					if($(this).prop("checked")){
						$(".option_aff").prop("checked",false);
						val=$(this).val();
						$(".option_recherche").each(function( index ){
							if($(this).val()!=val){
								$(this).prop("checked",false);
							}
						});
					}
				});
				$(".option_aff").click(function(){
					if($(this).prop("checked")){
						$(".option_recherche").prop("checked",false);
					}
				});
			});
		</script>
	</body>
</html>
