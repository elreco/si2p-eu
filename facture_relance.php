<?php

// AFFICHE LA LISTE DES FACTURES

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');
include('modeles/mod_get_correspondances.php');
include('modeles/mod_orion_utilisateur.php');
// DONNEE UTILE AU PROGRAMME

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}

$facture=0;
if(!empty($_GET['facture'])){
	$facture=intval($_GET['facture']);
}
$societe=0;
if(!empty($_GET['societe'])){
	$societe=intval($_GET['societe']);
}
$client_retour=0;
if(!empty($_GET['client'])){
	$client_retour=intval($_GET['client']);
}
$origine=0;
if(!empty($_GET['origine'])){
	$origine=intval($_GET['origine']);
}
$comment_id=0;
if(!empty($_GET['comment_id'])){
	$origine=intval($_GET['comment_id']);
}

$_SESSION['retourFacture'] = "facture_relance.php?client=" . $client_retour . "&facture=" . $facture . "&societe=" . $societe . "&origine=" . $origine;
$_SESSION['retourFactureRelance'] = "facture_relance.php?client=" . $client_retour . "&facture=" . $facture . "&societe=" . $societe . "&origine=" . $origine;
$ConnFct = connexion_fct($societe);
// RECHERCHER LA FACTURE
$sql="SELECT * FROM Factures WHERE fac_id =" . $facture;
$req = $ConnFct->query($sql);
$d_facture=$req->fetch();
// DONNES FACTURES
$fac_date="";
if(!empty($d_facture['fac_date'])){
    $dt_fac_date=date_create_from_format("Y-m-d",$d_facture['fac_date']);
    $fac_date=$dt_fac_date->format("d/m/Y");
}
$fac_date_reg_prev="";
if(!empty($d_facture['fac_date_reg_prev'])){
    $dt_fac_date_reg_prev=date_create_from_format("Y-m-d",$d_facture['fac_date_reg_prev']);
    $fac_date_reg_prev=$dt_fac_date_reg_prev->format("d/m/Y");
}
$fac_date_reg_rel="";
if(!empty($d_facture['fac_date_reg_rel'])){
    $dt_fac_date_reg_rel=date_create_from_format("Y-m-d",$d_facture['fac_date_reg_rel']);
    $fac_date_reg_rel=$dt_fac_date_reg_rel->format("d/m/Y");
}
$reste_du=$d_facture['fac_total_ttc']-$d_facture['fac_regle'];
// FIN DONNEES FACTURES
$client = $d_facture['fac_client'];

// FIN ECHEANCES

// URL RETOURS

if(!empty($client)){
    $client_retour = $client;
}

if($origine == "poste"){
    $url_retour =  "facture_relance_voir.php?client=" . $client_retour;
}elseif($origine == "histo"){
    $url_retour =  "facture_relance_histo.php?client=" . $client_retour;
}else{
    $url_retour = $_SESSION['retour'];
}

// RECHERCHER LE CLIENT

$sql="SELECT * FROM Clients WHERE cli_id = " . $client;
$req = $Conn->query($sql);
$d_client=$req->fetch();

// CONDITIONS DE REGLEMENTS
$con_reg = "";
if(!empty($client['cli_reg_type'])){
    $sql="SELECT rty_libelle FROM Reglements_types WHERE rty_id = " . $client['cli_reg_type'];
    $req = $Conn->query($sql);
    $reglement_type=$req->fetch();

    $con_reg = $con_reg . " " . $reglement_type['rty_libelle'];
}

if($d_client['cli_reg_formule'] == 1){

    $con_reg = $con_reg . " Date + " . $d_client['cli_reg_nb_jour'] . " jours";

}
if($d_client['cli_reg_formule'] == 2){
$con_reg = $con_reg . " Date + 45j + fin de mois";
}
if($d_client['cli_reg_formule'] == 3){
$con_reg = $con_reg . " Date + fin de mois + 45j";
}
if($d_client['cli_reg_formule'] == 4){
    $con_reg = $con_reg . " Date + " . $d_client['cli_reg_nb_jour'] . " jours + fin de mois + " . $d_client['cli_reg_fdm'];
}

// Etats de relances
$sql="SELECT * FROM Relances_Etats";
$req = $Conn->query($sql);
$d_relances_etats=$req->fetchAll();

// CHERCHER LES LIBELLE RELANCES_ETATS POUR SELECT TYPE DE PROCéDURE
$sql="SELECT * FROM Relances_Etats ORDER BY ret_j_deb";
$req = $Conn->query($sql);
$d_statut=$req->fetchAll();

// chercher les libelles
$d_statut_libelle=array();
foreach($d_statut as $ds){
    $d_statut_libelle[$ds['ret_id']]= $ds['ret_libelle'];
}


// CHECK LE REGLEMENT DE LA FACTURE
$regle = false;
$retard = 0;
$reste_du=$d_facture['fac_total_ttc']-$d_facture['fac_regle'];
if($reste_du == 0){
    $fac_date_reg_prev = strtotime($d_facture['fac_date_reg_prev']);
    $fac_date_reg = strtotime($d_facture['fac_date_reg']);
    $datediff = $fac_date_reg_prev - $fac_date_reg;
    $regle = true;
    $retard = round($datediff / (60 * 60 * 24));


}elseif(!empty($d_facture['fac_date_reg_rel'])){
    $datetime1 = new DateTime($d_facture['fac_date_reg_rel']);
    $datetime2 = new DateTime();
    $interval = $datetime1->diff($datetime2);
    $retard = $interval->format('%r%a');

}else{
    $datetime1 = new DateTime($d_facture['fac_date_reg_prev']);
    $datetime2 = new DateTime();
    $interval = $datetime1->diff($datetime2);
    $retard = $interval->format('%r%a');
}

// HISTORIQUE DES RELANCES
$sql="SELECT * FROM Relances,Relances_Factures WHERE rel_id=rfa_relance AND rfa_facture_soc = " . $societe . " AND rfa_facture=" . $facture . "
ORDER BY rel_date DESC,rel_id DESC;";
$req = $Conn->query($sql);
$d_histo_relances=$req->fetchAll();

/* $req = $Conn->prepare("SELECT * FROM Correspondances WHERE cor_client = :client AND cor_relance = 1 ORDER BY cor_date DESC;");
$req->bindParam("client",$client);
$req->execute();
$correspondances = $req->fetchAll(); */

$utilisateurs=orion_utilisateurs();

$req = $ConnFct->prepare("SELECT * FROM Factures_Commentaires WHERE fco_facture = " . $facture . " ORDER BY fco_date DESC;");
$req->execute();
$factures_commentaires = $req->fetchAll();


?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">
                        <h1>Suivi de la facture <?= $d_facture['fac_numero'] ?></h1>
                        <div class="row mt15">
                            <div class="col-md-4">
		                		<div class="panel mb10">
		                            <div class="panel-heading panel-head-sm">
		                                <span class="panel-icon">
		                                    <i class="fa fa-file-o" aria-hidden="true"></i>
		                                </span>
		                                <span class="panel-title"> Facture</span>
		                            </div>
		                            <div class="panel-body p10">
                                        <h4  class="text-left">Facture <?= $d_facture['fac_numero'] ?></h4>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <ul>
                                                    <li>
                                                        Date : <strong><?= convert_date_txt($d_facture['fac_date']) ?></strong>
                                                    </li>
                                                    <li>
                                                        Date d'échéance :
                                                        <?php if(!empty($fac_date_reg_prev)){ ?>
                                                            <strong><?= $fac_date_reg_prev ?></strong>
                                                        <?php }?>

                                                    </li>
                                                    <li>

                                                    Date réf. relance :
                                                    <?php if(!empty($fac_date_reg_rel)){ ?>
                                                        <strong><?= $fac_date_reg_rel ?></strong>
                                                    <?php }?>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?php if(!empty($d_facture['fac_etat_relance']) && $d_facture['fac_etat_relance'] == 6){ ?>
                                            <div class="col-md-6">
                                                <ul>
                                                    <li>
                                                        Statut : <strong><?= $d_statut_libelle[$d_facture['fac_etat_relance']] ?></strong>
                                                    </li>
                                                    <li>
                                                        Rééditer la facture avec la mention "TVA" : <strong><a href="facture_voir.php?facture=<?=$d_facture["fac_id"]?>&origine=act_voir" ><?=$d_facture["fac_numero"]?></a></strong>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?php }elseif($regle){ ?>
                                                <ul>
                                                    <li>
                                                        Statut actuel : <strong>Réglé</strong>
                                                    </li>
                                                    <li>
                                                        Date de règlement : <strong><?= convert_date_txt($d_facture['fac_date_reg']) ?></strong>
                                                    </li>
                                                    <li>
                                                    Retard : <strong><?= $retard ?> jour(s)</strong>
                                                    </li>
                                                </ul>
                                            <?php }elseif($d_facture['fac_relance_stop']){ ?>
                                                <ul>
                                                    <li>
                                                        Statut actuel :
														<?php if($d_facture['fac_relance_stop'] == 1){ ?>
														<strong style="color:orange">Processus suspendu par l'agence</strong>
														<?php }elseif($d_facture['fac_relance_stop'] == 2){ ?>
															<strong style="color:green">Processus suspendu par la compta</strong>
														<?php }elseif($d_facture['fac_relance_stop'] == 3){ ?>
															<strong style="color:red">Client douteux</strong>
														<?php }?>
                                                    </li>
                                                    <li>
                                                    Retard : <strong><?= $retard ?> jour(s)</strong>
                                                    </li>
                                                </ul>
                                            <?php }else{ ?>
                                                <ul>
                                                    <li>
                                                        Statut : <strong><?= $d_statut_libelle[$d_facture['fac_etat_relance']] ?></strong>
                                                    </li>
                                                    <li>
                                                    Retard : <strong><?= $retard ?> jour(s)</strong>
                                                    </li>
                                                </ul>
                                            <?php } ?>

                                        </div>
                                        </div>


		                        </div>
		                	</div>
                            <div class="col-md-4">
                                <div class="panel mb10">
		                            <div class="panel-heading panel-head-sm">
		                                <span class="panel-icon">
		                                    <i class="fa fa-file-o" aria-hidden="true"></i>
		                                </span>
		                                <span class="panel-title"> Client</span>
		                            </div>
		                            <div class="panel-body p10">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul>
                                                    <li>
                                                        Code : <strong><?= $d_client['cli_code'] ?></strong>
                                                    </li>
                                                    <li>
                                                        Nom : <strong><?= $d_client['cli_nom'] ?></strong>
                                                    </li>
                                                    <li>
                                                    Modalité de règlement : <strong><?= $con_reg ?></strong>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>


		                        </div>
                            </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel mb10">
		                            <div class="panel-heading panel-head-sm">
		                                <span class="panel-icon">
		                                    <i class="fa fa-file-o" aria-hidden="true"></i>
		                                </span>
		                                <span class="panel-title"> Règlements</span>
		                            </div>
		                            <div class="panel-body p10">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <ul>
                                                    <li>
                                                        Montant : <strong><?=number_format($d_facture['fac_total_ttc'], 2, ',', ' ');?> €</strong>
                                                    </li>
                                                    <li>
                                                        Réglé : <strong><?=number_format($d_facture['fac_regle'], 2, ',', ' ');?> €</strong>
                                                    </li>
                                                    <li>
                                                        Reste dû : <strong><?=number_format($reste_du, 2, ',', ' ');?> €</strong>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>


                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="row mt15">
                                            <div class="col-md-8">
                                                <?php if(!empty($d_histo_relances)){
                                                   // Etats de relances




                                                    ?>
                                                <div class="table-responsive">
                                                    <h4  class="text-left">Historique des relances</h4>
                                                    <table class="table table-striped table-hover" >
                                                        <thead>
                                                            <tr class="dark2" >
                                                                <th>Utilisateur</th>
                                                                <th>Contact</th>
                                                                <th>Commentaire</th>
                                                                <th>Rappeler</th>
                                                                <th>Mail / Courrier</th>
                                                                <th>&nbsp;</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                <?php  foreach($d_histo_relances as $dhr){
                                                    $sql="SELECT uti_nom, uti_prenom FROM Utilisateurs WHERE uti_id = " . $dhr['rel_utilisateur'];
                                                    $req = $Conn->query($sql);
                                                    $utilisateur=$req->fetch();
                                                    $sql="SELECT uti_nom, uti_prenom FROM Utilisateurs WHERE uti_id = " . $dhr['rel_envoie_uti'];
                                                    $req = $Conn->query($sql);
                                                    $envoi_uti=$req->fetch();
                                                    $sql="SELECT uti_nom, uti_prenom FROM Utilisateurs WHERE uti_id = " . $dhr['rel_courrier_uti'];
                                                    $req = $Conn->query($sql);
                                                    $courrier_uti=$req->fetch();
                                                                $txt_envoi = "";
                                                                if($dhr['rel_envoie']){
                                                                    $txt_envoi = "Mail envoyé le " . convert_date_txt($dhr['rel_envoie_date']);
                                                                    $txt_envoi = $txt_envoi . "<br>par " . $envoi_uti['uti_prenom'] . " " . $envoi_uti['uti_nom'];
                                                                }
                                                                if($dhr['rel_courrier']){
                                                                    if($txt_envoi != ""){
                                                                        $txt_envoi = $txt_envoi . "<br><br>";
                                                                    }
                                                                    $txt_envoi = "Courrier imprimé le " . convert_date_txt($dhr['rel_courrier_date']);
                                                                    $txt_envoi = $txt_envoi . "<br>par " . $courrier_uti['uti_prenom'] . " " . $courrier_uti['uti_nom'];
                                                                }
                                                                ?>
                                                                <tr>
                                                                    <td><?= $dhr['rel_date']?> <?= $utilisateur['uti_prenom'] ?> <?= $utilisateur['uti_nom'] ?></td>
                                                                    <td><?= $dhr['rel_con_prenom']?> <?= $dhr['rel_con_nom']?>
                                                                        <?php if(!empty($dhr['rel_con_tel'])){ ?>
                                                                            <br>Tél : <?= $dhr['rel_con_tel'] ?>
                                                                        <?php } ?>
                                                                </td>
                                                                    <td><?= $dhr['rel_comment'] ?></td>
                                                                    <td><?=$txt_envoi?></td>
                                                                    <td>
                                                                        <?php $rappel = new DateTime($dhr['rel_date_rappel']) ?>
                                                                        <?= $rappel->format('Y-m-d H:i:s'); ?>
                                                                    </td>
                                                                    <td class="text-right" >

                                                                            <a href="mail_prep.php?facture_relance=<?=$dhr["rel_id"]?>" class="btn btn-success">
                                                                                <i class="fa fa-enveloppe-o"></i> Nouveau mail
                                                                            </a>
                                                                    </td>


                                                                </tr>
                                    <?php					}
                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                            <?php 			}else{ ?>
                                                    <div class="alert alert-warning" style="border-radius:0px;">
                                                        Aucun historique de relance
                                                    </div>
                            <?php			} ?>

                                            </div>
                                            <div class="col-md-4">
                                            <?php if(!empty($factures_commentaires)){
                                                   // Etats de relances
                                                    ?>
                                                <div class="table-responsive mt15">
                                                    <h4  class="text-left">Commentaires</h4>
                                                    <table class="table mt15" >

                                                        <tbody>
                                                    <?php
                                                    $count = 1;
                                                    foreach($factures_commentaires as $fco){
                                                                ?>
                                                                <tr
                                                                <?php if ($count%2 == 1){  ?>
                                                                    style="background:#FAFAFA!important;"
                                                                <?php }?>
                                                                >
                                                                    <td>
                                                                    <?php if(!empty($utilisateurs[$fco['fco_utilisateur']])){ ?>
                                                                    <?= $utilisateurs[$fco['fco_utilisateur']]["identite"]?>
                                                                    <?php }?>
                                                                    </td>
                                                                    <td><?= convert_date_txt($fco['fco_date']); ?></td>
                                                                    <?php if($fco['fco_utilisateur'] == $acc_utilisateur){ ?>
                                                                        <td>
                                                                            <a href="facture_commentaire.php?id=<?= $fco['fco_id'] ?>" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                                                        </td>
                                                                    <?php }else{ ?>
                                                                        <td> </td>
                                                                    <?php }?>


                                                                </tr>
                                                                <tr
                                                                <?php if ($count%2 == 1){  ?>
                                                                style="background:#FAFAFA!important;"
                                                                <?php }?>
                                                                >
                                                                    <td colspan="3">
                                                                        <?=$fco['fco_comment']?>
                                                                    </td>
                                                                </tr>
                                                <?php
                                                $count++;
                                            } ?>
                                                        </tbody>
                                                    </table>
                                                </div>

                            <?php 			}else{ ?>
                                                    <div class="alert alert-warning" style="border-radius:0px;">
                                                        Aucun commentaire
                                                    </div>
                            <?php			} ?>
                                                <form action="facture_commentaire_enr.php" method="POST" class="mt15">
                                                    <input type="hidden" name="facture" value="<?= $facture ?>">
                                                    <input type="hidden" name="societe" value="<?= $societe ?>">
                                                    <div class="section">
                                                        <label class="field prepend-icon" style="width:100%">
                                                            <textarea class="summernote-img" id="commentaire" name="commentaire" placeholder="Texte du commentaire"></textarea>
                                                        </label>
                                                    </div>
                                                    <div class="section text-center">
                                                        <button type="submit" class="btn btn-success btn-sm">
                                                            <i class='fa fa-save'></i> Enregistrer
                                                        </button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>


		                        </div>


					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="<?= $url_retour ?>" class="btn btn-default btn-sm" role="button">
							<span class="fa fa-long-arrow-left"></span>
							<span class="hidden-xs">Retour</span>
						</a>
					</div>
					<!-- <div class="col-xs-6 footer-middle">&nbsp;</div> -->
					<div class="col-xs-9 footer-right">
                        <a href="facture_relance_voir.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-info btn-sm" >
                            <i class='fa fa-eye'></i> Afficher les impayés
                        </a>
                        <a href="facture_relance_histo.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-info btn-sm" >
                            <i class='fa fa-eye'></i> Afficher l'historique
                        </a>
                        <?php if(!empty($_SESSION['acces']['acc_droits'][30])){ ?>
                        <a href="facture_relance_facture_mod.php?facture=<?= $facture ?>&societe=<?= $societe ?>" class="btn btn-warning btn-sm" >
                            <i class='fa fa-pencil'></i> Modifier les infos de relance
                        </a>
                        <?php } ?>

                        <a href="#" class="btn btn-info btn-sm" onClick="imprimer();">
                            <i class='fa fa-print'></i> Imprimer
                        </a>
                    </div>
				</div>
			</footer>
		</form>
        <style>
            .note-editable { background-color: white !important;}
        </style>
<?php	include "includes/footer_script.inc.php"; ?>
        <script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
		<script type="text/javascript">
            function supprimer_corresp(json){
				console.log("supprimer_corresp(json)");
				if(json){
					if(json.correspondance>0){
						$("#correspondance_" + json.correspondance).remove();
					}
					if($(".correspondance").size()==0){
						$("#no_corresp").show();
					}else{
						if(json.non_traite>0){
							$("#corresp_rappel_" + json.non_traite).prop("checked",false);
						}
					}
				}
			}
			function preparer_supp_corresp(corresp){
				console.log("preparer_supp_corresp(" + corresp + ")");
				del_client_corresp(corresp,supprimer_corresp,corresp,"");
			}
			function actualiser_rappel(json,corresp){
				if(json){
					if(json.erreur==0){
						// reussite
						afficher_txt_user($(".footer-middle"),"Mise à jour terminée","text-success",5000);
					}else{
						// erreur
						if($("corresp_rappel_" + corresp).is(":checked")){
							$(this).prop("checked",false);
						}else{
							$(this).prop("checked",true);
						}
						if(json.erreur_txt == ""){
							modal_alerte_user("Veuillez vous reconnecter à Orion.");
						}else{
							modal_alerte_user(json.erreur_txt);
						}

					}
				}
			}
			jQuery(document).ready(function (){
				$(".corresp-supp").click(function(){
					corresp=$(this).data("corresp");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer cette correspondance ?",preparer_supp_corresp,corresp);
				});

				$(".corresp-rappel").click(function(){
					corresp=$(this).data("corresp");
					cor_rappel=0;
					if($(this).is(":checked")){
						cor_rappel=1;
					}
					set_base_champ("0","Correspondances","cor_rappel",cor_rappel,"cor_id",corresp,actualiser_rappel,corresp);
				});

            });
            function imprimer(){
				window.print();
			}
		</script>
	</body>
</html>
