<?php
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");


// DROITS D'ACCES
if($_SESSION['acces']["acc_profil"]!=13 AND $_SESSION['acces']["acc_service"][2]!=1){
    echo("Accès refusé!");
    die();
}

// FAMILLES
$req = $Conn->prepare("SELECT * FROM produits_familles,produits_sous_familles WHERE pfa_id=psf_pfa_id ORDER BY pfa_libelle,psf_libelle");
$req->execute();
$familles = $req->fetchAll();

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
        <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
        <form method="post" action="param_marge_st_enr.php" >
            <div id="main">
    <?php        include "includes/header_def.inc.php";?>
                <section id="content_wrapper">
                    <section id="content" class="animated fadeIn">

                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">

                                <div class="content-header">
                                    <h2>Marges sous-traitance <b class="text-primary">Paliers de dérogation</b></h2>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr class="dark" >
                                                <th rowspan="2" >Famille</th>
                                                <th rowspan="2" >Sous-famille</th>
                                                <th colspan="2" class="text-center" >Dérogation 1</th>
                                                <th colspan="2" class="text-center" >Dérogation 2</th>
                                            </tr>
                                            <tr class="dark" >									
                                                <th>Marge (M <=)</th>
                                                <th>Taux de marge (TM <=)</th>
                                                <th>Marge (M <=)</th>
                                                <th>Taux de marge (TM <=)</th>                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                    <?php   foreach ($familles as $f){ ?>
                                                <tr>
                                                    <td><?= $f['pfa_libelle'] ?></td>
                                                    <td><?= $f['psf_libelle'] ?></td>
                                                    <td>
                                                        
                                                        <div class="field append-icon">
                                                            <input type="text" name="<?= $f['pfa_id']?>_<?= $f['psf_id']?>_psf_marge_montant_1" class="gui-input input-float" id="<?= $f['pfa_id']?>_<?= $f['psf_id']?>_psf_marge_montant_1" placeholder="Marge minimum niv 1" value="<?=$f['psf_marge_montant_1']?>" />
                                                            <span class="field-icon">
                                                                <i class="fa fa-eur"></i>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="field append-icon">
                                                            <input type="text" name="<?= $f['pfa_id']?>_<?= $f['psf_id']?>_psf_marge_taux_1" class="gui-input input-float" id="<?= $f['pfa_id']?>_<?= $f['psf_id']?>_psf_marge_taux_1" placeholder="Taux de marge minimum niv 1" value="<?=$f['psf_marge_taux_1']?>" />
                                                            <span class="field-icon">
                                                                %
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="field append-icon">
                                                            <input type="text" name="<?= $f['pfa_id']?>_<?= $f['psf_id']?>_psf_marge_montant_2" class="gui-input input-float" id="<?= $f['pfa_id']?>_<?= $f['psf_id']?>_psf_marge_montant_2" placeholder="Marge minimum niv 1" value="<?=$f['psf_marge_montant_2']?>" />
                                                            <span class="field-icon">
                                                                <i class="fa fa-eur"></i>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="field append-icon">
                                                            <input type="text" name="<?= $f['pfa_id']?>_<?= $f['psf_id']?>_psf_marge_taux_2" class="gui-input input-float" id="<?= $f['pfa_id']?>_<?= $f['psf_id']?>_psf_marge_taux_2" placeholder="Taux de marge minimum niv 1" value="<?=$f['psf_marge_taux_2']?>" />
                                                            <span class="field-icon">
                                                            %
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                    <?php   } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
            <!-- End: Main -->
            <footer id="content-footer" class="affix" >
                <div class="row">
                    <div class="col-xs-3 footer-left" >
                        <a href="param_marge_st_voir.php" class="btn btn-default btn-sm" role="button" >
                            <span class="fa fa-long-arrow-left"></span>
                            Retour
                        </a>
                    </div>
                    <div class="col-xs-6 footer-middle" >&nbsp;</div>
                    <div class="col-xs-3 footer-right" >
                        <button type="submit" class="btn btn-success btn-sm" >
                            <span class="fa fa-save"></span>
                            Enregistrer
                        </a>
                    </div>
                </div>
            </footer>
        </form>
<?php
		include "includes/footer_script.inc.php"; ?>	
		
	</body>
</html>
