<?php

include "includes/controle_acces.inc.php";
include('modeles/mod_upload.php');

// UPLOAD D'UN FICHIER CSV qui sera synchro
$erreur=0;

$action_id=0;
if(isset($_POST["action"])){
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]);
	}	
}
$session_id=0;
if(isset($_POST["session"])){
	if(!empty($_POST["session"])){
		$session_id=intval($_POST["session"]);
	}	
}
$action_client_id=0;
if(isset($_POST["action_client"])){
	if(!empty($_POST["action_client"])){
		$action_client_id=intval($_POST["action_client"]);
	}	
}

$acc_societe=0;
if(!empty($_SESSION['acces']['acc_societe'])){
	$acc_societe=$_SESSION['acces']['acc_societe'];
}

if($action_id==0 OR $action_client_id==0 OR empty($_FILES['fichier']['tmp_name']) OR $acc_societe==0 ){
	
	$erreur=1;

}else{
	
	$dossier="import/stagiaires/";
	
	$nom="stagiaire_" . $acc_societe . "_" . $action_id . "_" . $action_client_id . "_" . $session_id;
	
	$extension=array("csv");
	

	$erreur=upload("fichier",$dossier,$nom,2048,$extension,1);
}

if($erreur==0){
	$erreur=-990;
}
header("location: action_sta_import.php?action=" . $action_id . "&session=" . $session_id . "&erreur=" . $erreur);
?>
