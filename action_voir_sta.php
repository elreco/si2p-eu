 <?php

	include "includes/controle_acces.inc.php";

	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");

	include 'modeles/mod_orion_client_info.php';
	include 'modeles/mod_orion_utilisateur.php';
	include 'modeles/mod_orion_pro_categorie.php';
	include 'modeles/mod_orion_pro_famille.php';
	include 'modeles/mod_orion_pro_s_famille.php';
	include 'modeles/mod_orion_vehicule.php';

	include 'modeles/mod_parametre.php';
	include 'modeles/mod_get_commerciaux.php';



	// ECRAN DE VISU DES STAGIIARE D'UNE ACTION

	$erreur_txt="";

	$action_id=0;
	if(isset($_GET["action"])){
		if(!empty($_GET["action"])){
			$action_id=$_GET["action"];
		}
	}

    $action_client_get=0;
	if(isset($_GET["action_client"])){
		if(!empty($_GET["action_client"])){
			$action_client_get=$_GET["action_client"];
		}
	}

	if($action_id>0){

		// ON RECUPERE LES DONNES NECESSAIRE A L'AFFICHAGE

		// l'action

		$sql="SELECT act_id,act_gest_sta,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,act_agence,act_verrou_admin FROM Actions WHERE act_id=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$action_id);
		$req->execute();
		$d_action=$req->fetch();
		if(empty($d_action)){
			$erreur_txt="Impossible d'afficher la page!";
		}
	}else{
		$erreur_txt="Impossible d'afficher la page!";
	}

	if(empty($erreur_txt)){

		// personne connecte
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];
		}
		if($conn_soc_id!=$acc_societe){
			$acc_societe=$conn_soc_id;
		}

		$acc_profil=0;
		if(isset($_SESSION['acces']["acc_profil"])){
			$acc_profil=intval($_SESSION['acces']["acc_profil"]);
		}

		// RETOUR

		$_SESSION["retourQualif"]="action_voir_sta.php?action=" . $action_id . "&societ=" . $conn_soc_id;
		$_SESSION["retourCaces"]="action_voir_sta.php?action=" . $action_id . "&societ=" . $conn_soc_id;
		$_SESSION["retourSsiap"]="action_voir_sta.php?action=" . $action_id . "&societ=" . $conn_soc_id;

		// FAMILLES

		$action_head="";
		$action_bg="";
		$action_txt="";

		$sql="SELECT pfa_libelle FROM Produits_Familles WHERE pfa_id=" . $d_action["act_pro_famille"] . ";";
		$req=$Conn->query($sql);
		$d_pro_fam=$req->fetch();
		if(!empty($d_pro_fam)){
			$action_head.=" " . $d_pro_fam["pfa_libelle"];
		}

		if(!empty($d_action["act_pro_sous_famille"])){
			$sql="SELECT psf_libelle,psf_couleur_bg,psf_couleur_txt FROM Produits_Sous_Familles WHERE psf_id=" . $d_action["act_pro_sous_famille"] . ";";
			$req=$Conn->query($sql);
			$d_pro_s_fam=$req->fetch();
			if(!empty($d_pro_s_fam)){
				$action_head.=" / " . $d_pro_s_fam["psf_libelle"];
				if(!empty($d_pro_s_fam["psf_couleur_bg"])){
					$action_bg=$d_pro_s_fam["psf_couleur_bg"];
					$action_txt=$d_pro_s_fam["psf_couleur_txt"];
				}
			}
		}
		if(!empty($d_action["act_pro_sous_sous_famille"])){
			$sql="SELECT pss_libelle,pss_couleur_bg,pss_couleur_txt FROM Produits_Sous_Sous_Familles WHERE pss_id=" . $d_action["act_pro_sous_sous_famille"] . ";";
			$req=$Conn->query($sql);
			$d_pro_s_s_fam=$req->fetch();
			if(!empty($d_pro_s_s_fam)){
				$action_head.=" / " . $d_pro_s_s_fam["pss_libelle"];
				if(!empty($d_pro_s_s_fam["pss_couleur_bg"])){
					$action_bg=$d_pro_s_s_fam["pss_couleur_bg"];
					$action_txt=$d_pro_s_s_fam["pss_couleur_txt"];
				}
			}
		}
		if(!empty($action_bg)){
			$action_bg="style='background-color:#" . $action_bg . ";color:" . $action_txt . "'";
		}

		// LES ATTESTATIONS

		$d_attestations=array();
		$sql="SELECT att_id,att_titre FROM Attestations WHERE att_famille=" . $d_action["act_pro_famille"] . " AND att_sous_famille=" . $d_action["act_pro_sous_famille"] . "
		AND att_sous_sous_famille=" . $d_action["act_pro_sous_sous_famille"] . " AND att_statut=1 ORDER BY att_titre;";
		$req=$Conn->query($sql);
		$results=$req->fetchAll();
		//print_r($results);
		if(!empty($results)){
			foreach($results as $r){
				$d_attestations[$r["att_id"]]=$r["att_titre"];
			}
		}

		// LES STAGIAIRES -> ne sont pas sur la meme base

		$liste_sta="";
		$sql="SELECT ast_stagiaire FROM Actions_Stagiaires WHERE ast_action=" . $action_id . ";";
		$req=$ConnSoc->query($sql);
		$src_sta=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($src_sta)){
			$tab_sta=array_column ($src_sta,"ast_stagiaire");
			$liste_sta=implode($tab_sta,",");
		}

		$d_stagiaire=array();
		if(!empty($liste_sta)){
			$sql="SELECT sta_id,sta_nom,sta_prenom,sta_naissance FROM Stagiaires WHERE sta_id IN (" . $liste_sta . ");";
			$req=$Conn->query($sql);
			$result_sta=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($result_sta)){
				foreach($result_sta as $rs){
					$d_stagiaire[$rs["sta_id"]]=array(
						"sta_nom" => $rs["sta_nom"],
						"sta_prenom" => $rs["sta_prenom"],
						"sta_naissance" => $rs["sta_naissance"]
					);
				}
			}
		}

		// LES QUALIFICATIONS

		$d_qualifications=array();

		$sql="SELECT qua_id,qua_caces,qua_ssiap FROM Qualifications ORDER BY qua_id;";
		$req=$Conn->query($sql);
		$result_qua=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($result_qua)){
			foreach($result_qua as $qua){
				$d_qualifications[$qua["qua_id"]]=array(
					"qua_caces" => $qua["qua_caces"],
					"qua_ssiap" => $qua["qua_ssiap"]
				);
			}
		}

		if($d_action["act_gest_sta"]==2){

			// INSCRIPTION A LA SESSION

			$session_id=0;
			$nb_session=-1;
			$donnees=array();

			$sql="SELECT int_label_1,pda_date,pda_demi,ase_id,ase_h_deb,ase_h_fin
			,ass_stagiaire,ast_confirme,ast_attestation,ast_attest_ok,ast_qualification,ast_diplome,ast_diplome_statut
			FROM Intervenants
			INNER JOIN Plannings_Dates ON (Intervenants.int_id=Plannings_Dates.pda_intervenant)
			INNER JOIN Actions_Sessions ON (Plannings_Dates.pda_id=Actions_Sessions.ase_date)
			LEFT JOIN Actions_Stagiaires_Sessions ON (Actions_Sessions.ase_id=Actions_Stagiaires_Sessions.ass_session)
			LEFT JOIN Actions_Stagiaires ON (Actions_Stagiaires_Sessions.ass_stagiaire=Actions_Stagiaires.ast_stagiaire AND ast_action=:action)
			WHERE pda_type=1 AND pda_ref_1=:action
			ORDER BY pda_date,pda_demi,ase_h_deb,ase_id,int_label_1;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action",$action_id);
			$req->execute();
			$resultat=$req->fetchAll();
			if(!empty($resultat)){
				foreach($resultat as $r){

					if($session_id!=$r["ase_id"]){

						$nb_session++;

						if(!empty($r["ase_h_deb"]) AND !empty($r["ase_h_deb"])){
							$h=$r["ase_h_deb"] . " / " . $r["ase_h_fin"];
						}elseif($r["ase_h_deb"]==1){
							$h="matin";
						}else{
							$h="après-midi";
						}
						$donnees[$nb_session]=array(
							"libelle" => $r["int_label_1"] . " " . $r["pda_date"] . " " . $h,
							"id" => $r["ase_id"],
							"data_id" => "data-session='" . $r["ase_id"] . "'",
							"stagiaires" => array()
						);

						$session_id=$r["ase_id"];

					}
					if(!empty($r["ass_stagiaire"])){

						$attest_statut="";
						if(!empty($r["ast_attestation"])){
							if(!empty($r["ast_attest_ok"])){
								$attest_statut=1;
							}else{
								$attest_statut=0;
							}

						}
						if(!empty($d_stagiaire[$r["ass_stagiaire"]])){
							$donnees[$nb_session]["stagiaires"][]=array(
								"nom" => $d_stagiaire[$r["ass_stagiaire"]]["sta_nom"] . " " . $d_stagiaire[$r["ass_stagiaire"]]["sta_prenom"],
								"id" => $r["ass_stagiaire"],
								"confirme" => $r["ast_confirme"],
								"attestation" => $r["ast_attestation"],
								"attest_statut" => $attest_statut,
								"qualification" => $r["ast_qualification"],
								"diplome" => $r["ast_diplome"],
								"diplome_statut" => $r["ast_diplome_statut"]
							);
						}
					}
				}
			}
		}else{
			// INSCRIPTION A LA FORMATION


			$action_client=0;
			$nb_client=-1;
			$donnees=array();

			$sql="SELECT acl_id,cli_id,cli_code,cli_nom,ast_stagiaire,ast_attestation,ast_confirme,ast_attest_ok,ast_qualification,ast_diplome,ast_diplome_statut
			FROM Actions_Clients INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND cli_agence=:cli_agence)
			LEFT JOIN Actions_Stagiaires ON (Actions_Clients.acl_id=Actions_Stagiaires.ast_action_client AND ast_action=:action)
			WHERE acl_action=:action AND NOT acl_archive
			ORDER BY acl_archive,cli_code,cli_nom,cli_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":cli_agence",$d_action["act_agence"]);
			$req->bindParam(":action",$action_id);
			$req->execute();
			$resultat=$req->fetchAll();
			if(!empty($resultat)){
				foreach($resultat as $r){

					if($action_client!=$r["acl_id"]){

						$nb_client++;

						$donnees[$nb_client]=array(
							"libelle" => $r["acl_id"] . " - [" . $r["cli_code"] . "] " . $r["cli_nom"],
							"id" => $r["acl_id"],
							"data_id" => "data-action_client='" . $r["acl_id"] . "'",
							"stagiaires" => array()
						);

						$action_client=$r["acl_id"];

					}
					if(!empty($r["ast_stagiaire"])){

						$attest_statut="";
						if(!empty($r["ast_attestation"])){
							if(!empty($r["ast_attest_ok"])){
								$attest_statut=1;
							}else{
								$attest_statut=0;
							}

						}

						$sta_nom="";
						if(isset($d_stagiaire[$r["ast_stagiaire"]])){
							$sta_nom=$d_stagiaire[$r["ast_stagiaire"]]["sta_nom"] . " " . $d_stagiaire[$r["ast_stagiaire"]]["sta_prenom"];
						}
						$donnees[$nb_client]["stagiaires"][]=array(
							"nom" => $sta_nom,
							"id" => $r["ast_stagiaire"],
							"confirme" => $r["ast_confirme"],
							"attestation" => $r["ast_attestation"],
							"attest_statut" => $attest_statut,
							"qualification" => $r["ast_qualification"],
							"diplome" => $r["ast_diplome"],
							"diplome_statut" => $r["ast_diplome_statut"]
						);
					}
				}
			}


		}
	}
	/*echo("<pre>");
		print_r($donnees);
	echo("</pre>");
	die();*/

	if(empty($erreur_txt)){
?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<title>SI2P - Orion - Recherche client</title>
				<meta name="keywords" content=""/>
				<meta name="description" content="">
				<meta name="author" content="">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<!-- CSS THEME -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
				<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />

				<!-- CSS PLUGINS -->
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

				<!-- CSS Si2P -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

				<!-- Favicon -->
				<link rel="shortcut icon" href="assets/img/favicon.png">

				<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
				<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
			</head>
			<body class="sb-top sb-top-sm ">

				<div id="main">
		<?php		include "includes/header_def.inc.php"; ?>
					<section id="content_wrapper">


						<section id="content" class="animated">

							<div class="tab-block">
								<ul class="nav nav-tabs responsive">
									<li>
										<a href="action_voir.php?action=<?=$action_id?>&action_client=<?=$action_client_get?>&societ=<?=$conn_soc_id?>" >Général</a>
									</li>
									<li class='active' >
										<a href="#action_stagiaire" id="ong_stagiaire" data-toggle="tab" aria-expanded="false">Stagiaires</a>
									</li>
							<?php	if($acc_profil!=1 AND !$d_action["act_verrou_admin"]){ ?>
										<li>
											<a href="action_voir.php?action=<?=$action_id?>&action_client=<?=$action_client_get?>&onglet=3&societ=<?=$conn_soc_id?>" >Editions</a>
										</li>
							<?php	} ?>
								</ul>
								<div class="tab-content responsive">

									<div class="admin-form theme-primary ">

										<h1>Action N° <?=$d_action["act_id"] . " " . $action_head?></h1>

										<form method="post" action="ajax/ajax_action_stagiaire_doc.php" id="form_sta_doc" >
											<div>
												<input type="hidden" name="action" value="<?=$action_id?>" />
												<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
											</div>
							<?php			foreach($donnees as $d){
												asort($d["stagiaires"]); ?>
												<div class="panel" >
													<div class="panel-heading panel-head-sm">
														<b><?=$d["libelle"]?></b>
														<div class="pull-right" >
															<button type="button" class="btn btn-success btn-sm btn-add" data-toggle="tooltip" data-placement="top" title="Ajouter des stagiaires" <?=$d["data_id"]?> >
																<i class="fa fa-user-plus" ></i>
															</button>
														</div>
													</div>
													<div class="panel-body">
														<table class="table" id="table_sta_<?=$d["id"]?>" <?php if(empty($d["stagiaires"])) echo("style='display:none;'"); ?> >
															<thead>
																<tr>
																	<th rowspan="2" >&nbsp;</th>
																	<th rowspan="2" >&nbsp;</th>
																	<th rowspan="2"  >Nom</th>
																	<th rowspan="2" >Statut</th>
																	<th colspan="2" class="text-center" >Convocation</th>
																	<th colspan="3" class="text-center" >Attestation</th>
																	<th class="text-center" >Qualification</th>
																	<th rowspan="2" >Mail</th>
																</tr>
																<tr>
																	<th>&nbsp;</th>
																	<th class="text-center" >
																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" class="check-all" id="convocation_<?=$d["id"]?>_all" >
																				<span class="checkbox"></span>
																			</label>
																		</div>
																	</th>

																	<th>Attestation</th>
																	<th>Statut</th>
																	<th class="text-center" >
																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" class="check-all" id="attestation_<?=$d["id"]?>_all" >
																				<span class="checkbox"></span>
																			</label>
																		</div>
																	</th>

																	<th class="text-center" >
																		<a href="action_qualif_liste.php?action=<?=$action_id?>&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
																			<i class="fa fa-eye" ></i>
																		</a>
																	</th>
																</tr>
															</thead>
															<tbody id="inscription_<?=$d["id"]?>" >
						<?php									foreach($d["stagiaires"] as $sta){
																	// ATTENTION  -> reporter le html dans ajouter_stagiaire() ?>

																	<tr id="stagiaire_<?=$sta["id"]?>" data-inscription="<?=$d["id"]?>" >
																		<td>
																	<?php	if(empty($sta["diplome"])){ ?>
																				<button type="button" class="btn btn-sm btn-danger btn-supp" data-stagiaire="<?=$sta["id"]?>" >
																					<i class="fa fa-times" ></i>
																				</button>
																	<?php	} ?>
																		</td>
																		<td>
																	<?php	if(empty($sta["diplome"])){ ?>
																				<button type="button" class="btn btn-sm btn-warning btn-edit" data-stagiaire="<?=$sta["id"]?>" >
																					<i class="fa fa-pencil" ></i>
																				</button>
																	<?php	} ?>
																		</td>
																		<td id="nom_<?=$sta["id"]?>" ><?=$sta["nom"]?></td>
																		<td id="statut_<?=$sta["id"]?>" >
																<?php		if($sta["confirme"]){
																				echo("confirmé");
																			}else{
																				echo("Reservation");
																			}?>
																		</td>

															<?php		// CONVOC ?>
																		<td id="convoc_<?=$sta["id"]?>" class="text-center" >
																			<a href="convocation_voir.php?action=<?=$action_id?>&stagiaire=<?=$sta["id"]?>&societ=<?=$conn_soc_id?>" class="btn btn-info btn-sm" >
																				<i class="fa fa-eye" ></i>
																			</a>
																		</td>
															<?php		if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Convocations/convoc_". $action_id . "_" . $sta["id"] . ".pdf")){ ?>
																			<td id="convoc_pdf_<?=$sta["id"]?>" class="text-center" >

																					<label class="option option-dark mr5">
																						<input type="checkbox" class="convocation-<?=$d["id"]?>" name="convoc_<?=$sta["id"]?>" value="<?=$sta["id"]?>">
																						<span class="checkbox"></span>
																					</label>
																			<a href="documents/Societes/<?=$acc_societe?>/Convocations/convoc_<?=$action_id?>_<?=$sta["id"]?>.pdf?<?= time() ?>" download="Convocation <?=$sta["nom"]?>.pdf" >
																					<i class='fa fa-file-pdf-o' ></i>
																				</a>
																			</td>
															<?php		}else{ ?>
																			<td id="convoc_pdf_<?=$sta["id"]?>" class="text-center" >
																				<div class="option-group field">
																					<label class="option option-dark">
																						<input type="checkbox" class="convocation-<?=$d["id"]?>" name="convoc_<?=$sta["id"]?>" value="<?=$sta["id"]?>">
																						<span class="checkbox"></span>
																					</label>
																				</div>
																			</td>
															<?php		}
																		// ATTESTATION
																		if(!empty($d_attestations[$sta["attestation"]])){ ?>
																			<td id="attest_<?=$sta["id"]?>" ><?=$d_attestations[$sta["attestation"]]?></td>
																			<td id="cont_attest_statut_<?=$sta["id"]?>" >

																<?php			if($sta["attest_statut"]==1){ ?>
																					<button type="button" id="attest_statut_<?=$sta["id"]?>" class="btn btn-sm btn-success <?php if($sta["diplome_statut"]==0) echo("btn-attestation"); ?>" data-stagiaire="<?=$sta["id"]?>" <?php if($sta["diplome_statut"]!=0) echo("disabled"); ?> >
																						<i class="fa fa-check" ></i> Réussite
																					</button>
																<?php			}else{ ?>
																					<button type="button" id="attest_statut_<?=$sta["id"]?>" class="btn btn-sm btn-danger <?php if($sta["diplome_statut"]==0) echo("btn-attestation"); ?>" data-stagiaire="<?=$sta["id"]?>" <?php if($sta["diplome_statut"]!=0) echo("disabled"); ?> >
																						<i class="fa fa-times" ></i> Echec
																					</button>
																<?php			 } ?>
																			</td>
																			<td id="attest_pdf_<?=$sta["id"]?>" class="text-center" >
																	<?php		if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Attestations/attestation_". $action_id . "_" . $sta["id"] . ".pdf")){ ?>
																					<a href="documents/Societes/<?=$acc_societe?>/Attestations/attestation_<?=$action_id?>_<?=$sta["id"]?>.pdf?<?= time() ?>" download="<?=$sta["nom"] . " " .$d_attestations[$sta["attestation"]]?>.pdf" >
																						<i class='fa fa-file-pdf-o' ></i>
																					</a>
																	<?php		}else{ ?>
																					<div class="option-group field">
																						<label class="option option-dark">
																							<input type="checkbox" class="attestation-<?=$d["id"]?>" name="attest_<?=$sta["id"]?>" value="<?=$sta["id"]?>">
																							<span class="checkbox"></span>
																						</label>
																					</div>

																	<?php		} ?>
																			</td>
																<?php	}else{ ?>
																			<td id="attest_<?=$sta["id"]?>" >&nbsp;</td>
																			<td id="cont_attest_statut_<?=$sta["id"]?>" >&nbsp;</td>
																			<td id="attest_pdf_<?=$sta["id"]?>" class='text-center' >&nbsp;</td>
																<?php	}

																		// QUALIF
																		$url_dip="";
																		if(isset($d_qualifications[$sta["qualification"]])){
																			if($d_qualifications[$sta["qualification"]]["qua_caces"]==1){
																				if(!empty($sta["diplome"])){
																					$fa_dip="fa-eye";
																					$class_dip="btn-info";
																					$url_dip="dip_caces_voir.php?dossier=" . $sta["diplome"] . "&action=" . $action_id . "&stagiaire=" . $sta["id"] . "&societ=" . $conn_soc_id;
																				}else{
																					$fa_dip="fa-plus";
																					$class_dip="btn-success";
																					$url_dip="dip_caces.php?action=" . $action_id . "&stagiaire=" . $sta["id"] . "&qualification=" . $sta["qualification"] . "&societ=" . $conn_soc_id;
																				}
																			}
																			if($d_qualifications[$sta["qualification"]]["qua_ssiap"]==1){
																				if(!empty($sta["diplome"])){
																					$fa_dip="fa-eye";
																					$class_dip="btn-info";
																					$url_dip="dip_ssiap_voir.php?diplome=" . $sta["diplome"]. "&societ=" . $conn_soc_id;
																					//$url_edit="dip_ssiap.php?diplome=" . $sta["diplome"] . "&action=" . $action_id . "&stagiaire=" . $sta["id"] . "&qualification=" . $sta["qualification"];
																				}else{
																					$fa_dip="fa-plus";
																					$class_dip="btn-success";
																					$url_dip="dip_ssiap.php?action=" . $action_id . "&stagiaire=" . $sta["id"] . "&qualification=" . $sta["qualification"]. "&societ=" . $conn_soc_id;
																				}
																			}
																		}?>
																		<td class="text-center" >
																	<?php	if(!empty($url_dip)){ 	?>
																				<a href="<?=$url_dip?>" class="btn btn-sm <?=$class_dip?>" data-toggle="tooltip" title="Gérer le diplôme" >
																					<i class="fa <?=$fa_dip?>" ></i>
																				</a>
																				<?php if(!empty($url_edit)){ ?>
																					<a href="<?=$url_edit?>" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Modifier le dîplôme" >
																						<i class="fa fa-pencil" ></i>
																					</a>
																				<?php } ?>
																	<?php	}else{
																				echo("&nbsp;");
																			} ?>
																		</td>

																		<td>
																			<a href="mail_prep.php?action=<?=$action_id?>&stagiaire=<?=$sta["id"]?>&societ=<?=$conn_soc_id?>" class="btn btn-sm btn-info" >
																				<i class="fa fa-envelope-o" ></i>
																			</a>
																		</td>
																	</tr>

						<?php									} ?>
															</tbody>
														</table>
														<p class="alert alert-warning" id="no_sta_<?=$d["id"]?>" <?php if(!empty($d["stagiaires"])) echo("style='display:none;'"); ?> >
															Il n'y a pas de stagiaire d'inscrit.
														</p>
													</div>
												</div>
							<?php			} ?>
										</form>
									</div>
								</div>
							</div>
						</section>
					</section>
				</div>
				<footer id="content-footer" class="affix">
					<div class="row">
						<div class="col-xs-3 footer-left">
							<a href="<?=$_SESSION["retour_action"]?>" class="btn btn-default btn-sm" >
								Retour
							</a>
							<a href="planning.php" class="btn btn-default btn-sm" >
								<i class="fa fa-calendar" ></i>
							</a>
							<button class="btn btn-system btn-sm pull-right" data-toggle="modal" data-target="#modal_aide" >
								<i class="fa fa-info" ></i>
							</a>
						</div>
						<div class="col-xs-6 footer-middle text-center">
							<button type="button" class="btn btn-info btn-sm" id="btn_pdf" >
								<i class="fa fa-refresh" ></i>
								Générer les PDF
							</button>
						</div>
						<div class="col-xs-3 footer-right">
							<!--<a href="action_sta_import.php?action=<?=$action_id?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Importer des stagiaires" >
								<i class="fa fa-download"></i>
							</a>-->
							<button type="button" class="btn btn-success btn-sm btn-add" data-toggle="tooltip" data-placement="top" title="Ajouter des stagiaires" >
								<i class="fa fa-user-plus" ></i>
							</button>
						</div>
					</div>
				</footer>

				<!-- MODAL AIDE -->
				<div id="modal_aide" class="modal fade" role="dialog" >
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" id="mdl_doc_titre" >
									<i class="fa fa-info" ></i> - <span>Aide</span>
								</h4>
							</div>
							<div class="modal-body" >
								<p><strong>Attestation :</strong> cliquer sur le statut d'une attestation pour le mettre à jour.</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
									<i class="fa fa-close" ></i>
									Fermer
								</button>
							</div>
						</div>
					</div>
				</div>

				<!-- AJOUT D4UN STAGIAIRE -->
				<div id="modal_stagiaire" class="modal fade" role="dialog" >
					<div class="modal-dialog<?php if($d_action["act_gest_sta"]==1) echo(" modal-lg"); ?>" >
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" >Nouveau stagiaire</h4>
							</div>
							<div class="modal-body" >

								<form method="post" action="ajax/ajax_action_stagiaire_add.php" id="form_sta" >
									<div>
										<input type="hidden" name="action" value="<?=$action_id?>" />
										<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
										<input type="hidden" name="stagiaire" id="set_sta_id" value="0" />
									</div>
									<div class="admin-form theme-primary ">

								<?php 	$col_md=6;
										if($d_action["act_gest_sta"]==2){
											$col_md=12;
										} ?>
										<div class="row" >
											<div class="col-md-<?=$col_md?>" >
									<?php		if($d_action["act_gest_sta"]==2){ ?>
													<div class="row" >
														<div class="col-md-12" >
															<label for="sta_session_id" >Session :</label>
															<select name="session" id="sta_session_id" class="select2" >
																<option value="0">Session ...</option>
														<?php	foreach($donnees as $d){
																	echo("<option value='" . $d["id"] . "' >" . $d["libelle"] . "</option>");
																} ?>
															</select>
														</div>
													</div>
										<?php	} ?>
												<div class="row mt15" >
													<div class="col-md-12" >
														<label for="sta_action_client" >Client :</label>
														<select name="action_client" id="sta_action_client" class="select2" >
															<option value="0">Client ...</option>
													<?php	$client_id=0;
															$attestation_id=0;
															if($d_action["act_gest_sta"]==1){
																if(isset($donnees)){
																	foreach($donnees as $d){
																		echo("<option value='" . $d["id"] . "' >" . $d["libelle"] . "</option>");
																	}
																}
															} ?>
														</select>
													</div>
												</div>
												<div class="row bloc-add" >
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Stagiaire déjà enregistré</span>
														</div>
													</div>
												</div>
												<div class="row bloc-add" >
													<div class="col-md-12" >
														<label for="sta_id" class="sr-only" >Stagiaires déjà enregistrés dans la base:</label>
														<select name="sta_id" id="sta_id" class="select2-stagiaire" data-client="<?=$client_id?>" style="z-index:9999;">
															<option value="0">Stagiaire ...</option>
														</select>
													</div>
												</div>

												<div class="row bloc-add">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Nouveau Stagiaire</span>
														</div>
													</div>
												</div>
												<div class="row bloc-add" >
													<div class="col-md-4" >
														<label for="sta_nom" >Nom :</label>
														<input type="text" name="sta_nom" class="gui-input nom" id="sta_nom" placeholder="Nom" />
													</div>
													<div class="col-md-4" >
														<label for="sta_prenom" >Prénom :</label>
														<input type="text" name="sta_prenom" class="gui-input prenom" id="sta_prenom" placeholder="Prénom" />
													</div>
													<div class="col-md-4" >
														<label for="sta_naissance" >Date de naissance</label>
														<span  class="field prepend-icon">
															<input type="text" id="sta_naissance" name="sta_naissance" class="gui-input datepicker" placeholder="Date de naissance" />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-12" >
														<label for="sta_attestation_id" >Attestation :</label>
														<select name="ast_attestation" id="sta_attestation_id" class="select2" >
															<option value="0" >Attestation ...</option>
													<?php	if(!empty($d_attestations)){
																foreach($d_attestations as $att_id => $att_lib){
																	if($att_id==$attestation_id){
																		echo("<option value='" . $att_id . "' selected >" . $att_lib . "</option>");
																	}else{
																		echo("<option value='" . $att_id . "' >" . $att_lib . "</option>");
																	}
																}
															}	?>
														</select>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-12" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" id="ast_confirme" name="ast_confirme" value="on">
																<span class="checkbox"></span>Inscription confirmé
															</label>
														</div>
													</div>
												</div>
											</div>
								<?php		if($d_action["act_gest_sta"]==1){ ?>

												<div class="col-md-6" >
													<div class="section-divider mb40">
														<span>Dates</span>
													</div>
													<div>
													<div class="option-group field ml5 mb10">
														<label class="option option-dark">
															<input type="checkbox" id="checkedAll" checked="">
															<span class="checkbox"></span> Tout cocher
														</label>
													</div>
													</div>
													<table class="table" id="sta_dates" >
														<tr>
															<td class="text-center" >Pas de dates</td>
														</tr>
													</table>
												</div>
								<?php		} ?>

										</div>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
									<i class="fa fa-close" ></i>
									Fermer
								</button>
								<button type="button" class="btn btn-success btn-sm" id="btn_sub_sta" >
									<i class="fa fa-save" ></i>
									Enregistrer
								</button>
							</div>
						</div>
					</div>
				</div>

				<!-- MODAL CONFIRMATION -->
				<div id="modal_confirmation_user" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title">Confirmer ?</h4>
							</div>
							<div class="modal-body"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
									Annuler
								</button>
								<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
									Confirmer
								</button>
							</div>
						</div>
					</div>
				</div>

				<!-- MODAL ATTESTAION -->
				<div id="modal_attestation" class="modal fade" role="dialog" >
					<div class="modal-dialog" >
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" >&nbsp;</h4>
							</div>
							<div class="modal-body" >

								<form method="post" action="ajax/ajax_stagiaire_attestation_set.php" id="form_attestation" >
									<div>
										<input type="hidden" name="action" value="<?=$action_id?>" />
										<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
										<input type="hidden" name="stagiaire" id="attestation_sta" value="0" />
									</div>
									<div class="admin-form theme-primary ">

										<table class="table" id="attestation_comp" >
											<thead>
												<tr>
													<th>Compétences</th>
													<th>Acquise</th>
													<th>Non acquise</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>

									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
									<i class="fa fa-close" ></i>
									Fermer
								</button>
								<button type="submit" class="btn btn-success btn-sm" id="btn_sub_attestation" >
									<i class="fa fa-save" ></i>
									Enregistrer
								</button>
							</div>
						</div>
					</div>
				</div>

		<?php	include "includes/footer_script.inc.php"; ?>

				<script src="/vendor/plugins/select2/js/select2.min.js"></script>
				<script src="/vendor/plugins/mask/jquery.mask.js"></script>
				<script src="/assets/js/custom.js"></script>
				<script type="text/javascript" >

					var societ=parseInt("<?=$conn_soc_id?>");
					var action="<?=$action_id?>";
					var session_id=0;
					var action_client=0;
					var stagiaire=0;
					var attestation=0;
					var gestion_sta=parseInt("<?=$d_action["act_gest_sta"]?>");

					var tab_attestation=new Array();
					tab_attestation[0]="";
			<?php	if(!empty($d_attestations)){
						foreach($d_attestations as $att_id => $att_titre){ ?>
							tab_attestation["<?=$att_id?>"]="<?=$att_titre?>";
			<?php		}
					} ?>

					var tab_qualification=new Array();
					tab_qualification[0]="";
			<?php	if(!empty($d_qualifications)){
						foreach($d_qualifications as $qua_id => $qualif){ ?>
							tab_qualification["<?=$qua_id?>"]=new Array();
							tab_qualification["<?=$qua_id?>"]["qua_caces"]="<?=$qualif["qua_caces"]?>";
							tab_qualification["<?=$qua_id?>"]["qua_ssiap"]="<?=$qualif["qua_ssiap"]?>";
			<?php		}
					} ?>

					jQuery(document).ready(function(){

						////////////////// CHECK ALL
						$("#checkedAll").change(function() {
							if (this.checked) {
								$(".checkSingle").each(function() {
									this.checked=true;
								});
							} else {
								$(".checkSingle").each(function() {
									this.checked=false;
								});
							}
						});


						//////////////// FIN CHECK ALL

						$("#btn_pdf").click(function(){
							envoyer_form_ajax($("#form_sta_doc"),afficher_doc);
							$(this).html("<i class='fa fa-spinner fa-pulse fa-1x fa-fw'></i> Génération des PDF");
						});

						$(".btn-add").click(function(){

							session_id=0;
							if($(this).data("session")){
								session_id=$(this).data("session");
							}
							action_client=0;
							if($(this).data("action_client")){
								action_client=$(this).data("action_client");
							}

							ini_modal_stagiaire_add(session_id,action_client);
						});

						$(".btn-supp").click(function(){
							stagiaire=$(this).data("stagiaire");
							modal_confirmation_user("Êtes-vous sur de vouloir supprimer l'inscription de ce stagiaire ?",confirme_supp,stagiaire,"","");
						});

						$(".btn-edit").click(function(){
							stagiaire=$(this).data("stagiaire");
							get_action_stagiaire(action,stagiaire,ini_modal_stagiaire_edit,"","");

						});

						$(".btn-attestation").click(function(){
							stagiaire=$(this).data("stagiaire");
							get_stagiaire_attestation(action,stagiaire,ini_modal_attestation,stagiaire,"");
						});

						//*******************************
						//	INTERCATION MODAL STAGIAIRE
						//*******************************

						$("#sta_session_id").change(function(){
							if($(this).val()>0){

								get_action_clients(action,$(this).val(),actualiser_select2,"#sta_action_client",action_client);
							}else{
								actualiser_select2("","#sta_action_client","");
							}
						});

						$("#sta_action_client").change(function(){
							action_client=$(this).select2("val");
							if(action_client>0){
								console.log("-> get_action_client()");
								get_action_client(action_client,stagiaire,actualiser_info_client,"","");
							}else{
								actualiser_info_client("");
							}
						});
						// envoie du form stagiaire
						$("#btn_sub_sta").click(function(){
							if($("#set_sta_id").val()>0){
								envoyer_form_ajax($("#form_sta"),modifier_stagiaire);
							}else{
								envoyer_form_ajax($("#form_sta"),ajouter_stagiaire);
							}
							$("#modal_stagiaire").modal("hide");
						});

						//*******************************
						//	INTERCATION MODAL ATTESTATION
						//*******************************
						$("#btn_sub_attestation").click(function(){
							envoyer_form_ajax($("#form_attestation"),actualiser_attestation);
							$("#modal_attestation").modal("hide");
						});


						/*$(".select2").select2();

						// ajout
						$(".session").change(function(){
							get_session_clients(action,$(this).val(),actualiser_client);

						});

						$('.stagiaire').change(function(){
							if($(this).val()>0){
								$('.champ-nouveau').val("");
							}
						});


						$("#naissance").datepicker({
							prevText: '<i class="fa fa-chevron-left"></i>',
							nextText: '<i class="fa fa-chevron-right"></i>',
							showButtonPanel: false,
							beforeShow: function(input, inst) {
								var newclass = 'admin-form';
								var themeClass = $(this).parents('.admin-form').attr('class');
								var smartpikr = inst.dpDiv.parent();
								if (!smartpikr.hasClass(themeClass)) {
									inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
								}
							}
						});

						$('.champ-nouveau').click(function(){
							$('.stagiaire').select2("val",0);
						});

						$('#form_add_stagiaire').on('submit', function(e) {
							e.preventDefault();
							inscrire_stagiaire($(this),afficher_stagiaire);
						});

						// suppression
						$('.stagiaire-supp').click(function(){
							texte="Êtes-vous sûr de vouloir désinscrire ce stagiaire ?";
							modal_confirmation_user(texte,del_action_stagiaire,$(this));
						});

						// confirmer inscription
						$('.stagiaire-confirme').click(function(){
							var checked=$(this).prop("checked");
							var stagiaire=$(this).data("stagiaire");
							console.log("checked : " + checked);
							$.ajax({
								url : 'ajax/ajax_set_stagiaire_confirme.php',
								type : 'POST',
								data : 'action=' + action + '&stagiaire=' + stagiaire + '&checked=' + checked,
								dataType : 'html',
								success : function(code_html, statut){
									$(".stagiaire_" + stagiaire).prop("checked",checked);
								},
								error : function(resultat, statut, erreur){
									console.log("ERROR");
									$(this).prop("checked",!checked);
								}
							});
						});

						// ----- FIN DOC READY -----
					});*/
					});
					(jQuery);

					function ini_modal_stagiaire_add(session_id,action_client){

						$("#form_sta").prop("action","ajax/ajax_action_stagiaire_add.php");
						$("#modal_stagiaire .modal-title").html("Nouveau stagiaire");
						$("#set_sta_id").val(0);
						$(".bloc-add").show();

						attestation=0;
						stagiaire=0;

						if(session_id>0){
							// on va chercher les clients qui participe à la session selection
							$("#sta_session_id").val(session_id).trigger("change");
						}else if(action_client>0){
							$("#sta_action_client").val(action_client).trigger("change");
						}else{
							$("#sta_session_id").val(0).trigger("change");
							//$("#sta_action_client").val(0).trigger("change");
						}

						$("#sta_id").select2("val",stagiaire);
						$("#sta_nom").val("");
						$("#sta_prenom").val("");
						$("#sta_naissance").val("");

						afficher_modal_stagiaire();
					}

					function ini_modal_stagiaire_edit(json){

						$("#form_sta").prop("action","ajax/ajax_action_stagiaire_set.php");
						$("#modal_stagiaire .modal-title").html("Inscription de " + json.sta_prenom + " " + json.sta_nom);

						$("#set_sta_id").val(json.sta_id);

						attestation=json.ast_attestation;
						action_client=json.ast_action_client;
						stagiaire=json.sta_id;



						if(gestion_sta==2){
							$("#sta_session_id").val(json.session_id).trigger("change");
						}else{
							$("#sta_action_client").val(action_client).trigger("change");
						}
						$("#sta_attestation_id").val(attestation).trigger("change");


						if(json.ast_confirme==1){
							$("#ast_confirme").prop("checked",true);
						}else{
							$("#ast_confirme").prop("checked",false);
						}
						$(".bloc-add").hide();

						afficher_modal_stagiaire();

					}

					function afficher_modal_stagiaire(){
						$("#modal_stagiaire").modal("show");
					}

					function actualiser_info_client(json){
						console.log("actualiser_info_client()");
						if(json){
							console.log("json :" + json);
							console.log("Client :" + json.acl_client);
							$("#sta_id").data("client",json.acl_client);
							if(attestation==0){
								$("#sta_attestation_id").val(json.acl_attestation).trigger("change");
							}
							actualiser_session(json.sessions);
						}else{
							$("#sta_id").data("client",0);
							$("#sta_attestation_id").val(0).trigger("change");
							actualiser_session("");
						}
					}

					function ajouter_stagiaire(json){
						if(json){

							if(json.session>0){
								inscription=json.session;
							}else{
								inscription=json.action_client;
							}

							// ON CONSTRUIT LA LIGNE
							html="<tr id='stagiaire_" + json.stagiaire + "' data-inscription='" + inscription + "' >";
								html=html + "<td>";
									html=html + "<button type='button' class='btn btn-sm btn-danger btn-supp' id='btn_supp_" + json.stagiaire + "' data-stagiaire='" + json.stagiaire + "' >";
										html=html + "<i class='fa fa-times' ></i>";
									html=html + "</button>";
								html=html + "</td>";
								html=html + "<td>";
									html=html + "<button type='button' class='btn btn-sm btn-warning' id='btn_edit_" + json.stagiaire + "' data-stagiaire='" + json.stagiaire + "' >";
										html=html + "<i class='fa fa-pencil' ></i>";
									html=html + "</button>";
								html=html + "</td>";
								html=html + "<td id='nom_" + json.stagiaire + "' >" + json.nom + " " + json.prenom + "</td>";
								html=html + "<td id='statut_" + json.stagiaire + "' >";
									if(json.confirme==1){
										html=html + "Confirmé";
									}else{
										html=html + "Reservation";
									}
								html=html + "</td>";

								// CONVOC
								html=html + "<td id='convoc_" + json.stagiaire + "' class='text-center' >";
									html=html + "<a href='convocation_voir.php?action=<?=$action_id?>&stagiaire=" + json.stagiaire + "&societ=" + societ + "' class='btn btn-info btn-sm' >";
										html=html + "<i class='fa fa-eye' ></i>";
									html=html + "</a>";
								html=html + "</td>";
								html=html + "<td id='convoc_pdf_" + json.stagiaire + "' class='text-center' >";
									html=html + "<div class='option-group field'>";
										html=html + "<label class='option option-dark'>";
											html=html + "<input type='checkbox' class='convocation-" + inscription + "' name='convoc_" + json.stagiaire + "' value='" + json.stagiaire + "' >";
											html=html + "<span class='checkbox' ></span>";
										html=html + "</label>";
									html=html + "</div>";
								html=html + "</td>";

								// ATTESTATION
								if(json.attestation>0){

									html=html + "<td id='attest_" + json.stagiaire + "' >";
										html=html + tab_attestation[json.attestation];
									html=html + "</td>";

									html=html + "<td id='cont_attest_statut_" + json.stagiaire + "' >";
										html=html + "<button type='button' id='attest_statut_" + json.stagiaire + "' class='btn btn-sm btn-success btn-attestation' data-stagiaire='" + json.stagiaire + "' >";
											html=html + "<i class='fa fa-check' ></i> Réussite";
										html=html + "</button>";
									html=html + "</td>";

									html=html + "<td id='attest_pdf_" + json.stagiaire + "' class='text-center' >";
										html=html + "<div class='option-group field' >";
												html=html + "<label class='option option-dark' >";
													html=html + "<input type='checkbox' class='attestation-" + inscription + "' name='attest_" + json.stagiaire + "' value='" + json.stagiaire + "' >";
													html=html + "<span class='checkbox' ></span>";
												html=html + "</label>";
											html=html + "</div>";
									html=html + "</td>";

								}else{
									html=html + "<td id='attest_" + json.stagiaire + "' >&nbsp;</td>";
									html=html + "<td id='cont_attest_statut_" + json.stagiaire + "' >&nbsp;</td>";
									html=html + "<td id='attest_pdf_" + json.stagiaire + "' class='text-center' >&nbsp;</td>";
								}

								// QUALIFICATION

								url_qualif="";
								if(json.qualification>0){
									if(tab_qualification[json.qualification]["qua_caces"]==1){
										url_qualif="dip_caces.php?action=<?=$action_id?>&stagiaire=" + json.stagiaire + "&qualification=" + json.qualification + "&societ=" + societ;
									}else if(tab_qualification[json.qualification]["qua_ssiap"]==1){
										url_qualif="dip_ssiap.php?action=<?=$action_id?>&stagiaire=" + json.stagiaire + "&qualification=" + json.qualification + "&societ=" + societ;
									}
								}
								console.log("url_qualif " + url_qualif);

								html=html + "<td class='text-center' >";
									if(url_qualif!=""){
										html=html + "<a href=" + url_qualif + " class='btn btn-sm btn-success' data-toggle='tooltip' title='Gérer le diplôme' <?php if($_SESSION['acces']['acc_service'][3]!=1 AND $acc_societe!=12) echo("disabled") ?> >";
											html=html + "<i class='fa fa-plus' ></i>";
										html=html + "</a>";
									}else{
										html=html + "&nbsp;";
									}
								html=html + "</td>";

								// MAIL

								html=html + "<td>";
									html=html + "<a href='mail_prep.php?action=" + action + "&stagiaire=" + json.stagiaire + "&societ=" + societ + "' class='btn btn-sm btn-info' >";
										html=html + "<i class='fa fa-envelope-o' ></i>";
									html=html + "</a>";
								html=html + "</td>";

							html=html + "</tr>";

							nb_sta=$("#inscription_" + inscription + " tr").length;
							if(nb_sta==0){
								$("#no_sta_" + inscription).hide();
								$("#table_sta_" + inscription).show();
								$("#inscription_" + inscription).html(html);
							}else{
								$("#inscription_" + inscription).append(html);
							}

							$("#btn_supp_" + json.stagiaire).click(function(){
								stagiaire=$(this).data("stagiaire");
								modal_confirmation_user("Êtes-vous sur de vouloir supprimer l'inscription de ce stagiaire ?",confirme_supp,stagiaire,"","");
							});

							$("#btn_edit_" + json.stagiaire).click(function(){
								stagiaire=$(this).data("stagiaire");
								get_action_stagiaire(action,stagiaire,ini_modal_stagiaire_edit,"","");
							});
							$("#attest_statut_" + json.stagiaire).click(function(){
								stagiaire=$(this).data("stagiaire");
								get_stagiaire_attestation(action,stagiaire,ini_modal_attestation,stagiaire,"");
							});
						}
					}

					function afficher_doc(json){
						if(json){
							if(json.attestations){
								$.each(json.attestations, function(i,sta_id){
									sta_nom=$("#nom_" + sta_id).html();
									attest_nom=$("#attest_" + sta_id).html();
									html="<a href='documents/Societes/<?=$acc_societe?>/Attestations/attestation_<?=$action_id?>_" + sta_id + ".pdf?<?= time() ?>' download='" + sta_nom + " " + attest_nom + ".pdf' >";
										html=html + "<i class='fa fa-file-pdf-o' ></i>";
									html=html + "</a>";
									$("#attest_pdf_" + sta_id).html(html);
								});
							}
							if(json.convocations){
								$.each(json.convocations, function(i,sta_id){
									sta_nom=$("#nom_" + sta_id).html();
									html="<a href='documents/Societes/<?=$acc_societe?>/Convocations/convoc_<?=$action_id?>_" + sta_id + ".pdf?<?= time() ?>' download='Convocation " + sta_nom + ".pdf' >";
										html=html + "<i class='fa fa-file-pdf-o' ></i>";
									html=html+ "</a>";
									$("#convoc_pdf_" + sta_id).html(html);
								});
							}
						}
						$("#btn_pdf").html("<i class='fa fa-refresh' ></i> Générer les PDF");
					}

					function confirme_supp(stagiaire){
						$.ajax({
							type:'POST',
							url: 'ajax/ajax_action_stagiaire_del.php',
							data : 'action=' + action + '&stagiaire=' + stagiaire + "&societ=" + societ,
							dataType: 'JSON',
							success: function(data){
								supprimer_stagiaire(stagiaire);

							},
							error: function(data){
								modal_alerte_user(data.responseText,"");
							}
						});
					}

					function get_action_stagiaire(action,stagiaire,callback,param1,param2){
						$.ajax({
							type:'POST',
							url: 'ajax/ajax_action_stagiaire_get.php',
							data : 'action=' + action + '&stagiaire=' + stagiaire + "&societ=" + societ,
							dataType: 'JSON',
							success: function(data){
								callback(data,param1,param2);
							},
							error: function(data){
								modal_alerte_user(data.responseText,"");
							}
						});
					}

					function modifier_stagiaire(json){
						if(json){
							if(json["suppression"]["action_client"]!=0 || json["suppression"]["session"]!=0){
								supprimer_stagiaire(json["stagiaire"]);
								ajouter_stagiaire(json);
							}else{
								if(json.confirme==1){
									$("#statut_" + json.stagiaire).html("Confirmé");
								}else{
									$("#statut_" + json.stagiaire).html("Reservation");
								};

								if(json.attest_modif==1){
									if(json.attestation>0){

										var html="";

										$("#attest_" + json.stagiaire).html(tab_attestation[json.attestation]);

										html="<button type='button' id='attest_statut_" + json.stagiaire + "' class='btn btn-sm btn-success btn-attestation' data-stagiaire='" + json.stagiaire + "' >";
											html=html + "<i class='fa fa-check' ></i> Réussite";
										html=html + "</button>";

										$("#cont_attest_statut_" + json.stagiaire).html(html);
										$("#attest_statut_" + json.stagiaire).click(function(){
											stagiaire=$(this).data("stagiaire");
											get_stagiaire_attestation(action,stagiaire,ini_modal_attestation,stagiaire,"");
										});

										if(json.session>0){
											inscription=json.session
										}else{
											inscription=json.action_client
										}

										html="";
										html + "<div class='option-group field' >";
											html=html + "<label class='option option-dark' >";
												html=html + "<input type='checkbox' class='attestation-" + inscription + "' name='attest_" + json.stagiaire + "' value='" + json.stagiaire + "' >";
												html=html + "<span class='checkbox' ></span>";
											html=html + "</label>";
										html=html + "</div>";

										$("#attest_pdf_" + json.stagiaire).html(html);
									}else{
										$("#attest_" + json.stagiaire).html("");
										$("#cont_attest_statut_" + json.stagiaire).html("");
										$("#attest_pdf_" + json.stagiaire).html("");
									}
								}
							}
						}
					}

					function supprimer_stagiaire(stagiaire){
						inscription=$("#stagiaire_" + stagiaire).data("inscription");

						$("#stagiaire_" + stagiaire).remove();

						nb_sta=$("#inscription_" + inscription + " tr").length;
						if(nb_sta==0){
							$("#table_sta_" + inscription).hide();
							$("#no_sta_" + inscription).show();
						}
					}

					function actualiser_session(json){
						var html="";
						if(json){
							$.each(json, function(i, obj){

								lib_date=obj.int_label_1 + " " + obj.pda_date_fr;
								if(obj.ase_h_deb!="" && obj.ase_h_fin!=""){
									lib_date=lib_date+ " " + obj.ase_h_deb + " / " + obj.ase_h_fin;
								}else if(obj.pda_demi==1){
									lib_date=lib_date + " matin";
								}else{
									lib_date=lib_date + " après-midi";
								}

								html=html + "<tr>";
									html=html + "<td>"
										html=html + "<div class='option-group field' >";
											html=html + "<label class='option option-dark' >";
												if(obj.ass_session!=null || stagiaire==0){
													html=html + "<input type='checkbox' class='checkSingle' name='sessions[][ase_id]' value='" + obj.ase_id + "' checked >";
												}else{
													html=html + "<input type='checkbox' class='checkSingle' name='sessions[][ase_id]' value='" + obj.ase_id + "' >";
												}
												html=html + "<span class='checkbox'></span>";
											html=html + "</label>";
										html=html + "</div>";

									html=html + "</td>";
									html=html + "<td>" + lib_date + "</td>";
								html=html + "</tr>";

							});

						}else{
							html=html + "<tr>";
									html=html + "<td class='text-center' >Pas de date</td>";
							html=html + "</tr>";
						}
						$("#sta_dates").html(html);
						checkSingle();
					}

					// ATTESTATIONS
					function get_stagiaire_attestation(action,stagiaire,callback,param1,param2){
						$.ajax({
							type:'POST',
							url: 'ajax/ajax_stagiaire_attestation_get.php',
							data : 'action=' + action + '&stagiaire=' + stagiaire + "&societ=" + societ,
							dataType: 'JSON',
							success: function(data){
								callback(data,param1,param2);
							},
							error: function(data){
								modal_alerte_user(data.responseText,"");
							}
						});
					}

					function ini_modal_attestation(json,stagiaire){
						var html="";
						if(json){

							$("#modal_attestation .modal-title").html("Attestation : " + json.titre);

							$("#attestation_sta").val(stagiaire);

							$.each(json.competences, function(i, obj){

								html=html + "<tr>";
									html=html + "<td>" + obj.competence + "</td>";
									html=html + "<td class='text-center'>";
										html=html + "<div class='radio-custom mb5' >";
											html=html + "<input id='comp_acquise_" + i + "' name='competence_" + i + "' type='radio' value='1'";
											if(obj.acquise==1){
												html=html + " checked ";
											}
											html=html + "/>";
											html=html + "<label for='comp_acquise_" + i + "' ></label>";
										html=html + "</div>";
									html=html + "</td>";
									html=html + "<td class='text-center'>";
										html=html + "<div class='radio-custom mb5' >";
											html=html + "<input id='comp_non_acquise_" + i + "' name='competence_" + i + "' type='radio' value='0'";
											if(obj.acquise==0){
												html=html + " checked ";
											}
											html=html + "/>";
											html=html + "<label for='comp_non_acquise_" + i + "' ></label>";
										html=html + "</div>";
									html=html + "</td>";
								html=html + "</tr>";
							});

						}else{
							html=html + "<tr>";
									html=html + "<td class='text-center' colspan='3' >Pas de date</td>";
							html=html + "</tr>";
						}
						$("#attestation_comp tbody").html(html);

						$("#modal_attestation").modal("show");

					}

					function actualiser_attestation(json){
						if(json){
							if(json.reussite==1){
								if($("#attest_statut_" + json.stagiaire).hasClass("btn-danger")){
									$("#attest_statut_" + json.stagiaire).removeClass("btn-danger");
									$("#attest_statut_" + json.stagiaire).addClass("btn-success");
									$("#attest_statut_" + json.stagiaire).html("<i class='fa fa-check' ></i> Réussite");
								}
							}else{
								if($("#attest_statut_" + json.stagiaire).hasClass("btn-success")){
									$("#attest_statut_" + json.stagiaire).removeClass("btn-success");
									$("#attest_statut_" + json.stagiaire).addClass("btn-danger");
									$("#attest_statut_" + json.stagiaire).html("<i class='fa fa-times' ></i> Echec");
								}
							}

							// la maj d'une attestation supp le fichier PDF

							var html="";

							html="<div class='option-group field' >";
								html=html + "<label class='option option-dark' >";
									html=html + "<input type='checkbox' class='attest-pdf' name='attest_" + json.stagiaire + "' value='" + json.stagiaire + "' >";
									html=html + "<span class='checkbox' ></span>";
								html=html + "</label>";
							html=html + "</div>";

							$("#attest_pdf_" + json.stagiaire).html(html);
						}
					}

					function checkSingle(){
						$(".checkSingle").click(function () {
							if ($(this).is(":checked")) {
								var isAllChecked = 0;

								$(".checkSingle").each(function() {
									if (!this.checked)
										isAllChecked = 1;
								});

								if (isAllChecked == 0) {
									$("#checkedAll").prop("checked", true);
								}
							}
							else {
								$("#checkedAll").prop("checked", false);
							}
						});
					}
				</script>
			</body>
		</html>
<?php
	} ?>
