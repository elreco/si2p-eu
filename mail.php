﻿<?php

	session_start();
	include "includes/connexion.php";
	include "includes/connexion_soc.php";
	include "includes/connexion_fct.php";
	include "modeles/mod_envoi_mail.php";
	include "modeles/mod_parametre.php";

	$erreur_txt="";
	$inter ="";
	$adr=array();

	if(!empty($_POST)){

		if(!empty($_POST["destinataire"])){
			foreach($_POST["destinataire"] as $dest){
				$val_ad=explode(";",$dest);
				if(!empty($val_ad[1])){
					$adr[]=array(
						"nom" => $val_ad[0],
						"adresse" => $val_ad[1]
					);
				}
			}
		}
		if(!empty($_POST["destinatiaire_libre"])){
			$destinatiaire_libre=explode(";",$_POST["destinatiaire_libre"]);
			foreach($destinatiaire_libre as $dest){
				if(!empty($dest)){
					$adr[]=array(
						"nom" => "",
						"adresse" => $dest
					);
				}
			}
		}

		// RELANCE FACTURE

		if(!empty($_POST['facture_relance'])){

			$nouvelle_relance=false;
			$facture_relance=$_POST['facture_relance'];
			$sql="SELECT * FROM Relances WHERE rel_id=" . $facture_relance . ";";
			$req=$Conn->query($sql);
			$d_relance=$req->fetch();
			$rel_etat_relance=$d_relance['rel_etat_relance'];

			// SI C'EST UNE MED
			if($rel_etat_relance == 5){
				if(!empty($_POST["pj"])){
					foreach($_POST["pj"] as $pj){
						if(!empty($_POST["facture_" . $pj])){

							// SI C'EST UNE MED
							$fac_id = $_POST["facture_" . $pj];
							$soc = $_POST["facture_soc_" . $pj];
							$ConnFct = connexion_fct($soc);
							// SOCIETE
							$sql="SELECT soc_responsable FROM Societes WHERE soc_id=" . $soc;
						    $req = $Conn->query($sql);
						    $d_societe=$req->fetch();

							$sql="SELECT fac_commercial FROM Factures WHERE fac_id=" . $fac_id;
						    $req = $ConnFct->query($sql);
						    $d_facture_search=$req->fetch();

							$sql="SELECT com_mail, com_label_1, com_label_2 FROM Commerciaux WHERE com_id=" . $d_facture_search['fac_commercial'];
							$req=$ConnFct->query($sql);
							$d_commercial=$req->fetch();

							if(!empty($d_facture_search['fac_commercial'])){
								$sql="SELECT com_mail, com_label_1, com_label_2, com_ref_1 FROM Commerciaux WHERE com_id=" . $d_facture_search['fac_commercial'];
								$req=$ConnFct->query($sql);
								$d_commercial=$req->fetch();
								if(!empty($d_commercial['com_mail'])){
									$adr[]=array(
										"nom" => $d_commercial['com_label_2'] . " " . $d_commercial['com_label_1'],
										"adresse" => $d_commercial['com_mail']
									);

								}elseif(!empty($d_commercial['com_ref_1'])){
									$sql="SELECT uti_mail, uti_prenom, uti_nom, uti_id FROM Utilisateurs WHERE uti_id=" . $d_commercial['com_ref_1'];
									$req=$Conn->query($sql);
									$d_utilisateur_commercial=$req->fetch();
									if(!empty($d_utilisateur_commercial['uti_mail']) && $d_utilisateur_commercial['uti_id']!=98){
										$adr[]=array(
											"nom" => $d_utilisateur_commercial['uti_prenom'] . " " . $d_utilisateur_commercial['uti_nom'],
											"adresse" => $d_utilisateur_commercial['uti_mail']
										);
									}
								}



							}
							if(!empty($d_societe['soc_responsable'])){
								$sql="SELECT uti_id,uti_mail, uti_prenom, uti_nom FROM Utilisateurs WHERE uti_id=" . $d_societe['soc_responsable'];
								$req=$Conn->query($sql);
								$d_responsable=$req->fetch();
								if(!empty($d_responsable['uti_mail']) && $d_responsable['uti_id']!=98){
									$adr[]=array(
										"nom" => $d_responsable['uti_prenom'] . " " . $d_responsable['uti_nom'],
										"adresse" => $d_responsable['uti_mail']
									);
								}
							}

						}
					}
				}
				// FIN SI C UNE MED
			}

			if($d_relance['rel_envoie'] AND $d_relance['rel_envoie_uti'] != $_SESSION['acces']['acc_ref_id']){
				$nouvelle_relance=true;

				$rel_comment="Message auto // Nouvel envoi de mail d'après la correspondance enregistrée par " . $d_relance['rel_uti_identite'] . " le " . convert_date_txt($d_relance['rel_date']) . ".";

				$rel_etat_relance=0;
			}

			$actu_fac=true;
		    $ret_traitement = 0;
		    $ret_libelle="";

		    $sql="SELECT ret_id,ret_traitement,ret_libelle FROM Relances_Etats ORDER BY ret_j_deb;";
		    $req = $Conn->query($sql);
		    $d_relances=$req->fetchAll();
		    $tab_relance = array();
		    $tab_relance[0] = $actu_fac;
		    foreach($d_relances as $d){
		        if($d['ret_id'] == $rel_etat_relance){
		            $actu_fac=false;
		            $ret_traitement = $d['ret_traitement'];
		            $ret_libelle = $d['ret_libelle'];
		        }
		        $tab_relance[$d['ret_id']] = $actu_fac;
		    }

		    if($nouvelle_relance){
		    	$sql="SELECT uti_nom, uti_prenom FROM Utilisateurs WHERE uti_id = " . $_SESSION['acces']['acc_ref_id'];
				$req = $Conn->query($sql);
				$utilisateur=$req->fetch();
		    	$sql="INSERT INTO Relances (rel_client, rel_opca, rel_utilisateur,rel_uti_identite, rel_etat_relance, rel_date, rel_comment, rel_con_nom)
			    VALUES (:rel_client, :rel_opca, :rel_utilisateur,:rel_uti_identite, :rel_etat_relance, NOW(), :rel_comment,:rel_con_nom)";
			    $req = $Conn->prepare($sql);
			    $req->bindValue(":rel_client",$d_relance['rel_client']);
			    $req->bindValue(":rel_opca",$d_relance['rel_opca']);
			    $req->bindValue(":rel_utilisateur",$_SESSION['acces']['acc_ref_id']);
			    $req->bindValue(":rel_uti_identite",$utilisateur['uti_prenom'] . " " . $utilisateur['uti_nom']);
			    $req->bindValue(":rel_etat_relance",0);
			    $req->bindValue(":rel_comment",$rel_comment);
			    if(!empty($adr[0]["nom"])){
			    	$req->bindValue(":rel_con_nom",$adr[0]["nom"]);
			    }else{
			    	$req->bindValue(":rel_con_nom","");
			    }
				/* var_dump($req);
				die(); */
			    $req->execute();
		    }

		}
	}

	if(empty($adr)){
		$erreur_txt="Aucun destinataire renseigné!";
	}

	if(empty($erreur_txt)){


		// GENERATION DU LIEN AUTOMATIQUE

		if(!empty($_POST['accuse'])){
			// A CHANGER EN LIGNE
			$lien_auto = "http://" . $_SERVER['SERVER_NAME'] . "/accuse.php?societ=" . $_POST['conn_soc_id'] . "&token=" . urlencode(my_encrypt($_POST['action_client_id'], 'orion'));
			$body_mail =$_POST["texte"];
			$body_mail .= "<br><a href='" . $lien_auto . "'>Confirmer la réception</a><br>";
			$body_mail .= "<br>" . $_POST["texte_bis"];

		}else{
			$body_mail=$_POST["texte"];
		}

		$param=array(
			"sujet" => $_POST["sujet"],
			"body_mail" => $body_mail,
			"pj" => array(),
			"copie_cache" => array()
		);

		if(!empty($_POST["pj"])){
			foreach($_POST["pj"] as $pj){
				if(!empty($_POST["pj_url_" . $pj])){
					$param["pj"][]=array(
						"fichier" => $_POST["pj_url_" . $pj],
						"nom" => $_POST["pj_nom_" . $pj]
					);
				}
			}
		}


		if(!empty($_POST["copie"]) OR !empty($_POST["signature"])){

			$acc_utilisateur=0;
			if(!empty($_SESSION['acces']["acc_ref"])){
				if($_SESSION['acces']["acc_ref"]==1){
					$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
				}
			}

			$req=$Conn->query("SELECT uti_nom,uti_prenom,uti_fonction,uti_mail,uti_tel,uti_mobile
			,soc_logo_1,soc_qualite_1,soc_qualite_2,soc_site,soc_nom
			,age_logo_1,age_qualite_1,age_qualite_2,age_site
			FROM Utilisateurs INNER JOIN Societes ON (Utilisateurs.uti_societe=Societes.soc_id)
			LEFT JOIN Agences ON (Utilisateurs.uti_agence=Agences.age_id) WHERE uti_id=" . $acc_utilisateur . ";");
			$d_utilisateur=$req->fetch();
			if(!empty($d_utilisateur)){

				if(!empty($d_utilisateur["uti_mail"]) AND !empty($_POST["copie"])){

					$param["copie_cache"]=array(
						"nom" => $d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"],
						"adresse" => $d_utilisateur["uti_mail"]
					);
				}

				if(!empty($_POST["signature"])){


					if(!empty($d_utilisateur["age_logo_1"])){
						$logo_1=$d_utilisateur["age_logo_1"];
					}else{
						$logo_1=$d_utilisateur["soc_logo_1"];
					}

					$qualite_1="";
					if(!empty($d_utilisateur["age_qualite_1"])){
						$qualite_1=$d_utilisateur["age_qualite_1"];
					}elseif(!empty($d_utilisateur["soc_qualite_1"])){
						$qualite_1=$d_utilisateur["soc_qualite_1"];
					}

					$qualite_2="";
					if(!empty($d_utilisateur["age_qualite_2"])){
						$qualite_2=$d_utilisateur["age_qualite_2"];
					}elseif(!empty($d_utilisateur["soc_qualite_2"])){
						$qualite_2=$d_utilisateur["soc_qualite_2"];
					}

					$site="";
					if(!empty($d_utilisateur["age_site"])){
						$site=$d_utilisateur["age_site"];
					}elseif(!empty($d_utilisateur["soc_site"])){
						$site=$d_utilisateur["soc_site"];
					}

					$signature="<div style='color:#7e8082;font-family:Century Gothic,Verdana;font-size:11pt;width:350px;' >";
						$signature.="<div style='border-bottom:1px solid #e31936;padding:5px;' >";
							$signature.="<img src='https://www.si2p.eu/Documents/Societes/logos/" . $logo_1 . "' style='margin:5px;max-width:180px;' />";
							$signature.="<div style='text-align:right;' >";
								$signature.="<div style='margin-bottom:5px;font-size:11pt;' ><b>" . $d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"] . "</b></div>";
								$signature.="<div style='color:#e31936;margin-bottom:5px;' >" . $d_utilisateur["uti_fonction"] . "</div>";
								if(!empty($d_utilisateur["uti_tel"])){
									$signature.="<div>" . $d_utilisateur["uti_tel"] . "</div>";
								}
								if(!empty($d_utilisateur["uti_mobile"])){
									$signature.="<div>" . $d_utilisateur["uti_mobile"] . "</div>";
								}
								$signature.="<div>";
									if(!empty($d_utilisateur["uti_mail"])){
										$signature.="<a href='mailto:" . $d_utilisateur["uti_mail"] . "' style='color:#2d589e;font-size:11pt;text-decoration:underline;font-family:Century Gothic;' >";
											$signature.=$d_utilisateur["uti_mail"];
										$signature.="</a>";
										$signature.="- ";
									}
									if(!empty($site)){
										$signature.="<a style='color:#2d589e;font-size:11pt;font-weight:bold;text-decoration:underline;font-family:Century Gothic;' href='" . $site . "'>";
											$signature.=$site;
										$signature.="</a>";
									}
								$signature.="</div>";
							$signature.="</div>";
						$signature.="</div>";
						$signature.="<div style='background-color:#efefef;font-size:10pt;padding:5px;text-align:center;' >";
							$signature.="<b>" . $d_utilisateur["soc_nom"] . "</b> <span style='color:#e31936;' > | </span> formation <span style='color:#e31936;'> | </span> sécurité <span style='color:#e31936;'> | </span> prévention";
						$signature.="</div>";
					$signature.="</div>";
					if(!empty($qualite_1) OR !empty($qualite_2)){
						$signature.="<div style='width:350px;' >";
							if(!empty($qualite_1)){
								$signature.="<div style='float:left;margin-top:27px;' >";
									$signature.="<img src='https://www.si2p.eu/Documents/Societes/logos/" . $qualite_1 . "' />";
								$signature.="</div>";
							}
							if(!empty($qualite_2)){
								$signature.="<div style='float:left;' >";
									$signature.="<img src='https://www.si2p.eu/Documents/Societes/logos/" . $qualite_2 . "' />";
								$signature.="</div>";
							}

							$signature.="<div style='clear:both;' > </div>";
						$signature.="</div>";
					}

					$param["body_mail"].=$signature;
				}

			}


		}
		if(!empty($_POST['facture_relance'])){
			$rel_email=1;
		}else{
			$rel_email=0;
		}
		if(!empty($_POST['inter'])){
			$inter = $_POST['inter'];
		}
		$envoie_ok=envoi_mail("",$param,$adr, $rel_email, $inter);
		if(!is_bool($envoie_ok)){
			$erreur_txt=$envoie_ok;
		}

	}

	if(!empty($erreur_txt)){
		$_SESSION['message'][]= array(
			"titre" => "Echec de l'envoi",
			"type" => "danger",
			"message" => addslashes($erreur_txt)
		);
	}else{
		// AJOUTER DR ET COMMERCIAL

		if(!empty($_POST['facture_relance'])){
			$rel_courrier=$d_relance['rel_courrier'];
			$sql="UPDATE Relances SET rel_envoie=:rel_envoie,rel_envoie_uti=:rel_envoie_uti,
			rel_envoie_date=NOW(),rel_envoie_dest=:rel_envoie_dest
			 WHERE rel_id =" . $_POST['facture_relance'];
			$req = $Conn->prepare($sql);
			$req->bindValue(":rel_envoie",true);
			$req->bindValue(":rel_envoie_uti",$_SESSION['acces']['acc_ref_id']);
			if(!empty($adr[0]["adresse"])){
		    	$req->bindValue(":rel_envoie_dest",$adr[0]["adresse"]);
		    }else{
		    	$req->bindValue(":rel_envoie_dest","");
		    }
		    $req->execute();
		    if(!empty($_POST["pj"])){
				foreach($_POST["pj"] as $pj){
					if(!empty($_POST["facture_" . $pj])){
						$fac_id = $_POST["facture_" . $pj];
						$soc = $_POST["facture_soc_" . $pj];
						$ConnFct = connexion_fct($soc);
						if($d_relance['rel_etat_relance']>0){
							if($rel_etat_relance != 5 OR $rel_courrier){
								$sql="UPDATE relances_Factures SET rfa_etat_relance=" . $d_relance['rel_etat_relance'] . "
								 WHERE rfa_relance =" . $_POST['facture_relance'] . " AND rfa_facture=" . $fac_id . " AND rfa_facture_soc=" . $soc;
								 $req = $Conn->prepare($sql);
								 $req->execute();
								 $sql="UPDATE Factures SET fac_etat_relance=" . $d_relance['rel_etat_relance'] . ",fac_date_relance=NOW() WHERE fac_id=" . $fac_id;
								 $req = $ConnFct->prepare($sql);
								 $req->execute();
							}
						}



					}
				}
			}

		}
		// AVIS DE PASSAGE
		if(!empty($_POST['accuse'])){
			$ConnFct = connexion_fct($_POST['conn_soc_id']);
			$sql="UPDATE actions_clients SET acl_avis_emis = 1, acl_avis_emis_date = NOW(), acl_avis_recu = 0 WHERE acl_id = " . $_POST['action_client_id'];
			$req = $ConnFct->prepare($sql);
			$req->execute();
		}
		if(empty($_POST['facture_relance'])){
			$_SESSION['message'][]= array(
				"titre" => "Mail envoyé",
				"type" => "success",
				"message" => "Votre mail a bien été envoyé!"
			);
		}
	}
	if(!empty($_POST["retour"])){
		header("location : " . $_POST["retour"]);
	}
	die();

?>
