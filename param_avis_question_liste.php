<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	
	// GESTION DU FILTRE
	
	
	if(isset($_POST["filtrer"])){
		
		if($_POST["filt_valide"]!=""){
			$filt_valide=intval($_POST["filt_valide"]);
		}else{
			$filt_valide="";
		}
		if($_POST["filt_rubrique"]!=""){
			$filt_rubrique=intval($_POST["filt_rubrique"]);
		}else{
			$filt_rubrique=0;
		}
		
		$_SESSION["filtre"]=array(
			"filt_valide" => $filt_valide,
			"filt_rubrique" => $filt_rubrique
		);
		
	}elseif(isset($_SESSION["filtre"])){
		$filt_valide=$_SESSION["filtre"]["filt_valide"];
		$filt_rubrique=$_SESSION["filtre"]["filt_rubrique"];
	}else{
		$filt_valide="";
		$filt_rubrique="";
		$_SESSION["filtre"]=array(
			"filt_valide" => $filt_valide,
			"filt_rubrique" => $filt_rubrique
		);
	}
	
	// LES RUBRIQUES
	
	// pour le filtre
	
	$sql="SELECT * FROM Avis_Rubriques";
	$sql.=" ORDER BY aru_libelle;";
	$req=$Conn->query($sql);
	$d_rubriques_filt=$req->fetchAll();
	
	// pour la liste 
	
	$sql="SELECT * FROM Avis_Rubriques";
	if($filt_rubrique>0){
		$sql.=" WHERE aru_id=" . $filt_rubrique;
	}
	$sql.=" ORDER BY aru_place,aru_id;";
	$req=$Conn->query($sql);
	$d_rubriques=$req->fetchAll();
	if(!empty($d_rubriques)){
		foreach($d_rubriques as $num => $d_rubrique){
			
			$sql="SELECT * FROM Avis_Questions WHERE aqu_rubrique=" . $d_rubrique["aru_id"];
			if(is_int($filt_valide)){
				$sql.=" AND aqu_valide=" . $filt_valide;
			}
			$sql.=" ORDER BY aqu_place,aqu_id;";
			$req=$Conn->query($sql);
			$dd_questions=$req->fetchAll();
			if(!empty($dd_questions)){
				$d_rubriques[$num]["questions"]=$dd_questions;
			}else{
				$d_rubriques[$num]["questions"]="";
			}
		}
	}

	
	$_SESSION["retour"]="param_avis_question_liste.php";
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Si2P</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

			<!-- Theme CSS PAR DEFAUT -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	
		<!-- PLUGIN -->
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css">
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css">
		
		<!-- PERSO Si2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="theme/assets/img/favicon.ico" >

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
	
					<div class="row" >
						<div class="col-md-10 col-md-offset-1" >
							<div class="panel">
								<div class="panel-heading">
									<span class="panel-title">Filtrer</span>
								</div>
								<div class="panel-body bg-light">
									
									<form class="form-inline" action="param_avis_question_liste.php" method="post" >			
										<div class="row" >
											<div class="col-md-5" >
												<label for="filt_valide">Statut des questions :</label>
												<select id="filt_valide" name="filt_valide" class="select2" >
													<option value="" <?php if($filt_valide==="") echo("selected"); ?> >Afficher toutes les questions</option>
													<option value="0" <?php if($filt_valide===0) echo("selected"); ?> >Afficher uniquement les questions non-validées</option>
													<option value="1" <?php if($filt_valide===1) echo("selected"); ?> >Afficher uniquement les questions validées</option>
												</select>	
											</div>
											<div class="col-md-5" >							
												<label for="filt_rubrique">Rubriques :</label>
												<select id="filt_rubrique" name="filt_rubrique" class="select2" >
													<option value="0" <?php if($filt_rubrique===0) echo("selected"); ?> >Toutes les rubriques</option>
											<?php	foreach($d_rubriques_filt as $rf){
														if($filt_rubrique==$rf["aru_id"]){
															echo("<option value='" . $rf["aru_id"] . "' selected >" . $rf["aru_libelle"] . "</option>");
														}else{
															echo("<option value='" . $rf["aru_id"] . "' >" . $rf["aru_libelle"] . "</option>");
														}
													} ?>
												</select>	
											</div>
											<div class="col-md-2 pt20" >		
												<button type="submit" name="filtrer" class="btn btn-primary">Filtrer</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
					<div class="content-header">
						<h2>Avis de stage : <b class="text-primary" > Base "questions" </b></h2>
					</div>

			<?php	if(!empty($d_rubriques)){
				
						foreach($d_rubriques as $r){ 
						
	
						
							$d_questions=$r["questions"]; 
							
							/*echo("<pre>");
							print_r($d_questions);
							echo("</pre>");
							die();*/
							 ?>
						
							<h4><?=$r["aru_libelle"]?></h4>
							
					<?php	IF(!empty($d_questions)){
						
								$nbQ=0;
								foreach($d_questions as $q){
									$nbQ++ ?>				
									<div id="question_<?=$q["aqu_id"]?>" class="panel <?php if($q["aqu_valide"]==1) echo(" panel-success") ?>" >
										<div class="panel-heading">
											<span class="panel-title">
												<button type="button" class="btn btn-info btn-sm q_down" id="down_<?=$q["aqu_id"]?>" data-question="<?=$q["aqu_id"]?>"  <?php if($nbQ==count($d_questions)) echo("style='display:none;'"); ?> >
														<i class="fa fa-arrow-down" ></i>
													</button>
													<button type="button" class="btn btn-info btn-sm q_up" id="up_<?=$q["aqu_id"]?>" data-question="<?=$q["aqu_id"]?>" <?php if($nbQ==1) echo("style='display:none;'"); ?> >
														<i class="fa fa-arrow-up" ></i>
													</button>
												Question <?=$q["aqu_id"]?>
											</span>
									 <?php	if($q["aqu_valide"]==0){ ?>
												<span class="pull-right" >
													<a href="param_avis_question.php?question=<?=$q["aqu_id"]?>" class="btn btn-sm btn-warning" >
														<i class="fa fa-pencil" ></i>
													</a>
												</span>
									<?php	} ?>	
										</div>
										<div class="panel-body">
											<p><?=$q["aqu_texte"]?></p>
											<div class="row" >
												<div class="col-md-4 text-center" >
													<?=$q["aqu_min_lib"]?>
												</div>
												<div class="col-md-offset-4 col-md-4 text-center" >
													<?=$q["aqu_max_lib"]?>
												</div>
											</div>
											<div class="row" >
												<div class="col-md-4 text-center" >
													<?=$q["aqu_min_val"]?>
												</div>
												<div class="col-md-offset-4 col-md-4 text-center" >
													<?=$q["aqu_max_val"]?>
												</div>
											</div>
										</div>
									</div>
					<?php		}
							}else{ ?>
								<div class="alert alert-danger text-center" >
					<?php			if($filt_valide!==""){
										echo("Aucune question ne correspond à votre recherche");							
									}else{
										echo("Vous n'avez enregistré aucune question.");
									} ?>
									
								</div>
					<?php	} 
						} 
					}?>
				</section>
			</section>
		</div>

		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
					<a href="param_avis_question.php" class="btn btn-success btn-sm" >
						<i class="fa fa-plus" ></i>
						Nouvelle question
					</a>
				</div>
			</div>
		</footer>
		
		
<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">

			jQuery(document).ready(function() {
				
				
				$(".q_down").click(function(){					
					var question=$(this).data("question");
					set_avis_question_place(question,1,deplacer_question,"","");
				});
				$(".q_up").click(function(){					
					var question=$(this).data("question");
					console.log("q_up");
					set_avis_question_place(question,-1,deplacer_question,"","");
				});
			});
			function deplacer_question(json){
				console.log("deplacer_question");
				console.log("json " + json.length);
				if(json){
					if(json.length==3){
						
						max_q=json[2]["max_q"];
						
						if(json[0]["place"]<json[1]["place"]){
							question=json[0]["question"];
							place=json[0]["place"];
							question_suiv=json[1]["question"];
						}else{
							question=json[1]["question"];
							place=json[1]["place"];
							question_suiv=json[0]["question"];
						}
						
						// on deplace la question
						$("#question_" + question).after($("#question_" + question_suiv));
							
						// affiche masque les bouttons en fonction de la place
						$("#up_" + question_suiv).show();
						$("#down_" + question_suiv).show();
						$("#up_" + question).show();
						$("#down_" + question).show();
						
						if(place==1){
							$("#up_" + question).hide();
						}
						if(place==max_q-1){
							$("#down_" + question_suiv).hide();
						}
					}
				}
			}
			
			function set_avis_question_place(question,deplacement,callback,param1,param2){
				console.log("set_avis_question_place(" + question + "," + deplacement + ")");
				$.ajax({			
					type:'POST',
					url: 'ajax/ajax_avis_question_place_set.php',
					data : 'question=' + question + "&deplacement=" + deplacement, 
					dataType: 'JSON',                
					success: function(data){	
						callback(data,param1,param2);
					},
					error: function(data) {
						console.log("ERREUR");
						afficher_message("Erreur","danger",data.responseText);
					}
				});
			}
			
		</script>
	</body>
</html>