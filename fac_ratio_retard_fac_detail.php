<?php

	// DETAIL DU RATION RETARD DE REGLEMENT

	include "includes/controle_acces.inc.php";

	include "includes/connexion.php";
	include "includes/connexion_soc.php";


    // CONTROLE ACCES

    $commercial=0;
	if(isset($_GET["commercial"])){
		if(!empty($_GET["commercial"])){
			$commercial=intval($_GET["commercial"]);
		}
    }

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}

    // TRAITEMENT

    $sql="SELECT acl_ca,acl_id
    ,act_id,DATE_FORMAT(act_date_deb,'%d/%m/%Y') AS act_date_deb
    ,com_label_1,com_label_2 
    ,max_date
    ,cli_code,cli_nom
    FROM Actions_Clients
	LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
	LEFT JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)
    LEFT JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND Actions.act_agence=Clients.cli_agence)
	INNER JOIN (
		SELECT acd_action_client, MAX(pda_date) AS max_date FROM Actions_Clients_Dates,plannings_dates WHERE acd_date=pda_id GROUP BY acd_action_client) AS groupe
	ON Actions_Clients.acl_id = groupe.acd_action_client
    WHERE NOT acl_facture AND NOT acl_archive AND NOT act_archive AND max_date<=NOW() AND NOT acl_non_fac AND acl_confirme";
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
    }elseif(!empty($commercial) AND $commercial != -1){
		$sql.=" AND acl_commercial=" . $commercial;
    }
    
    if(!empty($acc_agence)){
		$sql.=" AND act_agence=" . $acc_agence;
	}
	$sql.=" ORDER BY max_date";
    $req=$ConnSoc->query($sql);
    $d_actions=$req->fetchAll(); 

    // TITRE

    
    $titre="";
    
    if(!$_SESSION["acces"]["acc_droits"][35]){

        $titre="Actions non facturées terminées au " . date("d/m/Y");

	}elseif(!empty($commercial) AND $commercial!=-1){

        $sql="SELECT com_label_1,com_label_2 FROM Commerciaux WHERE com_id=" . $commercial . ";";
        $req=$ConnSoc->query($sql);
        $d_commercial=$req->fetch();
        if(!empty($d_commercial)){
            $titre="Actions non facturées de " . $d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"] . " terminées au " . date("d/m/Y");
        }
    }elseif(!empty($acc_agence)) {

        $sql="SELECT soc_nom,age_nom FROM Agences,Societes WHERE soc_id=age_societe AND age_id=" . $acc_agence . ";";
        $req=$Conn->query($sql);
        $d_agence=$req->fetch();
        if(!empty($d_agence)){
            $titre="Actions non facturées " . $d_agence["soc_nom"] . " " . $d_agence["age_nom"] . " terminées au " . date("d/m/Y");
        }

    }else{

        $sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
        $req=$Conn->query($sql);
        $d_societe=$req->fetch();
        if(!empty($d_societe)){
            $titre="Actions non facturées " . $d_societe["soc_nom"] . " terminées au " . date("d/m/Y");
        }

    }
	
	

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">

<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" >
					<div class="row ">

                       <h1 class="text-center" ><?=$titre?></h1>
                    
						<div class="table-responsive">
                            <table class="table table-striped table-hover" >
                                <thead>
                                    <tr class="dark">
                                        <th>Code client</th>	
                                        <th>Nom client</th>	
                                        <th>Action</th>
                                        <th>Commercial</th>
                                        <th>Date de début</th>
                                        <th>Date de fin</th>
                                        <th>CA J-1</th>
                                        <th>CA J</th>
                                    </tr>
                                </thead>
                                <tbody>
                        <?php		if(!empty($d_actions)){

                                        /*echo("<pre>");
                                            print_r($d_fac_reglees);
                                        echo("</pre>");
                                        die();*/
                                        $total_ajd=0;
                                        $total_hier=0;

                                        foreach($d_actions as $d){  

                                            
                                            
                                            $total_ajd=$total_ajd + $d["acl_ca"];

                                            $ca_hier=0;
                                            if($d["max_date"]<date("Y-m-d")){
                                                $total_hier=$total_hier + $d["acl_ca"];
                                                $ca_hier=$d["acl_ca"];
                                            }

                                            
                                            $Dt_fin=DateTime::createFromFormat('Y-m-d',$d["max_date"]);
                                            $act_date_fin=$Dt_fin->format("d/m/Y");
                                            ?>
                                            <tr>
                                                <td><?=$d["cli_code"]?></td>
                                                <td><?=$d["cli_nom"]?></td>
                                                <td><?=$d["act_id"] . "-" . $d["acl_id"]?></td>
                                                <td><?=$d["com_label_1"] . " " . $d["com_label_2"]?></td>
                                                <td><?=$d["act_date_deb"]?></td>
                                                <td><?=$act_date_fin?></td>
                                                <td class="text-right" ><?=$ca_hier?></td>
                                                <td class="text-right" ><?=$d["acl_ca"]?></td>
                                            </tr>
                        <?php			} ?>
                                        <tr>
                                            <th class="text-right" colspan="6" >Total :</th>
                                            <td class="text-right" ><?=number_format($total_hier,2,","," ")?></td>
                                            <td class="text-right" ><?=number_format($total_ajd,2,","," ")?></td>
                                        </tr>
                                        <tr>
                                            <th class="text-right" colspan="6" >Taux de retard de facturation :</th>
                                            <td class="text-center" colspan="2" >
                                        <?php   if(!empty($total_ajd)){
                                                    echo(number_format(($total_hier/$total_ajd)*100,2,","," "));
                                                } ?>
                                            </td>
                                        </tr>
                        <?php		} ?>
                                </tbody>
                            </table>
						</div>

					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
        <?php       if(!$_SESSION["acces"]["acc_droits"][35]){ ?>
                        <a href="<?=$_SESSION["retour"]?>" class="btn btn-default btn-sm" >
                            <i class="fa fa-arrow-left" ></i> Retour
                        </a>
        <?php       }else{ ?>
                        <a href="fac_ratio_retard_fac.php" class="btn btn-default btn-sm" >
                            <i class="fa fa-arrow-left" ></i> Retour
                        </a>
        <?php       } ?>
				</div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){



			});
		</script>
	</body>
</html>
