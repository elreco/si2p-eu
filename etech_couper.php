<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_document.php');

if(!empty($_POST)){


	$doc = get_document($_GET['id']);
	if($doc['doc_url'] . $doc['doc_nom_aff'] . "/" != $_POST['doc_url']){


		if($doc['doc_dossier'] == 1){
			update_document($doc['doc_id'], $doc['doc_nom_aff'],$_POST['doc_url'], 1, $doc['doc_mot_cle'], $doc['doc_descriptif']);
		}else{
			update_document($doc['doc_id'], $doc['doc_nom_aff'],$_POST['doc_url'], 0, $doc['doc_mot_cle'], $doc['doc_descriptif']);
		}


		$sous_docs = get_documents($doc['doc_url'] . $doc['doc_nom_aff'] . "/");

		foreach($sous_docs as $s){
			if($s['doc_dossier'] == 1){
				update_document($s['doc_id'], $s['doc_nom_aff'],$_POST['doc_url'], 1, $s['doc_mot_cle'], $s['doc_descriptif']);
			}else{
				update_document($s['doc_id'], $s['doc_nom_aff'],$_POST['doc_url'], 0, $s['doc_mot_cle'], $s['doc_descriptif']);
			}

		}

		if(isset($_POST['dos'])){
			header("Location: etech.php?dos=" . $_POST['dos'] . "&succes=6");
		}else{
			header("Location: etech.php?succes=6");
		}

	}else{
		if(isset($_POST['dos'])){
			header("Location: etech.php?dos=" . $_POST['dos'] . "&erreur=4");
		}else{
			header("Location: etech.php?erreur=4");
		}
	}
}