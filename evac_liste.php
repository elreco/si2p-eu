<?php
////////////////// MENU ACTIF HEADER ///////////////////
$menu_actif = "4-2";
////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour

$_SESSION['retour'] = "evac_liste.php?";
// INTERVENANT
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
////////////////////FIN TRAITEMENTS PHP//////////////////////
// RECHERCHE
if(!empty($_POST)){
	$mil="";

	if(!empty($_POST['date_deb'])){
		$date_deb = convert_date_sql($_POST['date_deb']);
		$mil.=" AND eva_date_exer >= '" . $date_deb . "'";
	}

	if(!empty($_POST['date_fin'])){
		$date_fin = convert_date_sql($_POST['date_fin']);
		$mil.=" AND eva_date_exer <= '" . $date_fin . "'";
	}

	if(!empty($_POST['eva_etat_valide'])){
		$mil.=" AND eva_etat_valide = " . $_POST['eva_etat_valide'];
	}
if($_SESSION['acces']['acc_profil'] == 1){
	$mil .= " AND eva_formateur=" . $_SESSION['acces']["acc_ref_id"];
}
	// CONSTRUCTION REQUETE
	$sql ="SELECT * FROM evacuations LEFT JOIN utilisateurs ON (utilisateurs.uti_id = evacuations.eva_formateur)";
	if($mil!=""){
      $sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
    }
	if($acc_societe == 4 && $acc_agence == 4){
		$sql .= "  ORDER BY eva_date_exer DESC;";
	}else{
		$sql .= "  AND ((eva_client_societe = " . $acc_societe . " AND eva_client_agence = " . $acc_agence . ") OR (uti_societe=" . $acc_societe . " AND uti_agence= " . $acc_agence . "))
	 ORDER BY eva_date_exer DESC;";
	}

    $req=$Conn->query($sql);
	$evacuations=$req->fetchAll();

}else{
	// EVAC
	if($_SESSION['acces']['acc_profil'] == 1){

			$req = $Conn->prepare("SELECT * FROM evacuations WHERE eva_date_exer >= '" . date("Y-m-01") . "' AND eva_formateur=" . $_SESSION['acces']["acc_ref_id"] . " ORDER BY eva_date_exer DESC;");
			$req->execute();
			$evacuations = $req->fetchAll();

	}else{
		if($acc_societe == 4 && $acc_agence == 4){
			$req = $Conn->prepare("SELECT * FROM evacuations
			LEFT JOIN utilisateurs ON (utilisateurs.uti_id = evacuations.eva_formateur)
			WHERE
				eva_date_creation >= '" . date('Y-m-d', strtotime('first day of this month')) . "'  
			 ORDER BY eva_date_exer DESC;");
		}else{
			$req = $Conn->prepare("SELECT * FROM evacuations
			LEFT JOIN utilisateurs ON (utilisateurs.uti_id = evacuations.eva_formateur)
			WHERE
				eva_date_creation >= '" . date('Y-m-d', strtotime('first day of this month')) . "' AND ((eva_client_societe = " . $acc_societe . " AND eva_client_agence = " . $acc_agence . ") OR (uti_societe=" . $acc_societe . " AND uti_agence= " . $acc_agence . "))
			 ORDER BY eva_date_exer DESC;");
		}

		$req->execute();
		$evacuations = $req->fetchAll();
	}


	// EVAC

}

if($_SESSION['acces']['acc_profil'] != 1){
	$req = $Conn->prepare("SELECT * FROM evacuations WHERE eva_avis = 5 AND eva_etat_valide = 1 ORDER BY eva_date_exer DESC;");
	$req->execute();
	$evacuations_meilleurs = $req->fetchAll();
}
?>
<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>SI2P - Orion - Rapports d'évacuations</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.2/rateit.min.css">
<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/img/favicon.png">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
<style type="text/css">
.panel-tabs > li > a {
    color: #AAA;
    font-size: 14px;
    letter-spacing: 0.2px;
    line-height: 30px;
    /*padding: 9px 20px 11px;*/
    border-radius: 0;
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;
}
.nav2 > li > a {
    position: relative;
    display: block;
    /* padding: 10px 15px; */
}
</style>
<body class="sb-top sb-top-sm">


		<!-- Start: Main -->
		<div id="main">

			<?php include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

<!-- MESSAGES SUCCES -->

				<section id="content" class="animated fadeIn pr20 pl20">
				<h1>Rapports d'évacuation</h1>
					<div class="row">
						<?php if(empty($evacuations_meilleurs)){ ?>
							<div class="col-md-12">
						<?php }else{ ?>
							<div class="col-md-9">
						<?php }?>
							<div class="panel mb10">
	                            <div class="panel-heading panel-head-sm">
	                                <span class="panel-icon">
	                                    <i class="fa fa-cogs" aria-hidden="true"></i>
	                                </span>
	                                <span class="panel-title"> Filtrer</span>
	                            </div>
	                            <div class="panel-body p20">
	                                <div class="row admin-form theme-primary">
	                                	<form action="evac_liste.php" method="POST">
				                        <div class="col-md-4">
			                               <label for="date_deb">A partir du :</label>
											<span class="field prepend-icon">
												<input type="text" class="gui-input datepicker date" id="date_deb" name="date_deb" required  placeholder="Selectionner une date"

												<?php if(!empty($_POST['date_deb'])){ ?>
													value="<?= $_POST['date_deb'] ?>"
												<?php }else{?>
													value="<?= date("01-m-Y") ?>"
												<?php }?>
												>
												<label class="field-icon">
													<i class="fa fa-calendar-o"></i>
												</label>
											</span>

			                            </div>
			                            <div class="col-md-4">
			                                <label for="date_fin">Jusqu'au :</label>
											<span class="field prepend-icon">
												<input type="text" class="gui-input datepicker date" id="date_fin" name="date_fin"  placeholder="Selectionner une date"
												<?php if(!empty($_POST['date_fin'])){ ?>
													value="<?= $_POST['date_fin'] ?>"
												<?php }?>
												>
												<label class="field-icon">
													<i class="fa fa-calendar-o"></i>
												</label>
											</span>
			                            </div>
			                            <div class="col-md-3">
			                            	<label for="date_fin">Etat :</label>
											<label class="field select">
			                                    <select id="eva_etat_valide" name="eva_etat_valide" >
			                                        <option value="0">Sélectionnez un état...</option>
			                                        <option value="1"
			                                        <?php if(!empty($_POST['eva_etat_valide']) && $_POST['eva_etat_valide']==1){ ?>
														selected
													<?php } ?>

			                                        >Terminé</option>
													<option value="2"
													<?php if(!empty($_POST['eva_etat_valide']) && $_POST['eva_etat_valide']==2){ ?>
														selected
													<?php }?>
													>En cours de saisie</option>

													<option value="3"
													<?php if(!empty($_POST['eva_etat_valide']) && $_POST['eva_etat_valide']==3){ ?>
														selected
													<?php }?>
													>Validé</option>
			                                    </select>
			                                    <i class="arrow simple"></i>
			                                </label>
			                            </div>

			                            <div class="col-md-1 pt15">
											<button type="submit" data-toggle="tooltip" data-placement="bottom" title="Filtrer" class="btn btn-primary">
												<i class="fa fa-search"></i>
											</button>
			                            </div>
			                            <?php if(!empty($_POST)){ ?>
				                            <div class="col-md-1 pt15 text-left">
												<a href="evac_liste.php" data-toggle="tooltip" data-placement="bottom" title="Annuler le filtre" class="btn btn-default">
													<i class="fa fa-times"></i>
												</a>
				                            </div>
			                        	<?php }?>

										</form>
						</div>


	                                </div>

	                            </div>

								<table class="table table-striped table-hover outprint"  id="table_id" style="border:1px solid #e2e2e2;margin-top:10px">
			                        <thead>
			                            <tr class="dark">
			                                <th class="text-center">Date exercice</th>
			                                <th class="text-center">Créé le</th>
			                                <th class="text-center">Terminé le</th>
			                                <th class="text-center">Client</th>
			                                <th class="text-center">Intervenant</th>
			                                <th class="text-center">Contact</th>
			                                <th class="text-center">Etat</th>
											<th class="text-center">Note</th>
			                                <th class="text-center">Contrôle</th>
			                                <th class="text-center no-sort">Actions</th>
			                            </tr>
			                        </thead>
			                        <tbody class="text-center">
				                       <?php foreach($evacuations as $e){


										   	$req = $Conn->prepare("SELECT uti_nom, uti_prenom FROM utilisateurs WHERE uti_id=" . $e['eva_formateur'] . ";");


										   $req->execute();
										   $intervenant = $req->fetch();
				                       	?>

				                       		<tr>
				                       			<td  data-order="<?= $e['eva_date_exer'] ?>"><?= convert_date_txt($e['eva_date_exer']) ?></td>
				                       			<td  data-order="<?= $e['eva_date_creation'] ?>"><?= convert_date_txt($e['eva_date_creation']) ?></td>
				                       			<td data-order="<?= $e['eva_date_termine'] ?>"><?= convert_date_txt($e['eva_date_termine']) ?></td>
				                       			<td><?= $e['eva_client_nom'] ?></td>
				                       			<td><?= $intervenant['uti_prenom'] ?> <?= $intervenant['uti_nom'] ?></td>
				                       			<td><?= $e['eva_contact_nom'] ?></td>
				                       			<td>
				                       				<?php switch ($e['eva_etat_valide']) {
				                       					case 1: ?>
				                       						Terminé
				                       					<?php	break;

				                       					case 2: ?>
				                       						En cours de rédaction
				                       					<?php	break;
				                       					case 3: ?>
				                       						Validé
				                       					<?php	break;

				                       				} ?>

			                       				</td>
												<td class="text-center" data-order="<?= $e['eva_avis'] ?>">
													<div class="rateit" data-rateit-value="<?= $e['eva_avis'] ?>" data-rateit-ispreset="true" data-rateit-readonly="true"></div>

												</td>
			                       				<td>
			                       					<?php switch ($e['eva_etat_controle']) {
				                       					case 1: ?>
				                       						<i class="fa fa-circle" data-toggle="tooltip" title="Valide" style="color:green"></i>
				                       					<?php	break;

				                       					case 2: ?>

				                       						<i class="fa fa-circle" data-toggle="tooltip" title="Incomplet :
				                       						" style="color:orange"></i>
				                       					<?php	break;
				                       					case 3: ?>
				                       						<i class="fa fa-circle" data-toggle="tooltip" title="Incomplet" style="color:red"></i>
				                       					<?php	break;
				                       					case 0: ?>
				                       					<?php	break;
				                       				} ?>
			                       				</td>

			                       				<td class="text-center">
			                       					<a href="evac.php?id=<?= $e['eva_id'] ?>" class="btn btn-warning btn-xs">
														<i class="fa fa-pencil"></i>
													</a>
													<a href="evac_voir.php?id=<?= $e['eva_id'] ?>" class="btn btn-info btn-xs">
														<i class="fa fa-eye"></i>
													</a>
			                       				</td>
				                       		</tr>

				                       <?php } ?>

			                        </tbody>
	                    		</table>

							</div>
							<?php if(!empty($evacuations_meilleurs)){ ?>
							<div class="col-md-3">

								<div class="panel mb10">
		                            <div class="panel-heading panel-head-sm">
		                                <span class="panel-icon">
		                                    <i class="fa fa-star" aria-hidden="true"></i>
		                                </span>
		                                <span class="panel-title"> Meilleurs rapports de la semaine</span>
		                            </div>
		                            <div class="panel-body p20">
		                                <div class="row admin-form theme-primary">
												<table class="table table-striped table-hover outprint"  id="table_id" style="border:1px solid #e2e2e2;margin-top:10px">

							                        <tbody class="text-center">

								                       <?php foreach($evacuations_meilleurs as $e){
								                       	?>

								                       		<tr>
								                       			<td><a href="evac_voir.php?id=<?= $e['eva_id'] ?>"><?= $e['eva_client_nom'] ?></a></td>
								                       			<td><a href="evac_voir.php?id=<?= $e['eva_id'] ?>"><?= $intervenant['uti_prenom'] ?> <?= $intervenant['uti_nom'] ?></a></td>



																<td class="text-center" data-order="<?= $e['eva_avis'] ?>">
																	<i class="fa fa-star" data-toggle="tooltip" title="5/5" style="color:#FFD700"></i>
																	<i class="fa fa-star" data-toggle="tooltip" title="5/5" style="color:#FFD700"></i>
																	<i class="fa fa-star" data-toggle="tooltip" title="5/5" style="color:#FFD700"></i>
																	<i class="fa fa-star" data-toggle="tooltip" title="5/5" style="color:#FFD700"></i>
																	<i class="fa fa-star" data-toggle="tooltip" title="5/5" style="color:#FFD700"></i>
																</td>

								                       		</tr>

								                       <?php
												   } ?>

							                        </tbody>
					                    		</table>
										</div>


	                                </div>

	                            </div>
							</div>
							<?php }?>
						</div>
					</div>
				</section>
			</section>

		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
			<div class="row">
				<div class="col-xs-4 footer-left pt7" >
					<a href="parametre.php" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a>
				</div>
				<div class="col-xs-4 footer-middle pt7"></div>
				<div class="col-xs-4 footer-right pt7">

		        	<a href="evac.php" class="btn btn-success btn-sm">
						<i class='fa fa-plus'></i> Nouveau rapport
					</a>
				</div>
			</div>
		</footer>

</form>



<?php
include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
<!-- plugin pour les masques formulaires -->
<script src="assets/js/custom.js"></script>

<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script src="vendor/plugins/summernote/summernote.min.js"></script>
<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.2/jquery.rateit.min.js"></script>

<script src="vendor/plugins/jquery.numberformat.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<style type="text/css">
	.tooltip-inner {
    white-space:pre-wrap;
}
</style>
<script>
// DOCUMENT READY //a
jQuery(document).ready(function () {

// initilisation plugin datatables
$('#table_id').DataTable({
    "language": {
      "url": "vendor/plugins/DataTables/media/js/French.json"
    },
    "paging": false,
    "searching": false,
    "info": false,
    "columnDefs": [ {
          "targets": 'no-sort',
          "orderable": false,
  	}]
});
// quand on veut imprimer
// edition d'une ligne

////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
});

///////////// FONCTIONS //////////////

//////////// FIN FONCTIONS //////////


</script>

</body>
</html>
