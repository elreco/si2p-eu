<?php
$menu_actif = 1;
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");;
include_once("includes/connexion_soc.php");
include_once("includes/connexion_fct.php");
include_once("modeles/mod_parametre.php");

// PARAMETRE

$erreur_txt="";

$commercial=0;
if(isset($_GET["commercial"])){
	if(!empty($_GET["commercial"])){
		$commercial=intval($_GET["commercial"]);
	}
}
$com_type=0;
if(isset($_GET["com_type"])){
	if(!empty($_GET["com_type"])){
		$com_type=intval($_GET["com_type"]);
	}
}
$agence=0;
if(isset($_GET["agence"])){
	if(!empty($_GET["agence"])){
		$agence=intval($_GET["agence"]);
	}
}
$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}
$vu=0;
if(isset($_GET["vu"])){
	if(!empty($_GET["vu"])){
		$vu=intval($_GET["vu"]);
	}
}

// PERSONNE CONNECTE
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_agence=0;
if(!empty($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}elseif(!empty($_GET['agence'])){
	$acc_agence = $_GET['agence'];
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
}

// CONTROLE D'ACCESS

if(!$_SESSION["acces"]["acc_droits"][35] AND $commercial==0){
	header("location:deconnect.php");
	die();
}elseif($agence>0){
	if($acc_agence>0 AND $agence!=$acc_agence){
		header("location:deconnect.php");
		die();
	}
}elseif(empty($agence) AND empty($commercial)){
	if(!isset($_SESSION["acces"]["acc_liste_societe"][$acc_societe . "-0"])){
		header("location:deconnect.php");
		die();
	}
}
$aff_geo_gc=false;
$aff_previ=true;
$aff_prct=false;
if($_SESSION['acces']['acc_service'][1]==1 OR $_SESSION['acces']["acc_profil"]==11 OR $_SESSION['acces']["acc_profil"]==5){
	// service com / DAF et DR
	$aff_geo_gc=true;
	$aff_prct=true;
}elseif($_SESSION['acces']["acc_profil"]==15){
	$aff_prct=true;
}

// VALEUR PAR DEFAUT
if($exercice==0){
	if(date("n")<4){
		$exercice=date("Y")-1;
	}else{
		$exercice=date("Y");
	}
}
$exercice_fin=$exercice+1;


// CATEGORIE
$d_categories=array(
	"0" => "Divers"
);
$sql="SELECT pca_id,pca_libelle FROM Produits_Categories ORDER BY pca_id;";
$req=$Conn->query($sql);
$d_result_cat=$req->fetchAll();
if(!empty($d_result_cat)){
	foreach($d_result_cat as $cat){
		$d_categories[$cat["pca_id"]]=$cat["pca_libelle"];
	}
}
// Famille
$d_familles=array();
$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles  ORDER BY pfa_id;";
$req=$Conn->query($sql);
$d_result_fam=$req->fetchAll();
if(!empty($d_result_fam)){
	foreach($d_result_fam as $fam){
		$d_familles[$fam["pfa_id"]]=$fam["pfa_libelle"];
	}
}
// Sous-Famille
$d_s_familles=array();
$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles ORDER BY psf_id;";
$req=$Conn->query($sql);
$d_result_s_fam=$req->fetchAll();
if(!empty($d_result_s_fam)){
	foreach($d_result_s_fam as $s_fam){
		$d_s_familles[$s_fam["psf_id"]]=$s_fam["psf_libelle"];
	}
}
// Sous-Sous-Famille
$d_s_s_familles=array();
$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles ORDER BY pss_id;";
$req=$Conn->query($sql);
$d_result_s_s_fam=$req->fetchAll();
if(!empty($d_result_s_s_fam)){
	foreach($d_result_s_s_fam as $s_s_fam){
		$d_s_s_familles[$s_s_fam["pss_id"]]=$s_s_fam["pss_libelle"];
	}
}


// LE COMMERCIAL

if($commercial>0){

	$sql="SELECT com_label_1,com_label_2,com_ref_1,com_agence FROM Commerciaux WHERE com_id=" . $commercial;
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif($acc_agence>0){
		$sql.=" AND com_agence=" . $acc_agence;
	}
	$sql.=";";
	$req=$ConnSoc->query($sql);
	$d_commercial=$req->fetch();
	if(!empty($d_commercial)){
		$agence=$d_commercial["com_agence"];
	}else{
		echo("impossible d'afficher la page!");
		die();
	}
}

$stat_periode=0;
if($agence>0){
	$sql="SELECT age_nom,age_societe,age_intra_inter FROM Agences WHERE age_id=" . $agence . ";";
	$req=$Conn->query($sql);
	$d_agence=$req->fetch();
	if(!empty($d_agence)){
		if($d_agence["age_societe"]!=$acc_societe){
			header("location:deconnect.php");
			die();
		}
		if(!empty($d_agence["age_intra_inter"])){
			$stat_periode=$d_agence["age_intra_inter"];
		}
	}
}else{
	$sql="SELECT soc_nom,soc_intra_inter FROM Societes WHERE soc_id=" . $acc_societe . ";";
	$req=$Conn->query($sql);
	$d_societe=$req->fetch();
	if(!empty($d_societe)){
		if(!empty($d_societe["soc_intra_inter"])){
			$stat_periode=$d_societe["soc_intra_inter"];
		}
	}
}

// PERIODE
if($vu==1){
	if(empty($stat_periode)){
		$erreur_txt="La période n'est pas configuré. Merci de contacter le SI.";
	}else{

		$d_periode=array();
		$sql="SELECT urp_mois,urp_periode FROM Utilisateurs_Rem_Periode WHERE urp_exercice=" . $exercice . " AND urp_type=" . $stat_periode . ";";
		$req=$Conn->query($sql);
		$d_r_periodes=$req->fetchAll();
		if(!empty($d_r_periodes)){
			$bcl_min=1;
			$bcl_max=1;
			foreach($d_r_periodes as $rp){
				$d_periode[$rp["urp_mois"]]=$rp["urp_periode"];
				if($bcl_max<$rp["urp_periode"]){
					$bcl_max=$rp["urp_periode"];
				}
			}


		}else{
			$erreur_txt="Les paramètres période n'ont pas été chargés. Merci de contacter le SI.";

		}
	}
}else{
	$bcl_min=4;
	$bcl_max=12;
}
// CONSTRUCTION DU TITRE

if($commercial>0){
	$titre="Résultat de " . $d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"] . " pour l'exercice " . $exercice . "/" . $exercice_fin;

}elseif($com_type>0){

	if($agence>0){
		$agence_libelle = $d_agence["age_nom"];
	}else{
		$agence_libelle = "";
	}
	if($com_type==1){
		$titre="Résultat Vendeurs " . $agence_libelle . " pour l'exercice " . $exercice . "/" . $exercice_fin;

	}elseif($com_type==2){
		$titre="Résultat Revendeurs " . $agence_libelle . " pour l'exercice " . $exercice . "/" . $exercice_fin;

	}elseif($com_type==3){
		$titre="Résultat Indivis " . $agence_libelle . " pour l'exercice " . $exercice . "/" . $exercice_fin;

	}
}elseif($agence>0){
	$titre="Résultat " . $d_agence["age_nom"] . " pour l'exercice " . $exercice . "/" . $exercice_fin;
}else{
	$titre="Résultat " . $d_societe["soc_nom"] . " pour l'exercice " . $exercice . "/" . $exercice_fin;
}



if(empty($erreur_txt)){

	// LES OBJECTIFS

	// [0] clé divers cree ici au cas ou pas d'objectifs et du CA
	$resultats=array(
		0 => array(
			0 => array(
				0 => array(
					0 => array(
						"objectif" => array(
							"mois_1" => 0,
							"mois_2" => 0,
							"mois_3" => 0,
							"mois_4" => 0,
							"mois_5" => 0,
							"mois_6" => 0,
							"mois_7" => 0,
							"mois_8" => 0,
							"mois_9" => 0,
							"mois_10" => 0,
							"mois_11" => 0,
							"mois_12" => 0,
							"total" => 0
						),
						"resultat" => array(
							"mois_1" => 0,
							"mois_2" => 0,
							"mois_3" => 0,
							"mois_4" => 0,
							"mois_5" => 0,
							"mois_6" => 0,
							"mois_7" => 0,
							"mois_8" => 0,
							"mois_9" => 0,
							"mois_10" => 0,
							"mois_11" => 0,
							"mois_12" => 0,
							"total" => 0
						),
						"resultat_geo" => array(
							"mois_1" => 0,
							"mois_2" => 0,
							"mois_3" => 0,
							"mois_4" => 0,
							"mois_5" => 0,
							"mois_6" => 0,
							"mois_7" => 0,
							"mois_8" => 0,
							"mois_9" => 0,
							"mois_10" => 0,
							"mois_11" => 0,
							"mois_12" => 0,
							"total" => 0
						),
						"resultat_gc" => array(
							"mois_1" => 0,
							"mois_2" => 0,
							"mois_3" => 0,
							"mois_4" => 0,
							"mois_5" => 0,
							"mois_6" => 0,
							"mois_7" => 0,
							"mois_8" => 0,
							"mois_9" => 0,
							"mois_10" => 0,
							"mois_11" => 0,
							"mois_12" => 0,
							"total" => 0
						),
						"previ_valid" => array(
							"mois_1" => 0,
							"mois_2" => 0,
							"mois_3" => 0,
							"mois_4" => 0,
							"mois_5" => 0,
							"mois_6" => 0,
							"mois_7" => 0,
							"mois_8" => 0,
							"mois_9" => 0,
							"mois_10" => 0,
							"mois_11" => 0,
							"mois_12" => 0,
							"total" => 0
						),
						"previ_option" => array(
							"mois_1" => 0,
							"mois_2" => 0,
							"mois_3" => 0,
							"mois_4" => 0,
							"mois_5" => 0,
							"mois_6" => 0,
							"mois_7" => 0,
							"mois_8" => 0,
							"mois_9" => 0,
							"mois_10" => 0,
							"mois_11" => 0,
							"mois_12" => 0,
							"total" => 0
						),
						"libelle" => "Divers",
						"lib_detail" => "",
						"autre" => 1
					)
				)
			)
		),
		"total" => array(
			"objectif" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"resultat" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"resultat_geo" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"resultat_gc" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"previ_valid" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"previ_option" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"resultat-1" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"libelle" => "Total",
			"lib_detail" => ""
		),
		"conso" => array(
			"objectif" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"resultat" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"resultat_geo" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"resultat_gc" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"previ_valid" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"previ_option" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"libelle" => "Autres",
			"lib_detail" => ""
		)
	);

	$cob_valide=false;
	$cob_valide_date=null;
	$cob_valide_utilisateur=0;

	$sql="SELECT cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille,
	SUM(cob_objectif_1) AS objectif_1,SUM(cob_objectif_2) AS objectif_2,SUM(cob_objectif_3) AS objectif_3,
	SUM(cob_objectif_4) AS objectif_4,SUM(cob_objectif_5) AS objectif_5,SUM(cob_objectif_6) AS objectif_6,
	SUM(cob_objectif_7) AS objectif_7,SUM(cob_objectif_8) AS objectif_8,SUM(cob_objectif_9) AS objectif_9,
	SUM(cob_objectif_10) AS objectif_10,SUM(cob_objectif_11) AS objectif_11,SUM(cob_objectif_12) AS objectif_12,
	SUM(cob_objectif_total) AS objectif_total,cob_autre";
	if($commercial>0){
		// seulement si on consulte un objectif commercial => sinon mélange de valide et non valide = bug conso
		$sql.=",cob_valide,cob_valide_date,cob_valide_utilisateur";
	}
	$sql.=" FROM Commerciaux
	LEFT JOIN Commerciaux_Objectifs ON (Commerciaux.com_id=Commerciaux_Objectifs.cob_commercial)
	WHERE cob_exercice=" . $exercice;
	/* 	FABIEN hotfix/objectif-com-archive
		suppression du critère "AND com_archive=0" -> le fait d'archiver un commercial ne doit pas faire perdre l'historique des objectifs
	*/

	// restriction d'accès
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif($acc_agence>0){
		$sql.=" AND com_agence=" . $acc_agence;
	}
	// gestion des filtres
	if($commercial>0){
		$sql.=" AND com_id=" . $commercial;
	}elseif($com_type>0){
		$sql.=" AND com_type=" . $com_type;
	}elseif($agence>0){
		$sql.=" AND com_agence=" . $agence;
	}
	$sql.=" GROUP BY cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille,cob_autre";
	if($commercial>0){
		$sql.=",cob_valide,cob_valide_date,cob_valide_utilisateur";
	}
	$sql.=" ORDER BY SUM(cob_objectif_total) DESC;";
	/*echo("SQL : " . $sql . "<br/>");
	die();*/
	$req=$ConnSoc->query($sql);
	$d_objectifs=$req->fetchAll();
	if(!empty($d_objectifs)){
		foreach($d_objectifs as $obj){
			if($commercial>0){
				$cob_valide=$obj["cob_valide"];
				$cob_valide_date=$obj["cob_valide_date"];
				$cob_valide_utilisateur=$obj["cob_valide_utilisateur"];
			}

			$lib_detail="";
			$libelle="";
			if(!empty($obj["cob_sous_sous_famille"])){
				$lib_detail=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]] . "/" .$d_s_familles[$obj["cob_sous_famille"]] . "/" .$d_s_s_familles[$obj["cob_sous_sous_famille"]];
				$libelle=$d_s_s_familles[$obj["cob_sous_sous_famille"]];
			}elseif(!empty($obj["cob_sous_famille"])){
				$lib_detail=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]] . "/" .$d_s_familles[$obj["cob_sous_famille"]];
				$libelle=$d_s_familles[$obj["cob_sous_famille"]];
			}elseif(!empty($obj["cob_famille"])){
				$lib_detail=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]];
				$libelle=$d_familles[$obj["cob_famille"]];
			}else{
				$libelle=$d_categories[$obj["cob_categorie"]];
			}

			$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]=array(
				"objectif" => array(
					"mois_1" => 0,
					"mois_2" => 0,
					"mois_3" => 0,
					"mois_4" => 0,
					"mois_5" => 0,
					"mois_6" => 0,
					"mois_7" => 0,
					"mois_8" => 0,
					"mois_9" => 0,
					"mois_10" => 0,
					"mois_11" => 0,
					"mois_12" => 0,
					"total" => 0,
				),
				"resultat" => array(
					"mois_1" => 0,
					"mois_2" => 0,
					"mois_3" => 0,
					"mois_4" => 0,
					"mois_5" => 0,
					"mois_6" => 0,
					"mois_7" => 0,
					"mois_8" => 0,
					"mois_9" => 0,
					"mois_10" => 0,
					"mois_11" => 0,
					"mois_12" => 0,
					"total" => 0
				),
				"resultat_geo" => array(
					"mois_1" => 0,
					"mois_2" => 0,
					"mois_3" => 0,
					"mois_4" => 0,
					"mois_5" => 0,
					"mois_6" => 0,
					"mois_7" => 0,
					"mois_8" => 0,
					"mois_9" => 0,
					"mois_10" => 0,
					"mois_11" => 0,
					"mois_12" => 0,
					"total" => 0
				),
				"resultat_gc" => array(
					"mois_1" => 0,
					"mois_2" => 0,
					"mois_3" => 0,
					"mois_4" => 0,
					"mois_5" => 0,
					"mois_6" => 0,
					"mois_7" => 0,
					"mois_8" => 0,
					"mois_9" => 0,
					"mois_10" => 0,
					"mois_11" => 0,
					"mois_12" => 0,
					"total" => 0
				),
				"previ_valid" => array(
					"mois_1" => 0,
					"mois_2" => 0,
					"mois_3" => 0,
					"mois_4" => 0,
					"mois_5" => 0,
					"mois_6" => 0,
					"mois_7" => 0,
					"mois_8" => 0,
					"mois_9" => 0,
					"mois_10" => 0,
					"mois_11" => 0,
					"mois_12" => 0,
					"total" => 0
				),
				"previ_option" => array(
					"mois_1" => 0,
					"mois_2" => 0,
					"mois_3" => 0,
					"mois_4" => 0,
					"mois_5" => 0,
					"mois_6" => 0,
					"mois_7" => 0,
					"mois_8" => 0,
					"mois_9" => 0,
					"mois_10" => 0,
					"mois_11" => 0,
					"mois_12" => 0,
					"total" => 0
				),
				"libelle" => $libelle,
				"lib_detail" => $lib_detail,
				"autre" => $obj["cob_autre"]
			);

			if($vu==0){

				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_1"]=$obj["objectif_1"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_2"]=$obj["objectif_2"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_3"]=$obj["objectif_3"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_4"]=$obj["objectif_4"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_5"]=$obj["objectif_5"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_6"]=$obj["objectif_6"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_7"]=$obj["objectif_7"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_8"]=$obj["objectif_8"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_9"]=$obj["objectif_9"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_10"]=$obj["objectif_10"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_11"]=$obj["objectif_11"];
				$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_12"]=$obj["objectif_12"];


				if($obj["cob_autre"]){

					// conso autre
					$resultats["conso"]["objectif"]["mois_1"]+=$obj["objectif_1"];
					$resultats["conso"]["objectif"]["mois_2"]+=$obj["objectif_2"];
					$resultats["conso"]["objectif"]["mois_3"]+=$obj["objectif_3"];
					$resultats["conso"]["objectif"]["mois_4"]+=$obj["objectif_4"];
					$resultats["conso"]["objectif"]["mois_5"]+=$obj["objectif_5"];
					$resultats["conso"]["objectif"]["mois_6"]+=$obj["objectif_6"];
					$resultats["conso"]["objectif"]["mois_7"]+=$obj["objectif_7"];
					$resultats["conso"]["objectif"]["mois_8"]+=$obj["objectif_8"];
					$resultats["conso"]["objectif"]["mois_9"]+=$obj["objectif_9"];
					$resultats["conso"]["objectif"]["mois_10"]+=$obj["objectif_10"];
					$resultats["conso"]["objectif"]["mois_11"]+=$obj["objectif_11"];
					$resultats["conso"]["objectif"]["mois_12"]+=$obj["objectif_12"];


				}

				// on totalise
				$resultats["total"]["objectif"]["mois_1"]+=$obj["objectif_1"];
				$resultats["total"]["objectif"]["mois_2"]+=$obj["objectif_2"];
				$resultats["total"]["objectif"]["mois_3"]+=$obj["objectif_3"];
				$resultats["total"]["objectif"]["mois_4"]+=$obj["objectif_4"];
				$resultats["total"]["objectif"]["mois_5"]+=$obj["objectif_5"];
				$resultats["total"]["objectif"]["mois_6"]+=$obj["objectif_6"];
				$resultats["total"]["objectif"]["mois_7"]+=$obj["objectif_7"];
				$resultats["total"]["objectif"]["mois_8"]+=$obj["objectif_8"];
				$resultats["total"]["objectif"]["mois_9"]+=$obj["objectif_9"];
				$resultats["total"]["objectif"]["mois_10"]+=$obj["objectif_10"];
				$resultats["total"]["objectif"]["mois_11"]+=$obj["objectif_11"];
				$resultats["total"]["objectif"]["mois_12"]+=$obj["objectif_12"];


			}else{

				foreach($d_periode as $m => $p){
					$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["mois_" . $p]+=$obj["objectif_" . $m];

					if($obj["cob_autre"]){
						$resultats["conso"]["objectif"]["mois_" . $p]+=$obj["objectif_" . $m];
					}

					$resultats["total"]["objectif"]["mois_" .  $p]+=$obj["objectif_" . $m];
				}
			}

			$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]["objectif"]["total"]=$obj["objectif_total"];
			if($obj["cob_autre"]){
				$resultats["conso"]["objectif"]["total"]+=$obj["objectif_total"];
			}
			$resultats["total"]["objectif"]["total"]+=$obj["objectif_total"];

		}

	}
	/*
	echo("<pre>");
		print_r($resultats);
	echo("</pre>");
	die();*/

	// LE FACTURES

	$sql="SELECT fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille
	,SUM(fli_montant_ht) AS montant_ht
	,MONTH(fac_date) AS mois,fac_date,fac_id,fac_cli_categorie,fac_cli_sous_categorie,fac_client
	FROM Factures_Lignes
	LEFT JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
	LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
	WHERE fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'";
	$sql.=" AND fli_categorie<4";
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif($acc_agence>0){
		$sql.=" AND com_agence=" . $acc_agence;
	}
	if($commercial>0){
		$sql.=" AND com_id=" . $commercial;
	}elseif($com_type>0){
		$sql.=" AND com_type=" . $com_type;
	}elseif($agence>0){
		$sql.=" AND fac_agence=" . $agence;
	}
	$sql.=" GROUP BY fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,MONTH(fac_date),fac_date,fac_id;";
	$req=$ConnSoc->query($sql);
	$d_realises=$req->fetchAll();
	if(!empty($d_realises)){
		foreach($d_realises as $realise){

			$cat_id=0;
			if(!empty($realise["fli_categorie"])){
				$cat_id=$realise["fli_categorie"];
			}
			$fam_id=0;
			if(!empty($realise["fli_famille"])){
				$fam_id=$realise["fli_famille"];
			}
			$s_fam_id=0;
			if(!empty($realise["fli_sous_famille"])){
				$s_fam_id=$realise["fli_sous_famille"];
			}
			$s_s_fam_id=0;
			if(!empty($realise["fli_sous_sous_famille"])){
				$s_s_fam_id=$realise["fli_sous_sous_famille"];
			}

			$mois=0;
			if(!empty($realise["mois"])){
				if($vu==1){
					$mois=$d_periode[$realise["mois"]];
				}else{
					$mois=$realise["mois"];
				}
			}

			$montant_ht=0;
			if(!empty($realise["montant_ht"])){
				$montant_ht=$realise["montant_ht"];
			}

			if(!empty($resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["objectif"]["total"])){
				$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat"]["total"]+=$montant_ht;
				if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
					$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat_gc"]["total"]+=$montant_ht;
				}else{
					$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat_geo"]["total"]+=$montant_ht;
				}
				if($resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["autre"]){
					$resultats["conso"]["resultat"]["mois_" . $mois]+=$montant_ht;
					$resultats["conso"]["resultat"]["total"]+=$montant_ht;
					if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
						$resultats["conso"]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"]["resultat_gc"]["total"]+=$montant_ht;
					}else{
						$resultats["conso"]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"]["resultat_geo"]["total"]+=$montant_ht;
					}
				}
			}elseif(!empty($resultats[$cat_id][$fam_id][$s_fam_id][0]["objectif"]["total"])){
				$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat"]["total"]+=$montant_ht;
				if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
					$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat_gc"]["total"]+=$montant_ht;
				}else{
					$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat_geo"]["total"]+=$montant_ht;
				}
				if($resultats[$cat_id][$fam_id][$s_fam_id][0]["autre"]){
					$resultats["conso"]["resultat"]["mois_" . $mois]+=$montant_ht;
					$resultats["conso"]["resultat"]["total"]+=$montant_ht;
					if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
						$resultats["conso"]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"]["resultat_gc"]["total"]+=$montant_ht;
					}else{
						$resultats["conso"]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"]["resultat_geo"]["total"]+=$montant_ht;
					}
				}

			}elseif(!empty($resultats[$cat_id][$fam_id][0][0]["objectif"]["total"])){
				$resultats[$cat_id][$fam_id][0][0]["resultat"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][$fam_id][0][0]["resultat"]["total"]+=$montant_ht;
				if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
					$resultats[$cat_id][$fam_id][0][0]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][$fam_id][0][0]["resultat_gc"]["total"]+=$montant_ht;
				}else{
					$resultats[$cat_id][$fam_id][0][0]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][$fam_id][0][0]["resultat_geo"]["total"]+=$montant_ht;
				}
				if($resultats[$cat_id][$fam_id][0][0]["autre"]){
					$resultats["conso"]["resultat"]["mois_" . $mois]+=$montant_ht;
					$resultats["conso"]["resultat"]["total"]+=$montant_ht;
					if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
						$resultats["conso"]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"]["resultat_gc"]["total"]+=$montant_ht;
					}else{
						$resultats["conso"]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"]["resultat_geo"]["total"]+=$montant_ht;
					}
				}
			}elseif(!empty($resultats[$cat_id][0][0][0]["objectif"]["total"])){
				$resultats[$cat_id][0][0][0]["resultat"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][0][0][0]["resultat"]["total"]+=$montant_ht;
				if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
					$resultats[$cat_id][0][0][0]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][0][0][0]["resultat_gc"]["total"]+=$montant_ht;
				}else{
					$resultats[$cat_id][0][0][0]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][0][0][0]["resultat_geo"]["total"]+=$montant_ht;
				}
				if($resultats[$cat_id][0][0][0]["autre"]){
					$resultats["conso"]["resultat"]["mois_" . $mois]+=$montant_ht;
					$resultats["conso"]["resultat"]["total"]+=$montant_ht;
					if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
						$resultats["conso"]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"]["resultat_gc"]["total"]+=$montant_ht;
					}else{
						$resultats["conso"]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"]["resultat_geo"]["total"]+=$montant_ht;
					}
				}
			}else{
				// divers CA SANS OBJECTIFS
				$resultats[0][0][0][0]["resultat"]["mois_" . $mois]+=$montant_ht;
				$resultats[0][0][0][0]["resultat"]["total"]+=$montant_ht;
				if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
					$resultats[0][0][0][0]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
					$resultats[0][0][0][0]["resultat_gc"]["total"]+=$montant_ht;
				}else{
					$resultats[0][0][0][0]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
					$resultats[0][0][0][0]["resultat_geo"]["total"]+=$montant_ht;
				}
				$resultats["conso"]["resultat"]["mois_" . $mois]+=$montant_ht;
				$resultats["conso"]["resultat"]["total"]+=$montant_ht;
				if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
					$resultats["conso"]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
					$resultats["conso"]["resultat_gc"]["total"]+=$montant_ht;
				}else{
					$resultats["conso"]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
					$resultats["conso"]["resultat_geo"]["total"]+=$montant_ht;
				}
			}


			$resultats["total"]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats["total"]["resultat"]["total"]+=$montant_ht;

			if($realise["fac_cli_categorie"]==2 OR $realise["fac_client"]==8891){
				$resultats["total"]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
				$resultats["total"]["resultat_gc"]["total"]+=$montant_ht;
			}else{
				$resultats["total"]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
				$resultats["total"]["resultat_geo"]["total"]+=$montant_ht;
			}

		}
	}

	// LE PREVI

	if($aff_previ){

		// pour le previ, on ne ventile pas GEO / GC

		$sql="SELECT DISTINCT acl_id,act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille
		,MONTH(pda_date) AS mois
		,acl_ca AS montant_ht,acl_facture_ht AS facture_ht,acl_confirme
		FROM Actions_Clients
		LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
		LEFT JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)

		INNER JOIN Actions_Clients_Dates ON (Actions_Clients_Dates.acd_action_client=acl_id)
		INNER JOIN plannings_dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)

		INNER JOIN (
			SELECT acd_action_client, MAX(pda_date) AS max_date FROM Actions_Clients_Dates,plannings_dates WHERE acd_date=pda_id GROUP BY acd_action_client) AS groupe
		ON Actions_Clients.acl_id = groupe.acd_action_client AND plannings_dates.pda_date = groupe.max_date


		WHERE pda_date>='" . $exercice . "-04-01' AND pda_date<='" . intval($exercice+1) . "-03-31'";
		$sql.=" AND act_pro_categorie<4 AND NOT act_archive AND NOT acl_archive AND NOT acl_non_fac AND NOT acl_ca_nc AND NOT acl_facture";
		if(!$_SESSION["acces"]["acc_droits"][35]){
			$sql.=" AND com_ref_1=" . $acc_utilisateur;
		}elseif($acc_agence>0){
			$sql.=" AND com_agence=" . $acc_agence;
		}
		if($commercial>0){
			$sql.=" AND com_id=" . $commercial;
		}elseif($com_type>0){
			$sql.=" AND com_type=" . $com_type;
		}elseif($agence>0){
			$sql.=" AND act_agence=" . $agence;
		}
		//$sql.=" GROUP BY act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,MONTH(pda_date),acl_confirme;";
		$req=$ConnSoc->query($sql);
		$d_realises=$req->fetchAll();
		if(!empty($d_realises)){
			foreach($d_realises as $realise){

				$cat_id=0;
				if(!empty($realise["act_pro_categorie"])){
					$cat_id=$realise["act_pro_categorie"];
				}
				$fam_id=0;
				if(!empty($realise["act_pro_famille"])){
					$fam_id=$realise["act_pro_famille"];
				}
				$s_fam_id=0;
				if(!empty($realise["act_pro_sous_famille"])){
					$s_fam_id=$realise["act_pro_sous_famille"];
				}
				$s_s_fam_id=0;
				if(!empty($realise["act_pro_sous_sous_famille"])){
					$s_s_fam_id=$realise["act_pro_sous_sous_famille"];
				}
				$mois=0;
				if(!empty($realise["mois"])){
					if($vu==1){
						$mois=$d_periode[$realise["mois"]];
					}else{
						$mois=$realise["mois"];
					}
				}

				$montant_ht=0;
				if(!empty($realise["montant_ht"])){
					$montant_ht=$realise["montant_ht"];
				}

				$facture_ht=0;
				if(!empty($realise["facture_ht"])){
					$facture_ht=$realise["facture_ht"];
					$montant_ht=$montant_ht-$facture_ht;
				}


				$cle_ventile="previ_option";
				if(!empty($realise["acl_confirme"])){
					$cle_ventile="previ_valid";
				}

				if(!empty($resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["objectif"]["total"])){
					$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id][$cle_ventile]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id][$cle_ventile]["total"]+=$montant_ht;
					if($resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["autre"]){
						$resultats["conso"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"][$cle_ventile]["total"]+=$montant_ht;
					}
				}elseif(!empty($resultats[$cat_id][$fam_id][$s_fam_id][0]["objectif"]["total"])){
					$resultats[$cat_id][$fam_id][$s_fam_id][0][$cle_ventile]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][$fam_id][$s_fam_id][0][$cle_ventile]["total"]+=$montant_ht;
					if($resultats[$cat_id][$fam_id][$s_fam_id][0]["autre"]){
						$resultats["conso"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"][$cle_ventile]["total"]+=$montant_ht;
					}

				}elseif(!empty($resultats[$cat_id][$fam_id][0][0]["objectif"]["total"])){
					$resultats[$cat_id][$fam_id][0][0][$cle_ventile]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][$fam_id][0][0][$cle_ventile]["total"]+=$montant_ht;
					if($resultats[$cat_id][$fam_id][0][0]["autre"]){
						$resultats["conso"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"][$cle_ventile]["total"]+=$montant_ht;
					}
				}elseif(!empty($resultats[$cat_id][0][0][0]["objectif"]["total"])){
					$resultats[$cat_id][0][0][0][$cle_ventile]["mois_" . $mois]+=$montant_ht;
					$resultats[$cat_id][0][0][0][$cle_ventile]["total"]+=$montant_ht;

					if($resultats[$cat_id][0][0][0]["autre"]){
						$resultats["conso"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
						$resultats["conso"][$cle_ventile]["total"]+=$montant_ht;

					}
				}else{
					// divers CA SANS OBJECTIFS
					$resultats[0][0][0][0][$cle_ventile]["mois_" . $mois]+=$montant_ht;
					$resultats[0][0][0][0][$cle_ventile]["total"]+=$montant_ht;

					$resultats["conso"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
					$resultats["conso"][$cle_ventile]["total"]+=$montant_ht;

				}


				$resultats["total"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
				$resultats["total"][$cle_ventile]["total"]+=$montant_ht;



			}
		}


		// CAS PARTICULIER : GC le previ de l'agence GC peut etre en région

		if($agence==4 OR ($acc_societe==4 AND $agence==0) ){

			// ON RECHERCHE LES COMMERCIAUX GC LIE AU TYPE DEMANDE POUR LA RECHERCHE DE CLIENT -> pas de jointure possible car pas sur la meme base
			// place ICI car spé GC

			$listeCom="";
			if($com_type>0){
				$sql="SELECT com_id FROM Commerciaux WHERE com_type=" . $com_type . " AND com_agence=4;";
				$req=$ConnSoc->query($sql);
				$d_com_gc=$req->fetchAll();
				if(!empty($d_com_gc)){
					$tab_com_gc=array_column ($d_com_gc,"com_id");
					$listeCom=implode($tab_com_gc,",");
				}
			}


			// CHOIX RETENU -> on liste les MM GC ou entreprise seul sur GFC affecter au commercial concerné.
			// AUTRE OPTION -> En base N, mmoriser le com GC de chaque client -> recupération plus simple mais traitement a code sur plus de script client mod client réseau ...

			$sql="SELECT cli_id,cli_code,cli_nom FROM Clients,Clients_Societes WHERE cli_id=cso_client
			AND cso_agence=4 AND cli_categorie=2 AND NOT cli_sous_categorie=2 AND cli_filiale_de=0";
			if(!$_SESSION["acces"]["acc_droits"][35]){
				$sql.=" AND cso_utilisateur=" . $acc_utilisateur;
			}
			if($commercial>0){
				$sql.=" AND cso_commercial=" . $commercial;
			}elseif(!empty($listeCom)){
				$sql.=" AND cso_commercial IN (" . $listeCom . ")";
			}
			$sql.=" ORDER BY cli_code;";
			$req=$Conn->query($sql);
			$d_mm_gc=$req->fetchAll();
			if(!empty($d_mm_gc)){
				$tab_mm_gc=array_column ($d_mm_gc,"cli_id");
				$liste_mm_gc=implode($tab_mm_gc,",");
			}


			if(isset($liste_mm_gc)){

				$sql="SELECT soc_id FROM Societes WHERE NOT soc_id=4 AND NOT soc_archive ORDER BY soc_id;";
				$req=$Conn->query($sql);
				$d_societe_region=$req->fetchAll();
				if(!empty($d_societe_region)){

					foreach($d_societe_region as $region){

						$ConnFct=connexion_fct($region["soc_id"]);


						$sql_gc="SELECT DISTINCT acl_id,act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille
						,MONTH(pda_date) AS mois
						,acl_ca_gfc AS montant_ht,acl_facture_gfc_ht AS facture_ht,acl_confirme
						FROM Actions_Clients
						LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
						INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)

						INNER JOIN Actions_Clients_Dates ON (Actions_Clients_Dates.acd_action_client=Actions_Clients.acl_id)
						INNER JOIN plannings_dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)

						INNER JOIN (
							SELECT acd_action_client, MAX(pda_date) AS max_date FROM Actions_Clients_Dates,plannings_dates WHERE acd_date=pda_id GROUP BY acd_action_client) AS groupe
						ON Actions_Clients.acl_id = groupe.acd_action_client AND plannings_dates.pda_date = groupe.max_date


						WHERE pda_date>='" . $exercice . "-04-01' AND pda_date<='" . intval($exercice+1) . "-03-31'";
						$sql_gc.=" AND act_pro_categorie<4 AND NOT act_archive AND NOT acl_archive AND NOT acl_ca_nc AND NOT acl_non_fac_gfc AND acl_facturation=1";

						$sql_gc.=" AND (cli_id IN (" . $liste_mm_gc . ") OR cli_filiale_de IN (" . $liste_mm_gc . "))";

						//$sql_gc.=" AND act_pro_sous_sous_famille=1 AND MONTH(pda_date)=9 AND acl_confirme=1";
						//$sql_gc.=" GROUP BY act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,MONTH(pda_date),acl_confirme;";
						$req_gc=$ConnFct->query($sql_gc);
						$d_realises_gc=$req_gc->fetchAll();
						if(!empty($d_realises_gc)){

							//var_dump($liste_mm_gc);
							/*echo("<pre>");
								print_r($d_realises_gc);
							echo("</pre>");
							die();*/

							foreach($d_realises_gc as $realise){

								$cat_id=0;
								if(!empty($realise["act_pro_categorie"])){
									$cat_id=$realise["act_pro_categorie"];
								}
								$fam_id=0;
								if(!empty($realise["act_pro_famille"])){
									$fam_id=$realise["act_pro_famille"];
								}
								$s_fam_id=0;
								if(!empty($realise["act_pro_sous_famille"])){
									$s_fam_id=$realise["act_pro_sous_famille"];
								}
								$s_s_fam_id=0;
								if(!empty($realise["act_pro_sous_sous_famille"])){
									$s_s_fam_id=$realise["act_pro_sous_sous_famille"];
								}
								$mois=0;
								if(!empty($realise["mois"])){
									if($vu==1){
										$mois=$d_periode[$realise["mois"]];
									}else{
										$mois=$realise["mois"];
									}
								}

								$montant_ht=0;
								if(!empty($realise["montant_ht"])){
									$montant_ht=$realise["montant_ht"];
								}
								$facture_ht=0;
								if(!empty($realise["facture_ht"])){
									$facture_ht=$realise["facture_ht"];
									$montant_ht=$montant_ht-$facture_ht;
								}

								$cle_ventile="previ_option";
								if(!empty($realise["acl_confirme"])){
									$cle_ventile="previ_valid";
								}

								if(!empty($resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["objectif"]["total"])){
									$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id][$cle_ventile]["mois_" . $mois]+=$montant_ht;
									$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id][$cle_ventile]["total"]+=$montant_ht;
									if($resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["autre"]){
										$resultats["conso"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
										$resultats["conso"][$cle_ventile]["total"]+=$montant_ht;
									}
								}elseif(!empty($resultats[$cat_id][$fam_id][$s_fam_id][0]["objectif"]["total"])){
									$resultats[$cat_id][$fam_id][$s_fam_id][0][$cle_ventile]["mois_" . $mois]+=$montant_ht;
									$resultats[$cat_id][$fam_id][$s_fam_id][0][$cle_ventile]["total"]+=$montant_ht;
									if($resultats[$cat_id][$fam_id][$s_fam_id][0]["autre"]){
										$resultats["conso"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
										$resultats["conso"][$cle_ventile]["total"]+=$montant_ht;
									}

								}elseif(!empty($resultats[$cat_id][$fam_id][0][0]["objectif"]["total"])){
									$resultats[$cat_id][$fam_id][0][0][$cle_ventile]["mois_" . $mois]+=$montant_ht;
									$resultats[$cat_id][$fam_id][0][0][$cle_ventile]["total"]+=$montant_ht;
									if($resultats[$cat_id][$fam_id][0][0]["autre"]){
										$resultats["conso"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
										$resultats["conso"][$cle_ventile]["total"]+=$montant_ht;
									}
								}elseif(!empty($resultats[$cat_id][0][0][0]["objectif"]["total"])){
									$resultats[$cat_id][0][0][0][$cle_ventile]["mois_" . $mois]+=$montant_ht;
									$resultats[$cat_id][0][0][0][$cle_ventile]["total"]+=$montant_ht;

									if($resultats[$cat_id][0][0][0]["autre"]){
										$resultats["conso"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
										$resultats["conso"][$cle_ventile]["total"]+=$montant_ht;

									}
								}else{
									// divers CA SANS OBJECTIFS
									$resultats[0][0][0][0][$cle_ventile]["mois_" . $mois]+=$montant_ht;
									$resultats[0][0][0][0][$cle_ventile]["total"]+=$montant_ht;

									$resultats["conso"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
									$resultats["conso"][$cle_ventile]["total"]+=$montant_ht;

								}


								$resultats["total"][$cle_ventile]["mois_" . $mois]+=$montant_ht;
								$resultats["total"][$cle_ventile]["total"]+=$montant_ht;

							}
						}
					}
				}
			}

		}
	}

	// RESULTAT DE L'EXERCICE PRECEDENT
	/* FG : oblige de garder le jointure Factures_Lignes pour le critère fli_categorie<4*/

	$sql="SELECT SUM(fli_montant_ht) AS montant_ht
	,MONTH(fac_date) AS mois
	FROM Factures_Lignes
	LEFT JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
	LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
	WHERE fac_date>='" . intval($exercice-1) . "-04-01' AND fac_date<='" . $exercice . "-03-31'";
	$sql.=" AND fli_categorie<4";
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif($acc_agence>0){
		$sql.=" AND com_agence=" . $acc_agence;
	}
	if($commercial>0){
		$sql.=" AND com_id=" . $commercial;
	}elseif($com_type>0){
		$sql.=" AND com_type=" . $com_type;
	}elseif($agence>0){
		$sql.=" AND fac_agence=" . $agence;
	}
	$sql.=" GROUP BY MONTH(fac_date);";
	$req=$ConnSoc->query($sql);
	$d_realises_1=$req->fetchAll();
	if(!empty($d_realises_1)){
		foreach($d_realises_1 as $realise){

			$mois=0;
			if(!empty($realise["mois"])){
				if($vu==1){
					$mois=$d_periode[$realise["mois"]];
				}else{
					$mois=$realise["mois"];
				}
			}

			$montant_ht=0;
			if(!empty($realise["montant_ht"])){
				$montant_ht=$realise["montant_ht"];
			}

			$resultats["total"]["resultat-1"]["mois_" . $mois]+=$montant_ht;
			$resultats["total"]["resultat-1"]["total"]+=$montant_ht;
		}
	}


	// VALIDATION

	if($cob_valide_utilisateur>0){
		$sql="SELECT uti_prenom,uti_nom FROM Utilisateurs WHERE uti_id=" . $cob_valide_utilisateur . ";";
		$req=$Conn->query($sql);
		$d_validateur=$req->fetch();
	}
}
//die();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#zone_print{
				display:none;
			}
			.objectif{
				color:#AA00AA;
			}
			.resultat{
				font-weight:bold;
			}
			.resultat a{
				color:#000;
			}
			.ligne-strip{
				background-color:#eee;
			}
			.taux-atteinte{
				color:blue;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 landscape;
				margin:5mm;
			}
			html{
				background-color:#fff!important
			}
			body{
				background-color:#fff!important
			}
			#zone_print{
				background-color:#fff!important;
				font-size:8pt;
			}
		</style>
	</head>

	<body class="sb-top sb-top-sm" >
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php		if(empty($d_objectifs)){ ?>
						<p class="alert alert-danger text-center" >
							Objectifs non disponible!
						</p>
		<?php		} ?>

					<div class="row" >
						<div class="col-md-offset-3 col-md-6" >
							<form method="get" action="commercial_result.php" id="filtre" >
								<div class="admin-form theme-primary mb25">
									<input type="hidden" name="commercial" value="<?=$commercial?>" />
									<input type="hidden" name="com_type" value="<?=$com_type?>" />
									<input type="hidden" name="agence" value="<?=$agence?>" />
									<label for="exercice" >Choix de l'exercice :</label>
									<select class="select" id="exercice" name="exercice" >
								<?php	for($i=2017;$i<=intval(date("Y")+1);$i++){
											if($i==$exercice){
												echo("<option value='" . $i . "' selected >" . $i . "/" . intval($i+1) . "</option>");
											}else{
												echo("<option value='" . $i . "' >" . $i . "/" . intval($i+1) . "</option>");
											}
										}?>
									</select>
								</div>
							</form>
						</div>
						<div class="col-md-3 pt25 text-center" >
					<?php	if($vu==0){ ?>

								<a href="commercial_result.php?commercial=<?=$commercial?>&com_type=<?=$com_type?>&agence=<?=$agence?>&vu=1" class="btn btn-sm btn-info" <?php if(empty($stat_periode)) echo("disabled"); ?> >
									<i class="fa fa-eye" ></i> Répartition par période
								</a>

					<?php	}else{ ?>
								<a href="commercial_result.php?commercial=<?=$commercial?>&com_type=<?=$com_type?>&agence=<?=$agence?>&vu=0"  class="btn btn-sm btn-info" >
									<i class="fa fa-eye" ></i> Répartition par mois
								</a>
					<?php	} ?>
						</div>
					</div>

			<?php	if(!empty($erreur_txt)){ ?>
						<p class="alert alert-danger text-center" >
							<?=$erreur_txt?>
						</p>
			<?php	}else{ ?>
						<div class="row" >
							<div class="col-md-12" id="page_print" >

								<div class="panel" >
									<div class="panel-body br-n">

										<h1><?=$titre?></h1>
										<div class="table-responsive">
											<table class="table" >
												<thead>
													<tr class="dark2" >
														<th colspan="2" >&nbsp;</th>
												<?php	if($vu==1){
															for($k=$bcl_min;$k<=$bcl_max;$k++){
																echo("<th class='text-center' >P" . $k . "</th>");
															}
														}else{ ?>
															<th>Avril</th>
															<th>Mai</th>
															<th>Juin</th>
															<th>Juillet</th>
															<th>Août</th>
															<th>Septembre</th>
															<th>Octobre</th>
															<th>Novembre</th>
															<th>Décembre</th>
															<th>Janvier</th>
															<th>Fevrier</th>
															<th>Mars</th>
												<?php	} ?>
														<th>Total</th>
													</tr>
												</thead>
									<?php		$numLigne=0;
												$aff_conso=false;

												if(!empty($resultats)){ ?>

													<tbody>
									<?php				foreach($resultats as $cat => $categorie){
															if($cat!="conso" AND $cat!="total" AND $cat!=0){
																foreach($categorie as $fam => $famille){
																	foreach($famille as $s_fam => $s_famille){
																		foreach($s_famille as $s_s_fam => $s_s_famille){

																			if($s_s_famille["autre"]==0){
																				if(!empty($s_s_famille["objectif"]["total"])){

																					$nb_row=2;
																					if($aff_previ AND $cat==1){
																						$nb_row=$nb_row+2;
																					}
																					if($aff_geo_gc){
																						$nb_row=$nb_row+2;
																					}
																					if($aff_prct){
																						$nb_row=$nb_row+2;
																					}

																					$numLigne++;
																					$classLigne="";
																					if($numLigne % 2 ==0){
																						$classLigne="ligne-strip";
																					}
																					if($s_s_famille["autre"]){
																						$classLigne.=" conso-detail hidden";
																					} ?>
																					<tr class="<?=$classLigne?>" style="page-break-after:avoid!important;" >
																						<th rowspan="<?=$nb_row?>" style="page-break-inside:avoid!important;" >
																				<?php		if(!empty($s_s_famille["lib_detail"])){ ?>
																								<button type="button" class="btn btn-default btn-sm hidden-print" data-toggle="tooltip" title="Classification : <?=$s_s_famille["lib_detail"]?>" >
																									<i class="fa fa-info" ></i>
																								</button>
																				<?php		} ?>
																							<?=$s_s_famille["libelle"]?>
																						</th>
																						<th class="objectif"  >Objectifs</th>
																				<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																							<td class="objectif text-right" ><?=number_format($s_s_famille["objectif"]["mois_" . $i],0,","," ")?></td>
																				<?php	}
																						if($vu==0){ ?>
																							<td class="objectif text-right" ><?=number_format($s_s_famille["objectif"]["mois_1"],0,","," ")?></td>
																							<td class="objectif text-right" ><?=number_format($s_s_famille["objectif"]["mois_2"],0,","," ")?></td>
																							<td class="objectif text-right" ><?=number_format($s_s_famille["objectif"]["mois_3"],0,","," ")?></td>
																				<?php	} ?>
																						<td class="objectif text-right" ><?=number_format($s_s_famille["objectif"]["total"],0,","," ")?></td>
																					</tr>

																		<?php		if($aff_prct){ ?>
																						<tr class="objectif <?=$classLigne?>" style="page-break-before:avoid!important;" >
																							<th>% obj. <sup>(1)</sup></th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="text-right" >
																					<?php			if(!empty($s_s_famille["objectif"]["total"])){
																										$prct=($s_s_famille["objectif"]["mois_" . $i]*100)/$s_s_famille["objectif"]["total"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="text-right" >
																					<?php			if(!empty($s_s_famille["objectif"]["total"])){
																										$prct=($s_s_famille["objectif"]["mois_1"]*100)/$s_s_famille["objectif"]["total"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																								<td class="text-right" >
																					<?php			if(!empty($s_s_famille["objectif"]["total"])){
																										$prct=($s_s_famille["objectif"]["mois_2"]*100)/$s_s_famille["objectif"]["total"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																								<td class="text-right" >
																					<?php			if(!empty($s_s_famille["objectif"]["total"])){
																										$prct=($s_s_famille["objectif"]["mois_3"]*100)/$s_s_famille["objectif"]["total"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																					<?php	} ?>
																							<td class="text-right" >
																				<?php			if(!empty($s_s_famille["objectif"]["total"])){
																									$prct=($s_s_famille["objectif"]["total"]*100)/$s_s_famille["objectif"]["total"];
																									echo(number_format($prct,2,","," ") . "%");
																								} ?>
																							</td>
																						</tr>

																		<?php		}

																					if($aff_previ AND $cat==1){	?>
																						<tr class="resultat <?=$classLigne?>" style="page-break-before:avoid!important;" >
																							<th>Prévi</th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=1&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["previ_valid"]["mois_" . $i],0,","," ")?>
																									</a>
																								</td>
																					<?php	}
																							if($vu==0){?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=1&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["previ_valid"]["mois_1"],0,","," ")?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=1&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["previ_valid"]["mois_2"],0,","," ")?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=1&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["previ_valid"]["mois_3"],0,","," ")?>
																									</a>
																								</td>
																					<?php	} ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=1&vu=<?=$vu?>" >
																									<?=number_format($s_s_famille["previ_valid"]["total"],0,","," ")?>
																								</a>
																							</td>
																						</tr>
																						<tr class="resultat <?=$classLigne?>" style="page-break-before:avoid!important;" >
																							<th>Prévi "Option"</th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=2&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["previ_option"]["mois_" . $i],0,","," ")?>
																									</a>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=2&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["previ_option"]["mois_1"],0,","," ")?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=2&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["previ_option"]["mois_2"],0,","," ")?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=2&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["previ_option"]["mois_3"],0,","," ")?>
																									</a>
																								</td>
																					<?php	} ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=2&vu=<?=$vu?>" >
																									<?=number_format($s_s_famille["previ_option"]["total"],0,","," ")?>
																								</a>
																							</td>
																						</tr>
																		<?php		}

																					if($aff_geo_gc){	?>
																						<tr class="resultat <?=$classLigne?>" style="page-break-before:avoid!important;" >
																							<th>Résultats GEO</th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["resultat_geo"]["mois_" . $i],0,","," ")?>
																									</a>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["resultat_geo"]["mois_1"],0,","," ")?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["resultat_geo"]["mois_2"],0,","," ")?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["resultat_geo"]["mois_3"],0,","," ")?>
																									</a>
																								</td>
																					<?php	} ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&vu=<?=$vu?>" >
																									<?=number_format($s_s_famille["resultat_geo"]["total"],0,","," ")?>
																								</a>
																							</td>
																						</tr>
																						<tr class="resultat <?=$classLigne?>" style="page-break-before:avoid!important;" >
																							<th>Résultats GC</th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["resultat_gc"]["mois_" . $i],0,","," ")?>
																									</a>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["resultat_gc"]["mois_1"],0,","," ")?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["resultat_gc"]["mois_2"],0,","," ")?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&vu=<?=$vu?>" >
																										<?=number_format($s_s_famille["resultat_gc"]["mois_3"],0,","," ")?>
																									</a>
																								</td>
																				<?php		} ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&vu=<?=$vu?>" >
																									<?=number_format($s_s_famille["resultat_gc"]["total"],0,","," ")?>
																								</a>
																							</td>
																						</tr>

																		<?php 		} ?>

																					<tr class="resultat <?=$classLigne?>" style="page-break-before:avoid!important;" >
																						<th>Résultats</th>
																				<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&vu=<?=$vu?>" >
																									<?=number_format($s_s_famille["resultat"]["mois_" . $i],0,","," ")?>
																								</a>
																							</td>
																				<?php	}
																						if($vu==0){ ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&vu=<?=$vu?>" >
																									<?=number_format($s_s_famille["resultat"]["mois_1"],0,","," ")?>
																								</a>
																							</td>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&vu=<?=$vu?>" >
																									<?=number_format($s_s_famille["resultat"]["mois_2"],0,","," ")?>
																								</a>
																							</td>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&vu=<?=$vu?>" >
																									<?=number_format($s_s_famille["resultat"]["mois_3"],0,","," ")?>
																								</a>
																							</td>
																				<?php	} ?>
																						<td class="resultat text-right" >
																							<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&vu=<?=$vu?>" >
																								<?=number_format($s_s_famille["resultat"]["total"],0,","," ")?>
																							</a>
																						</td>
																					</tr>

																			<?php	if($aff_prct){ ?>
																						<tr class="<?=$classLigne?>" style="page-break-before:avoid!important;" >
																							<th>% / CA. mois <sup>(2)</sup></th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="resultat text-right" >
																							<?php	if(!empty($resultats["total"]["resultat"]["mois_" . $i])){
																										$prct=($s_s_famille["resultat"]["mois_" . $i]*100)/$resultats["total"]["resultat"]["mois_" . $i];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="resultat text-right" >
																							<?php	if(!empty($resultats["total"]["resultat"]["mois_1"])){
																										$prct=($s_s_famille["resultat"]["mois_1"]*100)/$resultats["total"]["resultat"]["mois_1"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																								<td class="resultat text-right" >
																							<?php	if(!empty($resultats["total"]["resultat"]["mois_2"])){
																										$prct=($s_s_famille["resultat"]["mois_2"]*100)/$resultats["total"]["resultat"]["mois_2"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																								<td class="resultat text-right" >
																							<?php	if(!empty($resultats["total"]["resultat"]["mois_3"])){
																										$prct=($s_s_famille["resultat"]["mois_3"]*100)/$resultats["total"]["resultat"]["mois_3"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																					<?php	} ?>
																							<td class="taux-atteinte text-right" >
																						<?php	if(!empty($s_s_famille["objectif"]["total"])){
																									$prct=($s_s_famille["resultat"]["total"]*100)/$s_s_famille["objectif"]["total"];
																									echo(number_format($prct,2,","," ") . "% <sup>(4)</sup>");
																								}  ?>
																							</td>
																						</tr>
																			<?php	} ?>
																		<?php	}
																			}
																		}
																	}
																}
															}
														}

														// LIGNE CONSO

														$nb_row=2;
														if($aff_previ){
															$nb_row=$nb_row+2;
														}
														if($aff_geo_gc){
															$nb_row=$nb_row+2;
														}
														if($aff_prct){
															$nb_row=$nb_row+2;
														}
														$numLigne++;
														$classLigne="";
														if($numLigne % 2 ==0){
															$classLigne="ligne-strip";
														} ?>
														<tr class="<?=$classLigne?>" >
															<th rowspan="<?=$nb_row?>" >
																<button type="button" class="btn btn-default btn-sm hidden-print" id="conso_plus"  >
																	<i id="ico_conso" class="fa fa-plus" ></i>
																</button>
																<?=$resultats["conso"]["libelle"]?>
															</th>
															<th class="objectif"  >Objectifs</th>
													<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																<td class="objectif text-right" ><?=number_format($resultats["conso"]["objectif"]["mois_" . $i],0,","," ")?></td>
													<?php	}
															if($vu==0){ ?>
																<td class="objectif text-right" ><?=number_format($resultats["conso"]["objectif"]["mois_1"],0,","," ")?></td>
																<td class="objectif text-right" ><?=number_format($resultats["conso"]["objectif"]["mois_2"],0,","," ")?></td>
																<td class="objectif text-right" ><?=number_format($resultats["conso"]["objectif"]["mois_3"],0,","," ")?></td>
													<?php	} ?>
													<!-- NOTE ALEXANDRE-->
															<td class="objectif text-right" ><?=number_format($resultats["conso"]["objectif"]["total"],0,","," ")?></td>
														</tr>
											<?php		if($aff_prct){ ?>
															<tr class="objectif <?=$classLigne?>" style="page-break-before:avoid!important;" >
																<th>% obj. <sup>(1)</sup></th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="text-right" >
														<?php			if(!empty($resultats["conso"]["objectif"]["total"])){
																			$prct=($resultats["conso"]["objectif"]["mois_" . $i]*100)/$resultats["conso"]["objectif"]["total"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="text-right" >
														<?php			if(!empty($resultats["conso"]["objectif"]["total"])){
																			$prct=($resultats["conso"]["objectif"]["mois_1"]*100)/$resultats["conso"]["objectif"]["total"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
																	<td class="text-right" >
														<?php			if(!empty($resultats["conso"]["objectif"]["total"])){
																			$prct=($resultats["conso"]["objectif"]["mois_2"]*100)/$resultats["conso"]["objectif"]["total"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
																	<td class="text-right" >
														<?php			if(!empty($resultats["conso"]["objectif"]["total"])){
																			$prct=($resultats["conso"]["objectif"]["mois_3"]*100)/$resultats["conso"]["objectif"]["total"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
														<?php	} ?>
																<td class="text-right" >
													<?php			if(!empty($resultats["conso"]["objectif"]["total"])){
																		$prct=($resultats["conso"]["objectif"]["total"]*100)/$resultats["conso"]["objectif"]["total"];
																		echo(number_format($prct,2,","," ") . "%");
																	} ?>
																</td>
															</tr>

											<?php		}
														if($aff_previ){	?>
															<tr class="resultat <?=$classLigne?>" >
																<th>Prévi</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=conso&previ_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["previ_valid"]["mois_" . $i],0,","," ")?>
																		</a>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=conso&previ_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["previ_valid"]["mois_1"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=conso&previ_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["previ_valid"]["mois_2"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=conso&previ_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["previ_valid"]["mois_3"],0,","," ")?>
																		</a>
																	</td>
														<?php	} ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=conso&previ_type=1&vu=<?=$vu?>" >
																		<?=number_format($resultats["conso"]["previ_valid"]["total"],0,","," ")?>
																	</a>
																</td>
															</tr>
															<tr class="resultat <?=$classLigne?>" >
																<th>Prévi "option"</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=conso&previ_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["previ_option"]["mois_" . $i],0,","," ")?>
																		</a>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=conso&previ_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["previ_option"]["mois_1"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=conso&previ_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["previ_option"]["mois_2"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=conso&previ_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["previ_option"]["mois_3"],0,","," ")?>
																		</a>
																	</td>
														<?php	} ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=conso&previ_type=2&vu=<?=$vu?>" >
																		<?=number_format($resultats["conso"]["previ_option"]["total"],0,","," ")?>
																	</a>
																</td>
															</tr>
											<?php		}

														if($aff_geo_gc){	?>

															<tr class="resultat <?=$classLigne?>" >
																<th>Résultats GEO</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=conso&cli_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["resultat_geo"]["mois_" . $i],0,","," ")?>
																		</a>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=conso&cli_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["resultat_geo"]["mois_1"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=conso&cli_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["resultat_geo"]["mois_2"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=conso&cli_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["resultat_geo"]["mois_3"],0,","," ")?>
																		</a>
																	</td>
														<?php	} ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=conso&cli_type=1&vu=<?=$vu?>" >
																		<?=number_format($resultats["conso"]["resultat_geo"]["total"],0,","," ")?>
																	</a>
																</td>
															</tr>
															<tr class="resultat <?=$classLigne?>" >
																<th>Résultats GC</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=conso&cli_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["resultat_gc"]["mois_" . $i],0,","," ")?>
																		</a>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=conso&cli_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["resultat_gc"]["mois_1"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=conso&cli_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["resultat_gc"]["mois_2"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=conso&cli_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["conso"]["resultat_gc"]["mois_3"],0,","," ")?>
																		</a>
																	</td>
														<?php	} ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=conso&cli_type=2&vu=<?=$vu?>" >
																		<?=number_format($resultats["conso"]["resultat_gc"]["total"],0,","," ")?>
																	</a>
																</td>
															</tr>

											<?php		}	?>

														<tr class="resultat <?=$classLigne?>" >
															<th>Résultats</th>
													<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=conso&vu=<?=$vu?>" >
																		<?=number_format($resultats["conso"]["resultat"]["mois_" . $i],0,","," ")?>
																	</a>
																</td>
													<?php	}
															if($vu==0){ ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=conso&vu=<?=$vu?>" >
																		<?=number_format($resultats["conso"]["resultat"]["mois_1"],0,","," ")?>
																	</a>
																</td>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=conso&vu=<?=$vu?>" >
																		<?=number_format($resultats["conso"]["resultat"]["mois_2"],0,","," ")?>
																	</a>
																</td>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=conso&vu=<?=$vu?>" >
																		<?=number_format($resultats["conso"]["resultat"]["mois_3"],0,","," ")?>
																	</a>
																</td>
													<?php	} ?>
															<td class="resultat text-right" >
																<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=conso&vu=<?=$vu?>" >
																	<?=number_format($resultats["conso"]["resultat"]["total"],0,","," ")?>
																</a>
															</td>
														</tr>

												<?php	if($aff_prct){ ?>
															<tr class="<?=$classLigne?>" style="page-break-before:avoid!important;" >
																<th>% / CA. mois <sup>(2)</sup></th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																<?php	if(!empty($resultats["total"]["resultat"]["mois_" . $i])){
																			$prct=($resultats["conso"]["resultat"]["mois_" . $i]*100)/$resultats["total"]["resultat"]["mois_" . $i];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																<?php	if(!empty($resultats["total"]["resultat"]["mois_1"])){
																			$prct=($resultats["conso"]["resultat"]["mois_1"]*100)/$resultats["total"]["resultat"]["mois_1"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
																	<td class="resultat text-right" >
																<?php	if(!empty($resultats["total"]["resultat"]["mois_2"])){
																			$prct=($resultats["conso"]["resultat"]["mois_2"]*100)/$resultats["total"]["resultat"]["mois_2"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
																	<td class="resultat text-right" >
																<?php	if(!empty($resultats["total"]["resultat"]["mois_3"])){
																			$prct=($resultats["conso"]["resultat"]["mois_3"]*100)/$resultats["total"]["resultat"]["mois_3"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
														<?php	} ?>
																<td class="taux-atteinte text-right" >
															<?php	if(!empty($resultats["conso"]["objectif"]["total"])){
																		$prct=($resultats["conso"]["resultat"]["total"]*100)/$resultats["conso"]["objectif"]["total"];
																		echo(number_format($prct,2,","," ") . "% <sup>(4)</sup>");
																	} ?>
																</td>
															</tr>
												<?php	}

														// DETAIL CONSO
														foreach($resultats as $cat => $categorie){
															if($cat!="conso" AND $cat!="total" AND $cat!=0){
																foreach($categorie as $fam => $famille){
																	foreach($famille as $s_fam => $s_famille){
																		foreach($s_famille as $s_s_fam => $s_s_famille){
																			if($s_s_famille["autre"]==1){
																				if(!empty($s_s_famille["objectif"]["total"])){

																					$nb_row=2;
																					if($aff_previ AND $cat==1){
																						$nb_row=$nb_row+2;
																					}
																					if($aff_geo_gc){
																						$nb_row=$nb_row+2;
																					}
																					if($aff_prct){
																						$nb_row=$nb_row+2;
																					}

																					$numLigne++;
																					$classLigne="";
																					if($numLigne % 2 ==0){
																						$classLigne="ligne-strip";
																					}
																					if($s_s_famille["autre"]){
																						$classLigne.=" conso-detail hidden";
																					} ?>
																					<tr class="<?=$classLigne?>" >
																						<th rowspan="<?=$nb_row?>" >
																					<?php	if(!empty($s_s_famille["lib_detail"])){ ?>
																								<button type="button" class="btn btn-default btn-sm hidden-print" data-toggle="tooltip" title="Classification : <?=$s_s_famille["lib_detail"]?>" >
																									<i class="fa fa-info" ></i>
																								</button>
																				<?php		} ?>
																							<?=$s_s_famille["libelle"]?>
																						</th>
																						<th class="objectif"  >Objectifs</th>
																				<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																							<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_" . $i]?></td>
																				<?php	}
																						if($vu==0){ ?>
																							<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_1"]?></td>
																							<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_2"]?></td>
																							<td class="objectif text-right" ><?=$s_s_famille["objectif"]["mois_3"]?></td>
																				<?php	} ?>
																						<td class="objectif text-right" ><?=$s_s_famille["objectif"]["total"]?></td>
																					</tr>
																		<?php		if($aff_prct){ ?>
																						<tr class="objectif <?=$classLigne?>" style="page-break-before:avoid!important;" >
																							<th>% obj. <sup>(1)</sup></th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="text-right" >
																					<?php			if(!empty($s_s_famille["objectif"]["total"])){
																										$prct=($s_s_famille["objectif"]["mois_" . $i]*100)/$s_s_famille["objectif"]["total"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="text-right" >
																					<?php			if(!empty($s_s_famille["objectif"]["total"])){
																										$prct=($s_s_famille["objectif"]["mois_1"]*100)/$s_s_famille["objectif"]["total"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																								<td class="text-right" >
																					<?php			if(!empty($s_s_famille["objectif"]["total"])){
																										$prct=($s_s_famille["objectif"]["mois_2"]*100)/$s_s_famille["objectif"]["total"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																								<td class="text-right" >
																					<?php			if(!empty($s_s_famille["objectif"]["total"])){
																										$prct=($s_s_famille["objectif"]["mois_3"]*100)/$s_s_famille["objectif"]["total"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																					<?php	} ?>
																							<td class="text-right" >
																				<?php			if(!empty($s_s_famille["objectif"]["total"])){
																									$prct=($s_s_famille["objectif"]["total"]*100)/$s_s_famille["objectif"]["total"];
																									echo(number_format($prct,2,","," ") . "%");
																								} ?>
																							</td>
																						</tr>
																		<?php		}

																					if($aff_previ AND $cat==1){	?>
																						<tr class="resultat <?=$classLigne?>" >
																							<th>Prévi</th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=1&vu=<?=$vu?>" >
																										<?=$s_s_famille["previ_valid"]["mois_" . $i]?>
																									</a>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=1&vu=<?=$vu?>" >
																										<?=$s_s_famille["previ_valid"]["mois_1"]?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=1&vu=<?=$vu?>" >
																										<?=$s_s_famille["previ_valid"]["mois_2"]?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=1&vu=<?=$vu?>" >
																										<?=$s_s_famille["previ_valid"]["mois_3"]?>
																									</a>
																								</td>
																					<?php	} ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=1&vu=<?=$vu?>" >
																									<?=$s_s_famille["previ_valid"]["total"]?>
																								</a>
																							</td>
																						</tr>
																						<tr class="resultat <?=$classLigne?>" >
																							<th>Prévi "Option"</th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=2&vu=<?=$vu?>" >
																										<?=$s_s_famille["previ_option"]["mois_" . $i]?>
																									</a>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=2&vu=<?=$vu?>" >
																										<?=$s_s_famille["previ_option"]["mois_1"]?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=2&vu=<?=$vu?>" >
																											<?=$s_s_famille["previ_option"]["mois_2"]?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=2&vu=<?=$vu?>" >
																										<?=$s_s_famille["previ_option"]["mois_3"]?>
																								</td>
																					<?php	} ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&previ_type=2&vu=<?=$vu?>" >
																									<?=$s_s_famille["previ_option"]["total"]?>
																								</a>
																							</td>
																						</tr>
																			<?php	}

																					if($aff_geo_gc){	?>
																						<tr class="resultat <?=$classLigne?>" >
																							<th>Résultats GEO</th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&vu=<?=$vu?>" >
																										<?=$s_s_famille["resultat_geo"]["mois_" . $i]?>
																									</a>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&vu=<?=$vu?>" >
																										<?=$s_s_famille["resultat_geo"]["mois_1"]?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&vu=<?=$vu?>" >
																										<?=$s_s_famille["resultat_geo"]["mois_2"]?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&vu=<?=$vu?>" >
																										<?=$s_s_famille["resultat_geo"]["mois_3"]?>
																									</a>
																								</td>
																					<?php	} ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&vu=<?=$vu?>" >
																									<?=$s_s_famille["resultat_geo"]["total"]?>
																								</a>
																							</td>
																						</tr>
																						<tr class="resultat <?=$classLigne?>" >
																							<th>Résultats GC</th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&vu=<?=$vu?>" >
																										<?=$s_s_famille["resultat_gc"]["mois_" . $i]?>
																									</a>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&vu=<?=$vu?>" >
																										<?=$s_s_famille["resultat_gc"]["mois_1"]?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&vu=<?=$vu?>" >
																										<?=$s_s_famille["resultat_gc"]["mois_2"]?>
																									</a>
																								</td>
																								<td class="resultat text-right" >
																									<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&vu=<?=$vu?>" >
																										<?=$s_s_famille["resultat_gc"]["mois_3"]?>
																									</a>
																								</td>
																					<?php	} ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&vu=<?=$vu?>" >
																									<?=$s_s_famille["resultat_gc"]["total"]?>
																								</a>
																							</td>
																						</tr>
																			<?php	} ?>
																					<tr class="resultat <?=$classLigne?>" >
																						<th>Résultats</th>
																				<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&vu=<?=$vu?>" >
																									<?=$s_s_famille["resultat"]["mois_" . $i]?>
																								</a>
																							</td>
																				<?php	}
																						if($vu==0){ ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&vu=<?=$vu?>" >
																									<?=$s_s_famille["resultat"]["mois_1"]?>
																								</a>
																							</td>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&vu=<?=$vu?>" >
																									<?=$s_s_famille["resultat"]["mois_2"]?>
																								</a>
																							</td>
																							<td class="resultat text-right" >
																								<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&vu=<?=$vu?>" >
																									<?=$s_s_famille["resultat"]["mois_3"]?>
																								</a>
																							</td>
																				<?php	} ?>
																						<td class="resultat text-right" >
																							<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&vu=<?=$vu?>" >
																								<?=$s_s_famille["resultat"]["total"]?>
																							</a>
																						</td>
																					</tr>
																			<?php	if($aff_prct){ ?>
																						<tr class="<?=$classLigne?>" style="page-break-before:avoid!important;" >
																							<th>% / CA. mois <sup>(2)</sup></th>
																					<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																								<td class="resultat text-right" >
																							<?php	if(!empty($resultats["total"]["resultat"]["mois_" . $i])){
																										$prct=($s_s_famille["resultat"]["mois_" . $i]*100)/$resultats["total"]["resultat"]["mois_" . $i];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																					<?php	}
																							if($vu==0){ ?>
																								<td class="resultat text-right" >
																							<?php	if(!empty($resultats["total"]["resultat"]["mois_1"])){
																										$prct=($s_s_famille["resultat"]["mois_1"]*100)/$resultats["total"]["resultat"]["mois_1"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																								<td class="resultat text-right" >
																							<?php	if(!empty($resultats["total"]["resultat"]["mois_2"])){
																										$prct=($s_s_famille["resultat"]["mois_2"]*100)/$resultats["total"]["resultat"]["mois_2"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																								<td class="resultat text-right" >
																							<?php	if(!empty($resultats["total"]["resultat"]["mois_3"])){
																										$prct=($s_s_famille["resultat"]["mois_3"]*100)/$resultats["total"]["resultat"]["mois_3"];
																										echo(number_format($prct,2,","," ") . "%");
																									} ?>
																								</td>
																					<?php	} ?>
																							<td class="taux-atteinte text-right" >
																						<?php	if(!empty($s_s_famille["objectif"]["total"])){
																									$prct=($s_s_famille["resultat"]["total"]*100)/$s_s_famille["objectif"]["total"];
																									echo(number_format($prct,2,","," ") . "% <sup>(4)</sup>");
																								} ?>
																							</td>
																						</tr>
																			<?php	}
																				}

																			}
																		}
																	}
																}
															}
														} ?>


											<?php		// DIVERS
														if(!empty($resultats[0][0][0][0]["resultat"]["total"])){

															$nb_row=2;
															if($aff_previ){
																$nb_row=$nb_row+2;
															}
															if($aff_prct){
																$nb_row=$nb_row+2;
															}

															$numLigne++;
															$classLigne="conso-detail hidden";
															if($numLigne % 2 ==0){
																$classLigne.=" ligne-strip";
															} ?>
															<tr class="<?=$classLigne?>" >
																<th rowspan="<?=$nb_row?>" ><?=$resultats[0][0][0][0]["libelle"]?></th>
																<th class="objectif"  >Objectifs</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="objectif text-right" ><?=number_format($resultats[0][0][0][0]["objectif"]["mois_" . $i],0,","," ")?></td>
														<?php	}
																if($vu==0){ ?>
																	<td class="objectif text-right" ><?=number_format($resultats[0][0][0][0]["objectif"]["mois_1"],0,","," ")?></td>
																	<td class="objectif text-right" ><?=number_format($resultats[0][0][0][0]["objectif"]["mois_2"],0,","," ")?></td>
																	<td class="objectif text-right" ><?=number_format($resultats[0][0][0][0]["objectif"]["mois_3"],0,","," ")?></td>
														<?php	} ?>
																<td class="objectif text-right" ><?=number_format($resultats[0][0][0][0]["objectif"]["total"],0,","," ")?></td>
															</tr>

													<?php	if($aff_prct){ ?>
																<tr class="objectif <?=$classLigne?>" style="page-break-before:avoid!important;" >
																	<th>% obj. <sup>(1)</sup></th>
															<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																		<td class="text-right" >
															<?php			if(!empty($resultats[0][0][0][0]["objectif"]["total"])){
																				$prct=($resultats[0][0][0][0]["objectif"]["mois_" . $i]*100)/$resultats[0][0][0][0]["objectif"]["total"];
																				echo(number_format($prct,2,","," ") . "%");
																			} ?>
																		</td>
															<?php	}
																	if($vu==0){ ?>
																		<td class="text-right" >
															<?php			if(!empty($resultats[0][0][0][0]["objectif"]["total"])){
																				$prct=($resultats[0][0][0][0]["objectif"]["mois_1"]*100)/$resultats[0][0][0][0]["objectif"]["total"];
																				echo(number_format($prct,2,","," ") . "%");
																			} ?>
																		</td>
																		<td class="text-right" >
															<?php			if(!empty($resultats[0][0][0][0]["objectif"]["total"])){
																				$prct=($resultats[0][0][0][0]["objectif"]["mois_2"]*100)/$resultats[0][0][0][0]["objectif"]["total"];
																				echo(number_format($prct,2,","," ") . "%");
																			} ?>
																		</td>
																		<td class="text-right" >
															<?php			if(!empty($resultats[0][0][0][0]["objectif"]["total"])){
																				$prct=($resultats[0][0][0][0]["objectif"]["mois_3"]*100)/$resultats[0][0][0][0]["objectif"]["total"];
																				echo(number_format($prct,2,","," ") . "%");
																			} ?>
																		</td>
															<?php	} ?>
																	<td class="text-right" >
														<?php			if(!empty($resultats[0][0][0][0]["objectif"]["total"])){
																			$prct=($resultats[0][0][0][0]["objectif"]["total"]*100)/$resultats[0][0][0][0]["objectif"]["total"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
																</tr>
												<?php		}
															if($aff_previ){ ?>
																<tr class="<?=$classLigne?>" >
																	<th class="resultat"  >Prévi</th>
															<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																		<td class="resultat text-right" >
																			<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=0&previ_type=1&vu=<?=$vu?>" >
																				<?=number_format($resultats[0][0][0][0]["previ_valid"]["mois_" . $i],0,","," ")?>
																			</a>
																		</td>
															<?php	}
																	if($vu==0){ ?>
																		<td class="resultat text-right" >
																			<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=0&previ_type=1&vu=<?=$vu?>" >
																				<?=number_format($resultats[0][0][0][0]["previ_valid"]["mois_1"],0,","," ")?>
																			</a>
																		</td>
																		<td class="resultat text-right" >
																			<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=0&previ_type=1&vu=<?=$vu?>" >
																				<?=number_format($resultats[0][0][0][0]["previ_valid"]["mois_2"],0,","," ")?>
																			</a>
																		</td>
																		<td class="resultat text-right" >
																			<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=0&previ_type=1&vu=<?=$vu?>" >
																				<?=number_format($resultats[0][0][0][0]["previ_valid"]["mois_3"],0,","," ")?>
																			</a>
																		</td>
															<?php	} ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=0&previ_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats[0][0][0][0]["previ_valid"]["total"],0,","," ")?>
																		</a>
																	</td>
																</tr>
																<tr class="<?=$classLigne?>" >
																	<th class="resultat"  >Prévi "Option"</th>
															<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																		<td class="resultat text-right" >
																			<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=0&previ_type=2&vu=<?=$vu?>" >
																				<?=number_format($resultats[0][0][0][0]["previ_option"]["mois_" . $i],0,","," ")?>
																			</a>
																		</td>
															<?php	}
																	if($vu==0){ ?>
																		<td class="resultat text-right" >
																			<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=0&previ_type=2&vu=<?=$vu?>" >
																				<?=number_format($resultats[0][0][0][0]["previ_option"]["mois_1"],0,","," ")?>
																			</a>
																		</td>
																		<td class="resultat text-right" >
																			<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=0&previ_type=2&vu=<?=$vu?>" >
																				<?=number_format($resultats[0][0][0][0]["previ_option"]["mois_2"],0,","," ")?>
																			</a>
																		</td>
																		<td class="resultat text-right" >
																			<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=0&previ_type=2&vu=<?=$vu?>" >
																				<?=number_format($resultats[0][0][0][0]["previ_option"]["mois_3"],0,","," ")?>
																			</a>
																		</td>
															<?php	} ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=0&previ_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats[0][0][0][0]["previ_option"]["total"],0,","," ")?>
																		</a>
																	</td>
																</tr>

												<?php		} ?>

															<tr class="<?=$classLigne?>" >
																<th class="resultat" >Résultats</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=0&vu=<?=$vu?>" >
																			<?=number_format($resultats[0][0][0][0]["resultat"]["mois_" . $i],0,","," ")?>
																		</a>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=0&vu=<?=$vu?>" >
																			<?=number_format($resultats[0][0][0][0]["resultat"]["mois_1"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=0&vu=<?=$vu?>" >
																			<?=number_format($resultats[0][0][0][0]["resultat"]["mois_2"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=0&vu=<?=$vu?>" >
																			<?=number_format($resultats[0][0][0][0]["resultat"]["mois_3"],0,","," ")?>
																		</a>
																	</td>
														<?php	} ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=0&vu=<?=$vu?>" >
																		<?=number_format($resultats[0][0][0][0]["resultat"]["total"],0,","," ")?>
																	</a>
																</td>
															</tr>
												<?php		if($aff_prct){ ?>
																<tr class="resultat <?=$classLigne?>" style="page-break-before:avoid!important;" >
																	<th>% / CA. mois <sup>(2)</sup></th>
															<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																		<td class="resultat text-right" >
																	<?php	if(!empty($resultats["total"]["resultat"]["mois_" . $i])){
																				$prct=($resultats[0][0][0][0]["resultat"]["mois_" . $i]*100)/$resultats["total"]["resultat"]["mois_" . $i];
																				echo(number_format($prct,2,","," ") . "%");
																			} ?>
																		</td>
															<?php	}
																	if($vu==0){ ?>
																		<td class="resultat text-right" >
																	<?php	if(!empty($resultats["total"]["resultat"]["mois_1"])){
																				$prct=($resultats[0][0][0][0]["resultat"]["mois_1"]*100)/$resultats["total"]["resultat"]["mois_1"];
																				echo(number_format($prct,2,","," ") . "%");
																			} ?>
																		</td>
																		<td class="resultat text-right" >
																	<?php	if(!empty($resultats["total"]["resultat"]["mois_2"])){
																				$prct=($resultats[0][0][0][0]["resultat"]["mois_2"]*100)/$resultats["total"]["resultat"]["mois_2"];
																				echo(number_format($prct,2,","," ") . "%");
																			} ?>
																		</td>
																		<td class="resultat text-right" >
																	<?php	if(!empty($resultats["total"]["resultat"]["mois_3"])){
																				$prct=($resultats[0][0][0][0]["resultat"]["mois_3"]*100)/$resultats["total"]["resultat"]["mois_3"];
																				echo(number_format($prct,2,","," ") . "%");
																			} ?>
																		</td>
															<?php	} ?>
																	<td class="taux-atteinte text-right" >
																<?php	if(!empty($resultats[0][0][0][0]["objectif"]["total"])){
																			$prct=($resultats[0][0][0][0]["resultat"]["total"]*100)/$resultats[0][0][0][0]["objectif"]["total"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
																</tr>
													<?php	}
														} ?>

										<?php			// TOTAL

														$nb_row=2;
														if($aff_previ){
															$nb_row=$nb_row+2;
														}
														if($aff_geo_gc){
															$nb_row=$nb_row+2;
														}
														if($aff_prct){
															$nb_row=$nb_row+2;
														}
														if($_SESSION['acces']['acc_service'][1]==1){
															$nb_row=$nb_row+1;
														}

														$numLigne++;
														$classLigne="";
														if($numLigne % 2 ==0){
															$classLigne="ligne-strip";
														} ?>

														<tr class="<?=$classLigne?>" >
															<th rowspan="<?=$nb_row?>" >Total</th>
															<th class="objectif" >Objectifs</th>
													<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																<td class="objectif text-right" ><?=number_format($resultats["total"]["objectif"]["mois_" . $i],0,","," ")?></td>
													<?php	}
															if($vu==0){ ?>
																<td class="objectif text-right" ><?=number_format($resultats["total"]["objectif"]["mois_1"],0,","," ")?></td>
																<td class="objectif text-right" ><?=number_format($resultats["total"]["objectif"]["mois_2"],0,","," ")?></td>
																<td class="objectif text-right" ><?=number_format($resultats["total"]["objectif"]["mois_3"],0,","," ")?></td>
													<?php	} ?>
															<td class="objectif text-right" ><?=number_format($resultats["total"]["objectif"]["total"],0,","," ")?></td>
														</tr>

												<?php	if($aff_prct){ ?>
															<tr class="objectif <?=$classLigne?>" style="page-break-before:avoid!important;" >
																<th>% obj. <sup>(1)</sup></th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="text-right" >
														<?php			if(!empty($resultats["total"]["objectif"]["total"])){
																			$prct=($resultats["total"]["objectif"]["mois_" . $i]*100)/$resultats["total"]["objectif"]["total"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="text-right" >
														<?php			if(!empty($resultats["total"]["objectif"]["total"])){
																			$prct=($resultats["total"]["objectif"]["mois_1"]*100)/$resultats["total"]["objectif"]["total"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
																	<td class="text-right" >
														<?php			if(!empty($resultats["total"]["objectif"]["total"])){
																			$prct=($resultats["total"]["objectif"]["mois_2"]*100)/$resultats["total"]["objectif"]["total"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
																	<td class="text-right" >
														<?php			if(!empty($resultats["total"]["objectif"]["total"])){
																			$prct=($resultats["total"]["objectif"]["mois_3"]*100)/$resultats["total"]["objectif"]["total"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
														<?php	} ?>
																<td class="text-right" >
													<?php			if(!empty($resultats["total"]["objectif"]["total"])){
																		$prct=($resultats["total"]["objectif"]["total"]*100)/$resultats["total"]["objectif"]["total"];
																		echo(number_format($prct,2,","," ") . "%");
																	} ?>
																</td>
															</tr>
											<?php		}
														if($aff_previ){ ?>
															<tr class="<?=$classLigne?>" >
																<th>Prévi</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=total&previ_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["previ_valid"]["mois_" . $i],0,","," ")?>
																		</a>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=total&previ_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["previ_valid"]["mois_1"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=total&previ_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["previ_valid"]["mois_2"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=total&previ_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["previ_valid"]["mois_3"],0,","," ")?>
																		</a>
																	</td>
														<?php	} ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=total&previ_type=1&vu=<?=$vu?>" >
																		<?=number_format($resultats["total"]["previ_valid"]["total"],0,","," ")?>
																	</a>
																</td>
															</tr>
															<tr class="<?=$classLigne?>" >
																<th>Prévi "Option"</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=total&previ_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["previ_option"]["mois_" . $i],0,","," ")?>
																		</a>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=total&previ_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["previ_option"]["mois_1"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=total&previ_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["previ_option"]["mois_2"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=total&previ_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["previ_option"]["mois_3"],0,","," ")?>
																		</a>
																	</td>
														<?php	} ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_act.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=total&previ_type=2&vu=<?=$vu?>" >
																		<?=number_format($resultats["total"]["previ_option"]["total"],0,","," ")?>
																	</a>
																</td>
															</tr>
												<?php	}

														if($aff_geo_gc){ ?>
															<tr class="<?=$classLigne?>" >
																<th>Résultats GEO</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=total&cli_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat_geo"]["mois_" . $i],0,","," ")?>
																		</a>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=total&cli_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat_geo"]["mois_1"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=total&cli_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat_geo"]["mois_2"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=total&cli_type=1&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat_geo"]["mois_3"],0,","," ")?>
																		</a>
																	</td>
														<?php	} ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=total&cli_type=1&vu=<?=$vu?>" >
																		<?=number_format($resultats["total"]["resultat_geo"]["total"],0,","," ")?>
																	</a>
																</td>
															</tr>
															<tr class="<?=$classLigne?>" >
																<th>Résultats GC</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=total&cli_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat_gc"]["mois_" . $i],0,","," ")?>
																		</a>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=total&cli_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat_gc"]["mois_1"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=total&cli_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat_gc"]["mois_2"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=total&cli_type=2&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat_gc"]["mois_3"],0,","," ")?>
																		</a>
																	</td>
														<?php	} ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=total&cli_type=2&vu=<?=$vu?>" >
																		<?=number_format($resultats["total"]["resultat_gc"]["total"],0,","," ")?>
																	</a>
																</td>
															</tr>
												<?php	} ?>

														<tr class="<?=$classLigne?>" >
															<th>Résultats</th>
													<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=total&vu=<?=$vu?>" >
																		<?=number_format($resultats["total"]["resultat"]["mois_" . $i],0,","," ")?>
																	</a>
																</td>
													<?php	}
															if($vu==0){ ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=total&vu=<?=$vu?>" >
																		<?=number_format($resultats["total"]["resultat"]["mois_1"],0,","," ")?>
																	</a>
																</td>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=total&vu=<?=$vu?>" >
																		<?=number_format($resultats["total"]["resultat"]["mois_2"],0,","," ")?>
																	</a>
																</td>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=total&vu=<?=$vu?>" >
																		<?=number_format($resultats["total"]["resultat"]["mois_3"],0,","," ")?>
																	</a>
																</td>
													<?php	} ?>
															<td class="resultat text-right" >
																<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=total&vu=<?=$vu?>" >
																	<?=number_format($resultats["total"]["resultat"]["total"],0,","," ")?>
																</a>
															</td>
														</tr>
											<?php		if($aff_prct){ ?>
															<tr class="taux-atteinte <?=$classLigne?>" >
																<th>Taux d'atteinte <sup>(3)</sup></th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="taux-atteinte text-right" >
																<?php	if(!empty($resultats["total"]["objectif"]["mois_" . $i])){
																			$prct=($resultats["total"]["resultat"]["mois_" . $i]*100)/$resultats["total"]["objectif"]["mois_" . $i];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="taux-atteinte text-right" >
																<?php	if(!empty($resultats["total"]["objectif"]["mois_1"])){
																			$prct=($resultats["total"]["resultat"]["mois_1"]*100)/$resultats["total"]["objectif"]["mois_1"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
																	<td class="taux-atteinte text-right" >
																<?php	if(!empty($resultats["total"]["objectif"]["mois_2"])){
																			$prct=($resultats["total"]["resultat"]["mois_2"]*100)/$resultats["total"]["objectif"]["mois_2"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
																	<td class="taux-atteinte text-right" >
																<?php	if(!empty($resultats["total"]["objectif"]["mois_3"])){
																			$prct=($resultats["total"]["resultat"]["mois_3"]*100)/$resultats["total"]["objectif"]["mois_3"];
																			echo(number_format($prct,2,","," ") . "%");
																		} ?>
																	</td>
														<?php	} ?>
																<td class="taux-atteinte text-right" >
															<?php	if(!empty($resultats["total"]["objectif"]["total"])){
																		$prct=($resultats["total"]["resultat"]["total"]*100)/$resultats["total"]["objectif"]["total"];
																		echo(number_format($prct,2,","," ") . "%");
																	} ?>
																</td>
															</tr>
												<?php	}

														// RESALTAT EXERCICE PRECEDENT
														if($_SESSION['acces']['acc_service'][1]==1){ ?>
															<tr class="<?=$classLigne?>" style="page-break-before:avoid!important;" >
																<th>Résultat N-1</th>
														<?php	for($i=$bcl_min;$i<=$bcl_max;$i++){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=total&prec&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat-1"]["mois_" . $i],0,","," ")?>
																		</a>
																	</td>
														<?php	}
																if($vu==0){ ?>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=total&prec&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat-1"]["mois_1"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=total&prec&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat-1"]["mois_2"],0,","," ")?>
																		</a>
																	</td>
																	<td class="resultat text-right" >
																		<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=total&prec&vu=<?=$vu?>" >
																			<?=number_format($resultats["total"]["resultat-1"]["mois_3"],0,","," ")?>
																		</a>
																	</td>
														<?php	} ?>
																<td class="resultat text-right" >
																	<a href="commercial_result_fac.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&categorie=total&prec&vu=<?=$vu?>" >
																		<?=number_format($resultats["total"]["resultat-1"]["total"],0,","," ")?>
																	</a>
																</td>
															</tr>
												<?php	} ?>


													</tbody>
									<?php		} ?>
											</table>

									<?php	if($aff_prct){ ?>
												<p class="objectif mt25" >(1) % de l'objectif mensuel famille par rapport à l'objectif annuel famille.</p>
												<p class="" >(2) % du CA mensuel famille par rapport au total réalisé sur le mois.</p>
												<p class="taux-atteinte" >(3) Taux d'atteinte de l'objectif mensuel.</p>
												<p class="taux-atteinte" >(4) Taux d'atteinte de l'objectif famille sur l'exercice.</p>
									<?php	} ?>
										</div>
									</div>
								</div>
							</div>
						</div>

			<?php	} ?>

				</section>

			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="commercial_liste.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-left"></span>
						Retour
					</a>
			<?php	if(empty($erreur_txt)){ ?>
						<button type="button" class="btn btn-sm btn-info ml15" id="print" >
							<i class="fa fa-print"></i> Imprimer
						</button>
			<?php	} ?>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
	<?php			if(empty($erreur_txt)){
						if($cob_valide){ ?>
							<p class="text-center" >
								<b>Objectifs validés le <?=convert_date_txt($cob_valide_date)?><b/>
	<?php						if(isset($d_validateur)){
									echo("<b>par ". $d_validateur["uti_prenom"] . " " . $d_validateur["uti_nom"] . "</b>");
								}	?>
								<b>.</b>
							</p>
	<?php				}
					} ?>
				</div>
				<div class="col-xs-3 footer-right" >
			<?php	if (empty($erreur_txt)) {
						if($acc_utilisateur==98 OR $acc_utilisateur==403) { 
							// Eric et Christine ?>
							<a href="commercial_objectif.php?exercice=<?=$exercice?>&commercial=<?=$commercial?>" class="btn btn-warning btn-sm" role="button" >
								<span class="fa fa-pencil"></span>
								Mise à jour des objectifs
							</a>
				<?php	} elseif (!$cob_valide AND $_SESSION['acces']["acc_profil"]==10) { ?>
							<a href="commercial_objectif.php?exercice=<?=$exercice?>&commercial=<?=$commercial?>" class="btn btn-warning btn-sm" role="button" >
								<span class="fa fa-pencil"></span>
								Mise à jour des objectifs
							</a>
			<?php		}
					} ?>
				</div>
			</div>
		</footer>


<?php
		include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function (){
				$("#exercice").change(function(){
					$("#filtre").submit();
				});

				$("#conso_plus").click(function(){
					$sous_famille=$(this).data("sous_famille");
					if($("#ico_conso").prop("class")=="fa fa-minus"){
						// on masque
						$(".conso-detail").addClass("hidden");
						$("#ico_conso").prop("class","fa fa-plus")
					}else{
						$(".conso-detail").removeClass("hidden");
						$("#ico_conso").prop("class","fa fa-minus")
					}
				});

				$("#print").click(function(){

					$("#zone_print").html($("#page_print").html());

					$("#main").hide();
					$("#content-footer").hide();
					$("#zone_print").show();

					window.print();

					$("#zone_print").hide();
					$("#main").show();
					$("#content-footer").show();

					$("#main").show();
					$("#content-footer").show();

				});
			});
		</script>

	</body>
</html>
