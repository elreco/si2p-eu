<?php 
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php'); 
// Les lignes du tableau
    $lignes[0] = array(
        0 => "Nom",
        1 => "Pr�nom",
        2 => "Num�ro de DR",
        3 => "Code unit�",
        4 => "Formation",
        5 => "Date",
		6 => "Demi-journ�e",
		7 => "Lieu",
    );
    $i = 0;
    foreach($_SESSION['stagiaire_liste'] as $s){

        /*if(!empty($s['sta_last_session_id'])){
            $req = $Conn->prepare("SELECT ses_formation FROM sessions WHERE ses_id = " . $s['sta_last_session_id']);
            $req->execute();
            $for = $req->fetch();
            $ses_formation = $for['ses_formation'];
            $date = convert_date_fr($s['sta_last_session']) . " de " .  $s['sta_last_session_h_deb'] . " � " . $s['sta_last_session_h_fin'];
        }else{
            $ses_formation = "";
            $date = "";
        }*/
		
		$demi="";
		if($s['ses_demi']==1){
			$demi="Matin";
		}elseif($s['ses_demi']==2){
			$demi="Apr�s-midi";	
		}elseif($s['ses_demi']==3){
			$demi="Toute la journ�e";	
		} 
        
        $i++;
        $lignes[$i] = array(
            0 => $s['sta_nom'],
            1 => $s['sta_prenom'],
            2 => $s['sta_ref_1'],
            3 => $s['sta_ref_2'],
            4 => $s['cpr_libelle'],
            5 => convert_date_txt($s['ses_date']),
			6 => $demi,
			7 => $s['ses_lieu'],
        );
		
		
    }
  /*  echo("<pre>");
    print_r($lignes);
    echo("</pre>");
    die();*/

    // Param�trage de l'�criture du futur fichier CSV
    $chemin = 'documents/liste_stagiaire.csv';
    $delimiteur = ';'; // Pour une tabulation, utiliser $delimiteur = "t";

    // Cr�ation du fichier csv (le fichier est vide pour le moment)
    // w+ : consulter http://php.net/manual/fr/function.fopen.php
    $fichier_csv = fopen($chemin, 'w+');

    // Si votre fichier a vocation a �tre import� dans Excel,
    // vous devez imp�rativement utiliser la ligne ci-dessous pour corriger
    // les probl�mes d'affichage des caract�res internationaux (les accents par exemple)
    /*fprintf($fichier_csv, chr(0xEF).chr(0xBB).chr(0xBF));*/

    // Boucle foreach sur chaque ligne du tableau
    foreach($lignes as $ligne){
        // chaque ligne en cours de lecture est ins�r�e dans le fichier
        // les valeurs pr�sentes dans chaque ligne seront s�par�es par $delimiteur
       /* echo("<pre>");
        print_r($ligne);
        echo("</pre>");*/

        fputcsv($fichier_csv, $ligne, $delimiteur);
    }
    
    // fermeture du fichier csv
    fclose($fichier_csv);
// script a part


    // telechargement automatique
    $file = 'documents/liste_stagiaire.csv';

    if (file_exists($file)){
        header('Content-Encoding: iso-8859-1');
        header('Content-type: text/csv; charset=iso-8859-1');
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
    }

    Header("Location: stagiarie_liste.php");
?>