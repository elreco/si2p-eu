<?php  

 // VISU D'UNE FICHE STAGIAIRE

include "includes/controle_acces_cli.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');
$stagiaire=0;
if(isset($_GET['stagiaire'])){
	if(!empty($_GET['stagiaire'])){
		$stagiaire=intval($_GET['stagiaire']);
	}
}
if($stagiaire==0){
	echo("impossible d'afficher la page");
	die();
}
// DONNEES POUR AFFICHAGE

// retour
$_SESSION["retour"]="stagiaire_client_voir.php?stagiaire=" . $stagiaire;

// personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}


// LE STAGIAIRE

$req = $Conn->prepare("SELECT * FROM Stagiaires WHERE sta_id = :sta_id");
$req->bindParam(':sta_id',$stagiaire);
$req->execute();
$d_stagiaire = $req->fetch();
if(!empty($d_stagiaire)){
	
	$sta_naissance="";
	if(!empty($d_stagiaire["sta_naissance"])){
		$Dt_naiss=DateTime::createFromFormat('Y-m-d',$d_stagiaire["sta_naissance"]);
		$sta_naissance=$Dt_naiss->format("d/m/Y");
	}
	
	$sta_civilite=$d_stagiaire["sta_prenom"] . " " . $d_stagiaire["sta_nom"];
	if(!empty($d_stagiaire["sta_prenom"])){
		if($d_stagiaire["sta_titre"]==1){
			$sta_civilite="Monsieur " . $sta_civilite;
		}elseif($d_stagiaire["sta_titre"]==2){
			$sta_civilite="Madame " . $sta_civilite;
		}
	}
	
}else{
	echo("Impossible d'afficher la page!");
	die();
}

// HISTORIQUE DES EMPLOYEURS


// HISTO DE FORMATION

$sta_formations=array();

$sql="SELECT soc_id FROM Societes ORDER BY soc_id;";
$req = $Conn->query($sql);
$d_societes=$req->fetchAll();
if(!empty($d_societes)){
	
	foreach($d_societes as $soc){
	
		$ConnFct=connexion_fct($soc["soc_id"]);
	
		$sql_form="SELECT act_id,DATE_FORMAT(act_date_deb,'%d/%m/%Y') AS act_date_deb,act_agence,cli_code,cli_nom,acl_pro_reference,acl_pro_libelle,acl_id
		FROM Actions_Stagiaires LEFT JOIN Actions ON (Actions_Stagiaires.ast_action=Actions.act_id)
		LEFT JOIN Actions_Clients ON (Actions_Stagiaires.ast_action_client=Actions_Clients.acl_id)
		LEFT JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)
		WHERE ast_stagiaire=:stagiaire AND cli_id IN(" . $_SESSION['acces']['acc_groupe_liste'] . ");";
		$req_form=$ConnFct->prepare($sql_form);
		$req_form->bindParam(":stagiaire",$stagiaire);
		$req_form->execute();
		$d_formations=$req_form->fetchAll();
		if(!empty($d_formations)){
			foreach($d_formations as $df){
				$sta_formations[]=array(
					"date" => $df["act_date_deb"],
					"societe" => $soc["soc_id"],
					"agence" => $df["act_agence"],
					"produit_code" => $df["acl_pro_reference"],
					"produit_nom" => $df["acl_pro_libelle"],
					"action" => $df["act_id"],
					"action_client" => $df["acl_id"],
				);
			}
		}	
	}	
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
		<div id="main">
<?php		include "includes/header_cli.inc.php";  ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
				
					<h1><?=$sta_civilite?></h1>
					
					<div class="row">
						<div class="col-md-12" >					
							<div class="row">
								<div class="col-md-6" >
									<div class="panel" >
										<div class="panel-heading panel-head-sm">Informations générales</div>
										<div class="panel-body">
											
											<div class="row" >	
												<div class="col-md-4 text-right">Mail :</div>
												<div class="col-md-8" >
													<strong><?=$d_stagiaire["sta_mail_perso"]?></strong>
												</div>
											</div>
											<div class="row" >	
												<div class="col-md-4 text-right">Date de naissance :</div>
												<div class="col-md-8" >
													<strong><?=$sta_naissance?></strong>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								<?php
							if(!empty($sta_formations)){ ?>
								<div class="panel" >
									<div class="panel-heading panel-head-sm">Formations suivis par <?=$d_stagiaire["sta_prenom"] . " " . $d_stagiaire["sta_nom"]?></div>
									<div class="panel-body">
										<table class="table" >								
											<thead>
												<tr>
													<th class="text-center" >Date</th>
													<th colspan="2" class="text-center" >Formation</th>
													<th class="text-center" >Attestations</th>
												</tr>
											</thead>
											<tbody>
									<?php	foreach($sta_formations as $sta_form){ ?>
												<tr>
													<td class="text-center"><?=$sta_form["date"]?></td>
										
													<td class="text-center"><?=$sta_form["produit_code"]?> (<?=$sta_form["produit_nom"]?>)</td>
										
													<td class="text-center">
											<?php 		if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Attestations/" . $sta_form["societe"] . "/attestation_". $sta_form["action"] . "_" . $stagiaire . ".pdf")){ ?>
															<a href="documents/Attestations/<?= $sta_form["societe"]?>/attestation_<?=$sta_form["action"]?>_<?=$stagiaire?>.pdf" download="<?=$d_stagiaire["sta_nom"] . " " . $sta_form["produit_code"]?>.pdf" >
																<i class='fa fa-file-pdf-o' ></i>
															</a>
											<?php		}else{
															echo("&nbsp;");
														} ?>
				
													</td>
												</tr>
									<?php	} ?>
										</table>
									</div>
								</div>
					<?php	}else{ ?>
								<div class="col-md-12 text-center" style="padding:0;" >
									<div class="alert alert-warning" style="border-radius:0px;">
										Ce stagiaire n'a suivi aucune formation.
									</div>
								</div>
					<?php	} ?>
								</div>
							</div>
							
					
						</div>
					</div>
					
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="stagiaire_client_liste.php" class="btn btn-default btn-sm" role="button">
						<span class="fa fa-left"></span>
						<span class="hidden-xs">Retour</span>
					</a>
				</div>
				<div class="col-xs-6 footer-middle">&nbsp;</div>
				<div class="col-xs-3 footer-right">
					<a href="stagiaire_client.php?stagiaire=<?=$stagiaire?>" class="btn btn-warning btn-sm" role="button">
						<i class="fa fa-pencil"></i> Modifier
					</a>
				</div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>  
		<script type="text/javascript">
			jQuery(document).ready(function () {
	   
			});
		</script>
	</body>
</html>
