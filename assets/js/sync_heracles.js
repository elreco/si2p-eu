
/*********************************
	SYNCHRO DES ID HERACLES
*********************************/
var sync_heracles=false;

function devis_sync(societe,selecteur_form,selecteur_champ){
	if(sync_heracles){
		$.ajax({			
			type:'GET',
			url: 'http://192.168.2.16:83/orion/ajax/devis_enr.asp',
			data : 'societe=' + societe,
			dataType: 'JSON',
			success: function(json){
				if(json.id_heracles!=""){
					$(selecteur_champ).val(json.id_heracles);
					$(selecteur_form).submit();	
				}else{
					afficher_message("Erreur","danger","Erreur de synchronisation Héraclès");
				}
			},
			error: function(json) {
				afficher_message("Erreur","danger","Erreur de synchronisation Héraclès");
			}			
		});
	}else{
		$(selecteur_champ).val(0);
		$(selecteur_form).submit();		
	}
}
function action_sync(societe,selecteur_form,selecteur_champ,callback,param1,param2){
	console.log("action_sync()");
	var synchro=false;
	if(sync_heracles && societe<14){
		synchro=true;
	}
	if(synchro){
		$.ajax({			
			type:'GET',
			url: 'http://192.168.2.16:83/orion/ajax/action_enr.asp',
			data : 'societe=' + societe,
			dataType: 'JSON',
			success: function(json){
				if(json.id_heracles!=""){
					$(selecteur_champ).val(json.id_heracles);
					if(selecteur_form!=""){
						$(selecteur_form).submit();	
					}else{				
						callback(json,param1,param2);
					}
				}else{
					afficher_message("Erreur","danger","Erreur de synchronisation Héraclès");
				}
			},
			error: function(json) {
				afficher_message("Erreur","danger","Erreur de synchronisation Héraclès");
			}			
		});
	}else{
		$(selecteur_champ).val(0);
		if(selecteur_form!=""){
			$(selecteur_form).submit();	
		}else{				
			callback("",param1,param2);
		}
	}
}
function commande_sync(fournisseur,selecteur_form,selecteur_champ,callback,param1,param2){
	console.log("commande_sync()");
	if(sync_heracles){
		$.ajax({			
			type:'GET',
			url: 'http://192.168.2.16:83/orion/ajax/commande_enr.asp',
			data : 'fournisseur=' + fournisseur,
			dataType: 'JSON',
			success: function(json){
				if(json.id_heracles!=""){
					
					$(selecteur_champ).val(json.id_heracles);
					if(selecteur_form!=""){
						//alert("selecteur_form" + selecteur_form);
						$(selecteur_form).submit();	
					}else{			
						//alert("call");
						callback(json,param1,param2);
					}
				}else{
					afficher_message("Erreur","danger","Erreur de synchronisation Héraclès");
				}
			},
			error: function(json) {
				afficher_message("Erreur","danger","Erreur de synchronisation Héraclès");
			}			
		});
	}else{
		$(selecteur_champ).val(0);
		if(selecteur_form!=""){
			$(selecteur_form).submit();	
		}else{			
			callback(json,param1,param2);
		}
	}
}

function facture_sync(societe,selecteur_form,selecteur_champ,callback,param1,param2){
	console.log("facture_sync()");
	// utiliser uniquement sur facture.php
	if(societe!=""){
		societe=parseInt(societe);
	}else{
		societe=0;
	}
	var synchro=false;
	if(sync_heracles && societe!=11 && societe<14 && societe!=7){
		synchro=true;
	}

	if(synchro){
		$.ajax({			
			type:'GET',
			url: 'http://192.168.2.16:83/orion/ajax/facture_enr.asp',
			data : 'societe=' + societe,
			dataType: 'JSON',
			success: function(json){
				if(json.id_heracles!=""){
					$(selecteur_champ).val(json.id_heracles);
					console.log(json.id_heracles);
					if(selecteur_form!=""){
						$(selecteur_form).submit();	
					}else{				
						callback(json,param1,param2);
					}
				}else{
					afficher_message("Erreur","danger","Erreur de synchronisation Héraclès");
				}
			},
			error: function(json) {
				afficher_message("Erreur","danger","Erreur de synchronisation Héraclès");
			}			
		});
	}else{
		$(selecteur_champ).val(0);
		if(selecteur_form!=""){
			$(selecteur_form).submit();	
		}else{				
			callback("",param1,param2);
		}
	}
}