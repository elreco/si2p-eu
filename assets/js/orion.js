
/*********************
INI DOC READY
 1- INITIALISATION PLUGIN
	1-1- SELECT2
	1-2- DATEPICKER
	1-3- SUMMERNOTE
	1-4- DATATABLE
2- INTERRACTION UTILISATEUR
3- FORMATAGE AUTO

FONCTION
 A - DECONNECTION
 B - MODAL ET DIALOGUE UTILISATEUR
 C - FORMULAIRE
 D - Requetes AJAX
	D - 1 - ACTIONS
	D - 2 - FOURNISSEURS
	D - 3 - AGENCES
	D - 4 - CLIENTS
	D - 5 - PRODUITS
	D - 6 - ATTESTATIONS

 E - PLUGIN SELECT2
 F - CALLBACK AJAX NPU
 G - AFFICHAGE AUTOMATISE NPU
 H - FORMATAGE
******************************/

var delai_deconnect=1140000;
var delai_deconnect_alert=60000;
/*var delai_deconnect=30000;
var delai_deconnect_alert=30000;*/
var timer_deco;

var societ=0;

/*********************************
		INI DOC READY
*********************************/
jQuery(document).ready(function(){

	"use strict";
	// Init Theme Core
	Core.init();

	$(".check-all").click(function(){
		check_all($(this));
	});

	// gestion du modal erreur
	if($('#modal-error')){
		$('#modal-error').modal('show')
	};

	// initialise le systeme de deco
	timer_deco=setTimeout(alerte_deconnect, delai_deconnect);
	$("#reconnect").click(function(){
		$.stopSound();
		reconnect();
	});

	$("#user-role").change(function(){
		changerSociete($(this).val());
	});

	$(".timeline-item").click(function(){
		//console.log("NOTIFICATION");
	});

	//*********************
	//	1- INITIALISATION PLUGIN
	//************************ */

		/*------------------------------------
				1-1- SELECT2
		-------------------------------------*/

		if($(".select2").length){
			$(".select2").select2({
				 language: "fr"
			});
		}

		// client 0
		if($(".select2-client").length){
			$(".select2-client").select2({
				minimumInputLength: 1,
				ajax: {
					url: "ajax/ajax_client_select.php",
					dataType: 'json',
					delay: 250,
					method: 'GET',
					data: function (params) {
						return {
							q: params.term, // les termes de la requête
							categorie: $(this).data("client_categorie"), //$(".select2-client-categorie").val(),
							sous_categorie: $(this).data("client_s_categorie"), //$(".select2-client-s-categorie").val(),
							groupe: $(this).data("groupe"),
							region: $(this).data("region"),
							lib_option:$(this).data("lib_option"),
							blackliste:$(this).data("blackliste")

						};
					},
					processResults: function (data) {
						// data correspond au tableau json des résultat
						// data.nom permet d'accéder à l'information $retour["nom"]
						return {
							results: data,
						};
					},
				},
				templateResult: function(data) {
					// permet de chosir quelles informations seront affichées dans la liste de résultat
					// parmit les éléments de data
					//if (data.loading) return data.text;
					return data.nom;
				},
				templateSelection: function(data) {
					// permet de chosir quelles informations seront affichées dans le select
					// parmit les éléments de data
					// une fois que l'utilisateur aura fait un choix.

					//if (data.loading) return data.text;
					// Sinon affichage du placeholder (data.text).
					if(data.nom){
						// revoyer par la requete ajax
						return data.nom;
					}else{
						// cree par defaut à l'ini du plugin
						return data.text;
					}
					//if (data.text) return data.text;

					//console.log("C data : " + data);
					//	return data.nom;
				},
			});

			/*$(".select2-client-categorie").change(function(){
				$(".select2-client").val(0).trigger("change");
			});
			$(".select2-client-s-categorie").change(function(){
				$(".select2-client").val(0).trigger("change");
			});*/

		}

		// client enregistre en base N
		if($(".select2-client-n").length){
			$(".select2-client-n").select2({
				minimumInputLength: 1,
				ajax: {
					url: "ajax/ajax_client_select_n.php",
					dataType: 'json',
					delay: 250,
					method: 'GET',
					data: function (params) {
						return {
							q: params.term,
							categorie: $(".client-n-categorie").val(),
							sous_categorie: $(".client-n-s-categorie").val(),
							agence: $(".client-n-agence").val(),
							societe: $(".client-n-societe").val(),
							archive:$(this).data("archive"),
							acces:$(this).data("acces"),
							blackliste:$(this).data("blackliste"),
							first_fac:$(this).data("first_fac")
						};
					},
					processResults: function (data) {
						return {
							results: data,
						};
					},
				},
				templateResult: function(data) {
					return data.nom;
				},
				templateSelection: function(data) {
					if(data.nom){
						// revoyer par la requete ajax
						return data.nom;
					}else{
						// cree par defaut à l'ini du plugin
						return data.text;
					}
				},
			});
		}


		$(".select2-classification").change(function(){
			get_cli_classifications_categories($(this).val(),actualiser_select2,".select2-classification-categorie",0);
			get_cli_classifications_types($(this).val(),actualiser_select2,".select2-classification-type",0);
		});

		/*$(".select2-classification-categorie").change(function(){
			get_cli_classifications_types($(".select2-classification").val(),actualiser_select2,".select2-classification-type",0);
		});
		$(".select2-classification-type").change(function(){
			get_cli_classifications_categories($(".select2-classification").val(),actualiser_select2,".select2-classification-categorie",0);
		});*/

		$(".select2-secteur").change(function(){
			get_cli_s_secteurs(secteur,callback,param1,param2)
			get_cli_s_secteurs($(this).val(),actualiser_select2,".select2-sous-secteur",0);
		});

		// stagiaire

		if($(".select2-stagiaire").length){
			$(".select2-stagiaire").select2({
				minimumInputLength: 1,
				ajax: {
					url: "ajax/ajax_stagiaire_select.php",
					dataType: 'json',
					delay: 250,
					method: 'GET',
					data: function (params) {
						return {
							q: params.term,
							client:$(this).data("client"),
							client_actif:$(this).data("client_actif"),
							action_client:$($(this).data("e_action_client")).val(),

						};
					},
					processResults: function (data) {
						return {
							results: data,
						};
					},
				},
				templateResult: function(data) {
					return data.nom;
				},
				templateSelection: function(data) {
					if(data.nom){
						return data.nom;
					}else{
						return data.text;
					}
				},
			});
		}


		// fournisseur
		if($(".select2-fournisseur").length){
			$(".select2-fournisseur").select2({
				minimumInputLength: 1,
				ajax: {
					url: "ajax/ajax_fournisseur_select.php",
					dataType: 'json',
					delay: 250,
					method: 'GET',
					data: function (params) {
						return {
							q: params.term, // les termes de la requête
							famille_type : $(this).data("famille_type"),
						};
					},
					processResults: function (data) {
						// data correspond au tableau json des résultat
						// data.nom permet d'accéder à l'information $retour["nom"]

						return {
							results: data,
						};
					},
				},
				templateResult: function(data) {
					// permet de chosir quelles informations seront affichées dans la liste de résultat
					// parmit les éléments de data
					//if (data.loading) return data.text;
					return data.nom;
				},
				templateSelection: function(data) {
					// permet de chosir quelles informations seront affichées dans le select
					// parmit les éléments de data
					// une fois que l'utilisateur aura fait un choix.
					//if (data.loading) return data.text;
					// Sinon affichage du placeholder (data.text).
					if(data.nom){
						// revoyer par la requete ajax
						return data.nom;
					}else{
						// cree par defaut à l'ini du plugin
						return data.text;
					}
					//if (data.text) return data.text;

					//console.log("C data : " + data);
				//	return data.nom;
				},
			});
		}

		// utilisateur
		if($(".select2-utilisateur").length){
			$(".select2-utilisateur").select2({
				minimumInputLength: 1,
				ajax: {
					url: "ajax/ajax_utilisateur_select.php",
					dataType: 'json',
					delay: 250,
					method: 'GET',
					data: function (params) {
						return {
							q: params.term, // les termes de la requête
						};
					},
					processResults: function (data) {
						// data correspond au tableau json des résultat
						// data.nom permet d'accéder à l'information $retour["nom"]

						return {
							results: data,
						};
					},
				},
				templateResult: function(data) {
					// permet de chosir quelles informations seront affichées dans la liste de résultat
					// parmit les éléments de data
					//if (data.loading) return data.text;
					return data.nom;
				},
				templateSelection: function(data) {
					// permet de chosir quelles informations seront affichées dans le select
					// parmit les éléments de data
					// une fois que l'utilisateur aura fait un choix.

					//if (data.loading) return data.text;
					// Sinon affichage du placeholder (data.text).
					if(data.nom){
						// revoyer par la requete ajax
						return data.nom;
					}else{
						// cree par defaut à l'ini du plugin
						return data.text;
					}
					//if (data.text) return data.text;

					//console.log("C data : " + data);
				//	return data.nom;
				},
			});
		}

		// utilisateur societe agence
		if($(".select2-utilisateur-societe-agence").length){
			$(".select2-utilisateur-societe-agence").select2({
				minimumInputLength: 1,
				ajax: {
					url: "ajax/ajax_utilisateur_societe_agence_select.php",
					dataType: 'json',
					delay: 250,
					method: 'GET',
					data: function (params) {
						return {
							q: params.term, // les termes de la requête
							societe: $(".select2-societe").val(),
							agence: $(".select2-societe-agence").val(),
						};
					},
					processResults: function (data) {
						// data correspond au tableau json des résultat
						// data.nom permet d'accéder à l'information $retour["nom"]

						return {
							results: data,
						};
					},
				},
				templateResult: function(data) {
					// permet de chosir quelles informations seront affichées dans la liste de résultat
					// parmit les éléments de data
					//if (data.loading) return data.text;
						return data.nom;
				},
				templateSelection: function(data) {
					// permet de chosir quelles informations seront affichées dans le select
					// parmit les éléments de data
					// une fois que l'utilisateur aura fait un choix.

					//if (data.loading) return data.text;
					// Sinon affichage du placeholder (data.text).
					if(data.nom){
						// revoyer par la requete ajax
						return data.nom;
					}else{
						// cree par defaut à l'ini du plugin
						return data.text;
					}
				},
			});
		}
		// utilisateur societe agence

		// PRODUIT
		if($(".select2-produit").length){

			$(".select2-produit").select2({
				minimumInputLength: 1,
				ajax: {
					url: "ajax/ajax_select2_produit.php",
					dataType: 'json',
					delay: 250,
					method: 'GET',
					data: function (params) {
						return {
							q: params.term, // les termes de la requête
							categorie: $(".select2-produit-cat").val(),
							famille: $(".select2-produit-fam").val(),
							sous_famille: $(".select2-produit-sous-fam").val(),
							sous_sous_famille: $(".select2-produit-sous-sous-fam").val(),
							intra: $(".select2-produit-intra").is(":checked"),
							inter: $(".select2-produit-inter").is(":checked"),
							type: $(this).data("produit_type"),
							data_client: $(this).data("client"),
							select2_client: $(".select2-produit-client").val(),
							base_client : $(this).data("base_client"),
							produit : $(this).data("produit")
						};

					},
					processResults: function (data) {
						// data correspond au tableau json des résultat
						// data.nom permet d'accéder à l'information $retour["nom"]
						//console.log("A");
						return {
							results: data,
						};
					},
				},
				templateResult: function(data) {
					// permet de chosir quelles informations seront affichées dans la liste de résultat
					// parmit les éléments de data
					//if (data.loading) return data.nom;
					//console.log("INTRA " + $(".select2-produit-intra").is(":checked"));
					//console.log("INTER " + $(".select2-produit-inter").is(":checked"));
					//console.log("B : " + data);
					return data.text;
				},
				templateSelection: function(data) {
					// permet de chosir quelles informations seront affichées dans le select
					// parmit les éléments de data
					// une fois que l'utilisateur aura fait un choix.
					//console.log("B : " + data);
					if(data.nom){
						// revoyer par la requete ajax
						return data.text;
					}else{
						// cree par defaut à l'ini du plugin
						return data.text;
					}

				},
			});

			$(".select2-produit-client").change(function(){
				$(".select2-produit").val(0).trigger("change");
			});
			$(".select2-produit-intra").click(function(){
				$(".select2-produit").val(0).trigger("change");
			});
			$(".select2-produit-inter").click(function(){
				$(".select2-produit").val(0).trigger("change");
			});
		}

		$(".select2-famille").change(function(){
			get_sous_familles($(this).val(),actualiser_select2,".select2-famille-sous-famille",0);
		});

		$(".select2-famille-sous-famille").change(function(){
			get_sous_sous_familles($(this).val(),actualiser_select2,".select2-famille-sous-sous-famille",0);
		});

		$(".select2-categorie").change(function(){
			get_sous_categories($(this).val(),actualiser_select2,".select2-categorie-sous-categorie",0);
		});


		// VEHICULES
        //get_vehicules($(".select2-categorie-vehicule").val(),actualiser_select2,".select2-vehicule",0);
		$(".select2-categorie-vehicule").change(function(){
			var selecteur=".select2-vehicule";
			get_vehicules($(this).val(),actualiser_select2,selecteur,0);
		});


		// SOCIETE
		$(".select2-societe").change(function(){
			var selecteur=".select2-societe-agence";
			var acc_drt = $(this).data("acc_drt");
			//alert("acc_drt : " + acc_drt);
			/*if($(this).data("selecteur")!=""){
				selecteur=$(this).data("selecteur");
			}*/
			var age_interco="";
			get_agences($(this).val(),acc_drt,age_interco,actualiser_select2,selecteur,0);
		});

		// commercial d'une agence ou d'une société
		$(".select2-com-agence").change(function(){
			var archive=$(this).data("com_archive");
			var age_required=$(this).prop("required");
			var agence=$(this).val();

			var societe=0;
			if($(".select2-com-societe").length>0){
				societe=$(".select2-com-societe").val();
			}
			// maj des commerciaux de l'agence
			var select_com=".select2-commercial";
			if($(this).data("select_commercial")){
				select_com=$(this).data("select_commercial");
			};

			if(agence>0 || !age_required){
				get_commerciaux(societe,agence,archive,actualiser_select2,select_com,"");
			}else{
				actualiser_select2("",select_com,0);
			}
		});

		// categorie de client
		$(".select2-cli-cat").change(function(){
			var cli_s_cat=$(this).data("cli_s_cat");

			if(cli_s_cat!=""){

				// le select existe
				if($("#" + cli_s_cat).length>0){

					if($(this).val()>0){

						$.ajax({
							type:'POST',
							url: 'ajax/ajax_sous_categorie.php',
							data : 'field=' + $(this).val(),
							dataType: 'json',
							success: function(data){

								if (data['0'] == undefined){

									$("#" + cli_s_cat).find("option:gt(0)").remove();
									if($("#bloc_" + cli_s_cat).length>0){
										$("#bloc_" + cli_s_cat).hide();
									}
								}else{

									$("#" + cli_s_cat).find("option:gt(0)").remove();
									$.each(data, function(key, value) {
										$("#" + cli_s_cat).append($("<option></option>").attr("value",value["csc_id"]).text(value["csc_libelle"]));
									});
									if($("#bloc_" + cli_s_cat).length>0){
										$("#bloc_" + cli_s_cat).show();
									}
								}
							}
						});
					}else{
						// pas de categorie
						$("#" + cli_s_cat).find("option:gt(0)").remove();
						if($("#bloc_" + cli_s_cat).length>0){
							$("#bloc_" + cli_s_cat).show();
						}
					}
				}
			}
		});


		/*------------------------------------
			1-2- DATEPICKER
		-------------------------------------*/

		$(".datepicker").datepicker({
			defaultDate: "+1w",
			dateFormat: "dd/mm/yy",
			closeText: 'Fermer',
			prevText: 'Précédent',
			nextText: 'Suivant',
			currentText: 'Aujourd\'hui',
			monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
			monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
			dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
			dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
			dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
			weekHeader: 'Sem.',
			numberOfMonths: 1,
			prevText: '<i class="fa fa-chevron-left"></i>',
			nextText: '<i class="fa fa-chevron-right"></i>',
			beforeShow: function (input, inst) {
				var themeClass = $(this).parents('.admin-form').attr('class');
				var smartpikr = inst.dpDiv.parent();
				if (!smartpikr.hasClass(themeClass)) {
					inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
				}
			},
			onClose: function (selectedDate) {
				$("#datepicker-to").datepicker("option", "minDate", selectedDate);
			}
		});

		/*------------------------------------
			1-3- SUMMERNOTE
		-------------------------------------*/

		if($(".summernote").length){
			$('.summernote').summernote({
				lang: 'fr-FR', // default: 'en-US'
				height: 255, //set editable area's height
				focus: false, //set focus editable area after Initialize summernote
				oninit: function() {},
				onChange: function(contents, $editable) {},
				  toolbar: [
					// [groupName, [list of button]]
					['style', ['bold', 'italic', 'underline', 'clear']],
				  ]
			});
		}

		if($(".summernote-img").length){
			$('.summernote-img').summernote({
				lang: 'fr-FR',
				height:250,
				toolbar: [
					// [groupName, [list of button]]
					['style', ['bold', 'italic', 'underline', 'clear']],
					['font', ['strikethrough', 'superscript', 'subscript']],
					['fontsize', ['fontsize']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
					['height', ['height']],
					['picture', ['picture']]
				],
				callbacks: {
					onPaste: function (e) {
							alert
							var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
							e.preventDefault();
							document.execCommand('insertText', false, bufferText);
					}
				}
			});
		}

		/*------------------------------------
			1-4- DATATABLE
		-------------------------------------*/

		// format de base
		if($(".datatable").length){
			$('.datatable').DataTable({
				"language": {
					"url": "/vendor/plugins/DataTables/media/js/French.json"
				},
				paging: false,
				searching: false,
				info: false
			});
		}

	//***********************************
	//	2- INTERRACTION UTILISATEUR
	//************************************/

	// affiche ou masque un bloc en cliquant sur un boutton
	$(".btn-aff-mas").click(function(){

		var cible=$(this).data("cible");
		var btn=$(this).find("i");
		if(btn.hasClass("fa-plus")){
			$("#" + cible).show();
			btn.removeClass("fa-plus");
			btn.addClass("fa-minus");
		}else{
			$("#" + cible).hide();
			btn.removeClass("fa-minus");
			btn.addClass("fa-plus");
		};

	});

	// ini par defaut avec un callback determinee si la requete ajax doit être traité par un autre callback il faut personaliser dans le script concernee

	$(".get-client").change(function(){
		var client=$(this).val();
		var adr_type=adr_type=$(this).data("adr_type");
		var adr_contact=$(this).data("adr_contact");
		var agence=$(".get-client-agence").val();
		var societe=$(".get-client-societe").val();
		if(client==null){
			client=0;
		}
		if(client>0){
			get_client(client,societe,agence,adr_type,adr_contact,actualiser_get_client,"","");
		}else{
			actualiser_get_client();
		}
	});

	/*
	NPU CF  AFFICHAGE AUTOMATISE NPU
	$(".aff-adresse").change(function(){
		afficher_adresse($(this).val());
	});

	$(".aff-contact").change(function(){
		afficher_contact($(this).val());
	});
	*/

	// UPLOAD D'IMAGE
	$(".gui-file").change(function(){
		var input_file_id=$(this).attr('id');
		if($("#" + input_file_id + "_champ").length){
			$("#" + input_file_id + "_champ").val($(this).val());
		}
		if($("#" + input_file_id + "_preview").length){
			var input=this;
			if (input.files && input.files[0]){
			  var reader = new FileReader();

			  reader.onload = function (e) {
				$("#" + input_file_id + "_preview").prop('src', e.target.result);
			  }
			  reader.readAsDataURL(input.files[0]);
			}
		}
	});

	$(".img-preview-supp").click(function(){
		var id_img=$(this).data("id_img");
		var url_img="";
		if($(this).data("url_img")){
			url_img=$(this).data("url_img");
		};
		if(id_img!=""){
			// a l'enrehistrement impossible de différencier l'img edite dans modif de l'image supprimer
			// du coup on fait la suppression en Ajax avant enregistrement.
			if(url_img!="" && url_img!="undefined"){
				$.ajax({
					url : 'ajax/ajax_fichier_del.php',
					type : 'POST',
					data : 'url_fichier=' + url_img,
					dataType : 'JSON',
					success : function(data){
						supprimer_img_preview(id_img);
					},
					error : function(data){
						afficher_message("Erreur","danger",data.responseText);
					}
				});
			}else{
				supprimer_img_preview(id_img);
			}
		}
	});

	$(".btn-print").click(function(){
		imprimer_page();
	});

	$(".gui-date").blur(function(){
		if($(this).val()!=""){
			var date_valide=testerdate($(this).val());
			if(!date_valide){
				$(this).val("");
				if($("#" + this.id + "_alerte").length>0){
					$("#" + this.id + "_alerte").html("format de date incorrect");
					$("#" + this.id + "_alerte").show().delay(5000).fadeOut();
				}
			}
		}
	});

	/***********************************
		 3- FORMATAGE AUTO
	************************************/

	$('.prenom').keyup(function(event) {
        var textBox = event.target;
        var start = textBox.selectionStart;
        var end = textBox.selectionEnd;
        textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
        textBox.setSelectionRange(start, end);
    });

	$('.siren').focusout(function(){
		if($(this).val().length!=11){
			$(this).val("");
		}
	});

	$('.siret').focusout(function(){
		if($(this).val().length!=6){
			$(this).val("");
		}
	});

	$('.siretlong').focusout(function(){
		if($(this).val().length!=18){
			$(this).val("");
		}
	});

	$(".input-int").focusout(function(){
		// type numbre = nombre entier obligatoire positif ou negatif
		var int_value=$(this).val().replace(".",",");
		int_value=parseInt(int_value);
		if(isNaN(int_value)){
			$(this).val(0);
		}else{
			if($(this).prop("min")!=""){
				var min_value=parseInt($(this).prop("min"));
				if(int_value<min_value){
					int_value=min_value;
				}
			}
			if($(this).prop("max")!=""){
				var max_value=parseInt($(this).prop("max"));
				if(int_value>max_value){
					int_value=max_value;
				}
			}
			if($(this).val()!=int_value){
				$(this).val(int_value);
			}
		}
	});

	$(".input-float").focusout(function(){
		convertir_en_float($(this));
	});

	$(".input-euro").focusout(function(){
		convertir_en_euro($(this));
	});
});
(jQuery);

//***********************************
//	FONCTION
//**********************************/

	//***********************************
	//  A - DECONNECTION
	//***********************************/

	// gestion de la deconnection

	var deconnect_timer;
	function alerte_deconnect(){
		if($('#ModalDeconnect')){
			$.playSound("assets/sound/deco.mp3");
			$('#ModalDeconnect').modal('show');
		};
		deconnect_timer=setTimeout(deconnect, delai_deconnect_alert);
	}

	function deconnect(){
		document.location="deconnect.php";
	}

	function reconnect(){
		$.ajax({
			url : 'ajax/ajax_reconnect.php',
			//type : 'GET',
			//data : 'droit=' + droit + '&utilisateur=<?=$utilisateur?>&action=1',
			//dataType : 'html',
			success : function(code_html, statut){
				clearTimeout(deconnect_timer);
				if($('#ModalDeconnect')){
					$('#ModalDeconnect').modal('hide')
				};
				setTimeout(alerte_deconnect, delai_deconnect);
			},
			error : function(resultat, statut, erreur){
			}
		});
	}
	// fin gestion de la deconnection

	function changerSociete(val_soc){
		$.ajax({
			url : 'ajax/ajax_change_soc.php',
			type : 'GET',
			data : 'choix_soc=' + val_soc,
			dataType : 'html',
			success : function(code_html, statut){
				location.reload();
			},
			error : function(resultat, statut, erreur){
				alert("ERREUR");
			}
		});
	}

	//******************************
	//	B - MODAL ET DIALOGUE
	//*******************************/

	// modal de confirmation affichage et gestion de la reponse
	function modal_confirmation_user(texte,callback,param1,param2,param3){

		console.log("modal_confirmation_user()");

		if(texte!=""){
			$("#modal_confirmation_user .modal-body").html(texte);
		}
		$("#modal_confirmation_user").modal("show");
		$("#confirmation_user").off();
		$("#confirmation_user").click(function(){
			$("#modal_confirmation_user").modal("hide");
			callback(param1,param2,param3);
		});

	}

	function modal_alerte_user(texte,callback){
		if(texte!=""){
			$("#modal_alerte_user .modal-body").html(texte);
		}
		$("#modal_alerte_user").modal("show");

		if(callback){
			callback();
		}
	}

	function afficher_txt_user(elt,message,classe,delai){
		text_html="<p class='txt-user text-center " + classe + "' >" + message + "</p>";

		elt.html(text_html);
		if(delai>0){
			setTimeout(function() {
				$('.txt-user').remove();
			}, delai);
		}
	}

	function afficher_message(titre,type,message){

		new PNotify({
			title: titre,
			text: message,
			type: type, // all contextuals available(info,system,warning,etc)
			hide: true,
			delay: 8000
		});
	}


	function imprimer_page(){
		$("#zone_print").html($("#page_print").html());

		$("#main").hide();
		$("#content-footer").hide();
		$("#zone_print").show();

		window.print();

		$("#zone_print").hide();
		$("#main").show();
		$("#content-footer").show();

		$("#main").show();
		$("#content-footer").show();
	}

	//*************************
	//	C - FORMULAIRE
	//**************************/

	// envoie d'un formulaire en ajax
	function envoyer_form_ajax(formulaire,callback,param1,param2){
		console.log("envoyer_form_ajax()");

		var $this = formulaire; // L'objet jQuery du formulaire
		$.ajax({
			url: $this.attr('action'),
			type: $this.attr('method'),
			data: $this.serialize(),
			dataType: 'json',
			success: function(data) {
				if(callback){
					callback(data,param1,param2);
				}
			},
			error: function (data){
				modal_alerte_user(data.responseText);
			}
		});

	}


	// validation automatique d'un formulaire
	function valider_form(form_selecteur){

		var valide=true;
		//alert($(form_selecteur + " .gui-input[required]").length);
		$(form_selecteur + " .gui-input[required]").each(function(){
			if($(this).val()==""){
				if($("#" + this.id + "_required").html()==""){
					$("#" + this.id + "_required").html("Vous devez compléter ce champ");
				}
				$("#" + this.id + "_required").show();
				valide=false;
			}else{
				$("#" + this.id + "_required").hide();
			}
		});

		// REQUIRED PLUGGIN

		$(form_selecteur + " .select2[required]").each(function(){
			if($(this).val()=="" || $(this).val()==0 || $(this).val()==null){
				$("#" + this.id + "_required").show();
				valide=false;
			}else{
				$("#" + this.id + "_required").hide();
			}

		})

		// l'attribut required sur un summernote fausse la validation HTML
		$(form_selecteur + " .summernote[required]").each(function(){
			//console.log("summernote required");
			if($(this).code()==""){
				$("#" + this.id + "_required").show();
				valide=false;
			}else{
				$("#" + this.id + "_required").hide();
			}
		});

		return valide;

	}

	function check_all(elt){
		console.log("check_all : " + elt);
		var id=elt.attr('id');
		var classe=id.replace("_all", "");
		var classe=classe.replace("_", "-");
		if(elt.is(":checked")){
			$("." + classe).prop("checked",true);
		}else{
			$("." + classe).prop("checked",false);
		}
	}

	// PASSEWORD
	// test la conformité d'un password
	function testPasse(passe){
		var car_min=false;
		var car_maj=false;
		var chiffre=false;
		var car_spe=false;
		passe_valid=true;
		if(passe!=""){
			if(passe.length<8){
				passe_valid=false;
			}else{
				for(i=0;i<=passe.length-1;i++){
					if((passe.charCodeAt(i)>=65)&&(passe.charCodeAt(i)<=90)){
						car_maj=true;
					}else if((passe.charCodeAt(i)>=97)&&(passe.charCodeAt(i)<=122)){
						car_min=true;
					}else if((passe.charCodeAt(i)>=48)&&(passe.charCodeAt(i)<=57)){
						chiffre=true;
					}else if((passe.charCodeAt(i)==33)||(passe.charCodeAt(i)==45)||(passe.charCodeAt(i)==46)||(passe.charCodeAt(i)==58)||(passe.charCodeAt(i)==59)||(passe.charCodeAt(i)==63)||(passe.charCodeAt(i)==64)||(passe.charCodeAt(i)==95)){
						car_spe=true;
					};
				};
				if((!car_maj)||(!car_min)||(!chiffre)||(!car_spe)){
					passe_valid=false;
				};
			};
		};
		return passe_valid;
	}

	// genre un passeword
	function generePasse(){

	var liste_car_spe_passe="!-.:;?@_"; // attention si liste modifier mettre a jour testPasse()
	var rnum = Math.floor(Math.random() * liste_car_spe_passe.length);
	car_spe=liste_car_spe_passe.substring(rnum,rnum+1);


	var mon_passe="";
	var car_a="";
	var car_b="";
	var car_indice_b=0;

	var tab_mon_passe=new Array(8);
	tab_mon_passe[1]=String.fromCharCode(Math.round(Math.random()*25)+65);
	tab_mon_passe[2]=String.fromCharCode(Math.round(Math.random()*25)+65);
	tab_mon_passe[3]=String.fromCharCode(Math.round(Math.random()*25)+97);
	tab_mon_passe[4]=String.fromCharCode(Math.round(Math.random()*25)+97);
	tab_mon_passe[5]=String.fromCharCode(Math.round(Math.random()*25)+97);
	tab_mon_passe[6]=car_spe;
	tab_mon_passe[7]=String.fromCharCode(Math.round(Math.random()*8)+48);
	tab_mon_passe[8]=String.fromCharCode(Math.round(Math.random()*8)+48);
	for(i=1;i<=8;i++){
		car_a=tab_mon_passe[i];
		car_indice_b=Math.round(Math.random())+7;
		car_b=tab_mon_passe[car_indice_b];
		tab_mon_passe[car_indice_b]=car_a;
		tab_mon_passe[i]=car_b;
	};
	for(i=1;i<=8;i++){
		mon_passe=mon_passe + tab_mon_passe[i];
	};
	return mon_passe;
}

	function supprimer_img_preview(id_img){
		if(id_img!=""){
			$("#" + id_img + "_preview").prop("src","assets/img/400x140.jpg");
			$("#" + id_img + "_champ").val("");
		}
	}


	function testerdate(date_txt){
		var masque="##/##/####";
		if (date_txt!=""){
			if (date_txt.length!=masque.length){
				return false;
			}else{
				for (i=0;i<masque.length;i++){
					if((masque.charAt(i)=="#")&&(isNaN(date_txt.charAt(i)))){
						return false;
						break;
					};
					if ((masque.charAt(i)=="/")&&(date_txt.charAt(i)!="/")) {
						return false;
						break;
					};
				}
				fc_jour_txt=eval(date_txt.substring(0,2));
				fc_mois_txt=eval(date_txt.substring(3,5));
				fc_annee_txt=eval(date_txt.substring(6,10));

				fc_jour_int=parseInt(fc_jour_txt);
				fc_mois_int=parseInt(fc_mois_txt);
				fc_annee_int=parseInt(fc_annee_txt);
				if ((fc_jour_int>31) || (fc_jour_int<1) || (fc_mois_int>12) || (fc_mois_int<1)){
					return false;
				}else{
					bis=0
					if (((fc_annee_int % 4 == 0) && (fc_annee_int % 100 != 0)) || (fc_annee_int % 400 == 0)){
						bis=1;
					}
					if ((fc_mois_int==2)&&( ((bis == 0)&&(fc_jour_int>28)) || ((bis==1)&&(fc_jour_int>29)) ) ){
						return false;
					}else if (  ((fc_mois_int==4) || (fc_mois_int==6) || (fc_mois_int==9) || (fc_mois_int==11)) && (fc_jour_int>30)  ){
						return false;
					}else{
						return true;
					}
				}
			}
		}else{
			return true;
		}
	}

	//******************************************
	//	D - REQUETE AJAX
	//******************************************

	//******************************************
	//	D - 1 - ACTIONS
	//*******************************************/

		function get_action(action,callback,param){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_action.php',
				data : 'action=' + action,
				dataType: 'JSON',
				success: function(data){
					callback(data,param);
				},
				error: function(data) {


				}
			});
		}

		function get_action_adresses(action,callback,param1,param2){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_action_adresses.php',
				data : 'action=' + action,
				dataType: 'JSON',
				success: function(data){
					callback(data,param);
				},
				error: function() {
				}
			});
		}

		function get_action_sessions(action,action_client,callback,param1,param2){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_action_horraires.php',
				data : 'action=' + action,
				dataType: 'JSON',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function() {
				}
			});
		}

		// recupere tous les clients qui participe à une action ou une session
		function get_action_clients(action,session,callback,param1,param2){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_action_clients.php',
				data : 'action=' + action + "&session=" + session + "&societ=" + societ,
				dataType: 'json',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					afficher_message("Erreur","danger",data.responseText);
				}
			});
		}

		// recupere les infos d'un enregistrement action client
		function get_action_client(action_client,stagiaire,callback,param1,param2){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_action_client.php',
				data : 'action_client=' + action_client + "&stagiaire=" + stagiaire + "&societ=" + societ,
				dataType: 'json',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					afficher_message("Erreur","danger",data.responseText);
				}
			});
		}

		// recupere la liste des actions selon critere
		function get_actions(client,pro_fam,pro_s_fam,pro_s_s_fam,pro_type,callback,param1,param2){

			$.ajax({
				type:'POST',
				url: 'ajax/ajax_actions_get.php',
				data : "client=" + client + "&famille=" + pro_fam + "&sous_famille=" + pro_s_fam + "&sous_sous_famille=" + pro_s_s_fam + "&type=" + pro_type,
				dataType: 'json',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					afficher_message("Erreur","danger",data.responseText);
				}
			});
		}

		// recupere la liste des actions pouvant etre ajoute à un devis selon critere
		function get_actions_devis(client,pro_fam,pro_s_fam,pro_s_s_fam,pro_type,callback,param1,param2){

			$.ajax({
				type:'POST',
				url: 'ajax/ajax_actions_devis_get.php',
				data : "client=" + client + "&famille=" + pro_fam + "&sous_famille=" + pro_s_fam + "&sous_sous_famille=" + pro_s_s_fam + "&type=" + pro_type,
				dataType: 'json',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					afficher_message("Erreur","danger",data.responseText);
				}
			});
		}
		// retourne la liste des lignes de devis pouvant etre liées à une action
		function get_action_devis(client,action,pro_fam,pro_s_fam,pro_s_s_fam,pro_type,callback,param1,param2){

			$.ajax({
				type:'POST',
				url: 'ajax/ajax_action_devis_get.php',
				data : "client=" + client + "&action=" + action + "&famille=" + pro_fam + "&sous_famille=" + pro_s_fam + "&sous_sous_famille=" + pro_s_s_fam + "&type=" + pro_type,
				dataType: 'json',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					afficher_message("Erreur","danger",data.responseText);
				}
			});
		}

		// validation d'une dérogation marge
		function valider_marge_dero(action,callback,param1,param2){

			$.ajax({
				type:'POST',
				url: 'ajax/ajax_action_dero_valide.php',
				data : "action=" + action,
				dataType: 'json',
				success: function(data){
					if(data["warning"]!=""){
						modal_alerte_user(data["warning"]);
					}
					callback(data,param1,param2);
				},
				error: function(data) {
					modal_alerte_user(data.responseText);
				}
			});
		}


	//******************************************
	//	D - 2 - FOURNISSEURS
	//*******************************************
		// retourne un contact lié à un fournisseur
		function get_fournisseur_contact(contact, callback){
			//console.log("societe : " + societe);
			if(contact>0){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_fournisseur_contact_get.php',
					data : 'contact=' + contact,
					dataType: 'json',
					success: function(data){
						callback(data);
					},
					error: function() {

					}
				});
			}
		}

		function get_fournisseur(fournisseur,callback,param1,param2){
			if(fournisseur>0){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_fournisseur_get.php',
					data : 'fournisseur=' + fournisseur,
					dataType: 'json',
					success: function(data){
						callback(data,param1,param2);
					},
					error: function(data) {
						modal_alerte_user(data.responseText);
					}
				});
			}
		}

	//******************************************
	//	D - 3 - AGENCES
	//*******************************************/
	function get_agence(agence,callback,param1,param2){
		//console.log("get_agence(" + agence + ")");
		$.ajax({
			type:'POST',
			url: 'ajax/ajax_agence_get.php',
			data : 'agence=' + agence,
			dataType: 'JSON',
			success: function(data){
				callback(data);

			},
			error: function(data) {
			}
		});
	}

	// retourne les agences lié à une societe
	function get_agences(societe,acc_drt,age_interco,callback,param1,param2){
		console.log(callback);
		console.log(param1);
		if(societe>0){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_agences_get.php',
				data : 'societe=' + societe + "&acc_drt=" + acc_drt + "&age_interco=" + age_interco,
				dataType: 'json',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data){

				}
			});
		}else if(callback!=""){
			callback("",param1,param2);
		}
	}
		// retourne les agences lié à une societe
		function get_vehicules(categorie,callback,param1){

			console.log(callback);
			console.log(param1);
			if(categorie>0){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_vehicules_get.php',
					data : 'categorie=' + categorie,
					dataType: 'json',
					success: function(data){
						callback(data,param1);
					},
					error: function(data){

					}
				});
			}else if(callback!=""){
				callback("",param1);
			}
		}

	//******************************************
	//	D - 4 - CLIENTS
	//******************************************

		// retourne les infos d'un client
		function get_client(client,societe,agence,adr_type,adr_contact,callback,param1,param2){
			//console.log("get_client()");
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_client_get.php',
				data : 'client=' + client + '&adr_type=' + adr_type + '&adr_contact=' + adr_contact + "&agence=" + agence + "&societe=" + societe,
				dataType: 'JSON',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data){
					callback(data,param1,param2);

				}
			});
		}

		/*function get_cli_s_classifications(classification,callback,param1,param2){
			console.log("get_cli_s_classifications()");
			console.log("classification : " + classification);
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_cli_s_classifications.php',
				data : 'classification=' + classification,
				dataType: 'JSON',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					modal_alerte_user(data.responseText);
				}
			});
		}*/

		function get_sous_categories(categorie,callback,param1,param2){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_sous_categories.php',
				data : 'categorie=' + categorie,
				dataType: 'JSON',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function() {
				}
			});
		}

		function get_cli_s_secteurs(secteur,callback,param1,param2){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_activites_sous_secteurs.php',
				data : 'secteur=' + secteur,
				dataType: 'JSON',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					modal_alerte_user(data.responseText);
				}
			});
		}

		// classification ERP ERT
		function get_cli_classifications_categories(classification,callback,param1,param2){

			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_classifications_categories.php',
				data : 'classification=' + classification,
				dataType: 'JSON',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					modal_alerte_user(data.responseText);
				}
			});
		}

		function get_cli_classifications_types(classification,callback,param1,param2){

	$.ajax({
		type:'POST',
		url: 'ajax/ajax_get_classifications_types.php',
		data : 'classification=' + classification,
		dataType: 'JSON',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}

		// retourne les adresses des clients passés en paramètre ou ceux qui participe à l'action
		function get_adresses(client,ref,type,action,groupe,callback,param1,param2){

			$.ajax({
				type:'POST',
				url: 'ajax/ajax_client_adresses_get.php',
				data : 'client=' + client + "&ref=" + ref + "&type=" + type + "&action=" + action + "&groupe=" + groupe,
				dataType: 'JSON',
				success: function(data){
					//console.log("param1 : " + param1);
					//console.log("param2 : " + param2);
					callback(data,param1,param2);
				},
				error: function() {

				}
			});
		}

		function get_contacts(ref,ref_id,adresse,adr_lien,callback,param1,param2){
			//console.log("get_contacts(" + ref_id + "," + ref + ")");
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_client_contacts_get.php',
				data : 'ref_id=' + ref_id + "&ref=" + ref + "&adresse=" + adresse + "&adr_lien=" + adr_lien,
				dataType: 'JSON',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					afficher_message("Erreur","danger",data.responseText);
				}
			});
		}

		function get_clients(categorie,sous_categorie,archive,acces,callback,param1,param2){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_clients_get.php',
				data : 'categorie=' + categorie + "&sous_categorie=" + sous_categorie + "&archive=" + archive + "&acces=" + acces,
				dataType: 'JSON',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					afficher_message("Erreur","danger",data.responseText);
				}
			});
		}



	//******************************************
	//	D - 5 - PRODUITS
	//*******************************************/

		function get_sous_familles(famille,callback,param1,param2){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_sous_familles.php',
				data : 'famille=' + famille,
				dataType: 'JSON',
				success: function(data){
					console.log(famille);
					callback(data,param1,param2);
				},
				error: function(data) {
					data.responseText
				}
			});
		}

		function get_sous_sous_familles(sous_famille,callback,param1,param2){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_sous_sous_familles.php',
				data : 'sous_famille=' + sous_famille,
				dataType: 'JSON',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					data.responseText
				}
			});
		}

		// retourne LES DONNEES d'un produit ou plusieurs produits
		function get_produit(client_id,produit_id,aff_ht,base_client,callback,param1,param2){
			if(produit_id>0){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_get_produit.php',
					data : 'produit=' + produit_id + "&client=" + client_id + "&aff_ht=" + aff_ht + "&base_client=" + base_client + "&acces=1&societ=" + societ,
					dataType: 'json',
					success: function(data){
						callback(data,param1,param2);
					},
					error: function() {

					}
				});
			}
		}

		// retourne LA LISTE des produits d'un client
		/* permet de générer un select2 */
		function get_produits(client,categorie,famille,sous_famille,sous_sous_famille,intra,inter,callback,param1,param2){
			console.log("get_produits()");

			// la requete n'etant pas déclenchée par une interaction utilisateur mais appellé dans un contexte precis => le data- ne fonction pas
			// il doit donc etre passé en parametre
			// acces géré en auto sur script ajax

			$.ajax({
				type:'GET',
				url: 'ajax/ajax_select2_produit.php',
				data : 'data_client=' + client + '&categorie=' + categorie + "&famille=" + famille + "&sous_famille=" + sous_famille + "&sous_sous_famille=" + sous_sous_famille + "&intra=" + intra + "&inter=" + inter,
				dataType: 'json',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					afficher_message("Erreur","danger",data.responseText);

				}
			});
		}


	//******************************************
	//	D - 6 - ATTESTATIONS
	//******************************************

		function get_attestations(famille,sous_famille,sous_sous_famille,callback,param1,param2){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_get_attestations.php',
				data : "famille=" + famille + "&sous_famille=" + sous_famille + "&sous_sous_famille=" + sous_sous_famille,
				dataType: 'json',
				success: function(data){
					callback(data,param1,param2);
				},
				error: function(data) {
					afficher_message("Erreur","danger",data.responseText);

				}
			});
		}

	//******************************************
	//	D - 7 - AUTRES
	//*******************************************/

function get_intervenant(intervenant,callback){
	$.ajax({
		url : 'ajax/ajax_get_intervenant.php',
		type : 'POST',
		data : 'intervenant=' + intervenant,
		dataType : 'JSON',
		success : function(data, statut){
			callback(data);
		},
		error : function(resultat, statut, erreur){
		}
	});
}

function get_societe(societe,callback,param1,param2){
	//console.log("get_societe(" + societe + ")");
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_get_societe.php',
		data : 'societe=' + societe,
		dataType: 'JSON',
		success: function(data){
			callback(data);

		},
		error: function(data) {
			if(data){
			}else{
			}
		}
	});
}


// retourne les donnée d'un produit
function get_commerciaux(societe,agence,archive,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_get_commerciaux.php',
		data : 'societe=' + societe + "&agence=" + agence + "&archive=" + archive,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data){
			modal_alerte_user(data.responseText);
		}
	});
}

function get_utilisateur(utilisateur,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_get_utilisateur.php',
		data : 'utilisateur=' + utilisateur,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data){
			modal_alerte_user(data.responseText);
		}
	});
}


	//-----------------------------
	//		AJAX SET
	//-------------------------------*/
function set_client_produit(client,produit,ht_intra,ht_inter,callback,param1,param2){
	if(client>0){
		$.ajax({
			type:'POST',
			url: 'ajax/ajax_set_client_produit.php',
			data : "client=" + client + "&produit=" + produit + "&ht_intra=" + ht_intra + "&ht_inter=" + ht_inter,
			dataType: 'json',
			success: function(data){
				callback(data,param1,param2);
			},
			error: function(data) {
			}
		});
	}
}


function set_base_champ(societe,table,champ,valeur,champ_id,valeur_id,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_set_base_champ.php',
		data : "societe=" + societe + "&table=" + table + "&champ=" + champ + "&valeur=" + valeur + "&champ_id=" + champ_id + "&valeur_id=" + valeur_id,
		dataType: 'json',
		success: function(data){
			if(callback){
				callback(data,param1,param2);
			}
		},
		error: function(data) {
			if(callback){
				callback(data,param1,param2);
			}
		}
	});
}

//***** AJAX DEL *****

function del_client_produit(produit,client,callback,param1,param2){
	if(client>0){
		$.ajax({
			type:'POST',
			url: 'ajax/ajax_del_client_produit.php',
			data : "client=" + client + "&produit=" + produit,
			dataType: 'json',
			success: function(data){
				callback(data,param1,param2);
			},
			error: function(data) {
				modal_alerte_user(data.responseText);
			}
		});
	}
}

function del_client_info(info,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_del_client_info.php',
		data : "info=" + info,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}

function del_client_corresp(corresp,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_del_client_corresp.php',
		data : "correspondance=" + corresp,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}

function del_client_doc(doc_id,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_del_client_doc.php',
		data : "document=" + doc_id,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}

function del_client_contact(contact,client,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_del_client_contact.php',
		data : "contact=" + contact + "&client=" + client,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}

function del_client_adresse(adresse,client,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_del_client_adresse.php',
		data : "adresse=" + adresse + "&client=" + client,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}

// suspects
function del_suspect_adresse(adresse,suspect,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_del_suspect_adresse.php',
		data : "adresse=" + adresse + "&suspect=" + suspect,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}

function del_suspect_contact(contact,suspect,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_del_suspect_contact.php',
		data : "contact=" + contact + "&suspect=" + suspect,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}

function del_suspect_corresp(corresp,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_del_suspect_corresp.php',
		data : "correspondance=" + corresp,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}

// FONCTION LIE A L'ONGLET INFO

// delete un contact d'un fournisseur
function del_fournisseur_contact(contact,callback){
	if(contact>0){
		$.ajax({
			type:'POST',
			url: 'ajax/ajax_del_fournisseur_contact.php',
			data : "contact=" + contact,
			dataType: 'json',
			success: function(data){
				callback(data);
			},
			error: function(data) {
				console.log("Erreur : " + data.responseText);
			}
		});
	}
}


// ***** AJAX CHECK *****

function check_siret(siren,siret,adresse,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_check_siret.php',
		data : "siren=" + siren + "&siret=" + siret + "&adresse=" + adresse,
		dataType: 'JSON',
		success: function(data){
			callback(data,siren,siret,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}

function check_siret_suspect(siren,siret,adresse,callback,param1,param2){
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_check_siret_suspect.php',
		data : "siren=" + siren + "&siret=" + siret + "&adresse=" + adresse + "&societe=" + societe + "&agence=" + agence,
		dataType: 'json',
		success: function(data){
			callback(data,param1,param2);
		},
		error: function(data) {
			modal_alerte_user(data.responseText);
		}
	});
}



//****************************************
//	E - PLUGIN SELECT2
//*****************************************/

function actualiser_select2(json,selecteur,valeur_defaut){
    if(selecteur){
        $(selecteur + " option[value!='0'][value!='']").remove();
    	$(selecteur).select2({
    		data: json,
    		closeOnSelect: true
    	});
    }

	if(valeur_defaut>0){
		console.log("valeur_defaut : " + valeur_defaut);
		$(selecteur).val(valeur_defaut).trigger("change");
	}
}


//***************************************
//	F - CALLBACK AJAX NPU
//*****************************************
/*TROP COMPLIQUE DE GENERALISE LE CALLBACK
 Le traitement du retour JS varie selon le script d'appel
 les callbacks doivent etre géré sur les scripts d'appel */

/*function actualiser_client(json){
	//console.log("actualiser_client()");
	if(json){
		if(json[0]){
			if($(".actu-client-societe").length==1){
				$(".actu-client-societe").val(json[0].societe);
			}

			if($(".actu-client-select2-com").length==1){
				$(".actu-client-select2-com").val(json[0].commercial).trigger("change");
			}
			$(".reg-formule").each(function(){
				if($(this).val()==json[0].reg_formule){
					$(this).prop("checked",true);
					$("input[name=reg_nb_jour_" + json[0].reg_formule + "]").val(json[0].reg_nb_jour);
					$("input[name=reg_fdm_" + json[0].reg_formule + "]").val(json[0].reg_fdm);
					$(this).trigger("click");
				}
			});
		}

		if(json[1]){
			actualiser_adresses(json[1],"defaut");
		}
		if(json[2]){
			actualiser_contacts(json[2],"defaut");
		}
	}

}*/

// memorise le resultat ajax et actualise les options d'une liste adressse
function actualiser_adresses(json_adresses,selected){
	console.log("actualiser_adresses()");
	adresse_select=0;
	$(".actu-adresses option[value!='0']").remove();
	if(json_adresses){
		// on memorise les adressses
		tab_adresse=new Array();
		for(i=0;i<=json_adresses.length-1;i++){
			tab_adresse[json_adresses[i].id]=new Array(9);
			tab_adresse[json_adresses[i].id][0]=json_adresses[i].nom;
			tab_adresse[json_adresses[i].id][1]=json_adresses[i].service;
			tab_adresse[json_adresses[i].id][2]=json_adresses[i].ad1;
			tab_adresse[json_adresses[i].id][3]=json_adresses[i].ad2;
			tab_adresse[json_adresses[i].id][4]=json_adresses[i].ad3;
			tab_adresse[json_adresses[i].id][5]=json_adresses[i].cp;
			tab_adresse[json_adresses[i].id][6]=json_adresses[i].ville;
			tab_adresse[json_adresses[i].id][7]=json_adresses[i].defaut;
			tab_adresse[json_adresses[i].id][8]=json_adresses[i].ref_id;
			tab_adresse[json_adresses[i].id][9]=json_adresses[i].societe;
			console.log(json_adresses[i].societe);

			//console.log(" adresse " + json_adresses[i].id + " : " + tab_adresse[json_adresses[i].id]);

			if(json_adresses[i].defaut && selected=="defaut"){
				adresse_select=json_adresses[i].id;
			}
		}
		$(".actu-adresses").select2({
			data: json_adresses
		});
		if(adresse_select>0){
			$(".actu-adresses").val(adresse_select);
			if($(".actu-adresses").hasClass("aff-adresse")){
				$(".aff-adresse").trigger("change");
			}

		}
	}else if($(".actu-adresses").hasClass("aff-adresse")){
		$(".aff-adresse").val(0).trigger("change");
	}
};

// memorise le resultat ajax et actualise les options d'une liste contact
function actualiser_contacts(json_contacts,selected){
	//console.log("actualiser_contacts()");
	contact_select=0;
	$(".actu-contacts option[value!='0']").remove();

	if(json_contacts){
		// on memorise les contacts
		tab_contact=new Array();
		for(i=0;i<=json_contacts.length-1;i++){
			tab_contact[json_contacts[i].id]=new Array(6);
			tab_contact[json_contacts[i].id][0]=json_contacts[i].nom;
			tab_contact[json_contacts[i].id][1]=json_contacts[i].prenom;
			tab_contact[json_contacts[i].id][2]=json_contacts[i].tel;
			tab_contact[json_contacts[i].id][3]=json_contacts[i].portable;
			tab_contact[json_contacts[i].id][4]=json_contacts[i].defaut;
			if(json_contacts[i].defaut && selected=="defaut"){
				contact_select=json_contacts[i].id;
			}
		}
		$(".actu-contacts").select2({
			data: json_contacts
		});

		if(contact_select>0){
			$(".actu-contacts").val(contact_select);
			if($(".actu-contacts").hasClass("aff-contact")){
				$(".aff-contact").trigger("change");
			}

		}
	}else if($(".actu-contacts").hasClass("aff-contact")){
		$(".aff-contact").val(0).trigger("change");
	}
};

//****************************************
//	G - AFFICHAGE AUTOMATISE NPU
//****************************************
/* TROP COMPLIQUE DE GENERALISE DES FONCTIONS D'AFFICHAGE
MEME RAISON QUE POUR CALLBACk
 Le traitement du retour JS varie selon le script d'appel
 les callbacks doivent etre géré sur les scripts d'appel */

// renseigne les champ lié à une adresse
/*
function afficher_adresse(adresse_id){
	console.log("afficher_adresse(" + adresse_id + ")");
	if(adresse_id!=0){
		$(".adr-nom").val(tab_adresse[adresse_id][0]);
		$(".adr-service").val(tab_adresse[adresse_id][1]);
		$(".adr1").val(tab_adresse[adresse_id][2]);
		$(".adr2").val(tab_adresse[adresse_id][3]);
		$(".adr3").val(tab_adresse[adresse_id][4]);
		$(".adr-cp").val(tab_adresse[adresse_id][5]);
		$(".adr-ville").val(tab_adresse[adresse_id][6]);
		$(".adr-societe").val(tab_adresse[adresse_id][9]);
	}else{
		$(".champ_adresse").val("");
	}
	//console.log("FIN afficher_adresse()");
}
*/

// affiche les données d'un contact
/*function afficher_contact(contact_id){
	//console.log("afficher_contact(" + contact_id + ")");
	if(contact_id!=0){
		$(".con-nom").val(tab_contact[contact_id][0]);
		$(".con-prenom").val(tab_contact[contact_id][1]);
		$(".con-tel").val(tab_contact[contact_id][2]);
		$(".con-portable").val(tab_contact[contact_id][3]);
	}else{
		$(".champ-contact").val("");
	}
}*/

//****************************************
//	H - FORMATAGE
//*****************************************/

	function convertir_en_float(elt){

		// type numbre = nombre entier obligatoire positif ou negatif
		var float_value=elt.val().replace(",",".");
		float_value=parseFloat(float_value);
		if(isNaN(float_value)){
			elt.val(0);
		}else{
			if(elt.prop("min")!=""){
				var min_value=parseFloat(elt.prop("min"));
				if(float_value<min_value){
					float_value=min_value;
				}
			}
			if(elt.prop("max")!=""){
				var max_value=parseFloat(elt.prop("max"));
				if(float_value>max_value){
					float_value=max_value;
				}
			}
			if(elt.val()!=float_value){
				elt.val(float_value);
			}
		}
	}

	function convertir_en_euro(elt){
		console.log("convertir_en_euro");
		// type numbre = nombre entier obligatoire positif ou negatif
		var float_value=elt.val().replace(",",".");
		float_value=parseFloat(float_value);
		if(isNaN(float_value)){
			elt.val(0);
		}else{

			if(elt.prop("min")!=""){
				var min_value=parseFloat(elt.prop("min"));
				if(float_value<min_value){
					float_value=min_value;
				}
			}
			if(elt.prop("max")!=""){
				var max_value=parseFloat(elt.prop("max"));
				if(float_value>max_value){
					float_value=max_value;
				}
			}

			float_value=float_value.toFixed(2);
			if(elt.val()!=float_value){
				elt.val(float_value);
			}
		}
	}


	function nl2br (str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
function checkbox_shift(checkbox){
    var $chkboxes = checkbox;
    var lastChecked = null;
    $chkboxes.click(function(e) {
        if (!lastChecked) {
            lastChecked = this;
            return;
        }

        if (e.shiftKey) {
            var start = $chkboxes.index(this);
            var end = $chkboxes.index(lastChecked);

            $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
        }

        lastChecked = this;
    });
}
