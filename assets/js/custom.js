
$(document).ready(function(){
    
    $('.juridique').show(400);
    $('.telephone').mask('00 00 00 00 00');
    $('.siren').mask('000 000 000');
    $('.siret').mask('000 00');
    $('.date-2').mask('000 00');
    $('.siretlong').mask('000 000 000 000 00');
    $('.code-postal').mask('00 000');
    $('.date').mask('00/00/0000');
    $('.mois').mask('00/0000');
    $('.numero_bc').mask('00000'); 
    $('.nom').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
        'translation': {
            A: {pattern: /[A-Z a-z0-9-É]/}
        },
        onKeyPress: function (value, event) {
            event.currentTarget.value = value.toUpperCase();
        }
    }); 
	$('.heure').mask('12h34',{
		'translation': {
			1: {pattern: /[0-2]/},
			2: {pattern: /[0-9]/},
			3: {pattern: /[0-5]/},
			4: {pattern: /[0-9]/}
		},
    });
	
	 $('.input-int').mask('00000000000', {
      'translation': {
        0: {pattern: /[0-9]/}
      },

    });
	
	
});

