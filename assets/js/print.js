
jQuery(document).ready(function () {				
	
	mise_en_page();
	
	$("#print").click(function(){
		
		$("#zone_print").html($("#container_print").html());
		
		$("#main").hide();
		$("#content-footer").hide();					
		$(".btn").hide();
		
		$("#zone_print").show();
		
		window.print();
		
		$("#zone_print").hide();
		
		$("#main").show();
		$("#content-footer").show();
		$(".btn").show();
		
		
	});
	
	
});

function mise_en_page(){
	
	console.log("mise_en_page()");		
	
	var ppc=(parseInt($("#page_print").width())/l_page);
	var hauteur=parseInt((h_page*ppc));
	//console.log("Hauteur : " + hauteur + "px");
	
	var saut="";
		
	var h_header=0;
	var h_head=0;
	var h_ligne_header=0;
	var h_foot=0;
	var h_footer=0;
	var libre=0;
	
	var nb_ligne=0;
	var num_ligne=0;
	var h_ligne=0;
	
	// on recalcul la totalite de la misen en page
	// pas beseoin de reference page pour les éléments ajouté
	$(".header").remove();
	$(".footer").remove();
	$(".ligne-header").remove();
	$(".saut").remove();

	$(".page").each(function( index ){
		
		page=$(this).data("page");
		
		saut="";
		
		h_header=parseInt($("#page_print #header_" + page).height());
		
		h_head=0;
		if($("#page_print #head_" + page).length>0){
			h_head=parseInt($("#page_print #head_" + page).height());
		}
		h_ligne_header=0;
		if($("#page_print #ligne_header_" + page).length>0){
			h_ligne_header=parseInt($("#page_print #ligne_header_" + page).height());
		}
		h_foot=0;
		if($("#page_print #foot_" + page).length>0){
			$("#page_print #foot_" + page).css("margin-top","0px");
			h_foot=parseInt($("#page_print #foot_" + page).outerHeight(true));
		}
		
		$("#page_print #footer_" + page).css("margin-top","0px");
		h_footer=parseInt($("#page_print #footer_" + page).height());
	
		
		libre=hauteur-(h_header+h_head+h_ligne_header+h_footer);
		console.log("hauteur " + hauteur);
		console.log("h_header " + h_header);
		console.log("h_head " + h_head);
		console.log("h_ligne_header " + h_ligne_header);
		console.log("h_foot " + h_foot);
		console.log("h_footer " + h_footer);
	
		nb_ligne=$(".ligne-body-" + page).length;
		num_ligne=0;
		h_ligne=0;
		
		//console.log("Page " + page);
		//console.log("nb_ligne " + nb_ligne);
		//console.log("Libre " + libre);
		
		console.log("nbLigneBody " + $(".ligne-body-" + page).length);

		$(".ligne-body-" + page).each(function( index ){
			
			num_ligne=num_ligne+1;

			h_ligne=parseInt($(this).height());
			
			console.log("num_ligne : " + num_ligne);
			console.log("h_ligne : " + h_ligne + "px");
			console.log("Libre " + libre);
			
			if((libre-h_ligne<0)||(num_ligne==nb_ligne && (libre-(h_ligne+h_foot)<0)) ){
	
				saut="<div class='footer' style='margin-top:" + libre + "px;' >";
					saut=saut+$("#footer_" + page).html();
				saut=saut+"</div>"
				saut=saut+"<div class='saut' >&nbsp;</div>";
				
				saut=saut+"<div class='header' >";
					saut=saut+$("#header_" + page).html();
				saut=saut+"</div>";
				
				if($("#ligne_header_" + page).length>0){
					saut=saut+"<div class='ligne-header' >";
						saut=saut+$("#ligne_header_" + page).html();
					saut=saut+"</div>";
				}
				
				libre=hauteur-(h_header + h_ligne_header + h_footer);
				
				//console.log("Libre Nouvelle page " + libre + "px");
				if(num_ligne==nb_ligne){
					libre=libre-h_foot;
				}
				
				libre=libre-h_ligne;
				
				//console.log("saut");
				$(this).before(saut);	
				
			}else{
				//console.log("libre " + libre);
				libre=libre-h_ligne;
				if(num_ligne==nb_ligne){
					libre=libre-h_foot;
				}
				if(libre<0){
					libre=0;
				}					
			}
		});
		if($("#page_print #foot_" + page).length>0){
			$("#page_print #foot_" + page).css("margin-top", libre + "px");
		}else{
			$("#page_print #footer_" + page).css("margin-top", libre + "px");
		}
		
	});
	
	// ANNEXE
	$(".annexe").each(function( index ){
		var annexe=$(this).data("annexe");
		
		var h_annexe=0;
		if($("#h_annexe_" + annexe).length>0){
			h_annexe=parseInt($("#h_annexe_" + annexe).height());
		}
		var s_annexe=0;
		if($("#s_annexe_" + annexe).length>0){
			s_annexe=parseInt($("#s_annexe_" + annexe).height());
		}
		var f_annexe=0;
		if($("#f_annexe_" + annexe).length>0){
			f_annexe=parseInt($("#f_annexe_" + annexe).height());
		}
		libre=hauteur-(h_annexe + s_annexe + f_annexe);
		$("#f_annexe_" + annexe).css("margin-top", libre + "px");
	});
}