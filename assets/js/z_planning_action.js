
// UTILISE SUR action_client.php
// UTILISE SUR action_mod.php


	// retourne les infos d'un client
	function get_client(client){
		
		$.ajax({			
			type:'POST',
			url: 'ajax/ajax_get_client.php',
			data : 'client=' + client, 
			dataType: 'JSON',                
			success: function(data){	
				afficher_client(data);
			},
			error: function() {
				
			}
		});
	}
	
	function afficher_client(data){
		console.log("afficher_client()");
		//console.log("data.commercial : " + data.commercial);
		$("#commercial").val(data.commercial).trigger("change");
		if($("#produit_inter")){
			if(data.categorie==5){
				// interco
				if($("#produit_inter").is(":checked")){
					$("#produit_intra").prop("checked",true);
					$(".produit-type").change();
				}
				$("#produit_inter").prop("disabled",true);					
			}else{
				$("#produit_inter").prop("disabled",false);
			}
		}
	}
		
	// affiche les champs adresse selon le type d'adresse
	function affichage_adresse(type_adresse){
		console.log("affichage_adresse(" + type_adresse + ")");
		//console.log("adresse_defaut : " + adresse_defaut);
		switch(type_adresse){
			case "1":
				// client			
				$(".cont-adr-client").show();
				$(".cont-adresse").show();
				$(".cont-contact").show();			
				$(".cont-contact-liste").show();	
				
				$(".adresse-client").val(adresse_defaut).trigger("change");		
				$(".contact").val(contact_defaut).trigger("change");		
				
				break;
			case "2":
				// agence
				$(".cont-adr-client").hide();
				$(".cont-adresse").hide();
				$(".cont-contact").hide();			
				$(".cont-contact-liste").hide();

				$(".adresse-client").val(0).trigger("change");		
				$(".contact").val(0).trigger("change");				
				break;
				
			default:
				// autre
				$(".cont-adr-client").hide();
				$(".cont-adresse").show();
				$(".cont-contact").show();			
				$(".cont-contact-liste").hide();
				$(".adresse-client").val(0).trigger("change");		
				$(".contact").val(0).trigger("change");						
				break;
		}
	}

	// gestion de l'affichage des champs contacts
	/*function affichage_contact(type_adresse){
		console.log("affichage_contact(" + type_adresse + ")");
		switch(type_adresse){
			case "1":	
				// client
				$(".cont-contact").show();			
				$(".cont-contact-liste").show();				
				break;
			case "2":
				// agence
				$(".cont-contact").hide();			
				$(".cont-contact-liste").hide();
				
				break;
				
			default:
				// autre
				console.log("autre");
				$(".cont-contact").show();			
				$(".cont-contact-liste").hide();
				afficher_contact(0);
				break;
		}
		console.log("<br/>");
	}*/
	
	
	// affiche les elements du form en fonction du type de produit
	function afficher_produit_type(type){
		console.log("afficher_produit_type(" + type + ")");
		if(type==1){
			$("#cont_ca_inter").hide();
			$("#cont_ca_intra").show();
		}else{
			$("#cont_ca_inter").show();
			$("#cont_ca_intra").hide();
		}					
	}
	
			
	// actualise la liste des produits disponible
	function actualiser_produits(json_produits){
		console.log("actualiser_produits()");
		var value_old=$(".produit").val();
		var value_new=0;
		$(".produit option[value!='0']").remove();
		if(json_produits){				
			$(".produit").select2({
				data: json_produits				
			});
			//console.log("produit : " + produit);
			if(value_new>0){
				$(".produit").val(value_new).trigger("change");
			}else{
				$(".produit").val(value_old).trigger("change");
			}
		}
	}
	
	// retourne les donnée d'un produit
	function get_produit(produit_id,client_id,callback){
		console.log("planning_action get_produit()");
		if(produit_id>0){
			$.ajax({			
				type:'POST',
				url: 'ajax/ajax_get_produit.php',
				data : 'produit=' + produit_id + "&client=" + client_id, 
				dataType: 'json',
				success: function(data){
					callback(data);
					
				},
				error: function() {
					
				}			
			});
		}
	}
	
	function afficher_produit(produit){
		console.log("afficher_produit(" + produit + ")");
		ca_intra_unit=0;
		var ca_inter_unit=0;

		if(produit){
			// CA
			if(type_produit==1){
					// intra
				ca_intra_unit=produit.ht_intra/produit.nb_demi_jour;
				$(".ca-intra-unit").val(ca_intra_unit);
				$(".ca-inter-unit").val(0);
				calculer_ca_intra();
				
				if(produit.ca_verrou){
					$(".ca-intra").prop("readonly",true);
				}else{
					$(".ca-intra").prop("readonly",false);	
				}								
			}else{
					// inter
				ca_inter_unit=produit.ht_inter;
				$(".ca-inter-unit").val(ca_inter_unit);
				$(".ca-intra-unit").val(0);
				$(".ca-intra").val(0);
				if(produit.ca_verrou){
					$(".ca-inter-unit").prop("readonly",true);
				}else{
					$(".ca-inter-unit").prop("readonly",false);	
				}
			}
			
		}
		if(produit.nb_demi_jour>1){
			$(".nb-session").val(1).trigger("change");
			$(".nb-session").prop("readonly",true);
		}else{
			$(".nb-session").prop("readonly",false);
		}
		
		$(".produit-libelle").val(produit.libelle);
		$(".facturation").val(produit.facturation);
	}
	
	function calculer_ca_intra(){
		console.log("calculer_ca_intra()");
		//console.log("type_produit : " + type_produit);
		if(type_produit==1){
			var ca_intra=0;
			if($('.action-date').size()>0){
				nb_demi_j=$('.action-date:checked').size();
				//console.log("CHAT");
			}
			//console.log("nb_demi_j : " + nb_demi_j);
			//console.log("ca_intra_unit : " + ca_intra_unit);
			ca_intra=ca_intra_unit*nb_demi_j;
			$(".ca-intra").val(ca_intra);
		}
		
	}
	
	function calculer_ca_intra_unit(){
		console.log("calculer_ca_intra_unit()");
		if(type_produit==1){
			ca_intra=$(".ca-intra").val();
			ca_intra_unit=0;
			if($('.action-date').size()>0){
				nb_demi_j=$('.action-date:checked').size();
			}
			if(ca_intra!=0){
				ca_intra_unit=ca_intra/nb_demi_j;	
			}
			$(".ca-intra-unit").val(ca_intra_unit);
		}
		
	}
					
	

	