<?php

// UPLOAD ET ENREGISTREMENT DES LOGO D'UN CLIENT

	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	
	include('modeles/mod_upload.php');

	$client=0;
	if(!empty($_POST["client"])){
		$client=intval($_POST["client"]);
	}
	
	if($client>0)
	{
		
		$dossier="clients/client" . $client;
		if (!file_exists('documents/' . $dossier)) {
			mkdir('documents/' . $dossier, 0777, true);
		}
		$dossier.="/";
		
		$extension=array("png","jpg","jpeg");
		
		$taille=512000;
		
		 if (!empty($_FILES['cli_logo_1']['name'])) {
			$path = $_FILES['cli_logo_1']['name'];
			$ext = pathinfo($path, PATHINFO_EXTENSION);			
			$logo1=  "logo_1_" . date('m-d-Y') . "." . $ext;
			
			$result1=upload("cli_logo_1",$dossier,"logo_1_" . date('m-d-Y'),$taille,$extension,1);
			if(!empty($result1)){
				unset($logo1);
				$erreur_txt="Erreur d'upload sur le logo 1 (" . $result1 . ")";
				
			}
			
		}
		 if (!empty($_FILES['cli_logo_2']['name'])){
			$path = $_FILES['cli_logo_2']['name'];
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			$logo2 =  "logo_2_" . date('m-d-Y') . "." . $ext;
			$result2=upload("cli_logo_2",$dossier,"logo_2_" . date('m-d-Y'),$taille,$extension,1);
			if(!empty($result2)){
				unset($logo2);
				$erreur_txt="Erreur d'upload sur le logo 2 (" . $result2 . ")";
			}
		}
		
		if(isset($logo1) OR isset($logo2)){
			
			$champ="";
			if(isset($logo1)){
				$champ="cli_logo_1 =:cli_logo_1";
			}
			if(isset($logo2)){
				if(!empty($champ)){				
					$champ.=",";
				}
				$champ.="cli_logo_2 =:cli_logo_2";
			}
			$sql="UPDATE clients SET " . $champ . " WHERE cli_id = :cli_id;";
			$req = $Conn->prepare($sql);
			$req->bindParam(':cli_id', $client);
			if(isset($logo1)){
				$req->bindParam(':cli_logo_1', $logo1);
			}
			if(isset($logo2)){
				$req->bindParam(':cli_logo_2', $logo2);
			}
			try {
				$req->execute();
			}catch (Exception $e) {
				$erreur_txt=$e->getMessage();
				echo($erreur_txt);
				die();
			}
		}	
	}else{
		echo("Erreur paramètre!");
		die();
	}
	$url="client_voir.php?client=" . $client . "&onglet=8";
	if(isset($erreur_txt)){
		$url.="&erreur=" . $erreur_txt;
	}
	Header("Location:" . $url);

