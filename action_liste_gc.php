 <?php

	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");
	include_once("includes/connexion_fct.php");
	include_once("modeles/mod_get_groupe_filiales.php");

	// LISTE DES Actions

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	if(!empty($_POST)){

		// parametre utiliser dans la requete
		$sql_param=array();

		// parametre utiliser pour l'affichage et le réaffichage
		$aff_param=array();

		$mil="";
		$periode_lib="";

		$opt_recherche=0;
		if(!empty($_POST["option_recherche"])){
			$opt_recherche=intval($_POST["option_recherche"]);
		}
		$aff_param["opt_recherche"]=$opt_recherche;

		$act_societe=0;
		if(!empty($_POST["act_societe"])){
			$act_societe=intval($_POST["act_societe"]);
		}
		$aff_param["act_societe"]=$act_societe;

		$sql_action="SELECT act_id,act_adr_cp,act_adr_ville,act_date_deb";
		$sql_action.=",acl_non_fac_gfc, acl_id,acl_pro_inter,acl_creation_date,acl_ca,acl_pro_reference,acl_ca_unit,acl_confirme,acl_facturation,acl_ca_gfc";
		$sql_action.=",cli_code,cli_nom";
		$sql_action.=",com_label_1";
		$sql_action.=",COUNT(acd_date) AS nb_demi_j";
		$sql_action.=",int_label_1";
		$sql_action.=" FROM Actions INNER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)
		LEFT OUTER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND cli_agence=Actions.act_agence)
		LEFT OUTER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
		LEFT OUTER JOIN Intervenants ON (Actions.act_intervenant=Intervenants.int_id)
		LEFT OUTER JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)";

		$act_id=0;
		if(!empty($_POST["act_id"])){
			$act_id=intval($_POST["act_id"]);
		}
		if(!empty($act_id)){
			$mil.=" AND act_id=:act_id";
			$sql_param["act_id"]=$act_id;
		}
		$acl_id=0;
		if(!empty($_POST["acl_id"])){
			$acl_id=intval($_POST["acl_id"]);
		}
		if(!empty($acl_id)){
			$mil.=" AND acl_id=:acl_id";
			$sql_param["acl_id"]=$acl_id;
		}

		if(empty($mil)){

			if(!empty($_POST["act_date_deb"])){
				$DT_periode_deb=date_create_from_format('d/m/Y',$_POST["act_date_deb"]);
				if(!is_bool($DT_periode_deb)){
					$periode_deb=$DT_periode_deb->format("Y-m-d");
					$mil.=" AND act_date_deb>=:date_deb";
					$sql_param["date_deb"]=$periode_deb;
				}
			}
			if(!empty($_POST["act_date_fin"])){
				$DT_periode_fin=date_create_from_format('d/m/Y',$_POST["act_date_fin"]);
				if(!is_bool($DT_periode_fin)){
					$periode_fin=$DT_periode_fin->format("Y-m-d");
					$mil.=" AND act_date_deb<=:date_fin";
					$sql_param["date_fin"]=$periode_fin;
				}
			}

			if(!empty($sql_param["date_deb"]) AND empty($sql_param["date_fin"])){
				$aff_param["periode_lib"]="Actions à partir du " . $DT_periode_deb->format("d/m/Y");
			}elseif(empty($sql_param["date_deb"]) AND !empty($sql_param["date_fin"])){
				$aff_param["periode_lib"]="Actions jusqu'au " . $DT_periode_fin->format("d/m/Y");
			}elseif(!empty($sql_param["date_deb"]) AND !empty($sql_param["date_fin"])){
				$aff_param["periode_lib"]="Actions entre le " . $DT_periode_deb->format("d/m/Y") . " et le " . $DT_periode_fin->format("d/m/Y");
			}else{
				$aff_param["periode_lib"]="";
			}


		
			if(!empty($_POST["client"])){
				
				if(!empty($_POST["cli_filiale_de"])){
					
					// client et filiale
					
					// info sur le client nécéssaire à la construction de la requete
					
					$sql_get_cli="SELECT cli_id,cli_filiale_de FROM Clients WHERE cli_id=:client;";
					$req_get_cli=$ConnSoc->prepare($sql_get_cli);
					$req_get_cli->bindParam(":client",$_POST["client"]);
					$req_get_cli->execute();
					$d_client=$req_get_cli->fetch();
					if(!empty($d_client)){
						
						if(empty($d_client["cli_filiale_de"])){
							// u à selectionné une filiale
							$mil.=" AND (cli_id=:client OR cli_filiale_de=:client)";
							$sql_param["client"]=$_POST["client"];
							
						}else{
							
							// u a selectionné une filiale
							
							$liste_fil="";
							$filiales=get_groupe_filiales($_POST["client"]);
							if(is_array($filiales)){
								$tab_fil=array_column ($filiales,"cli_id");
								$liste_fil=implode($tab_fil,",");
							}
							if(!empty($liste_fil)){
								$mil.=" AND acl_client IN (" . $liste_fil . ")";
								//$sql_param["client"]=$liste_fil;
							}else{
								$mil.=" AND acl_client=:client";
								$sql_param["client"]=$_POST["client"];
							}
						}						
					}					
				}else{
					
					$mil.=" AND acl_client=:client";
					$sql_param["client"]=$_POST["client"];
				}
			}
			
			if(!empty($_POST["cli_code"])){
				$mil.=" AND cli_code LIKE :cli_code";
				$sql_param["cli_code"]=$_POST["cli_code"] . "%";
			}
			
			
			
			

			/*if(!empty($_POST["commercial"])){
				$mil.=" AND acl_commercial=:commercial";
				$sql_param["commercial"]=$_POST["commercial"];
			}
			if(!empty($_POST["intervenant"])){
				$mil.=" AND act_intervenant=:intervenant";
				$sql_param["intervenant"]=$_POST["intervenant"];
			}*/
			if(!empty($_POST["type"])){
				$mil.=" AND act_pro_type=:type";
				$sql_param["type"]=$_POST["type"];
			}
			if(!empty($_POST["famille"])){
				$mil.=" AND act_pro_famille=:famille";
				$sql_param["famille"]=$_POST["famille"];
			}
			if(!empty($_POST["sous_famille"])){
				$mil.=" AND act_pro_sous_famille=:sous_famille";
				$sql_param["sous_famille"]=$_POST["sous_famille"];
			}
			if(!empty($_POST["sous_sous_famille"])){
				$mil.=" AND act_pro_sous_sous_famille=:sous_sous_famille";
				$sql_param["sous_sous_famille"]=$_POST["sous_sous_famille"];
			}
			if(!empty($_POST["cp"])){
				$mil.=" AND act_adr_cp LIKE :cp";
				$sql_param["cp"]=$_POST["cp"] . "%";
			}
			if(!empty($_POST["ville"])){
				$mil.=" AND act_adr_ville=:ville";
				$sql_param["ville"]=$_POST["ville"];
			}
		}

		if(!empty($_POST["act_agence"])){
			$mil.= " AND act_agence = " . intval($_POST["act_agence"]);
		}

		$mil.=" AND cli_categorie=2";

		$aff_param["search_lib"]="";
		if($opt_recherche==1){
			$mil.=" AND (act_archive=1 OR acl_archive=1)";
			$aff_param["search_lib"]="Actions archivées";
		}else{
			$mil.=" AND act_archive=0";
			switch($opt_recherche){
				case 2:
					$mil.=" AND acl_confirme=0";
					$aff_param["search_lib"]="actions non-confirmées";
					break;
				case 4:
					$mil_soc.=" AND (NOT act_alerte_ok AND act_alerte_date<=NOW())";
					$aff_param["search_lib"]="alertes actives";
					break;
				case 5:
					$mil.=" AND acl_ca_nc";
					$aff_param["search_lib"]=" actions sans tarif";
					break;
				case 6:
					$mil.=" AND acl_non_fac_gfc=1";
					$aff_param["search_lib"]="actions non facturables";
					break;
			}
		}

		if(!empty($mil)){
			$sql_action.=" WHERE " . substr($mil, 5);
		}
		$sql_action.=" GROUP BY act_id,act_adr_ville,act_date_deb";
		$sql_action.=",acl_id,acl_pro_inter,acl_creation_date,acl_ca,acl_pro_reference";
		$sql_action.=",cli_code,cli_nom";
		$sql_action.=",com_label_1";
		$sql_action.=",int_label_1";
		$sql_action.=" ORDER BY act_date_deb,act_intervenant,act_id,acl_id";

		$_SESSION["sql"]=$sql_action;
		$_SESSION["sql_param"]=$sql_param;
		$_SESSION["aff_param"]=$aff_param;

	}else{
		$sql_action=$_SESSION["sql"];
		$sql_param=$_SESSION["sql_param"];
		$aff_param=$_SESSION["aff_param"];
	}
	// DONNEE POUR EXECUTION DE LA REQUETE

	$sql="SELECT soc_id,soc_nom FROM Societes";
	if(!empty($aff_param["act_societe"])){
		$sql.=" WHERE soc_id=" . $aff_param["act_societe"];
	}
	$sql.=" ORDER BY soc_nom;";
	$req=$Conn->query($sql);
	$d_societes=$req->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($d_societes)){

		foreach($d_societes as $num_soc => $soc){

			$d_societes[$num_soc]["actions"]=array();

			$ConnFct=connexion_fct($soc["soc_id"]);

			if(!empty($sql_action)){
				$req=$ConnFct->prepare($sql_action);
				foreach($sql_param as $k => $v){
					$req->bindValue($k,$v);
				}
				$req->execute();
			}else{
				$req=$ConnFct->query($sql_action);
			}
			$d_societes[$num_soc]["actions"]=$req->fetchAll(PDO::FETCH_ASSOC);

		}
	}

	// retour

	$_SESSION["retourFacture"]="action_liste_gc.php";
	$_SESSION["retour_action"]="action_liste_gc.php";

	// GESTION DU MENU
	if(isset($_GET["menu_actif"])){
		$_SESSION["menu_actif"]=explode("-",$_GET["menu_actif"]);
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion - Recherche client</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

		<!-- PLUGIN -->
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<!-- PERSO Si2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

	   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="action_liste.php">
			<div id="main">
    <?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">


					<section id="content" class="animated">
				<?php	if(!empty($aff_param["periode_lib"])){
							echo("<h1>" . $aff_param["periode_lib"] . "</h1>");
						}

						foreach($d_societes as $k => $soc){

							if(!empty($soc["actions"])){
								if(!empty($aff_param["search_lib"])){
									echo("<h2>");
										echo(count($soc["actions"]) . " " . $aff_param["search_lib"]);
									echo("</h2>");
								}	?>
								<h1><?=$soc["soc_nom"]?></h1>
								<div class="table-responsive" >
									<table class="table table-striped table-hover tabAction"  data-tri_col="7" data-tri_sens="asc">
										<thead id="tabRappelHead" >
											<tr class="dark" >
												<th>ID</th>
												<th>Code client</th>
												<th>Nom client</th>
												<th>Commercial</th>
												<th>Produits</th>
												<th>Lieu</th>
												<th>Intervenant</th>
												<th>Date de début</th>
												<th>Date de création</th>
												<th>Nb 1/2 j</th>
												<th>CA</th>
												<th>CA projet</th>
												<th class="no-sort">&nbsp;</th>
											</tr>
										</thead>
										<tbody id="tabRappelBody" >
						<?php				$total_ca=0;
											$total_ca_projet=0;

											foreach($soc["actions"] as $a){
												$act_date_deb="";
												if(!empty($a["act_date_deb"])){
													$date_deb=new DateTime($a["act_date_deb"]);
													$act_date_deb=$date_deb->format("d/m/Y");
												}
												$acl_creation_date="";
												if(!empty($a["acl_creation_date"])){
													$creation_date=new DateTime($a["acl_creation_date"]);
													$acl_creation_date=$creation_date->format("d/m/Y");
												}

												$ca=0;
												$ca_projet=0;
												// FG CA INTRA OU INTER ca ne chaneg rien acl_ca et acl_ca_gfc totalise
												//if($a["acl_pro_inter"]==0){
													// INTRA
													if($a["acl_confirme"]){
														if($a["acl_facturation"]==1){
															$ca=$a["acl_ca_gfc"];
														}else{
															$ca=$a["acl_ca"];
														}
													}else{
														if($a["acl_facturation"]==1){
															$ca_projet=$a["acl_ca_gfc"];
														}else{
															$ca_projet=$a["acl_ca"];
														}
													}
												//}else{
													// INTER
													//$ca=$a["acl_ca"];
													//$ca_projet=$a["acl_ca_unit"]*$a["acl_for_nb_sta_resa"];
											//	}
												$total_ca=$total_ca+$ca;
												$total_ca_projet=$total_ca_projet+$ca_projet;
												?>
												<tr>
													<td><?=$a["act_id"] . "-" . $a["acl_id"]?></td>
													<td><?=$a["cli_code"]?></td>
													<td><?=$a["cli_nom"]?></td>
													<td><?=$a["com_label_1"]?></td>
													<td><?=$a["acl_pro_reference"]?></td>
													<td><?=$a["act_adr_cp"] . " " . $a["act_adr_ville"]?></td>
													<td><?=$a["int_label_1"]?></td>
													<td data-order="<?= $a['act_date_deb'] ?>"><?=$act_date_deb?></td>
													<td data-order="<?= $a['acl_creation_date'] ?>"><?=$acl_creation_date?></td>
													<td><?=$a["nb_demi_j"]?></td>
													<td  data-order="<?= $ca ?>" class="text-right text-success" >
												<?php	if(!empty($ca)){
															echo($ca . "&nbsp;€");
														} ?>
                                                        <?php		if(!empty($a["acl_non_fac_gfc"])){ ?>
    														<span class="badge badge-danger">
                                                                NF
                                                            </span>
    													<?php } ?>
													</td>
													<td  data-order="<?= $ca_projet ?>" class="text-right text-warning" >
											<?php		if(!empty($ca_projet)){
															echo($ca_projet . "&nbsp;€");
														} ?>
													</td>
													<td class="text-center" >
														<a href="action_voir.php?action=<?=$a["act_id"]?>&action_client=<?=$a["acl_id"]?>&societ=<?=$soc["soc_id"]?>" class="btn btn-info btn-sm" >
															<i class="fa fa-eye" ></i>
														</a>
													</td>
												</tr>
						<?php				} ?>

										</tbody>
										<tfoot>
											<tr>
												<th colspan="10" class="text-right" >Total :</th>
												<td class="text-right text-success" >
											<?php	if(!empty($total_ca)){
														echo("<b>" . number_format($total_ca, 2, '.', ' ') . "&nbsp;€</b>");
													} ?>
												</td>
												<td class="text-right text-warning" >
										<?php		if(!empty($total_ca_projet)){
														echo("<b>" . number_format($total_ca_projet, 2, '.', ' ') . "&nbsp;€</b>");
													} ?>
												</td>
												<td>&nbsp;</td>
											</tr>
										</tfoot>
									</table>
								</div>
					<?php	}
						} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="action_tri_gc.php" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i> Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right">

					</div>
				</div>
			</footer>
		</form>

<?php
		include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="/vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="/vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css"></script>
		<script type="text/javascript" src="/vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script type="text/javascript" src="assets/js/custom.js"></script>


		<script type="text/javascript">

			jQuery(document).ready(function (){

                tri_col=$(".tabAction").data("tri_col");
            	tri_sens=$(".tabAction").data("tri_sens");
                // initilisation plugin datatables
                $('.tabAction').DataTable({
                    "language": {
                      "url": "vendor/plugins/DataTables/media/js/French.json"
                    },
                	"order":[tri_col,tri_sens],
                    "paging": false,
                    "searching": false,
                    "info": false,
                    "columnDefs": [ {
                          "targets": 'no-sort',
                          "orderable": false,
                  	}]
                });
				// ----- FIN DOC READY -----
			});


		</script>
	</body>
</html>
