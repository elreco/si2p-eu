<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_erreur.php');

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link href="assets/admin-tools/admin-forms/css/admin-forms-orion.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			.couleur-palette{
				width:15px;
				height:15px;
			}
		</style>
		
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php	
				include "includes/header_def.inc.php";
			?>	
				
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
							
				<section id="content" class="animated fadeIn">				
					<div class="row" >
						<div class="col-md-12" >
							<div class="table-responsive" >
								<table class="table table-hover table-striped" >
									<thead>
										<tr class="dark" >
											<th>Libelle</th>
											<th>Couleur</th>											
											<th>&nbsp;</th>
										</tr>															
									</thead>
									<tbody>
								<?php
										$req = $Conn->query("SELECT pca_id,pca_libelle,pca_couleur FROM Plannings_Cases WHERE pca_type=0 ORDER BY pca_libelle");
										$case = $req->fetchAll();
										if(!empty($case)){										
											foreach($case as $c){  ?>
												<tr>
													<td><?=$c["pca_libelle"]?></td>
													<td>
														<div style="background-color:<?=$c["pca_couleur"]?>;" class="couleur-palette" >&nbsp;</div>
													</td>
													<td class="text-center" >
														<span data-toggle="modal" data-target="#formEdit" >
															<a href="#" class="btn btn-warning btn-xs open-formEdit" data-id="<?=$c["pca_id"]?>" data-nom="<?=$c["pca_libelle"]?>" data-couleur="<?=$c["pca_couleur"]?>" data-toggle="tooltip" data-placement="bottom" title="Modifier" >
																<i class="fa fa-pencil"></i>
															</a>
														</span>
													</td>									
												</tr>	
								<?php		
											};
										}; ?>																			
									</tbody>
								</table>
							</div>
						</div>
					</div>
	
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >
				</div>
				<div class="col-xs-3 footer-right" >
					<button class="btn btn-success btn-sm open-formEdit" data-toggle="modal" data-target="#formEdit" data-id="0" data-nom="" data-couleur="">
						<i class="fa fa-plus" ></i>
						Nouvelle Case
					</button>
				</div>
			</div>
		</footer>
									
		<div id="formEdit" class="modal fade" role="dialog">
			
			
			<div class="modal-dialog admin-form theme-primary">
				<div class="modal-content">
				
					<form method="post" action="param_case_enr.php" enctype="multipart/form-data" >
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title" >Nouvelle case</h4>
						</div>
						<div class="modal-body">
						
							<div>
								<input type="hidden" name="pca_id" id="pca_id" value="0" >
							</div>
							
							<div class="row" >
								<div class="col-md-6" >						
									<div class="form-group">
										<label for="pca_libelle" class="sr-only" >Nom</label>
										<div>
											<input type="text" class="form-control" name="pca_libelle" id="pca_libelle" placeholder="Nom" required >
										</div>
									</div>
								</div>
								<div class="col-md-6" >						
									<div class="form-group">
										<div class="section">
										<label class="field sfcolor">
										  <input type="text" name="pca_couleur" id="colorpicker" class="gui-input" placeholder="Couleur">
										</label>
									  </div>
									</div>
								</div>															
							</div>														
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
		include "includes/footer_script.inc.php"; ?>	
		<script src="assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {

				var cPicker2 = $("#colorpicker");

				var cContainer2 = cPicker2.parents('.sfcolor').parent();

				$(cContainer2).addClass('posr');

				$("#colorpicker").spectrum({
					color: bgPrimary,
					appendTo: cContainer2,
					containerClassName: 'sp-left',
					showInput: true,
					showPalette: false,
					preferredFormat: "hex",
					cancelText: "Fermer",         //Texte pour le bouton "cancel"
					chooseText: "Sélectionner",  					
				});

				$("#colorpicker").show();
					

				$(document).on("click", ".open-formEdit", function () {
					
					var pca_id = $(this).data('id');
					var pca_libelle = $(this).data('nom');
					var pca_couleur = $(this).data('couleur');
					$(".modal-body #pca_id").val( pca_id );
					$(".modal-body #pca_libelle").val( pca_libelle );	
					
					$(".modal-body #colorpicker").val( pca_couleur );	
					$("#colorpicker").spectrum("set", pca_couleur);	
					
					if(pca_id==0){
						$("#formEdit .modal-title").html('Nouvelle case');					
					}else{
						$("#formEdit .modal-title").html('Edition d\'une case');
					};
				});		
			});
			(jQuery);
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
