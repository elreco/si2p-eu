<?php
////////////////// MENU ACTIF HEADER ///////////////////

////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
if (isset($_GET['menu_retour'])) {
	unset($_SESSION['retour']);
}

///////////////////// TRAITEMENTS SERVEUR ////////////////////

////////////////////FIN TRAITEMENTS SERVEUR /////////////////

///////////////////// TRAITEMENTS PHP ///////////////////////
// categories ndf
$req = $Conn->prepare("SELECT * FROM baremes_kilometres");
$req->execute();
$baremes_kilometres = $req->fetchAll();

$req = $Conn->prepare("SELECT * FROM baremes_kilometres_lignes
LEFT JOIN baremes_kilometres ON (baremes_kilometres.bki_id = baremes_kilometres_lignes.bkl_bareme)");
$req->execute();
$baremes_kilometres_lignes = $req->fetchAll();
// fin categories ndf
////////////////////FIN TRAITEMENTS PHP//////////////////////
?>
<!DOCTYPE html>
<html>

<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Notes de frais</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
	<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


	<!-- Favicon -->
	<link rel="icon" type="image/png" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<style type="text/css">
	.panel-tabs>li>a {
		color: #AAA;
		font-size: 14px;
		letter-spacing: 0.2px;
		line-height: 30px;
		/*padding: 9px 20px 11px;*/
		border-radius: 0;
		border-left: 1px solid transparent;
		border-right: 1px solid transparent;
	}

	.nav2>li>a {
		position: relative;
		display: block;
		/* padding: 10px 15px; */
	}
</style>

<body class="sb-top sb-top-sm">


	<!-- Start: Main -->
	<div id="main">

		<?php include "includes/header_def.inc.php"; ?>
		<!-- Start: Content-Wrapper -->
		<section id="content_wrapper">

			<section id="content" class="animated fadeIn pr20 pl20 admin-forms">
				<h1>Liste des barèmes</h1>
				<div class="row">

					<div class="col-md-8">

						<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
							<thead>
								<tr class="dark">
									<th>Barème</th>
									<th>Tranche KM</th>
									<th>Puissance CV</th>
									<th>Coeff</th>
									<th>Bonus</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($baremes_kilometres_lignes as $bkl) { ?>
									<tr>
										<td>
											<?= convert_date_txt($bkl["bki_date_deb"]) ?> - <?= convert_date_txt($bkl["bki_date_fin"]) ?>
										</td>
										<td>
											KM Annuels :
										<?php	switch($bkl["bkl_tranche_km"]){
											case 1:
												echo "<strong>0 - 5 000 km</strong>";
												break;
											case 2:
												echo "<strong>5001 - 20 000 km</strong>";
												break;
											case 3:
												echo "<strong> > 20 000 km</strong>";
												break;
										} ?>
										</td>
										<td>
											<?= $bkl["bkl_puissance"] ?> CV
										</td>
										<td>
											<?= $bkl["bkl_coeff"] ?>
										</td>
										<td>
											<?= $bkl["bkl_bonus"] ?>
										</td>
										<td>
											<a href="ndf_bareme.php?bkl_puissance=<?= $bkl['bkl_puissance'] ?>&bki_id=<?= $bkl['bki_id'] ?>&bkl_tranche_km=<?= $bkl['bkl_tranche_km'] ?>"
											class="btn btn-sm btn-warning">
												<i class="fa fa-pencil"></i>
											</a>
											<a href="ndf_bareme_suppr.php?bkl_puissance=<?= $bkl['bkl_puissance'] ?>&bkl_bareme=<?= $bkl['bkl_bareme'] ?>&bkl_tranche_km=<?= $bkl['bkl_tranche_km'] ?>"
											class="btn btn-sm btn-danger">
												<i class="fa fa-times"></i>
											</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>

					<form action="ndf_bareme_enr.php" class="admin-form" method="POST">
						<div class="col-md-4">
							<div class="panel mb10">
								<div class="panel-heading panel-head-sm">
									<span class="panel-icon">
										<i class="fa fa-plus" aria-hidden="true"></i>
									</span>
									<span class="panel-title">Ajouter un barème</span>
								</div>
								<div class="panel-body p10">
									<div class="row">
										<div class="col-md-3 text-right" >
											<strong>KM annuel :</strong>
										</div>

										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="bkl_tranche_km" value="1" checked
													/>
												<span class="radio mr5"></span> &lt;= 5000 Km
											</label>
										</div>
										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="bkl_tranche_km" value="2">
												<span class="radio mr5"></span> &lt;= 20000 Km
											</label>
										</div>
										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="bkl_tranche_km" value="3"
													/>
												<span class="radio mr5"></span> &gt; 20000 Km
											</label>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-6" >
											<label for="uti_veh_cv" >Puissance du véhicule :</label>
											<div class="input-group">
												<input type="text" name="bkl_puissance" id="bkl_puissance" min="0" max="20" class="gui-input input-int" placeholder="Puissance"
												/>
												<span class="input-group-addon" >CV</span>
											</div>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-6">
											<label for="bkl_coeff" class="text-left" style="font-weight:bold;">Coeff :</label>
											<input type="text" name="bkl_coeff" id="bkl_coeff" class="form-control input-float" placeholder="Coeff">
										</div>
										<div class="col-md-6">
											<label for="bkl_bonus" class="text-left" style="font-weight:bold;">Bonus :</label>
											<input type="text" name="bkl_bonus" id="bkl_bonus" class="form-control input-float" placeholder="Bonus">
										</div>
									</div>
								</div>
								<div class="panel-footer text-right">
									<button type="submit" class="btn btn-sm btn-primary">Enregistrer</button>
								</div>

							</div>
						</div>
					</form>
				</div>
			</section>
		</section>
	</div>
	<!-- End: Main -->
	<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
		<div class="row">
			<div class="col-xs-4 footer-left pt7">
				<a href="parametre.php" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a>
			</div>
			<div class="col-xs-4 footer-middle pt7"></div>
			<div class="col-xs-4 footer-right pt7">


			</div>
		</div>
	</footer>



	<?php
	include "includes/footer_script.inc.php"; ?>

	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
	<!-- plugin pour les masques formulaires -->
	<script src="assets/js/custom.js"></script>

	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script src="vendor/plugins/summernote/summernote.min.js"></script>
	<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

	<script src="vendor/plugins/jquery.numberformat.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->


	<script>
		// DOCUMENT READY //a
		jQuery(document).ready(function() {

			// initilisation plugin datatables
			initDatatable()
			////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
		});

		///////////// FONCTIONS //////////////
		function initDatatable() {
			$('#table_id').DataTable({
				"language": {
					"url": "vendor/plugins/DataTables/media/js/French.json"
				},
				"paging": false,
				"searching": false,
				"info": false,
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});
		}
		//////////// FIN FONCTIONS //////////
	</script>

</body>

</html>