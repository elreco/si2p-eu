<?php
// ENREGISTREMENT D'UN DEVIS

include "includes/controle_acces.inc.php";
include_once("includes/connexion_soc.php");
include_once("includes/connexion.php");
include('modeles/mod_parametre.php');
include('modeles/mod_envoi_mail.php');
include('modeles/mod_add_notification.php');
// FIN DES CONTROLES

// ENREGISTREMENT
if(!empty($_GET['id'])){

	$eva_id = $_GET['id'];

	// TOUTES LES VALIDATIONS

	if(isset($_POST['retour'])){
		$eva_etat_valide = 2;
		$eva_assist_valide_date = NULL;
		// NOTIFICATION AU FORMATEUR
		$req = $Conn->prepare("SELECT eva_formateur, eva_societe, eva_agence, eva_id  FROM evacuations WHERE eva_id = " . $eva_id);
		$req->execute();
		$evacuation = $req->fetch();
		add_notifications("<i class='fa fa-file'></i>","Votre rapport d'évacuation : " . $eva_id . " a été réouvert, vous devez le corriger.","bg-warning","/evac_voir.php?id=" . $evacuation['eva_id'],"","",$evacuation['eva_formateur'],$evacuation['eva_societe'],$evacuation['eva_agence'],"");

		$_SESSION['message'][] = array(
			"titre" => "Succès",
			"type" => "success",
			"message" => "Le rapport est réouvert"
		);
	}else{
		$eva_etat_valide = 1;
		$eva_assist_valide_date = date("Y-m-d");
		$_SESSION['message'][] = array(
			"titre" => "Succès",
			"type" => "success",
			"message" => "Le rapport est maintenant terminé"
		);
	}
	if(isset($_GET['assist'])){

		$sql="UPDATE Evacuations  SET   eva_assist_valide_date = :eva_assist_valide_date, eva_assist_text = :eva_assist_text WHERE eva_id = " . $eva_id;
		$req=$Conn->prepare($sql);
		$req->bindValue(":eva_assist_text",$_POST['eva_assist_text']);
		$req->bindValue(":eva_assist_valide_date",$eva_assist_valide_date);
		$req->execute();
	}else{
		// YANNICK
		$sql="UPDATE Evacuations  SET   eva_tech_valide_date = :eva_assist_valide_date, eva_tech_text = :eva_tech_text WHERE eva_id = " . $eva_id;
		$req=$Conn->prepare($sql);
		$req->bindValue(":eva_tech_text",$_POST['eva_tech_text']);
		$req->bindValue(":eva_assist_valide_date",$eva_assist_valide_date);
		$req->execute();
	}


	$req = $Conn->prepare("SELECT eva_formateur, eva_tech_valide_date, eva_assist_valide_date,eva_assist FROM evacuations WHERE eva_id = " . $eva_id);
	$req->execute();
	$evacuation = $req->fetch();

	$req = $Conn->prepare("SELECT uti_eva_pas_valide_tech FROM utilisateurs WHERE uti_id = " . $evacuation['eva_formateur']);
	$req->execute();
	$utilisateur = $req->fetch();
	if(!isset($_POST['retour'])){
		if(empty($utilisateur['uti_eva_pas_valide_tech'])){
			// le rapport nécéssite une validation tech
			if(!empty($evacuation['eva_tech_valide_date']) && (!empty($evacuation['eva_assist_valide_date']) OR empty($evacuation['eva_assist'])) ){
				// si validation tech et validation assistante ou pas de validation assistante
				$eva_etat_valide = 3;
			}
		}else{
			if(!empty($evacuation['eva_assist_valide_date']) OR empty($evacuation['eva_assist']) ){
				$eva_etat_valide = 3;
			}
		}
	}

	$sql="UPDATE Evacuations  SET  eva_etat_valide = :eva_etat_valide WHERE eva_id = " . $eva_id;
	$req=$Conn->prepare($sql);
	$req->bindValue(":eva_etat_valide",$eva_etat_valide);
	$req->execute();
}

Header("Location: evac_voir.php?id=" . $_GET['id']);
die();
