<?php
$menu_actif = "2-2";
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");
	
	// LA PERSONNE LOGUE
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		if(!empty($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];	
		}
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		if(!empty($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];	
		}
	}
	
	$archive=0;
	if(!empty($_GET["archive"])){
		$archive=intval($_GET["archive"]);
	}
	
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link rel="shortcut icon" href="assets/img/favicon.png">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php
			include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn">
			
					<h1>
						Liste des véhicules
				<?php	if($archive==1){
							echo(" archivés");
						} ?>
					</h1>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="dark" >
									<th>&nbsp;</th>
									<th>Code</th>
									<th>Nom</th>
									<th>Immatriculation</th>
									<!--<th class="text-center" >Planning</th>-->
									<th class="text-center" >Actions</th>
								</tr>
							</thead>
							<tbody>
					<?php 		$sql="SELECT veh_id,veh_code,veh_libelle,veh_couleur,veh_immat,veh_societe,veh_agence FROM Vehicules WHERE (veh_societe=:acc_societe OR veh_societe=0)";
								if($archive==1){
									$sql.=" AND veh_archive";
								}else{
									$sql.=" AND NOT veh_archive";
								}
								if($acc_agence>0){
									$sql.=" AND (veh_agence=:acc_agence OR veh_agence=0)";
								}
								$sql.=" ORDER BY veh_code;";
								$req=$Conn->prepare($sql);
								$req->bindParam(":acc_societe",$acc_societe);
								if($acc_agence>0){
									$req->bindParam(":acc_agence",$acc_agence);
								}							
								$req->execute();
								$vehicules=$req->fetchAll();
								if(!empty($vehicules)){
									foreach($vehicules as $v){ ?>
										<tr>
											<td>
												<span class="badge" style="background-color:<?= $v['veh_couleur']?>" >&nbsp;</span>
											</td>
											<td><?= $v['veh_code'] ?></td>
											<td><?= $v['veh_libelle'] ?></td>
											<td><?= $v['veh_immat'] ?></td>
											<!--<td class="text-center" >
												<a href="vehicule_planning.php?vehicule=<?= $v['veh_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Planning de mise à disposition">
													<i class="fa fa-calendar" ></i>
												</a>
											</td>-->
											<td class="text-center" >
										<?php	if($v["veh_societe"]==$acc_societe AND ($acc_agence==0 OR $acc_agence==$v["veh_agence"]) ){?>
													<a href="vehicule.php?vehicule=<?= $v['veh_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier le vehicule">
														<span class="fa fa-pencil"></span>
													</a>
										<?php	} ?>
											</td>
										</tr>
					<?php			}
								} ?>							
							</tbody>
						</table>
					</div>
				</section>
			</section>
			<!-- End: Content -->
		</div>
		<!-- End: Main -->
				
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" ></div>
				<div class="col-xs-6 footer-middle text-center" >
		<?php		if($archive==1){ ?>
						<a href="vehicule_liste.php" class="btn btn-info btn-sm" >
							<i class="fa fa-eye" ></i> Voir les véhicules actifs
						</a>
			<?php	}else{ ?>
						<a href="vehicule_liste.php?archive=1" class="btn btn-primary btn-sm" >
							<i class="fa fa-archive" ></i> Voir les véhicules archivés
						</a>
			<?php	} ?>
				</div>
				<div class="col-xs-3 footer-right" >
					<a href="vehicule.php" class="btn btn-success btn-sm" role="button" >
						<span class="fa fa-plus"></span>
						Nouveau véhicule
					</a>
				</div>
			</div>
		</footer>

<?php
include "includes/footer_script.inc.php"; ?>	
</body>
</html>
