<?php
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");


	if (!$_SESSION["acces"]["acc_droits"][39]) {	
		header("location: deconnect.php");
		die();
	}
	
	// PARAMETRE
	$utilisateur=0;
	if(isset($_POST["utilisateur"])){
		if(!empty($_POST["utilisateur"])){
			$utilisateur=intval($_POST["utilisateur"]);
		}
	}
	$exercice=0;
	if(isset($_POST["exercice"])){
		if(!empty($_POST["exercice"])){
			$exercice=intval($_POST["exercice"]);
		}
	}

	$erreur_txt="";
	if($utilisateur==0 OR $exercice==0){
		$erreur_txt="Formulaire incomplet : enregistrement impossible!";
	}else{
		
		$urp_prime_produit=0;
		if(!empty($_POST["urp_prime_produit"])){
			$urp_prime_produit=intval($_POST["urp_prime_produit"]);
		}
		
		$urp_prime_mois=0;
		if(!empty($_POST["urp_prime_mois"])){
			$urp_prime_mois=intval($_POST["urp_prime_mois"]);
		}
		
		$urp_prime_trim=0;
		if(!empty($_POST["urp_prime_trim"])){
			$urp_prime_trim=intval($_POST["urp_prime_trim"]);
		}
		
		$urp_prime_annee=0;
		if(!empty($_POST["urp_prime_annee"])){
			$urp_prime_annee=intval($_POST["urp_prime_annee"]);
		}
		
		$urp_obj_prospect=0;
		if(!empty($_POST["urp_obj_prospect"])){
			$urp_obj_prospect=intval($_POST["urp_obj_prospect"]);
		}
		
		$urp_prime_prospect=0;
		if(!empty($_POST["urp_prime_prospect"])){
			$urp_prime_prospect=intval($_POST["urp_prime_prospect"]);
		}
		
		$urp_prct_interco=0;
		if(!empty($_POST["urp_prct_interco"])){
			$urp_prct_interco=floatval($_POST["urp_prct_interco"]);
		}
		
		$upr_rem_type=1;
		if(!empty($_POST["upr_rem_type"])){
			$upr_rem_type=intval($_POST["upr_rem_type"]);
		}
		
		$urp_prime_autre=0;
		if(!empty($_POST["urp_prime_autre"])){
			$urp_prime_autre=intval($_POST["urp_prime_autre"]);
		}
		
		$urp_prct_no_obj=0;
		if(!empty($_POST["urp_prct_no_obj"])){
			$urp_prct_no_obj=floatval($_POST["urp_prct_no_obj"]);
		}
	
		
		$sql="SELECT * FROM Utilisateurs_Rem_Param WHERE urp_exercice=" . $exercice . " AND urp_utilisateur=" . $utilisateur . " ;";
		$req=$Conn->query($sql);
		$d_existe=$req->fetch();
		if(!empty($d_existe)){
			$sql="UPDATE Utilisateurs_Rem_Param SET urp_prime_produit=:urp_prime_produit,urp_prime_mois=:urp_prime_mois
			,urp_prime_trim=:urp_prime_trim,urp_prime_annee=:urp_prime_annee,urp_obj_prospect=:urp_obj_prospect
			,urp_prime_prospect=:urp_prime_prospect, urp_prct_interco=:urp_prct_interco, upr_rem_type=:upr_rem_type,urp_prime_autre=:urp_prime_autre,urp_prct_no_obj=:urp_prct_no_obj";
			$sql.=" WHERE urp_utilisateur = :urp_utilisateur AND urp_exercice = :urp_exercice ";
			$req = $Conn->prepare($sql);
			$req->bindParam(":urp_utilisateur",$utilisateur);
			$req->bindParam(":urp_exercice",$exercice);
			$req->bindParam(":urp_prime_produit",$urp_prime_produit);
			$req->bindParam(":urp_prime_mois",$urp_prime_mois);
			$req->bindParam(":urp_prime_trim",$urp_prime_trim);
			$req->bindParam(":urp_prime_annee",$urp_prime_annee);
			$req->bindParam(":urp_obj_prospect",$urp_obj_prospect);
			$req->bindParam(":urp_prime_prospect",$urp_prime_prospect);
			$req->bindParam(":urp_prct_interco",$urp_prct_interco);
			$req->bindParam(":upr_rem_type",$upr_rem_type);
			$req->bindParam(":urp_prime_autre",$urp_prime_autre);	
			$req->bindParam(":urp_prct_no_obj",$urp_prct_no_obj);	
			try{
				$req->execute();
			}Catch(Exception $e){
				$erreur_txt="UPDATE : " . $e->getMessage();
			}
		}else{
			$sql="INSERT INTO Utilisateurs_Rem_Param (
			urp_utilisateur,urp_exercice,urp_prime_produit,urp_prime_mois,urp_prime_trim,urp_prime_annee,urp_obj_prospect,urp_prime_prospect,urp_prct_interco,upr_rem_type,urp_prime_autre,urp_prct_no_obj
			) VALUE (
			:urp_utilisateur,:urp_exercice,:urp_prime_produit,:urp_prime_mois,:urp_prime_trim,:urp_prime_annee,:urp_obj_prospect,:urp_prime_prospect,:urp_prct_interco,:upr_rem_type,:urp_prime_autre,:urp_prct_no_obj
			)";
			$req = $Conn->prepare($sql);
			$req->bindParam(":urp_utilisateur",$utilisateur);
			$req->bindParam(":urp_exercice",$exercice);
			$req->bindParam(":urp_prime_produit",$urp_prime_produit);
			$req->bindParam(":urp_prime_mois",$urp_prime_mois);
			$req->bindParam(":urp_prime_trim",$urp_prime_trim);
			$req->bindParam(":urp_prime_annee",$urp_prime_annee);
			$req->bindParam(":urp_obj_prospect",$urp_obj_prospect);
			$req->bindParam(":urp_prime_prospect",$urp_prime_prospect);	
			$req->bindParam(":urp_prct_interco",$urp_prct_interco);	
			$req->bindParam(":upr_rem_type",$upr_rem_type);
			$req->bindParam(":urp_prime_autre",$urp_prime_autre);		
			$req->bindParam(":urp_prct_no_obj",$urp_prct_no_obj);				
			$req->execute();
		}
					
	}
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Succès",
			"type" => "success",
			"message" => "Paramètres enregistrés"
		);
	}
	header('Location: utilisateur_rem.php?utilisateur=' . $utilisateur . "&exercice=" . $exercice);
	
?>