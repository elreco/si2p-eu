<?php

	include "includes/controle_acces.inc.php";
	include_once 'includes/connexion.php';

	// MISE A JOUR D'UN BUG

	$erreur_txt="";

	if($_SESSION['acces']['acc_profil']!=13 AND $_SESSION['acces']['acc_profil']!=9){
		$erreur_txt="Impossible d'enregistrer les modifications!";
	}

	if(empty($erreur_txt)){

		if(!empty($_POST)){


			// TRAITEMENT DU FORM

			$bug_id=0;
			if(!empty($_POST['bug_id'])){
				$bug_id=intval($_POST['bug_id']);
			}

			$bug_texte=trim($_POST["bug_texte"]);

			$bug_date=null;
			if(!empty($_POST["bug_date"])){
				$DT_bug_date=date_create_from_format('d/m/Y',$_POST["bug_date"]);
				if(!is_bool($DT_bug_date)){
					$bug_date=$DT_bug_date->format("Y-m-d");
				}else{
					$bug_date=date("Y-m-d");
				}
			}else{
				$bug_date=date("Y-m-d");
			}

			$bug_statut=0;
			if(!empty($_POST['bug_statut'])){
				$bug_statut=intval($_POST['bug_statut']);
			}

			$bug_statut_date=null;
			if(!empty($_POST["bug_statut_date"])){
				$DT_bug_statut_date=date_create_from_format('d/m/Y',$_POST["bug_statut_date"]);
				if(!is_bool($DT_bug_statut_date)){
					$bug_statut_date=$DT_bug_statut_date->format("Y-m-d");
				}else{
					$bug_statut_date=date("Y-m-d");
				}
			}else{
				$bug_statut_date=date("Y-m-d");
			}

			$bug_niveau=1;
			if(!empty($_POST['bug_niveau'])){
				$bug_niveau=intval($_POST['bug_niveau']);
			}

		}else{
			$erreur_txt="Formulaire incomplet!";
		}

		// SI MODIF

		if(!empty($bug_id)){

			$sql="SELECT * FROM Bugs WHERE bug_id=:bug;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":bug",$bug_id);
			$req->execute();
			$d_bug=$req->fetch();
			if(!empty($d_bug)){

				if($d_bug["bug_statut"]==$bug_statut){
					$bug_statut_date=$d_bug["bug_statut_date"];

				}

			}else{
				$erreur_txt="Impossible de charger les données associées au bug";
			}

		}
	}

	if(empty($erreur_txt)){

		if(empty($bug_id)){

			// CREATION

			$sql="INSERT INTO Bugs (bug_texte,bug_date,bug_statut,bug_statut_date,bug_niveau) VALUE(:bug_texte,:bug_date,:bug_statut,:bug_statut_date,:bug_niveau);";
			$req=$Conn->prepare($sql);
			$req->bindParam(":bug_texte",$bug_texte);
			$req->bindParam(":bug_date",$bug_date);
			$req->bindParam(":bug_statut",$bug_statut);
			$req->bindParam(":bug_statut_date",$bug_statut_date);
			$req->bindParam(":bug_niveau",$bug_niveau);
			try{
				$req->execute();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
			}

		}else{

			// MODIFICATION

			$sql="UPDATE Bugs SET bug_texte=:bug_texte,bug_statut=:bug_statut,bug_statut_date=:bug_statut_date,bug_niveau=:bug_niveau,bug_date=:bug_date
			WHERE bug_id=:bug;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":bug_texte",$_POST["bug_texte"]);
			$req->bindParam(":bug_statut",$bug_statut);
			$req->bindParam(":bug_statut_date",$bug_statut_date);
			$req->bindParam(":bug_niveau",$bug_niveau);
			$req->bindParam(":bug_date",$bug_date);
			$req->bindParam(":bug",$bug_id);
			try{
				$req->execute();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
			}
		}

	}

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);

	}else{
		$_SESSION['message'][] = array(
			"titre" => "Enregistrement terminé",
			"type" => "success",
			"message" => "Les modifications ont bien été prise en compte"
		);

	}
	header("location : bugs_liste.php");

?>
