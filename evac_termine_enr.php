<?php
// ENREGISTREMENT D'UN DEVIS

include "includes/controle_acces.inc.php";
include_once("includes/connexion_soc.php");
include_once("includes/connexion.php");
include('modeles/mod_parametre.php');
include('modeles/mod_envoi_mail.php');
include('modeles/mod_add_notification.php');
// FIN DES CONTROLES
$param = array(
	"sujet" => "",
	"body" => ""
);
// ENREGISTREMENT
if(!empty($_GET['id'])){
	$sql="SELECT * FROM Evacuations WHERE eva_id = " . $_GET['id'];
	$req=$Conn->prepare($sql);
	$req->execute();
	$evacuation = $req->fetch();

	$sql="SELECT uti_id, uti_eva_pas_valide_tech FROM Utilisateurs WHERE uti_id = " . $evacuation['eva_formateur'];
	$req=$Conn->prepare($sql);
	$req->execute();
	$utilisateur = $req->fetch();

	$sql="SELECT * FROM Evacuations_Alertes WHERE eal_societe = " . $evacuation['eva_societe'] . " AND eal_agence = " . $evacuation['eva_agence'];
	$req=$Conn->prepare($sql);
	$req->execute();
	$evacuation_alertes = $req->fetchAll();

	$template = "rapport";
	// foreach($evacuation_alertes as $ea){
	// 	$sql="INSERT INTO Evacuations_Valides (evv_evacuation, evv_utilisateur, evv_societe) VALUES (:evv_evacuation, :evv_utilisateur, :evv_societe)";
	// 	$req=$Conn->prepare($sql);
	// 	$req->bindValue("evv_evacuation",$evacuation['eva_id']);
	// 	$req->bindValue("evv_utilisateur",$ea['eal_ref_id']);
	// 	$req->bindValue("evv_societe",$evacuation['eva_societe']);
	// 	$req->execute();
	// }

	$sql="SELECT * FROM Evacuations_Chronologies WHERE ech_valeur != '' AND ech_evacuation = " . $_GET['id'];
	$req=$Conn->prepare($sql);
	$req->execute();
	$chronologies = count($req->fetchAll());

	$sql="SELECT * FROM Evacuations_Bilans WHERE ebi_evacuation = " . $_GET['id'] . " AND ebi_position != 0";
	$req=$Conn->prepare($sql);
	$req->execute();
	$remarques = count($req->fetchAll());

	$sql="SELECT * FROM Evacuations_Chronologies_Param";
	$req=$Conn->prepare($sql);
	$req->execute();
	$chronologies_param = count($req->fetchAll());

	$sql="SELECT * FROM evacuations_parametres";
	$req=$Conn->prepare($sql);
	$req->execute();
	$parametre = $req->fetch();
	$pourcentage = ($chronologies * 100) /$chronologies_param;


	if(!empty($parametre)){
        if($parametre['epa_theme_alert'] <= strlen($evacuation['eva_theme'])){
            // BON
            $eva_controle_theme = 1;
        }else{
            $eva_controle_theme = 0;


        }

        if($parametre['epa_conclusion_alert'] <= strlen($evacuation['eva_conclusion'])){
            // BON
            $eva_controle_conclusion = 1;
        }else{
            $eva_controle_conclusion = 0;
        }

        if($parametre['epa_remarque_alert'] <= $remarques){
            // BON
            $eva_controle_remarque = 1;
        }else{
            $eva_controle_remarque = 0;

        }

        if($parametre['epa_chrono_alert'] <= $pourcentage){
            // BON
            $eva_controle_chrono = 1;
        }else{
            $eva_controle_chrono = 0;
            //// ALERTE SUR LES PERSONNES ...////
            }
        }

	if(empty($eva_controle_chrono) && empty($eva_controle_conclusion) && empty($eva_controle_remarque) && empty($eva_controle_theme)){
		$eva_etat_controle = 3;
	}elseif(empty($eva_controle_chrono) OR empty($eva_controle_conclusion) OR empty($eva_controle_remarque) OR empty($eva_controle_theme)){
		$eva_etat_controle = 2;
	}else{
		$eva_etat_controle = 1;
	}

	$sql_edit="UPDATE Evacuations SET eva_date_termine = NOW(), eva_controle_chrono = " . $eva_controle_chrono . ", eva_controle_remarque = " . $eva_controle_remarque . ", eva_controle_conclusion = " . $eva_controle_conclusion . ",eva_controle_theme = " . $eva_controle_theme . ",eva_etat_controle = " . $eva_etat_controle . ", eva_etat_valide = 1 WHERE eva_id = " . $_GET['id'];
	$req_edit=$Conn->prepare($sql_edit);

	$req_edit->execute();

	// YANNICK
	$sql="SELECT uti_id, uti_prenom, uti_nom FROM Utilisateurs WHERE uti_id = 198";
	$req=$Conn->query($sql);
	$yannick =$req->fetch();
	if(!empty($yannick) && $utilisateur['uti_eva_pas_valide_tech'] == 0){
		add_notifications("<i class='fa fa-file'></i>","Vous devez valider le rapport d'évacuation : " . $evacuation['eva_id'],"bg-info","/evac_voir.php?id=" . $evacuation['eva_id'],"","",$yannick['uti_id'],$evacuation['eva_societe'],$evacuation['eva_agence'],"");
	}

	// CHERCHER LE CLIENT
	$sql="SELECT * FROM Clients WHERE cli_id=" . $evacuation['eva_client'];
	$req=$Conn->query($sql);
	$d_client=$req->fetch();

	// ASSISTANTE
	$sql="SELECT uti_id, uti_prenom, uti_nom FROM Utilisateurs WHERE uti_id = " . $evacuation['eva_assist'];
	$req=$Conn->query($sql);
	$assist_region =$req->fetch();

	if(!empty($assist_region) && $utilisateur['uti_eva_pas_valide_tech'] == 0){
		add_notifications("<i class='fa fa-file'></i>","Vous devez valider le rapport d'évacuation : " . $evacuation['eva_id'],"bg-info","/evac_voir.php?id=" . $evacuation['eva_id'],"","",$assist_region['uti_id'],$evacuation['eva_societe'],$evacuation['eva_agence'],"");
	}
}
$_SESSION['message'][] = array(
	"titre" => "Succès",
	"type" => "success",
	"message" => "Le rapport est maintenant terminé"
);
Header("Location: evac_voir.php?id=" . $_GET['id']);
die();
