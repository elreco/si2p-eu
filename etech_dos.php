<?php 
$tech_li = true;
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

require "modeles/mod_utilisateur.php";
require "modeles/mod_profil.php";
require "modeles/mod_societe.php";
require "modeles/mod_agence.php";
require "modeles/mod_document.php";
require "modeles/mod_erreur.php";

$societes = get_societes();
$profils = get_profil();
if(!empty($_GET['dos'])){
  $dossier = get_document($_GET['dos']);
  $dossier_url= $dossier['doc_url'] . $dossier['doc_nom_aff'] . "/";
  
}else{
  $dossier_url = "ETECH/";
}
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>SI2P - Orion - Etech</title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
    <div id="main">
      <?php
      include "includes/header_def.inc.php";
      ?>


      <!-- Start: Content-Wrapper -->
      <section id="content_wrapper" class="">
        <section id="content" class="animated fadeIn" >
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="admin-form theme-primary ">
                <div class="panel heading-border panel-primary">
                  <div class="panel-body bg-light">
                    <div class="text-center">

                      <div class="content-header">
                        <h2>Ajouter un <b class="text-primary">dossier</b></h2>
                        <h3>Dans: <?= $dossier_url; ?></h3>

                      </div>

                      <div class="col-md-10 col-md-offset-1">
                        <form action="etech_dos_enr.php" method="POST" id="admin-form">

                          <div class="row">

                            <input type="hidden" name="doc_url" value="<?= $dossier_url; ?>">
                            <?php if(isset($_GET['dos'])): ?>
                            <input type="hidden" name="dos" value="<?= $_GET['dos']; ?>">
                            
                          <?php endif; ?>
                          <div class="col-md-12">
                            <div class="section">
                              <div class="field prepend-icon">
                                <input type="text" name="doc_nom_aff" id="doc_nom_aff" class="gui-input" placeholder="Nom du dossier" required="">
                                <label for="soc_code" class="field-icon">
                                  <i class="fa fa-tag"></i>
                                </label>
                              </div>
                            </div>
                          </div>

                        </div>
                       

                          
                        
                         
                          
                        </section>
                        <!-- End: Content -->
                      </section>
                    </div>
                    <!-- End: Main -->
                    <footer id="content-footer" class="affix">
                      <div class="row">
                        <div class="col-xs-3 footer-left" >
                          <a <?php if(isset($_GET['dos'])): ?>href="etech.php?dos=<?= $_GET['dos'] ?>"<?php else: ?>href="etech.php"<?php endif; ?> class="btn btn-default btn-sm">
                            <i class="fa fa-long-arrow-left"></i>
                            Retour
                          </a>
                        </div>
                        <div class="col-xs-6 footer-middle" ></div>
                        <div class="col-xs-3 footer-right" >
                          <button type="submit" name="submit" class="btn btn-success btn-sm">
                            <i class='fa fa-save'></i> Enregistrer
                          </button>
                        </div>
                      </div>
                    </footer>
                  </form>
                  <?php
                  if(!empty($_GET["erreur"])){ ?>

                  <div id="modal-error" class="modal fade" role="dialog" data-show="true" >
                    <div class="modal-dialog">      
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">×</button>
                          <h4 class="modal-title" id="titreModal" >Erreur</h4>
                        </div>
                        <div class="modal-body">
                          <?=get_erreur_txt($_GET["erreur"]);?> 
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                            <i class="fa fa-close" ></i>
                            Fermer
                          </button>         
                        </div>
                      </div>      
                    </div>
                  </div>

                  <?php 
                }; 
               
				include "includes/footer_script.inc.php"; ?>	
                <!-- validation inputs -->

                <script src="vendor/plugins/mask/jquery.mask.js"></script>
                <!-- plugin pour les masques formulaires -->

                <script src="vendor/plugins/holder/holder.min.js"></script>
                <!-- pour mettre des images de tests -->
                <!-- Theme Javascript -->
               
                <script src="assets/js/custom.js"></script>

                <script type="text/javascript">



   // formulaire agence, agence / société


        /* @custom validation method (smartCaptcha)
        ------------------------------------------------------------------ */






      </script>
      <?php foreach($profils as $p): ?>
        <script>
          $('#dpr_obligatoire<?= $p['pro_id'] ?>').click(function()
            { if($(this).is(':checked')){
              $('#dpr<?= $p['pro_id'] ?>').prop('checked', true);     
            }
          });

        </script>
      <?php endforeach; ?>
      
      <?php foreach($societes as $s): ?>
        <script>
          $('#dso<?= $s['soc_id'] ?>').click(function()
            { if($(this).is(':checked')){

              <?php $agences = get_agence_societe($s['soc_id']); ?>
              <?php foreach($agences as $a): ?>

              $('#dso_agence<?= $a['age_id'] ?>').prop('checked', true);
              if($('#dso_agence<?= $a['age_id'] ?>').is(':checked')){
                $('#dso<?= $s['soc_id'] ?>').prop('checked', true);
              }else{
                $('#dso<?= $s['soc_id'] ?>').prop('checked', false);
              }
            <?php endforeach; ?>     

          }
        });

      </script>

    <?php endforeach; ?>
  </body>
  </html>
