<?php
////////////////// MENU ACTIF HEADER ///////////////////
$menu_actif = 4;
////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
////////////////// TRAITEMENTS SERVEUR ///////////////////
/////////////////  PERSONNE CONNECTEE ///////////////////
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}

$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
}
// sur la societe
$dev_societe=0;

$req=$Conn->query("SELECT soc_id, soc_nom FROM Societes WHERE soc_archive = 0;");
$d_societes=$req->fetchAll();

// sur la societe
/*$dev_agence=0;
if($acc_agence==0){
	$req=$Conn->query("SELECT age_id,age_nom FROM Agences,Societes WHERE age_societe=soc_id AND soc_agence AND soc_id=" . $acc_societe . ";");
	$d_agences=$req->fetchAll();
}else{
	$dev_agence=$acc_agence;
}*/

// SELECT TOUS LES THEMES
$req=$Conn->query("SELECT * FROM Evacuations_Themes");
$d_themes=$req->fetchAll();

// C'EST UNE EDITION
if(!empty($_GET['id'])){
	$req=$Conn->query("SELECT * FROM Evacuations WHERE eva_id=" . $_GET['id']);
	$evacuation=$req->fetch();
	if(!empty($evacuation['eva_adresse'])){
		$req = $Conn->prepare("SELECT * FROM adresses WHERE adr_id = " . $evacuation['eva_adresse']);
	    $req->execute();
	    $adresses = $req->fetchAll();
		if(!empty($evacuation['eva_contact'])){
			$req = $Conn->prepare("SELECT * FROM contacts WHERE con_id = " . $evacuation['eva_contact']);
		    $req->execute();
		    $contacts = $req->fetchAll();
		}
	}

}
?>
<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>SI2P - Orion - Rapports d'évacuation</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
	<form method="post" action="evac_enr.php" enctype="multipart/form-data">
		<!-- Start: Main -->
		<div id="main">
			<?php include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper" >


				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="text-left">
											<div class="content-header">
												<h2>
													<?php if(isset($_GET['id'])){?>
														Editer le rapport <b class='text-primary'><?= $_GET['id'] ?></b>
													<?php }else{ ?>
														Créer un <b class='text-primary'>rapport d'évacuation</b>
													<?php }?>
												</h2>
											</div>
											<div class="col-md-10 col-md-offset-1">
												<div class="row">
												    <?php if(isset($_GET['id'])){ ?>
												    	<input type="hidden" name="eva_id" value="<?= $_GET['id'] ?>">
												    <?php }?>
												    <!-- COLONNE DE GAUCHE -->
												    <?php if(isset($d_societes) && empty($_GET['id'])){ ?>
												    	<div class="row mt10" >
															<div class="col-md-2 text-right" >Client :<span style="color:red;font-weight:bold;">*</span></div>
															<div class="col-md-10" >
																<div class="select2-sm" >
																	<select id="client" name="client" data-cp_ville="1" required class="select2 select2-client"  data-archive="0" data-adr_type="1" data-adr_contact="" >
																		<option value='0' selected='selected' >Client ...</option>
																	</select>
																</div>
															</div>
														</div>
														<div class="row mt10 societe_div" >
                                                            <div class="col-md-2  text-right" >Société :<span style="color:red;font-weight:bold;">*</span></div>
                                                            <div class="col-md-10" >
                                                                <div class="select2-sm" >
                                                                    <select name="societe" id="societe" class="select2 select2-societe" data-com_archive="0" required >
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row mt10 agence_div">
                                                            <div class="col-md-2  text-right" >Agence :</div>
                                                                <div class="col-md-10" >
                                                                    <div class="select2-sm" >
                                                                        <select name="agence" id="agence" class="select2 select2-societe-agence" data-com_archive="0" >
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                        </div>


														<?php }?>

				                                    <div class="row mt15">
														<div class="col-md-12">
															<div class="section-divider">
																<span>Exercice</span>
															</div>
														</div>
													</div>
				                                    <div class="row mt15">
														<div class="col-md-6">
															<label for="date_deb">Exercice réalisé le :</label>
															<span class="field prepend-icon">
																<input type="text" class="gui-input datepicker date" id="eva_date_exer" name="eva_date_exer" required placeholder="Selectionner une date"

																<?php if(!empty($evacuation['eva_date_exer'])){ ?>
																	value="<?= convert_date_txt($evacuation['eva_date_exer']) ?>"
																<?php } ?>
																>
																<label class="field-icon">
																	<i class="fa fa-calendar-o"></i>
																</label>
															</span>
														</div>
														<div class="col-md-6">
															<ul>
																<li style="padding-bottom:10px">
																	<span style="font-weight: bold;">Par :  </span><?= $_SESSION['acces']['acc_prenom'] ?> <?= $_SESSION['acces']['acc_nom'] ?>

																</li>
																<li style="padding-bottom:10px">
																	<span style="font-weight: bold;">Affaire suivie par :</span>
																	<?php if(empty($_GET['id'])){ ?>
																		<span id="commercial"><i>Veuillez sélectionner un client</i></span>
																	<?php }else{ ?>
																		<?php if(!empty($evacuation['eva_com_nom'])){ ?>
																			<?= $evacuation['eva_com_nom'] ?>
																		<?php }else{ ?>
																			Pas de commercial
																		<?php } ?>
																	<?php } ?>
																</li>
																<li style="padding-bottom:10px">
																	<span style="font-weight: bold;">Réalisé pour un revendeur :</span>
																	<label class="option">
																		<input type="checkbox" name="eva_revendeur" value="1"
																		<?php if(!empty($evacuation['eva_revendeur'])){ ?>
																			checked
																		<?php } ?>
																		>
														  				<span class="checkbox"></span>
													  				</label>
																</li>
															</ul>
														</div>
													</div>

													<div class="row mt5" >
														<div class="col-md-12 section-divider" >
															<span>Adresse</span>
														</div>
													</div>
														<div class="row mt5" >
															<div class="col-md-12" >
																<div class="select2-sm" >
																	<select name="adresse" id="adresse" class="select2" >
																		<option value="0" >Selectionnez une adresse</option>
															<?php		if(!empty($adresses)){
																			foreach($adresses as $ad){
																				if(!empty($ad["adr_libelle"])){
																					$adr_libelle=$ad["adr_libelle"];
																				}else{
																					$adr_libelle=$ad["adr_nom"];
																				}
																				if($evacuation["eva_adresse"]==$ad["adr_id"]){
																					echo("<option value='" . $ad["adr_id"] . "' selected >" . $adr_libelle . "</option>");
																				}else{
																					echo("<option value='" . $ad["adr_id"] . "' >" . $adr_libelle . "</option>");
																				}
																			}
																		}?>
																	</select>
																</div>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12" >
																<input type="text" id="adr_nom" name="adr_nom" class="gui-input gui-input-sm nom champ-adresse" placeholder="Nom"
																<?php if(!empty($evacuation['eva_adr_nom'])){ ?>
																	value="<?=$evacuation['eva_adr_nom']  ?>"
																<?php } ?>
																/>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12" >
																<input type="text" id="adr_service" name="adr_service" class="gui-input gui-input-sm champ-adresse" placeholder="Service"
																<?php if(!empty($evacuation['eva_adr_service'])){ ?>
																	value="<?=$evacuation['eva_adr_service']  ?>"
																<?php } ?>
																/>
															</div>
														</div>
														<div class="row mt5" >

															<div class="col-md-12" >
																<input type="text" id="adr1" name="adr_ad1" class="gui-input gui-input-sm champ-adresse" placeholder="Adresse" required
																<?php if(!empty($evacuation['eva_adr_ad1'])){ ?>
																	value="<?=$evacuation['eva_adr_ad1']  ?>"
																<?php } ?>
																/>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12" >
																<input type="text" id="adr2" name="adr_ad2" class="gui-input gui-input-sm champ-adresse" placeholder="Complément 1"
																<?php if(!empty($evacuation['eva_adr_ad2'])){ ?>
																	value="<?=$evacuation['eva_adr_ad2']  ?>"
																<?php } ?>
																/>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12" >
																<input type="text" id="adr3" name="adr_ad3" class="gui-input gui-input-sm champ-adresse" placeholder="Complément 2"
																<?php if(!empty($evacuation['eva_adr_ad3'])){ ?>
																	value="<?=$evacuation['eva_adr_ad3']  ?>"
																<?php } ?>
																/>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-3" >
																<input type="text" required id="adr_cp" name="adr_cp" class="gui-input gui-input-sm code-postal champ-adresse" placeholder="CP"
																<?php if(!empty($evacuation['eva_adr_cp'])){ ?>
																	value="<?=$evacuation['eva_adr_cp']  ?>"
																<?php } ?>
																/>
															</div>
															<div class="col-md-9" >
																<input type="text" required id="adr_ville" name="adr_ville" class="gui-input gui-input-sm nom champ-adresse" placeholder="Ville"
																<?php if(!empty($evacuation['eva_adr_ville'])){ ?>
																	value="<?=$evacuation['eva_adr_ville']  ?>"
																<?php } ?>
																/>
															</div>
														</div>
														<div class="row mt15">
															<!-- <div class="col-md-6 text-center">
																Zone géographique
																<div class="section">
																	<label for="adr_geo1" class="mt15 option option-info">
																		<input type="radio" id="adr_geo1" class="adr-geo" name="adr_geo" value="1"
																		<?php if(empty($evacuation['eva_adr_geo']) OR (!empty($evacuation['eva_adr_geo']) && $evacuation['eva_adr_geo'] == 1)){ ?>
																			checked
																		<?php } ?>


																		/>
																		<span class="radio"></span> France
																	</label>
																	<label for="adr_geo2" class=" mt15 option option-info">
																		<input type="radio" id="adr_geo2" class="adr-geo" name="adr_geo" value="2"
																		<?php if(!empty($evacuation['eva_adr_geo']) && $evacuation['eva_adr_geo'] == 2){ ?>
																			checked
																		<?php } ?>
																		/>
																		<span class="radio"></span> UE
																	</label>
																	<label for="adr_geo3" class=" mt15 option option-info">
																		<input type="radio" id="adr_geo3" class="adr-geo" name="adr_geo" value="3"
																		<?php if(!empty($evacuation['eva_adr_geo']) && $evacuation['eva_adr_geo'] == 3){ ?>
																			checked
																		<?php } ?>
																		/>
																		<span class="radio"></span> Autre
																	</label>
																</div>
															</div> -->
															<!-- <div class="col-md-6 text-left">

																<div class="option-group field mb20" id="bloc_adr_add">
																	<label class="option option-dark">
																		<input type="checkbox" name="adr_add" value="1">
																		<span class="checkbox"></span>Enregistrer comme nouvelle adresse
																	</label>
																</div>
																<div class="option-group field" id="bloc_adr_maj" style="display:none;">
																	<label class="option option-dark">
																		<input type="checkbox" name="adr_maj" value="1">
																		<span class="checkbox"></span>Mettre à jour l'adresse
																	</label>
																</div>

															</div> -->
														</div>
														<div class="row mt5" >
															<div class="col-md-12 section-divider" >
																<span>Contact</span>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12" >
																<div class="select2-sm" >
																	<select name="contact" id="contact" class="select2 actu-contacts aff-contact" >
																		<option value="0" >Selectionnez un contact client</option>
															<?php		if(!empty($contacts)){
																			foreach($contacts as $co){
																				if($evacuation["eva_contact"]==$co["con_id"]){
																					echo("<option value='" . $co["con_id"] . "' selected >" . $co['con_prenom'] . " " . $co['con_nom'] . "</option>");
																				}else{
																					echo("<option value='" . $co["con_id"] . "' >" . $co['con_prenom'] . " " . $co['con_nom'] . "</option>");
																				}
																			}
																		}?>
																	</select>
																</div>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-6" >
																<input required type="text" id="con_nom" name="con_nom" class="gui-input gui-input-sm nom champ-contact" placeholder="Nom"
																<?php if(!empty($evacuation['eva_contact_nom'])){ ?>
																	value="<?=$evacuation['eva_contact_nom']  ?>"
																<?php } ?>
																/>
															</div>
															<div class="col-md-6" >
																<input required type="text" id="con_prenom" name="con_prenom"  class="gui-input gui-input-sm prenom champ-contact" placeholder="Prénom"
																<?php if(!empty($evacuation['eva_contact_prenom'])){ ?>
																	value="<?=$evacuation['eva_contact_prenom']  ?>"
																<?php } ?>
																/>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12 section-divider" >
																<span>Thème</span>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-2 mt5 text-right" >Sélectionnez un thème :</div>
															<div class="col-md-10" >
																<div class="select2-sm" >
																	<select id="eva_theme" name="eva_theme_id" required class="select2">																	<option value='0' selected='selected' >Sélectionnez un thème ...</option>
																		<?php foreach($d_themes as $d){

																			?>

																				<option value="<?= $d['eth_id'] ?>"
																					<?php if(!empty($evacuation['eva_theme_id']) && $evacuation['eva_theme_id'] == $d['eth_id']){ ?>
																					selected
																				<?php }?>
																					><?= $d['eth_theme'] ?></option>
																		<?php } ?>
																	</select>
																</div>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-2 mt5 text-right" >Détail :</div>
															<div class="col-md-10" >
																<textarea name="eva_theme" class="form-control" id="detail" placeholder="Détail" rows="3" cols="50"><?php if(!empty($evacuation['eva_theme'])){ ?><?=$evacuation['eva_theme']?><?php }; ?></textarea>
															</div>
														</div>



												</div>


											</div>

										</div>


									</div>
								</div>
							</div>
						</div>
					</div>


				</section>
			</section>

		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<?php if(!isset($_SESSION['retour'])){ ?>
				        <a href="evac_liste.php" class="btn btn-default btn-sm">
				          <i class="fa fa-long-arrow-left"></i>
				          Retour
				        </a>
				    <?php }else{ ?>
				    	<a href="<?= $_SESSION['retour'] ?>" class="btn btn-default btn-sm">
				          <i class="fa fa-long-arrow-left"></i>
				          Retour
				        </a>
				    <?php } ?>
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
					<button type="submit" id="submit" class="btn btn-success btn-sm">
						<i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php
include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->
<script src="assets/js/custom.js"></script>
<script src="vendor/plugins/holder/holder.min.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->


<script>

jQuery(document).ready(function($) {
	getCommercial($("#client").val(), $("#societe").val(), $("#agence").val());
	$(".select2-client").change(function() {
		$("#societe").find("option:gt(0)").remove();
		$("#agence").find("option:gt(0)").remove();
		if($(this).val() != 0){
			societe_agence($(this).val());
		}
	});
    if($("#societe").val()){
        // $(".agence_div").show();
    }


	// GET COMMERCIAL LORSQUE CLIENT CHANGE
	var tab_contact=new Array(3);
	tab_contact[0]=new Array(3);

	var tab_adresse=new Array();
	tab_adresse[0]=new Array(7);

	var client_categorie=0;

	get_categories_client($("#client").val());
	$("#client").change(function() {
		get_adresse($(this).val());
		get_categories_client($(this).val());
	});

	$("#eva_theme").change(function() {
		getTheme($(this).val());
	});



	$("#societe").change(function(){
		if($(this).val()>0){
			$("#text_societe").hide();
			// $(".agence_div").show();
		}else{
			$("#text_societe").show();
			//$("#client").prop("disabled",true);
			// $(".agence_div").hide();
		}

		if($("#societe").val() != 0){
			$.ajax({
				url: "ajax/ajax_client_agence.php",
				type: "POST",
				data: 'field=' + $(this).val() + "&client=" + $("#client").val(),
				dataType: 'json',
				success: function(data) {
					if (data.length == 0){

						$("#agence").find("option:gt(0)").remove();
						$("#agence").trigger("change");

					}else{
						$("#agence").find("option:gt(0)").remove();
						$("#agence").trigger("change");
						$.each(data, function(key, value) {
							$('#agence')
							.append($("<option></option>")
							.attr("value",value["age_id"])
							.text(value["age_code"]));
						});
					}
				},
				error: function (data){
					modal_alerte_user(data.responseText);
					$("#societe").find("option:gt(0)").remove();
					$("#agence").find("option:gt(0)").remove();
				}
			});
		}
	});
	// chagement d'adresse
	$("#adresse").change(function(){

		adresse=$(this).val();
		client=$("#client").val();
		if(adresse>0){
			get_adresse_lignes(adresse);
		}else{
			$(".champ-adresse").val("");
		}
		if(client>0){
			// si adresse on prend contact associé sinon tous les contacts
			get_contacts(1,client,adresse,0,actualiser_contacts,"","");
		}
	});

	// chagement d'adresse
	$("#contact").change(function(){
		contact=$(this).val();
		if(contact>0){


			get_contact_lignes(contact);

		}else{
			$(".champ-contact").val("");

		}
	});
	//getCommercial($("#client").val());
	// 1-2- DATEPICKER
	$(".datepicker").datepicker({
		defaultDate: "+1w",
		dateFormat: "dd/mm/yy",
		closeText: 'Fermer',
		prevText: 'Précédent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
		weekHeader: 'Sem.',
		numberOfMonths: 1,
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		beforeShow: function (input, inst) {
			var themeClass = $(this).parents('.admin-form').attr('class');
			var smartpikr = inst.dpDiv.parent();
			if (!smartpikr.hasClass(themeClass)) {
				inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
			}
		},
		onClose: function (selectedDate) {
			$("#datepicker-to").datepicker("option", "minDate", selectedDate);
		}
	});
});

//////////////// FONCTIONS /////////////////

function getCommercial(client, societe, agence){
	if(client != ""){
		$.ajax({
			url: 'ajax/ajax_get_commercial_client.php',
			type: 'POST',
			dataType: 'json',
			data: {client: client, societe: societe, agence:agence},
		})
		.done(function(data) {
			prenom = data.com_label_1;
			nom = data.com_label_2;
			if(prenom == undefined){
				prenom = " commercial";
				nom = "Pas de";
			}
			setCommercial(prenom, nom);
		})
		.fail(function(data) {

		})
		.always(function(data) {

		});

	}
}

function getTheme(theme){
	if(theme != ""){
		$.ajax({
			url: 'ajax/ajax_get_eth_theme.php',
			type: 'POST',
			dataType: 'json',
			data: {theme: theme},
		})
		.done(function(data) {
			detail = data.eth_libelle;
			setTheme(detail);
		})
		.fail(function(data) {

		})
		.always(function(data) {

		});

	}
}

function setCommercial(prenom, nom){
	$("#commercial").html(nom + " " + prenom);
}

function setTheme(detail){
	$("#detail").html(detail);
}

function get_adresse(client){
	$.ajax({
		url: "ajax/ajax_adresse_client.php",
		type: "POST",
		data: 'field=' + client,
		dataType: 'json',
		success: function(data) {
			if (data['0'] == undefined){

				$("#adresse").find("option:gt(0)").remove();
				$("#adresse").trigger("change");

			}else{
				$("#adresse").find("option:gt(0)").remove();
				$("#adresse").trigger("change");
				console.log(data);
				$.each(data, function(key, value) {
					$('#adresse')
					.append($("<option></option>")
					.attr("value",value["adr_id"])
					.text(value["adr_libelle"]));
				});
			}
		},
		error: function (data){
			modal_alerte_user(data.responseText);
		}
	});
}
function get_categories_client(client){
	$.ajax({
		url: "ajax/ajax_get_categories_client.php",
		type: "GET",
		data: 'client=' + client,
		dataType: 'json',
		success: function(data) {
			if(data['cli_categorie'] == 2 && data['cli_sous_categorie'] == 1){
				$(".societe_div").hide();
				$(".agence_div").hide();
				$('#societe').prop('required',false);
			}else{
				$(".societe_div").show();
				$(".agence_div").show();
				$('#societe').prop('required',true);
			}

		},
		error: function (data){
			modal_alerte_user(data.responseText);
		}
	});
}
function get_adresse_lignes(adresse){
	$.ajax({
		url: "ajax/ajax_adresse_client_lignes.php",
		type: "POST",
		data: 'field=' + adresse,
		dataType: 'json',
		success: function(data) {
				$("#adr_nom").val(data["adr_nom"]);
				$("#adr_service").val(data["adr_service"]);
				$("#adr1").val(data["adr_ad1"]);
				$("#adr2").val(data["adr_ad2"]);
				$("#adr3").val(data["adr_ad3"]);
				$("#adr_cp").val(data["adr_cp"]);
				$("#adr_ville").val(data["adr_ville"]);
				if(data["adr_geo"]>0){
					$("#adr_geo" + data["adr_geo"]).prop("checked",true);
				}

		},
		error: function (data){
			modal_alerte_user(data.responseText);
		}
	});
}
function get_contact_lignes(contact){
	$.ajax({
		url: "ajax/ajax_contact_client_lignes.php",
		type: "POST",
		data: 'field=' + contact,
		dataType: 'json',
		success: function(data) {
			$("#con_nom").val(data["con_nom"]);
			$("#con_prenom").val(data["con_prenom"]);
			$("#con_tel").val(data["con_tel"]);
			$("#con_portable").val(data["con_portable"]);
			$("#con_mail").val(data["con_mail"]);
		},
		error: function (data){
			modal_alerte_user(data.responseText);
		}
	});
}
function societe_agence(client){
	$('#societe').empty().trigger("change");
	$('#agence').empty().trigger("change");
	$.ajax({
		url: "ajax/ajax_client_societe.php",
		type: "POST",
		data: 'field=' + client,
		dataType: 'json',
		success: function(data) {

			if (data == undefined){

				$("#societe").find("option:gt(0)").remove();

			}else{
				$("#societe").find("option:gt(0)").remove();
				console.log(data);

				$.each(data, function(key, value) {
					if(value["soc_id"] != 4){
						$('#societe')
						.append($("<option></option>")
						.attr("value",value["soc_id"])
						.text(value["soc_code"]));
					}

				});
			}
			getCommercial($("#client").val(), $("#societe").val(), 0);
			$.ajax({
				url: "ajax/ajax_client_agence.php",
				type: "POST",
				data: 'field=' + $("#societe").val() + "&client=" + $("#client").val(),
				dataType: 'json',
				success: function(data) {
					if (data.length == 0){

						$("#agence").find("option:gt(0)").remove();
						$("#agence").trigger("change");
					}else{

						// $(".agence_div").show();
						$("#agence").find("option:gt(0)").remove();
						$("#agence").trigger("change");

						$.each(data, function(key, value) {
							$('#agence')
							.append($("<option></option>")
							.attr("value",value["age_id"])
							.text(value["age_code"]));
						});

					}
					getCommercial($("#client").val(), $("#societe").val(), $("#agence").val());

				},
				error: function (data){
					modal_alerte_user(data.responseText);
					$("#societe").find("option:gt(0)").remove();
					$("#agence").find("option:gt(0)").remove();
				}
			});
		},
		error: function (data){
			modal_alerte_user(data.responseText);
			$("#societe").find("option:gt(0)").remove();
			$("#agence").find("option:gt(0)").remove();
		}
	});
}
</script>

</body>
</html>
