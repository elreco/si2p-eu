<?php
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'includes/connexion_fct.php';

// LISTE DES VISITES PRISE EN COMPTE POUR UN UTILISATEUR
$erreur_txt="";

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];    
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
    $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];  
}

$utilisateur=0;
if(!empty($_GET["utilisateur"])){
	$utilisateur=intval($_GET["utilisateur"]);
}
$societe=0;
if(!empty($_GET["societe"])){
	$societe=intval($_GET["societe"]);
}
$agence=0;
if(!empty($_GET["agence"])){
	$agence=intval($_GET["agence"]);
}


if($utilisateur==0 AND $societe==0){
	$erreur_txt="Vous ne pouvez pas accéder à cette page!";
}

if(empty($erreur_txt)){
	
	$_SESSION['retourClient']="market_concours_action.php?utilisateur=" . $utilisateur . "&societe=" . $societe . "&agence=" . $agence;
	
	if(!empty($utilisateur)){
		
		$sql="SELECT uti_nom,uti_prenom,uti_societe FROM Utilisateurs WHERE uti_id=" . $utilisateur . ";";
		$req=$Conn->query($sql);
		$d_utilisateur=$req->fetch();
		if(empty($d_utilisateur)){
			$erreur_txt="Vous ne pouvez pas accéder à cette page!";
		}else{
			if($utilisateur==432){
				// Patricia L joue pour NN
				// pas nécéssaire pour Jean car il ne fait pas partie du concours individuel
				$societe=3;
			}else{
				$societe=$d_utilisateur["uti_societe"];
			}
		}
		
	}else{
		
		$sql="SELECT soc_nom,age_nom FROM Societes LEFT JOIN Agences ON (Societes.soc_id=Agences.age_societe AND age_id=" . $agence . ")
		WHERE soc_id=" . $societe . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();
		if(empty($d_societe)){
			$erreur_txt="Vous ne pouvez pas accéder à cette page!";
			echo($erreur_txt);
			die();
		}
		
	}
}

if(empty($erreur_txt)){	


	// pour la plannification 2020, il faut identifier les clients jamais facturé (facile avce bool cli_facture ne bse N) 
	// mais nous avons aussi besoin d'identifier les prospects qui sont facturés pour la première fois sur le T1 2020 pour figer la stat
	
	$clients_2020=array();
	
	
	$sql_clients_2020="SELECT Clients.cli_id,Clients.cli_first_facture,Clients.cli_first_facture_soc FROM Clients LEFT JOIN Clients AS Holdings ON (Clients.cli_filiale_de=Holdings.cli_id) 
	WHERE (Clients.cli_first_facture_date>='2020-01-01') 
	AND (ISNULL(Holdings.cli_id) OR ISNULL(Holdings.cli_first_facture_date) OR (Holdings.cli_first_facture_date>='2020-01-01'))
	AND NOT Clients.cli_hors_concours;";
	$req_clients_2020=$Conn->query($sql_clients_2020);
	$clients_2020=$req_clients_2020->fetchAll();
	if(!empty($clients_2020)){
		$tab_src=array_column ($clients_2020,"cli_id");
		$liste_clients_2020=implode($tab_src,",");
	}

	$ConnFct=connexion_fct($societe);
	
	/*var_dump($liste_fac);
	
	die();*/
	
	/* selection des actions prospects coonfirmé avant le 31/12/2019 et qui debute avant le 01/04/2020. */
	
	/* Dans la sous requete on remet les critères de tri pour limite les résultats 
	mais on est obligé de conserver les jointure classique pour obtenir act_id et acl_id incompatible avec GROUP BY de la sous _requete*/
	
	$sql_action="SELECT cli_code,cli_nom,cli_id
	,Actions_Clients.acl_pro_reference,Actions_Clients.acl_id
	,DATE_FORMAT(act_date_deb,'%d/%m/%Y') AS act_date_deb_fr,act_id
	,com_label_1,com_label_2
	FROM Actions INNER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)
	INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND Clients.cli_agence=Actions.act_agence)
	
	INNER JOIN ( 
		SELECT MIN(act_date_deb) AS min_date_deb,acl_client FROM Actions,Actions_Clients WHERE Actions.act_id=Actions_Clients.acl_action 
		AND act_date_deb<='2020-03-31' AND NOT act_archive
		AND acl_confirme AND acl_confirme_date<='2019-12-31' AND NOT acl_archive 
		GROUP BY acl_client 
	) AS groupe ON Actions.act_date_deb = groupe.min_date_deb
	
	
	LEFT JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)
	WHERE act_date_deb<='2020-03-31' AND act_date_deb>='2020-01-01' AND NOT act_archive
	AND acl_confirme AND acl_confirme_date<='2019-12-31' AND NOT acl_archive
	AND Com_type=1";
	if(!isset($liste_clients_2020)){
		$sql_action.=" AND NOT cli_facture";
	}else{
		$sql_action.=" AND (NOT cli_facture OR cli_id IN (" . $liste_clients_2020 . ") )";
	}
	if(!empty($utilisateur)){
		$sql_action.=" AND com_ref_1=" . $utilisateur;
	}elseif(!empty($agence)){
		$sql_action.=" AND act_agence=" . $agence;
	}
	if($societe==7){
		$sql_action.=" AND NOT com_id=174";
	}elseif($agence==3){
		$sql_action.=" AND NOT com_id=196";
	}elseif($societe==2){
		$sql_action.=" AND NOT com_id=114";
	}
	$sql_action.=" ORDER BY cli_code,act_date_deb;";
	$req_fac=$ConnFct->query($sql_action);
	$d_actions=$req_fac->fetchAll();
	
	$nb_com=0;
	if(empty($utilisateur)){ 
		switch ($societe) {
			case 2:
				$nb_com=3;
				break;
			case 3:
				if($agence==3){
					$nb_com=3;
				}else{
					$nb_com=3;
				}
				break;
			case 4:
				$nb_com=1;
				break;
			case 5:
				$nb_com=3;
				break;
			case 7:
				$nb_com=2;
				break;
			case 8:
				$nb_com=1;
				break;
		}
	}

}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="market_suspect_liste.php" id="formulaire" >
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
					
						<div class="content-header">
					<?php	if(!empty($utilisateur)){ ?>
								<h2>Points "plannification T1" pour <b class="text-primary"><?=$d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"]?></b></h2>
					<?php	}elseif(!empty($agence)){ ?>		
								<h2>Points "plannification T1" pour  <b class="text-primary"><?=$d_societe["soc_nom"] . " " . $d_societe["age_nom"]?></b></h2>
					<?php	}else{ ?>		
								<h2>Points "plannification T1" pour  <b class="text-primary"><?=$d_societe["soc_nom"]?></b></h2>
					<?php	} ?>
						</div>
						
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="panel">
									<div class="panel-heading panel-head-sm">Actions "prospect" sur le T1 2020</div>
									<div class="panel-body" >
								<?php	if(!empty($d_actions)){ ?>			
											<table class="table" >
												<thead>
													<tr>
														<th>Code</th>
														<th>Nom</th>
														<th>Action</th>
														<th>Produit</th>
														<th>Commercial</th>
														<th>Date d'action</th>
														<th>Base point</th>		
														<th>Coefficient commercial</th>																
														<th class="text-center" >Nb Point</th>															
													</tr>
												</thead>
												<tbody>
									<?php			$total=0;
													$client=null;
													foreach($d_actions as $act){ 
													
														if($client!=$act["cli_id"]){
															
															$client=$act["cli_id"];
															
															$pt=10;														
															if(empty($utilisateur)){ 
																$pt=$pt/$nb_com;
															};
															$total=$total+$pt; ?>
															<tr>
																<td><?=$act["cli_code"]?></td>
																<td><?=$act["cli_nom"]?></td>
																<td><?=$act["act_id"] . "-" . $act["acl_id"]?></td>
																<td><?=$act["acl_pro_reference"]?></td>															
																<td><?=$act["com_label_1"] . " " . $act["com_label_2"]?></td>
																<td><?=$act["act_date_deb_fr"]?></td>		
																<td class="text-right" >10 pt</td>		
																<td class="text-right" ><?=$nb_com?></td>																		
																<td class="text-right" ><?=number_format($pt,2,","," ")?> pt</td>																																								
															</tr>
									<?php				}
													} ?>
												</tbody>
												<tfoot>
													<tr>
														<th class="text-right" colspan="8" >Total :</th>
														<th class="text-right" ><?=number_format($total,2,","," ")?> pt</th>
													</tr>
												</tfoot>
											</table>
								<?php	}else{ ?>
											<p class="alert alert-warning" >Aucune action.</p>
								<?php	} ?>
									</div>
								</div>
							</div>
						</div>
						
					</section>
				</section>
			</div>		
			<!-- End: Main -->
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
				<?php	if(!empty($utilisateur)){ ?>
							<a href="market_concours.php" class="btn btn-default btn-sm">
								<span class="fa fa-arrow-left"></span>
								<span class="hidden-xs">Retour</span>
							</a>
				<?php	}else{ ?>
							<a href="market_concours.php?agence=1" class="btn btn-default btn-sm">
								<span class="fa fa-arrow-left"></span>
								<span class="hidden-xs">Retour</span>
							</a>
				<?php	} ?>			
					</div>
					<div class="col-xs-6 footer-middle text-center"></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
				
			});
		</script>
	</body>
</html>
