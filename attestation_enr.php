<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	
	
	// CONTROLE

	if(isset($_POST) && !empty($_POST['att_titre']) && !empty($_POST['att_contenu']) && !empty($_POST['att_famille'])){
		
		$att_sous_famille=0;
		if(!empty($_POST['att_sous_famille'])){
			$att_sous_famille=intval($_POST['att_sous_famille']);
		}
		$att_sous_sous_famille=0;
		if(!empty($_POST['att_sous_sous_famille'])){
			$att_sous_sous_famille=intval($_POST['att_sous_sous_famille']);
		}
		
		// EDITION
		if(!empty($_POST['att_id'])){
			// insérer l'attestation
			$req = $Conn->prepare("UPDATE attestations SET att_titre = :att_titre, att_contenu = :att_contenu
			, att_famille=:att_famille, att_sous_famille=:att_sous_famille, att_sous_sous_famille=:att_sous_sous_famille WHERE att_id = :att_id");
		    $req->bindParam(':att_titre', $_POST['att_titre']);
		    $req->bindParam(':att_contenu', $_POST['att_contenu']);
			$req->bindParam(':att_famille', $_POST['att_famille']);
			$req->bindParam(':att_sous_famille', $att_sous_famille);
			$req->bindParam(':att_sous_sous_famille', $att_sous_sous_famille);
		    $req->bindParam(':att_id', $_POST['att_id']);
		    $req->execute();
		    $attestation = $_POST['att_id'];
		    // fin insérer l'attestation

		    // on récupère les compétences
		    /*$req = $Conn->prepare("SELECT * FROM attestations_competences WHERE aco_attestation = :aco_attestation");
		    $req->bindParam(':aco_attestation', $attestation);
		    $req->execute();
		    $competences = $req->fetchAll();*/
		    // fin on récupère les compétences

		    // insertion des compétences
		    $req = $Conn->prepare("DELETE FROM attestations_competences WHERE aco_attestation = :aco_attestation");
		    $req->bindParam(':aco_attestation', $attestation);
		    $req->execute();
		    foreach($_POST['aco_id'] as $k=>$p){
		    	/*foreach($competences as $c){*/

		    		

		    			if(!empty($_POST['aco_competence_pratique'])){
		    				if(array_key_exists($k, $_POST['aco_competence_pratique'])){
					    		$aco_pratique = 1;
					    	}else{
					    		$aco_pratique = 0;
					    	}
		    			}else{
		    				$aco_pratique = 0;
		    			}
		    			
		    			$req = $Conn->prepare("INSERT INTO attestations_competences (aco_competence, aco_attestation, aco_pratique, aco_id) VALUES (:aco_competence, :aco_attestation, :aco_pratique, :aco_id)");
					    $req->bindParam(':aco_competence', $_POST['aco_competence'][$k]);
					    $req->bindParam(':aco_pratique', $aco_pratique);
					     $req->bindParam(':aco_attestation', $attestation);
					    $req->bindParam(':aco_id', $p);
					    $req->execute();
		    		
		    	/*}*/	
		    }
		    foreach($_POST['aco_competence'] as $k=>$c){
		    	if(!array_key_exists($k, $_POST['aco_id'])){

		    		if(!empty($_POST['aco_competence_pratique'])){
	    				if(array_key_exists($k, $_POST['aco_competence_pratique'])){
				    		$aco_pratique = 1;
				    	}else{
				    		$aco_pratique = 0;
				    	}
	    			}else{
	    				$aco_pratique = 0;
	    			}
		    		$req = $Conn->prepare("INSERT INTO attestations_competences (aco_competence, aco_attestation, aco_pratique) VALUES (:aco_competence, :aco_attestation, :aco_pratique)");
				    $req->bindParam(':aco_competence', $c);
				    $req->bindParam(':aco_attestation', $attestation);
				    $req->bindParam(':aco_pratique', $aco_pratique);
				    $req->execute();
		    	}
		    }
		    $_SESSION['message'][] = array(
				"titre" => "Attestation",
				"type" => "success",
				"message" => "Vous avez modifié votre attestation."
			);

			header("location : ../" . $_SESSION["retour"]);
		// CREATION
		}else{
			// insérer l'attestation
			$req = $Conn->prepare("INSERT INTO attestations (att_titre, att_contenu, att_famille, att_sous_famille, att_sous_sous_famille, att_statut, att_date_creation) 
			VALUES (:att_titre, :att_contenu, :att_famille, :att_sous_famille, :att_sous_sous_famille, 0, NOW())");
		    $req->bindParam(':att_titre', $_POST['att_titre']);
		    $req->bindParam(':att_contenu', $_POST['att_contenu']);
			$req->bindParam(':att_famille', $_POST['att_famille']);
			$req->bindParam(':att_sous_famille', $att_sous_famille);
			$req->bindParam(':att_sous_sous_famille', $att_sous_sous_famille);
		    $req->execute();
		    $attestation = $Conn->lastInsertId();
		    // fin insérer l'attestation

		    // insertion des compétences
		    foreach($_POST['aco_competence'] as $k=>$c){
		    	if(!empty($_POST['aco_competence_pratique'])){
    				if(array_key_exists($k, $_POST['aco_competence_pratique'])){
			    		$aco_pratique = 1;
			    	}else{
			    		$aco_pratique = 0;
			    	}
    			}else{
    				$aco_pratique = 0;
    			}
		    	$req = $Conn->prepare("INSERT INTO attestations_competences (aco_competence, aco_attestation, aco_pratique) VALUES (:aco_competence, :aco_attestation, :aco_pratique)");
			    $req->bindParam(':aco_competence', $c);
			    $req->bindParam(':aco_attestation', $attestation);
			    $req->bindParam(':aco_pratique', $aco_pratique);
			    $req->execute();
		    }

		    $_SESSION['message'][] = array(
				"titre" => "Attestation",
				"type" => "success",
				"message" => "Vous avez créé une attestation."
			);

			header("location : ../" . $_SESSION["retour"]);
		}
		
	}else{
		echo("Problème");
		die();
	}



?>