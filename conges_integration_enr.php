<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_add_notification.php');
$societe = 0;
if(!empty($_POST['con_societe'])){
    $societe = $_POST['con_societe'];
}
$agence = 0;
if(!empty($_POST['con_agence'])){
    $agence= $_POST['con_agence'];
}


// Les lignes du tableau
$lignes[0] = array(
    0 => "Matricule salarié",
    1 => "Code nature évènement",
    2 => "Date début",
    3 => "Matin",
    4 => "Date fin",
    5 => "Après-midi",
    6 => "Nombre"
);

$a_date = date("Y-m-d");
$dte = date("Y-m-t", strtotime($a_date));
$lignes[0] = array_map("utf8_decode", $lignes[0] );
$sql="SELECT * FROM conges WHERE con_annul = 0 AND con_uti_valide IN (1,3) AND con_resp_valide IN (1,3) AND con_societe=" . $societe . " AND con_agence=" . $agence . " AND (con_integre IS NULL OR con_integre=0) AND con_date_deb <= :dte";
$req = $Conn->prepare($sql);
$req->bindParam(":dte",$dte);
$req->execute();
$conges = $req->fetchAll();

$i = 0;
foreach($conges as $s){
    $sql="SELECT * FROM conges_categories WHERE cca_id = " . $s['con_categorie'];
    $req = $Conn->query($sql);
    $categorie = $req->fetch();

    $sql="UPDATE conges SET con_integre=1 WHERE con_id=" . $s['con_id'];
    $req = $Conn->query($sql);
    
    if($s['con_demi_deb']==1){
        $s['con_demi_deb']="Oui";
    }else{
        $s['con_demi_deb']="Non";	
    }

    if($s['con_demi_fin']==1){
        $s['con_demi_fin']="Oui";
    }else{
        $s['con_demi_fin']="Non";	
    }
    
    $i++;
    $lignes[$i] = array(
        0 => $s['con_matricule'],
        1 => "'" . $categorie['cca_code'] . "'",
        2 => convert_date_txt($s['con_date_deb']),
        3 => $s['con_demi_deb'],
        4 => convert_date_txt($s['con_date_fin']),
        5 => $s['con_demi_fin'],
        6 => $s['con_nb_jour'],
    );
    
    
}

$chemin = 'documents/liste_conges.csv';
$delimiteur = ';'; // Pour une tabulation, utiliser $delimiteur = "t";

// Cr�ation du fichier csv (le fichier est vide pour le moment)
// w+ : consulter http://php.net/manual/fr/function.fopen.php
$fichier_csv = fopen($chemin, 'w+');

// Si votre fichier a vocation a �tre import� dans Excel,
// vous devez imp�rativement utiliser la ligne ci-dessous pour corriger
// les probl�mes d'affichage des caract�res internationaux (les accents par exemple)
/*fprintf($fichier_csv, chr(0xEF).chr(0xBB).chr(0xBF));*/

// Boucle foreach sur chaque ligne du tableau
foreach($lignes as $ligne){
    // chaque ligne en cours de lecture est ins�r�e dans le fichier
    // les valeurs pr�sentes dans chaque ligne seront s�par�es par $delimiteur
   /* echo("<pre>");
    print_r($ligne);
    echo("</pre>");*/

    fputcsv($fichier_csv, $ligne, $delimiteur);
}

// fermeture du fichier csv
fclose($fichier_csv);
// script a part


// telechargement automatique
$file = 'documents/liste_conges.csv';

if (file_exists($file)){
    header('Content-Encoding: iso-8859-1');
    header('Content-type: text/csv; charset=iso-8859-1');
    header('Content-Description: File Transfer');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;
}

Header("Location: conges_integration.php");
die();

?>