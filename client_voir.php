<?php

// VISUALISATION D'UNE FICHE CLIENT
//Tous les clients sont stockés sur la base ORION
// les suspect sont consulter via un autre script

// FG 28/04/2020 hotfix/devis-duplicate-id
// -> la synchro des ID devis provoque duplicate primary

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_societe.php');
include('modeles/mod_agence.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_droit.php');
include('modeles/mod_parametre.php');
include('modeles/mod_famille.php');
include('modeles/mod_sous_famille.php');
include('modeles/mod_categorie.php');
//require 'classes/base.php';

include('modeles/mod_orion_utilisateur.php');
include('modeles/mod_orion_pro_categories.php');
include('modeles/mod_orion_pro_familles.php');
include('modeles/mod_orion_pro_s_familles.php');
include('modeles/mod_orion_societes.php');
include('modeles/mod_orion_agences.php');

include('modeles/mod_get_groupe.php');
include('modeles/mod_get_adresse.php');
include('modeles/mod_get_adresses.php');
include('modeles/mod_get_contact.php');
include('modeles/mod_get_contacts.php');
include('modeles/mod_get_client_infos.php');
include('modeles/mod_get_pro_familles.php');
include('modeles/mod_erreur.php');

include('modeles/mod_get_groupe_parents.php');


	// VISU D'UNE FICHE CLIENT

	$erreur=0;

	$client=0;
	if(!empty($_GET['client'])){
		$client=intval($_GET['client']);
	}
	if($client==0){
		echo("Erreur : client inconnu !");
		die();
	}

	/******************************
			DONNEE PARAMETRE
	*******************************/

	// personne connecte

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	// DROITS ACTION UTILISATEUR

	// tjr initialisé à true -> il suffit d'une condition lors des differents controle pour les passer à false.
	// la gestion inverse est plus compliqué car il faut verifier tt les critères en même temps.
	$drt_edit_adr=true;
	$drt_edit_con=true;
	$drt_edit_cli=true;
	$drt_edit_autre=true;	// inclut info / documents / logo / Acces -> faire droit dissocié si nécéssaire

	$drt_acces_holding=false;

	// DONNEE DU CLIENT

	// donnée client

	$fct_fac_groupee=false;
	$req = $Conn->prepare("SELECT * FROM clients WHERE cli_id=" . $client . ";");
	$req->execute();
	$d_client = $req->fetch();
	if(empty($d_client)){
		echo("Erreur : client inconnu !");
		die();
	}else{
		// le client est succeptible d'avoir une facturation groupée
		if(($d_client['cli_sous_categorie']==1 OR $d_client['cli_sous_categorie']==3) AND $_SESSION["acces"]["acc_droits"][8]){
			$fct_fac_groupee=true;
		}

		if($d_client['cli_categorie']==3 OR (!empty($d_client['cli_interco_soc']) AND !$_SESSION["acces"]["acc_droits"][9])){
			// si particulier ou interco sans drt compta
			$drt_edit_adr=false;
		}

		if($d_client['cli_categorie']==3){
			$drt_edit_con=false;
		}

		if(!empty($d_client['cli_interco_soc']) AND !$_SESSION["acces"]["acc_droits"][9]){
			// si c'est une interco et que je n'ai pas le droit compta
			$drt_edit_cli=false;
		}

		if($d_client['cli_sous_categorie']==5 AND $_SESSION["acces"]["acc_profil"]!=13){
			$drt_edit_adr=false;
			$drt_edit_cli=false;
		}


	}

	// donnee client societe
	// on utilise Clients_N pour lien avec table commerciaux

	$sql="SELECT cli_agence,cli_rib,cli_rib_change,cli_archive,com_label_1,com_label_2 FROM Clients LEFT OUTER JOIN Commerciaux ON (Clients.cli_commercial=Commerciaux.com_id)
	WHERE cli_id = " . $client;
	if($acc_agence>0){
		$sql.=" AND cli_agence=" . $acc_agence;
	}
	if(!$_SESSION['acces']["acc_droits"][6]){
		$sql.=" AND cli_utilisateur=" . $acc_utilisateur;
	}
	$sql.=";";
	$req=$ConnSoc->query($sql);

	$d_client_societe=$req->fetchAll();
	if(empty($d_client_societe)){
		if($d_client["cli_categorie"]==2 AND empty($d_client["cli_filiale_de"])){
			// l'utilisateur n'est pas censé avoir accès à la fiche
			// mais comme il s'agit d'une MM GC -> accès en consultation uniquement.
			$drt_edit_con=false;
			$drt_edit_adr=false;
			$drt_edit_cli=false;
			$drt_edit_autre=false;
			$drt_acces_holding=true;

		}else{
			$erreur = 1;
			$erreur_txt = "Le client n'est pas accessible sur la société sur laquelle vous êtes connecté. <strong>Changez de société pour voir le client</strong>";
			/* echo("Erreur : impossible d'afficher la page! (Err 0001)");
			die(); */
		}
	}else{
		foreach($d_client_societe as $k => $d_cli_soc){

			if(empty($d_cli_soc["cli_agence"])){
				$d_client_societe[$k]["age_code"]="";
			}else{
				$sql="SELECT age_code FROM Agences WHERE age_id = " . $d_cli_soc["cli_agence"];
				$req=$Conn->query($sql);
				$d_agence=$req->fetch();
				$d_client_societe[$k]["age_code"]=$d_agence["age_code"];
			}

		}
	}

	// VALEURS PARAMETRES

	$categorie_produit=orion_produits_categories();
	$famille_produit=orion_produits_familles();
	$sous_famille_produit=orion_produits_sous_familles();
	$sous_sous_famille_produit = orion_produits_sous_sous_familles();
	$utilisateurs=orion_utilisateurs();

	// LA SOCIETE

	$sql="SELECT soc_intra_inter FROM Societes WHERE soc_id = " . $acc_societe;
	$req=$Conn->query($sql);
	$d_societe=$req->fetch();


	/******************************
			ONGLET GROUPE
	*******************************/

	$maison_mere_id=$d_client["cli_id"];
	if($d_client["cli_groupe"]==1){
		if($d_client["cli_filiale_de"]>0){

			// c'est une filiale
			$maison_mere_id=$d_client["cli_filiale_de"];

			// si GC
			if($d_client["cli_categorie"]==2){
				$drt_acces_holding=true;
			}
		}
		$factureur=false;
		if( ($d_client["cli_sous_categorie"]==1 OR $d_client["cli_sous_categorie"]==3) AND $_SESSION["acces"]["acc_droits"][8]){
			// U a le droit GC et les cient est succeptible d'avoir une facturation groupée
			$factureur=true;
		}
		$array_groupe=get_groupe($maison_mere_id,$factureur);

	}

	/******************************
			ONGLET GENERAL
	*******************************/

	// sur la categorie du client

	$sql="SELECT cca_gc,cca_libelle,cca_couleur FROM clients_categories WHERE cca_id=" . $d_client["cli_categorie"] . ";";
	$req=$Conn->query($sql);
	$d_categorie=$req->fetch();

	// sous categorie
	if(!empty($d_client["cli_sous_categorie"])){
		$sql="SELECT csc_libelle,csc_couleur FROM clients_Sous_categories WHERE csc_id=" . $d_client["cli_sous_categorie"] . ";";
		$req=$Conn->query($sql);
		$d_sous_categorie=$req->fetch();
	}

	// style
	$couleur="";
	$class_panel="info";
	if($d_client['cli_blackliste']){
		$couleur="#000";
	}elseif(empty($d_client['cli_first_facture'])){
		$class_panel="success";
	}elseif(isset($d_sous_categorie)){
		if(!empty($d_sous_categorie["csc_couleur"])){
			$couleur=$d_sous_categorie["csc_couleur"];
		}
	}elseif(!empty($d_categorie["cca_couleur"])){
		$couleur=$d_categorie["cca_couleur"];
	}

	//adresse d'intervention par défaut
	// on utilise l'adresse par défaut enregistrée sur la fiche client sur l'onglet général
	$adresse_int="";
	$adresse_int_id=0;
	if(!empty($d_client["cli_adresse"])){
		$adresse_int_id=$d_client["cli_adresse"];
		if(!empty($d_client["cli_adr_nom"])){
			$adresse_int="<strong>" . $d_client["cli_adr_nom"] . "</strong>";
		}
		if(!empty($d_client["cli_adr_service"])){
			if(!empty($adresse_int)){
				$adresse_int.="<br/>";
			}
			$adresse_int=$d_client["cli_adr_service"];
		}
		if(!empty($d_client["cli_adr_ad1"])){
			if(!empty($adresse_int)){
				$adresse_int.="<br/>";
			}
			$adresse_int.=$d_client["cli_adr_ad1"];
		}
		if(!empty($d_client["cli_adr_ad2"])){
			if(!empty($adresse_int)){
				$adresse_int.="<br/>";
			}
			$adresse_int.=$d_client["cli_adr_ad2"];
		}
		if(!empty($d_client["cli_adr_ad3"])){
			if(!empty($adresse_int)){
				$adresse_int.="<br/>";
			}
			$adresse_int.=$d_client["cli_adr_ad3"];
		}
		if(!empty($adresse_int)){
			$adresse_int.="<br/>";
		}
		$adresse_int.=$d_client["cli_adr_cp"] . " " . $d_client["cli_adr_ville"];
	}

	// adresse de facturation par defaut
	// => Voir onglet adresse


	//contact par defaut
	$d_con_fonction_nom=array(
		"0" => ""
	);
	$sql="SELECT * FROM contacts_fonctions ORDER BY cfo_libelle";
	$req=$Conn->query($sql);
	$d_contact_fonctions=$req->fetchAll();
	foreach($d_contact_fonctions as $d){
		$d_con_fonction_nom[$d["cfo_id"]]=$d["cfo_libelle"];
	}

	$contact_def="";
	$contact_def_id=0;
	if(!empty($d_client["cli_contact"])){
		$d_contact_def=get_contact($d_client["cli_contact"]);
		if(!empty($d_contact_def)){
			foreach($base_civilite as $k => $v){
				if($k == $d_contact_def['con_titre']){
					$contact_def=$v . " ";
				}
			}
			$contact_def.=$d_contact_def['con_prenom'] . " " . $d_contact_def['con_nom'] . " - ";
			if(!empty($d_contact_def['con_fonction_nom'])){
				$contact_def.=$d_contact_def['con_fonction_nom'];
			}elseif(!empty($d_contact_def['con_fonction'])){
				$contact_def.=$d_con_fonction_nom[$d_contact_def['con_fonction']];
			}
			$contact_def.="<br>";
			if(!empty($d_contact_def['con_tel'])){
				$contact_def.="Tél: " . $d_contact_def['con_tel'];
			};
			if(!empty($d_contact_def['con_portable'])){
				$contact_def.=" - Portable: " . $d_contact_def['con_portable'];
			};
			if(!empty($d_contact_def['con_fax'])){
				$contact_def.=" - Fax: " . $d_contact_def['con_fax'];
			};
			$contact_def.="<br>";
			$contact_def.=$d_contact_def['con_mail'];
		}
	}

	// fin contact

	if(!empty($d_client["cli_classification"])){
		$sql="SELECT ccl_libelle FROM clients_Classifications WHERE ccl_id=" . $d_client["cli_classification"] . ";";
		$req=$Conn->query($sql);
		$d_classification=$req->fetch();
	}

	if(!empty($d_client["cli_classification_categorie"])){
		$sql="SELECT ccc_libelle FROM Clients_classifications_categories WHERE ccc_id=" . $d_client["cli_classification_categorie"] . ";";
		$req=$Conn->query($sql);
		$d_classification_categorie=$req->fetch();
	}

	if(!empty($d_client["cli_classification_type"])){
		$sql="SELECT cct_libelle FROM Clients_classifications_types WHERE cct_id=" . $d_client["cli_classification_type"] . ";";
		$req=$Conn->query($sql);
		$d_classification_type=$req->fetch();
	}


	// OPCA

	if(!empty($d_client['cli_opca'])){
		$req = $Conn->prepare("SELECT * FROM clients WHERE cli_id=" . $d_client['cli_opca'] . ";");
		$req->execute();
		$opca = $req->fetch();
	}
	if(!empty($d_client['cli_opca_type'])){

		$cli_opca_type=intval($d_client['cli_opca_type']);

		$req = $Conn->prepare("SELECT csc_libelle FROM clients_Sous_Categories WHERE csc_id=" . $cli_opca_type . ";");
		$req->execute();
		$d_opca_type = $req->fetch();
	}


	// rib
	if(!empty($d_client_societe[0]["cli_rib"])){
		$sql="SELECT rib_nom FROM Rib WHERE rib_id=" . $d_client_societe[0]["cli_rib"] . ";";
		$req=$Conn->query($sql);
		$d_rib=$req->fetch();
	}


	// Code APE
	if(!empty($d_client["cli_ape"])){
		$sql="SELECT ape_code FROM Ape WHERE ape_id=" . $d_client["cli_ape"] . ";";
		$req=$Conn->query($sql);
		$d_ape=$req->fetch();
	}

	// exercie en cours

	$exercice=intval(date("Y"));
	if(date("m")<"04"){
		$exercice--;
	}

	// 5 dernier devis
	$sql="SELECT dev_id,dev_numero,dev_date,dev_total_ht,dev_esperance,dev_com_identite,dev_con_identite FROM Devis WHERE dev_client=". $client;
	if($acc_agence>0){
		$sql.=" AND dev_agence=" . $acc_agence;
	}
	if(!$_SESSION["acces"]["acc_droits"][6]){
		$sql.=" AND dev_utilisateur=" . $acc_utilisateur;
	}
	$sql.=" ORDER BY dev_date DESC LIMIT 5;";
	$req=$ConnSoc->query($sql);
	$d_devis=$req->fetchAll();

	// 5 prochaines actions
	$sql="SELECT act_id,act_date_deb,int_label_1,acl_id,acl_pro_reference,acl_ca FROM Actions_Clients,Actions,Intervenants
	WHERE acl_action=act_id AND act_intervenant=int_id AND act_date_deb>NOW() AND acl_client=" . $client;
	if($acc_agence>0){
		$sql.=" AND act_agence=" . $acc_agence;
	}

	$sql.=" ORDER BY act_date_deb LIMIT 5;";
	$req=$ConnSoc->query($sql);
	$d_actions=$req->fetchAll();


	// pagination
	if(isset($_SESSION['client_tableau'])){
		if(is_array($_SESSION['client_tableau'])){
			$cle=array_search ($client . "-2",$_SESSION['client_tableau']);
			if(!is_bool($cle)){
				if($cle>0){
					$tab_prec=explode("-",$_SESSION['client_tableau'][$cle-1]);
				}
				if($cle<count($_SESSION['client_tableau'])-1){
					$tab_suiv=explode("-",$_SESSION['client_tableau'][$cle+1]);
				}
			}
		}
	}



	// **** FIN ONGLET GENERAL ****


	// ONGLET ADRESSE

	$adresses_int = get_adresses(1,$d_client['cli_id'],1);
	$adresses_fac = get_adresses(1,$d_client['cli_id'],2);
	if(!empty($adresses_fac)){
		if($adresses_fac[0]["adr_defaut"]==1){
			$adresse_fac_def=$adresses_fac[0];
			$adresse_fac="";
			if(!empty($adresse_fac_def["adr_nom"])){
				$adresse_fac="<strong>" . $adresse_fac_def["adr_nom"] . "</strong>";
			}
			if(!empty($adresse_fac_def["adr_service"])){
				if(!empty($adresse_fac)){
					$adresse_fac.="<br/>";
				}
				$adresse_fac.=$adresse_fac_def["adr_service"];
			}
			if(!empty($adresse_fac_def["adr_ad1"])){
				if(!empty($adresse_fac)){
					$adresse_fac.="<br/>";
				}
				$adresse_fac.=$adresse_fac_def["adr_ad1"];
			}
			if(!empty($adresse_fac_def["adr_ad2"])){
				if(!empty($adresse_fac)){
					$adresse_fac.="<br/>";
				}
				$adresse_fac.=$adresse_fac_def["adr_ad2"];
			}
			if(!empty($adresse_fac_def["adr_ad3"])){
				if(!empty($adresse_fac)){
					$adresse_fac.="<br/>";
				}
				$adresse_fac.=$adresse_fac_def["adr_ad3"];
			}
			if(!empty($adresse_fac)){
				$adresse_fac.="<br/>";
			}
			$adresse_fac.=$adresse_fac_def["adr_cp"] . " " . $adresse_fac_def["adr_ville"];
		}
	}
	$adresses_env = get_adresses(1,$d_client['cli_id'],3);


	// ONGLET CONTACTS

	$sql="SELECT DISTINCT con_id,con_fonction,con_fonction_nom,con_titre,con_nom,con_prenom,con_tel,con_portable,con_fax,con_mail,con_compta,con_comment,aco_contact
	FROM Contacts LEFT OUTER JOIN Adresses_Contacts ON (Contacts.con_id=Adresses_Contacts.aco_contact AND aco_defaut)
	WHERE con_ref_id=" . $client . " AND con_ref=1;";
	$req=$Conn->query($sql);
	$contacts=$req->fetchAll();

	//*******************************
	//		ONGLET 2 DEVIS
	//********************************

	$devis_histo=array(
		"client" => array(
			"0" => 0,
			"1" => 0,
			"2" => 0,
			"3" => 0,
			"4" => 0,
			"5" => 0,
			"6" => 0,
			"7" => 0,
			"8" => 0,
			"9" => 0,
			"10" => 0
		)
	);

	// HISTO DEVIS

	$sql="SELECT SUM(dev_total_ht) AS dev_montant,MONTH(dev_date) AS dev_mois,YEAR(dev_date) AS dev_annee
	FROM Devis WHERE dev_client=". $client . " AND dev_date>='" . intval($exercice-10) . "-04-01' AND dev_date<='" . intval($exercice+1) . "-03-31' AND NOT dev_bordereau";
	if($acc_agence>0){
		$sql.=" AND dev_agence=" . $acc_agence;
	}
	$sql.=" GROUP BY MONTH(dev_date),YEAR(dev_date);";
	$req=$ConnSoc->query($sql);
	$result_devis=$req->fetchAll();
	if(!empty($result_devis)){
		foreach($result_devis as $r_devis){

			$dev_exercice=$r_devis["dev_annee"];
			if($r_devis["dev_mois"]<4){
				$dev_exercice--;
			};
			$cle_exe=$exercice-$dev_exercice;

			$devis_histo["client"][$cle_exe]=$devis_histo["client"][$cle_exe]+$r_devis["dev_montant"];

		}
	}

	// DEVIS EXERCICE EN COURS
	$sql_devis="SELECT dev_id,dev_adr_ville,dev_numero,DATE_FORMAT(dev_date,'%d/%m/%Y') AS dev_date_aff,dev_prospect,dev_esperance,dev_adr_ville,dev_bordereau
	,dli_reference,dli_action,dli_action_client,dli_montant_ht,dli_id
	,cli_id,cli_code,cli_nom,dev_chrono
	FROM Devis INNER JOIN Devis_Lignes ON (Devis.dev_id=Devis_Lignes.dli_devis)
	INNER JOIN Clients ON (Devis.dev_client=Clients.cli_id)
	INNER JOIN Commerciaux ON (Devis.dev_commercial=Commerciaux.com_id)
	WHERE dev_client=" . $client . " AND dev_date>='" . intval($exercice) . "-04-01' AND NOT dev_bordereau";
	if($acc_agence>0){
		$sql_devis.=" AND dev_agence=" . $acc_agence;
	}
	$sql_devis.=" ORDER BY dev_date,dev_chrono;";
	$req_devis = $ConnSoc->prepare($sql_devis);
	$req_devis->execute();
	$d_devis_exe = $req_devis->fetchAll();



	//*******************************
	//		ONGLET 3 FACTURE
	//********************************

	$facture_histo=array(
		"client" => array(
			"0" => 0,
			"1" => 0,
			"2" => 0,
			"3" => 0,
			"4" => 0,
			"5" => 0,
			"6" => 0,
			"7" => 0,
			"8" => 0,
			"9" => 0,
			"10" => 0
		)
	);

	// HISTO FACTURE

	$sql="SELECT SUM(fac_total_ht) AS fac_montant,MONTH(fac_date) AS fac_mois,YEAR(fac_date) AS fac_annee
	FROM Factures WHERE fac_client=". $client . " AND fac_date>='" . intval($exercice-10) . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'";
	if($acc_agence>0){
		$sql.=" AND fac_agence=" . $acc_agence;
	}
	$sql.=" GROUP BY MONTH(fac_date),YEAR(fac_date);";
	$req=$ConnSoc->query($sql);
	$result_facture=$req->fetchAll();
	if(!empty($result_facture)){
		foreach($result_facture as $r_facture){

			$fac_exercice=$r_facture["fac_annee"];
			if($r_facture["fac_mois"]<4){
				$fac_exercice--;
			};
			$cle_exe=$exercice-$fac_exercice;

			$facture_histo["client"][$cle_exe]=$facture_histo["client"][$cle_exe]+$r_facture["fac_montant"];

		}
	}

	// FACTURATION DE L'EXERCICE EN COURS

	$sql="SELECT fac_id,fac_chrono,fac_numero,fac_date,fac_total_ht,fac_total_ttc,fac_regle,fac_date_reg_prev
	,cli_code,cli_nom
	,com_label_1,com_label_2
	FROM Factures
	LEFT OUTER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)
	LEFT OUTER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
	WHERE fac_client=" . $client . " AND fac_date>='" . intval($exercice) . "-04-01'";
	if($acc_agence>0){
		$sql_devis.=" AND fac_agence=" . $acc_agence;
	}
	$sql.=" ORDER BY fac_date,fac_chrono,fac_numero;";
	$req = $ConnSoc->query($sql);
	$d_facture_exe = $req->fetchAll();


	//*******************************
	//		ONGLET 3 ACTIONS
	//********************************

		$sql_action="SELECT act_id,act_adr_ville,DATE_FORMAT(act_date_deb,'%d/%m/%Y') AS act_date_deb_fr,acl_id,acl_ca,acl_pro_reference,acl_confirme,acl_action,acl_pro_libelle,acl_pro_reference";
		$sql_action.=",COUNT(acd_date) AS nb_demi_j";
		$sql_action.=",int_label_1";
		$sql_action.=" FROM Actions LEFT OUTER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)
		LEFT OUTER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
		LEFT OUTER JOIN Intervenants ON (Actions.act_intervenant=Intervenants.int_id)
		WHERE NOT acl_archive AND act_date_deb>=NOW() AND acl_client=" . $client;
		if(!empty($acc_agence)){
			$sql_action.= " AND act_agence = " . $acc_agence;
		}
		$sql_action.=" GROUP BY act_id,act_adr_ville,act_date_deb,acl_id,acl_ca,acl_pro_reference,acl_confirme,acl_action,acl_pro_libelle,acl_pro_reference
		ORDER BY act_date_deb,act_intervenant,act_id,acl_id";
		$req=$ConnSoc->query($sql_action);
		$actions=$req->fetchAll();



	// ONGLET PRODUIT
	$aff_onglet_produit=false;
	if(!empty($d_categorie)){
		if($d_client["cli_categorie"]!=3 AND empty($d_client['cli_interco_soc'])){

			$aff_onglet_produit=true;

			$sql="SELECT pro_id,pro_code_produit,pro_libelle,pro_categorie,pro_famille,pro_sous_famille,pro_sous_sous_famille,pro_archive
			,cpr_intra,cpr_ht_intra,cpr_inter,cpr_ht_inter,cpr_valide,cpr_comment_txt,cpr_confirmation_gc,cpr_cn,cpr_valide,cpr_derogation,cpr_devis_txt,cpr_libelle
			FROM Produits INNER JOIN Clients_Produits ON (Produits.pro_id=Clients_Produits.cpr_produit)
			WHERE cpr_client=:maison_mere ORDER BY pro_code_produit";
			$req=$Conn->prepare($sql);
			$req->bindParam("maison_mere",$maison_mere_id);
			$req->execute();
			$produits=$req->fetchAll();

		}
	}

	// ONGLET CORRESPONDANCES
	$aff_onglet_corresp=false;
	if(empty($d_client['cli_interco_soc'])){
		$aff_onglet_corresp=true;

		$sql="SELECT * FROM Correspondances
		LEFT JOIN Correspondances_Types ON (Correspondances.cor_type=Correspondances_Types.cty_id)
		LEFT JOIN Correspondances_Raisons ON (Correspondances.cor_raison=Correspondances_Raisons.cra_id)
		WHERE cor_client=:client ORDER BY cor_date DESC;";
		$req=$Conn->prepare($sql);
		$req->bindParam("client",$client);
		$req->execute();
		$correspondances = $req->fetchAll();
		if(!empty($correspondances)){
			$last_cor=$correspondances[0];
		}
	}

	// ONGLET INFOS
	$d_info_type=array();
	$sql="SELECT cin_id,cin_libelle FROM Clients_Infos ORDER BY cin_libelle;";
	$req=$Conn->query($sql);
	$d_result=$req->fetchAll();
	foreach($d_result as $d){
		$d_info_type[$d["cin_id"]]=$d["cin_libelle"];
	}
	$infos=get_client_infos($client,0,0,0,0);

	// ONGLET 7 : DOCUMENTS

	$aff_onglet_doc=false;
	if($d_client["cli_categorie"]!=3 AND empty($d_client['cli_interco_soc'])){

		$aff_onglet_doc=true;

		// les doc du client
		$sql="SELECT * FROM clients_documents LEFT JOIN clients_documents_types ON (clients_documents.cdo_type=clients_documents_types.cdt_id)
		WHERE cdo_client = " . $client . ";";
		$req = $Conn->query($sql);
		$d_doc_client = $req->fetchAll();


		// dcouments MM
		$sql="SELECT doc_nom,doc_nom_aff FROM documents,Documents_Utilisateurs WHERE doc_id=dut_document
		AND dut_utilisateur=:utilisateur AND (doc_client = :client OR doc_client=:maison_mere);";
		$req = $Conn->prepare($sql);
		$req->bindParam(':client', $client);
		$req->bindParam(':maison_mere', $maison_mere_id);
		$req->bindParam(':utilisateur', $acc_utilisateur);
		$req->execute();
		$docs_etech = $req->fetchAll();
	}

	// ONGLET 8 - LOGO
	$aff_onglet_logo=false;
	if($d_client["cli_categorie"]!=3 AND empty($d_client['cli_interco_soc'])){
		$aff_onglet_logo=true;
	}

	// ONGLET 14 - ACCES
	foreach($contacts as $c){
		$req=$Conn->prepare("SELECT * FROM acces WHERE acc_ref = 2 AND acc_ref_id = :client ORDER BY acc_uti_ident, acc_archive ASC");
		$req->bindParam(':client', $c["con_id"]);
		$req->execute();
		$acces_result=$req->fetch();
		if(!empty($acces_result)){
			$d_acces_client[] = $acces_result;
		}
	}


	// ONGLET ACTIF ET Retour

	$_SESSION['retourCorresp'] = "client_voir.php?client=" . $client;
	$_SESSION['retourContact'] = "client_voir.php?client=" . $client;
	$_SESSION['retourAdresse'] = "client_voir.php?client=" . $client;
	$_SESSION['retourDevis'] = "client_voir.php?client=" . $client;
	$_SESSION['retour_action'] = "client_voir.php?client=" . $client;

	$onglet=1;
	if(isset($_GET['onglet'])){
		$onglet=intval($_GET['onglet']);
	}elseif(isset($_GET['tab2'])){
		$onglet=2;
	}elseif(isset($_GET['tab10'])){
		$onglet=10;
	}elseif(isset($_GET['tab6'])){
		$onglet=6;
	}elseif(isset($_GET['tab5'])){
		$onglet=5;
	}elseif(isset($_GET['tab7'])){
		$onglet=7;
	}elseif(isset($_GET['tab11'])){
		$onglet=11;
	}elseif(isset($_GET['tab13'])){
		$onglet=13;
	}elseif(isset($_GET['tab14'])){
		$onglet=14;
	}



?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - <?=$d_client['cli_nom']?></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >

		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<!-- Start: Main -->

		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper" class="">
				<section id="content" class="">
		<?php		if($erreur==0){ ?>

						<!-- Begin .page-heading -->

						<div class="row">

				  <?php 	if(isset($_GET['succes']) && $_GET['succes'] == 5){ ?>
							   <div class="col-md-12">
								 <div class="alert alert-success alert-dismissable">
								   <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
									&times;
								  </button>
								  Le client a été créé
								</div>
							  </div>
				  <?php 	} ?>
							<div class="col-md-12">

								<h1><?="[" . $d_client['cli_code'] . "] -" . $d_client['cli_nom']?></h1>

								<div class="tab-block">
									<ul class="nav nav-tabs responsive">
							<?php		if($d_client['cli_groupe'] == 1){ ?>
											<li <?php if($onglet==10) echo("class='active'");?> >
												<a href="#tab10" data-toggle="tab" class="onglet clic-groupe" aria-expanded="false" data-onglet="groupe" >Groupe</a>
											</li>
							<?php 		} ?>
										<li <?php if($onglet==1) echo("class='active'");?> >
										  <a href="#tab1" data-toggle="tab" class="onglet clic-general" aria-expanded="true" data-onglet="general" >Général</a>
										</li>
										<?php if($d_client["cli_categorie"]!=3){ ?>
											<li <?php if($onglet==11) echo("class='active'");?> >
												<a href="#tab11" data-toggle="tab" class="onglet clic-adresse" aria-expanded="false" data-onglet="adresse" >Carnet d'adresses</a>
											</li>
											<li <?php if($onglet==12) echo("class='active'");?> >
												<a href="#tab12" data-toggle="tab" class="onglet clic-contact" aria-expanded="false" data-onglet="contact" >Carnet de contacts</a>
											</li>
										<?php }
										if($aff_onglet_corresp){ ?>
											<li <?php if($onglet==5) echo("class='active'");?> >
												<a href="#tab5" data-toggle="tab" class="onglet clic-cor" aria-expanded="false" data-onglet="cor" >Correspondances</a>
											</li>
								<?php	} ?>
										<li <?php if($onglet==6) echo("class='active'");?> >
										  <a href="#tab6" data-toggle="tab" class="onglet clic-info" aria-expanded="false" data-onglet="info" >Infos</a>
										</li>
								<?php	if($aff_onglet_produit){ ?>
											<li <?php if($onglet==13) echo("class='active'");?> >
												<a href="#tab13"  data-toggle="tab" class="onglet clic-produit" aria-expanded="false" data-onglet="produit" >Produits</a>
											</li>
								<?php	}
										if($aff_onglet_doc){ ?>
											<li <?php if($onglet==7) echo("class='active'");?> >
												<a href="#tab7" data-toggle="tab" class="onglet clic-document" aria-expanded="false" data-onglet="document" >Documents</a>
											</li>
								<?php	}
										if($aff_onglet_logo){ ?>
											<li <?php if($onglet==8) echo("class='active'");?> >
												<a href="#tab8" data-toggle="tab" class="onglet clic-logos" aria-expanded="false" data-onglet="logos" >Logos</a>
											</li>
								<?php	} ?>
										<!-- devis-->
										<li <?php if($onglet==2) echo("class='active'");?> >
											<a href="#tab2" data-toggle="tab" class="onglet clic-devis" aria-expanded="false" data-onglet="devis" >Devis</a>
										</li>
										<li <?php if($onglet==4) echo("class='active'");?> >
											<a href="#tab4" data-toggle="tab" class="onglet clic-action" aria-expanded="false" data-onglet="action" >Planning</a>
										</li>
										<!--facture-->
										<li <?php if($onglet==3) echo("class='active'");?> >
											<a href="#tab3" data-toggle="tab" class="onglet clic-facture" aria-expanded="false" data-onglet="facture" >Factures</a>
										</li>
										<li <?php if($onglet==14) echo("class='active'");?> >
											<a href="#tab14" data-toggle="tab" class="onglet clic-acces" aria-expanded="false" data-onglet="acces" >Accès</a>
										</li>
									</ul>

									<div class="tab-content responsive p30" style="min-height:500px;">

						<?php 			// DEBUT TAB10
										if($d_client['cli_groupe'] == 1){  ?>
											<div id="tab10" class="tab-pane <?php if($onglet==10) echo("active"); ?>" >

												<h2 style="margin-top:0px;margin-bottom:15px;">Groupe</h2>
												<div id="nestable" class="dd" style="width:100%">
													<ol class="dd-list">
											<?php	if(isset($array_groupe)){
														$d_grp_mm=$array_groupe[-1][0]; ?>
														<li class="dd-item" data-level="-1" data-id="<?=$d_grp_mm["cli_id"]?>" >
															<!-- Maison MERE -->
															<div class="dd-handle" <?php if($d_grp_mm['cli_id'] == $d_client['cli_id']) echo("style='color:#e31936;'"); ?> >
																Maison mère : <?=$d_grp_mm['cli_code'] . " | " . $d_grp_mm['cli_nom']?>
												<?php 			if(!empty($d_grp_mm['cli_reference'])){ ?>
																	<strong>| Référence : </strong> <?= $d_grp_mm['cli_reference']; ?>
												<?php 			}
																if(!empty($d_grp_mm['cli_adr_cp'])): ?>
																	<strong> | </strong>  <?= $d_grp_mm['cli_adr_cp']; ?> - <?= $d_grp_mm['cli_adr_ville']; ?>
												<?php 			endif;
																if($fct_fac_groupee){ ?>
																	<a class="pull-right btn-xs btn-success dd-nodrag ml10 btn-facturation" data-fac_op="1" data-fac_client="<?=$d_grp_mm['cli_id']?>" data-toggle="tooltip" title="Activer la facturation groupée" >
																		<i class="fa fa-euro" ></i>
																	</a>
																	<a class="pull-right btn-xs btn-danger dd-nodrag ml10 btn-facturation" data-fac_op="0" data-fac_client="<?=$d_grp_mm['cli_id']?>" data-toggle="tooltip" title="Désactiver la facturation groupée" >
																		<i class="fa fa-euro" ></i>
																	</a>
												<?php			}
																if(!empty($d_grp_mm['cso_acces']) OR $drt_acces_holding){ ?>
																	<a class="pull-right btn-xs btn-info dd-nodrag mr10" href="client_voir.php?client=<?= $d_grp_mm['cli_id'] ?>">
																		<strong>Voir le client</strong>
																	</a>
												<?php			}
																if($fct_fac_groupee){ ?>
																	<div class="fac-groupee fac-groupee-<?=$d_grp_mm['cli_id']?> pull-right btn-xs btn-default mr10" >
													<?php				if(!empty($d_grp_mm['fac_code'])){ ?>
																			<div data-toggle="tooltip" title="Facturation groupée sur le compte <?=$d_grp_mm['fac_code']?>." >
																				<i class="fa fa-euro" ></i> <?=$d_grp_mm['fac_code']?>
																			</div>
													<?php				}?>
																	</div>
													<?php		} ?>
															</div>
															<ol class="dd-list">
											<?php 				if(isset($array_groupe[0])): ?>
											 <?php 					foreach($array_groupe[0] as $a): ?>

																		<!-- FILIALE DE 1er NIVEAU cli_niveau =0  -->
																		<li class="dd-item" data-level="0" data-id="<?= $a['cli_id']?>" >
																			<div class="dd-handle"  <?php if($d_client['cli_id'] == $a['cli_id']) echo("style='color:#e31936;'"); ?> >
																				<?=$a['cli_code'] . " | " . $a['cli_nom'] ?>
																  <?php 		if(!empty($a['cli_reference'])): ?>
																					<strong> | Référence : </strong> <?= $a['cli_reference']; ?>
																	<?php 		endif;
																				if(!empty($a['cli_adr_cp'])): ?>
																					<strong> | </strong> <?= $a['cli_adr_cp']; ?> - <?= $a['cli_adr_ville']; ?>
																	<?php 		endif;

																				if($fct_fac_groupee){ ?>
																					<a class="pull-right btn-xs btn-success dd-nodrag ml10 btn-facturation" data-fac_op="1" data-fac_client="<?=$a['cli_id']?>" data-toggle="tooltip" title="Activer la facturation groupée" >
																						<i class="fa fa-euro" ></i>
																					</a>
																					<a class="pull-right btn-xs btn-danger dd-nodrag ml10 btn-facturation" data-fac_op="0" data-fac_client="<?=$a['cli_id']?>" data-toggle="tooltip" title="Désactiver la facturation groupée" >
																						<i class="fa fa-euro" ></i>
																					</a>
																<?php			}
																				if(!empty($a['cso_acces'])){ ?>
																					<a class="pull-right btn-xs btn-info dd-nodrag mr10" onclick="document.location.href='client_voir.php?client=<?= $a['cli_id'] ?>'">
																						<strong>Voir le client</strong>
																					</a>
																	<?php		}
																				if($fct_fac_groupee){ ?>
																					<div class="fac-groupee fac-groupee-<?=$a['cli_id']?> pull-right btn-xs btn-default mr10" >
																	<?php				if(!empty($a['fac_code'])){ ?>
																							<div data-toggle="tooltip" title="Facturation groupée sur le compte <?=$a['fac_code']?>." >
																								<i class="fa fa-euro" ></i> <?=$a['fac_code']?>
																							</div>
																			<?php		}?>
																					</div>
																	<?php		} ?>
																			</div>
																<?php 		if(isset($array_groupe[1])): ?>
																				<ol class="dd-list">
																		<?php
																					foreach($array_groupe[1] as $a1):
																						if($a1['cli_fil_de'] == $a['cli_id']): ?>
																							<!-- FILIALE DE 2eme NIVEAU cli_niveau =1  -->
																							<li class="dd-item" data-level="1" data-id="<?= $a1['cli_id'] ?>" >
																								<div class="dd-handle" <?php if($d_client['cli_id'] == $a1['cli_id']) echo("style='color:#e31936;'"); ?> >
																									<?=$a1['cli_code'] . " | " . $a1['cli_nom'] ?>
																					<?php 			if(!empty($a1['cli_reference'])): ?>
																										<strong> | Référence : </strong> <?= $a1['cli_reference']; ?>
																					<?php 			endif;
																									if(!empty($a1['cli_adr_cp'])): ?>
																										<strong> | </strong> <?= $a1['cli_adr_cp']; ?> - <?= $a1['cli_adr_ville']; ?>
																					<?php 			endif;
																									if($fct_fac_groupee){ ?>
																										<a class="pull-right btn-xs btn-success dd-nodrag ml10 btn-facturation" data-fac_op="1" data-fac_client="<?=$a1['cli_id']?>" data-toggle="tooltip" title="Activer la facturation groupée" >
																											<i class="fa fa-euro" ></i>
																										</a>
																										<a class="pull-right btn-xs btn-danger dd-nodrag ml10 btn-facturation" data-fac_op="0" data-fac_client="<?=$a1['cli_id']?>" data-toggle="tooltip" title="Désactiver la facturation groupée" >
																											<i class="fa fa-euro" ></i>
																										</a>
																					<?php			}
																									if(!empty($a1['cso_acces'])){ ?>
																										<a class="pull-right btn-xs btn-info dd-nodrag" href="client_voir.php?client=<?= $a1['cli_id'] ?>">
																											<strong>Voir le client</strong>
																										</a>
																					<?php			}
																									if($fct_fac_groupee){ ?>
																										<div class="fac-groupee fac-groupee-<?=$a1['cli_id']?> pull-right btn-xs btn-default mr10" >
																					<?php					if(!empty($a1['fac_code'])){ ?>
																												<div data-toggle="tooltip" title="Facturation groupée sur le compte <?=$a1['fac_code']?>." >
																													<i class="fa fa-euro" ></i> <?=$a1['fac_code']?>
																												</div>
																								<?php		} ?>
																										</div>

																					<?php			} ?>


																								</div>
																				<?php 			if(isset($array_groupe[2])): ?>
																									<ol class="dd-list">
																				<?php 					foreach($array_groupe[2] as $a2):
																											if($a2['cli_fil_de'] ==$a1['cli_id']): ?>
																												<!-- FILIALE DE 3eme NIVEAU cli_niveau =2  -->
																												<li class="dd-item" data-level="2" data-id="<?= $a2['cli_id'] ?>">
																													<div class="dd-handle" <?php if($d_client['cli_id'] == $a2['cli_id']) echo("style='color:#e31936;'"); ?> >
																														<?=$a2['cli_code'] . " | " . $a2['cli_nom'] ?>
																												<?php 	if(!empty($a2['cli_reference'])): ?>
																															<strong> | Référence : </strong> <?= $a2['cli_reference']; ?>
																												<?php 	endif;
																														if(!empty($a2['cli_adr_cp'])): ?>
																															<strong> | </strong>  <?= $a2['cli_adr_cp']; ?> - <?= $a2['cli_adr_ville']; ?>
																			<?php 										endif;
																														if($fct_fac_groupee){ ?>
																															<a class="pull-right btn-xs btn-success dd-nodrag ml10 btn-facturation" data-fac_op="1" data-fac_client="<?=$a2['cli_id']?>" data-toggle="tooltip" title="Activer la facturation groupée" >
																																<i class="fa fa-euro" ></i>
																															</a>
																															<a class="pull-right btn-xs btn-danger dd-nodrag ml10 btn-facturation" data-fac_op="0" data-fac_client="<?=$a2['cli_id']?>" data-toggle="tooltip" title="Désactiver la facturation groupée" >
																																<i class="fa fa-euro" ></i>
																															</a>
																										<?php			}
																														if(!empty($a2['cso_acces'])){ ?>
																															<a class="pull-right btn-xs btn-info dd-nodrag" href="client_voir.php?client=<?= $a2['cli_id'] ?>">
																																<strong>Voir le client</strong>
																															</a>
																											<?php		}
																														if($fct_fac_groupee){ ?>
																															<div class="fac-groupee fac-groupee-<?=$a2['cli_id']?> pull-right btn-xs btn-default mr10" >
																										<?php					if(!empty($a2['fac_code'])){ ?>
																																	<div class="" data-toggle="tooltip" title="Facturation groupée sur le compte <?=$a2['fac_code']?>." >
																																		<i class="fa fa-euro" ></i> <?=$a2['fac_code']?>
																																	</div>
																														<?php	} ?>
																															</div>
																										<?php			} ?>
																													</div>
																											<?php 	if(isset($array_groupe[3])): ?>
																														<ol class="dd-list">
																												<?php 		foreach($array_groupe[3] as $a3):
																																if($a3['cli_fil_de'] ==$a2['cli_id']): ?>
																																	<!-- FILIALE DE 4eme NIVEAU cli_niveau =3  -->
																																	<li  class="dd-item" data-level="3" data-id="<?= $a3['cli_id'] ?>">
																																		<div class="dd-handle" <?php if($d_client['cli_id'] == $a3['cli_id']) echo("style='color:#e31936;'");  ?> >
																																			<?=$a3['cli_code'] . " | " . $a3['cli_nom'] ?>
																												<?php 						if(!empty($a3['cli_reference'])): ?>
																																				<strong> | Référence : </strong> <?= $a3['cli_reference']; ?>
																												<?php 						endif; ?>
																												<?php 						if(!empty($a3['cli_adr_cp'])): ?>
																																				<strong> | </strong>  <?= $a3['cli_adr_cp']; ?> - <?= $a3['cli_adr_ville']; ?>
																												<?php 						endif; ?>
																																			<a class="pull-right btn-xs btn-info dd-nodrag" href="client_voir.php?client=<?= $a['cli_id'] ?>">
																																				<strong>Voir le client</strong>
																																			</a>
																												<?php						if($fct_fac_groupee){ ?>
																																				<div class="fac-groupee fac-groupee-<?=$a3['cli_id']?> pull-right btn-xs btn-default mr10" >
																												<?php								if(!empty($a3['fac_code'])){ ?>
																																						<div data-toggle="tooltip" title="Facturation groupée sur le compte <?=$a3['fac_code']?>." >
																																							<i class="fa fa-euro" ></i> <?=$a3['fac_code']?>
																																						</div>
																																		<?php		}  ?>
																																				</div>
																																<?php		}  ?>
																																		</div>
																																	</li>
																												<?php 			endif; ?>
																												<?php 		endforeach; ?>
																														</ol>
																										<?php 		endif; ?>
																												</li>
																				<?php 						endif;
																										endforeach; ?>
																									</ol>
																				<?php 			endif; ?>
																							</li>
																				<?php 	endif;
																					endforeach; ?>
																				</ol>
																<?php 		endif; ?>
																		</li>
														  <?php 	endforeach;
																endif; ?>
															</ol>
													<?php 	} ?>
														</li>
													</ol>
												</div>
											</div>
							<?php 		}
										// FIN DE TAB10 ?>

										<!-- DEBUT TAB GENERALE 1 -->
										<div id="tab1" class="tab-pane <?php if($onglet==1) echo("active"); ?>" >

											<div class="row">
												<div class="col-md-12">

													<h2 style="margin-top:0px;">Général</h2>
													<div class="panel panel-<?=$class_panel?>">
														<div class="panel-heading  panel-head-sm" <?php if(!empty($couleur)) echo("style='background-color:" . $couleur . ";border-color:" . $couleur . "'") ?> >
															<span class="panel-icon" <?php if(!empty($couleur_txt)) echo("style='color:" . $couleur_txt. "'") ?>>
																<i class="fa fa-file"></i>
															</span>
															<span class="panel-title" <?php if(!empty($couleur_txt)) echo("style='color:" . $couleur_txt. "'") ?>
															>Informations principales</span>
														</div>
														<div class="panel-body">
															<div class="row mb15" >
																<div class="col-md-3">
																	<strong>Code:</strong> <?= $d_client['cli_code']; ?>
																</div>
																<div class="col-md-3">
																	<strong>Nom:</strong> <?= $d_client['cli_nom']; ?>
																</div>
																<div class="col-md-3">
																	<strong>Référence interne du client :</strong>
															 <?php	if(!empty($d_client['cli_reference'])){
																		echo($d_client['cli_reference']);
																	}else{
																		echo("<i>Pas de Référence</i>");
																	} ?>
																</div>
																<div class="col-md-3">
																	<strong>Client référence :</strong>
															<?php 	if($d_client['cli_important'] == 1){
																		echo("Oui");
																	}else{
																		echo("Non");
																	} ?>
																</div>
															</div>
															<div class="row mb15" >
																<div class="col-md-3"><strong>Catégorie:</strong>
														<?php 		if(!empty($d_categorie['cca_libelle'])){
																		echo($d_categorie['cca_libelle']);
																	}else{
																		echo("<i>Pas de catégorie</i>");
																	} ?>
																</div>
																<div class="col-md-3">
																	<strong>Sous catégorie:</strong>
															<?php 	if(isset($d_sous_categorie)){
																		echo($d_sous_categorie['csc_libelle']);
																	}else{
																		echo("<i>Pas de Sous-catégorie</i>");
																	} ?>
																</div>
														<?php 	if($d_client['cli_categorie'] != 3): ?>
																	<div class="col-md-3">
																	  <strong>Siren: </strong> <?php if(empty($d_client['cli_siren'])): ?><i> Pas de siren</i><?php else: ?> <?= $d_client['cli_siren'] ?><?php endif; ?>
																	</div>
													  <?php		endif; ?>
																<div class="col-md-3">
															<?php 	if(empty($d_client['cli_first_facture'])){
																		echo("<strong>Statut: </strong> prospect validé");
																	}else{
																		echo("<strong>Statut: </strong> client validé");
																	}?>
																</div>
															</div>
													<?php	foreach($d_client_societe as $d_cli_soc){ ?>
																<div class="row mb15" >
																	<div class="col-md-3">
																		<strong>Commercial<?php echo(" " . $d_cli_soc["age_code"]); ?> :</strong>
																		<?=$d_cli_soc['com_label_1'] . " " . $d_cli_soc['com_label_2']?>
																	</div>
													<?php			if(!empty($d_cli_soc['cli_archive'])){ ?>
																		<div class="col-md-3 text-danger">Archivé</div>
													<?php			} ?>
																</div>
													<?php 	}
															if(!empty($d_client['cli_interco_soc'])){ ?>
																<div class="row mb15" >
																	<div class="col-md-3">
																		<strong>Réseau Si2P : </strong>OUI
																	</div>
																</div>
													<?php	} ?>

														</div>
													</div>

												</div>
											</div>

											<div class="row" >
												<div class="col-md-4" <?php if($d_client["cli_categorie"]==3) echo("style='display:none;'");?>>
													<!-- INTERVENTION -->
											<?php	if($d_client["cli_categorie"]!=3){ ?>
														<div class="panel panel-<?=$class_panel?>" >
															<div class="panel-heading panel-head-sm">
																<span class="panel-icon">
																	<i class="fa fa-home"></i>
																</span>
																<span class="panel-title">
																	Adresse du lieu d'intervention par défaut
														<?php		if(!empty($adresse_int) AND $drt_edit_adr){ ?>
																		<span class="pull-right">
																			<a href="client_adresse.php?client=<?=$client?>&adresse=<?=$adresse_int_id?>" class="btn btn-sm btn-warning">
																				<i class="fa fa-pencil"></i>
																			</a>
																		</span>
														<?php		} ?>
																</span>
															</div>
															<div class="panel-body">
														<?php 	if(empty($d_client["cli_adresse"])){ ?>
																	<div class="alert alert-warning" style="margin-bottom:0px;">
																		Pas d'adresse d'intervention
																	</div>
														<?php	}else{ ?>
																<?php	echo($adresse_int);
																} ?>
															</div>
														</div>
											<?php 	} ?>

												</div>

										<?php 	$class_md="col-md-4";
												if($d_client["cli_categorie"]==3){
													$class_md="col-md-6";
												} ?>
												<div class="<?=$class_md?>" >
													<div class="panel panel-<?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-home"></i>
															</span>
															<span class="panel-title">Adresse de facturation par défaut
													<?php		if(isset($adresse_fac_def) AND $drt_edit_adr){ ?>
																	<span class="pull-right">
																		<a href="client_adresse.php?client=<?=$client?>&adresse=<?=$adresse_fac_def["adr_id"]?>" class="btn btn-sm btn-warning" >
																			<i class="fa fa-pencil"></i>
																		</a>
																	</span>
													<?php		} ?>
															</span>
														</div>
														<div class="panel-body">
													<?php 	if(!isset($adresse_fac_def)){ ?>
																<div class="alert alert-warning" style="margin-bottom:0px;">
																	Pas d'adresse de facturation
																</div>
													<?php	}else{ ?>


													<?php		echo($adresse_fac);
																if($d_client['cli_categorie'] != 3){
																	if(!empty($d_client['cli_siren'])){
																		echo("<br>Siret: " . $d_client['cli_siren'] . " " . $adresse_fac_def['adr_siret']);
																	}
																}
															} ?>
														</div>
													</div>
												</div>

										<?php	if($aff_onglet_corresp){ ?>
													<div class="<?=$class_md?>" >
														<div class="panel panel-<?=$class_panel?>">
															<div class="panel-heading panel-head-sm">
																<span class="panel-icon">
																	<i class="fa fa-comments-o"></i>
																</span>
																<span class="panel-title">
																	Dernière correspondance
																	<span class="pull-right">
																<?php 	if(!empty($last_cor) && $_SESSION['acces']['acc_ref_id'] == $last_cor['cor_utilisateur']){ ?>
																			<a href="client_correspondance.php?client=<?= $d_client['cli_id'] ?>&id=<?= $last_cor['cor_id'] ?>" class="btn btn-sm btn-warning">
																				<i class="fa fa-pencil"></i>
																			</a>
																<?php 	} ?>
																	</span>
																</span>
															</div>
															<div class="panel-body">
													<?php 		if(isset($last_cor)){
																	$cor = get_utilisateur($last_cor['cor_utilisateur']); ?>
																	<ul>
																		<li>
																			Créée par <?php if(!empty($cor)){ ?><strong><?= $cor['uti_prenom'] ?> <?= $cor['uti_nom'] ?></strong><?php } ?>
																			le <strong><?= convert_date_txt($last_cor['cor_date']); ?></strong>
																		</li>
																		<li><strong>Correspondant: </strong>
																			  <?php if(!empty($last_cor['cor_contact'])):
																			  $con = get_contact($last_cor['cor_contact']);  ?>
																			  <?= $con['con_prenom']; ?> <?= $con['con_nom']; ?>
																			<?php else: ?>
																			  <?= $last_cor['cor_contact_nom']; ?>
																			<?php endif; ?>
																		</li>
																		<li><strong>Commentaires: </strong> <?= $last_cor['cor_commentaire']; ?></li>
																		<li><strong>Téléphone: </strong>
																			<?php if(!empty($last_cor['con_contact'])): ?>
																			  <?= $con['con_tel']; ?>
																			<?php else: ?>
																			  <?= $last_cor['cor_contact_tel']; ?>
																			<?php endif; ?>
																		</li>
																		<li><strong>Rappeler le:</strong> <?= convert_date_txt($last_cor['cor_rappeler_le']); ?></li>
																	</ul>
													<?php 		}else{ ?>
																	<div class="alert alert-warning">
																		Il n'y a pas encore de correspondance pour ce client.
																	</div>
													<?php 		} ?>
															</div>
														</div>
													</div>
										<?php	} ?>
											</div>

											<div class="row">
												<div class="col-md-4">
													<!-- CONTACT -->
													<div class="panel panel-<?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-map-marker"></i>
															</span>
															<span class="panel-title">
																Contact par défaut
												<?php 			if(!empty($contact_def) AND $drt_edit_con){ ?>
																	<span class="pull-right">
																		<a href="client_contact.php?client=<?=$client?>&contact=<?=$d_contact_def["con_id"]?>" class="btn btn-sm btn-warning">
																			<i class="fa fa-pencil"></i>
																		</a>
																	</span>
												<?php			} ?>
															</span>
														</div>
														<div class="panel-body">
												<?php 		if(!empty($contact_def)){
																echo($contact_def);
															}else{
																echo("<div class='alert alert-warning' style='margin-bottom:0px;' >");
																	echo("Pas de contact");
																echo("</div>");
															} ?>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="panel panel-<?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-file"></i>
															</span>
															<span class="panel-title">Informations complémentaires</span>
														</div>
														<div class="panel-body">

															<div class="row mb15">
																<div class="col-md-4">
																	<strong>Source: </strong>
													  <?php 		if(!empty($d_client['cli_prescripteur'])){

																		if($d_client['cli_prescripteur']!=-1){

																			$sql="SELECT cpr_libelle FROM Clients_Prescripteurs WHERE cpr_id=:prescripteur;";
																			$req=$Conn->prepare($sql);
																			$req->bindParam(":prescripteur",$d_client['cli_prescripteur']);
																			$req->execute();
																			$d_prescripteur=$req->fetch();
																			if(!empty($d_prescripteur)){
																				echo($d_prescripteur["cpr_libelle"]);
																			}
																		}else{
																			echo("Autre");
																		}
																	}else{
																		echo("<i>Pas de source</i>");
																	} ?>
																</div>
																<div class="col-md-4">
															 <?php 	if(!empty($d_client['cli_prescripteur']) AND !empty($d_client['cli_prescripteur_info'])){

																		if($d_client['cli_prescripteur']!=-1){
																			echo("<strong>". $d_prescripteur["cpr_libelle"] . ":</strong> " . $d_client['cli_prescripteur_info']);
																		}else{
																			echo("Autre: " . $d_client['cli_prescripteur_info']);
																		}
																	} ?>
																</div>

																<div class="col-md-4">
																	<strong>Code APE: </strong>
															<?php 	if(!empty($d_ape)){
																		echo($d_ape['ape_code']);
																	}else{
																		echo("<i>Pas de code APE</i>");
																	}?>
																</div>
															</div>
															<div class="row mb15">
																<div class="col-md-8">
																	<strong>Type de stagiaires:</strong>
														<?php 		foreach($base_type_stagiaire as $k => $n){
																		if($k == $d_client['cli_stagiaire_type']){
																			$stagiaire_type = $n;
																		}
																	}
																	if(empty($stagiaire_type) OR $stagiaire_type == "&nbsp;"){
																		echo("<i>Pas de type de stagiaire</i>");
																	}else{
																		echo($stagiaire_type);
																	} ?>
																</div>
														<?php	if($d_client["cli_categorie"]!=3){ ?>

																	<div class="col-md-4">
																		<strong>Téléphone (standard):</strong>
															<?php 		if(empty($d_client["cli_tel"])){
																			echo("<i>non renseigné</i>");
																		}else{
																			echo($d_client["cli_tel"]);
																		} ?>
																	</div>
														<?php	} ?>
															</div>
															<div class="row mb15">
																<div class="col-md-4">
																	<strong>Classification: </strong>
															<?php 	if(isset($d_classification)){
																		echo($d_classification['ccl_libelle']);
																	}else{
																		echo("<i>Pas de classification</i>");
																	} ?>
																</div>
																<div class="col-md-4">
																	<strong>Catégorie de classification :</strong>
															<?php 	if(isset($d_classification_categorie)){
																		echo($d_classification_categorie['ccc_libelle']);
																	}else{
																		echo("<i>Pas de catégorie de classification</i>");
																	} ?>
																</div>
																<div class="col-md-4">
																	<strong>Type de classification :</strong>
															<?php 	if(isset($d_classification_type)){
																		echo($d_classification_type['cct_libelle']);
																	}else{
																		echo("<i>Pas de type de classification</i>");
																	} ?>
																</div>
															</div>
															<div class="row mb15">
																<div class="col-md-6">
														  <?php 	$date_crea = convert_date_txt($d_client['cli_date_creation']); ?>
																	<strong>Créé le </strong> <?= $date_crea ?>
																</div>
																<div class="col-md-6">
														  <?php		$creator = get_utilisateur($d_client['cli_uti_creation']);
																	if(!empty($creator)){
																		echo("<strong>par </strong> " . $creator['uti_prenom'] . " " . $creator['uti_nom']);
																	} ?>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="col-md-2">
													<div class="panel panel-<?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-file"></i>
															</span>
															<span class="panel-title">Compta</span>
														</div>
														<div class="panel-body">
													<?php	if($d_client['cli_categorie']!=4){
																if($d_client['cli_facture_opca']==1){ ?>
																	<div class="col-md-12 mb15" >
																		<strong>Type de financeur:</strong>
																<?php 	if(!empty($d_opca_type)){
																			echo($d_opca_type['csc_libelle']);
																		}else{
																			echo("<i>Non renseigné</i>");
																		}?>
																	</div>
																	<div class="col-md-12 mb15" >
																		<strong>Financeur :</strong>
																<?php 	if(!empty($opca)){
																			echo($opca['cli_nom']);
																		}else{
																			echo("<i>Non renseigné</i>");
																		} ?>
																	</div>
														<?php 	}else{ ?>
																	<div class="col-md-12 mb15" >
																		<strong>Financeur :</strong>
																		<i>Aucun financeur</i>
																	</div>
														<?php 	}
															} ?>

															<div class="col-md-12" style="margin-bottom:15px;">
																<strong>Relevés de factures :</strong>
													  <?php 	if(empty($d_client['cli_releve'])){
																	echo("Non");
																}else{
																	echo("Oui");
																} ?>
															</div>
															<div class="col-md-12" style="margin-bottom:15px;">
																<strong>Identification TVA:</strong>
														  <?php if(empty($d_client['cli_ident_tva'])){
																	echo("<i>Pas d'identification TVA</i>");
																}else{
																	echo($d_client['cli_ident_tva']);
																} ?>
															</div>


													<?php 	if($d_client['cli_affacturable']!=1){  ?>
																<div class="col-md-12 mb15 text-danger" ><strong>Client NON AFFACTURABLE</strong></div>
													<?php		if(!empty($d_client['cli_affacturable_txt'])){
																	echo("<div class='col-md-12 mb15 text-danger' ><strong>" . $d_client['cli_affacturable_txt'] . "</strong></div>");
																}
															}elseif(!empty($d_client['cli_affacturage'])){ ?>
																<div class="col-md-12 mb15" ><strong>Client en affacturage</strong></div>
													<?php	}

															// FG Arret de l'affacturage au 01/04/2020
															//if($acc_societe==17 AND !empty($d_client['cli_affacturage_iban'])){ ?>
																<!--<div class="col-md-12 mb15" >
																	<strong>IBAN AFFACTURAGE :</strong>
																	<?=$d_client['cli_affacturage_iban']?>
																</div>-->
													<?php	//} ?>


															<div class="col-md-12" style="margin-bottom:15px;"><strong>Mode de règlement:</strong>
																  <?php if(empty($d_client['cli_reg_type'])): ?>
																	<i>Pas de mode de règlement</i>
																  <?php else: ?>
																   <?php foreach($base_reglement as $k => $n): ?>
																	<?php if($k > 0){ ?>
																	  <?php if($d_client['cli_reg_type'] == $k): ?><?= $n ?><?php endif; ?>
																	  <?php } ?>
																	<?php endforeach; ?>
																  <?php endif; ?>
															</div>
															<div class="col-md-12" style="margin-bottom:15px;">
																<strong>Type de règlement:</strong>

																  <?php if($d_client['cli_reg_formule'] == 1): ?>

																	Date + <?php if(!empty($d_client['cli_reg_nb_jour'])): ?><?= $d_client['cli_reg_nb_jour'] ?><?php else: ?>0<?php endif; ?> jours

																  <?php endif; ?>
																  <?php if($d_client['cli_reg_formule'] == 2): ?>
																	Date + 45j + fin de mois
																  <?php endif; ?>
																  <?php if($d_client['cli_reg_formule'] == 3): ?>
																	Date + fin de mois + 45j
																  <?php endif; ?>
																  <?php if($d_client['cli_reg_formule'] == 4): ?>
																	Date + <?php if(!empty($d_client['cli_reg_nb_jour'])): ?><?= $d_client['cli_reg_nb_jour'] ?><?php else: ?>0<?php endif; ?> jours + fin de mois + <?php if(!empty($d_client['cli_reg_fdm'])): ?><?= $d_client['cli_reg_fdm'] ?><?php else: ?>0<?php endif; ?> jours
																  <?php endif; ?>
															</div>
															<div class="col-md-12" style="margin-bottom:15px;">
																<strong>RIB :</strong>
														<?php	if(isset($d_rib)){
																	echo($d_rib["rib_nom"]);
																}else{
																	echo("Pas de RIB");
																} ?>
															</div>
															<div class="col-md-12" style="margin-bottom:15px;">
																<strong>Changement RIB :</strong>
														<?php 	if(empty($d_client_societe[0]["cli_rib_change"])){
																	echo("Non");
																}else{
																	echo("Oui");
																} ?>
															</div>
															<div class="col-md-12" style="margin-bottom:15px;">
																<strong>Blacklisté :</strong>
														<?php 	if(empty($d_client["cli_blackliste"])){
																	echo("Non");
																}else{
																	echo("Oui");
																} ?>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="row" >

												<div class="col-md-6">
													<div class="panel panel-<?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
															<i class="fa fa-folder-open"></i>
															</span>
															<span class="panel-title">5 derniers devis</span>
														</div>
														<div class="panel-body">
												<?php		if(!empty($d_devis)){ ?>
																<table class="table table-striped table-hover" >
																	<thead>
																		<tr class="info">
																			<th>Devis</th>
																			<th>Commercial</th>
																			<th>Date</th>
																			<th>HT</th>
																			<th>Fiabilité</th>
																			<th>Contact</th>
																		</tr>
																	</thead>
																	<tbody>
											<?php						foreach($d_devis as $devis){
																			if(!empty($devis["dev_date"])){
																				$DT_devis_date=date_create_from_format('Y-m-d',$devis["dev_date"]);
																				if(!is_bool($DT_devis_date)){
																					$devis_date=$DT_devis_date->format("d/m/Y");
																				}
																			} ?>
																			<tr>
																				<td>
																					<a href="devis_voir.php?devis=<?=$devis["dev_id"]?>" >
																						<?=$devis["dev_numero"]?>
																					</a>
																				</td>
																				<td><?=$devis["dev_com_identite"]?></td>
																				<td><?=$devis_date?></td>
																				<td class="text-right" ><?=$devis["dev_total_ht"]?> €</td>
																				<td class="text-center"><?=$devis["dev_esperance"]?> %</td>
																				<td><?=$devis["dev_con_identite"]?></td>
																			</tr>
											<?php						} ?>
																	</tbody>
																</table>
											<?php			}else{ ?>
																<div class="alert alert-danger">
																	Il n'y a pas de devis pour ce client.
																</div>
											<?php			} ?>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="panel panel-<?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-tasks"></i>
															</span>
															<span class="panel-title">5 prochaines actions</span>
														</div>
														<div class="panel-body" >
												<?php		if(!empty($d_actions)){ ?>
																<table class="table table-striped table-hover" >
																	<thead>
																		<tr class="info">
																			<th>Date de debut</th>
																			<th>Produit</th>
																			<th>Intervenant</th>
																			<th>CA</th>
																		</tr>
																	</thead>
																	<tbody>
											<?php						foreach($d_actions as $action){
																			if(!empty($action["act_date_deb"])){
																				$DT_act_date_deb=date_create_from_format('Y-m-d',$action["act_date_deb"]);
																				if(!is_bool($DT_act_date_deb)){
																					$act_date_deb=$DT_act_date_deb->format("d/m/Y");
																				}
																			} ?>
																			<tr>
																				<td><?=$act_date_deb?></td>
																				<td>
																					<a href="action_voir.php?action=<?=$action["act_id"]?>&action_client=<?=$action["acl_id"]?>" >
																						<?=$action["acl_pro_reference"]?>
																					</a>
																				</td>
																				<td><?=$action["int_label_1"]?></td>
																				<td><?=$action["acl_ca"]?>€</td>
																			</tr>
											<?php						} ?>
																	</tbody>
																</table>
											<?php			}else{ ?>
																<div class="alert alert-warning">
																	il n'y pas d'action pour ce client.
																</div>
											<?php			} ?>

														</div>
													</div>
												</div>
											</div>

										</div>


										<!-- DEBUT TAB DEVIS 2 -->
										<div id="tab2" class="tab-pane <?php if($onglet==2) echo("active"); ?>" >

											<h2>Historique devis</h2>

											<div class="row">
												<table class="table" >
													<thead>
														<tr class="dark">
											<?php			for($bcl=10;$bcl>=0;$bcl--){ ?>
																<th class="text-center" ><?=intval($exercice-$bcl)?></th>
											<?php			} ?>
														</tr>
													</thead>
													<tbody>
														<tr>
											<?php			for($bcl=10;$bcl>=0;$bcl--){ ?>
																<td class="text-center" >
																	<a href="client_devis.php?exercice=<?=intval($exercice-$bcl)?>&client=<?=$client?>" >
																		<?=number_format($devis_histo["client"][$bcl],2,","," ")?> €
																	</a>
																</td>
											<?php			} ?>
														</tr>
													</tbody>
												</table>
											</div>

											<h2>Devis <?=$exercice?> / <?=$exercice+1?></h2>

								<?php		if(!empty($d_devis_exe)){ ?>
												<div class="table-responsive">
													<table class="table table-striped table-hover" id="table_id">
														<thead>
															<tr class="dark" >
																<th>ID</th>
																<th>Numéro</th>
																<th>Date</th>
																<th>Produit</th>
																<th>Montant HT</th>
																<th class="text-center" colspan="2" >Formation</th>
																<th>Etat fiabilité</th>
															</tr>
														</thead>
														<tbody>

													<?php	$total_ht=0;

															foreach($d_devis_exe as $d){
																if(empty($d['dev_bordereau'])){
																	$total_ht=$total_ht+$d['dli_montant_ht'];
																} ?>
																<tr>
																	<td><?=$d['dev_id']?></td>
																	<td>
																		<a href="devis_voir.php?devis=<?=$d['dev_id']?>" >
																			<?=$d['dev_numero']?>
																		</a>
																	</td>
																	<td><?=$d['dev_date_aff']?></td>
																	<td><?=$d['dli_reference']?></td>
																	<td class="text-right" ><?=number_format($d['dli_montant_ht'],2,","," ")?></td>
														<?php		if(empty($d['dli_action'])){
																		echo("<td class='text-center' colspan='2' >Pas de formation</td>");
																	}elseif(empty($d['dli_action_client'])){ ?>
																		<td class="text-center" >
																			N° <?=$d["dli_action"]?>
																		</td>
																		<td class="text-center" >
																			<a href="action_client_enr.php?devis=<?=$d["dli_id"]?>&action=<?=$d["dli_action"]?>&auto=1" class="btn btn-success btn-sm" >
																				<i class="fa fa-plus" ></i>
																			</a>
																		</td>
														<?php		}else{ ?>
																		<td class="text-center" >
																			N° <?=$d["dli_action"]?>
																		</td>
																		<td>
																			<a href="action_voir.php?action=<?=$d["dli_action"]?>&action_client=<?=$d["dli_action_client"]?>" class="btn btn-info btn-sm" >
																				<i class="fa fa-eye" ></i>
																			</a>
																		</td>
														<?php		}
																	switch ($d['dev_esperance']){
																		case 0:
																			echo("<td class='bg-dark' >Sans suite</td>");
																			break;
																		case 30:
																			echo("<td class='bg-danger' >30%</td>");
																			break;
																		case 50:
																			echo("<td class='bg-warning' >50%</td>");
																			break;
																		case 90:
																			echo("<td class='bg-success' >90%</td>");
																			break;
																		case 100:
																			echo("<td class='bg-info' >Commandé</td>");
																			break;
																		default:
																			echo("<td>&nbsp;</td>");
																	}	?>
																</tr>
													<?php 	} 	?>
															<tr>
																<td colspan="4" class="text-right" >Total :</td>
																<td class="text-right" ><?=number_format($total_ht,2,","," ")?></td>
																<td colspan="2" >&nbsp;</td>
															</tr>
														</tbody>
													</table>
												</div>
							<?php 			}else{ ?>
												<div class="col-md-12 text-center" style="padding:0;" >
													<div class="alert alert-warning" style="border-radius:0px;">
														Aucun devis sur l'exercice <?=$exercice?> / <?=$exercice+1?>
													</div>
												</div>
							<?php 			} ?>

										</div>

										<!-- DEBUT TAB FACTURE 3 -->
										<div id="tab3" class="tab-pane <?php if($onglet==3) echo("active"); ?>" >

											<h2>Historique de facturation</h2>

											<div class="row">
												<table class="table" >
													<thead>
														<tr class="dark">
											<?php			for($bcl=10;$bcl>=0;$bcl--){ ?>
																<th class="text-center" ><?=intval($exercice-$bcl)?></th>
											<?php			} ?>
														</tr>
													</thead>
													<tbody>
														<tr>
											<?php			for($bcl=10;$bcl>=0;$bcl--){ ?>
																<td class="text-center" >
																	<a href="client_facture.php?exercice=<?=intval($exercice-$bcl)?>&client=<?=$client?>" >
																		<?=number_format($facture_histo["client"][$bcl],2,","," ")?> €
																	</a>
																</td>
											<?php			} ?>
														</tr>
													</tbody>
												</table>
											</div>

											<h2>Facturation <?=$exercice?> / <?=$exercice+1?></h2>
								<?php		if(!empty($d_facture_exe)){ ?>
												<table class="table table-striped table-hover" >
													<thead>
														<tr class="dark2" >
															<th>Chrono</th>
															<th>Commercial</th>
															<th>Facture</th>
															<th>Date</th>
															<th class="text-right" >H.T.</th>
															<th class="text-right" >T.T.C.</th>
															<th class="text-right" >Réglé</th>
															<th class="text-right" >Reste Dû</th>
															<th class="text-center" >Date prévue de réglement</th>
														</tr>
													</thead>
													<tbody>
											<?php		$total_ht=0;
														$total_ttc=0;
														$total_regle=0;
														$total_reste_du=0;

														foreach($d_facture_exe as $f){

															$fac_date="";
															if(!empty($f['fac_date'])){
																$dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
																$fac_date=$dt_fac_date->format("d/m/Y");
															}
															$fac_date_reg_prev="";
															if(!empty($f['fac_date_reg_prev'])){
																$dt_fac_date_reg_prev=date_create_from_format("Y-m-d",$f['fac_date_reg_prev']);
																$fac_date_reg_prev=$dt_fac_date_reg_prev->format("d/m/Y");
															}
															$reste_du=$f['fac_total_ttc']-$f['fac_regle'];

															$total_ht+=$f['fac_total_ht'];
															$total_ttc+=$f['fac_total_ttc'];
															$total_regle+=$f['fac_regle'];
															$total_reste_du+=$reste_du; ?>
															<tr>
																<td><?= $f['fac_chrono'] ?></td>
																<td><?= $f['com_label_1'] ?></td>
																<td><?= $f['fac_numero'] ?></td>
																<td><?=	$fac_date?></td>
																<td class="text-right" ><?=$f['fac_total_ht']?></td>
																<td class="text-right" ><?=$f['fac_total_ttc']?></td>
																<td class="text-right" ><?= $f['fac_regle']?></td>
																<td class="text-right" ><?=$reste_du?></td>
																<td class="text-center" ><?=$fac_date_reg_prev?></td>
															</tr>
								<?php					}  ?>
													</tbody>
													<tfoot>
														<tr>
															<th colspan="4" class="text-right" >Total :</th>
															<td class="text-right" ><?=$total_ht?></td>
															<td class="text-right" ><?=$total_ttc?></td>
															<td class="text-right" ><?=$total_regle?></td>
															<td class="text-right" ><?=$total_reste_du?></td>
															<td>&nbsp;</td>
														</tr>
													</tfoot>
												</table>
								<?php 		}else{ ?>
												<div class="col-md-12 text-center" style="padding:0;" >
													<div class="alert alert-warning" style="border-radius:0px;">
														Aucune facture sur l'exercice <?=$exercice?> / <?=$exercice+1?>.
													</div>
												</div>
							<?php 			} ?>
										</div>

										<!-- DEBUT TAB PLANNING 4 -->
										<div id="tab4" class="tab-pane">
									<?php	if($d_societe["soc_intra_inter"]!=1){ ?>

												<h2 style="margin-top:0px;">Planning INTER</h2>

												<div class="row mb25" >
													<div class="col-md-10 col-md-offset-2">
														<div class="form-group col-md-3">
															<select class="select2 select2-famille" id="pla_inter_fam" >
																<option value="0" >Famille...</option>
													<?php		if(!empty($famille_produit)){
																	foreach($famille_produit as $cf => $df){
																		echo("<option value='" . $cf . "' >" . $df["pfa_libelle"] . "</option>");
																	}
																} ?>
															</select>
														</div>
														<div class="form-group col-md-3">
															<select class="select2 select2-famille-sous-famille" id="pla_inter_s_fam" >
																<option value="0" >Sous-Famille...</option>
															</select>
														</div>
														<div class="form-group col-md-3">
															<select class="select2 select2-famille-sous-sous-famille" id="pla_inter_s_s_fam" >
																<option value="0" >Sous-Sous-Famille...</option>
															</select>
														</div>
														<div class="col-sm-3">
															<button type="button" class="btn btn-primary" id="aff_inter" >Afficher le planning</button>
														</div>
													</div>
												</div>
												<div class="row mb25" style="display:none;" id="planning_inter"  >
													<div class="col-md-12">
														<div class="table-responsive" >
															<table class="table table-striped table-hover" >
																<thead>
																	<tr class="dark">
																		<th>Date</th>
																		<th>Produit</th>
																		<th>Texte planning</th>
																		<th>Intervenant</th>
																		<th>Lieu</th>
																		<th>Inscription</th>
																		<th>Devis</th>
																	</tr>
																</thead>
																<tbody id="planning_inter_ligne" >
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<div class="row mb25" style="display:none;" id="planning_no_inter"  >
													<p class="alert alert-warning" >Aucune formation ne correspond à votre recherche.</p>
												</div>

									<?php	} ?>

											<h2 style="margin-top:0px;">Planning du client</h2>
									<?php 	if(empty($actions)){  ?>
												<div class="alert alert-warning">
													Il n'y a pas d'action pour ce client.
												</div>
									<?php 	}else{ ?>
												<div class="row">
													<table class="table table-striped table-hover" >
														<thead>
															<tr class="dark">
																<th>Action</th>
																<th>Produit</th>
																<th>Libelle</th>
																<th>Intervenant</th>
																<th>Date de début</th>
																<th>Lieu</th>
																<th>Date de confirmation</th>
																<th>CA</th>
															</tr>
														</thead>
														<tbody>
												<?php 		foreach ($actions as $a) { ?>
																<tr>
																	<td><?= $a['acl_action']; ?></td>
																	<td><?= $a['acl_pro_reference'] ?></td>
																	<td><?= $a['acl_pro_libelle'] ?></td>
																	<td><?= $a['int_label_1'] ?></td>
																	<td><?= $a['act_date_deb_fr'] ?></td>
																	<td><?= $a['act_adr_ville'] ?></td>
																	<td>
															<?php 		if(!empty($a['acl_confirme_date'])): ?>
																			<?= convert_date_txt($a['acl_confirme_date']); ?>
															<?php 		else: ?>
																			<i>Non confirmée</i>
															<?php 		endif; ?>
																	</td>
																	<td><?= $a['acl_ca'] ?></td>
																</tr>
												  <?php  	} ?>
														</tbody>
													</table>
												</div>
								<?php 		} ?>
										</div>
										<!-- FIN  TAB ACTIONS -->


										<!-- TAB5 correspondances -->
							<?php		if($aff_onglet_corresp){ ?>
											<div id="tab5" class="tab-pane <?php if($onglet==5) echo("active"); ?>" >
												<h2 style="margin-top:0px;">Correspondances</h2>
												<div class="admin-form" >
													<table class="table table-striped table-hover" >
														<thead>
															<tr class="dark">
																<th>ID</th>
																<th>Date</th>
																<th>Enregistré par</th>
																<th>Méthode</th>
																<th>Raison</th>
																<th>Correspondant</th>
																<th>Tel</th>
																<th>Commentaires</th>
																<th>Rappeler le</th>
																<th>Rappel traité</th>
																<th>Devis</th>
																<th>Actions</th>
															</tr>
														</thead>
												<?php 	if(!empty($correspondances)){ ?>
															<tbody>
													<?php 		foreach($correspondances as $c){

																	$cor_date_aff="";
																	if(!empty($c['cor_date'])){
																		$cor_date=date_create_from_format('Y-m-d',$c['cor_date']);
																		$cor_date_aff=$cor_date->format("d/m/Y");
																	}
																	$cor_rappeler_le_aff="";
																	if(!empty($c['cor_rappeler_le'])){
																		$cor_rappeler_le=date_create_from_format('Y-m-d',$c['cor_rappeler_le']);
																		$cor_rappeler_le_aff=$cor_rappeler_le->format("d/m/Y");
																	}

																	$cor_auteur="";
																	$cor_responsable=0;
																	if(!empty($c['cor_utilisateur'])){
																		if(isset($utilisateurs[$c['cor_utilisateur']])){
																			$cor_auteur=$utilisateurs[$c['cor_utilisateur']]["identite"];
																			$cor_responsable=$utilisateurs[$c['cor_utilisateur']]["responsable"];
																		}
																	}

																	$raison_lib=$c['cra_libelle'];
																	if($c['cor_raison']==-1){
																		$raison_lib="Autre: " . $c['cor_raison_info'];
																	}

																	?>
																	<tr class="correspondance" id="correspondance_<?=$c['cor_id']?>" >
																		<td><?= $c['cor_id']; ?></td>
																		<td><?= $cor_date_aff ?></td>
																		<td><?= $cor_auteur ?></td>
																		<td><?= $c['cty_libelle']?></td>
																		<td><?= $raison_lib?></td>
																		<td><?= $c['cor_contact_prenom'] . " " .$c['cor_contact_nom']?></td>
																		<td><?= $c['cor_contact_tel']?></td>
																		<td  style="width:20%;"><?= $c['cor_commentaire']?></td>
																		<td><?=$cor_rappeler_le_aff?></td>
																<?php	if($c['cor_utilisateur']==$acc_utilisateur OR $cor_responsable==$acc_utilisateur){ ?>
																			<td class="text-left" >
																				<label class="option">
																					<input type="checkbox" id="corresp_rappel_<?=$c['cor_id']?>" class="corresp-rappel" data-corresp="<?=$c['cor_id']?>" <?php if($c['cor_rappel']==1) echo("checked"); ?> />
																					<span class="checkbox"></span>
																				</label>
																			</td>
																<?php	}else{ ?>
																			<td class="text-left" >
																<?php			if($c['cor_rappel']==1){
																					echo("OUI");
																				}else{
																					echo("NON");
																				} ?>
																			</td>
																<?php	} ?>
																		<td>
																	<?php	if(!empty($c['cor_devis'])){
																				echo($c['cor_devis_num']);
																			}else{
																				echo("&nbsp;");
																			} ?>
																		</td>
																<?php	if($c['cor_utilisateur']==$acc_utilisateur OR $cor_responsable==$acc_utilisateur OR $_SESSION['acces']['acc_service'][5]== 1){ ?>
																			<td>
																				<a href="client_correspondance.php?client=<?= $d_client['cli_id'] ?>&id=<?=$c['cor_id']?>&tab5" class="btn btn-warning btn-sm">
																					<i class="fa fa-pencil"></i>
																				</a>
																				<a href="#" class="btn btn-danger btn-sm corresp-supp" data-corresp="<?= $c['cor_id'] ?>">
																					<i class="fa fa-times"></i>
																				</a>
																			</td>
																<?php	}else{ ?>
																			<td>&nbsp;</td>
																<?php	} ?>
																	</tr>
													<?php 		} ?>
															</tbody>
												<?php	} ?>
													</table>
												</div>
												<div class="alert alert-warning" id="no_corresp" <?php if(!empty($correspondances)) echo("style='display:none;'")?> >
													Il n'y a pas de correspondance pour ce client
												</div>
											</div>
							<?php		} ?>
										<!-- fin correspondances -->


										<!-- DEBUT TAB PRODUIT 13 -->
								<?php	if($aff_onglet_produit){ ?>
											<div id="tab13" class="tab-pane <?php if($onglet==13) echo("active"); ?>" >

												<h2 style="margin-top:0px;">Produits</h2>

												<div class="col-md-12 admin-form">

													<div class="row" style="margin-bottom:25px;">
														<div class="col-md-12">
															<div class="form-group col-md-2">
																<select class="select2" id="pro_filtre_categorie" >
																	<option value="0" >Catégorie...</option>
															<?php	if(!empty($categorie_produit)){
																		foreach($categorie_produit as $fp_k => $fp_v){
																			echo("<option value='" . $fp_k . "' >" . $fp_v["pca_libelle"] . "</option>");
																		}
																	} ?>
																</select>
															</div>
															<div class="form-group col-md-2">
																<select class="select2 select2-famille" id="pro_filtre_famille" >
																	<option value="0">Famille...</option>
															<?php	if(!empty($famille_produit)){
																		foreach($famille_produit as $fp_k => $fp_v){
																			echo("<option value='" . $fp_k . "' >" . $fp_v["pfa_libelle"] . "</option>");
																		}
																	} ?>
																</select>
															</div>
															<div class="form-group col-md-2">
																<select class="select2 select2-famille-sous-famille" id="pro_filtre_sous_famille" >
																	<option value="0">Sous-famille ...</option>
															<?php	if(!empty($sous_famille_produit)){
																		foreach($sous_famille_produit as $fp_k => $fp_v){
																			echo("<option value='" . $fp_k . "' >" . $fp_v["psf_libelle"] . "</option>");
																		}
																	} ?>
																</select>
															</div>
															<div class="form-group col-md-2">
																<select class="select2 select2-famille-sous-sous-famille" id="pro_filtre_sous_sous_famille" >
																	<option value="0">Sous-Sous-famille ...</option>
															<?php	if(!empty($sous_famille_produit)){
																		foreach($sous_sous_famille_produit as $fp_k => $fp_v){
																			echo("<option value='" . $fp_k . "' >" . $fp_v["pss_libelle"] . "</option>");
																		}
																	} ?>
																</select>
															</div>
															<div class="form-group col-md-2 pt10">
																<div class="option-group field">
																	<label class="option option-dark">
																		<input type="checkbox" id="pro_filtre_archive" value="on">
																		<span class="checkbox"></span> Archivés
																	</label>
																</div>

															</div>
															<div class="col-sm-2">
																<button type="button" id="filtre_produit" class="btn btn-primary">Filtrer</button>
																<button type="button" id="raz_produit" class="btn btn-info">Remettre à zéro</button>
															</div>
														</div>
													</div>

													<div class="row">
											<?php		if(!empty($produits)){ ?>
															<form method="post" action="devis_enr.php" id="form_devis_add" >
																<div>
																	<!--
																	hotfix/devis-duplicate-id
																	<input type="hidden" id="id_heracles" name="id_heracles" value="0" />
																	-->
																	<input type="hidden" name="dev_client" value="<?=$client?>" />
																	<input type="hidden" name="auto" value="1" />
																</div>
																<table class="table" >
																	<thead>
																		<tr class="dark">
																			<th class="text-center" >Référence</th>
																			<th class="text-center" >Libellé</th>
																			<th class="text-center" >HT INTRA</th>
																			<th class="text-center" >
																				Devis INTRA
																				<input type="checkbox" name="devis_intra_multi" value="on" id="intra_multi_all" class="check-all" >
																			</th>
																			<th class="text-center" >HT INTER</th>
																			<th class="text-center" >
																				Devis INTER
																				<input type="checkbox" name="devis_inter_multi" value="on" id="inter_multi_all" class="check-all" >
																			</th>
																			<th class="text-center" >Texte devis</th>
																			<th class="text-center" >Commentaire</th>
																			<th class="text-center" >Dérog.</th>
																	<?php 	if($d_client["cli_categorie"]==2){ ?>
																				<th class="text-center" >Alerte GC</th>
																				<th class="text-center" >Confirmation GC</th>
																				<th class="text-center" >CN</th>
																	<?php 	}
																			if($d_categorie["cca_gc"]){ ?>
																				<th class="text-center" >Validé</th>
																	<?php 	} ?>
																			<th class="text-center" >Action</th>

																		</tr>
																	</thead>
																	<tbody>
														<?php			foreach($produits as $p){

																			$drt_edit=true;
																			if(!$_SESSION['acces']["acc_droits"][7]){
																				$drt_edit=false;
																			}elseif($d_client["cli_categorie"]==2 AND !$_SESSION["acces"]["acc_droits"][8]){
																				$drt_edit=false;
																			}elseif($d_categorie["cca_gc"] AND $p['cpr_valide'] AND !$_SESSION["acces"]["acc_droits"][8]){
																				$drt_edit=false;
																			}

																			$prix_intra="";
																			$prix_inter="";

																			if($p['cpr_intra']){
																				$prix_intra=$p['cpr_ht_intra'] . " €";
																			}
																			if($p['cpr_inter']){
																				$prix_inter=$p['cpr_ht_inter'] . " €";
																			}

																			$txt_devis="&nbsp;";
																			if(!empty($p['cpr_devis_txt'])){
																				$txt_devis=$p['cpr_devis_txt'];
																			}
																			?>
																			<tr class="produit_client produit_<?=$p['pro_id']?> pro_cli_cat_<?=$p['pro_categorie'];?> pro_cli_fam_<?=$p['pro_famille'];?> pro_cli_sous_fam_<?=$p['pro_sous_famille'];?> pro_cli_sous_sous_fam_<?=$p['pro_sous_sous_famille'];?> pro_archive_<?=$p['pro_archive']?>" style="border-top:1px;<?php if($p['pro_archive']==1) echo("display:none;");?>" >
																				<td>
																		<?php		echo($p['pro_code_produit']); ?>

																				</td>
																				<td><?=$p['cpr_libelle']?></td>
																				<td class="text-right" ><?=$prix_intra?></td>
																				<td class="text-center" >
																		<?php	if(!$p['pro_archive'] AND $p['cpr_intra'] AND ( ($d_categorie["cca_gc"] AND ($p['cpr_valide'] OR $_SESSION["acces"]["acc_droits"][8])) OR (!$d_categorie["cca_gc"]) ) ){ ?>
																					<div class="option-group field">
																						<label class="option option-dark">
																							<input type="checkbox" name="dev_produit_intra[]" value="<?=$p['pro_id']?>" class="intra-multi" >
																							<span class="checkbox"></span>
																						</label>
																					</div>
																		<?php	} ?>
																				</td>
																				<td class="text-right" ><?=$prix_inter?></td>
																				<td class="text-center" >
																			<?php	if(!$p['pro_archive'] AND $p['cpr_inter'] AND ( ($d_categorie["cca_gc"] AND ($p['cpr_valide'] OR $_SESSION["acces"]["acc_droits"][8])) OR (!$d_categorie["cca_gc"]) ) ){ ?>
																						<div class="option-group field">
																							<label class="option option-dark">
																								<input type="checkbox" name="dev_produit_inter[]" value="<?=$p['pro_id']?>" class="inter-multi" >
																								<span class="checkbox"></span>
																							</label>
																						</div>
																			<?php 	} ?>
																				</td>
																				<td><?=$txt_devis?></td>
																				<td><?=$p['cpr_comment_txt']?></td>
																		<?php	if($p['cpr_derogation']==2){ ?>
																					<td class="text-danger" >Niv. 2</td>
																		<?php	}elseif($p['cpr_derogation']==1){ ?>
																					<td class="text-warning" >Niv. 1</td>
																		<?php	}else{
																					echo("<td>&nbsp;</td>");
																				}
																				if($d_client["cli_categorie"]==2){ ?>
																					<td class="text-right" >
																		<?php			if(!empty($p['cpr_alerte_jour'])){
																							echo($p['cpr_alerte_jour'] . " jours");
																						}?>
																					</td>
																					<td class="text-center" >
																		<?php			if($p['cpr_confirmation_gc']==1){
																							echo("<i class='fa fa-check-square-o' ></i>");
																						}else{
																							echo("<i class='fa fa-square-o' ></i>");
																						}?>
																					</td>
																					<td class="text-center" >
																		<?php			if($p['cpr_cn']==1){
																							echo("<i class='fa fa-check-square-o' ></i>");
																						}else{
																							echo("<i class='fa fa-square-o' ></i>");
																						}?>
																					</td>
																		<?php 	}
																				if($d_categorie["cca_gc"]){ ?>
																					<td class="text-center" >
																		<?php			if($p['cpr_valide']==1){
																							echo("<i class='fa fa-check-square-o' ></i>");
																						}else{
																							echo("<i class='fa fa-square-o' ></i>");
																						}?>
																					</td>
																		<?php 	}?>
																				<td class="text-center" >
																					<a href="client_produit_histo.php?client=<?=$client?>&produit=<?=$p['pro_id']?>" class="btn btn-sm btn-default" >
																						<i class="fa fa-calendar"></i>
																					</a>
																		<?php		if($drt_edit){
																						if(!$p['pro_archive']){ ?>
																							<a href="client_produit.php?client=<?=$client?>&produit=<?=$p['pro_id']?>" class="produit-maj btn btn-sm btn-warning" >
																								<i class="fa fa-pencil"></i>
																							</a>
																		<?php			}
																						if(empty($p['pro_client'])){ ?>
																							<button type="button" class="produit-supp btn btn-sm btn-danger" data-produit="<?=$p['pro_id']?>" >
																								<i class="fa fa-times"></i>
																							</button>
																		<?php			}
																					}?>
																				</td>

																			</tr>
														<?php			} ?>
																	</tbody>
																</table>
															</form>
															<div id="no_result_produit" class="hidden alert alert-warning">
																Pas de produit correspondant à votre recherche.
															</div>

												<?php	}else{ ?>
															<div class="alert alert-warning">Pas de produit pour ce client.</div>
												<?php 	} ?>
													</div>
												</div>
											</div>
								<?php	} ?>
										<!-- fin tab13 produits -->


										<!-- infos -->
										<div id="tab6" class="tab-pane<?php if($onglet==6) echo(" active"); ?>" >
											<h2 style="margin-top:0px;">Infos</h2>
											<div class="row" style="margin-bottom:25px;">
												<div class="col-md-12">

													<div class="form-group col-md-3">
														<select class="form-control" id="info_filtre_type">
															<option value="0">Type d'info...</option>
												<?php   	if(!empty($d_info_type)){
																foreach($d_info_type as $ci => $di){
																	echo("<option value='" . $ci . "' >" . $di . "</option>");
																}
															} ?>
														</select>
													</div>
													<div class="form-group col-md-2">
														<select class="select2 select2-produit-fam select2-famille" id="info_filtre_famille" >
															<option value="0" >Famille...</option>
												<?php		if(!empty($famille_produit)){
																foreach($famille_produit as $cf => $df){
																	echo("<option value='" . $cf . "' >" . $df["pfa_libelle"] . "</option>");
																}
															} ?>
														</select>
													</div>
													<div class="form-group col-md-2">
														<select class="select2 select2-produit-sous-fam select2-famille-sous-famille" id="info_filtre_sous_famille" >
															<option value="0" >Sous-Famille...</option>
														</select>
													</div>
													<div class="form-group col-md-2">
														<select class="select2 select2-produit-sous-sous-fam select2-famille-sous-sous-famille" id="info_filtre_sous_famille" >
															<option value="0" >Sous-Sous-Famille...</option>
														</select>
													</div>
													<div class="col-sm-3">
														<button type="button" class="btn btn-primary" id="filtre_info" >Filtrer</button>
														<button type="button" class="btn btn-info" id="raz_info" >Remettre à zéro</button>
													</div>
												</div>
											</div>
											<div class="row">
												<table class="table table-striped table-hover" >
													<thead>
														<tr class="dark">
															<th>Type</th>
															<th>Description</th>
															<th>Famille</th>
															<th>Sous-Famille</th>
															<th>Sous-Sous-Famille</th>
															<th>Auteur</th>
															<th>Date</th>
												<?php		if($drt_edit_autre){ ?>
																<th>Actions</th>
												<?php		} ?>
														</tr>
													</thead>
									<?php 			if(!empty($infos)){ ?>
														<tbody>
									<?php					foreach($infos as $in){

																$type_info="";
																if(!empty($in['inf_type'])){
																	$type_info=$d_info_type[$in['inf_type']];
																}
																$info_famille="";
																if(!empty($in['inf_famille'])){
																	$info_famille=$famille_produit[$in['inf_famille']]["pfa_libelle"];
																}
																$info_sous_famille="";
																if(!empty($in['inf_sous_famille'])){
																	$info_sous_famille=$sous_famille_produit[$in['inf_sous_famille']]["psf_libelle"];
																}
																$info_sous_sous_famille="";
																if(!empty($in['inf_sous_sous_famille'])){
																	$info_sous_sous_famille=$sous_sous_famille_produit[$in['inf_sous_sous_famille']]["pss_libelle"];
																}
																$info_auteur="";
																if(!empty($in['inf_auteur'])){
																	$info_auteur=$utilisateurs[$in['inf_auteur']]["identite"];
																}

																$classe="info-client info-cli-type-" . $in['inf_type'] . " info-cli-fam-" . $in['inf_famille'] . " info-cli-sous-fam-" . $in['inf_sous_famille'] . "  info-cli-sous-sous-fam-" . $in['inf_sous_famille']; ?>
																<tr id="ligne_info_<?=$in['inf_id']?>" class="<?=$classe?>" >
																	<td><?=$type_info?></td>
																	<td><?= $in['inf_description']; ?></td>
																	<td><?=$info_famille?></td>
																	<td><?= $info_sous_famille; ?></td>
																	<td><?= $info_sous_sous_famille; ?></td>
																	<td><?= $info_auteur?></td>
																	<td><?= convert_date_txt($in['inf_date']); ?></td>
														<?php		if($drt_edit_autre){
																		if($in['inf_client']==$d_client["cli_id"]){ ?>
																			<td>
																				<a href="client_info_mod.php?client=<?= $d_client['cli_id'] ?>&info=<?= $in['inf_id'] ?>" class="btn btn-warning btn-sm">
																					<i class="fa fa-pencil"></i>
																				</a>
																				<button type="button" class="btn btn-danger btn-sm info-supp" data-info="<?= $in['inf_id']?>" >
																					<i class="fa fa-times"></i>
																				</button>
																			</td>
															<?php 		}else{ ?>
																			<td>&nbsp;</td>
															<?php		}
																	} ?>
																</tr>
									<?php 					} ?>
														</tbody>
									<?php			} ?>
												</table>
												<div id="no_result_info" class="hidden alert alert-warning">
													Pas d'info correspondant à votre recherche.
												</div>
											</div>
										</div>
										<!-- fin infos -->

										<!-- documents -->
								<?php	if($aff_onglet_doc){ ?>
											<div id="tab7" class="tab-pane <?php if($onglet==7) echo("active"); ?>" >

												<div class="row">
													<div class="col-md-6">
														<h2 style="margin-top:0px;">Documents Etech</h2>
												<?php 	if(!empty($docs_etech)){ ?>
															<table class="table table-striped table-hover">
																<thead>
																	  <tr class="dark">
																		<th>Nom</th>
																		<th>Fichier</th>
																		<th class="text-center">Télécharger</th>
																	  </tr>
																</thead>
																<tbody>

														  <?php 	foreach($docs_etech as $etech){ ?>
																		<tr>
																			<td><?= $etech['doc_nom_aff']; ?></td>
																			<td><?= $etech['doc_nom']; ?></td>
																			<td class="text-center">
																				<a href="documents/etech/<?= $etech['doc_nom']; ?>" class="btn btn-sm btn-warning" download >
																					<i class="fa fa-download" aria-hidden="true"></i>
																				</a>
																			</td>
																		</tr>
														  <?php 	} ?>
																</tbody>
															</table>
												<?php	}else{ ?>
															<div class="row">
																<div class="col-md-12">
																	<div class="alert alert-warning">Il n'y a pas de document ETECH pour ce client.</div>
																</div>
															</div>
												<?php	} ?>
													</div>
													<div class="col-md-6">

														<h2 style="margin-top:0px;">Documents du client</h2>

														<table class="table table-striped table-hover" >
															<thead>
																  <tr class="dark">
																	<th>Nom</th>
																	<th>Type</th>
																	<th>Famille</th>
																	<th>Sous-famille</th>
																	<th>Sous-Sous-famille</th>
																	<th class="text-center">Télécharger</th>
															<?php	if($drt_edit_autre){ ?>
																		<th>Actions</th>
															<?php	} ?>
																  </tr>
															</thead>
													<?php 	if(!empty($d_doc_client)){ ?>
																<tbody>
														<?php 		if(!empty($d_doc_client)){ ?>
														  <?php 		foreach($d_doc_client as $dc){

																			$doc_famille="";
																			if(!empty($dc["cdo_pro_famille"])){
																				$doc_famille=$famille_produit[$dc["cdo_pro_famille"]]['pfa_libelle'];
																			};
																			$doc_sous_famille="";
																			if(!empty($dc["cdo_pro_sous_famille"])){
																				$doc_sous_famille=$sous_famille_produit[$dc["cdo_pro_sous_famille"]]['psf_libelle'];
																			};
																			$doc_sous_sous_famille="";
																			if(!empty($dc["cdo_pro_sous_sous_famille"])){
																				$doc_sous_sous_famille=$sous_sous_famille_produit[$dc["cdo_pro_sous_sous_famille"]]['pss_libelle'];
																			};
																			?>

																			<tr id="document_<?=$dc['cdo_id']?>" >
																				<td><?= $dc['cdo_nom']; ?></td>
																				<td><?= $dc['cdt_libelle'] ?></td>
																				<td><?=$doc_famille;?></td>
																				<td><?=$doc_sous_famille?></td>
																				<td><?=$doc_sous_sous_famille?></td>
																				<td class="text-center">
																					<a href="documents/clients/client<?=$dc['cdo_client']?>/doc<?=$dc['cdo_id']; ?>.<?= $dc['cdo_ext']; ?>" class="btn btn-sm btn-warning" download="<?=$dc['cdo_nom'] . "." . $dc['cdo_ext'] ?>" >
																						<i class="fa fa-download" aria-hidden="true"></i>
																					</a>
																				</td>
																		<?php	if($drt_edit_autre){ ?>
																					<td class="text-center">
																						<a href="client_doc.php?client=<?=$dc['cdo_client']?>&document=<?=$dc['cdo_id']?>" class="btn btn-sm btn-warning">
																							<i class="fa fa-pencil" aria-hidden="true"></i>
																						</a>
																						<a href="#" data-document="<?=$dc['cdo_id']?>" class="btn btn-sm btn-danger doc-supp" >
																							<i class="fa fa-times" aria-hidden="true"></i>
																						</a>
																					</td>
																		<?php	} ?>
																			</tr>
														  <?php 		} ?>
												<?php 				} ?>
																</tbody>
												<?php		} ?>
														</table>

														<div class="row" id="no_document" <?php if(!empty($d_doc_client)) echo("style='display:none;'"); ?> >
															<div class="col-md-12">
																<div class="alert alert-warning">Il n'y a pas de document pour ce client.</div>
															</div>
														</div>
													</div>
												</div>
											</div>
								<?php	} ?>

										<!-- LOGO -->
								<?php	if($aff_onglet_logo){ ?>
											<div id="tab8" class="tab-pane <?php if($onglet==8) echo("active"); ?>" >

												<h2 style="margin-top:0px;">Logos</h2>
												<form action="client_logo_enr.php" method="post" class="admin-form form-inline form-inline-grid" id="form_logos" enctype="multipart/form-data">
													<div>
														<input type="hidden" name="client" value="<?=$client?>" />
													</div>
													<div class="row" >
														<div class="col-md-3 col-md-offset-3" >
													<?php	if($drt_edit_autre){ ?>
																<div class="row" >
																	<div class="col-md-12" >
																		<div class="image-preview image-350x140" >
															<?php			if(!empty($d_client['cli_logo_1'])){ ?>
																				<img src="documents/clients/client<?=$client?>/<?= $d_client['cli_logo_1']?>" alt="" id="logo_1_preview" />
															<?php			}else{ ?>
																				<img src="assets/img/400x140.jpg" alt="" id="logo_1_preview" />
															<?php			} ?>
																		</div>
																	</div>
																</div>
																<div class="row" >
																	<div class="col-md-12" >
																		<span class="prepend-icon file">
																			<span class="button btn-primary">Choisir</span>
																			<input type="file" class="gui-file" name="cli_logo_1" id="logo_1" />
																			<input type="text" class="gui-input" id="logo_1_champ" placeholder="Logo 1">
																			<span class="field-icon">
																				<i class="fa fa-upload"></i>
																			</span>
																		</span>
																	</div>
																</div>
																<div class="row mt15" >
																	<div class="col-md-12 text-center" >
																		<button type="button" class="btn btn-sm btn-danger logo-supp" data-logo="1" >
																			<i class="fa fa-times" ></i> Supprimer
																		</button>
																	</div>
																</div>
													<?php	}else{ ?>
																<div class="col-md-12" >
																	<div class="image-preview image-350x140" >
														<?php			if(!empty($d_client['cli_logo_1'])){ ?>
																			<img src="documents/clients/client<?=$client?>/<?= $d_client['cli_logo_1']?>" alt="" id="logo_1_preview" />
														<?php			}else{ ?>
																			<img src="assets/img/400x140.jpg" alt="" id="logo_1_preview" />
														<?php			} ?>
																	</div>
																</div>
													<?php	} ?>
														</div>

														<div class="col-md-3">
													<?php	if($drt_edit_autre){ ?>
																<div class="row" >
																	<div class="col-md-12">
																		<div class="image-preview image-350x140" >
															<?php			if(!empty($d_client['cli_logo_1'])){ ?>
																				<img src="documents/clients/client<?=$client?>/<?= $d_client['cli_logo_2']?>" alt="" id="logo_2_preview" />
															<?php			}else{ ?>
																				<img src="assets/img/400x140.jpg" alt="" id="logo_2_preview" />
															<?php			} ?>
																		</div>
																	</div>
																</div>
																<div class="row" >
																	<div class="col-md-12">
																		<span class="prepend-icon file">
																			<span class="button btn-primary">Choisir</span>
																			<input type="file" class="gui-file" name="cli_logo_2" id="logo_2" >
																			<input type="text" class="gui-input" id="logo_2_champ" placeholder="Logo 2">
																			<span class="field-icon">
																				<i class="fa fa-upload"></i>
																			</span>
																		</span>
																	</div>
																</div>
																<div class="row mt15" >
																	<div class="col-md-12 text-center" >
																		<button type="button" class="btn btn-sm btn-danger logo-supp" data-logo="2"  >
																			<i class="fa fa-times" ></i> Supprimer
																		</button>
																	</div>
																</div>
													<?php	}else{ ?>
																<div class="col-md-12" >
																	<div class="image-preview image-350x140" >
														<?php			if(!empty($d_client['cli_logo_2'])){ ?>
																			<img src="documents/clients/client<?=$client?>/<?= $d_client['cli_logo_2']?>" alt="" id="logo_2_preview" />
														<?php			}else{ ?>
																			<img src="assets/img/400x140.jpg" alt="" id="logo_2_preview" />
														<?php			} ?>
																	</div>
																</div>
													<?php	} ?>
														</div>
													</div>
												</form>

											</div>
							<?php		} ?>
										<!--FIN LOGO -->

										<!-- adresses TAB11 -->
										<div id="tab11" class="tab-pane <?php if($onglet==11) echo("active"); ?>" >

											<h2 style="margin-top:0px;">Carnet d'adresses</h2>
											<div class="row">
												<div class="col-md-4">
													<blockquote class="blockquote-success">
														<p>Adresses d'intervention.</p>
													</blockquote>
										<?php 		if(!empty($adresses_int)){
														foreach($adresses_int as $a){

															$text_adresse="";
															if(!empty($a['adr_nom'])){
																$text_adresse=$a['adr_nom'];
															}
															if(!empty($a['adr_service'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_service'];
															}
															if(!empty($a['adr_ad1'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_ad1'];
															}
															if(!empty($a['adr_ad2'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_ad2'];
															}
															if(!empty($a['adr_ad3'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_ad3'];
															}
															$text_adresse.="<br/>" . $a['adr_cp'] . " " . $a['adr_ville']; ?>

															<div class="panel adresse-1" id="adresse_<?=$a['adr_id']?>" >
																<div class="panel-heading panel-head-sm">
																	<span class="panel-title"><?=$a['adr_libelle']?></span>
																</div>
																<div class="panel-body">
																	<?=$text_adresse?>
																</div>
														<?php	if($drt_edit_adr){ ?>
																	<div class="panel-footer">

																			<a href="client_adresse.php?client=<?=$client?>&adresse=<?=$a['adr_id']?>&onglet=11" class="btn btn-warning btn-sm" >
																				<i class="fa fa-pencil"></i>
																			</a>
																<?php 		if($a['adr_defaut'] != 1){ ?>
																				<div class="pull-right" >
																					<button type="button" data-adresse="<?= $a['adr_id']?>" class="btn btn-danger btn-sm deladresse text-right">
																						<i class="fa fa-times"></i>
																					</button>
																				</div>
																<?php 		} ?>

																	</div>
														<?php	} ?>
															</div>
												<?php	}
													} ?>
													<div class="alert alert-warning" id="no_adresse_1" <?php if(!empty($adresses_int)) echo("style='display:none;'"); ?> >
														Pas d'adresse d'intervention pour ce client.
													</div>
												</div>

												<div class="col-md-4">
													<blockquote class="blockquote-primary">
														<p>Adresses de facturation.</p>
													</blockquote>
										  <?php		if(!empty($adresses_fac)){
														foreach($adresses_fac as $a){
															$text_adresse="";
															if(!empty($a['adr_nom'])){
																$text_adresse=$a['adr_nom'];
															}
															if(!empty($a['adr_service'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_service'];
															}
															if(!empty($a['adr_ad1'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_ad1'];
															}
															if(!empty($a['adr_ad2'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_ad2'];
															}
															if(!empty($a['adr_ad3'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_ad3'];
															}
															$text_adresse.="<br/>" . $a['adr_cp'] . " " . $a['adr_ville'];

															if(!empty($a['adr_siret']) AND !empty($d_client["cli_siren"])){
																$text_adresse.="<br/>" . $d_client["cli_siren"] . " " . $a['adr_siret'];
															} ?>
															<div class="panel adresse-2" id="adresse_<?=$a['adr_id']?>" >
																<div class="panel-heading panel-head-sm">
																	<span class="panel-title"><?=$a['adr_libelle']?></span>
																</div>
																<div class="panel-body">
																	<?=$text_adresse?>
																</div>

														<?php	if($drt_edit_adr){ ?>
																	<div class="panel-footer" >
																		<a href="client_adresse.php?client=<?=$client?>&adresse=<?=$a['adr_id']?>&onglet=11" class="btn btn-warning btn-sm" >
																			<i class="fa fa-pencil"></i>
																		</a>
															<?php 		if($a['adr_defaut'] != 1){ ?>
																		<!-- FG sans l'adresse, on ne peut plus associé de SIRET à la facture
																			<div class="pull-right" >
																				<button type="button" data-adresse="<?= $a['adr_id']?>" class="btn btn-danger btn-sm deladresse">
																					<i class="fa fa-times"></i>
																				</button>
																			</div>-->
															<?php 		} ?>
																		<?php if(!empty($a['adr_siret']) AND !empty($d_client["cli_siren"])){ ?>
																			<a data-toggle="tooltip" data-title="Vérification du siret" href="client_verif_siret.php?client=<?=$client?>&adresse=<?=$a['adr_id']?>&onglet=11" class="btn btn-success btn-sm" >
																				<i class="fa fa-check"></i>
																			</a>
																		<?php } ?>
																	</div>
														<?php	} ?>
															</div>
												<?php	}
													} ?>
													<div class="alert alert-warning" id="no_adresse_2" <?php if(!empty($adresses_fac)) echo("style='display:none;'"); ?> >
														Pas d'adresse de facturation pour ce client.
													</div>

												</div>

												<div class="col-md-4">
													<blockquote class="blockquote-danger">
														<p>Adresses d'envoi de facture.</p>
													</blockquote>
										  <?php 	if(!empty($adresses_env)){
														foreach($adresses_env as $a){

															$text_adresse="";
															if(!empty($a['adr_nom'])){
																$text_adresse=$a['adr_nom'];
															}
															if(!empty($a['adr_service'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_service'];
															}
															if(!empty($a['adr_ad1'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_ad1'];
															}
															if(!empty($a['adr_ad2'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_ad2'];
															}
															if(!empty($a['adr_ad3'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['adr_ad3'];
															}
															$text_adresse.="<br/>" . $a['adr_cp'] . " " . $a['adr_ville']; ?>

															<div class="panel adresse-3" id="adresse_<?=$a['adr_id']?>" >
																<div class="panel-heading panel-head-sm">
																	<span class="panel-title"><?=$a['adr_libelle']?></span>
																</div>
																<div class="panel-body">
																	<?=$text_adresse?>
																</div>
														<?php	if($drt_edit_adr){ ?>
																	<div class="panel-footer">
																		<a href="client_adresse.php?client=<?=$client?>&adresse=<?=$a['adr_id']?>&onglet=11" class="btn btn-warning btn-sm" >
																			<i class="fa fa-pencil"></i>
																		</a>
																<?php 	if($a['adr_defaut'] != 1){ ?>
																			<div class="pull-right" >
																				<button type="button" data-adresse="<?= $a['adr_id']?>" class="btn btn-danger btn-sm deladresse">
																					<i class="fa fa-times"></i>
																				</button>
																			</div>
																<?php 	} ?>
																	</div>
														<?php	} ?>
															</div>
												<?php	}
													} ?>
													<div class="alert alert-warning" id="no_adresse_3" <?php if(!empty($adresses_env)) echo("style='display:none;'"); ?> >
														Pas d'adresse d'envoi de facture pour ce client.
													</div>

												</div>
											</div>
										</div>
										<!-- fin adresses -->

										<!-- contacts -->
										<div id="tab12" class="tab-pane <?php if($onglet==12) echo("active"); ?>" >

											<div class="row">
												<h2 style="margin-top:0px;">Carnet de contacts</h2>
										<?php 	if(!empty($contacts)){ ?>
													<table class="table table-striped table-hover" id="tab_contact" >
														<thead>
															<tr class="dark">
																<th class="text-center" >Fonction</th>
																<th class="text-center" >Nom</th>
																<th class="text-center" >Tel</th>
																<th class="text-center" >Portable</th>
																<th class="text-center" >Fax</th>
																<th class="text-center" >Mail</th>
																<th class="text-center" >Commentaire</th>
												<?php			if($drt_edit_con){ ?>
																	<th class="text-center" >Actions</th>
												<?php			} ?>
															</tr>
														</thead>
														<tbody>
												<?php 		foreach($contacts as $c){

																$comment="";
																if(!empty($c['con_comment'])){
																	$comment=str_replace(CHR(13),"<br/>",$c['con_comment']);
																}?>
																<tr id="contact_<?=$c['con_id']?>" class="contact" >
																	<td>
												<?php 					if(!empty($c['con_fonction'])){
																			echo($d_con_fonction_nom[$c['con_fonction']]);
																		}else{
																			echo($c['con_fonction_nom']);
																		} ?>
																	</td>
																	<td>
																	<?php foreach($base_civilite as $k => $v): ?>
																	  <?php if($k == $c['con_titre']): ?>
																		<?= $v; ?>
																	  <?php endif; ?>
																	<?php endforeach; ?>
																	<?= $c['con_prenom']; ?> <?= $c['con_nom']; ?>
																	</td>
																	<td><?= $c['con_tel']; ?></td>
																	<td><?= $c['con_portable']; ?></td>
																	<td><?= $c['con_fax']; ?></td>
																	<td><?= $c['con_mail']; ?></td>
																	<td><?=$c['con_comment']?></td>
													<?php			if($drt_edit_con){ ?>
																		<td>
																			<a href="client_contact.php?client=<?=$client?>&contact=<?=$c['con_id']?>&onglet=12" class="btn btn-warning btn-sm" >
																				<i class="fa fa-pencil"></i>
																			</a>
																	<?php 	if(empty($c['aco_contact'])){ ?>
																				<button type="button" class="btn btn-danger btn-sm delcontact" data-contact="<?=$c['con_id']?>" >
																					<i class="fa fa-times"></i>
																				</button>
																	<?php 	} ?>
																		</td>
													<?php			} ?>
																</tr>
												<?php 		} ?>
														</tbody>
													</table>
										<?php 	} ?>

												<div class="alert alert-warning" id="no_contact" <?php if(!empty($contacts)) echo("style='display:none;'"); ?> >
													Pas de contact pour ce client.
												</div>

											</div>
										</div>
										<!-- fin contacts -->

										<!-- DEBUT TAB ACCES -->
										<div id="tab14" class="tab-pane <?php if($onglet==14) echo("active"); ?>" >


											<div class="row">
												<h2>Accès client</h2>
									<?php		if(!empty($d_acces_client)){ ?>
													<table class="table table-striped table-hover" >
														<thead>
															<tr class="dark2">
																<th class="text-center" >Identifiant</th>
																<th class="text-center" >Mot de passe</th>
																<th class="text-center" >Email</th>
																<th class="text-center" >Date de validité</th>
																<th class="text-center" >Archivé</th>
														<?php	if($drt_edit_autre){ ?>
																	<th class="text-center" >Actions</th>
														<?php	} ?>
															</tr>
														</thead>
														<tbody>
												<?php 		foreach($d_acces_client as $a){

																$class_valide="";
																if($a["acc_passe_modif"]<date("Y-m-d")){
																	$class_valide="class='text-danger'";
																}

																$date_valide="";
																$Dt_valide=DateTime::createFromFormat('Y-m-d H:i:s',$a["acc_passe_modif"]);
																if(!is_bool($Dt_valide)){
																	$date_valide=$Dt_valide->format("d/m/Y");
																}


?>
															<tr
															<?php if(!empty($a['acc_archive'])){ ?>
																class="danger text-center"
															<?php }else{?>
																class="text-center"
															<?php } ?>
															>
																<td><?= $a['acc_uti_ident']; ?></td>
																<td><?= $a['acc_uti_passe']; ?></td>
																<td><?= $a['acc_mail']; ?></td>
																<td <?=$class_valide?> ><?=$date_valide?></td>

																<td>
																<?php if(!empty($a['acc_archive'])){ ?>
																	Oui
																<?php }else{ ?>
																	Non
																<?php }?>

																</td>
														<?php	if($drt_edit_autre){ ?>
																	<td>
																		<a href="mail_prep.php?client=<?=$client?>&acc_ref_id=<?=$a['acc_ref_id']?>&onglet=14"  class="btn btn-sm btn-info">
																			<i class="fa fa-envelope-o" ></i>
																		</a>
																		<a href="client_acces.php?client=<?=$client?>&acc_ref_id=<?=$a['acc_ref_id']?>&onglet=14" class="btn btn-warning btn-sm" >
																			<i class="fa fa-pencil"></i>
																		</a>
																	</td>
														<?php	} ?>
																</tr>
															<?php } ?>
														</tbody>
													</table>
									<?php 		}else{ ?>
													<div class="col-md-12 text-center" style="padding:0;" >
														<div class="alert alert-warning" style="border-radius:0px;">
															Aucun accès pour ce client.
														</div>
													</div>
								<?php 			} ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
		<?php		}else{ ?>
						<h1 class="bg-danger text-center pt25 pb25" >Impossible d'afficher la page!</h1>
						<?php if(!empty($erreur_txt)){ ?>
							<div class="alert alert-danger">
								<?= $erreur_txt ?><br>
								<?php 		if(!empty($_SESSION['retourClient'])){ ?>
							<a href="<?= $_SESSION['retourClient']; ?>" class="btn btn-default btn-sm">
								<i class="fa fa-long-arrow-left"></i>
								Retour
							</a>
			<?php		}elseif(!empty($_SESSION['retour'])){ ?>
							<a href="<?= $_SESSION['retour']; ?>" class="btn btn-default btn-sm">
								<i class="fa fa-long-arrow-left"></i>
								Retour
							</a>
			<?php 		} ?>
							</div>
						<?php } ?>
		<?php		} ?>
				</section>
			<!-- End: Content -->
			</section>

		</div>
<?php	if($erreur==0){ ?>

			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-4 footer-left">
			<?php 		if(!empty($_SESSION['retourClient'])){ ?>
							<a href="<?= $_SESSION['retourClient']; ?>" class="btn btn-default btn-sm">
								<i class="fa fa-long-arrow-left"></i>
								Retour
							</a>
			<?php		}elseif(!empty($_SESSION['retour'])){ ?>
							<a href="<?= $_SESSION['retour']; ?>" class="btn btn-default btn-sm">
								<i class="fa fa-long-arrow-left"></i>
								Retour
							</a>
			<?php 		}
						if(isset($tab_prec)){
							if($tab_prec[1]==1){ ?>
								<a href="suspect_voir.php?suspect=<?=$tab_prec[0]?>" class="btn btn-default btn-sm btn-onglet btn-onglet-general">
									<i class="fa fa-long-arrow-left"></i>
								</a>
			<?php 			}else{ ?>
								<a href="client_voir.php?client=<?=$tab_prec[0]?>" class="btn btn-default btn-sm btn-onglet btn-onglet-general">
									<i class="fa fa-long-arrow-left"></i>
								</a>
			<?php			}
						}
						if(isset($tab_suiv)){
							if($tab_suiv[1]==1){ ?>
								<a href="suspect_voir.php?suspect=<?=$tab_suiv[0]?>" class="btn btn-default btn-sm btn-onglet btn-onglet-general">
									<i class="fa fa-long-arrow-right"></i>
								</a>
				<?php 		}else{ ?>
								<a href="client_voir.php?client=<?=$tab_suiv[0]?>" class="btn btn-default btn-sm btn-onglet btn-onglet-general">
									<i class="fa fa-long-arrow-right"></i>
								</a>
				<?php		}
						}

						if($aff_onglet_produit){ ?>
							<!-- onglet produits -->
							<a href="client_produit_histo.php?client=<?=$client?>" class="btn btn-default btn-sm btn-onglet btn-onglet-produit" <?php if($onglet!=13) echo("style='display:none;'");?> >
								<i class='fa fa-calendar'></i> Historique de prix
							</a>
				<?php	} ?>
					</div>

					<div class="col-xs-8 footer-right">
						<!-- onglet groupe -->
				<?php 	if($_SESSION["acces"]["acc_droits"][13] AND (!$d_categorie["cca_gc"] OR $_SESSION["acces"]["acc_droits"][8]) ){ ?>
							<a href="client_filiale.php?client=<?=$maison_mere_id?>" class="btn btn-warning btn-sm btn-onglet btn-onglet-groupe" <?php if($onglet!=10) echo("style='display:none;'");?> >
								<i class='fa fa-pencil'></i> Gérer les filiales
							</a>
				<?php 	} ?>

						<!-- onglet général -->
						<div class="btn-onglet btn-onglet-general" <?php if($onglet!=1) echo("style='display:none;'");?> >
						<?php
						if ($_ENV['APP_ENV'] && $_ENV['APP_ENV'] == 'local' && (empty($d_client['cli_groupe']) || empty($d_client['cli_filiale_de']))) { ?>
							<a href="client_plateforme.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-success btn-sm" >
								<i class='fa fa-upload'></i> Transfert vers la plateforme
							</a>
						<?php } ?>
			<?php			if($d_client["cli_categorie"]==2){
								// grand compte -> il faut avoir la gestion des GC (la service com est sencé l'avoir)
								if($_SESSION["acces"]["acc_droits"][8]){ ?>
									<a href="client_reseau.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-primary btn-sm" >
										<i class='fa fa-globe'></i> Client Réseau
									</a>
					<?php 		}
							}elseif($d_client["cli_categorie"]==5){
								if($_SESSION["acces"]["acc_droits"][15] AND $_SESSION['acces']['acc_service'][1]==1){ ?>
									<a href="client_reseau.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-primary btn-sm" >
										<i class='fa fa-globe'></i> Client Réseau
									</a>
					<?php		}
							}elseif($_SESSION["acces"]["acc_droits"][15]){ ?>
								<a href="client_reseau.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-primary btn-sm" >
									<i class='fa fa-globe'></i> Client Réseau
								</a>
					<?php	}
							if($drt_edit_adr){ ?>
								<a href="client_adresse.php?client=<?=$client?>" class="btn btn-success btn-sm" >
									<i class='fa fa-plus'></i> Adresse
								</a>
					<?php	}
							if($drt_edit_con){ ?>
								<a href="client_contact.php?client=<?=$client?>" class="btn btn-success btn-sm" >
									<i class='fa fa-plus'></i> Contact
								</a>
					<?php 	}
							if($aff_onglet_corresp){ ?>
								<a href="client_correspondance.php?client=<?=$client?>" class="btn btn-success btn-sm" >
									<i class='fa fa-plus'></i> Correspondance
								</a>
					<?php	}
							if($drt_edit_cli){ ?>
								<?php if($d_client['cli_categorie'] != 3){ ?>
									<a href="client_verif_siret.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-success btn-sm" >
										<i class='fa fa-check'></i> Vérifier le siret
									</a>
								<?php } ?>
								<a href="client_mod.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-warning btn-sm" >
									<i class='fa fa-pencil'></i> Modifier
								</a>
					<?php	}
							?>
						</div>
						<!-- fin onglet général -->

						<!-- onglet adresse -->
				<?php	if($drt_edit_adr){ ?>
							<a href="client_adresse.php?client=<?=$client?>&onglet=11" class="btn btn-success btn-sm btn-onglet btn-onglet-adresse" <?php if($onglet!=11) echo("style='display:none;'");?> >
								<i class='fa fa-plus'></i> Adresse
							</a>
				<?php	} ?>

						<!-- onglet contact -->
						<a href="client_contact.php?client=<?=$client?>&onglet=12" class="btn btn-success btn-sm btn-onglet btn-onglet-contact" <?php if($onglet!=12) echo("style='display:none;'");?> >
							<i class='fa fa-plus'></i> Contact
						</a>

						<!-- onglet correspondances -->
						<a href="client_correspondance.php?client=<?=$client?>&tab5" class="btn btn-success btn-sm btn-onglet btn-onglet-cor" <?php if($onglet!=5) echo("style='display:none;'");?> >
							<i class='fa fa-plus'></i> Correspondance
						</a>

						<!-- onglet Info -->
				<?php	if($drt_edit_autre){ ?>
							<a href="client_info_mod.php?client=<?= $client ?>" class="btn btn-success btn-sm btn-onglet btn-onglet-info" <?php if($onglet!=6) echo(" style='display:none;'");?> >
								<i class='fa fa-plus'></i> Info
							</a>
				<?php	} ?>

						<!-- onglet produits -->
				<?php	if($aff_onglet_produit AND $_SESSION['acces']["acc_droits"][7] AND ($d_client["cli_categorie"]!=2 OR $_SESSION['acces']["acc_droits"][8] ) ){ ?>
							<a href="client_produit.php?client=<?=$client?>" class="btn btn-success btn-sm btn-onglet btn-onglet-produit" <?php if($onglet!=13) echo("style='display:none;'");?> >
								<i class='fa fa-pencil'></i> Ajouter des produits
							</a>
				<?php	}
						if($d_client["cli_sous_categorie"]!=1 OR $acc_agence==4){ ?>
							<button type="button" id="devis_add" class="btn btn-success btn-sm btn-onglet btn-onglet-produit" <?php if($onglet!=13) echo("style='display:none;'");?> >
								<i class='fa fa-plus'></i> Enregistrer un devis
							</button>
				<?php	}
						?>

						<!-- onglet document -->
				<?php	if($drt_edit_autre){ ?>
							<a href="client_doc.php?client=<?=$client?>" class="btn btn-success btn-sm btn-onglet btn-onglet-document" <?php if($onglet!=7) echo("style='display:none;'");?> >
								<i class='fa fa-plus'></i> Ajouter un document
							</a>

							<!-- onglet acces -->
							<a href="client_acces.php?client=<?=$client?>" class="btn btn-success btn-sm btn-onglet btn-onglet-acces" <?php if($onglet!=14) echo("style='display:none;'");?> >
								<i class='fa fa-plus'></i> Ajouter un accès
							</a>
							<!-- onglet logo -->
							<button type="button" id="logo_import" class="btn btn-success btn-sm btn-onglet btn-onglet-logos" <?php if($onglet!=8) echo("style='display:none;'");?>>
								<i class='fa fa-floppy-o'></i> Importer
							</button>
				<?php	} ?>
					</div>
				</div>
			</footer>

			<!-- MODAL -->

			<!-- modal de confirmation -->
			<div id="modal_confirmation_user" class="modal fade" role="dialog" >
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title">Confirmer ?</h4>
						</div>
						<div class="modal-body">
							<p>Message</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								Annuler
							</button>
							<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
								Confirmer
							</button>
						</div>
					</div>
				</div>
			</div>

			<!-- FIN MODAL -->

<?php	}

// DEB JS?>


		<?php include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<script type="text/javascript" src="assets/admin-tools/admin-forms/js/jquery-ui-monthpicker.min.js"></script>

		<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>

		<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
		<script type="text/javascript" src="/assets/js/custom.js"></script>

		<!--
		hotfix/devis-duplicate-id
		<script type="text/javascript" src="/assets/js/sync_heracles.js"></script>
		-->

		<script type="text/javascript">
	<?php	if(isset($maison_mere)){ ?>
				var maison_mere="<?=$maison_mere["cli_id"]?>";
	<?php	} ?>

			var client="<?=$d_client["cli_id"]?>";

			jQuery(document).ready(function (){

				// INTERACTION ONGLET PRODUIT
				$("#filtre_produit").click(function (){
					categorie=$("#pro_filtre_categorie").val();
					famille=$("#pro_filtre_famille").val();
					sous_famille=$("#pro_filtre_sous_famille").val();
					sous_sous_famille=$("#pro_filtre_sous_sous_famille").val();
					archive=0;
					if($("#pro_filtre_archive").is(":checked")){
						archive=1;
					};

					filtrer_produit_client(categorie,famille,sous_famille, sous_sous_famille,archive);
				});

				$("#raz_produit").click(function (){
					$("#pro_filtre_categorie").select2("val",0);
					$("#pro_filtre_famille").select2("val",0);
					$("#pro_filtre_sous_famille").select2("val",0);
					$("#pro_filtre_sous_sous_famille").select2("val",0);
					$("#pro_filtre_archive").prop("checked",false);
					filtrer_produit_client(0,0,0,0,0);
				});

				$(".produit-supp").click(function (){
					produit=$(this).data("produit");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer ce produit ?",confirmer_client_supp,client,produit)
				});

				$("#devis_add").click(function (){
					if($(".intra-multi:checked").length>0 || $(".inter-multi:checked").length>0){
						// hotfix/devis-duplicate-id
						//devis_sync("<?=$acc_societe?>","#form_devis_add","#id_heracles");

						$("#form_devis_add").submit();
					}else{
						afficher_message("Pas de produit","danger","Merci de selectionner au moin un produit");
					}

				});


				// INTERACTION ONGLET

				$(".onglet").click(function(){

					onglet=$(this).data("onglet");
					console.log("onglet " + onglet);
					$(".btn-onglet").each(function(){
						if($(this).hasClass("btn-onglet-" + onglet)){
							$(this).show();
						}else{
							$(this).hide();
						}
					});
				});

				//INTERACTION INFO

				$(".info-supp").click(function(){
					info=$(this).data("info");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer cette info ?",supprimer_info,info)
				});

				$("#filtre_info").click(function (){
					type=$("#info_filtre_type").val();
					famille=$("#info_filtre_famille").val();
					sous_famille=$("#info_filtre_sous_famille").val();
					sous_sous_famille=$("#info_filtre_sous_sous_famille").val();
					filtrer_info_client(type,famille,sous_famille, sous_sous_famille);
				});

				$("#raz_info").click(function (){
					$("#info_filtre_type").val(0);
					$("#info_filtre_famille").select2("val",0);
					$("#info_filtre_sous_famille").select2("val",0);
					$("#info_filtre_sous_sous_famille").select2("val",0);
					filtrer_info_client(0,0,0,0);
				});

				// INTERACTION CORRESPONDANCE

				$(".corresp-supp").click(function(){
					corresp=$(this).data("corresp");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer cette correspondance ?",preparer_supp_corresp,corresp);
				});

				$(".corresp-rappel").click(function(){
					corresp=$(this).data("corresp");
					cor_rappel=0;
					if($(this).is(":checked")){
						cor_rappel=1;
					}
					set_base_champ("0","Correspondances","cor_rappel",cor_rappel,"cor_id",corresp,actualiser_rappel,corresp);
				});

				// INTERACTION LIE AU DOC
				$(".doc-supp").click(function(){
					var document=$(this).data("document");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer ce document ?",preparer_supp_doc,document)

				});

				// INTERACTION LIE AU LOGO

				$("#logo_import").click(function(){
					$("#form_logos").submit();
				});

				$(".logo-supp").click(function(){
					console.log("supp logo");
					console.log("supp logo");
					logo=$(this).data("logo");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer ce logo?",supprimer_logo,logo);
				});

				// INTERACTION CONTACT

				$(".delcontact").click(function(){
					contact=$(this).data("contact");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer ce contact?",confirmer_contact_supp,contact);
				});

				// INTERACTION LIE AUX ADRESSES

				$(".deladresse").click(function(){
					adresse=$(this).data("adresse");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer cette adresse?",confirmer_adresse_supp,adresse);
				});

				// INETRACTION Planning

				$("#aff_inter").click(function(){
					search_formation=true;
					if($("#pla_inter_s_s_fam option").length>1 && $("#pla_inter_s_s_fam").val()==0){
						search_formation=false;
					}else if($("#pla_inter_s_fam").val()==0){
						search_formation=false;
					}
					if(search_formation){
						get_actions(client,$("#pla_inter_fam").val(),$("#pla_inter_s_fam").val(),$("#pla_inter_s_s_fam").val(),2,afficher_planning_inter,"","");

					}

				});

				// INTERACTION LIE AU FILIALE

				$(".btn-facturation").click(function(){
					ope=$(this).data("fac_op");
					client=$(this).data("fac_client");
					if(client>0){
						$.ajax({
							type:'POST',
							url: 'ajax/ajax_client_facturation_set.php',
							data : 'client=' + client + "&ope=" + ope,
							dataType: 'json',
							success: function(data){
								if(data.resultat){
									if(data.filiales==null){
										if(ope==1){
											html="<div data-toggle='tooltip' title='Facturation groupée sur le compte" + data.code + ".' >";
												html=html + "<i class='fa fa-euro' ></i> " + data.code;
											html=html + "</div>";
											$(".fac-groupee").html(html);
											$('[data-toggle="tooltip"]').tooltip();
										}else{
											$(".fac-groupee").html("");
										}

									}else{

										// fac groupee sur un filiale
										if(ope==1){
											html="<div data-toggle='tooltip' title='Facturation groupée sur le compte" + data.code + ".' >";
												html=html + "<i class='fa fa-euro' ></i> " + data.code;
											html=html + "</div>";
											$.each(data.filiales,function(i,val){
												$(".fac-groupee-" + val).html(html);
											});
											$('[data-toggle="tooltip"]').tooltip();
										}else{
											$.each(data.filiales,function(i,val){
												$(".fac-groupee-" + val).html("");
											});
										}

									}

									if(ope==1){
										afficher_message("Enregistrement terminé","success","Le client et ses filiales sont bien passées en facturation groupée.");
									}else{
										afficher_message("Enregistrement terminé","success","La facturation groupée a bien été supprimée pour le client et ses filialess.");
									}
								}
							},
							error: function(data) {
								afficher_message("Echec de l'enregistrement","danger","Vos modifications n'ont pas été enregistrées." + data.responseText);
							}
						});
					}
				});

				/*$('.dd').nestable({}).on('change',".dd-item", function(e){
					e.stopPropagation();

					var levelId = $(this).data('level');
					var id = $(this).data('id');
					var soc = $(this).data('soc');
					var type = $(this).data('type');
					var parentLevel = $(this).parents('.dd-item').data('level');
					var parentId = $(this).parents('.dd-item').data('id');
					var parentSoc = $(this).parents('.dd-item').data('soc');
					var parentType = $(this).parents('.dd-item').data('type');

					if(parentLevel === undefined){
						alert("Attention: vous êtes en train de remplacer la maison mère");
					}
					if(parentLevel > 2){
						alert("Attention: vous ne pouvez pas faire plus de trois niveaux");
						document.location.href="suspect_voir.php?client=<?= $client ?>&tab10";
					}else{
						$.ajax({
							type:'POST',
							url: 'ajax/ajax_groupe_client.php',
							data : 'id=' + id + "&levelId=" + levelId + "&parentLevel=" + parentLevel + "&parentId=" + parentId + "&client=<?= $client ?>&parentType=" + parentType + "&parentSoc=" + parentSoc + "&type=" + type + "&soc=" + soc,
							dataType: 'json',
							success: function(data)
							{


							}

						});
					}
				});*/
			});
			// FIN DE DOCUMENT READY

			// **** FONCTION ADRESSE ****

			function confirmer_adresse_supp(adresse){
				del_client_adresse(adresse,client,supprimer_adresse_client,adresse);
			}

			function supprimer_adresse_client(json){
				if(json.adresse){
					$("#adresse_" + json.adresse).remove();
				}

				if($(".adresse-" + json.type).length==0){
					$("#no_adresse_" + json.type).show();
				}
			}


			// **** FONCTION CONTACT ****

			function confirmer_contact_supp(contact){
				del_client_contact(contact,client,supprimer_contact_client,contact);
			}

			// modif de l'affichage suite à supp
			function supprimer_contact_client(json){
				if(json.contact){
					$("#contact_" + json.contact).remove();
				}
				if($(".contact").length==0){
					$("#tab_contact").remove();
					$("#no_contact").show();
				}
			}

			// **** FONCTION ONGLET PRODUIT ****

			function filtrer_produit_client(categorie,famille,sous_famille, sous_sous_famille, archive){

				console.log("filtrer_produit_client");
				console.log("archive " + archive);
				resultat=false;
				$(".produit_client" ).each(function(){
					if( (categorie>0 && !$(this).hasClass("pro_cli_cat_" + categorie) ) || (famille>0 && !$(this).hasClass("pro_cli_fam_" + famille)) || (sous_famille>0  && !$(this).hasClass("pro_cli_sous_fam_" + sous_famille))  || (sous_sous_famille>0 && !$(this).hasClass("pro_cli_sous_sous_fam_" + sous_sous_famille) ) || (!$(this).hasClass("pro_archive_" + archive)) ){
						$(this).hide();
					}else{

						$(this).show();
						resultat=true;
					}
				});
				if(resultat){
					$("#no_result_produit").hide();
				}else{
					$("#no_result_produit").show();
				}
			}

			function confirmer_client_supp(client,produit){
				console.log("confirmer_client_supp(" + client + "," + produit + ")");
				del_client_produit(produit,client,supprimer_produit_client,produit);
			}

			function supprimer_produit_client(produit){
				console.log("supprimer_produit_client(" + produit + ")");
				console.log($("#produit_" + produit));
				$(".produit_" + produit).remove();
			}


			// *** FONCTION LIE A L'ONGLET INFO ***

			function supprimer_info(info){
				del_client_info(info,supprimer_ligne_info)
			}

			function supprimer_ligne_info(json){
				if(json){
					$("#ligne_info_" + json.info).remove();
				}
			}

			function filtrer_info_client(type,famille,sous_famille,sous_sous_famille){
				resultat=false;
				$(".info-client" ).each(function(){
					if( ( type>0 && !$(this).hasClass("info-cli-type-" + type) ) || famille>0 && !$(this).hasClass("info-cli-fam-" + famille) || sous_famille>0 && !$(this).hasClass("info-cli-sous-fam-" + sous_famille) || sous_sous_famille>0 && !$(this).hasClass("info-cli-sous-sous-fam-" + sous_sous_famille) ){
						$(this).hide();
					}else{
						$(this).show();
						resultat=true;
					}
				});
				if(resultat){
					$("#no_result_info").hide();
				}else{
					$("#no_result_info").show();
				}
			}

			// *** FONCTION LIE A L'ONGLET COORESPONDANCE ***

			function preparer_supp_corresp(corresp){
				console.log("preparer_supp_corresp(" + corresp + ")");
				del_client_corresp(corresp,supprimer_corresp,corresp,"");
			}

			function supprimer_corresp(json){
				console.log("supprimer_corresp(json)");
				if(json){
					if(json.correspondance>0){
						$("#correspondance_" + json.correspondance).remove();
					}
					if($(".correspondance").size()==0){
						$("#no_corresp").show();
					}else{
						if(json.non_traite>0){
							$("#corresp_rappel_" + json.non_traite).prop("checked",false);
						}
					}
				}
			}

			function actualiser_rappel(json,corresp){
				if(json){
					if(json.erreur==0){
						// reussite
						afficher_txt_user($(".footer-middle"),"Mise à jour terminée","text-success",5000);
					}else{
						// erreur
						if($("corresp_rappel_" + corresp).is(":checked")){
							$(this).prop("checked",false);
						}else{
							$(this).prop("checked",true);
						}
						if(json.erreur_txt == ""){
							modal_alerte_user("Veuillez vous reconnecter à Orion.");
						}else{
							modal_alerte_user(json.erreur_txt);
						}

					}
				}
			}

			// *** FONCTION LIE A L'ONGLET DOC ***

			function preparer_supp_doc(document){
				del_client_doc(document,supprimer_document);
			}

			function supprimer_document(json){
				if(json){
					if(json.document>0){
						$("#document_" + json.document).remove();
					}
					console.log("nb doc " + $(".doc-supp").length);
					if($(".doc-supp").length==0){
						$("#no_document").show();
					}
				}
			}

			// *** FONCTION LIE AUX LOGOS ***

			function supprimer_logo(logo){
				var champ_logo="cli_logo_" + logo;
				set_base_champ("0","Clients",champ_logo,"","cli_id",client,actualiser_logo,logo,"");
			}

			function actualiser_logo(json,logo){
				if(json){
					if(json.erreur==0){
						// reussite
						$("#logo_" + logo + "_preview").prop("src","assets/img/400x140.jpg");
					}else{
						// erreur
						modal_alerte_user(json.erreur_txt);
					}
				}
			}


			// **** FONCTION PLANNING ****

			function afficher_planning_inter(json){
				console.log(json);
				if(json!=""){

					$ligne="";

					$.each(json, function(key, action){
						$lieu=action.act_adr_nom;
						if(action.act_adr_service!=""){
							$lieu=$lieu + "<br/>" + action.act_adr_service;
						}
						if(action.act_adr1!=""){
							$lieu=$lieu + "<br/>" + action.act_adr1;
						}
						if(action.act_adr2!=""){
							$lieu=$lieu + "<br/>" + action.act_adr2;
						}
						if(action.act_adr3!=""){
							$lieu=$lieu + "<br/>" + action.act_adr3;
						}
						$lieu=$lieu + "<br/>" + action.act_adr_cp + " " + action.act_adr_ville;

						$ligne=$ligne + "<tr>";
							$ligne=$ligne + "<td>" + action.act_date_deb_fr + "</td>";
							$ligne=$ligne + "<td>" + action.act_pro_reference + "</td>";
							$ligne=$ligne + "<td>" + action.act_planning_txt + "</td>";
							$ligne=$ligne + "<td>" + action.int_label_1 + "</td>";
							$ligne=$ligne + "<td>" + $lieu + "</td>";
							$ligne=$ligne + "<td></td>";
							$ligne=$ligne + "<td>";
								$ligne=$ligne + "<a href='devis_enr.php?auto=1&dev_client=" + client + "&dev_action=" + action.act_id + "' class='btn btn-sm btn-success' data-toggle='tooltip' title='Ajouter à un devis' >";
									$ligne=$ligne + "<i class='fa fa-euro' ></i>";
								$ligne=$ligne + "</a>";
							$ligne=$ligne + "</td>";
						$ligne=$ligne + "</tr>";
					});

					$("#planning_inter_ligne").html($ligne);
					$("#planning_inter").show();
					$("#planning_no_inter").hide();
				}else{
					$("#planning_inter").hide();
					$("#planning_no_inter").show();
				}
			}

		</script>
	</body>
</html>
