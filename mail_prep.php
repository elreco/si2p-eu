<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');
include('modeles/mod_parametre.php');
include('modeles/mod_commande_data.php');
include('modeles/mod_commande_pdf.php');
include('modeles/mod_facture_data.php');
include('modeles/mod_facture_pdf.php');
	// ECRAN DE PREPARATION D'UN MAIL

	$erreur_txt="";
	$contenu ="";
	$devis_id=0;
	if(!empty($_GET["devis"])){
		$devis_id=intval($_GET["devis"]);
	}

	$action_id=0;
	if(!empty($_GET["action"])){
		$action_id=intval($_GET["action"]);
	}

	$action_client_id=0;
	if(!empty($_GET["action_client"])){
		$action_client_id=intval($_GET["action_client"]);
	}

	$stagiaire_id=0;
	if(!empty($_GET["stagiaire"])){
		$stagiaire_id=intval($_GET["stagiaire"]);
	}
	$facture_relance=0;
	if(!empty($_GET["facture_relance"])){
		$facture_relance=intval($_GET["facture_relance"]);
	}
	$mail=false;
	if(!empty($_GET["mail"])){
		$mail=true;
	}
	$releve=false;
	if(!empty($_GET["releve"])){
		$releve=true;
	}

	$facture_id=0;
	if(!empty($_GET["facture"])){
		$facture_id=intval($_GET["facture"]);
	}
	$acc_ref_id=0;
	if(!empty($_GET["acc_ref_id"])){
		$acc_ref_id=intval($_GET["acc_ref_id"]);
	}

	$mail=0;
	if(!empty($devis_id)){
		$mail=1;
	}elseif(!empty($action_id) AND !empty($stagiaire_id)){
		$mail=2;
	}elseif(isset($_GET["avis"])){

		if(!empty($action_id) AND !empty($action_client_id)){
			// avis de passage
			$mail=8;
		}

	}elseif(!empty($action_id) AND !empty($action_client_id)){
		// client lié a une action
		$mail=3;
	}elseif(!empty($facture_id)){
		// envoie depuis une facture
		$mail=4;
	}elseif(!empty($acc_ref_id)){
		$mail=6;
	}elseif(!empty($facture_relance)){
		$mail=7;
	}else{
		$mail = 5;
	}

	if(empty($mail)){
		$erreur_txt="Le type de mail n'a pas pu être identifié!";
	}

	if(empty($erreur_txt)){


		// la personne logue

		$acc_utilisateur=0;
		if(!empty($_SESSION['acces']["acc_ref"])){
			if($_SESSION['acces']["acc_ref"]==1){
				$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
			}
		}

		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];
		}

		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];
		}


		$client=0;

		if($mail==1){

			// envoie depuis un devis

			$retour="devis_voir.php?devis=" . $devis_id;

			$sql="SELECT dev_numero,dev_client FROM Devis WHERE dev_id=" . $devis_id;
			$req=$ConnSoc->query($sql);
			$devis=$req->fetch();
			if(!empty($devis)){
				$sujet=$devis["dev_numero"];
				$client=$devis["dev_client"];
				if(file_exists("documents/Societes/" .  $acc_societe . "/Devis/" . $devis["dev_numero"] . ".pdf")){
					$pj[]=array(
						"nom" => $devis["dev_numero"] . ".pdf",
						"ext" => "pdf",
						"url" => "documents/Societes/" .  $acc_societe . "/Devis/" . $devis["dev_numero"] . ".pdf",
						"check" => true,
						"taille" => number_format((filesize("documents/Societes/" .  $acc_societe . "/Devis/" . $devis["dev_numero"] . ".pdf")/1024)/1024,2)
					);
				}

				// LES PRODUIT CONCERNES

				$tab_produit=array();
				$sql="SELECT DISTINCT dli_produit FROM devis_lignes WHERE dli_devis=" . $devis_id . ";";
				$req=$ConnSoc->query($sql);
				$d_produit=$req->fetchAll();
				if(!empty($d_produit)){
					foreach($d_produit as $dp){
						$tab_produit[]=$dp["dli_produit"];
					}
				}
				// SUR LE CLIENT

				$sql="SELECT cli_filiale_de,cli_id FROM clients WHERE cli_id=" . $devis["dev_client"] . ";";
				$req=$Conn->query($sql);
				$d_client=$req->fetch();

				$contenu="Bonjour,<br/><br/><br/><br/><br/><br/>Cordialement.";


			}else{
				$erreur_txt="Err";
			}

			if(empty($erreur_txt)){

				// LES DOCS ETECH DISPONIBLE

				if(!empty($tab_produit)){
					$liste_produit=implode(",",$tab_produit);
				}

				$sql="SELECT DISTINCT doc_id,doc_nom_aff,doc_ext,doc_nom FROM
				Documents JOIN documents_Utilisateurs ON (Documents.doc_id=Documents_Utilisateurs.dut_document)";
				if(isset($liste_produit)){
					$sql.=" JOIN Documents_Produits ON (Documents.doc_id=Documents_Produits.dpr_document)";
				}
				$sql.=" WHERE NOT doc_archive AND dut_utilisateur=" . $acc_utilisateur . " AND ( doc_client=0";
				if(!empty($d_client["cli_id"])){
					$sql.=" OR doc_client=" . $d_client["cli_id"];
				}
				if(!empty($d_client["cli_filiale_de"])){
					$sql.=" OR doc_client=" . $d_client["cli_filiale_de"];
				}
				$sql.=")";
				if(isset($liste_produit)){
					$sql.=" AND dpr_produit IN (" . $liste_produit .")";
				}
				$sql.=" ORDER BY doc_nom_aff;";
				$req=$Conn->query($sql);
				$d_doc=$req->fetchAll();
				if(!empty($d_doc)){
					foreach($d_doc as $dd){
						$pj[]=array(
							"nom" => $dd["doc_nom_aff"],
							"ext" => $dd["doc_ext"],
							"url" => "documents/ETECH/" . $dd["doc_nom"],
							"check" => false,
							"taille" => number_format((filesize("documents/ETECH/" . $dd["doc_nom"])/1024)/1024,2)
						);
					}
				}
			}

		}elseif($mail==2){

			// ACTION STAGIAIRE

			$destinataire=array();

			$retour="action_voir_sta.php?action=" . $action_id . "&societ=" . $conn_soc_id;

			$sql="SELECT ast_action_client,acl_pro_libelle,acl_client FROM Actions_Stagiaires,Actions_Clients WHERE ast_action_client=acl_id AND ast_action=" . $action_id . " AND ast_stagiaire=" . $stagiaire_id . ";";
			$req=$ConnSoc->query($sql);
			$d_action=$req->fetch();
			if(empty($d_action)){
				$erreur_txt="Impossible de charger les données sur la formation";
			}else{
				$sujet="Votre formation " . $d_action["acl_pro_libelle"];
				$client=$d_action["acl_client"];
			}

			if(empty($erreur_txt)){

				$contenu="";

				// SUR LE STAGIAIRE

				$sql="SELECT sta_nom,sta_prenom,sta_mail_perso FROM Stagiaires WHERE sta_id=" . $stagiaire_id . ";";
				$req=$Conn->query($sql);
				$d_stagiaire=$req->fetch();
				if(!empty($d_stagiaire)){
					$contenu="Bonjour " . $d_stagiaire["sta_prenom"] . " " . $d_stagiaire["sta_nom"] . ",";
					if(!empty($d_stagiaire["sta_mail_perso"])){
						$destinataire[]=array(
							"nom" => $d_stagiaire["sta_nom"] . " " . $d_stagiaire["sta_prenom"],
							"mail" => $d_stagiaire["sta_mail_perso"]
						);
					}
				}

				// LES PIECES JOINTES DISPO

				$pj=array();

				// CONCOC
				$url_convoc=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Convocations/convoc_". $action_id . "_" . $stagiaire_id . ".pdf";
				if(file_exists($url_convoc)){
					$pj[]=array(
						"nom" => "Convocation.pdf",
						"url" => $url_convoc,
						"check" => false,
						"taille" => number_format((filesize($url_convoc)/1024/1024),2)
					);
				}
				/*,
						"taille" => number_format((filesize($url_convoc/1024)/1024,2))*/
				// ATTESTATION
				$url_attestation=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Attestations/attestation_". $action_id . "_" . $stagiaire_id . ".pdf";
				if(file_exists($url_attestation)){
					$pj[]=array(
						"nom" => "Attestation.pdf",
						"url" => $url_attestation,
						"check" => false,
						"taille" => number_format((filesize($url_attestation)/1024/1024),2)
					);
				}

			}

		}elseif($mail==3){

			$retour="action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id . "&onglet=3&societ=" . $conn_soc_id;

			$sql="SELECT acl_pro_libelle,acl_client FROM Actions_Clients WHERE acl_id=" . $action_client_id . ";";
			$req=$ConnSoc->query($sql);
			$d_action_client=$req->fetch();
			if(empty($d_action_client)){
				$erreur_txt="Impossible de charger les données sur la formation";
			}else{
				$sujet="Votre formation " . $d_action_client["acl_pro_libelle"];
				$client=$d_action_client["acl_client"];
			}

			if(empty($erreur_txt)){

				$contenu="Bonjour,<br/><br/><br/><br/><br/><br/>Cordialement.";

				// LES PIECES JOINTES DISPO

				$pj=array();

				// CONVENTIONS

				$sql="SELECT con_numero FROM Conventions WHERE con_action_client=" . $action_client_id . " AND con_action=" . $action_id . ";";
				$req=$ConnSoc->query($sql);
				$d_convention=$req->fetch();
				if(!empty($d_convention)){
					$url_convention=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Conventions/". $d_convention["con_numero"] . ".pdf";
					if(file_exists($url_convention)){
						$pj[]=array(
							"nom" => $d_convention["con_numero"] . ".pdf",
							"url" => $url_convention,
							"check" => false,
							"taille" => number_format((filesize($url_convention)/1024/1024),2)
						);
					}
				}

				// CONCOC
				$url_convoc=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Convocations/convoc_". $action_client_id . ".pdf";
				if(file_exists($url_convoc)){
					$pj[]=array(
						"nom" => "Convocations.pdf",
						"url" => $url_convoc,
						"check" => false,
						"taille" => number_format((filesize($url_convoc)/1024/1024),2)
					);
				}

				// ATTESTATION INDIVIDUELLE
				$url_attestation_ind=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Attestations/attestation_ind_". $action_id . "_" . $action_client_id . ".pdf";
				if(file_exists($url_attestation_ind)){
					$pj[]=array(
						"nom" => "Attestations individuelles.pdf",
						"url" => $url_attestation_ind,
						"check" => false,
						"taille" => number_format((filesize($url_attestation_ind)/1024/1024),2)
					);
				}

				// ATTESTATION / SESSION
				$url_attestation_ses=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Attestations/attestation_ses_". $action_id . "_" . $action_client_id . ".pdf";
				if(file_exists($url_attestation_ses)){
					$pj[]=array(
						"nom" => "Attestations groupes.pdf",
						"url" => $url_attestation_ind,
						"check" => false,
						"taille" => number_format((filesize($url_attestation_ses)/1024/1024),2)
					);
				}
				// ATTESTATION FORMATION COMPLETE
				$url_attestation_form=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Attestations/attestation_form_". $action_id . "_" . $action_client_id . ".pdf";
				if(file_exists($url_attestation_form)){
					$pj[]=array(
						"nom" => "Attestation formation.pdf",
						"url" => $url_attestation_form,
						"check" => false,
						"taille" => number_format((filesize($url_attestation_form)/1024/1024),2)
					);
				}
			}

		}elseif($mail==4){

			$retour="facture_voir.php?facture=" . $facture_id;

			$sql="SELECT fac_numero,fac_nature,fac_client FROM Factures WHERE fac_id=" . $facture_id . ";";
			$req=$ConnSoc->query($sql);
			$d_facture=$req->fetch();
			if(empty($d_facture)){
				$erreur_txt="Impossible de charger la facture";
			}else{
				if($d_facture["fac_nature"]==1){
					$sujet="Facture " . $d_facture["fac_numero"];

				}else{
					$sujet="Avoir " . $d_facture["fac_numero"];
				}
				$url_facture_form=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Factures/".  $d_facture["fac_numero"] . ".pdf";
				$url_facture_form_2 = "/Documents/Societes/" . $acc_societe . "/Factures/".  $d_facture["fac_numero"] . ".pdf";
					if(file_exists($url_facture_form)){
						$pj[]=array(
							"nom" =>  $d_facture["fac_numero"] . ".pdf",
							"url" => $url_facture_form,
							"check" => false,
							"taille" => number_format((filesize($url_facture_form)/1024/1024),2)
						);
					}
				$client=$d_facture["fac_client"];
			}
		}elseif($mail==7){

			$retour=$_SESSION['retourFacture'];
			// SELECTIONNER LA RELANCE

			$sql="SELECT * FROM Relances WHERE rel_id=" . $facture_relance . ";";
			$req=$Conn->query($sql);
			$d_relance=$req->fetch();

			$sql="SELECT cli_id, cli_filiale_de FROM Clients WHERE cli_id=" . $d_relance['rel_client'] . ";";
			$req=$Conn->query($sql);
			$d_client=$req->fetch();

			$sql="SELECT * FROM Relances_Etats WHERE ret_id=" . $d_relance['rel_etat_relance'] . ";";
			$req=$Conn->query($sql);
			$d_relance_etat=$req->fetch();
			$client = $d_relance['rel_client'];
			if(!empty($d_relance['rel_opca'])){
				$sql="SELECT cli_id, cli_filiale_de FROM Clients WHERE cli_id=" . $d_relance['rel_opca'] . ";";
				$req=$Conn->query($sql);
				$d_client=$req->fetch();
			}


			if(empty($d_relance)){
				$erreur_txt="Impossible de charger la relance";
			}else{

				$sql="SELECT * FROM Relances_Factures WHERE rfa_relance = " . $d_relance['rel_id'];
				$req=$Conn->query($sql);
				$relances_factures=$req->fetchAll();
				//. " AND NOT fac_total_ttc=fac_regle AND fac_nature=1;
				$sql="SELECT soc_nom FROM Societes WHERE soc_id = " . $acc_societe;
				$req=$Conn->query($sql);
				$d_societe=$req->fetch();
				foreach($relances_factures as $rfaf){
					$ConnFct = connexion_fct($rfaf['rfa_facture_soc']);
					$sql="SELECT * FROM Factures WHERE fac_id=" . $rfaf['rfa_facture'];
					$req=$ConnFct->query($sql);
					$d_facture=$req->fetch();

					$url_facture_form=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $rfaf["rfa_facture_soc"] . "/Factures/".  $d_facture["fac_numero"] . ".pdf";
					$url_facture_form_2="/Documents/Societes/" . $rfaf["rfa_facture_soc"] . "/Factures/".  $d_facture["fac_numero"] . ".pdf";
					if(file_exists($url_facture_form)){
						$pj[]=array(
							"nom" =>  $d_facture["fac_numero"] . ".pdf",
							"url" => $url_facture_form,
							"check" => true,
							"id" => $d_facture["fac_id"],
							"soc" => $rfaf["rfa_facture_soc"],
							"taille" => number_format((filesize($url_facture_form)/1024/1024),2)
						);
					}else{
						$data["pages"][0]=facture_data($d_facture['fac_id']);
			            if(empty($data["pages"][0])){
			                $erreur_txt="impossible d'afficher la page";
			            }else{
			                $pdf=model_facture_pdf($data);
			                if($pdf){

			                    // ON BLOQUE LA FACTURE
			                    $ConnFct->query("UPDATE Factures SET fac_blocage=1 WHERE fac_id=" . $d_facture['fac_id'] . ";");
								$pj[]=array(
									"nom" =>  $d_facture["fac_numero"] . ".pdf",
									"url" => $url_facture_form,
									"check" => true,
									"id" => $d_facture["fac_id"],
									"soc" => $rfaf["rfa_facture_soc"],
									"taille" => number_format((filesize($url_facture_form)/1024/1024),2)
								);
			                }else{
								$erreur_txt="Impossible de charger le pdf de la facture. Vérifiez la facture svp";
							}
			            }

					}
				}
				if(!empty($pj[0])){
					$sujet_facture = $pj[0]["nom"];
				}else{
					$sujet_facture = "";
				}
				if($d_relance['rel_etat_relance'] == 1){
					$sujet="Factures arrivant à échéance " . $sujet_facture;
					$contenu="Madame, Monsieur,<br><br>";
					$contenu=$contenu . "Veuillez trouver, ci-joint, le relevé de vos factures en nos comptes.<br><br>";
					$contenu=$contenu . "Nous vous rappelons que celle(s)-ci arrive(nt) à échéance dans les jours à venir.<br><br>";

					$contenu=$contenu . "Nous restons à votre entière disposition pour tout complément d'information.<br><br>";
					$contenu=$contenu . "Veuillez croire, Madame, Monsieur, en l'assurance de notre considération distinguée.<br><br>" . $d_societe['soc_nom'];
				}elseif($d_relance['rel_etat_relance'] == 2){
					$sujet="Factures en attente de règlement " . $sujet_facture;
					$contenu="Madame, Monsieur,<br><br>";
					$contenu=$contenu . "Sauf erreur ou omission de notre part, la mise à jour de votre compte client présente à ce jour un solde débiteur.<br><br>";
					$contenu=$contenu . "En effet, les factures ci-jointes n'ont pas encore été honorées.<br><br>";

					$contenu=$contenu . "L'échéance étant dépassée, nous vous demandons pour la bonne règle de nos écritures, de nous adresser le règlement";
					$contenu=$contenu . " par courrier, ou de nous informer en cas de litige.<br><br>";
					$contenu=$contenu . "Nous vous prions de croire, Madame, Monsieur, en l'assurance de notre considération distinguée.<br><br>" . $d_societe['soc_nom'];
				}elseif($d_relance['rel_etat_relance'] == 3){
					$sujet="Factures en attente de règlement " . $sujet_facture;
					$contenu="Madame, Monsieur,<br><br>";
					$contenu=$contenu . "Malgré nos différentes relances, les factures ci-jointes n'ont pas encore été honorées.<br><br>";
					$contenu=$contenu . "Nous vous prions de bien vouloir opérer au règlement dès réception de ce mail.<br><br>";

					$contenu=$contenu . "Nous vous prions de croire, Madame, Monsieur, en l'assurance de notre considération distinguée.<br><br>" . $d_societe['soc_nom'];

				}elseif($d_relance['rel_etat_relance'] == 4){
					$sujet="Factures en attente de règlement "  . $sujet_facture;
					$contenu="Madame, Monsieur,<br><br>";
					$contenu=$contenu . "Malgré nos différentes relances, les factures ci-jointes n'ont pas encore été honorées.<br><br>";
					$contenu=$contenu . "Nous vous prions de bien vouloir opérer au règlement dès réception de ce mail.<br><br>";

					$contenu=$contenu . "Nous vous prions de croire, Madame, Monsieur, en l'assurance de notre considération distinguée.<br><br>" . $d_societe['soc_nom'];

				}elseif($d_relance['rel_etat_relance'] == 5){
					$sujet="Mise en demeure " . $sujet_facture;
					$contenu="Madame, Monsieur,<br><br>";
					$contenu=$contenu . "Nous constatons malheureusement que votre société n'a toujours pas réglé le solde des factures détaillées ci dessous, arrivées à échéance.<br><br>";
					$contenu=$contenu . "Si dans un délai de 7 jours à compter de la première présentation de la lettre recommandée qui part ce jour, vous ne vous êtes toujours pas acquittés de votre obligation, nous saisirons la juridiction compétente afin d'obtenir le paiement des sommes susvisées.";
					$contenu=$contenu . "Nous vous prions de croire, Madame, Monsieur, en l'assurance de notre considération distinguée.<br><br>" . $d_societe['soc_nom'];

				}elseif($d_relance['rel_etat_relance'] == 6){
					$sujet="Contentieux " . $sujet_facture;
					$contenu="Madame, Monsieur,<br><br><br><br>" . $d_societe['soc_nom'];

				}else{
					$sujet="Factures en attente de règlement " . $sujet_facture;
					$contenu="Madame, Monsieur,<br><br><br><br>" . $d_societe['soc_nom'];
				}
			}

		}elseif($mail==5){

			$retour="commande_voir.php?commande=" . $_GET['commande'];
			$destinataire = array();
			$sql="SELECT com_contact_nom, com_contact_prenom, com_contact_mail FROM Commandes WHERE com_id=" . $_GET['commande'] . ";";
			$req=$Conn->query($sql);
			$d_commande=$req->fetch();

			if(!empty($d_commande)){
				$contenu="Bonjour " . $d_commande["com_contact_prenom"] . " " . $d_commande["com_contact_nom"] . ",";
				if(!empty($d_commande["com_contact_mail"])){
					$destinataire[]=array(
						"nom" => $d_commande["com_contact_nom"] . " " . $d_commande["com_contact_prenom"],
						"mail" => $d_commande["com_contact_mail"]
					);
				}
			}
			$sql="SELECT * FROM Commandes WHERE com_id=" . $_GET['commande'] . ";";
			$req=$Conn->query($sql);
			$commande=$req->fetch();
			if(empty($commande)){
				$erreur_txt="Impossible de charger les données sur la formation";
			}else{
				$sujet="Votre commande " . $commande["com_numero"];
			}

			if(empty($erreur_txt)){

				$contenu="Bonjour,<br/><br/><br/><br/><br/><br/>Cordialement.";

				// LES PIECES JOINTES DISPO

				$pj=array();

				// FACTURES

				$url_facture="Documents/Commandes/" . $commande['com_numero'] . ".pdf";
				// DONNEE POUR AFFICHAGE
				$data=array(
					"pages" => array()
				);
				// aller chercher la commande
				$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = " . $_GET['commande']);
				$req->execute();
				$commande= $req->fetch();
				$data["pages"][0]=commande_data($_GET['commande']);
				if(empty($data["pages"][0])){
					$erreur_txt="impossible d'afficher la page";
				}else{
					if(!file_exists("Documents/Commandes/" . $commande['com_numero'] . ".pdf")){
						$pdf=model_commande_pdf($data);
					}else{
						$pdf = 1;
					}


					if(!$pdf){
						$erreur_txt="impossible d'afficher la page";
					}
				}

				if(file_exists($url_facture)){
					$pj[]=array(
						"nom" => $commande['com_numero'] . ".pdf",
						"url" => $url_facture,
						"check" => false,
						"taille" => number_format((filesize($url_facture)/1024/1024),2)
					);
				}


			}

		}elseif($mail==6){

			$retour="client_voir.php?client=" . $_GET['client'] . "&tab14";
			$destinataire = array();
			$sql="SELECT * FROM Acces WHERE acc_ref=2 AND acc_ref_id=" . $_GET['acc_ref_id'] . ";";
			$req=$Conn->query($sql);
			$d_acces=$req->fetch();

			if(!empty($d_acces)){
				$sujet="Votre mot de passe pour l'espace client Si2P";
				$sql="SELECT * FROM Contacts WHERE con_id=" . $d_acces['acc_ref_id'] . ";";
				$req=$Conn->query($sql);
				$d_contact=$req->fetch();
				if(!empty($d_contact['con_mail'])){
					$email = $d_contact['con_mail'];
				}else{
					$email = $d_acces['acc_uti_ident'];
				}
				$contenu="Bonjour " . $d_contact["con_prenom"] . " " . $d_contact["con_nom"] . ",";
				if(!empty($email)){
					$destinataire[]=array(
						"nom" => $d_contact["con_prenom"] . " " . $d_contact["con_nom"],
						"mail" => $email
					);
				}
			}else{
				$erreur_txt="impossible d'afficher la page";
			}

			if(empty($erreur_txt)){

				$contenu="Bonjour,<br/><br/>
				Voici vos identifiants pour vous connecter à l'espace client <a href='https://si2p.eu'>Si2P</a>
				<br>
				<br/>
				<b>Identifiant :</b> " . $d_acces['acc_uti_ident'] . "<br>
				<b>Mot de passe :</b> " . $d_acces['acc_uti_passe'] . "
				<br/><br/><br/>Cordialement.";

				// LES PIECES JOINTES DISPO

				$pj=array();

				// FACTURES


				// DONNEE POUR AFFICHAGE
				$data=array(
					"pages" => array()
				);


			}

				// ATTESTATION FORMATION COMPLETE

		}elseif($mail==8){

			$action_retour = $action_client_id;
			if(!empty($_GET['action_retour'])){
				$action_retour = $_GET['action_retour'];
			}
			// Avis de passage

			$retour="action_voir.php?action=" . $action_id . "&action_client=" . $action_retour . "&societ=" . $conn_soc_id;

			$sql="SELECT acl_pro_libelle,acl_client,acl_confirme,cli_code,acl_facturation,cli_categorie,
			act_agence,acl_produit, act_adr_ref, act_adr_nom, act_adr_service, act_adr1, act_adr2, act_adr3, act_adr_cp, act_adr_ville
			FROM Actions_Clients,Clients,Actions WHERE acl_client=cli_id AND acl_action=act_id AND acl_id=" . $action_client_id . ";";
			$req=$ConnSoc->query($sql);
			$d_action_client=$req->fetch();
			if(empty($d_action_client)){
				$erreur_txt="Impossible de charger les données sur la formation";
			};

			if(empty($erreur_txt)){

				$client=$d_action_client["acl_client"];
				$inter = "";
				// info sur la societe

				if($d_action_client["acl_facturation"]==1){
					$mail_soc=4;
					$mail_age=4;
				}else{
					$mail_soc=$conn_soc_id;
					$mail_age=$d_action_client["act_agence"];
				}

				$soc_nom="";
				$soc_tel="";
				if(empty($mail_age)){

					$sql="SELECT soc_nom,soc_tel,soc_intra_inter FROM Societes WHERE soc_id=:societe;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":societe",$mail_soc);
					$req->execute();
					$d_societe=$req->fetch();
					if(!empty($d_societe)){
						$soc_nom=$d_societe["soc_nom"];
						$soc_tel=$d_societe["soc_tel"];
					}

				}else{
					$sql="SELECT soc_nom,soc_tel,age_tel, age_intra_inter FROM Societes,Agences WHERE soc_id=age_societe AND soc_id=:societe AND age_id=:agence;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":societe",$mail_soc);
					$req->bindParam(":agence",$mail_age);
					$req->execute();
					$d_societe=$req->fetch();
					if(!empty($d_societe)){
						$soc_nom=$d_societe["soc_nom"];
						if(!empty($d_societe["age_tel"])){
							$soc_tel=$d_societe["age_tel"];
						}else{
							$soc_tel=$d_societe["soc_tel"];
						}
					}
				}
				if(!empty($d_societe['soc_intra_inter'])){
					if($d_societe['soc_intra_inter']==0 OR $d_societe['soc_intra_inter'] == 2){
						$inter = $d_societe['soc_nom'] . " - Réseau Si2P";
					}

				}elseif(!empty($d_societe['age_intra_inter'])){
					if($d_societe['age_intra_inter']==0 OR $d_societe['age_intra_inter'] == 2){
						if (!empty($d_societe['age_nom'])) {
							$inter = $d_societe['age_nom'] . " - Réseau Si2P";
						} else {
							$inter = $d_societe['soc_nom'] . " - Réseau Si2P";
						}
					}

				}
				// Date de présence

				include('modeles/mod_planning_periode_lib.php');
				include('modeles/mod_planning_periode.php');
				$planning_txt = "";
				$sql="SELECT pda_date,pda_demi FROM Plannings_Dates,Actions_Clients_Dates WHERE pda_id=acd_date AND acd_action_client=:action_client_id ORDER BY pda_date,pda_demi;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":action_client_id",$action_client_id);
				$req->execute();
				$d_dates=$req->fetchAll();

				if(!empty($d_dates)){
					$planning_txt=mb_strtolower(planning_periode($d_dates));
				}


				if($d_action_client["acl_confirme"]==1){
					if($d_action_client["cli_categorie"] == 3){
						$sujet="Rappel: formation " . $soc_nom . " planifiées prochainement";
					}else{
						$sujet=$d_action_client["cli_code"] . " // Rappel: formation " . $soc_nom . " planifiées prochainement";
					}

					if($d_action_client['act_adr_ref'] == 2){

						// AGENCE
						$contenu="Madame,Monsieur,<br/>
						<br/>
						Nous vous rappelons votre inscription pour la " . $d_action_client["acl_pro_libelle"] . " qui aura lieu " . $planning_txt . " dans nos locaux :<br>
						<br/>
						" . $d_action_client['act_adr_nom'] . "<br>";
						if(!empty($d_action_client['act_adr_service'])){
							$contenu.=$d_action_client['act_adr_service'] . "<br/>";
						}
						$contenu.="
						" . $d_action_client['act_adr1'] . "
						<br/>";
						if(!empty($d_action_client['act_adr2'])){
							$contenu.=$d_action_client['act_adr2'] . "<br/>";
						}
						if(!empty($d_action_client['act_adr3'])){
							$contenu.=$d_action_client['act_adr3'] . "<br/>";
						}
						$contenu.=$d_action_client['act_adr_cp'] . " " . $d_action_client['act_adr_ville'] . "<br>

						<br>Merci de confirmer la réception de ce mail en cliquant sur le lien ci-dessous.";
					}else{
						$contenu="Madame,Monsieur,<br/>
						<br/>
						Nous vous rappelons notre venue pour la " . $d_action_client["acl_pro_libelle"] . " qui aura lieu " . $planning_txt . ".
						<br/>
						<br/>
						Merci de confirmer la réception de ce mail en cliquant sur le lien ci-dessous.";
					}


				}else{


					if($d_action_client["cli_categorie"] == 3){
						$sujet="Rappel: formations " . $soc_nom . " en attente de validation";
					}else{
						$sujet=$d_action_client["cli_code"] . " // Rappel: formations " . $soc_nom . " en attente de validation";
					}

					$contenu="Madame,Monsieur,
					<br/>
					<br/>
					Nous vous rappelons l'option mise " . $planning_txt;

					$contenu.=" pour la " . $d_action_client["acl_pro_libelle"] . "<br/>
					Merci de bien vouloir nous faire savoir si votre décision est prise et de confirmer la réception de ce mail en cliquant sur le lien ci-dessous.<br/>";


				}

				$contenu_bis="Nous restons à votre disposition au " . $soc_tel . ".<br/><br/>
				Cordialement.";
			}

			if(empty($erreur_txt)){

				// LES PIECES JOINTES DISPO

				$pj=array();

				// CONCOC
				$url_convoc=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Convocations/convoc_". $action_client_id . ".pdf";
				if(file_exists($url_convoc)){
					$pj[]=array(
						"nom" => "Convocation.pdf",
						"url" => $url_convoc,
						"check" => false,
						"taille" => number_format((filesize($url_convoc)/1024/1024),2)
					);
				}
				// CONVENTIONS
				$sql="SELECT con_numero FROM Conventions WHERE con_action_client=" . $action_client_id . " AND con_action=" . $action_id . ";";
				$req=$ConnSoc->query($sql);
				$d_convention=$req->fetch();
				if(!empty($d_convention)){
					$url_convention=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Conventions/" . $d_convention["con_numero"] . ".pdf";
					if(file_exists($url_convention)){
						$pj[]=array(
							"nom" => $d_convention["con_numero"] . ".pdf",
							"url" => $url_convention,
							"check" => false,
							"taille" => number_format((filesize($url_convention)/1024/1024),2)
						);
					}
				}
				// ETECH
				$sql="SELECT * FROM Documents LEFT JOIN Documents_Produits ON (Documents.doc_id = Documents_Produits.dpr_document)
				WHERE dpr_produit = " . $d_action_client['acl_produit'];

				$req=$Conn->query($sql);
				$d_documents_etech=$req->fetchAll();
				if(!empty($d_documents_etech)){
					foreach($d_documents_etech as $doe){
						$url_etech=$_SERVER["DOCUMENT_ROOT"] . "/Documents/etech/" . $doe['doc_nom'];
						if(file_exists($url_etech)){
							$pj[]=array(
								"nom" => $doe['doc_nom'],
								"url" => $url_etech,
								"check" => false,
								"taille" => number_format((filesize($url_etech)/1024/1024),2)
							);
						}
					}

				}


			}



		}
	}


	// LE CLIENT
	if(!empty($client)){
		$sql="SELECT con_nom,con_prenom,con_mail FROM Contacts WHERE con_ref=1 AND con_ref_id=" . $client . " AND NOT con_mail='' AND NOT ISNULL(con_mail);";
		$req=$Conn->query($sql);
		$d_contacts=$req->fetchAll();
		if(!empty($d_contacts)){
			foreach($d_contacts as $contact){
				$destinataire[]=array(
					"nom" => $contact["con_nom"] . " " . $contact["con_prenom"],
					"mail" => $contact["con_mail"]
				);
			}
		}
	}
 ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<!-- Plugin -->
		<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
		<link href="vendor/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css">
		<link href="vendor/plugins/select2/css/core.css" rel="stylesheet" type="text/css">

		<!-- PERSO -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">


		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >


		</style>
	</head>
	<body class="sb-top sb-top-sm document-affiche">
<?php 	if(empty($erreur_txt)){ ?>
			<form method="post" action="mail.php" >
				<div>
					<input type="hidden" name="retour" value="<?=$retour?>" />
					<input type="hidden" name="facture_relance" value="<?=$facture_relance?>" />
					<?php if($mail == 8){ ?>
						<input type="hidden" name="accuse" value="1" />
						<input type="hidden" name="action_client_id" value="<?= $action_client_id ?>" />
						<input type="hidden" name="conn_soc_id" value="<?= $conn_soc_id ?>" />
						<input type="hidden" name="inter" value="<?= $inter ?>" />
					<?php }?>
				</div>
				<div id="main">
			<?php	include "includes/header_def.inc.php";
					?>
					<!-- Start: Content-Wrapper -->
					<section id="content_wrapper">


						<!-- Begin: Content -->
						<section id="content" class="animated fadeIn">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											<div class="row" >
												<!-- zone mail -->
												<div class="col-md-8" >
													<div class="row" >
														<div class="col-md-2 text-right mt10" >Destinataires connus: </div>
														<div class="col-md-10" >
													<?php	if(!empty($destinataire)){ ?>
																<select name="destinataire[]" multiple class="select2" >
																	<option value="" >Destinataire ...</option>
												<?php				foreach($destinataire as $dest){
																		echo("<option value='" . $dest["nom"] . ";" . $dest["mail"] . "' >" . $dest["nom"] . " " . $dest["mail"] . "</option>");
																	}	 ?>
																</select>
													<?php	} ?>
														</div>
													</div>
													<div class="row mt5" >
														<div class="col-md-2 text-right mt10" >Saisie libre : </div>
														<div class="col-md-10" >
															<input type="text" name="destinatiaire_libre" id="destinatiaire_libre" class="gui-input" placeholder="Saisie libre" maxlength="255" />
															<small>Les adresses mails doivent-être séparées par un ;</small>
														</div>
													</div>
													<div class="row mt5" >
														<div class="col-md-2 text-right mt10" >Sujet : </div>
														<div class="col-md-10" >
															<input type="text" name="sujet" id="sujet" class="gui-input" placeholder="Sujet du mail" value="<?=$sujet?>" size="50" maxlength="50" />
														</div>
													</div>
													<div class="row mt5" >
														<div class="col-md-2" ></div>
														<div class="col-md-10" >
															 <textarea class="gui-textarea summernote" id="texte" name="texte" placeholder="Message" >
															 <?=$contenu?>
															 </textarea>
														</div>
													</div>
											<?php	if(isset($contenu_bis)){ ?>

														<div class="row mt5" >
															<div class="col-md-10 col-md-offset-2 text-center" >
																<p>Contenu automatique généré par ORION.
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-10 col-md-offset-2" >
																 <textarea class="gui-textarea summernote" id="texte_bis" name="texte_bis" placeholder="Message" >
																 <?=$contenu_bis?>
																 </textarea>
															</div>
														</div>
											<?php	}	?>

													<div class="row mt5" >
														<div class="col-md-2 text-right mt5" >Option :</div>
														<div class="col-md-2" >
															<label class="option">
																<input type="checkbox" name="copie" value="on" <?php if($mail==8) echo("checked"); ?> >
																<span class="checkbox"></span>Copie
															</label>
														</div>
														<div class="col-md-4" >
															<label class="option">
																<input type="checkbox" name="signature" value="on" checked>
																<span class="checkbox"></span>Afficher signature
															</label>
														</div>
													</div>

												</div>
												<!-- fin zone mail -->

												<div class="col-md-4" >
											<?php	if(!empty($pj)){ ?>
														<div class="section-divider" >
															<span>Pièces jointes</span>
														</div>

														<table class="table" >
															<thead>
																<tr>
																	<th>
																		<label class="option" >
																			<input type="checkbox" class="check-all" id="pj_all"/>
																			<span class="checkbox"></span>
																		</label>
																	</th>
																	<th colspan="2" >&nbsp;</th>
																</tr>
															</thead>
															<tbody>
												<?php			foreach($pj as $k=>$p){ ?>
																	<tr>
																		<td>
																			<label class="option" >
																				<input type="checkbox" name="pj[]" data-ligne="<?=$k?>" class="pj" <?php if($p["check"]) echo("checked"); ?> value="<?=$k?>" />
																				<span class="checkbox"></span>
																			</label>
																			<input type="hidden" id="taille_<?=$k?>" value="<?=$p["taille"]?>" />
																			<input type="hidden" name="pj_nom_<?=$k?>" value="<?=$p["nom"]?>" />
																			<input type="hidden" name="pj_url_<?=$k?>" value="<?=$p["url"]?>" />
																			<?php if(!empty($p["id"])){ ?>
																				<input type="hidden" name="facture_<?=$k?>" value="<?=$p["id"]?>" />
																				<input type="hidden" name="facture_soc_<?=$k?>" value="<?=$p["soc"]?>" />

																			<?php }?>
																		</td>
																		<td><?=$p["nom"]?></td>
																		<td>
																			<a href="<?=$p["url"]?>" target="_blank" ><i class="fa fa-file-pdf-o" ></i></a>
																		</td>
																		<td><?=$p["taille"]?> Mo</td>
																	</tr>
												<?php			} ?>
															</tbody>
															<tfoot>
																<tr>
																	<th colspan="3" >Poids des pièces selectionnées :</th>
																	<th>
																		<input type="text" id="taille_total" value="0" size="5" maxlength="5" readonly /> Mo
																	</th>
																</tr>
																<tr>
																	<th colspan="3" >&nbsp;</th>
																	<th>5 Mo Max</th>
																</tr>
															</tfoot>
														</table>
										<?php		} ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</section>
						<!-- End: Content -->
					</section>
				</div>
				<!-- End: Main -->

				<footer id="content-footer" class="affix">
					<div class="row">
						<div class="col-xs-3 footer-left">
							<a href="<?=$retour?>" class="btn btn-default btn-sm" role="button">
								<span class="fa fa-long-arrow-left"></span>
								<span class="hidden-xs">Retour</span>
							</a>
						</div>
						<div class="col-xs-6 footer-middle">&nbsp;</div>
						<div class="col-xs-3 footer-right">
							<button type="submit" class="btn btn-success btn-sm" >
								<span class="fa fa-envelope-o"></span>
								<span class="hidden-xs">Envoyer</span>
							</a>
						</div>
					</div>
				</footer>
			</form>
<?php	}else{ ?>

			<p class="alert alert-danger text-center" >
				<?=$erreur_txt?>
			</p>

<?php	}

		include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/summernote/summernote.min.js"></script>
		<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<script type="text/javascript">
			jQuery(document).ready(function () {
				$(".pj").click(function(){
					calculer_poids();
				});
				$("#pj_all").click(function(){
					calculer_poids();
				});
			});

			function calculer_poids(){
				poid=0;
				$(".pj").each(function(){
					if($(this).is(":checked")){
						poid=poid + 1*$("#taille_" + $(this).data("ligne")).val();
					}
				});
				$("#taille_total").val(poid);
			}
		</script>
	</body>
</html>
