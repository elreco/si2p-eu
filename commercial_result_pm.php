<?php
$menu_actif = 1;
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");;
include_once("includes/connexion_soc.php");
include_once("includes/connexion_fct.php");
include_once("modeles/mod_parametre.php");

// PARAMETRE
$commercial=0;
if(isset($_GET["commercial"])){
	if(!empty($_GET["commercial"])){
		$commercial=intval($_GET["commercial"]);
	}
}
$com_type=0;
if(isset($_GET["com_type"])){
	if(!empty($_GET["com_type"])){
		$com_type=intval($_GET["com_type"]);
	}
}
$agence=0;
if(isset($_GET["agence"])){
	if(!empty($_GET["agence"])){
		$agence=intval($_GET["agence"]);
	}
}
$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}
$pm_type=0;
if(isset($_GET["pm_type"])){
	if(!empty($_GET["pm_type"])){
		$pm_type=intval($_GET["pm_type"]);
	}
}


// PERSONNE CONNECTE
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_agence=0;
if(!empty($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}elseif(!empty($_GET['agence'])){
	$acc_agence = $_GET['agence'];
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	} 
}

// CONTROLE D'ACCESS

if($_SESSION["acces"]["acc_profil"]==3 AND $commercial==0){
	header("location:deconnect.php");
	die();
}elseif($agence>0){
	if($acc_agence>0 AND $agence!=$acc_agence){
		header("location:deconnect.php");
		die();
	}
}elseif(empty($agence) AND empty($commercial)){
	if(!isset($_SESSION["acces"]["acc_liste_societe"][$acc_societe . "-0"])){
		header("location:deconnect.php");
		die();
	}
}
$aff_geo_gc=false;
$aff_ca=false;
if($_SESSION['acces']['acc_service'][1]==1 OR $_SESSION['acces']["acc_profil"]==11){
	$aff_geo_gc=true;
	$aff_ca=false;
}

// VALEUR PAR DEFAUT

if($exercice==0){
	if(date("n")<4){
		$exercice=date("Y")-1;
	}else{
		$exercice=date("Y");
	}
}
$exercice_fin=$exercice+1;

// CATEGORIE
$d_categories=array(
	"0" => "Divers"
);
$sql="SELECT pca_id,pca_libelle FROM Produits_Categories ORDER BY pca_id;";
$req=$Conn->query($sql);
$d_result_cat=$req->fetchAll();
if(!empty($d_result_cat)){
	foreach($d_result_cat as $cat){
		$d_categories[$cat["pca_id"]]=$cat["pca_libelle"];
	}	
}
// Famille
$d_familles=array();
$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles  ORDER BY pfa_id;";
$req=$Conn->query($sql);
$d_result_fam=$req->fetchAll();
if(!empty($d_result_fam)){
	foreach($d_result_fam as $fam){
		$d_familles[$fam["pfa_id"]]=$fam["pfa_libelle"];
	}	
}
// Sous-Famille
$d_s_familles=array();
$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles ORDER BY psf_id;";
$req=$Conn->query($sql);
$d_result_s_fam=$req->fetchAll();
if(!empty($d_result_s_fam)){
	foreach($d_result_s_fam as $s_fam){
		$d_s_familles[$s_fam["psf_id"]]=$s_fam["psf_libelle"];
	}	
}
// Sous-Sous-Famille
$d_s_s_familles=array();
$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles ORDER BY pss_id;";
$req=$Conn->query($sql);
$d_result_s_s_fam=$req->fetchAll();
if(!empty($d_result_s_s_fam)){
	foreach($d_result_s_s_fam as $s_s_fam){
		$d_s_s_familles[$s_s_fam["pss_id"]]=$s_s_fam["pss_libelle"];
	}	
}


// LE COMMERCIAL
if($commercial>0){
	$sql="SELECT com_label_1,com_label_2,com_ref_1,com_agence FROM Commerciaux WHERE com_id=" . $commercial;
	if($_SESSION["acces"]["acc_profil"]==3){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif($acc_agence>0){
		$sql.=" AND com_agence=" . $acc_agence;
	}
	$sql.=";";
	$req=$ConnSoc->query($sql);
	$d_commercial=$req->fetch();
	if(!empty($d_commercial)){
		$titre="Résultat de " . $d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"] . " pour l'exercice " . $exercice . "/" . $exercice_fin; 
		$agence=$d_commercial["com_agence"];
	}else{
		echo("impossible d'afficher la page!");
		die();
	}
}elseif($com_type>0){
	

	if($agence>0){
		$sql="SELECT age_nom,age_societe FROM Agences WHERE age_id=" . $agence . ";";
		$req=$Conn->query($sql);
		$d_agence=$req->fetch();
		if(!empty($d_agence)){
			
			if($d_agence["age_societe"]!=$acc_societe){
				header("location:deconnect.php");
				die();
			}
			$agence_libelle = $d_agence["age_nom"];
		}
		
	}else{
		$agence_libelle = "";
	}
	if($com_type==1){
		$titre="Résultat Vendeurs " . $agence_libelle . " pour l'exercice " . $exercice . "/" . $exercice_fin; 
		
	}elseif($com_type==2){
		$titre="Résultat Revendeurs " . $agence_libelle . " pour l'exercice " . $exercice . "/" . $exercice_fin; 
		
	}elseif($com_type==3){
		$titre="Résultat Indivis " . $agence_libelle . " pour l'exercice " . $exercice . "/" . $exercice_fin; 
		
	}
	
}elseif($agence>0){
	$sql="SELECT age_nom,age_societe FROM Agences WHERE age_id=" . $agence . ";";
	$req=$Conn->query($sql);
	$d_agence=$req->fetch();
	if(!empty($d_agence)){
		
		if($d_agence["age_societe"]!=$acc_societe){
			header("location:deconnect.php");
			die();
		}
		$titre="Résultat " . $d_agence["age_nom"] . " pour l'exercice " . $exercice . "/" . $exercice_fin; 
	}	
}else{
	$sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
	$req=$Conn->query($sql);
	$d_societe=$req->fetch();
	if(!empty($d_societe)){
		$titre="Résultat " . $d_societe["soc_nom"] . " pour l'exercice " . $exercice . "/" . $exercice_fin; 
	}	
}

// LES OBJECTIFS

// [0] clé divers cree ici au cas ou pas d'objectifs et du CA
$resultats=array();

$cob_valide=false;
$cob_valide_date=null;
$cob_valide_utilisateur=0;

$sql="SELECT cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille,
SUM(cob_objectif_1) AS objectif_1,SUM(cob_objectif_2) AS objectif_2,SUM(cob_objectif_3) AS objectif_3,
SUM(cob_objectif_4) AS objectif_4,SUM(cob_objectif_5) AS objectif_5,SUM(cob_objectif_6) AS objectif_6,
SUM(cob_objectif_7) AS objectif_7,SUM(cob_objectif_8) AS objectif_8,SUM(cob_objectif_9) AS objectif_9,
SUM(cob_objectif_10) AS objectif_10,SUM(cob_objectif_11) AS objectif_11,SUM(cob_objectif_12) AS objectif_12,
SUM(cob_objectif_total) AS objectif_total,cob_autre";
if($commercial>0){
	// seulement si on consulte un objectif commercial => sinon mélange de valide et non valide = bug conso
	$sql.=",cob_valide,cob_valide_date,cob_valide_utilisateur";
}
$sql.=" FROM Commerciaux 
LEFT JOIN Commerciaux_Objectifs ON (Commerciaux.com_id=Commerciaux_Objectifs.cob_commercial)
WHERE com_archive=0 AND cob_exercice=" . $exercice . " AND cob_categorie=1";
// restriction d'accès 
if($_SESSION['acces']["acc_profil"]==3){
	$sql.=" AND com_ref_1=" . $acc_utilisateur;
}elseif($acc_agence>0){
	$sql.=" AND com_agence=" . $acc_agence;
}
// gestion des filtres
if($commercial>0){
	$sql.=" AND com_id=" . $commercial;
}elseif($com_type>0){
	$sql.=" AND com_type=" . $com_type;
}elseif($agence>0){
	$sql.=" AND com_agence=" . $agence;
}
$sql.=" GROUP BY cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille,cob_autre";
if($commercial>0){
	$sql.=",cob_valide,cob_valide_date,cob_valide_utilisateur";
}
$sql.=" ORDER BY SUM(cob_objectif_total) DESC;";
$req=$ConnSoc->query($sql);

$d_objectifs=$req->fetchAll();
if(!empty($d_objectifs)){
	foreach($d_objectifs as $obj){
		if($commercial>0){
			$cob_valide=$obj["cob_valide"];
			$cob_valide_date=$obj["cob_valide_date"];
			$cob_valide_utilisateur=$obj["cob_valide_utilisateur"];
		}
		
		$lib_detail="";
		$libelle="";
		if(!empty($obj["cob_sous_sous_famille"])){
			$lib_detail=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]] . "/" .$d_s_familles[$obj["cob_sous_famille"]] . "/" .$d_s_s_familles[$obj["cob_sous_sous_famille"]];	
			$libelle=$d_s_s_familles[$obj["cob_sous_sous_famille"]];
		}elseif(!empty($obj["cob_sous_famille"])){
			$lib_detail=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]] . "/" .$d_s_familles[$obj["cob_sous_famille"]];
			$libelle=$d_s_familles[$obj["cob_sous_famille"]];
		}elseif(!empty($obj["cob_famille"])){
			$lib_detail=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]];	
			$libelle=$d_familles[$obj["cob_famille"]];
		}else{
			$libelle=$d_categories[$obj["cob_categorie"]];		
		}
		$resultats[$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]=array(
			"objectif" => array(
				"mois_1" => $obj["objectif_1"],
				"mois_2" => $obj["objectif_2"],
				"mois_3" => $obj["objectif_3"],
				"mois_4" => $obj["objectif_4"],
				"mois_5" => $obj["objectif_5"],
				"mois_6" => $obj["objectif_6"],
				"mois_7" => $obj["objectif_7"],
				"mois_8" => $obj["objectif_8"],
				"mois_9" => $obj["objectif_9"],
				"mois_10" => $obj["objectif_10"],
				"mois_11" => $obj["objectif_11"],
				"mois_12" => $obj["objectif_12"],
				"total" => $obj["objectif_total"],
			),
			"resultat" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"resultat_geo" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"resultat_gc" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"unit_pm" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"unit_pm_geo" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"unit_pm_gc" => array(
				"mois_1" => 0,
				"mois_2" => 0,
				"mois_3" => 0,
				"mois_4" => 0,
				"mois_5" => 0,
				"mois_6" => 0,
				"mois_7" => 0,
				"mois_8" => 0,
				"mois_9" => 0,
				"mois_10" => 0,
				"mois_11" => 0,
				"mois_12" => 0,
				"total" => 0
			),
			"libelle" => $libelle,
			"lib_detail" => $lib_detail,
			"autre" => $obj["cob_autre"]
		);
	} 
}


// SOURCES DELTA IDENTIFIES
/* causes d'un ecrant entre le CA de tableau com et le CA du tableau PM
 
- commercial de l'action et commercial de la facture différent
- action se terminant le 01/10 mais facturé le 30/09
- intra multi-client mélangeant de la facturation direct et de la facturation CN
- intra multi-client mélangeant de la facturation interco et de la facturation direct
*/

// ACTIONS
// pour le PM INTRA, on se base sur le planning, donc sur les actions.

$sql="SELECT act_id,act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille";
$sql.=",cli_categorie";
$sql.=",SUM(fli_montant_ht) AS montant_ht";
//$sql.=",fac_cli_categorie";
$sql.=",MONTH(Plannings.act_date_fin) AS mois,Plannings.nb_demi_j";
if($pm_type==2){
	$sql.=",acl_id,acl_for_nb_sta";
}
$sql.=" FROM Actions";
$sql.=" INNER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)";
$sql.=" INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND Actions.act_agence=Clients.cli_agence)";
$sql.=" INNER JOIN (
	SELECT COUNT(pda_id) AS nb_demi_j, MAX(pda_date) AS act_date_fin,pda_ref_1 FROM Plannings_Dates WHERE pda_type=1 GROUP BY pda_ref_1 
) AS Plannings ON (Actions.act_id = Plannings.pda_ref_1)";

$sql.=" LEFT OUTER JOIN Factures_Lignes ON (Actions_Clients.acl_id=Factures_Lignes.fli_action_client AND fli_action_cli_soc=" . $acc_societe . ")";
//$sql.=" LEFT OUTER JOIN Factures ON (Factures.fac_id=Factures_Lignes.fli_facture)";
$sql.=" LEFT OUTER JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)";
$sql.=" WHERE (acl_facture OR acl_non_fac) AND NOT acl_facturation=1 AND NOT acl_ca_nc";
$sql.=" AND (cli_interco_soc=0 OR ISNULL(cli_interco_soc) )";
$sql.=" AND NOT act_archive";

$sql.=" AND Plannings.act_date_fin>='" . $exercice . "-04-01' AND Plannings.act_date_fin<='" . intval($exercice+1) . "-03-31'";
$sql.=" AND act_pro_type=" . $pm_type;

if($_SESSION['acces']["acc_profil"]==3){
	$sql.=" AND com_ref_1=" . $acc_utilisateur;
}elseif($acc_agence>0){
	$sql.=" AND com_agence=" . $acc_agence;
}
if($commercial>0){
	$sql.=" AND com_id=" . $commercial;
}elseif($com_type>0){
	$sql.=" AND com_type=" . $com_type;
}elseif($agence>0){
	$sql.=" AND act_agence=" . $agence;
}

//$sql.=" AND act_pro_sous_famille=4 AND MONTH(Plannings.act_date_fin)=5";
$sql.=" GROUP BY act_id,act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille";
$sql.=",cli_categorie";
//$sql.=",fac_cli_categorie";
if($pm_type==2){
	$sql.=",acl_id";
}
$sql.=" ORDER BY act_id;";
//echo($sql);
//echo("<br/>");
$req=$ConnSoc->query($sql);
$d_realises=$req->fetchAll(PDO::FETCH_ASSOC);
if(!empty($d_realises)){
	
	/*echo("<pre>");
		print_r($d_realises);
	echo("</pre>");
	die();*/
	foreach($d_realises as $realise){
		
		$cat_id=0;
		if(!empty($realise["act_pro_categorie"])){
			$cat_id=$realise["act_pro_categorie"];
		}
		$fam_id=0;
		if(!empty($realise["act_pro_famille"])){
			$fam_id=$realise["act_pro_famille"];
		}
		$s_fam_id=0;
		if(!empty($realise["act_pro_sous_famille"])){
			$s_fam_id=$realise["act_pro_sous_famille"];
		}
		$s_s_fam_id=0;
		if(!empty($realise["act_pro_sous_sous_famille"])){
			$s_s_fam_id=$realise["act_pro_sous_sous_famille"];
		}
		$mois=0;
		if(!empty($realise["mois"])){
			$mois=$realise["mois"];
		}
		$montant_ht=0;
		if(!empty($realise["montant_ht"])){
			$montant_ht=$realise["montant_ht"];
		}
		
		$unit_pm=0;
		if($pm_type==1){
			if(!empty($realise["nb_demi_j"])){
				$unit_pm=$realise["nb_demi_j"];
			}
		}else{
			if(!empty($realise["acl_for_nb_sta"])){
				$unit_pm=$realise["acl_for_nb_sta"];
			}
		}
		
		// dans le cas d'une action non fac, nous n'avons pas fac_cli_categorie donc on utilise cli_categorie
		// par contre si fac_cli_categorie est renseigné, on le priorise car il est figé. Le PM de changera pas entre GC et GEO en cas de changement d'affectation du client
		$cli_categorie=$realise["cli_categorie"];
		/*if(!empty($realise["fac_cli_categorie"])){
			$cli_categorie=$realise["fac_cli_categorie"];
		}*/
		
		if(!empty($resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["objectif"]["total"])){
			$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat"]["total"]+=$montant_ht;
			
			$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["unit_pm"]["mois_" . $mois]+=$unit_pm;
			$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["unit_pm"]["total"]+=$unit_pm;
			
			if($cli_categorie==2){
				$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat_gc"]["total"]+=$montant_ht;
				
				$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["unit_pm_gc"]["mois_" . $mois]+=$unit_pm;
				$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["unit_pm_gc"]["total"]+=$unit_pm;
			}else{
				$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["resultat_geo"]["total"]+=$montant_ht;
				
				$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["unit_pm_geo"]["mois_" . $mois]+=$unit_pm;
				$resultats[$cat_id][$fam_id][$s_fam_id][$s_s_fam_id]["unit_pm_geo"]["total"]+=$unit_pm;
			}
			
		}elseif(!empty($resultats[$cat_id][$fam_id][$s_fam_id][0]["objectif"]["total"])){
			$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat"]["total"]+=$montant_ht;
			
			$resultats[$cat_id][$fam_id][$s_fam_id][0]["unit_pm"]["mois_" . $mois]+=$unit_pm;
			$resultats[$cat_id][$fam_id][$s_fam_id][0]["unit_pm"]["total"]+=$unit_pm;
			
			if($cli_categorie==2){
				$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat_gc"]["total"]+=$montant_ht;
				
				$resultats[$cat_id][$fam_id][$s_fam_id][0]["unit_pm_gc"]["mois_" . $mois]+=$unit_pm;
				$resultats[$cat_id][$fam_id][$s_fam_id][0]["unit_pm_gc"]["total"]+=$unit_pm;
				
			}else{
				$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][$fam_id][$s_fam_id][0]["resultat_geo"]["total"]+=$montant_ht;
				
				$resultats[$cat_id][$fam_id][$s_fam_id][0]["unit_pm_geo"]["mois_" . $mois]+=$unit_pm;
				$resultats[$cat_id][$fam_id][$s_fam_id][0]["unit_pm_geo"]["total"]+=$unit_pm;
			}		
			
		}elseif(!empty($resultats[$cat_id][$fam_id][0][0]["objectif"]["total"])){
			$resultats[$cat_id][$fam_id][0][0]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats[$cat_id][$fam_id][0][0]["resultat"]["total"]+=$montant_ht;
			
			$resultats[$cat_id][$fam_id][0][0]["unit_pm"]["mois_" . $mois]+=$unit_pm;
			$resultats[$cat_id][$fam_id][0][0]["unit_pm"]["total"]+=$unit_pm;
			
			if($cli_categorie==2){
				$resultats[$cat_id][$fam_id][0][0]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][$fam_id][0][0]["resultat_gc"]["total"]+=$montant_ht;
				
				$resultats[$cat_id][$fam_id][0][0]["unit_pm_gc"]["mois_" . $mois]+=$unit_pm;
				$resultats[$cat_id][$fam_id][0][0]["unit_pm_gc"]["total"]+=$unit_pm;
			}else{
				$resultats[$cat_id][$fam_id][0][0]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][$fam_id][0][0]["resultat_geo"]["total"]+=$montant_ht;
				
				$resultats[$cat_id][$fam_id][0][0]["unit_pm_geo"]["mois_" . $mois]+=$unit_pm;
				$resultats[$cat_id][$fam_id][0][0]["unit_pm_geo"]["total"]+=$unit_pm;
			}

		}elseif(!empty($resultats[$cat_id][0][0][0]["objectif"]["total"])){
			$resultats[$cat_id][0][0][0]["resultat"]["mois_" . $mois]+=$montant_ht;
			$resultats[$cat_id][0][0][0]["resultat"]["total"]+=$montant_ht;
			
			$resultats[$cat_id][0][0][0]["unit_pm"]["mois_" . $mois]+=$unit_pm;
			$resultats[$cat_id][0][0][0]["unit_pm"]["total"]+=$unit_pm;
			
			if($cli_categorie==2){
				$resultats[$cat_id][0][0][0]["resultat_gc"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][0][0][0]["resultat_gc"]["total"]+=$montant_ht;
				
				$resultats[$cat_id][0][0][0]["unit_pm_gc"]["mois_" . $mois]+=$unit_pm;
				$resultats[$cat_id][0][0][0]["unit_pm_gc"]["total"]+=$unit_pm;
			}else{
				$resultats[$cat_id][0][0][0]["resultat_geo"]["mois_" . $mois]+=$montant_ht;
				$resultats[$cat_id][0][0][0]["resultat_geo"]["total"]+=$montant_ht;
				
				$resultats[$cat_id][0][0][0]["unit_pm_geo"]["mois_" . $mois]+=$unit_pm;
				$resultats[$cat_id][0][0][0]["unit_pm_geo"]["total"]+=$unit_pm;
			}
			
		}
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#zone_print{
				display:none;
			}
			.objectif{
				color:#AA00AA;
			}
			.resultat{
				font-weight:bold;
			}
			.resultat a{
				color:#000;
			}
			.ligne-strip{
				background-color:#eee;
			}
			.taux-atteinte{
				color:blue;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 landscape;
				margin:5mm;
			}
			html{
				background-color:#fff!important
			}
			body{
				background-color:#fff!important
			}
			#zone_print{
				background-color:#fff!important;
				font-size:8pt;
			}
		</style>
	</head>

	<body class="sb-top sb-top-sm" >
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php		if(empty($d_objectifs)){ ?>
						<p class="alert alert-danger text-center" >
							Objectifs non disponible!
						</p>
		<?php		} ?>
						
						
					<form method="get" action="commercial_result.php" id="filtre" >
						<div class="admin-form theme-primary mb25">
							<div class="row" >
								<div class="col-md-offset-3 col-md-6" >
									<input type="hidden" name="commercial" value="<?=$commercial?>" />
									<input type="hidden" name="com_type" value="<?=$com_type?>" />
									<input type="hidden" name="agence" value="<?=$agence?>" />					
									<label for="exercice" >Choix de l'exercice :</label>
									<select class="select" id="exercice" name="exercice" >
								<?php	for($i=2017;$i<=intval(date("Y")+1);$i++){
											if($i==$exercice){
												echo("<option value='" . $i . "' selected >" . $i . "/" . intval($i+1) . "</option>");
											}else{
												echo("<option value='" . $i . "' >" . $i . "/" . intval($i+1) . "</option>");	
											} 
										}?>
									</select>										
								</div>
							</div>
						</div>
					</form>
					
					<div class="row" >
						<div class="col-md-12" id="page_print" >
						
							<div class="panel" >
								<div class="panel-body br-n">
									<h1><?=$titre?></h1>
									<div class="table-responsive">
										<table class="table" >
											<thead>
												<tr class="dark2" >
													<th colspan="2" >&nbsp;</th>
													<th>Avril</th>
													<th>Mai</th>
													<th>Juin</th>
													<th>Juillet</th>
													<th>Août</th>
													<th>Septembre</th>
													<th>Octobre</th>
													<th>Novembre</th>
													<th>Décembre</th>
													<th>Janvier</th>
													<th>Fevrier</th>							
													<th>Mars</th>
													<th>Total</th>									
												</tr>
											</thead>					
								<?php		$numLigne=0;
											$aff_conso=false;
											if(!empty($resultats)){?>													
											
												<tbody>
								<?php				foreach($resultats as $cat => $categorie){ 
														if($cat!="conso" AND $cat!="total" AND $cat!=0){
															foreach($categorie as $fam => $famille){
																foreach($famille as $s_fam => $s_famille){
																	foreach($s_famille as $s_s_fam => $s_s_famille){
																		
																		if($s_s_famille["autre"]==0){
																			if(!empty($s_s_famille["objectif"]["total"])){ 
																				
																				$nb_row=1;																										
																				if($aff_geo_gc){
																					$nb_row=$nb_row+2;
																				}																				
																			
																				$numLigne++;
																				$classLigne="";
																				if($numLigne % 2 ==0){
																					$classLigne="ligne-strip";	
																				} 
																				if($s_s_famille["autre"]){
																					$classLigne.=" conso-detail hidden";	
																				} ?>
																				<tr class="<?=$classLigne?>" style="page-break-after:avoid!important;" >
																					<th rowspan="<?=$nb_row?>" style="page-break-inside:avoid!important;" >
																			<?php		if(!empty($s_s_famille["lib_detail"])){ ?>
																							<button type="button" class="btn btn-default btn-sm hidden-print" data-toggle="tooltip" title="Classification : <?=$s_s_famille["lib_detail"]?>" >
																								<i class="fa fa-info" ></i>
																							</button>
																			<?php		} ?>	
																						<?=$s_s_famille["libelle"]?>																												
																					</th>
																		<?php		if($aff_geo_gc){	?>																								
																							<th>Résultats GEO</th>
																					<?php	for($i=4;$i<=12;$i++){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&pm_type=<?=$pm_type?>" >
																								<?php	if(!empty($s_s_famille["resultat_geo"]["mois_" . $i]) OR !empty($s_s_famille["unit_pm_geo"]["mois_" . $i]) ){
																											if($aff_ca){
																												echo("(" . number_format($s_s_famille["resultat_geo"]["mois_" . $i],0,","," ") . ") ");
																											}
																											if(!empty($s_s_famille["unit_pm_geo"]["mois_" . $i])){
																												echo(number_format(($s_s_famille["resultat_geo"]["mois_" . $i]/$s_s_famille["unit_pm_geo"]["mois_" . $i]),0,","," "));
																											}
																											echo(" [" . number_format($s_s_famille["unit_pm_geo"]["mois_" . $i],0,","," ") . "]"); 
																										}?>
																									</a>
																								</td>
																					<?php	} ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&pm_type=<?=$pm_type?>" >
																							<?php	if(!empty($s_s_famille["resultat_geo"]["mois_1"]) OR !empty($s_s_famille["unit_pm_geo"]["mois_1"]) ){
																										if($aff_ca){
																											echo("(" . number_format($s_s_famille["resultat_geo"]["mois_1"],0,","," ") . ") ");
																										}
																										if(!empty($s_s_famille["unit_pm_geo"]["mois_1"])){
																											echo(number_format(($s_s_famille["resultat_geo"]["mois_1"]/$s_s_famille["unit_pm_geo"]["mois_1"]),0,","," "));
																										}
																										echo(" [" . number_format($s_s_famille["unit_pm_geo"]["mois_1"],0,","," ") . "]"); 
																									} ?>
																								</a>
																							</td>
																							<td class="resultat text-right" >
																								<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&pm_type=<?=$pm_type?>" >
																							<?php	if(!empty($s_s_famille["resultat_geo"]["mois_2"]) OR !empty($s_s_famille["unit_pm_geo"]["mois_2"]) ){
																										if($aff_ca){
																											echo("(" . number_format($s_s_famille["resultat_geo"]["mois_2"],0,","," ") . ") ");
																										}
																										if(!empty($s_s_famille["unit_pm_geo"]["mois_2"])){
																											echo(number_format(($s_s_famille["resultat_geo"]["mois_2"]/$s_s_famille["unit_pm_geo"]["mois_2"]),0,","," "));
																										}
																										echo(" [" . number_format($s_s_famille["unit_pm_geo"]["mois_2"],0,","," ") . "]"); 
																									} ?>
																								</a>	
																							</td>
																							<td class="resultat text-right" >
																								<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&pm_type=<?=$pm_type?>" >
																							<?php	if(!empty($s_s_famille["resultat_geo"]["mois_3"]) OR !empty($s_s_famille["unit_pm_geo"]["mois_3" ]) ){
																										if($aff_ca){
																											echo("(" . number_format($s_s_famille["resultat_geo"]["mois_3"],0,","," ") . ") ");
																										}
																										if(!empty($s_s_famille["unit_pm_geo"]["mois_3"])){
																											echo(number_format(($s_s_famille["resultat_geo"]["mois_3"]/$s_s_famille["unit_pm_geo"]["mois_3" ]),0,","," "));
																										}
																										echo(" [" . number_format($s_s_famille["unit_pm_geo"]["mois_3"],0,","," ") . "]"); 
																									} ?>
																								</a>	
																							</td>
																							<td class="resultat text-right" >
																								<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&pm_type=<?=$pm_type?>" >
																							<?php	if(!empty($s_s_famille["resultat_geo"]["total" ]) OR !empty($s_s_famille["unit_pm_geo"]["total"]) ){
																										if($aff_ca){
																											echo("(" . number_format($s_s_famille["resultat_geo"]["total"],0,","," ") . ") ");
																										}
																										if(!empty($s_s_famille["unit_pm_geo"]["total"])){
																											echo(number_format(($s_s_famille["resultat_geo"]["total"]/$s_s_famille["unit_pm_geo"]["total"]),0,","," "));
																										}
																										echo(" [" . number_format($s_s_famille["unit_pm_geo"]["total" ],0,","," ") . "]"); 
																									} ?>
																								</a>	
																							</td>																						
																						</tr>
																						<tr class="resultat <?=$classLigne?>" style="page-break-before:avoid!important;" >																
																							<th>Résultats GC</th>
																					<?php	for($i=4;$i<=12;$i++){ ?>
																								<td class="resultat text-right" >
																									<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&pm_type=<?=$pm_type?>" >
																								<?php	if(!empty($s_s_famille["resultat_gc"]["mois_" . $i]) OR !empty($s_s_famille["unit_pm_gc"]["mois_" . $i]) ){
																											if($aff_ca){
																												echo("(" . number_format($s_s_famille["resultat_gc"]["mois_" . $i],0,","," ") . ") ");
																											}
																											if(!empty($s_s_famille["unit_pm_gc"]["mois_" . $i])){
																												echo(number_format(($s_s_famille["resultat_gc"]["mois_" . $i]/$s_s_famille["unit_pm_gc"]["mois_" . $i]),0,","," "));
																											}
																											echo(" [" . number_format($s_s_famille["unit_pm_gc"]["mois_" . $i],0,","," ") . "]"); 
																										} ?>
																									</a>
																								</td>
																					<?php	} ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&pm_type=<?=$pm_type?>" >
																									<?php	if(!empty($s_s_famille["resultat_gc"]["mois_1"]) OR !empty($s_s_famille["unit_pm_gc"]["mois_1"]) ){
																										if($aff_ca){
																											echo("(" . number_format($s_s_famille["resultat_gc"]["mois_1"],0,","," ") . ") ");
																										}
																										if(!empty($s_s_famille["unit_pm_gc"]["mois_1"])){
																											echo(number_format(($s_s_famille["resultat_gc"]["mois_1"]/$s_s_famille["unit_pm_gc"]["mois_1"]),0,","," "));
																										}
																										echo(" [" . number_format($s_s_famille["unit_pm_gc"]["mois_1"],0,","," ") . "]"); 
																									} ?>
																								</a>
																							</td>
																							<td class="resultat text-right" >
																								<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&pm_type=<?=$pm_type?>" >
																									<?php	if(!empty($s_s_famille["resultat_gc"]["mois_2"]) OR !empty($s_s_famille["unit_pm_gc"]["mois_2"]) ){
																										if($aff_ca){
																											echo("(" . number_format($s_s_famille["resultat_gc"]["mois_2"],0,","," ") . ") ");
																										}
																										if(!empty($s_s_famille["unit_pm_gc"]["mois_2"])){
																											echo(number_format(($s_s_famille["resultat_gc"]["mois_2"]/$s_s_famille["unit_pm_gc"]["mois_2"]),0,","," "));
																										}
																										echo(" [" . number_format($s_s_famille["unit_pm_gc"]["mois_2"],0,","," ") . "]"); 
																									} ?>
																								</a>	
																							</td>
																							<td class="resultat text-right" >
																								<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&pm_type=<?=$pm_type?>" >
																									<?php	if(!empty($s_s_famille["resultat_gc"]["mois_3"]) OR !empty($s_s_famille["unit_pm_gc"]["mois_3"]) ){
																										if($aff_ca){
																											echo("(" . number_format($s_s_famille["resultat_gc"]["mois_3"],0,","," ") . ") ");
																										}
																										if(!empty($s_s_famille["unit_pm_gc"]["mois_3"])){
																											echo(number_format(($s_s_famille["resultat_gc"]["mois_3"]/$s_s_famille["unit_pm_gc"]["mois_3"]),0,","," "));
																										}
																										echo(" [" . number_format($s_s_famille["unit_pm_gc"]["mois_3"],0,","," ") . "]"); 
																									} ?>
																								</a>	
																							</td>
																							<td class="resultat text-right" >
																								<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&pm_type=<?=$pm_type?>" >
																									<?php	if(!empty($s_s_famille["resultat_gc"]["total"]) OR !empty($s_s_famille["unit_pm_gc"]["total"]) ){
																										if($aff_ca){
																											echo("(" . number_format($s_s_famille["resultat_gc"]["total"],0,","," ") . ") ");
																										}
																										if(!empty($s_s_famille["unit_pm_gc"]["total"])){
																											echo(number_format(($s_s_famille["resultat_gc"]["total"]/$s_s_famille["unit_pm_gc"]["total"]),0,","," "));
																										}
																										echo(" [" . number_format($s_s_famille["unit_pm_gc"]["total"],0,","," ") . "]"); 
																									} ?>
																								</a>	
																							</td>
																						</tr>
																						<tr class="resultat <?=$classLigne?>" style="page-break-before:avoid!important;" >	
																		<?php 		} ?>														
																					<th>Résultats</th>
																			<?php	for($i=4;$i<=12;$i++){ ?>
																						<td class="resultat text-right" >
																							<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&pm_type=<?=$pm_type?>" >
																						<?php	if(!empty($s_s_famille["resultat"]["mois_" . $i]) OR !empty($s_s_famille["unit_pm"]["mois_" . $i]) ){
																									if($aff_ca){
																										echo("(" . number_format($s_s_famille["resultat"]["mois_" . $i],0,","," ") . ") ");
																									}
																									if(!empty($s_s_famille["unit_pm"]["mois_" . $i])){
																										echo(number_format(($s_s_famille["resultat"]["mois_" . $i]/$s_s_famille["unit_pm"]["mois_" . $i]),0,","," "));
																									}
																									echo(" [" . number_format($s_s_famille["unit_pm"]["mois_" . $i],0,","," ") . "]"); 
																								} ?>																
																							</a>
																						</td>
																			<?php	} ?>
																					<td class="resultat text-right" >
																						<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&pm_type=<?=$pm_type?>" >
																					<?php	if(!empty($s_s_famille["resultat"]["mois_1"]) OR !empty($s_s_famille["unit_pm"]["mois_1"]) ){
																								if($aff_ca){
																									echo("(" . number_format($s_s_famille["resultat"]["mois_1"],0,","," ") . ") ");
																								}
																								if(!empty($s_s_famille["unit_pm"]["mois_1"])){
																									echo(number_format(($s_s_famille["resultat"]["mois_1"]/$s_s_famille["unit_pm"]["mois_1"]),0,","," "));
																								}
																								echo(" [" . number_format($s_s_famille["unit_pm"]["mois_1"],0,","," ") . "]"); 
																							} ?>
																						</a>
																					</td>
																					<td class="resultat text-right" >
																						<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&pm_type=<?=$pm_type?>" >
																					<?php	if(!empty($s_s_famille["resultat"]["mois_2"]) OR !empty($s_s_famille["unit_pm"]["mois_2"]) ){
																								if($aff_ca){
																									echo("(" . number_format($s_s_famille["resultat"]["mois_2"],0,","," ") . ") ");
																								}
																								if(!empty($s_s_famille["unit_pm"]["mois_2"])){
																									echo(number_format(($s_s_famille["resultat"]["mois_2"]/$s_s_famille["unit_pm"]["mois_2"]),0,","," "));
																								}
																								echo(" [" . number_format($s_s_famille["unit_pm"]["mois_2"],0,","," ") . "]"); 
																							} ?>
																						</a>	
																					</td>
																					<td class="resultat text-right" >
																						<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&pm_type=<?=$pm_type?>" >
																					<?php	if(!empty($s_s_famille["resultat"]["mois_3"]) OR !empty($s_s_famille["unit_pm"]["mois_3"]) ){
																								if($aff_ca){
																									echo("(" . number_format($s_s_famille["resultat"]["mois_3"],0,","," ") . ") ");
																								}
																								if(!empty($s_s_famille["unit_pm"]["mois_3"])){
																									echo(number_format(($s_s_famille["resultat"]["mois_3"]/$s_s_famille["unit_pm"]["mois_3"]),0,","," "));
																								}
																								echo(" [" . number_format($s_s_famille["unit_pm"]["mois_3"],0,","," ") . "]"); 
																							} ?>
																						</a>	
																					</td>
																					<td class="resultat text-right" >
																						<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&pm_type=<?=$pm_type?>" >
																					<?php	if(!empty($s_s_famille["resultat"]["total"]) OR !empty($s_s_famille["unit_pm"]["total"]) ){
																								if($aff_ca){
																									echo("(" . number_format($s_s_famille["resultat"]["total"],0,","," ") . ") ");
																								}
																								if(!empty($s_s_famille["unit_pm"]["total"])){
																									echo(number_format(($s_s_famille["resultat"]["total"]/$s_s_famille["unit_pm"]["total"]),0,","," "));
																								}
																								echo(" [" . number_format($s_s_famille["unit_pm"]["total"],0,","," ") . "]"); 
																							} ?>									
																						</a>	
																					</td>
																				</tr>
																	<?php	}
																		}
																	}
																}
															}
														}
													}

													// DETAIL CONSO
													foreach($resultats as $cat => $categorie){ 
														if($cat!="conso" AND $cat!="total" AND $cat!=0){
															foreach($categorie as $fam => $famille){
																foreach($famille as $s_fam => $s_famille){
																	foreach($s_famille as $s_s_fam => $s_s_famille){														
																		if($s_s_famille["autre"]==1){
																			if(!empty($s_s_famille["objectif"]["total"])){ 
																				
																				$nb_row=1;						
																				
																				if($aff_geo_gc){
																					$nb_row=$nb_row+2;
																				}
																		
																				$numLigne++;
																				$classLigne="";
																				if($numLigne % 2 ==0){
																					$classLigne="ligne-strip";	
																				} 
																				if($s_s_famille["autre"]){
																					$classLigne.=" conso-detail";	
																				} ?>
																				<tr class="<?=$classLigne?>" >
																					<th rowspan="<?=$nb_row?>" >
																				<?php	if(!empty($s_s_famille["lib_detail"])){ ?>
																							<button type="button" class="btn btn-default btn-sm hidden-print" data-toggle="tooltip" title="Classification : <?=$s_s_famille["lib_detail"]?>" >
																								<i class="fa fa-info" ></i>
																							</button>
																			<?php		} ?>
																						<?=$s_s_famille["libelle"]?>															
																					</th>																				
																	<?php		if($aff_geo_gc){	?>
																																		
																						<th>Résultats GEO</th>
																				<?php	for($i=4;$i<=12;$i++){ ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&pm_type=<?=$pm_type?>" >													
																							<?php	if(!empty($s_s_famille["resultat_geo"]["mois_" . $i])){
																										if($aff_ca){
																											echo("(" . number_format($s_s_famille["resultat_geo"]["mois_" . $i],0,","," ") . ") ");
																										}
																										if(!empty($s_s_famille["unit_pm_geo"]["mois_" . $i])){
																											echo(number_format(($s_s_famille["resultat_geo"]["mois_" . $i]/$s_s_famille["unit_pm_geo"]["mois_" . $i]),0,","," "));
																										}
																										echo(" [" . number_format($s_s_famille["unit_pm_geo"]["mois_" . $i],0,","," ") . "]"); 
																									} ?>
																								</a>
																							</td>
																				<?php	} ?>
																						<td class="resultat text-right" >
																							<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&pm_type=<?=$pm_type?>" >
																						<?php	if(!empty($s_s_famille["resultat_geo"]["mois_1"])){
																									if($aff_ca){
																										echo("(" . number_format($s_s_famille["resultat_geo"]["mois_1"],0,","," ") . ") ");
																									}
																									if(!empty($s_s_famille["unit_pm_geo"]["mois_1"])){
																										echo(number_format(($s_s_famille["resultat_geo"]["mois_1"]/$s_s_famille["unit_pm_geo"]["mois_1"]),0,","," "));
																									}
																									echo(" [" . number_format($s_s_famille["unit_pm_geo"]["mois_1"],0,","," ") . "]"); 
																								} ?>
																							</a>
																						</td>
																						<td class="resultat text-right" >
																							<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&pm_type=<?=$pm_type?>" >
																							<?php	if(!empty($s_s_famille["resultat_geo"]["mois_2"])){
																									if($aff_ca){
																										echo("(" . number_format($s_s_famille["resultat_geo"]["mois_2"],0,","," ") . ") ");
																									}
																									if(!empty($s_s_famille["unit_pm_geo"]["mois_2"])){
																										echo(number_format(($s_s_famille["resultat_geo"]["mois_2"]/$s_s_famille["unit_pm_geo"]["mois_2"]),0,","," "));
																									}
																									echo(" [" . number_format($s_s_famille["unit_pm_geo"]["mois_2"],0,","," ") . "]"); 
																								} ?>
																							</a>	
																						</td>
																						<td class="resultat text-right" >
																							<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&pm_type=<?=$pm_type?>" >
																						<?php	if(!empty($s_s_famille["resultat_geo"]["mois_3"])){
																									if($aff_ca){
																										echo("(" . number_format($s_s_famille["resultat_geo"]["mois_1"],0,","," ") . ") ");
																									}
																									if(!empty($s_s_famille["unit_pm_geo"]["mois_3"])){
																										echo(number_format(($s_s_famille["resultat_geo"]["mois_3"]/$s_s_famille["unit_pm_geo"]["mois_3"]),0,","," "));
																									}
																									echo(" [" . number_format($s_s_famille["unit_pm_geo"]["mois_3"],0,","," ") . "]"); 
																								} ?>
																							</a>	
																						</td>
																						<td class="resultat text-right" >
																							<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=1&pm_type=<?=$pm_type?>" >
																								<?php	if(!empty($s_s_famille["resultat_geo"]["total"])){
																									if($aff_ca){
																										echo("(" . number_format($s_s_famille["resultat_geo"]["total"],0,","," ") . ") ");
																									}
																									if(!empty($s_s_famille["unit_pm_geo"]["total"])){
																										echo(number_format(($s_s_famille["resultat_geo"]["total"]/$s_s_famille["unit_pm_geo"]["total"]),0,","," "));
																									}
																									echo(" [" . number_format($s_s_famille["unit_pm_geo"]["total"],0,","," ") . "]"); 
																								} ?>
																							</a>	
																						</td>
																					</tr>
																					<tr class="resultat <?=$classLigne?>" >																
																						<th>Résultats GC</th>
																				<?php	for($i=4;$i<=12;$i++){ ?>
																							<td class="resultat text-right" >
																								<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&pm_type=<?=$pm_type?>" >
																							<?php	if(!empty($s_s_famille["resultat_gc"]["mois_" . $i])){
																										if($aff_ca){
																											echo("(" . number_format($s_s_famille["resultat_gc"]["mois_" . $i],0,","," ") . ") ");
																										}
																										if(!empty($s_s_famille["unit_pm_gc"]["mois_" . $i])){
																											echo(number_format(($s_s_famille["resultat_gc"]["mois_" . $i]/$s_s_famille["unit_pm_gc"]["mois_" . $i]),0,","," "));
																										}
																										echo(" [" . number_format($s_s_famille["unit_pm_gc"]["mois_" . $i],0,","," ") . "]"); 
																									} ?>
																								</a>
																							</td>
																				<?php	} ?>
																						<td class="resultat text-right" >
																							<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&pm_type=<?=$pm_type?>" >
																						<?php	if(!empty($s_s_famille["resultat_gc"]["mois_1"])){
																									if($aff_ca){
																										echo("(" . number_format($s_s_famille["resultat_gc"]["mois_1" ],0,","," ") . ") ");
																									}
																									if(!empty($s_s_famille["unit_pm_gc"]["mois_1"])){
																										echo(number_format(($s_s_famille["resultat_gc"]["mois_1"]/$s_s_famille["unit_pm_gc"]["mois_1"]),0,","," "));
																									}
																									echo(" [" . number_format($s_s_famille["unit_pm_gc"]["mois_1"],0,","," ") . "]"); 
																								} ?>
																							</a>
																						</td>
																						<td class="resultat text-right" >
																							<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&pm_type=<?=$pm_type?>" >
																								<?php	if(!empty($s_s_famille["resultat_gc"]["mois_2"])){
																									if($aff_ca){
																										echo("(" . number_format($s_s_famille["resultat_gc"]["mois_2" ],0,","," ") . ") ");
																									}
																									if(!empty($s_s_famille["unit_pm_gc"]["mois_2"])){
																										echo(number_format(($s_s_famille["resultat_gc"]["mois_2"]/$s_s_famille["unit_pm_gc"]["mois_2"]),0,","," "));
																									}
																									echo(" [" . number_format($s_s_famille["unit_pm_gc"]["mois_2"],0,","," ") . "]"); 
																								} ?>
																							</a>	
																						</td>
																						<td class="resultat text-right" >
																							<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&pm_type=<?=$pm_type?>" >
																								<?php	if(!empty($s_s_famille["resultat_gc"]["mois_3"])){
																									if($aff_ca){
																										echo("(" . number_format($s_s_famille["resultat_gc"]["mois_3" ],0,","," ") . ") ");
																									}
																									if(!empty($s_s_famille["unit_pm_gc"]["mois_3"])){
																										echo(number_format(($s_s_famille["resultat_gc"]["mois_3"]/$s_s_famille["unit_pm_gc"]["mois_3"]),0,","," "));
																									}
																									echo(" [" . number_format($s_s_famille["unit_pm_gc"]["mois_3"],0,","," ") . "]"); 
																								} ?>
																							</a>	
																						</td>
																						<td class="resultat text-right" >
																							<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&cli_type=2&pm_type=<?=$pm_type?>" >
																								<?php	if(!empty($s_s_famille["resultat_gc"]["total"])){
																									if($aff_ca){
																										echo("(" . number_format($s_s_famille["resultat_gc"]["total" ],0,","," ") . ") ");
																									}
																									if(!empty($s_s_famille["unit_pm_gc"]["total"])){
																										echo(number_format(($s_s_famille["resultat_gc"]["total"]/$s_s_famille["unit_pm_gc"]["total"]),0,","," "));
																									}
																									echo(" [" . number_format($s_s_famille["unit_pm_gc"]["total"],0,","," ") . "]"); 
																								} ?>
																							</a>	
																						</td>
																					</tr>
																					<tr class="resultat <?=$classLigne?>" >		
																		<?php	} ?>
																																		
																					<th>Résultats</th>
																			<?php	for($i=4;$i<=12;$i++){ ?>
																						<td class="resultat text-right" >
																							<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=<?=$i?>&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&pm_type=<?=$pm_type?>" >
																						<?php	if(!empty($s_s_famille["resultat"]["mois_" . $i])){
																									if($aff_ca){
																										echo("(" . number_format($s_s_famille["resultat"]["mois_" . $i],0,","," ") . ") ");
																									}
																									if(!empty($s_s_famille["unit_pm"]["mois_" . $i])){
																										echo(number_format(($s_s_famille["resultat"]["mois_" . $i]/$s_s_famille["unit_pm"]["mois_" . $i]),0,","," "));
																									}
																									echo(" [" . number_format($s_s_famille["unit_pm"]["mois_" . $i],0,","," ") . "]"); 
																								} ?>
																							</a>
																						</td>
																			<?php	} ?>
																					<td class="resultat text-right" >
																						<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=1&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&pm_type=<?=$pm_type?>" >
																					<?php	if(!empty($s_s_famille["resultat"]["mois_1"])){
																								if($aff_ca){
																									echo("(" . number_format($s_s_famille["resultat"]["mois_1"],0,","," ") . ") ");
																								}
																								if(!empty($s_s_famille["unit_pm"]["mois_1" ])){
																									echo(number_format(($s_s_famille["resultat"]["mois_1"]/$s_s_famille["unit_pm"]["mois_1"]),0,","," "));
																								}
																								echo(" [" . number_format($s_s_famille["unit_pm"]["mois_1"],0,","," ") . "]"); 
																							} ?>
																						</a>
																					</td>
																					<td class="resultat text-right" >
																						<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=2&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&pm_type=<?=$pm_type?>" >
																					<?php	if(!empty($s_s_famille["resultat"]["mois_2"])){
																								if($aff_ca){
																									echo("(" . number_format($s_s_famille["resultat"]["mois_2"],0,","," ") . ") ");
																								}
																								if(!empty($s_s_famille["unit_pm"]["mois_2" ])){
																									echo(number_format(($s_s_famille["resultat"]["mois_2"]/$s_s_famille["unit_pm"]["mois_2"]),0,","," "));
																								}
																								echo(" [" . number_format($s_s_famille["unit_pm"]["mois_2"],0,","," ") . "]"); 
																							} ?>
																						</a>	
																					</td>
																					<td class="resultat text-right" >
																						<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=3&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&pm_type=<?=$pm_type?>" >
																							<?php	if(!empty($s_s_famille["resultat"]["mois_3"])){
																								if($aff_ca){
																									echo("(" . number_format($s_s_famille["resultat"]["mois_3"],0,","," ") . ") ");
																								}
																								if(!empty($s_s_famille["unit_pm"]["mois_3" ])){
																									echo(number_format(($s_s_famille["resultat"]["mois_3"]/$s_s_famille["unit_pm"]["mois_3"]),0,","," "));
																								}
																								echo(" [" . number_format($s_s_famille["unit_pm"]["mois_3"],0,","," ") . "]"); 
																							} ?>
																						</a>	
																					</td>
																					<td class="resultat text-right" >
																						<a href="commercial_result_pm_detail.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&mois=0&categorie=<?=$cat?>&famille=<?=$fam?>&s_famille=<?=$s_fam?>&s_s_famille=<?=$s_s_fam?>&pm_type=<?=$pm_type?>" >
																					<?php	if(!empty($s_s_famille["resultat"]["total"])){
																								if($aff_ca){
																									echo("(" . number_format($s_s_famille["resultat"]["total"],0,","," ") . ") ");
																								}
																								if(!empty($s_s_famille["unit_pm"]["total" ])){
																									echo(number_format(($s_s_famille["resultat"]["total"]/$s_s_famille["unit_pm"]["total"]),0,","," "));
																								}
																								echo(" [" . number_format($s_s_famille["unit_pm"]["total"],0,","," ") . "]"); 
																							} ?>
																						</a>	
																					</td>
																				</tr>
																		<?php		
																			} 
																		}
																	}
																}
															}
														}
													} ?>									
												</tbody>
								<?php		} ?>
										</table>
									</div>
									<p>
										<b>Légende : X [Y]</b><br/>
							<?php	if($pm_type==2){ ?>
										X : prix moyen par stagiaire<br/>
										Y : nombre de stagiaires formés
							<?php	}else{  ?>
										X : prix moyen par demi-journée<br/>
										Y : nombre de demi-journées plannifiées.
							<?php	} ?>
									<p><b>Note :</b> ce tableau ne prend pas en compte les actions INTERCO ni les actions en CONTRAT NATIONAL (facturation via GFC).<p>
								</div>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="commercial_liste.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-left"></span>
						Retour
					</a>
					<button type="button" class="btn btn-sm btn-info ml15" id="print" >
						<i class="fa fa-print"></i> Imprimer
					</button>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
	
				</div>
				<div class="col-xs-3 footer-right" >
		
				</div>
			</div>
		</footer>
		
		
<?php
		include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				$("#exercice").change(function(){
					$("#filtre").submit();
				});
				
				$("#conso_plus").click(function(){
					$sous_famille=$(this).data("sous_famille");
					if($("#ico_conso").prop("class")=="fa fa-minus"){
						// on masque
						$(".conso-detail").addClass("hidden");
						$("#ico_conso").prop("class","fa fa-plus")
					}else{
						$(".conso-detail").removeClass("hidden");
						$("#ico_conso").prop("class","fa fa-minus")
					}
				});
				
				$("#print").click(function(){
					
					$("#zone_print").html($("#page_print").html());
					
					$("#main").hide();
					$("#content-footer").hide();
					$("#zone_print").show();
					
					window.print();
					
					$("#zone_print").hide();
					$("#main").show();
					$("#content-footer").show();
					
					$("#main").show();
					$("#content-footer").show();
					
				});
			});						
		</script>
	
	</body>
</html>
