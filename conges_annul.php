<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include("modeles/mod_envoi_mail.php");


if(!empty($_GET['conge'])){
    $conge_id = $_GET['conge'];
}else{
    die("pb");
}

$req = $Conn->prepare("UPDATE conges SET con_annul = 1, con_annul_date=NOW(), con_annul_uti = :con_annul_uti WHERE con_id = :con_id");
$req->bindValue(':con_id', $conge_id);
$req->bindValue(':con_annul_uti', $_SESSION['acces']['acc_ref_id']);
$req->execute();

$sql="SELECT * FROM utilisateurs LEFT JOIN conges ON (conges.con_utilisateur = utilisateurs.uti_id) WHERE con_id = " . $conge_id;
$req = $Conn->query($sql);
$utilisateur = $req->fetch();

$req = $ConnSoc->prepare("DELETE FROM plannings_dates
WHERE pda_ref_1 = " . $conge_id);
$req->execute();

// NOTIFICATIONS A AFFICHER
$param_mail=array(
    "sujet" => "ORION : Demande de congés annulée pour " . $utilisateur['uti_prenom'] . " " . $utilisateur['uti_nom'],
    "identite" => "ORION",
    "message" => "Les congés pour " . $utilisateur['uti_prenom'] . " " . $utilisateur['uti_nom'] . " ont été annulés pour la période du " . convert_date_txt($utilisateur['con_date_deb']) . " au " . convert_date_txt($utilisateur['con_date_fin'])
);

$adr_mail=array(
    0 => array(
        "adresse" => "conges@si2p.net",
        "nom" => "Congés Si2P"
    ),
);

$mail=envoi_mail("si2p",$param_mail,$adr_mail);

$_SESSION['message'][] = array(
    "titre" => "Succès",
    "type" => "success",
    "message" => "Votre demande est annulée"
);
Header("Location: conges_voir.php?conge=" . $conge_id);
die();

?>