 <?php
 
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");
	include_once("includes/connexion_soc.php");
	
	
	// LISTE LES CLIENTS UNIQUES FACTUREES SUR UN EXERCICE
	
	$commercial=0;
	if(!empty($_GET["commercial"])){
		$commercial=intval($_GET["commercial"]);
	}
	
	$exercice=0;
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
	
	if(empty($commercial) OR empty($exercice) ){
		echo("Erreur!");
		die();
	}
	
	$cat_lib=array();
	$sql="SELECT cca_id,cca_libelle FROM Clients_Categories ORDER BY cca_id;";
	$req=$Conn->query($sql);
	$d_result=$req->fetchAll();
	if(!empty($d_result)){
		foreach($d_result as $r){
			$cat_lib[$r["cca_id"]]=$r["cca_libelle"];
		}
	} 
	
	
	
	
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- CSS THEME -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link href="assets/admin-tools/admin-forms/css/admin-forms.css" rel="stylesheet" type="text/css">

		<!-- CSS PLUGINS -->
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<!-- CSS Si2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
	   
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
<?php
		
		$sql="SELECT cli_code,cli_nom,cli_categorie,fac_client,SUM(fac_total_ht) AS ht FROM factures LEFT JOIN Clients ON (factures.fac_client=clients.cli_id AND factures.fac_agence=clients.cli_agence)
		WHERE fac_commercial=" . $commercial . " AND fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'
		GROUP BY cli_id,cli_code,cli_nom,fac_client ORDER BY cli_code,cli_nom,cli_id;";
		$req=$ConnSoc->query($sql);
		$d_clients=$req->fetchAll();
		if(!empty($d_clients)){ ?>
			<table class="table" >
				<tr>
					<th>ID</td>
					<th>CODE</td>
					<th>NOM</td>
					<th>CATEGORIE</th>
					<th>CA</td>
				</tr>
		<?php	$total=0;
				foreach($d_clients as $d_cli){ 
					if(empty($d_cli["cli_code"])){ ?>
						<tr>
							<td><?=$d_cli["fac_client"]?><td>
							<td colspan="4" >Err</td>
						</tr>
		<?php		}else{
				
							$total=$total+$d_cli["ht"]; ?>
							<tr>
								<td><?=$d_cli["fac_client"]?></td>
								<td><?=$d_cli["cli_code"]?></td>
								<td><?=$d_cli["cli_nom"]?></td>
								<td><?=$cat_lib[$d_cli["cli_categorie"]]?></td>
								<td><?=number_format($d_cli["ht"],2,",","")?></td>
							</tr>
		<?php			
					} 
				} ?>
				<tr>
					<td colspan="4" >Total</td>
					<td><?=$total?></td>
				</tr>
			</table>
<?php	} ?>
	</body>
</html>