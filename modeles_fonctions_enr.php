<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	// contrôle des champs
	if(!empty($_POST['mfo_id'])){ 
		
		// EDITION
		// UPDATE MODELE FONCTION
		$req=$Conn->prepare("UPDATE Modeles_fonctions 
		SET mfo_nom=:mfo_nom, mfo_include=:mfo_include, mfo_menu=:mfo_menu
		WHERE mfo_id=:mfo_id");
		$req->bindParam("mfo_nom", $_POST['mfo_nom']);
		$req->bindParam("mfo_include", $_POST['mfo_include']);
		$req->bindParam("mfo_menu", $_POST['mfo_menu']);
		$req->bindParam("mfo_id", $_POST['mfo_id']);
		$req->execute();
		
		// SELECT AVANT LA MODIF
		
		$req = $Conn->prepare("SELECT pro_id,pro_libelle,mfp_profil FROM Profils 
		LEFT OUTER JOIN modeles_fonctions_profils ON (Profils.pro_id=modeles_fonctions_profils.mfp_profil AND mfp_fonction=:mfo_id)");
		$req->bindParam("mfo_id", $_POST['mfo_id']);
		$req->execute();
		$d_profil_fct = $req->fetchAll();

		foreach($d_profil_fct as $p){
			if(!empty($_POST['pro_id_' . $p["pro_id"]])){
				// PROFIL COCHE
				
				if(empty($p["mfp_profil"])){
					// IL NE l'ETAIT PAS -> ADD
					$req = $Conn->prepare("INSERT INTO Modeles_fonctions_profils (mfp_fonction, mfp_profil) VALUES (:mfp_fonction, :mfp_profil)");
					$req->bindParam("mfp_fonction", $_POST['mfo_id']);
					$req->bindParam("mfp_profil", $p['pro_id']);
					$req->execute();
				}
				/*
				}else{
					ETAIT COCHE 
					RAS
				} */
				
			}elseif(empty($_POST['pro_id_' . $p["pro_id"]]) AND !empty($p["mfp_profil"]) ){
				
				
				
				// ETAIT COCHE NE L'EST PLUS
				
				// -> DELETE
				$req = $Conn->prepare("DELETE FROM Modeles_fonctions_profils 
				WHERE mfp_profil = :mfp_profil AND mfp_fonction=:mfp_fonction");
				$req->bindParam("mfp_profil", $p["pro_id"]);
				$req->bindParam("mfp_fonction", $_POST['mfo_id']);
				$req->execute();
				
				// ON DELETE L'UTILISATION DE CETTE FONCTION AUX PROFILS QU'IL N'Y ON PLUS ACCES
				
				$sql="SELECT mod_id,mod_menu,mod_fct_1,mod_fct_2,mod_fct_3,mod_fct_4,mod_fct_5,mod_fct_6
				FROM Modeles LEFT JOIN Utilisateurs ON (Modeles.mod_utilisateur=Utilisateurs.uti_id)
				WHERE uti_profil=:mfp_profil;";
				$req=$Conn->prepare($sql);
				$req->bindParam(":mfp_profil", $p['pro_id']);
				//$req->bindParam(":fct_supp", $_POST['mfo_id']);
				$req->execute();
				$d_modeles=$req->fetchAll();
				
				foreach($d_modeles as $modele){
					
					for ($i = 1; $i <= 6; $i++){
						if($modele["mod_fct_" . $i]==$_POST['mfo_id']){
							$modele["mod_fct_" . $i]=0;
							echo($i . " 0<br/>");
						}
					}
					
					
					$sql="UPDATE Modeles SET mod_fct_1=:mod_fct_1,mod_fct_2=:mod_fct_2,mod_fct_3=:mod_fct_3, 
					mod_fct_4=:mod_fct_4,mod_fct_5=:mod_fct_5,mod_fct_6=:mod_fct_6 WHERE mod_id=:modele;";
					$req_mod_up=$Conn->prepare($sql);
					$req_mod_up->bindParam(":modele", $modele['mod_id']);
					$req_mod_up->bindParam(":mod_fct_1", $modele['mod_fct_1']);
					$req_mod_up->bindParam(":mod_fct_2", $modele['mod_fct_2']);
					$req_mod_up->bindParam(":mod_fct_3", $modele['mod_fct_3']);
					$req_mod_up->bindParam(":mod_fct_4", $modele['mod_fct_4']);
					$req_mod_up->bindParam(":mod_fct_5", $modele['mod_fct_5']);
					$req_mod_up->bindParam(":mod_fct_6", $modele['mod_fct_6']);
					$req_mod_up->execute();	
				}
			}
		}
	}else{ 
	
		// CREATION
		// INSERT MODELE FONCTIONS
		
		$req = $Conn->prepare("INSERT INTO Modeles_fonctions (mfo_nom, mfo_include,mfo_menu) VALUES (:mfo_nom, :mfo_include,:mfo_menu)");
		$req->bindParam("mfo_nom", $_POST['mfo_nom']);
		$req->bindParam("mfo_include", $_POST['mfo_include']);
		$req->bindParam("mfo_menu", $_POST['mfo_menu']);
		$req->execute();
		$id = $Conn->lastInsertId();

		$req = $Conn->prepare("SELECT pro_id,pro_libelle,mfp_profil FROM Profils 
		LEFT OUTER JOIN modeles_fonctions_profils ON (Profils.pro_id=modeles_fonctions_profils.mfp_profil AND mfp_fonction=:mfo_id)");
		$req->bindParam("mfo_id", $id);
		$req->execute();
		$d_profil_fct = $req->fetchAll();

		foreach($d_profil_fct as $p){
			if(!empty($_POST['pro_id_' . $p["pro_id"]])){
				// PROFIL COCHE
					
				
				// INSERT MODELE FONCTIONS
				$req = $Conn->prepare("INSERT INTO Modeles_fonctions_profils (mfp_fonction, mfp_profil) VALUES (:mfp_fonction, :mfp_profil)");
				$req->bindParam("mfp_fonction", $id);
				$req->bindParam("mfp_profil", $p["pro_id"]);
				$req->execute();
			}

			
		}
		
	}	
	header('Location: modeles_fonctions_liste.php');      
	
?>

