<?php

// MISE à JOUR D'UN SUSPECT

include "includes/controle_acces.inc.php";
include "includes/connexion.php";
include "includes/connexion_soc.php";

include "modeles/mod_check_siret.php";

$erreur="";

$suspect=0;
if(!empty($_POST["suspect"])){
	$suspect=intval($_POST["suspect"]);
}

if(!empty($_POST)){

    if(empty($_POST['sus_code']) OR empty($_POST['sus_nom'] OR $suspect==0)){
		$erreur="Paramètres absents";
	}

	if(empty($erreur)){

		// DONNEE POUR CONTROLE

		// l'utilisateur
		$acc_utilisateur=0;
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}

		// la société
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);
		}
		$sql="SELECT soc_agence FROM Societes WHERE soc_id=" . $acc_societe . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();

		// valeurs avant modif
		// on ne peux pas utiliser sus_adresse car adr int et non adr fac
		$sql="SELECT sus_agence,sus_categorie,sus_siren,sad_siret,sus_prescripteur,sus_prescripteur_info FROM Suspects LEFT OUTER JOIN Suspects_Adresses ON (Suspects.sus_id=Suspects_Adresses.sad_ref_id AND sad_defaut) WHERE sus_id=" . $suspect . ";";
		$req=$ConnSoc->query($sql);
		$d_suspect=$req->fetch();
		if(empty($d_suspect)){
			$erreur="Suspect inconnu!";
		}
	}

	if(empty($erreur)){

		// CONTROLE DU FORM
		$sus_agence=0;
		if($d_societe["soc_agence"]){
			if(!empty($_POST["sus_agence"])){
				$sus_agence=intval($_POST["sus_agence"]);
			}
		}

		$sus_commercial=0;
		$sus_utilisateur=0;
		if(!empty($_POST["sus_commercial"])){
			$sus_commercial=intval($_POST["sus_commercial"]);
		}
		if($sus_commercial>0){
			// utilisateur lie au com
			$sql="SELECT com_ref_1 FROM Commerciaux WHERE com_id=" . $sus_commercial . " AND com_type=1";
			$req=$ConnSoc->query($sql);
			$d_utilisateur=$req->fetch();
			if(!empty($d_utilisateur)){
				if(!empty($d_utilisateur["com_ref_1"])){
					$sus_utilisateur=$d_utilisateur["com_ref_1"];
				}
			}
		}

		$sus_categorie=0;
		if(!empty($_POST["sus_categorie"])){
			$sus_categorie=intval($_POST["sus_categorie"]);
		}else{
			$sus_categorie=1;
		}

		$sus_sous_categorie=0;
		if(!empty($_POST["sus_sous_categorie"])){
			$sus_sous_categorie=intval($_POST["sus_sous_categorie"]);
		}

		// SIREN / SIRET
		$sus_siren="";
		//$sus_siret="";
		$sad_siret="";
		if($sus_categorie!=3){
			if(!empty($_POST["sus_siren"])){
				if(strlen($_POST["sus_siren"])==11){
					$sus_siren=$_POST["sus_siren"];
					if(!empty($_POST["sad_siret"])){
						if(strlen($_POST["sad_siret"])==6){
							$sad_siret=$_POST["sad_siret"];
						}
					}
				}
			}
			//if(!empty($sus_siren) AND !empty($sad_siret)){
			//	$sus_siret=$sus_siren . " " . $sad_siret;
			//}
		}

		// MODE DE REG

		$sus_reg_type=0;
		if(!empty($_POST["sus_reg_type"])){
			$sus_reg_type=intval($_POST["sus_reg_type"]);
		}
		$sus_reg_formule=0;
		$sus_reg_nb_jour=0;
		$sus_reg_fdm=0;
		if(!empty($_POST["reg_formule"])){
			$sus_reg_formule=intval($_POST["reg_formule"]);
		}
		if(!empty($_POST["reg_nb_jour_" . $sus_reg_formule])){
			$sus_reg_nb_jour=intval($_POST["reg_nb_jour_" . $sus_reg_formule]);
		}
		if(!empty($_POST["reg_fdm_" . $sus_reg_formule])){
			$sus_reg_fdm=intval($_POST["reg_fdm_" . $sus_reg_formule]);
		}

		$sus_facture_opca=0;
		$sus_opca=0;
		$sus_opca_type=0;
		if($sus_categorie!=4){
			if(!empty($_POST["sus_facture_opca"])){
				$sus_facture_opca=1;
				if(!empty($_POST["sus_opca_type"])){
					$sus_opca_type=intval($_POST["sus_opca_type"]);
				}
				if($sus_opca_type>0){
					if(!empty($_POST["sus_opca"])){
						$sus_opca=intval($_POST["sus_opca"]);
					}
				}
			}
		}


		$sus_releve=0;
		if(!empty($_POST["sus_releve"]) && $sus_categorie!=3){
			$sus_releve=1;
		}

		$sus_classification=0;
		if(!empty($_POST["sus_classification"])){
			$sus_classification=intval($_POST["sus_classification"]);
		}

		$sus_classification_type=0;
		if(!empty($_POST["sus_classification_type"])){
			$sus_classification_type=intval($_POST["sus_classification_type"]);
		}
		$sus_classification_categorie=0;
		if(!empty($_POST["sus_classification_categorie"])){
			$sus_classification_categorie=intval($_POST["sus_classification_categorie"]);
		}

		$sus_stagiaire_type=0;
		if(!empty($_POST["sus_stagiaire_type"])){
			$sus_stagiaire_type=intval($_POST["sus_stagiaire_type"]);
		}


		if(isset($_POST["sus_prescripteur"])){
			$sus_prescripteur=0;
			$sus_prescripteur_info="";
			if(!empty($_POST["sus_prescripteur"])){
				$sus_prescripteur=intval($_POST["sus_prescripteur"]);
			}
			if(!empty($sus_prescripteur)){
				if(!empty($_POST["sus_prescripteur_info"])){
					$sus_prescripteur_info=$_POST["sus_prescripteur_info"];
				}
			}
		}else{
			$sus_prescripteur=$d_suspect["sus_prescripteur"];
			$sus_prescripteur_info=$d_suspect["sus_prescripteur_info"];
		}

		$sus_ape=0;
		if(!empty($_POST["sus_ape"])){
			$sus_ape=intval($_POST["sus_ape"]);
		}

		$sus_capital="";
		$sus_soc_type="";
		$sus_immat_lieu="";
		$sus_tel="";
		if($sus_categorie!=3){
			if(!empty($_POST["sus_capital"])){
				$sus_capital=$_POST["sus_capital"];
			}
			if(!empty($_POST["sus_soc_type"])){
				$sus_soc_type=$_POST["sus_soc_type"];
			}
			if(!empty($_POST["sus_immat_lieu"])){
				$sus_immat_lieu=$_POST["sus_immat_lieu"];
			}
			if(!empty($_POST["sus_tel"])){
				$sus_tel=$_POST["sus_tel"];
			}
		}

		// adresse intervention

		$adresse_int=0;
		if(!empty($_POST["adresse_int"])){
			$adresse_int=intval($_POST["adresse_int"]);
		}

		$sad_nom1=$_POST["sad_nom1"];
		$sad_service1=$_POST["sad_service1"];
		$sad_ad11=$_POST["sad_ad11"];
		$sad_ad21=$_POST["sad_ad21"];
		$sad_ad31=$_POST["sad_ad31"];
		$sad_cp1=$_POST["sad_cp1"];
		$sad_ville1=$_POST["sad_ville1"];
		$sad_geo1=1;
		if(!empty($_POST["sad_geo1"])){
			$sad_geo1=intval($_POST["sad_geo1"]);
		}

		// adresse de facturation

		$adresse_fac=0;
		if(!empty($_POST["adresse_fac"])){
			$adresse_fac=intval($_POST["adresse_fac"]);
		}
		$sad_geo2=1;
		if(!empty($_POST["sad_geo2"])){
			$sad_geo2=intval($_POST["sad_geo2"]);
		}

		// contact

		$contact=0;
		if(!empty($_POST["contact"])){
			$contact=intval($_POST["contact"]);
		}

		$sco_nom="";
		$sco_fonction=0;
		$sco_fonction_nom="";
		$sco_titre=0;
		$sco_prenom=0;
		$sco_tel="";
		$sco_fax="";
		$sco_portable="";
		$sco_mail="";
		$sco_compta=0;
		$sus_stagiaire_naiss=null;
		$sus_sta_naiss_cp=null;
		$sus_sta_naiss_ville=null;

		if(!empty($_POST["sco_nom"])){

			$sco_nom=$_POST["sco_nom"];
			if(!empty($_POST["sco_fonction"])){
				$sco_fonction=intval($_POST["sco_fonction"]);
				if(!empty($_POST["sco_fonction_nom"]) AND $sco_fonction==0){
					$sco_fonction_nom=$_POST["sco_fonction_nom"];
				}
			}

			if(!empty($_POST["sco_titre"])){
				$sco_titre=intval($_POST["sco_titre"]);
			}
			$sco_prenom=$_POST["sco_prenom"];
			$sco_tel=$_POST["sco_tel"];
			$sco_fax=$_POST["sco_fax"];
			$sco_portable=$_POST["sco_portable"];
			$sco_mail=$_POST["sco_mail"];

			if(!empty($_POST["sco_compta"])){
				$sco_compta=1;
			}

			if($sus_categorie==3){
				if(!empty($_POST["sus_stagiaire_naiss"])){
					var_dump($_POST["sus_stagiaire_naiss"]);
					$stagiaire_naiss=date_create_from_format('d/m/Y',$_POST["sus_stagiaire_naiss"]);
					if(!is_bool($stagiaire_naiss)){
						$sus_stagiaire_naiss=$stagiaire_naiss->format("Y-m-d");
					}
				}
				if(!empty($_POST["sus_sta_naiss_cp"])){
					$sus_sta_naiss_cp=$_POST["sus_sta_naiss_cp"];
				}
				if(!empty($_POST["sus_sta_naiss_ville"])){
					$sus_sta_naiss_ville=$_POST["sus_sta_naiss_ville"];
				}
			}

		}

		/***************************************
			CONTROLE
		***************************************/

		// SIREN / SIRET
		/* Note :en enr on ne test que base client -> doublon suspect n'a pas d'importance
		en visu on test les deux
		*/

		if(!empty($sus_siren)){

			if($sus_siren!=$d_suspect["sus_siren"]){

				// l'utilisateur à changer le siren -> impact toutes les adresses de facturation
				$req=$ConnSoc->query("SELECT sad_siret FROM Suspects_Adresses WHERE sad_type=2 AND sad_ref_id=" . $suspect . ";");
				$d_adresses_fac=$req->fetchAll();
				if(!empty($d_adresses_fac)){
					foreach($d_adresses_fac as $ad_fac){
						$siret_existe=check_siret($sus_siren,$ad_fac["sad_siret"],0);
						if(!empty($siret_existe)){
							$erreur.="Le siret " . $sus_siren . " " . $ad_fac["sad_siret"] . " est déjà utilisé dans la base client<br/>";
						}
					}
				}
			}elseif(!empty($sad_siret) AND $d_suspect["sad_siret"]!=$sad_siret){
				// l'utilisateur change le siret -> on ne test que l'adresse par defaut
				$siret_existe=check_siret($sus_siren,$d_suspect["sad_siret"],0);
				if(!empty($siret_existe)){
					$erreur="Le siret " . $sus_siren . " " . $d_suspect["sad_siret"] . " est déjà utilisé dans la base client<br/>";
				}
			}
		}

		// FIN DES CONTROLES

	}


	/***************************************
		ENREGISTREMENT
	***************************************/
	if(empty($erreur)){

		// ON TRAITE D'ABORD LES ENREGISTREMENT ANNEXE POUR METTRE A JOUR LA FICHE

		// ADRESSES D'INTERVENTION

		if($sus_categorie!=3){

			// AUTRE QUE PARTICULIER
			if(!empty($_POST["sad_cp1"]) AND !empty($_POST["sad_ville1"])){

				if($adresse_int>0){

					// MAJ

					$sql="UPDATE Suspects_Adresses SET
					sad_nom=:sad_nom,
					sad_service=:sad_service,
					sad_ad1=:sad_ad1,
					sad_ad2=:sad_ad2,
					sad_ad3=:sad_ad3,
					sad_cp=:sad_cp,
					sad_ville=:sad_ville,
					sad_libelle=:sad_libelle,
					sad_geo=:sad_geo
					WHERE sad_id=:adresse AND sad_defaut=1;";
					$req=$ConnSoc->prepare($sql);
					$req->bindParam(":sad_nom",$sad_nom1);
					$req->bindParam(":sad_service",$sad_service1);
					$req->bindParam(":sad_ad1",$sad_ad11);
					$req->bindParam(":sad_ad2",$sad_ad21);
					$req->bindParam(":sad_ad3",$sad_ad31);
					$req->bindParam(":sad_cp",$sad_cp1);
					$req->bindParam(":sad_ville",$sad_ville1);
					$req->bindParam(":sad_libelle",$_POST["sad_libelle1"]);
					$req->bindParam(":sad_geo",$sad_geo1);
					$req->bindParam(":adresse",$adresse_int);
					try{
						$req->execute();
					}catch(Exception $e){
						$erreur="E - " . $e->getMessage();
					}


				}else{
					// ADD

					$sql="INSERT INTO Suspects_Adresses
					(sad_ref,sad_ref_id,sad_type,sad_nom,sad_service,sad_ad1,sad_ad2,sad_ad3,sad_cp,sad_ville,sad_libelle,sad_geo,sad_defaut)
					VALUES (1,:sad_ref_id,1,:sad_nom,:sad_service,:sad_ad1,:sad_ad2,:sad_ad3,:sad_cp,:sad_ville,:sad_libelle,:sad_geo,1);";
					$req=$ConnSoc->prepare($sql);
					$req->bindParam(":sad_ref_id",$suspect);
					$req->bindParam(":sad_nom",$sad_nom1);
					$req->bindParam(":sad_service",$sad_service1);
					$req->bindParam(":sad_ad1",$sad_ad11);
					$req->bindParam(":sad_ad2",$sad_ad21);
					$req->bindParam(":sad_ad3",$sad_ad31);
					$req->bindParam(":sad_cp",$sad_cp1);
					$req->bindParam(":sad_ville",$sad_ville1);
					$req->bindParam(":sad_libelle",$_POST["sad_libelle1"]);
					$req->bindParam(":sad_geo",$sad_geo1);
					try{
						$req->execute();
						$adresse_int=$ConnSoc->lastInsertId();
					}catch(Exception $e){
						$erreur="B - " . $e->getMessage();
					}
				}

			}

		}elseif($d_suspect["sus_categorie"]!=$sus_categorie){

			// C'EST UN CLIENT QUE L'ON VEUX PASSER EN PARTICULIER

			$liste_ad="";
			$sql="SELECT sad_id FROM Suspects_Adresses WHERE sad_ref_id=:suspect;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":suspect",$suspect);
			$req->execute();
			$d_adresses_sus=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($d_adresses_sus)){
				$tab_adr=array_column ($d_adresses_sus,"sad_id");
				$liste_ad=implode(",",$tab_adr);
			}

			$liste_con="";
			$sql="SELECT sco_id FROM Suspects_Contacts WHERE sco_ref_id=:suspect;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":suspect",$suspect);
			$req->execute();
			$d_contacts_sus=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($d_contacts_sus)){
				$tab_con=array_column ($d_contacts_sus,"sco_id");
				$liste_con=implode(",",$tab_con);
			}

			// on supp les liens adresses contact
			if(!empty($liste_ad) OR !empty($liste_con)){
				if(!empty($liste_ad) AND !empty($liste_con)){
					$sql="DELETE FROM Suspects_Adresses_Contacts WHERE (sac_contact IN (" . $liste_con . ") OR sac_adresse IN (" . $liste_ad . ") );";
				}elseif(!empty($liste_ad)){
					$sql="DELETE FROM Suspects_Adresses_Contacts WHERE sac_adresse IN (" . $liste_ad . ");";
				}else{
					$sql="DELETE FROM Suspects_Adresses_Contacts WHERE sac_contact IN (" . $liste_con . ");";
				}
				$req=$ConnSoc->query($sql);
			}


			// on supp les adresses autre que l'adresse de fac
			$sql="DELETE FROM Suspects_Adresses WHERE sad_ref_id=:suspect AND NOT sad_id=:adresse_fac;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":suspect",$suspect);
			$req->bindParam(":adresse_fac",$adresse_fac);
			$req->execute();

			// on supp les contacts
			$sql="DELETE FROM Suspects_Contacts WHERE sco_ref_id=:suspect AND NOT sco_id=:contact;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":suspect",$suspect);
			$req->bindParam(":contact",$contact);
			$req->execute();

		}

		// ADRESSE DE FACTURATION

		if(!empty($_POST["sad_cp2"]) AND !empty($_POST["sad_ville2"])){

			if($adresse_fac>0){

				// MAJ

				$sql="UPDATE Suspects_Adresses SET
				sad_nom=:sad_nom,
				sad_service=:sad_service,
				sad_ad1=:sad_ad1,
				sad_ad2=:sad_ad2,
				sad_ad3=:sad_ad3,
				sad_cp=:sad_cp,
				sad_ville=:sad_ville,
				sad_libelle=:sad_libelle,
				sad_geo=:sad_geo,
				sad_siret=:sad_siret
				WHERE sad_id=:adresse AND sad_defaut=1;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":sad_nom",$_POST["sad_nom2"]);
				$req->bindParam(":sad_service",$_POST["sad_service2"]);
				$req->bindParam(":sad_ad1",$_POST["sad_ad12"]);
				$req->bindParam(":sad_ad2",$_POST["sad_ad22"]);
				$req->bindParam(":sad_ad3",$_POST["sad_ad32"]);
				$req->bindParam(":sad_cp",$_POST["sad_cp2"]);
				$req->bindParam(":sad_ville",$_POST["sad_ville2"]);
				$req->bindParam(":sad_libelle",$_POST["sad_libelle2"]);
				$req->bindParam(":sad_geo",$sad_geo2);
				$req->bindParam(":sad_siret",$sad_siret);
				$req->bindParam(":adresse",$adresse_fac);
				try{
					$req->execute();
				}catch(Exception $e){
					$erreur="D - " . $e->getMessage();
				}


			}else{
				// ADD

				$sqla="INSERT INTO Suspects_Adresses
				(sad_ref,sad_ref_id,sad_type,sad_nom,sad_service,sad_ad1,sad_ad2,sad_ad3,sad_cp,sad_ville,sad_libelle,sad_geo,sad_siret,sad_defaut)
				VALUES (1,:sad_ref_id,2,:sad_nom,:sad_service,:sad_ad1,:sad_ad2,:sad_ad3,:sad_cp,:sad_ville,:sad_libelle,:sad_geo,:sad_siret,1);";
				$req=$ConnSoc->prepare($sqla);
				$req->bindParam(":sad_ref_id",$suspect);
				$req->bindParam(":sad_nom",$_POST["sad_nom2"]);
				$req->bindParam(":sad_service",$_POST["sad_service2"]);
				$req->bindParam(":sad_ad1",$_POST["sad_ad12"]);
				$req->bindParam(":sad_ad2",$_POST["sad_ad22"]);
				$req->bindParam(":sad_ad3",$_POST["sad_ad32"]);
				$req->bindParam(":sad_cp",$_POST["sad_cp2"]);
				$req->bindParam(":sad_ville",$_POST["sad_ville2"]);
				$req->bindParam(":sad_libelle",$_POST["sad_libelle2"]);
				$req->bindParam(":sad_geo",$sad_geo2);
				$req->bindParam(":sad_siret",$sad_siret);
				try{
					$req->execute();
					$adresse_fac = $Conn->lastInsertId();
				}catch(Exception $e){
					$erreur="G - " . $e->getMessage();
				}
			}

			// c'est un particulier
			if($sus_categorie==3){

				$sad_nom1=$_POST["sad_nom2"];
				$sad_service1=$_POST["sad_service2"];
				$sad_ad11=$_POST["sad_ad12"];
				$sad_ad21=$_POST["sad_ad22"];
				$sad_ad31=$_POST["sad_ad32"];
				$sad_cp1=$_POST["sad_cp2"];
				$sad_ville1=$_POST["sad_ville2"];
				$sad_geo1=$sad_geo2;

			}

		}

		// CONTACT PAR DEFAUT

		if(!empty($sco_nom)){

			if($contact>0){

				// MAJ

				$sql="UPDATE Suspects_Contacts SET
				sco_fonction=:sco_fonction,
				sco_fonction_nom=:sco_fonction_nom,
				sco_titre=:sco_titre,
				sco_nom=:sco_nom,
				sco_prenom=:sco_prenom,
				sco_tel=:sco_tel,
				sco_portable=:sco_portable,
				sco_fax=:sco_fax,
				sco_mail=:sco_mail,
				sco_compta=:sco_compta
				WHERE sco_id=:contact";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":sco_fonction",$sco_fonction);
				$req->bindParam(":sco_fonction_nom",$sco_fonction_nom);
				$req->bindParam(":sco_titre",$sco_titre);
				$req->bindParam(":sco_nom",$sco_nom);
				$req->bindParam(":sco_prenom",$sco_prenom);
				$req->bindParam(":sco_tel",$sco_tel);
				$req->bindParam(":sco_portable",$sco_portable);
				$req->bindParam(":sco_fax",$sco_fax);
				$req->bindParam(":sco_mail",$sco_mail);
				$req->bindParam(":sco_compta",$sco_compta);
				$req->bindParam(":contact",$contact);
				try{
					$req->execute();
				}catch(Exception $e){
					$erreur="B - " . $e->getMessage();
				}
			}else{
				// ADD

				$sql="INSERT INTO Suspects_Contacts (
				sco_ref,sco_ref_id,sco_fonction,sco_fonction_nom,sco_titre,sco_nom,sco_prenom,sco_tel,sco_portable,sco_fax,
				sco_mail,sco_compta)
				VALUES (1,:suspect,:sco_fonction,:sco_fonction_nom,:sco_titre,:sco_nom,:sco_prenom,:sco_tel,:sco_portable,:sco_fax,
				:sco_mail,:sco_compta);";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":suspect",$suspect);
				$req->bindParam(":sco_fonction",$sco_fonction);
				$req->bindParam(":sco_fonction_nom",$sco_fonction_nom);
				$req->bindParam(":sco_titre",$sco_titre);
				$req->bindParam(":sco_nom",$sco_nom);
				$req->bindParam(":sco_prenom",$sco_prenom);
				$req->bindParam(":sco_tel",$sco_tel);
				$req->bindParam(":sco_portable",$sco_portable);
				$req->bindParam(":sco_fax",$sco_fax);
				$req->bindParam(":sco_mail",$sco_mail);
				$req->bindParam(":sco_compta",$sco_compta);
				try{
					$req->execute();
					$contact=$ConnSoc->lastInsertId();

					if($sus_categorie == 3 && !empty($adresse_fac)){
						$req = $ConnSoc->prepare("INSERT INTO Suspects_Adresses_Contacts (
						sac_adresse, sac_contact, sac_defaut ) VALUES (
						:sac_adresse, :sac_contact, 1);");
						$req->bindParam("sac_adresse",$adresse_fac);
						$req->bindParam("sac_contact",$contact);
						if(!$req->execute()){
							die();
						}
					}
				}catch(Exception $e){
					$erreur="F - " . $e->getMessage();
				}
			}

		}


		// MISE A JOUR DU SUSPECT

		// MISE A JOUR DE LA FICHE

		$sql="UPDATE Suspects SET
		sus_code=:sus_code,
		sus_nom=:sus_nom,
		sus_agence=:sus_agence,
		sus_commercial=:sus_commercial,
		sus_utilisateur=:sus_utilisateur,
		sus_categorie=:sus_categorie,
		sus_sous_categorie=:sus_sous_categorie,
		sus_siren=:sus_siren,
		sus_reg_type=:sus_reg_type,
		sus_reg_formule=:sus_reg_formule,
		sus_reg_nb_jour=:sus_reg_nb_jour,
		sus_reg_fdm=:sus_reg_fdm,
		sus_facture_opca=:sus_facture_opca,
		sus_opca_type=:sus_opca_type,
		sus_opca=:sus_opca,
		sus_releve=:sus_releve,
		sus_ident_tva=:sus_ident_tva,
		sus_classification=:sus_classification,
		sus_classification_categorie=:sus_classification_categorie,
		sus_classification_type=:sus_classification_type,
		sus_stagiaire_type=:sus_stagiaire_type,
		sus_stagiaire_naiss=:sus_stagiaire_naiss,
		sus_sta_naiss_cp=:sus_sta_naiss_cp,
		sus_sta_naiss_ville=:sus_sta_naiss_ville,
		sus_ape=:sus_ape,
		sus_prescripteur=:sus_prescripteur,
		sus_prescripteur_info=:sus_prescripteur_info,
		sus_capital=:sus_capital,
		sus_soc_type=:sus_soc_type,
		sus_immat_lieu=:sus_immat_lieu,
		sus_adresse=:sus_adresse,
		sus_adr_nom=:sus_adr_nom,
		sus_adr_service=:sus_adr_service,
		sus_adr_ad1=:sus_adr_ad1,
		sus_adr_ad2=:sus_adr_ad2,
		sus_adr_ad3=:sus_adr_ad3,
		sus_adr_cp=:sus_adr_cp,
		sus_adr_ville=:sus_adr_ville,
		sus_contact=:sus_contact,
		sus_con_titre=:sus_con_titre,
		sus_con_prenom=:sus_con_prenom,
		sus_con_nom=:sus_con_nom,
		sus_con_fct=:sus_con_fct,
		sus_con_fct_nom=:sus_con_fct_nom,
		sus_con_tel=:sus_con_tel,
		sus_con_mail=:sus_con_mail,
		sus_tel=:sus_tel
		WHERE sus_id=:suspect;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":sus_code",$_POST["sus_code"]);
		$req->bindParam(":sus_nom",$_POST["sus_nom"]);
		$req->bindParam(":sus_agence",$sus_agence);
		$req->bindParam(":sus_commercial",$sus_commercial);
		$req->bindParam(":sus_utilisateur",$sus_utilisateur);
		$req->bindParam(":sus_categorie",$sus_categorie);
		$req->bindParam(":sus_sous_categorie",$sus_sous_categorie);
		$req->bindParam(":sus_siren",$sus_siren);
		//$req->bindParam(":sus_siret",$sus_siret);
		$req->bindParam(":sus_reg_formule",$sus_reg_formule);
		$req->bindParam(":sus_reg_type",$sus_reg_type);
		$req->bindParam(":sus_reg_nb_jour",$sus_reg_nb_jour);
		$req->bindParam(":sus_reg_fdm",$sus_reg_fdm);
		$req->bindParam(":sus_facture_opca",$sus_facture_opca);
		$req->bindParam(":sus_opca_type",$sus_opca_type);
		$req->bindParam(":sus_opca",$sus_opca);
		$req->bindParam(":sus_releve",$sus_releve);
		$req->bindParam(":sus_ident_tva",$_POST["sus_ident_tva"]);
		$req->bindParam(":sus_classification",$sus_classification);
		$req->bindParam(":sus_classification_type",$sus_classification_type);
		$req->bindParam(":sus_classification_categorie",$sus_classification_categorie);
		$req->bindParam(":sus_stagiaire_type",$sus_stagiaire_type);
		$req->bindParam(":sus_stagiaire_naiss",$sus_stagiaire_naiss);
		$req->bindParam(":sus_sta_naiss_cp",$sus_sta_naiss_cp);
		$req->bindParam(":sus_sta_naiss_ville",$sus_sta_naiss_ville);
		$req->bindParam(":sus_ape",$sus_ape);
		$req->bindParam(":sus_prescripteur",$sus_prescripteur);
		$req->bindParam(":sus_prescripteur_info",$sus_prescripteur_info);
		$req->bindParam(":sus_capital",$sus_capital);
		$req->bindParam(":sus_soc_type",$sus_soc_type);
		$req->bindParam(":sus_immat_lieu",$sus_immat_lieu);
		$req->bindParam(":sus_adresse",$adresse_int);
		$req->bindParam(":sus_adr_nom",$sad_nom1);
		$req->bindParam(":sus_adr_service",$sad_service1);
		$req->bindParam(":sus_adr_ad1",$sad_ad11);
		$req->bindParam(":sus_adr_ad2",$sad_ad21);
		$req->bindParam(":sus_adr_ad3",$sad_ad31);
		$req->bindParam(":sus_adr_cp",$sad_cp1);
		$req->bindParam(":sus_adr_ville",$sad_ville1);
		$req->bindParam(":sus_contact",$contact);
		$req->bindParam(":sus_con_titre",$sco_titre);
		$req->bindParam(":sus_con_prenom",$sco_prenom);
		$req->bindParam(":sus_con_nom",$sco_nom);
		$req->bindParam(":sus_con_fct",$sco_fonction);
		$req->bindParam(":sus_con_fct_nom",$sco_fonction_nom);
		$req->bindParam(":sus_con_tel",$sco_tel);
		$req->bindParam(":sus_con_mail",$sco_mail);
		$req->bindParam(":sus_tel",$sus_tel);
		$req->bindParam(":suspect",$suspect);
		try{
			$req->execute();
		}catch(Exception $e){
			$erreur="C-" . $e->getMessage();
		}
	}
}else{
	$erreur="Paramètres absents";
}

if(!empty($erreur)){

	$erreur="Le suspect n'a pas été modifié pour les raisons suivantes :<br/>" . $erreur;
	$_SESSION['message'] = array(
		"aff" => "modal",
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur
	);
}else{
	$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Votre suspect a été actualisé"
	);
}
header("Location: suspect_voir.php?suspect=" . $suspect);
?>
