<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_document.php');


 // chercher l'url du document
 
$req=$Conn->prepare("SELECT doc_url FROM documents WHERE doc_id = " . $_GET['id']);
$req->execute();
$doc = $req->fetch();

// DELETE ACCES A UN FICHIER
$sql_del="DELETE FROM Documents_Utilisateurs WHERE dut_document = :document AND dut_utilisateur = :dut_utilisateur;";
$req_del = $Conn->prepare($sql_del);
		
// sélectionner tous les utilisateurs du document
$sql="SELECT * FROM Documents_Utilisateurs WHERE dut_document = :document;";
$req = $Conn->prepare($sql);
$req->bindParam(":document",$_GET['id']);
$req->execute();
$utilisateurs = $req->fetchAll();
// parcourir les utilisateurs avec accès coché
foreach($utilisateurs as $u){
	
	// si c'est pas coché
  	if(!isset($_POST['check_acc' . $u['dut_utilisateur']]) && empty($_POST['check_acc' . $u['dut_utilisateur']])){
        
		// rechercher l'url du document

        $utis[0]['uti_id'] =  $u['dut_utilisateur'];
		
		$req_del->bindParam(":document",$_GET['id']);
		$req_del->bindParam(":dut_utilisateur",$u['dut_utilisateur']);
		$req_del->execute();
		
		delete_affichage_dossier($utis, $doc['doc_url']);
	
  	
	}else{

		// si c'est obligatoire
		if(isset($_POST['check_obl' . $u['dut_utilisateur']]) && !empty($_POST['check_obl' . $u['dut_utilisateur']])){
			// OBLIGATOIRE
			$obligatoire = 1;

		}else{
			// UPDATE ET METTRE A ZERO
			$obligatoire = 0;
			
		}
		$sql="UPDATE Documents_Utilisateurs SET dut_obligatoire = :obligatoire WHERE dut_document = :document AND dut_utilisateur = :dut_utilisateur;";
		$req = $Conn->prepare($sql);
		$req->bindParam(":document",$_GET['id']);
		$req->bindParam(":dut_utilisateur",$u['dut_utilisateur']);
		$req->bindParam(":obligatoire",$obligatoire);
		$req->execute();
	}
	

}
//REDIRECTION
if(!empty($_GET['dos'])){
  
    Header("Location: etech_voir.php?id=" . $_GET['id'] . "&dos=" . $_GET['dos']);
}else{
    Header("Location: etech_voir.php?id=" . $_GET['id']);
}

    
    ?>