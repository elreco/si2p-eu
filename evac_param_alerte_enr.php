<?php

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

$erreur="";

if(!empty($_POST)){




	$req=$Conn->query("SELECT * FROM evacuations_parametres WHERE epa_id = 1");
	$param=$req->fetch();
	if(empty($param)){
		// EDITION
		$req = $Conn->prepare("INSERT INTO evacuations_parametres (epa_theme_alert,epa_chrono_alert, epa_remarque_alert, epa_conclusion_alert, epa_id)
		VALUES (:epa_theme_alert,:epa_chrono_alert, :epa_remarque_alert, :epa_conclusion_alert, 1)");
		$req->bindParam("epa_theme_alert",$_POST['epa_theme_alert']);
		$req->bindParam("epa_chrono_alert",$_POST['epa_chrono_alert']);
		$req->bindParam("epa_remarque_alert",$_POST['epa_remarque_alert']);
		$req->bindParam("epa_conclusion_alert",$_POST['epa_conclusion_alert']);
		$req->execute();
	}else{
		// EDITION
		$req = $Conn->prepare("UPDATE evacuations_parametres SET epa_theme_alert=:epa_theme_alert,epa_chrono_alert=:epa_chrono_alert,	epa_remarque_alert=:epa_remarque_alert,	epa_conclusion_alert=:epa_conclusion_alert WHERE epa_id=1;");
		$req->bindParam("epa_theme_alert",$_POST['epa_theme_alert']);
		$req->bindParam("epa_chrono_alert",$_POST['epa_chrono_alert']);
		$req->bindParam("epa_remarque_alert",$_POST['epa_remarque_alert']);
		$req->bindParam("epa_conclusion_alert",$_POST['epa_conclusion_alert']);
		$req->execute();
	}




}else{
	$erreur="Formulaire incomplet!";
}

if(!empty($erreur)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur
	);
}else{
	$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Chrono ajoutée"
	);
}

header("location : evac_param_alerte.php");

 ?>
