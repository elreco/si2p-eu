<?php 

/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');

///////////////////// Contrôles des parametres ////////////////////

$formation=0;
if(!empty($_GET['formation'])){
    $formation=intval($_GET['formation']);
}

$stagiaire=0;
if(!empty($_GET['stagiaire'])){
    $stagiaire=intval($_GET['stagiaire']);
}

$session=0;
if(!empty($_GET['session'])){
    $session=intval($_GET['session']);
}

if($formation == 0){
    echo("Impossible d'afficher la page");
    die();
}
/////////////// Fin Contrôles des parametres ///////////
/////////////// TRAITEMENTS BDD ///////////////////////


$req = $Conn->prepare("SELECT * FROM sessions WHERE ses_id = " . $formation);
$req->execute();
$d_session = $req->fetch();

// LES HORRAIRES

$req = $Conn->prepare("SELECT * FROM sessions_horaires WHERE sho_session = " . $formation . " ORDER BY sho_h_deb;");
$req->execute();
$d_horaires = $req->fetchAll();



if($stagiaire>0){
	
	$sql="SELECT sta_nom,sta_prenom,sst_horaire
	FROM CNRS_stagiaires LEFT JOIN sessions_stagiaires ON (CNRS_stagiaires.sta_id = sessions_stagiaires.sst_stagiaire)
	WHERE sst_session = " . $formation . " AND sst_stagiaire=" . $stagiaire . ";";
	$req = $Conn->query($sql);
	$d_inscription = $req->fetch();
	if(!empty($d_inscription)){
		$session=$d_inscription["sst_horaire"];
	}
}


?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Inscrire un stagiaire</title> 
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
	
	<form method="post" action="session_stagiaire_mod_enr.php" >
		<div>	
			<input type="hidden" name="formation" value="<?=$formation?>" />
			<input type="hidden" name="stagiaire_old" value="<?=$stagiaire?>" />
		</div>
		<div id="main">
<?php 		include "includes/header_cli.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
					<div class="row"> 		
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary ">
								<div class="panel heading-border panel-primary">								
									<div class="panel-body bg-light">
										
										<div class="content-header">
											<h2>
												Formation <b class="text-primary"><?= $d_session['ses_formation'] ?></b> 
												du <b class="text-primary"><?= convert_date_txt($d_session['ses_date']) ?></b>
									<?php		if(!empty($d_session['ses_lieu'])){ ?>
													à <b class="text-primary"><?=$d_session['ses_lieu']?></b>
									<?php		} ?>
											</h2>
									<?php	if($stagiaire>0){
												echo("<h4 class='modal-title' >Modification d'une inscription</h4>");
											}else{
												echo("<h4 class='modal-title' >Nouvelle inscription</h4>");
											} ?>
										</div>
								
										<div class="row">
											<div class="col-md-12" >
												<label for="horaire" >Session :</label>
												<select class="select2 " name="horaire" required>
													<option value="" >Session ...</option>
										<?php		if(!empty($d_horaires)){
														foreach($d_horaires as $h){ 
															if($h["sho_id"]==$session){ ?>
																<option value="<?=$h["sho_id"]?>" selected ><?=$h["sho_h_deb"] . " / " . $h["sho_h_fin"]?></option>
										<?php				}else{ ?>
																<option value="<?=$h["sho_id"]?>" ><?=$h["sho_h_deb"] . " / " . $h["sho_h_fin"]?></option>
										<?php				}
														}
													} ?>
												</select>
											</div>
										</div>									
										<div class="row mt20">
											<div class="col-md-11" >
												<label for="horaire" >Stagiaire :</label>
												<select class="select2 " id="sta" name="sta_id" required >
									<?php			if($stagiaire>0){
														echo("<option value='" . $stagiaire . "' >" . $d_inscription["sta_nom"] . " " . $d_inscription["sta_prenom"] . "</option>");
													} ?>
												</select>
											</div>	
											<div class="col-md-1 mt20" >
												<button type="button" class="btn btn-sm btn-success" id="add_sta" data-toggle="tooltip" title="Ajouter un nouveau stagiaire" >
													<i class="fa fa-plus" ></i>
												</button>
											</div>
										</div>
										
										<div id="bloc_add_sta" style="display:none;margin-top:100px;" >
										
											<h4>Nouveau stagiaire</h4>
											<p class="text-center" >Complètez le formulaire ci-dessous pour enregistrer un nouveau stagiaire et l'inscrire à la session ci-dessus</p>
										
											<div class="row mt5" >
												<div class="col-md-2 mt5 text-right" >Titre :</div>
												<div class="col-md-4" >
													<div class="select2-sm">
														<select class="select2 chp_required" id="" name="sta_titre" >										
											<?php 			foreach($base_civilite as $k => $b){ 
																if($k > 0){
																	if($k==$stagiaire['sta_titre']){
																		echo("<option value='" . $k . "' selected >" . $b . "</option>");
																	}else{
																		echo("<option value='" . $k . "' >" . $b . "</option>");
																	}
																}
															} ?>
														</select>
													</div>
												</div>
											</div>
											<div class="row mt5" >
												<div class="col-md-2 mt5 text-right" >Nom <span style="color:red;font-size:15px;">*</span> :</div>
												<div class="col-md-4" >
													<input type="text" name="sta_nom" class="gui-input gui-input-sm nom chp_required" placeholder="" value="<?= $stagiaire['sta_nom'] ?>" />									
												</div>
												<div class="col-md-2 mt5 text-right" >Prenom <span style="color:red;font-size:15px;">*</span>:</div>
												<div class="col-md-4" >
													<input type="text" name="sta_prenom" class="gui-input gui-input-sm prenom chp_required" placeholder="" value="<?= $stagiaire['sta_prenom'] ?>" />							
												</div>
											</div>
											<div class="row mt5" >
												<div class="col-md-2 mt5 text-right" >Date de naissance:</div>
												<div class="col-md-4" >
													<input type="text" name="sta_naissance" class="gui-input gui-input-sm datepicker date" placeholder="" value="<?= convert_date_txt($stagiaire['sta_naissance']) ?>" />							
												</div>
											</div>
											<div class="row mt5" >
                                    <?php 		 if(!empty($_SESSION['acces']['acc_client_ref'])){ 
													foreach($_SESSION['acces']['acc_client_ref'] as $k=>$ref){ ?>
														<div class="col-md-2 mt5 text-right" ><?= $ref ?> :</div>
														<div class="col-md-4" >
															<input type="text" name="scl_ref_<?= $k ?>" class="gui-input gui-input-sm" placeholder="<?= $ref ?>" value="<?= $stagiaire['sta_ref_' . $k] ?>" />
														</div>
									<?php 			} 
												}?>
                                            </div>
											
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="session_stagiaire.php?id=<?=$formation?>" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle">

				</div>
				<div class="col-xs-3 footer-right">
					<button type="submit" class="btn btn-success btn-sm">
						<i class='fa fa-plus'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>

<?php include "includes/footer_script.inc.php"; ?>  

	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<!-- plugin pour les masques formulaires -->

	<script src="vendor/plugins/holder/holder.min.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->

	<script src="assets/js/custom.js"></script>
	<!-- SCRIPT SELECT2 -->
	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	
	<script type="text/javascript">
		
		jQuery(document).ready(function () {
			
			$("#add_sta").click(function (){
				afficher_add_sta();
			});
			
			$("#sta").select2({
				minimumInputLength: 1,
				ajax: {
					url: "ajax/ajax_get_stagiaires_sessions.php",
					dataType: 'json',
					delay: 250,
					method: 'POST',
					data: function (params) {
						return {
							q: params.term, // les termes de la requête
						};
					},
					processResults: function (data) {
						if(data==""){
							afficher_add_sta();
						}			
						return {
							results: data,
						};	
						
							
						// data correspond au tableau json des résultat
						// data.nom permet d'accéder à l'information $retour["nom"]
						
					},
				},
				templateResult: function(data) {
					return data.nom;
				},
				templateSelection: function(data) {
					if(data.id>0){
						masquer_add_sta();
					}
					if(data.nom){
						return data.nom;
						
					}else{
						return data.text;
						
					}
					
				},
			});
		});
		function afficher_add_sta(){
			$("#sta").val(0).trigger("change");
			$("#sta").prop("required",false);
			$(".chp_required").prop("required",true);
			$("#bloc_add_sta").show();
		}
		function masquer_add_sta(){
			$("#bloc_add_sta").hide();
			$("#sta").prop("required",true);
			$(".chp_required").prop("required",false)
		}
	</script>

</body>
</html>