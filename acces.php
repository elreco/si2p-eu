<?php

session_start();	

include "includes/connexion.php";;

include_once 'modeles/mod_set_new_passe.php';
include_once 'modeles/mod_get_user_acces.php';

$erreur=0;

$autorise=false;

if(isset($_POST["ident"]) && isset($_POST["passe"])){
	
	if(!empty($_POST["ident"]) && !empty($_POST["passe"])){
		//CNRS
		if(($_POST['ident'] == "cnrs" OR $_POST['ident'] == "CNRS") && $_POST['passe'] == "Si2P@2017"){
			
			Header("Location: https://cnrs.si2p.eu/acces.php?id=cnrs412912");
			
			//Header("Location: http://cnrs.si2p.eu.local/acces.php?id=cnrs412912");
			die();
		}
		// FIN CNRS
		
		$req = $Conn->prepare("SELECT * FROM Acces WHERE acc_uti_ident LIKE :acc_uti_ident AND acc_uti_passe =:acc_uti_passe  AND NOT acc_archive");
		$req->bindParam(':acc_uti_ident', $_POST["ident"]);
		$req->bindParam(':acc_uti_passe', $_POST["passe"]);
		$req->execute();
		$acces=$req->fetch();

			
		if(!empty($acces)){
				
			$_SESSION["acces"]['acc_ref'] = $acces["acc_ref"];
			$_SESSION["acces"]['acc_ref_id'] = $acces["acc_ref_id"];

			// peremption du passe
			
			if(!empty($acces["acc_passe_modif"])){
				
				$timestamp_jour=mktime(date("H"), date("i"), date("s"),date("m"),date("d"),date("Y"));
				$timestamp_valide=strtotime($acces["acc_passe_modif"]);

				
				if($acces["acc_ref"]==1){
					
					if($timestamp_valide-$timestamp_jour>345600){
						// le passe expire dans plus de 4 jours
						$autorise=true; 
						
					}elseif($timestamp_valide-$timestamp_jour>=0){
						// le passe temporaire est encore valide
						header("location:acces_changement.php");
						die();
					}else{
						//echo("PERIME");
						// mot de passe perimé
						$new_password=set_new_passe($acces["acc_ref"],$acces["acc_ref_id"],$acces["acc_mail"],"mdp_temp");
						if(is_string($new_password)){
							$_SESSION['message'][] = array(
								"titre" => "Mot de passe périmé",
								"type" => "warning",
								"message" => "Votre mot de passe n'est plus valide. Un mot de passe temporaire vient de vous être envoyé par mail." 
							);
						}
						//var_dump($new_password);
					}					
					
				}else{
					
					// CLIENT STAGIAIRE
					
					if($timestamp_valide-$timestamp_jour<0){
					
						$new_password=set_new_passe($acces["acc_ref"],$acces["acc_ref_id"],$acces["acc_mail"],"mdp_new");
						if(is_string($new_password)){
							$_SESSION['message'][] = array(
								"titre" => "Mot de passe périmé",
								"type" => "warning",
								"message" => "Votre mot de passe n'est plus valide. Un nouveau mot de passe vient de vous être envoyé par mail." 
							);
						}						
					}else{
						$autorise = true;
					}

				}
				
			}else{
				// pas de premption du passe
				$autorise=true;
			}
		}
	}
}

if($autorise){
	
	// on connect l'utilisateur
	
	$d_access=get_user_acces($acces["acc_ref"],$acces["acc_ref_id"]);

	if(is_array($d_access)){
		
		$_SESSION["acces"]=$d_access;
		if(!empty($_SESSION["acces"]["acc_profil"]) && ($_SESSION["acces"]["acc_profil"]==13 OR $_SESSION["acces"]["acc_ref_id"] == 1 OR $_SESSION["acces"]["acc_ref_id"] == 191)){
			header("location:acces_multi.php");	
		}elseif($_SESSION["acces"]["acc_ref"] == 2){
			header("location:accueil_client.php");
		}
		else{
			header("location:accueil.php");	
		}
		
				
	}else{
		
		$_SESSION['message'][] = array(
			"titre" => "Echec de connexion",
			"type" => "danger",
			"message" => "Identifiant ou mot de passe incorrect." 
		);
		header("location:index.php");
	}
	var_dump(is_array($d_access));
	die();
	
}else{
	
	if(empty($_SESSION['message'])){
		$_SESSION['message'][] = array(
			"titre" => "Echec de connexion",
			"type" => "danger",
			"message" => "Identifiant ou mot de passe incorrect." 
		);
	}
	header("location:index.php");
}


?>
