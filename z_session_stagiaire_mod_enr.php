<?php 
/////////////////// MENU ACTIF /////////////////////// 

/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');

$error=0;
$succes=0;
$warning=0;
///////////////////// Contrôles des parametres ////////////////////

$formation=0;
if(!empty($_POST['formation'])){
    $formation=intval($_POST['formation']);
}
$session=0;
if(!empty($_POST['horaire'])){
    $session=intval($_POST['horaire']);
}
$stagiaire=0;
if(!empty($_POST['sta_id'])){
    $stagiaire=intval($_POST['sta_id']);
}

$sta_nom="";
if(!empty($_POST['sta_nom'])){
    $sta_nom=$_POST['sta_nom'];
}

$sta_prenom="";
if(!empty($_POST['sta_prenom'])){
    $sta_prenom=$_POST['sta_prenom'];
}

if($formation==0 OR $session==0 OR ($stagiaire==0 AND (empty($sta_nom) OR empty($sta_prenom) ) ) ){
    echo("Impossible d'afficher la page");
    die();
}

// AUTRES PARAMETRES

$stagiaire_old=0;
if(!empty($_POST['stagiaire_old'])){
    $stagiaire_old=intval($_POST['stagiaire_old']);
}


if($stagiaire==0){
	
	$succes=1;
	
	$sta_naissance=convert_date_sql($_POST['sta_naissance']);
		
	$sta_nom="";
	if(!empty($_POST['sta_nom'])){
		$sta_nom=trim($_POST['sta_nom']);
	}
	
	$sta_prenom="";
	if(!empty($_POST['sta_prenom'])){
		$sta_prenom=trim($_POST['sta_prenom']);
	}

	$sql="SELECT sta_id FROM CNRS_Stagiaires WHERE sta_nom='" . $sta_nom . "' AND sta_prenom='" . $sta_prenom . "'";
	if(!empty($sta_naissance)){
		$sql.=" AND (sta_naissance LIKE '" . $sta_naissance . "' OR ISNULL(sta_naissance) )";	
	}
	$sql.=";";
	$req = $Conn->query($sql);
	echo($sql);
	$d_doublon=$req->fetch();
	if(!empty($d_doublon)){
		$stagiaire=$d_doublon["sta_id"];
		$warning=1;
	}else{
		
	}
	
	IF($stagiaire==0){
		
		if(empty($sta_naissance)){
			$sta_naissance=NULL;
		}
		
		$req = $Conn->prepare("INSERT INTO CNRS_stagiaires 
			(sta_nom, 
			sta_prenom, 
			sta_naissance, 
			sta_ref_1,
			sta_ref_2,					
			sta_titre					
			) 
			VALUES 
			(:sta_nom, 
			:sta_prenom, 
			:sta_naissance, 
			:sta_ref_1,
			:sta_ref_2, 
			:sta_titre);"
		);
		$req->bindParam(':sta_nom', $sta_nom);
		$req->bindParam(':sta_prenom', $sta_prenom);
		$req->bindParam(':sta_naissance', $sta_naissance);
		$req->bindParam(':sta_ref_1', $_POST['scl_ref_1']);
		$req->bindParam(':sta_ref_2', $_POST['scl_ref_2']);
		$req->bindParam(':sta_titre', $_POST['sta_titre']);
		$req->execute();
		$stagiaire = $Conn->lastInsertId();
		
	}
}else{
	$succes=2;
}
	
if($stagiaire_old>0 AND $stagiaire_old!=$stagiaire){
	
	// on supprime l'inscription de l'ancien stagiaire
	$req = $Conn->prepare("DELETE FROM Sessions_Stagiaires WHERE sst_session=:formation AND sst_stagiaire=:stagiaire;");
	$req->bindParam(":formation",$formation);
	$req->bindParam(":stagiaire",$stagiaire_old);
	$req->execute();
	
}

// INSCRIPTION

if($stagiaire==$stagiaire_old){
	
	// MODIF INSCRIPTION ACTUEL
	
	$req = $Conn->prepare("UPDATE Sessions_Stagiaires SET sst_horaire=:horaire
	WHERE sst_session=:formation AND sst_stagiaire=:stagiaire;");
	$req->bindParam(":formation",$formation);
	$req->bindParam(":stagiaire",$stagiaire);
	$req->bindParam(":horaire",$session);
	$req->execute();
	
}else{
	
	// NOUVELLE INSCRIPTION
	
	$req = $Conn->prepare("INSERT INTO Sessions_Stagiaires (sst_session,sst_stagiaire,sst_horaire)
	VALUES (:sst_session,:sst_stagiaire,:sst_horaire);");
	$req->bindParam(":sst_session",$formation);
	$req->bindParam(":sst_stagiaire",$stagiaire);
	$req->bindParam(":sst_horaire",$session);
	$req->execute();
	
	// MISE A JOUR DES ATTESTATIONS
	
	// on supp attestation ancien stagiaire
	
	if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/documents/attestations/' . $formation . '/' . $stagiaire_old . '.pdf')){
		unlink($_SERVER['DOCUMENT_ROOT'] . '/documents/attestations/' . $formation . '/' . $stagiaire_old . '.pdf');	
	}
	
	// on supp attestation groupée
	
	if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/documents/attestations/' . $formation . '/conso_' . $formation . '.pdf')){
		unlink($_SERVER['DOCUMENT_ROOT'] . '/documents/attestations/' . $formation . '/conso_' . $formation . ".pdf");	
	}
	
	$req = $Conn->prepare("SELECT * FROM sessions WHERE ses_id = " . $formation . ";");
	$req->execute();
	$attestation_session = $req->fetch();
			
	$req = $Conn->query("SELECT sta_id, sta_nom, sta_prenom, sta_titre 
	FROM sessions_stagiaires LEFT JOIN CNRS_Stagiaires ON (sessions_stagiaires.sst_stagiaire = CNRS_Stagiaires.sta_id)
	WHERE sst_session = " . $formation . " ORDER BY sta_nom,sta_prenom;");
	$req->execute();
	$d_stagiaires = $req->fetchAll();
	if(!empty($d_stagiaires)){
		
		setlocale(LC_TIME, "fr");
		date_default_timezone_set('Europe/Paris');
		
		foreach($d_stagiaires as $sta){
			
			// attestation individuelle du stagiaire lié à l'inscription
			if($sta["sta_id"]==$stagiaire){
			
				$attestation_stagiaires=array(
					0 => $sta
				);

				include("modeles/mod_attestation_pdf.php");
			}

		}
		
		// attestation conso
		if(count($d_stagiaires)>1){
			
			$attestation_stagiaires=$d_stagiaires;
			
			include("modeles/mod_attestation_pdf.php");
		}
	
	}
}
if($warning==1){
	header("location : session_stagiaire.php?id=" . $formation . "&warning=" . $warning);	
}else{
	header("location : session_stagiaire.php?id=" . $formation . "&succes=" . $succes);	
}
?>
