<?php 
// TACHE RECURENTE POUR AIDER A DEBUGER act_agence =0

include("../includes/connexion.php");
include("../includes/connexion_fct.php");
include("../modeles/mod_envoi_mail.php");


	$rapport="";
	
	$sql="SELECT soc_id,soc_nom FROM Societes WHERE soc_agence ORDER BY soc_nom;";
	$req=$Conn->query($sql);
	$d_societes=$req->fetchAll();
	if(!empty($d_societes)){
		foreach($d_societes as $d_soc){
			
			$ConnFct=connexion_fct($d_soc["soc_id"]);
			
			$erreur="";
			
		/*	$sql_int="SELECT int_id,int_label_1,int_label_2,pin_semaine,pin_annee FROM Intervenants,Plannings_Intervenants WHERE pin_agence=0;";
			$req_int=$ConnFct->query($sql_int);
			$d_intervenants=$req_int->fetchAll();
			if(!empty($d_intervenants)){
				foreach($d_intervenants as $d_int){
					$erreur.="L'intervenant " . $d_int["int_label_1"] . " " . $d_int["int_label_2"] . " (N°" . $d_int["int_id"] . ") est inscrit au planning sans agence [" . $d_int["pin_annee"] . "-" . $d_int["pin_semaine"] . "]<br/>";
				}
			}
			*/
			$sql_act="SELECT act_id,act_date_deb,act_archive FROM Actions WHERE act_agence=0;";
			$req_act=$ConnFct->query($sql_act);
			$d_actions=$req_act->fetchAll();
			if(!empty($d_actions)){
				foreach($d_actions as $d_act){
					$erreur.="L'action N°" . $d_act["act_id"] . " du " . $d_act["act_date_deb"];
					if($d_act["act_archive"]){
						$erreur.="(archivé)";
					}
					$erreur.= " ne dispose pas d'agence<br/>";
				}
			}

			if(empty($erreur)){
				$rapport.=$d_soc["soc_nom"] . " : RAS<br/>";
			}else{
				$rapport.=$d_soc["soc_nom"] . " :<br/>";
				$rapport.=$erreur;
				$rapport.="<br/>";
			}
		}
		
		
	}
	
	echo($rapport);
	die();

?>