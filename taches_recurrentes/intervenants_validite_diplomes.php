<?php
	include("../includes/connexion.php");
	include("../modeles/mod_envoi_mail.php");

	// on recupere tous les utilisateurs à mettre a jour

	$sql="SELECT fin_id FROM fournisseurs_intervenants";
	$req=$Conn->query($sql);
	$intervenants = $req->fetchAll();

	$ref = 2;

	foreach($intervenants as $i){
		
		$ref_id = $i['fin_id'];
		// on recupere la liste de toutes les compétences affectées au diplome
		$sql="SELECT dco_competence, dco_diplome FROM diplomes_competences WHERE dco_externe = 1 AND dco_obligatoire = 1";
		$req=$Conn->query($sql);
		$diplomes_competences = $req->fetchAll();
		
		$add_competence = 1;
		foreach($diplomes_competences as $d){
			$sql="SELECT idi_ref_id FROM intervenants_diplomes WHERE (idi_date_fin > NOW() OR idi_date_fin IS NULL) AND idi_ref=" . $ref . " AND idi_ref_id =" . $ref_id . " AND idi_diplome = " . $d['dco_diplome'];
			$req=$Conn->query($sql);
			$intervenants_diplomes = $req->fetch();
			// on ajoute pas la compétence
			if(!empty($intervenants_diplomes)){
				$no_add_competence[$d['dco_competence']]=1;
			}else{
				$no_add_competence[$d['dco_competence']]=0;
			}
		}

		foreach($no_add_competence as $k=>$n){
			if($n == 0){
				$req = $Conn->prepare("DELETE FROM Intervenants_Competences WHERE ico_ref = :ico_ref AND ico_ref_id=:ico_ref_id AND ico_competence=:ico_competence");
				$req -> bindParam(":ico_ref",$ref);
				$req -> bindParam(":ico_ref_id",$ref_id);
				$req -> bindParam(":ico_competence",$k);
				$req->execute();
			}
		}
	}
	
	$param_mail=array(
	"sujet" => "Tâche récurrente ORION : intervenants_validite_diplomes.php",
	"identite" => "Informatique",
	"message" => "La tâche planifiée s'est super bien passée, niquel, bon travail."
	);

	$adr_mail=array(
		"0" => array(
			"adresse" => "informatique@si2p.mobi",
			"nom" => "Informatique"
		)
	);

	$mail=envoi_mail("si2p",$param_mail,$adr_mail);

	var_dump($mail);
	die();