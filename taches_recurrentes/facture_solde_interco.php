<?php 

// CRON JOB : RUN EVERY DAY AT 12H PM

include("../includes/connexion.php");
include("../includes/connexion_fct.php");
include("../modeles/mod_envoi_mail.php");

$mois=0;
if(isset($_GET["mois"])){
	if(!empty($_GET["mois"])){
		$mois=intval($_GET["mois"]);
	}
}
$annee=0;
if(isset($_GET["annee"])){
	if(!empty($_GET["annee"])){
		$annee=intval($_GET["annee"]);
	}
}

if(empty($mois) OR empty($annee)){
	$mois=date("m");
	$annee=date("Y");
}

// ATTENTION pour un solde au 31/05 ou envoie 01/06 
// dernier jours du mois variable alors que  fixe

$DT_date_reg=date_create_from_format('Y-m-d',$annee . "-" . $mois . "-01");
$DT_date_reg->sub(new DateInterval('P1D'));
$reg_date=$DT_date_reg->format("Y-m-d");

$DT_date_fac=date_create_from_format('Y-m-d',$annee . "-" . $mois . "-01");
$DT_date_fac->sub(new DateInterval('P3M'));
$fac_mois=$DT_date_fac->format("m");
$fac_annee=$DT_date_fac->format("Y");

$rapport="";

$req=$Conn->query("SELECT soc_id,soc_nom FROM societes AND NOT soc_archive ORDER BY soc_id;");
$d_societes=$req->fetchAll();
if(!empty($d_societes)){
	foreach($d_societes as $societe){
	
		$ConnFct=connexion_fct($societe["soc_id"]);
		
		$erreur="";
		
		$reini=false;
		
		$sql="SELECT fac_id,fac_numero,fac_regle,fac_total_ttc,cli_code,cli_interco_soc FROM Factures LEFT JOIN Clients ON (fac_client=cli_id AND fac_agence=cli_agence)
		WHERE MONTH(fac_date)=" . $fac_mois . " AND YEAR(fac_date)=" . $fac_annee . " AND (cli_interco_soc>0 OR ISNULL(cli_interco_soc))";
		$sql.=" AND NOT fac_regle=fac_total_ttc";
		$sql.=" ORDER BY fac_date,fac_numero";
		$req_fac=$ConnFct->query($sql);
		$d_factures=$req_fac->fetchAll();
		if(!empty($d_factures)){
			foreach($d_factures as $fac){
				
				$erreur_fac="";
				
				if(empty($fac["cli_interco_soc"])){					
					$erreur_fac=$fac["fac_numero"] . " pas de client identifiable!";
				}
				
				// ON SOMME LE REGLE + SUR QUE PRENDRE fac_regle
				
				
				if(empty($erreur_fac)){
					$regle=0;
					
					if($reini){
						$sql_get_reg="DELETE FROM Reglements WHERE reg_facture=" . $fac["fac_id"] . ";";
						$req_get_reg=$ConnFct->query($sql_get_reg);
					}else{
						$sql_get_reg="SELECT SUM(reg_montant) FROM Reglements WHERE reg_facture=" . $fac["fac_id"] . ";";
						$req_get_reg=$ConnFct->query($sql_get_reg);
						$d_regle=$req_get_reg->fetch();
						if(!empty($d_regle)){
							$regle=round($d_regle[0],2);
						}
					}
					
					$total_ttc=round($fac["fac_total_ttc"],2);
					$solde=$total_ttc-$regle;
					$solde=round($solde,2);
					
					if($solde==0){
						$erreur_fac=$fac["fac_numero"] . " est déjà soldée!";
					}
				}
				
				
				// ON ENREGISTRE LE REGLEMENT
				if(empty($erreur_fac)){
					$sql_add_reg="INSERT INTO Reglements (reg_facture,reg_date,reg_montant,reg_type,reg_reference,reg_banque) 
					VALUES (" . $fac["fac_id"] . ",'" . $reg_date. "'," . $solde . ",2,'Solde auto des INTERCO',20)";
					try{
						$ConnFct->query($sql_add_reg);
					}Catch(Exception $e){
						$erreur_fac=$fac["fac_numero"] . " impossible d'enregistrer le règlement! (" . $e->getMessage() . ")";
					}
				}
				
				if(empty($erreur_fac)){
					
					// ON MAJ LA FACTURE

					$sql_up_fac="UPDATE Factures SET fac_regle=:fac_regle,fac_date_reg=:fac_date_reg WHERE fac_id=:fac_id;";			
					$req_up_fac=$ConnFct->prepare($sql_up_fac);
					$req_up_fac->bindParam("fac_regle",$total_ttc);
					$req_up_fac->bindParam("fac_date_reg",$reg_date);
					$req_up_fac->bindParam("fac_id",$fac["fac_id"]);
					try{
						$req_up_fac->execute();
					}Catch(Exception $e){
						$erreur_fac=$fac["fac_numero"] . " règlement enregisté mais impossible d'actualiser la facture! (" . $e->getMessage() . ")";
					}
				}
				
				if(!empty($erreur_fac)){
					$erreur.=$erreur_fac . "<br/>";
				}
			}
		}else{
			$erreur="Pas de factures INTERCO non soldées";
		}
		
		$rapport.="Société " . $societe["soc_nom"] . " :";
		if(!empty($erreur)){
			$rapport.=" ERREUR!<br/>" . $erreur;
		}else{
			$rapport.=" OK!<br/>Toutes les factures ont été soldées.";
		}
		$rapport.="<br/><br/>";
	}
}
$param_mail=array(
	"sujet" => "Tâche récurrente ORION : Solde des factures INTERCO",
	"body" => "Détail de la tâche :<br/>Solde automatique des factures INTERCO de " . $fac_mois . "/" . $fac_annee . " en date du " . $DT_date_reg->format("d/m/Y") . ".<br>Rapport :<br/>" . $rapport
);

$adr_mail=array(
	"0" => array(
		"adresse" => "informatique@si2p.mobi",
		"nom" => "Informatique"
	),
	"1" => array(
		"adresse" => "laurent.lachevre@si2p.mobi",
		"nom" => "Laurent Lachèvre"
	)
);
$mail=envoi_mail("rapport",$param_mail,$adr_mail);
var_dump($param_mail["body"]);

die();
?>