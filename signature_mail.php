<?php

// GESTION DE LA SIGNATURE

include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include 'modeles/mod_parametre.php';


$erreur_text="";

// PARAMETRE

$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
if(empty($acc_utilisateur)){
	$erreur_text="Impossible d'afficher la page!";
}

// SUR UTILISATEUR
if(empty($erreur_text)){

	$sql="SELECT uti_id,uti_prenom,uti_nom,uti_titre,uti_societe,uti_agence,uti_fonction,uti_tel,uti_mobile,uti_mail,uti_fax FROM Utilisateurs WHERE uti_id=" . $acc_utilisateur . ";";
	$req=$Conn->query($sql);
	$d_utilisateur=$req->fetch();
	if(empty($d_utilisateur)){

		$erreur_text="Impossible d'afficher la page!";

	}
}
?>
<!DOCTYPE html>
<html lang="fr" >
<head>
    <meta charset="utf-8">
    <title>SI2P - Orion</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

	<style type="text/css" >
		.signature{
			margin-top:25px;
			display:none;
		}
		#signature_html{
			width:450px;
			margin:auto;

		}
	</style>

</head>

<body class="sb-top sb-top-sm ">

	<form action="#" id="admin-form" method="post" >
		<div id="main">
	<?php	if($_SESSION["acces"]["acc_ref"]==3){
				include "includes/header_sta.inc.php";
			}else{
				include "includes/header_def.inc.php";
			} ?>
			<section id="content_wrapper" class="">
				<section id="content" class="animated fadeIn">

					<form method="POST" action="ajax/ajax_signature_mail_set.php" id="form_signature" >
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<div class="admin-form theme-primary admin-form-label">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											<div class="content-header">
												<h2>Signature <b class="text-primary">mail</b></h2>
											</div>

											<div class="col-md-12">
												<div class="row mt15" >
													<div class="col-md-4 text-right" >Nom :</div>
													<div class="col-md-8" ><?=$d_utilisateur['uti_nom']?></div>
												</div>
												<div class="row mt15" >
													<div class="col-md-4 text-right" >Prénom :</div>
													<div class="col-md-8" ><?=$d_utilisateur['uti_prenom']?></div>
												</div>
												<div class="row mt15" >
													<div class="col-md-4 text-right pt10" >Fonction :</div>
													<div class="col-md-8" >
														<input type="tel" name="uti_fonction" id="uti_fonction" class="gui-input" placeholder="Fonction" value="<?=$d_utilisateur['uti_fonction']?>" />
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-4 text-right pt10" >Tél :</div>
													<div class="col-md-8" >
														<input type="tel" name="uti_tel" id="uti_tel" class="gui-input telephone" placeholder="Numéro de téléphone" value="<?=$d_utilisateur['uti_tel']?>" />
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-4 text-right pt10" >Portable :</div>
													<div class="col-md-8" >
														<input type="tel" name="uti_mobile" id="uti_mobile" class="gui-input telephone" placeholder="Numéro de portable" value="<?=$d_utilisateur['uti_mobile']?>" />
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-4 text-right pt10" >Fax :</div>
													<div class="col-md-8" >
														<input type="tel" name="uti_fax" id="uti_fax" class="gui-input telephone" placeholder="Numéro de fax" value="<?=$d_utilisateur['uti_fax']?>" />
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-4 text-right pt10" >Mail :</div>
													<div class="col-md-8" >
														<input type="tel" name="uti_mail" id="uti_mail" class="gui-input" placeholder="Adresse mail" value="<?=$d_utilisateur['uti_mail']?>" />
													</div>
												</div>

												<div class="row mt15" >
													<div class="col-md-4 text-right" >DataDock :</div>
													<div class="col-md-8" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" name="logo_datadock" id="logo_datadock" value="on">
																<span class="checkbox"></span>
															</label>
														</div>
													</div>
												</div>

												<div class="row mt15" >
													<div class="col-md-4 text-right" >NF :</div>
													<div class="col-md-8" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" name="logo_nf" id="logo_nf" value="on">
																<span class="checkbox"></span>
															</label>
														</div>
													</div>
												</div>

												<div class="row mt15" >
													<div class="col-md-12 text-center" >
														<button type="button" class="btn btn-success" id="btn_signature" >
															<i class="fa fa-refresh" ></i>
															Générer ma signature
														</button>
													</div>
												</div>
											</div>
											<h1 class="signature" >Signature</h1>
											<div id="signature_html" class="signature" ></div>

											<h1 class="signature">Code source</h1>
											<div class="signature text-center" >
												<textarea id="signature_code" rows="9" cols="100" ></textarea>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</section>

			</section>
		</div>

		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="mon_compte.php" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				<div class="col-xs-6 footer-middle"></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>
	</form>
<?php
	include "includes/footer_script.inc.php"; ?>

	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script src="assets/js/custom.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function (){
			$("#btn_signature").click(function(){

				uti_fonction=$("#uti_fonction").val();
				uti_tel=$("#uti_tel").val();
				uti_mobile=$("#uti_mobile").val();
				uti_fax=$("#uti_fax").val();
				uti_mail=$("#uti_mail").val();
				logo_nf=0;
				if($("#logo_nf").is(":checked")){
					logo_nf=1;
				};
				logo_datadock=0;
				if($("#logo_datadock").is(":checked")){
					logo_datadock=1;
				};

				$.ajax({
					type:'POST',
					url: 'ajax/ajax_signature_mail_set.php',
					data : 'uti_fonction=' + uti_fonction + "&uti_tel=" + uti_tel + "&uti_mobile=" + uti_mobile + "&uti_fax=" + uti_fax + "&uti_mail=" + uti_mail + "&logo_nf=" + logo_nf + "&logo_datadock=" + logo_datadock,
					dataType: 'json',
					success: function(data){
						afficher_signature(data);
					},
					error: function(data) {
						afficher_message("Erreur","danger",data.responseText);
					}
				});
			});
		});
		function afficher_signature(json){

			signature="";

			signature=signature + "<div style='width:400px;' >";
				signature=signature + "<div style='display:inline-block;width:220px;' >";
				if(json.logo_2!=""){
						signature=signature + "<img src='https://www.si2p.eu/Documents/Societes/logos/" + json.logo_1 + "' style='margin:5px;max-width:210px;max-height:120px;' />";
				}else{
					<?php if(date('m') == 12){ ?>
						signature=signature + "<img src='https://www.si2p.eu/Documents/Societes/logos/logo_reseau_noel.png' style='margin:5px;max-width:210px;max-height:120px;' />";

					<?php }else{ ?>
						signature=signature + "<img src='https://www.si2p.eu/Documents/Societes/logos/" + json.logo_1 + "' style='margin:5px;max-width:210px;max-height:120px;' />";
					<?php } ?>
				}
				signature=signature + "</div>";
				if(json.logo_2!=""){
					signature=signature + "<div style='display:inline-block;text-align:right;width:180px;vertical-align:bottom;' >";
					<?php if(date('m') == 12){ ?>
						signature=signature + "<img src='https://www.si2p.eu/Documents/Societes/logos/logo_reseau_noel.png' style='margin:5px;max-width:210px;max-height:120px;' />";
					<?php }else{ ?>
						signature=signature + "<img src='https://www.si2p.eu/Documents/Societes/logos/" + json.logo_2 + "' style='margin:5px;max-width:170px;' />";
					<?php } ?>

					signature=signature + "</div>";
				}
			signature=signature + "</div>";

			signature=signature + "<div style='color:#7e8082;font-family:Century Gothic,Verdana;font-size:11pt;width:400px;' >";
				signature=signature + "<div style='border-bottom:1px solid #e31936;padding:5px;' >";
					signature=signature + "<div style='text-align:right;' >";
						signature=signature + "<div style='margin-bottom:5px;font-size:11pt;' ><b>" + json.uti_prenom + " " + json.uti_nom + "</b></div>";
						signature=signature + "<div style='color:#e31936;margin-bottom:5px;' >" + json.uti_fonction + "</div>";
						if(json.uti_tel!==""){
							signature=signature + "<div>" + json.uti_tel + "</div>";
						}
						if(json.uti_mobile!=""){
							signature=signature + "<div>" + json.uti_mobile + "</div>";
						}
						signature=signature + "<div>";
							if(json.uti_mail!=""){
								signature=signature + "<a href='mailto:" + json.uti_mail + "' style='color:#2d589e;font-size:11pt;text-decoration:underline;font-family:Century Gothic;' >";
									signature=signature + json.uti_mail;
								signature=signature + "</a>";
								//*signature=signature + "- ";
							}
							if(json.site!=""){
								signature=signature + "<br/>";
								signature=signature + "<a style='color:#2d589e;font-size:11pt;font-weight:bold;text-decoration:underline;font-family:Century Gothic;' href='http://" + json.site + "'>";
									signature=signature + json.site;
								signature=signature + "</a>";
							}
						signature=signature + "</div>";
					signature=signature + "</div>";
				signature=signature + "</div>";
				signature=signature + "<div style='background-color:#efefef;font-size:10pt;padding:5px;text-align:center;' >";
					signature=signature + "<b>" + json.soc_nom + "</b> <span style='color:#e31936;' > | </span> formation <span style='color:#e31936;'> | </span> sécurité <span style='color:#e31936;'> | </span> prévention";
				signature=signature + "</div>";

				console.log(" logo_nf " + json.logo_nf);
				console.log("logo_datadock " + json.logo_datadock);
				if(json.logo_nf==1||json.logo_datadock==1){
					signature=signature + "<div style='width:400px;' >";
						if(json.logo_datadock==1){
							signature=signature + "<div style='float:left;' >";
								signature=signature + "<img src='https://www.si2p.eu/assets/img/logos/logo_datadock.jpg' />";
							signature=signature + "</div>";
						}
						if(json.logo_nf==1){
							signature=signature + "<div style='float:right;' >";
								signature=signature + "<a href='http://www.marque-nf.com' >";
									signature=signature + "<img src='https://www.si2p.eu/assets/img/logos/logo_nf_service.jpg' style='height:90px;' />";
								signature=signature + "</a>";
							signature=signature + "</div>";
						}
						signature=signature + "<div style='clear:both;' > </div>";
					signature=signature + "</div>";
				}
				if(json.cnaps!=""){
					signature=signature + "<p style='text-align:center;' >N° CNAPS : " + json.cnaps + "</p><p>L’autorisation d’exercice ne confère aucune prérogative de puissance publique à l’entreprise ou aux personnes qui en bénéficient.</p>";
				}
			signature=signature + "</div>";

			$("#signature_html").html(signature);
			$("#signature_code").val(signature);
			$(".signature").show();

		}
	</script>
</body>
</html>
