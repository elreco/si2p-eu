<?php


include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';


include_once 'modeles/mod_orion_pro_categories.php';
include_once 'modeles/mod_orion_pro_familles.php';
include_once 'modeles/mod_orion_pro_s_familles.php';
include_once 'modeles/mod_orion_qualifications.php';
$_SESSION["retour"] = "produit_liste.php";

	// TRAITEMENT DU FORM

	if(isset($_POST['search'])){

		$pro_categorie=0;
		if(!empty($_POST['pro_categorie'])){
			$pro_categorie=intval($_POST['pro_categorie']);
		}

		$pro_famille=0;
		if(!empty($_POST['pro_famille'])){
			$pro_famille=intval($_POST['pro_famille']);
		}

		$pro_sous_famille=0;
		if(!empty($_POST['pro_sous_famille'])){
			$pro_sous_famille=intval($_POST['pro_sous_famille']);
		}
		
		$pro_sous_sous_famille=0;
		if(!empty($_POST['pro_sous_sous_famille'])){
			$pro_sous_sous_famille=intval($_POST['pro_sous_sous_famille']);
		}


		if(!isset($_POST['pro_recurrent'])){
			$pro_recurrent = "";
		}else{
			$pro_recurrent = $_POST['pro_recurrent'];
		}

		if(!isset($_POST['pro_deductible'])){
			$pro_deductible = "";
		}else{
			$pro_deductible = $_POST['pro_deductible'];
		}

		if(!isset($_POST['pro_archive'])){
			$pro_archive = "";
		}else{
			$pro_archive = $_POST['pro_archive'];
		}
		
		if(!isset($_POST['pro_challenge'])){
			$pro_challenge = "";
		}else{
			$pro_challenge = $_POST['pro_challenge'];
		}

		$pro_inter_intra=0;
		if(!empty($_POST['pro_inter_intra'])){
			$pro_inter_intra=intval($_POST['pro_inter_intra']);
		}



		$pro_tri= array(
			"pro_code_produit" => $_POST['pro_code_produit'],
			"pro_libelle" => $_POST['pro_libelle'],
			"pro_categorie" => $pro_categorie,
			"pro_famille" => $pro_famille,
			"pro_qualification" => $_POST['pro_qualification'],
			"pro_recurrent" => $pro_recurrent,
			"pro_deductible" => $pro_deductible,
			"pro_archive" => $pro_archive,
			"pro_sous_famille" => $pro_sous_famille,
			"pro_sous_sous_famille" => $pro_sous_sous_famille,
			"pro_inter_intra" => $pro_inter_intra,
			"pro_challenge" => $pro_challenge
		);

		$_SESSION['pro_tri'] =$pro_tri;

	}else{
		if(!empty($_SESSION['pro_tri'])){
			$pro_tri=$_SESSION['pro_tri'];
		}



	}

	// construction de la requete
   $pro_tri = $_SESSION['pro_tri'];
   $sql="SELECT * FROM produits";
    $mil="";
    if(!empty($pro_tri["pro_code_produit"])){
		$mil.=" AND pro_code_produit LIKE '%" . $pro_tri["pro_code_produit"] . "%'";
    }
	if(!empty($pro_tri["pro_libelle"])){
			 $mil.=" AND pro_libelle LIKE '%" . $pro_tri["pro_libelle"] . "%'";
	}
    if(!empty($pro_tri["pro_categorie"])){
        $mil.=" AND pro_categorie=" . $pro_tri["pro_categorie"];
    }
    if(!empty($pro_tri["pro_famille"])){
        $mil.=" AND pro_famille =" . $pro_tri["pro_famille"];
    }
    if(!empty($pro_tri["pro_sous_famille"])){
        $mil.=" AND pro_sous_famille = " . $pro_tri["pro_sous_famille"];
    }
	 if(!empty($pro_tri["pro_sous_sous_famille"])){
        $mil.=" AND pro_sous_sous_famille = " . $pro_tri["pro_sous_sous_famille"];
    }

    if(!empty($pro_tri["pro_qualification"])){
        $mil.=" AND pro_qualification = ". $pro_tri["pro_qualification"];
    }
    if($pro_tri["pro_recurrent"] == "pro_recurrent"){
        $mil.=" AND pro_recurrent = 1";
    }
    if($pro_tri["pro_deductible"] == "pro_deductible"){
        $mil.=" AND pro_deductible = 1";
    }
    if($pro_tri["pro_archive"] == "pro_archive"){
        $mil.=" AND pro_archive = 1";
    }else{
        $mil.=" AND pro_archive = 0";
    }
	if($pro_tri["pro_challenge"] == "on"){
        $mil.=" AND pro_pt_0 >0";
    }
	
    if($pro_tri["pro_inter_intra"] != 0){
        if($pro_tri["pro_inter_intra"] == 1){
            $mil.=" AND pro_ht_inter != 0";
        }else{
            $mil.=" AND pro_ht_intra != 0";
        }
    }
	
	// critere auto
	if($_SESSION["acces"]["acc_service"][1]!=1 AND $_SESSION["acces"]["acc_service"][2]!=1 AND $_SESSION["acces"]["acc_service"][5]!=1){
		$mil.=" AND (pro_client=0 OR ISNULL(pro_client))";
	}
	
    if($mil!=""){
        $sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
    }
    $sql.=" ORDER BY pro_code_produit";
	/*echo($sql);
	die();*/
    $req = $Conn->query($sql);
    $produits = $req->fetchAll();
	if(!empty($produits)){

		$orion_pro_categories=orion_produits_categories();
		$orion_pro_familles=orion_produits_familles();
		$orion_pro_s_familles=orion_produits_sous_familles();
		$orion_pro_s_s_familles=orion_produits_sous_sous_familles();

		$orion_qualifications=orion_qualifications();
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php
			include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<section id="content" class="animated fadeIn">
					<div class="col-md-12">

				<?php 	if(isset($_GET['succes'])):

							if($_GET['succes'] == 1): ?>
								<div class="alert alert-success alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									Le produit a été créé
								</div>
					<?php 	endif;
							if($_GET['succes'] == 2):
						?>
								<div class="alert alert-success alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									Le produit a été modifié
								</div>
					<?php  endif;
						endif; ?>

					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="dark" >
									<th>Catégorie</th>
									<th>Famille</th>
									<th>Sous-famille</th>
									<th>Sous-sous-famille</th>
									<th>Libellé</th>
									<th>Code produit</th>
									<th>Qualification</th>
									<th>Prix intra (€)</th>
									<th>Prix inter (€)</th>
							<?php 	if($_SESSION['acces']["acc_droits"][2] OR $_SESSION['acces']["acc_droits"][24]){ ?>
										<th>Actions</th>
							<?php	} ?>
								</tr>
							</thead>
					<?php	if(!empty($produits)){ ?>
								<tbody>
					<?php 			foreach($produits as $s){
										$cat_lib="";
										if(!empty($s['pro_categorie'])){
											$cat_lib=$orion_pro_categories[$s['pro_categorie']]["pca_libelle"];
										}
										$fam_lib="";
										if(!empty($s['pro_famille'])){
											$fam_lib=$orion_pro_familles[$s['pro_famille']]["pfa_libelle"];
										}
										$s_fam_lib="";
										if(!empty($s['pro_sous_famille'])){
											$s_fam_lib=$orion_pro_s_familles[$s['pro_sous_famille']]["psf_libelle"];
										}
										$s_s_fam_lib="";
										if(!empty($s['pro_sous_sous_famille'])){
											$s_s_fam_lib=$orion_pro_s_s_familles[$s['pro_sous_sous_famille']]["pss_libelle"];
											//$s_s_fam_lib="Ordinateurs";
										}
										$qua_lib="";
										if(!empty($s['pro_qualification'])){
											$qua_lib=$orion_qualifications[$s['pro_qualification']]["qua_libelle"];
										}
										$style="";
										if(!empty($orion_pro_s_s_familles[$s['pro_sous_sous_famille']]["pss_couleur_bg"])){
											$style="background-color:" . $orion_pro_s_s_familles[$s['pro_sous_sous_famille']]["pss_couleur_bg"] . ";";
											if(!empty($orion_pro_s_s_familles[$s['pro_sous_sous_famille']]["pss_couleur_txt"])){
												$style.="color:" . $orion_pro_s_s_familles[$s['pro_sous_sous_famille']]["pss_couleur_txt"] . ";";
											}
										}elseif(!empty($orion_pro_s_familles[$s['pro_sous_famille']]["psf_couleur_bg"])){
											$style="background-color:" . $orion_pro_s_familles[$s['pro_sous_famille']]["psf_couleur_bg"] . ";";
											if(!empty($orion_pro_s_familles[$s['pro_sous_famille']]["psf_couleur_txt"])){
												$style.="color:" . $orion_pro_s_familles[$s['pro_sous_famille']]["psf_couleur_txt"] . ";";
											}
										} ?>
										<tr>
											<td><?=$cat_lib?></td>
											<td><?=$fam_lib?></td>
											<td><?=$s_fam_lib?></td>
											<td><?=$s_s_fam_lib?></td>
											<td><?= $s['pro_libelle'] ?></td>
											<td style="<?=$style?>" ><?= $s['pro_code_produit'] ?></td>
											<td><?=$qua_lib?></td>
											<td class="text-right" >
										<?php	if(!empty($s['pro_ht_intra'])){
													echo($s['pro_ht_intra'] . " €");
												}else{
													echo("&nbsp;");
												} ?>
											</td>
											<td class="text-right" >
										<?php	if(!empty($s['pro_ht_inter'])){
													echo($s['pro_ht_inter'] . " €");
												}else{
													echo("&nbsp;");
												} ?>
											</td>
									<?php 	if($_SESSION['acces']["acc_droits"][2] OR $_SESSION['acces']["acc_droits"][24]){ ?>
												<td class="text-center" >
									<?php			if($_SESSION['acces']["acc_droits"][2]){ ?>
														<a href="produit.php?id=<?= $s['pro_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier ce produit">
															<span class="fa fa-pencil"></span>
														</a>
									<?php			}
													if($_SESSION['acces']["acc_droits"][24]){ ?>
														<a href="produit_societe.php?produit=<?= $s['pro_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier le paramètres regionnaux">
															<span class="fa fa-building-o"></span>
														</a>
									<?php			} ?>
												</td>
									<?php 	} ?>
										</tr>
				<?php 				} ?>
								</tbody>
							</table>
				<?php 	} ?>
					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="produit_tri.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-long-arrow-left"></span>
						Retour
					</a>
					<a href="produit_tri.php" class="btn btn-primary btn-sm" role="button">
						<span class="fa fa-search"></span>
						<span class="hidden-xs">Nouvelle recherche</span>
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
			<?php 	if($_SESSION['acces']["acc_droits"][2]){  ?>
						<a href="produit.php" class="btn btn-success btn-sm" role="button" >
							<span class="fa fa-plus"></span>
							Nouveau produit
						</a>
			<?php 	} ?>
				</div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
	</body>
</html>
