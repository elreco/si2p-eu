<?php
$commerce_li = true;
/* variable pour la nav (active) */
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
/* include pour chaque fichier */
include_once 'modeles/mod_produit.php';
$modeles = get_produit_modeles();
?>
<!DOCTYPE html>
<html>
<!-- Mirrored from admindesigns.com/demos/absolute/1.1/layout_horizontal-sm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 09:52:52 GMT -->
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Script</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!--
       <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
       <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
   -->
   <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
   <!-- Favicon -->
   <link rel="shortcut icon" href="assets/img/favicon.png">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<?php
include "includes/header_def.inc.php";
?>
<body class="sb-top sb-top-sm ">
    <section id="content_wrapper">
        <section id="content">
            <div class="row">
                <div class="col-md-5">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr class="dark">
                                    <th>Nom</th>
                                    <th>Edition</th> 
                                    <th>Famille</th>
                                    <th>Sous-famille</th>
                                    <th>Qualification</th>
                                    <th>URL</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($modeles as $k => $v): ?>
                                    <tr>
                                        <td>Test 1</td>
                                        <td>Test 2</td>
                                        <td>test 3</td>
                                        <td>test 4</td>
                                        <td>test 5</td>
                                        <td>test 6</td>
                                        <td><a href="produit_convention_edition.php?id=" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a> <a href="produit_convention_edition.php?id=" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a></td>
                                    </tr>

                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php if(empty($modeles)): ?>
                            <div class="col-md-12 text-center" style="padding:0;" >
                                <div class="alert alert-warning" style="border-radius:0px;">
                                    Aucun modèle.
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="col-md-7">
                    <div class="panel panel-100">
                        <div class="panel-heading panel-md">
                            <span class="panel-title">Editions du modèle <strong>123</strong></span>
                            
                        </div>
                        <div class="panel-body panel-scroller scroller-overlay" >

                            <table class="table mbn tc-med-1 tc-bold-2 table-hover pad-right-td">
                                <thead>
                                    <tr>
                                        <th>Convention</th>
                                        <th>Convocation</th>
                                        <th>Avis de passage</th>
                                        <th>Feuille de présence</th>
                                        <th>Attestation</th>
                                        <th>Diplôme</th>
                                        <th>Editer</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
</section>


<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left">
            <a href="parametre.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle"></div>
        <div class="col-xs-3 footer-right">

            <a href="" class="btn btn-success btn-sm">Ajouter un modèle</a>

        </div>
    </div>
</footer>

<?php
include "includes/footer_script.inc.php"; 
?>	
<script src="assets/js/custom.js"></script>
</body>
</html>
