<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	
	include('modeles/mod_profil.php');
	include('modeles/mod_erreur.php');

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">		

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php	
				include "includes/header_def.inc.php";
			?>			
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">	

				
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn">		
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="dark" >
									<th>ID</th>
									<th>Profil</th>
									<th>Type</th>
									<th>&nbsp;</th>									
								</tr>
							</thead>
							<tbody>
							
								<?php
							
									$req=$Conn->query("SELECT * FROM profils ORDER BY pro_libelle");
									while ($donnees = $req->fetch())
									{ 	echo "<tr>";
											echo "<td>" . $donnees["pro_id"] . "</td>";
											echo "<td>" . $donnees["pro_libelle"] . "</td>";
											echo "<td>" . $base_ref_profil[$donnees["pro_ref"]] . "</td>";	
											echo '<td class="text-center" >';
												echo '<span data-toggle="modal" data-target="#formProfil" >';
													echo '<a href="#" class="btn btn-warning btn-xs open-MajProfil" data-id="' . $donnees["pro_id"] . '" data-libelle="' . $donnees["pro_libelle"] . '" data-ref="' . $donnees["pro_ref"] . '" data-toggle="tooltip" data-placement="bottom" title="Modifier" >';
														echo '<i class="fa fa-pencil"></i>';
													echo '</a>';
												echo '</span>';
											echo '</td>	';										
										echo '</tr>';
									};
									
								?>
								
							</tbody>
						</table>
					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-long-arrow-left"></span>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="#" class="btn btn-success btn-sm open-MajProfil" data-id="0" data-ref="0" data-libelle="" role="button" data-toggle="modal" data-target="#formProfil" >
						<span class="fa fa-plus"></span>
						Nouveau profil
					</a>
				</div>
			</div>
		</footer>
		
		<div id="formProfil" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<form method="post" action="profil_enr.php" >
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title" id="titreModal" >Nouveau profil</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input type="hidden" name="pro_id" id="pro_id" value="0" />
							</div>
							<div class="form-group">
								<label for="pro_libelle" class="sr-only" >Nom</label>
								<div>
									<input type="text" class="form-control" name="pro_libelle" id="pro_libelle" placeholder="Nom du profil" required >
								</div>
							</div>	
							<div class="form-group" >
								<label class="sr-only" for="pro_ref">Type</label>
								<select name="pro_ref" id="pro_ref" class="form-control" required >
									<?=get_ref_profil_select();?>								
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	
	<?php
		include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function(){				
				

				$(document).on("click", ".open-MajProfil", function () {
					 var pro_id = $(this).data('id');
					 var pro_libelle = $(this).data('libelle');
					 var pro_ref = $(this).data('ref');				 
					 $(".modal-body #pro_id").val( pro_id );					  
					 $(".modal-body #pro_libelle").val( pro_libelle );					
					 $(".modal-body #pro_ref").val( pro_ref );
					if(pro_id==0){
						$("#titreModal").html('Nouveau profil');
					}else{
						$("#titreModal").html('Edition d\'un profil');
					};
				});				
			});
			(jQuery);
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
