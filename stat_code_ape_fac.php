<?php

// AFFICHE LA LISTE DES FACTURES

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');

// DONNEE UTILE AU PROGRAMME
if($_SESSION['acces']['acc_service'][1]!=1){
	echo("Accès refusé!");
	die();
}

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}

$aff_detail_opca=false;
if($_SESSION['acces']['acc_service'][2]==1 OR $_SESSION['acces']['acc_service'][3]==1){
	$aff_detail_opca=true;
}

$_SESSION['retour'] = "stat_code_ape_fac.php";
$_SESSION["retourFacture"]="stat_code_ape_fac.php";
$_SESSION["retourClient"]="stat_code_ape_fac.php";
$_SESSION["retour_action"]="stat_code_ape_fac.php";


// SELECT FACTURES
if(!empty($_SESSION['factures_stats'])){
    $sql=$_SESSION['factures_stats'];
    $req = $ConnSoc->prepare($sql);
    $req->execute();
    $factures = $req->fetchAll();
}else{
    $factures = [];
}

$ape_libelle="";
$affichage=0;
$ape=null;
$div=null;

if(isset($_GET['ape'])){
	
	$affichage=1;
	
	$ape=0;
	if(!empty($_GET['ape'])){
		$ape=intval($_GET['ape']);
	}
	
    $sql="SELECT ape_libelle FROM ape WHERE ape_id = :ape_id";
    $req=$Conn->prepare($sql);
    $req->bindValue("ape_id", $ape);
    $req->execute();
    $aped=$req->fetch();
    $ape_libelle = $aped['ape_libelle'];
	

	
}elseif(isset($_GET['div'])){
	
	$affichage=2;
	$div=0;
	if(!empty($_GET['div'])){
		$div=$_GET['div'];
	}
	
	$sql="SELECT adi_libelle FROM ape_divisions WHERE adi_code = :adi_code";
    $req=$Conn->prepare($sql);
    $req->bindValue("adi_code", $div);
    $req->execute();
    $aped=$req->fetch();
    $ape_libelle = $aped['adi_libelle'];
	
	// on recherche la liste des codes APE de la division
	
	$sql="SELECT ape_id FROM Ape WHERE ape_code LIKE '" . $div . "%'";
    $req=$Conn->query($sql);
    $src_ape=$req->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($src_ape)){
		$tab_ape=array_column ($src_ape,"ape_id");
		$liste_ape=implode($tab_ape,",");
	}else{
		echo("La division " . $div . " ne contient aucun code APE!");
		die();
	}

	
}

if(!empty($factures) AND (!is_null($ape) OR !is_null($div) ) ){
	
	// si $ape et div sont null c'est que l'utilisateur à cliqué sur total
	
    foreach($factures as $k=>$f){
        if(!empty($ape)){
			
			$sql="SELECT ape_code, ape_libelle, ape_id FROM Clients LEFT JOIN ape ON (Clients.cli_ape = ape.ape_id) WHERE cli_ape = :cli_ape AND cli_id = :fac_client";
			$req=$Conn->prepare($sql);
			$req->bindValue("cli_ape", intval($_GET['ape']));
			$req->bindValue("fac_client", $f['fac_client']);
			$req->execute();
			$d_ape=$req->fetch();
				
		}elseif(!empty($div)){
			
			$sql="SELECT ape_code, ape_libelle, ape_id FROM Clients LEFT JOIN ape ON (Clients.cli_ape = ape.ape_id) WHERE cli_ape IN (" . $liste_ape . ") AND cli_id = :fac_client";
			$req=$Conn->prepare($sql);
			$req->bindValue("fac_client", $f['fac_client']);
			$req->execute();
			$d_ape=$req->fetch();
			
			
			
			
        }else{
			
			// 0 =  Clients sans code APE
            $sql="SELECT ape_code, ape_libelle, ape_id FROM Clients LEFT JOIN ape ON (Clients.cli_ape = ape.ape_id) WHERE (cli_ape = 0 OR cli_ape IS NULL)  AND cli_id = :fac_client";
            $req=$Conn->prepare($sql);
            $req->bindValue("fac_client", $f['fac_client']);
            $req->execute();
            $d_ape=$req->fetch();
        }
		
        if(empty($d_ape)){
            unset($factures[$k]);
        }

    }
}

?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">
				<?php 	if(!empty($ape_libelle)){ ?>
                            <h1>Liste des factures pour
				<?php		if($affichage==1){
								echo(" le code APE <br><strong>" . $ape_libelle . "</strong>");
							}else{
								echo(" la division APE <br><strong>" . $ape_libelle . "</strong>");
							} ?>
							</h1>
				<?php 	}elseif(!is_null($ape) OR !is_null($div)){ ?>
							<h1>Liste des factures pour les clients sans code APE</h1>
				<?php 	}else{ ?>
							<h1>Liste des factures pour tous les clients</h1>
                <?php 	}
						if(!empty($factures)){
								$total_ht=0;
								$total_ttc=0;
								$total_regle=0;
								$total_reste_du=0;

								$total_cols=5; ?>

								<div class="table-responsive">
									<table class="table table-striped table-hover" >
										<thead>
											<tr class="dark2" >
												<th>Chrono</th>
												<th>Client</th>
										<?php	if($aff_detail_opca){
													$total_cols++; ?>
													<th>Entité de facturation</th>
										<?php	} ?>
												<th>Commercial</th>
												<th>Facture</th>
												<th>Date</th>
												<th class="text-right" >H.T.</th>
												<th class="text-right" >T.T.C.</th>
											</tr>
										</thead>
										<tbody>
								<?php		$page=array();

											foreach($factures as $f){

												$page[]=$f["fac_id"];

												// style de la ligne

												$class_fac="";
												$style="";



												$fac_date="";
												if(!empty($f['fac_date'])){
													$dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
													$fac_date=$dt_fac_date->format("d/m/Y");
												}
												$fac_date_reg_prev="";
												if(!empty($f['fac_date_reg_prev'])){
													$dt_fac_date_reg_prev=date_create_from_format("Y-m-d",$f['fac_date_reg_prev']);
													$fac_date_reg_prev=$dt_fac_date_reg_prev->format("d/m/Y");
												}
												$reste_du=$f['fac_total_ttc']-$f['fac_regle'];

												$total_ht+=$f['fac_total_ht'];
												$total_ttc+=$f['fac_total_ttc'];
												$total_regle+=$f['fac_regle'];
												$total_reste_du+=$reste_du;



												?>
												<tr <?=$class_fac?> <?=$style?> >
													<td><?= $f['fac_chrono'] ?></td>
													<td>
												<?php	if($_SESSION['acces']['acc_service'][2]==1){ ?>
															<a href="client_voir.php?client=<?=$f['fac_client']?>" <?=$style?> >
																<?=$f['cli_code']?>
															</a>
												<?php	}else{
															echo($f['cli_code']);
														} ?>
													</td>
											<?php	if($aff_detail_opca){ ?>
														<td><?=$f['fac_cli_nom']?></td>
											<?php	} ?>
													<td><?= $f['com_label_2'] ?> <?= $f['com_label_1'] ?></td>
													<td>
														<a href="facture_voir.php?facture=<?=$f['fac_id']?>" <?=$style?> >
															<?= $f['fac_numero'] ?>
														</a>
													</td>
													<td><?=	$fac_date?></td>
													<td class="text-right" ><?=number_format($f['fac_total_ht'], 2, ',', ' '); ?></td>
													<td class="text-right" >
										<?php			if($_SESSION["acces"]["acc_droits"][30]){ ?>
															<a href="reglement.php?facture=<?=$f['fac_id']?>" <?=$style?> >
																<?=number_format($f['fac_total_ttc'], 2, ',', ' ');?>
															</a>
										<?php			}else{
															echo(number_format($f['fac_total_ttc'], 2, ',', ' '));
														} ?>
													</td>
												</tr>
					<?php					}
										 ?>
										</tbody>
										<tfoot>
											<tr>
												<th colspan="<?=$total_cols?>" class="text-right" >Total :</th>
												<td class="text-right" ><?=number_format($total_ht, 2, ',', ' ');?></td>
												<td class="text-right" ><?=number_format($total_ttc, 2, ',', ' ');?></td>

											</tr>
										</tfoot>
									</table>
								</div>
			<?php 			}else{ ?>
								<div class="col-md-12 text-center" style="padding:0;" >
									<div class="alert alert-warning" style="border-radius:0px;">
										Aucune facture correspondant à votre recherche.
									</div>
								</div>
			<?php			} ?>


					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="stat_com_ape.php" class="btn btn-default btn-sm" >
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle">

					</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>


<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function (){


			});
		</script>
	</body>
</html>
