<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "modeles/mod_parametre.php";

	// SI C'EST UNE EDITION
	if(!empty($_GET['id'])){
		// attestation
		$req = $Conn->prepare("SELECT att_id,att_titre,att_statut,att_date_creation,att_date_prod,att_date_archive,att_contenu 
		,pfa_libelle,psf_libelle,pss_libelle
		FROM attestations LEFT OUTER JOIN Produits_Familles ON (attestations.att_famille=Produits_Familles.pfa_id)
		LEFT OUTER JOIN Produits_Sous_Familles ON (attestations.att_sous_famille=Produits_Sous_Familles.psf_id)
		LEFT OUTER JOIN Produits_Sous_Sous_Familles ON (attestations.att_sous_sous_famille=Produits_Sous_Sous_Familles.pss_id)
		WHERE att_id = :att_id ");
	    $req->bindParam(':att_id', $_GET['id']);
	    $req->execute();
	    $attestation = $req->fetch();
		
	    // competence
	    $req = $Conn->prepare("SELECT * FROM attestations_competences WHERE aco_attestation = :aco_attestation");
	    $req->bindParam(':aco_attestation', $_GET['id']);
	    $req->execute();
	    $competences = $req->fetchAll();
	}else{
		echo "erreur";
		die();
	}
	// FIN EDITION
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Si2P</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		
		<!-- Personalisation du theme -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<!-- Favicon -->
		<link rel="shortcut icon" href="theme/assets/img/favicon.ico" >

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm onload-check" >
	
		
			<!-- Start: Main -->
			<div id="main" >
				
		<?php	require "includes/header_def.inc.php"; ?>
				
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper" >
					<!-- DEBUT DU CONTENU -->
					<section id="content" class=""  >
					
						<div class="row" >	
							<div class="col-md-12">			
								<div class="panel">
									<div class="panel-heading">
										<span class="panel-title"><strong>Attestation : <?= $attestation['att_titre'] ?></strong></span>
									</div>
									<div class="panel-body">
										<div class="row" >	
											<div class="col-md-4">Famille : <strong><?=$attestation["pfa_libelle"]?></strong></div>
									<?php	if(!empty($attestation["psf_libelle"])){ ?>
												<div class="col-md-4">Sous-Famille : <strong><?=$attestation["psf_libelle"]?></strong></div>
									<?php	}
											if(!empty($attestation["pss_libelle"])){ ?>
												<div class="col-md-4">Sous Sous-Famille : <strong><?=$attestation["pss_libelle"]?></strong></div>
									<?php	} ?>
										</div>
										
									</div>
								</div>
							</div>
						</div>
							
							

						<div class="row" >		
							<div class="col-md-2">			
								<div class="panel">
									<div class="panel-heading">
										<span class="panel-title">Statut de l'attestation</span>
									</div>
									<div class="panel-body pn">
										<?php switch ($attestation['att_statut']) {
											case 0:  // PRE PROD?>
												<div class="panel panel-tile text-center panel-statut" style="border-radius:0px;margin-bottom:0;">
												    <div class="panel-body bg-warning light" style="border-radius:0px;">
												        <i class="fa fa-refresh  text-muted fs45 br64 bg-warning p15 ph20 mt10"></i>
												        <h1 class="fs35 mbn">Production</h1>
												        <h6 class="text-white"><?= convert_date_fr($attestation['att_date_creation']); ?></h6>
												    </div>
												</div>
										<?php		break;
											
											case 1: ?>
												<div class="panel panel-tile text-center panel-statut" style="border-radius:0px;margin-bottom:0;">
												    <div class="panel-body bg-success light" style="border-radius:0px;">
												        <i class="fa fa-check text-muted fs45 br64 bg-success p15 ph20 mt10"></i>
												        <h1 class="fs35 mbn">Application</h1>
												        <h6 class="text-white"><?= convert_date_fr($attestation['att_date_prod']); ?></h6>
												    </div>
												</div>
											<?php	break;
												case 2: ?>
												<div class="panel panel-tile text-center panel-statut" style="border-radius:0px;margin-bottom:0;">
												    <div class="panel-body bg-danger light" style="border-radius:0px;">
												        <i class="fa fa-archive text-muted fs45 br64 bg-danger p15 ph20 mt10"></i>
												        <h1 class="fs35 mbn">Archivée</h1>
												        <h6 class="text-white"><?= convert_date_fr($attestation['att_date_archive']); ?></h6>
												    </div>
												</div>
											<?php break;
										} ?>
										
										
									</div>
									
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="panel">
									<div class="panel-heading">
										<span class="panel-title">Liste des compétences</span>
									</div>
									<div class="panel-body pn">
										
										<div class="table-responsive">
											<table class="table table-striped" >
												<thead>
													<tr class="dark" >
														<th class="text-center">Libellé</th>
														<th class="text-center">Pratique</th>
														
													</tr>
												</thead>
												<tbody>
													<?php foreach($competences as $c){ ?>
														<tr>
															<td><?= $c['aco_competence'] ?></td>
															<?php if($c['aco_pratique'] == 1){ ?>
																<td>Oui</td>
															<?php }else{ ?>
																<td>Non</td>
															<?php }?>
														</tr>
													<?php } ?>
												<tbody>				
											</table>
										</div>
										
									</div>
									
								</div>
								
							</div>
							<div class="col-md-6">			
								<div class="panel">
									<div class="panel-heading">
										<span class="panel-title">Contenu de l'attestation</span>
									</div>
									<div class="panel-body">
										
										<?= $attestation['att_contenu'] ?>
										
									</div>
									
								</div>
							</div>
						
						</div>

					</section>
					<!-- FIN DU CONTENU -->
				</section>
	
			</div>
			<!-- End: Main -->
			
			<!-- DEBUT DU FOOTER -->
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
					<?php if(!empty($_SESSION['retour'])){ ?>
						<a href="<?=$_SESSION["retour"]?>" class="btn btn-default btn-sm" >
							<i class='fa fa-left'></i> Retour
						</a>
					<?php }else{ ?>
						<a href="attestation_liste.php" class="btn btn-default btn-sm" >
							<i class='fa fa-left'></i> Retour
						</a>
					<?php } ?>
					</div>
					<div class="col-xs-6" >
					</div>
					<div class="col-xs-3 footer-right" >
						<?php switch ($attestation['att_statut']) {
							case 0:  // PRE PROD?>
								<button type="button" class="btn btn-primary btn-sm" id="btn_prod" data-id="<?= $attestation['att_id'] ?>" data-toggle="modal" data-target="#modal1">Passer en application</button>
						<?php		break;
							
							case 1: ?>
								<button type="button" class="btn btn-primary btn-sm" id="btn_archive" data-id="<?= $attestation['att_id'] ?>" data-toggle="modal" data-target="#modal2">Archiver</button>
							<?php	break;
								case 2: ?>
								
							<?php break;
						} ?>
					</div>
				</div>
			</footer>
			<!-- FIN DE FOOTER  -->
			
<!-- Modal -->
			<div id="modal1" class="modal fade" role="dialog" style="z-index:999999">
			  <div class="modal-dialog modal-sm">

			    <!-- Modal content-->
			    <div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Passer en application</h4>
			      	</div>
			      	<div class="modal-body">
			        	<p>L'attestation sera passée en application, elle sera utilisée pour les formations.</p>
			      	</div>
			      	<div class="modal-footer">
			      		<button type="button" class="btn btn-primary" id="btn_prod_modal">Oui</button>
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			      	</div>
			    </div>

			  </div>
			</div>

		<!-- BEGIN: PAGE SCRIPTS -->
		<!-- Modal -->
			<div id="modal2" class="modal fade" role="dialog" style="z-index:999999">
			  <div class="modal-dialog modal-sm">

			    <!-- Modal content-->
			    <div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Archiver</h4>
			      	</div>
			      	<div class="modal-body">
			        	<p>L'attestation sera archivée, elle être dupliquée mais pas réutilisée.</p>
			      	</div>
			      	<div class="modal-footer">
			      		<button type="button" class="btn btn-primary" id="btn_archive_modal">Oui</button>
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			      	</div>
			    </div>

			  </div>
			</div>

		<!-- BEGIN: PAGE SCRIPTS -->

		<!-- jQuery -->
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function() {
				// clique sur le bouton prod
				$("#btn_prod").click(function() {
					id = $(this).data("id");
					$("#btn_prod_modal").data("id", id);
				});

				$("#btn_prod_modal").click(function() {
					passer_prod($(this).data("id"));
					$("#modal1").modal("hide");
				});

				// clique sur le bouton archive
				$("#btn_archive").click(function() {
					id = $(this).data("id");
					$("#btn_archive_modal").data("id", id);
				});

				$("#btn_archive_modal").click(function() {
					passer_archive($(this).data("id"));
					$("#modal2").modal("hide");
				});



			});

			////////////////// FONCTIONS ////////////////////////
			function passer_prod(id){
				$.ajax({
				    type:'POST',
				    url: 'ajax/ajax_attestation_prod.php',
				    data : 'att_id=' + id,
				    dataType: 'html',
				    success: function(data)         
				    {

						traitement_passer_prod(data);
				    }
				});
			}

			function traitement_passer_prod(date){
				$(".panel-statut > div").removeClass("bg-warning").addClass("bg-success");
				$(".panel-statut > div > i").removeClass("fa-refresh bg-warning").addClass("fa-check bg-success");
				$(".panel-statut > div > h1").html("Application");
				$(".panel-statut > div > h6").html(date);
				$("#btn_prod").html("Archiver");
				$("#btn_prod").attr("data-target", "#modal2");
				$("#btn_prod").attr("id", "btn_archive");
			}

			function passer_archive(id){
				$.ajax({
				    type:'POST',
				    url: 'ajax/ajax_attestation_archive.php',
				    data : 'att_id=' + id,
				    dataType: 'html',
				    success: function(data)         
				    {

						traitement_passer_archive(data);
				    }
				});
			}

			function traitement_passer_archive(date){
				$(".panel-statut > div").removeClass("bg-success").addClass("bg-danger");
				$(".panel-statut > div > i").removeClass("fa-check bg-success").addClass("fa-archive bg-danger");
				$(".panel-statut > div > h1").html("Archivée");
				$(".panel-statut > div > h6").html(date);
				$("#btn_archive").remove();
			}
			///////////// FIN FONCTIONS /////////////////////////
		</script>
	</body>
</html>