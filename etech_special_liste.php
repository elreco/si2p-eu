
<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_parametre.php');
	// TOUS LES THEMES

$sql="SELECT doc_id,doc_nom,doc_nom_aff,doc_date_creation,uti_prenom,uti_nom
 FROM documents 
LEFT JOIN documents_utilisateurs ON (documents.doc_id = documents_utilisateurs.dut_document)
LEFT OUTER JOIN utilisateurs ON (utilisateurs.uti_id=documents.doc_utilisateur)
WHERE  dut_utilisateur = " . $_GET['id'] . " AND dut_obligatoire = 1 AND (dut_vu = 0 OR ISNULL(dut_vu) ) ORDER BY doc_date_creation;";
$req=$Conn->query($sql);
$documents = $req->fetchAll();

$sql="SELECT uti_prenom, uti_nom FROM utilisateurs WHERE uti_id = " . $_GET['id'];
$req=$Conn->query($sql);
$uti = $req->fetch();
	

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Documents non consultés</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link href="assets/admin-tools/admin-forms/css/admin-forms-orion.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
	<!-- Start: Main -->
		<div id="main">
			<?php	
				include "includes/header_def.inc.php";
			?>	
				
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
				<header id="topbar">
	                <div class="text-center">
	                    <h4 style="margin:0;">Documents obligatoires non consultés de <strong class="text-primary"><?= $uti['uti_prenom'] ?> <?= $uti['uti_nom'] ?></strong></h4>
	                </div>
	            </header>		
				<section id="content" class="animated fadeIn">
				
					<div class="row" >
						<div class="col-md-12" >
							<div class="table-responsive" >
								<table class="table table-hover table-striped" >
									<thead>
					<tr class="dark">
						<th style="text-align:center!important;">Nom</th>
						<th style="text-align:center!important;">Date de dépôt</th>
						<th style="text-align:center!important;">Déposé par</th>
						<th style="text-align:center!important;">Télécharger</th>
					</tr>
				</thead>
									<tbody>
								<?php 	foreach($documents as $d){ ?>
						<tr>
							<td  style="text-align:center!important;">
								<?= $d['doc_nom_aff'] ?>
							</td>
							<td style="text-align:center!important;"><?= convert_date_txt($d['doc_date_creation']) ?></td>
							<td style="text-align:center!important;">
								<?=$d['uti_prenom'] ?> <?=$d['uti_nom'] ?>
							</td>
							<td style="text-align:center!important;">
								<span data-toggle="tooltip" title="Télécharger">
									<a href="documents/etech/<?= $d['doc_nom'] ?>" class="btn btn-warning btn-sm telecharger-fichier" data-id="<?= $d['doc_id'] ?>" download>
										<i class="fa fa-download" aria-hidden="true"></i>
									</a>
								</span>
							</td>
								

						</tr>
			<?php 	} ?>																			
									</tbody>
								</table>
							</div>
						</div>
					</div>
	
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="etech_special.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6">
					
				</div>
				<div class="col-xs-3 footer-right" >
					
				</div>
			</div>
		</footer>
		
	<?php
		include "includes/footer_script.inc.php"; ?>	                        

		
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script type="text/javascript">
			
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
