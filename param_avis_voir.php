<?php
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");
	
	include_once("modeles/mod_parametre.php");
	
	// ECRAN D'EDITION D'UN AVIS
	
	$erreur_txt="";
	
	$avis=0;
	if(isset($_GET["avis"])){
		$avis=intval($_GET["avis"]);
	}
	if($avis==0){
		$erreur_txt="Impossible de visualiser l'avis de stage.";
	}
	if(empty($erreur_txt)){
		try{
			$req=$Conn->query("SELECT * FROM Avis WHERE avi_id=" . $avis . ";");
			$d_avis=$req->fetch();
			if(empty($d_avis)){
				$erreur_txt="Impossible de visualiser l'avis de stage.";
			}
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}			
	}
	
	$sql="SELECT * FROM Avis_Rubriques LEFT JOIN Avis_Questions ON (Avis_Rubriques.aru_id=Avis_Questions.aqu_rubrique)
	LEFT JOIN Avis_Questionner ON (Avis_Questions.aqu_id=Avis_Questionner.aqu_question)
	WHERE aqu_avis=" . $avis . " ORDER BY aru_place,aqu_place,aqu_id;";
	$req=$Conn->query($sql);
	$d_questions=$req->fetchAll();

	
	$_SESSION["retour"]="avis_voir.php?avis=" . $avis;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	   
	   <!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper" class="" >
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
						
								
					
						<div class="panel">
							<div class="panel-heading">
								<span class="panel-title">
						<?php		echo($d_avis["avi_nom"]);
									if($d_avis["avi_type"]==1){
										echo(" - INTRA");
									}else{
										echo(" - INTER");
									}?>
									<div class="pull-right" >
							<?php		if($d_avis["avi_statut"]==0){
											echo("<strong class='text-primary' >Production</strong>");
										}elseif($d_avis["avi_statut"]==1){
											echo("<strong class='text-success' >Application</strong>");								
										}else{
											echo("<strong class='text-danger' >Archivé</strong>");	
										} ?>
									</div>
								</span>						
							</div>
					<?php	if($d_avis["avi_statut"]>0){ ?>
								<div class="panel-body">
									<div class="row" >
										<div class="col-md-6" >
											Mis en application le : <strong> <?=convert_date_txt($d_avis["avi_prod_deb"])?></strong>
										</div>
					<?php				if(!empty($d_avis["avi_prod_fin"])){ ?>
											<div class="col-md-6" >
												Archivé le : <strong><?=convert_date_txt($d_avis["avi_prod_fin"])?></strong>
											</div>
					<?php				} ?>
									</div>
								</div>
					<?php	} ?>
						</div>
						
				<?php	if(!empty($d_questions)){
							
							$rubrique=0;
							foreach($d_questions as $q){
								if($rubrique!=$q["aru_id"]){
									$rubrique=$q["aru_id"];
									echo("<h4>" . $q["aru_libelle"] . "</h4>");
								} ?>
								<div class="panel panel-success" >
									<div class="panel-heading ">
										<span class="panel-title">Question <?=$q["aqu_id"]?></span>									
									</div>
									<div class="panel-body">									
										<?=$q["aqu_texte"]?>
										<div class="row" >
											<div class="col-md-4 text-center" >
												<?=$q["aqu_min_lib"]?>
											</div>
											<div class="col-md-offset-4 col-md-4 text-center" >
												<?=$q["aqu_max_lib"]?>
											</div>
										</div>
										<div class="row" >
											<div class="col-md-4 text-center" >
												<?=$q["aqu_min_val"]?>
											</div>
											<div class="col-md-offset-4 col-md-4 text-center" >
												<?=$q["aqu_max_val"]?>
											</div>
										</div>
									</div>
								</div>						
			<?php			} 
						} ?>
					</div>
				</section>
				<!-- FIN DU CONTENU -->
			</section>

		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="param_avis_liste.php" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
			<?php	if($d_avis["avi_statut"]==0){ ?>
						<a href="param_avis.php?avis=<?=$d_avis["avi_id"]?>" class="btn btn-sm btn-warning" >
							<i class="fa fa-pencil" ></i> Modifier
						</a>													
						<button type="button" class="btn btn-sm btn-success" id="valider" >
							<i class="fa fa-check" ></i> Passer en application
						</a>
				
			<?php	} ?>
				</div>
			</div>
		</footer>
		
		<div id="modal_confirmation_user" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Confirmer ?</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							Annuler
						</button>
						<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
							Confirmer
						</button>
					</div>
				</div>
			</div>
		</div>
	
<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				$("#valider").click(function() {
					modal_confirmation_user("Vous êtes sur le point de passer cet avis en application.<br/> Vous ne pourrez plus le modifier et l'avis actuellement en application sera archivé.<br/>Êtes-vous sûr de vouloir continuer ?",valider_avis,"","","");
				});
			});
			function valider_avis(){
				document.location.href="param_avis_valide.php?avis=<?=$d_avis["avi_id"]?>";
			}
		</script>
	</body>
</html>