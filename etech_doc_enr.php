<?php
session_start();
//include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_document.php');
include('modeles/mod_upload.php');
include('modeles/mod_erreur.php');
include('modeles/mod_parametre.php');
include('modeles/mod_profil.php');
include('modeles/mod_societe.php');
include('modeles/mod_agence.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_produit.php');
/*
echo("<pre>");
	print_r($_POST);
echo("</pre>");

echo("<pre>");
	print_r($_GET);
echo("</pre>");
die();*/

$warning=array();

if($_POST && !isset($_POST['submit2']) && !isset($_GET['deleteall'])){
	// check si document nouveau est coché
	if(!empty($_POST['doc_nouveau'])){
		$doc_nouveau = 1;
	}else{
		$doc_nouveau = 0;
	}

	$nom = pathinfo($_FILES['doc']['name']);
	
	// formatage du nom sur le serveur
	$nom_srv=str_replace("#","_",$nom["filename"]);
	$nom_srv=str_replace("+","_",$nom_srv);
	$fichier_srv=$nom_srv . "." . $nom["extension"];
	
	if(isset($_GET['replace'])){
		$erreur = upload('doc', 'etech/',$nom_srv, 0, 0, 1);
	}else{
		$erreur = upload('doc', 'etech/',$nom_srv, 0, 0, 0);
	}

	if(get_erreur_txt($erreur) != "OK"){
		echo get_erreur_txt($erreur);
	}else{

		if(!isset($_GET['replace'])){
			$doc = insert_document($fichier_srv, clean($nom['filename']), $_POST['url'] ,$_SESSION['acces']['acc_ref_id'], $nom['extension'], date('Y-m-d'), 0);
			insert_document_utilisateur($_SESSION['acces']['acc_ref_id'], 0, $doc);
			// actualise le champs doc_nouveau
			$req=$Conn->prepare("UPDATE documents SET doc_nouveau = :doc_nouveau WHERE doc_id = :doc");
			$req->bindParam(':doc', $doc);
			$req->bindParam(':doc_nouveau', $doc_nouveau);
			$req->execute();

		}else{
			//historique
			$document_actuel = get_document($_GET['id']);
			$file = pathinfo($document_actuel['doc_nom']);

			insert_document_histo($_GET['id'], $document_actuel['doc_nom'], $document_actuel['doc_date_creation'], $document_actuel['doc_utilisateur']);
			update_document_mod($_GET['id'], $fichier_srv, $_SESSION['acces']['acc_ref_id'], $nom['extension'], date('Y-m-d'));
			$doc = $_GET['id'];
			// actualise le champs doc_nouveau
			$req=$Conn->prepare("UPDATE documents SET doc_nouveau = :doc_nouveau WHERE doc_id = :doc");
			$req->bindParam(':doc', $doc);
			$req->bindParam(':doc_nouveau', $doc_nouveau);
			$req->execute();

		}
		echo $doc; 
	}

}

// suppression d'un fichier/dossier simple
if(isset($_GET['delete'])){
	
	

	// chercher la liste des utilisateurs ayant accès au document
	$req=$Conn->prepare("SELECT uti_id FROM Utilisateurs LEFT JOIN documents_utilisateurs ON (Utilisateurs.uti_id = documents_utilisateurs.dut_utilisateur) WHERE dut_document = " . $_GET['delete']);
	$req->execute();
	$utilisateurs = $req->fetchAll();
	
	// chercher l'url du document
	$req=$Conn->prepare("SELECT doc_url FROM documents WHERE doc_id = " . $_GET['delete']);
	$req->execute();
	$doc = $req->fetch();
	
	delete_document($_GET['delete']);
	
	// actualiser l'affichage des dossiers pour la liste des utilisateurs
	delete_affichage_dossier($utilisateurs, $doc['doc_url']);


	

	if(isset($_GET['dos'])){
		header("Location: etech.php?dos=" . $_GET['dos'] . "&succes=5");
	}else{
		header("Location: etech.php?succes=5");
	}
	die();
}

if(isset($_GET['deleteall'])){
	

	// si c'est la fonction couper/coller
	if(isset($_POST['coller'])){

		if(!empty($_POST['check_list'])){
			foreach($_POST['check_list'] as $check) {
				/* DEBUT COLLER */
				$doc = get_document($check);
				if($doc['doc_url'] . $doc['doc_nom_aff'] . "/" != $_POST['coller']){


					if($doc['doc_dossier'] == 1){
						update_document($doc['doc_id'], $doc['doc_nom_aff'],$_POST['coller'], 1, $doc['doc_mot_cle'], $doc['doc_descriptif'], $_POST['doc_type'], $_POST['doc_client'], $_POST['doc_service'],0);
					}else{
						update_document($doc['doc_id'], $doc['doc_nom_aff'],$_POST['coller'], 0, $doc['doc_mot_cle'], $doc['doc_descriptif'], $_POST['doc_type'], $_POST['doc_client'], $_POST['doc_service'],0);
					}


					$sous_docs = get_documents($doc['doc_url'] . $doc['doc_nom_aff'] . "/");

					foreach($sous_docs as $s){
						if($s['doc_dossier'] == 1){
							update_document($s['doc_id'], $s['doc_nom_aff'],$_POST['coller'], 1, $s['doc_mot_cle'], $s['doc_descriptif'], $_POST['doc_type'], $_POST['doc_client'], $_POST['doc_service'],0);
						}else{
							update_document($s['doc_id'], $s['doc_nom_aff'],$_POST['coller'], 0, $s['doc_mot_cle'], $s['doc_descriptif'], $_POST['doc_type'], $_POST['doc_client'], $_POST['doc_service'],0);
						}

					}

					if(isset($_GET['dos'])){
						header("Location: etech.php?dos=" . $_GET['dos'] . "&succes=6");
					}else{
						header("Location: etech.php?succes=6");
					}

				}else{
					if(isset($_GET['dos'])){
						header("Location: etech.php?dos=" . $_GET['dos'] . "&erreur=4");
					}else{
						header("Location: etech.php?erreur=4");
					}
				}
			}
		}elseif(empty($_POST['check_list'])){
			header("Location: etech.php?erreur=5");
		}else{
			header("Location: etech.php?erreur=6");
		}
		/* FIN COLLER */

	// si c'est la fonction rechercher
	}else{



		if(!empty($_POST['check_list']) && isset($_POST['submit5'])){
			
			// SUPPRESSION MULTIPLE DOC
			
			foreach($_POST['check_list'] as $check){
				
				// chercher la liste des utilisateurs ayant accès au document
				$req=$Conn->prepare("SELECT uti_id FROM Utilisateurs LEFT JOIN documents_utilisateurs ON (Utilisateurs.uti_id = documents_utilisateurs.dut_utilisateur) WHERE dut_document = " . $check);
				$req->execute();
				$utilisateurs = $req->fetchAll();
				
				// chercher l'url du document
				$req=$Conn->prepare("SELECT doc_url FROM documents WHERE doc_id = " . $check);
				$req->execute();
				$doc = $req->fetch();

				delete_document($check);
				
				// actualiser l'affichage des dossiers pour la liste des utilisateurs
				delete_affichage_dossier($utilisateurs, $doc['doc_url']);
			}
			if(isset($_GET['dos'])){
				header("Location: etech.php?dos=" . $_GET['dos'] . "&succes=5");
			}else{
				header("Location: etech.php?succes=5");
			}
			
		}elseif(empty($_POST['check_list']) && isset($_POST['submit5'])){
			// AUCUN TRAITEMNT ERREUR FORM
			header("Location: etech.php?erreur=5");
		}else{
			// AUCUN TRAITEMNT ERREUR FORM
			header("Location: etech.php?erreur=6");
		}
	}
	
}



// MODIFICATION D'UN DOCUMENT
if(isset($_POST['submit2'])){
	
	$doc_id=0;
	if(!empty($_POST['doc_id'])){
		$doc_id=intval($_POST['doc_id']);
	};
	
	if($doc_id>0){
		
		// ELEMENT DU FORM
	
		if(!isset($_POST['doc_client'])){
			$_POST['doc_client'] = 0;
		}
		
		// on regarde si l'archive est cochée
		if(isset($_POST['doc_archive']) && $_POST['doc_archive'] == "doc_archive"){
			$_POST['doc_archive'] = 1;
		}else{
			$_POST['doc_archive'] = 0;
		}
		
		// on verifie que le repertoire ne contient pas un autre fichier du même nom 
		$check = check_doc_mod($_POST['doc_url'], $_POST['doc_nom_aff'],$doc_id);
		
		if(empty($check)){
			
			// on memorise les valeurs du doc avant modif
			$req=$Conn->query("SELECT doc_url FROM Documents WHERE doc_id=" . $doc_id . ";");
			$d_doc_old=$req->fetch();

			// MAJ DOC
			update_document($doc_id,$_POST['doc_nom_aff'], $_POST['doc_url'] ,0, $_POST['doc_mot_cle'], $_POST['doc_descriptif'], $_POST['doc_type'], $_POST['doc_client'], $_POST['doc_service'], 0, $_POST['doc_archive']);
			// actualise le champs doc_nouveau
			// check si document nouveau est coché
			if(!empty($_POST['doc_nouveau'])){
				$doc_nouveau = 1;
			}else{
				$doc_nouveau = 0;
			}
			$req=$Conn->prepare("UPDATE documents SET doc_nouveau = :doc_nouveau WHERE doc_id = :doc");
			$req->bindParam(':doc', $doc_id);
			$req->bindParam(':doc_nouveau', $doc_nouveau);
			$req->execute();

			// MAJ PRODUIT
			// on supp toutes les produits liées
			try{
				$req=$Conn->query("DELETE FROM Documents_Produits WHERE dpr_document=" . $doc_id . ";"); 
			}Catch(Exception $e){
				$erreur_produit=true;
			}
			// on enregistre les nouveaux si nécéssaire
			if(!isset($erreur_produit)){
				if(!empty($_POST['doc_produit'])){
					foreach($_POST['doc_produit'] as $pr){
						// get inutile car l'existant a été delete
						insert_document_produit($pr, $doc_id);					
					}
				}
			}else{
				$warning[]="Les codes produits n'ont pas été mis à jour";
			}

			// MAJ DES SOCIETES LIEES AU DOC		
			$get_doc_soc=$Conn->prepare("SELECT dso_document FROM Documents_Societes WHERE dso_document=:document AND dso_societe=:societe AND dso_agence=:agence;");
			$add_doc_soc=$Conn->prepare("INSERT INTO Documents_Societes (dso_document,dso_societe,dso_agence) VALUES (:document,:societe,:agence);");
			$del_doc_soc=$Conn->prepare("DELETE FROM Documents_Societes WHERE dso_document=:document AND dso_societe=:societe AND dso_agence=:agence");
			
			$zero=0;
			
			$sql="SELECT soc_id,age_id FROM Societes LEFT JOIN Agences ON (Societes.soc_id=Agences.age_societe AND soc_agence AND NOT age_archive) WHERE NOT soc_archive ORDER BY soc_id,age_id;";
			$req=$Conn->query($sql);
			$d_societes=$req->fetchAll();
			
			$soc_id=0;
			foreach($d_societes as $s){
				
				if($s["soc_id"]!=$soc_id){
					
					$soc_id=$s["soc_id"];
					
					// enregistrement avant modif
					$get_doc_soc->bindParam(":document",$_POST['doc_id']);
					$get_doc_soc->bindParam(":societe",$s['soc_id']);
					$get_doc_soc->bindParam(":agence",$zero);
					$get_doc_soc->execute();
					$d_doc_soc=$get_doc_soc->fetch();
					
					if(isset($_POST["dso" . $s['soc_id']]) && $_POST["dso" . $s['soc_id']] == "checked"){
						
						// SOCIETE EST COCHE

						if(empty($d_doc_soc)){
							// L'ENREGISTREMENT N'EXISTE PAS 
							// AJOUT
							$add_doc_soc->bindParam(":document",$_POST['doc_id']);
							$add_doc_soc->bindParam(":societe",$s["soc_id"]);
							$add_doc_soc->bindParam(":agence",$zero);
							$add_doc_soc->execute();

						}
						
					}else{
						
						// PAS COCHE
						if(!empty($d_doc_soc)){
							// L'ENREGISTREMENT N'EXISTE PAS 
							// AJOUT
							$del_doc_soc->bindParam(":document",$_POST['doc_id']);
							$del_doc_soc->bindParam(":societe",$s["soc_id"]);
							$del_doc_soc->bindParam(":agence",$zero);
							$del_doc_soc->execute();					
						}
						
					}
				}
				
				// AGENCE
				
				if(!empty($s["age_id"])){
					
					// enregistrement avant modif
					$get_doc_soc->bindParam(":document",$_POST['doc_id']);
					$get_doc_soc->bindParam(":societe",$s["soc_id"]);
					$get_doc_soc->bindParam(":agence",$s["age_id"]);
					$get_doc_soc->execute();
					$d_doc_soc=$get_doc_soc->fetch();
					
					if(isset($_POST["dso_agence" . $s['age_id']]) && $_POST["dso_agence" . $s['age_id']] == "checked"){
						
						// AGENCE EST COCHE

						if(empty($d_doc_soc)){
							// L'ENREGISTREMENT N'EXISTE PAS 
							// AJOUT
							$add_doc_soc->bindParam(":document",$_POST['doc_id']);
							$add_doc_soc->bindParam(":societe",$s["soc_id"]);
							$add_doc_soc->bindParam(":agence",$s["age_id"]);
							$add_doc_soc->execute();					
						}
						
					}else{
						
						// PAS COCHE
						if(!empty($d_doc_soc)){
							// L'ENREGISTREMENT N'EXISTE PAS 
							// AJOUT
							$del_doc_soc->bindParam(":document",$_POST['doc_id']);
							$del_doc_soc->bindParam(":societe",$s["soc_id"]);
							$del_doc_soc->bindParam(":agence",$s["age_id"]);
							$del_doc_soc->execute();					
						}
						
					}
					
				}
				
			}
			// FIN MAJ DES SOCIETES LIEES AU DOC	
			
			
			// MAJ DES PROFILS LIES AUX DOCS		
			$get_doc_pro=$Conn->prepare("SELECT dpr_profil,dpr_obligatoire FROM Documents_Profils WHERE dpr_document=:document AND dpr_profil=:profil;");
			$add_doc_pro=$Conn->prepare("INSERT INTO Documents_Profils (dpr_document,dpr_profil,dpr_obligatoire) VALUES (:document,:profil,:obligatoire);");
			$del_doc_pro=$Conn->prepare("DELETE FROM Documents_Profils WHERE dpr_document=:document AND dpr_profil=:profil");
			$up_doc_pro=$Conn->prepare("UPDATE Documents_Profils SET dpr_obligatoire=:obligatoire WHERE dpr_document=:document AND dpr_profil=:profil");

			$sql="SELECT pro_id FROM Profils ORDER BY pro_id;";
			$req=$Conn->query($sql);
			$d_profils=$req->fetchAll();
			foreach($d_profils as $p){
				
				// enregistrement avant modif
				$get_doc_pro->bindParam(":document",$_POST['doc_id']);
				$get_doc_pro->bindParam(":profil",$p['pro_id']);
				$get_doc_pro->execute();
				$d_doc_pro=$get_doc_pro->fetch();
					
				if(isset($_POST["dpr" . $p['pro_id']]) && $_POST["dpr" . $p['pro_id']] == "checked"){
					
					// COCHE
					
					$obligatoire=0;
					if(isset($_POST["dpr_obligatoire" . $p['pro_id']]) && $_POST["dpr_obligatoire" . $p['pro_id']] == "checked"){				
						// consultation obligatoire
						$obligatoire=1;
					}
						
					if(empty($d_doc_pro)){
						
						// L'ENREGISTREMENT N'EXISTE PAS 
						// AJOUT
						$add_doc_pro->bindParam(":document",$_POST['doc_id']);
						$add_doc_pro->bindParam(":profil",$p["pro_id"]);
						$add_doc_pro->bindParam(":obligatoire",$obligatoire);
						$add_doc_pro->execute();	
						
					}elseif($d_doc_pro["dpr_obligatoire"]!=$obligatoire){
						// changement etat obligatoire
						$up_doc_pro->bindParam(":document",$_POST['doc_id']);
						$up_doc_pro->bindParam(":profil",$p["pro_id"]);
						$up_doc_pro->bindParam(":obligatoire",$obligatoire);
						$up_doc_pro->execute();					
							  
					}
						
				}elseif(!empty($d_doc_pro)){
					
							// PLUS COCHE
					$del_doc_pro->bindParam(":document",$_POST['doc_id']);
					$del_doc_pro->bindParam(":profil",$p["pro_id"]);
					$del_doc_pro->execute();	
				}
			}
			// FIN DE PROFIL
			
			/****************************************************
				TRAITEMENTS ASSOCIES -> CONSEQUENCE DE LA MODIF
			*****************************************************/
			
			// DEPLACEMENT
			if($d_doc_old["doc_url"]!=$_POST["doc_url"]){
				
				// on controle que les utilisateurs on toujours 
					
				echo("EH TU M'A DEPLACE");
				
				// on recupere les utilisateurs qui ont accès aux doc avant modif
				$sql="SELECT uti_id FROM Utilisateurs 
				LEFT JOIN Documents_Utilisateurs ON (Utilisateurs.uti_id=Documents_Utilisateurs.dut_utilisateur)
				WHERE dut_document=:document
				ORDER BY uti_id;";
				$req_uti_dep=$Conn->prepare($sql);
				$req_uti_dep->bindParam(":document",$doc_id);
				$req_uti_dep->execute();
				$uti_dep=$req_uti_dep->fetchAll();
				if(!empty($uti_dep)){
					
					// doc est déjà modifié donc il ne se trouve plus dans $d_doc_old["doc_url"]
					delete_affichage_dossier($uti_dep,$d_doc_old["doc_url"]);
				}
				
			}

			// calcul des utilisateurs n'ayant pas encore accès au document et qui devraient y avoir accès
			$add_doc_uti=$Conn->prepare("INSERT INTO Documents_Utilisateurs (dut_utilisateur,dut_document,dut_obligatoire) VALUES (:utilisateur,:document,:obligatoire);");
			$up_doc_uti=$Conn->prepare("UPDATE Documents_Utilisateurs SET dut_obligatoire = :obligatoire WHERE dut_utilisateur = :utilisateur AND dut_document = :document;");
			
			$sql="SELECT uti_id,dut_utilisateur,dpr_obligatoire FROM Utilisateurs 
			LEFT JOIN Documents_Societes ON (Utilisateurs.uti_societe=Documents_Societes.dso_societe AND Utilisateurs.uti_agence=Documents_Societes.dso_agence)
			LEFT JOIN Documents_Profils ON (Utilisateurs.uti_profil=Documents_Profils.dpr_profil)
			LEFT OUTER JOIN Documents_Utilisateurs ON (Utilisateurs.uti_id=Documents_Utilisateurs.dut_utilisateur AND dut_document=:document)
			WHERE dso_document=:document AND dpr_document=:document
			ORDER BY uti_id;";
			$req_uti_autorises=$Conn->prepare($sql);
			$req_uti_autorises->bindParam(":document",$_POST['doc_id']);
			$req_uti_autorises->execute();
			$d_uti_autorises=$req_uti_autorises->fetchAll();
			if(!empty($d_uti_autorises)){
				// gérer l'affichage des dossiers
				set_affichage_dossier($d_uti_autorises, $_POST['doc_url']);
				foreach($d_uti_autorises as $auth){
					if(empty($auth["dut_utilisateur"])){
						// ON AJOUTE L'ACESS AU DOC
						echo("INSERT");
						$add_doc_uti->bindParam(":document",$_POST['doc_id']);
						$add_doc_uti->bindParam(":utilisateur",$auth["uti_id"]);
						$add_doc_uti->bindParam(":obligatoire",$auth["dpr_obligatoire"]);
						$add_doc_uti->execute();
					}else{
						echo("UPDATE");
						// ON AJOUTE L'ACESS AU DOC
						$up_doc_uti->bindParam(":document",$_POST['doc_id']);
						$up_doc_uti->bindParam(":utilisateur",$auth["uti_id"]);
						$up_doc_uti->bindParam(":obligatoire",$auth["dpr_obligatoire"]);
						$up_doc_uti->execute();
					}
					
					

				}
				
			}
			/*echo("AUTORISE<br/>");
			echo("<pre>");
			print_r($d_uti_autorises);
			echo("</pre>");*/
			
			// calcul des utilisateurs ne devant plus avoir acces au document
			
			$del_doc_uti=$Conn->prepare("DELETE FROM Documents_Utilisateurs WHERE dut_document=:document AND dut_utilisateur=:utilisateur");
			
			$sql="SELECT uti_id,dut_utilisateur FROM Utilisateurs 
			LEFT JOIN Documents_Utilisateurs ON (Utilisateurs.uti_id=Documents_Utilisateurs.dut_utilisateur)";
			$sql.="LEFT OUTER JOIN Documents_Societes ON 
			(Utilisateurs.uti_societe=Documents_Societes.dso_societe AND Utilisateurs.uti_agence=Documents_Societes.dso_agence
			AND Documents_Societes.dso_document=:document)";
			$sql.="LEFT OUTER JOIN Documents_Profils 
			ON (Utilisateurs.uti_profil=Documents_Profils.dpr_profil AND dpr_document=:document)";
			$sql.=" WHERE dut_document=:document AND (ISNULL(dso_document) OR ISNULL(dpr_document))"; 
			$sql.=" ORDER BY uti_id;";
			$req_uti_non_autorises=$Conn->prepare($sql);
			$req_uti_non_autorises->bindParam(":document",$_POST['doc_id']);
			$req_uti_non_autorises->execute();
			$d_uti_non_autorises=$req_uti_non_autorises->fetchAll();
			
			if(!empty($d_uti_non_autorises)){

				// supp l'acces au document
				foreach($d_uti_non_autorises as $no_auth){
					
					// ON AJOUTE L'ACESS AU DOC
					$del_doc_uti->bindParam(":document",$_POST['doc_id']);
					$del_doc_uti->bindParam(":utilisateur",$no_auth["uti_id"]);
					$del_doc_uti->execute();
										
				}
				// gérer la suppression de l'affichage des dossiers parents
				delete_affichage_dossier($d_uti_non_autorises, $_POST['doc_url']);

				
			}
			
			if(!empty($_POST['dos'])){
				$dos = get_document($_POST['dos']);
				if(isset($_POST['upload'])){
					header("Location: etech.php?dos=" . $dos['doc_id'] . "&succes=4");
				}else{
					header("Location: etech.php?dos=" . $dos['doc_id'] . "&succes=3");
				}
			}else{
				if(isset($_POST['upload'])){
					header("Location: etech.php?succes=4");
				}else{
					header("Location: etech.php?succes=3");
				}
			}

		}else{
			if(isset($_POST['dos'])){
				header("Location: etech_doc_mod.php?id=" . $_POST['doc_id'] . "&dos=" . $_POST['dos'] . "&erreur=2");
			}else{
				header("Location: etech_doc_mod.php?id=" . $_POST['doc_id'] . "&erreur=2");
			}
		}
		die("je suis la");
	}
}
// FIN MODIFCATION DOC
?>

