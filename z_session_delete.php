<?php 
include "includes/controle_acces.inc.php";
include "includes/connexion.php";
include "modeles/mod_parametre.php";

// LES STAGIAIRES
$req = $Conn->prepare("DELETE FROM sessions_stagiaires WHERE sst_session = " . $_GET['id']);
$req->execute();

// LES HORAIRES
$req = $Conn->prepare("DELETE FROM sessions_horaires WHERE sho_session = " . $_GET['id']);
$req->execute();

// LA SESSION
$req = $Conn->prepare("DELETE FROM sessions WHERE ses_id = " . $_GET['id']);
$req->execute();



Header('Location: session_liste.php');
?>