<?php 

// CREATION OU MAJ D'UNE FICHE STAGIAIRE
/* NOTE :
seul la maj fonctionne. La création des stagiaires n'existe pas en direct. Elle est déclencher la la céation d'une fiche stagiaire ou par des inscription. */
	
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');

$stagiaire=0;
if(isset($_GET['stagiaire'])){
	if(!empty($_GET['stagiaire'])){
		$stagiaire=intval($_GET['stagiaire']);
	}
}
if($stagiaire==0){
	echo("impossible d'afficher la page");
	die();
}


// DONNEES POUR AFFICHAGE


// personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}

// HISTORIQUE DES EMPLOYEURS
// on s'assure que le stagiaire a été lié à un client auquel à accès l'utilisateur

$sql="SELECT cli_code,cli_nom,cli_stagiaire,cli_categorie FROM Stagiaires_Clients INNER JOIN Clients ON (Stagiaires_Clients.scl_client=Clients.cli_id) 
INNER JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client) WHERE scl_stagiaire=:stagiaire AND cso_societe=:societe";
if(!empty($acc_agence)){
	$sql.=" AND cso_agence=:agence";
}
$sql.=" ORDER BY cli_code,cli_nom;";
$req = $Conn->prepare($sql);
$req ->bindParam(":stagiaire",$stagiaire);
$req ->bindParam(":societe",$acc_societe);
if(!empty($acc_agence)){
	$req ->bindParam(":agence",$acc_agence);
}
$req->execute();
$d_clients=$req->fetchAll();
if(empty($d_clients)){
	echo("Impossible d'afficher la page!");
	die();
}

// LE STAGIAIRE

$req = $Conn->prepare("SELECT * FROM Stagiaires WHERE sta_id = :sta_id");
$req->bindParam(':sta_id',$stagiaire);
$req->execute();
$d_stagiaire = $req->fetch();
if(!empty($d_stagiaire)){
	
	$sta_naissance="";
	if(!empty($d_stagiaire["sta_naissance"])){
		$Dt_naiss=DateTime::createFromFormat('Y-m-d',$d_stagiaire["sta_naissance"]);
		$sta_naissance=$Dt_naiss->format("d/m/Y");
	}
	
}else{
	echo("Impossible d'afficher la page!");
	die();
}

// FICHE CLIENT PARTICULIER
// on ne peut pas utiliser $d_clients car ne contient que les clients accéssibles à l'Uti alors que la maj de la fiche particulier est indépendante de l'accès
$sql="SELECT cli_code,cli_nom FROM Clients WHERE cli_categorie=3 AND cli_stagiaire=:stagiaire";
$sql.=" ORDER BY cli_code,cli_nom;";
$req = $Conn->prepare($sql);
$req ->bindParam(":stagiaire",$stagiaire);
$req->execute();
$d_particuliers=$req->fetchAll();

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Stagiaire</title> 
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<!-- Admin forms -->
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<!-- SELECT2 -->
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
		<!-- ORION.CSS a mettre toujours à la fin -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="icon" type="image/png" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="stagiaire_enr.php" >
			<input type="hidden" name="sta_id" value="<?=$stagiaire?>">
			<div id="main">
<?php 			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" >
					<section id="content" class="animated fadeIn">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary ">									
									<div class="panel heading-border panel-primary">
									
										<div class="panel-body bg-light">
											<div class="content-header">
									<?php 		if(!empty($stagiaire)){ ?>
													<h2>Modifier la fiche stagiaire de <b class="text-primary"><?= $d_stagiaire['sta_prenom'] ?> <?= $d_stagiaire['sta_nom'] ?></b></h2>  
									<?php 		}else{ ?>
													<h2>Nouveau <b class="text-primary"> stagiaire</b></h2>
									<?php 		} ?>   
												<span style="color:red">*</span> Les champs marqués d'un astérisque sont obligatoires       
											</div>									
											<div class="col-md-10 col-md-offset-1">		
																						
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Identité</span>
														</div>
													</div>
												</div>
												<div class="row mt5" >
													<div class="col-md-3" >
														<label for="sta_titre" >Civilité :</label>
														<select class="select2" id="sta_titre" name="sta_titre" required>										
											<?php 			foreach($base_civilite as $k => $b){ 
																if($k > 0){
																	if($k==$d_stagiaire['sta_titre']){
																		echo("<option value='" . $k . "' selected >" . $b . "</option>");
																	}else{
																		echo("<option value='" . $k . "' >" . $b . "</option>");
																	}
																}
															} ?>
														</select>
													</div>
													<div class="col-md-3" >
														<label for="sta_nom" >Nom :</label>	
														<input type="text" name="sta_nom" id="sta_nom" class="gui-input nom" placeholder="Nom" value="<?= $d_stagiaire['sta_nom'] ?>" required />									
													</div>
													<div class="col-md-3" >
														<label for="sta_nom" >Prénom :</label>	
														<input type="text" name="sta_prenom" class="gui-input prenom" placeholder="Prénom" value="<?= $d_stagiaire['sta_prenom'] ?>" required />							
													</div>
													<div class="col-md-3" >
														<label for="sta_naissance" >Date de naissance :</label>
														<span  class="field prepend-icon">
															<input type="text" id="sta_naissance" name="sta_naissance" class="gui-input datepicker" placeholder="Date de naissance" value="<?=$sta_naissance?>" />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Employeur</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-5" >Code : <b><?=$d_stagiaire['sta_client_code']?></b></div>
													<div class="col-md-5" >Nom : <b><?=$d_stagiaire['sta_client_nom']?></b></div>
													<div class="col-md-2" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" name="employeur" id="employeur" value="on">
																<span class="checkbox"></span>Changer
															</label>
														</div>
													</div>
												</div>
												<div class="row mt15" id="bloc_employeur" style="display:none;" >
													<div class="col-md-12" >
														<label for="sta_client" >Employeur :</label>
														<select name="sta_client" id="sta_client" class="select2-client-n" >
															<option value="0">Employeur ...</option>
														</select>
													</div>
												</div>
												
												<div class="row" >
													<div class="col-md-6">
													
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Adresse</span>
																</div>
															</div>
														</div>
												
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="text" name="sta_ad1" id="sta_ad1" class="gui-input" placeholder="Adresse" value="<?=$d_stagiaire['sta_ad1']?>" >
																		<label for="sta_ad1" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="text" name="sta_ad2" class="gui-input" id="sta_ad2" placeholder="Adresse (Complément 1)" value="<?=$d_stagiaire['sta_ad2']?>" >
																		<label for="sta_ad2" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="text" name="sta_ad3" id="sta_ad3" class="gui-input" placeholder="Adresse (Complément 2)" value="<?=$d_stagiaire['sta_ad3']?>" >
																		<label for="sta_ad3" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="row">
															<div class="col-md-3">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="text" id="sta_cp" name="sta_cp" class="gui-input code-postal" placeholder="Code postal" value="<?=$d_stagiaire['sta_cp']?>" >
																		<label for="sta_cp" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-9">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="text" name="sta_ville" id="sta_ville" class="gui-input nom" placeholder="Ville" value="<?=$d_stagiaire['sta_ville']?>" >
																		<label for="uti_ville" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
													
													<div class="col-md-6">
													
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Coordonnées</span>
																</div>
															</div>
														</div>
														
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="tel" name="sta_tel" id="sta_tel"  class="gui-input telephone" placeholder="Numéro de téléphone" value="<?= $d_stagiaire['sta_tel'] ?>" >
																		<label for="sta_tel" class="field-icon">
																			<i class="fa fa fa-phone"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="tel" name="sta_portable" id="sta_portable" class="gui-input telephone" placeholder="Numéro de mobile" value="<?= $d_stagiaire['sta_portable'] ?>" >
																		<label for="sta_portable" class="field-icon">
																			<i class="fa fa-mobile"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">												
															<div class="col-md-12">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="email" name="sta_mail_perso" id="sta_mail_perso" class="gui-input" placeholder="Adresse Email" value="<?= $d_stagiaire['sta_mail_perso'] ?>" >
																		<label for="sta_mail_perso" class="field-icon">
																			<i class="fa fa-envelope"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Autres informations</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-4">
																<label for="sta_nom" >Code postal de naissance :</label>	
																<input type="text" name="sta_cp_naiss" class="gui-input code-postal" placeholder="Code postal" value="<?= $d_stagiaire['sta_cp_naiss'] ?>" />													
															</div>
															<div class="col-md-8">
																<label for="sta_nom" >Ville de naissance :</label>	
																<input type="text" name="sta_ville_naiss" class="gui-input" placeholder="Ville" value="<?= $d_stagiaire['sta_ville_naiss'] ?>" />													
															</div>
														</div>
													</div>																						
												</div>
										<?php	if(!empty($d_particuliers)){ 
													if(count($d_particuliers)==1){ ?>
														<p>
															Cette fiche stagiaire est liée au client particulier <b><?=$d_particuliers[0]["cli_code"] . " " . $d_particuliers[0]["cli_nom"]?></b>.
															Le client sera automatiquement mise à jour avec les données du stagiaire.
														</p>
										<?php		}else{ ?>
														<div class="alert alert-danger" >
															<p>
																<b>Erreur de configuration !</b><br/>Ce stagiaire est associé à plusieurs clients "Particulier".
															</p>
															
															<ul>
														<?php	foreach($d_particuliers as $particulier){
																	echo("<li>" . $particulier["cli_code"] . " " . $particulier["cli_nom"]. "</li>");
																} ?>
															</ul>
														</div>
										<?php		}
												}?>											
											</div>
										</div>				
									</div>                             
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="stagiaire_voir.php?stagiaire=<?=$stagiaire?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
									Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle">

					</div>
					<div class="col-xs-3 footer-right">
						
						<button type="submit" name="search" class="btn btn-success btn-sm">
							<i class='fa fa-floppy-o'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php 
		include "includes/footer_script.inc.php"; ?>  
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				$("#employeur").click(function () {
					if($(this).is(":checked")){
						$("#bloc_employeur").show();
					}else{
						$("#bloc_employeur").hide();
					}
				});
			});
		</script>
	</body>
</html>