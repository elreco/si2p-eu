<?php
/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');
///////////////////// Contrôles des parametres ////////////////////
$param_commande = 0;
if (!empty($_GET['utilisateur'])) {
    // si les get sont pas remplis
    $param_commande = intval($_GET['utilisateur']);
}
if ($param_commande == 0) {
    echo ("Impossible d'afficher la page");
    die();
}
///////////////////// FIN Contrôles des parametres ////////////////////
/////////////// TRAITEMENTS BDD ///////////////////////
if (!empty($_GET['id'])) { // si la ndf existe
    // sélectionner la ndf
    $req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id =" . $_GET['id']);
    $req->execute();
    $ndf = $req->fetch();
}
// récupérer l'utilisateur
$req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id =" . $_GET['utilisateur']);
$req->execute();
$utilisateur = $req->fetch();
// fin récupérer l'utilisateur
$format = "d/m/Y";
// le premier jour du mois
$premier_jour = new DateTime("first day of this month");
$premier_jour = $premier_jour->format('d/m/Y');
// le milieu 15
$milieu_1 = DateTime::createFromFormat($format, "15/" . date("m") . "/" . date("Y"));
$milieu_1_str = $milieu_1->format('d/m/Y');
// le milieu 16
$milieu_2 = "16/" . date("m") . "/" . date("Y");
$dernier_jour = new DateTime('last day of this month');
$dernier_jour = $dernier_jour->format('d/m/Y');
// le dernier jour du mois
$now = DateTime::createFromFormat($format, date("d/m/Y"));
// déterminer la quinzaine
/*si l'utilisateur a une carte affaire :
il faut que ce soit découpé sur le mois et pas en quinzaine */
if (!empty($_GET['id'])) {
    $req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id =" . $_GET['id']);
    $req->execute();
    $ndf_actuelle = $req->fetch();
}
if ($utilisateur['uti_carte_affaire'] == 1) {
    if (!empty($ndf_actuelle)) { // si la ndf est renseignée on utilise les champs dans la table
        $date1 =  convert_date_txt($ndf_actuelle['ndf_quinzaine_deb']);
        $date2 =  convert_date_txt($ndf_actuelle['ndf_quinzaine_fin']);
    } else {
        $date1 = $premier_jour;
        $date2 = $dernier_jour;
    }
} else {
    if (!empty($ndf_actuelle)) { // si la ndf est renseignée on utilise les champs dans la table
        $date1 =  convert_date_txt($ndf_actuelle['ndf_quinzaine_deb']);
        $date2 =  convert_date_txt($ndf_actuelle['ndf_quinzaine_fin']);
    } else {
        if ($now > $milieu_1) {
            /////////////// DATE 1 ==> DEBUT /////////////////
            /////////////// DATE 2 ==> FIN ///////////////////
            $date1 = $milieu_2;
            $date2 = $dernier_jour;
        } else {
            /////////////// DATE 1 ==> DEBUT /////////////////
            /////////////// DATE 2 ==> FIN ///////////////////
            $date1 = $premier_jour;
            $date2 = $milieu_1_str;
        }
    }
}
// retrouver le jour des deux dates
$day1 = substr($date1, 0, 2);
$day2 = substr($date2, 0, 2);
// si c'est un édition, aller chercher la ligne
if (!empty($_GET['ligne'])) {
    $req = $Conn->prepare("SELECT * FROM ndf_lignes WHERE nli_id =" . $_GET['ligne']);
    $req->execute();
    $nli = $req->fetch();
    if(!empty($nli['nli_vehicule_categorie'])){
        $sql="SELECT veh_id, veh_libelle, veh_immat FROM vehicules WHERE veh_categorie = " . $nli['nli_vehicule_categorie'];
        $req = $Conn->query($sql);
        $d_vehicules=$req->fetchAll();
    }
} else {
    $sql="SELECT veh_id, veh_libelle, veh_immat FROM vehicules WHERE veh_categorie = 1";
	$req = $Conn->query($sql);
	$d_vehicules=$req->fetchAll();
}

$sql="SELECT * FROM vehicules_categories WHERE vca_id != 4";
$req = $Conn->query($sql);
$d_vehicules_categories=$req->fetchAll();

if (!empty($ndf_actuelle) && !empty($nli)) {
    $sql="SELECT * FROM ndf_invitations
    LEFT JOIN utilisateurs ON (utilisateurs.uti_id = ndf_invitations.nin_utilisateur_invite)
    WHERE nin_ndf = " . $ndf_actuelle['ndf_id'] . " AND nin_ligne = " . $nli['nli_id'] . " ORDER BY uti_nom, uti_prenom DESC";
    $req = $Conn->query($sql);
    $ndf_invitations=$req->fetchAll();
}
if (!empty($ndf_actuelle)) {
    $sql="SELECT * FROM utilisateurs WHERE uti_archive = 0 AND uti_id != " . $ndf_actuelle['ndf_utilisateur'] . " ORDER BY uti_nom, uti_prenom DESC";
    $req = $Conn->query($sql);
    $utilisateurs=$req->fetchAll();
} else {
    $sql="SELECT * FROM utilisateurs WHERE uti_archive = 0 AND uti_id != " . $_SESSION['acces']['acc_ref_id'] . " ORDER BY uti_nom, uti_prenom DESC";
    $req = $Conn->query($sql);
    $utilisateurs=$req->fetchAll();
}

// comptes ndf
$req = $Conn->prepare("SELECT * FROM comptes");
$req->execute();
$comptes = $req->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Enregistrer une dépense</title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
<body class="sb-top sb-top-sm ">
    <form method="post" action="ndf_ligne_enr.php" id="form" enctype="multipart/form-data">
        <!-- Start: Main -->
        <div id="main">
            <?php include "includes/header_def.inc.php"; ?>
            <!-- Start: Content-Wrapper -->
            <section id="content_wrapper">
                <!--DEBUT CONTENT -->
                <section id="content" class="animated fadeIn">
                    <!--DEBUT ROW -->
                    <div class="row">
                        <!--DEBUT COL-MD-10 -->
                        <div class="col-md-10 col-md-offset-1">
                            <!--DEBUT ADMIN FORM -->
                            <div class="admin-form theme-primary ">
                                <!--DEBUT PANEL HEADING -->
                                <div class="panel heading-border panel-primary">
                                    <!--DEBUT PANEL BODY -->
                                    <div class="panel-body bg-light">
                                        <div class="content-header">
                                            <h2>Enregistrer une dépense pour la période du <b class="text-primary"><?= $date1 ?></b> au <b class="text-primary"><?= $date2 ?></b></h2>
                                        </div>
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <input type="hidden" name="utilisateur" value="<?= $_GET['utilisateur'] ?>">
                                                <?php if (!empty($_GET['id'])) { // si la ndf existe
                                                ?>
                                                    <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
                                                <?php } else { // si la ndf existe pas
                                                ?>
                                                    <input type="hidden" name="id" value="0">
                                                <?php } ?>
                                                <?php if (!empty($nli)) { // si c'est une édition
                                                ?>
                                                    <input type="hidden" name="ligne" value="<?= $nli['nli_id'] ?>">
                                                <?php } ?>
                                                <input type="hidden" name="ndf_quinzaine_deb" value="<?= convert_date_sql($date1) ?>">
                                                <input type="hidden" name="ndf_quinzaine_fin" value="<?= convert_date_sql($date2) ?>">
                                                <div class="col-md-4">
                                                    <p class="text-left mb20" style="font-weight:bold;">Date :</p>
                                                    <input required type="text" min="0" name="nli_date" id="nli_date" class="date-2" value="<?= !empty($nli) ? date("d", strtotime($nli['nli_date'])) : date("d") ?>" placeholder="Total"> /
                                                    <?php if (!empty($_GET['id'])) { ?>
                                                        <?= date("m", strtotime($ndf_actuelle['ndf_quinzaine_deb'])); ?>/<?= date("Y", strtotime($ndf_actuelle['ndf_quinzaine_fin'])); ?>
                                                        <input type="hidden" name="ndf_date_mois" id="ndf_date_mois" value="<?= date("m", strtotime($ndf_actuelle['ndf_date'])) ?>">
                                                        <input type="hidden" name="ndf_date_annee" id="ndf_date_annee" value="<?= date("Y", strtotime($ndf_actuelle['ndf_date'])) ?>">
                                                    <?php } else { ?>
                                                        <?= date("m") ?>/<?= date("Y") ?>
                                                        <input type="hidden" name="ndf_date_mois" id="ndf_date_mois" value="<?= date("m") ?>">
                                                        <input type="hidden" name="ndf_date_annee" id="ndf_date_annee" value="<?= date("Y") ?>">
                                                    <?php } ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <p class="text-left mb20" style="font-weight:bold;">Type :</p>
                                                    <div class="section">
                                                        <select id="nli_type" name="nli_type" class="select2">
                                                            <?php if (!empty($nli)) { ?>
                                                                <option value="1" <?php if ($nli['nli_type'] == 1) { ?> selected <?php } ?>>Classique</option>
                                                                <option value="2" <?php if ($nli['nli_type'] == 2) { ?> selected <?php } ?>>Formation</option>
                                                            <?php } else { ?>
                                                                <option value="1" selected="selected">Classique</option>
                                                                <option value="2">Formation</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <p class="text-left mb20" style="font-weight:bold;">Frais :</p>
                                                    <div class="section">
                                                        <select id="nli_categorie" name="nli_categorie" class="select2" required>
                                                            <option value="">Frais...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row vehicule_section mb20">
                                            <div class="alert alert-danger" id="message_vehicule" style="display:none">
                                                Vous devez renseigner un véhicule
                                            </div>
                                                <div class="col-md-4">
                                                    <p class="text-left mb20" style="font-weight:bold;">Catégorie du véhicule</p>
                                                    <select id="nli_vehicule_categorie" name="nli_vehicule_categorie" class="select2 select2-categorie-vehicule" placeholder="Catégorie du véhicule">
                                                        <option value="0">Sélectionnez une catégorie</option>
                                                        <?php foreach($d_vehicules_categories as $c){ ?>
                                                            <option value="<?= $c['vca_id'] ?>"
                                                                <?php if(!empty($_GET['ligne']) && $nli['nli_vehicule_categorie'] == $c['vca_id']){ ?>
                                                                    selected
                                                                <?php }?>
                                                                ><?= $c['vca_libelle'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <p class="text-left mb20" style="font-weight:bold;">Véhicule</p>
                                                    <select id="nli_vehicule" name="nli_vehicule" class="select2 select2-vehicule">
                                                        <option value="0">Sélectionnez un véhicule</option>
                                                        <?php if(!empty($nli['nli_vehicule'])){
                                                        foreach($d_vehicules as $v){ ?>
                                                            <option value="<?= $v['veh_id'] ?>"
                                                                <?php if($v['veh_id'] == $nli['nli_vehicule']){ ?>selected<?php }?>
                                                                ><?= $v['veh_libelle'] ?></option>
                                                        <?php }
                                                            }?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="panel repas_invitation_interne"
                                            <?php if(empty($ndf_actuelle) || (!empty($nli) && $nli['nli_categorie'] != 31 && $nli['nli_categorie'] != 37)) { ?>
                                                        style="display:none;"
                                                    <?php } ?>>
						                        <div class="panel-heading panel-head-sm pb5 pt5 bg-dark">Sélectionnez les utilisateurs invités</div>
                                                <div class="panel-body" style="background:#EEEEEE;">
                                                    <div class="row">
                                                        <?php
                                                        foreach($utilisateurs as $u) {

                                                            $checked = false;
                                                            if (!empty($ndf_invitations)){
                                                                foreach($ndf_invitations as $nin) {
                                                                    if($nin['nin_utilisateur_invite'] == $u['uti_id']){
                                                                        $checked = true;
                                                                    }
                                                                }
                                                            }

                                                            ?>
                                                        <div class="col-md-3">
                                                            <div class="option-group field mb5">
                                                                <label class="option option-dark">
                                                                <input type="checkbox"  name="nin_utilisateur_invite[]" value="<?= $u['uti_id'] ?>" <?php if($checked) { ?>checked<?php } ?>>
                                                                    <span class="checkbox"></span> <?= $u['uti_prenom'] ?> <?= $u['uti_nom'] ?>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel repas_invitation_client"
                                            <?php if(empty($nli) || (!empty($nli) && $nli['nli_categorie'] != 30 && $nli['nli_categorie'] != 36)) { ?>
                                                        style="display:none;"
                                                    <?php } ?>>
						                        <div class="panel-heading panel-head-sm pb5 pt5 bg-dark">Entrez le client/fournisseur invité</div>
                                                <div class="panel-body" style="background:#EEEEEE;">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="text-left mb20" style="font-weight:bold;">Entrez un nom</p>
                                                            <input type="text" name="nin_autre_client" id="nin_autre_client" class="form-control" placeholder="Entrez un nom" <?php if(!empty($ndf_invitations[0])) { ?> value="<?= $ndf_invitations[0]['nin_autre_client'] ?>" <?php } ?>>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel hotel"
                                            <?php if(empty($nli) || (!empty($nli) && $nli['nli_categorie'] != 38 && $nli['nli_categorie'] != 15 && $nli['nli_categorie'] != 16)) { ?>
                                                        style="display:none;"
                                                    <?php } ?>>
						                        <div class="panel-heading panel-head-sm pb5 pt5 bg-dark">Mes nuits à l'hôtel</div>
                                                <div class="panel-body" style="background:#EEEEEE;">
                                                    <div class="row">
                                                        <div class="col-md-3">

                                                                <p class="text-left mb20" style="font-weight:bold;">Nombre de nuits</p>
                                                                <input type="number" min="0" name="hotel_qte" id="hotel_qte" class="form-control" placeholder="Quantité"
                                                                <?php
                                                                if(!empty($nli['nli_qte'])) {?>
                                                                    value="<?= $nli['nli_qte'] ?>"
                                                                <?php }?>
                                                                >
                                                            </div>
                                                        <div class="col-md-3">
                                                            <p class="text-left mb20" style="font-weight:bold;">Hôtel du</p>
                                                            <span id="ndf_hotel_date_du"><i>Renseignez la quantité</i></span>
                                                        </div>
                                                        <div class="col-md-3">
                                                        <p class="text-left mb20" style="font-weight:bold;">Au</p>
                                                        <span id="ndf_hotel_date_au"><i>Renseignez la quantité</i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel hotel_invitation_interne mt20"
                                            <?php if(empty($nli) || (!empty($nli) && $nli['nli_categorie'] != 16)) { ?>
                                                style="display:none;"
                                            <?php } ?>>
                                                <div class="panel-heading panel-head-sm pb5 pt5 bg-dark">Mes invitations</div>
                                                <div class="panel-body" style="background:#EEEEEE;">
                                                    <?php for ($i=1; $i <= 2; $i++) { ?>
                                                    <div class="mt20 mb20">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <p class="text-left mb20" style="font-weight:bold;">Invitation interne <?= $i ?></p>
                                                                <select id="select-utilisateur-<?= $i-1 ?>" name="nin_utilisateur_invite_2[<?= $i ?>]" class="select2">
                                                                <option value=""></option>
                                                                    <?php
                                                                        foreach($utilisateurs as $u) {
                                                                            if(!empty($ndf_invitations[$i-1]) && $u['uti_id'] == $ndf_invitations[$i-1]['nin_utilisateur_invite']) {?>
                                                                        ?>
                                                                                <option value="<?= $u['uti_id'] ?>" selected><?= $u['uti_prenom'] ?> <?= $u['uti_nom'] ?></option>
                                                                            <?php } else { ?>
                                                                                <option value="<?= $u['uti_id'] ?>"><?= $u['uti_prenom'] ?> <?= $u['uti_nom'] ?></option>
                                                                        <?php }
                                                                        }
                                                                 ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">

                                                                <p class="text-left mb20" style="font-weight:bold;">Nombre de nuits</p>
                                                                <input type="number" min="0" name="nin_qte[<?= $i ?>]" id="nin_qte_<?= $i ?>" class="form-control" placeholder="Quantité"
                                                                <?php
                                                                if(!empty($ndf_invitations[$i-1])) {?>
                                                                    value="<?= $ndf_invitations[$i-1]['nin_qte'] ?>"
                                                                <?php }?>
                                                                >
                                                            </div>
                                                            <div class="col-md-3">
                                                                <p class="text-left mb20" style="font-weight:bold;">Hôtel du</p>
                                                                <?php if(!empty($ndf_invitations[$i-1])) {
                                                                    $timestamp = strtotime($ndf_invitations[$i-1]['nin_date']);
                                                                    $day = date('d', $timestamp);
                                                                 } else {
                                                                    $day = '';
                                                                 } ?>
                                                            <input type="text" min="0" name="nin_date[<?= $i ?>]" id="nin_date_<?= $i ?>" class="date-2" value="<?= $day ?>" placeholder="Date">/
                                                                <?php if (!empty($_GET['id'])) { ?>
                                                                    <?= date("m", strtotime($ndf_actuelle['ndf_quinzaine_deb'])); ?>/<?= date("Y", strtotime($ndf_actuelle['ndf_quinzaine_fin'])); ?>
                                                                    <input type="hidden" name="ndf_date_mois" value="<?= date("m", strtotime($ndf_actuelle['ndf_date'])) ?>">
                                                                    <input type="hidden" name="ndf_date_annee" value="<?= date("Y", strtotime($ndf_actuelle['ndf_date'])) ?>">
                                                                <?php } else { ?>
                                                                    <?= date("m") ?>/<?= date("Y") ?>
                                                                    <input type="hidden" name="ndf_date_mois" value="<?= date("m") ?>">
                                                                    <input type="hidden" name="ndf_date_annee" value="<?= date("Y") ?>">
                                                                <?php } ?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <p class="text-left mb20" style="font-weight:bold;">Au</p>
                                                                <span id="nin_date_au_<?= $i ?>"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <p class="text-left mb20" style="font-weight:bold;">Région :</p>
                                                    <div class="section">
                                                        <label class="option">
                                                            <input type="radio" name="nli_region" id="nli_region1" value="1" <?php if (empty($nli['nli_region']) OR $nli['nli_region'] == 1) { ?> checked <?php } ?>/>
                                                            <span class="radio"></span>Province
                                                        </label>
                                                        <label class="option">
                                                            <input type="radio" name="nli_region" id="nli_region2" value="2" <?php if (!empty($nli['nli_region']) && $nli['nli_region'] == 2) { ?> checked <?php } ?>/>
                                                            <span class="radio"></span>Région parisienne
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <label for="cli_qte" class="text-left mb20" style="font-weight:bold;">Quantité :</label>
                                                    <input type="number" min="0" name="nli_qte" id="nli_qte" class="form-control" required placeholder="Quantité" <?php if (!empty($nli)) { ?> value="<?= $nli['nli_qte'] ?>" <?php } ?>>

                                                </div>
                                                <div class="col-md-2">
                                                    <label for="cli_qte" class="text-left mb20" style="font-weight:bold;">Total T.T.C (€) :</label>
                                                    <input type="text" required name="nli_ttc" id="nli_ttc" class="form-control input-float" placeholder="Total" <?php if (!empty($nli)) { ?> value="<?= $nli['nli_ttc'] ?>" <?php } ?>>

                                                </div>
                                                <?php if(in_array($_SESSION['acces']['acc_profil'], [7,13,12,8,11])){ ?>
                                                <div class="col-md-2">
                                                    <label for="nli_montant_tva" class="text-left mb20" style="font-weight:bold;">Montant TVA (€) :</label>
                                                    <input type="text" name="nli_montant_tva" id="nli_montant_tva" class="form-control input-float" placeholder="Montant TVA" <?php if (!empty($nli)) { ?> value="<?= $nli['nli_montant_tva'] ?>" <?php } ?>>
                                                </div>
                                                <?php } ?>
                                                <?php if(in_array($_SESSION['acces']['acc_profil'], [7,13,12, 10, 11, 12, 9, 14])){ ?>
                                                <div class="col-md-2">

                                                    <label for="nli_montant_tva" class="text-left mb20" style="font-weight:bold;">Dépassement accordé (€) :</label>
                                                    <input type="text" name="nli_depassement_accorde" id="nli_depassement_accorde" class="form-control input-float" placeholder="Dépassement accordé"
                                                    <?php if (!empty($nli['nli_depassement_accorde'])) { ?>
                                                        value="<?= $nli['nli_depassement_accorde'] ?>"
                                                    <?php } ?>

                                                </div>
                                                <?php if (!empty($nli['nli_depassement_valide'])) { ?>
                                                        <div class="alert alert-info mt10 p5">
                                                            Dépassement à valider <br><strong><?= $nli['nli_depassement_valide'] ?> €</strong>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>

                                            </div>
                                            <?php if(in_array($_SESSION['acces']['acc_profil'], [7,13,12,8,11])){ ?>
                                                <div class="row">
                                                    <div class="col-md-4 mt20 mb20">
                                                        <p class="mb20" style="font-weight:bold;">Compte :</p>
                                                        <select name="nli_compte" class="select2">
                                                            <option value="0">Compte non renseigné</option>
                                                            <?php foreach ($comptes as $c) { ?>
                                                                <option value="<?= $c['cpt_id'] ?>"
                                                                    <?php if(!empty($nli['nli_compte']) && $nli['nli_compte'] == $c['cpt_id']) { ?>
                                                                        selected
                                                                    <?php }?>
                                                                ><?= $c['cpt_numero'] ?> (<?= $c['cpt_libelle'] ?>)</option>
                                                            <?php } ?>
                                                        </select>

                                                    </div>

                                                </div>
                                            <?php } ?>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="text-left mb20" style="font-weight:bold;">Commentaire :</p>

                                                    <div class="alert alert-info" id="indemnite_forfaitaire" style="display:none;">
                                                    Merci d'indiquer le lieu géographique du déplacement et le nom du client
                                                    </div>
                                                    <textarea class="gui-textarea" id="nli_commentaire" name="nli_commentaire" placeholder="Commentaire"><?php if (!empty($nli)) { ?><?= $nli['nli_commentaire'] ?><?php } ?></textarea>
                                                </div>
                                            </div>


                                        </div>
                                        <!-- FIN COL-MD-10 -->
                                    </div>
                                    <!-- FIN PANEL BODY -->
                                </div>
                                <!-- FIN PANEL HEADING -->
                            </div>
                            <!-- FIN ADMIN FORM -->
                        </div>
                    </div>
                </section>
            </section>
        </div>
        <!-- End: Main -->
        <footer id="content-footer" class="affix">
            <div class="row">
                <div class="col-xs-3 footer-left">
                    <?php if (isset($_GET['id'])) { ?>
                        <a href="ndf.php?id=<?= $_GET['id'] ?>" class="btn btn-default btn-sm">
                        <?php } else { ?>
                            <a href="ndf.php" class="btn btn-default btn-sm">
                            <?php } ?>
                            <i class='fa fa-long-arrow-left'></i> Retour
                            </a>
                </div>
                <div class="col-xs-6 footer-middle">
                </div>
                <div class="col-xs-3 footer-right">
                    <button type="button" id="sub_form" name="search" class="btn btn-success btn-sm">
                        <i class='fa fa-floppy-o'></i> Enregistrer
                    </button>
                    <button type="submit" id="submit" style="display:none"></button>
                </div>
            </div>
        </footer>
    </form>
    <?php include "includes/footer_script.inc.php"; ?>
    <script src="vendor/plugins/mask/jquery.mask.js"></script>
    <!-- plugin pour les masques formulaires -->
    <script src="vendor/plugins/holder/holder.min.js"></script>
    <!-- pour mettre des images de tests -->
    <!-- Theme Javascript -->
    <script src="vendor/plugins/summernote/summernote.min.js"></script>
    <script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- SCRIPT SELECT2 -->
    <script src="vendor/plugins/select2/js/select2.min.js"></script>
    <script type="text/javascript">
        ////////////// FONCTIONS ///////////////
        // fonction qui actualise le select frais
        function actualise_frais(type) {
            $.ajax({
                type: 'POST',
                url: 'ajax/ajax_get_categories_ndf.php',
                data: 'type=' + type + '&profil=<?= $utilisateur['uti_profil'] ?>',
                dataType: 'json',
                success: function(data) {
                    $("#nli_categorie").find("option:gt(0)").remove();
                    $.each(data, function(index, value) {
                        <?php if (!empty($nli)) { ?>
                            if (<?= $nli['nli_categorie'] ?> == value["nca_id"]) {
                                $('#nli_categorie').append($("<option selected></option>").attr("data-vehicule", value['nca_vehicule']).attr("value", value["nca_id"]).text(value["nca_libelle"]));
                            } else {
                                $('#nli_categorie').append($("<option></option>").attr("data-vehicule", value['nca_vehicule']).attr("value", value["nca_id"]).text(value["nca_libelle"]));
                            }
                        <?php } else { ?>
                            $('#nli_categorie').append($("<option></option>").attr("data-vehicule", value['nca_vehicule']).attr("value", value["nca_id"]).text(value["nca_libelle"]));
                        <?php } ?>
                    });
                    setTimeout(actualise_vehicule(), 500)

                    if($("#nli_categorie").val() == 18) {
                        $("#nli_ttc").prop("readonly", true);
                        if ($("#nli_qte").val()) {
                            get_ik();
                        } else {
                            <?php if(!empty($nli['nli_ttc'])) { ?>
                                $("#nli_ttc").val(0);
                            <?php } ?>
                        }
                    } else if ($("#nli_categorie").val() == 17) {
                        $("#nli_ttc").prop("readonly", true);
                        $("#indemnite_forfaitaire").show();
                        $("#nli_commentaire").attr('required', true);
                        $("#nli_qte").prop("readonly", true);
                        $("#nli_qte").val(1);
                        <?php if(empty($nli['nli_ttc'])) { ?>
                            $("#nli_ttc").val(0);
                        <?php } ?>
                    } else {

                        $("#nli_ttc").prop("readonly", false);
                        <?php if(empty($nli['nli_ttc'])) { ?>
                            $("#nli_ttc").val(0);
                        <?php } ?>
                        $("#indemnite_forfaitaire").hide();
                        $("#nli_commentaire").attr('required', false);
                    }

                    hotel();
                    repas_invitation_interne();
                    repas_invitation_client();
                }
            });
        }

        function addDays(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);
            return result;
        }

        function get_ik() {
            $.ajax({
                type: 'POST',
                url: 'ajax/ajax_get_ndf_ik.php',
                data: 'uti_id=<?= $utilisateur['uti_id'] ?>&id=<?= !empty($ndf_actuelle) ? $ndf_actuelle['ndf_id'] : 0 ?>&nli_qte=' + $("#nli_qte").val(),
                dataType: 'json',
                success: function(data) {
                    if (data.erreur_txt) {
                        afficher_message("Erreur","danger",data.erreur_txt);
                    } else {
                        $("#nli_ttc").val(data);
                    }
                }
            });
        }

        function actualise_vehicule() {

            if($('#nli_categorie').find(':selected').data('vehicule')) {
                $('.vehicule_section').show()
                <?php if(!empty($nli)){ ?>
                $("#nli_vehicule_categorie").val(<?= $nli['nli_vehicule_categorie'] ?>)
                $("#nli_vehicule").val(<?= $nli['nli_vehicule'] ?>)
                <?php } ?>
            }else {
                $('.vehicule_section').hide()
                $("#nli_vehicule").val(0)
                $("#nli_vehicule_categorie").val(0)
            }
        }

        function setHotelDateAu(number) {
            var dateDeb = $("#ndf_date_annee").val() + "-" + $("#ndf_date_mois").val() + "-" + $("#nin_date_" + number).val();
            dateDebFormat = new Date(dateDeb);
            $("#nin_date_au_" + number).text(addDays(dateDebFormat, Number($("#nin_qte_" + number).val())).toLocaleDateString("fr-FR"));
        }

        function setHotelDate() {
            var dateDeb = $("#ndf_date_annee").val() + "-" + $("#ndf_date_mois").val() + "-" + $("#nli_date").val();
            dateDebFormat = new Date(dateDeb);
            $("#ndf_hotel_date_du").text($("#nli_date").val() + "/" + $("#ndf_date_mois").val() + "/" + $("#ndf_date_annee").val());
            $("#ndf_hotel_date_au").text(addDays(dateDeb, Number($("#hotel_qte").val())).toLocaleDateString("fr-FR"));
        }

        function hotel() {
            if ($("#nli_categorie").val() == 15 || $("#nli_categorie").val() == 16 || $("#nli_categorie").val() == 38) {
                $(".hotel").show();
                setHotelDate();
                $("#nli_qte").prop("readonly", true);
                $("#nli_date").change(function () {
                    if ($("#hotel_qte").val()) {
                        setHotelDate();
                    }
                });
                $("#hotel_qte").change(function () {
                    if ($("#nli_categorie").val()) {
                        setHotelDate();
                    }
                });
                $("#hotel_qte").focusout(function () {
                    var sum = 0;
                    sum += Number($("#nin_qte_1").val());
                    sum += Number($("#nin_qte_2").val());
                    $("#nli_qte").val(Number($("#hotel_qte").val()) + sum)
                })
                $("#nin_qte_1").focusout(function () {
                    var sum = 0;
                    sum += Number($("#nin_qte_1").val());
                    sum += Number($("#nin_qte_2").val());
                    if ($("select-utilisateur-1").val()) {
                        $("#nli_qte").val(Number($("#hotel_qte").val()) + sum)
                    }
                })
                $("#nin_qte_2").focusout(function () {
                    var sum = 0;
                    sum += Number($("#nin_qte_1").val());
                    sum += Number($("#nin_qte_2").val());
                    if ($("select-utilisateur-2").val()) {
                        $("#nli_qte").val(Number($("#hotel_qte").val()) + sum)
                    }
                })
            } else {
                $(".hotel").hide();
                $("#nli_qte").prop("readonly", false);
            }

            if ($("#nli_categorie").val() == 16) {
                $(".hotel_invitation_interne").show();
                if ($("#nin_date_1").val() != '') {
                    setHotelDateAu(1);
                }
                if ($("#nin_date_2").val() != '') {
                    setHotelDateAu(2);
                }

                    $("#nin_date_1").change(function() {
                        setHotelDateAu(1);
                    });
                    $("#nin_date_2").change(function() {
                        setHotelDateAu(2);
                    });
                    $("#nin_qte_2").change(function() {
                        setHotelDateAu(1);
                    });
                    $("#nin_qte_2").change(function() {
                        setHotelDateAu(2);
                    });

            } else {
                $(".hotel_invitation_interne").hide();
            }
        }

        function repas_invitation_client() {
            if ($("#nli_categorie").val() == 30 || $("#nli_categorie").val() == 36) {
                $(".repas_invitation_client").show()
                $("#nli_qte").prop("readonly", false);
                $("#nli_qte").val(2);
                $("#nin_autre_client").attr("required", true);
            } else {
                $("#nin_autre_client").attr("required", false);
                $(".repas_invitation_client").hide()
                if ($("#nli_categorie").val() != 31 && $("#nli_categorie").val() != 37 && $("#nli_categorie").val() == 38 && $("#nli_categorie").val() == 15 && $("#nli_categorie").val() == 16) {
                    $("#nli_qte").prop("readonly", false);
                }
            }
        }

        function repas_invitation_interne() {
            if ($("#nli_categorie").val() == 31 || $("#nli_categorie").val() == 37) {
                $(".repas_invitation_interne").show()
                $("#nli_qte").prop("readonly", true);
                $("#nli_qte").val($('input[name="nin_utilisateur_invite[]"]:checked').length + 1);
                $('input[name="nin_utilisateur_invite[]"]').change(function() {
                    $("#nli_qte").val($('input[name="nin_utilisateur_invite[]"]:checked').length + 1);
                });
            } else {
                $(".repas_invitation_interne").hide()
                if ($("#nli_categorie").val() != 30 && $("#nli_categorie").val() != 36 && $("#nli_categorie").val() == 38 && $("#nli_categorie").val() == 15 && $("#nli_categorie").val() == 16) {
                    $("#nli_qte").prop("readonly", false);
                }
            }
        }
        // fin fonction qui actualise le select frais
        function submit_form() {
            if ($("#nli_categorie").val() == 17) {
                if($("#nli_commentaire").val() == ""){
                    return;
                }
            }

            if($('#nli_categorie').find(':selected').data('vehicule')) {
                if($("#nli_vehicule").val() == 0){
                    $("#message_vehicule").show();
                } else {
                    $("#submit").trigger('click');
                }
            } else {
                $("#submit").trigger('click');
            }
        }
        ////////////// FIN FONCTIONS ///////////////
        //////// EVENEMENTS UTILISATEURS //////////
        jQuery(document).ready(function() {
            $("#sub_form").click(function(){
                submit_form()
            });

            $("#select-utilisateur-0").change(function () {
                $("#nin_qte_1").val(1);
            });
            $("#select-utilisateur-1").change(function () {
                $("#nin_qte_2").val(1);
            });
            $("#nin_qte_1").change(function () {
                if($("#nli_categorie").val() == 16 && Number($("#nin_qte_1").val()) > 0) {
                    if (Number($("#nin_qte_2").val()) > 0) {
                        $("#nli_qte").val(Number($("#nin_qte_1").val()) + Number($("#nin_qte_2").val()))
                    } else {
                        $("#nli_qte").val(Number($("#nin_qte_1").val()))
                    }

                    hotel();
                }
            })
            $("#nin_qte_2").change(function () {
                if($("#nli_categorie").val() == 16 && Number($("#nin_qte_2").val()) > 0) {
                    if (Number($("#nin_qte_2").val()) > 0) {
                        $("#nli_qte").val(Number($("#nin_qte_2").val()) + Number($("#nin_qte_1").val()))
                    } else {
                        $("#nli_qte").val(Number($("#nin_qte_2").val()))
                    }

                    hotel();
                }
            })
            $("#nli_depassement_accorde").change(function(){
                actualise_reste_a_charge()
            })
            // ttc
            $('.number').keypress(function(event) {
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });
            // fin ttc
            // actualise la liste des frais en fonction du type
            actualise_frais($("#nli_type").val());
            actualise_vehicule();

            // fin actualise la liste des frais en fonction du type
            $("#nli_date").on('change', function() {
                if ($(this).val() < <?= $day1 ?> || $(this).val() > <?= $day2 ?>) {
                    $(this).val("<?= date("d"); ?>");
                    alert("Merci d'entrer une date entre les deux dates de la plage.")
                }
            });

            $("#nli_type").change(function() {
                actualise_frais($(this).val());
                actualise_vehicule();
            });

            $("#nli_qte").keyup(function() {
                if($("#nli_categorie").val() == 18) {
                    $("#nli_ttc").prop("readonly", true);
                    if ($("#nli_qte").val()) {
                        get_ik();
                    } else {
                        $("#nli_ttc").val("");
                    }
                }
            });

            $("#nli_categorie").change(function() {

                if($(this).val() == 18) {
                    $("#nli_ttc").prop("readonly", true);
                    if ($("#nli_qte").val()) {
                        get_ik();
                    } else {
                        $("#nli_ttc").val(0);
                    }
                } else if ($(this).val() == 17) {
                    $("#nli_ttc").prop("readonly", true);
                    $("#indemnite_forfaitaire").show();
                    $("#nli_commentaire").attr('required', true);
                    $("#nli_qte").prop("readonly", true);
                    $("#nli_qte").val(1);
                    $("#nli_ttc").val(0);
                } else if ($(this).val() == 15 || $(this).val() == 16  || $(this).val() == 38) {
                    $("#hotel_qte").val(1);
                    $("#nli_qte").val(1);
                } else {
                    $("#nli_ttc").prop("readonly", false);
                    $("#nli_ttc").val("");
                    $("#indemnite_forfaitaire").hide();
                    $("#nli_commentaire").attr('required', false);
                }

                hotel();
                repas_invitation_interne();
                repas_invitation_client();
                actualise_vehicule();
            });

        });
        //////// FIN ÉVENEMENTS UTILISATEURS //////
    </script>
</body>
</html>