<?php

 include "includes/controle_acces.inc.php";

 include_once("includes/connexion_soc.php");
 include_once("includes/connexion_fct.php");
 include_once("includes/connexion.php");

 include_once("modeles/mod_parametre.php");
 // EDITION D'UN DIPLOME SSIAP

$erreur_txt="";

$action_id=0;
if(isset($_GET["action"])){
    if(!empty($_GET["action"])){
        $action_id=intval($_GET["action"]);
    }
}

$stagiaire_id=0;
if(isset($_GET["stagiaire"])){
    if(!empty($_GET["stagiaire"])){
        $stagiaire_id=intval($_GET["stagiaire"]);
    }
}

$diplome=0;
if(isset($_GET["diplome"])){
    if(!empty($_GET["diplome"])){
        $diplome=intval($_GET["diplome"]);
    }
}
$qualification=0;
if(isset($_GET["qualification"])){
    if(!empty($_GET["qualification"])){
        $qualification=intval($_GET["qualification"]);
    }
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=intval($_SESSION['acces']["acc_societe"]);

}
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=intval($_SESSION['acces']["acc_agence"]);
}
if($acc_societe!=$conn_soc_id){
	$acc_societe=$conn_soc_id;
}

	// SUR LA SOCIETE
    $sql="SELECT soc_agre_ssiap FROM Societes WHERE soc_id=:societe;";
	$req=$Conn->prepare($sql);
    $req->bindParam("societe",$acc_societe);
	$req->execute();
    $d_societe=$req->fetch();
    if(empty($d_societe)){
        $erreur_txt="Les paramètres actuels ne vous permettent pas d'enregistrer des diplômes SSIAP.";
    }else{
		if(empty($d_societe["soc_agre_ssiap"])){
			$erreur_txt="Les paramètres actuels ne vous permettent pas d'enregistrer des diplômes SSIAP.";
		}
	}

	if(empty($erreur_txt)){

		 // SUR L'INSCRIPTION STAGIAIRE

		// FG 16/04/2020
		// ajout du critère 'AND pda_categorie=2' pour recupérer la première date d'évaluation -> en l'absence de ce critère c'est la premier jour de formation qui sort en date_jury
		$sql="SELECT DISTINCT ast_qualification,DATE_FORMAT(pda_date,'%d/%m/%Y') AS date_jury,acl_pro_reference
		FROM Actions_Stagiaires,Actions_Stagiaires_Sessions,Actions_Sessions,Plannings_Dates,Actions_Clients
		WHERE ast_stagiaire=ass_stagiaire AND ast_action=ass_action AND ass_session=ase_id AND ase_date=pda_id AND ast_action_client=acl_id
		AND ast_action=:action AND ast_stagiaire=:stagiaire AND pda_categorie=2;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("action",$action_id);
		$req->bindParam("stagiaire",$stagiaire_id);
		$req->execute();
		$d_action_client=$req->fetch();
		if(empty($d_action_client)){
			$erreur_txt="Impossible de charger les données relatives au code produit.";
		}else{

			if(empty($qualification)){
				$qualification=$d_action_client["ast_qualification"];
			}

			if($d_action_client["ast_qualification"]==1){
				$dss_form_titre="DIPLÔME D'AGENT DES SERVICES DE SÉCURITÉ INCENDIE ET D'ASSISTANCE A PERSONNES";
				$dss_form_libelle="S.S.I.A.P 1";
			}elseif($d_action_client["ast_qualification"]==2){
				$dss_form_titre="DIPLÔME DE CHEF D'ÉQUIPE DES SERVICES DE SÉCURITÉ INCENDIE ET D'ASSISTANCE A PERSONNES";
				$dss_form_libelle="S.S.I.A.P 2";
			}elseif($d_action_client["ast_qualification"]==3){
				$dss_form_titre="DIPLÔME DE CHEF DES SERVICES DE SÉCURITÉ INCENDIE ET D'ASSISTANCE A PERSONNES";
				$dss_form_libelle="S.S.I.A.P 3";
			}else{
				$erreur_txt="Impossible de charger les données relatives au code produit.";
			}
		}

	}

	if(empty($erreur_txt)){

		if($diplome > 0){

			// DOPLOME EXISTANT

			$sql="SELECT * FROM diplomes_ssiap WHERE dss_id=:diplome;";
			$req=$Conn->prepare($sql);
			$req->bindParam("diplome",$diplome);
			$req->execute();
			$dss=$req->fetch();
			if(!empty($dss)){

				if($d_action_client["ast_qualification"]!=$dss['dss_qualification']){
					$erreur_txt="Le code produit selectionné n'est pas compatible avec le diplôme déjà enregistré.";
				}else{

					// les données deja enregistrées surclasse les données calculés en fonction du diplomes
					$dss_form_titre=$dss['dss_form_titre'];
					$dss_form_libelle=$dss['dss_form_libelle'];
					$dss_form_texte_1=$dss['dss_form_texte_1'];
					$dss_form_texte_2=$dss['dss_form_texte_2'];
					$dss_form_texte_3=$dss['dss_form_texte_3'];

					$dss_type=$dss['dss_type'];

					$d_stagiaire["sta_nom"]=$dss['dss_sta_nom'];
					$d_stagiaire["sta_prenom"]=$dss['dss_sta_prenom'];
					$d_stagiaire["sta_naissance"]=$dss['dss_sta_naissance'];
					$d_stagiaire["sta_cp_naiss"]=$dss['dss_sta_cp'];
					$d_stagiaire["sta_ville_naiss"]=$dss['dss_sta_ville'];

				}

			}else{
				$erreur_txt="Impossible de charger les données relatives au diplôme.";
			}

		}else{

			$dss_type=1;

			// DONNEE DU STAGIAIRE

			$sql="SELECT * FROM Stagiaires WHERE sta_id=" . $stagiaire_id . ";";
			$req=$Conn->query($sql);
			$d_stagiaire=$req->fetch();
			if(empty($d_stagiaire)){
				$erreur_txt="Impossible de charger les données du stagiaire";
			}

			// HISTORIQUE DIPLOME

			if(empty($erreur_txt)){

				// recherche d'un diplome initial (autre que RAN).

				$sql="SELECT * FROM diplomes_ssiap WHERE dss_stagiaire=:stagiaire AND dss_qualification=:qualification AND NOT dss_type=3;";
				$req=$Conn->prepare($sql);
				$req->bindParam("stagiaire",$stagiaire_id);
				$req->bindParam("qualification",$d_action_client["ast_qualification"]);
				$req->execute();
				$d_diplome_ini=$req->fetch();
				if(!empty($d_diplome_ini)){
					$dss_type=3;
				}else{

					 if(substr($d_action_client["acl_pro_reference"],6,2) == "MC"){
						$dss_type=2;
					}else{
						$dss_type=1;
					}

				}
			}
		}
	}

	$ini_form_texte_1="";
	$ini_form_texte_2="";

	$mc_form_texte_1="";
	$mc_form_texte_2="";

	if(empty($erreur_txt)){

		$ini_form_texte_1="Vu le procès verbal du jury d'examen en date du " . $d_action_client['date_jury'] . " déclarant que";
		$ini_form_texte_2="a subi avec succès les épreuves exigées pour l'obtention du \u000A" . $dss_form_titre;

		$mc_form_texte_1="Vu sa situation d'homme du rang des sapeurs pompiers volontaires et l'attestation de suivi du module complémentaire " . $dss_form_libelle . " du " . $d_action_client['date_jury'];
		$mc_form_texte_2="est détenteur par équivalence du \u000A" . $dss_form_titre;

		if($dss_type==1){
			$dss_form_texte_1=$ini_form_texte_1;
			$dss_form_texte_2=str_replace("\u000A",chr(10),$ini_form_texte_2);
			$dss_form_texte_3="";
		}elseif($dss_type==2){
			$dss_form_texte_1=$mc_form_texte_1;
			$dss_form_texte_2=str_replace("\u000A",chr(10),$mc_form_texte_2);
			$dss_form_texte_3="";
		}else{
			$erreur_txt="L'édition des remises à niveau n'est pas disponible";
		}

		// FORMAT DU CP
		if(!empty($d_stagiaire["sta_cp_naiss"])){
			$d_stagiaire["sta_cp_naiss"]=str_replace(" ","",$d_stagiaire["sta_cp_naiss"]);
			if(strlen($d_stagiaire["sta_cp_naiss"])==6){
				$d_stagiaire["sta_cp_naiss"]=substr($d_stagiaire["sta_cp_naiss"],0,3);
			}else{
				$d_stagiaire["sta_cp_naiss"]=substr($d_stagiaire["sta_cp_naiss"],0,2);
			}
		}

	}



 ?>
 <!DOCTYPE html>
 <html>
     <head>
         <meta charset="utf-8">
         <title>SI2P - Orion - Diplôme SSIAP</title>
         <meta name="keywords" content=""/>
         <meta name="description" content="">
         <meta name="author" content="">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <!-- CSS THEME -->
         <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
         <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />

         <!-- CSS PLUGINS -->
         <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
         <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

         <!-- CSS Si2P -->
         <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

         <!-- Favicon -->
         <link rel="shortcut icon" href="assets/img/favicon.png">

         <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
         <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
         <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
         <![endif]-->
     </head>
     <body class="sb-top sb-top-sm">
         <form method="post" action="dip_ssiap_enr.php" id="form" >
             <div id="main">
     <?php		include "includes/header_def.inc.php"; ?>
                 <section id="content_wrapper">
                     <section id="content" class="animated" >
                 <?php	if(empty($erreur_txt)){	?>
                             <div>
								<input type="hidden" name="societ" value="<?=$conn_soc_id?>">
                                <input type="hidden" name="dss_action" value="<?=$action_id?>">
                                <input type="hidden" name="dss_stagiaire" value="<?=$stagiaire_id?>">
                                <input type="hidden" name="dss_qualification" value="<?=$qualification?>">
                                <input type="hidden" name="dss_form_date" value="<?=$d_action_client['date_jury']?>">
                                <?php if(!empty($diplome)){ ?>
                                    <input type="hidden" name="diplome" value="<?=$diplome?>">
                                <?php } ?>
                             </div>

                             <div class="admin-form theme-primary ">
                                 <div class="panel heading-border panel-primary">
                                     <div class="panel-body bg-light">

                                         <div class="content-header">
                                             <h2>
                                                Diplôme <b class="text-primary"><?=$dss_form_libelle?></b>
                                                <?php if($diplome>0){ ?>
                                                    N° <?= $dss['dss_numero'] ?>
                                                <?php } ?>
                                             </h2>
                                         </div>

										 <div class="row">
											<div class="col-md-12">
												<div class="section-divider">
													<span>Type de diplôme</span>
												</div>
											</div>
										</div>
										<div class="row text-center" >
											<div class="col-md-4" >
												<div class="radio-custom mb5">
													<input id="ini" name="dss_type" class="diplome-type" type="radio" value="1" <?php if($dss_type==1) echo("checked"); ?> />
													<label for="ini">INI</label>
												</div>
											</div>
											<div class="col-md-4" >
												<div class="radio-custom mb5">
													<input id="mc" name="dss_type" class="diplome-type"type="radio" value="2" <?php if($dss_type==2) echo("checked"); ?> />
													<label for="mc">MC</label>
												</div>
											</div>
											<div class="col-md-4" >
												<div class="radio-custom mb5">
													<input id="ran" name="dss_type" class="diplome-type" type="radio" value="3" disabled >
													<label for="ran">RAN</label>
												</div>
											</div>
										</div>
										<div class="row mt15">
											 <div class="col-md-12" >
												 <label for="sta_nom" >Titre du diplôme :</label>
												 <input type="text" name="dss_form_titre" class="gui-input" id="dss_form_titre" placeholder="Titre" required value="<?=$dss_form_titre?>" />
											 </div>
										</div>
										<div class="row mt15">
											 <div class="col-md-12" >
												 <label for="sta_prenom" >Libellé du diplôme :</label>
												 <input type="text" name="dss_form_libelle" class="gui-input" id="dss_form_libelle" placeholder="Libellé" required value="<?=$dss_form_libelle?>" />
											 </div>
										</div>
										<div class="row mt15">
											 <div class="col-md-12 mt10 mb10">
												<textarea name="dss_form_texte_1" id="dss_form_texte_1" class="gui-textarea" rows="2" cols="105" ><?=$dss_form_texte_1?></textarea>
											 </div>
										</div>
										<div class="row mt15" >
                                             <div class="col-md-4" >
                                                 <label for="sta_nom" >Nom :</label>
                                                 <input type="text" name="dss_sta_nom" class="gui-input nom" id="sta_nom" placeholder="Nom" required value="<?=$d_stagiaire["sta_nom"]?>" />
                                             </div>
                                             <div class="col-md-4" >
                                                 <label for="sta_prenom" >Prénom :</label>
                                                 <input type="text" name="dss_sta_prenom" class="gui-input prenom" id="sta_prenom" placeholder="Prénom" required value="<?=$d_stagiaire["sta_prenom"]?>" />
                                             </div>
                                             <div class="col-md-4" >
                                                 <label for="sta_naissance" >Date de naissance :</label>
                                                 <input type="text" name="dss_sta_naissance" class="gui-input datepicker date" id="sta_naissance" placeholder="Date de naissance" required value="<?=convert_date_txt($d_stagiaire["sta_naissance"])?>" />
                                             </div>
                                         </div>
                                         <div class="row mt15" >
                                            <div class="col-md-4" >
                                                 <label for="dss_sta_ville" >Lieu de naissance :</label>
                                                 <input type="text" name="dss_sta_ville" class="gui-input" id="dss_sta_ville" placeholder="Lieu de naissance" required value="<?=$d_stagiaire["sta_ville_naiss"]?>" />
                                            </div>
                                            <div class="col-md-4" >
                                                 <label for="dss_sta_cp" >Code postal :</label>
                                                 <input type="text" name="dss_sta_cp" class="gui-input" id="dss_sta_cp" placeholder="Code postal" required value="<?=$d_stagiaire["sta_cp_naiss"]?>" />
                                            </div>
                                         </div>
                                         <div class="row" >
                                            <div class="col-md-12" >
                                                <div class="mt10">

                                                    <textarea class="gui-textarea" id="dss_form_texte_2" name="dss_form_texte_2" rows="2" cols="105" ><?=$dss_form_texte_2?></textarea>

                                                </div>
                                            </div>

                                         </div>

                                     </div>
                                 </div>
                             </div>
                 <?php	}else{ ?>
                             <p class="alert alert-danger" >
                                 <?=$erreur_txt?>
                             </p>
                 <?php	} ?>
                     </section>
                 </section>
             </div>
             <footer id="content-footer" class="affix">
                 <div class="row">
                     <div class="col-xs-3 footer-left">
                 <?php	if(!empty($_SESSION["retourSsiap"])){ ?>
                            <a href="<?=$_SESSION["retourSsiap"]?>" class="btn btn-default btn-sm" >
                                 Retour
                            </a>
                 <?php	}else{ ?>
                             <a href="dip_ssiap_voir.php?diplome=" class="btn btn-default btn-sm" >
                                 Retour
                             </a>
                 <?php	} ?>
                     </div>
                     <div class="col-xs-6 footer-middle text-center"></div>
                     <div class="col-xs-3 footer-right">
             <?php		if(empty($erreur_txt)){ ?>
                             <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Enregistrer le diplôme" id="sub_form" >
                                 <i class="fa fa-save" ></i> Enregistrer
                             </button>
             <?php		} ?>
                     </div>
                 </div>
             </footer>
         </form>


 <?php	include "includes/footer_script.inc.php"; ?>

         <script src="/vendor/plugins/select2/js/select2.min.js"></script>
         <script src="/vendor/plugins/mask/jquery.mask.js"></script>
         <script src="/assets/js/custom.js"></script>
         <script type="text/javascript">

			var $ini_form_texte_1="<?=$ini_form_texte_1?>";
			var $ini_form_texte_2="<?=$ini_form_texte_2?>";

			var $mc_form_texte_1="<?=$mc_form_texte_1?>";
			var $mc_form_texte_2="<?=$mc_form_texte_2?>";

			jQuery(document).ready(function(){

				$("input[name$='dss_type']").change(function(){
					if($(this).val()==1){
						$("#dss_form_texte_1").val($ini_form_texte_1);
						$("#dss_form_texte_2").val($ini_form_texte_2);
					}else if($(this).val()==2){
						$("#dss_form_texte_1").val($mc_form_texte_1);
						$("#dss_form_texte_2").val($mc_form_texte_2);
					}else{
						$("#dss_form_texte_1").val("");
						$("#dss_form_texte_2").val("");
					}

				});

			});

         </script>
     </body>
 </html>
