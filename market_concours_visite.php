<?php
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'includes/connexion_fct.php';

// LISTE DES VISITES PRISE EN COMPTE POUR UN UTILISATEUR
$erreur_txt="";

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];    
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
    $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];  
}

$utilisateur=0;
if(!empty($_GET["utilisateur"])){
	$utilisateur=intval($_GET["utilisateur"]);
}
$societe=0;
if(!empty($_GET["societe"])){
	$societe=intval($_GET["societe"]);
}
$agence=0;
if(!empty($_GET["agence"])){
	$agence=intval($_GET["agence"]);
}

if($utilisateur==0 AND $societe==0){
	$erreur_txt="Vous ne pouvez pas accéder à cette page!";
}

if(empty($erreur_txt)){
	
	$_SESSION['retourClient']="market_concours_visite.php?utilisateur=" . $utilisateur . "&societe=" . $societe . "&agence=". $agence;
	
	if(!empty($utilisateur)){
		
		$sql="SELECT uti_nom,uti_prenom,uti_societe FROM Utilisateurs WHERE uti_id=" . $utilisateur . ";";
		$req=$Conn->query($sql);
		$d_utilisateur=$req->fetch();
		if(empty($d_utilisateur)){
			$erreur_txt="Vous ne pouvez pas accéder à cette page!";
			echo($erreur_txt);
			die();
		}else{
			$societe=$d_utilisateur["uti_societe"];
		}
		
	}else{

		$sql="SELECT soc_nom,age_nom FROM Societes LEFT JOIN Agences ON (Societes.soc_id=Agences.age_societe AND age_id=" . $agence . ")
		WHERE soc_id=" . $societe . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();
		if(empty($d_societe)){
			$erreur_txt="Vous ne pouvez pas accéder à cette page!";
			echo($erreur_txt);
			die();
		}
		
		if(empty($erreur_txt)){
			
			// ON RECUPERE LA LISTE DES UTILISATEURS POUR POUVOIR RECUPERER UNIQUEMENT LES CORRESPONDANCES DE L'AGENCE
			
			$d_utilisateurs=array();
			
			$sql="SELECT uti_id,uti_nom,uti_prenom FROM Utilisateurs WHERE NOT uti_archive";
			if($agence==9){
				$sql.=" AND (uti_agence=9 OR uti_id=22)";
			}elseif($agence==3){	
				$sql.=" AND (uti_agence=3 OR uti_id=432)";
			}elseif($agence>0){
				$sql.=" AND NOT uti_id=22 AND NOT uti_id=432 AND uti_agence=" . $agence;
			}else{
				$sql.=" AND NOT uti_id=22 AND NOT uti_id=432 AND uti_societe=" . $societe;
			}
			$sql.=" ORDER BY uti_id;";
			
			$req=$Conn->query($sql);
			$d_r_uti=$req->fetchAll();
			/*echo("<pre>");
				print_r($d_r_uti);
			echo("</pre>");
			die();*/
			if(!empty($d_r_uti)){
				foreach($d_r_uti as $r){
					$d_utilisateurs[$r["uti_id"]]=$r["uti_prenom"] . " " . $r["uti_nom"];
				}
				$tab_uti=array_column ($d_r_uti,"uti_id");
				$liste_uti=implode($tab_uti,",");

				
			}else{
				$erreur_txt="Vous ne pouvez pas accéder à cette page!";
				echo($erreur_txt);
				die();
			}
			
			
		}
		
		// LES Correspondances suspects Correspondances n'ont pas de notion d
		
	}
}
if(empty($erreur_txt)){	

	$nb_com=0;
	if(empty($utilisateur)){ 
		switch ($societe) {
			case 2:
				$nb_com=3;
				break;
			case 3:
				if($agence==3){
					$nb_com=3;
				}else{
					$nb_com=3;
				}
				break;
			case 4:
				$nb_com=1;
				break;
			case 5:
				$nb_com=3;
				break;
			case 7:
				$nb_com=2;
				break;
			case 8:
				$nb_com=1;
				break;
		}
	}
	
	// LES VISITES SUSPECTS
	
	$ConnFct=connexion_fct($societe);
		
	$sql_sus="SELECT sus_id,sus_code,sus_nom,DATE_FORMAT(sco_date,'%d/%m/%Y') AS sco_date_fr,sco_utilisateur FROM Suspects_Correspondances INNER JOIN ( 
		SELECT sco_suspect, MIN(sco_date) AS min_date FROM Suspects_Correspondances WHERE sco_type=3 GROUP BY sco_suspect ) AS groupe 
	ON Suspects_Correspondances.sco_suspect = groupe.sco_suspect AND Suspects_Correspondances.sco_date = groupe.min_date
	INNER JOIN Suspects ON (Suspects_Correspondances.sco_suspect=Suspects.sus_id) WHERE sco_date>='2019-07-01' AND sco_date<='2019-12-31'";
	if(!empty($utilisateur)){
		$sql_sus.=" AND sco_utilisateur=" . $utilisateur;
	}elseif(isset($liste_uti)){
		$sql_sus.=" AND sco_utilisateur IN (" . $liste_uti . ")";
		
	}
	
	$sql_sus.=" ORDER BY sco_date DESC";
	$req_sus=$ConnFct->query($sql_sus);
	$d_visite_suspect=$req_sus->fetchAll();
	
	// VISITE CLIENT
	$sql_cor="SELECT Clients.cli_id AS client_id,Clients.cli_nom,Clients.cli_code
	,DATE_FORMAT(cor_date,'%d/%m/%Y') AS cor_date_fr,cor_devis,cor_devis_num,cor_utilisateur
	,Holdings.cli_id
	FROM Clients INNER JOIN Correspondances ON (Clients.cli_id=Correspondances.cor_client)";
	$sql_cor.="INNER JOIN ( 
	SELECT cor_client, MIN(cor_date) AS min_date FROM Correspondances WHERE cor_type=3 GROUP BY cor_client ) AS groupe 
	ON Correspondances.cor_client = groupe.cor_client AND Correspondances.cor_date = groupe.min_date AND Correspondances.cor_type=3";
	$sql_cor.=" LEFT JOIN Clients AS Holdings ON (Clients.cli_filiale_de=Holdings.cli_id)";
	$sql_cor.=" WHERE Correspondances.cor_date>='2019-07-01' AND Correspondances.cor_date<='2019-12-31' AND NOT Clients.cli_categorie=2";
	if(!empty($utilisateur)){
		$sql_cor.=" AND Correspondances.cor_utilisateur=" . $utilisateur;
	}elseif(isset($liste_uti)){
		$sql_cor.=" AND Correspondances.cor_utilisateur IN (" . $liste_uti . ")";
		
	}
	$sql_cor.=" AND (Clients.cli_first_facture=0 OR ISNULL(Clients.cli_first_facture) OR Clients.cli_first_facture_date>='2019-07-01')";
	$sql_cor.=" AND (ISNULL(Holdings.cli_id) OR Holdings.cli_first_facture=0 OR ISNULL(Clients.cli_first_facture) OR Clients.cli_first_facture_date>='2019-07-01')"; 
	$sql_cor.=" AND NOT Clients.cli_hors_concours
	ORDER BY cor_date DESC"; 
	$req_cor=$Conn->query($sql_cor);
	$d_visite_client=$req_cor->fetchAll();
	

}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Marketing client</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="market_suspect_liste.php" id="formulaire" >
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
					
						<div class="content-header">
					<?php	if(!empty($utilisateur)){ ?>
								<h2>Visites de <b class="text-primary"><?=$d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"]?></b></h2>
					<?php	}elseif(!empty($agence)){ ?>		
								<h2>Visites de <b class="text-primary"><?=$d_societe["soc_nom"] . " " . $d_societe["age_nom"]?></b></h2>
					<?php	}else{ ?>		
								<h2>Visites de <b class="text-primary"><?=$d_societe["soc_nom"]?></b></h2>
					<?php	} ?>
						</div>

						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="panel">
									<div class="panel-heading panel-head-sm">Visites suspects</div>
									<div class="panel-body" >
							<?php		if(!empty($d_visite_suspect)){ ?>									
											<table class="table" >
												<thead>
													<tr>
														<th>Code</th>
														<th>Nom</th>
												<?php	$nb_cols=3;
														if(empty($utilisateur)){ 
															$nb_cols++; ?>
															<th>Utilisateur</th>														
												<?php	} ?>
														<th>Date de la visite</th>
												<?php	if(empty($utilisateur)){ 
															$nb_cols++; ?>
															<th>Coeff commercial</th>														
												<?php	}  ?>
														<th class="text-center" >Points "Visites"</th>	
														<th colspan="2" style="width:16%;" >&nbsp;</th>
													</tr>
												</thead>
												<tbody>
									<?php			$total_suspect=0;
													foreach($d_visite_suspect as $vs){  
														$pt=0.5;
														if(empty($utilisateur)){
															$pt=$pt/$nb_com;
														}
														$total_suspect=$total_suspect+$pt; ?>
														<tr>
															<td><?=$vs["sus_code"]?></td>
															<td><?=$vs["sus_nom"]?></td>
													<?php	if(empty($utilisateur)){ ?>
																<td>
																	<?=$d_utilisateurs[$vs["sco_utilisateur"]]?>
																</td>
													<?php	} ?>	
															<td><?=$vs["sco_date_fr"]?></td>
													<?php	if(empty($utilisateur)){ ?>
																<td><?=$nb_com?></td>
													
													<?php	} ?>
															<td class="text-right" ><?=number_format($pt,3,","," ")?></td>
															<td colspan="2" >&nbsp;</td>															
														</tr>
									<?php			} ?>
												</tbody>
												<tfoot>
													<tr>
														<th class="text-right" colspan="<?=$nb_cols?>" >Total :</th>
														<th class="text-right" ><?=number_format($total_suspect,2,","," ")?> pt</th>
														<td colspan="2" >&nbsp;</td>			
													</tr>
												</tfoot>
											</table>
								<?php	}else{ ?>
											<p class="alert alert-warning" >Aucune visite suspect.</p>
								<?php	} ?>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="panel">
									<div class="panel-heading panel-head-sm">Visites client</div>
									<div class="panel-body" >
							<?php		if(!empty($d_visite_client)){ ?>					
											<table class="table" >
												<thead>
													<tr>
														<th>Code</th>
														<th>Nom</th>
												<?php	$nb_cols=3;
														if(empty($utilisateur)){ 
															$nb_cols++; ?>
															<th>Utilisateur</th>
												<?php	} ?>
														<th>Date de la visite</th>
												<?php	if(empty($utilisateur)){ 
															$nb_cols++; ?>
															<th>Coeff commercial</th>														
												<?php	}  ?>
														<th class="text-center" >Points "Visites"</th>	
														<th style="width:8%;" >Devis</th>	
														<th style="width:8%;" class="text-center" >Points "Devis"</th>														
													</tr>
												</thead>
												<tbody>
									<?php			$total_client=0;
													$total_devis=0;
													foreach($d_visite_client as $vc){
														
														$pt=0.5;
														if(empty($utilisateur)){
															$pt=$pt/$nb_com;
														}
														
														$total_client=$total_client+$pt;
														?>
														<tr>

															<td><a href="client_voir.php?client=<?=$vc["client_id"]?>&tab5" ><?=$vc["cli_code"]?></a></td>
															<td><?=$vc["cli_nom"]?></td>
													<?php	if(empty($utilisateur)){ ?>
																<td>
																	<?=$d_utilisateurs[$vc["cor_utilisateur"]]?>
																</td>
													<?php	} ?>		
															<td><?=$vc["cor_date_fr"]?></td>
													<?php	if(empty($utilisateur)){ ?>
																<td><?=$nb_com?></td>													
													<?php	} ?>
															<td class="text-right" ><?=number_format($pt,3,","," ")?> pt</td>	
													<?php	if(!empty($vc["cor_devis"])){ 
													
																$pt_devis=0.5;
																if(empty($utilisateur)){
																	$pt_devis=$pt_devis/$nb_com;
																}
													
																$total_devis=$total_devis+$pt_devis; ?>
																<td><?=$vc["cor_devis_num"]?></td>
																<td class="text-right" ><?=number_format($pt_devis,3,","," ")?> pt</td>
													<?php	}else{ ?>
																<td colspan="2" >&nbsp;</td>
													<?php	} ?>
														</tr>
									<?php			} ?>
												</tbody>
												<tfoot>
													<tr>
														<th colspan="<?=$nb_cols?>" class="text-right" >Total :</th>
														<th class="text-right" ><?=number_format($total_client,2,","," ")?> pt</th>
														<th>&nbsp;</th>
														<th class="text-right" ><?=number_format($total_devis,2,","," ")?> pt</th>
													</tr>
												</tfoot>
											</table>
								<?php	}else{ ?>
											<p class="alert alert-warning" >Aucune visite client.</p>
								<?php	} ?>
									</div>
								</div>
							</div>
						</div>
				
						
					</section>
				</section>
			</div>		
			<!-- End: Main -->
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
			<?php		if(!empty($utilisateur)){ ?>
							<a href="market_concours.php" class="btn btn-default btn-sm">
								<span class="fa fa-arrow-left"></span>
								<span class="hidden-xs">Retour</span>
							</a>
			<?php		}else{ ?>
							<a href="market_concours.php?agence=1" class="btn btn-default btn-sm">
								<span class="fa fa-arrow-left"></span>
								<span class="hidden-xs">Retour</span>
							</a>
			<?php		} ?>			
					</div>
					<div class="col-xs-6 footer-middle text-center"></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
			
			});
			
			
		</script>
	</body>
</html>
