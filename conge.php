<?php
////////////////// MENU ACTIF HEADER ///////////////////
$menu_actif = 3;
////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
////////////////// TRAITEMENTS SERVEUR ///////////////////
/////////////////PERSONNE CONNECTEE ///////////////////
$acc_agence = 0;
if (isset($_SESSION['acces']["acc_agence"])) {
    $acc_agence = $_SESSION['acces']["acc_agence"];
}
$acc_societe = 0;
if (isset($_SESSION['acces']["acc_societe"])) {
    $acc_societe = $_SESSION['acces']["acc_societe"];
}
// SELECT LES UTILISATEURS ET CATEGORIES

if ($_SESSION['acces']['acc_profil'] == 7 or $_SESSION['acces']['acc_profil'] == 12 or $_SESSION['acces']['acc_profil'] == 10 or $_SESSION['acces']['acc_profil'] == 15  or $_SESSION['acces']['acc_profil'] == 13) {
    $req = $Conn->prepare("SELECT uti_id,uti_nom, uti_prenom, uti_matricule FROM utilisateurs WHERE uti_archive = 0   AND uti_societe = " . $acc_societe . " AND uti_agence = " . $acc_agence);
} else {
    if (!empty($acc_agence)) {
        $req = $Conn->prepare("SELECT uti_id,uti_nom, uti_prenom, uti_matricule FROM utilisateurs WHERE uti_archive = 0 AND (uti_responsable=" . $_SESSION['acces']["acc_ref_id"] . " OR  uti_id = " . $_SESSION['acces']["acc_ref_id"] . " ) AND uti_societe = " . $acc_societe . " AND uti_agence = " . $acc_agence);
    } else {
        $req = $Conn->prepare("SELECT uti_id,uti_nom, uti_prenom, uti_matricule FROM utilisateurs WHERE uti_archive = 0 AND (uti_responsable=" . $_SESSION['acces']["acc_ref_id"] . " OR  uti_id = " . $_SESSION['acces']["acc_ref_id"] . " ) AND uti_societe = " . $acc_societe);
    }
}
$req->execute();
$utilisateurs_resp = $req->fetchAll();

// EVAC
if (!empty($utilisateurs_resp)) {
    // je suis reponsable d'au moins une personne
    foreach ($utilisateurs_resp as $u) {
        $list_utis[] = $u['uti_id'];
    }
    $query_ids = implode(',', $list_utis);
} else {
    $query_ids = $_SESSION['acces']["acc_ref_id"];
}
$req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id IN(" . $query_ids . ")");
$req->execute();
$utilisateurs = $req->fetchAll();
// EDITION
if (!empty($_GET['id'])) {
    $req = $Conn->prepare("SELECT * FROM conges WHERE con_id =" . $_GET['id']);
    $req->execute();
    $conge = $req->fetch();
}

?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Congé</title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

    <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css">
    <!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="sb-top sb-top-sm ">
    <form method="post" action="conge_enr.php" enctype="multipart/form-data">
        <!-- Start: Main -->
        <div id="main">
            <?php include "includes/header_def.inc.php"; ?>
            <!-- Start: Content-Wrapper -->
            <section id="content_wrapper">
                <section id="content" class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="admin-form theme-primary">
                                <div class="panel heading-border panel-primary">
                                    <div class="panel-body bg-light">
                                        <div class="text-left">
                                            <div class="content-header">
                                                <h2>
                                                    <?php if (isset($_GET['id'])) { ?>
                                                        Editer la demande de congés <b class='text-primary'><?= $_GET['id'] ?></b>
                                                    <?php } else { ?>
                                                        Nouvelle demande de <b class='text-primary'>congés</b>
                                                    <?php } ?>
                                                </h2>
                                            </div>
                                            <div class="col-md-10 col-md-offset-1">
                                                <?php if (isset($_GET['id'])) { ?>
                                                    <input type="hidden" name="con_id" value="<?= $_GET['id'] ?>">
                                                <?php } ?>
                                                <!-- COLONNE DE GAUCHE -->
                                                <div class="row mt10">
                                                    <div class="col-md-6">
                                                        <label for="com_donneur_ordre">Utilisateur : <span style="color:red;font-weight:bold;">*</span></label>
                                                        <select name="con_utilisateur" id="con_utilisateur" class="select2 calcul">
                                                            <option value="0" selected="selected">Utilisateur...</option>
                                                            <?php foreach ($utilisateurs as $u) { ?>
                                                                <option <?php
                                                                        if (!empty($conge) && $conge['con_utilisateur'] == $u['uti_id']) {
                                                                            echo "selected";
                                                                        } elseif ($u['uti_id'] == $_SESSION['acces']['acc_ref_id']) {
                                                                            echo "selected";
                                                                        }
                                                                        ?> value="<?= $u['uti_id'] ?>"><?= $u['uti_prenom'] ?> <?= $u['uti_nom'] ?></option>
                                                            <?php  } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="con_categorie">Catégorie : <span style="color:red;font-weight:bold;">*</span></label>
                                                        <select id="con_categorie" name="con_categorie" class="select2" required>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row" id="display_justif" style="display:none">
                                                    <div class="alert alert-info mt20 mb20">
                                                        Veuillez envoyer le justificatif au service RH
                                                    </div>
                                                </div>
                                                <div class="row mt10">
                                                    <div class="col-md-2  text-right">Du :<span style="color:red;font-weight:bold;">*</span></div>
                                                    <div class="col-md-6">
                                                        <span class="field prepend-icon">
                                                            <input type="text" class="gui-input datepicker-weeks date calcul" id="date_deb" name="con_date_deb" placeholder="Selectionner une date" <?php if (!empty($_GET['id'])) { ?> value="<?= convert_date_txt($conge['con_date_deb']) ?>" <?php } ?>>
                                                            <label class="field-icon">
                                                                <i class="fa fa-calendar-o"></i>
                                                            </label>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-3 mt10">
                                                        <div class="col-md-6">
                                                            <label class="option">
                                                                <input class="calcul" type="radio" name="con_demi_deb" id="con_demi_deb" value="1" <?php if (!empty($_GET['id']) && $conge['con_demi_deb'] == 1) { ?> checked <?php } elseif (empty($_GET['id'])) { ?> checked <?php } ?>>
                                                                <span class="radio mr5"></span> Matin
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="option">
                                                                <input class="calcul" type="radio" name="con_demi_deb" id="con_demi_deb" value="2" <?php if (!empty($_GET['id']) && $conge['con_demi_deb'] == 2) { ?> checked <?php } ?>>
                                                                <span class="radio mr5"></span> Après-Midi
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 mt10">
                                                        inclus
                                                    </div>
                                                </div>
                                                <div class="row mt10">
                                                    <div class="col-md-2  text-right">Au :<span style="color:red;font-weight:bold;">*</span></div>
                                                    <div class="col-md-6">
                                                        <span class="field prepend-icon">
                                                            <input type="text" class="gui-input datepicker-weeks date calcul" id="date_fin" name="con_date_fin" placeholder="Selectionner une date" <?php if (!empty($_GET['id'])) { ?> value="<?= convert_date_txt($conge['con_date_fin']) ?>" <?php } ?>>
                                                            <label class="field-icon">
                                                                <i class="fa fa-calendar-o"></i>
                                                            </label>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-3 mt10">
                                                        <div class="col-md-6">
                                                            <label class="option">
                                                                <input class="calcul" type="radio" name="con_demi_fin" id="con_demi_fin" value="1" <?php if (!empty($_GET['id']) && $conge['con_demi_fin'] == 1) { ?> checked <?php } elseif (empty($_GET['id'])) { ?> checked <?php } ?>>
                                                                <span class="radio mr5"></span> Matin
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="option">
                                                                <input class="calcul" type="radio" name="con_demi_fin" id="con_demi_fin" value="2" <?php if (!empty($_GET['id']) && $conge['con_demi_fin'] == 2) { ?> checked <?php } ?>>
                                                                <span class="radio mr5"></span> Après-Midi
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 mt10">
                                                        inclus
                                                    </div>
                                                </div>
                                                <div class="row mt10">
                                                    <div class="col-md-2  text-right">Nombre de jour(s) : </div>
                                                    <div class="col-md-10 text-left">
                                                        <span id="nbjours" style="color:red;font-weight:bold;">
                                                            <?php if (!empty($_GET['id'])) { ?>
                                                                <?= $conge['con_nb_jour'] ?>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <input type="hidden" name="con_nb_jour" id="nbjours_input">
                                                </div>

                                                <div class="row mt25">
                                                    <div class="col-md-2 mt5 text-right">Commentaire :</div>
                                                    <div class="col-md-10">

                                                        <textarea id="dli_texte" name="con_comment" class="summernote" rows="1">
                                                                    <?php if (!empty($_GET['id'])) { ?>
                                                                        <?= $conge['con_comment'] ?>
                                                                    <?php } ?>
                                                                </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
            </section>
        </div>
        <!-- End: Main -->
        <footer id="content-footer" class="affix">
            <div class="row">
                <div class="col-xs-3 footer-left">
                    <?php if (!isset($_SESSION['retour'])) { ?>
                        <a href="conges_liste.php" class="btn btn-default btn-sm">
                            <i class="fa fa-long-arrow-left"></i>
                            Retour
                        </a>
                    <?php } else { ?>
                        <a href="<?= $_SESSION['retour'] ?>" class="btn btn-default btn-sm">
                            <i class="fa fa-long-arrow-left"></i>
                            Retour
                        </a>
                    <?php } ?>
                    <?php if (!empty($_GET['id'])) {
                        $_SESSION['retour'] = "conge.php?id=" . $_GET['id'];
                    } ?>
                </div>
                <div class="col-xs-6 footer-middle"></div>
                <div class="col-xs-3 footer-right">
                    <button type="submit" id="submit" class="btn btn-success btn-sm">
                        <i class='fa fa-save'></i> Enregistrer
                    </button>
                </div>
            </div>
        </footer>
    </form>
    <?php
    include "includes/footer_script.inc.php"; ?>

    <script src="vendor/plugins/mask/jquery.mask.js"></script>
    <!-- plugin pour les masques formulaires -->
    <script src="assets/js/custom.js"></script>
    <script src="vendor/plugins/holder/holder.min.js"></script>
    <script src="vendor/plugins/select2/js/select2.min.js"></script>
    <script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
    <script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>
    <!-- pour mettre des images de tests -->
    <!-- Theme Javascript -->

                    } else {
                        $("#con_categorie").find("option:gt(0)").remove();
                        $.each(data, function(key, value) {
                            if (defaut == value["id"]) {
                                $('#con_categorie')
                                    .append($("<option></option>")
                                        .attr("value", value["id"])
                                        .attr("selected", "true")
                                        .text(value["nom"]));
                            } else {
                                $('#con_categorie')
                                    .append($("<option></option>")
                                        .attr("value", value["id"])
                                        .text(value["nom"]));
                            }
                        });
                        $("#con_categorie").trigger('change');

    <script>
        $(function() {
            <?php if (!empty($_GET['id'])) { ?>
                actualiser_categorie(<?= $conge['con_categorie'] ?>, $("#con_utilisateur").val());
                $("#con_utilisateur").change(() => {
                    actualiser_categorie(<?= $conge['con_categorie'] ?>, $("#con_utilisateur").val());
                });
            <?php } else { ?>
                actualiser_categorie(null, $("#con_utilisateur").val());
                $("#con_utilisateur").change(() => {
                    actualiser_categorie(null, $("#con_utilisateur").val());
                });
            <?php } ?>

            $("#con_categorie").change(() => {
                if ($("#con_categorie").val() == 3 || $("#con_categorie").val() == 9 || $("#con_categorie").val() == 10) {
                    $("#display_justif").show();
                } else {
                    $("#display_justif").hide();
                }
            })
        });

        function actualiser_categorie(defaut, utilisateur) {
            $.ajax({
                url: "ajax/ajax_conges_categories.php",
                type: "POST",

                data: 'field=<?= $_SESSION['acces']['acc_ref_id'] ?>&utilisateur=' + utilisateur,
                dataType: 'json',
                success: function(data) {
                    if (data['0'] == undefined) {

                        $("#con_categorie").find("option").remove();
                        $("#con_categorie").val("");
                        $("#con_categorie").trigger('change');
                    } else {
                        $("#con_categorie").find("option:gt(0)").remove();
                        $.each(data, function(key, value) {
                            if (defaut == value["id"]) {
                                $('#con_categorie')
                                    .append($("<option></option>")
                                        .attr("value", value["id"])
                                        .attr("selected", "true")
                                        .text(value["nom"]));
                            } else {
                                $('#con_categorie')
                                    .append($("<option></option>")
                                        .attr("value", value["id"])
                                        .text(value["nom"]));
                            }
                        });
                        $("#con_categorie").trigger('change');

                    }

                    if ($("#con_categorie").val() == 3 || $("#con_categorie").val() == 9 || $("#con_categorie").val() == 10) {
                        $("#display_justif").show();
                    }
                },
                error: function(data) {
                    modal_alerte_user(data.responseText);
                }
            });
        }
        <?php
        if (!empty($_GET['id'])) {

        ?>
            calculJours($("#con_utilisateur").val(), $("#date_deb").val(), $("#date_fin").val(), $('input[name=con_demi_deb]:checked').val(), $('input[name=con_demi_fin]:checked').val());
        <?php } ?>
        $(".calcul").change(function() {

            var ok = 1;
            $(".calcul").each(function() {
                // element == this
                if (!$(this).val()) {
                    ok = 0;
                }
            });
            if (ok == 1) {
                calculJours($("#con_utilisateur").val(), $("#date_deb").val(), $("#date_fin").val(), $('input[name=con_demi_deb]:checked').val(), $('input[name=con_demi_fin]:checked').val());
            }
        });

        function calculJours(utilisateur, date_deb, date_fin, demi_deb, demi_fin) {
            $.ajax({
                url: "ajax/ajax_conges_nb_jours.php",
                type: "POST",
                data: 'date_deb=' + date_deb + "&date_fin=" + date_fin + "&demi_deb=" + demi_deb + "&demi_fin=" + demi_fin,
                dataType: 'html',
                success: function(data) {
                    $("#nbjours").html(data);
                    $("#nbjours_input").val(data);
                },
                error: function(data) {
                    modal_alerte_user(data.responseText);
                }
            });
        }

        // DATEPICKER WITHOUT WEEKENDS
        $(".datepicker-weeks").datepicker({
            defaultDate: "+1w",
            dateFormat: "dd/mm/yy",
            closeText: 'Fermer',
            prevText: 'Précédent',
            nextText: 'Suivant',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
            dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            weekHeader: 'Sem.',
            numberOfMonths: 1,
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            beforeShowDay: $.datepicker.noWeekends,
            beforeShow: function(input, inst) {
                var themeClass = $(this).parents('.admin-form').attr('class');
                var smartpikr = inst.dpDiv.parent();
                if (!smartpikr.hasClass(themeClass)) {
                    inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
                }
            },
            onClose: function(selectedDate) {
                $("#datepicker-to").datepicker("option", "minDate", selectedDate);
            }
        });
    </script>

</body>

</html>