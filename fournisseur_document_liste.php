<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");;

$req = $Conn->prepare("SELECT * FROM fournisseurs_documents_Param");
$req->execute();
$fournisseurs_documents = $req->fetchAll();
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">

					<h1>Liste des documents fournisseurs</h1>
				
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="dark" >
									<th>ID</th>
									<th>Nom</th>
									<th>Type de document</th>
									<th>Type de fournisseur</th>
									<th>Obligatoire</th>
									<th>Validité</th>
									<th>Réseau Si2P</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
					<?php 		foreach ($fournisseurs_documents as $f){ ?>
									<tr>
										<td><?= $f['fdp_id'] ?></td>
										<td><?= $f['fdp_libelle'] ?></td>
								<?php 	if($f['fdp_type'] == 1){ ?>
											<td>Demandé au fournisseur</td>
								<?php 	}else{?>
											<td>Demandé aux intervenants</td>
								<?php	} 
										if($f['fdp_type'] == 1){
											if($f['fdp_entreprise'] AND !$f['fdp_indiv'] ){
												echo("<td>Entreprises uniquement</td>");
											}elseif(!$f['fdp_entreprise'] AND $f['fdp_indiv'] ){
												echo("<td>Auto-entrepreneurs uniquement</td>");
											}elseif($f['fdp_entreprise'] AND $f['fdp_indiv'] ){
												echo("<td>Entreprises et Auto-entrepreneurs</td>");
											}else{
												echo("<td>&nbsp;</td>");
											}
										}else{
											echo("<td>&nbsp;</td>");
										}

										if($f['fdp_obligatoire'] == 1){ ?>
											<td>OUI</td>
								<?php	}else{ ?>
											<td>&nbsp;</td>
								<?php	}
										if(empty($f['fdp_periodicite_mois'])){ ?>
											<td>illimité</td>
								<?php	}else{ ?>
											<td><?= $f['fdp_periodicite_mois'] ?> mois</td>
								<?php	} 
										if($f['fdp_une_seule_fois'] == 1){ ?>
											<td>Un document pour l'ensemble du réseau</td>
								<?php	}else{ ?>
											<td>Un document par société</td>
								<?php	} ?>

										<td class="text-center" >
											<a href="fournisseur_document.php?id=<?= $f['fdp_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier ce document">
												<span class="fa fa-pencil"></span>
											</a>
											<a href="fournisseur_document_suppr.php?id=<?= $f['fdp_id'] ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="bottom" title="Supprimer ce document">
												<span class="fa fa-times"></span>
											</a>
										</td>
									</tr>
						<?php	}	?>
							</tbody>
						</table>
					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-long-arrow-left"></span>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="fournisseur_document.php" class="btn btn-success btn-sm" role="button" >
						<span class="fa fa-plus"></span>
						Nouveau document
					</a>
				</div>
			</div>
		</footer>
<?php
		include "includes/footer_script.inc.php"; ?>	
		
	</body>
</html>
