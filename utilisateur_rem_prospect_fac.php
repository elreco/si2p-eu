<?php 

// AFFICHE LES FACTURES D'UN CLIENT SUR L'ENSEMBLE DU RESEAU
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


// DONNEE UTILE AU PROGRAMME

// personne connecté
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	} 
}

// param get
$client=0;
if(isset($_GET["client"])){
	if(!empty($_GET["client"])){
		$client=intval($_GET["client"]);
	}
}
$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}
$utilisateur=0;
if(isset($_GET["utilisateur"])){
	if(!empty($_GET["utilisateur"])){
		$utilisateur=intval($_GET["utilisateur"]);
	}
}
// CONTROLE ACCES

if( ($_SESSION['acces']["acc_profil"]!=10 AND $_SESSION['acces']["acc_profil"]!=13 AND $acc_utilisateur!=372) OR empty($client) OR empty($exercice) OR empty($utilisateur) ){
	$erreur_txt="Impossible d'afficher la page!";
}else{
	
	$sql="SELECT cli_code FROM Clients WHERE cli_id=" . $client . ";";
	$req=$Conn->query($sql);
	$d_client=$req->fetch();
	
}
?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">

		<?php 			if(!empty($erreur_txt)){ ?>
							<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-danger" style="border-radius:0px;"><?=$erreur_txt?></div>
							</div>
		<?php			}else{
			
							echo("<h1>Liste des factures de " . $d_client["cli_code"] . "</h1>");
							
							$sql="SELECT soc_id,soc_nom FROM Societes ORDER BY soc_nom;";
							$req=$Conn->query($sql);
							$d_societe=$req->fetchAll();
							if(!empty($d_societe)){  
								
								foreach($d_societe as $d_soc){ 
								
									$ConnFct=connexion_fct($d_soc["soc_id"]); 
									
									$sql="SELECT cli_code,cli_nom,DATE_FORMAT(fac_date,'%d/%m/%Y') as fac_date_aff,fac_numero,com_label_1 
									FROM Factures,Commerciaux,Clients WHERE fac_commercial=com_id AND fac_client=cli_id 
									AND (cli_id=" . $client . " OR cli_filiale_de=" . $client . ") ORDER BY fac_date;";
									$req=$ConnFct->query($sql);
									$d_facture=$req->fetchAll();
									if(!empty($d_facture)){ 
									
										echo("<h1>" . $d_soc["soc_nom"] . "</h1>"); ?>
				
										<div class="table-responsive">
											<table class="table table-striped" >
												<thead>
													<tr class="dark2" >	
														<th>Code</th> 
														<th>Nom</th> 
														<th>Facture</th> 
														<th>Date</th>										
														<th>Commercial</th>																		
													</tr>
												</thead>
												<tbody>
										<?php		foreach($d_facture as $f){	?>
														<tr>
															<td><?= $f['cli_code']?></td>	
															<td><?= $f['cli_nom']?></td>	
															<td><?= $f['fac_numero']?></td>	
															<td><?= $f['fac_date_aff']?></td>																							
															<td><?=	$f['com_label_1']?></td>															
														</tr>
							<?php					}  ?>
												</tbody>										
											</table>
										</div>
			<?php 					}
								} ?>
			<?php			} 
						}?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="utilisateur_rem_prospect.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>" class="btn btn-default btn-sm" role="button">
							<i class="fa fa-left"></i> Retour
						</a>	</div>
					<div class="col-xs-6 footer-middle">&nbsp;</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
			
			});						
		</script>
	</body>
</html>
