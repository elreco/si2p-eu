<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_get_client_infos.php');
include('modeles/mod_action_data.php');

// VISUALISATION D'UNE CONVENTION

$erreur_txt="";

$action_id=0;
if(!empty($_GET["action"])){
	$action_id=intval($_GET["action"]);
}
if(empty($action_id)){
	$erreur_txt="impossible d'afficher la page";

}else{

	$data=action_data($action_id);
	if(empty($data)){
		$erreur_txt="impossible d'afficher la page";
	}
}

$dateAnnexe=20;

 ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >



		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#zone_print{
				display:none;
				margin:0px!important;
			}


			/* GENERAL */

			#container_print{
				width:21cm;
				margin:auto;
				padding:0.5cm; /* remplace marge d'impression */
				background-color:#fff;
			}

			#page_print{
				width:100%;
				font-family:"Arial";
				border:1px solid white;

			}
			#page_print h1{
				font-family: "Arial";
				font-size: 16pt;
				color: #e31936;
			}
			#page_print h2{
				font-family: "Arial";
				font-size: 14pt;
				color: #7e8082;
			}
			#page_print .txt-strong{
				color: #e31936;
				font-weight: bold;
			}

			#page_print table{
				width:100%;
			}

			/* HEADER */

			#page_print header{

			}
			#page_print header .print-logo{
				width:4cm;
			}
			#page_print header img{
				max-width:4cm;
				max-height:2.5cm;
			}

			/* FOOTER */

			#page_print footer{
				font-size:7pt;
			}
			#page_print footer .print-logo{
				width:2.5cm;
			}
			#page_print footer img{
				max-width:2.5cm;
				max-height:2cm;
			}
			#page_print #foot{
				margin-bottom:30px;
			}
			#page_print .saut{
				page-break-after:always;
			}

			/* GRILLE PRINT */
			.print-col{
				display:inline-block;
				vertical-align:top;
			}
			.print-col-25{
				width:24%;
			}
			.print-col-33{
				width:32%;
			}
			.print-col-40{
				width:39%;
			}
			.print-col-50{
				width:49%;
			}
			.print-col-60{
				width:59%;
			}
			.print-col-67{
				width:66%;
			}
			.print-col-75{
				width:74%;
			}
			.print-col-100{
				width:99%;
			}

			#foot{
				min-height:4cm;
			}

			.ligne-body>p{
				text-align:justify;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
			html{
				background-color:#fff!important;
				margin:0px!important;
				padding:0px!important;;
			}
			body{
				background-color:#fff!important;
				margin:0px!important;;
				padding:0px!important;;
			}

		</style>
	</head>
	<body class="sb-top sb-top-sm document-affiche">
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php 		if(empty($erreur_txt)){ ?>

							<div class="row" >

								<!-- zone d'affichage du devis -->

								<div class="col-md-8"  >

									<div id="container_print" >

										<div id="page_print" >

											<div id="header" >
												<header>
													<h1 class="text-center" >Formation N° <?=$data["act_id"]?> du <?=$data["act_date_deb"]?></h1>
													<h2 class="text-center" ><?=$data["lib_famille"]?></h2>
												</header>
											</div>

											<div class="ligne-body" >
										<?php 	if(count($data["dates"])<=$dateAnnexe){ ?>
													<div class="print-col print-col-50" >
														<table class="table table-condensed">
															<caption class="txt-strong" >Dates</caption>
															<tbody>
									<?php						$date_id=0;
																foreach($data["dates"] as $ad){

																	$h="";
																	if(!empty($ad["ase_h_deb"])&&!empty($ad["ase_h_fin"])){
																		$h=$ad["ase_h_deb"] . " - " . $ad["ase_h_fin"];
																	}else{
																		if($ad["pda_demi"]==1){
																			$h="Matin";
																		}else{
																			$h="Après-midi";
																		}
																	} ?>
																	<tr>
															<?php		if($date_id!=$ad["pda_id"]){ ?>
																			<td>&nbsp;</td>
															<?php		}else{
																			echo("<td>&nbsp;</td>");
																		} ?>
																		<td><?=$ad["int_label_1"]?></td>
																		<td><?=$ad["pda_date"]?></td>
																		<td><?=$h?></td>
																	</tr>
												<?php			} ?>
															</tbody>
														</table>
													</div>
										<?php	} ?>

												<div class="print-col print-col-50" >
													<p>
												<?php	echo("<b>Lieu d'intervention :</b><br/>");
														echo($data["act_adr_nom"]);
														if(!empty($data["act_adr_service"])){
															echo("<br/>" . $data["act_adr_service"]);
														}
														if(!empty($data["act_adr1"])){
															echo("<br/>" . $data["act_adr1"]);
														}
														if(!empty($data["act_adr2"])){
															echo("<br/>" . $data["act_adr2"]);
														}
														if(!empty($data["act_adr3"])){
															echo("<br/>" . $data["act_adr3"]);
														}
														echo("<br/>" . $data["act_adr_cp"] . " " . $data["act_adr_ville"]); ?>
													</p>

													<p>
												<?php	echo("<b>Contact :</b><br/>");
														if(!empty($data["act_con_prenom"])){
															echo($data["act_con_prenom"]);
														}
														if(!empty($data["act_con_nom"])){
															echo(" " . $data["act_con_nom"]);
														}
														if(!empty($data["act_con_tel"])){
															echo("<br/>Tél. : " . $data["act_con_tel"]);
														}
														if(!empty($data["act_con_portable"])){
															echo("<br/>Port. : " . $data["act_con_portable"]);
														} ?>
													</p>
													<?php if(!empty($data["commercial"])){ ?>
													<p>
												<?php	echo("<b>Commercial :</b><br/>");
															echo($data["commercial"]);
														 ?>
													</p>
												<?php }?>
													<p>
											<?php		if(isset($data["vehicule"])){
															echo("<b>Véhicule :</b>" . $data["vehicule"]);
														} ?>
													</p>
											<?php	if(!empty($data["act_commentaire"])){ ?>
														<p class="p5" style="background-color:yellow;" >
															<b>Commentaire :</b><br/>
															<?=nl2br($data["act_commentaire"])?>
														</p>
											<?php	} ?>
												</div>


										<?php 	if(!empty($data["clients"])){
													if(count($data["dates"])>$dateAnnexe){
														// LES DATES PASSE EN ANNEXE ?>
														<div class="print-col print-col-50">
															<table class="mt15" >
																<caption class="txt-strong text-left" >Client <?=$data["clients"][0]["cli_code"]?></caption>
																<tr>
																	<th colspan="2" ><?=$data["clients"][0]["cli_nom"]?></th>
																</tr>
																<tr>
																	<th>Produit&nbsp;:</th>
																	<td><?=$data["clients"][0]["acl_pro_reference"]?></td>
																</tr>
																<tr>
																	<th>Formation&nbsp;:</th>
																	<td><?=$data["clients"][0]["acl_pro_libelle"]?></td>
																</tr>

															</table>
										<?php				if(!empty($data["clients"][0]["acl_commentaire"])){
																echo("<p class='p5' style='background-color:#FFDDBB;' ><b>Commentaire :</b><br/>" . $data["clients"][0]["acl_commentaire"] . "</p>");

															}
															if(!empty($data["clients"][0]["infos"])){

																echo("<strong>Infos :</strong>");
																foreach($data["clients"][0]["infos"] as $info){
																	echo("<p>" . $info["inf_description"] . "</p>");
																}

															} ?>
														</div>
										<?php			unset($data["clients"][0]);
													}
												} ?>
											</div>
											<div class="ligne-body" >
												<!-- CLIENTS -->
								<?php			$nbCli=0;
												foreach($data["clients"] as $k => $cli){ 	?>
													<div class="print-col print-col-50">
														<table class="mt15" >
															<caption class="txt-strong text-left" >Client <?=$cli["cli_code"]?></caption>
															<tr>
																<th colspan="2" ><?=$cli["cli_nom"]?></th>
															</tr>
															<tr>
																<th>Produit&nbsp;:</th>
																<td><?=$cli["acl_pro_reference"]?></td>
															</tr>
															<tr>
																<th>Formation&nbsp;:</th>
																<td><?=$cli["acl_pro_libelle"]?></td>
															</tr>
															<?php if(empty($data['commercial'])){ ?>
																<tr>
																	<th>Commercial&nbsp;:</th>
																	<td><?=$cli["commercial"]?></td>
																</tr>
															<?php }?>
														</table>
									<?php				if(!empty($cli["acl_commentaire"])){
															echo("<p class='p5' style='background-color:#FFDDBB;' ><b>Commentaire :</b><br/>" . $cli["acl_commentaire"] . "</p>");

														}
														if(!empty($cli["infos"])){
															echo("<strong>Infos :</strong>");
															foreach($cli["infos"] as $info){
																echo("<p>" . $info["inf_description"] . "</p>");
															}
														} ?>

													</div>
									<?php			$nbCli++;
													if($nbCli%2==0 AND $nbCli<count($data["clients"])){
														echo("</div><div class='ligne-body' >");
													}
												}?>
											</div>

								<?php		if(!empty($data["sta"])){
												foreach($data["sta"] as $donnee){  ?>
													<div class="ligne-body">
														<table class="table">
															<caption class="txt-strong" ><?=$donnee["libelle"]?></caption>
															<thead>
																<tr>
																	<th>Stagiaire</th>
																	<th class="print-col-40" >Attestation</th>
																</tr>
															</thead>
															<tbody>
									<?php						foreach($donnee["stagiaires"] as $sta){	?>
																	<tr>
																		<td><?=$sta["nom"]?></td>
																		<td><?=$sta["attestation"]?></td>
																	</tr>
									<?php						} ?>
															</tbody>
														</table>
													</div>
								<?php			}
											} ?>

											<div id="footer" >
												<footer>&nbsp;</footer>
											</div>

									<?php 	if(count($data["dates"])>$dateAnnexe){ ?>
												<!--ANNEXE -->
												<div class="annexe" data-annexe="1" id="annexe_1" style="page-break-before:always;" >
													<header id="h_annexe_1" >
														<header>
															<h1 class="text-center" >Formation N° <?=$data["act_id"]?> du <?=$data["act_date_deb"]?></h1>
															<h2 class="text-center" ><?=$data["lib_famille"]?></h2>
															<h3 class="text-center" >Planning</h3>
														</header>
													</header>
													<section id="s_annexe_1" >
														<div class="print-col print-col-50">
															<table class="table table-condensed">
																<tbody>
										<?php						for($bcl=0;$bcl<=count($data["dates"])/2;$bcl++){
																		$h="";
																		if(!empty($data["dates"][$bcl]["ase_h_deb"])&&!empty($data["dates"][$bcl]["ase_h_fin"])){
																			$h=$data["dates"][$bcl]["ase_h_deb"] . " - " . $data["dates"][$bcl]["ase_h_fin"];
																		}else{
																			if($data["dates"][$bcl]["pda_demi"]==1){
																				$h="Matin";
																			}else{
																				$h="Après-midi";
																			}
																		} ?>
																		<tr>
																			<td><?=$data["dates"][$bcl]["int_label_1"]?></td>
																			<td><?=$data["dates"][$bcl]["pda_date"]?></td>
																			<td><?=$h?></td>
																		</tr>
													<?php			} ?>
																</tbody>
															</table>
														</div>
														<div class="print-col print-col-50">
															<table class="table table-condensed">
																<tbody>
										<?php						for($bcl;$bcl<=count($data["dates"])-1;$bcl++){
																		$h="";
																		if(!empty($data["dates"][$bcl]["ase_h_deb"])&&!empty($data["dates"][$bcl]["ase_h_fin"])){
																			$h=$data["dates"][$bcl]["ase_h_deb"] . " - " . $data["dates"][$bcl]["ase_h_fin"];
																		}else{
																			if($data["dates"][$bcl]["pda_demi"]==1){
																				$h="Matin";
																			}else{
																				$h="Après-midi";
																			}
																		} ?>
																		<tr>
																			<td><?=$data["dates"][$bcl]["int_label_1"]?></td>
																			<td><?=$data["dates"][$bcl]["pda_date"]?></td>
																			<td><?=$h?></td>
																		</tr>
													<?php			} ?>
																</tbody>
															</table>
														</div>
													</section>
													<footer id="f_annexe_1" >

													</footer>
												</div>
									<?php	} ?>
										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->

								<div class="col-md-4" >
									<!-- DEBUT ANNEXE -->

									<!-- FIN ANNEXE -->
								</div>

							</div>


		<?php		}else{ ?>

						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>

		<?php		} ?>

				</section>

				<!-- End: Content -->
			</section>


		</div>

		<!-- End: Main -->

		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="action_voir.php?action=<?=$action_id?>&onglet=3&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
					<button type="button" class="btn btn-sm btn-info ml15" id="print" >
						<i class="fa fa-print"></i> Imprimer
					</button>
				</div>
				<div class="col-xs-3 footer-right" ></div>
			</div>
		</footer>


<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function () {

				mise_en_page();

				$("#print").click(function(){

					$("#zone_print").html($("#container_print").html());

					$("#main").hide();
					$("#content-footer").hide();
					$(".btn").hide();

					$("#zone_print").show();

					window.print();

					$("#zone_print").html("");
					$("#zone_print").hide();

					$("#main").show();
					$("#content-footer").show();
					$(".btn").show();
					//mise_en_page();


				});

			});

			function mise_en_page(){

				$(".header").remove();
				$(".footer").remove();
				$(".ligne-header").remove();
				$(".saut").remove();

				var ppc=(parseInt($("#page_print").width())/21);
				var hauteur=parseInt((27*ppc));

				var saut="";

				var h_header=parseInt($("#page_print #header").height());

				var h_head=0;
				if($("#page_print #head").length>0){
					h_head=parseInt($("#page_print #head").height());
				}

				var h_ligne_header=0;
				if($("#page_print #ligne_header").length>0){
					h_ligne_header=parseInt($("#page_print #ligne_header").height());
				}

				var h_foot=0;
				if($("#page_print #foot").length>0){
					// marge ini a 0 -> sinon comptabilise lors du second appel
					$("#page_print #foot").css("margin-top","0px");
					h_foot=parseInt($("#page_print #foot").outerHeight(true));
				}
				// marge ini a 0 -> sinon comptabilise lors du second appel
				$("#page_print #footer").css("margin-top","0px");
				var h_footer=parseInt($("#page_print #footer").height());

				var libre=hauteur-(h_header+h_head+h_ligne_header+h_footer);

				var nb_ligne=$(".ligne-body").length;
				var num_ligne=0;
				var num_page=1;
				var h_ligne=0;



				$(".ligne-body").each(function( index ){

					num_ligne=num_ligne+1;

					h_ligne=parseInt($(this).height());

					if((libre-h_ligne<0)||(num_ligne==nb_ligne && (libre-(h_ligne+h_foot)<0)) ){
						saut=""
						saut=saut + "<div class='footer' style='margin-top:" + libre + "px;' >";
							saut=saut+$("#footer").html();
						saut=saut+"</div>"
						saut=saut+"<div class='saut' ></div>";
						saut=saut+"<div class='header' >";
							saut=saut+$("#header").html();
						saut=saut+"</div>";

						if($("#ligne_header").length>0){
							saut=saut+"<div class='ligne-header' >";
								saut=saut+$("#ligne_header").html();
							saut=saut+"</div>";
						}

						libre=hauteur-(h_header + h_ligne_header + h_footer);
						if(num_ligne==nb_ligne){
							libre=libre-h_foot;
						}

						libre=libre-h_ligne;

						$(this).before(saut);



					}else{
						libre=libre-h_ligne;
						if(num_ligne==nb_ligne){
							libre=libre-h_foot;
						}
						if(libre<0){
							libre=0;
						}
					}
				});
				if($("#page_print #foot").length>0){
					$("#foot").css("margin-top", libre + "px");
				}else{
					$("#footer").css("margin-top", libre + "px");
				}

				// ANNEXE
				$(".annexe").each(function( index ){
					var annexe=$(this).data("annexe");

					var h_annexe=0;
					if($("#h_annexe_" + annexe).length>0){
						h_annexe=parseInt($("#h_annexe_" + annexe).height());
					}
					var s_annexe=0;
					if($("#s_annexe_" + annexe).length>0){
						s_annexe=parseInt($("#s_annexe_" + annexe).height());
					}
					var f_annexe=0;
					if($("#f_annexe_" + annexe).length>0){
						f_annexe=parseInt($("#f_annexe_" + annexe).height());
					}
					libre=hauteur-(h_annexe + s_annexe + f_annexe);
					$("#f_annexe_" + annexe).css("margin-top", libre + "px");
				});
			}

		</script>
	</body>
</html>
