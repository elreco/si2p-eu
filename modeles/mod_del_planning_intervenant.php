<?php

/*
	SUPPRIME UNE LIGNE DE PLANNING SUR UNE SEMAINE DONNEE
	s'il n'y a rien d'enregistré

	ATTENTION LUNDI doit etre un DATE TIME

	auteur FG 20/02/2017

*/
function del_planning_intervenant($intervenant,$lundi,$samedi,$agence){

	global $ConnSoc;

	$retour=true;

	if($intervenant==0 OR empty($lundi) OR empty($samedi) ){
		$retour=false;
	}

	if($retour){

		if(!empty($agence)){
			$agence=intval($agence);
		}


		$semaine=$lundi->format("W");
		$date_deb=$lundi->format("Y-m-d");
		$date_fin=$samedi->format("Y-m-d");

		$semaine=intval($semaine);
		$annee=$lundi->format("Y");

		// on verif que l'intervenant n'a pas d'action sur la periode selectionnée
		$sql_case="SELECT pda_date FROM Plannings_Dates,Actions WHERE pda_ref_1=act_id AND pda_type=1 AND pda_intervenant=:intervenant AND pda_date>=:date_deb AND pda_date<=:date_fin AND NOT pda_archive
		AND act_agence=:agence;";
		$req_case=$ConnSoc->prepare($sql_case);
		$req_case->bindParam(":intervenant",$intervenant);
		$req_case->bindParam(":date_deb",$date_deb);
		$req_case->bindParam(":date_fin",$date_fin);
		$req_case->bindParam(":agence",$agence);
		$req_case->execute();
		$case=$req_case->fetch();
		if(empty($case)){

			// non utiliser sur la periode on delete
			

			$sql_delete="DELETE FROM Plannings_Intervenants WHERE pin_intervenant=:intervenant AND pin_semaine=:semaine AND pin_annee=:annee AND pin_agence=:agence";
			//$sql_delete="SELECT * FROM Plannings_Intervenants WHERE pin_intervenant=:intervenant AND pin_semaine=:semaine AND pin_annee=:annee";
			$req_delete=$ConnSoc->prepare($sql_delete);
			$req_delete->bindParam(":intervenant",$intervenant);
			$req_delete->bindParam(":semaine",$semaine);
			$req_delete->bindParam(":annee",$annee);
			$req_delete->bindParam(":agence",$agence);
			try {
				$req_delete->execute();
			}catch (Exception $e){
				echo 'Exception reçue : ',  $e->getMessage(), "\n";
				$retour=false;
			}
		}else{
			$retour=false;
		}
	}
	return $retour;
}
?>
