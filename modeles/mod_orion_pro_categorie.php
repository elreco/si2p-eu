<?php

// RETOURNE UNE LIGNE DE LA TABLE Produits_Categories  qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_pro_categorie($pca_id){
	
	global $Conn;
	
	$sql="SELECT pca_libelle FROM Produits_Categories WHERE pca_id=:pca_id";
	$req=$Conn->prepare($sql);
	$req->bindParam(":pca_id",$pca_id);
	$req->execute();
	$pro_categorie = $req->fetch();
	if(!empty($pro_categorie)){
		return $pro_categorie;
	}
 
}
