<?php

// GENERE DES ATTESTATIONS EN PDF
require_once("vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;

function model_commande_pdf($data){

	global $acc_societe;
	
	$erreur="";
	ob_start(); ?>
	
	<style type="text/css" >
<?php	require("assets/skin/si2p/css/orion_pdf.css"); ?>


table{
	width:100%;
	color:#333333;
	font-size:11pt;
	font-family:helvetica;
	line-height:6mm;
	border-collapse:collapse;
}


.text-right{
	text-align:right;
}
.text-center{
	text-align:center;
}

table.border{
	
}
table.border th{
	background-color:#333333;
	color:#FFFFFF;
	text-align:center;
}

table.border td{
	border:1px solid #7e8082;
}



h3{
	font-size:12pt;
}
.cadre{
	border:1px solid #333333;
	padding:5px;
}

.juridique{
	font-size:7pt;
	line-height:3mm;
	text-align:center;
}

h2{
	color:#333333;
	font-size: 24px;
	margin-top: 9.5px;
	margin-bottom: 9.5px;
}
.text-primary{
	color: #E31936;
}
h4{
	font-size:12pt;
	margin:0px;
	
}
h5{
	font-size:8pt;
	color:#E31936;
	margin:5px 0px 0px 0px;
}
h6{
	font-size:8pt;
	margin:5px 0px 0px 0px;
}
.cgv-intro{
	color:#E31936;
	font-size:8pt;
	text-align:justify;
	margin:5px 0px 0px 0px;
}
.cgv{
	text-align:justify;
	font-size:7pt;
	margin:5px 0px 0px 0px;
}
.titre{
	
	background-color:#333333;
	color:white;
}
.mt20{
	
	margin-top:20px;
}
	</style>
	
<?php	
	foreach($data["pages"] as $p => $data){


			/*echo("<pre>");
				print_r($page);
			echo("</pre>"); */ ?>

			<page backtop="30mm" backleft="5mm" backright="5mm" backbottom="15mm" >
				<page_header>
					
					<?php       if(!empty($data['agence'])){ ?>
						<div class="text-left" style="width:15%;display:inline;">
						<?php       if(!empty($data['agence']["age_logo_1"])){
							echo("<img style='max-width:100%;' src='documents/societes/logos/" . $data['agence']["age_logo_1"] . "' />");
							} ?> 
							</div>
							
							<div class="text-center" style="width:70%;display:inline;">
								<h1 class="text-center" >COMMANDE D'ACHAT</h1>													
							</div>
						<div class="text-right" style="width:15%;display:inline;">
							<?php       if(!empty($data['agence']["age_logo_2"])){
								echo("<img style='max-width:100%;' src='documents/societes/logos/" . $data['agence']["age_logo_2"] . "' />");
							} ?>   
						</div>
						
					<?php }elseif(!empty($data['societe'])){ ?>
						<div class="text-left" style="width:15%;display:inline;">
							<?php if(!empty($data['societe']["soc_logo_1"])){
								echo("<img style='max-width:100%;' src='documents/societes/logos/" . $data['societe']["soc_logo_1"] . "' />");
							} ?> 
							</div>
							
							<div class="text-center"  style="width:70%;display:inline;">
								<h1 class="text-center" >COMMANDE D'ACHAT</h1>													
							</div>
						<div class="text-right" style="width:15%;display:inline;">
							<?php       if(!empty($data['societe']["soc_logo_2"])){
								echo("<img style='max-width:100%;' src='documents/societes/logos/" . $data['societe']["soc_logo_2"] . "' />");
							} ?>   
						</div>
						
					<?php } ?>
																				
				</page_header>
				<page_footer>
				<hr/>
        <table>
            <tr>
			<?php if(!empty($data['agence'])){ ?>
					<td style="width:15%;max-height:35px" >
			<?php       if(!empty($data['agence']["age_qualite_1"])){
							echo("<img style='max-width:100%;' src='documents/societes/logos/" . $data['agence']["age_qualite_1"] . "' />");
						} ?>    
					</td>
					<td style="width:70%;text-align:center;font-size:10px;line-height:12px;" >
						
                    <?= $data['societe']['soc_nom'] ?> <?= $data['agence']['age_nom'] ?> - <?= $data['societe']['soc_type'] ?> au capital de <?= $data['societe']['soc_capital'] ?> € - APE <?= $data['societe']['soc_ape'] ?> - RCS <?= $data['societe']['soc_rcs'] ?><br>
						Siret <?= $data['agence']['age_siret'] ?> - N° TVA <?= $data['societe']['soc_tva'] ?><br>
						N° déclaration Existence <?= $data['societe']['soc_num_existence'] ?>

					</td>
					<td style="width:15%;text-align:right;max-height:35px" >
			<?php       if(!empty($data['agence']["age_qualite_2"])){
							echo("<img style='max-width:100%;' src='documents/societes/logos/" . $data['agence']["age_qualite_2"] . "' />");
						} ?>    
					</td>
			
			<?php       }elseif(!empty($data['societe'])){ ?>
					<td style="width:15%;max-height:35px" >
			<?php       if(!empty($data['societe']["soc_logo_1"])){
							echo("<img style='max-width:100%;' src='documents/societes/logos/" . $data['societe']["soc_logo_1"] . "' />");
						} ?>    
					</td>
					<td style="width:70%;text-align:center;font-size:10px;line-height:12px;" >
						
						<?= $data['societe']['soc_nom'] ?> - <?= $data['societe']['soc_type'] ?> au capital de <?= $data['societe']['soc_capital'] ?> € - APE <?= $data['societe']['soc_ape'] ?> - RCS <?= $data['societe']['soc_rcs'] ?><br>
						Siret <?= $data['societe']['soc_siren'] ?> <?= $data['societe']['soc_siret'] ?> - N° TVA <?= $data['societe']['soc_tva'] ?><br>
						N° déclaration Existence <?= $data['societe']['soc_num_existence'] ?>

					</td>
					<td style="width:15%;text-align:right;max-height:35px;" >
			<?php       if(!empty($data['societe']["soc_logo_2"])){
							echo("<img style='max-width:100%;' src='documents/societes/logos/" . $data['societe']["soc_logo_2"] . "' />");
						} ?>    
					</td>
					
					<?php } ?> 
				</tr>
			</table>    
				</page_footer>

				<table style="margin-bottom:20px;">
						<tr>
							<td style="width:70%;" >
								<h3 style="margin-bottom:0px;">Adresse de facturation</h3>
								<div style="margin-bottom:10px;">
									<strong><?= $data['com_fac_nom'] ?></strong><br>
									<?= $data['com_fac_adr1'] ?>
									<?php if(!empty($data['com_fac_adr2'])){ ?>
										<br> <?= $data['com_fac_adr2'] ?>
									<?php } ?>
									<?php if(!empty($data['com_fac_adr3'])){ ?>
										<br> <?= $data['com_fac_adr3'] ?>
									<?php } ?>
									<br><?= $data['com_fac_cp'] ?> <?= $data['com_fac_ville'] ?> 
								</div>
								<h3 style="margin-bottom:0px;">Adresse de livraison</h3>
								<div style="margin-bottom:10px;">
									<strong><?= $data['com_liv_nom'] ?></strong><br>
									<?= $data['com_liv_adr1'] ?>
									<?php if(!empty($data['com_liv_adr2'])){ ?>
										<br> <?= $data['com_liv_adr2'] ?>
									<?php } ?>
									<?php if(!empty($data['com_liv_adr3'])){ ?>
										<br> <?= $data['com_liv_adr3'] ?>
									<?php } ?>
									<br><?= $data['com_liv_cp'] ?> <?= $data['com_liv_ville'] ?> 
								</div>
							</td>
							<td style="width:30%;">
								<h3 style="margin-bottom:0px;"><?= $data['fournisseur']['fou_nom'] ?></h3>
								<div style="margin-bottom:40px;">
									<?= $data['fournisseur_adresse']['fad_ad1'] ?>
									<?php if(!empty($data['fournisseur_adresse']['fad_ad2'])){ ?>
										<br> <?= $data['fournisseur_adresse']['fad_ad2'] ?>
									<?php } ?>
									<?php if(!empty($data['fournisseur_adresse']['fad_ad3'])){ ?>
										<br> <?= $data['fournisseur_adresse']['fad_ad3'] ?>
									<?php } ?>
									<br><?= $data['fournisseur_adresse']['fad_cp'] ?> <?= $data['fournisseur_adresse']['fad_ville'] ?>
								</div>
								<h3 style="margin-bottom:0px;">Affaire suivie par : </h3>
								<div style="margin-bottom:10px;">
									<?= $data['ordre']['uti_prenom'] ?> <?= $data['ordre']['uti_nom'] ?>
								</div>
							</td>
						</tr>
					</table>

				<table class="mt20">
        
		<tr>
			<td style="width:30%;background-color:#333333;color:#FFFFFF;text-align:center;" ><strong>Numéro : </strong><?= $data['com_numero'] ?></td>
			<td style="width:40%;" > </td>
			<td style="width:30%;background-color:#333333;color:#FFFFFF;text-align:center;" ><?= convert_date_fr($data['com_date']) ?></td>    
		</tr>
	
</table>

<table class="border mt20">
	<thead>
		<tr style="border:1px solid #333333">
			<th style="width:25%;height:20px;" >Référence</th>
			<th style="width:35%;height:20px;" >Désignation</th>
			<th style="width:15%;height:20px;" >Prix HT</th>
			<th style="width:5%;height:20px;" >Qté</th>
			<th style="width:20%;height:20px;">Montant HT</th>
		</tr>
	</thead>    
	<tbody>
<?php foreach($data['lignes'] as $l){ ?>
			<tr>
				<td style="width:25%;" ><?php if(!empty($l['pro_code_produit'])){ ?><?= $l['pro_code_produit'] ?><?php }else{ ?><?= $l['cli_reference'] ?><?php } ?></td>
				<td style="width:35%;" >
				<strong><?= $l['cli_libelle'] ?></strong><br><?= $l['cli_descriptif'] ?>
				</td>
				<td style="width:15%;text-align:right;"><?= number_format($l['cli_pu'], 2, ',', ' ') ?> €</td>
				<td style="width:5%;text-align:center;"><?=str_replace(",00", "", (string)number_format ($l['cli_qte'], 2, ",", "")); ?></td>
				<td style="width:20%;text-align:right;"><?= number_format($l['cli_ht'], 2, ',', ' ') ?> €</td>
				
					
			</tr>
	<?php   } ?>
		

	</tbody>
</table>
<table class="border mt20"> 
	
		<tr>
			<td style="width:40%;border-left:none;border-bottom:none;border-top:none;" >
					
			</td>
			<td style="width:30%;text-align:center;background-color:#333333;color:#FFFFFF;" >
				
					
					Total HT
				
			</td>
			<td style="width:30%;text-align:right;" >
				
					
					<?= number_format($data['com_ht'], 2, ',', ' '); ?> €
				
			</td>
		</tr>
		<?php foreach($data['total_lignes'] as $k => $v){ ?>
		<tr>
			<td style="width:40%;border-left:none;border-bottom:none;border-top:none;" >
				
			</td>
			
				<td style="width:30%;text-align:center;background-color:#333333;color:#FFFFFF;" >
					 TVA (<?= $v ?> %)
				</td>
				<td style="width:30%;text-align:right;" >
				<?= number_format($data['com_ttc'] - $data['com_ht'], 2, ',', ' '); ?> € 
				</td>
			
		</tr>
		<?php } ?>
		<tr>
			<td style="width:40%;border-left:none;border-bottom:none;border-top:none;" >
				
			</td>
			<td style="width:30%;text-align:center;background-color:#333333;color:#FFFFFF;" >
					Total TTC
			</td>
			<td style="width:30%;text-align:right;" >
				
					
					<?= number_format($data['com_ttc'], 2, ',', ' '); ?> €
				
			</td>
		</tr>
</table>

<!-- <div style="position:absolute;bottom:10px;left:0;border:1px solid white;" > -->
	<table> 
		<tr>
			<td style="width:30%;" >
				<div class="cadre" >
					<h4 style="margin:0;font-size:12px;">Condition de règlement</h4>
					<?php if(!empty($data['com_reg_type'])){ ?>
						
						<?php foreach($data['base_reglement'] as $k => $n){ ?>
							<?php if($k > 0){ ?>
								<?php if($data['com_reg_type'] == $k){ ?><?= $n ?><?php } ?>
							<?php } ?>
						<?php } ?>
					<?php } ?> 

					<?php if($data['com_reg_formule'] == 1){ ?>

						Date + <?php if(!empty($data['com_reg_formule'])){ ?><?= $data['com_reg_nb_jour'] ?><?php }else{ ?>0<?php } ?> jours

					<?php } ?>
					<?php if($data['com_reg_formule'] == 2){ ?>
						Date + 45j + fin de mois
					<?php } ?>
					<?php if($data['com_reg_formule'] == 3){ ?>
						Date + fin de mois + 45j
					<?php } ?>
					<?php if($data['com_reg_formule'] == 4){ ?>
						Date + <?php if(!empty($data['com_reg_nb_jour'])){ ?><?= $data['com_reg_nb_jour'] ?><?php }else{ ?>0<?php } ?> jours + fin de mois + <?php if(!empty($data['com_reg_fdm'])){ ?><?= $data['com_reg_fdm'] ?><?php }else{ ?>0<?php } ?> jour(s)
					<?php } ?>
				</div>
			</td>
			<td style="width:35%;">
				<div class="cadre" style="height:100px;">
					<h4 style="margin:0;width:100%;font-size:12px;">Signature et cachet du fournisseur</h4>
					
				</div>
			</td>
			<td style="width:35%;" >
                <div class="cadre" style="height:100px;text-align:center;position: relative;top: 0;left: 0;">
                    <h4 style="margin:0;width:100%;font-size:12px;">Signature et cachet du donneur d'ordre</h4>
                    <?php       if(!empty($data['agence']) && file_exists("documents/societes/signatures/tampons/age_".$data['agence']['age_id'] . ".png")){ ?>
						<img style='max-width:60%;position: relative;top: 0;left: 0;' src='documents/societes/signatures/tampons/age_<?=$data['agence']['age_id']?>.png'/>
						<?php if(file_exists("documents/utilisateurs/signatures/signature_".$data['com_donneur_ordre'].".png")){ ?>
							<img style="position:absolute;top:10px;left:35px;max-width:70%;" src="documents/utilisateurs/signatures/signature_<?=$data['com_donneur_ordre']?>.png">
						<?php }?>
					<?php }elseif(file_exists("documents/societes/signatures/tampons/soc_".$data['agence']['soc_id'] . ".png")){ ?>
						<img style='max-width:60%;position: relative;top: 0;left: 0;' src='documents/societes/signatures/tampons/age_<?=$data['societe']['soc_id']?>.png'/>
						<?php if(file_exists("documents/utilisateurs/signatures/signature_".$data['com_donneur_ordre'].".png")){ ?>
							<img style="position:absolute;top:10px;left:35px;max-width:70%;" src="documents/utilisateurs/signatures/signature_<?=$data['com_donneur_ordre']?>.png">
						<?php }?>
					<?php } ?>
                </div>
			</td>
		</tr>
	</table>
<!-- </div> -->
			</page>
			
<?php	
	} 

	$dataontent=ob_get_clean();
    /* echo $dataontent;
    die(); */
	try{
		$pdf=new HTML2PDF('P','A4','fr');
		$pdf->pdf->SetDisplayMode('fullwidth');
		//echo($dataontent);
		$pdf->writeHTML($dataontent);
		if($pdf){
			$pdf->Output($_SERVER["DOCUMENT_ROOT"] . '/Documents/Commandes/' . $data["com_numero"] . '.pdf', 'F');					
		}
	}catch(HTML2PDF_exception $e){
		$erreur="Erreur lors de la génération du fichier PDF";
	}
	
	if(!empty($erreur)){
		return false;
	}else{
		return true;
	}
}
?>