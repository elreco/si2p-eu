<?php

/* RETOURNES LES STAGIAIRES AFFECTE A UNE ACTION

action : id de l'action dont on veut recuperer les dates

option
action_client >0 retourne les dates de l'action avec les valeurs de presence du client concernee

AUTEUR FG
*/
function get_action_stagiaires($action,$action_client,$session){

	global $ConnSoc;
	
	//echo("action : " . $action);
	//echo("action_client : " . $action_client);
	//die();

	$sql="SELECT DISTINCT sta_id,sta_nom,sta_prenom,cli_code,ast_confirme,ast_confirme";
	$sql.=" FROM Stagiaires,Actions_Stagiaires,Actions_Stagiaires_Sessions,Actions_Clients,Clients";
	$sql.=" WHERE sta_id=ast_stagiaire AND sta_id=ass_stagiaire AND ast_action_client=acl_id AND acl_client=cli_id";
	$sql.=" AND acl_action=:action";
	if($action_client>0){
		$sql.=" AND ast_action_client=:action_client";
	}
	if($session>0){
		$sql.=" AND ass_session=:session";
	}
	$sql.=" ORDER BY sta_nom,sta_prenom;";
	$req = $ConnSoc->prepare($sql);
	$req->bindParam(":action",$action);
	if($action_client>0){
		$req->bindParam(":action_client",$action_client);
	}
	if($session>0){
		$req->bindParam(":session",$session);
	}
	$req->execute();
	$stagiaires = $req->fetchAll();
	
    return $stagiaires;
}
