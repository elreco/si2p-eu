<?php

/* PERMET D'AFFECTER UNE DATE DE FORMATIION A UN CLIENT

date_id : id de la date = pda_id
action_client : id de l'inscription du client = acl_id

AUTEUR FG
*/
function add_action_client_date($date_id,$action_client_id){

	global $ConnSoc;
	
	$sql="INSERT INTO Actions_Clients_Dates (acd_action_client,acd_date) VALUES (:action_client,:date)";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action_client",$action_client_id);
	$req->bindParam(":date",$date_id);
	$req->execute();

}
