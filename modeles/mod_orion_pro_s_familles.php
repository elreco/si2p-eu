<?php

// RETOURNE LA TABLE Clients_Infos qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_produits_sous_familles(){

	global $Conn;
	
	/*$result[0]=array(
		"psf_libelle" => "",
		"psf_couleur" => ""
	);*/
	
	$sql="SELECT psf_id,psf_libelle,psf_couleur_bg,psf_couleur_txt,psf_pfa_id FROM Produits_Sous_Familles ORDER BY psf_libelle";
	$req=$Conn->prepare($sql);
	$req->execute();
	$pro_s_famille = $req->fetchAll();
	if(!empty($pro_s_famille)){
		foreach($pro_s_famille as $psf){
			$result[$psf["psf_id"]]=array(
				"psf_libelle" => $psf["psf_libelle"],
				"psf_couleur_bg" => $psf["psf_couleur_bg"],
				"psf_couleur_txt" => $psf["psf_couleur_txt"],
				"psf_pfa_id" => $psf["psf_pfa_id"]
			);
		}
	}

    return $result;
}

function orion_produits_sous_sous_familles(){

	global $Conn;
	
	/*$result[0]=array(
		"pss_libelle" => "",
		"pss_couleur" => ""
	);*/
	
	$sql="SELECT pss_id,pss_libelle,pss_couleur_bg,pss_couleur_txt,pss_psf_id FROM Produits_Sous_sous_Familles ORDER BY pss_libelle";
	$req=$Conn->prepare($sql);
	$req->execute();
	$pro_s_famille = $req->fetchAll();
	if(!empty($pro_s_famille)){
		foreach($pro_s_famille as $psf){
			$result[$psf["pss_id"]]=array(
				"pss_libelle" => $psf["pss_libelle"],
				"pss_couleur_bg" => $psf["pss_couleur_bg"],
				"pss_couleur_txt" => $psf["pss_couleur_txt"],
				"pss_psf_id" => $psf["pss_psf_id"]
			);
		}
	}

    return $result;
}
