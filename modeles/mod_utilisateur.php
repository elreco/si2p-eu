<?php

// MODELE LIE AUX UTILISATEURS Si2P 

/*-------------------------------------------------
	FONCTION DE SELECTION 
---------------------------------------------------*/

  function get_utilisateur($id){
	 
	global $Conn;
	
	/*if(empty($id)){
		$id=0;
	}*/

    $req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id=:uti_id"); // . $id);
	$req->bindParam(':uti_id', $id);
    $req->execute();

    $utilisateur = $req->fetch();

    return $utilisateur;
}



	function get_utilisateurs($societe,$agence,$profil,$nom,$archive){
		 
		global $Conn;

		$sql="SELECT * FROM utilisateurs";
		
		$mil="";
		if(!empty($societe)){
			$mil.=" AND uti_societe=" . $societe; 
		};
		if(!empty($agence)){
			$mil.=" AND uti_agence=" . $agence; 
		};
		if(!empty($profil)){
			$mil.=" AND uti_profil=" . $profil; 
		};
		if($nom!=""){
			$mil.=" AND uti_nom LIKE '" . $nom . "%'"; 
		};
		if($archive){
			$mil.=" AND uti_archive"; 
		}else{
			$mil.=" AND NOT uti_archive"; 
		};
		
		if($mil!=""){
			$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		}
		
		$sql.=" ORDER BY uti_nom";

		$req = $Conn->prepare($sql);
		$req->execute();
		
		$utilisateurs = $req->fetchAll();

		return $utilisateurs;
	}

/*-------------------------------------------------
	SELECTION POUR INJECTION HTML
---------------------------------------------------*/

	function get_utilisateur_select($societe,$agence,$list_profil,$list_profil_type,$valeur_select){
		
		global $Conn;

		$get_utilisateur_select="";
		
		$sql="SELECT * FROM Utilisateurs LEFT OUTER JOIN Profils ON (Utilisateurs.uti_profil=Profils.pro_id)";
		
		$mil="";
		if(!empty($societe)){
			$mil=$mil . " AND uti_societe=" . $societe;
			if(!empty($agence)){
				$mil=$mil . " AND uti_agence=" . $agence;	
			}
		}
		if(!empty($list_profil)){
			$mil=$mil . " AND uti_profil IN (" . $list_profil . ")";
		}
		if(!empty($list_profil_type)){
			$mil=$mil . " AND pro_id IN (" . $list_profil_type . ")";
		}
		
		if(!empty($mil)){
			$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		}
		$sql=$sql . " ORDER BY uti_nom,uti_prenom";	
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{	if($valeur_select==$donnees["uti_id"]){
				$get_utilisateur_select=$get_utilisateur_select . "<option value='" . $donnees["uti_id"] . "' selected >" . $donnees["uti_nom"] . " " . $donnees["uti_prenom"] . "</option>";
			}else{
				$get_utilisateur_select=$get_utilisateur_select . "<option value='" . $donnees["uti_id"] . "' >" . $donnees["uti_nom"] . " " . $donnees["uti_prenom"] . "</option>";	
			}
			
		};
		return $get_utilisateur_select;
	}

/*-------------------------------------------------
	CREATION
---------------------------------------------------*/
	
	function insert_utilisateur($uti_titre,$uti_nom,$uti_prenom,$uti_ad1,$uti_ad2,$uti_ad3,$uti_cp,$uti_ville,$uti_tel_perso,$uti_mobile_perso,$uti_mail_perso,$uti_tel,$uti_fax,$uti_mobile,$uti_mail,$uti_societe,$uti_agence,$uti_profil,$uti_contrat,$uti_fonction,$uti_responsable)
	{	// Ajout un nouvel utilisateur retourne id si création ok sinon 0
		
		global $Conn;
		
		$utilisateur;
		
		$sql="INSERT INTO Utilisateurs (uti_titre,uti_nom,uti_prenom,uti_ad1,uti_ad2,uti_ad3,uti_cp,uti_ville,uti_tel_perso,uti_mobile_perso,uti_mail_perso,uti_tel,uti_fax,uti_mobile,uti_mail,uti_societe,uti_agence,uti_profil,uti_contrat,uti_fonction,uti_responsable)";
		$sql.=" VALUE (:uti_titre, :uti_nom, :uti_prenom, :uti_ad1, :uti_ad2, :uti_ad3, :uti_cp, :uti_ville, :uti_tel_perso, :uti_mobile_perso, :uti_mail_perso, :uti_tel, :uti_fax, :uti_mobile, :uti_mail, :uti_societe, :uti_agence, :uti_profil, :uti_contrat, :uti_fonction, :uti_responsable)";
		$req = $Conn->prepare($sql);
		$req ->bindParam(":uti_titre",$uti_titre);
		$req ->bindParam(":uti_nom",$uti_nom);
		$req ->bindParam(":uti_prenom",$uti_prenom);
		$req ->bindParam(":uti_ad1",$uti_ad1);
		$req ->bindParam(":uti_ad2",$uti_ad2);
		$req ->bindParam(":uti_ad3",$uti_ad3);
		$req ->bindParam(":uti_cp",$uti_cp);
		$req ->bindParam(":uti_ville",$uti_ville);
		$req ->bindParam(":uti_tel_perso",$uti_tel_perso);
		$req ->bindParam(":uti_mobile_perso",$uti_mobile_perso);
		$req ->bindParam(":uti_mail_perso",$uti_mail_perso);
		$req ->bindParam(":uti_tel",$uti_tel);
		$req ->bindParam(":uti_fax",$uti_fax);
		$req ->bindParam(":uti_mobile",$uti_mobile);
		$req ->bindParam(":uti_mail",$uti_mail);
		$req ->bindParam(":uti_societe",$uti_societe);
		$req ->bindParam(":uti_agence",$uti_agence);
		$req ->bindParam(":uti_profil",$uti_profil);
		$req ->bindParam(":uti_contrat",$uti_contrat);
		$req ->bindParam(":uti_fonction",$uti_fonction);
		$req ->bindParam(":uti_responsable",$uti_responsable);
		$req->execute();
		$utilisateur=$Conn->lastInsertId();
		
		return $utilisateur;
	}
	
	
	
	function insert_acces_societe($uso_utilisateur,$uso_societe,$uso_agence){
		
		// AUTORISE L'ACCES A UNE SOCIETE POUR UN UTILISATEUR
		
		global $Conn;
		
		$req = $Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur,uso_societe,uso_agence)
		VALUES (:uso_utilisateur, :uso_societe, :uso_agence)");
		$req->bindParam(':uso_utilisateur', $uso_utilisateur);
		$req->bindParam(':uso_societe', $uso_societe);
		$req->bindParam(':uso_agence', $uso_agence);
		$req->execute();
	
	}
	/*-------------------------------------------------
		FUNCTION DE MISE A JOUR
	---------------------------------------------------*/
		function update_utilisateur($uti_id,$uti_societe,$uti_agence,$uti_responsable,$uti_profil,$uti_titre,$uti_nom,$uti_prenom,$uti_ad1,$uti_ad2,$uti_ad3,$uti_cp,$uti_ville,$uti_tel,$uti_fax,$uti_mobile,$uti_mail,$uti_tel_perso,$uti_mobile_perso,$uti_mail_perso,$uti_matricule,$uti_archive,$uti_carte_affaire,$uti_tranche_km,$uti_veh_cv,$uti_population,$uti_contrat,$uti_charge,$uti_charge_dom,$uti_dist_dom,$uti_service,$uti_fonction){
			
			global $Conn;
			$sql="UPDATE Utilisateurs 
			SET uti_societe=:uti_societe,
			uti_agence=:uti_agence,
			uti_responsable=:uti_responsable,
			uti_profil=:uti_profil,
			uti_titre=:uti_titre,
			uti_nom=:uti_nom,
			uti_prenom=:uti_prenom, 
			uti_ad1=:uti_ad1,
			uti_ad2=:uti_ad2,
			uti_ad3=:uti_ad3,
			uti_cp=:uti_cp,
			uti_ville=:uti_ville,
			uti_tel=:uti_tel,
			uti_fax=:uti_fax,
			uti_mobile=:uti_mobile,
			uti_mail=:uti_mail,
			uti_tel_perso=:uti_tel_perso,
			uti_mobile_perso=:uti_mobile_perso,
			uti_mail_perso=:uti_mail_perso,
			uti_matricule=:uti_matricule,
			uti_archive=:uti_archive,
			uti_carte_affaire=:uti_carte_affaire,
			uti_tranche_km=:uti_tranche_km,
			uti_veh_cv=:uti_veh_cv, 
			uti_population=:uti_population,
			uti_contrat=:uti_contrat,
			uti_charge=:uti_charge,
			uti_charge_dom=:uti_charge_dom,
			uti_dist_dom=:uti_dist_dom,
			uti_service=:uti_service,
			uti_fonction=:uti_fonction
			WHERE uti_id=:uti_id";
			
			$req = $Conn->prepare($sql);
			
			$req->bindParam(":uti_societe", $uti_societe);
			$req->bindParam(":uti_agence", $uti_agence);
			$req->bindParam(":uti_responsable", $uti_responsable);
			$req->bindParam(":uti_profil", $uti_profil);
			$req->bindParam(":uti_titre", $uti_titre);
			$req->bindParam(":uti_nom", $uti_nom);
			$req->bindParam(":uti_prenom", $uti_prenom); 
			$req->bindParam(":uti_ad1", $uti_ad1);
			$req->bindParam(":uti_ad2", $uti_ad2);
			$req->bindParam(":uti_ad3", $uti_ad3);
			$req->bindParam(":uti_cp", $uti_cp);
			$req->bindParam(":uti_ville", $uti_ville);
			$req->bindParam(":uti_tel", $uti_tel);
			$req->bindParam(":uti_fax", $uti_fax);
			$req->bindParam(":uti_mobile", $uti_mobile);
			$req->bindParam(":uti_mail", $uti_mail);
			$req->bindParam(":uti_tel_perso", $uti_tel_perso);
			$req->bindParam(":uti_mobile_perso", $uti_mobile_perso);
			$req->bindParam(":uti_mail_perso", $uti_mail_perso);
			$req->bindParam(":uti_matricule", $uti_matricule);
			$req->bindParam(":uti_archive", $uti_archive);
			$req->bindParam(":uti_carte_affaire", $uti_carte_affaire);
			$req->bindParam(":uti_tranche_km", $uti_tranche_km);
			$req->bindParam(":uti_veh_cv", $uti_veh_cv); 
			$req->bindParam(":uti_population", $uti_population);
			$req->bindParam(":uti_contrat", $uti_contrat);
			$req->bindParam(":uti_charge", $uti_charge);
			$req->bindParam(":uti_charge_dom", $uti_charge_dom);
			$req->bindParam(":uti_dist_dom", $uti_dist_dom);
			$req->bindParam(":uti_service", $uti_service);
			$req->bindParam(":uti_fonction", $uti_fonction);
			$req->bindParam(":uti_id", $uti_id);
			$req->execute();
		}
		function update_utilisateur_ma_fiche($uti_id,$uti_ad1,$uti_ad2,$uti_ad3,$uti_cp,$uti_ville,$uti_tel,$uti_fax,$uti_mobile,$uti_mail,$uti_tel_perso,$uti_mobile_perso,$uti_mail_perso){
			
			global $Conn;
			$sql="UPDATE Utilisateurs 
			SET uti_ad1=:uti_ad1,
			uti_ad2=:uti_ad2,
			uti_ad3=:uti_ad3,
			uti_cp=:uti_cp,
			uti_ville=:uti_ville,
			uti_tel=:uti_tel,
			uti_fax=:uti_fax,
			uti_mobile=:uti_mobile,
			uti_mail=:uti_mail,
			uti_tel_perso=:uti_tel_perso,
			uti_mobile_perso=:uti_mobile_perso,
			uti_mail_perso=:uti_mail_perso		
			WHERE uti_id=:uti_id";
			
			$req = $Conn->prepare($sql);
			$req->bindParam(":uti_ad1", $uti_ad1);
			$req->bindParam(":uti_ad2", $uti_ad2);
			$req->bindParam(":uti_ad3", $uti_ad3);
			$req->bindParam(":uti_cp", $uti_cp);
			$req->bindParam(":uti_ville", $uti_ville);
			$req->bindParam(":uti_tel", $uti_tel);
			$req->bindParam(":uti_fax", $uti_fax);
			$req->bindParam(":uti_mobile", $uti_mobile);
			$req->bindParam(":uti_mail", $uti_mail);
			$req->bindParam(":uti_tel_perso", $uti_tel_perso);
			$req->bindParam(":uti_mobile_perso", $uti_mobile_perso);
			$req->bindParam(":uti_mail_perso", $uti_mail_perso);		
			$req->bindParam(":uti_id", $uti_id);
			$req->execute();
		}
		function update_utilisateur_compta($uti_id,$uti_titre,$uti_nom,$uti_prenom,$uti_ad1,$uti_ad2,$uti_ad3,$uti_cp,$uti_ville,$uti_tel,$uti_fax,$uti_mobile,$uti_mail,$uti_archive,$uti_carte_affaire,$uti_population){
			
			global $Conn;
			$sql="UPDATE Utilisateurs 
			SET uti_titre=:uti_titre,
			uti_nom=:uti_nom,
			uti_prenom=:uti_prenom, 
			uti_ad1=:uti_ad1,
			uti_ad2=:uti_ad2,
			uti_ad3=:uti_ad3,
			uti_cp=:uti_cp,
			uti_ville=:uti_ville,
			uti_tel=:uti_tel,
			uti_fax=:uti_fax,
			uti_mobile=:uti_mobile,
			uti_mail=:uti_mail,			
			uti_archive=:uti_archive,
			uti_carte_affaire=:uti_carte_affaire,		
			uti_population=:uti_population			
			WHERE uti_id=:uti_id";
			
			$req = $Conn->prepare($sql);
			$req->bindParam(":uti_titre", $uti_titre);
			$req->bindParam(":uti_nom", $uti_nom);
			$req->bindParam(":uti_prenom", $uti_prenom); 
			$req->bindParam(":uti_ad1", $uti_ad1);
			$req->bindParam(":uti_ad2", $uti_ad2);
			$req->bindParam(":uti_ad3", $uti_ad3);
			$req->bindParam(":uti_cp", $uti_cp);
			$req->bindParam(":uti_ville", $uti_ville);
			$req->bindParam(":uti_tel", $uti_tel);
			$req->bindParam(":uti_fax", $uti_fax);
			$req->bindParam(":uti_mobile", $uti_mobile);
			$req->bindParam(":uti_mail", $uti_mail);			
			$req->bindParam(":uti_archive", $uti_archive);
			$req->bindParam(":uti_carte_affaire", $uti_carte_affaire);		
			$req->bindParam(":uti_population", $uti_population);		
			$req->bindParam(":uti_id", $uti_id);
			$req->execute();
		}
		
	/*--------------------------------------------
		FONCTION DE SUPPRESION
	----------------------------------------------*/
		// supprime l'acces d'un utilisateur à toutes les sociétes et toutes les agences
		function delete_utilisateur_societes($utilisateur){
			
			global $Conn;
			
			$sql="DELETE FROM Utilisateurs_Societes WHERE uso_utilisateur=:utilisateur";
			$req = $Conn->prepare($sql);
			$req->bindParam(":utilisateur",$utilisateur);
			$req->execute();
		}
    ?>
