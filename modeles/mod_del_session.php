<?php
	function del_session($session,$date)
	{

		global $ConnSoc;
		
		// on verifie qu'il restera au moins un session pour cette date
		
		$sql="SELECT ase_id FROM Actions_Sessions WHERE ase_date=:date AND NOT ase_id=:session;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":date",$date);
		$req->bindParam(":session",$session);
		$req->execute();
		$result=$req->fetch();
		if(!empty($result)){
			// il reste au moins une session
			// on peut faire la supression
			
			echo("session : " . $session . "<br/>");
			echo("date : " . $date . "<br/>");
			
			$sql="DELETE FROM Actions_Sessions WHERE ase_id=:session;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":session",$session);
			$req->execute();
		}
    }
?>
