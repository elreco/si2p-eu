<?php

// suppression d'une correspondance et traitement associé
// retourne true en cas de résussite false en cas d'echec

function del_suspect_correspondance($correspondance){
	
	
	if(empty($correspondance)){	
		return false;
	}
	
	global $ConnSoc;
	
	// SUR LA CORRESPONDANCE
	
	$sql="SELECT sco_contact,sco_utilisateur,sco_date FROM Suspects_Correspondances WHERE sco_id=" . $correspondance . ";";
	$req=$ConnSoc->query($sql);
	$d_corresp=$req->fetch();
	if(empty($d_corresp)){
		return false;
	}

	if($d_corresp["sco_contact"]>0 AND $d_corresp["sco_utilisateur"]>0 AND !empty($d_corresp["sco_date"])){
		
		// on recherche s'il y a une correspondance plus récente	
		$sql="SELECT sco_id FROM Suspects_Correspondances WHERE sco_contact=" . $d_corresp["sco_contact"] . " AND sco_utilisateur=" . $d_corresp["sco_utilisateur"];
		$sql.=" AND sco_date>'" . $d_corresp["sco_date"] . "';";
		$req=$ConnSoc->query($sql);
		$d_recente=$req->fetch();
		if(empty($d_recente)){
			
			// on tente de supprimer la correspondance la plus récente => la précédente ne sera plus traité
			
			// on recherche la corresp précédente pour la noté non traitée
			$sql="SELECT sco_id,MAX(sco_date) FROM Suspects_Correspondances WHERE sco_contact=" . $d_corresp["sco_contact"] . " AND sco_utilisateur=" . $d_corresp["sco_utilisateur"];
			$sql.=" AND sco_date<'" . $d_corresp["sco_date"] . "';";
			$req=$ConnSoc->query($sql);
			$d_precedente=$req->fetch();
			if(!empty($d_precedente)){
				
				if($d_precedente["sco_id"]>0){
					$sql="UPDATE Suspects_Correspondances SET sco_rappel = 0 WHERE sco_id=" . $d_precedente["sco_id"] . ";";
					try{
						$req=$ConnSoc->query($sql);
					}catch (Exception $e) {
						echo 'Exception reçue : ',  $e->getMessage(), "\n";
						return false;
						//return "bug A" . $e->getMessage();
					}
				}
			}
		}
	}

	// suppression de la correspondance
	try {
		$req=$ConnSoc->query("DELETE FROM Suspects_Correspondances WHERE sco_id=" . $correspondance . " ;");
		$retour=array(
			"correspondance" => $correspondance,
			"non_traite" => 0
		);
		if(isset($d_precedente)){
			if(!empty($d_precedente["sco_id"])){
				$retour["non_traite"]=$d_precedente["sco_id"];
			}
		}
		return $retour;
	}
	catch( PDOException $Exception ) {
		$erreur=$Exception->getMessage();
		return false;
		//return "bug B" . $e->getMessage();
	}
}
?>