<?php

// RETOURNE LES COMMERCIAUX AUXQUELS L'UTILISATEUR A ACCES

// AUTEUR FG

function get_commerciaux($societe,$agence,$archive = ""){

	$Conn=connexion_fct($societe);
	
	$sql="SELECT com_id,com_label_1,com_label_2, com_ref_1 FROM Commerciaux";
	
	$mil="";
	if($agence>0){
		$mil.=" AND com_agence=:agence";
	}
	if($archive==="1"){
		$mil.=" AND com_archive = 1";
	}elseif($archive==="0"){
		$mil.=" AND com_archive=0 OR com_archive IS NULL";
	}
	if(!$_SESSION["acces"]["acc_droits"][6]){
		$mil.=" AND com_ref_1=:utilisateur";		
	}
	if(!empty($mil)){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);	
	}
	$sql.=" ORDER BY com_label_1,com_label_2";
	$req = $Conn->prepare($sql);
	if($agence>0){
		$req->bindParam(":agence",$agence);
	}
	if(!$_SESSION["acces"]["acc_droits"][6]){
		$req->bindParam(":utilisateur",$_SESSION["acces"]["acc_ref_id"]);
	}
	$req->execute();
	$commerciaux = $req->fetchAll();

    return $commerciaux;
}
