<?php 

function convention_data($convention_id){

	global $ConnSoc;
	global $Conn;
	
	$erreur=false;
	$data=array();
	
	if(!empty($convention_id)){
	
		$sql="SELECT con_numero,con_cli_nom,con_cli_service,con_cli_ad1,con_cli_ad2,con_cli_ad3,con_cli_cp,con_cli_ville,con_societe,con_agence,DATE_FORMAT(con_date,'%d/%m/%Y') AS con_date 
		,con_action_client,con_client,con_form_dates,con_form_duree,con_form_effectif,con_form_effectif_text,con_unite,con_unite_txt,con_qte,con_ht,con_tva_id,con_tva_periode,con_tva_taux,con_ttc
		,con_action,con_comment,con_ht,con_cli_adresse FROM Conventions WHERE con_id=" . $convention_id . ";";
		$req=$ConnSoc->query($sql);
		$data=$req->fetch(PDO::FETCH_ASSOC);
		if(!empty($data)){
			
			$data["adresse_client"]=$data["con_cli_nom"];
			if(!empty($data["con_cli_service"])){
				$data["adresse_client"].="<br/>" . $data["con_cli_service"];
			}
			if(!empty($data["con_cli_ad1"])){
				$data["adresse_client"].="<br/>" . $data["con_cli_ad1"];
			}
			if(!empty($data["con_cli_ad2"])){
				$data["adresse_client"].="<br/>" . $data["con_cli_ad2"];
			}
			if(!empty($data["con_cli_ad3"])){
				$data["adresse_client"].="<br/>" . $data["con_cli_ad3"];
			}
			$data["adresse_client"].="<br/>" . $data["con_cli_cp"] . " " . $data["con_cli_ville"];
			
			$data["con_tva"]=$data["con_ttc"]-$data["con_ht"];

			$data["liste_stagiaires"]=array();
			
		}else{
			$erreur=true;
		}
	}
	
	if(empty($erreur)){
		
		$data["juridique"]=get_juridique($data["con_societe"],$data["con_agence"]);
		
		
		// DONNEE LU SUR ACTION CLIENT
		
		$sql="SELECT acl_pro_libelle FROM Actions_Clients WHERE acl_id=" . $data["con_action_client"] . ";";
		$req=$ConnSoc->query($sql);
		$d_action_client=$req->fetch(PDO::FETCH_ASSOC);
		if(!empty($d_action_client)){
			$data["libelle"]=$d_action_client["acl_pro_libelle"];
		}
		
		// DONNEE LU SUR ACTION
		
		$sql="SELECT act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville FROM Actions WHERE act_id=" . $data["con_action"] . ";";
		$req=$ConnSoc->query($sql);
		$d_action=$req->fetch(PDO::FETCH_ASSOC);
		if(!empty($d_action)){
			$data["lieu"]=$d_action["act_adr_nom"];
			if(!empty($d_action["act_adr_service"])){
				$data["lieu"].="<br/>" . $d_action["act_adr_service"];
			}
			if(!empty($d_action["act_adr1"])){
				$data["lieu"].="<br/>" . $d_action["act_adr1"];
			}
			if(!empty($d_action["act_adr2"])){
				$data["lieu"].="<br/>" . $d_action["act_adr2"];
			}
			if(!empty($d_action["act_adr3"])){
				$data["lieu"].="<br/>" . $d_action["act_adr3"];
			}
			$data["lieu"].="<br/>" . $d_action["act_adr_cp"] . " " . $d_action["act_adr_ville"];
		}
		
		// LISTE DES STAGIAIRES
		
		$sql="SELECT ast_stagiaire FROM Actions_Stagiaires WHERE ast_action_client=" . $data["con_action_client"] . ";";
		$req=$ConnSoc->query($sql);
		$d_sta_action=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($d_sta_action)){
			$tab_sta=array_column ($d_sta_action,"ast_stagiaire");
			$liste_sta=implode($tab_sta,",");
		}
		if(!empty($liste_sta)){
			$sql="SELECT sta_id,sta_nom,sta_prenom,sta_naissance FROM Stagiaires WHERE sta_id IN (" . $liste_sta . ") ORDER BY sta_nom,sta_prenom;";
			$req=$Conn->query($sql);
			$result_sta=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($result_sta)){
				$data["liste_stagiaires"]=$result_sta;
			}
		}
		
		
		
	} 
	if(!empty($erreur)){
		return false;	
	}else{
		return $data;
	}
}?>
