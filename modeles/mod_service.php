<?php

	// MODELE

	function get_service_select($valeur_select){

		global $Conn;

		$service_select="";
		$req=$Conn->query("SELECT ser_id,ser_libelle  FROM services ORDER BY ser_libelle");
		while ($donnees = $req->fetch())
		{	if($valeur_select==$donnees["ser_id"]){
				$service_select=$service_select . "<option value='" . $donnees["ser_id"] . "' selected >" . $donnees["ser_libelle"] . "</option>";
			}else{
				$service_select=$service_select . "<option value='" . $donnees["ser_id"] . "' >" . $donnees["ser_libelle"] . "</option>";	
			}
			
		};
		return $service_select;

	}
	
	function get_service($service){

		global $Conn;

		$req=$Conn->prepare("SELECT ser_libelle FROM Services WHERE ser_id=:service");
		$req->bindParam(':service', $service);
		$req->execute();
		$get_service = $req->fetch();
		return $get_service;

	}
	function get_services(){

		global $Conn;

		$req=$Conn->query("SELECT ser_id,ser_libelle FROM Services ORDER BY ser_libelle");
		$get_service = $req->fetchAll();
		return $get_service;

	}

	function insert_service($ser_libelle){

		global $Conn;
		$req = $Conn->prepare("INSERT INTO services (ser_libelle) VALUES (:ser_libelle)");
		$req->bindParam(':ser_libelle', $ser_libelle);
		$req->execute();

	}

	function update_service($ser_id,$ser_libelle){

		global $Conn;
		$req = $Conn->prepare("UPDATE services SET ser_libelle=:ser_libelle WHERE ser_id=:ser_id");
		$req->bindParam(':ser_id', $ser_id);
		$req->bindParam(':ser_libelle', $ser_libelle);
		$req->execute();

	}

?>
