<?php

// GENERE FEUILLE DE PRESENCE CONSO

require_once("../vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;

function model_presence_conso_pdf($data){
	
	global $acc_societe;
	
	$erreur="";
	
	ob_start(); ?>

	<style type="text/css" >
		body{
			font-family:arial;
			font-size:8pt;
			
		}
		table{
			width:100%;
			border-collapse: collapse;
			border:1px solid red;
		}
		.tab_border{
			border-collapse: collapse;
			table-layout: fixed; 
		}
		.tab_border th{
			background-color:#FFF;
			color:#333;
			text-align:center;
			border-collapse: collapse;
			border-left:1px solid #4d4d4d;
			border-right:1px solid #4d4d4d;
			
		}
		.tab_border td{
			border:1px solid #4d4d4d;
			border-collapse:collapse;
		}
		.footer{
			font-size:7pt;
			text-align:center;
		}
		.footer-bar{
			color:red;
		}
		.w12{
			width:36mm;
		}
		.w25{
			width:72mm;
		}
		.w50{
			width:144mm;
		}
		.tab-head{
			height:9mm;
		}
		.tab-ligne{
			height:15mm;
		}
		.text-center{
			text-align:center;
		}
		.logo{
			max-width:100%;
		}
	</style>
	
<?php
	foreach($data["pages"] as $page){ 
	
		// calcul du nombre de page pour afficher toutes les dates du formateur
		/*$bcl_dates=ceil(count($dates)/5);
		
		$cle_date=0; 
		
		for($i=1;$i<=$bcl_dates;$i++){ 
		
			$col_date=array();
			for($bk=0;$bk<=4;$bk++){ 
				if(!empty($dates[$cle_date+$bk])){
					$col_date[$bk]=$dates[$cle_date+$bk];
				}else{
					$col_date[$bk]="";
				}								
			} 
			$cle_date=$cle_date+5; */
			?>
	
		
		<page backtop="65mm" backleft="0mm" backright="0mm" backbottom="15mm" >
			<page_header> 
				<div style="height:26mm;" >
					<table>
						<tr>
							<td class="w25" >
					<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
									<img src="../<?=$data["juridique"]["logo_1"]?>" class="logo" />
					<?php		} ?>
							</td>
							<td class="w50" ><h1 class="text-center" >FEUILLE D'EMARGEMENT</h1></td>
							<td class="text-right w25" >
						<?php	echo($data["juridique"]["nom"]);
								if(!empty($data["juridique"]["agence"])){
									echo("<br/>".$data["juridique"]["agence"]);
								}
								if(!empty($data["juridique"]["ad1"])){
									echo("<br/>".$data["juridique"]["ad1"]);
								}
								if(!empty($data["juridique"]["ad2"])){
									echo("<br/>".$data["juridique"]["ad2"]);
								}
								if(!empty($data["juridique"]["ad3"])){
									echo("<br/>".$data["juridique"]["ad3"]);
								}
								echo("<br/>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"]); ?>
							</td>
						</tr>
					</table>
				</div>
				<table>
					<tr>
						<td class="w25" >
							Client : <strong><?=$page["cli_nom"]?></strong>
						</td>
						<td class="w50" >
							Formation : <strong><?=$page["acl_pro_libelle"]?></strong>
						</td>
						<td class="w25" >
							Durée : <strong><?=$page["duree"]?></strong>
						</td>						
					</tr>
				</table>
				
				<table class="tab_border" >
					<tr>
						<th class="tab-head" style='width:70mm;border-top:1px solid #4d4d4d;border-bottom:1px solid #4d4d4d;' >Formateur (1)</th>
						<th class="" style='width:34mm;border-top:1px solid #4d4d4d;border-bottom:1px solid #4d4d4d;' >&nbsp;</th>
	<?php				$col=0;
						for($col=0;$col<=4;$col++){ 
							if(!empty($page["sessions"][$col])){								
								echo("<th style='width:34mm;border-top:1px solid #4d4d4d;border-bottom:1px solid #4d4d4d;' >" . $page["sessions"][$col] . "</th>");
							}else{
								echo("<th style='width:34mm;border-top:1px solid #4d4d4d;border-bottom:1px solid #4d4d4d;' >&nbsp;</th>");	
							}								
						} ?>
					</tr>
					<tr>
						<td class="tab-ligne" style='width:70mm;padding-left:1mm;'  ><?=$page["int_identite"]?></td>
						<td style='width:34mm;' >&nbsp;</td>
	<?php				for($col=0;$col<=4;$col++){ 
							echo("<td style='width:34mm;' >&nbsp;</td>");							
						} ?>
					</tr>
					<tr>
						<th style="width:70mm;padding-bottom:1mm;" class="tab-head"  >Stagiaires (2)</th>
						<th style="width:34mm;padding-bottom:1mm;" >Date de naiss.</th>
	<?php				for($col=0;$col<=4;$col++){ 
							if(!empty($col_date[$col])){
								echo("<th style='width:34mm;padding-bottom:1mm;' >" . $page["sessions"][$col] . "</th>");
							}else{
								echo("<th style='width:34mm;padding-bottom:1mm;' >&nbsp;</th>");	
							}								
						} ?>
					</tr>
			<?php	if(!empty($page["stagiaires"])){
						foreach($page["stagiaires"] as $sta){ ?>
							<tr>
								<td style="width:70mm;padding-left:1mm;" class="tab-ligne" ><?=$sta["sta_prenom"] . " " . $sta["sta_nom"]?></td>
								<td style="width:34mm;text-align:center;" ><?=$sta["sta_naissance"]?></td>								
			<?php				for($col=0;$col<=4;$col++){ 
									if(!empty($sta[$col])){
										echo("<td style='width:34mm;padding-bottom:1mm;' >" . $sta[$col] . "</td>");
									}else{
										echo("<td style='width:34mm;padding-bottom:1mm;' >&nbsp;</td>");	
									}								
								} ?>
							</tr>
			<?php		}
					} ?>
				</table>
				
			</page_header> 

			<page_footer>
				
				<p class="footer" >(1) - Par ma signature, j'atteste avoir reçu la formation ci-dessus référencée et il m'a été remis en main propre une attestation de formation. (2) - Par ma signature, j'atteste avoir dispensé la formation ci-dessous.</p>
				<hr/>
				<p class="footer">
					<?=$data["juridique"]["nom"]?> <span class="footer-bar" >|</span>
			<?php	if(!empty($data["juridique"]["agence"])){ ?>
						<?=$data["juridique"]["agence"]?> <span class="footer-bar" >|</span>
			<?php	} ?>
					<?=$data["juridique"]["ad1"] . " " .$data["juridique"]["ad2"] . " " . $data["juridique"]["ad3"]?> <span class="footer-bar" >|</span>
					<?=$data["juridique"]["cp"] . " " . $data["juridique"]["ville"]?> <span class="footer-bar" >|</span>
					TEL. <?=$data["juridique"]["tel"]?> <span class="footer-bar" >|</span>
					FAX. <?=$data["juridique"]["fax"]?>
				</p>
			</page_footer>
			<table class="tab_border" >
<?php		/*	
				foreach($stagiaires as $sta){?>
					<tr>
						<td style="width:70mm;padding-left:1mm;" class="tab-ligne" ><?=$sta["nom"] . " " . $sta["prenom"]?></td>
						<td style="width:34mm;text-align:center;" ><?=convert_date_txt($sta["naissance"])?></td>
		<?php			for($col=0;$col<=4;$col++){ 
							if(!empty($col_date[$col])){ 
								if(file_exists("documents/Presences/signatures/signature_" . $d_formation["for_id"] . "_" . $int["id"] . "_" . $sta["id"] . ".png")){
								
									$L=rand(28,34); 
									$va=rand(0,2); 
									switch($va){
										case 0:
											$Val_va="top";
											$Val_ta="left";
											break;
										case 1:
											$Val_va="middle";
											$Val_ta="center";
											break;
										case 2:
											$Val_va="bottom";
											$Val_ta="right";
											break;
									} ?>
									<td style="width:34mm;text-align:center;vertical-align:<?=$Val_va?>;" >
										<img style="width:<?=$L?>mm;"  src="documents/Presences/signatures/signature_<?=$d_formation["for_id"] . "_" . $int["id"] . "_" . $sta["id"] . ".png"?>" />
									</td>
				<?php			}else{ ?>	
									<td style="width:34mm;" >&nbsp;</td>
				<?php			}
							}else{ ?>
								<td style="width:34mm;" >&nbsp;</td>									
			<?php			}								
						} ?>
					</tr>
	<?php		}*/	?>
			</table>
			
		</page>
<?php	//	} 
	} 					

	$content=ob_get_clean();
	
	try{
		$pdf=new HTML2PDF('L','A4','fr');
		$pdf->pdf->SetDisplayMode('fullwidth');
		/*echo($content);
		die();*/
		$pdf->writeHTML($content);
		if($pdf){
			$pdf->Output($_SERVER["DOCUMENT_ROOT"] . '/Documents/Societes/' . $acc_societe . '/Presences/' . $data["pdf"] . '.pdf', 'F');					
		}
	}catch(HTML2PDF_exception $e){
		$erreur="Erreur lors de la génération du fichier PDF";
	}
	
	if(!empty($erreur)){
		return false;
	}else{
		return true;
	}
}
?>