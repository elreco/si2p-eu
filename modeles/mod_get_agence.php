<?php
	function get_agence($id)
	{

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM agences WHERE age_id=:age_id");
		$req->bindParam(':age_id', $id);
		$req->execute();

		$agence = $req->fetch();

		return $agence;
    }
?>
