<?php
// creation ou maj d'un contact

// $d_contact => array avec clé = nom champ base

// retourne false en cas d'échec et ID au format INT du contact en cas de réussite

function set_suspect_contact($d_contact){
	
	global $ConnSoc;
	
	if(!empty($d_contact)){
		
		$criteres=array();
		
		if(!empty($d_contact["sco_id"])){
			
			// MAJ
			
			$sql="UPDATE suspects_contacts SET ";
			if(isset($d_contact["sco_fonction"])){
				if(!empty($d_contact["sco_fonction"])){
					$sql.="sco_fonction=:sco_fonction,
					sco_fonction_nom=''";	
					$criteres["sco_fonction"]=$d_contact["sco_fonction"];
				}else{
					$sql.="sco_fonction=0,
					sco_fonction_nom=:sco_fonction_nom";	
					$criteres["sco_fonction_nom"]=$d_contact["sco_fonction_nom"];
				}
			}
			if(isset($d_contact["sco_titre"])){
				if(!empty($d_contact["sco_titre"])){
					$sql.="sco_titre=:sco_titre,";	
					$criteres["sco_titre"]=$d_contact["sco_titre"];
				}else{
					$sql.="sco_titre=0,";	
				}
			}
			if(isset($d_contact["sco_nom"])){
				$sql.="sco_nom=:sco_nom,";	
				$criteres["sco_nom"]=$d_contact["sco_nom"];
			}
			if(isset($d_contact["sco_prenom"])){
				$sql.="sco_prenom=:sco_prenom,";	
				$criteres["sco_prenom"]=$d_contact["sco_prenom"];
			}
			if(isset($d_contact["sco_tel"])){
				$sql.="sco_tel=:sco_tel,";	
				$criteres["sco_tel"]=$d_contact["sco_tel"];
			}
			if(isset($d_contact["sco_portable"])){
				$sql.="sco_portable=:sco_portable,";	
				$criteres["sco_portable"]=$d_contact["sco_portable"];
			}
			if(isset($d_contact["sco_fax"])){
				$sql.="sco_fax=:sco_fax,";	
				$criteres["sco_fax"]=$d_contact["sco_fax"];
			}
			if(isset($d_contact["sco_mail"])){
				$sql.="sco_mail=:sco_mail,";	
				$criteres["sco_mail"]=$d_contact["sco_mail"];
			}
			if(isset($d_contact["sco_defaut"])){
				if(!empty($d_contact["sco_defaut"])){
					$sql.="sco_defaut=1,";	
				}else{
					$sql.="sco_defaut=0,";	
				}
			}
			if(isset($d_contact["sco_compta"])){
				if(!empty($d_contact["sco_compta"])){
					$sql.="sco_compta=1,";	
				}else{
					$sql.="sco_compta=0,";	
				}
			}
			if(isset($d_contact["sco_comment"])){
				$sql.="sco_comment=:sco_comment,";	
				$criteres["sco_comment"]=$d_contact["sco_comment"];
			}
			if(!empty($criteres)){
				$sql=substr($sql,0,-1); 			
				$sql.=" WHERE sco_id=:contact;";
				$req = $ConnSoc->prepare($sql);
				$req->bindParam("contact",$d_contact["sco_id"]);
				foreach($criteres as $c => $u){
					$req->bindValue($c,$u);
				}						
				try {
					$req->execute();
					return intval($d_contact["sco_id"]);
				}catch (Exception $e) {
					return false;
				}
			}else{
				return false;
			}
			
		}else{
			
			// ADD
			
			$sco_ref_id=0;
			if(!empty($d_contact["sco_ref_id"])){
				$sco_ref_id=intval($d_contact["sco_ref_id"]);
			}

			if($sco_ref_id>0 AND !empty($d_contact["sco_nom"])){
				
				$sql="INSERT INTO Suspects_Contacts (sco_ref,sco_ref_id";
				$valeurs="1,:sco_ref_id";
				
				if(isset($d_contact["sco_fonction"])){
					if(!empty($d_contact["sco_fonction"])){
						$sql.=",sco_fonction,sco_fonction_nom";
						$valeurs.=",:sco_fonction,''";							
						$criteres["sco_fonction"]=$d_contact["sco_fonction"];
					}else{
						$sql.=",sco_fonction,sco_fonction_nom";
						$valeurs.=",0,:sco_fonction_nom";							
						$criteres["sco_fonction_nom"]=$d_contact["sco_fonction_nom"];
					}
				}
				if(isset($d_contact["sco_titre"])){
					if(!empty($d_contact["sco_titre"])){
						$sql.=",sco_titre";	
						$valeurs.=",:sco_titre";	
						$criteres["sco_titre"]=$d_contact["sco_titre"];
					}
				}
				if(isset($d_contact["sco_nom"])){
					$sql.=",sco_nom";	
					$valeurs.=",:sco_nom";	
					$criteres["sco_nom"]=$d_contact["sco_nom"];
				}
				if(isset($d_contact["sco_prenom"])){
					$sql.=",sco_prenom";
					$valeurs.=",:sco_prenom";						
					$criteres["sco_prenom"]=$d_contact["sco_prenom"];
				}
				if(isset($d_contact["sco_tel"])){
					$sql.=",sco_tel";
					$valeurs.=",:sco_tel";						
					$criteres["sco_tel"]=$d_contact["sco_tel"];
				}
				if(isset($d_contact["sco_portable"])){
					$sql.=",sco_portable";
					$valeurs.=",:sco_portable";						
					$criteres["sco_portable"]=$d_contact["sco_portable"];
				}
				if(isset($d_contact["sco_fax"])){
					$sql.=",sco_fax";
					$valeurs.=",:sco_fax";						
					$criteres["sco_fax"]=$d_contact["sco_fax"];
				}
				if(isset($d_contact["sco_mail"])){
					$sql.=",sco_mail";
					$valeurs.=",:sco_mail";						
					$criteres["sco_mail"]=$d_contact["sco_mail"];
				}
				if(isset($d_contact["sco_defaut"])){
					if(!empty($d_contact["sco_defaut"])){
						$sql.=",sco_defaut";
						$valeurs.=",1";	
					}else{
						$sql.=",sco_defaut";
						$valeurs.=",0";	
					}
				}
				if(isset($d_contact["sco_compta"])){
					if(!empty($d_contact["sco_compta"])){
						$sql.=",sco_compta";
						$valeurs.=",1";	
					}else{
						$sql.=",sco_compta";
						$valeurs.=",0";	
					}
				}
				if(isset($d_contact["sco_comment"])){
					$sql.=",sco_comment";	
					$valeurs.=",:sco_comment";	
					$criteres["sco_comment"]=$d_contact["sco_comment"];
				}
				$sql.=" ) VALUES (" . $valeurs . ");";
				$req = $ConnSoc->prepare($sql);
				$req->bindParam(":sco_ref_id",$sco_ref_id);
				foreach($criteres as $c => $u){
					$req->bindValue($c,$u);
				}
				
				try{
					$req->execute();
					return intval($ConnSoc->lastInsertId());
				}catch (Exception $e) {
					return false;
				}
			}else{
				return false;
			}
			
		}
	}
}