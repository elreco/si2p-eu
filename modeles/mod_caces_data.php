<?php


setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


function caces_data($dossiers){
	
	global $acc_societe;
	global $acc_agence;
	global $acc_utilisateur;
	global $Conn;
	
	// INFO RECO
	
	$caces_reco=array(
		"R400" => array(
			"7" => array(
				"code" => "R489",
				"nom" => "Chariots automoteurs à conducteur porté",
				"mention_num" => "",
				"mention_text" => ""
			),
			"8" => array(
				"code" => "R482",
				"nom" => "Engins de chantier",
				"mention_num" => "",
				"mention_text" => "",
			),
			"9" => array(
				"code" => "R486",
				"nom" => "Plates formes élévatrices mobiles de personnes",
				"mention_num" => "",
				"mention_text" => ""
			),
			"10" => array(
				"code" => "R490",
				"nom" => "Grues de chargement",
				"mention_num" => "",
				"mention_text" => "Une autorisation de conduite peut être délivrée durant les 5 années qui suivent l'échéance, sous réserve du respect des exigences définies au 3 / 3 / 5 de la recommandation R.490"
			),
			"12" => array(
				"code" => "R485",
				"nom" => "Gerbeurs à conducteur accompagnant",
				"mention_num" => "",
				"mention_text" => ""
			),
			"13" => array(
				"code" => "R484",
				"nom" => "Ponts roulants et Portiques",
				"mention_num" => "",
				"mention_text" => ""
			),
			"18" => array(
				"code" => "R483",
				"nom" => "Grues Mobiles",
				"mention_num" => "",
				"mention_text" => ""
			)
		),
		"R300" => array(
			"7" => array(
				"code" => "389",
				"nom" => "Chariots automoteurs à conducteur porté",
				"mention_num" => "",
				"mention_text" => "Ni la recommandation ni le FAQ ne prévoient de mention particulière ou d'option pour cette famille."
			),
			"8" => array(
				"code" => "372m",
				"nom" => "Engins de chantier",
				"mention_num" => "(1,2)",
				"mention_text" => "(1) 'Porte-engin NON': le CACES® ne permet pas le chargement / déchargement sur porte-engin (cf. question 83 du FAQ)<br/>(2) 'Télécommande OUI': le CACES® permet la conduite d'engins télécommandés de la catégorie correspondantes (cf. question 23 du FAQ)",
			),
			"9" => array(
				"code" => "386",
				"nom" => "Plates formes élévatrices mobiles de personnes",
				"mention_num" => "",
				"mention_text" => "Ni la recommandation ni le FAQ ne prévoient de mention particulière ou d'option pour cette famille."
			),
			"10" => array(
				"code" => "390",
				"nom" => "Grue auxiliaire",
				"mention_num" => "",
				"mention_text" => "Ni la recommandation ni le FAQ ne prévoient de mention particulière ou d'option pour cette famille."
			),
			"18" => array(
				"code" => "383",
				"nom" => "Grues Mobiles",
				"mention_num" => "",
				"mention_text" => "Ni la recommandation ni le FAQ ne prévoient de mention particulière ou d'option pour cette famille."
			)
		),
		"30" => array(	// CAUSPR spécifique pour ACB non concerne par R300 -> R400
			"16" => array(
				"code" => "30",
				"nom" => "Ponts roulants, portiques, semi-portiques",
				"mention_num" => "",
				"mention_text" => ""
			)
		)		
	);
	
	// INFO SOCIETE
	$d_societes=array(
		"0" => array()
	);
	$sql="SELECT soc_id,soc_nom,soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville,soc_inrs,soc_logo_1,soc_logo_2,soc_tel,soc_fax,soc_mail,soc_site
	,uti_nom,uti_prenom,uti_titre,uti_fonction,soc_responsable
	FROM Societes,Utilisateurs WHERE soc_responsable=uti_id AND soc_caces>0;";
	$req=$Conn->query($sql);
	$d_r_soc=$req->fetchAll();
	if(!empty($d_r_soc)){
		foreach($d_r_soc as $r){
			
			$soc_resp_identite="";
			if($r["uti_titre"]==1){
				$soc_resp_identite="M.";
			}elseif($r["uti_titre"]==2){
				$soc_resp_identite="Mme";
			}
			if(!empty($r["uti_nom"])){
				$soc_resp_identite.=" " . $r["uti_nom"];
			}
			if(!empty($r["uti_prenom"])){
				$soc_resp_identite.=" " . $r["uti_prenom"];
			}
			
			$d_societes[$r["soc_id"]]=array(
				"soc_nom" => $r["soc_nom"],
				"soc_ad1" => $r["soc_ad1"],
				"soc_ad2" => $r["soc_ad2"],
				"soc_ad3" => $r["soc_ad3"],
				"soc_cp" => $r["soc_cp"],
				"soc_ville" => $r["soc_ville"],
				"soc_tel" => $r["soc_tel"],
				"soc_fax" => $r["soc_fax"],
				"soc_mail" => $r["soc_mail"],
				"soc_site" => $r["soc_site"],
				"soc_inrs" => $r["soc_inrs"],
				"soc_logo_1" => $r["soc_logo_1"],
				"soc_logo_2" => $r["soc_logo_2"],
				"soc_responsable" => $r["soc_responsable"],
				"soc_resp_identite" => $soc_resp_identite,
				"soc_resp_fonction" => $r["uti_fonction"]
				
			);
			
		}
	}
	
	// REQUETE PREP
	
	$sql_cat="SELECT qca_libelle,qca_nom
	,qua_opt_1,qua_opt_2,qua_opt_3
	,dcc_numero,dcc_date,DATE_FORMAT(dcc_date,'%d/%m/%Y') AS dcc_date_fr,dcc_testeur_identite,dcc_opt_1_valide,dcc_opt_2_valide,dcc_opt_3_valide
	FROM Qualifications_Categories INNER JOIN Qualifications ON (Qualifications_Categories.qca_qualification=Qualifications.qua_id) 
	LEFT JOIN Diplomes_Caces_Cat ON (Qualifications_Categories.qca_id=Diplomes_Caces_Cat.dcc_categorie AND dcc_diplome=:diplome)
	WHERE qua_id=:qualification AND qca_archive=:qca_archive ORDER BY qca_libelle";
	$req_cat=$Conn->prepare($sql_cat);
	
	// ON RECUPERE LES DOSSIERS SELECTIONNES
	
	$data=array();
	$dossiers_liste=implode(",",$dossiers);

	$sql="SELECT dca_id,dca_qualification,dca_sta_titre,dca_sta_nom,dca_sta_prenom,dca_sta_naissance,
	DATE_FORMAT(dca_date_validite,'%d/%m/%Y') AS dca_date_validite_fr,DATE_FORMAT(dca_aipr_date,'%d/%m/%Y') AS dca_aipr_date_fr
	,dca_action_soc_1,dca_action_soc_2,dca_action_soc_3,dca_action_soc_4,dca_action_soc_5,dca_action_soc_6
	,dca_date,DATE_FORMAT(dca_date,'%d/%m/%Y') AS dca_date_fr,dca_date,dca_stagiaire
	FROM Diplomes_Caces WHERE dca_id IN (" . $dossiers_liste . ") ORDER BY dca_sta_nom,dca_sta_prenom";
	$req=$Conn->query($sql);
	$d_caces=$req->fetchAll();
	if(!empty($d_caces)){
		foreach($d_caces as $caces){
			
			// si on imprime le CACES c'est qu'il est valide donc dca_date not null
			
			if(!empty($caces["dca_date"])){
				
				
				
				$Dt_diplome=DateTime::createFromFormat('Y-m-d',$caces["dca_date"]);

				$reco="R300";
				$qualif_cat_archive=1;
				if($caces["dca_qualification"]==16){
					$reco="30";
					$qualif_cat_archive=0;
				}elseif($Dt_diplome->format("Y")>="2020"){
					$reco="R400";
					$qualif_cat_archive=0;
				}
				
			
				if(!empty($caces_reco[$reco][$caces["dca_qualification"]])){
				
					$sta_identite="";
					if($caces["dca_sta_titre"]==1){
						$sta_identite="M.";
					}elseif($caces["dca_sta_titre"]==2){
						$sta_identite="Mme";
					}
					if(!empty($caces["dca_sta_nom"])){
						$sta_identite.=" " . $caces["dca_sta_nom"];
					}
					if(!empty($caces["dca_sta_prenom"])){
						$sta_identite.=" " . $caces["dca_sta_prenom"];
					}
					
					$dca_sta_naissance="";
					if(!empty($caces["dca_sta_naissance"])){
						$DT_periode=date_create_from_format('Y-m-d',$caces["dca_sta_naissance"]);
						if(!is_bool($DT_periode)){
							$dca_sta_naissance=utf8_encode(strftime("%d %B %Y",$DT_periode->getTimestamp()));
						}
					}
					
					// LA DERNIERE SOCIETE QUI EST INTERVENU SUR LE CACES
					
					$diplome_soc=0;
					for($bcl=6;$bcl>=1;$bcl--){
						if(!empty($caces["dca_action_soc_" . $bcl])){
							if(isset($d_societes[$caces["dca_action_soc_" . $bcl]])){
								$diplome_soc=$caces["dca_action_soc_" . $bcl];
							}else{
								$erreur_txt="Dossier N° " . $caces["dca_id"] . " : erreur de paramétrage!";
							}
							break;
						}
					}
					$donnee_soc=$d_societes[$diplome_soc];
					if($diplome_soc==17 AND $reco!="R400"){
						$donnee_soc["soc_inrs"]="FR034915-2";
					}elseif($diplome_soc==19 AND $reco!="R400"){
						$donnee_soc["soc_inrs"]="ICSCAC0218";
					}
					
					if(empty($erreur_txt)){
						
						// LES CATEGORIES
						$req_cat->bindParam("diplome",$caces["dca_id"]);
						$req_cat->bindParam("qualification",$caces["dca_qualification"]);
						$req_cat->bindParam("qca_archive",$qualif_cat_archive);
						$req_cat->execute();
						$result_cat=$req_cat->fetchAll(PDO::FETCH_ASSOC);
						if($caces["dca_qualification"]==10 AND $reco=="R300"){
							// grue aux R300 -> grue chargement R400
							// l'option Poste Fixe a été supprimé. On force l'affichage pour continuité print R300
							if(!empty($result_cat)){
								foreach($result_cat as $cle_cat => $cat){
									$result_cat[$cle_cat]["qua_opt_2"]="Poste fixe";
								}
							}
						}elseif($caces["dca_qualification"]==18 AND $reco=="R300"){
							// Grue mobile
							// n'avait pas d'option en R300
							if(!empty($result_cat)){
								foreach($result_cat as $cle_cat => $cat){
									$result_cat[$cle_cat]["qua_opt_1"]="";
									$result_cat[$cle_cat]["qua_opt_2"]="";
								}
							}
						}			
							
						$data[]=array(
							"sta_id" => $caces["dca_stagiaire"],
							"sta_identite" => trim($sta_identite),
							"sta_naissance_txt" => $dca_sta_naissance,
							"categories" => $result_cat,
							"reco" => $reco,
							"reco_code" => $caces_reco[$reco][$caces["dca_qualification"]]["code"],
							"reco_nom" => $caces_reco[$reco][$caces["dca_qualification"]]["nom"],
							"reco_mention" => $caces_reco[$reco][$caces["dca_qualification"]]["mention_num"],
							"reco_mention_txt" => $caces_reco[$reco][$caces["dca_qualification"]]["mention_text"],
							"validite" => $caces["dca_date_validite_fr"],
							"societe" => $donnee_soc,
							"aipr" => $caces["dca_aipr_date_fr"],
							"obtention" => $caces["dca_date"],
							"obtention_fr" => $caces["dca_date_fr"],
							"qualification" => $caces["dca_qualification"]
						);
					}
				}
			}
		}
	}
	
	if(empty($erreur_txt)){
		return $data;
	}else{
		return $erreur_txt;
	}
}
?>