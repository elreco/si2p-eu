 <?php

	function planning_periode_lib($date_deb,$demi_deb,$date_fin,$demi_fin){

		$dtime = DateTime::createFromFormat("Y-m-d",$date_deb);

		$lib=$dtime->format("d/m/Y");
		if($date_deb==$date_fin){
			$periode="Le  " . $lib;
			if($demi_deb==$demi_fin){
				if($demi_fin==2){
					$periode.=" après-midi";
				}else{
					$periode.=" matin";
				}
			}
		}else{

			$periode="Du ";

			$periode.=$lib;
			if($demi_deb==2){
				$periode.=" après-midi";
			}
			$periode.=" au ";

			$dtime = DateTime::createFromFormat("Y-m-d",$date_fin);
			$lib=$dtime->format("d/m/Y");
			$periode.=$lib;
			if($demi_fin==1){
				$periode.=" matin";
			}
		}

		return  $periode;
	}
?>
