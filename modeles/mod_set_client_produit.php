<?php

// MISE A JOUR D'UN PRODUIT DANS LA TABLE ClIENTS PRODUITS
// Pour l'ajour voir mod_add_client_produit
 
function set_client_produit($produit,$client,$donnee,$histo){

	global $Conn;
	
	global $acc_utilisateur;
	
	$update_histo=false;
	$resultat=true;
	
	if(empty($produit) OR empty($client) OR empty($donnee)){
		$resultat=false;
	}
	
	$sql="UPDATE Clients_Produits SET ";
	foreach($donnee as $c => $u){
		$sql.=$c . "=:" . $c . ",";
	}
	$sql=substr($sql, 0, -1);
	$sql.= " WHERE cpr_produit=:produit AND cpr_client=:client;";					
	$req=$Conn->prepare($sql);					
	$req->bindParam(':produit', $produit);
	$req->bindParam(':client', $client);
	foreach($donnee as $c => $u){
		$req->bindValue($c,$u);	
		if(isset($histo[$c])){
			if($histo[$c]!=$u){
				$update_histo=true;
			}
		}
	}
	try{
		$req->execute();
	}catch(Exception $e){
		//echo($e->getMessage());
		//die();
		$resultat=false;
	}
	
	if($update_histo AND $resultat){

		// VARIATION DE PRIX ON MAJ HISTO
		
		$sql="INSERT INTO Clients_Produits_Histo (cph_produit,cph_client,cph_date,cph_utilisateur,cph_intra,cph_ht_intra,cph_inter,cph_ht_inter,cph_derogation,cph_nego)
		VALUES (:cph_produit,:cph_client,NOW(),:cph_utilisateur,:cph_intra,:cph_ht_intra,:cph_inter,:cph_ht_inter,:cph_derogation,:cph_nego);";
		$req=$Conn->prepare($sql);
		$req->bindParam(":cph_produit",$produit);
		$req->bindParam(":cph_client",$client);
		$req->bindParam(":cph_utilisateur",$acc_utilisateur);
		$req->bindParam(":cph_intra",$histo["cpr_intra"]);
		$req->bindParam(":cph_ht_intra",$histo["cpr_ht_intra"]);
		$req->bindParam(":cph_inter",$histo["cpr_inter"]);
		$req->bindParam(":cph_ht_inter",$histo["cpr_ht_inter"]);		
		$req->bindParam(":cph_derogation",$histo["cpr_derogation"]);
		$req->bindParam(":cph_nego",$histo["cpr_nego"]);
		
		$req->execute();
	};
	
	return $resultat;
	
}
?>