<?php

	// MODELE 
	
	function get_rib($rib_id){
		
		global $Conn;	
		
		$req_sql="SELECT * FROM Rib WHERE rib_id=" . $rib_id;
		$req=$Conn->query($req_sql);
		$get_rib = $req->fetchAll();	
		return $get_rib;
	}
	
	function get_ribs($rib_societe = 0){
		
		global $Conn;	
		
		$req_sql="SELECT * FROM Rib";
		if($rib_societe>0){
			$req_sql=$req_sql . " WHERE rib_societe=" . $rib_societe;
		}
		$req_sql=$req_sql . " ORDER BY rib_nom";
		
		$req=$Conn->query($req_sql);
		$get_rib = $req->fetchAll();	
		return $get_rib;
	}
	
	function get_rib_defaut($rib_societe){
		
		global $Conn;	
		
		$req_sql="SELECT * FROM Rib WHERE rib_societe=" . $rib_societe . " AND rib_defaut";	
		$req=$Conn->query($req_sql);
		$get_rib_defaut = $req->fetch();	
		return $get_rib_defaut;
	}
	
	
	function update_rib($rib_id,$rib_nom,$rib_iban,$rib_bic,$rib_defaut,$rib_change,$rib_affacturage,$rib_banque){
		
		global $Conn;	
		$req = $Conn->prepare("UPDATE rib SET rib_nom=:rib_nom, rib_iban=:rib_iban, rib_bic=:rib_bic, rib_defaut=:rib_defaut, rib_change=:rib_change, rib_affacturage=:rib_affacturage, rib_banque=:rib_banque WHERE rib_id=:rib_id");
		$req->bindParam(':rib_id', $rib_id);
		$req->bindParam(':rib_nom', $rib_nom);
		$req->bindParam(':rib_iban', $rib_iban);
		$req->bindParam(':rib_bic', $rib_bic);
		$req->bindParam(':rib_defaut', $rib_defaut);
		$req->bindParam(':rib_change', $rib_change);
		$req->bindParam(':rib_affacturage', $rib_affacturage);
		$req->bindParam(':rib_banque', $rib_banque);
		$req->execute();	
	}
	
	function update_rib_defaut($rib_id,$rib_defaut){
		
		global $Conn;	
		$req = $Conn->prepare("UPDATE rib SET rib_defaut=:rib_defaut WHERE rib_id=:rib_id");
		$req->bindParam(':rib_id', $rib_id);
		$req->bindParam(':rib_defaut', $rib_defaut);	
		$req->execute();	
	}
	
	
	function insert_rib($rib_nom,$rib_societe,$rib_iban,$rib_bic,$rib_defaut,$rib_change,$rib_affacturage,$rib_banque){
		global $Conn;

		$rib_id=0;
		
		$req = $Conn->prepare("INSERT INTO rib (rib_nom, rib_societe, rib_iban, rib_bic, rib_defaut, rib_change, rib_affacturage, rib_banque) VALUES (:rib_nom, :rib_societe, :rib_iban, :rib_bic, :rib_defaut, :rib_change, :rib_affacturage, :rib_banque)");
		$req->bindParam(':rib_nom', $rib_nom);
		$req->bindParam(':rib_societe', $rib_societe);
		$req->bindParam(':rib_iban', $rib_iban);
		$req->bindParam(':rib_bic', $rib_bic);
		$req->bindParam(':rib_defaut', $rib_defaut);
		$req->bindParam(':rib_change', $rib_change);
		$req->bindParam(':rib_affacturage', $rib_affacturage);
		$req->bindParam(':rib_banque', $rib_banque);
		$req->execute();
		$rib_id=$Conn ->lastInsertId();
		
		return $rib_id;
	}

?>	
