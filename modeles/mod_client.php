<?php


/*--------------------------------------------------
	SELECTION
	----------------------------------------------*/
	
	function get_clients($societe)
	{	

		global $Conn;
		$req = $Conn->prepare("SELECT cli_id,cli_code,cli_nom,cli_categorie FROM clients WHERE cli_societe = :societe ORDER BY cli_code");
		$req->bindParam("societe",$societe);
		$req->execute();
		$clients = $req->fetchAll();
		return $clients;
	} 
	function get_suspects_array($array)
	{	

		global $ConnSoc;
		$a = implode(",", $array);
		if(!empty($a)){
		//	die();
		}
		//echo("a : " . $a);
		
		$req = $ConnSoc->prepare("SELECT * FROM suspects WHERE sus_id IN(" . $a . ") ORDER BY sus_code");
		$req->execute();
		$clients = $req->fetchAll();
		return $clients;
	} 
	function get_clients_get()
	{	

		global $ConnGet;
		$req = $ConnGet->prepare("SELECT cli_id,cli_code,cli_nom,cli_categorie FROM clients ORDER BY cli_code");
		$req->execute();
		$clients = $req->fetchAll();
		return $clients;
	} 
	function get_sous_classifications()
	{	

		global $Conn;
		$req = $Conn->prepare("SELECT * FROM clients_sous_classifications");
		$req->execute();
		$clients = $req->fetchAll();
		return $clients;
	} 

	function get_classifications()
	{	

		global $Conn;
		$req = $Conn->prepare("SELECT * FROM clients_classifications");
		$req->execute();
		$clients = $req->fetchAll();
		return $clients;
	} 
	function get_sous_classification($id)
	{	

		global $Conn;
		$req = $Conn->prepare("SELECT * FROM clients_sous_classifications WHERE csc_id =" . $id);
		$req->execute();
		$clients = $req->fetch();
		return $clients;
	}
	function get_classification($id)
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_classifications WHERE ccl_id = :ccl_id");
		$req->bindParam(':ccl_id', $id);
		$req->execute();
		

		$clients = $req->fetch();

		return $clients;
	}
	function insert_classification()
	{

		global $Conn;
		$ccl_libelle= $_POST['ccl_libelle'];
		
		if (isset($_POST['ccl_id'])) {
			$id = $_POST['ccl_id'];
			$req = $Conn->prepare("UPDATE clients_classifications SET ccl_libelle = :ccl_libelle WHERE ccl_id = :ccl_id");
		}else{
			$req = $Conn->prepare("INSERT INTO clients_classifications (ccl_libelle) VALUES (:ccl_libelle)");
		}  

		$req->bindParam(':ccl_libelle', $ccl_libelle);
		if (isset($_POST['ccl_id'])) {
			$req->bindParam(':ccl_id', $id);
		}
		$req->execute();

		header('Location: classification_liste.php');


	}
	function insert_client_info($sin_type, $sin_description, $sin_famille, $sin_sous_famille, $sin_auteur, $sin_suspect)
	{

		global $ConnSoc;

		$req = $ConnSoc->prepare("INSERT INTO suspects_infos (sin_type, sin_description, sin_famille, sin_sous_famille, sin_auteur, sin_date, sin_suspect) VALUES (:sin_type, :sin_description, :sin_famille, :sin_sous_famille, :sin_auteur, NOW(), :sin_suspect)");
		

		$req->bindParam(':sin_type', $sin_type);
		$req->bindParam(':sin_description', $sin_description);
		$req->bindParam(':sin_famille', $sin_famille);
		$req->bindParam(':sin_sous_famille', $sin_sous_famille);
		$req->bindParam(':sin_auteur', $sin_auteur);
		$req->bindParam(':sin_suspect', $sin_suspect);
	
		$req->execute();


	}
	function insert_cli_info($cin_libelle)
	{

		global $Conn;

		$req = $Conn->prepare("INSERT INTO clients_infos (cin_libelle) VALUES (:cin_libelle)");
		

		$req->bindParam(':cin_libelle', $cin_libelle);
	
		$req->execute();


	}
	function update_cli_info($cin_id, $cin_libelle)
	{

		global $Conn;

		$req = $Conn->prepare("UPDATE clients_infos SET cin_libelle =:cin_libelle WHERE cin_id = :cin_id;");
		

		$req->bindParam(':cin_id', $cin_id);
		$req->bindParam(':cin_libelle', $cin_libelle);
	
		$req->execute();


	}
	function update_client_info($sin_id, $sin_type, $sin_description, $sin_famille, $sin_sous_famille)
	{

		global $ConnSoc;

		$req = $ConnSoc->prepare("UPDATE suspects_infos SET sin_id=:sin_id, sin_type =:sin_type, sin_description=:sin_description, sin_famille=:sin_famille, sin_sous_famille=:sin_sous_famille WHERE sin_id = :sin_id;)");
		
		$req->bindParam(':sin_id', $sin_id);
		$req->bindParam(':sin_type', $sin_type);
		$req->bindParam(':sin_description', $sin_description);
		$req->bindParam(':sin_famille', $sin_famille);
		$req->bindParam(':sin_sous_famille', $sin_sous_famille);
		$req->execute();


	}
	function delete_suspect_info($id){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("DELETE FROM suspects_infos WHERE sin_id = :sin_id");

	$req->bindParam("sin_id",$id);
	$req->execute();

}
	function get_correspondances($suspect)
	{

		global $ConnSoc;
		
		$req = $ConnSoc->prepare("SELECT * FROM suspects_correspondances WHERE sco_suspect = :sco_suspect");

		$req->bindParam(':sco_suspect', $suspect);
		$req->execute();
		$clients = $req->fetchAll();

		return $clients;

	}

	function get_correspondance_last($suspect){
		
		global $ConnSoc;
		
		$req = $ConnSoc->prepare("SELECT * FROM suspects_correspondances WHERE sco_suspect = :sco_suspect ORDER BY sco_date DESC");

		$req->bindParam(':sco_suspect', $suspect);
		$req->execute();
		$clients = $req->fetch();

		return $clients;
	}

	function insert_sous_classification()
	{

		global $Conn;
		$csc_libelle= $_POST['csc_libelle'];
		$csc_classification = $_POST['csc_classification'];
		
		if (isset($_POST['csc_id'])) {
			$id = $_POST['csc_id'];
			$req = $Conn->prepare("UPDATE clients_sous_classifications SET csc_libelle = :csc_libelle, csc_classification = :csc_classification  WHERE csc_id = :csc_id");
		}else{
			$req = $Conn->prepare("INSERT INTO clients_sous_classifications (csc_libelle, csc_classification) VALUES (:csc_libelle, :csc_classification)");
		}  

		$req->bindParam(':csc_libelle', $csc_libelle);
		$req->bindParam(':csc_classification', $csc_classification);
		if (isset($_POST['csc_id'])) {
			$req->bindParam(':csc_id', $id);
		}
		$req->execute();

		header('Location: /sous_classification_liste.php');


	}

	function get_clients_categories($id)
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_categories WHERE cca_id = :cca_id");
		$req->bindParam(':cca_id', $id);
		$req->execute();
		

		$clients = $req->fetch();

		return $clients;
	}
	function get_clients_sous_categories($id)
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_sous_categories WHERE csc_id = :csc_id");
		$req->bindParam(':csc_id', $id);
		$req->execute();
		

		$clients = $req->fetch();

		return $clients;
	}
	function get_clients_infos($id)
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_infos WHERE cin_id = :cin_id");
		$req->bindParam(':cin_id', $id);
		$req->execute();
		

		$clients = $req->fetch();

		return $clients;
	}  
	function get_clients_infos_suspect($id)
	{	

		global $ConnSoc;

		$req = $ConnSoc->prepare("SELECT * FROM suspects_infos WHERE sin_suspect = :sin_suspect");
		$req->bindParam(':sin_suspect', $id);
		$req->execute();
		

		$clients = $req->fetchAll();

		return $clients;
	} 

	function get_clients_infos_suspect_search($sin_suspect, $sin_type, $sin_famille, $sin_sous_famille)
	{	

		global $ConnSoc;

		$sql="SELECT * FROM suspects_infos";
		
		$mil="";
		if(!empty($sin_suspect)){
			$mil.=" AND sin_suspect=" . $sin_suspect; 
		};
		if(!empty($sin_type)){
			$mil.=" AND sin_type=" . $sin_type; 
		};
		if(!empty($sin_famille)){
			$mil.=" AND sin_famille=" . $sin_famille; 
		};
		if(!empty($sin_sous_famille)){
			$mil.=" AND sin_sous_famille=" . $sin_sous_famille; 
		};
		
		if($mil!=""){
			$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		}
		
		$sql.=" ORDER BY sin_type";

		$req = $ConnSoc->prepare($sql);
		$req->execute();
		$utilisateurs = $req->fetchAll();

		return $utilisateurs;
	}

	function get_client_info_select($selected, $client){

  global $Conn;
  global $ConnSoc;

  	$req = $ConnSoc->prepare("SELECT * FROM suspects WHERE sus_id = $client");
	$req->execute();


	$client = $req->fetch();



  $service_select="";

	    if($client['sus_groupe'] == 1 && $client['sus_filiale_de'] == 0 && $client['sus_categorie'] == 2){
	  	$req=$Conn->query("SELECT * FROM clients_infos ORDER BY cin_libelle");
	  }
	  	elseif($client['sus_groupe'] == 1 && $client['sus_filiale_de'] == 0 && $client['sus_categorie'] !=2){
	  		$req=$Conn->query("SELECT * FROM clients_infos WHERE cin_id != 6 ORDER BY cin_libelle");
	  }elseif(($client['sus_groupe'] == 1 OR $client['sus_groupe'] == 0) && $client['sus_filiale_de'] != 0 && $client['sus_categorie'] == 2){
	  		$req=$Conn->query("SELECT * FROM clients_infos WHERE cin_id != 5 ORDER BY cin_libelle");
	  }elseif($client['sus_categorie'] == 2){
	  	$req=$Conn->query("SELECT * FROM clients_infos WHERE cin_id != 6 ORDER BY cin_libelle");
	  }else{
	  	$req=$Conn->query("SELECT * FROM clients_infos WHERE cin_id != 6 AND cin_id != 5 ORDER BY cin_libelle");
	  }
  
  while ($donnees = $req->fetch())
  {
    if($selected == 0){
            $service_select=$service_select . "<option value='" . $donnees["cin_id"] . "' >" . $donnees["cin_libelle"] . "</option>";
        }else{
            if($donnees['cin_id'] == $selected){
               $service_select=$service_select . "<option selected value='" . $donnees["cin_id"] . "' >" . $donnees["cin_libelle"] . "</option>";
            }else{
                $service_select=$service_select . "<option value='" . $donnees["cin_id"] . "' >" . $donnees["cin_libelle"] . "</option>";
            }
           
        }
    
  }
  return $service_select;
  }


	function get_client_categorie()
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_categories");
		$req->execute();
		

		$clients = $req->fetchAll();

		return $clients;
	}
	function get_client_info()
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_infos");
		$req->execute();
		

		$clients = $req->fetchAll();

		return $clients;
	}
	function get_suspect_info($id)
	{	

		global $ConnSoc;

		$req = $ConnSoc->prepare("SELECT * FROM suspects_infos WHERE sin_id = :sin_id");
		$req->bindParam(':sin_id', $id);
		$req->execute();
		

		$clients = $req->fetch();

		return $clients;
	}
	
	function insert_client_categorie($libelle)
	{	

		global $Conn;
		$req = $Conn->prepare("INSERT INTO clients_categories (cca_libelle) VALUES (:cca_libelle);");

		$req->bindParam("cca_libelle",$libelle);
		
		$req->execute();
	}
	function update_client_categorie($id,$libelle)
	{	

		global $Conn;
		$req = $Conn->prepare("UPDATE clients_categories SET cca_libelle = :cca_libelle WHERE cca_id = :cca_id;");
		$req->bindParam("cca_id",$id);
		$req->bindParam("cca_libelle",$libelle);
		
		$req->execute();
	}


	
	
	function get_suspects_categories_select_2($selected)
	{	

		global $Conn;

		$client_select="";
		
		$sql="SELECT * FROM clients_categories WHERE cca_id != 5 AND cca_id !=2"; 
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["cca_id"] . "' >" . $donnees["cca_libelle"] . "</option>";
			}else{
				if($donnees['cca_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["cca_id"] . "' >" . $donnees["cca_libelle"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["cca_id"] . "' >" . $donnees["cca_libelle"] . "</option>";
				}

			}
		}
		return $client_select;
	}

	function get_suspects_sous_categories_select($selected)
	{	

		global $Conn;

		$client_select="";
		
		$sql="SELECT * FROM clients_sous_categories"; 
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["csc_id"] . "' >" . $donnees["csc_libelle"] . "</option>";
			}else{
				if($donnees['csc_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["csc_id"] . "' >" . $donnees["csc_libelle"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["csc_id"] . "' >" . $donnees["csc_libelle"] . "</option>";
				}

			}
		}
		return $client_select;
	}
	function get_client($id, $type)
	{
		// type: cli ou sus pour client ou suspect

		global $ConnSoc;
		if($type == "cli"){
			$req = $ConnSoc->prepare("SELECT * FROM clients WHERE cli_id=:cli_id");
		}else{
			$req = $ConnSoc->prepare("SELECT * FROM suspects WHERE sus_id=:cli_id");
		}
		
		$req->bindParam(':cli_id', $id);
		$req->execute();

		$client = $req->fetch();

		return $client;
	}
	function get_client_classification_select($selected){
		
		// $societe "" par defaut car $societe=0 permet de ne renvoyer aucun résultat 

		global $Conn;

		$client_select="";
		
		$sql="SELECT * FROM clients_classifications";
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["ccl_id"] . "' >" . $donnees["ccl_libelle"] . "</option>";
			}else{
				if($donnees['ccl_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["ccl_id"] . "' >" . $donnees["ccl_libelle"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["ccl_id"] . "' >" . $donnees["ccl_libelle"] . "</option>";
				}

			}
		}
		return $client_select;
	}
	
	/* SELECTION ET INJECTION HTML <select/> */
	
	function get_client_select($selected){
		
		// $societe "" par defaut car $societe=0 permet de ne renvoyer aucun résultat 

		global $ConnSoc;

		$client_select="";
		
		$sql="SELECT cli_id, cli_nom FROM clients";
		
		$req=$ConnSoc->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
			}else{
				if($donnees['cli_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
				}

			}
		}
		return $client_select;
	}

	function get_client_opca_select($selected){
		
		// $societe "" par defaut car $societe=0 permet de ne renvoyer aucun résultat 

		global $ConnSoc;

		$client_select="";
		
		$sql="SELECT cli_id, cli_nom, cli_opca FROM clients WHERE cli_opca != 0";
		
		$req=$ConnSoc->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
			}else{
				if($donnees['cli_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
				}

			}
		}
		return $client_select;
	}
	
	/* FG le 07/01/2021
	cette fonction n'est jamais appelé.
	Attention : ne pas décommenté en l'état car j'ai supprimé les champs cli_facture, cli_facture_date et cli_facture_soc remplacé par 
	cli_first_facture, cli_first_facture_date et cli_first_facture_soc
	

	function insert_client($cli_code,$cli_nom,$cli_agence,$cli_commercial,$cli_societe,$cli_categorie,$cli_code_compta,$cli_stagiaire,$cli_stagiaire_type,$cli_secteur,$cli_sous_secteur,$cli_facturable,$cli_classification,$cli_filiale_de,$cli_dep,$cli_reg_type,$cli_siren,$cli_siret,$cli_ape,$cli_opca,$cli_facture_opca,$cli_niveau,$cli_fil_de,$cli_rib,$cli_rib_change,$cli_reg_formule,$cli_reg_nb_jour,$cli_reg_fdm,$cli_sous_classification,$cli_facture,$cli_facture_date,$cli_prospect,$cli_date_suspect,$cli_groupe, $cli_geo, $cli_ident_tva)
	{

		global $ConnSoc;
		if(session_id() == '' || !isset($_SESSION)) {
			session_start();
		}
		$creator = $_SESSION['acces']['acc_ref_id'];
		$req = $ConnSoc->prepare("INSERT INTO clients (cli_code, cli_nom, cli_agence, cli_commercial, cli_societe, cli_categorie, cli_code_compta, cli_stagiaire, cli_stagiaire_type, cli_secteur, cli_sous_secteur, cli_facturable, cli_classification, cli_sous_classification, cli_facture, cli_facture_date, cli_date_creation, cli_prospect, cli_date_suspect, cli_groupe, cli_filiale_de, cli_dep, cli_reg_type, cli_siren, cli_siret, cli_ape, cli_uti_creation, cli_opca, cli_facture_opca, cli_niveau, cli_fil_de, cli_rib, cli_rib_change, cli_reg_formule, cli_reg_nb_jour, cli_reg_fdm) VALUES (:cli_code, :cli_nom, :cli_agence, :cli_commercial, :cli_societe, :cli_categorie, :cli_code_compta, :cli_stagiaire, :cli_stagiaire_type, :cli_secteur, :cli_sous_secteur, :cli_facturable, :cli_classification, :cli_sous_classification, :cli_facture, :cli_facture_date, NOW(), :cli_prospect, :cli_date_suspect, :cli_groupe, :cli_filiale_de, :cli_dep, :cli_reg_type, :cli_siren, :cli_siret, :cli_ape, :creator, :cli_opca, :cli_facture_opca, :cli_niveau,:cli_fil_de, :cli_rib, :cli_rib_change, :cli_reg_formule, :cli_reg_nb_jour, :cli_reg_fdm);");

		$req->bindParam("cli_code",$cli_code);
		$req->bindParam("cli_nom",$cli_nom);
		$req->bindParam("cli_agence",$cli_agence);
		$req->bindParam("cli_commercial",$cli_commercial);
		$req->bindParam("cli_societe",$cli_societe);
		$req->bindParam("cli_categorie",$cli_categorie);
		$req->bindParam("cli_code_compta",$cli_code_compta);
		$req->bindParam("cli_stagiaire",$cli_stagiaire);
		$req->bindParam("cli_stagiaire_type",$cli_stagiaire_type);
		$req->bindParam("cli_secteur",$cli_secteur);
		$req->bindParam("cli_sous_secteur",$cli_sous_secteur);
		$req->bindParam("cli_facturable",$cli_facturable);
		$req->bindParam("cli_classification",$cli_classification);
		$req->bindParam("cli_filiale_de",$cli_filiale_de);
		$req->bindParam("cli_dep",$cli_dep);
		$req->bindParam("cli_reg_type",$cli_reg_type);
		$req->bindParam("cli_siren",$cli_siren);
		$req->bindParam("cli_siret",$cli_siret);
		$req->bindParam("cli_ape",$cli_ape);
		$req->bindParam("cli_opca",$cli_opca);
		$req->bindParam("cli_facture_opca",$cli_facture_opca);
		$req->bindParam("cli_niveau",$cli_niveau);
		$req->bindParam("cli_fil_de",$cli_fil_de);
		$req->bindParam("cli_rib",$cli_rib);
		$req->bindParam("cli_rib_change",$cli_rib_change);
		$req->bindParam("cli_reg_formule",$cli_reg_formule);
		$req->bindParam("cli_reg_nb_jour",$cli_reg_nb_jour);
		$req->bindParam("cli_reg_fdm",$cli_reg_fdm);
		$req->bindParam("cli_sous_classification",$cli_sous_classification);
		$req->bindParam("cli_facture",$cli_facture);
		$req->bindParam("cli_facture_date",$cli_facture_date);
		$req->bindParam("cli_prospect",$cli_prospect);
		$req->bindParam("cli_date_suspect",$cli_date_suspect);
		$req->bindParam("cli_groupe",$cli_groupe);
		$req->bindParam("creator",$creator);
		$req->bindParam("cli_geo",$cli_geo);
		$req->bindParam("cli_ident_tva",$cli_ident_tva);
		$req->bindParam("cli_prescripteur",$cli_prescripteur);
		$req->execute();
	//print_r($req->errorInfo());

		$id = $ConnSoc->lastInsertId();

		return $id;

	}*/
	
	function insert_suspect($sus_code,$sus_nom,$sus_agence,$sus_commercial,$sus_categorie,$sus_sous_categorie,$sus_stagiaire_type,$sus_secteur,$sus_sous_secteur,$sus_classification,$sus_filiale_de,$sus_filiale_de_soc,$sus_reg_type,$sus_siren,$sus_ape,$sus_opca,$sus_facture_opca,$sus_reg_formule,$sus_reg_nb_jour,$sus_reg_fdm,$sus_sous_classification, $sus_ident_tva, $sus_prescripteur, $sus_groupe, $sus_filiale_de_type, $sus_releve, $sus_reference)
	{

		global $ConnSoc;
		if(session_id() == '' || !isset($_SESSION)) {
			session_start();
		}

		$creator = $_SESSION['acces']['acc_ref_id'];

		$req = $ConnSoc->prepare("INSERT INTO suspects (sus_code, sus_nom, sus_agence, sus_commercial, sus_categorie,sus_sous_categorie,sus_stagiaire_type, sus_secteur, sus_sous_secteur, sus_classification, sus_sous_classification, sus_date_creation, sus_filiale_de,sus_filiale_de_soc, sus_reg_type, sus_siren, sus_ape, sus_uti_creation, sus_opca, sus_facture_opca, sus_reg_formule, sus_reg_nb_jour, sus_reg_fdm, sus_ident_tva, sus_prescripteur, sus_groupe, sus_filiale_de_type, sus_releve, sus_reference) VALUES (:sus_code, :sus_nom, :sus_agence, :sus_commercial, :sus_categorie,:sus_sous_categorie,:sus_stagiaire_type, :sus_secteur, :sus_sous_secteur, :sus_classification, :sus_sous_classification, NOW(), :sus_filiale_de,:sus_filiale_de_soc, :sus_reg_type, :sus_siren, :sus_ape, :creator, :sus_opca, :sus_facture_opca, :sus_reg_formule, :sus_reg_nb_jour, :sus_reg_fdm, :sus_ident_tva, :sus_prescripteur, :sus_groupe, :sus_filiale_de_type, :sus_releve, :sus_reference);");
		
		$req->bindParam("sus_code",$sus_code);
		$req->bindParam("sus_nom",$sus_nom);
		$req->bindParam("sus_agence",$sus_agence);
		$req->bindParam("sus_commercial",$sus_commercial);
		$req->bindParam("sus_stagiaire_type",$sus_stagiaire_type);
		$req->bindParam("sus_categorie",$sus_categorie);
		$req->bindParam("sus_sous_categorie",$sus_sous_categorie);
		$req->bindParam("sus_secteur",$sus_secteur);
		$req->bindParam("sus_sous_secteur",$sus_sous_secteur);
		$req->bindParam("sus_classification",$sus_classification);
		$req->bindParam("sus_filiale_de",$sus_filiale_de);
		$req->bindParam("sus_reg_type",$sus_reg_type);
		$req->bindParam("sus_siren",$sus_siren);
		$req->bindParam("sus_ape",$sus_ape);
		$req->bindParam("sus_opca",$sus_opca);
		$req->bindParam("sus_facture_opca",$sus_facture_opca);
		$req->bindParam("sus_reg_formule",$sus_reg_formule);
		$req->bindParam("sus_reg_nb_jour",$sus_reg_nb_jour); 
		$req->bindParam("sus_reg_fdm",$sus_reg_fdm);
		$req->bindParam("sus_sous_classification",$sus_sous_classification);
		$req->bindParam("creator",$creator);
		$req->bindParam("sus_ident_tva",$sus_ident_tva);
		$req->bindParam("sus_prescripteur",$sus_prescripteur);
		$req->bindParam("sus_filiale_de_soc",$sus_filiale_de_soc);
		$req->bindParam("sus_groupe",$sus_groupe);
		$req->bindParam("sus_filiale_de_type",$sus_filiale_de_type);
		$req->bindParam("sus_releve",$sus_releve);
		$req->bindParam("sus_reference",$sus_reference);
		$req->execute();
	//print_r($req->errorInfo());

		$id = $ConnSoc->lastInsertId();

		return $id;

	}
	function insert_suspect_csv($sus_commercial, $sus_categorie, $sus_sous_categorie,$sus_code,$sus_reference,$sus_nom,$sus_siren, $sus_siret, $sus_ape, $sus_ident_tva, $sad_ad1, $sad_ad2, $sad_ad3, $sad_cp, $sad_ville, $sco_nom, $sco_prenom, $sco_mail, $sco_telephone, $sco_portable)
	{

		global $ConnSoc;
		if(session_id() == '' || !isset($_SESSION)) {
			session_start();
		}

		intval($sus_siren);
		if(strlen($sus_siren) != 11 && !empty($sus_siren)){
			/*$sus_siren = number_format($sus_siren, 0, ',', ' ');*/
			$sus_siren = null;
		}

		intval($sad_cp);	
		if(strlen($sad_cp) != 6 && !empty($sad_cp)){
			$sad_cp = null;
		}

		$sus_code = strtoupper($sus_code);
		$sad_ville = strtoupper($sad_ville);
		$sco_nom = strtoupper($sco_nom);

		intval($sco_telephone);
		intval($sco_portable);

		if(strlen($sus_siret) != 6){
			$sus_siret = 0;
		}
		$creator = $_SESSION['acces']['acc_ref_id'];
		$req = $ConnSoc->prepare("INSERT INTO suspects (sus_commercial, sus_categorie,sus_sous_categorie, sus_code,sus_reference, sus_nom, sus_date_creation, sus_uti_creation, sus_siren, sus_ape, sus_ident_tva) VALUES (:sus_commercial, :sus_categorie, :sus_sous_categorie, :sus_code,:sus_reference, :sus_nom, NOW(), :creator, :sus_siren,:sus_ape, :sus_ident_tva);");
		
		$req->bindParam("sus_code",$sus_code);
		$req->bindParam("sus_nom",$sus_nom);
		$req->bindParam("creator",$creator);
		$req->bindParam("sus_categorie",$sus_categorie);
		$req->bindParam("sus_sous_categorie",$sus_sous_categorie);
		$req->bindParam("sus_code",$sus_code);
		$req->bindParam("sus_nom",$sus_nom);
		$req->bindParam("sus_siren",$sus_siren);
		$req->bindParam("sus_ape",$sus_ape);
		$req->bindParam("sus_ident_tva",$sus_ident_tva);
		$req->bindParam("sus_commercial",$sus_commercial);
		$req->bindParam("sus_commercial",$sus_reference);
		$req->execute();
		//print_r($req->errorInfo());

		$id = $ConnSoc->lastInsertId(); 

		$req = $ConnSoc->prepare("INSERT INTO suspects_adresses (sad_ref, sad_ref_id,sad_type, sad_ad1, sad_ad2, sad_ad3, sad_cp, sad_ville, sad_defaut) VALUES (1, :id,1, :sad_ad1, :sad_ad2, :sad_ad3, :sad_cp, :sad_ville, 1);");
		
		
		$req->bindParam("id",$id);
		$req->bindParam("sad_ad1",$sad_ad1);
		$req->bindParam("sad_ad2",$sad_ad2);
		$req->bindParam("sad_ad3",$sad_ad3);
		$req->bindParam("sad_cp",$sad_cp);
		$req->bindParam("sad_ville",$sad_ville);

		$req->execute();

		$req = $ConnSoc->prepare("INSERT INTO suspects_adresses (sad_ref, sad_ref_id,sad_type, sad_ad1, sad_ad2, sad_ad3, sad_cp, sad_ville, sad_siret, sad_defaut) VALUES (1, :id,2, :sad_ad1, :sad_ad2, :sad_ad3, :sad_cp, :sad_ville, :sus_siret, 1);");
		
		
		$req->bindParam("id",$id);
		$req->bindParam("sad_ad1",$sad_ad1);
		$req->bindParam("sad_ad2",$sad_ad2);
		$req->bindParam("sad_ad3",$sad_ad3);
		$req->bindParam("sad_cp",$sad_cp);
		$req->bindParam("sus_siret",$sus_siret);
		$req->bindParam("sad_ville",$sad_ville);

		$req->execute();

		$req = $ConnSoc->prepare("INSERT INTO suspects_contacts (sco_ref, sco_ref_id,sco_nom, sco_prenom,sco_mail, sco_tel, sco_portable, sco_defaut) VALUES (1, :id, :sco_nom, :sco_prenom, :sco_mail, :sco_telephone,:sco_portable, 1);");
		
		
		$req->bindParam("id",$id);
		$req->bindParam("sco_nom",$sco_nom);
		$req->bindParam("sco_prenom",$sco_prenom);
		$req->bindParam("sco_mail",$sco_mail);
		$req->bindParam("sco_telephone",$sco_telephone);
		$req->bindParam("sco_portable",$sco_portable);

		$req->execute();

		return $id;

	}

	function update_suspect_siren($sus_id, $sus_siren){
//die();
		global $ConnSoc;
		$req = $ConnSoc->prepare("UPDATE suspects SET sus_siren = :sus_siren WHERE sus_id = :sus_id");

		$req->bindParam("sus_id",$sus_id);
		$req->bindParam("sus_siren",$sus_siren);

		$req->execute();
	}

	function update_suspect($sus_id, $sus_code,$sus_nom,$sus_agence,$sus_commercial,$sus_categorie,$sus_sous_categorie,$sus_secteur,$sus_sous_secteur,$sus_reg_type,$sus_siren,$sus_ape,$sus_opca,$sus_facture_opca,$sus_reg_formule,$sus_reg_nb_jour,$sus_reg_fdm,$sus_sous_classification, $sus_ident_tva, $sus_prescripteur, $sus_stagiaire_type, $sus_classification, $sus_groupe, $sus_filiale_de_type, $sus_releve, $sus_reference)
	{
			
		//die();
		global $ConnSoc;
		$req = $ConnSoc->prepare("UPDATE suspects SET sus_code = :sus_code, sus_nom = :sus_nom, sus_agence = :sus_agence, sus_commercial = :sus_commercial, sus_categorie = :sus_categorie,sus_sous_categorie = :sus_sous_categorie, sus_secteur = :sus_secteur, sus_sous_secteur = :sus_sous_secteur, sus_classification = :sus_classification, sus_reg_type = :sus_reg_type, sus_siren = :sus_siren, sus_ape = :sus_ape, sus_opca = :sus_opca, sus_facture_opca = :sus_facture_opca, sus_reg_formule = :sus_reg_formule, sus_reg_nb_jour = :sus_reg_nb_jour, sus_reg_fdm = :sus_reg_fdm, sus_sous_classification = :sus_sous_classification, sus_ident_tva = :sus_ident_tva, sus_prescripteur= :sus_prescripteur, sus_stagiaire_type = :sus_stagiaire_type, sus_groupe = :sus_groupe, sus_filiale_de_type = :sus_filiale_de_type, sus_releve = :sus_releve, sus_reference = :sus_reference  WHERE sus_id = :sus_id");

		$req->bindParam("sus_id",$sus_id);
		$req->bindParam("sus_code",$sus_code);
		$req->bindParam("sus_nom",$sus_nom);
		$req->bindParam("sus_agence",$sus_agence);
		$req->bindParam("sus_commercial",$sus_commercial);
		$req->bindParam("sus_categorie",$sus_categorie);
		$req->bindParam("sus_sous_categorie",$sus_sous_categorie);
		$req->bindParam("sus_secteur",$sus_secteur);
		$req->bindParam("sus_sous_secteur",$sus_sous_secteur);
		$req->bindParam("sus_classification",$sus_classification);
		$req->bindParam("sus_reg_type",$sus_reg_type);
		$req->bindParam("sus_siren",$sus_siren);
		$req->bindParam("sus_ape",$sus_ape);
		$req->bindParam("sus_opca",$sus_opca);
		$req->bindParam("sus_facture_opca",$sus_facture_opca);
		$req->bindParam("sus_reg_formule",$sus_reg_formule);
		$req->bindParam("sus_reg_nb_jour",$sus_reg_nb_jour);
		$req->bindParam("sus_reg_fdm",$sus_reg_fdm);
		$req->bindParam("sus_ident_tva",$sus_ident_tva);
		$req->bindParam("sus_prescripteur",$sus_prescripteur);
		$req->bindParam("sus_stagiaire_type",$sus_stagiaire_type);
		$req->bindParam("sus_sous_classification",$sus_sous_classification);
		$req->bindParam("sus_groupe",$sus_groupe);
		$req->bindParam("sus_filiale_de_type",$sus_filiale_de_type);
		$req->bindParam("sus_reference",$sus_reference);
		$req->bindParam("sus_releve",$sus_releve);
		$req->execute();

	}




