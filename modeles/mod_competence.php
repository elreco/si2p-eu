<?php

	/*-------------------------------------------
		FONCTION DE SELECTION
	---------------------------------------------*/
	function get_competences()
	{

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM Competences ORDER BY com_libelle");
		$req->execute();

		$familles = $req->fetchAll();

		return $familles;
	}

	function get_competences_select($select)
	{

		global $Conn;

		$client_select="";
		
		$sql="SELECT * FROM Competences";
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($select == 0){
				$client_select=$client_select . "<option value='" . $donnees["com_id"] . "' >" . $donnees["com_libelle"] . "</option>";
			}else{
				if($donnees['com_id'] == $select){
					$client_select=$client_select . "<option selected value='" . $donnees["com_id"] . "' >" . $donnees["com_libelle"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["com_id"] . "' >" . $donnees["com_libelle"] . "</option>";
				}

			}
		}
		return $client_select;
	}

	
	function get_competence($id)
	{

		global $Conn;
		
		$competence=null;
		
		$req = $Conn->prepare("SELECT * FROM Competences WHERE com_id=:id");
		$req -> bindParam(":id",$id);
		$req->execute();
		$competence = $req->fetch();
		
		return $competence;
	}
	// LIEN ENTRE UNE COMPETENCE ET LES DIPLOMES
	
	// retourne tous les diplomes affectés à une compétence donnée
	function get_competence_diplomes($competence)
	{	
		global $Conn;
		
		$req = $Conn->prepare("SELECT * FROM Diplomes LEFT JOIN Diplomes_Competences ON(Diplomes.dip_id=Diplomes_Competences.dco_diplome) WHERE dco_competence=:competence ORDER BY dip_libelle");
		$req->bindParam(":competence",$competence);
		$req->execute();
		$get_competence_diplomes = $req->fetchAll();

		return $get_competence_diplomes;
	}
	// retourne les valeurs de liaison entre une competence et un diplome (obligatoire, interne, ...) 
	function get_competence_diplome($competence,$diplome){
		
		global $Conn;
		
		$req = $Conn->prepare("SELECT * FROM Diplomes_Competences WHERE dco_competence=:competence AND dco_diplome=:diplome");
		$req -> bindParam(":competence",$competence);
		$req -> bindParam(":diplome",$diplome);
		$req->execute();
		$get_competence_diplome = $req->fetch();

		return $get_competence_diplome;
	}
	
	// LIEN ENTRE LES COMPETENCES ET LES INTERVENANTS
	
	function get_competences_intervenant($intervenant_type,$intervenant_id,$strict){
		// retourne la liste des compétences 
		// si strict=false retourne toutes les compétences avec leurs valeurs d'affectation à un intervenant donnée
		// si strict=true retourne que les compétences affecté à l'utilisateur
		
		global $Conn;
		$result;
		
		
		$sql="SELECT * FROM Competences";
		if($strict==1){
			$sql=$sql . " LEFT JOIN"; 	
		}else{
			$sql=$sql . " LEFT OUTER JOIN"; 	
		}
		$sql=$sql . " Intervenants_Competences";
		$sql=$sql . " ON(Competences.com_id=Intervenants_Competences.ico_competence AND Intervenants_Competences.ico_ref=:intervenant_type AND Intervenants_Competences.ico_ref_id=:intervenant_id)";
		$sql=$sql . " ORDER BY com_libelle";	
		$req = $Conn->prepare($sql);
		$req->bindParam(":intervenant_type",$intervenant_type);
		$req->bindParam(":intervenant_id",$intervenant_id);
		$req->execute();
		$result = $req->fetchAll();

		return $result;
	}

	/*-------------------------------------------
		FONCTION DE VERIF
	---------------------------------------------*/	
	/* Le fonction de verif permette de verifier la présence d'un enregistrement dans la base
	Elles renvoient True si au moin une occurence a été trouvé sinon elles renvoient False. */
	
	function bool_competence_intervenant($ref,$ref_id,$competence){
		
		global $Conn;
		
		$retour=false;
		
		$sql="SELECT * FROM Intervenants_Competences WHERE ico_competence=:competence AND ico_ref=:ref AND ico_ref_id=:ref_id";
		$req = $Conn->prepare($sql);
		$req->bindParam(":competence",$competence);
		$req->bindParam(":ref_id",$ref_id);
		$req->bindParam(":ref",$ref);
		$req->execute();
		$result = $req->fetchAll();
		if(!empty($result)){
			$retour=true;
		}
		return $retour;
		
	}

	/*-------------------------------------------
		CREATION
	---------------------------------------------*/
	function insert_competence($libelle)
	{
		global $Conn;

		$req = $Conn->prepare("INSERT INTO Competences (com_libelle) VALUES (:libelle)");
		$req->bindParam(":libelle",$libelle);
		$req->execute();
		$competence=$Conn->lastInsertId();
		
		return $competence;
	}
	function insert_competence_diplome($competence,$diplome,$obligatoire,$interne,$externe){
		
		global $Conn;
		
		$req = $Conn->prepare("INSERT INTO Diplomes_Competences (dco_competence, dco_diplome, dco_obligatoire, dco_interne, dco_externe ) VALUES (:competence, :diplome, :obligatoire, :interne, :externe)");
		$req -> bindParam(":competence",$competence);
		$req -> bindParam(":diplome",$diplome);
		$req -> bindParam(":obligatoire",$obligatoire);
		$req -> bindParam(":interne",$interne);
		$req -> bindParam(":externe",$externe);
		$req->execute();
	}
	// affecte une compétence à un intervenant donnée
	
	function insert_competence_intervenant($ico_ref,$ico_ref_id,$ico_competence,$ico_controle_dt,$ico_controle_dt_txt){
		
		global $Conn;
		$req = $Conn->prepare("INSERT INTO Intervenants_Competences (ico_ref, ico_ref_id, ico_competence, ico_controle_dt, ico_controle_dt_txt)
		VALUES (:ico_ref, :ico_ref_id, :ico_competence, :ico_controle_dt, :ico_controle_dt_txt)");
		$req -> bindParam(":ico_ref",$ico_ref);
		$req -> bindParam(":ico_ref_id",$ico_ref_id);
		$req -> bindParam(":ico_competence",$ico_competence);
		$req -> bindParam(":ico_controle_dt",$ico_controle_dt);
		$req -> bindParam(":ico_controle_dt_txt",$ico_controle_dt_txt);
		$req->execute();
		
	}
	
	/*-------------------------------------------
		MISE A JOUR
	---------------------------------------------*/
	function update_competence($id,$libelle){

		global $Conn;

		$req = $Conn->prepare("UPDATE Competences SET com_libelle=:libelle WHERE com_id=:id");
		$req->bindParam(":id",$id);
		$req->bindParam(":libelle",$libelle);
		$req->execute();
	}
	function update_competence_diplome($competence,$diplome,$obligatoire,$interne,$externe){
		
		global $Conn;
		
		$sql="UPDATE Diplomes_Competences SET dco_obligatoire=:obligatoire,dco_interne=:interne,dco_externe=:externe";
		$sql=$sql . " WHERE dco_competence=:competence AND dco_diplome=:diplome";
		$req = $Conn->prepare($sql);
		$req -> bindParam(":competence",$competence);
		$req -> bindParam(":diplome",$diplome);
		$req -> bindParam(":obligatoire",$obligatoire);
		$req -> bindParam(":interne",$interne);
		$req -> bindParam(":externe",$externe);
		$req->execute();
	
	}
	
	// MET A JOUR L'ETAT D'UN COMPETENCE POUR UN INTERVENANT DONNEE
	function update_competence_intervenant($ico_ref,$ico_ref_id,$ico_competence,$delete){
		
		global $Conn;
				
		$sql="UPDATE Intervenants_Competences SET ico_controle_dt=:ico_controle_dt, ico_controle_dt_txt=:ico_controle_dt_txt";
		$sql=$sql . " WHERE ico_ref=:ico_ref AND ico_ref_id=:ico_ref_id AND ico_competence=:ico_competence";
		$req = $Conn->prepare($sql);
		$req -> bindParam(":ico_ref",$ico_ref);
		$req -> bindParam(":ico_ref_id",$ico_ref_id);
		$req -> bindParam(":ico_competence",$ico_competence);
		$req -> bindParam(":ico_controle_dt",$ico_controle_dt);
		$req -> bindParam(":ico_controle_dt_txt",$ico_controle_dt_txt);
		$req->execute();
		$erreur=$Conn->errorInfo();
		
	}

	
	/*-------------------------------------------
		FONCTION DE SUPPRESION
	---------------------------------------------*/
	
	// SUPPRIME L'AFFECTATION D'UN DIPLOME A UNE COMPETENCE DONNEE
	function delete_competence_diplome($competence,$diplome){

		global $Conn;

		$req = $Conn->prepare("DELETE FROM Diplomes_Competences WHERE dco_competence=:competence AND dco_diplome=:diplome");
		$req->bindParam(":competence",$competence);
		$req->bindParam(":diplome",$diplome);
		$req->execute();
		if($req->errorCode()!="00000"){
			print($req->errorInfo());
			die();
		}
	}
	// SUPPRIME L'AFFECTATION D'UN DIPLOME A UNE COMPETENCE DONNEE
	function delete_competence_intervenant($ref,$ref_id,$competence){

		global $Conn;

		$req = $Conn->prepare("DELETE FROM Intervenants_Competences WHERE ico_competence=:competence AND ico_ref=:ref AND ico_ref_id=:ref_id");
		$req->bindParam(":competence",$competence);
		$req->bindParam(":ref",$ref);
		$req->bindParam(":ref_id",$ref_id);
		$req->execute();
		if($req->errorCode()!="00000"){
			print($req->errorInfo());
			die();
		}
	}
	

	
	