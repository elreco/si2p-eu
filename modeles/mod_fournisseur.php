<?php 


function get_fournisseur_famille_select($selected){

global $Conn;

$service_select="";
$req=$Conn->query("SELECT * FROM fournisseurs_familles ORDER BY ffa_libelle");
    while ($donnees = $req->fetch())
    {
        if($selected == 0){
            $service_select=$service_select . "<option value='" . $donnees["ffa_id"] . "' >" . $donnees["ffa_libelle"] . "</option>";
        }else{
            if($donnees['ffa_id'] == $selected){
               $service_select=$service_select . "<option selected value='" . $donnees["ffa_id"] . "' >" . $donnees["ffa_libelle"] . "</option>";
            }else{
                $service_select=$service_select . "<option value='" . $donnees["ffa_id"] . "' >" . $donnees["ffa_libelle"] . "</option>";
            }
           
        }

    }
return $service_select;
}

function get_fournisseur_famille($famille_id){

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM fournisseurs_familles WHERE ffa_id = $famille_id");
    $req->execute();
    $famille = $req->fetch();
    return $famille['ffa_libelle'];

}

function get_fournisseur_categorie($cat_id){

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_categories WHERE pca_id = $cat_id");
    $req->execute();
    $categorie = $req->fetch();
    return $categorie['pca_libelle'];

}

?>