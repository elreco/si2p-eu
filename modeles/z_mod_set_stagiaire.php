 <?php
 
 
	function set_stagiaire($stagiaire,$titre,$nom,$prenom,$ad1,$ad2,$ad3,$cp,$ville,$tel,$portable,$mail_perso,$naissance,$cp_naiss,$ville_naiss,$mail){
		
		global $ConnSoc;
		
		$stagiaire=intval($stagiaire);
		$titre=intval($titre);
		
		$naissance_test=date_create_from_format("d/m/Y",$naissance);
		if(is_bool($naissance_test)){
			$naissance_sql=null;
		}else{
			$naissance_sql=$naissance_test->format("Y-m-d");
		}
		
		$sql="UPDATE Stagiaires
		SET sta_titre = :titre,
		sta_titre = :titre,
		sta_nom = :nom,
		sta_prenom = :prenom,
		sta_ad1 = :ad1,
		sta_ad2 = :ad2,
		sta_ad3 = :ad3,
		sta_cp = :cp,
		sta_ville = :ville,
		sta_tel = :tel,
		sta_portable = :portable,
		sta_mail_perso = :mail_perso,
		sta_naissance = :naissance,
		sta_cp_naiss = :cp_naiss,
		sta_ville_naiss = :ville_naiss,
		sta_mail = :mail
		WHERE sta_id=:stagiaire;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":titre",$titre);
		$req->bindParam(":nom",$nom);
		$req->bindParam(":prenom",$prenom);
		$req->bindParam(":ad1",$ad1);
		$req->bindParam(":ad2",$ad2);
		$req->bindParam(":ad3",$ad3);
		$req->bindParam(":cp",$cp);
		$req->bindParam(":ville",$ville);
		$req->bindParam(":tel",$tel);
		$req->bindParam(":portable",$portable);
		$req->bindParam(":mail_perso",$mail_perso);
		$req->bindParam(":naissance",$naissance_sql);
		$req->bindParam(":cp_naiss",$cp_naiss);
		$req->bindParam(":ville_naiss",$ville_naiss);
		$req->bindParam(":mail",$mail);
		$req->bindParam(":stagiaire",$stagiaire);
		$req->execute();
		$erreur=$ConnSoc->errorCode();
		if($erreur!="00000"){
			return false;
		}else{
			return true;	
		}
		
	} 
?>