
<?php


function plateformCall($array, $url, $type = 'POST', $local = true) {


    $base_url = $_ENV['API_PLATEFORME_URL'];

    $cleApi = $_ENV['API_PLATEFORME_KEY'];

    $curl = curl_init();

    curl_setopt_array($curl, [
    CURLOPT_URL => $base_url . $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => $type,
    CURLOPT_POSTFIELDS => json_encode($array),
    CURLOPT_HTTPHEADER => [
        "Content-Type: application/json",
        "authorization: Bearer " . $cleApi
    ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        die("cURL Error #:" . $err);
    } else {
        return json_decode($response, true);
    }
}