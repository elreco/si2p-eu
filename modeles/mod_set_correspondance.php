<?php
// creation ou maj d'un contact

// $d_correspondance => array avec clé = nom champ base
// retourne false en cas d'échec et IN du contact en cas de réussite

function set_correspondance($d_correspondance){

	global $Conn;

	if(!empty($d_correspondance)){

		$criteres=array();
		if(empty($d_correspondance["cor_date"]) OR empty($d_correspondance["cor_commentaire"]) OR empty($d_correspondance["cor_utilisateur"]) OR empty($d_correspondance["cor_client"]) OR empty($d_correspondance["cor_type"]) ){
			return false;
		}

		if(!empty($d_correspondance["cor_id"])){

			// MAJ

			/* on ne maj plus l'utilisateur pour que le service market puissent changer les dates des correspondances sans bloquer l'accès à la personne 
			qui a enregistré la correspondance
			-> si retour UTI à ce sujet, faire un cas particulier pour le service market
			*/
			$sql="UPDATE correspondances SET ";
			$sql.="cor_date=:cor_date,cor_commentaire=:cor_commentaire,cor_type=:cor_type,cor_raison=:cor_raison,cor_raison_info=:cor_raison_info,";

			$criteres["cor_date"]=$d_correspondance["cor_date"];
			$criteres["cor_commentaire"]=$d_correspondance["cor_commentaire"];
			//$criteres["cor_utilisateur"]=$d_correspondance["cor_utilisateur"];
			$criteres["cor_type"]=$d_correspondance["cor_type"];
			$criteres["cor_raison"]=$d_correspondance["cor_raison"];
			$criteres["cor_raison_info"]=$d_correspondance["cor_raison_info"];

			if(isset($d_correspondance["cor_contact"])){
				if(!empty($d_correspondance["cor_contact"])){
					$sql.="cor_contact=:cor_contact,";
					$criteres["cor_contact"]=$d_correspondance["cor_contact"];
				}else{
					$sql.="cor_contact=0,";
				}
			}
			if(isset($d_correspondance["cor_contact_nom"])){
				if(!empty($d_correspondance["cor_contact_nom"])){
					$sql.="cor_contact_nom=:cor_contact_nom,";
					$criteres["cor_contact_nom"]=$d_correspondance["cor_contact_nom"];
				}else{
					$sql.="cor_contact_nom='',";
				}
			}
			if(isset($d_correspondance["cor_contact_prenom"])){
				if(!empty($d_correspondance["cor_contact_prenom"])){
					$sql.="cor_contact_prenom=:cor_contact_prenom,";
					$criteres["cor_contact_prenom"]=$d_correspondance["cor_contact_prenom"];
				}else{
					$sql.="cor_contact_prenom='',";
				}
			}
			if(isset($d_correspondance["cor_contact_tel"])){
				if(!empty($d_correspondance["cor_contact_tel"])){
					$sql.="cor_contact_tel=:cor_contact_tel,";
					$criteres["cor_contact_tel"]=$d_correspondance["cor_contact_tel"];
				}else{
					$sql.="cor_contact_tel='',";
				}
			}
			if(isset($d_correspondance["cor_rappeler_le"])){
				if(!empty($d_correspondance["cor_rappeler_le"])){
					$sql.="cor_rappeler_le=:cor_rappeler_le,";
					$criteres["cor_rappeler_le"]=$d_correspondance["cor_rappeler_le"];
				}else{
					$sql.="cor_rappeler_le=NULL,";
				}
			}
			if(isset($d_correspondance["cor_rappel"])){
				if(!empty($d_correspondance["cor_rappel"])){
					$sql.="cor_rappel=1,";
				}else{
					$sql.="cor_rappel=0,";
				}
			}
			$sql=substr($sql,0,-1);
			$sql.=" WHERE cor_id=:correspondance;";
			$req = $Conn->prepare($sql);
			$req->bindParam("correspondance",$d_correspondance["cor_id"]);
			foreach($criteres as $c => $u){
				$req->bindValue($c,$u);
			}
			try {
				$req->execute();
			}catch (Exception $e) {
				echo("MAJ " . $e->getMessage());
				echo("<br/>");
				return false;
			}

		}else{

			// ADD


			$sql="INSERT INTO Correspondances (cor_client,cor_date,cor_utilisateur,cor_commentaire,cor_type,cor_raison,cor_raison_info";
			$valeurs=":cor_client,:cor_date,:cor_utilisateur,:cor_commentaire,:cor_type,:cor_raison,:cor_raison_info";

			if(isset($d_correspondance["cor_contact"])){
				$sql.=",cor_contact";
				$valeurs.=",:cor_contact";
				if(!empty($d_correspondance["cor_contact"])){
					$criteres["cor_contact"]=$d_correspondance["cor_contact"];
				}else{
					$criteres["cor_contact"]=0;
				}
			}
			if(isset($d_correspondance["cor_contact_nom"])){
				$sql.=",cor_contact_nom";
				$valeurs.=",:cor_contact_nom";
				$criteres["cor_contact_nom"]=$d_correspondance["cor_contact_nom"];
			}
			if(isset($d_correspondance["cor_contact_prenom"])){
				$sql.=",cor_contact_prenom";
				$valeurs.=",:cor_contact_prenom";
				$criteres["cor_contact_prenom"]=$d_correspondance["cor_contact_prenom"];
			}
			if(isset($d_correspondance["cor_contact_tel"])){
				$sql.=",cor_contact_tel";
				$valeurs.=",:cor_contact_tel";
				$criteres["cor_contact_tel"]=$d_correspondance["cor_contact_tel"];
			}

			if(isset($d_correspondance["cor_rappeler_le"])){
				$sql.=",cor_rappeler_le";
				$valeurs.=",:cor_rappeler_le";
				if(!empty($d_correspondance["cor_rappeler_le"])){
					$criteres["cor_rappeler_le"]=$d_correspondance["cor_rappeler_le"];
				}else{
					$criteres["cor_rappeler_le"]=NULL;
				}
			}
			$sql.=" ) VALUES (" . $valeurs . ");";
			$req = $Conn->prepare($sql);
			$req->bindParam(":cor_client",$d_correspondance["cor_client"]);
			$req->bindParam(":cor_date",$d_correspondance["cor_date"]);
			$req->bindParam(":cor_commentaire",$d_correspondance["cor_commentaire"]);
			$req->bindParam(":cor_utilisateur",$d_correspondance["cor_utilisateur"]);
			$req->bindParam(":cor_type",$d_correspondance["cor_type"]);
			$req->bindParam(":cor_raison",$d_correspondance["cor_raison"]);
			$req->bindParam(":cor_raison_info",$d_correspondance["cor_raison_info"]);
			foreach($criteres as $c => $u){
				$req->bindValue($c,$u);
			}
			try{
				$req->execute();
				$d_correspondance["cor_id"]=$Conn->lastInsertId();
			}catch (Exception $e){
				echo($e->getMessage());
				echo("<br/>");
				return false;
			}
		}

		// FIN D'EDITION DE LA Correspondances

		// traitement auto du rappel

		if(!empty($d_correspondance["cor_contact"])){

			echo("contact " . $d_correspondance["cor_contact"] . "<br/>");
			$sql="UPDATE Correspondances SET cor_rappel=1 WHERE cor_client=" . $d_correspondance["cor_client"] . " AND cor_utilisateur=" . $d_correspondance["cor_utilisateur"] . "
			AND cor_contact=" . $d_correspondance["cor_contact"] . " AND cor_date<='" . $d_correspondance["cor_date"] . "' AND NOT cor_id=" . $d_correspondance["cor_id"];
			if(!empty($d_correspondance["cor_rappeler_le"])){
				$sql.=" AND cor_rappeler_le<='" . $d_correspondance["cor_rappeler_le"] . "'";
			};
			$sql.=";";
			try{
				$Conn->query($sql);
			}catch (Exception $e){

				echo("Erreur rappel auto");
				echo("<br/>");
				echo($e->getMessage());
				echo("<br/>");
				die("ok");
				/*
				echo($sql);
				echo("<br/>");
				echo($d_correspondance["cor_date"]);
				echo("<br/>");
				echo($d_correspondance["cor_rappeler_le"]);
				echo("<br/>");
				echo("Fin debug rappel auto<br/>");*/

			}


		}

		// MAJ DE LA FICHE CLIENT

		$sql="SELECT MAX(cor_date),MAX(cor_rappeler_le) FROM Correspondances WHERE cor_client=" . $d_correspondance["cor_client"] . ";";
		$req=$Conn->query($sql);
		$d_client=$req->fetch();
		if(!empty($d_client)){

			if(!empty($d_client[0])){
				$cli_cor_date=date_create_from_format('Y-m-d',$d_client[0]);
				if(!empty($cli_cor_date)){
					$sql="UPDATE Clients SET cli_cor_date='" . $cli_cor_date->format("Y-m-d") . "' WHERE cli_id=" . $d_correspondance["cor_client"] . ";";
					try{
						$Conn->query($sql);
					}catch (Exception $e){
						//echo($e->getMessage());
						//echo("<br/>");
					}
				}
			}
			
			if(!empty($d_client[1])){			
				$cli_cor_rappel=date_create_from_format('Y-m-d',$d_client[1]);
				
				if(!empty($cli_cor_rappel)){
				
					$sql="UPDATE Clients SET cli_cor_rappel='" . $cli_cor_rappel->format("Y-m-d") . "' WHERE cli_id=" . $d_correspondance["cor_client"] . ";";
					try{
						$Conn->query($sql);
					}catch (Exception $e){
						//echo($e->getMessage());
						//echo("<br/>");
					}
				}
			}
		}

		return intval($d_correspondance["cor_id"]);
	}

}
