<?php

/* RETOURNES LES DATES AFFECTES A UNE ACTION 

action : id de l'action dont on veut recuperer les dates

option
action_client >0 retourne les dates de l'action avec les valeurs de presence du client concernee

AUTEUR FG
*/
function get_action_sessions($action,$action_client){
	
	global $ConnSoc;
	global $ConnGet;
	if(isset($ConnSoc)){
		$ConnF=$ConnSoc;
		//echo("ICI<br/>");
	}else{
		$ConnF=$ConnGet;
		//echo("LA<br/>");
	}
	

	$sql="SELECT int_label_1";
	$sql.=",pda_id,pda_date,pda_demi,pda_test";
	$sql.=",ase_id,ase_h_deb,ase_h_fin";
	if($action_client>0){
		$sql.=",acd_action_client";
	}
	
	$sql.=" FROM Plannings_Dates LEFT JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)";
	$sql.=" LEFT JOIN Actions_Sessions ON (Plannings_Dates.pda_id=Actions_Sessions.ase_date)";
	if($action_client>0){
		$sql.=" LEFT OUTER JOIN Actions_Clients_Dates ON (Plannings_Dates.pda_id=Actions_Clients_Dates.acd_date AND acd_action_client=:action_client))";
	}
	$sql.=" WHERE pda_type=1 AND pda_ref_1=:action";
	
	$sql.=" ORDER BY pda_date,pda_demi,ase_h_deb,pda_intervenant;";
	//echo($sql);
	//die();
	$req = $ConnSoc->prepare($sql);
	$req->bindParam(":action",$action);
	if($action_client>0){
		$req->bindParam(":action_client",$action_client);
	}
	$req->execute();
	$dates = $req->fetchAll();
	
    return $dates;
}
