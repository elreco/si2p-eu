<?php


	function affacturage_fichier($remise,$aff_date,$d_factures){

		global $Conn;
		global $ConnSoc;
		global $acc_societe;
		
		error_reporting(E_ALL);


		$return_erreur=array(
			"texte" => "",
			"factures" => array()
		);

		// la societe

		$sql="SELECT soc_nom,soc_tva,soc_contrat_aff FROM Societes WHERE soc_id=" . $acc_societe . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();

		$vide=" ";

		$d_reg_type=array(
			"1" => "CHQ",
			"2" => "VIR",
			"3" => "",
			"4" => "",
			"5" => "MAD",
			"6" => "ESP",
			"7" => "",
			"8" => "",
		);

		// CREATION DU FICHIER

		// on supp l'ancien fichier
		
		$nouvelle_remise = false;
		if(file_exists("documents/Societes/" . $acc_societe . "/Affacturages/remise" . $remise . ".txt")){
			unlink("documents/Societes/" . $acc_societe . "/Affacturages/remise" . $remise . ".txt");

		}else{
			$nouvelle_remise = true;
		}
		$file = fopen("documents/Societes/" . $acc_societe . "/Affacturages/remise" . $remise . ".txt", "w");

		// ECRITURE DE LA PREMIERE LIGNE

		$code="100";
		$code_vendeur=$d_societe["soc_contrat_aff"];
		$nom_vendeur=mb_str_pad($d_societe["soc_nom"],40," ");
		$date_fichier="";
		if(!empty($aff_date)){
			$DT_aff_date=date_create_from_format('Y-m-d',$aff_date);
			if(!is_bool($DT_aff_date)){
				$date_fichier=$DT_aff_date->format("Ymd");
			}
		}
		$resp_nom=mb_str_pad($vide,40,' ',STR_PAD_RIGHT);
		$resp_tel=mb_str_pad($vide,10,' ',STR_PAD_RIGHT);
		$resp_fax=mb_str_pad($vide,10,' ',STR_PAD_RIGHT);
		$client_ident="1";
		$codif_pays="2";

		$tva_vendeur=$d_societe["soc_tva"];
		$tva_vendeur=mb_str_pad($tva_vendeur,16,' ',STR_PAD_RIGHT);

		$num_fichier=mb_str_pad($remise,3,'0',STR_PAD_LEFT);

		$zone_libre=mb_str_pad($vide,213,' ',STR_PAD_RIGHT);
		$devise="EUR";
		$ope_type="000000";

		$ligne=$code . $code_vendeur . $nom_vendeur . $date_fichier . $resp_nom . $resp_tel . $resp_fax . $client_ident . $codif_pays . $tva_vendeur . $num_fichier;
		$ligne.=$zone_libre . $devise . $ope_type . "\r\n";
		$ligne = iconv( mb_detect_encoding( $ligne ), 'Windows-1252', $ligne );
		fwrite($file, $ligne);


		// AJOUT DES FACTURES

		// var pour dernière ligne
		$nb_facture=0;
		$total_facture=0;
		$nb_avoir=0;
		$total_avoir=0;
		$nb_reg=0;
		$total_reg=0;
		$nb_ajust=0;
		$total_ajust=0;
		// requete prep

		$sql_fac="SELECT fac_id,DATE_FORMAT(fac_date,'%Y%m%d') AS fac_date,DATE_FORMAT(fac_date_reg_prev,'%Y%m%d') AS fac_date_reg_prev,fac_numero
		,fac_adresse,fac_nature,fac_client,fac_cli_nom,fac_cli_ad1,fac_cli_ad2,fac_cli_ad3,fac_cli_cp,fac_cli_ville,fac_reg_type,fac_total_ttc
		FROM Factures WHERE fac_id=:facture;";
		$req_fac=$ConnSoc->prepare($sql_fac);

		$sql_get_cli="SELECT cli_siren,adr_siret,cli_code,cli_code FROM Clients,Adresses WHERE cli_id=adr_ref_id AND adr_ref=1 AND adr_id=:adresse;";
		$req_get_cli=$Conn->prepare($sql_get_cli);

		$sql_up_fac="UPDATE Factures SET fac_aff_remise=:remise WHERE fac_id=:facture;";
		$rem_up_fac=$ConnSoc->prepare($sql_up_fac);


		$fac_erreur=array();
		
		// on passe en revu les facture selectionne par l'utilisateur
		foreach($d_factures as $fac){

			$req_fac->bindParam(":facture",$fac["fac_id"]);
			$req_fac->execute();
			$d_facture=$req_fac->fetch();
			if(!empty($d_facture)){

				$erreur="";

				$req_get_cli->bindParam("adresse",$d_facture["fac_adresse"]);
                $req_get_cli->execute();
                $d_client=$req_get_cli->fetch();
				if(empty($d_client)){
					$erreur.="Adresse de facturation non identifiée [ID-" . $d_facture["fac_adresse"] . "] (" . $d_facture["fac_numero"] . ")<br/>";
				}elseif(empty($d_client["cli_siren"]) OR empty($d_client["adr_siret"])){
					$erreur.="L'adresse de facturation de " . $d_client["cli_code"] . " [ADR-ID-" . $d_facture["fac_adresse"] . "] ne dispose pas de SIRET  (" . $d_facture["fac_numero"] . ")<br/>";
				}

				if(!empty($d_client["cli_siren"])){
					$client_ident_1=$d_client["cli_siren"];
					if(!empty($client_ident_1)){
						$client_ident_1=str_replace(" ","",$client_ident_1);
					}
					if(strlen($client_ident_1)!=9){
						$erreur.="SIREN incompatible (" . $d_facture["fac_numero"] . ")<br/>";
					}
				}

				if(!empty($d_client["adr_siret"])){
					$client_ident_2=$d_client["adr_siret"];
					if(!empty($client_ident_2)){
						$client_ident_2=str_replace(" ","",$client_ident_2);
					}
					if(strlen($client_ident_2)!=5){
						$erreur.="NIC incompatible (" . $d_facture["fac_numero"] . ")<br/>";						
					}
				}
				
				if(mb_strlen($d_facture["fac_cli_nom"]) > 40){
					$d_facture["fac_cli_nom"] = mb_substr($d_facture["fac_cli_nom"], 0, 40);
				}

				$entite_nom=mb_str_pad($d_facture["fac_cli_nom"],40,' ',STR_PAD_RIGHT);
				$entite_enseigne=mb_str_pad($vide,40,' ',STR_PAD_RIGHT);

				$entite_ad1="";
				$entite_ad2="";
				if(!empty($d_facture["fac_cli_ad1"])){
					$entite_ad1=$d_facture["fac_cli_ad1"];
					if(!empty($d_facture["fac_cli_ad2"])){
						$entite_ad2=$d_facture["fac_cli_ad2"];
					}elseif(!empty($d_facture["fac_cli_ad3"])){
						$entite_ad2=$d_facture["fac_cli_ad3"];
					}
				}elseif(!empty($d_facture["fac_cli_ad2"])){
					$entite_ad1=$d_facture["fac_cli_ad2"];
					if(!empty($d_facture["fac_cli_ad3"])){
						$entite_ad2=$d_facture["fac_cli_ad3"];
					}
				}elseif(!empty($d_facture["fac_cli_ad3"])){
					$entite_ad1=$d_facture["fac_cli_ad3"];				
				}
				
				if(!empty($entite_ad1)){											
					$entite_ad1=str_replace("","",$entite_ad1);		
					$entite_ad1=str_replace("","-",$entite_ad1);						
					if(strlen($entite_ad1)>40){
						$entite_ad1=mb_substr($entite_ad1,0,40);
					}
					$entite_ad2=str_replace("","",$entite_ad2);	
					$entite_ad2=str_replace("","",$entite_ad2);	
					if(strlen($entite_ad2)>40){
						$entite_ad2=mb_substr($entite_ad2,0,40);
					}
					$entite_ad1=mb_str_pad($entite_ad1,40,' ',STR_PAD_RIGHT);
					$entite_ad2=mb_str_pad($entite_ad2,40,' ',STR_PAD_RIGHT);
				}else{
					$erreur.="Adresse incompatible (" . $d_facture["fac_numero"] . ")<br/>";
				}

				if(!empty($d_facture["fac_cli_cp"])){
					$entite_cp=str_replace(" ","",$d_facture["fac_cli_cp"]);
					if(strlen($entite_cp)>6){
						$entite_cp=mb_substr($entite_cp,0,6);
					}
					$entite_cp=mb_str_pad($entite_cp,6,' ',STR_PAD_RIGHT);
				}else{
					
					$erreur.="CP incompatible (" . $d_facture["fac_numero"] . ")<br/>";
				}

				if(!empty($d_facture["fac_cli_ville"])){
					if(strlen($d_facture["fac_cli_ville"])>34){
						$d_facture["fac_cli_ville"]=mb_substr($d_facture["fac_cli_ville"],0,34);
					}
					$entite_ville=mb_str_pad($d_facture["fac_cli_ville"],34,' ',STR_PAD_RIGHT);
				}else{
					$erreur.="Ville incompatible (" . $d_facture["fac_numero"] . ")<br/>";
				}

				$code_pays="FR ";
				$entite_tel=mb_str_pad($vide,10,' ',STR_PAD_RIGHT);

				if(!empty($d_client["cli_code"])){
					$entite_code_compta="411" . $d_client["cli_code"];
					if(strlen($entite_code_compta)>10){
						$entite_code_compta=mb_substr($entite_code_compta,0,10);
					}else{
						$entite_code_compta=mb_str_pad("411" . $d_client["cli_code"],10,' ',STR_PAD_RIGHT);
					}
				}else{
					$erreur.="CP incompatible (" . $d_facture["fac_numero"] . ")<br/>";
				}

				$piece_date=$d_facture["fac_date"];

				$piece_numero=mb_str_pad($d_facture["fac_numero"],15,' ',STR_PAD_RIGHT);
				
				$piece_montant="";
				if(!empty($d_facture["fac_total_ttc"])){
					$piece_montant=number_format(abs($d_facture["fac_total_ttc"]),2,'','');
					$piece_montant=mb_str_pad($piece_montant,15,0,STR_PAD_LEFT);
				}
				
				// SPE FACTURE AVOIR
				
				if($d_facture["fac_nature"]==2){
					$code="102";
					$piece_signe="-";
					$piece_ope="AVO";
					
					$position_102_297=mb_str_pad($vide,11,' ',STR_PAD_RIGHT);
					$num_avoir=mb_str_pad($d_facture["fac_numero"],15,' ',STR_PAD_RIGHT);
					$position_102_323=mb_str_pad($vide,35,' ',STR_PAD_RIGHT);
					
				}else{
					$code="101";
					$piece_signe="+";
					$piece_ope="FAC";
					
					if(!empty($d_reg_type[$d_facture["fac_reg_type"]])){
						$reg_type=$d_reg_type[$d_facture["fac_reg_type"]];
					}else{
						$erreur.="Mode de règlement incompatible (" . $d_facture["fac_numero"] . ")<br/>";
					}
					$piece_echeance=$d_facture["fac_date_reg_prev"];
					if(empty($piece_echeance)){
						$piece_echeance = $d_facture["fac_date"];
					}
					$piece_bc=mb_str_pad($vide,10,' ',STR_PAD_RIGHT);
					$piece_reference=mb_str_pad($vide,40,' ',STR_PAD_RIGHT);
				}

				


				if(empty($erreur)){
					
					// on ne totailise que s'il n'y a pas d'erreur
					if($d_facture["fac_nature"]==2){
						$nb_avoir++;
						$total_avoir=$total_avoir+$d_facture["fac_total_ttc"];
					}else{							
						$nb_facture++;
						$total_facture=$total_facture+$d_facture["fac_total_ttc"];
						
					}
					
					$ligne=$code . $date_fichier . $code_vendeur . $client_ident_1 . $client_ident_2 . $entite_nom . $entite_enseigne . $entite_ad1 . $entite_ad2 . $entite_cp;
					$ligne.=$entite_ville . $code_pays . $entite_tel . $entite_code_compta . $piece_date . $piece_numero . $devise . $piece_signe . $piece_montant;
					if($d_facture["fac_nature"]==1){
						$ligne.=$reg_type . $piece_echeance . $piece_bc . $piece_reference;
					}else{
						$ligne.=$position_102_297 . $num_avoir . $position_102_323;
					}
					$ligne.=$piece_ope . "\r\n";
					$ligne = iconv( mb_detect_encoding( $ligne ), 'Windows-1252//IGNORE', $ligne );
					
					fwrite($file, $ligne);

				}
				
				if(!empty($erreur)){
					$return_erreur["texte"].=$erreur;
					$return_erreur["factures"][$fac["fac_id"]]=$fac["fac_id"];
					
				}
			
					
			}else{
				$return_erreur["texte"].="Impossible de récupérer les données (Facture ID N°" . $fac["fac_id"] . ")<br/>";
				$return_erreur["factures"][$fac["fac_id"]]=$fac["fac_id"];
			}



		}

		//////////////////////////////////////
		// REGLEMENTS ET AJUSTEMENTS
		/////////////////////////////////////
		if($nouvelle_remise){

			// ON PREND TOUS LES REGLEMENTS NON REMIS LIES A UNE FACTURE DEJA REMISES (inclut la remise en cours)
			$sql_reg="SELECT fac_id,DATE_FORMAT(fac_date,'%Y%m%d') AS fac_date,DATE_FORMAT(fac_date_reg_prev,'%Y%m%d') AS fac_date_reg_prev,fac_numero
			,fac_adresse,fac_nature,fac_cli_nom,fac_cli_ad1,fac_cli_ad2,fac_cli_ad3,fac_cli_cp,fac_cli_ville,fac_reg_type,fac_total_ttc 
			,reg_id, reg_facture,DATE_FORMAT(reg_date,'%Y%m%d') AS reg_date,reg_montant
			,reg_type,reg_reference,reg_banque 
			FROM factures,reglements WHERE fac_id=reg_facture AND fac_aff_remise>0 AND reg_aff_remise=0;";
			$req_reg=$ConnSoc->prepare($sql_reg);
			$req_reg->execute();
			$d_factures_reg=$req_reg->fetchAll();

		}else{

			$sql_reg="SELECT fac_id,DATE_FORMAT(fac_date,'%Y%m%d') AS fac_date,DATE_FORMAT(fac_date_reg_prev,'%Y%m%d') AS fac_date_reg_prev,fac_numero
			,fac_adresse,fac_nature,fac_cli_nom,fac_cli_ad1,fac_cli_ad2,fac_cli_ad3,fac_cli_cp,fac_cli_ville,fac_reg_type,fac_total_ttc
			,reg_id,reg_facture,DATE_FORMAT(reg_date,'%Y%m%d') AS reg_date,reg_montant,DATE_FORMAT(reg_date_enr,'%Y%m%d') AS reg_date_enr
			,reg_type,reg_reference,reg_banque 
			FROM reglements,factures WHERE fac_id=reg_facture AND reg_aff_remise=" . $remise;
			$req_reg=$ConnSoc->prepare($sql_reg);
			$req_reg->execute();
			$d_factures_reg=$req_reg->fetchAll();

		}
	
		
		if(!empty($d_factures_reg)){
			
			
			foreach($d_factures_reg as $k_reg => $d_reglement){
			
				$erreur="";
				
				// securite au cas ou le reg soit lié a une facture que l'on vient de tenter de remettre et qui a générer une erreur
				if(!isset($return_erreur["factures"][$d_reglement["fac_id"]])){

					$req_get_cli->bindParam("adresse",$d_reglement["fac_adresse"]);
					$req_get_cli->execute();
					$d_client=$req_get_cli->fetch();

					if(empty($d_client["adr_siret"])){
						$sql_get_cli_2="SELECT cli_siren,adr_siret,cli_code,cli_code FROM Clients LEFT JOIN Adresses ON Adresses.adr_ref_id = Clients.cli_id
						WHERE adr_ref=1 AND adr_defaut=1 AND adr_ref_id = :fac_client;";
						$req_get_cli_2=$Conn->prepare($sql_get_cli_2);
						$req_get_cli_2->bindParam("adresse",$d_reglement["fac_client"]);
						$req_get_cli_2->execute();
						$d_client=$req_get_cli_2->fetch();
					}
					$client_ident_1=$d_client["cli_siren"];
					if(!empty($client_ident_1)){
						$client_ident_1=str_replace(" ","",$client_ident_1);
					}
					if(strlen($client_ident_1)!=9){
						$erreur.="SIREN incompatible (" . $d_reglement["fac_numero"] . ")<br/>";
					}

					$client_ident_2=$d_client["adr_siret"];
					if(!empty($client_ident_2)){
						$client_ident_2=str_replace(" ","",$client_ident_2);
					}

					if(strlen($client_ident_2)!=5){
						$erreur.="NIC incompatible (" . $d_reglement["fac_numero"] . ")<br/>";						
					}
					if(mb_strlen($d_reglement["fac_cli_nom"]) > 40){
						$d_reglement["fac_cli_nom"] = mb_substr($d_reglement["fac_cli_nom"], 0, 40);
					}

					$entite_nom=mb_str_pad($d_reglement["fac_cli_nom"],40,' ',STR_PAD_RIGHT);
					$entite_enseigne=mb_str_pad($vide,40,' ',STR_PAD_RIGHT);

					$entite_ad1="";
					$entite_ad2="";
					if(!empty($d_reglement["fac_cli_ad1"])){
						$entite_ad1=$d_reglement["fac_cli_ad1"];
						if(!empty($d_reglement["fac_cli_ad2"])){
							$entite_ad2=$d_reglement["fac_cli_ad2"];
						}elseif(!empty($d_reglement["fac_cli_ad3"])){
							$entite_ad2=$d_reglement["fac_cli_ad3"];
						}
					}elseif(!empty($d_reglement["fac_cli_ad2"])){
						$entite_ad1=$d_reglement["fac_cli_ad2"];
						if(!empty($d_reglement["fac_cli_ad3"])){
							$entite_ad2=$d_reglement["fac_cli_ad3"];
						}
					}elseif(!empty($d_reglement["fac_cli_ad3"])){
						$entite_ad1=$d_reglement["fac_cli_ad3"];						
					}
					
					if(!empty($entite_ad1)){											
						$entite_ad1=str_replace("","'",$entite_ad1);	
						$entite_ad1=str_replace("’","'",$entite_ad1);	
						$entite_ad1=str_replace("","-",$entite_ad1);
				
						if(strlen($entite_ad1)>40){
							$entite_ad1=mb_substr($entite_ad1,0,40);
						}
						$entite_ad2=str_replace("","'",$entite_ad2);	
						$entite_ad2=str_replace("’","'",$entite_ad2);	
						$entite_ad2=str_replace("","-",$entite_ad2);
						
						if(strlen($entite_ad2)>40){
							$entite_ad2=mb_substr($entite_ad2,0,40);
							
						}
						
						
						$entite_ad1=mb_str_pad($entite_ad1,40,' ',STR_PAD_RIGHT);
						$entite_ad2=mb_str_pad($entite_ad2,40,' ',STR_PAD_RIGHT);
					}else{
						$erreur.="Adresse incompatible (" . $d_facture["fac_numero"] . ")<br/>";
					}

					if(!empty($d_reglement["fac_cli_cp"])){
						$entite_cp=mb_str_pad($d_reglement["fac_cli_cp"],6,' ',STR_PAD_RIGHT);
					}else{
						$erreur.="CP incompatible (" . $d_reglement["fac_numero"] . ")<br/>";
					}

					if(!empty($d_reglement["fac_cli_ville"])){
						$entite_ville=mb_str_pad($d_reglement["fac_cli_ville"],34,' ',STR_PAD_RIGHT);
					}else{
						$erreur.="CP incompatible (" . $d_reglement["fac_numero"] . ")<br/>";
					}

					$code_pays="FR ";
					$entite_tel=mb_str_pad($vide,10,' ',STR_PAD_RIGHT);

					if(!empty($d_client["cli_code"])){
						$entite_code_compta="411" . $d_client["cli_code"];
						if(strlen($entite_code_compta)>10){
							$entite_code_compta=mb_substr($entite_code_compta,0,10);
						}else{
							$entite_code_compta=mb_str_pad("411" . $d_client["cli_code"],10,' ',STR_PAD_RIGHT);
						}
					}else{
						$erreur.="CP incompatible (" . $d_reglement["fac_numero"] . ")<br/>";
					}
					$piece_numero=mb_str_pad($d_reglement["fac_numero"],15,' ',STR_PAD_RIGHT);

					
					if($d_reglement['reg_type'] != 7){
						// CE N'EST PAS UN AJUSTEMENT
						
						$code="103";
						
						//Position 297 la date d’arrivée des fonds en banque 
						$piece_echeance=$d_reglement["reg_date"];
						if(!empty($d_reg_type[$d_reglement["reg_type"]])){
							$reg_type=mb_str_pad($d_reg_type[$d_reglement["reg_type"]],3,' ',STR_PAD_RIGHT);
						}else{
							$reg_type=mb_str_pad("VIR",3,' ',STR_PAD_RIGHT);
						}
						if($d_reglement["fac_nature"]==2){
							$piece_signe="+";
						}else{
							$piece_signe="-";
						}
						
						$position_103_305=mb_str_pad($vide,13,' ',STR_PAD_RIGHT);
					}else{
						// C'EST UN AJUSTEMENT
						
						$code="104";
						
						//Position 297 la date d’arrivée des fonds en banque 
						// = date du relevé
						$piece_echeance = mb_str_pad($vide,8,' ',STR_PAD_RIGHT);
						if(empty($d_reglement["reg_type"])){
							$reg_type=mb_str_pad("VIR",3,' ',STR_PAD_RIGHT);
						}else{
							$reg_type=mb_str_pad($d_reglement["reg_type"],3,' ',STR_PAD_RIGHT);
						}
						if(!empty($d_reglement["reg_montant"])){
							if($d_reglement["reg_montant"] > 0){
								$reg_type = "AJC";
								$piece_signe = "-";
							}else{
								$reg_type = "AJD";
								$piece_signe = "+";
							}
						}
						
						$position_104_297=mb_str_pad($vide,21,' ',STR_PAD_RIGHT);
						
					}

					//Position 255 la date du lettrage de ce virement 
					// = date de la saisie dur ORION
					//if(!empty($d_reglement["reg_date_enr"])){
					//	$piece_date=$d_reglement["reg_date_enr"];
					//}else{
						$piece_date=$d_reglement["reg_date"];
					//}
					
					
					$piece_montant="";
					if(!empty($d_reglement["reg_montant"])){
						$piece_montant=number_format(abs($d_reglement["reg_montant"]),2,'','');
						$piece_montant=mb_str_pad($piece_montant,15,0,STR_PAD_LEFT);
					}


					$observations=mb_str_pad($vide,40,' ',STR_PAD_RIGHT);	

					if(empty($erreur)){
						
						// on totalise que s'il n'y a pas d'erreur
						
						if($d_reglement['reg_type'] != 7){
							// CE N'EST PAS UN AJUSTEMENT
							$nb_reg++;
							$total_reg=$total_reg+$d_reglement["reg_montant"];
						}else{
							// C'EST UN AJUSTEMENT
							$nb_ajust++;
							$total_ajust=$total_ajust+$d_reglement["reg_montant"];				
						}

						$ligne=$code . $date_fichier . $code_vendeur . $client_ident_1 . $client_ident_2 . $entite_nom . $entite_enseigne . $entite_ad1 . $entite_ad2 . $entite_cp;
						$ligne.=$entite_ville . $code_pays . $entite_tel . $entite_code_compta . $piece_date . $piece_numero . $devise . $piece_signe . $piece_montant;
						if($d_reglement['reg_type'] != 7){
							// reglement 103
							$ligne.=$piece_echeance . $position_103_305;
						}else{
							// ajustement 104
							$ligne.=$position_104_297;
						}
						$ligne.=$observations. $reg_type . "\r\n";					
								
						$ligne = iconv( mb_detect_encoding( $ligne ), 'Windows-1252//IGNORE', $ligne );
						fwrite($file, $ligne);
				
						
						
						
						// ON memorise que le reglement est lié à la remise
						$sql_up_reg="UPDATE Reglements SET reg_aff_remise=:remise WHERE reg_id=" . $d_reglement['reg_id'];
						$req_up_reg=$ConnSoc->prepare($sql_up_reg);
						$req_up_reg->bindParam(":remise",$remise);
						$req_up_reg->execute();

					}else{
						$return_erreur["texte"].=$erreur;
					}
				}
			}
		}
		// ON ECRIT LA DERNIERE LIGNE
		$code="199";

		$nb_facture=mb_str_pad($nb_facture,4,'0',STR_PAD_LEFT);

		$total_facture=number_format($total_facture,2,'','');
		$total_facture=mb_str_pad($total_facture,15,'0',STR_PAD_LEFT);

		$nb_avoir=mb_str_pad($nb_avoir,4,'0',STR_PAD_LEFT);
		$total_avoir=number_format(abs($total_avoir),2,'','');
		$total_avoir=mb_str_pad($total_avoir,15,'0',STR_PAD_LEFT);

		$nb_reg=mb_str_pad($nb_reg,4,'0',STR_PAD_LEFT);
		$total_reg=number_format(abs($total_reg),2,'','');
		$total_reg=mb_str_pad($total_reg,15,'0',STR_PAD_LEFT);

		$nb_ajust=mb_str_pad($nb_ajust,4,'0',STR_PAD_LEFT);
		$total_ajust=number_format(abs($total_ajust),2,'','');
		$total_ajust=mb_str_pad($total_ajust,15,'0',STR_PAD_LEFT);

		$nb_garantie=mb_str_pad("0",4,'0',STR_PAD_LEFT);
		$total_garantie=number_format($nb_garantie,2,'','');
		$total_garantie=mb_str_pad("0",15,'0',STR_PAD_LEFT);

		$zone_libre=mb_str_pad($vide,199,' ',STR_PAD_RIGHT);

		$ligne=$code . $code_vendeur . $nom_vendeur . $date_fichier . $nb_facture .$total_facture . $nb_avoir . $total_avoir . $nb_reg . $total_reg . $nb_ajust . $total_ajust;
		$ligne.=$nb_garantie . $total_garantie . $zone_libre . $devise . $ope_type;
		$ligne = iconv( mb_detect_encoding( $ligne ), 'Windows-1252', $ligne );
		fwrite($file, $ligne);

		fclose($file);

		if(empty($return_erreur["texte"])){
			return true;
		}else{
			
			return $return_erreur;
		}

	} ?>
