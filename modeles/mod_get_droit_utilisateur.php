<?php
// valeur d'affectation d'un droit pour un utilisateur donnée
	function get_droit_utilisateur($droit,$utilisateur){
		
		global $Conn;	
	
		$req = $Conn->prepare("SELECT udr_reaffecte FROM Utilisateurs_droits WHERE udr_droit=:udr_droit AND udr_utilisateur=:udr_utilisateur");
		$req->bindParam(':udr_droit', $droit);
		$req->bindParam(':udr_utilisateur', $utilisateur);
		$req->execute();
		$donnees=$req->fetch();
		return $donnees;	
	}
?>
	