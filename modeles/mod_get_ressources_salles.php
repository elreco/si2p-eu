<?php
// retourne la liste des salles dispo sur la periode $date (array)
	function get_ressources_salles($dates,$agence,$action){
	
		global $ConnSoc;

		$sql="SELECT DISTINCT sal_id,sal_nom,pda_id FROM Salles LEFT JOIN Plannings_Dates ON (Salles.sal_id=Plannings_Dates.pda_salle
		AND (NOT pda_type=1 OR (pda_type=1 AND NOT pda_ref_1=" . $action  . ")) AND NOT pda_archive";
		if(!empty($d_dates)){
			$sql.=" AND (";
			foreach($d_dates as $d){
				$sql.=" (pda_date=" . $d[0] . " AND pda_demi=" . $d[1] . ") OR";
			}
			$sql=substr($sql,0,-3);
			$sql.=" )";
		}
		$sql.=" ) WHERE ISNULL(pda_id)";
		if(!empty($agence)){
			$sql.=" AND sal_agence=" . $agence;
		}
		$req=$ConnSoc->query($sql);
		$d_salles=$req->fetchAll();
		
		return $d_salles;
	}
?>
			