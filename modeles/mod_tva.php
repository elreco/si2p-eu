<?php

	// CONTENANT LES VARIABLE SYSTEME QUI NE SONT PAS STOCKE EN DB
	
	$base_tva=array(
		0 => "&nbsp;",
		1 => "Normale",
		2 => "Réduit alimentaire",
		3 => "Sans TVA",
		4 => "Réduit",
		5 => "Spé"
		);

	
	function get_tva_periode($tva_periode){
		
		global $Conn;
		
		$sql="SELECT * FROM tva_periodes WHERE tpe_id=" . $tva_periode;
		$req = $Conn->query($sql);			
		$get_tva_periode=$req->fetch();
		
		return $get_tva_periode;
		
	}
	
	function get_tvas_periode($tva = 0){
		
		global $Conn;
		
		$sql="SELECT * FROM tva_periodes";
		if($tva>0){
			$sql=$sql . " WHERE tpe_tva=" . $tva;
		};
		$sql=$sql . " ORDER BY tpe_date_deb,tpe_taux";
		$req = $Conn->query($sql);
		$get_tvas_periode=$req->fetchAll();
		
		return $get_tvas_periode;
		
	}
	
	function insert_tva_periode($tpe_tva,$tpe_taux,$tpe_date_deb,$tpe_cpt_ven,$tpe_cpt_ach){
		
		global $Conn;

		$sql="INSERT INTO Tva_Periodes(tpe_tva,tpe_taux,tpe_date_deb,tpe_cpt_ven,tpe_cpt_ach) VALUES (:tpe_tva,:tpe_taux,:tpe_date_deb,:tpe_cpt_ven,:tpe_cpt_ach)";
		$req=$Conn->prepare($sql);
		$req->bindParam(':tpe_tva',$tpe_tva);
		$req->bindParam(':tpe_taux',$tpe_taux);
		$req->bindParam(':tpe_date_deb',$tpe_date_deb);
		$req->bindParam(':tpe_cpt_ven',$tpe_cpt_ven);
		$req->bindParam(':tpe_cpt_ach',$tpe_cpt_ach);
		$req->execute();
	}
	
	function update_tva_periode($tpe_id,$tpe_tva,$tpe_taux,$tpe_date_deb,$tpe_cpt_ven,$tpe_cpt_ach){
		
		global $Conn;

		$sql="UPDATE Tva_Periodes SET tpe_tva=:tpe_tva, tpe_taux=:tpe_taux, tpe_date_deb=:tpe_date_deb, tpe_cpt_ven=:tpe_cpt_ven, tpe_cpt_ach=:tpe_cpt_ach WHERE tpe_id=:tpe_id";
		$req=$Conn->prepare($sql);
		$req->bindParam(':tpe_id',$tpe_id);
		$req->bindParam(':tpe_tva',$tpe_tva);
		$req->bindParam(':tpe_taux',$tpe_taux);
		$req->bindParam(':tpe_date_deb',$tpe_date_deb);
		$req->bindParam(':tpe_cpt_ven',$tpe_cpt_ven);
		$req->bindParam(':tpe_cpt_ach',$tpe_cpt_ach);
		$req->execute();
	}
	
	function update_tva_periode_fin($tpe_id,$tpe_date_fin){
		
		global $Conn;
		
		$sql="UPDATE Tva_Periodes SET tpe_date_fin=:tpe_date_fin WHERE tpe_id=:tpe_id";
		$req=$Conn->prepare($sql);
		$req->bindParam(':tpe_id',$tpe_id);
		$req->bindParam(':tpe_date_fin',$tpe_date_fin);
		$req->execute();
	}
	
?>	
