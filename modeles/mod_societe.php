<?php


/*--------------------------------------------
	FONCTION DE SELECTION 
----------------------------------------------*/

function get_societes()
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM societes");
    $req->execute();

    $societes = $req->fetchAll();

    return $societes;
}
function get_societe($id)
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM societes WHERE soc_id=" . $id);
    $req->execute();

    $societe = $req->fetch();

    return $societe;
}

function get_societe_select($selected)
{

    global $Conn;

    $service_select = "";
    $req = $Conn->query("SELECT * FROM societes ORDER BY soc_nom");
	
    while ($donnees = $req->fetch()) {
	
        if($selected == 0){
            $service_select = $service_select . "<option value='" . $donnees["soc_id"] . "' >" . $donnees["soc_nom"] . "</option>";
        }else{
            if($donnees['soc_id'] == $selected){
                $service_select = $service_select . "<option selected value='" . $donnees["soc_id"] . "' >" . $donnees["soc_nom"] . "</option>";
            }else{
                $service_select = $service_select . "<option value='" . $donnees["soc_id"] . "' >" . $donnees["soc_nom"] . "</option>";
            }   
        }
    }
    
    return $service_select;

}
	// retourne les societe qui gere une agence
	
	function get_societe_agence_select($selected = 0)
	{
		global $Conn;
		
		$service_select = "";
		$req = $Conn->query("SELECT * FROM societes WHERE soc_agence ORDER BY soc_nom");
		while ($donnees = $req->fetch()) {
			if($selected == 0){
				$service_select = $service_select . "<option value='" . $donnees["soc_id"] . "' >" . $donnees["soc_nom"] . "</option>";
			}else{
				if($donnees['soc_id'] == $selected){
					$service_select = $service_select . "<option selected value='" . $donnees["soc_id"] . "' >" . $donnees["soc_nom"] . "</option>";
				}else{
					$service_select = $service_select . "<option value='" . $donnees["soc_id"] . "' >" . $donnees["soc_nom"] . "</option>";
				}
			}
		}

		return $service_select;
	}
	
	// retourne la liste des sociétes avec la valeurs d'accès pour l'utilisateur passer en parametre
	function get_societes_utilisateur($utilisateur,$strict){
		
		global $Conn;
		
		$sql="SELECT soc_id,soc_code,soc_nom,soc_agence,uso_utilisateur,uso_agence FROM Societes";
		if(!$strict){
			$sql.=" LEFT OUTER JOIN Utilisateurs_Societes on (Societes.soc_id=Utilisateurs_Societes.uso_societe AND uso_utilisateur=:utilisateur AND uso_agence = 0)";
				
		}else{
			$sql.=" LEFT OUTER JOIN Utilisateurs_Societes on (Societes.soc_id=Utilisateurs_Societes.uso_societe)";
			$sql.=" WHERE uso_utilisateur=:utilisateur"; 					
		}
		$sql.=" ORDER BY soc_code;";
		$req=$Conn ->prepare($sql);
		$req->bindParam(":utilisateur",$utilisateur);
		$req->execute();
		$societes = $req->fetchAll();
		return $societes;

	}



	


