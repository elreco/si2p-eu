<?php

/* RETOURNES LES DONNEES DE LA TABLE Stagiaires

AUTEUR FG 03/11/2016
*/

function get_stagiaire($stagiaire){

	global $ConnSoc;

	$sql="SELECT * FROM Stagiaires WHERE sta_id=:stagiaire;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":stagiaire",$stagiaire);
	$req->execute();
	$sta=$req->fetch();
	if(!empty($sta)){
		return $sta;
	}
}
