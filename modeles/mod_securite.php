<?php

// FONCTION DE SECURISATION D'ORION
// NE SERT A RIEN 
// il faut utiliser des requete prepare  ou $conn->quote($sql);

function securite_bdd($string)
{ 	// On regarde si le type de string est un nombre entier (int)
	if(ctype_digit($string))
	{
		$string = intval($string);
	}else{
		// Pour tous les autres types
		$string = quote($string);
		$string = addcslashes($string, '%_');
	}
	return $string;
}
?>
