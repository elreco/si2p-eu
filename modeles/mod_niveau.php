<?php
//include_once '/connexion.php';
function get_niveaux()
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_niveaux");
    $req->execute();

    $niveaux = $req->fetchAll();

    return $niveaux;
}
function get_niveau($id)
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_niveaux WHERE pni_id = " . $id);
    $req->execute();

    $niveau = $req->fetch();

    return $niveau;
}
function insert_niveau()
{

    global $Conn;

    // fin upload fichier
    if (isset($_POST['pni_id'])) {
    	$req = $Conn->prepare("REPLACE INTO produits_niveaux (pni_id,pni_libelle) VALUES (" . $_POST['pni_id'] . ", :pni_libelle)");
    }else{
    	$req = $Conn->prepare("INSERT INTO produits_niveaux (pni_libelle) VALUES (:pni_libelle)");
      }  
    $req->bindParam("pni_libelle",$_POST['pni_libelle']);
    $req->execute();

    header('Location: /niveau_liste.php');
    

}
function get_niveau_select($selected){

  global $Conn;

  $service_select="";
  $req=$Conn->query("SELECT * FROM produits_niveaux ORDER BY pni_libelle");
  while ($donnees = $req->fetch())
  {
    if($selected == 0){
            $service_select=$service_select . "<option value='" . $donnees["pni_id"] . "' >" . $donnees["pni_libelle"] . "</option>";
        }else{
            if($donnees['pni_id'] == $selected){
               $service_select=$service_select . "<option selected value='" . $donnees["pni_id"] . "' >" . $donnees["pni_libelle"] . "</option>";
            }else{
                $service_select=$service_select . "<option value='" . $donnees["pni_id"] . "' >" . $donnees["pni_libelle"] . "</option>";
            }
           
        }
    
  }
  return $service_select;
  }