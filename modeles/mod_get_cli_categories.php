<?php

// RETOURNE LES FAMILLES DE PRODUITS
// AUTEUR FG

function get_cli_categories($droits=0){

	global $Conn;

	$sql="SELECT cca_id,cca_libelle,cca_gc FROM Clients_Categories";
	$sql.=" ORDER BY cca_libelle";
	$req = $Conn->prepare($sql);
	$req->execute();
	$categories = $req->fetchAll();

    return $categories;
}
