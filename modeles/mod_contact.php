<?php
function insert_contact($ref_id, $ref, $fonction, $fonction_nom, $titre, $nom, $prenom, $tel, $portable, $fax, $mail, $defaut, $compta){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("INSERT INTO contacts (con_ref, con_ref_id, con_fonction, con_fonction_nom, con_titre, con_nom, con_prenom, con_tel, con_portable, con_fax, con_mail, con_defaut, con_compta) VALUES (1, :con_ref_id, :con_fonction, :con_fonction_nom, :con_titre, :con_nom, :con_prenom, :con_tel, :con_portable, :con_fax, :con_mail, :con_defaut, :con_compta);");

	$req->bindParam("con_ref_id",$ref_id);
	$req->bindParam("con_fonction",$fonction);
	$req->bindParam("con_fonction_nom",$fonction_nom);
	$req->bindParam("con_titre",$titre); 
	$req->bindParam("con_nom",$nom);
	$req->bindParam("con_prenom",$prenom);
	$req->bindParam("con_tel",$tel);
	$req->bindParam("con_portable",$portable);
	$req->bindParam("con_fax",$fax);
	$req->bindParam("con_mail",$mail);
	$req->bindParam("con_defaut",$defaut);
	$req->bindParam("con_compta",$compta);


	$req->execute();
	$last_adr = $ConnSoc->lastInsertId();
	if($defaut == 1){
		$req = $ConnSoc->prepare("UPDATE clients SET cli_con_defaut = " . $last_adr . "  WHERE cli_id = " . $ref_id);
		$req->execute();
	}

}
function insert_suspect_contact($ref_id, $ref, $fonction, $fonction_nom, $titre, $nom, $prenom, $tel, $portable, $fax, $mail, $defaut, $compta){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("INSERT INTO suspects_contacts (sco_ref, sco_ref_id, sco_fonction, sco_fonction_nom, sco_titre, sco_nom, sco_prenom, sco_tel, sco_portable, sco_fax, sco_mail, sco_defaut, sco_compta) VALUES (1, :sco_ref_id, :sco_fonction, :sco_fonction_nom, :sco_titre, :sco_nom, :sco_prenom, :sco_tel, :sco_portable, :sco_fax, :sco_mail, :sco_defaut, :sco_compta);");


	$req->bindParam("sco_ref_id",$ref_id);
	$req->bindParam("sco_fonction",$fonction);
	$req->bindParam("sco_fonction_nom",$fonction_nom);
	$req->bindParam("sco_titre",$titre);
	$req->bindParam("sco_nom",$nom);
	$req->bindParam("sco_prenom",$prenom);
	$req->bindParam("sco_tel",$tel);
	$req->bindParam("sco_portable",$portable);
	$req->bindParam("sco_fax",$fax);
	$req->bindParam("sco_mail",$mail);
	$req->bindParam("sco_defaut",$defaut);
	$req->bindParam("sco_compta",$compta);
	$req->execute();

	$last = $ConnSoc->lastInsertId();

		return $last;

}
function insert_client_contact($ref_id, $ref, $fonction, $fonction_nom, $titre, $nom, $prenom, $tel, $portable, $fax, $mail, $defaut, $compta){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("INSERT INTO contacts (con_ref, con_ref_id, con_fonction, con_fonction_nom, con_titre, con_nom, con_prenom, con_tel, con_portable, con_fax, con_mail, con_defaut, con_compta) VALUES (1, :con_ref_id, :con_fonction, :con_fonction_nom, :con_titre, :con_nom, :con_prenom, :con_tel, :con_portable, :con_fax, :con_mail, :con_defaut, :con_compta);");


	$req->bindParam("con_ref_id",$ref_id);
	$req->bindParam("con_fonction",$fonction);
	$req->bindParam("con_fonction_nom",$fonction_nom);
	$req->bindParam("con_titre",$titre);
	$req->bindParam("con_nom",$nom);
	$req->bindParam("con_prenom",$prenom);
	$req->bindParam("con_tel",$tel);
	$req->bindParam("con_portable",$portable);
	$req->bindParam("con_fax",$fax);
	$req->bindParam("con_mail",$mail);
	$req->bindParam("con_defaut",$defaut);
	$req->bindParam("con_compta",$compta);
	$req->execute();

	$last = $ConnSoc->lastInsertId();

		return $last;

}

function update_contact_suspect($id, $fonction, $fonction_nom, $titre, $nom, $prenom, $tel, $portable, $fax, $mail, $defaut){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE suspects_contacts SET sco_fonction = :con_fonction, sco_fonction_nom = :con_fonction_nom, sco_titre = :con_titre, sco_nom = :con_nom, sco_prenom = :con_prenom, sco_tel = :con_tel,sco_portable = :con_portable, sco_mail = :con_mail, sco_fax= :con_fax, sco_defaut = :con_defaut WHERE sco_id = :con_id;");

	$req->bindParam("con_fonction",$fonction);
	$req->bindParam("con_fonction_nom",$fonction_nom);
	$req->bindParam("con_titre",$titre);
	$req->bindParam("con_nom",$nom);
	$req->bindParam("con_prenom",$prenom);
	$req->bindParam("con_tel",$tel);
	$req->bindParam("con_portable",$portable);
	$req->bindParam("con_fax",$fax);
	$req->bindParam("con_mail",$mail);
	$req->bindParam("con_defaut",$defaut);
	$req->bindParam("con_id",$id);


	$req->execute();

}
function update_contact_suspect_2($id, $fonction, $fonction_nom, $titre, $nom, $prenom, $tel, $portable, $fax, $mail){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE suspects_contacts SET sco_fonction = :con_fonction, sco_fonction_nom = :con_fonction_nom, sco_titre = :con_titre, sco_nom = :con_nom, sco_prenom = :con_prenom, sco_tel = :con_tel,sco_portable = :con_portable, sco_mail = :con_mail, sco_fax= :con_fax WHERE sco_id = :con_id;");

	$req->bindParam("con_fonction",$fonction);
	$req->bindParam("con_fonction_nom",$fonction_nom);
	$req->bindParam("con_titre",$titre);
	$req->bindParam("con_nom",$nom);
	$req->bindParam("con_prenom",$prenom);
	$req->bindParam("con_tel",$tel);
	$req->bindParam("con_portable",$portable);
	$req->bindParam("con_fax",$fax);
	$req->bindParam("con_mail",$mail);
	$req->bindParam("con_id",$id);


	$req->execute();

}

function update_contact_client($id, $fonction, $fonction_nom, $titre, $nom, $prenom, $tel, $portable, $fax, $mail, $defaut){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE contacts SET con_fonction = :con_fonction, con_fonction_nom = :con_fonction_nom, con_titre = :con_titre, con_nom = :con_nom, con_prenom = :con_prenom, con_tel = :con_tel,con_portable = :con_portable, con_mail = :con_mail, con_fax= :con_fax, con_defaut = :con_defaut WHERE con_id = :con_id;");

	$req->bindParam("con_fonction",$fonction);
	$req->bindParam("con_fonction_nom",$fonction_nom);
	$req->bindParam("con_titre",$titre);
	$req->bindParam("con_nom",$nom);
	$req->bindParam("con_prenom",$prenom);
	$req->bindParam("con_tel",$tel);
	$req->bindParam("con_portable",$portable);
	$req->bindParam("con_fax",$fax);
	$req->bindParam("con_mail",$mail);
	$req->bindParam("con_defaut",$defaut);
	$req->bindParam("con_id",$id);


	$req->execute();

}
function update_contact_client_2($id, $fonction, $fonction_nom, $titre, $nom, $prenom, $tel, $portable, $fax, $mail){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE contacts SET con_fonction = :con_fonction, con_fonction_nom = :con_fonction_nom, con_titre = :con_titre, con_nom = :con_nom, con_prenom = :con_prenom, con_tel = :con_tel,con_portable = :con_portable, con_mail = :con_mail, con_fax= :con_fax WHERE con_id = :con_id;");

	$req->bindParam("con_fonction",$fonction);
	$req->bindParam("con_fonction_nom",$fonction_nom);
	$req->bindParam("con_titre",$titre);
	$req->bindParam("con_nom",$nom);
	$req->bindParam("con_prenom",$prenom);
	$req->bindParam("con_tel",$tel);
	$req->bindParam("con_portable",$portable);
	$req->bindParam("con_fax",$fax);
	$req->bindParam("con_mail",$mail);
	$req->bindParam("con_id",$id);


	$req->execute();

}

function get_contact_fonction_select($selected){
	global $Conn;

		$client_select="";
		
		$sql="SELECT * FROM contacts_fonctions";
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["cfo_id"] . "' >" . $donnees["cfo_libelle"] . "</option>";
			}else{
				if($donnees['cfo_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["cfo_id"] . "' >" . $donnees["cfo_libelle"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["cfo_id"] . "' >" . $donnees["cfo_libelle"] . "</option>";
				}

			}
		}
		return $client_select;
}
function update_contact_defaut($id,$con_defaut){
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE contacts SET con_defaut = :con_defaut WHERE con_id = :con_id;");

	$req->bindParam("con_id",$id);
	$req->bindParam("con_defaut",$adr_defaut);
	$req->execute();

}
function update_contact_suspect_defaut($id,$con_defaut){
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE suspects_contacts SET sco_defaut = :con_defaut WHERE sco_id = :con_id;");

	$req->bindParam("con_id",$id);
	$req->bindParam("con_defaut",$adr_defaut);
	$req->execute();

}
function update_contact_client_defaut($id,$con_defaut){
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE contacts SET con_defaut = :con_defaut WHERE con_id = :con_id;");

	$req->bindParam("con_id",$id);
	$req->bindParam("con_defaut",$adr_defaut);
	$req->execute();

}
function get_contact($ref_id, $ref){
		
	global $ConnSoc;	
	if($ref == null){
		$ref = 0;
	}
	$req_sql="SELECT * FROM Contacts WHERE con_defaut = 1 AND con_ref = 1 AND con_ref_id = " . $ref_id;
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetch();
	return $get_compte;
}
function delete_suspect_contact($id){


	global $ConnSoc;

	$req = $ConnSoc->prepare("DELETE FROM suspects_contacts WHERE sco_id = :sco_id;");

	$req->bindParam("sco_id",$id);
	$req->execute();

}
function get_fonction($id){
		
	global $Conn;	
	if($id == null){
		$id = 0;
	}
	$req_sql="SELECT * FROM contacts_fonctions WHERE cfo_id = $id";
	$req=$Conn->query($req_sql);
	$get_compte = $req->fetch();
	return $get_compte;
}

function get_contact_suspect($ref_id, $ref){
		
	global $ConnSoc;	
	if($ref == null){
		$ref = 0;
	}
	$req_sql="SELECT * FROM suspects_contacts WHERE sco_defaut = 1 AND sco_ref = 1 AND sco_ref_id = " . $ref_id;
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetch();
	return $get_compte;
}
function get_contact_client($ref_id, $ref){
		
	global $ConnSoc;	
	if($ref == null){
		$ref = 0;
	}
	$req_sql="SELECT * FROM contacts WHERE con_defaut = 1 AND con_ref = 1 AND con_ref_id = " . $ref_id;
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetch();
	return $get_compte;
}
function get_contact_suspect_unique($ref_id){
		
	global $ConnSoc;	
	if($ref_id == null){
		$ref_id = 0;
	}
	$req_sql="SELECT * FROM suspects_contacts WHERE sco_id = " . $ref_id;
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetch();
	return $get_compte;
}
function get_contact_client_unique($ref_id){
		
	global $ConnSoc;	
	if($ref_id == null){
		$ref_id = 0;
	}
	$req_sql="SELECT * FROM contacts WHERE con_id = " . $ref_id;
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetch();
	return $get_compte;
}
function get_contact_suspect_select($ref_id, $ref, $selected){
	global $ConnSoc;

		$client_select="";
		
		$sql="SELECT * FROM suspects_contacts WHERE sco_ref_id = $ref_id AND sco_ref = 1";
		
		$req=$ConnSoc->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["sco_id"] . "' >" . $donnees["sco_nom"] . " " . $donnees["sco_prenom"] . "</option>";
			}else{
				if($donnees['sco_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["sco_id"] . "' >" . $donnees["sco_nom"] . " " . $donnees["sco_prenom"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["sco_id"] . "' >" . $donnees["sco_nom"] . " " . $donnees["sco_prenom"] . "</option>";
				}

			}
		}
		return $client_select;
}
function get_contact_client_select($ref_id, $ref, $selected){
	global $ConnSoc;

		$client_select="";
		
		$sql="SELECT * FROM contacts WHERE con_ref_id = $ref_id AND con_ref = 1";
		
		$req=$ConnSoc->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["con_id"] . "' >" . $donnees["con_nom"] . " " . $donnees["con_prenom"] . "</option>";
			}else{
				if($donnees['con_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["con_id"] . "' >" . $donnees["con_nom"] . " " . $donnees["con_prenom"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["con_id"] . "' >" . $donnees["con_nom"] . " " . $donnees["con_prenom"] . "</option>";
				}

			}
		}
		return $client_select;
}
function get_contacts_clients($ref_id){

	global $ConnSoc;	

	$req_sql="SELECT * FROM contacts WHERE con_ref_id=$ref_id ORDER BY con_defaut DESC";
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetchAll();
	return $get_compte;
}
function get_contacts_suspects($ref_id){

	global $ConnSoc;	

	$req_sql="SELECT * FROM suspects_contacts WHERE sco_ref_id=$ref_id ORDER BY sco_defaut DESC";
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetchAll();
	return $get_compte;
}

?>