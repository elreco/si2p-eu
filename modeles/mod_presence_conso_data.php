<?php


setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


function presence_conso_data($action_id,$action_client_id){

	$data=array();

	global $acc_societe;
	global $Conn;
	global $ConnSoc;

	$erreur_txt="";

	// SUR LA FORMATION

	$sql="SELECT act_agence,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville,
	act_con_nom,act_con_prenom,act_con_tel,act_con_portable FROM Actions WHERE act_id=" . $action_id . ";";
	$req=$ConnSoc->query($sql);
	$d_action=$req->fetch();
	if(!empty($d_action)){
		$data['action'] = $d_action;
	}else{
		$erreur_txt="Impossible de charger les données liées à la formation!";
	}

	// LE INTERVENANT ET DATES DE FORMATIONS

	$intervenants=array();
	$int_id=0;
	$cle=-1;
	$duree=array();

	$bcl_action_client=0;
	$bcl_int=0;
	$bcl_date_cat=0;

	$nb_session=0;

	$bcl_session=0;



	$nb_page=0;
	$facturation=0;

	$sql="SELECT acl_id,acl_pro_libelle,acl_facturation,acl_client
	,cli_nom,pda_date,pda_demi,pda_categorie
	,int_id,int_label_1,int_label_2
	,ase_id,ase_h_deb,ase_h_fin
	,ast_stagiaire
	,ase_int_signature, ass_signature,
	acl_annule, ase_annule
	FROM Actions_Clients INNER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
	INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND cli_agence=" . $d_action["act_agence"] . ")
	INNER JOIN Plannings_Dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
	INNER JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)
	INNER JOIN Actions_Sessions ON (Plannings_Dates.pda_id=Actions_Sessions.ase_date)

	LEFT JOIN Actions_Stagiaires_Sessions ON (Actions_Sessions.ase_id=Actions_Stagiaires_Sessions.ass_session)
	LEFT JOIN Actions_Stagiaires ON (Actions_Stagiaires_Sessions.ass_stagiaire=Actions_Stagiaires.ast_stagiaire AND Actions_Clients.acl_id=Actions_Stagiaires.ast_action_client)

	WHERE acl_action=" . $action_id . " AND NOT acl_archive AND NOT pda_archive";
	if($action_client_id>0){
		$sql.=" AND acl_id=" . $action_client_id;
	}
	$sql.=" ORDER BY cli_nom,acl_client,acl_id,int_label_1,int_label_2,pda_intervenant,pda_date,pda_demi,ase_h_deb,ase_id,ass_stagiaire;";
	$req=$ConnSoc->query($sql);
	$d_results=$req->fetchAll(PDO::FETCH_ASSOC);

	if(!empty($d_results)){

		$data["pages"]=array();

		$facturation=$d_results[0]["acl_facturation"];

	
		foreach($d_results as $r){

			if ($r["acl_id"]!=$bcl_action_client  OR $bcl_int!=$r["int_id"] OR ($nb_session >= 4 AND $r["ase_id"]!=$bcl_session) ) {

				// affiche 5 session par page mais $nb_session est initialisé à -1 pour que la première clé ($nb_session++) soit égale ) 0
				// du coup pour 5 session par page $nb_session >= 4

				/*Ajout de la condition AND $r["ase_id"]!=$bcl_session 
				car si plusieurs stagiaires $nb_session=4 dès le premier mais il faut rester sur la même page pour les stagiaires suivants
				*/

			

				$nb_page++;

	
				if($r["acl_id"]!=$bcl_action_client){

					// Nouvelle action_client

					if($bcl_action_client!=0){
						// CALCUL DE LA DUREE
						$duree_before = 0;
						foreach($duree as $k=>$d){

							if(!empty($duree_before)){
								$duree_add = explode_time($d) + $duree_before;
							}else{
								$duree_add = explode_time($d);
							}
							$duree_before = $duree_add;

						}
						if(!empty($duree_add )){
							$duree_affich = second_to_hhmm($duree_add);
						}else{
							$duree_affich = "";
						}
						for($p_deb_cli;$p_deb_cli<$nb_page;$p_deb_cli++){
							$data["pages"][$p_deb_cli]["duree"]=str_replace(":", "h",$duree_affich);
						}
					}
					$p_deb_cli=$nb_page;
					$duree=array();
				}

				$bcl_action_client=$r["acl_id"];
				$bcl_int=$r["int_id"];
				$bcl_date_cat=$r["pda_categorie"];


				$bcl_session=0;

				$nb_session=-1;

				//$nb_page++;

				// on regarde si le client a un logo

				$client_logo="";

				$sql_get_client="SELECT Clients.cli_id, Clients.cli_logo_1,Clients.cli_logo_2
				,Holdings.cli_id AS mm_id, Holdings.cli_logo_1 AS mm_logo_1 ,Holdings.cli_logo_2 AS mm_logo_2
				FROM Clients LEFT JOIN Clients AS Holdings ON (Clients.cli_filiale_de=Holdings.cli_id)
				WHERE Clients.cli_id=:client;";
				$req_get_client=$Conn->prepare($sql_get_client);
				$req_get_client->bindParam(":client",$r["acl_client"]);
				$req_get_client->execute();
				$d_client=$req_get_client->fetch();
				if(!empty($d_client)){
					if(!empty($d_client["mm_logo_1"])){

						$client_logo="documents/clients/client" . $d_client["mm_id"] . "/" . $d_client["mm_logo_1"];

					}elseif(!empty($d_client["mm_logo_2"])){

						$client_logo="documents/clients/client" . $d_client["mm_id"] . "/" . $d_client["mm_logo_2"];

					}elseif(!empty($d_client["cli_logo_1"])){

						$client_logo="documents/clients/client" . $d_client["cli_id"] . "/" . $d_client["cli_logo_1"];

					}elseif(!empty($d_client["cli_logo_2"])){

						$client_logo="documents/clients/client" . $d_client["cli_id"] . "/" . $d_client["cli_logo_2"];

					}
				}

				$data["pages"][$nb_page]=array(
					"cli_nom" => $r["cli_nom"],
					"acl_pro_libelle" => $r["acl_pro_libelle"],
					"duree" => 0,
					"int_identite" => $r["int_label_2"] . " " . $r["int_label_1"],
					"pda_categorie" => $r["pda_categorie"],
					"sessions" => array(),
					"stagiaires" => array(),
					"client_logo" => $client_logo,
					"acl_annule" => $r["acl_annule"]
				);

			}

			
			if($r["ase_id"]!=$bcl_session){

				$bcl_session=$r["ase_id"];

				$nb_session++;

				if(!empty($r["ase_h_deb"]) AND !empty($r["ase_h_fin"])){
					$DT_deb =date_create_from_format('Y-m-d H\hi:s', $r["pda_date"] . " " . $r["ase_h_deb"] . ":00");
					$DT_fin =date_create_from_format('Y-m-d H\hi:s', $r["pda_date"] . " " . $r["ase_h_fin"] . ":00");
					/* $duree->add($DT_deb->diff($DT_fin)); */
					$interval = $DT_deb->diff($DT_fin);

					/* if(!empty($duree)){
						$duree_before = $duree;
					}else{
						$duree_before = 0;
					} */

					$duree[$r["pda_date"] . "-" . $r["pda_demi"]] = sprintf(
						'%d:%02d',
						($interval->d * 24) + $interval->h,
						$interval->i
					);

					$lib_h=$DT_deb->format("d/m/Y") . "<br/>" . $r["ase_h_deb"] . " / " . $r["ase_h_fin"];

				}else{
					$DT_deb =date_create_from_format('Y-m-d',$r["pda_date"]);
					$lib_h=$DT_deb->format("d/m/Y");

					$duree[$r["pda_date"] . "-" . $r["pda_demi"]] =second_to_hhmm(12600);
					// AJOUTER 3h30
					//$duree->add(new DateInterval('PT3H30M'));
					if($r["pda_demi"]==1){
						$lib_h.="<br/>matin";
					}elseif($r["pda_demi"]==2){
						$lib_h.="<br/>après-midi";
					}
				}
				$data["pages"][$nb_page]["sessions"][$nb_session]["h"]=$lib_h;
				if(!empty($r["ase_int_signature"])){
					$data["pages"][$nb_page]["sessions"][$nb_session]["int_signature"]="data:image/png;base64," . $r["ase_int_signature"];

				}else{
					$data["pages"][$nb_page]["sessions"][$nb_session]["int_signature"]="";

				}

			}
			if(!empty($r["ast_stagiaire"])){

				if(!isset($data["pages"][$nb_page]["stagiaires"][$r["ast_stagiaire"]])){

					$sql="SELECT sta_nom,sta_prenom,sta_naissance FROM Stagiaires WHERE sta_id=" . $r["ast_stagiaire"] . ";";
					$req=$Conn->query($sql);
					$d_stagiaire=$req->fetch();

					$data["pages"][$nb_page]["stagiaires"][$r["ast_stagiaire"]]=array(
						"sta_nom" => $d_stagiaire["sta_nom"],
						"sta_prenom" => $d_stagiaire["sta_prenom"],
						"sta_naissance" =>  $d_stagiaire["sta_naissance"],
						0 => "",
						1 => "",
						2 => "",
						3 => "",
						4 => ""
					);
				}
				if(!empty($r["ass_signature"])){
					$data["pages"][$nb_page]["stagiaires"][$r["ast_stagiaire"]][$nb_session]="data:image/png;base64," . $r["ass_signature"];
				}
			}
		}
		// fin de boucle

		// CALCUL DE LA DUREE
		$duree_before = 0;
		foreach($duree as $k=>$d){

			if(!empty($duree_before)){
				$duree_add = explode_time($d) + $duree_before;
			}else{
				$duree_add = explode_time($d);
			}
			$duree_before = $duree_add;

		}
		if(!empty($duree_add )){
			$duree_affich = second_to_hhmm($duree_add);
		}else{
			$duree_affich = "";
		}

		for($p_deb_cli;$p_deb_cli<=$nb_page;$p_deb_cli++){
			$data["pages"][$p_deb_cli]["duree"]=str_replace(":", "h",$duree_affich);
		}

		if(empty($erreur_txt)){

			// on part du principe que toutes les incvriptions clients ont le même acl_facturation. Si ce n'est pas le cas -> il faut placer juridique dans le boucle
			if($facturation==1){
				$data["juridique"]=get_juridique(4,4);
			}else{
				$data["juridique"]=get_juridique($acc_societe,$d_action["act_agence"]);
			}

		}

		if(!empty($action_client_id)){
			$data["pdf"]="presence_conso_" . $action_id . "_" . $action_client_id;
		}else{
			$data["pdf"]="presence_conso_" . $action_id;
		}


	}

	if(empty($erreur_txt)){
		return $data;
	}else{
		return $erreur_txt;
	}

}
?>
