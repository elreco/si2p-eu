<?php


setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


function presence_data($action_id,$action_client_id){

	$data=array();

	global $acc_societe;
	global $Conn;
	global $ConnSoc;

	$erreur_txt="";

	// SUR LA FORMATION

	$sql="SELECT act_agence,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville,
	act_con_nom,act_con_prenom,act_con_tel,act_con_portable FROM Actions WHERE act_id=" . $action_id . ";";
	$req=$ConnSoc->query($sql);
	$data=$req->fetch();
	if(empty($data)){
		$erreur_txt="Impossible de charger les données liées à la formation!";
	}

	if(empty($erreur_txt)){

		// SESSION ET DONNES ASSOCIES

		$bcl_action_client=0;
		$bcl_session=0;
		$nb_page=-1;

		$sql="SELECT ase_id ,ase_h_deb,ase_h_fin,ase_int_signature,ase_int_signature
		,pda_date,pda_categorie
		,acl_id,acl_pro_libelle,acl_facturation,acl_client
		,cli_nom
		,int_label_1,int_label_2
		,ast_stagiaire
		,ass_signature,
		acl_annule,ase_annule
		FROM Actions_Sessions
		INNER JOIN Plannings_Dates ON (Actions_Sessions.ase_date=Plannings_Dates.pda_id)
		INNER JOIN Actions_Clients_Dates ON (Plannings_Dates.pda_id=Actions_Clients_Dates.acd_date)
		INNER JOIN Actions_Clients ON (Actions_Clients_Dates.acd_action_client=Actions_Clients.acl_id)

		INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND cli_agence=" . $data["act_agence"] . ")
		INNER JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)

		LEFT JOIN Actions_Stagiaires_Sessions ON (Actions_Sessions.ase_id=Actions_Stagiaires_Sessions.ass_session)
		LEFT JOIN Actions_Stagiaires ON (Actions_Stagiaires_Sessions.ass_stagiaire=Actions_Stagiaires.ast_stagiaire AND Actions_Clients.acl_id=Actions_Stagiaires.ast_action_client)

		WHERE ase_action=" . $action_id . " AND NOT acl_archive AND NOT pda_archive";
		if($action_client_id>0){
			$sql.=" AND acl_id=" . $action_client_id;
		}
		$sql.=" ORDER BY pda_date,pda_demi,ase_h_deb,ase_h_fin,cli_nom,acl_client,acl_id,int_label_1,int_label_2,pda_intervenant,pda_date,pda_demi,ase_h_deb,ase_id,ass_stagiaire;";
		$req=$ConnSoc->query($sql);
		$d_results=$req->fetchAll(PDO::FETCH_ASSOC);
		/*echo("<pre>");
			print_r($d_results);
		echo("<pre>");
		die();*/
		if(!empty($d_results)){

			$data["pages"]=array();

			foreach($d_results as $r){

				if($r["acl_id"]!=$bcl_action_client OR $bcl_session!=$r["ase_id"] OR $nb_sta>=10){

					$bcl_action_client=$r["acl_id"];
					$bcl_session=$r["ase_id"];

					$nb_page++;

					$nb_sta=0;

					if(!empty($r["ase_h_deb"]) AND !empty($r["ase_h_fin"])){
						$DT_deb =date_create_from_format('Y-m-d H\hi:s', $r["pda_date"] . " " . $r["ase_h_deb"] . ":00");
						$DT_fin =date_create_from_format('Y-m-d H\hi:s', $r["pda_date"] . " " . $r["ase_h_fin"] . ":00");

						$date_diff = date_diff($DT_deb, $DT_fin);
						$DT_fin->sub($DT_fin->diff($DT_deb));
					}else{
						$DT_fin =date_create_from_format('Y-m-d H\hi:s', $r["pda_date"] . " 03h30:00");

					}
					if(empty($date_diff)){
						$date_diff = "";
					}
					$r["ase_int_signature_png"] = "";
					if(!empty($r["ase_int_signature"])){
						$r["ase_int_signature_png"] = "data:image/png;base64," . $r["ase_int_signature"];
					}
					$data["pages"][$nb_page]=array(
						"cli_nom" => $r["cli_nom"],
						"acl_pro_libelle" => $r["acl_pro_libelle"],
						"date" => $DT_fin,
						"date_alternate" => $date_diff,
						"ase_h_deb" => $r["ase_h_deb"],
						"ase_h_fin" => $r["ase_h_fin"],
						"int_identite" => $r["int_label_2"] . " " . $r["int_label_1"],
						"pda_categorie" => $r["pda_categorie"],
						"int_signature" =>  $r["ase_int_signature"],
						"int_signature_png" =>  $r["ase_int_signature_png"],
						"acl_annule" =>  $r["acl_annule"],
						"ase_annule" =>  $r["ase_annule"],
						"stagiaires" => array()
					);

					if($r["acl_facturation"]==1){
						$data["pages"][$nb_page]["juridique"]=get_juridique(4,4);
					}else{
						$data["pages"][$nb_page]["juridique"]=get_juridique($acc_societe,$data["act_agence"]);
					}

					// ON regarde si le client dispose d'un logo

					$sql_get_client="SELECT Clients.cli_id, Clients.cli_logo_1,Clients.cli_logo_2
					,Holdings.cli_id AS mm_id, Holdings.cli_logo_1 AS mm_logo_1 ,Holdings.cli_logo_2 AS mm_logo_2
					FROM Clients LEFT JOIN Clients AS Holdings ON (Clients.cli_filiale_de=Holdings.cli_id)
					WHERE Clients.cli_id=:client;";
					$req_get_client=$Conn->prepare($sql_get_client);
					$req_get_client->bindParam(":client",$r["acl_client"]);
					$req_get_client->execute();
					$d_client=$req_get_client->fetch();
					if(!empty($d_client)){
						if(!empty($d_client["mm_logo_1"])){

							$data["pages"][$nb_page]["juridique"]["logo_1"]="documents/clients/client" . $d_client["mm_id"] . "/" . $d_client["mm_logo_1"];

						}elseif(!empty($d_client["mm_logo_2"])){

							$data["pages"][$nb_page]["juridique"]["logo_1"]="documents/clients/client" . $d_client["mm_id"] . "/" . $d_client["mm_logo_2"];

						}elseif(!empty($d_client["cli_logo_1"])){

							$data["pages"][$nb_page]["juridique"]["logo_1"]="documents/clients/client" . $d_client["cli_id"] . "/" . $d_client["cli_logo_1"];

						}elseif(!empty($d_client["cli_logo_2"])){

							$data["pages"][$nb_page]["juridique"]["logo_1"]="documents/clients/client" . $d_client["cli_id"] . "/" . $d_client["cli_logo_2"];

						}
					}
				}

				if(!empty($r["ast_stagiaire"])){
					if(!isset($data["pages"][$nb_page]["stagiaires"][$r["ast_stagiaire"]])){

						$nb_sta++;
						$sql="SELECT sta_nom,sta_prenom,sta_naissance FROM Stagiaires WHERE sta_id=" . $r["ast_stagiaire"] . ";";
						$req=$Conn->query($sql);
						$d_stagiaire=$req->fetch();
						$r["ass_signature_png"] = "";
						if(!empty($r["ass_signature"])){
							$r["ass_signature_png"] = "data:image/png;base64," . $r["ass_signature"];
						}
						$data["pages"][$nb_page]["stagiaires"][$r["ast_stagiaire"]]=array(
							"sta_nom" => $d_stagiaire["sta_nom"],
							"sta_prenom" => $d_stagiaire["sta_prenom"],
							"sta_naissance" =>  $d_stagiaire["sta_naissance"],
							"sta_signature" =>  $r["ass_signature"],
							"sta_signature_png" =>  $r["ass_signature_png"],
							0 => "",
							1 => "",
							2 => "",
							3 => "",
							4 => ""
						);
					}
				}
			}

		}



		if(!empty($action_client_id)){
			$data["pdf"]="presence_" . $action_id . "_" . $action_client_id;
		}else{
			$data["pdf"]="presence_" . $action_id;
		}


	}

	if(empty($erreur_txt)){
		return $data;
	}else{
		return $erreur_txt;
	}
}
?>
