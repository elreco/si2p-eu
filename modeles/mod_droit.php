<?php

	// MODELE LIE AU DROITS D'ACCES
	
	/*-----------------------------------
		SELECTION 
	------------------------------------- */
	
	function get_droit($drt_service){
		
		global $Conn;	
		
		$req_sql="SELECT * FROM Droits";
		if($drt_service>0){
			$req_sql=$req_sql . " WHERE drt_service=" . $drt_service;
		}
		$req_sql=$req_sql . " ORDER BY drt_nom";
		
		$req=$Conn->query($req_sql);
		$get_droit = $req->fetchAll();	
		return $get_droit;
	}
	
	// retourne la liste des droits avec leurs valeurs d'affectation à un profil
	// on peut restreindre l'affiche aux droit d'un service
	function get_liste_droit_profil($profil,$service = 0){
		
		global $Conn;																			
		$sql="SELECT drt_id,drt_nom,drt_descriptif,udr_droit,udr_reaffecte FROM Droits LEFT OUTER JOIN Utilisateurs_Droits ON (Droits.drt_id=Utilisateurs_Droits.udr_droit AND udr_profil=" . $profil . ")";
		if($service>0){
			$sql=$sql . " WHERE drt_service=" . $service;
		}
		$sql=$sql . " ORDER BY drt_nom";						
		$req=$Conn->query($sql);
		$droit_profil = $req->fetchAll();

		return $droit_profil;	
	}
	
	// valeur d'affectation d'un droit pour un profil donner
	function get_droit_profil($droit,$profil){
		global $Conn;	
		
		$req = $Conn->prepare("SELECT udr_reaffecte FROM Utilisateurs_droits WHERE udr_droit=:udr_droit AND udr_profil=:udr_profil");
		$req->bindParam(':udr_droit', $droit);
		$req->bindParam(':udr_profil', $profil);
		$req->execute();
		$donnees=$req->fetch();
		return $donnees;	
	}
	// valeur d'affectation d'un droit pour un utilisateur donnée
	function get_droit_utilisateur($droit,$utilisateur){
		
		global $Conn;	
	
		$req = $Conn->prepare("SELECT udr_reaffecte FROM Utilisateurs_droits WHERE udr_droit=:udr_droit AND udr_utilisateur=:udr_utilisateur");
		$req->bindParam(':udr_droit', $droit);
		$req->bindParam(':udr_utilisateur', $utilisateur);
		$req->execute();
		$donnees=$req->fetch();
		return $donnees;	
	}
	
	/*-----------------------------------
		AJOUT DE DONNEE 
	------------------------------------- */
	
	function insert_droit($drt_nom,$drt_service,$drt_descriptif){
		global $Conn;		
		$req = $Conn->prepare("INSERT INTO droits (drt_nom, drt_service, drt_descriptif) VALUES (:drt_nom, :drt_service, :drt_descriptif)");
		$req->bindParam(':drt_nom', $drt_nom);
		$req->bindParam(':drt_service', $drt_service);
		$req->bindParam(':drt_descriptif', $drt_descriptif);
		$req->execute();
	}
	
	// affecte un droit à un profil
	function insert_droit_profil($droit,$profil){
		global $Conn;	
		$req = $Conn->prepare("INSERT INTO utilisateurs_droits (udr_droit, udr_profil) VALUES (:udr_droit, :udr_profil)");
		$req->bindParam(':udr_droit', $droit);
		$req->bindParam(':udr_profil', $profil);
		$req->execute();	
	}
	// ajoute un droit d'accès à un utilisateur
	function insert_droit_utilisateur($droit,$utilisateur,$reaffecte){
		global $Conn;	
		$req = $Conn->prepare("INSERT INTO utilisateurs_droits (udr_droit, udr_utilisateur, udr_reaffecte) VALUES (:udr_droit, :udr_utilisateur, :udr_reaffecte)");
		$req->bindParam(':udr_droit', $droit);
		$req->bindParam(':udr_utilisateur', $utilisateur);
		$req->bindParam(':udr_reaffecte', $reaffecte);
		$req->execute();	
	}
	
	/*-----------------------------------
		MISE A JOUR  
	------------------------------------- */
	function update_droit($drt_id,$drt_nom,$drt_service,$drt_descriptif){
		global $Conn;	
		$req = $Conn->prepare("UPDATE droits SET drt_nom=:drt_nom, drt_service=:drt_service, drt_descriptif=:drt_descriptif WHERE drt_id=:drt_id");
		$req->bindParam(':drt_id', $drt_id);
		$req->bindParam(':drt_nom', $drt_nom);
		$req->bindParam(':drt_service', $drt_service);
		$req->bindParam(':drt_descriptif', $drt_descriptif);
		$req->execute();	
	}
	
	// --------------
	// OPERATION SUR LES DROITS EN LIEN AVEC LES PROFILS
	//---------------
	
	// mise à jour des valeur du droit lié au profil
	function update_droit_profil($droit,$profil,$affecte){
		global $Conn;	
		$req = $Conn->prepare("UPDATE utilisateurs_droits SET udr_reaffecte=:udr_reaffecte WHERE udr_droit=:udr_droit AND udr_profil=:udr_profil");
		$req->bindParam(':udr_droit', $droit);
		$req->bindParam(':udr_profil', $profil);
		$req->bindParam(':udr_reaffecte', $affecte);
		$req->execute();	
	}
	function update_droit_utilisateur($droit,$utilisateur,$affecte){
		global $Conn;	
		$req = $Conn->prepare("UPDATE utilisateurs_droits SET udr_reaffecte=:udr_reaffecte WHERE udr_droit=:udr_droit AND udr_utilisateur=:udr_utilisateur");
		$req->bindParam(':udr_droit', $droit);
		$req->bindParam(':udr_utilisateur', $utilisateur);
		$req->bindParam(':udr_reaffecte', $affecte);
		$req->execute();	
	}
	
	// retire un droit à un profil
	function delete_droit_profil($droit,$profil){
		global $Conn;	
		$req = $Conn->prepare("DELETE FROM Utilisateurs_droits WHERE udr_droit=:udr_droit AND udr_profil=:udr_profil");
		$req->bindParam(':udr_droit', $droit);
		$req->bindParam(':udr_profil', $profil);
		$req->execute();	
	}
	// retire un droit à un utilisateur
	function delete_droit_utilisateur($droit,$utilisateur){
		global $Conn;	
		$req = $Conn->prepare("DELETE FROM Utilisateurs_droits WHERE udr_droit=:udr_droit AND udr_utilisateur=:udr_utilisateur");
		$req->bindParam(':udr_droit', $droit);
		$req->bindParam(':udr_utilisateur', $utilisateur);
		$req->execute();	
	}
	// retire tous les droits à un utilisateur
	function delete_droits_utilisateur($utilisateur){
		global $Conn;	
		$req = $Conn->prepare("DELETE FROM Utilisateurs_droits WHERE udr_utilisateur=:udr_utilisateur");
		$req->bindParam(':udr_utilisateur', $utilisateur);
		$req->execute();	
	}
?>	
