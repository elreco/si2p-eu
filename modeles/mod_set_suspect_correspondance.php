<?php
// creation ou maj d'un contact

// $d_correspondance => array avec clé = nom champ base

// retourne false en cas d'échec et IN du contact en cas de réussite

function set_suspect_correspondance($d_correspondance){
	
	global $ConnSoc;
	
	if(!empty($d_correspondance)){
		
		$criteres=array();
		
		if(empty($d_correspondance["sco_date"]) OR empty($d_correspondance["sco_commentaire"]) OR empty($d_correspondance["sco_utilisateur"]) OR empty($d_correspondance["sco_suspect"]) ){
			return false;
		}
		
		if(!empty($d_correspondance["sco_id"])){
			
			// MAJ
			
			$sql="UPDATE suspects_correspondances SET ";
			$sql.="sco_date=:sco_date,sco_commentaire=:sco_commentaire,sco_utilisateur=:sco_utilisateur,sco_type=:sco_type,
			sco_raison=:sco_raison,sco_raison_info=:sco_raison_info,";
			
			$criteres["sco_date"]=$d_correspondance["sco_date"];
			$criteres["sco_commentaire"]=$d_correspondance["sco_commentaire"];
			$criteres["sco_utilisateur"]=$d_correspondance["sco_utilisateur"];
			$criteres["sco_type"]=$d_correspondance["sco_type"];
			$criteres["sco_raison"]=$d_correspondance["sco_raison"];
			$criteres["sco_raison_info"]=$d_correspondance["sco_raison_info"];
			
			
			if(isset($d_correspondance["sco_contact"])){
				if(!empty($d_correspondance["sco_contact"])){
					$sql.="sco_contact=:sco_contact,";	
					$criteres["sco_contact"]=$d_correspondance["sco_contact"];
				}else{
					$sql.="sco_contact=0,";
				}
			}
			if(isset($d_correspondance["sco_contact_nom"])){
				if(!empty($d_correspondance["sco_contact_nom"])){
					$sql.="sco_contact_nom=:sco_contact_nom,";	
					$criteres["sco_contact_nom"]=$d_correspondance["sco_contact_nom"];
				}else{
					$sql.="sco_contact_nom='',";		
				}
			}
			if(isset($d_correspondance["sco_contact_prenom"])){
				if(!empty($d_correspondance["sco_contact_prenom"])){
					$sql.="sco_contact_prenom=:sco_contact_prenom,";	
					$criteres["sco_contact_prenom"]=$d_correspondance["sco_contact_prenom"];
				}else{
					$sql.="sco_contact_prenom='',";	
				}
			}
			if(isset($d_correspondance["sco_contact_tel"])){
				if(!empty($d_correspondance["sco_contact_tel"])){
					$sql.="sco_contact_tel=:sco_contact_tel,";	
					$criteres["sco_contact_tel"]=$d_correspondance["sco_contact_tel"];
				}else{
					$sql.="sco_contact_tel='',";	
				}
			}
			if(isset($d_correspondance["sco_rappeler_le"])){
				if(!empty($d_correspondance["sco_rappeler_le"])){
					$sql.="sco_rappeler_le=:sco_rappeler_le,";	
					$criteres["sco_rappeler_le"]=$d_correspondance["sco_rappeler_le"];
				}else{
					$sql.="sco_rappeler_le=NULL,";	
				}
			}
			if(isset($d_correspondance["sco_rappel"])){
				if(!empty($d_correspondance["sco_rappel"])){
					$sql.="sco_rappel=1,";	
				}else{
					$sql.="sco_rappel=0,";	
				}
			}
			$sql=substr($sql,0,-1); 			
			$sql.=" WHERE sco_id=:correspondance;";
			$req = $ConnSoc->prepare($sql);
			$req->bindParam("correspondance",$d_correspondance["sco_id"]);
			foreach($criteres as $c => $u){
				$req->bindValue($c,$u);
			}						
			try {
				$req->execute();			
			}catch (Exception $e) {
				//echo($e->getMessage());			
				//echo("<br/>");
				return false;
			}

		}else{
			
			// ADD
			

			$sql="INSERT INTO suspects_correspondances (sco_suspect,sco_date,sco_utilisateur,sco_commentaire,sco_type,sco_raison,sco_raison_info";
			$valeurs=":sco_suspect,:sco_date,:sco_utilisateur,:sco_commentaire,:sco_type,:sco_raison,:sco_raison_info";
			
			if(isset($d_correspondance["sco_contact"])){				
				$sql.=",sco_contact";
				$valeurs.=",:sco_contact";	
				if(!empty($d_correspondance["sco_contact"])){
					$criteres["sco_contact"]=$d_correspondance["sco_contact"];										
				}else{
					$criteres["sco_contact"]=0;
				}
			}
			if(isset($d_correspondance["sco_contact_nom"])){
				$sql.=",sco_contact_nom";	
				$valeurs.=",:sco_contact_nom";	
				$criteres["sco_contact_nom"]=$d_correspondance["sco_contact_nom"];
			}
			if(isset($d_correspondance["sco_contact_prenom"])){
				$sql.=",sco_contact_prenom";	
				$valeurs.=",:sco_contact_prenom";	
				$criteres["sco_contact_prenom"]=$d_correspondance["sco_contact_prenom"];
			}
			if(isset($d_correspondance["sco_contact_tel"])){
				$sql.=",sco_contact_tel";	
				$valeurs.=",:sco_contact_tel";	
				$criteres["sco_contact_tel"]=$d_correspondance["sco_contact_tel"];
			}
			
			if(isset($d_correspondance["sco_rappeler_le"])){				
				$sql.=",sco_rappeler_le";
				$valeurs.=",:sco_rappeler_le";	
				if(!empty($d_correspondance["sco_rappeler_le"])){
					$criteres["sco_rappeler_le"]=$d_correspondance["sco_rappeler_le"];										
				}else{
					$criteres["sco_rappeler_le"]=NULL;
				}
			}
			$sql.=" ) VALUES (" . $valeurs . ");";
			$req = $ConnSoc->prepare($sql);			
			$req->bindParam(":sco_suspect",$d_correspondance["sco_suspect"]);
			$req->bindParam(":sco_date",$d_correspondance["sco_date"]);
			$req->bindParam(":sco_commentaire",$d_correspondance["sco_commentaire"]);
			$req->bindParam(":sco_utilisateur",$d_correspondance["sco_utilisateur"]);
			$req->bindParam(":sco_type",$d_correspondance["sco_type"]);
			$req->bindParam(":sco_raison",$d_correspondance["sco_raison"]);
			$req->bindParam(":sco_raison_info",$d_correspondance["sco_raison_info"]);
			foreach($criteres as $c => $u){
				$req->bindValue($c,$u);
			}
			try{
				$req->execute();
				$d_correspondance["sco_id"]=$ConnSoc->lastInsertId();
			}catch (Exception $e){
				echo($e->getMessage());			
				echo("<br/>");
				return false;
			}
		}
		
		// FIN D'EDITION DE LA Correspondances

		// traitement auto du rappel
	
		if(!empty($d_correspondance["sco_contact"])){

			echo("contact " . $d_correspondance["sco_contact"] . "<br/>");
			$sql="UPDATE suspects_correspondances SET sco_rappel=1 WHERE sco_suspect=" . $d_correspondance["sco_suspect"] . " AND sco_utilisateur=" . $d_correspondance["sco_utilisateur"] . "
			AND sco_contact=" . $d_correspondance["sco_contact"] . " AND sco_date<='" . $d_correspondance["sco_date"] . "' AND NOT sco_id=" . $d_correspondance["sco_id"];
			if(!empty($d_correspondance["sco_rappeler_le"])){
				$sql.=" AND sco_rappeler_le<='" . $d_correspondance["sco_rappeler_le"] . "'";
			};
			$sql.=";";			
			try{
				$ConnSoc->query($sql);
			}catch (Exception $e){
				
				echo("Erreur rappel auto");
				echo("<br/>");			
				echo($e->getMessage());
				echo("<br/>");
				die();
				/*
				echo($sql);
				echo("<br/>");
				echo($d_correspondance["sco_date"]);
				echo("<br/>");
				echo($d_correspondance["sco_rappeler_le"]);
				echo("<br/>");
				echo("Fin debug rappel auto<br/>");*/
				
			}
			
			
		}
		
		// MAJ DE LA FICHE SUSPECT
		
		$sql="SELECT MAX(sco_date),MAX(sco_rappeler_le) FROM suspects_correspondances WHERE sco_suspect=" . $d_correspondance["sco_suspect"] . ";";
		$req=$ConnSoc->query($sql);
		$d_suspect=$req->fetch();
		if(!empty($d_suspect)){
			
			$cli_date=null;
			if(!empty($d_suspect[0])){
				$cli_sco_date=date_create_from_format('Y-m-d',$d_suspect[0]);
				if(!is_bool($cli_sco_date)){
					$cli_date=$cli_sco_date->format("Y-m-d");
				}
				
			}
			$cli_rappel=null;
			if(!empty($d_suspect[0])){
				$cli_sco_rappel=date_create_from_format('Y-m-d',$d_suspect[1]);
				if(!is_bool($cli_sco_rappel)){
					$cli_rappel=$cli_sco_rappel->format("Y-m-d");
				}
			}
			
		
			$sql="UPDATE suspects SET sus_cor_date=:sus_cor_date, sus_cor_rappel=:sus_cor_rappel WHERE sus_id=:suspect;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":sus_cor_date",$cli_date);
			$req->bindParam(":sus_cor_rappel",$cli_rappel);
			$req->bindParam(":suspect",$d_correspondance["sco_suspect"]);
			try{
				$req->execute();
			}catch (Exception $e){
				echo("A - " . $e->getMessage());
				die();
				//echo("<br/>");
			}
			
			
		}
		return intval($d_correspondance["sco_id"]);
	}
}