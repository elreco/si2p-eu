<?php

// INSCRIPTION D'UN STAGIARE SUR UNE ACTION

/*
option $session_id >0 le stagiaire sera inscrit sur une session spé sinon le stagiaire est inscrit sur tt les dates

auteur FG 28/10/2016


*/
function del_action_stagiaire($stagiaire,$action,$session){

	global $ConnSoc;
	global $acc_societe;

	$retour=true;

	$supp_stagaire=false;

	if($stagiaire>0 AND $action>0){

		if($session>0){
			$sql="SELECT ass_signature FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire AND ass_action=:action AND ass_session=:session;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":stagiaire",$stagiaire);
			$req->bindParam(":action",$action);
			$req->bindParam(":session",$session);
			$req->execute();
			$result=$req->fetch();

			if(!empty($result['ass_signature'])) {
				$retour=2;
			}else{
				$sql="DELETE FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire AND ass_action=:action AND ass_session=:session;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":stagiaire",$stagiaire);
				$req->bindParam(":action",$action);
				$req->bindParam(":session",$session);
				$req->execute();
				$result=$ConnSoc->errorCode();
				if(!empty($result)){
					if(!empty($result)){
						if($result!="00000"){
							$retour=false;
						}
					}
				}
			}

			// on regarde si le stagiaire est toujours inscrit
			if($retour && $retour !== 2){

				$sql="SELECT ass_stagiaire FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire AND ass_action=:action;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":stagiaire",$stagiaire);
				$req->bindParam(":action",$action);
				$req->execute();
				$result=$req->fetch();
				if(empty($result)){
					$supp_stagaire=true;
				}
			}
		}else{
			$supp_stagaire=true;
		}
		if($retour AND $supp_stagaire){

			if($session==0){
				$sql="SELECT ass_signature FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire AND ass_action=:action;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":stagiaire",$stagiaire);
				$req->bindParam(":action",$action);
				$req->execute();
				$result=$req->fetch();

				if(!empty($result['ass_signature'])) {
					$retour=2;
				}else{
					// si session>0 elle a deja été supp
					$sql="DELETE FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire AND ass_action=:action;";
					$req=$ConnSoc->prepare($sql);
					$req->bindParam(":stagiaire",$stagiaire);
					$req->bindParam(":action",$action);
					$req->execute();
					$result=$ConnSoc->errorCode();
					if(!empty($result)){
						if($result!="00000"){
							$retour=false;
						}
					}
				}

			}

			$sql="SELECT ast_confirme,ast_action_client,acl_pro_inter,acl_ca,acl_ca_unit FROM Actions_Stagiaires,Actions_Clients WHERE ast_action_client=acl_id AND ast_stagiaire=:stagiaire AND ast_action=:action;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":stagiaire",$stagiaire);
			$req->bindParam(":action",$action);
			$req->execute();
			$inscription=$req->fetch();
			if(empty($inscription)){
				$retour=false;
			}

			// supp l'inscription
			if($retour && $retour !== 2){

				$sql="DELETE FROM Actions_Stagiaires WHERE ast_stagiaire=:stagiaire AND ast_action=:action;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":stagiaire",$stagiaire);
				$req->bindParam(":action",$action);
				$req->execute();
				$result=$ConnSoc->errorCode();
				if($result!="00000"){
					$retour=false;
				}
			}

			// on supp les fichiers PDF
			if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Attestations/" . $acc_societe . "/attestation_". $action . "_" . $stagiaire . ".pdf")){
				unlink($_SERVER["DOCUMENT_ROOT"] . "/Documents/Attestations/" . $acc_societe . "/attestation_". $action . "_" . $stagiaire . ".pdf");
			}

			if($retour){

				// maj action

				/*if($inscription["ast_confirme"]==1){

					$sql="UPDATE Actions SET act_nb_stagiaire = act_nb_stagiaire-1 WHERE act_id = :action;";
				}else{
					$sql="UPDATE Actions SET act_nb_stagiaire_resa = act_nb_stagiaire_resa-1 WHERE act_id = :action;";
				}
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":action",$action);
				$req->execute();
				$result=$ConnSoc->errorCode();
				if($result!="00000"){
					$retour=false;
				}*/

				// maj action client
				/*if($inscription["ast_confirme"]==1){

					$ca=$inscription["acl_ca"]-$inscription["acl_ca_unit"];
					$ca=round($ca,2);

					$sql="UPDATE Actions_Clients SET acl_for_nb_sta=acl_for_nb_sta-1";
					if($inscription["acl_pro_inter"]==1){
						$sql.=" ,acl_ca=" . $ca;
					}
					$sql.=" WHERE acl_id=" . $inscription["ast_action_client"] . ";";
					$req=$ConnSoc->query($sql);
				}*/
			}

		}

	}else{
		$retour=false;
	}


	return $retour;

}
?>
