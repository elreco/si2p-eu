<?php

// RETOURNE UNE LIGNE DE LA TABLE Produits_Familles qui se trouve dans ORION

// AUTEUR FG

function orion_pro_famille($pfa_id){

	global $Conn;

	$sql="SELECT pfa_libelle,pfa_couleur FROM Produits_Familles WHERE pfa_id=:pfa_id";
	$req=$Conn->prepare($sql);
	$req->bindParam(":pfa_id",$pfa_id);
	$req->execute();
	$pro_famille = $req->fetch();
	if(!empty($pro_famille)){
		return $pro_famille;
	}

    
}
