<?php

// RETOURNE LA TABLE Clients_Sous_Categories qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_cli_categories(){

	global $Conn;
	
	$result[0]=array(
		"cca_libelle" => "",
	);
	
	$sql="SELECT cca_id,cca_libelle,cca_couleur,cca_gc FROM Clients_Categories ORDER BY cca_id";
	$req=$Conn->prepare($sql);
	$req->execute();
	$sous_categories = $req->fetchAll();
	if(!empty($sous_categories)){
		foreach($sous_categories as $csc){
			$result[$csc["cca_id"]]=array(
				"cca_libelle" => $csc["cca_libelle"],
				"cca_couleur" => $csc["cca_couleur"],
				"cca_gc" => $csc["cca_gc"]
			);
		}
	}

    return $result;
}
