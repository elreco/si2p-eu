<?php

/* RETOURNE LES ADRESSES CLIENT POUVANT ETRE UTILISE DANS UNE ACTION COMME LIEU D'INTERVENTION


action : id de l'action dont on veut recuperer LES ADRESSES


AUTEUR FG 29/09/2016
*/

function get_action_adresses($action){

	global $ConnFct;
	global $ConnSoc;
	
	// LES CLIENTS D'UNE MEME FORMATION PEUVENT ETER STOCKE SUR DES BASES DIFFERENTES
	// GC ET REVENDEURS EN BASE 4
	
	$adresses=array();
	
	$sql="SELECT DISTINCT acl_client,acl_client_societe FROM Actions_Clients WHERE acl_action=" . $action . ";";
	$req=$ConnSoc->query($sql);
	$clients=$req->fetchAll();
	if(!empty($clients)){
		foreach($clients as $c){
			
			$societe=$_SESSION['acces']["acc_societe"];
			if(!empty($c["acl_client_societe"])){
				$societe=$c["acl_client_societe"];
			}
			
			$ConnFct=connexion_fct($societe);
			
			$sql="SELECT adr_id,adr_ref_id,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_defaut
			FROM Adresses WHERE adr_type=1 AND adr_ref=1 AND adr_ref_id=" . $c["acl_client"] . ";";
			$req=$ConnFct->query($sql);
			$d_adresse=$req->fetchAll();
			if(!empty($d_adresse)){
				foreach($d_adresse as $r){
					$adresses[]=array(
						"id" => $r["adr_id"],		// nécessaire pour injection plugin select2
						"text" => $r["adr_nom"] . " " . $r["adr_cp"] . " " . $r["adr_ville"],	// nécessaire pour injection plugin select2
						"ref_id" => $r["adr_ref_id"],
						"nom" => $r["adr_nom"],
						"service" => $r["adr_service"],
						"ad1" => $r["adr_ad1"],
						"ad2" => $r["adr_ad2"],
						"ad3" => $r["adr_ad3"],
						"cp" => $r["adr_cp"],
						"ville" => $r["adr_ville"],
						"defaut" => $r["adr_defaut"],
						"societe" => $societe
					);	
				}
			}
		}
	}
	return $adresses;
}
