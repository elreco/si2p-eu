<?php

/* RETOURNE UN TABLEAU CONTENANT LES ID ET REFERENCE PRODUIT

parametres obigatoires

INT type : intra (valeur = 1) ou inter (valeur = 2)

parametre facultatifs

INT client : restriction sur les produits accessible au client (utile seulment s'il s'agit d'un GC sinon client à accès à l'ensemble du catalogue)
STRING famille : utilisé en IN LIST() permet de restreindre les produits sur certaines familles
STRING categorie : utilisé en IN LIST() permet de restreindre les produits sur certaines categorie

auteur : FG 27/09/2016
*/

function get_produits_ref($type,$client,$categorie,$famille){
	
	global $ConnSoc;
	global $Conn;
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}

	if($client>0){
		// selectionne que si grand compte
		$req = $ConnSoc->prepare("SELECT cli_categorie,cli_groupe,cli_filiale_de,cli_filiale_de_soc FROM Clients WHERE cli_id=:client AND cli_categorie=2;");
		$req->bindParam(":client",$client);
		$req->execute();
		$d_gc=$req->fetch();
		if(empty($d_gc)){
			unset($d_gc);
		}
	};
	if(isset($d_gc)){
		
		// GRAND COMPTE
		
		if($d_gc["cli_groupe"]&&$d_gc["cli_filiale_de"]>0){
			// il s'agit d'une filiale et les produits sont renseignés sur la holding
			$conn_get_id=$c["cli_filiale_de_soc"]; // societe de la mm
			$produit_client=$c["cli_filiale_de"];
		}else{
			$conn_get_id=$acc_societe;
			$produit_client=$client;
		}
		include('../connexion_get.php');
		
		if($type==1){
			// INTRA
			$sql="SELECT cpr_produit AS id,cpr_reference AS reference FROM Clients_Produits WHERE cpr_client=:client";
			$sql.=" AND NOT cpr_archive";
			if($categorie!=""){
				$sql.=" AND cpr_categorie IN (:categorie)";
			}
			if($famille!=""){
				$sql.=" AND cpr_famille IN (:famille)";
			}
			$sql.=" ORDER BY cpr_reference;";		
		}elseif($type==2){
			// iNTER
			$sql="SELECT cpr_produit AS id,cpr_reference_inter AS reference FROM Clients_Produits WHERE cpr_client=:client";
			$sql.=" AND NOT cpr_archive";
			if($categorie>0){
				$sql.=" AND cpr_categorie IN (:categorie)";
			}
			if($famille>0){
				$sql.=" AND cpr_famille IN (:famille)";
			}
			$sql.=" ORDER BY cpr_reference_inter;";			
		}
		$req = $ConnGet->prepare($sql);
		$req->bindParam(":client",$produit_client);
		if($categorie!=""){
			$req ->bindParam(":categorie",$categorie);
		}
		if($famille!=""){
			$req ->bindParam(":famille",$famille);
		}	
		$req->execute();
		$produits=$req->fetchAll();
		
	}else{
		
		// CLIENT CLASSIQUE
			
		// il a accès à tout les codes produits du catalogue
			
		if($type==1){
			// INTRA
			$sql="SELECT pro_id AS id,pro_reference AS reference FROM Produits";
			$sql.=" WHERE NOT pro_archive AND NOT ISNULL(pro_reference)";
			if($categorie!=""){
				$sql.=" AND pro_categorie IN (:categorie)";
			}
			if($famille!=""){
				$sql.=" AND pro_famille IN (:famille)";
			}
			$sql.=" AND NOT pro_reference='' ORDER BY pro_reference;";	
		}elseif($type==2){
			// iNTER
			$sql="SELECT pro_id AS id,pro_reference_inter AS reference FROM Produits";
			$sql.=" WHERE NOT pro_archive AND NOT ISNULL(pro_reference_inter)";
			if($categorie!=""){
				$sql.=" AND pro_categorie IN (:categorie)";
			}
			if($famille!=""){
				$sql.=" AND pro_famille IN (:famille)";
			}
			$sql.=" AND NOT pro_reference_inter='' ORDER BY pro_reference_inter;";				
		}
		$req = $Conn->prepare($sql);
		if($categorie!=""){
			$req ->bindParam(":categorie",$categorie);
		}
		if($famille!=""){
			$req ->bindParam(":famille",$famille);
		}
		$req ->execute();
		$produits=$req->fetchAll();			
	}
	if(isset($produits)){
		return $produits;
	}
}
?>
