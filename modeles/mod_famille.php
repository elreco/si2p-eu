<?php
//include_once '/connexion.php';
function get_familles()
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_familles");
    $req->execute();

    $familles = $req->fetchAll();

    return $familles;
}
function get_famille($id)
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_familles WHERE pfa_id = " . $id);
    $req->execute();

    $famille = $req->fetch();

    return $famille;
}
function get_sous_famille($id)
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_sous_familles WHERE psf_id = " . $id);
    $req->execute();

    $famille = $req->fetch();

    return $famille;
}

function get_famille_select($selected){

  global $Conn;
  
  $selected=intval($selected);
 
	$service_select="";
	$req=$Conn->query("SELECT * FROM produits_familles ORDER BY pfa_libelle");
	while ($donnees = $req->fetch())
	{
		if($selected == $donnees["pfa_id"]){
			$service_select=$service_select . "<option value='" . $donnees["pfa_id"] . "' selected >" . $donnees["pfa_libelle"] . "</option>";
		}else{
			$service_select=$service_select . "<option value='" . $donnees["pfa_id"] . "' >" . $donnees["pfa_libelle"] . "</option>";
		} 
	}
	return $service_select;
}