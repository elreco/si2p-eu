<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

function envoi_mail($modele,$param,$adr, $relance=0, $inter=""){
	include_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';


	global $Conn;
	// Atention $adr , $cc , $cci sont des array

	if(!empty($modele)){
		$body_mail=file_get_contents($_SERVER['DOCUMENT_ROOT']."/email-templates/" . $modele .".php");
		if(!empty($param)){
			foreach($param as $par_k=>$par_v){
				$body_mail = str_replace('%'. $par_k . '%',$par_v, $body_mail);
			}
		}
	}else{
		$body_mail=$param["body_mail"];
	}

	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
	);
	if($relance == 1){
		$mail->Host = 'smtp.gmail.com';  						// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               	// Enable SMTP authentication
		$mail->Username = 'dorothee.raynaud@si2p.mobi';    	// SMTP username
		$mail->Password = 'drra@1456';                        	// SMTP password
		//$mail->SMTPSecure = '';                            	// Enable TLS encryption, `ssl` also accepted
		//$mail->Port = 587;
		$mail->SMTPSecure = 'ssl';                            	// Enable TLS encryption, `ssl` also accepted
		$mail->Port = 465;
		$mail->CharSet = 'UTF-8';

		$mail->setFrom('dorothee.raynaud@si2p.fr', 'Réseau Si2P');
	}else{
		$mail->Host = 'ssl0.ovh.net';  						// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               	// Enable SMTP authentication
		$mail->Username = 'nepasrepondre@si2p.eu';    	// SMTP username
		$mail->Password = 'Orion@4976';                        	// SMTP password
		//$mail->SMTPSecure = '';                            	// Enable TLS encryption, `ssl` also accepted
		//$mail->Port = 587;
		$mail->SMTPSecure = 'ssl';                            	// Enable TLS encryption, `ssl` also accepted
		$mail->Port = 465;
		$mail->CharSet = 'UTF-8';
		if(!empty($inter)){
			$mail->setFrom('nepasrepondre@si2p.eu', $inter);
		}else{
			$mail->setFrom('nepasrepondre@si2p.eu', 'Réseau Si2P');
		}

		if(!empty($_SESSION["acces"]['acc_ref_id'])){
			// ALLER CHERCHE NOM PRENOM ADRESSE EMAIL
			$req = $Conn->prepare("SELECT uti_mail, uti_prenom, uti_nom FROM Utilisateurs WHERE uti_id = " . $_SESSION["acces"]['acc_ref_id']);
		    $req->execute();
			$d_uti = $req->fetch();
			if(!empty($d_uti['uti_mail'])){
				$mail->addReplyTo($d_uti['uti_mail'], $d_uti['uti_prenom'] . " " . $d_uti['uti_nom']);
			}
		}


	}


	if(!empty($adr)){
		foreach($adr as $a){
			$mail->addAddress($a["adresse"],$a["nom"]);     // Add a recipient
		}
	}
	if(!empty($param["pj"])){
		foreach($param["pj"] as $pj){
			$mail->addAttachment($pj["fichier"],$pj["nom"]);
		}
	}
	if(!empty($param["copie_cache"])){
		$mail->addBCC($param["copie_cache"]["adresse"],$param["copie_cache"]["nom"]);
	}
	$mail->isHTML(true);                                  // Set email format to HTML
	if(!empty($param["sujet"])){
		$mail->Subject = $param["sujet"];
	}else{
		$mail->Subject = "Si2P";
	}
	$mail->Body    = $body_mail;

	if(!$mail->send()) {
		return $mail->ErrorInfo;
	}else{
		return true;
	}
}
