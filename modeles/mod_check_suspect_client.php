<?php
function check_suspect($suspect){
	global $Conn;
	global $ConnSoc;
	//=============================
	//CONTROLES
	//=============================
	// CHAMPS SUSPECTS: code, nom, categorie, reg_formule, stagiaire_naiss, siren, siret, agence, id(0 = transfert direct)
	// CHAMPS ADRESSES FACTURATION :
	// CHAMPS ADRESSES INTERVENTION :
	// CONTROLES SUSPECT
	$erreur = "";
	if($suspect['id'] == 0 AND empty($suspect['siret'])){
		$erreur .= "<li>Veuillez renseigner le siret</li>";
	}
	if(empty($suspect['code']) OR empty($suspect['nom'])){
		//erreur
		$erreur .= "<li>Veuillez renseigner un code et un nom pour votre suspect</li>";
	}
	// CATEGORIE
	if(empty($suspect["categorie"])){
		$erreur.="<li>la catégorie n'est pas renseignée</li>";
	// FORMULE
	}elseif(empty($suspect["reg_formule"])){
		$erreur.="<li>le délai de règlement n'est pas renseigné</li>";
	}else{

		if($suspect["categorie"]==3){
			if(empty($suspect["stagiaire_naiss"])){
				$erreur.="<li>La date de naissance est obligatoire pour les particuliers</li>";
			}
		}else{
			// le siren est obligatoire
			if(empty($suspect["siren"])){
				$erreur.="<li>le siren n'est pas renseigné</li>";
			}
		}
	}
	// FIN CONTROLE SUSPECTS

	// CONTROLES ADRESSES
	$adr_inter_defaut = false;
	$adr_fac_defaut = false;
	echo("<pre>");

	foreach($suspect["adresses"] as $adr){
		if(empty($adr['sad_nom']) OR empty($adr['sad_cp']) OR empty($adr['sad_ville'])){
			$erreur .= "<li>Veuillez renseigner un nom, un code postal et une ville pour chaque adresse svp</li>";

		}

		// ADRESSE PAR DEFAUT // TYPE 1 INTER
		if($adr['sad_type'] == 1 && $adr['sad_type'] == 1){
			$adr_inter_defaut = true;
			if($suspect['categorie'] != 3 && !empty($adr['sad_id'])){
				// AUTRE QUE PARTICULIER
				$sql="SELECT * FROM suspects_adresses_contacts WHERE sac_adresse =" . $adr['sad_id'];
				$req = $ConnSoc->prepare($sql);
				$req->execute();
				$sac = $req->fetch();
				// IL N'Y A PAS DE CONTACT PAR DEFAUT LIEE A L'ADRESSE D'INTERVENTION PAR DEFAUT
				if(empty($sac)){
					$erreur .= "<li>Veuillez renseigner un contact par défaut pour l'adresse d'intervention par défaut</li>";
				}
			}
		}
		// ADRESSE PAR DEFAUT // TYPE 2 FAC
		if($adr['sad_defaut'] == 1 && $adr['sad_type'] == 2){
			$suspect['fac_adresse'] = $adr['sad_id'];
			$adr_fac_defaut = true;
			if($suspect['categorie'] == 3  && !empty($adr['sad_id'])){
				// PARTICULIER
				$sql="SELECT * FROM suspects_adresses_contacts WHERE sac_adresse =" . $adr['sad_id'];
				$req = $ConnSoc->prepare($sql);
				$req->execute();
				$sac2 = $req->fetch();

				if(empty($sac2)){
					$erreur .= "<li>Veuillez renseigner un contact par défaut pour l'adresse</li>";
				}
			}
		}

	}
	// CONTROLES SUPPLEMENTAIRES ADRESSES

	if($adr_fac_defaut == false){
		$erreur .= "<li>Il n'y a pas d'adresse de facturation par défaut</li>";
	}
	if($suspect['categorie'] != 3){
		if($adr_inter_defaut == false){
			$erreur .= "<li>Il n'y a pas d'adresse d'intervention par défaut</li>";
		}
	}
	// CONTROLES CONTACTS
	$con_compta = false;
	foreach($suspect['contacts'] as $con){
		if(!empty($con['sco_compta'])){
			$con_compta = true;
		}
	}
	if($con_compta == false){
		$erreur .= "<li>Il vous faut un contact compta</li>";
	}
	// ON RENVOIE L'ERREUR
	if(!empty($erreur)){
		return $erreur;
		exit();
	}
	// ALLER CHERCHER LE NOM DE LA SOCIETE
	$sql="SELECT soc_nom FROM societes WHERE soc_id =" . $_SESSION['acces']['acc_societe'];
	$req = $Conn->prepare($sql);
	$req->execute();
	$soc_nom = $req->fetch();

	$agence=0;
	if(!empty($suspect['agence'])){
		$suspect['agence']=intval($suspect['agence']);
	}
	// VERIFICATION DU SIRET
	if($suspect['categorie'] != 3){
		// SI L'ADRESSE EXISTE
		if($suspect['id'] != 0){
			$sql="SELECT cli_id,adr_id FROM Clients,Adresses WHERE cli_id=adr_ref_id AND cli_siren LIKE :siren AND adr_siret LIKE :siret
			AND NOT adr_id=" . $suspect['fac_adresse'];
		}else{
			$sql="SELECT cli_id,adr_id FROM Clients,Adresses WHERE cli_id=adr_ref_id AND cli_siren LIKE :siren AND adr_siret LIKE :siret";
		}
		$req=$Conn->prepare($sql);
		foreach($suspect["adresses"] as $adr){
			if($adr['sad_type'] == 2){
				$req->bindParam(":siren", $suspect['siren']);
				$req->bindParam(":siret", $adr['sad_siret']);
				$req->execute();
				$d_doublon=$req->fetchAll();
				$d_doublon_2 = "";
				foreach($d_doublon as $d){
					$sql="SELECT * FROM Clients_Societes LEFT JOIN societes ON (societes.soc_id = clients_societes.cso_societe) WHERE cso_client=" . $d['cli_id'];
					$req=$Conn->prepare($sql);
					$req->execute();
					$d_doublon_2[]=$req->fetch();
				}
				if(!empty($d_doublon_2)){
					foreach($d_doublon_2 as $d){
						$erreur .= "<li>Le siret " . $suspect['siren'] . " " . $adr['sad_siret'] . " existe déjà sur la société " . $d['soc_nom'] . "</li>";
					}
				}
			}
		}

	}

	if(!empty($erreur)){
		return $erreur;
		exit();
	}
	return null;
	// ON PEUT PASSER LE SUSPECT EN CLIENT




}
