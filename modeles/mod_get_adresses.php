<?php
// return la liste des adresses de adr_ref_id 
// adr_ref permet d'identifier la source
// type permet de ne retourner qu'un type d'adresse intervention par exemple

function get_adresses($ref,$ref_id, $type = 1){
	
	global $Conn;

	$req_sql="SELECT * FROM adresses WHERE adr_ref=:ref AND adr_ref_id=:ref_id";
	if($type>0){
		$req_sql.=" AND adr_type=:type";
	}
	$req_sql.=" ORDER BY adr_defaut DESC";

	$req=$Conn->prepare($req_sql);
	$req->bindParam(":ref",$ref);
	$req->bindParam(":ref_id",$ref_id);
	if($type>0){
		$req->bindParam(":type",$type);
	};
	$req->execute();
	$get_adresses = $req->fetchAll();
	
	return $get_adresses;
	
}

?>