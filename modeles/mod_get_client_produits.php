<?php

//RETOURNE LES DONNEES ASSOCIEE AUX PRODUITS D'UN CLIENT (TABLE PRODUITS ET Clients_Produits ET Produits_Societes)
// ancien mod_get_client.php => généralisé avec les différentes valeurs de $produit_id

 function get_client_produits($client,$produit_id,$aff_ht,$base_client,$acces,$pro_type,$pro_cat,$pro_fam,$pro_s_fam,$pro_s_s_fam,$archive){

	global $Conn;
	global $acc_societe;
	global $acc_agence;

	/*var_dump($acc_societe);
	die();*/

	/*echo("client : " . $client . "<br/>");
	echo("produit_id : " . $produit_id . "<br/>");
	echo("aff_ht : " . $aff_ht . "<br/>");
	echo("base_client : " . $base_client . "<br/>");
	echo("acces : " . $acces . "<br/>");
	echo("pro_type : " . $pro_type . "<br/>");
	echo("pro_cat : " . $pro_cat . "<br/>");
	echo("pro_fam : " . $pro_fam . "<br/>");
	echo("pro_s_fam : " . $pro_s_fam . "<br/>");
	echo("pro_s_s_fam : " . $pro_s_s_fam . "<br/>");
	die();*/

	$erreur=0;

	if(empty($client) AND empty($produit_id) ){
		$erreur=1;
	}else{

		if(is_null($produit_id)){

			$produit=array(
				"id" => 0,
				"client" => 0,
				"reference" => "",
				"libelle" => "",
				"intra" => false,		// existe en intra
				"intra_cli" => false,	// est propose au client en intra
				"ht_intra" => 0,
				"tg_intra" => 0,
				"min_intra" => 0,
				"min_dr_intra" => 0,
				"ca_verrou_intra" => false,
				"inter" => false,	// existe en intra
				"inter_cli" => false,	// est propose au client en intra
				"ht_inter" => 0,
				"tg_inter" => 0,
				"min_inter" => 0,
				"min_dr_inter" => 0,
				"ca_verrou_inter" => false,
				"derogation" => 0,
				"rem_famille" => 0,
				"rem_ca" => 0,
				"categorie" => 0,
				"famille" => 0,
				"sous_famille" => 0,
				"sous_sous_famille" => 0,
				"type" => 0,
				"facturation" => 0,
				"devis_txt" => "",
				"devis_txt_defaut" => "",
				"comment_txt" => "",
				"tva" => 0,
				"nb_demi_jour" => 0,
				"alerte_jour" => 0,
				"confirmation_gc" => 0,
				"cn" => 0,
				"valide" => 0,
				"deductible" => 0,
				"nego" => 0,
				"qualification" => 0
			);


		}else{

			$produit_id=intval($produit_id);

			// SUR LE CLIENT

			$client_produit=0;
			if(!empty($client)){

				$sql="SELECT cli_filiale_de,cli_interco_age,cli_interco_soc,cli_categorie FROM Clients WHERE cli_id=" . $client . ";";
				$req=$Conn->query($sql);
				$d_client=$req->fetch();
				if(!empty($d_client)){
					if(!empty($d_client["cli_filiale_de"])){
						$client_produit=$d_client["cli_filiale_de"];
					}else{
						$client_produit=$client;
					}
				}


				// SUR LA SOCIETE / AGENCE

				// regle => Moi acc_societe / acc_agence je facture le client (qui est une interco) N % du TG

				if(!empty($d_client["cli_interco_age"]) OR !empty($d_client["cli_interco_soc"]) ){

					$prct_interco_intra=50;
					$prct_interco_inter=50;
					if(!empty($acc_agence)){
						$sql="SELECT age_interco_intra,age_interco_inter FROM Agences WHERE age_id=" . $acc_agence . ";";
						$req = $Conn->query($sql);
						$d_agence = $req->fetch();
						if(!empty($d_agence)){
							if(!empty($d_agence["age_interco_intra"])){
								$prct_interco_intra=$d_agence["age_interco_intra"];
							}
							if(!empty($d_agence["age_interco_inter"])){
								$prct_interco_inter=$d_agence["age_interco_inter"];
							}
						}
					}else{

						$sql="SELECT soc_interco_intra,soc_interco_inter FROM Societes WHERE soc_id=" . $acc_societe . ";";
						$req = $Conn->query($sql);
						$d_societe = $req->fetch();
						if(!empty($d_societe)){
							if(!empty($d_societe["soc_interco_intra"])){
								$prct_interco_intra=$d_societe["soc_interco_intra"];
							}
							if(!empty($d_societe["soc_interco_inter"])){
								$prct_interco_inter=$d_societe["soc_interco_inter"];
							}
						}
					}
				}
			}else{
				$d_client=array(
					"cli_categorie" => 1
				);
			}

			// DONNEE GENRIQUE DU PRODUIT

			$sql="SELECT pro_id,pro_code_produit,pro_libelle,
			pro_intra,pro_ht_intra,pro_min_intra,pro_min_dr_intra,
			pro_inter,pro_ht_inter,pro_min_inter,pro_min_dr_inter,
			pro_categorie,pro_famille,pro_sous_famille,pro_sous_sous_famille,
			pro_nb_demi_jour,pro_devis_txt,pro_tva,pro_client,
			pso_devis_txt,pro_deductible,pro_qualification";
			if($client_produit>0){
				$sql.=",cpr_produit,cpr_libelle,cpr_intra,cpr_ht_intra,cpr_min_intra,cpr_inter,cpr_ht_inter,cpr_min_inter,cpr_alerte_jour,cpr_cn,cpr_comment_txt,cpr_confirmation_gc
				,cpr_devis_txt,cpr_valide,cpr_derogation,cpr_rem_famille,cpr_rem_ca,cpr_nego";
			}
			$sql.=" FROM Produits LEFT OUTER JOIN Produits_Societes ON (Produits.pro_id=Produits_Societes.pso_produit AND pso_societe=:acc_societe)";
			if($client_produit>0){
				$sql.=" LEFT OUTER JOIN Clients_Produits ON (Produits.pro_id=Clients_Produits.cpr_produit AND cpr_client=:client";
				if(!empty($pro_type)){
					if($pro_type==1){
						$sql.=" AND cpr_intra";
					}elseif($pro_type==2){
						$sql.=" AND cpr_inter";
					}
				}
				$sql.=")";
			}

			$mil="";
			if(!is_null($archive)){
				if($archive==0){
					$mil=" AND NOT pro_archive";
				}else{
					$mil=" AND pro_archive";
				}
			}

			if(!empty($pro_cat)){
				$mil.=" AND pro_categorie=:pro_cat";
			}
			if(!empty($pro_fam)){
				$mil.=" AND pro_famille=:pro_fam";
			}
			if(!empty($pro_s_fam)){
				$mil.=" AND pro_sous_famille=:pro_s_fam";
			}
			if(!empty($pro_s_s_fam)){
				$mil.=" AND pro_sous_sous_famille=:pro_s_s_fam";
			}
			if(!empty($produit_id)){
				$mil.=" AND pro_id=:produit";
			}
			if(!empty($base_client)){
				if($base_client==1){
					$mil.=" AND NOT ISNULL(cpr_produit)";
				}
			}
			if(!empty($pro_type)){
				if($pro_type==1){
					$mil.=" AND pro_intra";
				}elseif($pro_type==2){
					$mil.=" AND pro_inter";
				}
			}
			if(!empty($acces)){
				if($acces==1){
					// seul drt GC peuvent utiliser des tarifs non validés
					if($d_client["cli_categorie"]==2 AND !$_SESSION["acces"]["acc_droits"][8]){
						$mil.=" AND cpr_valide=1";
					}
				}
			}
			if($mil!=""){
				$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
			}

			$sql.=" ORDER BY pro_code_produit";
			$req = $Conn->prepare($sql);
			if(!empty($pro_cat)){
				$req->bindParam(":pro_cat",$pro_cat);
			}
			if(!empty($pro_fam)){
				$req->bindParam(":pro_fam",$pro_fam);
			}
			if(!empty($pro_s_fam)){
				$req->bindParam(":pro_s_fam",$pro_s_fam);
			}
			if(!empty($pro_s_s_fam)){
				$req->bindParam(":pro_s_s_fam",$pro_s_s_fam);
			}
			if(!empty($produit_id)){
				$req->bindParam(":produit",$produit_id);
			}
			$req->bindParam(":acc_societe",$acc_societe);
			if($client_produit>0){
				$req->bindParam(":client",$client_produit);
			}
			$req->execute();
			//var_dump($sql);
			$donnees_produits = $req->fetchAll();
			if(!empty($donnees_produits)){

				foreach($donnees_produits as $p){


					// VALEUR GENERIQUE

					$produit[$p["pro_id"]]=array();

					$produit[$p["pro_id"]]["id"]=$p["pro_id"];
					$produit[$p["pro_id"]]["client"]=$p["pro_client"];
					$produit[$p["pro_id"]]["reference"]=$p["pro_code_produit"];
					$produit[$p["pro_id"]]["libelle"]=$p["pro_libelle"];
					$produit[$p["pro_id"]]["categorie"]=$p["pro_categorie"];
					$produit[$p["pro_id"]]["famille"]=$p["pro_famille"];
					$produit[$p["pro_id"]]["sous_famille"]=$p["pro_sous_famille"];
					$produit[$p["pro_id"]]["sous_sous_famille"]=$p["pro_sous_sous_famille"];
					if($p["pro_intra"]){
						$produit[$p["pro_id"]]["intra"]=true;
						$produit[$p["pro_id"]]["tg_intra"]=$p["pro_ht_intra"];
						$produit[$p["pro_id"]]["min_intra"]=$p["pro_min_intra"];
						$produit[$p["pro_id"]]["min_dr_intra"]=$p["pro_min_dr_intra"];
						//valeur pouvant etre surclasse par base produit du client
						$produit[$p["pro_id"]]["ht_intra"]=$p["pro_ht_intra"];
						$produit[$p["pro_id"]]["cpr_min_intra"]=$p["pro_min_intra"]; // val min selon derogation

					}else{
						$produit[$p["pro_id"]]["intra"]=false;
						$produit[$p["pro_id"]]["tg_intra"]=0;
						$produit[$p["pro_id"]]["min_intra"]=0;
						$produit[$p["pro_id"]]["min_dr_intra"]=0;
						$produit[$p["pro_id"]]["ht_intra"]=0;
						$produit[$p["pro_id"]]["cpr_min_intra"]=0;
					}


					if($p["pro_inter"]){
						$produit[$p["pro_id"]]["inter"]=true;
						$produit[$p["pro_id"]]["tg_inter"]=$p["pro_ht_inter"];
						$produit[$p["pro_id"]]["min_inter"]=$p["pro_min_inter"];
						$produit[$p["pro_id"]]["min_dr_inter"]=$p["pro_min_dr_inter"];
						//valeur pouvant etre surclasse par base produit du client
						$produit[$p["pro_id"]]["ht_inter"]=$p["pro_ht_inter"];
						$produit[$p["pro_id"]]["cpr_min_inter"]=$p["pro_min_inter"]; // val min selon derogation

					}else{
						$produit[$p["pro_id"]]["inter"]=false;
						$produit[$p["pro_id"]]["tg_inter"]=0;
						$produit[$p["pro_id"]]["min_inter"]=0;
						$produit[$p["pro_id"]]["min_dr_inter"]=0;
						$produit[$p["pro_id"]]["ht_inter"]=0;
						$produit[$p["pro_id"]]["cpr_min_inter"]=0;

					}

					$produit[$p["pro_id"]]["devis_txt"]="";
					$produit[$p["pro_id"]]["devis_txt_defaut"]="";
					if(!empty($p["pro_devis_txt"])){
						$produit[$p["pro_id"]]["devis_txt"]=$p["pro_devis_txt"];
					}
					if(!empty($p["pso_devis_txt"])){
						$produit[$p["pro_id"]]["devis_txt_defaut"]=$produit[$p["pro_id"]]["devis_txt"];
						$produit[$p["pro_id"]]["devis_txt"]=$p["pso_devis_txt"];
					}
					if(!empty($p["pro_tva"])){
						$produit[$p["pro_id"]]["tva"]=$p["pro_tva"];
					}else{
						$produit[$p["pro_id"]]["tva"]=3;	// par defaut tva 0
					}

					if(!empty($p["pro_nb_demi_jour"])){
						$produit[$p["pro_id"]]["nb_demi_jour"]=$p["pro_nb_demi_jour"];
					}else{
						$produit[$p["pro_id"]]["nb_demi_jour"]=0;
					}
					if(!empty($p["pro_deductible"])){
						$produit[$p["pro_id"]]["deductible"]=1;
					}else{
						$produit[$p["pro_id"]]["deductible"]=0;
					}
					if(!empty($p["pro_qualification"])){
						$produit[$p["pro_id"]]["qualification"]=$p["pro_qualification"];
					}

					// ini val presente que dans clients produits
					$produit[$p["pro_id"]]["intra_cli"]=false;
					$produit[$p["pro_id"]]["inter_cli"]=false;
					$produit[$p["pro_id"]]["comment_txt"]="";
					$produit[$p["pro_id"]]["alerte_jour"]=0;
					$produit[$p["pro_id"]]["confirmation_gc"]=0;
					$produit[$p["pro_id"]]["cn"]=0;
					$produit[$p["pro_id"]]["valide"]=0;
					$produit[$p["pro_id"]]["ca_verrou_inter"]=false;
					$produit[$p["pro_id"]]["ca_verrou_intra"]=false;
					$produit[$p["pro_id"]]["facturation"]=0;

					$produit[$p["pro_id"]]["derogation"]=0;
					$produit[$p["pro_id"]]["rem_ca"]=0;
					$produit[$p["pro_id"]]["rem_famille"]=0;

					$produit[$p["pro_id"]]["nego"]=0;

					if($client_produit>0){

						// perso par le client

						// donnee surclasse seulement si Clients_Produits existe

						if(!empty($p["cpr_produit"])){

							if(!empty($p["cpr_libelle"])){
								$produit[$p["pro_id"]]["libelle"]=$p["cpr_libelle"];
							}

							if(!empty($p["pro_intra"]) AND !empty($p["cpr_intra"])){
								$produit[$p["pro_id"]]["intra_cli"]=true;
								$produit[$p["pro_id"]]["ht_intra"]=$p["cpr_ht_intra"];
                                $produit[$p["pro_id"]]["min_intra"]=$p["cpr_ht_intra"];
								$produit[$p["pro_id"]]["cpr_min_intra"]=$p["cpr_min_intra"];
							}else{
								$produit[$p["pro_id"]]["ht_intra"]=0;
								$produit[$p["pro_id"]]["cpr_min_intra"]=0;
							}

							if(!empty($p["cpr_inter"]) AND !empty($p["cpr_inter"]) ){
								$produit[$p["pro_id"]]["inter_cli"]=true;
								$produit[$p["pro_id"]]["ht_inter"]=$p["cpr_ht_inter"];
								$produit[$p["pro_id"]]["cpr_min_inter"]=$p["cpr_min_inter"];
							}else{
								$produit[$p["pro_id"]]["inter_cli"]=false;
								$produit[$p["pro_id"]]["ht_inter"]=0;
								$produit[$p["pro_id"]]["cpr_min_inter"]=0;
							}

							$produit[$p["pro_id"]]["derogation"]=$p["cpr_derogation"];
							$produit[$p["pro_id"]]["rem_famille"]=$p["cpr_rem_famille"];
							$produit[$p["pro_id"]]["rem_ca"]=$p["cpr_rem_ca"];
							$produit[$p["pro_id"]]["nego"]=$p["cpr_nego"];

							if(!empty($p["cpr_devis_txt"])){
								$produit[$p["pro_id"]]["devis_txt_defaut"]=$produit[$p["pro_id"]]["devis_txt"];
								$produit[$p["pro_id"]]["devis_txt"]=$p["cpr_devis_txt"];
							}
							if(!empty($p["cpr_comment_txt"])){
								$produit[$p["pro_id"]]["comment_txt"]=$p["cpr_comment_txt"];
							}

							if(!empty($p["cpr_alerte_jour"])){
								$produit[$p["pro_id"]]["alerte_jour"]=$p["cpr_alerte_jour"];
							}

							if(!empty($p["cpr_confirmation_gc"])){
								$produit[$p["pro_id"]]["confirmation_gc"]=1;
							}else{
								$produit[$p["pro_id"]]["confirmation_gc"]=0;
							}
							if(!empty($p["cpr_cn"])){
								$produit[$p["pro_id"]]["cn"]=1;
							}else{
								$produit[$p["pro_id"]]["cn"]=0;
							}
							if(!empty($p["cpr_valide"])){
								$produit[$p["pro_id"]]["valide"]=1;
							}else{
								$produit[$p["pro_id"]]["valide"]=0;
							}


							if($p["cpr_derogation"]>0 AND !$_SESSION["acces"]["acc_droits"][27] AND !$_SESSION["acces"]["acc_droits"][31]){
								$produit[$p["pro_id"]]["ca_verrou_inter"]=true;
								$produit[$p["pro_id"]]["ca_verrou_intra"]=true;
							}elseif($p["cpr_derogation"]>1 AND !$_SESSION["acces"]["acc_droits"][31]){
								$produit[$p["pro_id"]]["ca_verrou_inter"]=true;
								$produit[$p["pro_id"]]["ca_verrou_intra"]=true;
							}
						}
					}

					if(!empty($d_client["cli_interco_age"]) OR !empty($d_client["cli_interco_soc"]) ){

						// INTERCO PAS DE BASE PRODUIT
						//if($_SESSION["acces"]["acc_profil"]!=15 AND $_SESSION["acces"]["acc_profil"]!=5 AND $_SESSION["acces"]["acc_service"][1]!=1){
						//	$produit[$p["pro_id"]]["ca_verrou_inter"]=true;
						//	$produit[$p["pro_id"]]["ca_verrou_intra"]=true;
						//}

						if($aff_ht==1){
							$produit[$p["pro_id"]]["ht_inter"]=($produit[$p["pro_id"]]["tg_inter"]*$prct_interco_inter)/100;
							$produit[$p["pro_id"]]["min_inter"]=($produit[$p["pro_id"]]["tg_inter"]*$prct_interco_inter)/100;
							$produit[$p["pro_id"]]["cpr_min_inter"]=($produit[$p["pro_id"]]["tg_inter"]*$prct_interco_inter)/100;

							$produit[$p["pro_id"]]["ht_intra"]=($produit[$p["pro_id"]]["tg_intra"]*$prct_interco_intra)/100;
							$produit[$p["pro_id"]]["min_intra"]=($produit[$p["pro_id"]]["tg_intra"]*$prct_interco_intra)/100;
							$produit[$p["pro_id"]]["cpr_min_intra"]=($produit[$p["pro_id"]]["tg_intra"]*$prct_interco_intra)/100;
						}
					}elseif($d_client["cli_categorie"]==2){
						// GC
						if(!$_SESSION["acces"]["acc_droits"][8]){
							$produit[$p["pro_id"]]["ca_verrou_inter"]=true;
							$produit[$p["pro_id"]]["ca_verrou_intra"]=true;
						}


						if($produit[$p["pro_id"]]["cn"]==1 AND $acc_societe!=4){
							// CONTRAT NATIONAL
							// utiliser pour la facturation des actions et double fac impssible sur meme soc de gfc à gfc
							$produit[$p["pro_id"]]["facturation"]=1;
						}

						if($aff_ht==1 AND $produit[$p["pro_id"]]["facturation"]==1){

							// U facture 88% du prix
							$produit[$p["pro_id"]]["ht_inter"]=($produit[$p["pro_id"]]["ht_inter"]*88)/100;
							$produit[$p["pro_id"]]["ht_intra"]=($produit[$p["pro_id"]]["ht_intra"]*88)/100;

							// Palier tombe a 88%
							$produit[$p["pro_id"]]["min_intra"]=($produit[$p["pro_id"]]["min_intra"]*88)/100;
							$produit[$p["pro_id"]]["min_dr_intra"]=($produit[$p["pro_id"]]["min_dr_intra"]*88)/100;
							$produit[$p["pro_id"]]["min_inter"]=($produit[$p["pro_id"]]["min_inter"]*88)/100;
							$produit[$p["pro_id"]]["min_dr_inter"]=($produit[$p["pro_id"]]["min_dr_inter"]*88)/100;

							// prix mini selon dérogation tombe à 88%
							$produit[$p["pro_id"]]["cpr_min_inter"]=($produit[$p["pro_id"]]["cpr_min_inter"]*88)/100;
							$produit[$p["pro_id"]]["cpr_min_intra"]=($produit[$p["pro_id"]]["cpr_min_intra"]*88)/100;

						}

					}elseif($d_client["cli_categorie"]==5 AND $produit[$p["pro_id"]]["valide"] AND $_SESSION["acces"]["acc_service"][1]!=1){
						// CLIENT NATIONAL
						$produit[$p["pro_id"]]["ca_verrou_inter"]=true;
						$produit[$p["pro_id"]]["ca_verrou_intra"]=true;
						/* var_dump($produit[$p["pro_id"]]["ca_verrou_intra"]);
							die(); */
					}
				}

				if(!empty($produit_id)){
					$produit=$produit[$produit_id];
				}
			}else{
				$produit=array();
			}
		}
		// FIN NOT ISNULL
	}

	if(empty($erreur)){
		return $produit;
	}else{
		return $erreur;
	}
 }
?>
