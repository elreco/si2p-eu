<?php

// GENERE DES ATTESTATIONS EN PDF

//require_once("../vendor/autoload.php");
require_once("../vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;

function model_convention_pdf($data){
	
	global $acc_societe;

	$erreur="";
	ob_start(); ?>
	
	<style type="text/css" >

<?php	require("../assets/skin/si2p/css/orion_pdf.css"); ?>

	</style>
	<page backtop="22mm" backleft="0mm" backright="0mm" backbottom="14mm" >
		<page_header>							
			<table class="w-100" >
				<tr>
					<td class="w-25" >
			<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
							<img src="../<?=$data["juridique"]["logo_1"]?>" />
			<?php		}else{
							echo("&nbsp;");
						} ?>
					</td>
					<td class="w-50" >&nbsp;</td>
					<td class="w-25 text-right" >
			<?php		if(!empty($data["juridique"]["logo_2"])){ ?>
							<img src="../<?=$data["juridique"]["logo_2"]?>" />
			<?php		}else{
							echo("&nbsp;");
						} ?>
					</td>
				</tr>													
			</table>
		</page_header>
		<page_footer>
			<div class="footer" >
				<table class="w-100" >
					<tr>
						<td class="w-10"  >
				<?php		if(!empty($data["juridique"]["qualite_1"])){ ?>
								<img src="../<?=$data["juridique"]["qualite_1"]?>" />
				<?php		}else{
								echo("&nbsp;");
							} ?>
						</td>
						<td class="w-80 text-center" >
					<?php	echo($data["juridique"]["nom"] . " - ");
							if(!empty($data["juridique"]["ad1"])){
								echo($data["juridique"]["ad1"] . " - ");
							}
							if(!empty($data["juridique"]["ad2"])){
								echo($data["juridique"]["ad2"] . " - ");
							}
							if(!empty($data["juridique"]["ad3"])){
								echo($data["juridique"]["ad3"] . " - ");
							}
							echo($data["juridique"]["cp"] . " " . $data["juridique"]["ville"]);
							
							if(!empty($data["juridique"]["tel"])){
								echo(" - TEL. " . $data["juridique"]["tel"]);
							}
							if(!empty($data["juridique"]["fax"])){
								echo(" - FAX. " . $data["juridique"]["fax"]);
							}
							echo("<br/>");
							echo($data["juridique"]["footer"]);
							?>
						</td>
						<td class="w-10"  >
				<?php		if(!empty($data["juridique"]["qualite_2"])){ ?>
								<img src="../<?=$data["juridique"]["qualite_2"]?>" />
				<?php		}else{
								echo("&nbsp;");
							} ?>
						</td>
					</tr>
				</table>
			</div>
		</page_footer>
		
		
			<h1 class="text-center" >CONVENTION DE FORMATION PROFESSIONNELLE</h1>
			<h2 class="text-center" ><?=$data["con_numero"]?></h2>
		
		
		
			<table>
				<tr>
					<td class="text-right w-33 tab-label" ><strong>Entre les soussignés :</strong></td>
					<td class="w-67" >													
				<?php	echo("<b>" . $data["juridique"]["nom"] . "</b>");
						if(!empty($data["juridique"]["agence"])){
							echo("<br/><b>" . $data["juridique"]["agence"] . "</b>");
						}
						if(!empty($data["juridique"]["ad1"])){
							echo("<br/><b>" . $data["juridique"]["ad1"] . "</b>");
						}
						if(!empty($data["juridique"]["ad2"])){
							echo("<br/><b>" . $data["juridique"]["ad2"] . "</b>");
						}
						if(!empty($data["juridique"]["ad3"])){
							echo("<br/><b>" . $data["juridique"]["ad3"] . "</b>");
						}
						echo("<br/><b>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"] . "</b>");
						echo("<br/>SIRET " . $data["juridique"]["siret"]);
						echo("<br/>APE " . $data["juridique"]["ape"]);
						/*echo("<pre>");
							print_r($data["juridique"]);
						echo("</pre>");*/ ?>
					</td>
				</tr>
			</table>
		
		
		
			<p style="border:1px solid red;" >
				N° de déclaration d’existence <?=$data["juridique"]["num_existence"]?> enregistré auprès de la Préfecture de la Région <?=$data["juridique"]["region_nom"]?>. Ci-après désigné <b>« l’Organisme de Formation »</b>
			</p>
		
		
		
			<table>
				<tr>
					<td class="w-33 text-right tab-label" >				
						<strong>Et :</strong>
					</td>
					<td class="w-67 text-strong" id="con_adresse_txt" ><?=$data["adresse_client"]?></td>
				</tr>
			</table>
		
		
		
			<p>
				Ci-après désigné <b>« l’Entreprise »</b>
			</p>
												
		
		
			<p>
				est conclue la convention suivante, en application des dispositions de la sixième partie du Code du Travail portant organisation de la formation professionnelle tout au long de la vie. 
			</p>
											
		
		
			<strong>ARTICLE 1 : Objet de la Convention</strong>
			<p>En exécution de la présente convention, l’organisme de formation <?=$data["juridique"]["nom"]?> s’engage à organiser l’action de formation suivante : </p>
	
		
		
			<table>
				<tr>
					<td class="w-33 text-right tab-label" >						
						<strong>Intitulé du stage :</strong>
					</td>																								
					<td class="w-67 text-strong" id="con_libelle_txt" ><?=$data["libelle"]?></td>
				</tr>
			</table>
		
		
	
			<p>
				Le programme, les objectifs, les méthodes et moyens pédagogiques sont annexés à la présente convention. 
			</p>
		
		
		
			<table>
				<tr>
					<td class="w-33 text-right tab-label" >					
						<strong>Date(s) :</strong>
					</td>
					<td class="w-67 text-strong con_form_dates" id="con_form_dates" >
				<?php	if(!empty($data["con_form_dates"])){
							echo(str_replace("\r\n","<br>",$data["con_form_dates"]));
						} ?>
					</td>
				</tr>
			</table>
		
		
		
			<table>
				<tr>
					<td class="w-33 text-right" ><strong>Durée :</strong></td>
					<td class="w-67 text-strong" id="con_form_duree" ><?=$data["con_form_duree"]?></td>
				</tr>
			</table>
		
		
		
			<table>
				<tr>
					<td class="w-33 text-right tab-label" >						
						<strong>Lieu(x) de formation :</strong>
					</td>
					<td class="w-67 text-strong" id="con_lieu" ><?=$data["lieu"]?></td>
				</tr>
			</table>
		
		
			<table>
				<tr>
					<td class="w-33 text-right tab-label" >						
						<strong>Effectif formé :</strong>													
					</td>
					<td class="w-67 text-strong" id="con_form_effectif_text" >												
				<?php	if($data["con_form_effectif"]==1){
							echo("cf. annexe");
						}elseif($data["con_form_effectif"]==3){
							echo("cf. feuille de présence");
						}else{
							echo(str_replace("\r\n","<br>",$data["con_form_effectif_text"]));
						} ?>
					</td>
				</tr>
			</table>
		
		
		
			<p>
				<b>Modalités de suivi :</b> fiches de présence émargées  
			</p>
		
		
		
			<p>
				<b>Appréciation des résultats :</b> évaluation en fin de stage qui permet au formateur de déterminer si les stagiaires ont acquis les connaissances et les gestes professionnels, dont la maîtrise constitue l'objectif initial de l'action de formation. 
			</p>
		
		
		
			<strong>ARTICLE 2 : Dispositions financières</strong>
			<p>
				a) Le client, en contrepartie des actions de formation réalisées, s’engage à verser à l’organisme de formation, une somme correspondant :<br/>
				• aux frais de formation :
			</p>
											
		
			<nobreak>
				<table class="tab-border w-100" >
					<tr>	
						<th class="w-25" >&nbsp;</th>
						<th class="w-25" >Dates</th>
						<th class="w-25" >Quantité</th>
						<th class="w-25" >€uros</th>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="text-strong con_form_dates" >
					<?php	if(!empty($data["con_form_dates"])){
								echo(str_replace("\r\n","<br>",$data["con_form_dates"]));
							} ?>
						</td>
						<td id="con_qte" ><?=$data["con_qte"] . " " . $data["con_unite_txt"]?></td>
						<td class="con_ht text-right" ><?=$data["con_ht"]?> €</td>
					</tr>
					<tr>
						<td class="text-right" >Coût Total € HT :</td>
						<td colspan="2" >&nbsp;</td>
						<td class="con_ht text-right" ><?=$data["con_ht"]?> €</td>														
					</tr>											
					<tr id="bloc_tva" <?php	if($data["con_tva_id"]==3) echo("style='display:none;'"); ?> >												
						<td class="text-right" id="con_tva_taux" >TVA à <?=$data["con_tva_taux"]?>% :</td>
						<td colspan="2" >&nbsp;</td>
						<td id="con_tva" class="text-right" ><?=$data["con_tva"]?> €</td>														
					</tr>										
					<tr>
						<td class="text-right" >Coût TTC :</td>
						<td colspan="2" >&nbsp;</td>
						<td id="con_ttc" class="text-right" ><?=$data["con_ttc"]?> €</td>														
					</tr>
				</table>
			</nobreak>
		
		
	<?php	if(!empty($data["con_comment"])){
				echo(str_replace("\r\n","<br>",$data["con_comment"]));
			} ?>
	
		
		
			<p>
				L’organisme de formation, en contrepartie des sommes reçues, s’engage à réaliser toutes les actions prévues dans le cadre de la présente convention ainsi qu’à fournir tout document et pièce de nature à justifier la réalité et la validité des dépenses de formation engagées à ce titre. 
			</p>
		
		
			<p>
				b) Modalités de règlement : La facture devra être réglée à 30 jours par chèque ou virement. Aucun escompte ne sera accordé quelle que soit la date de règlement. Les intérêts de retard seront calculés sur la base de 1,5 fois le taux d'intérêt légal. 
			</p>
		
		
		
			<strong>ARTICLE 3 : Dédit ou abandon</strong>
			<p>
				a) En cas de résiliation de la présente convention par le client à moins de 10 jours ouvrés avant le début d’une des actions mentionnées, l’organisme se réserve le droit de réclamer une indemnité égale :													
				<ul>
					<li>
						à 50% du montant de la formation, si la demande d’annulation parvient entre le 9ème et le 3ème jour ouvrés avec le début de
				la session de Formation ;
					</li>
					<li>
						à 100% du montant de la formation, si la demande d’annulation parvient moins de 3 jours ouvrés avec le début de la session
				de formation
					</li>
				</ul>
				En cas d’absence à la formation, de retard, de participation partielle, d’abandon ou de cessation anticipée pour tout autre raison
				que la force majeure dûment reconnue, le client sera redevable de l’intégralité du montant de la formation.

			</p>
		
		
		
			<p>
				c) Les montants versés par le client au titre de dédommagement ne pourront pas être imputés par le client sur son obligation définie à l’article L6331-1 du Code du Travail ni faire l’objet d’une demande de remboursement ou de prise en charge par un OPCA. 
			</p>
		
		
		
			<p>
				d) Dans le cas où <?=$data["juridique"]["nom"]?> serait amené à annuler l'action objet de la présente convention, cette dernière serait considérée comme caduque. L'entreprise sera avertie dans les meilleurs délais par lettre recommandée. Les sommes éventuellement perçues lui seront intégralement reversées. 
			</p>
		
		
		
			<strong>ARTICLE 4 : Date d’effet et durée de la convention</strong>
			<p>
				La présente convention prend effet à compter de sa signature par le client, pour s’achever au terme de l'action de formation visé à l'article 1 de la présente convention. 
			</p>
		
		
		
			<strong>ARTICLE 5 : Différends éventuels</strong>
			<p>
				Si une contestation ou un différend ne peuvent être réglés à l’amiable, le Tribunal d'Angers sera seul compétent pour se prononcer sur le litige. 
			</p>
		
		
		
			<strong>ARTICLE 6 : Données personnelles et confidentialité</strong>
			<p>
				L’Organisme de Formation gère toutes les données personnelles reçues ou collectées dans le cadre de la Convention (collectivement les «Données personnelles») conformément aux lois et règlements applicables en matière de protection des données personnelles. En particulier, l’Organisme de Formation veillera à ce que des mesures de sécurité techniques et organisationnelles adéquates soient mises en œuvre et maintenues pour protéger les Données personnelles contre toute destruction ou perte accidentelle ou illégale, toute altération, divulgation ou accès non-autorisé, et toute autres formes illégales de traitement. Toutes les données et informations divulguées par l’Entreprise et acquises ou traitées par l’Organisme de Formation, dans le cadre de la Convention doivent être traitées comme strictement confidentielles. 
			</p>
		
		
		<end_last_page end_height="95mm">
			<p>Fait en double exemplaire, à <?=$data["juridique"]["ville"]?>, le <span class="text-strong" ><?=$data["con_date"]?></span></p>	
			<table class="table" >
				<tr>
					<td class="w-50 tab-label" >
						Pour le client,
						(nom et qualité du signataire) 
					</td>
					<td class="" style="padding-bottom:100px;" >
							<div>
								Pour le <?=$data["juridique"]["nom"]?>,
								(nom et qualité du signataire)								
						<?php	if(!empty($data["juridique"]["resp_ident"])){
									echo("<br/>" . $data["juridique"]["resp_ident"]);
								}
								if(!empty($data["juridique"]["resp_fonction"])){
									echo("<br/>" . $data["juridique"]["resp_fonction"]);
								} ?>
							</div>
							
					<?php	if(!empty($data["juridique"]["resp_signature_tampon"])){ ?>
								<div>
									<img src="../<?=$data["juridique"]["resp_signature_tampon"]?>" />
								</div>
								<div>
									<img src="../<?=$data["juridique"]["resp_signature"]?>" style="margin-top:-100px;"/>
								</div>
					<?php	}elseif(!empty($data["juridique"]["resp_signature"])){ ?>
								<div>
									<img src="../<?=$data["juridique"]["resp_signature"]?>" />
								</div>
					<?php	}?>
					</td>
				</tr>
			</table>
		</end_last_page>	
		
	</page>

<?php 	
	if($data["con_form_effectif"]==1){ ?>

		<!--ANNEXE -->
		<page backtop="22mm" backleft="0mm" backright="0mm" backbottom="14mm" >
			<page_header>							
				<table class="w-100" >
					<tr>
						<td class="w-25" >
				<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
								<img src="../<?=$data["juridique"]["logo_1"]?>" class="w-100" />
				<?php		}else{
								echo("&nbsp;");
							} ?>
						</td>
						<td class="w-50" >&nbsp;</td>
						<td class="w-25 text-right" >
				<?php		if(!empty($data["juridique"]["logo_2"])){ ?>
								<img src="../<?=$data["juridique"]["logo_2"]?>" class="w-100" />
				<?php		}else{
								echo("&nbsp;");
							} ?>
						</td>
					</tr>													
				</table>
			</page_header>
			
			<h1 class="text-center" >CONVENTION DE FORMATION PROFESSIONNELLE</h1>
			<h2>Annexe : Effectif formé</h2>
			
			<p id="annexe_stagiaire" >
		<?php	$html_sta="";
				if(!empty($data["liste_stagiaires"])){															
					echo("<ul>");
						foreach($data["liste_stagiaires"] as $sta){
							echo("<li>" . $sta["sta_nom"] . " " . $sta["sta_prenom"] . "</li>");
							$html_sta.=$sta["sta_nom"] . " " . $sta["sta_prenom"] . "<br/>";
						}
					echo("</ul>");	
				} ?>													
			</p>
			<page_footer>
				<div class="footer" >
					<table class="w-100" >
						<tr>
							<td class="w-10" >
					<?php		if(!empty($data["juridique"]["qualite_1"])){ ?>
									<img src="../<?=$data["juridique"]["qualite_1"]?>" class="img-responsive" />
					<?php		}else{
									echo("&nbsp;");
								} ?>
							</td>
							<td class="w-80 text-center" >
						<?php	echo($data["juridique"]["nom"] . " - ");
								if(!empty($data["juridique"]["ad1"])){
									echo($data["juridique"]["ad1"] . " - ");
								}
								if(!empty($data["juridique"]["ad2"])){
									echo($data["juridique"]["ad2"] . " - ");
								}
								if(!empty($data["juridique"]["ad3"])){
									echo($data["juridique"]["ad3"] . " - ");
								}
								echo($data["juridique"]["cp"] . " " . $data["juridique"]["ville"]);
								
								if(!empty($data["juridique"]["tel"])){
									echo(" - TEL. " . $data["juridique"]["tel"]);
								}
								if(!empty($data["juridique"]["fax"])){
									echo(" - FAX. " . $data["juridique"]["fax"]);
								}
								echo("<br/>");
								echo($data["juridique"]["footer"]); ?>
							</td>
							<td class="text-right w-10" >
					<?php		if(!empty($data["juridique"]["qualite_2"])){ ?>
									<img src="../<?=$data["juridique"]["qualite_2"]?>" />
					<?php		}else{
									echo("&nbsp;");
								} ?>
							</td>
						</tr>
					</table>
				</div>
			</page_footer>											
		</page>
<?php	
	}
		
	$content=ob_get_clean();
	
	$resultat=true;
	try{
		$pdf=new HTML2PDF('P','A4','fr');
		$pdf->pdf->SetDisplayMode('fullwidth');
		/*echo($content);
		die();*/
		$pdf->writeHTML($content);
		if($pdf){
			$pdf->Output($_SERVER["DOCUMENT_ROOT"] . '/Documents/Societes/' . $acc_societe . '/Conventions/' . $data["con_numero"] . '.pdf', 'F');					
		}
	}catch(HTML2PDF_exception $e){
		$resultat=false;
	}
	
	return $resultat;
	
}
?>