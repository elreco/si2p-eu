<?php

// RETOURNE LES INTERVENANTS AUXQUELS L'UTILISATEUR A ACCES

// AUTEUR FG

function get_intervenants($agence,$utilisateur,$type,$archive){

	global $ConnSoc;
	
	$sql="SELECT int_id,int_ref_1,int_ref_2,int_label_1,int_label_2,int_type,int_agence,int_option FROM Intervenants";
	
	$mil="";
	if($agence>0){
		$mil.=" AND (int_agence=:agence OR int_agence=0)";
	}
	if(is_int($type)){
		$mil.=" AND int_type=:type";
	}
	if(is_int($archive)){
		if($archive==1){
			$mil.=" AND int_archive";
		}else{
			$mil.=" AND NOT int_archive";
		}
	}
	if(!empty($mil)){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY int_label_1,int_label_2";
	$req = $ConnSoc->prepare($sql);
	if($agence>0){
		$req->bindParam(":agence",$agence);
	}
	if(is_int($type)){
		$req->bindParam(":type",$type);
	}
	$req->execute();
	$intervenants = $req->fetchAll();

    return $intervenants;
}
