<?php

function add_notifications($icone,$texte,$classe,$url,$liste_profil,$liste_droit,$liste_utilisateur,$societe,$agence,$critere_mod, $type= 0){

		Global $Conn;
		Global $acc_utilisateur;

		$critere_mod=intval($critere_mod);

		$sql="SELECT uti_id FROM utilisateurs";
		if(!empty($liste_droit)){
			$sql.=" INNER JOIN utilisateurs_Droits ON (utilisateurs.uti_id=utilisateurs_Droits.udr_utilisateur)";
		}
		if(!empty($societe) OR !empty($agence)){
			$sql.=" INNER JOIN Utilisateurs_Societes ON (utilisateurs.uti_id=Utilisateurs_Societes.uso_utilisateur)";
		}
		$sql.=" WHERE NOT uti_archive";
		if(!empty($societe)){
			$sql.=" AND uso_societe=" . $societe;
			$sql.=" AND uso_agence=" . $agence;
		}

		if(!empty($acc_utilisateur)){
			// inutil de notifier l'utilisateur qui est a l'origine du traitement
			$sql.=" AND NOT uti_id=" . $acc_utilisateur;
		}

		$mil="";
		if(!empty($liste_profil)){
			$mil=" uti_profil IN (" . $liste_profil . ")";
		}
		if(!empty($liste_droit)){
			if(!empty($mil)){
				if($critere_mod==1){
					$mil.=" AND ";
				}else{
					$mil.=" OR ";
				}
			}
			$mil=" udr_droit IN (" . $liste_droit . ")";
		}
		if(!empty($liste_utilisateur)){
			if(!empty($mil)){
				if($critere_mod==1){
					$mil.=" AND ";
				}else{
					$mil.=" OR ";
				}
			}
			$mil=" uti_id IN (" . $liste_utilisateur . ")";
		}
		if(!empty($mil)){
			$sql.=" AND (" . $mil . ")";
		}
		$req=$Conn->query($sql);
		$result=$req->fetchAll();

		if(!empty($result)){

			$sql="INSERT INTO Notifications (not_utilisateur,not_icone,not_texte,not_vu,not_classe,not_url,not_date,not_societe,not_agence, not_type)";
			$sql.=" VALUE (:not_utilisateur, :not_icone, :not_texte, false, :not_classe, :not_url, NOW(), :not_societe, :not_agence,:not_type)";
			$req = $Conn->prepare($sql);

			foreach($result as $r){
				$req ->bindParam(":not_utilisateur",$r["uti_id"]);
				$req ->bindParam(":not_icone",$icone);
				$req ->bindParam(":not_texte",$texte);
				$req ->bindParam(":not_classe",$classe);
				$req ->bindParam(":not_url",$url);
				$req ->bindParam(":not_societe",$societe);
				$req ->bindParam(":not_agence",$agence);
				$req ->bindParam(":not_type",$type);
				$req->execute();
			}
		}
	}


?>
