<?php 

function insert_correspondance($utilisateur, $commentaire, $tel, $contact, $nom, $rappeler_le, $suspect, $sco_date){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("INSERT INTO suspects_correspondances (sco_date, sco_utilisateur, sco_commentaire, sco_contact_tel, sco_contact, sco_contact_nom, sco_rappeler_le, sco_suspect) VALUES (:sco_date, :utilisateur, :commentaire, :tel, :contact, :nom, :rappeler_le, :suspect);");

	$req->bindParam("utilisateur",$utilisateur);
	$req->bindParam("commentaire",$commentaire);
	$req->bindParam("tel",$tel);
	$req->bindParam("contact",$contact);
	$req->bindParam("nom",$nom);
	$req->bindParam("rappeler_le",$rappeler_le);
	$req->bindParam("suspect",$suspect);
	$req->bindParam("sco_date",$sco_date);
	$req->execute();

}

function update_correspondance($id, $utilisateur, $commentaire, $tel, $contact, $nom, $rappeler_le, $suspect, $sco_rappel, $sco_date){
	
	global $ConnSoc;
	$req = $ConnSoc->prepare("UPDATE suspects_correspondances SET sco_utilisateur = :utilisateur, sco_commentaire = :commentaire, sco_contact_tel =:tel, sco_contact = :contact, sco_contact_nom = :nom, sco_rappeler_le = :rappeler_le, sco_suspect =:suspect, sco_rappel=:sco_rappel, sco_date=:sco_date WHERE sco_id = :sco_id;");

	$req->bindParam("sco_id",$id);
	$req->bindParam("utilisateur",$utilisateur);
	$req->bindParam("commentaire",$commentaire);
	$req->bindParam("tel",$tel);
	$req->bindParam("contact",$contact);
	$req->bindParam("nom",$nom);
	$req->bindParam("rappeler_le",$rappeler_le);
	$req->bindParam("suspect",$suspect);
	$req->bindParam("sco_rappel",$sco_rappel);
	$req->bindParam("sco_date",$sco_date);
	$req->execute();

}
function get_correspondance($id){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("SELECT * FROM suspects_correspondances WHERE sco_id = :sco_id;");

	$req->bindParam("sco_id",$id);
	$req->execute();

	$corr = $req->fetch();

	return $corr;

}

function delete_correspondance($id){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("DELETE FROM suspects_correspondances WHERE sco_id = :sco_id");

	$req->bindParam("sco_id",$id);
	$req->execute();

}
?>