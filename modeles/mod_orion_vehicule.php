<?php

// RETOURNE UNE LIGNE DE LA TABLE Produits_Familles qui se trouve dans ORION

// AUTEUR FG

function orion_vehicule($veh_id){

	global $Conn;

	$sql="SELECT veh_libelle FROM Vehicules WHERE veh_id=:veh_id";
	$req=$Conn->prepare($sql);
	$req->bindParam(":veh_id",$veh_id);
	$req->execute();
	$vehicule = $req->fetch();
	if(!empty($vehicule)){
		return $vehicule;
	}

    
}
