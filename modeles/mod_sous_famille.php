<?php
//include_once '/connexion.php';
function get_sous_familles()
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_sous_familles");
    $req->execute();

    $sous_familles = $req->fetchAll();

    return $sous_familles;
}

function insert_sous_famille()
{

    global $Conn;

    // fin upload fichier
    if (isset($_POST['psf_id'])) {
    	$req = $Conn->prepare("REPLACE INTO produits_sous_familles (psf_id,psf_pfa_id,psf_libelle) VALUES (" . $_POST['psf_id'] . "," . $_POST['psf_pfa_id'] . ", '" . $_POST['psf_libelle'] . "')");
    }else{
    	$req = $Conn->prepare("INSERT INTO produits_sous_familles (psf_pfa_id, psf_libelle) VALUES (" . $_POST['psf_pfa_id'] . ",'" . $_POST['psf_libelle'] . "')");
      }  

    $req->execute();

    header('Location: /sous_famille_liste.php');
    

}
function get_sous_famille_select($id, $selected){

  global $Conn;

  $service_select="";
  $req=$Conn->query("SELECT * FROM produits_sous_familles WHERE psf_pfa_id = $id ORDER BY psf_libelle");
  while ($donnees = $req->fetch())
  {
    if($selected == 0){
            $service_select=$service_select . "<option value='" . $donnees["psf_id"] . "' >" . $donnees["psf_libelle"] . "</option>";
        }else{
            if($donnees['psf_id'] == $selected){
               $service_select=$service_select . "<option selected value='" . $donnees["psf_id"] . "' >" . $donnees["psf_libelle"] . "</option>";
            }else{
                $service_select=$service_select . "<option value='" . $donnees["psf_id"] . "' >" . $donnees["psf_libelle"] . "</option>";
            }
           
        }
    
  }
  return $service_select;
  }

  function get_sous_sous_famille_select($id, $selected){

  global $Conn;

  $service_select="";
  $req=$Conn->query("SELECT * FROM produits_sous_sous_familles WHERE pss_psf_id = $id ORDER BY pss_libelle");
  while ($donnees = $req->fetch())
  {
    if($selected == 0){
            $service_select=$service_select . "<option value='" . $donnees["pss_id"] . "' >" . $donnees["pss_libelle"] . "</option>";
        }else{
            if($donnees['pss_id'] == $selected){
               $service_select=$service_select . "<option selected value='" . $donnees["pss_id"] . "' >" . $donnees["pss_libelle"] . "</option>";
            }else{
                $service_select=$service_select . "<option value='" . $donnees["pss_id"] . "' >" . $donnees["pss_libelle"] . "</option>";
            }
           
        }
    
  }
  return $service_select;
  }