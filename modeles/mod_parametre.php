<?php

	// CONTENANT LES VARIABLE SYSTEME QUI NE SONT PAS STOCKE EN DB
/*
	A - table Paramètres
	B - Fonctions DATES
	C - Fonction TIME

*/

// A - table Paramètres

$base_civilite=array(
	0 => "&nbsp;",
	1 => "Monsieur",
	2 => "Madame"
	);
$base_contrat=array(
	0 => "&nbsp;",
	1 => "CDI",
	2 => "CDD"
	);
$base_population=array(
	0 => "&nbsp;",
	1 => "COM",
	2 => "FOR",
	3 => "ADM",
	4 => "DIR",
	5 => "EXT",
	);
$base_type_stagiaire=array(
	0 => "&nbsp;",
	1 => "Salariés bénéficiant d'un financement par l'employeur",
	2 => "Salariés sous contrat de professionnalisation",
	3 => "Demandeurs d'emploi bénéficiant d'un financement public",
	4 => "Particuliers à leurs propres frais",
	5 => "Autres stagiaires",
	);
$base_type_adresse = array(
	0 => "&nbsp;",
	1 => "Adresse d'intervention",
	2 => "Adresse de facturation",
	3 => "Adresse d'envoi de facture"
	);
// TYPES DE FOURNISSEURS
// il faut le droit compta pour avoir accès à interco
if(!empty($_SESSION['acces']['acc_droits'][9])){
    $base_type_fournisseur = array(
        0 => "&nbsp;",
        1 => "Société",
        2 => "Interco",
        3 => "Auto-entrepreneur",
    );
}else{
    $base_type_fournisseur = array(
        0 => "&nbsp;",
        1 => "Société",
        // 2 => "Interco",
        3 => "Auto-entrepreneur",
    );
}
// FIN TYPES DE FOURNISSEURS

$base_reglement=array(
	0 => "&nbsp;",
	1 => "Chèque",
	2 => "Virement",
	3 => "Traite acceptée",
	4 => "Traite non acceptée",
	5 => "Mandat",
	6 => "Espèces",
	7 => "Ajustement",
);

	// B - FONCTION SUR LES DATES
	// transforme la date aaaa-mm-jj en le 19 janvier 2014
function convert_date_fr($date){
	if(!empty($date)){
		$date_part=explode("-",$date);
		$annee=intval($date_part["0"]);
		$mois=intval($date_part["1"]);
		$jour=intval($date_part["2"]);
	}
	$date_sql1=date("w", mktime(0, 0, 0, $mois, $jour, $annee));
	$date_sql2=date("d", mktime(0, 0, 0, $mois, $jour, $annee));
	$date_sql3=date("n", mktime(0, 0, 0, $mois, $jour, $annee));
	$date_sql4=date("Y", mktime(0, 0, 0, $mois, $jour, $annee));
	$jour = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");

	$mois = array("","Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");

	$datefr = $jour[$date_sql1]." ".date($date_sql2)." ".$mois[$date_sql3]." ".$date_sql4;

	return "le ". $datefr;
}

function convert_date_ndf($date){
	if(!empty($date)){
		$date_part=explode("-",$date);
		$annee=intval($date_part["0"]);
		$mois=intval($date_part["1"]);
		$jour=intval($date_part["2"]);
	}
	$date_sql1=date("m", mktime(0, 0, 0, $mois, $jour, $annee));
	$date_sql2=date("d", mktime(0, 0, 0, $mois, $jour, $annee));
	$date_sql4=date("y", mktime(0, 0, 0, $mois, $jour, $annee));

	return $date_sql2 . $date_sql1 . $date_sql4;
}
// Polyfill for array_key_last() available from PHP 7.3
if (!function_exists('array_key_last')) {
  function array_key_last($array) {
    return array_slice(array_keys($array),-1)[0];
  }
}
function convert_date_fr_le($date){
	if(!empty($date)){
		$date_part=explode("-",$date);
		$annee=intval($date_part["0"]);
		$mois=intval($date_part["1"]);
		$jour=intval($date_part["2"]);
	}
	$date_sql1=date("w", mktime(0, 0, 0, $mois, $jour, $annee));
	$date_sql2=date("d", mktime(0, 0, 0, $mois, $jour, $annee));
	$date_sql3=date("n", mktime(0, 0, 0, $mois, $jour, $annee));
	$date_sql4=date("Y", mktime(0, 0, 0, $mois, $jour, $annee));
	$jour = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");

	$mois = array("","Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");

	$datefr = $jour[$date_sql1]." ".date($date_sql2)." ".$mois[$date_sql3]." ".$date_sql4;

	return $datefr;

}
function hoursandmins($time, $format = '%02dh%02d')
{
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

// transforme une date jj/mm/aaaa en format sql aaaa-mm-jj
function convert_date_sql($date){
	if(!empty($date)){
		$date_part=explode("/",$date);
		$jour=intval($date_part["0"]);
		$mois=intval($date_part["1"]);
		$annee=intval($date_part["2"]);

		$date_sql=date("Y-m-d", mktime(0, 0, 0, $mois, $jour, $annee));
	}else{
		$date_sql = "";
	}
	return $date_sql;

}

// transforme une date mm/aaaa en format mm
function get_mois_date($date){
	if(!empty($date)){
		$date_part=explode("/",$date);
		$mois=intval($date_part["0"]);
		$annee=intval($date_part["1"]);

		$date_sql=date("m", mktime(0, 0, 0, $mois, 0, $annee));
	}else{
		$date_sql = "";
	}
	return $date_sql;

}
function replace_extension($filename, $new_extension) {
    $info = pathinfo($filename);
    return $info['filename'] . '.' . $new_extension;
}


// transforme une date mm/aaaa en format aaaa
function get_annee_date($date){
	if(!empty($date)){
		$date_part=explode("/",$date);
		$mois=intval($date_part["0"]);
		$annee=intval($date_part["1"]);

		$date_sql=date("Y", mktime(0, 0, 0, $mois, 0, $annee));
	}else{
		$date_sql = "";
	}
	return $date_sql;

}
	// transforme une aaaa-mm-jj en format sql jj/mm/aaaa

function convert_date_txt($date){

	$date_txt="";
	if(!empty($date)){

		$date_part=explode("-",$date);
		$annee=intval($date_part["0"]);
		$mois=intval($date_part["1"]);
		$jour=intval($date_part["2"]);

		$date_txt=date("d/m/Y", mktime(0, 0, 0, $mois, $jour, $annee));
	};

	return $date_txt;

}
// transforme une aaaa-mm-jj en format sql aaaammjj
function convert_date_datatable($date){

	$date_txt="";
	if(!empty($date)){

		$date_part=explode("-",$date);
		$annee=intval($date_part["0"]);
		$mois=intval($date_part["1"]);
		$jour=intval($date_part["2"]);

		$date_txt=date("Ymd", mktime(0, 0, 0, $mois, $jour, $annee));
	};

	return $date_txt;

}
	// permet d'ajout ou de soustraire des d m ou y a une date
	// ATENTION : fonctionne avec une date au format sql

function ajout_date($date,$elt_ajout,$nb_ajout){

	$date_part=explode("-",$date);
	$annee=intval($date_part["0"]);
	$mois=intval($date_part["1"]);
	$jour=intval($date_part["2"]);

	switch ($elt_ajout) {
		case "d":
		$jour=$jour + $nb_ajout;
		break;
		case "m":
		$mois=$mois + $nb_ajout;
		break;
		case "Y":
		$annee=$annee + $nb_ajout;
		break;
	};


	$date_sql=date("Y-m-d", mktime(0, 0, 0, $mois, $jour, $annee));

	return $date_sql;

}
// C - Fonction TIME
// retourne le nombre de minute entre deux horraires aux format 12:34:00
function nb_minutes($h_deb,$h_fin){
	$tab_fin=explode(":",$h_fin);
	$m_fin=60*intval($tab_fin[0])+intval($tab_fin[1]);
	$tab_deb=explode(":",$h_deb);
	$m_deb=60*intval($tab_deb[0])+intval($tab_deb[1]);
	$nb_minutes=$m_fin-$m_deb;

	return $nb_minutes;
}
// convertie un format 12h30 en 12:00
function time_sql($time_txt){
	$time_sql=null;
	if(!empty($time_txt)){
		switch(strlen($time_txt)){
			case 1:
				$time_sql="0" . $time_txt . ":00:00";
				break;
			case 2:
				$time_sql=$time_txt . ":00:00";
				break;
			default:
				$time_sql=str_replace("h",":",$time_txt);
				$time_sql.=":00";
				break;
		}
	}
	return $time_sql;
}
function time_txt($time_sql){
	$time_txt="";
	if(!empty($time_sql)){
		$time_txt=substr($time_sql,0,5);
		$time_txt=str_replace(":","h",$time_txt);
	}
	return $time_txt;
}


// permet d'enlever les accents d'un string !!! ////

function clean($str) {
	$str = htmlentities($str, ENT_NOQUOTES, 'utf-8');

	$str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

    return $str;
	// return iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $string);

}

// permet de voir si un clé est associée a une valeur d'un tableau

function array_check($array, $key, $val) {
    foreach ($array as $item){
        if (isset($item[$key]) && $item[$key] == $val){
            return true;
        }else{
        	return false;
        }
    }

}

// permet de tronquer un texte // $chars: nombre de caractères avant la tronquature

function truncate($text, $chars) {
    $text = $text." ";
    $text = substr($text,0,$chars);
    $text = substr($text,0,strrpos($text,' '));
    $text = $text."...";
    return $text;
}

// permet de formater un numéro de téléphone, de 0000000000 a 00 00 00 00 00

function tel($str) {
    if(strlen($str) == 10) {
    $res = substr($str, 0, 2) .'&nbsp;';
    $res .= substr($str, 2, 2) .'&nbsp;';
    $res .= substr($str, 4, 2) .'&nbsp;';
    $res .= substr($str, 6, 2) .'&nbsp;';
    $res .= substr($str, 8, 2) .'&nbsp;';
    return $res;
    }
}
// permet de dire "Il y'a ... secondes | minutes | heures ..."
function ago($time){
    $diff_time = time() - $time;
    if($diff_time < 1){
      return '0 secondes';
    }

    $sec = array(   31556926    => 'an',
                    2629743.83  => 'mois',
                    86400       => 'jour',
                    3600        => 'heure',
                    60          => 'minute',
                    1           => 'seconde'
    );

    foreach($sec as $sec => $value){
      $div = $diff_time / $sec;
      if($div >= 1){
        $time_ago = round($div);
        $time_type = $value;
        if($time_ago >= 2 && $time_type != "mois"){
            $time_type .= "s";
        }
        return 'Il y a ' . $time_ago . ' ' . $time_type;
      }
    }
  }

// calculer un pourcentage
function pourcentage($num, $total)
{
	$pourcentage = (100*$num)/$total;

    return $pourcentage;
}

function number_of_working_days($from, $to) {
    $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
    $holidayDays = []; # variable and fixed holidays

    $from = new DateTime($from);
    $to = new DateTime($to);
    $to->modify('+1 day');
    $interval = new DateInterval('P1D');
    $periods = new DatePeriod($from, $interval, $to);

    $days = 0;
    foreach ($periods as $period) {
        if (!in_array($period->format('N'), $workingDays)) continue;
        if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
        if (in_array($period->format('*-m-d'), $holidayDays)) continue;
        $days++;
    }
    return $days;
}
function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

function explode_time($time) { //explode time and convert into seconds
	$time = explode(':', $time);

	$time = $time[0] * 3600 + $time[1] * 60;
	return $time;
}

function second_to_hhmm($time) { //convert seconds to hh:mm
	$hour = floor($time / 3600);
	$minute = strval(floor(($time % 3600) / 60));
	if ($minute == 0) {
		$minute = "00";
	} else {
		$minute = $minute;
	}
	$time = $hour . ":" . $minute;
	return $time;
}
function getStartAndEndDate($week, $year) {
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $dto->modify('-1 day');
    $ret['week_start'] = $dto->format('Y-m-d');
    $dto->modify('+6 days');
    $ret['week_end'] = $dto->format('Y-m-d');
    return $ret;
}

function datediffInWeeks($date1, $date2)
{
    if($date1 > $date2) return datediffInWeeks($date2, $date1);
    $first = DateTime::createFromFormat('m/d/Y', $date1);
    $second = DateTime::createFromFormat('m/d/Y', $date2);
    return floor($first->diff($second)->days/7);
}
function mb_str_pad( $input, $pad_length, $pad_string = ' ', $pad_type = STR_PAD_RIGHT)
{
	$input = trim(preg_replace('/\s+/', ' ', $input));
    $diff = strlen( $input ) - mb_strlen( $input, "UTF-8" );
    return str_pad( $input, $pad_length + $diff, $pad_string, $pad_type );
}

if( !function_exists('ceiling') )
{
    function ceiling($number, $significance = 1)
    {
        return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
    }
}

//$key is our base64 encoded 256bit key that we created earlier. You will probably store and define this key in a config file.


function my_encrypt($data) {
    // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
    return base64_encode($data);
}

function my_decrypt($data) {
    $encryption_key = base64_decode($data);
    // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
    return $encryption_key;
}
?>
