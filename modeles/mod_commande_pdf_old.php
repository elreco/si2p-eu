<?php

// aller chercher la commande
$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = " . $commande['com_id']);
$req->execute();
$c= $req->fetch();
// aller chercher les lignes de commandes
$req = $Conn->prepare("SELECT cli_id,cli_livraison,pro_code_produit,cli_libelle, ffa_libelle, cli_reference, cli_descriptif, cli_ht, cli_qte, cli_tva, veh_libelle, uti_nom, uti_prenom, cli_utilisateur, cli_vehicule FROM commandes_lignes 
	LEFT JOIN produits ON produits.pro_id = commandes_lignes.cli_produit_si2p 
	LEFT JOIN fournisseurs_familles ON fournisseurs_familles.ffa_id = commandes_lignes.cli_famille 
	LEFT JOIN vehicules ON vehicules.veh_id = commandes_lignes.cli_vehicule 
	LEFT JOIN utilisateurs ON utilisateurs.uti_id = commandes_lignes.cli_utilisateur
	WHERE cli_commande = " . $c['com_id'] . " ORDER BY cli_id");
$req->execute();
$lignes = $req->fetchAll();

/* if(!empty($c['com_agence'])){// aller chercher la societe
    $req = $Conn->prepare("SELECT * FROM agences WHERE age_id = " . $c['com_agence']);
    $req->execute();
    $agence= $req->fetch();

} */
/*     $req = $Conn->prepare("SELECT * FROM societes WHERE soc_id = " . $c['com_societe']);
    $req->execute();
    $societe= $req->fetch(); */

// rechercher le fournisseur de la commande
$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id = " . $c['com_fournisseur']);
$req->execute();
$fournisseur = $req->fetch();

// rechercher l'adresse du fournisseur
$req = $Conn->prepare("SELECT * FROM fournisseurs_adresses WHERE fad_fournisseur = " . $c['com_fournisseur']);
$req->execute();
$fad = $req->fetch();

// rechercher le donneur d'ordres
$req = $Conn->prepare("SELECT uti_nom, uti_prenom, uti_societe, uti_agence FROM utilisateurs WHERE uti_id = " . $c['com_donneur_ordre']);
$req->execute();
$ordre = $req->fetch();
foreach($lignes as $li){ // pour toutes les lignes calculer la tva
    $date = $c['com_date'];
    $req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = " . $li['cli_tva'] . " AND tpe_date_deb <= '" . $date . "' AND (tpe_date_fin >= '"  . $date . "' OR tpe_date_fin IS NULL)");
    $req->execute();
    $tva = $req->fetch();

    $req = $Conn->prepare("UPDATE commandes_lignes SET cli_tva_taux = " . $tva['tpe_taux'] . " WHERE cli_commande = " . $li['cli_id']);
    $req->execute();

     
    $tva_nom = $base_tva[$li['cli_tva']];
    $total_ligne[$tva_nom] = $tva['tpe_taux'];
}

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

/*include "includes/controle_acces.inc.php";*/

// VISUALISATION D'UN DEVIS EN PDF

ob_start(); ?>


<style type="text/css" >

    table{
        width:100%;
        color:#333333;
        font-size:11pt;
        font-family:helvetica;
        line-height:6mm;
        border-collapse:collapse;
    }


    .text-right{
        text-align:right;
    }
    .text-center{
        text-align:center;
    }
    
    table.border{
        
    }
    table.border th{
        background-color:#333333;
        color:#FFFFFF;
        text-align:center;
    }

    table.border td{
        border:1px solid #7e8082;
    }

   
    
    h3{
        font-size:12pt;
    }
    .cadre{
        border:1px solid #333333;
        padding:5px;
    }
    
    .juridique{
        font-size:7pt;
        line-height:3mm;
        text-align:center;
    }
    
    h2{
    	color:#333333;
    	font-size: 24px;
    	margin-top: 9.5px;
		margin-bottom: 9.5px;
    }
    .text-primary{
	    color: #E31936;
    }
    h4{
        font-size:12pt;
        margin:0px;
        
    }
    h5{
        font-size:8pt;
        color:#E31936;
        margin:5px 0px 0px 0px;
    }
    h6{
        font-size:8pt;
        margin:5px 0px 0px 0px;
    }
    .cgv-intro{
        color:#E31936;
        font-size:8pt;
        text-align:justify;
        margin:5px 0px 0px 0px;
    }
    .cgv{
        text-align:justify;
        font-size:7pt;
        margin:5px 0px 0px 0px;
    }
    .titre{
    	
    	background-color:#333333;
    	color:white;
    }
    .mt20{
    	
    	margin-top:20px;
    }
    
    
</style>

<page backtop="30mm" backleft="5mm" backright="5mm" backbottom="15mm" >
    <page_header> 
        <table>
            <tr>
                <td style="width:30%;" >
        <?php       if(!empty($agence["age_logo_1"])){
                        echo("<img style='max-width:100%;' src='documents/agences/logos/" . $agence["age_logo_1"] . "' />");
                    }elseif(!empty($societe["soc_logo_1"])){ 
                        echo("<img style='max-width:100%;' src='documents/societes/logos/" . $societe["soc_logo_1"] . "' />");
                    }
                    ?>    
                </td>
                <td style="width:40%;" >
            
                </td>
                <td style="width:30%;text-align:right;" >
        <?php       if(!empty($societe["soc_logo_2"])){
                        echo("<img style='max-width:100%;' src='documents/societes/logos/" . $societe["soc_logo_2"] . "' />");
                    }elseif(!empty($agence["age_logo_1"])){ 
                        echo("<img style='max-width:100%;' src='documents/agences/logos/" . $societe["soc_logo_2"] . "' />");
                    }   ?>    
                </td>
                
                
            </tr>
        </table>  
                
    </page_header> 
    
    <page_footer>
        <hr/>
        <table>
            <tr>
        <?php       if(!empty($societe)){ ?>
                <td style="width:15%;" >
        <?php       if(!empty($societe["soc_qualite_1"])){
                        echo("<img style='max-width:100%;' src='documents/societes/logos/" . $societe["soc_qualite_1"] . "' />");
                    } ?>    
                </td>
				<td style="width:70%;text-align:center;font-size:10px;line-height:12px;" >
					
            		<?= $societe['soc_nom'] ?> - <?= $societe['soc_type'] ?> au capital de <?= $societe['soc_capital'] ?> € - APE <?= $societe['soc_ape'] ?> - RCS <?= $societe['soc_rcs'] ?><br>
            		Siret <?= $societe['soc_siren'] ?> <?= $societe['soc_siret'] ?> - N° TVA <?= $societe['soc_tva'] ?><br>
            		N° déclaration Existence <?= $societe['soc_num_existence'] ?>

                </td>
                <td style="width:15%;text-align:right;" >
        <?php       if(!empty($societe["soc_qualite_2"])){
                        echo("<img style='max-width:100%;' src='documents/societes/logos/" . $societe["soc_qualite_2"] . "' />");
                    } ?>    
                </td>
                
        <?php }elseif(!empty($agence)){ ?>
                <td style="width:15%;" >
        <?php       if(!empty($agence["age_qualite_1"])){
                        echo("<img style='max-width:100%;' src='documents/agences/logos/" . $agence["age_qualite_1"] . "' />");
                    } ?>    
                </td>
                <td style="width:70%;text-align:center;font-size:10px;line-height:12px;" >
                    
                    <?= $agence['age_nom'] ?> - <?= $agence['age_type'] ?> au capital de <?= $agence['age_capital'] ?> € - APE <?= $agence['age_ape'] ?> - RCS <?= $agence['age_rcs'] ?><br>
                    Siret <?= $agence['age_siren'] ?> <?= $agence['age_siret'] ?> - N° TVA <?= $agence['age_tva'] ?><br>
                    N° déclaration Existence <?= $agence['age_num_existence'] ?>

                </td>
                <td style="width:15%;text-align:right;" >
        <?php       if(!empty($agence["age_qualite_2"])){
                        echo("<img style='max-width:100%;' src='documents/agences/logos/" . $agence["age_qualite_2"] . "' />");
                    } ?>    
                </td>
        <?php } ?> 
            </tr>
        </table>            
    </page_footer>
    <h2 style="text-align:center;margin-top:5px;">COMMANDE <b class="text-primary">D'ACHAT</b></h2>  
    <table style="margin-bottom:20px;">
        <tr>
            <td style="width:70%;" >
            	<h3 style="margin-bottom:0px;">Adresse de facturation</h3>
				<div style="margin-bottom:10px;">
					<strong><?= $c['com_fac_nom'] ?></strong><br>
					<?= $c['com_fac_adr1'] ?>
                	<?php if(!empty($c['com_fac_adr2'])){ ?>
                    	<br> <?= $c['com_fac_adr2'] ?>
                    <?php } ?>
                    <?php if(!empty($c['com_fac_adr3'])){ ?>
                    	<br> <?= $c['com_fac_adr3'] ?>
                    <?php } ?>
                    <br><?= $c['com_fac_cp'] ?> <?= $c['com_fac_ville'] ?> 
                </div>
				<h3 style="margin-bottom:0px;">Adresse de livraison</h3>
				<div style="margin-bottom:10px;">
					<strong><?= $c['com_liv_nom'] ?></strong><br>
					<?= $c['com_liv_adr1'] ?>
	            	<?php if(!empty($c['com_liv_adr2'])){ ?>
	                	<br> <?= $c['com_liv_adr2'] ?>
	                <?php } ?>
	                <?php if(!empty($c['com_liv_adr3'])){ ?>
	                	<br> <?= $c['com_liv_adr3'] ?>
	                <?php } ?>
	                <br><?= $c['com_liv_cp'] ?> <?= $c['com_liv_ville'] ?> 
                </div>
            </td>
            <td style="width:30%;">
   			 	<h3 style="margin-bottom:0px;"><?= $fournisseur['fou_nom'] ?></h3>
   			 	<div style="margin-bottom:40px;">
	   			 	<?= $fad['fad_ad1'] ?>
	            	<?php if(!empty($fad['fad_ad2'])){ ?>
	                	<br> <?= $fad['fad_ad2'] ?>
	                <?php } ?>
	                <?php if(!empty($fad['fad_ad3'])){ ?>
	                	<br> <?= $fad['fad_ad3'] ?>
	                <?php } ?>
	                <br><?= $fad['fad_cp'] ?> <?= $fad['fad_ville'] ?>
                </div>
                <h3 style="margin-bottom:0px;">Affaire suivie par : </h3>
                <div style="margin-bottom:10px;">
                	<?= $ordre['uti_prenom'] ?> <?= $ordre['uti_nom'] ?>
                </div>
            </td>
        </tr>
    </table>
    <table class="mt20">
        
            <tr>
                <td style="width:30%;background-color:#333333;color:#FFFFFF;text-align:center;" ><strong>Numéro : </strong><?= $c['com_numero'] ?></td>
                <td style="width:40%;" > </td>
                <td style="width:30%;background-color:#333333;color:#FFFFFF;text-align:center;" ><?= convert_date_fr($c['com_date']) ?></td>    
            </tr>
        
    </table>
    
    <table class="border mt20">
        <thead>
            <tr style="border:1px solid #333333">
                <th style="width:25%;height:40px;" >Référence</th>
                <th style="width:35%;height:40px;" >Désignation</th>
                <th style="width:15%;height:40px;" >Prix HT</th>
                <th style="width:5%;height:40px;" >Qté</th>
                <th style="width:20%;height:40px;">Montant HT</th>
            </tr>
        </thead>    
        <tbody>
    <?php foreach($lignes as $l){ ?>
                <tr>
                    <td style="width:25%;" ><?php if(!empty($l['pro_code_produit'])){ ?><?= $l['pro_code_produit'] ?><?php }else{ ?><?= $l['cli_reference'] ?><?php } ?></td>
                    <td style="width:35%;" >
                    <strong><?= $l['cli_libelle'] ?></strong><br><?= $l['cli_descriptif'] ?>
                    </td>
                    <td style="width:15%;text-align:center;"><?= number_format($l['cli_ht'], 2, ',', ' ') ?> €</td>
                    <td style="width:5%;text-align:center;"><?= number_format($l['cli_qte'], 2, ',', ' ') ?></td>
                    <td style="width:20%;text-align:center;"><?= number_format($l['cli_ht'] * $l['cli_qte'], 2, ',', ' ') ?> €</td>
                    
                        
                </tr>
    	<?php   } ?>
    		

        </tbody>
    </table>
    <table class="border mt20"> 
    	
            <tr>
                <td style="width:40%;border-left:none;border-bottom:none;border-top:none;" >
                        
                </td>
                <td style="width:30%;text-align:center;background-color:#333333;color:#FFFFFF;" >
                    
                        
                        Total HT
                    
                </td>
                <td style="width:30%;text-align:center;" >
                    
                        
                        <?= number_format($c['com_ht'], 2, ',', ' '); ?> €
                    
                </td>
            </tr>
            <?php foreach($total_ligne as $k => $v){ ?>
            <tr>
                <td style="width:40%;border-left:none;border-bottom:none;border-top:none;" >
                    
                </td>
                
                    <td style="width:30%;text-align:center;background-color:#333333;color:#FFFFFF;" >
                         TVA <?= $k ?>
                    </td>
                    <td style="width:30%;text-align:center;" >
                        <?= $v ?> %
                    </td>
                
            </tr>
            <?php } ?>
            <tr>
                <td style="width:40%;border-left:none;border-bottom:none;border-top:none;" >
                    
                </td>
                <td style="width:30%;text-align:center;background-color:#333333;color:#FFFFFF;" >
                        Total TTC
                </td>
                <td style="width:30%;text-align:center;" >
                    
                        
                        <?= number_format($c['com_ttc'], 2, ',', ' '); ?> €
                    
                </td>
            </tr>
    </table>
    
    <div style="position:absolute;bottom:10px;left:0;border:1px solid white;" >
        <table> 
            <tr>
                <td style="width:30%;" >
                    <div class="cadre" >
                        <h4 style="margin:0;font-size:12px;">Condition de règlement</h4>
                        <?php if(!empty($c['com_reg_type'])){ ?>
							
							<?php foreach($base_reglement as $k => $n){ ?>
								<?php if($k > 0){ ?>
									<?php if($c['com_reg_type'] == $k){ ?><?= $n ?><?php } ?>
								<?php } ?>
							<?php } ?>
						<?php } ?> 

						<?php if($c['com_reg_formule'] == 1){ ?>

							Date + <?php if(!empty($c['com_reg_formule'])){ ?><?= $c['com_reg_nb_jour'] ?><?php }else{ ?>0<?php } ?> jours

						<?php } ?>
						<?php if($c['com_reg_formule'] == 2){ ?>
							Date + 45j + fin de mois
						<?php } ?>
						<?php if($c['com_reg_formule'] == 3){ ?>
							Date + fin de mois + 45j
						<?php } ?>
						<?php if($c['com_reg_formule'] == 4){ ?>
							Date + <?php if(!empty($c['com_reg_nb_jour'])){ ?><?= $c['com_reg_nb_jour'] ?><?php }else{ ?>0<?php } ?> jours + fin de mois + <?php if(!empty($c['com_reg_fdm'])){ ?><?= $c['com_reg_fdm'] ?><?php }else{ ?>0<?php } ?> jour(s)
						<?php } ?>
                    </div>
                </td>
                <td style="width:35%;">
                    <div class="cadre" style="height:100px;">
                        <h4 style="margin:0;width:100%;font-size:12px;">Signature et cachet du fournisseur</h4>
                        
                    </div>
                </td>
                <td style="width:35%;" >
                    <div class="cadre" style="height:100px;">
                        <h4 style="margin:0;width:100%;font-size:12px;">Signature et cachet du donneur d'ordre</h4>
                        
                    </div>
                </td>
            </tr>
        </table>
    </div>
    

</page>










<?php

$content=ob_get_clean();

/*echo(dirname(__FILE__));
die();*/


require_once(dirname(__FILE__)."/../vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;

try{
$pdf=new HTML2PDF('P','A4','fr');
$pdf->pdf->SetDisplayMode('fullwidth');
//echo($content);
$pdf->writeHTML($content);
if($pdf){
$pdf->Output(dirname(__FILE__) . '/../documents/commandes/'. $c["com_numero"] . '.pdf', 'F');
/*header("location : devis_voir.php?devis=" . $devis_id);*/
}else{
$pdf->Output($c["com_numero"] . '.pdf');	
}



}catch(HTML2PDF_exception $e){
die($e);
}

?>