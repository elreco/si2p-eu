<?php 

function get_activites_secteurs(){
	
	global $Conn;

	$req = $Conn->query("SELECT * FROM Activites_Secteurs ORDER BY ase_libelle;");
	$secteurs = $req->fetchAll();
	return $secteurs;

}
?>