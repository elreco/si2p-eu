<?php 

/*
 RETOURNE LES CORRESPONDANCES 
 Attention la dernière correspondance doit toujours etre en 1 
 */
function get_suspect_correspondances($suspect){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("SELECT * FROM Suspects_Correspondances WHERE sco_suspect = :suspect ORDER BY sco_date DESC;");
	$req->bindParam("suspect",$suspect);
	$req->execute();
	$suspect_correspondances = $req->fetchAll();
	return $suspect_correspondances;

}
?>