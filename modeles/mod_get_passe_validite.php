<?php

/*--------------------------------------------
	CALCUL LA DATE DE VALIDITE D'UN PASSE Si2P
----------------------------------------------*/

function get_passe_validite(){
	
	$timestamp_validite=mktime(23, 59, 59, date("m")+1, 1,date("Y"));
	
	// on part du premier jour pour aller au premier lundi 
	//puis on ajoute 4 jours pour que users puissent changer passe jusqu'au jeudi
	$info_date=getdate($timestamp_validite);
	switch($info_date["weekday"]){
		case "Sunday":
			$timestamp_validite+=345600;
			break;
		case "Monday":
			$timestamp_validite+=259200;
			break;
		case "Tuesday":
			$timestamp_validite+=777600;
			break;
		case "Wednesday":
			$timestamp_validite+=691200;
			break;
		case "Thursday":
			$timestamp_validite+=604800;
			break;
		case "Friday":
			$timestamp_validite+=518400;
			break;
		case "Saturday":
			$timestamp_validite+=432000;
			break;
	}
	
	$validite=date("Y-m-d H:i:s",$timestamp_validite);
	
	return $validite;
}
?>
	


