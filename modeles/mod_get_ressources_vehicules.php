<?php
// retourne la liste des vehicule dispo sur la periode $date (array)
	function get_ressources_vehicules($dates,$agence,$action){
	
		global $Conn;
		global $ConnSoc;
		global $acc_societe;
		
		// attention vehicules et dates ne sont pas sur la même base
		$veh_pas_dispo="";
		$sql="SELECT DISTINCT pda_vehicule FROM Plannings_Dates";
		$mil="";
		if(!empty($dates)){
			$mil.=" AND (";
			foreach($dates as $d){
				$mil.=" (pda_date='" . $d[0] . "' AND pda_demi=" . $d[1] . ") OR";
			}
			$mil=substr($mil,0,-3);
			$mil.=" )";
		}
		if(!empty($action)){
			$mil.=" AND (NOT pda_type=1 OR (pda_type=1 AND NOT pda_ref_1=" . $action . ") )";
		}
		$mil.=" AND NOT pda_archive";
		if($mil!=""){
			$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		}
		$sql.=";";
		$req=$ConnSoc->query($sql);
		$d_veh_non_dispo=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($d_veh_non_dispo)){
			foreach($d_veh_non_dispo as $vpl){
				$veh_pas_dispo.=$vpl["pda_vehicule"] . ",";
			}
			$veh_pas_dispo=substr($veh_pas_dispo,0,-1);
		}
		
		
		$sql="SELECT veh_id,veh_libelle FROM Vehicules WHERE NOT veh_archive AND veh_societe=". $acc_societe;
		if(!empty($agence)){
			$sql.=" AND (veh_agence=" . $agence . " OR veh_agence=0 OR ISNULL(veh_agence))";
		}
		if(!empty($veh_pas_dispo)){		
			$sql.=" AND NOT veh_id IN (" . $veh_pas_dispo . ")";
		}
		$sql.=" ORDER BY veh_libelle;";
		$req=$Conn->query($sql);
		$d_vehicules=$req->fetchAll(PDO::FETCH_ASSOC);
		return $d_vehicules;
	}
?>
			