<?php
// retourne les actions pouvant-être facturé

function get_facture_actions($client,$commercial,$fac_date,$facture,$fac_cli_suc,$fac_nature){

	// controle du format date
	$DT_periode=date_create_from_format('Y-m-d',$fac_date);
	if(is_bool($DT_periode)){
		$DT_periode=date_create_from_format('d/m/Y',$fac_date);
		if(!is_bool($DT_periode)){
			$fac_date=$DT_periode->format("Y-m-d");
		}
	}
	
	global $ConnSoc;
	global $Conn;
	global $acc_societe;
	global $acc_agence;
	
	$action_client=null;
	$nb_action=-1;
	$d_actions=array();
	
	
	$sql="SELECT acl_id,acl_pro_libelle,acl_ca,acl_pro_reference,acl_facture_ht,acl_pro_inter,acl_for_nb_sta,acl_produit,acl_commentaire 
	,pda_intervenant,pda_date,pda_demi,pda_categorie
	,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville,act_id
	,int_label_1,int_label_2";
	if(!empty($facture)){
		$sql.=",fli_facture";
	}
	$sql.=" FROM Actions_Clients 
	LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
	LEFT JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
	LEFT JOIN Plannings_Dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
	LEFT JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)";
	if(!empty($facture)){
		$sql.=" LEFT JOIN Factures_Lignes ON (Actions_Clients.acl_id=Factures_Lignes.fli_action_client AND Factures_Lignes.fli_facture=" . $facture . ")";
	}
	$sql.=" WHERE NOT acl_archive AND NOT act_archive AND acl_facture=0 AND acl_confirme AND act_date_deb<=NOW()";
	if(!empty($facture)){
		$sql.=" AND ISNULL(fli_facture)";
	}
	if(!empty($fac_cli_suc)){
		$sql.=" AND acl_client=" . $fac_cli_suc;
	}else{
		$sql.=" AND acl_client=" . $client;
	}
	if($fac_nature==1){
		$sql.=" AND (acl_ca-acl_facture_ht)>0";
	}else{
		$sql.=" AND (acl_ca-acl_facture_ht)<0";
	}
	$sql.=" AND acl_commercial=" . $commercial;
	$sql.=" AND act_date_deb<='" . $fac_date . "'";
	$sql.=" ORDER BY act_date_deb,act_id,acl_id,pda_date;";
	$req=$ConnSoc->query($sql);
	$d_results=$req->fetchAll(PDO::FETCH_ASSOC);

	if(!empty($d_results)){
		foreach($d_results as $r){
			
			if($action_client!=$r["acl_id"]){
				
				if($nb_action>-1){
					$d_actions[$nb_action]["intervenants_lib"]=$intervenant;
					$d_actions[$nb_action]["testeurs_lib"]=$testeur;
					$d_actions[$nb_action]["planning"]=planning_periode($d_date);	
				}
				
				// NOUVELLE ACTION
				
				$action_client=$r["acl_id"];
				
				$nb_action++;
				
				$d_int_id=array();
				$d_date=array();				
				$intervenant="";
				
				$d_test_id=array();
				$testeur="";
				
				$ht=$r["acl_ca"]-$r["acl_facture_ht"];	
			
				$qte=0;
				if($r["acl_pro_inter"]==1){
					$qte=$r["acl_for_nb_sta"];	
				}else{
					$qte=1;	
				};
				$pu=0;
				if(!empty($qte)){
					$pu=$ht/$qte;
				}
				
				$lieu=$r["act_adr_nom"] . "<br/>";
				if(!empty($r["act_adr_service"])){
					$lieu.=$r["act_adr_service"] . "<br/>";
				}
				if(!empty($r["act_adr1"])){
					$lieu.=$r["act_adr1"] . "<br/>";
				}
				if(!empty($r["act_adr2"])){
					$lieu.=$r["act_adr2"] . "<br/>";
				}
				if(!empty($r["act_adr3"])){
					$lieu.=$r["act_adr3"] . "<br/>";
				}
				$lieu.=$r["act_adr_cp"] . " " . $r["act_adr_ville"];
				

				$d_actions[$nb_action]=array(
					"action" => $r["act_id"],
					"action_client" => $r["acl_id"],
					"reference" => $r["acl_pro_reference"],
					"libelle" => $r["acl_pro_libelle"],
					"produit" => $r["acl_produit"],
					"pu" => $pu,
					"qte" => $qte,
					"ht" => $ht,
					"stagiaires" => "",
					"lieu" => $lieu,
					"complement" => $r["acl_commentaire"],
					"action_client_soc" => $acc_societe
				);

			}
			
			if($r["pda_categorie"]!=2){
				if(!isset($d_int_id[$r["pda_intervenant"]])){
					$d_int_id[$r["pda_intervenant"]]=1;
					if(!empty($intervenant)){
						$intervenant.=",";
					}
					
					$intervenant.=$r["int_label_2"] . " " . $r["int_label_1"];
				}
			}else{
				if(!isset($d_test_id[$r["pda_intervenant"]])){
					$d_test_id[$r["pda_intervenant"]]=1;
					if(!empty($testeur)){
						$testeur.=",";
					}
					
					$testeur.=$r["int_label_2"] . " " . $r["int_label_1"];
				}
			}
			
			$d_date[]=array(
				"pda_date" => $r["pda_date"],
				"pda_demi" => $r["pda_demi"]
			);
			
		}	
		$d_actions[$nb_action]["intervenants_lib"]=$intervenant;	
		$d_actions[$nb_action]["planning"]=planning_periode($d_date);		
	}
	
	if(!empty($d_actions)){
		
		$sql_sta="SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id IN (:liste_sta) ORDER BY sta_nom,sta_prenom;";
		$req_sta=$Conn->prepare($sql_sta);
					
		foreach($d_actions as $k => $a){
			
			$sql="SELECT DISTINCT ast_stagiaire FROM Actions_Stagiaires WHERE ast_action_client=" . $a["action_client"] . " AND ast_confirme;";
			$req=$ConnSoc->query($sql);
			$src_sta=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($src_sta)){
				$tab_sta=array_column ($src_sta,"ast_stagiaire");
				$liste_sta=implode($tab_sta,",");
				if(!empty($liste_sta)){
					
					$req_sta->bindParam("liste_sta",$liste_sta);
					$req_sta->execute();
					$d_stagiaires=$req_sta->fetchAll(PDO::FETCH_ASSOC);
					if(!empty($d_stagiaires)){
						$sta_texte="<u>Stagiaire(s) :</u><ul>";
						foreach($d_stagiaires as $sta){
							$sta_texte.="<li>" . $sta["sta_nom"] . " " . $sta["sta_prenom"] . "</li>";
						}
						$sta_texte.="</ul>";
						
						$d_actions[$k]["stagiaires"]=$sta_texte;	
					}
					
				}
			}
			
		}
	}
	
	return $d_actions;
	
 
}
?>
