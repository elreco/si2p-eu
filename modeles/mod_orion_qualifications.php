<?php

// RETOURNE LA TABLE Qualifications qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_qualifications(){

	global $Conn;
	
	$result[0]=array(
		"psf_libelle" => "",
		"psf_couleur" => ""
	);
	
	$sql="SELECT qua_id,qua_libelle,qua_duree FROM Qualifications ORDER BY qua_libelle";
	$req=$Conn->prepare($sql);
	$req->execute();
	$qualifications = $req->fetchAll();
	if(!empty($qualifications)){
		foreach($qualifications as $qua){
			$result[$qua["qua_id"]]=array(
				"qua_libelle" => $qua["qua_libelle"],
				"qua_duree" => $qua["qua_duree"]
			);
		}
	}

    return $result;
}
