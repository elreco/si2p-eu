<?php 

// ATTESTATION GROUPE PAR SESSION OU PAR FORMATION

function commande_data($commande){

	global $ConnSoc;
	global $Conn;
	global $acc_societe;
	
	$erreur=false;
	
	$data=array();

	$base_tva=array(
		0 => "&nbsp;",
		1 => "Normale",
		2 => "Réduit alimentaire",
		3 => "Sans TVA",
		4 => "Réduit",
		5 => "Spé"
		);
	
	// aller chercher la commande
	$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = " . $commande);
	$req->execute();
	$data= $req->fetch();
	// aller chercher les lignes de commandes
	$req = $Conn->prepare("SELECT cli_id,cli_livraison,cli_pu,pro_code_produit,cli_libelle, ffa_libelle, cli_reference, cli_descriptif, cli_ht, cli_qte, cli_tva, veh_libelle, uti_nom, uti_prenom, cli_utilisateur, cli_vehicule FROM commandes_lignes 
		LEFT JOIN produits ON produits.pro_id = commandes_lignes.cli_produit_si2p 
		LEFT JOIN fournisseurs_familles ON fournisseurs_familles.ffa_id = commandes_lignes.cli_famille 
		LEFT JOIN vehicules ON vehicules.veh_id = commandes_lignes.cli_vehicule 
		LEFT JOIN utilisateurs ON utilisateurs.uti_id = commandes_lignes.cli_utilisateur
		WHERE cli_commande = " . $data['com_id'] . " ORDER BY cli_id");
	$req->execute();
	$data['lignes'] = $req->fetchAll();

	foreach($data['lignes'] as $li){ // pour toutes les lignes calculer la tva
		$date = $data['com_date'];
		$req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = " . $li['cli_tva'] . " AND tpe_date_deb <= '" . $date . "' AND (tpe_date_fin >= '"  . $date . "' OR tpe_date_fin IS NULL)");
		$req->execute();
		$tva = $req->fetch();
	
		 
		$tva_nom = $base_tva[$li['cli_tva']];
		$data['total_lignes'][$tva_nom] = $tva['tpe_taux'];
	}
	
	$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id = " . $data['com_fournisseur']);
	$req->execute();
	$data['fournisseur'] = $req->fetch();
	// rechercher l'adresse du fournisseur
	$req = $Conn->prepare("SELECT * FROM fournisseurs_adresses WHERE fad_fournisseur = " . $data['com_fournisseur']);
	$req->execute();
	$data['fournisseur_adresse'] = $req->fetch();
	// rechercher le donneur d'ordres
	$req = $Conn->prepare("SELECT uti_nom, uti_prenom, uti_societe, uti_agence FROM utilisateurs WHERE uti_id = " . $data['com_donneur_ordre']);
	$req->execute();
	$data['ordre'] = $req->fetch();

	// rechercher le donneur d'ordres
	$req = $Conn->prepare("SELECT * FROM societes WHERE soc_id = " . $data['com_societe']);
	$req->execute();
	$data['societe'] = $req->fetch();

	$req = $Conn->prepare("SELECT * FROM agences WHERE age_id = " . $data['com_agence']);
	$req->execute();
	$data['agence'] = $req->fetch();
	$data['base_reglement'] =array(
		0 => "&nbsp;",
		1 => "Chèque",
		2 => "Virement",
		3 => "Traite acceptée",
		4 => "Traite non acceptée",
		5 => "Mandat",
		6 => "Espèces",
	);
	if(!empty($erreur)){
		return false;	
	}else{
		return $data;
	}
}?>
