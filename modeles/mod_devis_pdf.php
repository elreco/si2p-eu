<?php

// GENERE DES ATTESTATIONS EN PDF

require_once("../vendor/autoload.php");
//require_once("vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;

function model_devis_pdf($data){

	global $acc_societe;
	global $base_reglement;

	$erreur="";
	ob_start(); ?>

	<style type="text/css" >

<?php	require("../assets/skin/si2p/css/orion_pdf.css"); ?>

		.ligne-col-1{
			width:20%;
			padding:1px 5px;
		}
		.ligne-col-2{
			width:52%;
			padding:1px 5px;
		}
		.ligne-col-3{
			width:11%;
			padding:1px 5px;

		}
		.ligne-col-4{
			width:6%;
			padding:1px 5px;
		}
		.ligne-col-5{
			width:11%;
			padding:1px 5px;
		}
		.designation{
			font-size:10px;
		}
		.cachet{
			padding:5px 5px 20px 5px;
			border:1px solid #333;
		}
		.cachet small{
			font-size:7pt;
		}
	</style>

<?php
	if(!empty($data)){ ?>
		<page backtop="30mm" backleft="0mm" backright="0mm" backbottom="15mm" >
			<page_header>
				<table class="w-100" >
					<tr>
						<td class="w-25" >
				<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
								<img src="../<?=$data["juridique"]["logo_1"]?>" style="max-width:200px;" />
				<?php		}else{
								echo("&nbsp;");
							} ?>
						</td>
						<td class="w-50 text-center" ><h1>DEVIS <?=$data["dev_numero"]?></h1></td>
						<td class="w-25" >
				<?php		if(!empty($data["juridique"]["logo_2"])){ ?>
								<img src="../<?=$data["juridique"]["logo_2"]?>" style="max-width:150px;" />
				<?php		}else{
								echo("&nbsp;");
							} ?>
						</td>
					</tr>
				</table>
			</page_header>
			<page_footer>
				<div class="footer" >
					<table>
						<tr>
							<td class="w-15" >
					<?php		if(!empty($data["juridique"]["qualite_1"])){ ?>
									<img src="../<?=$data["juridique"]["qualite_1"]?>"/>
					<?php		}else{
									echo("&nbsp;");
								} ?>
							</td>
							<td class="text-center w-70" ><?=$data["juridique"]["footer"]?>
							<br>
							<strong style="font-size:10px;">MESURES COVID-19</strong><br>
							<strong style="font-size:10px;">Les mesures sanitaires de toutes nos formations  ont été renforcées dans le respect des mesures barrières en vigueur. Nos formateurs habitués au secours à personne ont reçu une formation spécifique adaptée pour garantir la sécurité de tous nos stagiaires.</strong>
</td>
							<td class="text-right w-15" >
					<?php		if(!empty($data["juridique"]["qualite_2"])){ ?>
									<img src="../<?=$data["juridique"]["qualite_2"]?>" />
					<?php		}else{
									echo("&nbsp;");
								} ?>
							</td>
						</tr>
					</table>
				</div>
			</page_footer>


			<table class="w-100" style="border:1px solid red;" >
				<tr>
					<td class="w-60 va-t" >
			<?php		echo($data["juridique"]["nom"]);
						if(!empty($data["juridique"]["agence"])){
							echo("<br/>Agence " . $data["juridique"]["agence"]);
						}
						if(!empty($data["juridique"]["ad1"])){
							echo("<br/>" . $data["juridique"]["ad1"]);
						}
						if(!empty($data["juridique"]["ad2"])){
							echo("<br/>" . $data["juridique"]["ad2"]);
						}
						if(!empty($data["juridique"]["ad3"])){
							echo("<br/>" . $data["juridique"]["ad3"]);
						}
						echo("<br/>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"]);
						echo("<br/>" . $data["juridique"]["mail"]);
						echo("<br/>" . $data["juridique"]["site"]);
						echo("<br/>Affaire suivi par : " . $data["dev_com_identite"]);
						if(!empty($data["dev_uti_mobile"])){
							echo("<br/>Tél. : " . $data["dev_uti_mobile"]);
						}
						if(!empty($data["dev_uti_mail"])){
							echo("<br/>Mail : " . $data["dev_uti_mail"]);
						}
						if(!empty($data["dev_reference"])){
							echo("<br/>Référence : " . $data["dev_reference"]);
						}
						if(!empty($data["dev_cli_code"])){  ?>
							<br/>Code : <strong><?=$data["dev_cli_code"]?></strong>
				<?php 	}
						if(!empty($data["dev_cli_reference"])){  ?>
							<br/>Référence interne : <?=$d_client["dev_cli_reference"]?>
				<?php 	} ?>
					</td>
					<td class="w-40" >
			<?php		echo("<b>" . $data["dev_adr_nom"] . "</b>");
						if(!empty($data["dev_con_identite"])){
							echo("<br/>A l'attention de " . $data["dev_con_identite"]);
						}
						if(!empty($data["dev_adr_service"])){
							echo("<br/>" . $data["dev_adr_service"]);
						}
						if(!empty($data["dev_adr1"])){
							echo("<br/>" . $data["dev_adr1"]);
						}
						if(!empty($data["dev_adr2"])){
							echo("<br/>" . $data["dev_adr2"]);
						}
						if(!empty($data["dev_adr3"])){
							echo("<br/>" . $data["dev_adr3"]);
						}
						echo("<br/>" . $data["dev_adr_cp"] . " " . $data["dev_adr_ville"]); ?>
					</td>
				</tr>
			</table>

			<table  class="w-100" >
				<tr>
					<td class="w-100 text-right" >
						Le <?=$data["dev_date_texte"]?>
					</td>
				</tr>
			</table>


			<table class="tab-border w-100" >
				<thead>
					<tr>
						<th class="ligne-col-1 text-center" >Référence</th>
						<th class="ligne-col-2 text-center" >Désignation</th>
						<th class="ligne-col-3 text-center" >Prix HT</th>
						<th class="ligne-col-4 text-center" >Qté</th>
						<th class="ligne-col-5 text-center" >Montant HT</th>
					</tr>
				</thead>
		<?php	if(!empty($data["lignes"])){ ?>
					<tbody>
		<?php			foreach($data["lignes"] as $l){


							if($l["dli_revente"]==1){
								$revente=1;
							}

							$designation="";
							if(!empty($l["dli_libelle"])){
								$designation=$l["dli_libelle"];
							}
							if(!empty($l["dli_texte"])){
								if(!empty($designation)){
									$designation.="<br/>";
								}
								$designation.=$l["dli_texte"];
							}
							if(!empty($l["dli_dates"])){
								if(!empty($designation)){
									$designation.="<br/>";
								}
								$designation.=$l["dli_dates"];
							}

							$stagiaires_txt=$l["dli_stagiaires_txt"];


							if($l["dli_exo"]){
								if(!empty($designation)){
									$designation.="<br/>";
								}
								$designation.="<b class='text-danger' >Exonération de TVA article 261-4-4°a du CGI</b>";
							}

							$montant_tva=($l["dli_montant_ht"]*$l["dli_tva_taux"]/100);

							if(!isset($data["tva"][$l["dli_tva_taux"]])){
								$data["tva"][$l["dli_tva_taux"]]=0;
							}
							$data["tva"][$l["dli_tva_taux"]]+=$montant_tva;
							?>

							<tr>
								<td class="ligne-col-1" >
									<?=$l["dli_reference"]?>
								</td>
								<td class="ligne-col-2 designation" >
									<div><?=$designation?></div>
									<div><?=$stagiaires_txt?></div>
								</td>
								<td class="text-right ligne-col-3" ><?=$l["dli_ht"]?> €</td>
								<td class="text-right ligne-col-4 " ><?=$l["dli_qte"]?></td>
								<td class="text-right ligne-col-5" ><?=$l["dli_montant_ht"]?> €</td>
							</tr>
				<?php	} ?>
					</tbody>
		<?php	} ?>
			</table>
			<end_last_page end_height="60mm">
		<?php	if(!empty($data["dev_libelle"])){ ?>
					<p><?=$data["dev_libelle"]?></p>
		<?php	}?>
				<table class="w-100" >
					<tr>
						<td class="w-33" >
			<?php			if(!empty($data["dev_reg_formule"])){
								echo("Mode de paiement : " . $base_reglement[$data["dev_reg_type"]] . "<br/>");
							}
							echo("Délais de paiement :	" . $data["reg_delai"]); ?>
						</td>
						<td class="w-33 va-t" >
			<?php			if(empty($data["dev_bordereau"])){
								if($data["dev_adr_geo"]!=1){
									echo("<b class='text-danger' >Exonération de TVA article 262 1° du CGI</b>");
								} ?>
								<div class="cachet" >
									<b>Cachet, Nom et Signature du Client</b>
									<small>
										Signature du devis après acceptation des conditions générales et particulières CONSULTABLES SUR NOTRE SITE INTERNET
										ET SUR NOTRE CATALOGUE, et ce compris les dispositions à la clause de réserve de propriété
										et prise de connaissance des prestations annexées au présent contrat.
									</small>
									<p class="text-center" >
										Bon pour accord
									</p>
								</div>
			<?php			}else{
								echo("&nbsp;");
							} ?>
						</td>
						<td class="w-33 va-t" >
			<?php			if(empty($data["dev_bordereau"])){ ?>
								<table class="tab-border w-100" >
									<tr>
										<th class="text-right w-40" >Total HT :</th>
										<td class="text-right w-60" ><?=number_format($data["dev_total_ht"],2,","," ")?> €</td>
									</tr>
							<?php	if(!empty($data["tva"])){
										foreach($data["tva"] as $tk => $tv){
											if(!empty($tv)){?>
												<tr>
													<th class="text-right w-40" >Tva <?=$tk?>% :</th>
													<td class="text-right w-60" ><?=number_format($tv,2,","," ")?> €</td>
												</tr>
							<?php			}
										}
									} ?>
									<tr>
										<th class="text-right w-40" >Total TTC :</th>
										<td class="text-right w-60" ><?=number_format($data["dev_total_ttc"],2,","," ")?> €</td>
									</tr>
								</table>
			<?php			}else{
								echo("&nbsp;");
							} ?>
						</td>
					</tr>
				</table>
			</end_last_page>
		</page>
<?php
	}
	$content=ob_get_clean();

	$resultat=true;
	try{
		$pdf=new HTML2PDF('P','A4','fr');
		$pdf->pdf->SetDisplayMode('fullwidth');
		$pdf->writeHTML($content);
		if($pdf){
			$pdf->Output($_SERVER["DOCUMENT_ROOT"] . '/Documents/Societes/' . $acc_societe . '/devis/' . $data["dev_numero"] . '.pdf', 'F');
		}
	}catch(HTML2PDF_exception $e){
		$resultat=false;
	}

	return $resultat;

}
?>