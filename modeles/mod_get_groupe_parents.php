<?php

// PERMET DE RECUPERER LES PARENTS D'UN CLIENTS DANS UN GROUPE
// ORGANISEE EN NIVEAU POUR L'AFFICHAGE
function get_groupe_parents($client){
	
	global $Conn;
	
	$grp_parents=array();
			
	$cle_groupe=0;
	$grp_parents[]=intval($client);
	
	$securite=0;
	
	While(is_int($grp_parents[$cle_groupe]) AND $securite<10){
		
		$sql="SELECT cli_groupe,cli_id,cli_filiale_de,cli_fil_de FROM Clients WHERE cli_id=" .$grp_parents[$cle_groupe] . ";";
		$req=$Conn->query($sql);
		$d_parent=$req->fetch();
		if(!empty($d_parent)){
			if(!empty($d_parent["cli_groupe"])){
				if(!empty($d_parent["cli_fil_de"])){
					$cle_groupe++;
					$grp_parents[$cle_groupe]=intval($d_parent["cli_fil_de"]);
				}elseif(!empty($d_parent["cli_filiale_de"])){
					$cle_groupe++;
					$grp_parents[$cle_groupe]=intval($d_parent["cli_filiale_de"]);
				}else{
					$cle_groupe++;
					$grp_parents[$cle_groupe]=false;
				}
			}else{
				$cle_groupe++;
				$grp_parents[$cle_groupe]=false;
			}
		}else{
			$cle_groupe++;
			$grp_parents[$cle_groupe]=false;
		}
		
		$securite++;
	}
	
	if($cle_groupe>0){
		unset($grp_parents[$cle_groupe]);
	}
	return $grp_parents;
}
