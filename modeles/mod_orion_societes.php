<?php

// RETOURNE LA TABLE Clients_Sous_Categories qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_societes(){

	global $Conn;
	
	$result[0]=array(
		"soc_nom" => "",
	);
	
	$sql="SELECT soc_id,soc_nom FROM Societes ORDER BY soc_id";
	$req=$Conn->prepare($sql);
	$req->execute();
	$societes = $req->fetchAll();
	if(!empty($societes)){
		foreach($societes as $soc){
			$result[$soc["soc_id"]]=array(
				"soc_nom" => $soc["soc_nom"],
			);
		}
	}

    return $result;
}
