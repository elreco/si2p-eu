 <?php
 
	// PERMET DE RECUPERER LES INFOS LIE A L'INSCRIPTION D'UN STAGIAIRE
 
	function planning_periode($dates){
		
		$periode="";
		/*echo("<pre>");
			print_r($dates);
		echo("</pre>");*/
		
		if(!empty($dates)){
		
			foreach($dates as $k => $d){
				
				if($k==0){
					$date_deb=$d["pda_date"];
					$demi_deb=$d["pda_demi"];
					$date_fin=$d["pda_date"];
					$demi_fin=$d["pda_demi"];

				}else{
					
					if($d["pda_demi"]==2){
						
						if($demi_fin!=1 OR ($d["pda_date"]!=$date_fin)){
							if(!empty($periode)){
								$periode.="<br/>";
							}
							$periode.=planning_periode_lib($date_deb,$demi_deb,$date_fin,$demi_fin);
							$date_deb=$d["pda_date"];
							$demi_deb=$d["pda_demi"];
							$date_fin=$d["pda_date"];
							$demi_fin=$d["pda_demi"];
						}else{
							$date_fin=$d["pda_date"];
							$demi_fin=$d["pda_demi"];
						}
					}else{
						
						// c'est un matin

						$DT_periode=date_create_from_format('Y-m-d',$d["pda_date"]);						
						$j=$DT_periode->format("N");
						if($j==1){
							// C'est un lundu matin
							$DT_periode->sub(new DateInterval('P3D'));							
						}else{
							$DT_periode->sub(new DateInterval('P1D'));			
						}
						$d_compare=$DT_periode->format("Y-m-d");
						
						if($demi_fin!=2 OR $date_fin!=$d_compare){
							if(!empty($periode)){
								$periode.="<br/>";
							}
							$periode.=planning_periode_lib($date_deb,$demi_deb,$date_fin,$demi_fin);
							$date_deb=$d["pda_date"];
							$demi_deb=$d["pda_demi"];
							$date_fin=$d["pda_date"];
							$demi_fin=$d["pda_demi"];					
						}else{
							$date_fin=$d["pda_date"];
							$demi_fin=$d["pda_demi"];
						}
					}
				}
			}
			$periode.=planning_periode_lib($date_deb,$demi_deb,$date_fin,$demi_fin);
		}
		return $periode;	
	}
?>