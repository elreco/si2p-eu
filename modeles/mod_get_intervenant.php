<?php

// RETOURNE LES DONNEES D'INTERVENANT

// AUTEUR FG

function get_intervenant($intervenant){

	global $ConnSoc;
	
	$sql="SELECT * FROM Intervenants WHERE int_id=:intervenant;";
	$req = $ConnSoc->prepare($sql);
	$req->bindParam(":intervenant",$intervenant);
	$req->execute();
	$intervenant = $req->fetch();

    return $intervenant;
}
