<?php

// MISE A JOUR D'UN PRODUIT DANS LA TABLE ClIENTS PRODUITS
// Pour l'ajour voir mod_add_client_produit
 
function add_client_produit($produit,$client,$donnee){
	 
	global $Conn;
	
	if(empty($produit) OR empty($client) OR empty($donnee)){
		return false;
	}
	
	
	$sql="INSERT INTO Clients_Produits (cpr_client,cpr_produit";
	$valeur=":cpr_client,:cpr_produit";
	foreach($donnee as $c => $u){
		$sql.="," . $c;
		$valeur.=",:" . $c;
	}
	$sql.= ") VALUES (" . $valeur . ");";
	$req=$Conn->prepare($sql);	
	$req->bindParam(":cpr_client",$client);	
	$req->bindParam(":cpr_produit",$produit);	
	foreach($donnee as $c => $u){
		$req->bindValue($c,$u);	
	}
	try{
		$req->execute();	
	}catch(Exception $e){
		echo($e->getMessage());
		die();
	}
	
	return true;
	
	die();

}
?>