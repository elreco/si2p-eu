<?php

/*--------------------------------------------------
	RETOUR TOUTES LES INFOS D'UN CLIENT 
----------------------------------------------*/


	function get_client($client)
	{	
		global $Conn;
		
		$req = $Conn->prepare("SELECT * FROM clients WHERE cli_id=" . $client . ";");
		$req->execute();
		$client = $req->fetch();
		return $client;
	}