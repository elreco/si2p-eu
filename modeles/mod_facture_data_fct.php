<?php


setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


function facture_data_fct($facture_id){

	$data=array();

	global $acc_societe;
	global $ConnFct;
	global $Conn;

	// SUR LA FORMATION

	$sql="SELECT * FROM Factures WHERE fac_id=" . $facture_id . ";";
	$req=$ConnFct->query($sql);
	$data=$req->fetch();
	if(empty($data)){
		$erreur_txt="Impossible de charger les données liées à la facture!";
	}



	// CONSTRUCTION DU FOOTER
	/* ON UTILISE PAS MOD_GET_JURIDIQUE CAR EN DEHORS DU FOOTER IL FAUT UTILISER LES DONNEES FIGEES SUR LA FACTURES*/

	if(empty($erreur_txt)){

		$juridique=array();
		$age_structure=0;

		if($data["fac_agence"]>0){
			$sql="SELECT age_siret,age_siren,age_tva,age_ape,age_rcs,age_capital,age_type,age_structure FROM Agences WHERE age_id=" . $data["fac_agence"];
			$req=$Conn->query($sql);
			$d_agence=$req->fetch();
			if(!empty($d_agence)){
				$age_structure=$d_agence["age_structure"];
				if($age_structure==1){
					$juridique["siren"]=$d_agence["age_siren"];
					$juridique["tva"]=$d_agence["age_tva"];
					$juridique["ape"]=$d_agence["age_ape"];
					$juridique["rcs"]=$d_agence["age_rcs"];
					$juridique["capital"]=$d_agence["age_capital"];
					$juridique["type"]=$d_agence["age_type"];
				}else{

				}
				$juridique["siret"]=$d_agence["age_siret"];
			}else{
				$erreur_txt="Impossible de charger les données sur l'agence";
			}
		}
	}
	if(empty($erreur_txt)){



		$sql="SELECT soc_type,soc_capital,soc_rcs,soc_ape,soc_siret,soc_siren,soc_tva,soc_num_existence,soc_region_nom,soc_cnaps
		FROM Societes WHERE soc_id=" . $acc_societe;
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();
		if(!empty($d_societe)){
			if($data["fac_agence"]==0){
				$juridique["siret"]=$d_societe["soc_siret"];
			}
			if($age_structure==0){
				$juridique["siren"]=$d_societe["soc_siren"];
				$juridique["tva"]=$d_societe["soc_tva"];
				$juridique["ape"]=$d_societe["soc_ape"];
				$juridique["rcs"]=$d_societe["soc_rcs"];
				$juridique["capital"]=$d_societe["soc_capital"];
				$juridique["type"]=$d_societe["soc_type"];

			}
			$juridique["num_existence"]=$d_societe["soc_num_existence"];
			$juridique["region_nom"]=$d_societe["soc_region_nom"];
			$juridique["cnaps"]=$d_societe["soc_cnaps"];
		}else{
			$erreur_txt="Impossible de charger les données sur l'agence";
		}

		$footer=$data["fac_soc_nom"];
		if(!empty($data["fac_soc_agence"])){
			$footer.=" - Agence " . $data["fac_soc_agence"];
		}
		$footer.=" - " . $juridique["type"] . " au capital de " . $juridique["capital"] . " € - R.C.S. " . $juridique["rcs"] . " - APE " . $juridique["ape"];
		$footer.="<br/>Siret " . $juridique["siret"] . " - n° TVA " . $juridique["tva"];
		$footer.="<br/>Déclaration d'activité enregistrée sous le numéro " . $juridique["num_existence"] . " auprès du préfet de région de " . $juridique["region_nom"];
		if(!empty($juridique["cnaps"])){
			$footer.="<br/>N° CNAPS : " . $juridique["cnaps"] . ". L’autorisation d’exercice ne confère aucune prérogative de puissance publique à l’entreprise ou aux personnes qui en bénéficient.";
		}
		$data["fac_footer"]=$footer;

		// LIGNES DE FACTURES

		$sql="SELECT * FROM Factures_Lignes WHERE fli_facture=" . $facture_id . ";";
		$req=$ConnFct->query($sql);
		$lignes=$req->fetchAll();
		$data["lignes"]=$lignes;

	}

	if(empty($erreur_txt)){
		return $data;
	}else{
		return $erreur_txt;
	}
}
?>
