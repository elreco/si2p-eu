<?php
// retourne les actions pouvant-être facturé

function get_facture_actions($client,$commercial,$fac_date,$facture,$fac_cli_suc,$fac_nature,$client_categorie,$filiale_liste){
	
	/*echo("<br/>client : ");
	var_dump($client);
	
	echo("<br/>commercial : ");
	var_dump($commercial);
	
	echo("<br/>fac_date : ");
	var_dump($fac_date);
	
	echo("<br/>facture : ");
	var_dump($facture);
	
	echo("<br/>fac_cli_suc : ");
	var_dump($fac_cli_suc);
	
	echo("<br/>fac_cli_suc : ");
	var_dump($fac_cli_suc);
	
	echo("<br/>fac_nature : ");
	var_dump($fac_nature);
	
	echo("<br/>client_categorie : ");
	var_dump($client_categorie);
	
	echo("<br/>filiale_liste : ");
	var_dump($filiale_liste);
	die();*/

	// controle du format date
	$DT_periode=date_create_from_format('Y-m-d',$fac_date);
	if(is_bool($DT_periode)){
		$DT_periode=date_create_from_format('d/m/Y',$fac_date);
		if(!is_bool($DT_periode)){
			$fac_date=$DT_periode->format("Y-m-d");
		}
	}
	
	global $ConnSoc;
	global $Conn;
	global $acc_societe;
	global $acc_agence;
	
	$action_client=null;
	$nb_action=-1;
	$d_actions=array();
	
	
	$sql="SELECT acl_id,acl_pro_libelle,acl_ca,acl_pro_reference,acl_facture_ht,acl_pro_inter,acl_for_nb_sta,acl_produit,acl_commentaire,acl_opca_num,acl_bc_num
	,pda_intervenant,pda_date,pda_demi,pda_categorie
	,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville,act_id,act_date_deb
	,int_label_1,int_label_2";
	if(!empty($facture)){
		$sql.=",fli_facture";
	}
	$sql.=" FROM Actions_Clients 
	LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
	LEFT JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
	LEFT JOIN Plannings_Dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
	LEFT JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)";
	if(!empty($facture)){
		$sql.=" LEFT JOIN Factures_Lignes ON (Actions_Clients.acl_id=Factures_Lignes.fli_action_client AND Factures_Lignes.fli_facture=" . $facture . ")";
	}
	$sql.=" WHERE NOT acl_archive AND NOT act_archive AND acl_facture=0 AND acl_confirme AND NOT acl_ca_nc 
	AND (
		act_date_deb<=NOW() 
		OR acl_produit IN (1429,1582,1331,1472,1480,1462,1363,1451,1454,1280,1281,1282,1351,1330,1467,1337,1475,1387,1301,1374,1285,1284,1342,1345,1379,1356,1315,1283,1425,1310,1384,1649,1650,1651,1652,1653,1654) 
		OR (acl_reporte AND (MONTH(act_date_deb)=11 OR MONTH(act_date_deb)=12) AND YEAR(act_date_deb)=2020) 
	)";
	if(!empty($facture)){
		$sql.=" AND ISNULL(fli_facture)";
	}
	if(!empty($fac_cli_suc)){
		$sql.=" AND acl_client=" . $fac_cli_suc;
	}elseif($filiale_liste){
		$sql.=" AND acl_client IN (" . $filiale_liste . ")";
	}else{
		$sql.=" AND acl_client=" . $client;
	}
	if($fac_nature==1){
		$sql.=" AND (acl_ca-acl_facture_ht)>0";
	}else{
		$sql.=" AND (acl_ca-acl_facture_ht)<0";
	}
	$sql.=" AND acl_commercial=" . $commercial;
	$sql.=" AND (
		act_date_deb<='" . $fac_date . "' 
		OR acl_produit IN (1429,1582,1331,1472,1480,1462,1363,1451,1454,1280,1281,1282,1351,1330,1467,1337,1475,1387,1301,1374,1285,1284,1342,1345,1379,1356,1315,1283,1425,1310,1384,1649,1650,1651,1652,1653,1654)
		OR (acl_reporte AND (MONTH(act_date_deb)=11 OR MONTH(act_date_deb)=12) AND YEAR(act_date_deb)=2020) 
	)";
	$sql.=" ORDER BY act_date_deb,act_id,acl_id,pda_date;";
	$req=$ConnSoc->query($sql);
	$d_results=$req->fetchAll(PDO::FETCH_ASSOC);

	if(!empty($d_results)){
		foreach($d_results as $r){
			
			if($action_client!=$r["acl_id"]){
				
				if($nb_action>-1){
					
					$d_actions[$nb_action]["intervenants_lib"]=$intervenant;
					$d_actions[$nb_action]["testeurs_lib"]=$testeur;
					if(!empty($d_date[0]["pda_date"])){
						$d_actions[$nb_action]["planning"]=planning_periode($d_date);
					}else{
						$d_actions[$nb_action]["planning"]="";
					}
				}
				
				// NOUVELLE ACTION
				
				$action_client=$r["acl_id"];
				
				$nb_action++;
				
				$d_int_id=array();
				$d_date=array();				
				$intervenant="";
				
				$d_test_id=array();
				$testeur="";
				
				$ht=$r["acl_ca"]-$r["acl_facture_ht"];	
			
				$qte=0;
				if($r["acl_pro_inter"]==1){
					$qte=$r["acl_for_nb_sta"];	
				}else{
					$qte=1;	
				};
				$pu=0;
				if(!empty($qte)){
					$pu=$ht/$qte;
				}
				
				$lieu=$r["act_adr_nom"] . "<br/>";
				if(!empty($r["act_adr_service"])){
					$lieu.=$r["act_adr_service"] . "<br/>";
				}
				if(!empty($r["act_adr1"])){
					$lieu.=$r["act_adr1"] . "<br/>";
				}
				if(!empty($r["act_adr2"])){
					$lieu.=$r["act_adr2"] . "<br/>";
				}
				if(!empty($r["act_adr3"])){
					$lieu.=$r["act_adr3"] . "<br/>";
				}
				$lieu.=$r["act_adr_cp"] . " " . $r["act_adr_ville"];
				
				$acl_commentaire="";
				if(!empty($r["acl_commentaire"])){
					// le champ n'est pas un summernot donc il ne doit contenir aucune balise en dehors des <br/>
					$acl_commentaire=str_replace('"',"''",$r["acl_commentaire"]);
					// normalement acl_commentaire ne doit pas contenir <> pour les actions crééer après le 26/11 
					// cette securité permet de ne pas générer de bugs pour les actions créer avant le 26/11
					$acl_commentaire=str_replace('<br>',chr(10),$acl_commentaire);
					$acl_commentaire=str_replace('<br/>',chr(10),$acl_commentaire);
					$acl_commentaire=str_replace('<','',$acl_commentaire);
					$acl_commentaire=str_replace('>','',$acl_commentaire);
				}
				

				$d_actions[$nb_action]=array(
					"date_deb" => $r["act_date_deb"],
					"action" => $r["act_id"],
					"action_client" => $r["acl_id"],
					"reference" => $r["acl_pro_reference"],
					"libelle" => $r["acl_pro_libelle"],
					"produit" => $r["acl_produit"],
					"pu" => $pu,
					"qte" => $qte,
					"ht" => $ht,
					"stagiaires" => "",
					"lieu" => $lieu,
					"complement" => $acl_commentaire,
					"action_client_soc" => $acc_societe,
					"opca_num" => $r["acl_opca_num"],
					"bc_num" => $r["acl_bc_num"],
					"inter" => $r["acl_pro_inter"]	
					
				);

			}
			
			if($r["pda_categorie"]!=2){
				if(!isset($d_int_id[$r["pda_intervenant"]])){
					$d_int_id[$r["pda_intervenant"]]=1;
					if(!empty($intervenant)){
						$intervenant.=",";
					}
					
					$intervenant.=$r["int_label_2"] . " " . $r["int_label_1"];
				}
			}else{
				if(!isset($d_test_id[$r["pda_intervenant"]])){
					$d_test_id[$r["pda_intervenant"]]=1;
					if(!empty($testeur)){
						$testeur.=",";
					}
					
					$testeur.=$r["int_label_2"] . " " . $r["int_label_1"];
				}
			}
			
			$d_date[]=array(
				"pda_date" => $r["pda_date"],
				"pda_demi" => $r["pda_demi"]
			);
			
		}	
		
		$d_actions[$nb_action]["intervenants_lib"]=$intervenant;	
		if(!empty($d_date[0]["pda_date"])){
			$d_actions[$nb_action]["planning"]=planning_periode($d_date);
		}else{
			$d_actions[$nb_action]["planning"]="";
		}
				
	}
	
	
	if($acc_societe==4 AND $client_categorie==2 AND $_SESSION["acces"]["acc_droits"][8]){
		
	
		
		$sql_act="SELECT fli_id FROM Factures_Lignes,Factures WHERE fli_facture=fac_id AND fli_action_client=:action_client AND fli_action_cli_soc=:action_societe AND NOT fac_blocage;";
		$req_act=$ConnSoc->prepare($sql_act);
		
		// GC SUR GFC -> ON VA CHERCHE LES ACTIONS DES REGIONS
		
		$sql_soc="SELECT soc_id FROM Societes WHERE NOT soc_archive AND NOT soc_id=4";
		$req_soc=$Conn->query($sql_soc);
		$d_societes=$req_soc->fetchAll();
		if(!empty($d_societes)){
			
			foreach($d_societes as $soc){
				
				$sql="SELECT acl_id,acl_pro_libelle,acl_ca_gfc,acl_pro_reference,acl_facture_gfc_ht,acl_pro_inter,acl_for_nb_sta,acl_produit,acl_commentaire
				,acl_opca_num,acl_bc_num
				,pda_intervenant,pda_date,pda_demi,pda_categorie
				,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville,act_id,act_date_deb 
				,int_label_1,int_label_2";
				$sql.=" FROM Actions_Clients 
				LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
				LEFT JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
				LEFT JOIN Plannings_Dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
				LEFT JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)";				
				$sql.=" WHERE NOT acl_archive AND NOT act_archive AND acl_facture_gfc=0 AND acl_confirme AND NOT acl_ca_nc 
				AND (
					act_date_deb<=NOW() 
					OR acl_produit IN (1429,1582,1331,1472,1480,1462,1363,1451,1454,1280,1281,1282,1351,1330,1467,1337,1475,1387,1301,1374,1285,1284,1342,1345,1379,1356,1315,1283,1425,1310,1384,1649,1650,1651,1652,1653,1654) 
					OR (acl_reporte AND (MONTH(act_date_deb)=11 OR MONTH(act_date_deb)=12) AND YEAR(act_date_deb)=2020) 
				)";
				if(!empty($filiale_liste)){				
					$sql.=" AND acl_client IN (" . $filiale_liste . ")";
				}else{
					$sql.=" AND acl_client=" . $client;
				}
				$sql.=" AND (acl_ca_gfc-acl_facture_gfc_ht)>0 AND acl_facturation=1 AND NOT acl_non_fac_gfc";
				$sql.=" AND (
					act_date_deb<='" . $fac_date . "' 
					OR acl_produit IN (1429,1582,1331,1472,1480,1462,1363,1451,1454,1280,1281,1282,1351,1330,1467,1337,1475,1387,1301,1374,1285,1284,1342,1345,1379,1356,1315,1283,1425,1310,1384,1649,1650,1651,1652,1653,1654) 
					OR (acl_reporte AND (MONTH(act_date_deb)=11 OR MONTH(act_date_deb)=12) AND YEAR(act_date_deb)=2020)
				)";
				$sql.=" ORDER BY act_date_deb,act_id,acl_id,pda_date;";
				
				$ConnFct=connexion_fct($soc["soc_id"]);
				
				$action_client=null;
				$premier_tour=true;
				$act_ok=true;
				
				$req=$ConnFct->query($sql);
				$d_results=$req->fetchAll(PDO::FETCH_ASSOC);
			
				if(!empty($d_results)){
					foreach($d_results as $r){
						
						if($action_client!=$r["acl_id"]){
							
							if(!$premier_tour){
								
								$d_actions[$nb_action]["intervenants_lib"]=$intervenant;
								$d_actions[$nb_action]["testeurs_lib"]=$testeur;
								if(!empty($d_date[0]["pda_date"])){
									$d_actions[$nb_action]["planning"]=planning_periode($d_date);
								}else{
									$d_actions[$nb_action]["planning"]="";
								}
									
							}
							
							$act_ok=true;
							
							$req_act->bindParam(":action_client",$r["acl_id"]);
							$req_act->bindParam(":action_societe",$soc["soc_id"]);
							$req_act->execute();
							$d_deja_fac=$req_act->fetch();
							if(!empty($d_deja_fac)){
								$act_ok=false;
							}
							
							if($act_ok){
							
								// NOUVELLE ACTION
								
								$premier_tour=false;
								
								$action_client=$r["acl_id"];
								
								$nb_action++;
								
								$d_int_id=array();
								$d_date=array();				
								$intervenant="";
								
								$d_test_id=array();
								$testeur="";
								
								$ht=$r["acl_ca_gfc"]-$r["acl_facture_gfc_ht"];	
							
								$qte=0;
								if($r["acl_pro_inter"]==1){
									$qte=$r["acl_for_nb_sta"];	
								}else{
									$qte=1;	
								};
								$pu=0;
								if(!empty($qte)){
									$pu=$ht/$qte;
								}
								
								$lieu=$r["act_adr_nom"] . "<br/>";
								if(!empty($r["act_adr_service"])){
									$lieu.=$r["act_adr_service"] . "<br/>";
								}
								if(!empty($r["act_adr1"])){
									$lieu.=$r["act_adr1"] . "<br/>";
								}
								if(!empty($r["act_adr2"])){
									$lieu.=$r["act_adr2"] . "<br/>";
								}
								if(!empty($r["act_adr3"])){
									$lieu.=$r["act_adr3"] . "<br/>";
								}
								$lieu.=$r["act_adr_cp"] . " " . $r["act_adr_ville"];
								
								$acl_commentaire="";
								//if(!empty($r["acl_commentaire"])){
								//	$acl_commentaire=str_replace('"',"''",$r["acl_commentaire"]);
								//}

								$d_actions[$nb_action]=array(
									"date_deb" => $r["act_date_deb"],
									"action" => $r["act_id"],
									"action_client" => $r["acl_id"],
									"reference" => $r["acl_pro_reference"],
									"libelle" => $r["acl_pro_libelle"],
									"produit" => $r["acl_produit"],
									"pu" => $pu,
									"qte" => $qte,
									"ht" => $ht,
									"stagiaires" => "",
									"lieu" => $lieu,
									"complement" => $acl_commentaire,
									"action_client_soc" => $soc["soc_id"],
									"opca_num" => $r["acl_opca_num"],
									"bc_num" => $r["acl_bc_num"],
									"inter" => $r["acl_pro_inter"]								
								);
							}

						}
						if($act_ok){
							/*if($r["pda_categorie"]!=2){
								if(!isset($d_int_id[$r["pda_intervenant"]])){
									$d_int_id[$r["pda_intervenant"]]=1;
									if(!empty($intervenant)){
										$intervenant.=",";
									}
									
									$intervenant.=$r["int_label_2"] . " " . $r["int_label_1"];
								}
							}else{
								if(!isset($d_test_id[$r["pda_intervenant"]])){
									$d_test_id[$r["pda_intervenant"]]=1;
									if(!empty($testeur)){
										$testeur.=",";
									}
									
									$testeur.=$r["int_label_2"] . " " . $r["int_label_1"];
								}
							}*/
							
							$d_date[]=array(
								"pda_date" => $r["pda_date"],
								"pda_demi" => $r["pda_demi"]
							);
						}		
					}
					
					if($act_ok){					
						$d_actions[$nb_action]["intervenants_lib"]=$intervenant;	
						if(!empty($d_date[0]["pda_date"])){
							$d_actions[$nb_action]["planning"]=planning_periode($d_date);
						}else{
							$d_actions[$nb_action]["planning"]="";
						}
							
					}
				}
			}
		}
		
		function cmp($a, $b)
		{
			return strcmp($a["date_deb"], $b["date_deb"]);
		}
		usort($d_actions, "cmp");
	}
	
	
	if(!empty($d_actions)){
		
		//$sql_sta="SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id IN (:liste_sta) ORDER BY sta_nom,sta_prenom;";
		//$req_sta=$Conn->prepare($sql_sta);
					
		foreach($d_actions as $k => $a){
			
			if($a["inter"]){

				$sql="SELECT DISTINCT ast_stagiaire FROM Actions_Stagiaires WHERE ast_action_client=" . $a["action_client"] . " AND ast_confirme;";
				$req=$ConnSoc->query($sql);
				$src_sta=$req->fetchAll(PDO::FETCH_ASSOC);
				if(!empty($src_sta)){
					
					//	print_r($src_sta);
					
					$tab_sta=array_column ($src_sta,"ast_stagiaire");
					$liste_sta=implode($tab_sta,",");
					/*var_dump($liste_sta);
					echo("<br/>");
					die();*/
					if(!empty($liste_sta)){
						
						$sql_sta="SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id IN (" . $liste_sta . ") ORDER BY sta_nom,sta_prenom;";
						$req_sta=$Conn->query($sql_sta);
						
						//$req_sta->bindValue(":liste_sta",$liste_sta);
						//$req_sta->execute();
						$d_stagiaires=$req_sta->fetchAll(PDO::FETCH_ASSOC);
						
						//echo("<pre>");
						//	print_r($d_stagiaires);
						//echo("</pre>");
						
						if(!empty($d_stagiaires)){
							$sta_texte="<u>Stagiaire(s) :</u><ul>";
							foreach($d_stagiaires as $sta){
								//echo("<br/>");
								//var_dump($sta);
							
								$sta_texte.="<li>" . $sta["sta_nom"] . " " . $sta["sta_prenom"] . "</li>";
							}
							$sta_texte.="</ul>";
							
							//var_dump($sta_texte);
						//die();
							$d_actions[$k]["stagiaires"]=$sta_texte;	
						}
						
					}
				}
			}
			
		}
	}
	
	return $d_actions;
	
 
}
?>
