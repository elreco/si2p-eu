<?php

/* AJOUT D'UN STAGIAIRE DANS LA BASE D'UNE SOCIETE

option
controle_boublon INT controle si le stagiaire est déjà connu

AUTEUR FG
*/
function add_stagiaire($nom,$prenom,$naissance,$mail,$ad1,$ad2,$ad3,$cp,$ville,$tel,$portable,$cp_naiss,$ville_naiss,$mail_perso,$client,$controle_boublon){

	global $Conn;
	
	$stagiaire=0;
	
	$erreur="";

	if(!empty($nom) AND !empty($prenom)){
		
		$date_naissance_sql=null;
		if(!empty($naissance)){
			$date_naissance=date_create_from_format('j/m/Y',$naissance);
			$date_naissance_sql=$date_naissance->format("Y-m-d");
		}
		
		$nom=strtoupper($nom);
		$prenom=ucfirst($prenom);
		if(!empty($ville)){
			$ville=strtoupper($ville);
		}
		if(!empty($ville_naiss)){
			$ville_naiss=strtoupper($ville_naiss);
		}

		if(!empty($controle_boublon)){
			
			// premier controle sur les stagiaire du client
			// pour un même client on considère que même nom + même prénom = meme stagiaire
			
			$sql="SELECT sta_id FROM Stagiaires,Stagiaires_clients WHERE sta_id=scl_stagiaire AND sta_nom=:nom AND sta_prenom=:prenom";
			$sql.=" AND scl_client=:client ORDER BY sta_id";
			$req=$Conn->prepare($sql);
			$req->bindParam(":nom",$nom);
			$req->bindParam(":prenom",$prenom);
			$req->bindParam(":client",$client);
			$req->execute();
			$doublons=$req->fetch();
			if(!empty($doublons)){
				if($controle_boublon==1){
					$stagiaire=intval($doublons["sta_id"]);
					$client=0;
				}else{
					$erreur="Le stagiaire " . $nom . " " . $prenom . " est déjà enregistré pour ce client";	
				}
			
			}
			
			if(empty($erreur)){

			/* 	sur le stagiaire des autres clients	
				si n'y mail perso n'y date de naissance => controle inutile
				même nom + même prenom dans 2 entreprise différente ne permet pas d'établir que c'est la même personne
				=> On crée comme nouveau stagiaire et on utilisera une fonction de fusion si besoin */
				
				if($stagiaire==0 AND (!empty($mail_perso) OR !empty($date_naissance_sql)) ){
				
					$sql="SELECT sta_id, sta_mail_perso, sta_naissance FROM Stagiaires,Stagiaires_clients WHERE sta_id=scl_stagiaire AND sta_nom=:nom AND sta_prenom=:prenom";
					$sql.=" AND NOT scl_client=:client";
					if(!empty($mail_perso)){
						$sql.=" AND (sta_mail_perso=:mail_perso OR ISNULL(sta_mail_perso) )";
					}
					if(!empty($date_naissance_sql)){
						$sql.=" AND (sta_naissance=:naissance OR ISNULL(sta_naissance) )";
					}
					$sql.=" ORDER BY sta_id";
					$req=$Conn->prepare($sql);
					$req->bindParam(":nom",$nom);
					$req->bindParam(":prenom",$prenom);
					$req->bindParam(":client",$client);
					if(!empty($mail_perso)){
						$req->bindParam(":mail_perso",$mail_perso);
					}
					if(!empty($date_naissance_sql)){
						$req->bindParam(":naissance",$date_naissance_sql);
					}
					$req->execute();
					$doublons=$req->fetchAll();
					if(!empty($doublons)){
						foreach($doublons as $d){
							if(!empty($mail_perso) AND !empty($d["sta_mail_perso"])){
								if($controle_boublon==1){
									$stagiaire=intval($d["sta_id"]);
								}else{
									$erreur="Le stagiaire " . $nom . " " . $prenom . " existe déjà";	
								}
								break;
							}elseif(!empty($date_naissance_sql) AND !empty($d["sta_naissance"])){
								if($controle_boublon==1){
									$stagiaire=intval($d["sta_id"]);
								}else{
									$erreur="Le stagiaire " . $nom . " " . $prenom . " existe déjà";	
								}
								break;
							}
						}
						// il y a des boublon mais les données ne permettent pas d'être sur que c'est la même personne
						if($stagiaire==0){
							$sta_doublon=1;
						}
					}
				}
			}
		}
		
		if($stagiaire==0 AND empty($erreur)){
			
			$sql="SELECT cli_code,cli_nom FROM Clients WHERE cli_id=:client;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":client",$client);
			$req->execute();
			$d_client=$req->fetch();
	
			$sql="INSERT INTO Stagiaires (
			sta_nom,
			sta_prenom,
			sta_naissance,
			sta_mail,
			sta_ad1,
			sta_ad2,
			sta_ad3,
			sta_cp,
			sta_ville,
			sta_tel,
			sta_ville_naiss,
			sta_cp_naiss,
			sta_portable,
			sta_mail_perso,
			sta_doublon,
			sta_client,
			sta_client_code,
			sta_client_nom
			) VALUES (
			:nom,
			:prenom,
			:naissance,
			:mail,
			:ad1,
			:ad2,
			:ad3,
			:cp,
			:ville,
			:tel,
			:ville_naiss,
			:cp_naiss,
			:portable,
			:mail_perso,
			:doublon,
			:client,
			:client_code,
			:client_nom
			);";
			
			$req=$Conn->prepare($sql);
			$req->bindParam(":nom",$nom);
			$req->bindParam(":prenom",$prenom);
			$req->bindParam(":naissance",$date_naissance_sql);
			$req->bindParam(":mail",$mail);
			$req->bindParam(":ad1",$ad1);
			$req->bindParam(":ad2",$ad2);
			$req->bindParam(":ad3",$ad3);
			$req->bindParam(":cp",$cp);
			$req->bindParam(":ville",$ville);
			$req->bindParam(":tel",$tel);
			$req->bindParam(":ville_naiss",$ville_naiss);
			$req->bindParam(":cp_naiss",$cp_naiss);
			$req->bindParam(":portable",$portable);
			$req->bindParam(":mail_perso",$mail_perso);
			$req->bindParam(":doublon",$sta_doublon);
			$req->bindParam(":client",$client);
			$req->bindParam(":client_code",$d_client["cli_code"]);
			$req->bindParam(":client_nom",$d_client["cli_nom"]);
			
			$req->execute();
			$stagiaire=$Conn->lastInsertId ();
			$stagiaire=intval($stagiaire);
			if($client>0){
				$sql="INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:stagiaire, :client);";
				$req=$Conn->prepare($sql);
				$req->bindParam(":stagiaire",$stagiaire);
				$req->bindParam(":client",$client);
				$req->execute();
			}
		}
	}
	if(!empty($stagiaire)){
		return intval($stagiaire);
	}elseif(!empty($erreur)){
		return $erreur;
	}else{
		return "Le stagiaire n'a pas été enregistré";
	}	
}
?>
