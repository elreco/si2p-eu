<?php

function import_planning($for_demi, $import_date, $intervenant, $acc_agence, $planning_date_import = array()){
    global $Conn;
    global $ConnSoc;
    $p_date = new DateTime($import_date);
    $for_annee = $p_date->format("Y");
    if($p_date->format("N")== 7){
    	$for_semaine = $p_date->format("W") + 1;
    }else{
    	$for_semaine = $p_date->format("W");
    }
    $for_utilisation=1;
    $for_nb_groupe=0;
    $for_nb_h_groupe=0;
    $total_h=0;
    $for_action=0;
    $for_vehicule_id=0;
    $for_vehicule_categorie = 0;
    $for_vehicule_immat="";
    $sql="SELECT * FROM plannings_dates WHERE pda_date = '" . $import_date . "' AND pda_demi = ". $for_demi . " AND pda_intervenant = " . $intervenant . " AND pda_archive = 0";
    $req = $ConnSoc->query($sql);
    $planning_date=$req->fetch();
    $continue2 = true;
    if(!empty($planning_date_import) && !empty($planning_date)){
        if($planning_date['pda_date'] == $planning_date_import['pda_date'] && $planning_date['pda_ref_1'] == $planning_date_import['pda_ref_1'] && $planning_date['pda_type'] == $planning_date_import['pda_type']){
            $sql="SELECT * FROM formations WHERE for_id = " .$planning_date_import['for_id'];
            $req = $ConnSoc->query($sql);
            $d_formation_import=$req->fetch();
            if($planning_date_import['pda_type'] == 1 && !empty($d_formation_import['for_action'])){
                // FORMATION
                $sql="SELECT act_gest_sta FROM actions WHERE act_id = " .$d_formation_import['for_action'];
                $req = $ConnSoc->query($sql);
                $d_action=$req->fetch();

                $sql="SELECT * FROM actions_sessions WHERE ase_action = " .$d_formation_import['for_action'] . " AND ase_date = " . $planning_date_import['pda_id'] . " ORDER BY left(ase_h_deb, 2) ASC";
                $req = $ConnSoc->query($sql);
                $sessions=$req->fetchAll();
                if($d_action['act_gest_sta']==2){
                    // INSCRIPTION SESSION ==> ON ADITIONNE TOUT
                    $for_nb_groupe = $d_formation_import["for_nb_groupe"] + count($sessions);
                }else{
                    // INSCRIPTION FORMATION ==> ON PREND LA PLUS GRANDE VALEUR
                    $for_nb_groupe = 1;
                    // JE COMPTE LE
                }
                if(!empty($sessions[0]['ase_h_fin']) && !empty($sessions[0]['ase_h_deb'])){
                        $DT_deb =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $sessions[0]["ase_h_deb"] . ":00");
                        $DT_fin =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $sessions[0]["ase_h_fin"] . ":00");
                        $interval = $DT_deb->diff($DT_fin);

                        if($d_action['act_gest_sta']==2){
                            $for_nb_h_groupe=$interval->h + $interval->i/60;
                        }else{
                            $sql="SELECT * FROM actions_sessions WHERE ase_action = " .$d_formation_import['for_action'] . " AND ase_date = " . $planning_date['pda_id'] . " ORDER BY left(ase_h_deb, 2) ASC";
                            $req = $ConnSoc->query($sql);
                            $ses=$req->fetch();
                            $DT_deb =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $ses["ase_h_deb"] . ":00");
                            $DT_fin =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $ses["ase_h_fin"] . ":00");
                            $interval = $DT_deb->diff($DT_fin);
                            $for_nb_h_groupe=($interval->h + $interval->i/60) + $d_formation_import['for_nb_h_groupe'];
                        }
                        // var_dump()
                }else{
                    $for_nb_h_groupe=0;
                }

                $total_h = $for_nb_groupe*$for_nb_h_groupe;



                // METTRE A JOUR NB GROUPE et NB GROUPE PAR heures
                $sql="UPDATE formations SET for_nb_groupe = ". $for_nb_groupe. ", for_nb_h_groupe = " . $for_nb_h_groupe. " , for_total_h = " . $total_h . " WHERE for_id = " .$planning_date_import['for_id'];
                $req = $ConnSoc->prepare($sql);
                $req->execute();
            }

            $sql="UPDATE formations SET for_demi = 1.5, for_nb_demi = 2 WHERE for_id = " .$planning_date_import['for_id'];
            $req = $ConnSoc->prepare($sql);
            $req->execute();

            $continue2 = false;
            // var_dump($planning_date_import['pda_date'] );
            // echo("<br>");
            // var_dump($planning_date['pda_date'] );
            // echo("<br><br>");

        }
    }

    if(!empty($planning_date) && $continue2==true){
        // var_dump($planning_date['pda_type']);
        // var_dump($planning_date['pda_ref_1']);
        // die();
        if($planning_date['pda_type'] == 1){
            $for_utilisation=1;

            // C UNE ACTION
            $sql="SELECT * FROM actions WHERE act_id = " .$planning_date['pda_ref_1'];
            $req = $ConnSoc->query($sql);
            $action=$req->fetch();
            $for_action = $action['act_id'];
            $sql="SELECT * FROM actions_sessions WHERE ase_action = " .$planning_date['pda_ref_1'] . " AND ase_date = " . $planning_date['pda_id'] . " ORDER BY ase_h_deb ASC";
            $req = $ConnSoc->query($sql);
            $sessions=$req->fetchAll();
            $for_nb_groupe = count($sessions);


            if(!empty($sessions[0]['ase_h_fin']) && !empty($sessions[0]['ase_h_deb'])){
                    $DT_deb =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $sessions[0]["ase_h_deb"] . ":00");
                    $DT_fin =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $sessions[0]["ase_h_fin"] . ":00");
                    $interval = $DT_deb->diff($DT_fin);

                    //$duree->add(date_diff($DT_deb, $DT_fin));
                    /* */
                    // echo("<pre>");
                    // print_r($interval);
                    // print_r($sessions[0]["ase_h_fin"]);
                    // echo("</pre>");
                    // die();
                    $for_nb_h_groupe=$interval->h + $interval->i/60;
                    // var_dump()
            }else{
                $for_nb_h_groupe=0;
            }
            $total_h = $for_nb_groupe*$for_nb_h_groupe;

            if(!empty($action['act_vehicule'])){
                $sql="SELECT * FROM vehicules WHERE veh_id = " . $action['act_vehicule'];
                $req = $Conn->query($sql);
                $vehicule=$req->fetch();
                $for_vehicule_id = $action['act_vehicule'];
                if(!empty($vehicule['veh_categorie'])){
                    $for_vehicule_immat = $vehicule['veh_immat'];
                    $for_vehicule_categorie=$vehicule['veh_categorie'];

                }else{
                    $for_vehicule_id = 0;
                    $for_vehicule_immat = "";
                    $for_vehicule_categorie=0;
                }
            }
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==7){
            $for_utilisation=3;
        }elseif($planning_date['pda_type']==2){
            $sql="SELECT con_categorie FROM conges WHERE con_id = " . $planning_date['pda_ref_1'];
            $req = $Conn->query($sql);
            $conge=$req->fetch();
            if(!empty($conge)){
                if($conge['con_categorie']==1){
                    $for_utilisation=5;
                }elseif($conge['con_categorie']==2){
                    $for_utilisation=6;
                }elseif($conge['con_categorie']==3){
                    $for_utilisation=6;
                }elseif($conge['con_categorie']==4){
                    $for_utilisation=6;
                }elseif($conge['con_categorie']==5){
                    $for_utilisation=6;
                }elseif($conge['con_categorie']==6){
                    $for_utilisation=1;
                }elseif($conge['con_categorie']==7){
                    $for_utilisation=9;
                }elseif($conge['con_categorie']==8){
                    $for_utilisation=7;
                }elseif($conge['con_categorie']==9){
                    $for_utilisation=6;
                }elseif($conge['con_categorie']==10){
                    $for_utilisation=6;
                }
            }else{
                $for_utilisation=2;
            }
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==11 OR $planning_date['pda_type']==3 && $planning_date['pda_ref_1']==0){
            $for_utilisation=4;
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==23){
            $for_utilisation=8;
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==18){
            $for_utilisation=7;
        }elseif($planning_date['pda_type']==0 && $planning_date['pda_ref_1']==0){
            $for_utilisation=10;
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==18){
            $for_utilisation=7;
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==20){
            $for_utilisation=9;
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==21){
            $for_utilisation=6;
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==22){
            $for_utilisation=6;
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==23){
            $for_utilisation=8;
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==24){
            $for_utilisation=1;
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==12){
            $for_utilisation=1;
        }elseif($planning_date['pda_type']==3 && $planning_date['pda_ref_1']==9){
            $for_utilisation=1;
        }else{
            $for_utilisation=2;
        }

        $sql="INSERT INTO Formations (for_annee, for_semaine, for_intervenant, for_date, for_demi, for_nb_demi, for_nb_groupe, for_nb_h_groupe,
        for_total_h, for_vehicule_categorie, for_vehicule_id, for_vehicule_numero, for_utilisation,for_action)
        VALUES (:for_annee, :for_semaine, :for_intervenant, :for_date, :for_demi, :for_nb_demi, :for_nb_groupe, :for_nb_h_groupe,
        :for_total_h, :for_vehicule_categorie, :for_vehicule_id, :for_vehicule_numero, :for_utilisation, :for_action)";
        $req = $ConnSoc->prepare($sql);
        $req->bindValue(":for_annee",$for_annee);
        $req->bindValue(":for_semaine",$for_semaine);
        $req->bindValue(":for_intervenant",$intervenant);
        $req->bindValue(":for_date",$import_date);
        $req->bindValue(":for_demi",$for_demi);
        $req->bindValue(":for_nb_demi",$for_demi);
        $req->bindValue(":for_nb_groupe",$for_nb_groupe);
        // nb sessions
        $req->bindValue(":for_nb_h_groupe",$for_nb_h_groupe);
        //volume horaire premiere session
        $req->bindValue(":for_total_h",$total_h);
        $req->bindValue(":for_vehicule_categorie",$for_vehicule_categorie);
        $req->bindValue(":for_vehicule_id",$for_vehicule_id);
        $req->bindValue(":for_vehicule_numero",$for_vehicule_immat);
        $req->bindValue(":for_utilisation",$for_utilisation);
        $req->bindValue(":for_action",$for_action);
        $req->execute();
        $for_id =  $ConnSoc->lastInsertId();
        ///////////////////////////////////////////////////////
        if($planning_date['pda_type'] == 1){
            $sql="SELECT DISTINCT acl_id,acl_pro_reference,acl_client,acl_produit,acl_pro_libelle,cli_id, cli_code,acl_archive ";
            $sql.=" FROM Actions_Clients INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id";
            if($acc_agence>0){
                $sql.=" AND cli_agence=:agence";
            }
            $sql.=")
            WHERE acl_client=cli_id AND acl_action=:action AND acl_archive=0;";
            $req=$ConnSoc->prepare($sql);
            $req->bindParam(":action",$for_action);
            if($acc_agence>0){
                $req->bindParam(":agence",$action["act_agence"]);
            }
            $req->execute();
            $action_clients=$req->fetchAll();

            // NB STAGIAIRES
            $sql="SELECT COUNT(ast_stagiaire),ast_action_client,ast_confirme FROM Actions_Stagiaires WHERE ast_action=:action GROUP BY ast_action_client,ast_confirme;";
            $req=$ConnSoc->prepare($sql);
            $req->bindParam(":action",$for_action);
            $req->execute();
            $d_stagiaires=$req->fetchAll();
            if(!empty($d_stagiaires)){
                foreach($d_stagiaires as $sta){

                    $cle_cli=array_search($sta["ast_action_client"],array_column($action_clients,"acl_id"));

                    if(!is_bool($cle_cli)){
                        if(!empty($sta["ast_confirme"])){
                            $action_clients[$cle_cli]["nb_confirme"]=$sta[0];
                        }else{
    						if(empty($action_clients[$cle_cli]["acl_archive"])){
    							$action_clients[$cle_cli]["nb_confirme"]=$sta[0];
    						}
    					}
                    }
                }
            }

            ////////////////////////////////////////////////////////////////

            if(!empty($action_clients)){


                foreach($action_clients as $acl){

                    if(!empty($acl['nb_confirme'])){
                        $nb_stas = $acl['nb_confirme'];
                    }else{
                        $nb_stas = 0;
                    }
                    $sql="INSERT INTO formations_actions (fac_formation, fac_date, fac_demi, fac_action_client, fac_produit, fac_nb_stagiaires) VALUES (:fac_formation, :fac_date, :fac_demi, :fac_action_client, :fac_produit, :fac_nb_stagiaires)";
                    $req = $ConnSoc->prepare($sql);


                    $req->bindValue(":fac_formation",$for_id);
                    $req->bindValue(":fac_date",$import_date);
                    $req->bindValue(":fac_demi",$for_demi);
                    $req->bindValue(":fac_action_client",$acl['acl_id']);
                    $req->bindValue(":fac_produit",$acl['acl_produit']);
                    $req->bindValue(":fac_nb_stagiaires",$nb_stas);
                    $req->execute();
                    $sql="SELECT act_gest_sta FROM Actions  WHERE act_id =" . $for_action;
                    $req = $ConnSoc->prepare($sql);
                    $req->execute();
                    $d_action=$req->fetch();
                    $acl_nb_sta_rap = array();
                    if($d_action['act_gest_sta']==2){
                        // INSCRIPTION SESSION ==> ON ADITIONNE TOUT
                        if(empty($acl_nb_sta_rap[$acl['acl_client']])){
                            $acl_nb_sta_rap[$acl['acl_client']] = $nb_stas;
                        }else{
                            $acl_nb_sta_rap[$acl['acl_client']] = $acl_nb_sta_rap[$acl['acl_client']] + $nb_stas;
                        }
                    }else{
                        // INSCRIPTION FORMATION ==> ON PREND LA PLUS GRANDE VALEUR
                        if(empty($acl_nb_sta_rap[$acl['acl_client']]) OR (!empty($acl_nb_sta_rap[$acl['acl_client']]) && $nb_stas > $acl_nb_sta_rap[$acl['acl_client']])){
                            $acl_nb_sta_rap[$acl['acl_client']] = $nb_stas;
                        }
                        // JE COMPTE LE
                    }
                }

                foreach($acl_nb_sta_rap as $c=>$n){
                    $sql="UPDATE Actions_Clients SET acl_nb_sta_rap = " . $n . "  WHERE acl_client = " .$c . " AND acl_action =" . $for_action;
                    $req = $ConnSoc->prepare($sql);
                    $req->execute();
                }
                $sql="UPDATE Formations SET for_action = " . $for_action . "  WHERE for_id = " .$for_id;
                $req = $ConnSoc->prepare($sql);
                $req->execute();
            }
        }
        ////////////////////////////////////////////////////////////////
        $planning_date["for_id"] = $for_id;
    }

    if(empty($planning_date)){
        $planning_date = array();
    }
    return $planning_date;

}
