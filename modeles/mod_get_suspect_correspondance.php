<?php 

function get_suspect_correspondance($correspondance){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("SELECT * FROM Suspects_Correspondances WHERE sco_id = :correspondance;");
	$req->bindParam("correspondance",$correspondance);
	$req->execute();
	$suspect_correspondance = $req->fetch();
	return $suspect_correspondance;

}
?>