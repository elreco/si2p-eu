<?php

function upload_image($target_dir, $fichier, $fichier_nom, $fichier_taille, $fichier_new_nom){

	$target_file = $target_dir . $fichier_new_nom;
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

  // Vérifier si le fichier d'image est une image réelle ou fausse
	$erreurs = array();

	$check = getimagesize($fichier);
	if($check !== false) {
		echo "Le fichier est une image - " . $check["mime"] . ".";
		$uploadOk = 1;
	} else {
		$erreurs['1'] = "Le fichier n'est pas une image.";
		$uploadOk = 0;
	}


  // Vérifier la taille du fichier
	if ($fichier_taille > 500000) {
		$erreurs['3'] = "Désolé, votre fichier est trop volumineux.";
		$uploadOk = 0;
	}
  // ici on charge une image donc on vérifier si l'extension est bonne
  // NOTE: changer les extensions en fonction du type de fichier attendu
	if($imageFileType != "png") {
		$erreurs['4'] =  "Désolé, seule l'extension PNG est autorisée.";
		$uploadOk = 0;
	}
  // Vérifier si $uploadOk est à 0, si 0 alors il y a un souci.

	if ($uploadOk == 0) {
		$erreurs['5'] = "Désolé, votre fichier n'a pas été téléchargé.";
    // Si tout est ok, on upload le fichier
	} else {
		if (move_uploaded_file($fichier, $target_file)) {
			echo "Le fichier a été téléchargé.";
		} else {
			$erreurs['6'] = "Désolé, il y a eu une erreur lors de l'envoi de votre fichier.";
		}
	}

	return $erreurs;

}

function upload($input_fichier,$dossier,$nom,$taille,$extension,$replace){

	// $input_fichier => name du champ type="file"
	// $dossier => dossier de destination sans /document/"
	// $nom => renomme le fichier sur le serveur par defaut nom local = nom serveur Attention :
	// $taille => poids limite du fichier si 0 : no limite
	// $extension => array contenant les extension autorisé"
	// $replace => si $replace est égal à 1, ça remplace le fichier existant

	/* Attention :
	- $dossier doit se terminer par /
	- $nom ne doit pas contenir l'extention
	*/
	$erreur=0;

	$fichier=$_FILES[$input_fichier]['tmp_name'];
	$fichier_nom=$_FILES[$input_fichier]['name'];
	$fichier_taille=$_FILES[$input_fichier]['size'];
	$fichier_ext = pathinfo($fichier_nom, PATHINFO_EXTENSION);

	$dossier="documents/" . $dossier;

	// test restriction sur upload
	if(!empty($extension)){

		if(!in_array($fichier_ext,$extension)){
			$erreur=991;
		};
	}
	if($erreur==0 AND $taille>0){
		if($fichier_taille>$taille){
			$erreur=992;
		};
	};
	if($erreur==0){
		if(!file_exists($dossier)){
			if(!file_exists("../" . $dossier)){
				$erreur=993;
			}else{
				$dossier = "../" . $dossier;
			}
		};
	}

	if($replace!= 1){
		if($erreur==0){
			if(file_exists($dossier . $fichier_nom)){
				$erreur=994;
			};
		}
	}

	if($erreur==0){

		if(empty($nom)){
			$nom=$fichier_nom;
		};

		$cible=$dossier . $nom . "." . $fichier_ext;
		if(!move_uploaded_file($fichier, $cible)){
			$erreur=990;
		};

	}

	return $erreur;


}

?>
