<?php

function insert_client_adresse($adr_ref,$adr_ref_id,$adr_type,$adr_nom,$adr_service,$adr_ad1,$adr_ad2,$adr_ad3,$adr_cp,$adr_ville,$adr_defaut,$adr_libelle, $adr_geo, $adr_siret){
	global $ConnSoc;

	$req = $ConnSoc->prepare("INSERT INTO adresses (adr_ref, adr_ref_id, adr_type, adr_nom, adr_service, adr_ad1, adr_ad2, adr_ad3, adr_cp, adr_ville, adr_defaut, adr_libelle, adr_geo, adr_siret) VALUES (1, :adr_ref_id, :adr_type, :adr_nom, :adr_service, :adr_ad1, :adr_ad2, :adr_ad3, :adr_cp, :adr_ville, :adr_defaut, :adr_libelle, :adr_geo, :adr_siret);");


	$req->bindParam("adr_ref_id",$adr_ref_id);
	$req->bindParam("adr_type",$adr_type);
	$req->bindParam("adr_nom",$adr_nom);
	$req->bindParam("adr_service",$adr_service);
	$req->bindParam("adr_ad1",$adr_ad1);
	$req->bindParam("adr_ad2",$adr_ad2);
	$req->bindParam("adr_ad3",$adr_ad3);
	$req->bindParam("adr_cp",$adr_cp);
	$req->bindParam("adr_ville",$adr_ville);
	$req->bindParam("adr_defaut",$adr_defaut);
	$req->bindParam("adr_libelle",$adr_libelle);
	$req->bindParam("adr_geo",$adr_geo);
	$req->bindParam("adr_siret",$adr_siret);

	$req->execute();

	$last_adr = $ConnSoc->lastInsertId();
		if($adr_defaut == 1 && $adr_type == 1){
			$req = $ConnSoc->prepare("UPDATE clients SET cli_adr_int_defaut = " . $last_adr . "  WHERE cli_id = " . $adr_ref_id);
			$req->execute();
		}elseif($adr_defaut == 1 && $adr_type == 2){
			$req = $ConnSoc->prepare("UPDATE clients SET cli_adr_fac_defaut = " . $last_adr . "  WHERE cli_id = " . $adr_ref_id);
			$req->execute();
		}
		if($adr_defaut == 1 && $adr_type == 1 && !empty($adr['adr_cp'])){
				$departement = substr($adr['adr_cp'],0,2);
				$req8 = $Conn->prepare("SELECT * FROM societes_departements WHERE sde_departement=" . $departement . " AND sde_gc = 1");
				$req8->execute();
				$dep = $req->fetch();

				$req = $ConnSoc->prepare("UPDATE clients SET cli_region_soc = :cli_region_soc, cli_region_age = :cli_region_age  WHERE cli_id = :adr_id;");
				$req->bindParam("adr_id",$adr['adr_ref_id']);
				$req->bindParam("cli_region_age",$dep['sde_agence']);
				$req->bindParam("cli_region_soc",$adr['sde_soc']);
				$req->execute();

		}

}

function insert_suspect_adresse($sad_ref,$sad_ref_id,$sad_type,$sad_nom,$sad_service,$sad_ad1,$sad_ad2,$sad_ad3,$sad_cp,$sad_ville,$sad_defaut,$sad_libelle, $sad_geo, $sad_siret){
	global $ConnSoc;

	$req = $ConnSoc->prepare("INSERT INTO suspects_adresses (sad_ref, sad_ref_id, sad_type, sad_nom, sad_service, sad_ad1, sad_ad2, sad_ad3, sad_cp, sad_ville, sad_defaut, sad_libelle, sad_geo, sad_siret) VALUES (1, :sad_ref_id, :sad_type, :sad_nom, :sad_service, :sad_ad1, :sad_ad2, :sad_ad3, :sad_cp, :sad_ville, :sad_defaut, :sad_libelle, :sad_geo, :sad_siret);");


	$req->bindParam("sad_ref_id",$sad_ref_id);
	$req->bindParam("sad_type",$sad_type);
	$req->bindParam("sad_nom",$sad_nom);
	$req->bindParam("sad_service",$sad_service);
	$req->bindParam("sad_ad1",$sad_ad1);
	$req->bindParam("sad_ad2",$sad_ad2);
	$req->bindParam("sad_ad3",$sad_ad3);
	$req->bindParam("sad_cp",$sad_cp);
	$req->bindParam("sad_ville",$sad_ville);
	$req->bindParam("sad_defaut",$sad_defaut);
	$req->bindParam("sad_libelle",$sad_libelle);
	$req->bindParam("sad_geo",$sad_geo);
	$req->bindParam("sad_siret",$sad_siret);

	$req->execute();

}
function update_adresse_client($id,$adr_nom,$adr_service,$adr_ad1,$adr_ad2,$adr_ad3,$adr_cp,$adr_ville,$adr_defaut,$adr_libelle, $adr_geo, $adr_siret){
	global $ConnSoc;
	global $Conn;

	$req = $ConnSoc->prepare("UPDATE adresses SET adr_nom = :adr_nom, adr_service = :adr_service, adr_ad1 = :adr_ad1, adr_ad2 = :adr_ad2, adr_ad3 = :adr_ad3, adr_cp = :adr_cp, adr_ville = :adr_ville, adr_libelle= :adr_libelle, adr_defaut = :adr_defaut, adr_siret = :adr_siret, adr_geo = :adr_geo WHERE adr_id = :adr_id;");

	$req->bindParam("adr_id",$id);
	$req->bindParam("adr_nom",$adr_nom);
	$req->bindParam("adr_service",$adr_service);
	$req->bindParam("adr_ad1",$adr_ad1);
	$req->bindParam("adr_ad2",$adr_ad2);
	$req->bindParam("adr_ad3",$adr_ad3);
	$req->bindParam("adr_cp",$adr_cp);
	$req->bindParam("adr_ville",$adr_ville);
	$req->bindParam("adr_libelle",$adr_libelle);
	$req->bindParam("adr_defaut",$adr_defaut);
	$req->bindParam("adr_geo",$adr_geo);
	$req->bindParam("adr_siret",$adr_siret);
	$req->execute();

	$req = $ConnSoc->prepare("SELECT * FROM adresses WHERE adr_id = :adr_id;");
	$req->bindParam("adr_id",$id);
	$req->execute();
	$adr = $req->fetch();

		if($adr['adr_defaut'] == 1 && $adr['adr_type'] == 1 && !empty($adr['adr_cp'])){
				$departement = substr($adr['adr_cp'],0,2);
				$req8 = $Conn->prepare("SELECT * FROM societes_departements WHERE sde_departement=" . $departement . " AND sde_gc = 1");
				$req8->execute();
				$dep = $req->fetch();

				$req = $ConnSoc->prepare("UPDATE clients SET cli_region_soc = :cli_region_soc, cli_region_age = :cli_region_age  WHERE cli_id = :adr_id;");
				$req->bindParam("adr_id",$adr['adr_ref_id']);
				$req->bindParam("cli_region_age",$dep['sde_agence']);
				$req->bindParam("cli_region_soc",$adr['sde_soc']);
				$req->execute();

		}





}
function update_adresse_suspect($id,$adr_nom,$adr_service,$adr_ad1,$adr_ad2,$adr_ad3,$adr_cp,$adr_ville,$adr_defaut,$adr_libelle, $adr_geo, $adr_siret){
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE suspects_adresses SET sad_nom = :adr_nom, sad_service = :adr_service, sad_ad1 = :adr_ad1, sad_ad2 = :adr_ad2, sad_ad3 = :adr_ad3, sad_cp = :adr_cp, sad_ville = :adr_ville, sad_libelle= :adr_libelle, sad_defaut = :adr_defaut, sad_siret = :adr_siret, sad_geo = :adr_geo WHERE sad_id = :adr_id;");

	$req->bindParam("adr_id",$id);
	$req->bindParam("adr_nom",$adr_nom);
	$req->bindParam("adr_service",$adr_service);
	$req->bindParam("adr_ad1",$adr_ad1);
	$req->bindParam("adr_ad2",$adr_ad2);
	$req->bindParam("adr_ad3",$adr_ad3);
	$req->bindParam("adr_cp",$adr_cp);
	$req->bindParam("adr_ville",$adr_ville);
	$req->bindParam("adr_libelle",$adr_libelle);
	$req->bindParam("adr_defaut",$adr_defaut);
	$req->bindParam("adr_geo",$adr_geo);
	$req->bindParam("adr_siret",$adr_siret);
	$req->execute();

}
function update_adresse_defaut($id,$adr_defaut){
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE adresses SET adr_defaut = :adr_defaut WHERE adr_id = :adr_id;");

	$req->bindParam("adr_id",$id);
	$req->bindParam("adr_defaut",$adr_defaut);
	$req->execute();

}
function update_adresse_suspect_defaut($id,$adr_defaut){
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE suspects_adresses SET sad_defaut = :adr_defaut WHERE sad_id = :adr_id;");

	$req->bindParam("adr_id",$id);
	$req->bindParam("adr_defaut",$adr_defaut);
	$req->execute();

}
function update_adresse_client_defaut($id,$adr_defaut){
	global $ConnSoc;

	$req = $ConnSoc->prepare("UPDATE adresses SET adr_defaut = :adr_defaut WHERE adr_id = :adr_id;");

	$req->bindParam("adr_id",$id);
	$req->bindParam("adr_defaut",$adr_defaut);
	$req->execute();

}

function get_adresse($ref_id, $ref, $adr_type){

	global $ConnSoc;	
	if($ref == null){
		$ref = 0;
	}
	$req_sql="SELECT * FROM adresses WHERE adr_defaut = 1 AND adr_type = $adr_type AND adr_ref = 1 AND adr_ref_id = " . $ref_id;
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetch();
	return $get_compte;
}
function get_adresse_suspect($ref_id, $ref, $adr_type){

	global $ConnSoc;	
	if($ref == null){
		$ref = 0;
	}
	$req_sql="SELECT * FROM suspects_adresses WHERE sad_defaut = 1 AND sad_type = $adr_type AND sad_ref = 1 AND sad_ref_id = " . $ref_id;
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetch();
	return $get_compte;
}

function get_adresse_client($ref_id, $ref, $adr_type){

	global $ConnSoc;	
	if($ref == null){
		$ref = 0;
	}
	$req_sql="SELECT * FROM adresses WHERE adr_defaut = 1 AND adr_type = $adr_type AND adr_ref = 1 AND adr_ref_id = " . $ref_id;
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetch();
	return $get_compte;
}

function get_adresses($ref_id){

	global $ConnSoc;	
	$req_sql="SELECT * FROM adresses WHERE adr_ref_id=$ref_id ORDER BY adr_defaut DESC";
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetchAll();
	return $get_compte;
}

function delete_suspect_adresse($id){


	global $ConnSoc;

	$req = $ConnSoc->prepare("DELETE FROM suspects_adresses WHERE sad_id = :adr_id;");

	$req->bindParam("adr_id",$id);
	$req->execute();

}

function delete_client_adresse($id){


	global $ConnSoc;

	$req = $ConnSoc->prepare("DELETE FROM adresses WHERE adr_id = :adr_id;");

	$req->bindParam("adr_id",$id);
	$req->execute();

}

function get_adresses_suspects($ref_id, $type){
	if($type == null){
		$type = 0;
	}
	global $ConnSoc;	
	$req_sql="SELECT * FROM suspects_adresses WHERE sad_ref_id=$ref_id AND sad_type = $type ORDER BY sad_defaut DESC";
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetchAll();
	return $get_compte;
}
function get_adresses_clients($ref_id, $type){
	if($type == null){
		$type = 0;
	}
	global $ConnSoc;	
	$req_sql="SELECT * FROM adresses WHERE adr_ref_id=$ref_id AND adr_type = $type ORDER BY adr_defaut DESC";
	$req=$ConnSoc->query($req_sql);
	$get_compte = $req->fetchAll();
	return $get_compte;
}
?>