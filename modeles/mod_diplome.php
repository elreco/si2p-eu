<?php

	// Modele lié à la table Diplomes 
	
	/*----------------------------------------
		FONCTION DE SELECTION
	------------------------------------------ */
	
	function get_diplomes(){
		
		global $Conn;	
		
		$req_sql="SELECT * FROM Diplomes ORDER BY dip_libelle";
		$req=$Conn->query($req_sql);
		$get_diplomes = $req->fetchAll();	
		return $get_diplomes;
	}
	
	function get_diplome($diplome){
		
		global $Conn;	
		
		$req_sql="SELECT * FROM Diplomes WHERE dip_id=:diplome";
		$req=$Conn->prepare($req_sql);
		$req->bindParam(":diplome",$diplome);
		$req->execute();
		$get_diplome = $req->fetch();	
		return $get_diplome;
	}
	// retourne la liste de tous les diplomes avec leurs valeurs d'affectation à la compétence passé en parametre
	function get_diplomes_competence($competence){
		
		global $Conn;	
		
		$sql="SELECT * FROM Diplomes LEFT OUTER JOIN Diplomes_Competences ON (Diplomes.dip_id=Diplomes_Competences.dco_diplome AND Diplomes_Competences.dco_competence=" . $competence . ") ORDER BY dip_libelle";
		
		$req=$Conn->query($sql);
		//$req -> bindParam(":competence",$competence);
		$get_diplomes_competence = $req->fetchAll();	
		return $get_diplomes_competence;
	}
	
	// retourne la liste de tous les diplomes lié a une compétences avec leurs valeurs d'affectations à un intervenants
	// permet de calculer etat de la competence
	function get_diplomes_competence_inter($ref,$ref_id,$competence){
		
		global $Conn;	
		
		$sql="SELECT * FROM Diplomes LEFT JOIN Diplomes_Competences ON (Diplomes.dip_id=Diplomes_Competences.dco_diplome)";
		$sql.=" LEFT OUTER JOIN Intervenants_Diplomes ON (Diplomes.dip_id=Intervenants_Diplomes.idi_diplome AND Intervenants_Diplomes.idi_ref=:ref AND Intervenants_Diplomes.idi_ref_id=:ref_id)";
		$sql.=" WHERE dco_competence=:competence";
		if($ref==1){
			$sql.=" AND dco_interne";
		}else{
			$sql.=" AND dco_externe";	
		}
		$sql.=" ORDER BY dip_libelle";
		
		$req=$Conn->prepare($sql);
		$req -> bindParam(":ref",$ref);
		$req -> bindParam(":ref_id",$ref_id);
		$req -> bindParam(":competence",$competence);
		$req->execute();
		$get_diplomes = $req->fetchAll();	
		return $get_diplomes;
	}
	// retourne la liste de tous les diplomes qui doivent être demandé à un intervenant en fonction de la liste de ces compétences
	// la fonction retourne aussi les infos les diplomes déjà chargés.
	
	function get_diplomes_intervenant($ref,$ref_id){
		
		global $Conn;	
		
		$sql="SELECT DISTINCT ";
		$sql.="dip_id,dip_libelle";
		$sql.=",idi_date_deb,idi_date_fin,idi_fichier";
		
		$sql.=" FROM Intervenants_Competences LEFT JOIN diplomes_competences ON (";
		$sql.="Intervenants_Competences.ico_competence=Diplomes_Competences.dco_competence)";
		
		$sql.=" LEFT JOIN Diplomes ON (diplomes_competences.dco_diplome=Diplomes.dip_id)";
		
		$sql.=" LEFT OUTER JOIN Intervenants_Diplomes ON (";
		$sql.="Diplomes.dip_id=Intervenants_Diplomes.idi_diplome AND idi_ref=:ref AND idi_ref_id=:ref_id)";
		$sql.=" WHERE ico_ref=:ref AND ico_ref_id=:ref_id";
		if($ref==1){
			// INTERNE
			$sql.=" AND dco_interne";
		}else{
			$sql.=" AND dco_externe";	
		}
		$sql.=" ORDER BY dip_libelle";
		
		$req=$Conn->prepare($sql);
		$req -> bindParam(":ref",$ref);
		$req -> bindParam(":ref_id",$ref_id);
		$req->execute();
		$get_diplomes = $req->fetchAll(PDO::FETCH_ASSOC);	
		return $get_diplomes;
	}
	
	// retourne les données associé a un diplome et un intervenant
	
	function get_diplome_intervenant($ref,$ref_id,$diplome){
		
		global $Conn;	
		
		$sql="SELECT * FROM Intervenants_Diplomes WHERE idi_ref=:ref AND idi_ref_id=:ref_id AND idi_diplome=:diplome";
		$req=$Conn->prepare($sql);
		$req -> bindParam(":ref",$ref);
		$req -> bindParam(":ref_id",$ref_id);
		$req -> bindParam(":diplome",$diplome);
		$req->execute();
		$get_diplome = $req->fetch();	
		return $get_diplome;
	}
	/*----------------------------------------
		FONCTION DE MISE A JOUR
	------------------------------------------ */

	function update_diplome($diplome_id,$diplome_libelle,$diplome_validite){
		
		global $Conn;	
		$req = $Conn->prepare("UPDATE diplomes SET dip_libelle=:diplome_libelle, dip_validite=:diplome_validite WHERE dip_id=:diplome_id");
		$req->bindParam(':diplome_id', $diplome_id);
		$req->bindParam(':diplome_libelle', $diplome_libelle);
		$req->bindParam(':diplome_validite', $diplome_validite);
		$req->execute();	
	}
	function update_diplome_intervenant($ref,$ref_id,$diplome,$date_fin,$fichier){
		
		global $Conn;	
		
		$sql="UPDATE Intervenants_Diplomes SET idi_date_fin=:date_fin, idi_fichier=:fichier";
		$sql.=" WHERE idi_ref=:ref AND idi_ref_id=:ref_id AND idi_diplome=:diplome";
		$req=$Conn->prepare($sql);
		$req -> bindParam(":ref",$ref);
		$req -> bindParam(":ref_id",$ref_id);
		$req -> bindParam(":diplome",$diplome);
		$req -> bindParam(":date_fin",$date_fin);
		$req -> bindParam(":fichier",$fichier);
		$req->execute();
	}
	/*----------------------------------------
		FONCTION DE CREATION
	------------------------------------------ */
	function insert_diplome($diplome_libelle,$diplome_validite){
		
		global $Conn;

		$diplome_id=0;
		
		$req = $Conn->prepare("INSERT INTO diplomes (dip_libelle, dip_validite) VALUES (:diplome_libelle, :diplome_validite)");
		$req->bindParam(':diplome_libelle', $diplome_libelle);
		$req->bindParam(':diplome_validite', $diplome_validite);
		$req->execute();
		$diplome_id=$Conn ->lastInsertId();
		
		return $diplome_id;
	}
	function insert_diplome_intervenant($ref,$ref_id,$diplome,$date_fin,$fichier){
		
		global $Conn;	
		
		$sql="INSERT INTO Intervenants_Diplomes (idi_ref,idi_ref_id,idi_diplome,idi_date_fin,idi_fichier)";
		$sql.=" VALUES (:ref, :ref_id, :diplome, :date_fin, :fichier)";
		$req=$Conn->prepare($sql);
		$req -> bindParam(":ref",$ref);
		$req -> bindParam(":ref_id",$ref_id);
		$req -> bindParam(":diplome",$diplome);
		$req -> bindParam(":date_fin",$date_fin);
		$req -> bindParam(":fichier",$fichier);
		$req->execute();
	}
	/*----------------------------------------
		FONCTION DE SUPPRESION
	------------------------------------------ */
	function delete_diplome_intervenant($ref,$ref_id,$diplome){
			
			global $Conn;
			
			$sql="DELETE FROM Intervenants_Diplomes WHERE idi_ref=:ref AND idi_ref_id=:ref_id AND idi_diplome=:diplome";
			$req = $Conn->prepare($sql);
			$req->bindParam(":ref",$ref);
			$req->bindParam(":ref_id",$ref_id);
			$req->bindParam(":diplome",$diplome);
			$req->execute();
	}
 
?>	
