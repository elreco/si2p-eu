<?php
	
	// PERMET DE VERIFIER SI UN SIRET EXISTE DEJA DANS LA BASE SUSPECT N
	

	function check_suspect_siret($siren,$siret,$adresse){
		
		global $ConnSoc;
		
		
		if(empty($siren) OR empty($siret)){
			return false;
		}
		
		if(!empty($adresse)){
			$adresse=intval($adresse);
		}else{
			$adresse=0;
		}
		
		$d_doublon=array(
			"doublon" => false
		);
		
		
		$sql="SELECT DISTINCT sus_id,sus_code,sus_nom FROM Suspects LEFT JOIN Suspects_Adresses ON (Suspects.sus_id=Suspects_Adresses.sad_ref_id)
		WHERE sus_siren LIKE :siren AND sad_siret LIKE :siret";
		if(!empty($adresse)){
			$sql.=" AND NOT sad_id=" . $adresse; 
		}
		$sql.=" ORDER BY sus_code,sus_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":siren",$siren);
		$req->bindParam(":siret",$siret);
		$req->execute();
		$d_result=$req->fetchAll();
		if(!empty($d_result)){
			$d_doublon["doublon"] = true;
		}
		
		return $d_doublon;	
	}
?>
	