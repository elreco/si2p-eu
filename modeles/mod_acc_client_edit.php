<?php

	// IDENTIFIE SI U A LE DROIT DE MODIFIER UNE FICHE

	function acc_client_edit($client){
		
		Global $Conn;
		
		$acc_utilisateur=0;
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
		
		$acc_societe=0;
		if(!empty($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];
		}
		$acc_agence=0;
		if(!empty($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];
		}
		
		$sql="SELECT cli_societe,cli_agence,cli_categorie,cli_utilisateur FROM Clients WHERE cli_id=" . $client . ";";
		$req=$Conn->query($sql);
		$client=$req->fetch();
		if(!empty($client)){
			
			if(!$_SESSION['acces']["acc_droits"][6] AND $client["cli_utilisateur"]!=$acc_utilisateur){
				return false;
			}else{
				
				$hors_region=false;
				if($client["cli_societe"]!=$acc_societe OR ($acc_agence>0 AND $acc_agence!=$client["cli_agence"])){
					$hors_region=true;
				}
					
				if(!$_SESSION['acces']["acc_droits"][8]){
					// non gc
					if($hors_region OR ($client["cli_categorie"]==6)  OR ($client["cli_categorie"]==2 AND $client["cli_sous_categorie"]!=2)){
						return false;
					}else{
						return true;
					}					
				}else{
					if(!$client["cli_categorie"]==2 AND !$client["cli_categorie"]==6 AND $hors_region){
						return false;
					}else{
						return true;
					}
					
				}
			}
		}else{
			return false;
		}
	}
?>
