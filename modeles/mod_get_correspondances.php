<?php 

function get_correspondances($client){
	
	global $Conn;

	$req = $Conn->prepare("SELECT * FROM Correspondances WHERE cor_client = :client ORDER BY cor_date DESC;");
	$req->bindParam("client",$client);
	$req->execute();
	$correspondances = $req->fetchAll();
	return $correspondances;

}
?>