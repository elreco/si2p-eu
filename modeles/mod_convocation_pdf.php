<?php

// GENERE DES CONVOCATION EN PDF

//require_once("../vendor/autoload.php");

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

require_once("../vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;

function model_convocation_pdf($data){
	
	global $acc_societe;

	ob_start(); ?>
	
	<style type="text/css" >
<?php	require("../assets/skin/si2p/css/orion_pdf.css"); ?>
	</style>
<?php
	foreach($data["pages"] as $p => $page){ ?>
								
		<page backtop="27mm" backleft="5mm" backright="5mm" backbottom="10mm" >
			<page_header>						
				<table>
					<tr>
						<td class="w-25" >
				<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
								<img src="../<?=$data["juridique"]["logo_1"]?>" />
				<?php		}else{
								echo("&nbsp;");
							} ?>
						</td>
						<td class="w-50" >&nbsp;</td>
						<td class="w-25 text-right" >
				<?php		if(!empty($data["juridique"]["logo_2"])){ ?>
								<img src="../<?=$data["juridique"]["logo_2"]?>" />
				<?php		}else{
								echo("&nbsp;");
							} ?>
						</td>
					</tr>													
				</table>														
			</page_header>
			<table>
				<tr>
					<td class="w-50 tab-label" >												
				<?php	echo("<b>" . $data["juridique"]["nom"] . "</b>");
						if(!empty($data["juridique"]["agence"])){
							echo("<br/><b>" . $data["juridique"]["agence"] . "</b>");
						}
						if(!empty($data["juridique"]["ad1"])){
							echo("<br/><b>" . $data["juridique"]["ad1"] . "</b>");
						}
						if(!empty($data["juridique"]["ad2"])){
							echo("<br/><b>" . $data["juridique"]["ad2"] . "</b>");
						}
						if(!empty($data["juridique"]["ad3"])){
							echo("<br/><b>" . $data["juridique"]["ad3"] . "</b>");
						}
						echo("<br/><b>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"] . "</b>");
						echo("<br/>Tél :" . $data["juridique"]["tel"]);
						if(!empty($data["juridique"]["fax"])){
							echo("<br/>Fax " . $data["juridique"]["fax"]);
						} ?>
					</td>
					<td class="w-50" style="padding-top:100px;" >
				<?php	echo($data["adresse_client"]); 
						echo("<br/><br/>");
						echo("le " . utf8_encode($data["date_doc"])); ?>
					</td>
				</tr>
			</table>
			
			<h1 class="text-center text-strong" >CONVOCATION de <?=$page["sta_prenom"] . " " . $page["sta_nom"]?></h1>
			
			<p>Madame, Monsieur,</p>

			<p>Nous avons le plaisir de vous convier à la session de formation :</p>
			
			<p class="text-center text-strong" ><?=$data["acl_pro_libelle"]?></p>
			<p>
				Cette formation débutera le <?=utf8_encode($page["date_deb"])?> et se terminera le <?=utf8_encode($page["date_fin"])?>, pour une durée de <?=$page["duree"]?> au total. 
				Le planning détaillé de vos journées est décrit ci-dessous.
			</p>
			<table>
				<tr>
					<th class="w-33 text-right" >Planning : </th>
					<td><?=$page["dates"]?></td>
				</tr>
				<tr>
					<th class="w-33 text-right" >Merci d'être présent le : </th>
					<td><?=utf8_encode($page["date_deb"])?> à <?=$page["heure_deb"]?></td>
				</tr>
				<tr>
					<th class="w-33 text-right tab-label" >à l'adresse suivante :</th>
					<td ><?=$data["lieu"]?></td>
				</tr>
				<tr>
					<th class="w-33 text-right" >Horaires : </th>
					<td><?=$page["horaire"]?></td>
				</tr>
				<tr>
					<th class="w-33 text-right" >Informations complémentaires :</th>
					<td><?=$data["reference_sta"]?></td>
				</tr>
			</table>
			<p>
				Restant à votre entière disposition, nous vous prions d'agréer, Madame, Monsieur, nos respectueuses salutations. 
			</p>
		
			<end_last_page end_height="65mm">										
				<table>
					<tr>
						<td class="w-67" >&nbsp;</td>
						<td style="padding-bottom:50px;" >
					<?php	if(!empty($data["juridique"]["resp_ident"])){
								echo($data["juridique"]["resp_ident"]);
							}
							if(!empty($data["juridique"]["resp_fonction"])){
								echo("<br/>" . $data["juridique"]["resp_fonction"]);
							}
							if(!empty($data["juridique"]["resp_signature"])){ 
								echo("<br/>"); ?>
								<img src="../<?=$data["juridique"]["resp_signature"]?>" class="img-responsive" />
					<?php	}?>
						</td>
					</tr>
				</table>
			</end_last_page>
			
			<page_footer>
				<div class="footer" >
					<table>
						<tr>
							<td class="w-15" >
					<?php		if(!empty($data["juridique"]["qualite_1"])){ ?>
									<img src="../<?=$data["juridique"]["qualite_1"]?>" />
					<?php		}else{
									echo("&nbsp;");
								} ?>
							</td>
							<td class="w-70 text-center" ><?=$data["juridique"]["footer"]?></td>
							<td class="w-15 text-right" >
					<?php		if(!empty($data["juridique"]["qualite_2"])){ ?>
									<img src="../<?=$data["juridique"]["qualite_2"]?>" />
					<?php		}else{
									echo("&nbsp;");
								} ?>
							</td>
						</tr>
					</table>
				</div>
			</page_footer>	
		</page>							
<?php 									
	}

	$content=ob_get_clean();
	
	$success=true;
	try{
		$pdf=new HTML2PDF('P','A4','fr');
		$pdf->pdf->SetDisplayMode('fullwidth');	
		$pdf->writeHTML($content);
		if($pdf){
			$pdf->Output($_SERVER["DOCUMENT_ROOT"] . '/Documents/Societes/' . $acc_societe . '/Convocations/' . $data["pdf"] . ".pdf", 'F');					
		}
	}catch(HTML2PDF_exception $e){
		$erreur_convoc="Erreur lors de la génération du fichier PDF";
		$success=false;
	}
	return $success;
}
?>