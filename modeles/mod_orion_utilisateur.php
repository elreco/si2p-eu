<?php

// RETOURNE LA TABLE Clients_Infos qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_utilisateurs(){

	global $Conn;
	
	$result=array();
	
	$result[0]=array(
		"identite" => "",
		"responsable" => 0
	);
	
	$sql="SELECT uti_id,uti_nom,uti_prenom,uti_responsable FROM Utilisateurs ORDER BY uti_id";
	$req=$Conn->prepare($sql);
	$req->execute();
	$utilisateurs = $req->fetchAll();
	if(!empty($utilisateurs)){
		foreach($utilisateurs as $u){
			$result[$u["uti_id"]]=array(
				"identite" => $u["uti_prenom"] . " " .$u["uti_nom"],
				"responsable" => 0
			);
		}
	}

    return $result;
}
