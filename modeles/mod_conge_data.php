<?php


setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


function conge_data($conge_id){

	$data=array();
	
	global $acc_societe;
	global $acc_agence;
	global $acc_utilisateur;
	global $ConnSoc;
	global $Conn;

    $sql="SELECT * FROM Conges WHERE con_id=" . $conge_id;
	$req=$Conn->query($sql);
	$data=$req->fetch();
	if(!empty($data)){
		
		
        $data["juridique"]=get_juridique($data['con_societe'],$data["con_agence"]);
        
        $sql="SELECT uti_nom,uti_prenom FROM utilisateurs WHERE uti_id=" . $data['con_responsable'];
        $req=$Conn->query($sql);
        $data['responsable']=$req->fetch();

        if(!empty($data['con_resp_po'])){
            $sql="SELECT uti_nom,uti_prenom FROM utilisateurs WHERE uti_id=" . $data['con_resp_po_uti'];
            $req=$Conn->query($sql);
            $data['po']=$req->fetch();
        }
        if(!empty($data['con_annul'])){
            $sql="SELECT uti_nom,uti_prenom FROM utilisateurs WHERE uti_id=" . $data['con_annul_uti'];
            $req=$Conn->query($sql);
            $data['annul']=$req->fetch();
        }
        $req = $Conn->prepare("SELECT * FROM conges_categories");
    $req->execute();
    $conges_categories_special = $req->fetchAll();
    foreach($conges_categories_special as $c){
        $data['categories'][$c['cca_id']] = $c['cca_libelle'];
    }
	
	}
    
	if(empty($erreur_txt)){
		return $data;
	}else{
		return $erreur_txt;
	}
}
?>