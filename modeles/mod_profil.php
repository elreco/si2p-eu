<?php

	// MODELE
	
	// PARAMETRE FIGE
	
	$base_ref_profil;
	$base_ref_profil=array(
		1=>"Agence",
		2=>"Groupe",
		3=>"Client",
		4=>"Fournisseur",
		5=>"Stagiaire",
	);
	
	// FONCTION GET
	
	// génére la liste des types de profils pour injections dans un <select>
	
	function get_ref_profil_select(){
		
		global $base_ref_profil;	
		
		$ref_profil_select="";	
		
		foreach($base_ref_profil as $cle => $val){
			$ref_profil_select=$ref_profil_select . "<option value='" . $cle . "' >" . $val . "</option>";
		}
		return $ref_profil_select;
			
	}
	
	
	
	function get_profil_select($valeur_select){

		global $Conn;

		$profil_select="";
		$req=$Conn->query("SELECT * FROM Profils ORDER BY pro_libelle");
		while ($donnees = $req->fetch())
		{	if($valeur_select==$donnees["pro_id"]){
				$profil_select=$profil_select . "<option value='" . $donnees["pro_id"] . "' selected >" . $donnees["pro_libelle"] . "</option>";
			}else{
				$profil_select=$profil_select . "<option value='" . $donnees["pro_id"] . "' >" . $donnees["pro_libelle"] . "</option>";	
			}
			
		};
		return $profil_select;

	}
	
	// Retourne les infos liés à un profil donnée
	function get_profil(){
		
		global $Conn;

		$req=$Conn->query("SELECT pro_id,pro_libelle FROM profils ORDER BY pro_libelle");
		$profil = $req->fetchAll();
		return $profil;
	}
	
	// Retourne un liste de profils
	function get_profils($pro_ref = ""){
		
		global $Conn;
		
		$sql="SELECT pro_id,pro_libelle FROM profils";
		$sql=$sql . " ORDER BY pro_libelle";
		$req=$Conn->query($sql);
		$profils = $req->fetchAll();
		return $profils;
	}
	
	function get_profil_nom($id){
		
		global $Conn;
		
		if($id>0){
			$req = $Conn->prepare("SELECT pro_libelle FROM Profils WHERE pro_id=:pro_id");
			$req->bindParam(':pro_id', $id);
			$req->execute();
			$donnees = $req->fetch();
			return $donnees["pro_libelle"];			
		}else{
			return "";
		};
	}
	
	function insert_profil($pro_libelle,$pro_ref){
		
		global $Conn;		
		$req = $Conn->prepare("INSERT INTO profils (pro_libelle, pro_ref) VALUES (:pro_libelle, :pro_ref)");
		$req->bindParam(':pro_libelle', $pro_libelle);
		$req->bindParam(':pro_ref', $pro_ref);
		$req->execute();
	
	}
	
	function update_profil($pro_id,$pro_libelle,$pro_ref){
		
		global $Conn;	
		$req = $Conn->prepare("UPDATE profils SET pro_libelle=:pro_libelle, pro_ref=:pro_ref WHERE pro_id=:pro_id");
		$req->bindParam(':pro_id', $pro_id);
		$req->bindParam(':pro_libelle', $pro_libelle);
		$req->bindParam(':pro_ref', $pro_ref);
		$req->execute();
			
	}
	
	
?>	
