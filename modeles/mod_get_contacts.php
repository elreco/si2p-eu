<?php


function get_contacts($ref,$ref_id,$adresse,$adr_lien){
		
	global $Conn;	
	
	$req_sql="SELECT * FROM contacts";
	if($adresse>0){
		$req_sql.=" LEFT OUTER JOIN Adresses_Contacts ON (contacts.con_id=Adresses_Contacts.aco_contact)";
	}
	if($ref>0){
		$req_sql.=" WHERE con_ref = :ref";
		if($ref_id>0){
			$req_sql.=" AND con_ref_id = :ref_id";
		}
	}
	if($adresse>0){
		if($adr_lien==1){
			// stric seulement les contacts du lien à l'adresse
			$req_sql.=" AND aco_adresse=" . $adresse;
		}else{
			$req_sql.=" AND (aco_adresse=" . $adresse . " OR ISNULL(aco_adresse))";
		}
	}
	$req_sql.=" ORDER BY con_nom,con_prenom;";

	$req=$Conn->prepare($req_sql);
	if($ref>0){
		$req->bindParam(":ref",$ref);
		if($ref_id>0){
			$req->bindParam(":ref_id",$ref_id);
		}
	}
	try{
		$req->execute();
		$contacts = $req->fetchAll();
		return $contacts;
	}Catch(Exception $e){
		return $e->getMessage();
	}
}
?>