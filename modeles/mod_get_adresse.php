<?php

function get_adresse($adresse){
	
	global $Conn;	
	
	$sql="SELECT * FROM adresses WHERE adr_id=:adresse;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":adresse",$adresse);
	$req->execute();
	$d_adresse=$req->fetch();
	return $d_adresse;
}

?>