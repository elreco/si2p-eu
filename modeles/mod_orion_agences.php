<?php

// RETOURNE LA TABLE Clients_Sous_Categories qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_agences(){

	global $Conn;
	
	$result[0]=array(
		"age_nom" => "",
	);
	
	$sql="SELECT age_id,age_nom FROM Agences ORDER BY age_id";
	$req=$Conn->prepare($sql);
	$req->execute();
	$agences = $req->fetchAll();
	if(!empty($agences)){
		foreach($agences as $age){
			$result[$age["age_id"]]=array(
				"age_nom" => $age["age_nom"],
			);
		}
	}

    return $result;
}
