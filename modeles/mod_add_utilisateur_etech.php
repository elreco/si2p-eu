<?php
// quand c'est un nouvel utilisateur
function add_utilisateur_etech($utilisateur,$profil, $societe, $agence){

	global $Conn;
	
	if(!empty($utilisateur) AND !empty($profil) AND !empty($societe)){
			
			// ON SUPP LES DOCS NON ACCESSIBLE AU PORFIL
			$utis[0]['uti_id'] =  $utilisateur;
			/*$req=$Conn->query("SELECT doc_id,dpr_profil FROM Documents 
			LEFT OUTER JOIN Documents_Profils ON (Documents.doc_id=Documents_Profils.dpr_document AND Documents_Profils.dpr_profil=" . $profil . ")
			LEFT OUTER JOIN Documents_Utilisateurs ON (Documents_Utilisateurs.dut_document=Documents_Profils.dpr_document AND Documents_Profils.dpr_profil=" . $profil . ")
			WHERE dut_utilisateur=" . $utilisateur . " AND ISNULL(drp_profil);");
			$req->execute();
			$documents = $req->fetchAll();*/
			// ON RECHERCHE TOUS LES DOCUMENTS   
			// selectionner tous les documents du profil
			$req=$Conn->query("SELECT DISTINCT doc_id, dpr_document, dpr_obligatoire, doc_url, dso_document FROM documents 
				LEFT JOIN Documents_Profils ON (documents.doc_id = Documents_Profils.dpr_document) 
				LEFT JOIN Documents_Societes ON (documents.doc_id = Documents_Societes.dso_document)  
			 WHERE dpr_profil = " . $profil . " AND dso_societe = " . $societe . " AND dso_agence = " . $agence);
			$req->execute();
			$documents = $req->fetchAll();
						// POUR TOUS LES DOCUMENTS, ACTUALISER LE PROFIL
			foreach($documents as $d){
				
				try{
					$req=$Conn->query("INSERT INTO Documents_utilisateurs (dut_utilisateur, dut_document, dut_obligatoire) VALUES(" . $utilisateur . ", " . $d['dpr_document'] . "," . $d['dpr_obligatoire'] . ")");
				}Catch(Exception $e){
					echo $e->getMessage();
					die();
				}
				
				// on n'oublie pas de set l'affichage des dossiers
				set_affichage_dossier($utis, $d['doc_url']);
			}
			
		
			
	}else{
		return false;
	}
}
?>