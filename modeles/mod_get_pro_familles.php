<?php

// RETOURNE LES FAMILLES DE PRODUITS
// AUTEUR FG

function get_pro_familles(){

	global $Conn;
	
	$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles";
	$sql.=" ORDER BY pfa_libelle";
	$req = $Conn->prepare($sql);
	$req->execute();
	$familles = $req->fetchAll();

    return $familles;
}
