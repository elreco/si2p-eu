<?php

// GENERE DES ATTESTATIONS EN PDF
require_once("../vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;

function model_action_attestation_pdf($data){

	global $acc_societe;
	
	$erreur="";
	ob_start(); ?>
	
	<style type="text/css" >
<?php	require("../assets/skin/si2p/css/orion_pdf.css"); ?>

		.stagiaire{
				list-style:none;
				text-align:center;
				font-size:12pt;
				color:#e31936;
			}
			.head-section{
				padding-top:50px;
			}
			.foot-section{
				padding-bottom:50px;
			}
	</style>
	
<?php	
	foreach($data["pages"] as $p => $page){
		
		if(!empty($page["stagiaires"])){
		

			/*echo("<pre>");
				print_r($page);
			echo("</pre>"); */ ?>

			<page backtop="55mm" backleft="0mm" backright="0mm" backbottom="15mm" >
				<page_header>
					<div class="w-100" >
						<img src="../<?=$data["juridique"]["logo_1"]?>" />
					</div>
					<table class="w-100" >
						<tr>
							<td class="w-50" >
					<?php		echo("<b>" . $data["juridique"]["nom"] . "</b>");
								if(!empty($data["juridique"]["agence"])){
									echo("<br/><b>" . $data["juridique"]["agence"] . "</b>");
								};
								if(!empty($data["juridique"]["ad1"])){
									echo("<br/>" . $data["juridique"]["ad1"]);
								};
								if(!empty($data["juridique"]["ad2"])){
									echo("<br/>" . $data["juridique"]["ad2"]);
								};
								if(!empty($data["juridique"]["ad3"])){
									echo("<br/>" . $data["juridique"]["ad3"]);
								};
								echo("<br/>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"]); 
								
								if(!empty($data["juridique"]["tel"])){
									echo("<br/>Tél : " . $data["juridique"]["tel"]);
								};																		
								if(!empty($data["juridique"]["fax"])){
									echo("<br/>Fax : " . $data["juridique"]["fax"]);
								};
								if(!empty($data["juridique"]["site"])){
									echo("<br/>" . $data["juridique"]["site"]);
								};
								
								?>
							</td>
							
							<td class="w-50 pt50" >
					<?php		echo($data["adresse_client"]); ?>
							</td>																
						</tr>													
					</table>
					<div class="text-center" >
						<h1 class="text-center mb25" >ATTESTATION DE FORMATION</h1>													
					</div>																
				</page_header>
				<page_footer>
					<div class="foot-section" >
						<p>Fait à <?=$data["juridique"]["ville"]?>, le <?=utf8_encode($data["date_doc"])?></p>
						
						<table>
							<tr>
								<td class="w-60" >&nbsp;</td>
								<td>
						<?php		if(!empty($data["juridique"]["resp_signature"])){ 
										echo("<br/>"); ?>
										<img src="../<?=$data["juridique"]["resp_signature"]?>" />
							<?php	}?>
								</td>
							</tr>
						</table>
					</div>
					<div class="footer text-center" ><?=$data["juridique"]["footer"]?></div>
				</page_footer>

				<div class="head-section" >
					<p>
						Je soussigné,
				<?php	if(!empty($data["juridique"]["resp_ident"])){
							echo(" " . $data["juridique"]["resp_ident"] . ",");
						}
						if(!empty($data["juridique"]["resp_fonction"])){
							echo(" " . $data["juridique"]["resp_fonction"] . ",");
						} ?> atteste que 
					</p>
				</div>
			
				<div class="section" >
					<table>
						<tr>
		<?php				$nbBcl=ceil(count($page["stagiaires"])/8);
							if($nbBcl==1){
								$class="w-100";
							}elseif($nbBcl==2){
								$class="w-50";
							}else{
								$class="w-33";
							} 
							$nb_sta_col=intval(count($page["stagiaires"])/$nbBcl);
							if( count($page["stagiaires"]) % $nbBcl>0){
								$nb_sta_col++;
							}
							$num_sta=0;
							for($bcl=1;$bcl<=$nbBcl;$bcl++){ 
								$fin_sta=$nb_sta_col*$bcl; ?>																
								<td class="<?=$class?>" >
<?php								for($num_sta;$num_sta<$fin_sta;$num_sta++){																			
										echo("<div class='stagiaire' >" . $page["stagiaires"][$num_sta]["sta_nom"] . " " . $page["stagiaires"][$num_sta]["sta_prenom"] . "</div>");
										if($num_sta>=count($page["stagiaires"])-1){
											break;
										}
									} ?>
								</td>
<?php						}	?>
						</tr>
					</table>
				</div>
			

			
				<div class="section" >ont suivi la formation suivante :</div>
			
			
			
			
				<table class="w-100" >
					<tr>
						<td class="w-33 text-right pt5" ><b>Intitulé</b> :</td>
						<td class="w-67 text-strong pt5" ><?=$data["acl_pro_libelle"]?></td>
					</tr>
					<tr>
						<td class="w-33 text-right pt5"><b>Date</b> :</td>
						<td class="w-67 text-strong pt5" >
				<?php		if(!empty($page["dates"])){
								echo(date_periode_lib($page["dates"],"%A %d %B %Y"));
							} ?>										
						</td>
					</tr>
					<tr>
						<td class="w-33 text-right pt5" ><b>Durée</b> :</td>
						<td class="w-67 text-strong pt5" >
				<?php		echo($page["duree"]); ?>																
						</td>
					</tr>
					<tr>
						<td class="w-33 text-right pt5" ><b>Formateur</b> :</td>
						<td class="w-67 text-strong pt5" >
				<?php		if(!empty($page["formateurs"])){
								$liste_form=implode($page["formateurs"],",");
								echo($liste_form);
							} ?>
						</td>
					</tr>
		<?php		if(!empty($page["testeurs"])){	 ?>
						<tr>
							<td class="w-33 text-right pt5" ><b>Testeurs</b> :</td>
							<td class="w-67 text-strong pt5" >
				<?php			$liste_form=implode($page["testeurs"],",");
								echo($liste_form); ?>
							</td>
						</tr>
		<?php		} ?>
					<tr>
						<td class="w-33 text-right pt5" ><b>Lieu</b> :</td>
						<td class="w-67 text-strong pt5" ><?=$data["act_adr_ville"] . " (" . substr ($data["act_adr_cp"],0,2) . ")"?></td>
					</tr>
				</table>
				<div class="section" >
					<p>Fait pour servir et valoir ce que de droit.</p>
				</div>
			</page>
			
<?php	} 
	} 

	$content=ob_get_clean();
	/*echo($content);
	die();*/
	try{
		$pdf=new HTML2PDF('P','A4','fr');
		$pdf->pdf->SetDisplayMode('fullwidth');
		//echo($content);
		$pdf->writeHTML($content);
		if($pdf){
			$pdf->Output($_SERVER["DOCUMENT_ROOT"] . '/Documents/Societes/' . $acc_societe . '/Attestations/' . $data["pdf"] . '.pdf', 'F');					
		}
	}catch(HTML2PDF_exception $e){
		$erreur="Erreur lors de la génération du fichier PDF";
	}
	
	if(!empty($erreur)){
		return false;
	}else{
		return true;
	}
}
?>