<?php

	// MODELE 
// fonction permettant de supprimer l'accès aux dossiers
function delete_affichage_dossier($utilisateurs, $url){
	
	global $Conn;

	$_dossiers = explode('/', $url);

	$url = "";
	foreach($_dossiers as $k=>$d){
		if(!empty($d)){
			
			if($k > 0){
				$req=$Conn->prepare("SELECT doc_id,doc_url,doc_nom_aff FROM documents WHERE doc_url LIKE :doc_url AND doc_dossier = 1 AND doc_nom_aff LIKE :doc_nom_aff");
				$req->bindParam(':doc_url', $url);
				$req->bindParam(':doc_nom_aff', $d);
				$req->execute();
				$_dos = $req->fetch();
				$doss[]= $_dos;
				
			}
			
			$url = $url . $d . "/";
			
			
		}
	}

	if(!empty($doss)){
		
		krsort($doss);

		foreach($utilisateurs as $no_auth){
		
			// ON CONTROLE QUE L'ACCES AU DOSSIER PARENT EST TOUJOURS NECESSAIRE
			// rangmenet inverse du table doss

			foreach($doss as $d){

				$url = $d['doc_url'] . $d['doc_nom_aff'] . "/";				
				$req=$Conn->prepare("SELECT * FROM documents 
					LEFT JOIN documents_utilisateurs ON (documents.doc_id=documents_utilisateurs.dut_document) 
					 WHERE doc_url = :doc_url AND dut_utilisateur = " . $no_auth['uti_id']);
				$req->bindParam(':doc_url', $url);
				//var_dump($url);
				
				$req->execute();
				$_uti = $req->fetch();	
				if(empty($_uti)){
					//echo("DELETE");
					delete_document_utilisateur($no_auth['uti_id'],$d['doc_id']);
					
				}else{
					break;
				}
			}
		}
	}

}

function set_affichage_dossier($utilisateurs, $url){

	global $Conn;

	$_dossiers = explode('/', $url);

	$url = "";

	foreach($_dossiers as $k=>$d){
		if(!empty($d)){
			
			if($k > 0){				
				$req=$Conn->prepare("SELECT doc_id, doc_url, doc_nom_aff FROM documents WHERE doc_url LIKE :doc_url AND doc_dossier = 1 AND doc_nom_aff LIKE :doc_nom_aff");
				$req->bindParam(':doc_url', $url);
				$req->bindParam(':doc_nom_aff', $d);
				$req->execute();
				$_dos = $req->fetch();
				if(!empty($_dos)){
					$doss[]= $_dos;
				}
				
			}
	
			$url = $url . $d . "/";

		}
	}
	foreach($utilisateurs as $auth){
		
		if(!empty($doss)){
				
			// UTI DOIT AVOIR ACCES AU DOSSIER PARENTS

			foreach($doss as $d){
				
		
				if(empty(get_document_utilisateur($auth['uti_id'], $d['doc_id']))){
					insert_document_utilisateur($auth['uti_id'], 0, $d['doc_id']);
				}
				
			}
			
			// ON CONTROLE QUE L'UTILISATEUR A ACCES AU DOSSIER PARENT
					
			foreach($doss as $d){
				
				$req=$Conn->prepare("SELECT dut_utilisateur FROM documents_utilisateurs WHERE dut_utilisateur = " . $auth['uti_id'] . " AND dut_document=" . $d['doc_id']);
				$req->execute();
				$_uti = $req->fetch();

				if(empty($_uti)){
					// insert le dossier
					$req=$Conn->prepare("INSERT INTO documents_utilisateurs (dut_utilisateur, dut_utilisateur) VALUES (" . $auth['uti_id'] .  ", " . $d['doc_id'] . ")");
					$req->execute();

				}
			}
			
			
		}
	}

}

function get_document($id){

	global $Conn;	

	$req_sql="SELECT * FROM documents WHERE doc_id=" . $id;
	$req=$Conn->query($req_sql);
	$get_doc = $req->fetch();	
	return $get_doc;
}

function get_documents($dos_url){

	global $Conn;	


	$sql = "SELECT DISTINCT * FROM documents";
	$mil="";
	if(!empty($_SESSION['acces']['acc_liste_societe'])){
		foreach($_SESSION['acces']['acc_liste_societe'] as $cle=>$s){
			if(!empty($societe_liste)){
				$societe_liste = $societe_liste . "," . substr($cle, 0, 1);
			}else{
				$societe_liste = substr($cle, 0, 1);
			}
		}

	}
	if(!$_SESSION["acces"]["acc_droits"][5]){
		$sql.=" LEFT JOIN documents_utilisateurs ON (documents.doc_id=documents_utilisateurs.dut_document)";
		if(!$_SESSION["acces"]["acc_droits"][1]){
			$mil .=" AND dut_utilisateur = " . $_SESSION['acces']['acc_ref_id'];
		}else{
			$mil .=" AND (dut_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " OR doc_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " )";
		}
	}
	
	$mil.=" AND doc_archive = 0 AND doc_url LIKE :dos_url";
	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	
	$sql.= " ORDER BY doc_dossier DESC, doc_nom_aff ASC";
	
	$req=$Conn->prepare($sql);

	$req->bindParam("dos_url", $dos_url);
	$req->execute();
	$get_doc = $req->fetchAll();	
	
	return $get_doc;
}

function get_documents_like($dos_url, $doc_id){

	global $Conn;	

	 $dos_url =  $dos_url . "%";
	$sql = "SELECT * FROM documents WHERE doc_url LIKE :dos_url AND doc_archive = 0 AND doc_id != $doc_id ORDER BY doc_dossier DESC";

	
	$req=$Conn->prepare($sql);
	$req->bindParam("dos_url", $dos_url);

	$req->execute();
	$get_doc = $req->fetchAll();	

	return $get_doc;
}

function get_documents_noms(){

	global $Conn;	


	$sql = "SELECT doc_id, doc_nom_aff, doc_dossier, doc_url FROM documents WHERE doc_dossier = 1 ORDER BY doc_url DESC";


	$req=$Conn->prepare($sql);


	$req->execute();
	$get_doc = $req->fetchAll();	

	return $get_doc;
}

function get_documents_tout(){

	global $Conn;	

	$req=$Conn->prepare("SELECT * FROM documents ORDER BY doc_dossier DESC");


	$req->execute();
	$get_doc = $req->fetchAll();	

	return $get_doc;
}

function get_documents2(){

	global $Conn;	

	$req=$Conn->prepare("SELECT DISTINCT doc_url, doc_nom_aff FROM documents WHERE doc_dossier = 1");


	$req->execute();
	$get_doc = $req->fetchAll();	

	return $get_doc;
}

function get_document_histo($id){

	global $Conn;	

	$req=$Conn->prepare("SELECT * FROM documents_histo WHERE dhi_document=" . $id . " ORDER BY dhi_date_archive DESC");


	$req->execute();
	$get_doc = $req->fetchAll();	

	return $get_doc;
}

function get_document_url($dos_url,$dos_nom){

	global $Conn;	

	$req=$Conn->prepare("SELECT * FROM documents WHERE doc_dossier = 1 AND doc_url LIKE :doc_url AND doc_nom_aff LIKE :dos_nom");

	$req->bindParam(':doc_url', $dos_url);
	$req->bindParam(':dos_nom', $dos_nom);

	$req->execute();
	$get_doc = $req->fetch();

	return $get_doc;
}

function check_dos($dos_url, $doc_nom){

	global $Conn;	

	$req=$Conn->prepare("SELECT * FROM documents WHERE doc_dossier = 1 AND doc_url = :doc_url AND doc_nom_aff = :doc_nom");

	$req->bindParam(':doc_url', $dos_url);
	$req->bindParam(':doc_nom', $doc_nom);
	$req->execute();
	$get_doc = $req->fetch();

	return $get_doc;
}

function check_dos_mod($dos_url, $doc_nom, $doc_id){

	global $Conn;	

	$req=$Conn->prepare("SELECT * FROM documents WHERE doc_dossier = 1 AND doc_url = :doc_url AND doc_nom_aff = :doc_nom AND doc_id != :doc_id");

	$req->bindParam(':doc_url', $dos_url);
	$req->bindParam(':doc_nom', $doc_nom);
	$req->bindParam(':doc_id', $doc_id);
	$req->execute();
	$get_doc = $req->fetch();

	return $get_doc;
}

function check_doc_mod($dos_url, $doc_nom, $doc_id){

	global $Conn;	

	$req=$Conn->prepare("SELECT * FROM documents WHERE doc_dossier = 0 AND doc_url = :doc_url AND doc_nom_aff = :doc_nom AND doc_id != :doc_id");

	$req->bindParam(':doc_url', $dos_url);
	$req->bindParam(':doc_nom', $doc_nom);
	$req->bindParam(':doc_id', $doc_id);
	$req->execute();
	$get_doc = $req->fetch();

	return $get_doc;
}

function get_document_utilisateur($uti, $document){

	global $Conn;	

	$req=$Conn->prepare("SELECT * FROM documents_utilisateurs WHERE dut_utilisateur = :uti AND dut_document = :document");

	$req->bindParam(':uti', $uti);
	$req->bindParam(':document', $document);
	$req->execute();
	$get_doc = $req->fetch();

	return $get_doc;
}

function get_document_utilisateur_obligatoire($uti, $obligatoire, $document){

	global $Conn;	

	$req=$Conn->prepare("SELECT * FROM documents_utilisateurs WHERE dut_utilisateur = :uti AND dut_document = :document AND dut_obligatoire = :dut_obligatoire");

	$req->bindParam(':uti', $uti);
	$req->bindParam(':document', $document);
	$req->bindParam(':dut_obligatoire', $obligatoire);
	$req->execute();
	$get_doc = $req->fetch();

	return $get_doc;
}

function update_document_utilisateur($uti, $obligatoire, $document){

	global $Conn;

	$req = $Conn->prepare("UPDATE documents_utilisateurs SET dut_obligatoire = :obligatoire WHERE dut_document = :document AND dut_utilisateur = :uti");
	$req->bindParam(':uti', $uti);
	$req->bindParam(':obligatoire', $obligatoire);
	$req->bindParam(':document', $document);
	$req->execute();

}

function insert_document($doc_nom, $doc_nom_aff,$doc_url, $doc_utilisateur, $doc_ext, $doc_date_creation, $doc_dossier){
	global $Conn;

	$doc_id=0;

	$req = $Conn->prepare("INSERT INTO documents (doc_nom, doc_nom_aff,doc_url, doc_utilisateur, doc_ext, doc_date_creation, doc_dossier) VALUES (:doc_nom, :doc_nom_aff,:doc_url, :doc_utilisateur, :doc_ext, :doc_date_creation, :doc_dossier)");
	$req->bindParam(':doc_nom', $doc_nom);
	$req->bindParam(':doc_nom_aff', $doc_nom_aff);
	$req->bindParam(':doc_utilisateur', $doc_utilisateur);
	$req->bindParam(':doc_ext', $doc_ext);
	$req->bindParam(':doc_date_creation', $doc_date_creation);
	$req->bindParam(':doc_dossier', $doc_dossier);
	$req->bindParam(':doc_url', $doc_url);
	$req->execute();
	$doc_id=$Conn->lastInsertId();

	return $doc_id;
}

function insert_document_histo($dhi_document, $dhi_nom, $dhi_archive,$dhi_utilisateur){

	global $Conn;

	$doc_id=0;

	$req = $Conn->prepare("INSERT INTO documents_histo (dhi_document, dhi_nom,dhi_date_archive,dhi_utilisateur) VALUES (:dhi_document, :dhi_nom, :dhi_archive,:dhi_utilisateur)");
	$req->bindParam(':dhi_document', $dhi_document);
	$req->bindParam(':dhi_nom', $dhi_nom);
	$req->bindParam(':dhi_archive', $dhi_archive);
	$req->bindParam(':dhi_utilisateur', $dhi_utilisateur);
	$req->execute();
	$doc_id=$Conn->lastInsertId();

	return $doc_id;
}

function update_document($doc_id, $doc_nom_aff,$doc_url, $doc_dossier, $doc_mot_cle, $doc_descriptif, $doc_type, $doc_client, $doc_service, $doc_societe, $doc_archive){
	global $Conn;


	$req = $Conn->prepare("UPDATE documents SET doc_nom_aff = :doc_nom_aff, doc_url = :doc_url, doc_dossier = :doc_dossier, doc_mot_cle = :doc_mot_cle, doc_descriptif=:doc_descriptif, doc_type = :doc_type, doc_client = :doc_client, doc_service =:doc_service, doc_societe = :doc_societe, doc_archive=:doc_archive WHERE doc_id=:doc_id");

	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':doc_nom_aff', $doc_nom_aff);
	$req->bindParam(':doc_url', $doc_url);
	$req->bindParam(':doc_dossier', $doc_dossier);
	$req->bindParam(':doc_mot_cle', $doc_mot_cle);
	$req->bindParam(':doc_descriptif', $doc_descriptif);
	$req->bindParam(':doc_type', $doc_type);
	$req->bindParam(':doc_client', $doc_client);
	$req->bindParam(':doc_service', $doc_service);
	$req->bindParam(':doc_societe', $doc_societe);
	$req->bindParam(':doc_archive', $doc_archive);
	$req->execute();
}
function update_document_coller($doc_id, $doc_url){
	global $Conn;


	$req = $Conn->prepare("UPDATE documents SET doc_url = :doc_url WHERE doc_id=:doc_id");

	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':doc_url', $doc_url);

	$req->execute();
}
function update_document_dossier($doc_id, $doc_url){
	global $Conn;


	$req = $Conn->prepare("UPDATE documents SET doc_url = :doc_url WHERE doc_id=:doc_id");

	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':doc_url', $doc_url);

	$req->execute();
}

function update_document_mod($doc_id, $doc_nom, $doc_utilisateur, $doc_extension,  $doc_date_creation){
	global $Conn;
	
		$req = $Conn->prepare("UPDATE documents SET doc_nom = :doc_nom, doc_utilisateur = :doc_utilisateur,doc_ext = :doc_extension, doc_date_creation = :doc_date_creation WHERE doc_id=:doc_id");

		$req->bindParam(':doc_id', $doc_id);
		$req->bindParam(':doc_nom', $doc_nom);
		$req->bindParam(':doc_utilisateur', $doc_utilisateur);
		$req->bindParam(':doc_extension', $doc_extension);
		$req->bindParam(':doc_date_creation', $doc_date_creation);
		$req->execute();
	
}

function insert_dossier($doc_nom_aff,$doc_url, $doc_utilisateur, $doc_date_creation, $doc_dossier){
	global $Conn;

	$dos_id=0;

	$req = $Conn->prepare("INSERT INTO documents (doc_nom_aff,doc_url, doc_utilisateur, doc_date_creation, doc_dossier) VALUES (:doc_nom_aff,:doc_url, :doc_utilisateur, :doc_date_creation, :doc_dossier)");
	$req->bindParam(':doc_nom_aff', $doc_nom_aff);
	$req->bindParam(':doc_utilisateur', $doc_utilisateur);
	$req->bindParam(':doc_date_creation', $doc_date_creation);
	$req->bindParam(':doc_dossier', $doc_dossier);
	$req->bindParam(':doc_url', $doc_url);
	$req->execute();
	$dos_id=$Conn->lastInsertId();

	return $dos_id;
}

function update_dossier($doc_id, $doc_nom_aff,$doc_url, $doc_dossier){
	global $Conn;

	$req = $Conn->prepare("UPDATE documents SET doc_id = :doc_id, doc_nom_aff = :doc_nom_aff,doc_url = :doc_url, doc_dossier = :doc_dossier WHERE doc_id = :doc_id");
	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':doc_nom_aff', $doc_nom_aff);
	$req->bindParam(':doc_dossier', $doc_dossier);
	$req->bindParam(':doc_url', $doc_url);
	$req->execute();

}

function insert_document_profil($pro_id, $doc_id, $obligatoire){

	global $Conn;

	$req = $Conn->prepare("INSERT INTO documents_profils (dpr_profil, dpr_document, dpr_obligatoire) VALUES (:pro_id, :doc_id, :obligatoire)");
	$req->bindParam(':pro_id', $pro_id);
	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':obligatoire', $obligatoire);
	$req->execute();

}

function update_document_profil($pro_id, $doc_id, $obligatoire){

	global $Conn;

	$req = $Conn->prepare("UPDATE documents_profils SET dpr_obligatoire = :obligatoire WHERE dpr_profil = :pro_id AND dpr_document = :doc_id");
	$req->bindParam(':pro_id', $pro_id);
	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':obligatoire', $obligatoire);
	$req->execute();

}

function insert_document_utilisateur($uti, $obligatoire, $document){

	global $Conn;

	$req = $Conn->prepare("INSERT INTO documents_utilisateurs (dut_utilisateur, dut_obligatoire, dut_document) VALUES (:uti, :obligatoire, :document)");
	$req->bindParam(':uti', $uti);
	$req->bindParam(':obligatoire', $obligatoire);
	$req->bindParam(':document', $document);
	$req->execute();

}


function delete_document_utilisateur($uti, $document){

	global $Conn;

	$req = $Conn->prepare("DELETE FROM documents_utilisateurs WHERE dut_utilisateur = :uti AND dut_document = :document");
	$req->bindParam(':uti', $uti);
	$req->bindParam(':document', $document);
	$req->execute();

}
function get_document_type($selected){
	global $Conn;

	$client_select="";

	$sql="SELECT dty_id,dty_libelle  FROM documents_types";

	$req=$Conn->query($sql);
	while ($donnees = $req->fetch())
	{
		if($selected == 0){
			$client_select=$client_select . "<option value='" . $donnees["dty_id"] . "' >" . $donnees["dty_libelle"] . "</option>";
		}else{
			if($donnees['dty_id'] == $selected){
				$client_select=$client_select . "<option selected value='" . $donnees["dty_id"] . "' >" . $donnees["dty_libelle"] . "</option>";
			}else{
				$client_select=$client_select . "<option value='" . $donnees["dty_id"] . "' >" . $donnees["dty_libelle"] . "</option>";
			}

		}
	}
	return $client_select;
}
function delete_document($doc_id){

	global $Conn;
	$get_doc = get_document($doc_id);
	$req = $Conn->prepare("DELETE FROM documents WHERE doc_id =" . $doc_id);
	$req->execute();

	if($get_doc['doc_dossier'] ==1){	
		
		$docs = get_documents($get_doc['doc_url'] . $get_doc['doc_nom_aff'] . "/");

		foreach($docs as $d){

			if(file_exists('documents/etech/' . $d['doc_nom'])){
				$lol = unlink('documents/etech/' . $d['doc_nom']);
			}else{
				die("impossible de delete");
			}
			
			$req = $Conn->prepare("DELETE FROM documents WHERE doc_id =" . $d['doc_id']);
			$req->execute();
			$req = $Conn->prepare("DELETE FROM documents_histo WHERE dhi_document = ". $d['doc_id']);
			$req->execute();
			$req = $Conn->prepare("DELETE FROM documents_profils WHERE dpr_document = ". $d['doc_id']);
			$req->execute();
			$req = $Conn->prepare("DELETE FROM documents_societes WHERE dso_document = ". $d['doc_id']);
			$req->execute();
			$req = $Conn->prepare("DELETE FROM documents_utilisateurs WHERE dut_document = ". $d['doc_id']);
			$req->execute();
		}



	}else{

		if(file_exists('documents/etech/' . $get_doc['doc_nom'])){
			$lol = unlink('documents/etech/' . $get_doc['doc_nom']);
		}else{
			die("impossible de delete");
		}
		$req = $Conn->prepare("DELETE FROM documents_histo WHERE dhi_document = ". $doc_id);
		$req->execute();
		$req = $Conn->prepare("DELETE FROM documents_profils WHERE dpr_document = ". $doc_id);
		$req->execute();
		$req = $Conn->prepare("DELETE FROM documents_societes WHERE dso_document = ". $doc_id);
		$req->execute();
		$req = $Conn->prepare("DELETE FROM documents_utilisateurs WHERE dut_document = ". $doc_id);
		$req->execute();
	}
	

}

function insert_document_societe($soc_id, $doc_id, $age_id){

	global $Conn;

	$req = $Conn->prepare("INSERT INTO documents_societes (dso_societe, dso_document, dso_agence) VALUES (:soc_id, :doc_id, :age_id)");
	$req->bindParam(':soc_id', $soc_id);
	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':age_id', $age_id);
	$req->execute();

}
function get_document_profil($pro_id, $doc_id){

	global $Conn;

	$req = $Conn->prepare("SELECT * FROM documents_profils WHERE dpr_document = :doc_id AND dpr_profil = :pro_id");
	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':pro_id', $pro_id);
	$req->execute();

	$get_doc = $req->fetch();

	return $get_doc;

}
function get_document_profil_obligatoire($pro_id, $doc_id,$obligatoire){

	global $Conn;

	$req = $Conn->prepare("SELECT * FROM documents_profils WHERE dpr_document = :doc_id AND dpr_profil = :pro_id AND dpr_obligatoire = :dpr_obligatoire");
	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':pro_id', $pro_id);
	$req->bindParam(':dpr_obligatoire', $obligatoire);
	$req->execute();

	$get_doc = $req->fetch();

	return $get_doc;

}

function delete_document_profil( $pro_id,$doc_id){

	global $Conn;

	$req = $Conn->prepare("DELETE FROM documents_profils WHERE dpr_document = :doc_id AND dpr_profil = :pro_id");
	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':pro_id', $pro_id);
	$req->execute();

}

function get_document_societe( $soc_id, $doc_id, $age_id){

	global $Conn;

	$req = $Conn->prepare("SELECT * FROM documents_societes WHERE dso_document = :doc_id AND dso_societe = :soc_id AND dso_agence = :age_id");
	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':soc_id', $soc_id);
	$req->bindParam(':age_id', $age_id);
	$req->execute();

	$get_doc = $req->fetch();

	return $get_doc;

}

function get_document_produits($doc_id){

	global $Conn;

	$req = $Conn->prepare("SELECT dpr_document, dpr_produit FROM documents_produits WHERE dpr_document = :doc_id");
	$req->bindParam(':doc_id', $doc_id);
	$req->execute();

	$produits = $req->fetchAll();

	$array = array();
	$i = 0;
	foreach($produits as $p){
		$i++;
		$req = $Conn->prepare("SELECT pro_id, pro_code_produit FROM produits WHERE pro_id =" . $p['dpr_produit']);
		$req->execute();

		$code_produit = $req->fetch();

	$array[$i][1] = $code_produit['pro_id'];
	$array[$i][2] = $code_produit['pro_code_produit'];

	}


	return $array;
}

function delete_document_societe( $soc_id,$doc_id, $age_id){

	global $Conn;

	$req = $Conn->prepare("DELETE FROM documents_societes WHERE dso_document = :doc_id AND dso_societe = :soc_id AND dso_agence = :age_id");
	$req->bindParam(':doc_id', $doc_id);
	$req->bindParam(':soc_id', $soc_id);
	$req->bindParam(':age_id', $age_id);
	$req->execute();

}

function delete_document_produits($doc_id){

	global $Conn;
		$req = $Conn->prepare("DELETE FROM documents_produits WHERE dpr_document = :doc_id");
		$req->bindParam(':doc_id', $doc_id);
		$req->execute();

	

}

function insert_document_produits($doc, $field){

	global $Conn;

	$req = $Conn->prepare("INSERT INTO documents_produits (dpr_document, dpr_produit) VALUES (:doc, :field)");
	$req->bindParam(':doc', $doc);
	$req->bindParam(':field', $field);
	$req->execute();

}

?>	
