<?php

// RETOURNE LES CATEGORIES DE PRODUITS 
// $planning bool 0 / 1 permet de filtres les categories dont les produits peuvent être positionné au planning  

// AUTEUR FG

function get_pro_categories($planning = ""){

	global $Conn;
	
	$mil="";
	$sql="SELECT pca_id,pca_libelle,pca_planning FROM Produits_Categories";
	if(!$_SESSION['acces']["acc_droits"][9]){
		$mil=" AND NOT pca_compta";
	}
	if($planning!=""){
		if($planning==1){
			$mil.=" AND pca_planning";	
		}else{
			$mil.=" AND NOT pca_planning";
		}
	}
	if(!empty($mil)){
		$sql.=" WHERE " . substr($mil,5);	
	}
	$sql.=" ORDER BY pca_libelle";
	$req = $Conn->prepare($sql);
	$req->execute();
	$categories = $req->fetchAll();

    return $categories;
}
