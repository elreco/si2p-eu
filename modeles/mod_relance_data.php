<?php


setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


function relance_data($relance){

	$data=array();

	global $acc_societe;
	global $acc_agence;
	global $acc_utilisateur;
	global $ConnSoc;
	global $Conn;

	$sql="SELECT * FROM Relances WHERE rel_id=" . $relance;

	$req=$Conn->query($sql);
	$data=$req->fetch();
	if(!empty($data)){

		// Si lié à un utilisateur
		if(!empty($acc_utilisateur)){

			$sql="SELECT uti_nom, uti_prenom, uti_tel,uti_mobile,uti_fax,uti_mail FROM Utilisateurs WHERE uti_id=" . $acc_utilisateur . ";";
			$req=$Conn->query($sql);
			$utilisateur=$req->fetch();
			if(!empty($utilisateur)){
				$data["rel_uti_tel"]=$utilisateur["uti_tel"];
				$data["rel_uti_mobile"]=$utilisateur["uti_mobile"];
				$data["rel_uti_fax"]=$utilisateur["uti_fax"];
                $data["rel_uti_mail"]=$utilisateur["uti_mail"];
                $data["rel_uti_nom"]=$utilisateur["uti_nom"];
                $data["rel_uti_prenom"]=$utilisateur["uti_prenom"];
			}
		}

		// LE CLIENT

		$req=$Conn->query("SELECT cli_code,cli_reference FROM clients WHERE cli_id=" . $data["rel_client"] . ";");
		$d_client=$req->fetch();
		if(!empty($d_client)){
			$data["rel_cli_code"]=$d_client["cli_code"];
			$data["rel_cli_reference"]=$d_client["cli_reference"];
        }
        $adresse_cli = get_adresses(1,$data["rel_client"],2);
		$data["rel_adresse_facturation"]=$adresse_cli;

		$sql="SELECT * FROM Relances_Factures WHERE rfa_relance=" . $relance;
		$req=$Conn->query($sql);
		$relances=$req->fetchAll();
		foreach($relances as $r){
			$ConnFct = connexion_fct($r['rfa_facture_soc']);
			$sql="SELECT * FROM Factures WHERE fac_id = " . $r['rfa_facture'] . " AND NOT fac_total_ttc=fac_regle AND fac_nature=1";
			$req=$ConnFct->query($sql);
			$data["rel_factures"][$r['rfa_facture_soc']][]=$req->fetch();
		}



		$data["rel_date_texte"]=convert_date_fr_le(date('Y-m-d'));
		// ADRESSE RESEAU
        $data["juridique"]=get_juridique($acc_societe,$acc_agence);

	}

	if(empty($erreur_txt)){
		return $data;
	}else{
		return $erreur_txt;
	}
}
?>
