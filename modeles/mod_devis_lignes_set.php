 <?php
 
 
	function set_devis_lignes($lignes){
		
		global $ConnSoc;

		if(!empty($lignes)){
			
			foreach($lignes as $ligne){
				
				$dli_id=0;
				if(!empty($ligne['dli_id'])){
					$dli_id=intval($ligne['dli_id']);
				}
				
				$dli_devis=0;
				if(!empty($ligne['dli_devis'])){
					$dli_devis=intval($ligne['dli_devis']);
				}
				
				if(!empty($dli_id) AND !empty($dli_devis)){
					
					$sql="UPDATE Devis_Lignes SET ";
					foreach($ligne as $chp => $val){
						if($chp!="dli_id" AND $chp!="dli_devis"){
							$sql.=$chp . "='" . $val . "',";
						}
					}
					$sql=substr($sql, 0, -1);
					$sql.=" WHERE dli_id=" . $dli_id . " AND dli_devis=" . $dli_devis . ";";
					$ConnSoc->query($sql);				
				}else{					
					$erreur="formulaire incomplet!";
					break;
				}
			}
		}
		
		if(!empty($erreur)){
			return $erreur;
		}else{
			return true;
		}
	} 
?>