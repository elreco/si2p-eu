<?php

	// VERIFIE Si UN CLIENT EXISTE DEJA

function check_client($client,$code,$nom,$siren =""){
	
	global $Conn;

	$result=array(
		"doublon_siren_blacklist" => false,
		"doublon_code" => false,
		"doublon_code_soc" => array(),
		"doublon_nom" => false,
		"doublon_nom_soc" => array(),
		"doublon_siren" => false,
		"doublon_siren_soc" => array()
	);
	
	// retourne uniquement la présence de boublon et les societe associé au doublon
	// les conséquence liées à la présence de doublon sont a gérer dans le script d'appel

	
	$sql="SELECT DISTINCT cli_code,cli_nom,cli_siren,cli_blackliste,cso_societe,cso_agence,cso_archive FROM Clients 
	LEFT OUTER JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client)";
	$mil="";
	if(!empty($code)){
		$mil.=" OR cli_code=:code";
	}
	if(!empty($nom)){
		$mil.=" OR cli_nom=:nom";
	}
	if(!empty($siren)){
		$mil.=" OR cli_siren=:siren";
	}
	if(!empty($mil)){
		$sql.=" WHERE (" . substr($mil, 4) . ")";
	}
	if(!empty($client)){
		$sql.=" AND NOT cli_id=:client";
	}
	$req=$Conn->prepare($sql);
	// param
	if(!empty($code)){
		$req->bindParam(":code",$code);
	}
	if(!empty($nom)){
		$req->bindParam(":nom",$nom);
	}
	if(!empty($siren)){
		$req->bindParam(":siren",$siren);
	}
	if(!empty($client)){
		$req->bindParam(":client",$client);
	}
	$req->execute();
	$clients=$req->fetchAll();
	if(!empty($clients)){
		foreach($clients as $client){
			
			if(strcasecmp($client["cli_code"],$code)==0 AND !empty($code)){
				
				$result["doublon_code"]=true;
				$result["doublon_code_soc"][]=array(
					"societe" => $client["cso_societe"],
					"agence" => $client["cso_agence"],
					"archive" => $client["cso_archive"]
				);
			}
			
			if(strcasecmp($client["cli_nom"],$nom)==0 AND !empty($nom)){
				$result["doublon_nom"]=true;
				$result["doublon_nom_soc"][]=array(
					"societe" => $client["cso_societe"],
					"agence" => $client["cso_agence"],
					"archive" => $client["cso_archive"]
				);	
			}
			
			if($client["cli_siren"]==$siren AND !empty($siren)){
				$result["doublon_siren"]=true;
				if($client["cli_blackliste"]){
					$result["doublon_siren_blacklist"]=true;
				}
				$result["doublon_siren_soc"][]=array(
					"societe" => $client["cso_societe"],
					"agence" => $client["cso_agence"],
					"archive" => $client["cso_archive"]
				);	
			}
		}
		
	}
	return $result;
}
?>
