<?php 

// retourne les infos d'un client en tenant compte de l'heritage et de du lieu d'affichage

function get_client_infos($client,$affichage,$famille,$sous_famille,$sous_sous_famille){
	
	global $Conn;
	
	$info_liste="";
	$info_liste_herit="";
	$sql="SELECT cin_id,cin_heritage FROM Clients_Infos";
	if($affichage==1){
		$sql.=" WHERE cin_action=1";	
	}elseif($affichage==2){
		$sql.=" WHERE cin_facture=1";	
	}elseif($affichage==3){
		// info a destination des formateur
		$sql.=" WHERE cin_id IN (2,3)";	
	}
	$sql.=" ORDER BY cin_libelle;";
	$req=$Conn->query($sql);
	$d_info_type=$req->fetchAll();
	if(!empty($d_info_type)){
		foreach($d_info_type as $dit){
			$info_liste.=$dit["cin_id"] . ",";
			if($dit["cin_heritage"]==1){
				$info_liste_herit.=$dit["cin_id"] . ",";
			}
		}
	}
	
	$infos=array();
	
	if(!empty($info_liste)){
		
		$info_liste=substr($info_liste, 0, -1); 

		// recherche des infos associés au client
		
		$sql="SELECT inf_id,inf_type,inf_description,inf_famille,inf_sous_famille,inf_sous_sous_famille,inf_auteur,inf_date,inf_client FROM Infos
		WHERE inf_client=" . $client . " AND inf_type IN (" . $info_liste . ")";
		if(!empty($famille)){
			$sql.=" AND (inf_famille=" . $famille . " OR inf_famille=0)";
		}
		if(!empty($sous_famille)){
			$sql.=" AND (inf_sous_famille=" . $sous_famille . " OR inf_sous_famille=0)";
		}
		if(!empty($sous_sous_famille)){
			$sql.=" AND (inf_sous_sous_famille=" . $sous_sous_famille . " OR inf_sous_sous_famille=0)";
		}
		$sql.=" ORDER BY inf_date DESC;";
		$req=$Conn->query($sql);
		$info_client=$req->fetchAll();
		
		$infos=$info_client;
		
		if(!empty($info_liste_herit)){
		
			// donnée du client
			
			$info_liste_herit=substr($info_liste_herit, 0, -1); 
			
			$sql="SELECT cli_filiale_de,cli_fil_de FROM Clients WHERE cli_id=" . $client . " AND cli_groupe;";
			$req=$Conn->query($sql);
			$d_client=$req->fetch();
			if(!empty($d_client)){
				
				$cli_filiale_de=0;
				
				if($d_client["cli_filiale_de"]>0){
					if(!empty($d_client["cli_fil_de"])){
						$cli_filiale_de=$d_client["cli_fil_de"];
					}else{
						$cli_filiale_de=$d_client["cli_filiale_de"];
					}
				}
				
				$securite=0;
				
				while($cli_filiale_de>0 AND $securite<10){
					
					$securite++;
					if(empty($cli_filiale_de_soc)){
						$cli_filiale_de_soc=$_SESSION['acces']["acc_societe"];
					}
					
					
					
					$sql="SELECT inf_id,inf_type,inf_description,inf_famille,inf_sous_famille,inf_sous_sous_famille,inf_auteur,inf_date,inf_client FROM Infos
					WHERE inf_client=" . $cli_filiale_de . " AND inf_type IN (" . $info_liste_herit . ")";
					if(!empty($famille)){
						$sql.=" AND (inf_famille=" . $famille . " OR inf_famille=0)";
					}
					if(!empty($sous_famille)){
						$sql.=" AND (inf_sous_famille=" . $sous_famille . " OR inf_sous_famille=0)";
					}
					if(!empty($sous_sous_famille)){
						$sql.=" AND (inf_sous_sous_famille=" . $sous_sous_famille . " OR inf_sous_sous_famille=0)";
					}
					$sql.=" ORDER BY inf_date DESC;";
					$req=$Conn->query($sql);
					$info_parent=$req->fetchAll();
					if(!empty($info_parent)){
						$infos=array_merge($infos,$info_parent);
					}
					
					$sql="SELECT cli_filiale_de,cli_fil_de FROM Clients WHERE cli_id=" . $cli_filiale_de . ";";
					$req=$Conn->query($sql);
					$d_client=$req->fetch();
					if(!empty($d_client)){
						if($d_client["cli_filiale_de"]>0){
							if(!empty($d_client["cli_fil_de"])){
								$cli_filiale_de=$d_client["cli_fil_de"];
							}else{
								$cli_filiale_de=$d_client["cli_filiale_de"];
							}
						}else{
							// permet de quitter le while
							$cli_filiale_de=0;
						}
					}
				}
			}
		}
	}
	return $infos;
}
?>