<?php


/*--------------------------------------------------
	SELECTION
----------------------------------------------*/


	function get_agence_societe($id){
		// permet de récuperer la liste des agences d'une société
		
		global $Conn;

		$req = $Conn->prepare("SELECT * FROM agences WHERE age_societe = " . $id . " AND NOT age_archive");
		$req->execute();
		$agences = $req->fetchAll();
		return $agences;
	}
	
	function get_agences()
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM agences");
		$req->execute();

		$agences = $req->fetchAll();

		return $agences;
	}
	function get_agence($id)
	{

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM agences WHERE age_id=:age_id");
		$req->bindParam(':age_id', $id);
		$req->execute();

		$agence = $req->fetch();

		return $agence;
	}
	// retourne la liste des agences avec la valeurs d'accès pour l'utilisateur passer en parametre
	function get_agences_utilisateur($societe,$utilisateur,$strict){
		
		global $Conn;
		
		$sql="SELECT age_id,age_code,age_nom,uso_utilisateur FROM Agences";
		if($strict){
			$sql.=" INNER JOIN";
		}else{
			$sql.=" LEFT OUTER JOIN";
		}
		$sql.=" Utilisateurs_Societes
		on (Agences.age_id=Utilisateurs_Societes.uso_agence AND uso_utilisateur=:utilisateur)
		WHERE age_societe=:societe ORDER BY age_code";
		$req=$Conn ->prepare($sql);
		$req->bindParam(":societe",$societe);
		$req->bindParam(":utilisateur",$utilisateur);
		$req->execute();
		$agences = $req->fetchAll();
		return $agences;
	}
	
	/* SELECTION ET INJECTION HTML <select/> */
	
	function get_agence_select($selected, $societe = ""){
		
		// $societe "" par defaut car $societe=0 permet de ne renvoyer aucun résultat 

		global $Conn;

		$agence_select="";
		
		$sql="SELECT * FROM agences";
		if($societe!=""){
			$sql.= " WHERE age_societe=" . $societe;
		}
		$sql.= " ORDER BY age_nom";
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$agence_select=$agence_select . "<option value='" . $donnees["age_id"] . "' >" . $donnees["age_nom"] . "</option>";
			}else{
				if($donnees['age_id'] == $selected){
				   $agence_select=$agence_select . "<option selected value='" . $donnees["age_id"] . "' >" . $donnees["age_nom"] . "</option>";
				}else{
					$agence_select=$agence_select . "<option value='" . $donnees["age_id"] . "' >" . $donnees["age_nom"] . "</option>";
				}
			   
			}
		}
		return $agence_select;
	  }

