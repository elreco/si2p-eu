<?php 

function insert_correspondance($utilisateur, $commentaire, $tel, $contact, $nom, $rappeler_le, $suspect, $sco_date){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("INSERT INTO correspondances (cor_date, cor_utilisateur, cor_commentaire, cor_contact_tel, cor_contact, cor_contact_nom, cor_rappeler_le, cor_client) VALUES (:sco_date, :utilisateur, :commentaire, :tel, :contact, :nom, :rappeler_le, :suspect);");

	$req->bindParam("utilisateur",$utilisateur);
	$req->bindParam("commentaire",$commentaire);
	$req->bindParam("tel",$tel);
	$req->bindParam("contact",$contact);
	$req->bindParam("nom",$nom);
	$req->bindParam("rappeler_le",$rappeler_le);
	$req->bindParam("suspect",$suspect);
	$req->bindParam("sco_date",$sco_date);
	$req->execute();

}

function update_correspondance($id, $utilisateur, $commentaire, $tel, $contact, $nom, $rappeler_le, $suspect, $cor_rappel, $sco_date){
	
	global $ConnSoc;
	$req = $ConnSoc->prepare("UPDATE correspondances SET cor_utilisateur = :utilisateur, cor_commentaire = :commentaire, cor_contact_tel =:tel, cor_contact = :contact, cor_contact_nom = :nom, cor_rappeler_le = :rappeler_le, cor_client =:suspect, cor_rappel=:cor_rappel, cor_date = :sco_date WHERE cor_id = :cor_id;");

	$req->bindParam("cor_id",$id);
	$req->bindParam("utilisateur",$utilisateur);
	$req->bindParam("commentaire",$commentaire);
	$req->bindParam("tel",$tel);
	$req->bindParam("contact",$contact);
	$req->bindParam("nom",$nom);
	$req->bindParam("rappeler_le",$rappeler_le);
	$req->bindParam("suspect",$suspect);
	$req->bindParam("cor_rappel",$cor_rappel);
	$req->bindParam("sco_date",$sco_date);
	$req->execute();

}
function get_correspondance($id){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("SELECT * FROM correspondances WHERE cor_id = :cor_id;");

	$req->bindParam("cor_id",$id);
	$req->execute();

	$corr = $req->fetch();

	return $corr;

}

function delete_correspondance($id){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("DELETE FROM correspondances WHERE cor_id = :cor_id");

	$req->bindParam("cor_id",$id);
	$req->execute();

}
?>