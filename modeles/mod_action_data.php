 <?php

	function action_data($action_id){

		$erreur=0;
		$erreur_txt="";

		global $Conn;
		global $ConnSoc;

		if($action_id>0){

			// ON RECUPERE LES DONNES NECESSAIRE A L'AFFICHAGE

			// l'action

			$sql="SELECT act_id,DATE_FORMAT(act_date_deb,'%d/%m/%Y') AS act_date_deb,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville,act_con_prenom,act_con_nom,act_con_tel,act_con_portable
			,act_commentaire,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,act_vehicule,act_gest_sta,act_agence FROM Actions WHERE act_id=:action;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action",$action_id);
			$req->execute();
			$data=$req->fetch(PDO::FETCH_ASSOC);
			if(!empty($data)){


				/*if(!empty($action["act_vehicule"])){
					$vehicule=orion_vehicule($action["act_vehicule"]);
				}*/

			}else{
				$erreur_txt="Impossible de charger la formation!";
			}
		}else{
			$erreur_txt="Impossible de charger la formation!";
		}
		if(empty($erreur_txt)){

			// FAMILLES

			$action_head="";

			$sql="SELECT pfa_libelle FROM Produits_Familles WHERE pfa_id=" . $data["act_pro_famille"] . ";";
			$req=$Conn->query($sql);
			$d_pro_fam=$req->fetch();
			if(!empty($d_pro_fam)){
				$action_head.=" " . $d_pro_fam["pfa_libelle"];
			}

			if(!empty($data["act_pro_sous_famille"])){
				$sql="SELECT psf_libelle,psf_couleur_bg,psf_couleur_txt FROM Produits_Sous_Familles WHERE psf_id=" . $data["act_pro_sous_famille"] . ";";
				$req=$Conn->query($sql);
				$d_pro_s_fam=$req->fetch();
				if(!empty($d_pro_s_fam)){
					$action_head.=" / " . $d_pro_s_fam["psf_libelle"];
				}
			}
			if(!empty($data["act_pro_sous_sous_famille"])){
				$sql="SELECT pss_libelle,pss_couleur_bg,pss_couleur_txt FROM Produits_Sous_Sous_Familles WHERE pss_id=" . $data["act_pro_sous_sous_famille"] . ";";
				$req=$Conn->query($sql);
				$d_pro_s_s_fam=$req->fetch();
				if(!empty($d_pro_s_s_fam)){
					$action_head.=" / " . $d_pro_s_s_fam["pss_libelle"];
				}
			}
			$data["lib_famille"]=$action_head;

			// VEHICULE
			if(!empty($data["act_vehicule"])){
				$sql="SELECT veh_libelle FROM Vehicules WHERE veh_id=" . $data["act_vehicule"] . ";";
				$req=$Conn->query($sql);
				$d_vehicule=$req->fetch();
				if(!empty($d_vehicule)){
					$data["vehicule"]=$d_vehicule["veh_libelle"];
				}
			}


			// LES DATES

			$d_intervenants=array();
			$d_sous_traitants=array();

			$sql="SELECT int_label_1,int_label_2";
			$sql.=",pda_id,pda_date as pda_date_sql,DATE_FORMAT(pda_date,'%d/%m/%Y') AS pda_date,pda_demi,pda_categorie";
			$sql.=",ase_id,ase_h_deb,ase_h_fin";
			$sql.=" FROM ";
			$sql.=" Intervenants LEFT JOIN Plannings_Dates ON (Intervenants.int_id=Plannings_Dates.pda_intervenant)";
			$sql.=" LEFT JOIN Actions_Sessions ON (Plannings_Dates.pda_id=Actions_Sessions.ase_date)";
			$sql.=" WHERE pda_type=1 AND pda_ref_1=:action AND NOT pda_archive";
			$sql.=" ORDER BY pda_date_sql,pda_demi,ase_h_deb,int_label_1,int_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action",$action_id);
			$req->execute();
			$data["dates"]=$req->fetchAll(PDO::FETCH_ASSOC);

			// LES CLIENTS

			$sql="SELECT cli_code,cli_nom,acl_id,acl_client,acl_pro_reference,acl_pro_libelle,acl_commentaire, acl_commercial FROM Actions_Clients,Clients WHERE acl_client=cli_id
			AND acl_action=:action ORDER BY acl_archive,cli_code;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action",$action_id);
			$req->execute();
			$data["clients"]=$req->fetchAll(PDO::FETCH_ASSOC);
			// DONNEES ORION 0 LIEES AUX CLIENTS
            $commercial_prec = 0;
			if(!empty($data["clients"])){

				foreach($data["clients"] as $k => $client){
					$data["clients"][$k]["infos"]=get_client_infos($client["acl_client"],3,$data["act_pro_famille"],$data["act_pro_sous_famille"],$data["act_pro_sous_sous_famille"]);
                    // DONNEES LIEES AU COMMERCIAL
                    if(!empty($client['acl_commercial'])){
                        $sql="SELECT com_id,com_label_1,com_label_2, com_portable FROM Commerciaux WHERE com_id = " . $client['acl_commercial'];
                        $req=$ConnSoc->prepare($sql);
                        $req->execute();
                        $com_source=$req->fetch();
                        if(!empty($com_source)){
                            $data["clients"][$k]['commercial'] = $com_source['com_label_2'] . " " . $com_source['com_label_1'] . " - " . $com_source['com_portable'];
                            if(empty($commercial_prec)){
                                $commercial_prec = $client['acl_commercial'];
                                $data["commercial"] = $com_source['com_label_2'] . " " . $com_source['com_label_1'] . " - " . $com_source['com_portable'];
                            }else{
                                if($commercial_prec != $client['acl_commercial']){
                                    $data["commercial"] = "";
                                }
                            }

                        }
                    }
                }
			}

			// LES ATTESTATIONS

			$d_attestations=array(
				"0" => ""
			);
			$sql="SELECT att_id,att_titre FROM Attestations WHERE att_famille=" . $data["act_pro_famille"] . " AND att_sous_famille=" . $data["act_pro_sous_famille"] . "
			AND att_sous_sous_famille=" . $data["act_pro_sous_sous_famille"] . " ORDER BY att_titre;";
			$req=$Conn->query($sql);
			$results=$req->fetchAll();
			if(!empty($results)){
				foreach($results as $r){
					$d_attestations[$r["att_id"]]=$r["att_titre"];
				}
			}

				// LES STAGIAIRES

				// LES STAGIAIRES -> ne sont pas sur la meme base

			$liste_sta="";
			$sql="SELECT ast_stagiaire FROM Actions_Stagiaires WHERE ast_action=" . $action_id . ";";
			$req=$ConnSoc->query($sql);
			$src_sta=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($src_sta)){
				$tab_sta=array_column ($src_sta,"ast_stagiaire");
				$liste_sta=implode($tab_sta,",");
			}

			$d_stagiaire=array();
			if(!empty($liste_sta)){
				$sql="SELECT sta_id,sta_nom,sta_prenom,sta_naissance FROM Stagiaires WHERE sta_id IN (" . $liste_sta . ");";
				$req=$Conn->query($sql);
				$result_sta=$req->fetchAll(PDO::FETCH_ASSOC);
				if(!empty($result_sta)){
					foreach($result_sta as $rs){
						$d_stagiaire[$rs["sta_id"]]=array(
							"sta_nom" => $rs["sta_nom"],
							"sta_prenom" => $rs["sta_prenom"],
							"sta_naissance" => $rs["sta_naissance"]
						);
					}
				}
			}

			$donnees=array();
			if($data["act_gest_sta"]==2){

				// INSCRIPTION A LA SESSION

				$session_id=0;
				$nb_session=-1;

				$sql="SELECT int_label_1,pda_date,pda_demi,ase_id,ase_h_deb,ase_h_fin
				,ass_stagiaire,ast_confirme,ast_attestation,ast_attest_ok
				FROM Intervenants
				INNER JOIN Plannings_Dates ON (Intervenants.int_id=Plannings_Dates.pda_intervenant)
				INNER JOIN Actions_Sessions ON (Plannings_Dates.pda_id=Actions_Sessions.ase_date)
				LEFT JOIN Actions_Stagiaires_Sessions ON (Actions_Sessions.ase_id=Actions_Stagiaires_Sessions.ass_session)
				LEFT JOIN Actions_Stagiaires ON (Actions_Stagiaires_Sessions.ass_stagiaire=Actions_Stagiaires.ast_stagiaire AND ast_action=:action)
				WHERE pda_type=1 AND pda_ref_1=:action
				ORDER BY pda_date,pda_demi,ase_h_deb,ase_id,int_label_1;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":action",$action_id);
				$req->execute();
				$resultat=$req->fetchAll();
				if(!empty($resultat)){
					foreach($resultat as $r){

						if($session_id!=$r["ase_id"]){

							$nb_session++;

							if(!empty($r["ase_h_deb"]) AND !empty($r["ase_h_deb"])){
								$h=$r["ase_h_deb"] . " / " . $r["ase_h_fin"];
							}elseif($r["ase_h_deb"]==1){
								$h="matin";
							}else{
								$h="après-midi";
							}
							$donnees[$nb_session]=array(
								"libelle" => $r["int_label_1"] . " " . $r["pda_date"] . " " . $h,
								"stagiaires" => array()
							);

							$session_id=$r["ase_id"];

						}
						if(!empty($d_stagiaire[$r["ass_stagiaire"]]["sta_nom"])){
							$donnees[$nb_session]["stagiaires"][]=array(
								"nom" => $d_stagiaire[$r["ass_stagiaire"]]["sta_nom"] . " " . $d_stagiaire[$r["ass_stagiaire"]]["sta_prenom"],
								"attestation" => $d_attestations[$r["ast_attestation"]]
							);
						}
					}
				}
			}else{
				// INSCRIPTION A LA FORMATION


				$action_client=0;
				$nb_client=-1;

				$sql="SELECT acl_commercial,acl_id,cli_id,cli_code,cli_nom,ast_stagiaire,ast_attestation,ast_confirme,ast_attest_ok
				FROM Actions_Clients INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND cli_agence=:cli_agence)
				LEFT JOIN Actions_Stagiaires ON (Actions_Clients.acl_id=Actions_Stagiaires.ast_action_client AND ast_action=:action)
				WHERE acl_action=:action AND NOT acl_archive
				ORDER BY acl_archive,cli_code,cli_nom,cli_id;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":cli_agence",$data["act_agence"]);
				$req->bindParam(":action",$action_id);
				$req->execute();
				$resultat=$req->fetchAll();
				if(!empty($resultat)){
					foreach($resultat as $r){

						if($action_client!=$r["acl_id"]){

							$nb_client++;

							$donnees[$nb_client]=array(
								"libelle" => $r["cli_nom"],
								"stagiaires" => array()
							);

							$action_client=$r["acl_id"];

						}
						if(!empty($d_stagiaire[$r["ast_stagiaire"]])){
							$donnees[$nb_client]["stagiaires"][]=array(
								"nom" => $d_stagiaire[$r["ast_stagiaire"]]["sta_nom"] . " " . $d_stagiaire[$r["ast_stagiaire"]]["sta_prenom"],
								"attestation" => $d_attestations[$r["ast_attestation"]]
							);
						}


					}
				}
			}
			$data["sta"]=$donnees;

			return $data;
		}
	} ?>
