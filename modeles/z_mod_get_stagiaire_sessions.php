<?php

/* RETOURNES LES SESSIONS DISPONIBLE POUR UN STAGIARE AVEC LEURS VALEURS D'AFFECTAION


AUTEUR FG
08/11/2016
*/
function get_stagiaire_sessions($action,$stagiaire){

	
	global $ConnSoc;

	$sql="SELECT ast_action_client FROM Actions_Stagiaires WHERE ast_action=:action AND ast_stagiaire=:stagiaire;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action",$action);
	$req->bindParam(":stagiaire",$stagiaire);
	$req->execute();
	$result=$req->fetch();
	if(!empty($result)){
		$action_client=$result["ast_action_client"];
	}
	
	$sql="SELECT int_label_1";
	$sql.=",pda_date,pda_demi,pda_test";
	$sql.=",ase_id,ase_h_deb,ase_h_fin";
	$sql.=",ass_session";
	$sql.=" FROM ((((";
	$sql.=" Intervenants LEFT JOIN Plannings_Dates ON (Intervenants.int_id=Plannings_Dates.pda_intervenant))";
	$sql.=" LEFT JOIN Actions_Clients_Dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id))";	
	$sql.=" LEFT JOIN Actions_Sessions ON (Plannings_Dates.pda_id=Actions_Sessions.ase_date))";
	$sql.=" LEFT OUTER JOIN Actions_Stagiaires_Sessions ON (Actions_Sessions.ase_id=Actions_Stagiaires_Sessions.ass_session AND ass_stagiaire=:stagiaire))";
	$sql.=" WHERE pda_type=1 AND pda_ref_1=:action AND acd_action_client=:action_client";
	$sql.=" ORDER BY pda_date,pda_demi,ase_h_deb,pda_intervenant;";
	$req = $ConnSoc->prepare($sql);
	$req->bindParam(":action",$action);
	$req->bindParam(":action_client",$action_client);
	$req->bindParam(":stagiaire",$stagiaire);
	$req->execute();
	$dates = $req->fetchAll();
	
    return $dates;
}
