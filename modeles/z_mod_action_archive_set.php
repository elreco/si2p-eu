<?php

 // ACHIVAGE D'UNE ACTION
 
 // retourne FALSE EN CAS D'ERREUR
 // un tableau en cas de réussite
// ["action"] -> indique si l'action a été complèment archivé 0 NON N -> id de l'action archivé
 
 function set_archive_action($action,$action_client){
	 
	
	 global $ConnSoc;

	if($_SESSION['acces']["acc_droits"][10]){
		
		if($action_client==0){
			
			// archivage de tous les clients qui participent à l'action
			
			$sql="SELECT acl_id FROM Actions_Clients WHERE acl_action=". $action .";";
			$req=$ConnSoc->query($sql);
			$d_action_client=$req->fetchAll();
					
		}else{
			// archivage d'un seul client
			
			$d_action_client=array(				
				0 => array(
					"acl_id" =>	$action_client
				)
			);	
		}

		$archive_ok=0;
		if($_SESSION['acces']["acc_droits"][10]){
			$archive_ok=1;
		}
		$retour=array(
			"archive_action" => 0
		);
		
		

		foreach($d_action_client as $dac){
			
			
			$slq="UPDATE Actions_Clients SET acl_archive = 1, acl_archive_date=NOW(), acl_archive_uti=" . intval($_SESSION['acces']['acc_ref_id']) . "
			,acl_archive_ok=" . $archive_ok . " WHERE acl_id=" . $dac["acl_id"] . ";";
			try {
				$req=$ConnSoc->query($slq);
			}catch( PDOException $Exception ) {
				return ("1 :" . $Exception->getMessage());	
			}
			
			
		}
		
		$sql="SELECT acl_action FROM Actions_Clients WHERE acl_action=" . $action . " AND acl_archive=0;";
		$req=$ConnSoc->query($sql);
		$result=$req->fetch();
		if(empty($result)){
			
			// tous les clients sont archivées on archive l'action
			
			
			// archivage de l'action
			$sql="UPDATE Actions SET 
			act_archive = 1,
			act_archive_date=NOW(),
			act_archive_uti=" . intval($_SESSION['acces']['acc_ref_id']) . ",
			act_archive_ok=" . $archive_ok . " 
			WHERE act_id=" . $action . ";";
			try {
				$req=$ConnSoc->query($sql);				
			}catch( PDOException $Exception ) {
				return ("2" . $Exception->getMessage());	
			}
			
			// archivage des dates
			
			$sql="UPDATE Plannings_Dates SET pda_archive=1
			WHERE pda_type=1 AND pda_ref_1=" . $action . ";";
			try {
				$req=$ConnSoc->query($sql);
				$retour["action"]=$action;
			}catch( PDOException $Exception ) {
				return ("3" . $Exception->getMessage());	
			}
			
			
		}
		return $retour;

	}else{
		return "Accès refusé";	
	}
	
	
}
?>