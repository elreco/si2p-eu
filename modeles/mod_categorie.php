<?php
//include_once '../connexion.php';
function get_categories()
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_categories");
    $req->execute();

    $categories = $req->fetchAll();

    return $categories;
}
function get_categorie($id)
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_categories WHERE pca_id = " . $id);
    $req->execute();

    $categorie = $req->fetch();

    return $categorie;
}
function get_categorie_couleur($id)
{

    global $Conn;
	
	$id=intval($id);

    $req = $Conn->prepare("SELECT cca_id, cca_couleur FROM clients_categories WHERE cca_id = " . $id);
    $req->execute();

    $categorie = $req->fetch();

    return $categorie;
}
function get_sous_categorie_couleur($id)
{

    global $Conn;
	
	$id=intval($id);

    $req = $Conn->prepare("SELECT csc_id, csc_couleur FROM clients_sous_categories WHERE csc_id = " . $id);
    $req->execute();

    $categorie = $req->fetch();

    return $categorie;
}
function insert_categorie()
{

    global $Conn;
	
	$pca_planning=0;
	if(!empty($_POST['pca_planning'])){
		$pca_planning=$_POST['pca_planning'];
	}

    // fin upload fichier
    if (isset($_POST['pca_id'])) {
    	$req = $Conn->prepare("REPLACE INTO produits_categories (pca_id,pca_libelle,pca_planning) VALUES (" . $_POST['pca_id'] . ", '" . $_POST['pca_libelle'] . "'," . $pca_planning . ")");
    }else{
    	$req = $Conn->prepare("INSERT INTO produits_categories (pca_libelle,pca_planning) VALUES ('" . $_POST['pca_libelle'] . "'," . $pca_planning . ")");
      }  

    $req->execute();

    header('Location: /categorie_liste.php');
    

}
function get_categorie_select($selected){

  global $Conn;

  $service_select="";
  
	$sql="SELECT * FROM produits_categories";
	if(!$_SESSION['acces']["acc_droits"][9]){
		$sql.=" WHERE NOT pca_compta";
	}
	$sql.=" ORDER BY pca_libelle;";
	$req=$Conn->query($sql);
	
  while ($donnees = $req->fetch())
  {
    if($selected == 0){
            $service_select=$service_select . "<option value='" . $donnees["pca_id"] . "' >" . $donnees["pca_libelle"] . "</option>";
        }else{
            if($donnees['pca_id'] == $selected){
               $service_select=$service_select . "<option selected value='" . $donnees["pca_id"] . "' >" . $donnees["pca_libelle"] . "</option>";
            }else{
                $service_select=$service_select . "<option value='" . $donnees["pca_id"] . "' >" . $donnees["pca_libelle"] . "</option>";
            }
           
        }
    
  }
  return $service_select;
  }