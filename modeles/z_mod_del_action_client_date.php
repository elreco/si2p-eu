<?php

/* 	PERMET DE SUPPRIMER L'INSCRIPTION D'UN CLIENT A UNE DATE DE FORMATION
	ON SUPPRIME EGALEMENT L'INSCRIPTION DES STAGIAIRES

date_id : id de la date = pda_id
action_client : id de l'inscription du client = acl_id

AUTEUR FG
*/
function del_action_client_date($date_id,$action_client_id){

	global $ConnSoc;
	
	$sql="DELETE FROM Actions_Clients_Dates WHERE acd_action_client=:action_client AND acd_date=:date";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action_client",$action_client_id);
	$req->bindParam(":date",$date_id);
	$req->execute();

}
