<?php


function stagiaire_attestation_data($sta){

	global $ConnSoc;
	global $Conn;
	global $acc_societe;

	$erreur=false;

	$data=array();

	if(!empty($sta)){
		if(is_array($sta)){
			$action_id=$sta["ast_action"];
			$stagiaire_id=$sta["ast_stagiaire"];
			$action_client_id=$sta["ast_action_client"];
		}else{
			$erreur=true;
			//echo("D");
		}
	}else{
		$erreur=true;
		//echo("E");
	}

	if(!empty($action_id) AND !empty($stagiaire_id) AND !empty($action_client_id)){

		$page=array();

		$req=$ConnSoc->prepare("SELECT act_agence FROM Actions WHERE act_id=:action_id;");
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$d_action=$req->fetch();

		// L'ATTESTATION
		$sql="SELECT att_id,att_contenu,att_titre FROM Attestations WHERE att_id=:attestation;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":attestation",$sta["ast_attestation"]);
		$req->execute();
		$result_attest=$req->fetch();
		if(!empty($result_attest)){
			$page["att_titre"]=$result_attest["att_titre"];
			$page["att_contenu"]=$result_attest["att_contenu"];
			$page["competences"]=array();
		}else{
			//echo("A");
			$erreur=true;
		}
	}else{
		//echo("B");
		$erreur=true;
	}
	//die();
	if(empty($erreur)){

		// ACTION CLIENT
		$sql="SELECT acl_client,acl_facturation FROM Actions_Clients WHERE acl_id=:action_client_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_client_id",$action_client_id);
		$req->execute();
		$d_action_client=$req->fetch();

		if(!empty($d_action_client["acl_client"])){

			$req=$Conn->prepare("SELECT cli_nom,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville
			FROM adresses,clients WHERE cli_id=adr_ref_id AND adr_ref=1 AND adr_ref_id=:adr_ref_id AND adr_type=1 AND adr_defaut;");
			$req->bindParam(":adr_ref_id",$d_action_client["acl_client"]);
			$req->execute();
			$d_client=$req->fetch();
			if(!empty($d_client)){

				$adresse_client="<b>" . $d_client["cli_nom"] . "</b>";

				if(!empty($d_client["adr_service"])){
					$adresse_client.="<br/>" . $d_client["adr_service"];
				}
				if(!empty($d_client["adr_ad1"])){
					$adresse_client.="<br/>" . $d_client["adr_ad1"];
				}
				if(!empty($d_client["adr_ad2"])){
					$adresse_client.="<br/>" . $d_client["adr_ad2"];
				}
				if(!empty($d_client["adr_ad3"])){
					$adresse_client.="<br/>" . $d_client["adr_ad3"];
				}
				$adresse_client.="<br/>" . $d_client["adr_cp"] . " " . $d_client["adr_ville"];
				$page["adresse_client"]=$adresse_client;
			}
		}

		// TEXTE AUTO SI DIPLOME

		if($sta["ast_diplome_statut"]==1 AND !empty($sta["ast_qualification"]) AND !empty($sta["ast_diplome"]) ){

			// INFO SUR LA QUALIF
			$sql="SELECT qua_caces FROM Qualifications WHERE qua_id=:qualification;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":qualification",$sta["ast_qualification"]);
			$req->execute();
			$d_qualif=$req->fetch();
			if(!empty($d_qualif)){

				// c'est une qualif CACES
				if(!empty($d_qualif["qua_caces"])){

					$liste_cat="";

					$sql="SELECT qca_libelle FROM Diplomes_Caces_Cat,Qualifications_Categories WHERE dcc_categorie=qca_id AND dcc_valide
					AND dcc_diplome=:dcc_diplome;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":dcc_diplome",$sta["ast_diplome"]);
					$req->execute();
					$d_caces_cat=$req->fetchAll(PDO::FETCH_ASSOC);
					if(!empty($d_caces_cat)){
						foreach($d_caces_cat as $cat){
							if(!empty($liste_cat)){
								$liste_cat.=",";
							}
							$liste_cat.=$cat["qca_libelle"];
						}
						$page["att_titre"].=" catégorie(s) " . 	$liste_cat;
					}
				}

			}
		}

		// LES COMPETENCES

		$sql="SELECT aco_id,aco_competence,aco_pratique FROM Attestations_competences WHERE aco_attestation=:attestation ORDER BY aco_competence;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":attestation",$sta["ast_attestation"]);
		$req->execute();
		$result_comp=$req->fetchAll();
		if(!empty($result_comp)){
			foreach($result_comp as $comp){
				$page["competences"][$comp["aco_id"]]=array(
					"comp_txt" => $comp["aco_competence"],
					"comp_acquise" => 1
				);
			}
		}

		if(!is_null($sta["ast_attest_ok"])){

			if(empty($sta["ast_attest_ok"])){

				// ATTESTATION EN ECHEC

				$sql_comp="SELECT sac_attest_competence,sac_acquise FROM Stagiaires_Attestations_Competences WHERE sac_stagiaire=:stagiaire_id AND sac_action=:action_id;";
				$req_comp=$ConnSoc->prepare($sql_comp);
				$req_comp->bindParam("action_id",$action_id);
				$req_comp->bindParam("stagiaire_id",$stagiaire_id);
				$req_comp->execute();
				$d_competences=$req_comp->fetchAll();
				if(!empty($d_competences)){
					foreach($d_competences as $d_comp){
						$page["competences"][$d_comp["sac_attest_competence"]]["comp_acquise"]=$d_comp["sac_acquise"];
					}
				}
			}
		}

		// LE STAGIAIRE
		$req_sta=$Conn->query("SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id=" . $stagiaire_id . ";");
		$d_stagiaire=$req_sta->fetch();
		if(!empty($d_stagiaire)){

			$page["sta_nom"]=$d_stagiaire["sta_nom"];
			$page["sta_prenom"]=$d_stagiaire["sta_prenom"];
		}

		// INFO JURIDIQUE
		if($d_action_client["acl_facturation"]==1){
			$data["juridique"]=get_juridique(4,4);
		}else{
			$data["juridique"]=get_juridique($acc_societe,$d_action["act_agence"]);
		}

		// Date du doc

		$data["date_doc"]=strftime("%A %d %B %Y",time());

		// CALCUL DE LA DURRE
		$calcul_duree=true;

		$pda_date=null;
		$pda_demi=null;
		$nb_p=-1;

		$periodes=array(
			"duree_form" => "",
			"periodes" => array(),
			"deb_form" => "",
			"fin_form" => ""
		);

		$req_h=$ConnSoc->query("SELECT ase_h_deb,ase_h_fin,pda_date,pda_demi FROM Actions_Sessions,Actions_Stagiaires_Sessions,Plannings_Dates
		WHERE ase_id=ass_session AND ase_date=pda_id AND ass_stagiaire=" . $stagiaire_id . " AND ass_action=" . $action_id . " ORDER BY pda_date,pda_demi,ase_h_deb;");
		$d_heures=$req_h->fetchAll();
		if(!empty($d_heures)){

			$duree="";

			foreach($d_heures as $k => $h){

				if($k==0){

					$nb_p++;

					$periodes["periodes"][$nb_p]=array(
						"date_deb" => $h["pda_date"],
						"demi_deb" => $h["pda_demi"],
						"h_deb" =>$h["ase_h_deb"],
						"date_fin" => $h["pda_date"],
						"demi_fin" => $h["pda_demi"],
						"h_fin" =>$h["ase_h_fin"],
						"duree" => ""
					);
					$periodes["deb_form"]=$h["pda_date"];
					$periodes["fin_form"]=$h["pda_date"];

				}elseif($h["pda_date"]!=$pda_date OR $h["pda_demi"]!=$pda_demi){

					// nouvelle demi journée

					$periodes["fin_form"]=$h["pda_date"];

					if($h["pda_demi"]==2){

						if($pda_demi==1 AND $h["pda_date"]==$pda_date){

							// pas de rupture de date
							$periodes["periodes"][$nb_p]["date_fin"]=$h["pda_date"];
							$periodes["periodes"][$nb_p]["demi_fin"]=$h["pda_demi"];
							$periodes["periodes"][$nb_p]["h_fin"]=$h["ase_h_fin"];

						}else{

							$nb_p++;

							$periodes["periodes"][$nb_p]=array(
								"date_deb" => $h["pda_date"],
								"demi_deb" => $h["pda_demi"],
								"h_deb" =>$h["ase_h_deb"],
								"date_fin" => $h["pda_date"],
								"demi_fin" => $h["pda_demi"],
								"h_fin" =>$h["ase_h_fin"],
								"duree" => ""
							);

						}

					}else{

						// c'est un matin

						$DT_periode=date_create_from_format('Y-m-d',$h["pda_date"]);
						$j=$DT_periode->format("N");
						if($j==1){
							// C'est un lundu matin
							$DT_periode->sub(new DateInterval('P3D'));
						}else{
							$DT_periode->sub(new DateInterval('P1D'));
						}
						$d_compare=$DT_periode->format("Y-m-d");

						if($pda_demi==2 AND $pda_date==$d_compare){

							// pas de rupture de date
							$periodes["periodes"][$nb_p]["date_fin"]=$h["pda_date"];
							$periodes["periodes"][$nb_p]["demi_fin"]=$h["pda_demi"];
							$periodes["periodes"][$nb_p]["h_fin"]=$h["ase_h_fin"];

						}else{

							$nb_p++;

							$periodes["periodes"][$nb_p]=array(
								"date_deb" => $h["pda_date"],
								"demi_deb" => $h["pda_demi"],
								"h_deb" =>$h["ase_h_deb"],
								"date_fin" => $h["pda_date"],
								"demi_fin" => $h["pda_demi"],
								"h_fin" =>$h["ase_h_fin"],
								"duree" => ""
							);
						}
					}
				}else{
					// c'est la meme demi journée
					$periodes["periodes"][$nb_p]["h_fin"]=$h["ase_h_fin"];
				}
				$pda_date=$h["pda_date"];
				$pda_demi=$h["pda_demi"];



				// Calcul de la duree de la partcipation du stagiaire
				if(!empty($h["ase_h_deb"]) AND !empty($h["ase_h_fin"]) ){
					$DT_deb =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $h["ase_h_deb"] . ":00");
					$DT_fin =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $h["ase_h_fin"] . ":00");
					$interval = $DT_deb->diff($DT_fin);

					//$duree->add(date_diff($DT_deb, $DT_fin));
					/* var_dump($duree);
					die(); */
					if(!empty($duree)){
						$duree_before = $duree;
					}else{
						$duree_before = 0;
					}

					$duree = sprintf(
						'%d:%02d',
						($interval->d * 24) + $interval->h,
						$interval->i
					);
					if(!empty($duree_before)){
						$duree_add = explode_time($duree) + explode_time($duree_before);
					}else{
						$duree_add = explode_time($duree);
					}

					$duree = second_to_hhmm($duree_add);
				}else{
					$calcul_duree=false;
				}
			}
		} else {
			// le stagiaire n'est inscrit à aucune date

			$calcul_duree=false;

			$page["alert"]=array(
				"type" => "warning",
				"texte" => $d_stagiaire["sta_prenom"] . " " . $d_stagiaire["sta_nom"] . " ne participe à aucune session! Attestation sans durée."
			);
		}
		// PERIODE DE FORMATION

		if(!empty($periodes["periodes"])){

			if($periodes["deb_form"]!=$periodes["fin_form"]){
				$DT_deb=date_create_from_format('Y-m-d',$periodes["deb_form"]);
				$DT_fin=date_create_from_format('Y-m-d',$periodes["fin_form"]);
				$page["att_planning"]="Du " . $DT_deb->format("d/m/Y") . " au " . $DT_fin->format("d/m/Y");
			}else{
				$DT_deb=date_create_from_format('Y-m-d',$periodes["deb_form"]);
				if($periodes["periodes"][0]["demi_deb"]!=$periodes["periodes"][0]["demi_fin"]){
					$page["att_planning"]="Le " . $DT_deb->format("d/m/Y");
				}else{
					if(!empty($periodes["periodes"][0]["h_deb"]) AND !empty($periodes["periodes"][0]["h_fin"])){
						$page["att_planning"]="Le " . $DT_deb->format("d/m/Y") . " de " . $periodes["periodes"][0]["h_deb"] . " à " . $periodes["periodes"][0]["h_fin"];
					}elseif($periodes["periodes"][0]["demi_deb"]==1){
						$page["att_planning"]="Le " . $DT_deb->format("d/m/Y") . " matin";
					}else{
						$page["att_planning"]="Le " . $DT_deb->format("d/m/Y") . " après-midi";
					}
				}

			}

		};

		// DUREE TOTAL DE LA PARTICIPATION

		if($calcul_duree){
			$page["att_duree"]=str_replace(":", "h",$duree);
		}else{
			$page["att_duree"]="";
		}
		


		if(!empty($page)){
			$data["pages"][]=$page;
		}

		$data["pdf"]="attestation_". $action_id . "_" . $stagiaire_id;
	}

	if(!empty($erreur)){
		return false;
	}else{
		return $data;
	}
}?>
