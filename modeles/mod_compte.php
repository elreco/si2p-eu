<?php

	// MODELE 
	
	function get_compte($compte_id){
		
		global $Conn;	
		
		$req_sql="SELECT * FROM Comptes WHERE cpt_id=" . $compte_id;
		$req=$Conn->query($req_sql);
		$get_compte = $req->fetchAll();	
		return $get_compte;
	}
	
	function get_comptes($cpt_numero = ""){
		
		global $Conn;	
		
		$req_sql="SELECT * FROM Comptes";
		if($cpt_numero!=""){
			$req_sql=$req_sql . " WHERE cpt_numero LIKE '" . $cpt_numero . "%'";
		}
		$req_sql=$req_sql . " ORDER BY cpt_numero";
		
		$req=$Conn->query($req_sql);
		$get_comptes = $req->fetchAll();	
		return $get_comptes;
	}
	function get_compte_select($selected){
		
		// $societe "" par defaut car $societe=0 permet de ne renvoyer aucun résultat 

		global $Conn;

		$client_select="";
		
		$sql="SELECT * FROM comptes";
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["cpt_id"] . "' >" . $donnees["cpt_libelle"] . "</option>";
			}else{
				if($donnees['cpt_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["cpt_id"] . "' >" . $donnees["cpt_libelle"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["cpt_id"] . "' >" . $donnees["cpt_libelle"] . "</option>";
				}

			}
		}
		return $client_select;
	}

	function update_compte($cpt_id,$cpt_numero,$cpt_libelle){
		
		global $Conn;	
		$req = $Conn->prepare("UPDATE Comptes SET cpt_numero=:cpt_numero, cpt_libelle=:cpt_libelle WHERE cpt_id=:cpt_id");
		$req->bindParam(':cpt_id', $cpt_id);
		$req->bindParam(':cpt_numero', $cpt_numero);
		$req->bindParam(':cpt_libelle', $cpt_libelle);
		$req->execute();	
	}
	
	function insert_compte($cpt_numero,$cpt_libelle){
		global $Conn;

		$cpt_id=0;
		
		$req = $Conn->prepare("INSERT INTO Comptes (cpt_numero, cpt_libelle) VALUES (:cpt_numero, :cpt_libelle)");
		$req->bindParam(':cpt_numero', $cpt_numero);
		$req->bindParam(':cpt_libelle', $cpt_libelle);
		$req->execute();
		$cpt_id=$Conn ->lastInsertId();
		
		return $cpt_id;
	}

?>	
