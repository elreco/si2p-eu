<?php
// creation ou maj d'un contact

// $d_contact => array avec clé = nom champ base

// retourne false en cas d'échec et ID au format INT du contact en cas de réussite

function set_contact($d_contact){
	
	global $Conn;
	
	if(!empty($d_contact)){
		
		$criteres=array();
		
		if(!empty($d_contact["con_id"])){
			
			// MAJ
			
			$sql="UPDATE contacts SET ";
			if(isset($d_contact["con_fonction"])){
				if(!empty($d_contact["con_fonction"])){
					$sql.="con_fonction=:con_fonction,
					con_fonction_nom=''";	
					$criteres["con_fonction"]=$d_contact["con_fonction"];
				}else{
					$sql.="con_fonction=0,
					con_fonction_nom=:con_fonction_nom";	
					$criteres["con_fonction_nom"]=$d_contact["con_fonction_nom"];
				}
			}
			if(isset($d_contact["con_titre"])){
				if(!empty($d_contact["con_titre"])){
					$sql.="con_titre=:con_titre,";	
					$criteres["con_titre"]=$d_contact["con_titre"];
				}else{
					$sql.="con_titre=0,";	
				}
			}
			if(isset($d_contact["con_nom"])){
				$sql.="con_nom=:con_nom,";	
				$criteres["con_nom"]=$d_contact["con_nom"];
			}
			if(isset($d_contact["con_prenom"])){
				$sql.="con_prenom=:con_prenom,";	
				$criteres["con_prenom"]=$d_contact["con_prenom"];
			}
			if(isset($d_contact["con_tel"])){
				$sql.="con_tel=:con_tel,";	
				$criteres["con_tel"]=$d_contact["con_tel"];
			}
			if(isset($d_contact["con_portable"])){
				$sql.="con_portable=:con_portable,";	
				$criteres["con_portable"]=$d_contact["con_portable"];
			}
			if(isset($d_contact["con_fax"])){
				$sql.="con_fax=:con_fax,";	
				$criteres["con_fax"]=$d_contact["con_fax"];
			}
			if(isset($d_contact["con_mail"])){
				$sql.="con_mail=:con_mail,";	
				$criteres["con_mail"]=$d_contact["con_mail"];
			}
			if(isset($d_contact["con_defaut"])){
				if(!empty($d_contact["con_defaut"])){
					$sql.="con_defaut=1,";	
				}else{
					$sql.="con_defaut=0,";	
				}
			}
			if(isset($d_contact["con_compta"])){
				if(!empty($d_contact["con_compta"])){
					$sql.="con_compta=1,";	
				}else{
					$sql.="con_compta=0,";	
				}
			}
			if(isset($d_contact["con_comment"])){
				$sql.="con_comment=:con_comment,";	
				$criteres["con_comment"]=$d_contact["con_comment"];
			}
			if(!empty($criteres)){
				$sql=substr($sql,0,-1); 			
				$sql.=" WHERE con_id=:contact;";
				$req = $Conn->prepare($sql);
				$req->bindParam("contact",$d_contact["con_id"]);
				foreach($criteres as $c => $u){
					$req->bindValue($c,$u);
				}						
				try {
					$req->execute();
					return intval($d_contact["con_id"]);
				}catch (Exception $e) {
					return false;
				}
			}else{
				return false;
			}
			
		}else{
			
			// ADD
			
			$con_ref_id=0;
			if(!empty($d_contact["con_ref_id"])){
				$con_ref_id=intval($d_contact["con_ref_id"]);
			}

			if($con_ref_id>0 AND !empty($d_contact["con_nom"])){
				
				$sql="INSERT INTO Contacts (con_ref,con_ref_id";
				$valeurs="1,:con_ref_id";
				
				if(isset($d_contact["con_fonction"])){
					if(!empty($d_contact["con_fonction"])){
						$sql.=",con_fonction,con_fonction_nom";
						$valeurs.=",:con_fonction,''";							
						$criteres["con_fonction"]=$d_contact["con_fonction"];
					}else{
						$sql.=",con_fonction,con_fonction_nom";
						$valeurs.=",0,:con_fonction_nom";							
						$criteres["con_fonction_nom"]=$d_contact["con_fonction_nom"];
					}
				}
				if(isset($d_contact["con_titre"])){
					if(!empty($d_contact["con_titre"])){
						$sql.=",con_titre";	
						$valeurs.=",:con_titre";	
						$criteres["con_titre"]=$d_contact["con_titre"];
					}
				}
				if(isset($d_contact["con_nom"])){
					$sql.=",con_nom";	
					$valeurs.=",:con_nom";	
					$criteres["con_nom"]=$d_contact["con_nom"];
				}
				if(isset($d_contact["con_prenom"])){
					$sql.=",con_prenom";
					$valeurs.=",:con_prenom";						
					$criteres["con_prenom"]=$d_contact["con_prenom"];
				}
				if(isset($d_contact["con_tel"])){
					$sql.=",con_tel";
					$valeurs.=",:con_tel";						
					$criteres["con_tel"]=$d_contact["con_tel"];
				}
				if(isset($d_contact["con_portable"])){
					$sql.=",con_portable";
					$valeurs.=",:con_portable";						
					$criteres["con_portable"]=$d_contact["con_portable"];
				}
				if(isset($d_contact["con_fax"])){
					$sql.=",con_fax";
					$valeurs.=",:con_fax";						
					$criteres["con_fax"]=$d_contact["con_fax"];
				}
				if(isset($d_contact["con_mail"])){
					$sql.=",con_mail";
					$valeurs.=",:con_mail";						
					$criteres["con_mail"]=$d_contact["con_mail"];
				}
				if(isset($d_contact["con_defaut"])){
					if(!empty($d_contact["con_defaut"])){
						$sql.=",con_defaut";
						$valeurs.=",1";	
					}else{
						$sql.=",con_defaut";
						$valeurs.=",0";	
					}
				}
				if(isset($d_contact["con_compta"])){
					if(!empty($d_contact["con_compta"])){
						$sql.=",con_compta";
						$valeurs.=",1";	
					}else{
						$sql.=",con_compta";
						$valeurs.=",0";	
					}
				}
				if(isset($d_contact["con_comment"])){
					$sql.=",con_comment";	
					$valeurs.=",:con_comment";	
					$criteres["con_comment"]=$d_contact["con_comment"];
				}
				$sql.=" ) VALUES (" . $valeurs . ");";
				$req = $Conn->prepare($sql);
				$req->bindParam(":con_ref_id",$con_ref_id);
				foreach($criteres as $c => $u){
					$req->bindValue($c,$u);
				}
				
				try{
					$req->execute();
					return intval($Conn->lastInsertId());
				}catch (Exception $e) {
					return false;
				}
			}else{
				return false;
			}
			
		}
	}
}