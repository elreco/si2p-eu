<?php


	function get_clients($categorie,$sous_categorie,$archive,$acces){
		
		global $Conn;
		global $acc_societe;
		global $acc_agence;
		global $acc_utilisateur;
		
		
		$d_clients=array();
		
		$sql="SELECT DISTINCT cli_id,cli_code,cli_nom FROM Clients,Clients_Societes WHERE cli_id=cso_client AND cso_societe=" . $acc_societe;
		if(!empty($acc_agence)){
			$sql.=" AND cso_agence=" . $acc_agence;
		}
		if(!empty($categorie)){
			$sql.=" AND cli_categorie=" . $categorie;
		}
		if(!empty($sous_categorie)){
			$sql.=" AND cli_sous_categorie=" . $sous_categorie;
		}
		if(!is_null($archive)){
			$sql.=" AND cso_archive=" . $archive;
		}
		if(!is_null($acces)){
			if($acces==1){
				if(!$_SESSION["acces"]["acc_droits"][6]){
					$sql.=" AND cso_utilisateur=" . $acc_utilisateur;
				}
			}
		}
		$sql.=" ORDER BY cli_code,cli_nom;";
		$req=$Conn->query($sql);
		$d_resultats=$req->fetchAll();
		if(!empty($d_resultats)){
			foreach($d_resultats as $r){
				$d_clients[]=array(
					"id" => $r["cli_id"],
					"text" => $r["cli_code"] . " - " . $r["cli_nom"]
				);
			}		
		}
		
		return $d_clients;
	}
?>