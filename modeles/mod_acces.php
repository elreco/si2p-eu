<?php

// GESTIONS DES ACCES A L'APPLICATION

function insert_acces($acc_ref,$acc_ref_id,$acc_profil,$acc_uti_ident,$acc_uti_passe,$acc_passe_modif,$acc_mail){
	
	global $Conn;
    $req = $Conn->prepare("INSERT INTO Acces (acc_ref,acc_ref_id,acc_profil,acc_uti_ident,acc_uti_passe,acc_passe_modif,acc_mail)
	VALUES (:acc_ref, :acc_ref_id, :acc_profil, :acc_uti_ident, :acc_uti_passe, :acc_passe_modif, :acc_mail)");
	$req->bindParam(':acc_ref', $acc_ref);
	$req->bindParam(':acc_ref_id', $acc_ref_id);
	$req->bindParam(':acc_profil', $acc_profil);
	$req->bindParam(':acc_uti_ident', $acc_uti_ident);
	$req->bindParam(':acc_uti_passe', $acc_uti_passe);
	$req->bindParam(':acc_passe_modif', $acc_passe_modif);
	$req->bindParam(':acc_mail', $acc_mail);
    $req->execute();
	
}
function get_acces($acc_ref,$acc_ref_id){
	
	global $Conn;
	$result;
    $req = $Conn->prepare("SELECT * FROM Acces WHERE acc_ref=:acc_ref AND acc_ref_id=:acc_ref_id");
	$req->bindParam(':acc_ref', $acc_ref);
	$req->bindParam(':acc_ref_id', $acc_ref_id);
    $req->execute();
	$result = $req->fetch();
	
	return $result;
	
}
function update_acces($acc_ref,$acc_ref_id,$acc_profil,$acc_uti_ident,$acc_uti_passe,$acc_passe_modif,$acc_mail){
	
	global $Conn;

    $req = $Conn->prepare("UPDATE Acces SET 
		acc_profil=:acc_profil,
		acc_uti_ident=:acc_uti_ident,
		acc_uti_passe=:acc_uti_passe,
		acc_passe_modif=:acc_passe_modif,
		acc_mail=:acc_mail
		WHERE acc_ref=:acc_ref AND acc_ref_id=:acc_ref_id");
	$req->bindParam(':acc_ref', $acc_ref);
	$req->bindParam(':acc_ref_id', $acc_ref_id);
	$req->bindParam(':acc_profil', $acc_profil);
	$req->bindParam(':acc_uti_ident', $acc_uti_ident);
	$req->bindParam(':acc_uti_passe', $acc_uti_passe);
	$req->bindParam(':acc_passe_modif', $acc_passe_modif);
	$req->bindParam(':acc_mail', $acc_mail);	
    $req->execute();

}
function get_acces_compte($acc_uti_ident,$acc_uti_passe){
	
	global $Conn;
	$result;
    $req = $Conn->prepare("SELECT * FROM Acces WHERE acc_uti_ident=:acc_uti_ident AND BINARY acc_uti_passe=:acc_uti_passe AND NOT acc_archive");
	$req->bindParam(':acc_uti_ident', $acc_uti_ident);
	$req->bindParam(':acc_uti_passe', $acc_uti_passe);
    $req->execute();
	$result = $req->fetch();
	
	return $result;
	
}

 ?>
