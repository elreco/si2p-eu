<?php

// GENERE DES ATTESTATIONS EN PDF
require_once("../vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;

function model_stagiaire_attestation_pdf($data){

	global $acc_societe;
	
	$erreur="";
	ob_start(); ?>

	<style type="text/css" >
		body{
			font-family:arial;
			font-size:8pt;
			
		}
		p{
			margin:2mm;
		}
		.table{
			width:100%;
			border-collapse: collapse;
			border:1px solid red;
		}
		.tab_border{
			border-collapse: collapse;
			table-layout: fixed; 
		}
		.tab_border td{
			border:1px solid #4d4d4d;
			border-collapse:collapse;
			padding:10px;
		}
		.w33{
			width:33%;
		}
		.w67{
			width:67%;
		}
		.w50{
			width:50%;
		}
		.w25{
			width:25%;
		}
		.text-center{
			text-align:center;
		}
		.text-left{
			text-align:left;
		}
		.logo{
			max-height:20mm;
		}
		.signature{
			max-width:60mm;
		}
	</style>
<?php
	foreach($data["pages"] as $page){ ?>
		<page backtop="43mm" backleft="0mm" backright="0mm" backbottom="15mm" >
			<page_header> 
				<div class="text-center" >
					<img src="../<?=$data["juridique"]["logo_1"]?>" class="logo" />
				</div>
				<div class="text-center" >
					<h1 class="text-center" >ATTESTATION DE FIN DE FORMATION</h1>
					<p>Article L. 6353-1 du Code du Travail</p>
				</div>
						
			</page_header>
			
			<p>
				<?=$data["juridique"]["nom"]?> organisme de formation déclaré sous le n° <?=$data["juridique"]["num_existence"]?>, auprès du préfet de la région <?=$data["juridique"]["region_nom"]?>, atteste que :
			</p>
			<p class="text-center" ><b><?=$page["sta_nom"] . " " . $page["sta_prenom"]?></b></p>
			<p>a suivi la formation <?=$page["att_titre"]?> dispensée par <?=$data["juridique"]["nom"]?> :</p>
			
			<table class="tab_border" style="width:100%;" >
				<tr>
					<td class="w33" ><b>Intitulé</b></td>
					<td class="w67" ><?=$page["att_titre"]?></td>
				</tr>
				<tr>
					<td class="w33"><b>Rappel des objectifs</b></td>
					<td class="w67" ><?=trim($page["att_contenu"])?></td>
				</tr>
				<tr>
					<td class="w33" ><b>Durée</b></td>
					<td class="w67" ><?=$page["att_duree"]?></td>
				</tr>
			</table>
			<p>a été évalué(e), au regard des objectifs de formation rappelés ci-dessus :</p>
			
			<table class="tab_border" style="width:100%;margin-bottom:50px;" >
				<tr>
					<td class="w50 text-center" rowspan="2" >Compétences ou connaissances visées</td>
					<td class="w50 text-center" colspan="2" >Résultats à l’issue de la formation</td>
				</tr>
				<tr>
					<td class="w25 text-center" style="border-left:none;"  >Acquise</td>
					<td class="w25 text-center" >Reste à acquérir</td>
				</tr>
		<?php	if(!empty($page["competences"])){
					foreach($page["competences"] as $comp){ ?>
						<tr>
							<td class="w50" ><?=$comp["comp_txt"]?></td>
			<?php			if($comp["comp_acquise"]){ ?>
								<td class="w25 text-center" >
									<img src="../assets/img/checked.png" />
								</td>
								<td class="w25 text-center" >
									<img src="../assets/img/unchecked.png" />
								</td>
			<?php			}else{ ?>
								<td class="w25 text-center" >
									<img src="../assets/img/unchecked.png" />
								</td>
								<td class="w25 text-center" >
									<img src="../assets/img/checked.png" />
								</td>
			<?php 			} ?>
						</tr>
		<?php		}
				}?>
			</table>
			
			<p>Fait à <?=$data["juridique"]["ville"]?>, le <?=$data["date_doc"]?></p>
			
			<table style="width:100%;" >
				<tr>
					<td class="w67" >&nbsp;</td>
					<td>
				<?php	if(!empty($data["juridique"]["resp_ident"])){
							echo($data["juridique"]["resp_ident"]);
						}
						if(!empty($data["juridique"]["resp_fonction"])){
							echo("<br/>" . $data["juridique"]["resp_fonction"]);
						}
						if(!empty($data["juridique"]["resp_signature"])){ 
							echo("<br/>"); ?>
							<img src="../<?=$data["juridique"]["resp_signature"]?>" class="img-responsive" />
				<?php	}?>
					</td>
				</tr>
			</table>
		</page>
<?php	
	}

	$content=ob_get_clean();
	
	try{
		$pdf=new HTML2PDF('P','A4','fr');
		$pdf->pdf->SetDisplayMode('fullwidth');
		//echo($content);
		$pdf->writeHTML($content);
		if($pdf){
			$pdf->Output($_SERVER["DOCUMENT_ROOT"] . '/Documents/Societes/' . $acc_societe . '/Attestations/' . $data["pdf"] . '.pdf', 'F');					
		}
	}catch(HTML2PDF_exception $e){
		$erreur="Erreur lors de la génération du fichier PDF";
	}
	
	if(!empty($erreur)){
		return false;
	}else{
		return true;
	}
}
?>