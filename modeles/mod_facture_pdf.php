<?php

// GENERE DES ATTESTATIONS EN PDF

//require_once("../vendor/autoload.php");
require_once("vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;

function model_facture_pdf($data){
	
	global $acc_societe;
	global $base_reglement;

	$erreur="";
	ob_start(); ?>
	
	<style type="text/css" >

<?php	require("assets/skin/si2p/css/orion_pdf.css"); ?>

		.bandeau_alerte{
			background-color:yellow;
			border:2px solid black;
			padding:3px;
			text-align:center;
			font-weight:bold;
			margin:3px 0px;
		}
		.text-foot{
			font-size:10px;
		}
		
		.ligne-col-1{
			width:20%;
			padding:1px 5px;
			vertical-align:middle;
		}
		.ligne-col-2{
			width:52%;
			padding:1px 5px;
		}
		.ligne-col-3{
			width:11%;
			padding:1px 5px;
			vertical-align:middle;
		}
		.ligne-col-4{
			width:6%;
			padding:1px 5px;
			vertical-align:middle;
		}
		.ligne-col-5{
			width:11%;
			padding:1px 5px;
			vertical-align:middle;
		}
		
		.text-danger{
			color:#df5640;
			font-size:12px;
		}
	</style>
	
<?php	
	foreach($data["pages"] as $p => $page){ ?>
										
		<page backtop="27mm" backleft="0mm" backright="0mm" backbottom="5mm" >
			<page_header>							
				<table class="w-100" >
					<tr>
						<td class="w-40" >
				<?php		if(!empty($page["fac_logo_1"])){ ?>
								<img src="Documents/Societes/logos/<?=$page["fac_logo_1"]?>" style="max-width:220px;max-height:95px;">
								<!-- style="width:220px;max-height:95px;" -->
				<?php		}else{
								echo("&nbsp;");
							} ?>
						</td>
						<td class="w-60" >
					<?php	if($page["fac_nature"]==1){
								if($page["fac_convention"]==1){
									echo("<h1>Facture valant Convention</h1>");
								}else{
									echo("<h1>Facture client</h1>");
								}
							}else{
								echo("<h1>Avoir client</h1>");
							}?> 
						</td>
					</tr>
				</table>																								
			</page_header>
			<page_footer>
				<div class="footer" >
					<table>
						<tr>
							<td class="w-15" >
					<?php		if(!empty($page["fac_qualite_1"])){ ?>
									<img src="Documents/Societes/logos/<?=$page["fac_qualite_1"]?>" class="w-15" />
					<?php		}else{
									echo("&nbsp;");
								} ?>
							</td>
							<td class="w-70 text-center" ><?=$page["fac_footer"]?></td>
							<td class="w-15 text-right" >
					<?php		if(!empty($page["fac_qualite_2"])){ ?>
									<img src="Documents/Societes/logos/<?=$page["fac_qualite_2"]?>" class="w-15" />
					<?php		}else{
									echo("&nbsp;");
								} ?>
							</td>
						</tr>
					</table>
				</div>
			</page_footer>
			<table>
				<tr>
					<td class="w-60 va-t" >
			<?php		echo("<b>" . $page["fac_soc_nom"] . "</b>");
						if(!empty($page["fac_soc_agence"])){
							echo("<br/><b>Agence " . $page["fac_soc_agence"] . "</b>");
						};
						if(!empty($page["fac_soc_ad1"])){
							echo("<br/>" . $page["fac_soc_ad1"]);
						};
						if(!empty($page["fac_soc_ad2"])){
							echo("<br/>" . $page["fac_soc_ad2"]);
						};
						if(!empty($page["fac_soc_ad3"])){
							echo("<br/>" . $page["fac_soc_ad3"]);
						};
						echo("<br/><b>" . $page["fac_soc_cp"] . " " . $page["fac_soc_ville"] . "</b>"); 
						if(!empty($page["fac_soc_tel"])){
							echo("<br/>Tél. : " . $page["fac_soc_tel"]); 
						}
						if(!empty($page["fac_soc_fax"])){
							echo("<br/>Fax. : " . $page["fac_soc_fax"]); 
						}
						if(!empty($page["fac_soc_mail"])){
							echo("<br/>" . $page["fac_soc_mail"]); 
						}
						if(!empty($page["fac_soc_site"])){
							echo("<br/>" . $page["fac_soc_site"]); 
						}
						echo("<br/><br/>");
						echo("Affaire suivie : " . $page["fac_com_identite"]); 
						if(!empty($page["fac_reference"])){
							echo("<br/>Référence : " . $page["fac_reference"]); 
						}
						echo("<br/>");
						echo("Code client : " . $page["fac_cli_code"]); 
						?>
					</td>
					<td class="w-40 va-t" 
					<?php if(empty($page["fac_env_adresse"])){ ?>
						style="padding-top:50px"
					<?php }?>
					>
			<?php		if(!empty($page["fac_env_adresse"])){
							echo("<b>" . $page["fac_env_nom"] . "</b>");
							if(!empty($page["fac_env_agence"])){
								echo("<br/><b>" . $page["fac_soc_agence"] . "</b>");
							};
							if(!empty($page["fac_env_ad1"])){
								echo("<br/>" . $page["fac_env_ad1"]);
							};
							if(!empty($page["fac_env_ad2"])){
								echo("<br/>" . $page["fac_env_ad2"]);
							};
							if(!empty($page["fac_env_ad3"])){
								echo("<br/>" . $page["fac_env_ad3"]);
							};
							echo("<br/><b>" . $page["fac_env_cp"] . " " . $page["fac_env_ville"] . "</b>");
							echo("<br/><br/><i>Adresse de facturation :</i><br/>");																		
						}
						
						echo("<b>" . $page["fac_cli_nom"] . "</b>");
						if(!empty($page["fac_opca"])){
							if($page["fac_opca_type"]==7){
								if(!empty($page["fac_opca_compte"])){
									echo("<br/><b>pour " . $page["fac_opca_compte"] . "</b>");
								}
							}
						}
						if(!empty($page["fac_cli_service"])){
							echo("<br/>" . $page["fac_cli_service"]);
						};
						if(!empty($page["fac_cli_ad1"])){
							echo("<br/>" . $page["fac_cli_ad1"]);
						};
						if(!empty($page["fac_cli_ad2"])){
							echo("<br/>" . $page["fac_cli_ad2"]);
						};
						if(!empty($page["fac_cli_ad3"])){
							echo("<br/>" . $page["fac_cli_ad3"]);
						};
						echo("<br/><b>" . $page["fac_cli_cp"] . " " . $page["fac_cli_ville"] . "</b>"); ?>	
					</td>															
				</tr>													
			</table>
			<table class="w-100" >
				<tr>
					<td class="w-50" ><b>Numéro : <?=$page["fac_numero"]?></b></td>
					<td class="w-50 text-right" >
			<?php		if(!empty($page["fac_date"])){
							$DT_periode=date_create_from_format('Y-m-d',$page["fac_date"]);
							if(!is_bool($DT_periode)){
								echo(" Le " . utf8_encode(strftime("%A %d %B %Y",$DT_periode->getTimestamp())));
							}
						} ?>
					</td>
				</tr>
			</table>
				
			<table class="tab-border w-100" >
				<tr>
					<th class="ligne-col-1 text-center" >Référence</th>
					<th class="ligne-col-2 text-center" >Désignation</th>
					<th class="ligne-col-3 text-center" >Prix HT</th>
					<th class="ligne-col-4 text-center" >Qté</th>
					<th class="ligne-col-5 text-center" >Montant&nbsp;HT</th>															
				</tr>
		<?php	$tot_tva=array();
				if(!empty($page["lignes"])){
					foreach($page["lignes"] as $ligne){ 
					
						$ligne_texte=$ligne["fli_libelle"];														
						if(!empty($ligne["fli_formation"])){															
							$ligne_texte.="<br/>" . nl2br($ligne["fli_formation"]);
						}
						if(!empty($ligne["fli_complement"])){															
							$ligne_texte.="<br/>" . nl2br($ligne["fli_complement"]);
						}
						if(!empty($ligne["fli_stagiaire"])){															
							$ligne_texte.="<br/>" . nl2br($ligne["fli_stagiaire"]);
						}
						if(!empty($ligne["fli_exo"])){
							if(!empty($ligne_texte)){
								$ligne_texte.="<br/>";
							}
							$ligne_texte.="<b class='text-danger' >Exonération de TVA article 261-4-4°a du CGI</b>";
						}
						if(!empty($ligne["fli_opca_num"])){
							if(!empty($ligne_texte)){
								$ligne_texte.="<br/>";
							}
							$ligne_texte.="Numéro de prise en charge : " . $ligne["fli_opca_num"];
						}
						if(!empty($ligne["fli_bc_num"])){
							if(!empty($ligne_texte)){
								$ligne_texte.="<br/>";
							}
							$ligne_texte.="Numéro de BC : " . $ligne["fli_bc_num"];
						}
						
						$ht=0;
						if(!empty($ligne["fli_qte"])){
							$ht=$ligne["fli_montant_ht"]/$ligne["fli_qte"];	
							$ht=round($ht,2);
						}
						if(!isset($tot_tva[$ligne["fli_tva_taux"]])){
							$tot_tva[$ligne["fli_tva_taux"]]=0;															
						}
						$tva=$ligne["fli_montant_ht"]*($ligne["fli_tva_taux"]/100);
						$tva=round($tva,2);
						$tot_tva[$ligne["fli_tva_taux"]]=$tot_tva[$ligne["fli_tva_taux"]]+$tva;
						?>
						<tr>
							<td class="ligne-col-1" ><?=$ligne["fli_code_produit"]?></td>
							<td class="ligne-col-2" ><?=$ligne_texte?></td>		
							<td class="ligne-col-3 text-right" ><?=number_format($ht,2,","," ")?> €</td>
							<td class="ligne-col-4 text-right" ><?=$ligne["fli_qte"]?></td>
							<td class="ligne-col-5 text-right" ><?=number_format($ligne["fli_montant_ht"],2,","," ")?> €</td>
						</tr>
		<?php		}
				}
				if(!empty($page["fac_libelle"])){ ?>
					<tr>
						<td colspan="5" ><?=$page["fac_libelle"]?></td>
					</tr>
		<?php	} ?>
			</table>
			
			<end_last_page end_height="85mm">
				<table class="w-100" >
					<tr>
						<td class="w-60" >
					<?php	if($page["fac_affacturage"]==1 AND $acc_societe==17){ ?>
								<table class="tab-border text-foot w-100" >
									<tr>
										<td>
											REFERENCE RIB A UTILISER : <?=$page["fac_affacturage_iban"]?>
										</td>
									</tr>
								</table>																											
					<?php	}else{
								if($page["fac_rib_change"]){ ?>	
									<div class="bandeau_alerte" >
										ATTENTION : CHANGEMENT DE RIB
									</div>
						<?php	} ?>
								<table class="tab-border text-foot w-100" >
									<tr>
										<td class="text-center w-67" >IBAN</td>
										<td class="text-center w-33" >BIC</td>
									</tr>
									<tr>
										<td class="text-center w-67" ><?=$page["fac_rib_iban"]?></td>
										<td class="text-center w-33" ><?=$page["fac_rib_bic"]?></td>
									</tr>
								</table>
								<p class="text-foot" >
									En votre aimable règlement par <b><?=$base_reglement[$page["fac_reg_type"]]?></b>
									<br/>
									A régler avant le : 
							<?php	if(!empty($page["fac_date_reg_prev"])){
										$DT_periode=date_create_from_format('Y-m-d',$page["fac_date_reg_prev"]);
										if(!is_bool($DT_periode)){
											echo("<b>" . utf8_encode(strftime("%A %d %B %Y",$DT_periode->getTimestamp())) . "</b>");
										}
									} ?>
								</p>
				<?php		} ?>
						</td>
						<td class="w-40 va-t" >
						
							<table class="tab-border w-100" >
								<tr id="cont_total_ht" >
									<th class="text-right w-40" >Total H.T.</th>
									<td class="text-right w-60" ><?=number_format($page["fac_total_ht"],2,","," ")?> €</td>
								</tr>
					<?php		foreach($tot_tva as $tva_taux => $tva_montant){ ?>
									<tr>
										<th class="text-right w-40" >T.V.A. à <?=$tva_taux?> %</th>
										<td class="text-right w-60" ><?=number_format($tva_montant,2,","," ")?> €</td>
									</tr>
					<?php		} ?>
								<tr>
									<th class="text-right w-40" >Total T.T.C.</th>
									<td class="text-right w-60" ><?=number_format($page["fac_total_ttc"],2,","," ")?> €</td>
								</tr>
							</table>
						
						</td>
					</tr>
				</table>
		<?php	if($page["fac_affacturage"]==1 AND $acc_societe==17){ ?>
					<p class="text-danger" >
						Pour être libératoire, votre règlement doit être effectué directement à l'ordre de Crédit Agricole Leasing & Factoring 12 place des Etats-Unis CS 20001 - 92548 MONTROUGE Cedex - France
						qui le reçoit par subrogation dans le cadre d'un contrat d'affacturage et devra être avisé de toute réclamation relative à cette créance.
					</p>											
		<?php	}else{
					if($page["fac_compta_change"]){ ?>
						<div class="bandeau_alerte" >
							ATTENTION : CHANGEMENT D'ADRESSE
						</div>
		<?php		} ?>
					<table>
						<tr>
							<td class="w-60 text-foot" >		
								<p>
									Merci d'adresser vos règlements à :<br/>
					<?php			echo("<b>" . $data["pages"][0]["fac_soc_nom"] . "</b>");
									echo("<br/><b>Service comptabilité</b>");
									if(!empty($page["fac_compta_ad1"])){
										echo("<br/>" . $page["fac_compta_ad1"]);
									};
									if(!empty($page["fac_compta_ad2"])){
										echo("<br/>" . $page["fac_compta_ad2"]);
									};
									if(!empty($page["fac_compta_ad3"])){
										echo("<br/>" . $page["fac_compta_ad3"]);
									};
									echo("<br/>" . $page["fac_compta_cp"] . " " . $page["fac_compta_ville"]); ?>
								</p>
							</td>
							<td class="w-40 va-t text-foot" >
						<?php	echo("<br/>" . $page["fac_compta_mail"]);
								if(!empty($page["fac_compta_tel"])){
									echo("<br/>Tél. : " . $page["fac_compta_tel"]);
								};
								if(!empty($page["fac_compta_fax"])){
									echo("<br/>Fax : " . $page["fac_compta_fax"]);
								}; ?>
							</td>
						</tr>
					</table>
	<?php		} ?>				
				<p class="text-foot" >
					Escompte pour règlement anticipé : 0% <br/>
					En cas de retard de paiement, une pénalité égale à 3 fois le taux d’intérêt légal et une indemnité forfaitaire pour frais de recouvrement de
					40 euros seront exigibles (Article L 446.1 du Code de Commerce)
				</p>
			</end_last_page>
		</page>
<?php	
	}
	
	if(count($data["pages"])==1){
		
		if($data["pages"][0]["fac_affacturage"] AND !empty($data["pages"][0]["cli_first_facture"]) AND empty($data["pages"][0]["cso_aff_courrier"]) AND $acc_societe!=17){
			// il s'agit d'une facture en affacturage pour un client déjà facturé et qui n'a pas encore recu le courrier 
			// -> on genere le courrier ?>
			
			<page backtop="27mm" backleft="0mm" backright="0mm" backbottom="5mm" >
				<page_header>							
					<table class="w-100" >
						<tr>
							<td class="w-40" >
					<?php		if(!empty($data["pages"][0]["fac_logo_1"])){ ?>
									<img src="Documents/Societes/logos/<?=$data["pages"][0]["fac_logo_1"]?>" style="max-width:220px;max-height:95px;">
									<!-- style="width:220px;max-height:95px;" -->
					<?php		}else{
									echo("&nbsp;");
								} ?>
							</td>
							<td class="w-60" >&nbsp;</td>
						</tr>
					</table>																								
				</page_header>
				<page_footer>
					<div class="footer" >
						<table>
							<tr>
								<td class="w-15" >
						<?php		if(!empty($data["pages"][0]["fac_qualite_1"])){ ?>
										<img src="Documents/Societes/logos/<?=$data["pages"][0]["fac_qualite_1"]?>" class="w-15" />
						<?php		}else{
										echo("&nbsp;");
									} ?>
								</td>
								<td class="w-70 text-center" ><?=$data["pages"][0]["fac_footer"]?></td>
								<td class="w-15 text-right" >
						<?php		if(!empty($data["pages"][0]["fac_qualite_2"])){ ?>
										<img src="Documents/Societes/logos/<?=$data["pages"][0]["fac_qualite_2"]?>" class="w-15" />
						<?php		}else{
										echo("&nbsp;");
									} ?>
								</td>
							</tr>
						</table>
					</div>
				</page_footer>
				
				<p style="text-align:justify;margin-top:2cm;" >
				Nous vous informons par la présente du changement de coordonnées bancaires de la société
				<?=$data["pages"][0]["fac_soc_nom"]?>. Tous les virements à venir doivent être effectués sur ce nouveau compte. À cet effet,
				nous vous prions de trouver ci-dessous le relevé d&#39;identité bancaire (RIB) du nouveau compte.
				Les avis de virements et les chèques doivent être transmis à l’adresse suivante :
				</p>
				
				<p class="text-center" >																		
	<?php			echo("<b>" . $data["pages"][0]["fac_soc_nom"] . "</b>");
					echo("<br/><b>Service comptabilité</b>");
					if(!empty($data["pages"][0]["fac_compta_ad1"])){
						echo("<br/>" . $data["pages"][0]["fac_compta_ad1"]);
					};
					if(!empty($data["pages"][0]["fac_compta_ad2"])){
						echo("<br/>" . $data["pages"][0]["fac_compta_ad2"]);
					};
					if(!empty($page["fac_compta_ad3"])){
						echo("<br/>" . $data["pages"][0]["fac_compta_ad3"]);
					};
					echo("<br/>" . $data["pages"][0]["fac_compta_cp"] . " " . $data["pages"][0]["fac_compta_ville"]); ?>
				</p>
				
				<p>Courriel : <?=$data["pages"][0]["fac_compta_mail"]?> </p>
				
				<p class="text-center" style="margin-top:2cm;" >
					<img src="Documents/Societes/Rib/rib_aff_<?=$acc_societe?>.png" />
				</p>
				
				<table class="w-100" >
					<tr>
						<td class="w-60" ></td>
						<td class="w-40" >La comptabilité</td>
					</tr>
				</table>	


			</page>
			
<?php	}	
	}

	$content=ob_get_clean();
	$resultat=true;
	/*echo($content);
	die();*/
	try{
		$pdf=new HTML2PDF('P','A4','fr');
		$pdf->pdf->SetDisplayMode('fullwidth');
		$pdf->writeHTML($content);
		if($pdf){
			if(count($data["pages"])==1){
				$pdf->Output($_SERVER["DOCUMENT_ROOT"] . '/Documents/Societes/' . $acc_societe . '/factures/' . $data["pages"][0]["fac_numero"] . '.pdf', 'F');		
			}
		}
	}catch(HTML2PDF_exception $e){
		$resultat=false;
	}
	
	return $resultat;
	
}
?>