<?php

 // PERMET DE METTRE A JOUR LE LIEN DE RECURRENCE ENTRE LES DIFFERENTES Actions
 
 function set_action_recurrente($action_client_id){
	 
	 echo("<br/><br/>");
	  
	echo("MODELE set_action_recurrente(" . $action_client_id . ")<br/>");
	 
	 global $ConnSoc;
	 global $Conn;
	 
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	
	$action_client_id=intval($action_client_id);
	
	if($acc_societe>0 AND $action_client_id>0){

		// SUR l'ACTION
		
		$sql="SELECT acl_client,acl_produit,acl_archive,acl_pro_recurrent,act_date_deb,acl_act_recurrent FROM Actions,Actions_Clients WHERE act_id=acl_action AND acl_id=:action_client;";
		$req=$ConnSoc->prepare($sql);	
		$req->bindParam(":action_client",$action_client_id);
		$req->execute();
		$action_client=$req->fetch();
	
		echo("action_client");
		echo("<pre>");
			print_r($action_client);
		echo("</pre>");

		
		//var_dump($action_client["acl_archive"]);
		//var_dump($action_client["acl_pro_recurrent"]);
		if($action_client["acl_archive"]==1 OR $action_client["acl_pro_recurrent"]!=1){
			
			echo("PAS DE RECURRENCE");
			
			if(!$action_client["acl_pro_recurrent"]){
				// L'action n'est plus recurrente
				$sql="UPDATE Actions_Clients SET acl_pro_recurrent=0, acl_act_recurrent=0 WHERE acl_id=:action_client;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":action_client",$action_client_id);
				$req->execute();
			}

			// L'action ne peut plus traiter de recurrence
			$sql="UPDATE Actions_Clients SET acl_act_recurrent=0 WHERE acl_act_recurrent=:action_client;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client",$action_client_id);
			$req->execute();
			
			
		}else{
			
			
			// GESTION DE LA RECURRENCE
		
			// SUR LE PRODUIT
				
			$sql="SELECT pro_recurrent,pro_recur_mois FROM Produits WHERE pro_id=:produit;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":produit",$action_client["acl_produit"]);
			$req->execute();
			$produit=$req->fetch();

			// LE CLIENT
			
			$sql="SELECT cli_code,cli_filiale_de,cli_filiale_de_soc FROM Clients WHERE cli_id=:client;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":client",$action_client["acl_client"]);
			$req->execute();
			$client=$req->fetch();
			if($client["cli_filiale_de"]>0){
				$base_produit_client=$client["cli_filiale_de"];
				$conn_get_id=$client["cli_filiale_de_soc"];
			}else{
				$base_produit_client=$action_client["acl_client"];
				$conn_get_id=$acc_societe;
			}
			
			// LA PERIODE DE RECURRENCE
			
			$mois_recurrent=0;
		
			try {
				$ConnGet = new PDO('mysql:host=127.0.0.1;port=3306;dbname=orion_' . $conn_get_id, 'root', '');
				$ConnGet->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$ConnGet->exec("SET NAMES 'UTF8'");
			} catch (PDOException $e) {
				die ("Erreur ! : " . $e->getMessage());
			};
			
			echo("conn_get_id : " . $conn_get_id . "<br/>");
			echo("base_produit_client : " . $base_produit_client . "<br/>");
			
			$sql="SELECT cpr_recur_mois FROM Clients_Produits WHERE cpr_client=" . $base_produit_client .  ";";
			$req=$ConnGet->query($sql);
			$produit_client=$req->fetch();
			if(!empty($produit_client)){			
				$mois_recurrent=intval($produit_client["cpr_recur_mois"]);
				if($mois_recurrent==0){
					// securite au cas ou la recurence perso est à 0
					$mois_recurrent=intval($produit["pro_recur_mois"]);
				}
			}else{
				echo("Periode par defaut<br/>");
				$mois_recurrent=intval($produit["pro_recur_mois"]);
			}
		
			echo("mois_recurrent");
			var_dump($mois_recurrent);
			echo("<br/>");
			
			$interval="P" . ($mois_recurrent+1) . "M";
			$date_deb=date_create_from_format('Y-m-d',$action_client["act_date_deb"]);
			
			// preparation des requetes
			$sql="UPDATE Actions_Clients SET acl_act_recurrent=:act_reccurent WHERE acl_id=:action_client;";			
			$req_maj=$ConnSoc->prepare($sql);
			
		
			// action precedente recurrence traitée par $action_client
			
			$date_recur_prec=date_create_from_format('Y-m-d',$action_client["act_date_deb"]);
			$date_recur_prec->sub(new DateInterval($interval));
				
			$act_recurrent=0;
			$sql="SELECT acl_id,acl_produit,acl_client,act_date_deb,acl_act_recurrent FROM Actions_Clients,Actions
			WHERE act_id=acl_action AND 
			(acl_act_recurrent=:action_client 
			OR (acl_produit=:produit AND acl_client=:client AND act_date_deb>=:date_deb AND act_date_deb<:date_action)
			) ORDER BY act_date_deb DESC ;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client",$action_client_id);
			$req->bindParam(":produit",$action_client["acl_produit"]);
			$req->bindParam(":client",$action_client["acl_client"]);
			$req->bindParam(":date_deb",$date_recur_prec->format("Y-m-d"));
			$req->bindParam(":date_action",$date_deb->format("Y-m-d"));
			$req->execute();
			$prec=$req->fetchAll();
			if(!empty($prec)){
				$act_recurrent=0;
				echo("prec");
				echo("<pre>");
					print_r($prec);
				echo("</pre>");
				foreach($prec as $p){
					if($p["acl_act_recurrent"]==$action_client_id){
						
						// la recurrence est déjà enregsitré	
						if($act_recurrent>0 OR $p["acl_produit"]!=$action_client["acl_produit"] OR $p["acl_client"]!=$action_client["acl_client"] OR $p["act_date_deb"]<$date_recur_prec){
							// une recurrence plus recente a ete trouve ou la recurrence enregistre n'est plus valide
							$val_recurrence=0;
							$req_maj->bindParam(":act_reccurent",$val_recurrence);
							$req_maj->bindParam(":action_client",$p["acl_id"]);
							$req_maj->execute();	
						}else{
							// la recurrence est toujours valable
							$act_recurrent=$p["acl_id"];
						}
						
					}elseif($act_recurrent==0){
						
						$req_maj->bindParam(":act_reccurent",$action_client_id);
						$req_maj->bindParam(":action_client",$p["acl_id"]);
						$req_maj->execute();	
						$act_recurrent=$p["acl_id"];
					}
					
				}
			}
			
			// action suivante qui traite la recurrence de $action_client_id
			
			//echo("ACTION QUI TRAITE LA RECURENCE<br/>");
			
			$date_recur_suiv=date_create_from_format('Y-m-d',$action_client["act_date_deb"]);
			$date_recur_suiv->add(new DateInterval($interval));
				
			//var_dump($date_recur_suiv->format("Y-m-d"));
			$act_recurrent=0;
			
			$sql="SELECT acl_id,acl_produit,acl_client,act_date_deb,acl_act_recurrent FROM Actions_Clients,Actions
			WHERE act_id=acl_action AND 
			(acl_id=:act_reccurent 
			OR (acl_produit=:produit AND acl_client=:client AND act_date_deb<=:date_deb AND act_date_deb>:date_action)
			) ORDER BY act_date_deb;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":act_reccurent",$action_client["acl_act_recurrent"]);
			$req->bindParam(":produit",$action_client["acl_produit"]);
			$req->bindParam(":client",$action_client["acl_client"]);
			$req->bindParam(":date_deb",$date_recur_suiv->format("Y-m-d"));
			$req->bindParam(":date_action",$date_deb->format("Y-m-d"));
			$req->execute();
			$suiv=$req->fetchAll();
			
			echo("<br/>");
			echo("act_reccurent : " . $action_client["acl_act_recurrent"] . "<br/>");
			echo("produit : " . $action_client["acl_produit"] . "<br/>");
			echo("client : " . $action_client["acl_client"] . "<br/>");
			echo("date_deb : " . $date_recur_suiv->format("Y-m-d") . "<br/>");
			echo("date_action : " . $date_deb->format("Y-m-d") . "<br/>");
			
			if(!empty($suiv)){
				echo("Suiv");
				echo("<pre>");
					print_r($suiv);
				echo("</pre>");
				foreach($suiv as $s){
					if($s["acl_id"]==$action_client["acl_act_recurrent"]){
						
						// il s'agit de l'action qui traite la recurence de l'action modifié	
						if($act_recurrent>0 OR $s["acl_produit"]!=$action_client["acl_produit"] OR $s["acl_client"]!=$action_client["acl_client"] OR $s["act_date_deb"]>$date_recur_suiv){
							// une recurrence plus recente a ete trouve ou la recurrence enregistre n'est plus valide
							$act_recurrent=0;
						}else{
							// la recurrence est toujours valable
							$act_recurrent=$action_client["acl_act_recurrent"];
						}
						
					}elseif($act_recurrent==0){
						$act_recurrent=$s["acl_id"];
					}
					
				}
			}else{
				echo("ACTION SUIVNATE NON TROUVE<br/>");	
			}
			
			echo("act_reccurent<br/>");
			var_dump($act_recurrent);
			echo("<br/>");
			
			echo("acl_act_recurrent<br/>");
			var_dump($action_client["acl_act_recurrent"]);
			echo("<br/>");
			
			if($act_recurrent!=$action_client["acl_act_recurrent"]){
				$req_maj->bindParam(":act_reccurent",$act_recurrent);
				$req_maj->bindParam(":action_client",$action_client_id);
				$req_maj->execute();		
			}
		}
	}
	//die();
}
?>