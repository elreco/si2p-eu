<?php

function get_utilisateur_societes($param_uti){
	
	global $Conn;
	
	$d_societes=array();
	
	// intval

	$utilisateur=0;
	if(intval($param_uti)){
		$utilisateur = intval($param_uti);
	}

	if($utilisateur > 0){
			
			$req=$Conn->query("SELECT DISTINCT soc_id,soc_code,soc_nom FROM Societes LEFT JOIN Utilisateurs_Societes ON uso_societe = soc_id WHERE uso_utilisateur = " . $utilisateur . " AND soc_archive=0 ORDER BY soc_nom;");
			$req->execute();
			$d_societes=$req->fetchAll();

			
		
	}
	
	return $d_societes;
}
?>