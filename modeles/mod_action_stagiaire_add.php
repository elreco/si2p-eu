<?php

// INSCRIPTION D'UN STAGIARE SUR UNE ACTION

/*
option $session_id >0 le stagiaire sera inscrit sur une session spé sinon le stagiaire est inscrit sur tt les dates

auteur FG 28/10/2016


*/
function add_action_stagiaire($stagiaire,$action,$action_client,$sessions,$confirme,$attestation,$qualification){
	
	global $ConnSoc;
	
	$erreur_txt="";
	
	if($stagiaire>0 AND $action>0 AND $action_client>0){
		
		$attestation_ok=0;
		if(!empty($attestation)){
			$attestation_ok=1;
		}
		
		$sql="SELECT ast_stagiaire FROM Actions_Stagiaires WHERE ast_stagiaire=:stagiaire AND ast_action=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":stagiaire",$stagiaire);
		$req->bindParam(":action",$action);
		$req->execute();
		$result=$req->fetch();
		if(empty($result)){
			// LE STAGIAIRE N4EST PAS ENCORE INSCRIT
			
			// INSCRIPTION DU STAGIAIRE
			$sql="INSERT INTO Actions_Stagiaires (ast_stagiaire,ast_action,ast_action_client,ast_confirme,ast_attestation,ast_valide,ast_attest_ok,ast_qualification) 
			VALUES (:stagiaire,:action,:action_client,:ast_confirme,:ast_attestation,0,:attestation_ok,:qualification);";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":stagiaire",$stagiaire);
			$req->bindParam(":action",$action);
			$req->bindParam(":action_client",$action_client);
			$req->bindParam(":ast_attestation",$attestation);
			$req->bindParam(":ast_confirme",$confirme);
			$req->bindParam(":attestation_ok",$attestation_ok);
			$req->bindParam(":qualification",$qualification);
			try{
				$req->execute();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
			}
		}else{
			$erreur_txt="Le stagiaire est déjà inscrit";
		}
		
		/*
		ANCIEN SYSTEME OU LES INSCRIPTIONS SURCLASSAIT LA SAISIE SUR ACTION_CLIENT
		-> DESORMAIS L'ACTUALISATION DOIT SE FAIRE VIA UNE INTERRATION DE L'UTILISATEUR -> PLUS SYSTEMATIQUE
		if(empty($erreur_txt) AND $confirme==1){
			
			// SI INSCRIPTION CONFIMER ON ACTUALISE LE NOMBRE DE STAGIAIRE
			
			$sql="SELECT COUNT(ast_stagiaire) as nb_sta FROM Actions_Stagiaires WHERE ast_action_client=:action_client AND ast_confirme;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client",$action_client);
			$req->execute();
			$d_inscription=$req->fetch();
			if(!empty($d_inscription)){
				if(!empty($d_inscription["nb_sta"])){
					$acl_for_nb_sta=$d_inscription["nb_sta"];
				}						
			}
			
			$sql="SELECT acl_pro_inter,acl_for_nb_sta,acl_ca_unit FROM Actions_CLients WHERE acl_id=:action_client;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client",$action_client);
			$req->execute();
			$d_action_client=$req->fetch();
			if(!empty($d_action_client)){
				
				if(!isset($acl_for_nb_sta)){
					$acl_for_nb_sta=$d_action_client["acl_for_nb_sta"];
				}
				
				if($d_action_client["acl_pro_inter"]==1){
					$ca=$d_action_client["acl_ca_unit"]*$acl_for_nb_sta;
				}					
			}
			// ON MET A JOUR LE CA SI ¨PRODUIT INTER
			$sql="UPDATE Actions_Clients SET acl_for_nb_sta=" . $acl_for_nb_sta;
			if($d_action_client["acl_pro_inter"]==1){
				$sql.=",acl_ca=" . $ca;
			}
			$sql.=" WHERE acl_id=:action_client;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client",$action_client);
			$req->execute();
		}*/
		
		if(empty($erreur_txt)){

			// INSCRIPTION AUX SESSIONS
			
			$sql_verif_session="SELECT ass_stagiaire FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire AND ass_session=:session;";
			$verif_session=$ConnSoc->prepare($sql_verif_session);
			
			$sql_ins_session="INSERT INTO Actions_Stagiaires_Sessions (ass_session,ass_stagiaire,ass_action) VALUES (:session, :stagiaire, :action);";
			$ins_session=$ConnSoc->prepare($sql_ins_session);

			$tab_sessions=array();
			if(is_int($sessions)){
				// on a passé un id de session precis -> on l'inscrit a cette session précise
				if($sessions>0){				
					$tab_sessions[0]["ase_id"]=$sessions;

				}else{
					// on a passé 0 -> on l'inscrit a toutes les sessions auxquelle participe le client
					$sql="SELECT ase_id FROM Plannings_Dates,Actions_Sessions,Actions_Clients_Dates WHERE pda_id=ase_date AND ase_date=acd_date AND acd_action_client=:action_client;";
					$req=$ConnSoc->prepare($sql);
					$req->bindParam(":action_client",$action_client);
					$req->execute();
					$tab_sessions=$req->fetchAll();
				}
			}elseif(is_array($sessions)){
				// on a passe un tableau -> on inscrit au session contenu dans le tableau
				$tab_sessions=$sessions;
			}else{
				$erreur_txt="L'inscription aux sessions n'a pas fonctionnée.";
			}
			
			
			
			if(!empty($tab_sessions)){
				foreach($tab_sessions as $s){
					$verif_session->bindParam(":session",$s["ase_id"]);
					$verif_session->bindParam(":stagiaire",$stagiaire);
					$verif_session->execute();
					$result=$verif_session->fetch();
					
					if(empty($result)){
						
						$ins_session->bindParam(":session",$s["ase_id"]);
						$ins_session->bindParam(":stagiaire",$stagiaire);
						$ins_session->bindParam(":action",$action);
						$ins_session->execute();
					}
				}
			}
		}
	}else{
		$erreur_txt="Formulaire incomplet!";
	}
		
	
	return $erreur_txt;

}
?>
