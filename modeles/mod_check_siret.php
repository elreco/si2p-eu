<?php
	
	// PERMET DE VERIFIER SI UN SIRET EXITS DEJA DANS LA BASE CLIENT
	

	function check_siret($siren,$siret,$adresse){
		
		global $Conn;
		
		if(empty($siren) OR empty($siret)){
			return false;
		}
		
		if(!empty($adresse)){
			$adresse=intval($adresse);
		}else{
			$adresse=0;
		}
		
		$client=0;
		$cle=0;
		$d_doublon=array();
		
		// TEST SIRET SUR BASE 0
		$sql="SELECT cli_id,cli_code,cli_nom,adr_id,soc_nom,age_nom FROM Clients LEFT JOIN Adresses ON (Clients.cli_id=Adresses.adr_ref_id)
		LEFT OUTER JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client) 
		LEFT OUTER JOIN Societes ON (Clients_Societes.cso_societe=Societes.soc_id) 
		LEFT OUTER JOIN Agences ON (Clients_Societes.cso_agence=Agences.age_id)
		WHERE cli_siren LIKE :siren AND adr_siret LIKE :siret";
		if(!empty($adresse)){
			// test client ID se refere a une adresse client
			$sql.=" AND NOT adr_id=" . $adresse; 
		}
		$sql.=" ORDER BY cli_code,cli_id,soc_nom,age_nom;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":siren",$siren);
		$req->bindParam(":siret",$siret);
		$req->execute();
		$d_result=$req->fetchAll();
		foreach($d_result as $d){
			
			if($client!=$d["cli_id"]){
				$cle++;
				$d_doublon[$cle]=array(
					"source" => "cli",
					"code" => $d["cli_code"],
					"nom" => $d["cli_nom"],
					"societes" => array()
				);
				$client=$d["cli_id"];
			}
			$lib=$d["soc_nom"];
			if(!empty($d["age_nom"])){
				$lib.=$d["age_nom"];
			}
			$d_doublon[$cle]["societes"][]=$lib;
			
		}
		
		return $d_doublon;	
	}
?>
	