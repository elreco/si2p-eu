<?php

/*
	SUPPRESION DE L'INSCRIPTION DE TOUT LES STAGIAIRES D'UNE FORMATION

option $action_client >0 permet de restreindre la suppresion des stagiaires sur un client donnée

auteur FG 28/10/2016


*/
function del_action_stagiaires($action,$action_client){

	global $ConnSoc;

	// securité

	$action=intval($action);
	$action_client=intval($action_client);

	$retour=true;

	if($action>0){

		if($action_client>0){

			// SUPPRESSION DE L'INSCRIPTION DES STAGIAIRES AUX SESSIONS

			$sql="DELETE FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire AND ass_action=:action;";
			$req_supp_session=$ConnSoc->prepare($sql);

			$sql="SELECT ast_stagiaire FROM Actions_Stagiaires WHERE ast_action_client=:action_client;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client",$action_client);
			$req->execute();
			$stagiaires=$req->fetchAll();
			if(!empty($stagiaires)){
				foreach($stagiaires as $s){
					$req_supp_session->bindParam(":stagiaire",$s["ast_stagiaire"]);
					$req_supp_session->bindParam(":action",$action);
					$req_supp_session->execute();
				}
			}

		}else{
			$sql="DELETE FROM Actions_Stagiaires_Sessions WHERE ass_action=:action;";
			$req_supp_session=$ConnSoc->prepare($sql);
			$req_supp_session->bindParam(":action",$action);
			$req_supp_session->execute();
		}

		// SUPPRESSION DE L'INSCRIPTION DES STAGIAIRES à LA FORMATION

		$sql="DELETE FROM Actions_Stagiaires WHERE ast_action=:action";
		if($action_client>0){
			$sql.=" AND ast_action_client=:action_client";
		}
		$sql.=";";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$action);
		if($action_client>0){
			$req->bindParam(":action_client",$action_client);
		}
		$req->execute();

		// MISE A JOUR DU CA INTER
		$sql="UPDATE Actions_Clients SET acl_ca=0 WHERE acl_action=:action AND acl_pro_inter=1";
		if($action_client>0){
			$sql.=" AND acl_id=:action_client";
		}
		$sql.=";";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$action);
		if($action_client>0){
			$req->bindParam(":action_client",$action_client);
		}
		$req->execute();

		//MISE A JOUR DU NOMBRE D'INSCRIT

		$nb_stagiaire=0;
		$nb_stagiaire_resa=0;
		if($action_client>0){
			$sql="SELECT COUNT(ast_stagiaire),ast_confirme FROM Actions_Stagiaires WHERE ast_action=" . $action . ";";
			$req=$ConnSoc->query($sql);
			$inscrit=$req->fetchAll();
			if(!empty($inscrit)){
				foreach($inscrit as $i){
					if($i["ast_confirme"]){
						$nb_stagiaire=$i[0];
					}else{
						$nb_stagiaire_resa=$i[0];
					}
				}
			}
		}
		// $sql="UPDATE Actions SET act_nb_stagiaire=" . $nb_stagiaire . ", act_nb_stagiaire_resa=" . $nb_stagiaire_resa . " WHERE act_id=" . $action . ";";
		// $req=$ConnSoc->query($sql);


	}else{
		$retour=false;
	}

	return $retour;

}
?>
