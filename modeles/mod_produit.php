<?php

function get_produits_codes($pro_id, $pro_code_produit)
{

    global $Conn;
    if($pro_id != 0){
        $req = $Conn->prepare("SELECT pro_id, pro_code_produit FROM produits WHERE pro_code_produit LIKE :pro_code_produit AND pro_id !=" . $pro_id);
    }else{
        $req = $Conn->prepare("SELECT pro_id, pro_code_produit FROM produits WHERE pro_code_produit LIKE :pro_code_produit");
    }
    
    $req->bindParam(':pro_code_produit', $pro_code_produit);
    $req->execute();

    $produits = $req->fetch();

    return $produits;
}


function insert_produit()
{
}

function get_produits()
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits");
    $req->execute();

    $produits = $req->fetchAll();

    return $produits;
}

function get_produits_code()
{

    global $Conn;

    $req = $Conn->prepare("SELECT pro_id, pro_code_produit FROM produits");
    $req->execute();

    $produits = $req->fetchAll();

    return $produits;
}
function get_document_produit($dpr_produit,$dpr_document)
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM documents_produits WHERE dpr_produit = :dpr_produit AND dpr_document = :dpr_document");
    $req->bindParam(':dpr_produit', $dpr_produit);
    $req->bindParam(':dpr_document', $dpr_document);
    $req->execute();

    $produits = $req->fetch();

    return $produits;
}
function get_documents_produits($dpr_document)
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM documents_produits WHERE dpr_document = :dpr_document");
    $req->bindParam(':dpr_document', $dpr_document);
    $req->execute();

    $produits = $req->fetchAll();

    return $produits;
}

function insert_document_produit($dpr_produit,$dpr_document){

    global $Conn;       
    $req = $Conn->prepare("INSERT INTO documents_produits (dpr_produit, dpr_document) VALUES (:dpr_produit, :dpr_document)");
    $req->bindParam(':dpr_produit', $dpr_produit);
    $req->bindParam(':dpr_document', $dpr_document);
    $req->execute();
    
}

function delete_document_produit($dpr_produit,$dpr_document){

    global $Conn;

    $req = $Conn->prepare("DELETE FROM documents_produits WHERE dpr_produit = :dpr_produit AND dpr_document=:dpr_document ");
    $req->bindParam(':dpr_produit', $dpr_produit);
    $req->bindParam(':dpr_document', $dpr_document);
    $req->execute();

}


function insert_produit_modele($pmo_nom, $pmo_famille, $pmo_sous_famille, $pmo_qualification){

    global $Conn;       
    $req = $Conn->prepare("INSERT INTO produits_modeles (pmo_nom, pmo_famille, pmo_sous_famille,pmo_qualification) VALUES (:pmo_nom, :pmo_famille, :pmo_sous_famille, :pmo_qualification)");
    $req->bindParam(':pmo_nom', $pmo_nom);
    $req->bindParam(':pmo_famille', $pmo_famille);
    $req->bindParam(':pmo_sous_famille', $pmo_sous_famille);
    $req->bindParam(':pmo_qualification', $pmo_qualification);
    $req->execute();
    
}

function get_produit($id)
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits WHERE pro_id=" . $id);
    $req->execute();

    $produit = $req->fetch();

    return $produit;
}
function get_produit_modeles()
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_modeles");
    $req->execute();

    $produits = $req->fetchAll();

    return $produits;
}
function get_produit_modele($id)
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM produits_modeles WHERE pmo_id = :id");
    $req->bindParam(':id', $id);

    $req->execute();
    $produit = $req->fetch();

    return $produit;
}
function update_produit_modele($id,$pmo_nom, $pmo_famille, $pmo_sous_famille, $pmo_qualification)
{

    global $Conn;

    $req = $Conn->prepare("UPDATE produits_modeles SET pmo_nom = :pmo_nom, pmo_famille = :pmo_famille, pmo_sous_famille = :pmo_sous_famille, pmo_qualification = :pmo_qualification WHERE pmo_id = :id");
    $req->bindParam(':id', $id);
    $req->bindParam(':pmo_nom', $pmo_nom);
    $req->bindParam(':pmo_famille', $pmo_famille);
    $req->bindParam(':pmo_sous_famille', $pmo_sous_famille);
    $req->bindParam(':pmo_qualification', $pmo_qualification);
    $req->execute();
    
}

function get_produit_select($selected)
{

    global $Conn;

    $service_select = "";
    $req            = $Conn->query("SELECT * FROM produits ORDER BY soc_nom");
    while ($donnees = $req->fetch()) {
        if($selected == 0){
            $service_select = $service_select . "<option value='" . $donnees["pro_id"] . "' >" . $donnees["pro_libelle"] . "</option>";
        }else{
            if($donnees['pro_id'] == $selected){
                $service_select = $service_select . "<option selected value='" . $donnees["pro_id"] . "' >" . $donnees["pro_libelle"] . "</option>";
            }else{
                $service_select = $service_select . "<option value='" . $donnees["pro_id"] . "' >" . $donnees["pro_libelle"] . "</option>";
            }

        }
        
    }
    ;
    return $service_select;

}
