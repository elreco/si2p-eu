<?php


	function demande_garanties(){

		global $Conn;
		global $ConnSoc;
		global $acc_societe;
        global $acc_agence;

		$erreur_txt="";

		// la societe

		$sql="SELECT soc_nom,soc_tva,soc_contrat_aff FROM Societes WHERE soc_id=" . $acc_societe . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();

		$vide=" ";

		// CREATION DU FICHIER

		// on supp l'ancien fichier
		if(file_exists("documents/Societes/" . $acc_societe . "/garanties.txt")){
			unlink("documents/Societes/" . $acc_societe . "/garanties.txt");
		}
		$file = fopen("documents/Societes/" . $acc_societe . "/garanties.txt", "w");

		// ECRITURE DE LA PREMIERE LIGNE

		$code="107";
		$code_vendeur=$d_societe["soc_contrat_aff"];
		$nom_vendeur=mb_str_pad($d_societe["soc_nom"],40," ");
		$date_fichier="";
		if(!empty($aff_date)){
			$DT_aff_date=date_create_from_format('Y-m-d',$aff_date);
			if(!is_bool($DT_aff_date)){
				$date_fichier=$DT_aff_date->format("Ymd");
			}
		}
		$resp_nom=mb_str_pad($vide,40,' ',STR_PAD_RIGHT);
		$resp_tel=mb_str_pad($vide,10,' ',STR_PAD_RIGHT);
		$resp_fax=mb_str_pad($vide,10,' ',STR_PAD_RIGHT);
		$client_ident="1";
		$codif_pays="2";

		$tva_vendeur=$d_societe["soc_tva"];
		$tva_vendeur=mb_str_pad($tva_vendeur,16,' ',STR_PAD_RIGHT);

		$num_fichier=mb_str_pad($vide,3,'0',STR_PAD_LEFT);

		$zone_libre=mb_str_pad($vide,213,' ',STR_PAD_RIGHT);
		$devise="EUR";
		$ope_type="000000";

		$ligne=$code . $code_vendeur . $nom_vendeur . $date_fichier . $resp_nom . $resp_tel . $resp_fax . $client_ident . $codif_pays . $tva_vendeur . $num_fichier;
		$ligne.=$zone_libre . $devise . $ope_type . "\r\n";
		$ligne = iconv( mb_detect_encoding( $ligne ), 'Windows-1252', $ligne );
		fwrite($file, $ligne);
		// AJOUT DES FACTURES

		// var pour dernière ligne
		$nb_facture=0;
		$total_facture=0;
		$nb_avoir=0;
		$total_avoir=0;
		$nb_reg=0;
		$total_reg=0;
		$nb_ajust=0;
		$total_ajust=0;
        $nb_garantie=0;
        $total_garantie=0;
        $erreur="";

		// Chercher les factures affacturables
		// requete prep
        // aller chercher la date il y a un an
        $date = strtotime('-1 year');
        $date = date('Y-m-d', $date);
        $ca = array();
		var_dump($date);

		$sql="SELECT SUM(fac_total_ttc) AS fac_total_ttc, fac_client FROM Factures,Clients
		WHERE fac_client=cli_id AND fac_agence=cli_agence
		AND fac_date>='" . $date . "' AND cli_interco_soc=0 AND NOT cli_categorie=3 AND NOT cli_sous_categorie=5
		AND NOT cli_archive
		GROUP BY fac_client";
		$req=$ConnSoc->query($sql);
		$result_facture=$req->fetchAll();

		
		$new_ca = array();
        foreach($result_facture as $clid){

            $sql="SELECT cli_siren, cli_groupe,adr_id, cli_filiale_de, cli_code, adr_siret, cli_id, cli_nom, adr_ad1,adr_ad2,adr_ad3,adr_nom,adr_cp,adr_ville
            FROM Clients LEFT OUTER JOIN Adresses ON (Adresses.adr_ref_id = Clients.cli_id   AND adr_defaut = 1 AND adr_type=2)
			WHERE cli_id = " . $clid['fac_client'];
            $req=$Conn->query($sql);
            $cli_siren=$req->fetch();

            if(!empty($cli_siren['cli_siren'])){
				// SI c4EST UNE FILIALE ==> ALLER CHERCHER LA MAISON MERE
				// LE GROUPE EST MULTI-SIREN ==> MESSAGE D'ERREUR
				$cli_siren['cli_siren'] = preg_replace('/\s/','', $cli_siren['cli_siren']);
                $cli_siren['cli_siren'] = str_replace(' ','', $cli_siren['cli_siren']);
				
				$cli_siren['adr_siret'] = preg_replace('/\s/', '', $cli_siren['adr_siret']);

				if(empty($cli_siren['cli_groupe'])){
					// ENTREPRISE SEUL
					if($clid["fac_total_ttc"] >= 30416){

						if(strlen($cli_siren['cli_siren'])==9 AND strlen($cli_siren['adr_siret'])==5){
							// premiere fois que l'on trouve le siren
							if(empty($new_ca[$cli_siren['cli_siren']])){
								$new_ca[$cli_siren['cli_siren']]["ca"] = $clid["fac_total_ttc"];
								$new_ca[$cli_siren['cli_siren']]["siret"] = $cli_siren['adr_siret'];
								$new_ca[$cli_siren['cli_siren']]["client"] = $cli_siren['cli_id'];
								$new_ca[$cli_siren['cli_siren']]["adresse"] = $cli_siren['adr_id'];
								$new_ca[$cli_siren['cli_siren']]["cli_nom"] = $cli_siren['cli_nom'];
								$new_ca[$cli_siren['cli_siren']]["cli_code"] = $cli_siren['cli_code'];
								$new_ca[$cli_siren['cli_siren']]["adr_nom"] = $cli_siren['adr_nom'];
								$new_ca[$cli_siren['cli_siren']]["adr_ad1"] = $cli_siren['adr_ad1'];
								$new_ca[$cli_siren['cli_siren']]["adr_ad2"] = $cli_siren['adr_ad2'];
								$new_ca[$cli_siren['cli_siren']]["adr_ad3"] = $cli_siren['adr_ad3'];
								$new_ca[$cli_siren['cli_siren']]["adr_cp"] = $cli_siren['adr_cp'];
								$new_ca[$cli_siren['cli_siren']]["adr_ville"] = $cli_siren['adr_ville'];
							}else{
								$erreur.="Le SIREN de cette entreprise est déjà utilisé (" .$cli_siren['cli_nom'] . ")<br/>";
							}
						}

		            }
				}elseif(!empty($cli_siren['cli_groupe']) AND $cli_siren['cli_filiale_de'] == 0){
					// MM
					if(strlen($cli_siren['cli_siren'])==9 AND strlen($cli_siren['adr_siret'])==5){
						if(empty($new_ca[$cli_siren['cli_siren']])){
							$new_ca[$cli_siren['cli_siren']]["ca"] = $clid["fac_total_ttc"];
							$new_ca[$cli_siren['cli_siren']]["siret"] = $cli_siren['adr_siret'];
							$new_ca[$cli_siren['cli_siren']]["client"] = $cli_siren['cli_id'];
							$new_ca[$cli_siren['cli_siren']]["adresse"] = $cli_siren['adr_id'];
							$new_ca[$cli_siren['cli_siren']]["cli_nom"] = $cli_siren['cli_nom'];
							$new_ca[$cli_siren['cli_siren']]["cli_code"] = $cli_siren['cli_code'];
							$new_ca[$cli_siren['cli_siren']]["adr_nom"] = $cli_siren['adr_nom'];
							$new_ca[$cli_siren['cli_siren']]["adr_ad1"] = $cli_siren['adr_ad1'];
							$new_ca[$cli_siren['cli_siren']]["adr_ad2"] = $cli_siren['adr_ad2'];
							$new_ca[$cli_siren['cli_siren']]["adr_ad3"] = $cli_siren['adr_ad3'];
							$new_ca[$cli_siren['cli_siren']]["adr_cp"] = $cli_siren['adr_cp'];
							$new_ca[$cli_siren['cli_siren']]["adr_ville"] = $cli_siren['adr_ville'];
						}else{
							if($cli_siren['cli_id']==$new_ca[$cli_siren['cli_siren']]["client"]){
								$new_ca[$cli_siren['cli_siren']]["ca"] = $clid["fac_total_ttc"];
							}else{
								$erreur.="Le SIREN de ce groupe est déjà utilisé (" .$cli_siren['cli_nom'] . ")<br/>";
							}
						}
					}
				}else{
					// FILIALE

					$sql="SELECT cli_siren, cli_groupe,adr_id, cli_filiale_de, cli_code, adr_siret, cli_id, cli_nom, adr_ad1,adr_ad2,adr_ad3,adr_nom,adr_cp,adr_ville
					FROM Clients LEFT OUTER JOIN Adresses ON (Adresses.adr_ref_id = Clients.cli_id   AND adr_defaut = 1 AND adr_type=2)
					WHERE cli_id = " . $cli_siren['cli_filiale_de'];
					$req=$Conn->query($sql);
					$maison_mere=$req->fetch();
					
					$maison_mere['cli_siren'] = preg_replace('/\s/', '', $maison_mere['cli_siren']);

					if($maison_mere['cli_siren'] == $cli_siren['cli_siren']){

						$maison_mere['adr_siret'] = preg_replace('/\s/', '', $maison_mere['adr_siret']);

						
						if(strlen($maison_mere['cli_siren'])==9 AND strlen($maison_mere['adr_siret'])==5){
							if(empty($new_ca[$maison_mere['cli_siren']]["ca"])){
								$new_ca[$maison_mere['cli_siren']]["ca"] = $clid["fac_total_ttc"];
								$new_ca[$maison_mere['cli_siren']]["siret"] = $maison_mere['adr_siret'];
								$new_ca[$maison_mere['cli_siren']]["client"] = $maison_mere['cli_id'];
								$new_ca[$maison_mere['cli_siren']]["adresse"] = $maison_mere['adr_id'];
								$new_ca[$maison_mere['cli_siren']]["cli_nom"] = $maison_mere['cli_nom'];
								$new_ca[$maison_mere['cli_siren']]["cli_code"] = $maison_mere['cli_code'];
								$new_ca[$maison_mere['cli_siren']]["adr_nom"] = $maison_mere['adr_nom'];
								$new_ca[$maison_mere['cli_siren']]["adr_ad1"] = $maison_mere['adr_ad1'];
								$new_ca[$maison_mere['cli_siren']]["adr_ad2"] = $maison_mere['adr_ad2'];
								$new_ca[$maison_mere['cli_siren']]["adr_ad3"] = $maison_mere['adr_ad3'];
								$new_ca[$maison_mere['cli_siren']]["adr_cp"] = $maison_mere['adr_cp'];
								$new_ca[$maison_mere['cli_siren']]["adr_ville"] = $maison_mere['adr_ville'];
							}elseif($maison_mere['cli_id']==$new_ca[$maison_mere['cli_siren']]["client"]){
								$new_ca[$cli_siren['cli_siren']]["ca"] = $clid["fac_total_ttc"];
							}else{
								$erreur.="Le SIREN de la holding " . $maison_mere['cli_code'] . " est déjà utilisé (" .$cli_siren['cli_code'] . ")<br/>";
							}
						}else{
							/*var_dump($maison_mere['cli_siren']);
							var_dump($maison_mere['adr_siret']);
							die();*/
							$erreur.="Le SIRET de la holding " . $maison_mere['cli_code'] . " est incorrect ou inexistant (" .$cli_siren['cli_code'] . ")<br/>";
						}
					}else{
						/*echo("HOLDING SIREN : ");
						var_dump($maison_mere['cli_siren']);
						echo("<br/>");
						echo("CLIENT SIREN : ");
						var_dump($cli_siren['cli_siren']);
						die();*/
						$erreur.="Le SIREN de la holding " . $maison_mere['cli_code'] . " (" . $maison_mere['cli_siren'] . ") est différent de celui du client " . $cli_siren['cli_code'] .  " (". $cli_siren['cli_siren'] . ")<br/>";
					}
				}
            }
        }

		if(!empty($erreur)){
			$erreur_txt.=$erreur;
		}
        // NE PAS INCLURE LES CLIENTS DONT LE CA EST INFERIEUR A 30416
        foreach($new_ca as $k=>$c){
            if($c["ca"] < 30416){
                unset($new_ca[$k]);
            }
        }

		// on passe en revu les facture selectionne par l'utilisateur



		foreach($new_ca as $siren => $c){

				$erreur="";
                $nb_garantie++;
				$code="107";

				$client_ident_1=$siren;
				$client_ident_2=$c["siret"];

				if(mb_strlen($c["cli_nom"]) > 40){
					$c["cli_nom"] = mb_substr($c["cli_nom"], 0, 40);
				}
				$entite_nom=mb_str_pad($c["cli_nom"],40,' ',STR_PAD_RIGHT);

				$entite_enseigne=mb_str_pad($vide,40,' ',STR_PAD_RIGHT);

				$entite_ad1="";
				$entite_ad2="";

				if(!empty($c["adr_ad1"])){
					$entite_ad1=$c["adr_ad1"];
					if(!empty($c["adr_ad2"])){
						$entite_ad2=$c["adr_ad2"];
					}elseif(!empty($c["adr_ad3"])){
						$entite_ad2=$c["adr_ad3"];
					}
				}elseif(!empty($c["adr_ad2"])){
					$entite_ad1=$c["adr_ad2"];
					if(!empty($c["adr_ad3"])){
						$entite_ad2=$c["adr_ad3"];
					}
				}

				if(!empty($entite_ad1)){
					if(strlen($entite_ad1)>40){
						$entite_ad1=mb_substr($entite_ad1,0,40);
					}
					if(strlen($entite_ad2)>40){
						$entite_ad2=mb_substr($entite_ad2,0,40);
					}
					$entite_ad1=mb_str_pad($entite_ad1,40,' ',STR_PAD_RIGHT);
					$entite_ad2=mb_str_pad($entite_ad2,40,' ',STR_PAD_RIGHT);
				}else{
					$erreur.="Adresse incompatible (" . $c["cli_code"] . ")<br/>";
				}

				if(!empty($c["adr_cp"])){
					if(strlen($c["adr_cp"])>6){
						$c["adr_cp"]=mb_substr($entite_cp,0,6);
					}
					$entite_cp=mb_str_pad($c["adr_cp"],6,' ',STR_PAD_RIGHT);
				}else{
					$erreur.="CP incompatible (" .  $c["cli_code"]  . ")<br/>";
				}

				if(!empty($c["adr_ville"])){
					if(strlen($c["adr_ville"])>34){
						$c["adr_ville"]=mb_substr($entite_ville,0,34);
					}
					$entite_ville=mb_str_pad($c["adr_ville"],34,' ',STR_PAD_RIGHT);
				}else{
					$erreur.="CP incompatible (" . $c["cli_code"] . ")<br/>";
				}

				$code_pays="FR ";

				$entite_tel=mb_str_pad($vide,10,' ',STR_PAD_RIGHT);

				if(!empty($c["cli_code"])){
					$entite_code_compta="411" . $c["cli_code"];
					if(strlen($entite_code_compta)>10){
						$entite_code_compta=mb_substr($entite_code_compta,0,10);
					}else{
						$entite_code_compta=mb_str_pad("411" . $c["cli_code"],10,' ',STR_PAD_RIGHT);
					}
				}else{
					$erreur.="CP incompatible (" . $c["cli_code"] . ")<br/>";
				}

				$piece_date=date("Ymd");
				$piece_montant="";
				if(!empty($c['ca'])){
					var_dump($c['ca']);
                    $piece_montant = ceiling(abs($c['ca']) * 60 / 365, 1000);
                    $total_garantie =$total_garantie+ $piece_montant;
					$piece_montant=mb_str_pad($piece_montant,15,0,STR_PAD_LEFT);
				}
				$devise_vide=mb_str_pad($vide,5,' ',STR_PAD_RIGHT);
                $fin_vide=mb_str_pad($vide,77,' ',STR_PAD_RIGHT);
				$piece_ope="DGA";

				if(empty($erreur)){

					$ligne=$code . $date_fichier . $code_vendeur . $client_ident_1 . $client_ident_2 . $entite_nom . $entite_enseigne . $entite_ad1 . $entite_ad2 . $entite_cp;
					$ligne.=$entite_ville . $code_pays . $entite_tel . $entite_code_compta . $piece_date . $piece_montant . $devise . $fin_vide .$piece_ope."\r\n";
					$ligne = iconv( mb_detect_encoding( $ligne ), 'Windows-1252//IGNORE', $ligne );
					fwrite($file, $ligne);

				}



				if(!empty($erreur)){
					$erreur_txt.=$erreur;
				}

			}
		// ON ECRIT LA DERNIERE LIGNE
		$code="199";

		$nb_facture=mb_str_pad($nb_facture,4,'0',STR_PAD_LEFT);

		$total_facture=number_format($total_facture,2,'','');
		$total_facture=mb_str_pad($total_facture,15,'0',STR_PAD_LEFT);

		$nb_avoir=mb_str_pad($nb_avoir,4,'0',STR_PAD_LEFT);
		$total_avoir=number_format(abs($total_avoir),2,'','');
		$total_avoir=mb_str_pad($total_avoir,15,'0',STR_PAD_LEFT);

		$nb_reg=mb_str_pad($nb_reg,4,'0',STR_PAD_LEFT);
		$total_reg=number_format(abs($total_reg),2,'','');
		$total_reg=mb_str_pad($total_reg,15,'0',STR_PAD_LEFT);

		$nb_ajust=mb_str_pad($nb_ajust,4,'0',STR_PAD_LEFT);
		$total_ajust=number_format(abs($nb_ajust),2,'','');
		$total_ajust=mb_str_pad($total_ajust,15,'0',STR_PAD_LEFT);

		$nb_garantie=mb_str_pad($nb_garantie,4,'0',STR_PAD_LEFT);
		$total_garantie=number_format($nb_garantie,2,'','');
		$total_garantie=mb_str_pad("0",15,'0',STR_PAD_LEFT);

		$zone_libre=mb_str_pad($vide,199,' ',STR_PAD_RIGHT);

		$ligne=$code . $code_vendeur . $nom_vendeur . $date_fichier . $nb_facture .$total_facture . $nb_avoir . $total_avoir . $nb_reg . $total_reg . $nb_ajust . $total_ajust;
		$ligne.=$nb_garantie . $total_garantie . $zone_libre . $devise . $ope_type;
		$ligne = iconv( mb_detect_encoding( $ligne ), 'Windows-1252', $ligne );
		fwrite($file, $ligne);


		fclose($file);

		if(empty($erreur_txt)){
			return true;
		}else{
			return $erreur_txt;
		}
		
	} ?>
