<?php

// GENERE DES ATTESTATIONS EN PDF
require_once("../vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;

function model_stagiaire_attestation_pdf($d_attestations){

	global $acc_societe;
	
	$erreur="";
	ob_start(); ?>
	

		<style type="text/css" >
	<?php	require("../assets/skin/si2p/css/orion_pdf.css"); ?>
	* {font-family: Helvetica; }
		</style>

<?php	foreach($d_attestations as $p => $data){
			if(is_int($p)){

				foreach($data["pages"] as $page){ ?>

					<page backtop="80mm" backleft="0mm" backright="0mm" backbottom="8mm" >
					<page_header>
						<div class="text-center w-100" >
									<img src="../<?=$data["juridique"]["logo_1"]?>" style="max-height:150px;" />
								</div>
							<table class="w-100" style="margin-top:10px;margin-bottom:10px;">
								
								<tr>
										
									<td class="w-50" >
							<?php		echo("<b>" . $data["juridique"]["nom"] . "</b>");
										if(!empty($data["juridique"]["agence"])){
											echo("<br/><b>" . $data["juridique"]["agence"] . "</b>");
										};
										if(!empty($data["juridique"]["ad1"])){
											echo("<br/>" . $data["juridique"]["ad1"]);
										};
										if(!empty($data["juridique"]["ad2"])){
											echo("<br/>" . $data["juridique"]["ad2"]);
										};
										if(!empty($data["juridique"]["ad3"])){
											echo("<br/>" . $data["juridique"]["ad3"]);
										};
										echo("<br/>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"]); 
										
										if(!empty($data["juridique"]["tel"])){
											echo("<br/>Tél : " . $data["juridique"]["tel"]);
										};																		
										if(!empty($data["juridique"]["fax"])){
											echo("<br/>Fax : " . $data["juridique"]["fax"]);
										};
										if(!empty($data["juridique"]["site"])){
											echo("<br/>" . $data["juridique"]["site"]);
										}; ?>
									</td>
									<td class="w-50 va-t" >
										
								<?php	if(!empty($page["adresse_client"])){
											echo("<p class='mt25' >" . $page["adresse_client"] . "</p>");
										}; ?>
									</td>
								</tr>
							</table>
							<div class="text-center" style="margin-bottom:10px;">
								<h1 class="text-center" style="">ATTESTATION DE FIN DE FORMATION</h1>
								<p>Article L. 6353-1 du Code du Travail</p>
							</div>
						</page_header>
						<page_footer>
							<div class="footer text-center" ><?=$data["juridique"]["footer"]?></div>
						</page_footer>
					
						
						<div class="section" >
							<p>
								<?=$data["juridique"]["nom"]?> organisme de formation déclaré sous le n° <?=$data["juridique"]["num_existence"]?>, auprès du préfet de la région <?=$data["juridique"]["region_nom"]?>, atteste que :
							</p>
							<p class="text-center text-strong" ><b><?=$page["sta_nom"] . " " . $page["sta_prenom"]?></b></p>
							<p>a suivi la formation <?=$page["att_titre"]?> dispensée par <?=$data["juridique"]["nom"]?> :</p>
						</div>
						<div class="section" >
							<table class="w-100 " >
								<tr>
									<td class="w-25 text-right p5" ><b>Intitulé</b> :</td>
									<td class="w-75 text-strong p5" ><?=$page["att_titre"]?></td>
								</tr>
								<tr>
									<td class="w-25 text-right p5" style="vertical-align:top;" ><b>Rappel des objectifs</b> :</td>
									<td class="w-75 text-strong p5" ><?=strip_tags(trim($page["att_contenu"]),'<br>')?></td>								
								</tr>
					<?php		if(!empty($page["att_planning"])){ ?>
									<tr>
										<td class="w-25 text-right p5" ><b>Date(s) de formation</b> :</td>
										<td class="w-75 text-strong p5" ><?=$page["att_planning"]?></td>
									</tr>
					<?php		} ?>
								<tr>
									<td class="w-25 text-right p5" ><b>Durée</b> :</td>
									<td class="w-75 text-strong p5" ><?=$page["att_duree"]?></td>
								</tr>
							</table>
						</div>
						<div class="section" >
							<p>a été évalué(e), au regard des objectifs de formation rappelés ci-dessus :</p>
						</div>
					
						
						<table class="tab-border w-100" >
							<tr>
								<td class="text-center w-70 bt" rowspan="2" >Compétences ou connaissances visées</td>
								<td class="text-center w-30 bt" colspan="2" >
									Résultats à l’issue de la formation									
								</td>
							</tr>	
							<tr>
								<th class="text-center w-15" >Acquise</th>
								<th class="text-center w-15" >Reste à acquérir</th>
							</tr>
					<?php	if(!empty($page["competences"])){
								foreach($page["competences"] as $comp){ ?>
									<tr>														
										<td class="w-70 p5" ><?=$comp["comp_txt"]?></td>
						<?php			if($comp["comp_acquise"]){ ?>
											<td class="w-15 text-center" >
												<img src="../assets/img/checked.png" />
											</td>
											<td class="w-15 text-center" >
												<img src="../assets/img/unchecked.png" />
											</td>
						<?php			}else{ ?>
											<td class="w-15 text-center" >
												<img src="../assets/img/unchecked.png" />
											</td>
											<td class="w-15 text-center" >
												<img src="../assets/img/checked.png" />
											</td>
						<?php 			} ?>
									</tr>
					<?php		}
							}?>
						</table>
						<div class="section" style="padding-bottom:50px;" >
							<p>Fait à <?=$data["juridique"]["ville"]?>, le <?=utf8_encode($data["date_doc"])?></p>
							
							<table id="attest_signe" >
								<tr>
									<td class="w-60" >&nbsp;</td>
									<td>
								<?php	if(!empty($data["juridique"]["resp_ident"])){
											echo($data["juridique"]["resp_ident"]);
										}
										if(!empty($data["juridique"]["resp_fonction"])){
											echo("<br/>" . $data["juridique"]["resp_fonction"]);
										}
										if(!empty($data["juridique"]["resp_signature"])){ 
											echo("<br/>"); ?>
											<img src="../<?=$data["juridique"]["resp_signature"]?>" class="img-responsive" />
								<?php	}?>
									</td>
								</tr>
							</table>				
						</div>
						
					</page>
	<?php		}
				
			}
		} 

	$content=ob_get_clean();
	
	if(!empty($d_attestations["pdf"])){
		$nom_pdf=$d_attestations["pdf"];
	}else{
		$nom_pdf=$d_attestations[0]["pdf"];
	}

	try{
		$pdf=new HTML2PDF('P','A4','fr');
		$pdf->pdf->SetFont('courier', '', 14);
		$pdf->pdf->SetDisplayMode('fullwidth');
		//echo($content);
		$pdf->writeHTML($content);
		if($pdf){
			$pdf->Output($_SERVER["DOCUMENT_ROOT"] . '/Documents/Societes/' . $acc_societe . '/Attestations/' . $nom_pdf . '.pdf', 'F');					
		}
	}catch(HTML2PDF_exception $e){
		$erreur="Erreur lors de la génération du fichier PDF";
	}
	
	if(!empty($erreur)){
		return false;
	}else{
		return true;
	}
}
?>