<?php

// PERMET DE RECUPERER UN TABLEAU CONTENANT TOUT LES ELEMENTS ENFANTS D'UN ELEMENT GROUPE
// SANS L'ORGANISATION EN NIVEAU
function get_groupe_filiales($client){
	
	global $Conn;
	
	$array_groupe=array();
	$array_fil=array();
	
	$sql="SELECT cli_id,cli_filiale_de,cli_niveau,cli_fil_de
	,cli_code,cli_nom,cli_reference,cli_categorie
	FROM Clients WHERE cli_id=" . $client . ";";
	$req=$Conn->query($sql);
	$d_client=$req->fetch();
	if(!empty($d_client)){
		
		$niveau=$d_client["cli_niveau"];
		if($d_client["cli_filiale_de"]==0){
			$niveau=-1;
		};
		$array_groupe[$niveau][]=array(
			"cli_id" => $d_client["cli_id"],
			"cli_code" => $d_client["cli_code"],
			"cli_nom" => $d_client["cli_nom"],
			"cli_niveau" => $d_client["cli_niveau"],
			"cli_fil_de" => $d_client["cli_fil_de"]
		);
		
		
		/*echo("<pre>");
			print_r($array_groupe);
		echo("</pre>");
		die();*/
		

		while($niveau<=2){
			
			if(isset($array_groupe[$niveau])){
			
				$donnee_bcl=$array_groupe[$niveau];
				$niveau++;
				
				if(!empty($donnee_bcl)){
				
					foreach($donnee_bcl as $grp){
						
						$sql="SELECT cli_id,cli_filiale_de,cli_niveau,cli_fil_de
						,cli_code,cli_nom,cli_reference,cli_categorie
						FROM Clients WHERE cli_fil_de=" . $grp["cli_id"]. " AND cli_niveau=" . $niveau . ";";
						$req=$Conn->query($sql);
						$d_client=$req->fetchAll();
						if(!empty($d_client)){
							
							foreach($d_client as $d_cli){
								$array_groupe[$niveau][]=array(
									"cli_id" => $d_cli["cli_id"],
									"cli_code" => $d_cli["cli_code"],
									"cli_nom" => $d_cli["cli_nom"],
									"cli_niveau" => $d_cli["cli_niveau"],
									"cli_fil_de" => $d_cli["cli_fil_de"]
								);
								
								$array_fil[]=array(
									"cli_id" => $d_cli["cli_id"],
									"cli_code" => $d_cli["cli_code"],
									"cli_nom" => $d_cli["cli_nom"],
									"cli_niveau" => $d_cli["cli_niveau"],
									"cli_fil_de" => $d_cli["cli_fil_de"]
								);
							}
						}
					}
				}else{
					$niveau=999;	
				}
			}else{
				$niveau=999;
			}
		}
	}
	
	/*echo("<pre>");
		print_r($array_groupe);
	echo("</pre>");*/
	
	return $array_fil;
	
}
