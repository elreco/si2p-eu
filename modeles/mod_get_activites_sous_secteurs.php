<?php 

function get_activites_sous_secteurs($secteur){
	
	global $Conn;

	if(!empty($secteur)){
		$req = $Conn->query("SELECT * FROM Activites_Sous_Secteurs WHERE ass_secteur=" . $secteur . " ORDER BY ass_libelle;");
		$s_secteurs = $req->fetchAll();
		return $s_secteurs;
	}

}
?>