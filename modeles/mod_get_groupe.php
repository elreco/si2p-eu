<?php

// PERMET DE RECUPERER L'ORGANISATION D'UN GROUPE
// ORGANISEE EN NIVEAU POUR L'AFFICHAGE
function get_groupe($maison_mere_id,$factureur){
	
	global $Conn;
	global $acc_societe;
	global $acc_agence;
	global $acc_utilisateur;

	$array_groupe=array();
	
	$sql="SELECT DISTINCT Clients.cli_id,Clients.cli_filiale_de,Clients.cli_fil_de,Clients.cli_niveau,Clients.cli_code,Clients.cli_nom,Clients.cli_reference,Clients.cli_adr_cp,Clients.cli_adr_ville
	,cso_societe";
	if($factureur){
		$sql.=",Factureur.cli_code AS fac_code";
	}
	$sql.=" FROM clients 
	LEFT OUTER JOIN Clients_Societes ON (clients.cli_id=Clients_Societes.cso_client AND cso_societe=:acc_societe";
		if($acc_agence>0){
			$sql.=" AND cso_agence=:acc_agence";
		}
		if(!$_SESSION["acces"]["acc_droits"][6]){
			$sql.=" AND cso_utilisateur=:acc_utilisateur";
		}
	$sql.=" )";
	if($factureur){
		$sql.=" LEFT OUTER JOIN Clients AS Factureur ON (clients.cli_fac_groupe=Factureur.cli_id)";
	}
	$sql.=" WHERE (Clients.cli_id = :id OR Clients.cli_filiale_de=:filiale_de) ORDER BY cli_code,cli_nom;";
	$req = $Conn->prepare($sql);
	$req->bindParam(':id', $maison_mere_id);
	$req->bindParam(':filiale_de', $maison_mere_id);
	$req->bindParam(':acc_societe', $acc_societe);
	if($acc_agence>0){
		$req->bindParam(':acc_agence', $acc_agence);
	}
	if(!$_SESSION["acces"]["acc_droits"][6]){
		$req->bindParam(':acc_utilisateur', $acc_utilisateur);
	}
	$req->execute();
	$d_result = $req->fetchAll();
	if(!empty($d_result)){
		foreach($d_result as $r){
			
			$cli_niveau=0;
			// OR secu au cas ou cli_filiale_de ne serait pas à 0 sur MM bug MONO30ALES
			if($r["cli_filiale_de"]==0 OR $r["cli_filiale_de"]==$r["cli_id"]){
				$cli_niveau=-1;
			}else{
				$cli_niveau=$r["cli_niveau"];
			}
			
			$fac_code="";
			if(!empty($r['fac_code'])){
				$fac_code=$r['fac_code'];
			}
			$array_groupe[$cli_niveau][] = array(
				"cli_id" => $r['cli_id'],
				"cli_fil_de" => $r['cli_fil_de'],
				"cli_code" => $r['cli_code'],
				"cli_nom" => $r['cli_nom'],
				"cli_reference" => $r['cli_reference'],
				"cli_adr_cp" => $r['cli_adr_cp'],
				"cli_adr_ville" => $r['cli_adr_ville'],
				"cso_acces" => $r['cso_societe'], // indique si utilisateur à acces à la fiche
				"fac_code" => $fac_code,
				
			); 
		}
	}
	return $array_groupe;
}
