<?php 

function get_correspondance($correspondance){
	
	global $Conn;

	$req = $Conn->prepare("SELECT * FROM Correspondances WHERE cor_id = :correspondance;");
	$req->bindParam("correspondance",$correspondance);
	$req->execute();
	$correspondance = $req->fetch();
	return $correspondance;

}
?>