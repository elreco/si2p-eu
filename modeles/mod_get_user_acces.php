<?php


function get_user_acces($acc_ref,$acc_ref_id){
	
	global $Conn;
	
	include "mod_societe.php";
	include "mod_agence.php";
	include "mod_get_contact.php";
	include "mod_get_groupe_filiales.php";
	
	$erreur=0;

	if(empty($acc_ref) OR empty($acc_ref_id) ){
		$erreur=1;
	}else{
		
		$d_acces=array(		
			"acc_ref" => $acc_ref,
			"acc_ref_id" => $acc_ref_id,
			"acc_timer" => time()
		);
	
		if($acc_ref==1){
			
			// il s'agit d'un utilisateur
			
			$req=$Conn->query("SELECT * FROM Utilisateurs WHERE uti_id=" . $acc_ref_id . ";");
			$u=$req->fetch();
			if(!empty($u)){
				
				$full_access=false;
				if($u["uti_profil"]==13 OR $u["uti_profil"]==9 OR $acc_ref_id==98){
					$full_access=true;	
				}
				
				$d_acces['acc_nom'] = $u["uti_nom"];
				$d_acces['acc_prenom'] = $u["uti_prenom"];
				$d_acces['acc_societe'] = $u["uti_societe"];
				$d_acces['acc_agence'] = $u["uti_agence"];	
				$d_acces['acc_profil'] = $u["uti_profil"];
				$d_acces['acc_testeur']=1;
				/* if($u["uti_agence"]==9 OR $u["uti_profil"]==13 OR $u["uti_agence"]==4 OR $u["uti_societe"]==11 OR $u["uti_societe"]==8){
					$d_acces['acc_testeur']=1;	
				} */

				// les services
				
				// utilisateur lié à un service // $full_access à tous les services
				
				$drt_service=array();
				
				$req=$Conn->query("SELECT ser_id FROM Services ORDER BY ser_id;");
				$d_services=$req->fetchAll();
				foreach($d_services as $s){
					if($full_access OR $u["uti_service"]==$s["ser_id"]){
						$drt_service[$s["ser_id"]]=1;	
					}else{
						$drt_service[$s["ser_id"]]=0;
					}
				}			
				$d_acces['acc_service'] = $drt_service;
				
				// On crée la liste des sociétés accéssibles
				$acc_liste_societe=Array();
				
				if($full_access){

					$societe_id=0;
					$req=$Conn->query("SELECT soc_id,soc_code,age_id,age_code FROM Societes LEFT JOIN Agences ON (Societes.soc_id=Agences.age_societe AND soc_agence AND NOT age_archive)
					WHERE NOT soc_archive ORDER BY soc_code,soc_id,age_code,age_id;");
					$d_societes=$req->fetchAll();
					foreach($d_societes as $s){
						
						if($societe_id!=$s["soc_id"]){
							$acc_liste_societe[$s["soc_id"] . "-0"]=$s["soc_code"];	
						}
						if(!empty($s["age_id"])){
							$acc_liste_societe[$s["soc_id"] . "-" . $s["age_id"]]=$s["soc_code"] . " " . $s["age_code"];
						}
						
					}
					
				}else{

					$societes=get_societes_utilisateur($acc_ref_id,True);
					
					if(!empty($societes)){
						foreach($societes as $s){
							if($s["uso_agence"]==0){
								// si uso_agence n'est pas egal à 0, l'utilisateur à accès à une agence
								$acc_liste_societe[$s["soc_id"] . "-0"]=$s["soc_code"];	
							}
							if($s["soc_agence"]){
								$agences=get_agences_utilisateur($s["soc_id"],$acc_ref_id,True);
								if(!empty($agences)){
									foreach($agences as $a){
										$acc_liste_societe[$s["soc_id"] . "-" . $a["age_id"]]=$s["soc_code"] . " " . $a["age_code"];										
									}
								}	
							}
						}
					}
				}
				$d_acces['acc_liste_societe']=$acc_liste_societe;

				// on recupere ces droits
				// valeur d'affectation d'un droit pour un utilisateur donnée
				
				if($full_access){
					// on affecte tous les droits					
					$req=$Conn->query("SELECT drt_id FROM Droits ORDER BY drt_id;");
					$d_droits=$req->fetchAll();

				}else{
					
					$req = $Conn->prepare("SELECT drt_id,udr_droit FROM Droits LEFT JOIN Utilisateurs_droits 
					ON (Droits.drt_id=Utilisateurs_droits.udr_droit AND Utilisateurs_droits.udr_utilisateur=:utilisateur) ORDER BY udr_droit;");
					$req->bindParam(':utilisateur', $acc_ref_id);
					$req->execute();
					$d_droits=$req->fetchAll();
				}
				if(!empty($d_droits)){
					foreach($d_droits as $d){
						
						if($full_access){
							$d_acces['acc_droits'][$d["drt_id"]]=true;
						}elseif(!empty($d["udr_droit"])){						
							$d_acces['acc_droits'][$d["drt_id"]]=true;
						}else{
							// on affecte le droit avec valeu faux pour evite msg erreur undefined var
							$d_acces['acc_droits'][$d["drt_id"]]=false;
						}
					}
				}
				
				// ELEMENT DU HEADER
				
				$req = $Conn->query("SELECT cca_id,cca_libelle FROM Clients_Categories WHERE cca_gc ORDER BY cca_libelle;");
				$result=$req->fetchAll();
				if(!empty($result)){
					$d_acces["menu_gc"]=array();
					foreach($result as $r){
						$d_acces["menu_gc"][]=array(
							"id" => $r["cca_id"],
							"nom" => $r["cca_libelle"]
						);
					}
				}
			
				// ELEMENT PARAMETRE STOCKE EN SESSION
			
				$d_acces['par_soc_gc']=4;
			
				// FIN UTILISATEUR Si2P 
			}else{
				$erreur=1;
			}
	
		}elseif($acc_ref==2){
		
			// ESPACE CLIENT

			// il s'agit d'un utilisateur
			$c=get_contact($acc_ref_id);
			
			$d_acces['acc_nom'] = $c["con_nom"];
			$d_acces['acc_prenom'] = $c["con_prenom"];
			$d_acces['acc_client'] = $c["con_ref_id"];
			if(empty($c["con_ref_id"])){
				$c["con_ref_id"] = 0;
			}
			
			$req = $Conn->query("SELECT cli_id, cli_nom, cli_filiale_de FROM Clients WHERE cli_id=" . $c["con_ref_id"]);
			$req->execute();
			$result=$req->fetch();

			if(!empty($result)){
				$d_acces['acc_client_nom'] = $result["cli_nom"];
			}
			// determine la maison mère
			if($result['cli_filiale_de'] > 0){
				$d_acces['acc_holding'] = $result["cli_filiale_de"];
			}else{
				$d_acces['acc_holding'] = $c["con_ref_id"];
			}
			// chercher les filiales 
			$d_acces['acc_groupe'] = get_groupe_filiales($result["cli_id"]);
			if(empty($d_acces['acc_groupe'])){
				$d_acces['acc_groupe_liste'] = $result["cli_id"];
			}else{
				$d_acces['acc_groupe_liste'] = implode(', ', array_column($d_acces['acc_groupe'], 'cli_id')) . ", " . $result["cli_id"];
			}
			
			
			// 

		}elseif($acc_ref==3){
			
			// ESPACE STAGIAIRE
			
			// il s'agit d'un stagiaire
			
			// il s'agit d'un stagiaire
			$req=$Conn->query("SELECT * FROM Stagiaires WHERE sta_id=" . $acc_ref_id . ";");
			$s=$req->fetch();
			if(!empty($s)){
				
				$d_acces['acc_nom'] = $s["sta_nom"];
				$d_acces['acc_prenom'] = $s["sta_prenom"];
				$d_acces['acc_client'] = $s["sta_client"];
				
				
				
			}else{
				$erreur=1;
			}
		}
	}
			
	if($erreur==0){
		return $d_acces;
	}else{
		return false;
	}
}


?>