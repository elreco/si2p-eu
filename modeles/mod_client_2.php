<?php


/*--------------------------------------------------
	SELECTION
	----------------------------------------------*/
	
	function get_clients($societe)
	{	

		global $Conn;
		$req = $Conn->prepare("SELECT cli_id,cli_code,cli_nom,cli_categorie FROM clients WHERE cli_societe = :societe ORDER BY cli_code");
		$req->bindParam("societe",$societe);
		$req->execute();
		$clients = $req->fetchAll();
		return $clients;
	} 
	function get_clients_get()
	{	

		global $ConnGet;
		$req = $ConnGet->prepare("SELECT cli_id,cli_code,cli_nom,cli_categorie FROM clients ORDER BY cli_code");
		$req->execute();
		$clients = $req->fetchAll();
		return $clients;
	} 
	function get_sous_classifications()
	{	

		global $Conn;
		$req = $Conn->prepare("SELECT * FROM clients_sous_classifications");
		$req->execute();
		$clients = $req->fetchAll();
		return $clients;
	} 

	function get_classifications()
	{	

		global $Conn;
		$req = $Conn->prepare("SELECT * FROM clients_classifications");
		$req->execute();
		$clients = $req->fetchAll();
		return $clients;
	} 
	function get_sous_classification($id)
	{	

		global $Conn;
		$req = $Conn->prepare("SELECT * FROM clients_sous_classifications WHERE csc_id =" . $id);
		$req->execute();
		$clients = $req->fetch();
		return $clients;
	}
	function get_classification($id)
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_classifications WHERE ccl_id = :ccl_id");
		$req->bindParam(':ccl_id', $id);
		$req->execute();
		

		$clients = $req->fetch();

		return $clients;
	}
	function insert_classification()
	{

		global $Conn;
		$ccl_libelle= $_POST['ccl_libelle'];
		
		if (isset($_POST['ccl_id'])) {
			$id = $_POST['ccl_id'];
			$req = $Conn->prepare("UPDATE clients_classifications SET ccl_libelle = :ccl_libelle WHERE ccl_id = :ccl_id");
		}else{
			$req = $Conn->prepare("INSERT INTO clients_classifications (ccl_libelle) VALUES (:ccl_libelle)");
		}  

		$req->bindParam(':ccl_libelle', $ccl_libelle);
		if (isset($_POST['ccl_id'])) {
			$req->bindParam(':ccl_id', $id);
		}
		$req->execute();

		header('Location: classification_liste.php');


	}
	function insert_client_info($inf_type, $inf_description, $inf_famille, $inf_sous_famille, $inf_auteur, $inf_client)
	{

		global $ConnSoc;

		$req = $ConnSoc->prepare("INSERT INTO infos (inf_type, inf_description, inf_famille, inf_sous_famille, inf_auteur, inf_date, inf_client) VALUES (:inf_type, :inf_description, :inf_famille, :inf_sous_famille, :inf_auteur, NOW(), :inf_client)");
		

		$req->bindParam(':inf_type', $inf_type);
		$req->bindParam(':inf_description', $inf_description);
		$req->bindParam(':inf_famille', $inf_famille);
		$req->bindParam(':inf_sous_famille', $inf_sous_famille);
		$req->bindParam(':inf_auteur', $inf_auteur);
		$req->bindParam(':inf_client', $inf_client);
	
		$req->execute();


	}
	function insert_cli_info($cin_libelle)
	{

		global $Conn;

		$req = $Conn->prepare("INSERT INTO clients_infos (cin_libelle) VALUES (:cin_libelle)");
		

		$req->bindParam(':cin_libelle', $cin_libelle);
	
		$req->execute();


	}
	function update_cli_info($cin_id, $cin_libelle)
	{

		global $Conn;

		$req = $Conn->prepare("UPDATE clients_infos SET cin_libelle =:cin_libelle WHERE cin_id = :cin_id;");
		

		$req->bindParam(':cin_id', $cin_id);
		$req->bindParam(':cin_libelle', $cin_libelle);
	
		$req->execute();


	}
	function update_client_info($inf_id, $inf_type, $inf_description, $inf_famille, $inf_sous_famille)
	{

		global $ConnSoc;

		$req = $ConnSoc->prepare("UPDATE infos SET inf_id=:inf_id, inf_type =:inf_type, inf_description=:inf_description, inf_famille=:inf_famille, inf_sous_famille=:inf_sous_famille WHERE inf_id = :inf_id;)");
		
		$req->bindParam(':inf_id', $inf_id);
		$req->bindParam(':inf_type', $inf_type);
		$req->bindParam(':inf_description', $inf_description);
		$req->bindParam(':inf_famille', $inf_famille);
		$req->bindParam(':inf_sous_famille', $inf_sous_famille);
		$req->execute();


	}
	function delete_client_info($id){
	
	global $ConnSoc;

	$req = $ConnSoc->prepare("DELETE FROM infos WHERE inf_id = :inf_id");

	$req->bindParam("inf_id",$id);
	$req->execute();

}
	function get_correspondances($client)
	{

		global $ConnSoc;
		
		$req = $ConnSoc->prepare("SELECT * FROM correspondances WHERE cor_client = :sco_client");

		$req->bindParam(':sco_client', $client);
		$req->execute();
		$clients = $req->fetchAll();

		return $clients;

	}

	function get_correspondance_last($client){
		
		global $ConnSoc;
		
		$req = $ConnSoc->prepare("SELECT * FROM correspondances WHERE cor_client = :sco_client ORDER BY cor_date DESC");

		$req->bindParam(':sco_client', $client);
		$req->execute();
		$clients = $req->fetch();

		return $clients;
	}

	function insert_sous_classification()
	{

		global $Conn;
		$csc_libelle= $_POST['csc_libelle'];
		$csc_classification = $_POST['csc_classification'];
		
		if (isset($_POST['csc_id'])) {
			$id = $_POST['csc_id'];
			$req = $Conn->prepare("UPDATE clients_sous_classifications SET csc_libelle = :csc_libelle, csc_classification = :csc_classification  WHERE csc_id = :csc_id");
		}else{
			$req = $Conn->prepare("INSERT INTO clients_sous_classifications (csc_libelle, csc_classification) VALUES (:csc_libelle, :csc_classification)");
		}  

		$req->bindParam(':csc_libelle', $csc_libelle);
		$req->bindParam(':csc_classification', $csc_classification);
		if (isset($_POST['csc_id'])) {
			$req->bindParam(':csc_id', $id);
		}
		$req->execute();

		header('Location: /sous_classification_liste.php');


	}

	function get_clients_categories($id)
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_categories WHERE cca_id = :cca_id");
		$req->bindParam(':cca_id', $id);
		$req->execute();
		

		$clients = $req->fetch();

		return $clients;
	}
	function get_clients_infos($id)
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_infos WHERE cin_id = :cin_id");
		$req->bindParam(':cin_id', $id);
		$req->execute();
		

		$clients = $req->fetch();

		return $clients;
	}  
	function get_clients_infos_client($id)
	{	

		global $ConnSoc;

		$req = $ConnSoc->prepare("SELECT * FROM infos WHERE inf_client = :inf_client");
		$req->bindParam(':inf_client', $id);
		$req->execute();
		

		$clients = $req->fetchAll();

		return $clients;
	} 

	function get_clients_infos_client_search($inf_client, $inf_type, $inf_famille, $inf_sous_famille)
	{	

		global $ConnSoc;

		$sql="SELECT * FROM infos";
		
		$mil="";
		if(!empty($inf_client)){
			$mil.=" AND inf_client=" . $inf_client; 
		};
		if(!empty($inf_type)){
			$mil.=" AND inf_type=" . $inf_type; 
		};
		if(!empty($inf_famille)){
			$mil.=" AND inf_famille=" . $inf_famille; 
		};
		if(!empty($inf_sous_famille)){
			$mil.=" AND inf_sous_famille=" . $inf_sous_famille; 
		};
		
		if($mil!=""){
			$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		}
		
		$sql.=" ORDER BY inf_type";

		$req = $ConnSoc->prepare($sql);
		$req->execute();
		$utilisateurs = $req->fetchAll();

		return $utilisateurs;
	}

function get_client_info_select($selected, $client){

  global $Conn;
  global $ConnSoc;

  	$req = $ConnSoc->prepare("SELECT * FROM clients WHERE cli_id = $client");
	$req->execute();


	$client = $req->fetch();



  $service_select="";

	    if($client['cli_groupe'] == 1 && $client['cli_filiale_de'] == 0 && $client['cli_categorie'] == 2){
	  	$req=$Conn->query("SELECT * FROM clients_infos ORDER BY cin_libelle");
	  }
	  	elseif($client['cli_groupe'] == 1 && $client['cli_filiale_de'] == 0 && $client['cli_categorie'] !=2){
	  		$req=$Conn->query("SELECT * FROM clients_infos WHERE cin_id != 6 ORDER BY cin_libelle");
	  }elseif(($client['cli_groupe'] == 1 OR $client['cli_groupe'] == 0) && $client['cli_filiale_de'] != 0 && $client['cli_categorie'] == 2){
	  		$req=$Conn->query("SELECT * FROM clients_infos WHERE cin_id != 5 ORDER BY cin_libelle");
	  }elseif($client['cli_categorie'] == 2){
	  	$req=$Conn->query("SELECT * FROM clients_infos WHERE cin_id != 6 ORDER BY cin_libelle");
	  }else{
	  	$req=$Conn->query("SELECT * FROM clients_infos WHERE cin_id != 6 AND cin_id != 5 ORDER BY cin_libelle");
	  }
  
  while ($donnees = $req->fetch())
  {
    if($selected == 0){
            $service_select=$service_select . "<option value='" . $donnees["cin_id"] . "' >" . $donnees["cin_libelle"] . "</option>";
        }else{
            if($donnees['cin_id'] == $selected){
               $service_select=$service_select . "<option selected value='" . $donnees["cin_id"] . "' >" . $donnees["cin_libelle"] . "</option>";
            }else{
                $service_select=$service_select . "<option value='" . $donnees["cin_id"] . "' >" . $donnees["cin_libelle"] . "</option>";
            }
           
        }
    
  }
  return $service_select;
  }


	function get_client_categorie()
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_categories");
		$req->execute();
		

		$clients = $req->fetchAll();

		return $clients;
	}
	function get_client_infos()
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_infos");
		$req->execute();
		

		$clients = $req->fetchAll();

		return $clients;
	}
	function get_client_info($id)
	{	

		global $ConnSoc;

		$req = $ConnSoc->prepare("SELECT * FROM infos WHERE inf_id = :inf_id");
		$req->bindParam(':inf_id', $id);
		$req->execute();
		

		$clients = $req->fetch();

		return $clients;
	}
	
	function insert_client_categorie($libelle)
	{	

		global $Conn;
		$req = $Conn->prepare("INSERT INTO clients_categories (cca_libelle) VALUES (:cca_libelle);");

		$req->bindParam("cca_libelle",$libelle);
		
		$req->execute();
	}
	function update_client_categorie($id,$libelle)
	{	

		global $Conn;
		$req = $Conn->prepare("UPDATE clients_categories SET cca_libelle = :cca_libelle WHERE cca_id = :cca_id;");
		$req->bindParam("cca_id",$id);
		$req->bindParam("cca_libelle",$libelle);
		
		$req->execute();
	}


	
	function get_clients_categories_select($selected)
	{	

		global $Conn;

		$client_select="";
		
		$req = $Conn->prepare("SELECT soc_id, soc_gc FROM societes WHERE soc_id = " . $_SESSION['acces']["acc_societe"]);
		$req->execute();
		$societe = $req->fetch();
		if($_SESSION['acces']['acc_societe'] == 4 && $societe['soc_gc'] == 1 && $_SESSION['acces']['acc_droits'][8]){
			$sql="SELECT * FROM clients_categories WHERE cca_id != 5"; 
		}elseif($_SESSION['acces']['acc_societe'] == 4 && $societe['soc_gc'] == 1 && !$_SESSION['acces']['acc_droits'][8]){
			$sql="SELECT * FROM clients_categories WHERE cca_id != 5 AND cca_id != 2 AND cca_id != 6"; 	
		}else{
			$sql="SELECT * FROM clients_categories WHERE cca_id != 5"; 
		} 
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["cca_id"] . "' >" . $donnees["cca_libelle"] . "</option>";
			}else{
				if($donnees['cca_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["cca_id"] . "' >" . $donnees["cca_libelle"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["cca_id"] . "' >" . $donnees["cca_libelle"] . "</option>";
				}

			}
		}
		return $client_select;
	}
	function get_clients_categories_select_2($selected)
	{	

		global $Conn;

		$client_select="";
		
		if($_SESSION['acces']['acc_societe'] == 4 && $societe['soc_gc'] == 1 && $_SESSION['acces']['acc_droits'][8]){
			$sql="SELECT * FROM clients_categories WHERE cca_id != 5"; 
		}elseif($_SESSION['acces']['acc_societe'] == 4 && $societe['soc_gc'] == 1 && !$_SESSION['acces']['acc_droits'][8]){
			$sql="SELECT * FROM clients_categories WHERE cca_id != 5 AND cca_id != 2 AND cca_id != 6"; 	
		}else{
			$sql="SELECT * FROM clients_categories WHERE cca_id != 5"; 
		}
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["cca_id"] . "' >" . $donnees["cca_libelle"] . "</option>";
			}else{
				if($donnees['cca_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["cca_id"] . "' >" . $donnees["cca_libelle"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["cca_id"] . "' >" . $donnees["cca_libelle"] . "</option>";
				}

			}
		}
		return $client_select;
	}
	function get_client($id)
	{
		// type: cli ou sus pour client ou client

		global $ConnSoc;
			$req = $ConnSoc->prepare("SELECT * FROM clients WHERE cli_id=:cli_id");
		
		$req->bindParam(':cli_id', $id);
		$req->execute();

		$client = $req->fetch();

		return $client;
	}
	function get_client_classification_select($selected){
		
		// $societe "" par defaut car $societe=0 permet de ne renvoyer aucun résultat 

		global $Conn;

		$client_select="";
		
		$sql="SELECT * FROM clients_classifications";
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["ccl_id"] . "' >" . $donnees["ccl_libelle"] . "</option>";
			}else{
				if($donnees['ccl_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["ccl_id"] . "' >" . $donnees["ccl_libelle"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["ccl_id"] . "' >" . $donnees["ccl_libelle"] . "</option>";
				}

			}
		}
		return $client_select;
	}
	
	/* SELECTION ET INJECTION HTML <select/> */
	
	function get_client_select($selected){
		
		// $societe "" par defaut car $societe=0 permet de ne renvoyer aucun résultat 

		global $ConnSoc;

		$client_select="";
		
		$sql="SELECT cli_id, cli_nom FROM clients";
		
		$req=$ConnSoc->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
			}else{
				if($donnees['cli_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
				}

			}
		}
		return $client_select;
	}

	function get_client_opca_select($selected){
		
		// $societe "" par defaut car $societe=0 permet de ne renvoyer aucun résultat 

		global $ConnSoc;

		$client_select="";
		
		$sql="SELECT cli_id, cli_nom, cli_opca FROM clients WHERE cli_opca != 0";
		
		$req=$ConnSoc->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
			}else{
				if($donnees['cli_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["cli_id"] . "' >" . $donnees["cli_nom"] . "</option>";
				}

			}
		}
		return $client_select;
	}
	
	
	
	function insert_client($cli_code,$cli_nom,$cli_agence,$cli_commercial,$cli_categorie,$cli_sous_categorie,$cli_stagiaire_type,$cli_secteur,$cli_sous_secteur,$cli_classification,$cli_filiale_de,$cli_reg_type,$cli_siren,$cli_ape,$cli_opca,$cli_facture_opca,$cli_reg_formule,$cli_reg_nb_jour,$cli_reg_fdm,$cli_sous_classification, $cli_ident_tva, $cli_prescripteur)
	{

		global $ConnSoc;
		if(session_id() == '' || !isset($_SESSION)) {
			session_start();
		}
		$creator = $_SESSION['acces']['acc_ref_id'];
		$req = $ConnSoc->prepare("INSERT INTO clients (cli_code, cli_nom, cli_agence, cli_commercial, cli_categorie,cli_sous_categorie,cli_stagiaire_type, cli_secteur, cli_sous_secteur, cli_classification, cli_sous_classification, cli_date_creation, cli_filiale_de, cli_reg_type, cli_siren, cli_ape, cli_uti_creation, cli_opca, cli_facture_opca, cli_reg_formule, cli_reg_nb_jour, cli_reg_fdm, cli_ident_tva, cli_prescripteur) VALUES (:cli_code, :cli_nom, :cli_agence, :cli_commercial, :cli_categorie,:cli_stagiaire_type, :cli_secteur, :cli_sous_secteur, :cli_classification, :cli_sous_classification, NOW(), :cli_filiale_de, :cli_reg_type, :cli_siren, :cli_ape, :creator, :cli_opca, :cli_facture_opca, :cli_reg_formule, :cli_reg_nb_jour, :cli_reg_fdm, :cli_ident_tva, :cli_prescripteur);");
		
		$req->bindParam("cli_code",$cli_code);
		$req->bindParam("cli_nom",$cli_nom);
		$req->bindParam("cli_agence",$cli_agence);
		$req->bindParam("cli_commercial",$cli_commercial);
		$req->bindParam("cli_stagiaire_type",$cli_stagiaire_type);
		$req->bindParam("cli_categorie",$cli_categorie);
		$req->bindParam("cli_sous_categorie",$cli_sous_categorie);
		$req->bindParam("cli_secteur",$cli_secteur);
		$req->bindParam("cli_sous_secteur",$cli_sous_secteur);
		$req->bindParam("cli_classification",$cli_classification);
		$req->bindParam("cli_filiale_de",$cli_filiale_de);
		$req->bindParam("cli_reg_type",$cli_reg_type);
		$req->bindParam("cli_siren",$cli_siren);
		$req->bindParam("cli_ape",$cli_ape);
		$req->bindParam("cli_opca",$cli_opca);
		$req->bindParam("cli_facture_opca",$cli_facture_opca);
		$req->bindParam("cli_reg_formule",$cli_reg_formule);
		$req->bindParam("cli_reg_nb_jour",$cli_reg_nb_jour); 
		$req->bindParam("cli_reg_fdm",$cli_reg_fdm);
		$req->bindParam("cli_sous_classification",$cli_sous_classification);
		$req->bindParam("creator",$creator);
		$req->bindParam("cli_ident_tva",$cli_ident_tva);
		$req->bindParam("cli_prescripteur",$cli_prescripteur);
		$req->execute();
	//print_r($req->errorInfo());

		$id = $ConnSoc->lastInsertId();

		return $id;

	}
	function insert_client_csv($cli_code,$cli_nom)
	{

		global $ConnSoc;
		if(session_id() == '' || !isset($_SESSION)) {
			session_start();
		}
		$creator = $_SESSION['acces']['acc_ref_id'];
		$req = $ConnSoc->prepare("INSERT INTO clients (cli_code, cli_nom, cli_date_creation, cli_uti_creation) VALUES (:cli_code, :cli_nom, NOW(), :creator);");
		
		$req->bindParam("cli_code",$cli_code);
		$req->bindParam("cli_nom",$cli_nom);
		$req->bindParam("creator",$creator);
		$req->execute();
	//print_r($req->errorInfo());

		$id = $ConnSoc->lastInsertId();

		return $id;

	}

	function update_client_siren($cli_id, $cli_siren){
//die();
		global $ConnSoc;
		$req = $ConnSoc->prepare("UPDATE clients SET cli_siren = :cli_siren WHERE cli_id = :cli_id");

		$req->bindParam("cli_id",$cli_id);
		$req->bindParam("cli_siren",$cli_siren);

		$req->execute();
	}

	function update_client($cli_id, $cli_code,$cli_nom,$cli_agence,$cli_commercial,$cli_categorie,$cli_sous_categorie,$cli_secteur,$cli_sous_secteur,$cli_reg_type,$cli_siren,$cli_ape,$cli_opca,$cli_facture_opca,$cli_reg_formule,$cli_reg_nb_jour,$cli_reg_fdm,$cli_sous_classification, $cli_ident_tva, $cli_prescripteur, $cli_stagiaire_type, $cli_classification, $cli_archive, $cli_groupe, $cli_groupe_societe, $cli_releve, $cli_reference, $cli_contrat, $cli_important)
	{
			
		//die();
		global $ConnSoc;
		$req = $ConnSoc->prepare("UPDATE clients SET cli_code = :cli_code, cli_nom = :cli_nom, cli_agence = :cli_agence, cli_commercial = :cli_commercial, cli_categorie = :cli_categorie,cli_sous_categorie = :cli_sous_categorie, cli_secteur = :cli_secteur, cli_sous_secteur = :cli_sous_secteur, cli_classification = :cli_classification, cli_reg_type = :cli_reg_type, cli_siren = :cli_siren, cli_ape = :cli_ape, cli_opca = :cli_opca, cli_facture_opca = :cli_facture_opca, cli_reg_formule = :cli_reg_formule, cli_reg_nb_jour = :cli_reg_nb_jour, cli_reg_fdm = :cli_reg_fdm, cli_sous_classification = :cli_sous_classification, cli_ident_tva = :cli_ident_tva, cli_prescripteur= :cli_prescripteur, cli_stagiaire_type = :cli_stagiaire_type, cli_archive = :cli_archive, cli_groupe = :cli_groupe, cli_filiale_de_soc = :cli_filiale_de_soc, cli_releve = :cli_releve, cli_reference = :cli_reference, cli_contrat = :cli_contrat, cli_important = :cli_important WHERE cli_id = :cli_id");

		$req->bindParam("cli_id",$cli_id);
		$req->bindParam("cli_code",$cli_code);
		$req->bindParam("cli_nom",$cli_nom);
		$req->bindParam("cli_agence",$cli_agence);
		$req->bindParam("cli_commercial",$cli_commercial);
		$req->bindParam("cli_categorie",$cli_categorie);
		$req->bindParam("cli_sous_categorie",$cli_sous_categorie);
		$req->bindParam("cli_secteur",$cli_secteur);
		$req->bindParam("cli_sous_secteur",$cli_sous_secteur);
		$req->bindParam("cli_classification",$cli_classification);
		$req->bindParam("cli_reg_type",$cli_reg_type);
		$req->bindParam("cli_siren",$cli_siren);
		$req->bindParam("cli_ape",$cli_ape);
		$req->bindParam("cli_opca",$cli_opca);
		$req->bindParam("cli_facture_opca",$cli_facture_opca);
		$req->bindParam("cli_reg_formule",$cli_reg_formule);
		$req->bindParam("cli_reg_nb_jour",$cli_reg_nb_jour);
		$req->bindParam("cli_reg_fdm",$cli_reg_fdm);
		$req->bindParam("cli_ident_tva",$cli_ident_tva);
		$req->bindParam("cli_prescripteur",$cli_prescripteur);
		$req->bindParam("cli_stagiaire_type",$cli_stagiaire_type);
		$req->bindParam("cli_classification",$cli_classification);
		$req->bindParam("cli_sous_classification",$cli_sous_classification);
		$req->bindParam("cli_archive",$cli_archive);
		$req->bindParam("cli_groupe",$cli_groupe);
		$req->bindParam("cli_filiale_de_soc",$cli_groupe_societe);
		$req->bindParam("cli_releve",$cli_releve);
		$req->bindParam("cli_reference",$cli_reference);
		$req->bindParam("cli_contrat",$cli_contrat);
		$req->bindParam("cli_important",$cli_important);
		$req->execute();

	}
function get_clients_sous_categories($id)
	{	

		global $Conn;

		$req = $Conn->prepare("SELECT * FROM clients_sous_categories WHERE csc_id = :csc_id");
		$req->bindParam(':csc_id', $id);
		$req->execute();
		

		$clients = $req->fetch();

		return $clients;
	}

function get_clients_sous_categories_select($selected)
	{	

		global $Conn;

		$client_select="";
		
		$sql="SELECT * FROM clients_sous_categories"; 
		
		$req=$Conn->query($sql);
		while ($donnees = $req->fetch())
		{
			if($selected == 0){
				$client_select=$client_select . "<option value='" . $donnees["csc_id"] . "' >" . $donnees["csc_libelle"] . "</option>";
			}else{
				if($donnees['csc_id'] == $selected){
					$client_select=$client_select . "<option selected value='" . $donnees["csc_id"] . "' >" . $donnees["csc_libelle"] . "</option>";
				}else{
					$client_select=$client_select . "<option value='" . $donnees["csc_id"] . "' >" . $donnees["csc_libelle"] . "</option>";
				}

			}
		}
		return $client_select;
	}

