<?php 

// ATTESTATION GROUPE PAR SESSION OU PAR FORMATION

function action_attestation_data($action_id,$action_client_id,$modele){

	global $ConnSoc;
	global $Conn;
	global $acc_societe;
	
	$erreur=false;
	
	$data=array();

	if(empty($action_id) OR empty($action_client_id)){
		$erreur=true;
	}
	
	if(empty($erreur)){

		$req=$ConnSoc->prepare("SELECT act_agence,acl_pro_libelle,act_adr_ville,act_adr_cp,act_gest_sta,acl_client,acl_facturation
		FROM Actions,Actions_Clients WHERE act_id=acl_action AND act_id=:action_id AND acl_id=:action_client_id;");
		$req->bindParam(":action_id",$action_id);
		$req->bindParam(":action_client_id",$action_client_id);
		$req->execute();
		$data=$req->fetch();
		if(empty($data)){
			$erreur=true;
		}
		
		// ADRESSE DU CLIENT
		
		$req=$Conn->prepare("SELECT cli_nom,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville
		FROM adresses,clients WHERE cli_id=adr_ref_id AND adr_ref=1 AND adr_ref_id=:adr_ref_id AND adr_type=1 AND adr_defaut;");
		$req->bindParam(":adr_ref_id",$data["acl_client"]);
		$req->execute();
		$d_client=$req->fetch();
		if(empty($d_client)){
			$erreur=true;
		}else{
			
			$adresse_client="<b>" . $d_client["cli_nom"] . "</b>";
			
			if(!empty($d_client["adr_service"])){
				$adresse_client.="<br/>" . $d_client["adr_service"];
			}
			if(!empty($d_client["adr_ad1"])){
				$adresse_client.="<br/>" . $d_client["adr_ad1"];
			}
			if(!empty($d_client["adr_ad2"])){
				$adresse_client.="<br/>" . $d_client["adr_ad2"];
			}
			if(!empty($d_client["adr_ad3"])){
				$adresse_client.="<br/>" . $d_client["adr_ad3"];
			}
			$adresse_client.="<br/>" . $d_client["adr_cp"] . " " . $d_client["adr_ville"];
			$data["adresse_client"]=$adresse_client;
		}
		
	}
	
	
	if(empty($erreur)){
		
		// INFO JURIDIQUE
		if($data["acl_facturation"]==1){
			$data["juridique"]=get_juridique(4,4);
		}else{
			$data["juridique"]=get_juridique($acc_societe,$data["act_agence"]);
		}
		
		// Date du doc
		
		$data["date_doc"]=strftime("%A %d %B %Y",time());
		
		//
		if($modele==1){
			$data["pdf"]="attestation_form_" . $action_id . "_" . $action_client_id;
		}else{
			$data["pdf"]="attestation_ses_" . $action_id . "_" . $action_client_id;
		}
		
		
		
		
		if($modele==2){
			
			// ATTESTATION PAR SESSION
			
			$sql="SELECT pda_date,pda_demi,pda_id,pda_intervenant,pda_categorie,ase_h_deb,ase_h_fin,ase_id,int_label_1,int_label_2 
			FROM Actions_Clients,Actions_Clients_Dates,Plannings_Dates,Intervenants,Actions_Sessions
			WHERE acl_id=acd_action_client AND acd_date=pda_id AND pda_intervenant=int_id AND pda_id=ase_date
			AND acl_id=:action_client_id
			ORDER BY pda_date,pda_demi,pda_id,ase_h_deb,ase_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client_id",$action_client_id);
			$req->execute();
			$result=$req->fetchAll();
			if(!empty($result)){
				foreach($result as $d){
					
					$ses_duree="";
					if(!empty($d["ase_h_deb"]) AND !empty($d["ase_h_fin"]) ){
						$duree=new DateTime("2000-01-01 00:00:00");
						$DT_deb =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $d["ase_h_deb"] . ":00");
						$DT_fin =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $d["ase_h_fin"] . ":00");
						$duree->add($DT_deb->diff($DT_fin));
						$ses_duree=$duree->format("H \H i");;
					}
					
					$formateurs=array();
					$testeurs=array();
					if($d["pda_categorie"]==2){
						if(!isset($testeurs[$d["pda_intervenant"]])){
							$testeurs[$d["pda_intervenant"]]=$d["int_label_2"] . " " . $d["int_label_1"];
						}
					}else{
						if(!isset($formateurs[$d["pda_intervenant"]])){
							$formateurs[$d["pda_intervenant"]]=$d["int_label_2"] . " " . $d["int_label_1"];
						}
					};

					// LES STAGIAIRES
					
					$stagiaires=array();
					$liste_sta="";
					$sql="SELECT ast_stagiaire FROM Actions_Stagiaires,Actions_Stagiaires_Sessions WHERE ast_stagiaire=ass_stagiaire
					AND ast_action=" . $action_id . " AND ast_action_client=" . $action_client_id. " AND ast_confirme AND ass_session=" . $d["ase_id"] . ";";
					$req=$ConnSoc->query($sql);
					$src_sta=$req->fetchAll(PDO::FETCH_ASSOC);
					if(!empty($src_sta)){
						$tab_sta=array_column ($src_sta,"ast_stagiaire");
						$liste_sta=implode($tab_sta,",");
					}
					if(!empty($liste_sta)){
						$sql="SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id IN (" . $liste_sta . ") ORDER BY sta_nom,sta_prenom;";
						$req=$Conn->query($sql);
						$stagiaires=$req->fetchAll(PDO::FETCH_ASSOC);
					}
					
					$data["pages"][]=array(
						"stagiaires" =>$stagiaires,
						"dates" => array(
							"0" => array(
								"date_deb" => $d["pda_date"],
								"demi_deb" => $d["pda_demi"],
								"h_deb" => $d["ase_h_deb"],
								"date_fin" => $d["pda_date"],
								"demi_fin" => $d["pda_demi"],
								"h_fin" => $d["ase_h_fin"]
							)
						),
						"duree" => $ses_duree,
						"formateurs" => $formateurs,
						"testeurs" => $testeurs
					);
				}
			}
		
		}else{
			
			// ATTESTATION POUR LA FORMATIONS COMPLETES
			
			$calcul_duree=true;
			$duree=new DateTime("2000-01-01 00:00:00");
			
			$formateurs=array();
			$testeurs=array();
			$periode=array();

			$sql="SELECT pda_date,pda_demi,pda_id,pda_intervenant,pda_categorie,ase_h_deb,ase_h_fin,int_label_1,int_label_2 FROM Actions_Clients,Actions_Clients_Dates,Plannings_Dates,Intervenants,Actions_Sessions
			WHERE acl_id=acd_action_client AND acd_date=pda_id AND pda_intervenant=int_id AND pda_id=ase_date
			AND acl_id=:action_client_id
			ORDER BY pda_date,pda_demi,pda_id,ase_h_deb,ase_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client_id",$action_client_id);
			$req->execute();
			$result=$req->fetchAll();
			if(!empty($result)){
				
				$data["pages"][]=array(
					"stagiaires" => array(),
					"dates" => "",
					"duree" => 0,
					"formateurs" => array(),
					"testeurs" => array()
				);
				foreach($result as $k => $d){
				
					if($k==0){
						
						$date_deb=$d["pda_date"];
						$demi_deb=$d["pda_demi"];
						$date_fin=$d["pda_date"];
						$demi_fin=$d["pda_demi"];
						$h_deb=$d["ase_h_deb"];
						$h_fin=$d["ase_h_fin"];
						
						$pda_id=$d["pda_id"];
						
						// duree
						if(!empty($d["ase_h_deb"]) AND !empty($d["ase_h_fin"]) ){
							$DT_deb =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $d["ase_h_deb"] . ":00");
							$DT_fin =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $d["ase_h_fin"] . ":00");
							$duree->add($DT_deb->diff($DT_fin));
						}else{
							$calcul_duree=false;
						}

					}else{
						
						if($pda_id!=$d["pda_id"]){
							
							$pda_id=$d["pda_id"];
							
							if($d["pda_categorie"]==2){
								if(!isset($testeurs[$d["pda_intervenant"]])){
									$testeurs[$d["pda_intervenant"]]=$d["int_label_2"] . " " . $d["int_label_1"];
								}
							}else{
								if(!isset($formateurs[$d["pda_intervenant"]])){
									$formateurs[$d["pda_intervenant"]]=$d["int_label_2"] . " " . $d["int_label_1"];
								}
							};
							
							// CALCUL DU PLANNING DE FORMATION
							if($d["pda_demi"]==2){
								
								if($demi_fin!=1 OR ($d["pda_date"]!=$date_fin)){
									
									$periode[]=array(
										"date_deb" => $date_deb,
										"demi_deb" => $demi_deb,
										"h_deb" => $h_deb,
										"date_fin" => $date_fin,
										"demi_fin" => $demi_fin,
										"h_fin" => $h_fin	
									);
									
									
									$date_deb=$d["pda_date"];
									$demi_deb=$d["pda_demi"];
									$h_deb=$d["ase_h_deb"];
									
									$date_fin=$d["pda_date"];
									$demi_fin=$d["pda_demi"];
									$h_fin=$d["ase_h_fin"];
									
								}else{
									$date_fin=$d["pda_date"];
									$demi_fin=$d["pda_demi"];
									$h_fin=$d["ase_h_fin"];
								}
							}else{
								
								// c'est un matin

								$DT_periode=date_create_from_format('Y-m-d',$d["pda_date"]);						
								$j=$DT_periode->format("N");
								if($j==1){
									// C'est un lundu matin
									$DT_periode->sub(new DateInterval('P3D'));							
								}else{
									$DT_periode->sub(new DateInterval('P1D'));			
								}
								$d_compare=$DT_periode->format("Y-m-d");
								
								if($demi_fin!=2 OR $date_fin!=$d_compare){
									
									$periode[]=array(
										"date_deb" => $date_deb,
										"demi_deb" => $demi_deb,
										"h_deb" => $h_deb,
										"date_fin" => $date_fin,
										"demi_fin" => $demi_fin,
										"h_fin" => $h_fin	
									);
									
									$date_deb=$d["pda_date"];
									$demi_deb=$d["pda_demi"];
									$h_deb=$d["ase_h_deb"];
									$date_fin=$d["pda_date"];
									$demi_fin=$d["pda_demi"];	
									$h_fin=$d["ase_h_fin"];

									
								}else{
									$date_fin=$d["pda_date"];
									$demi_fin=$d["pda_demi"];
									$h_fin=$d["ase_h_fin"];
								}
							}
						}
					
						//SI GESTION A LA SESSION ON NE COMPTABILISE QUE LA PERMI7RE SESSION cf fetch [0]
						if($data["act_gest_sta"]==1){
							// duree
							if(!empty($d["ase_h_deb"]) AND !empty($d["ase_h_fin"]) ){
								$DT_deb =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $d["ase_h_deb"] . ":00");
								$DT_fin =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $d["ase_h_fin"] . ":00");
								$duree->add($DT_deb->diff($DT_fin));
							}else{
								$calcul_duree=false;
							}
						}
						
					}
					
					
				}
				$periode[]=array(
					"date_deb" => $date_deb,
					"demi_deb" => $demi_deb,
					"h_deb" => $h_deb,
					"date_fin" => $date_fin,
					"demi_fin" => $demi_fin,
					"h_fin" => $h_fin	
				);
			}

			if($calcul_duree){
				$data["pages"][0]["duree"]=$duree->format("H \H i");
				if($data["act_gest_sta"]==2){
					$data["pages"][0]["duree"].=" / session";
				}
			}else{
				$data["pages"][0]["duree"]="";
			}
			
			$data["pages"][0]["formateurs"]=$formateurs;
			$data["pages"][0]["testeurs"]=$testeurs;
			$data["pages"][0]["dates"]=$periode;
			
			// LES STAGIAIRES
			
			$liste_sta="";
			$sql="SELECT ast_stagiaire FROM Actions_Stagiaires WHERE ast_action=" . $action_id . " AND ast_action_client=" . $action_client_id. " AND ast_confirme;";
			$req=$ConnSoc->query($sql);
			$src_sta=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($src_sta)){
				$tab_sta=array_column ($src_sta,"ast_stagiaire");
				$liste_sta=implode($tab_sta,",");
			}
			if(!empty($liste_sta)){
				$sql="SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id IN (" . $liste_sta . ") ORDER BY sta_nom,sta_prenom;";
				$req=$Conn->query($sql);
				$d_stagiaires=$req->fetchAll(PDO::FETCH_ASSOC);
				if(!empty($d_stagiaires)){
					$data["pages"][0]["stagiaires"]=$d_stagiaires;
				}
			}
			
			
			// FIN GESTION FORMATION COMPLETE
		}
		
	}

	
	/*	
		// LES COMPETENCES
		
		$sql="SELECT aco_id,aco_competence,aco_pratique FROM Attestations_competences WHERE aco_attestation=:attestation ORDER BY aco_competence;"; 
		$req=$Conn->prepare($sql);
		$req->bindParam(":attestation",$attestation);
		$req->execute();
		$result_comp=$req->fetchAll();
		if(!empty($result_comp)){
			foreach($result_comp as $comp){
				$page["competences"][$comp["aco_id"]]=array(
					"comp_txt" => $comp["aco_competence"],
					"comp_acquise" => 1
				);
			}
		}
		
		if(!is_null($reussite)){
			
			if(empty($reussite)){
				
				// ATTESTATION EN ECHEC
				
				$sql_comp="SELECT sac_attest_competence,sac_acquise FROM Stagiaires_Attestations_Competences WHERE sac_stagiaire=:stagiaire_id AND sac_action=:action_id;";
				$req_comp=$ConnSoc->prepare($sql_comp);
				$req_comp->bindParam("action_id",$action_id); 
				$req_comp->bindParam("stagiaire_id",$stagiaire_id); 
				$req_comp->execute();
				$d_competences=$req_comp->fetchAll();
				if(!empty($d_competences)){
					foreach($d_competences as $d_comp){
						$page["competences"][$d_comp["sac_attest_competence"]]["comp_acquise"]=$d_comp["sac_acquise"];
					}
				}
			}
		}
		
		// LE STAGIAIRE
		$req_sta=$Conn->query("SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id=" . $stagiaire_id . ";");
		$d_stagiaire=$req_sta->fetch();
		if(!empty($d_stagiaire)){
			
			$page["sta_nom"]=$d_stagiaire["sta_nom"];
			$page["sta_prenom"]=$d_stagiaire["sta_prenom"];
		}
			
		// INFO JURIDIQUE
		$data["juridique"]=get_juridique($acc_societe,$d_action["act_agence"]);
		
		// Date du doc
		
		$data["date_doc"]=strftime("%A %d %B %Y",time());
		
		// CALCUL DE LA DURRE
		$calcul_duree=true;
		$req_h=$ConnSoc->query("SELECT ase_h_deb,ase_h_fin FROM Actions_Sessions,Actions_Stagiaires_Sessions 
		WHERE ase_id=ass_session AND ass_stagiaire=" . $stagiaire_id . " AND ass_action=" . $action_id . " ORDER BY ase_h_deb;");
		$d_heures=$req_h->fetchAll();
		if(!empty($d_heures)){
			$duree=new DateTime("2000-01-01 00:00:00");
			foreach($d_heures as $h){						
				if(!empty($h["ase_h_deb"]) AND !empty($h["ase_h_fin"]) ){
					$DT_deb =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $h["ase_h_deb"] . ":00");
					$DT_fin =date_create_from_format('Y-m-d H\hi:s',"2000-01-01 " . $h["ase_h_fin"] . ":00");
					$duree->add($DT_deb->diff($DT_fin));
				}else{
					$calcul_duree=false;
				}						
			}
		}
		if($calcul_duree){
			$page["att_duree"]=$duree->format("H \H i");
		}else{
			$page["att_duree"]="";
		}

		if(!empty($page)){
			$data["pages"][]=$page;
		}
		
		$data["pdf"]="attestation_". $action_id . "_" . $stagiaire_id; */
	if(!empty($erreur)){
		return false;	
	}else{
		return $data;
	}
}?>
