<?php

// RETOURNE LA TABLE Clients_Sous_Categories qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_cli_sous_categories(){

	global $Conn;
	
	$result[0]=array(
		"csc_libelle" => "",
	);
	
	$sql="SELECT csc_id,csc_libelle,csc_couleur FROM Clients_Sous_Categories ORDER BY csc_id";
	$req=$Conn->prepare($sql);
	$req->execute();
	$sous_categories = $req->fetchAll();
	if(!empty($sous_categories)){
		foreach($sous_categories as $csc){
			$result[$csc["csc_id"]]=array(
				"csc_libelle" => $csc["csc_libelle"],
				"csc_couleur" => $csc["csc_couleur"]
			);
		}
	}

    return $result;
}
