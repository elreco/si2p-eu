<?php

/* RETOURNE LA LISTES DES VEHICULES DISPO A UNE DATE D'ACTIONS

param optionnel  
$agence : permet de seulement retourné les vehicules d'une agence

SCRIP A MODIFIER APRES LA GESTION DU PLANNING VEHICULE
*/

function get_vehicules_dispo($societe,$agence,$action){

	global $Conn;
	
	$sql="SELECT veh_id,veh_libelle FROM Vehicules WHERE veh_societe=:societe";
	if($agence>0){
		$sql.=" AND veh_agence=:agence";
	}
	$sql.=" ORDER BY veh_libelle";
	$req = $Conn->prepare($sql);
	
	$req->bindParam(":societe",$societe);
	if($agence>0){
		$req->bindParam(":agence",$agence);
	}
	$req->execute();
	$vehicules = $req->fetchAll();

    return $vehicules;
}
