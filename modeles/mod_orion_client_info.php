<?php

// RETOURNE LA TABLE Clients_Infos qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_client_infos(){

	global $Conn;
	
	$result[0]="";
	
	$sql="SELECT * FROM Clients_Infos ORDER BY cin_id";
	$req=$Conn->prepare($sql);
	$req->execute();
	$client_infos = $req->fetchAll();
	if(!empty($client_infos)){
		foreach($client_infos as $ci){
			$result[$ci["cin_id"]]=$ci["cin_libelle"];
		}
	}

    return $result;
}
