<?php

// RETOURNE LES FAMILLES DE PRODUITS
// AUTEUR FG

function get_juridique($societe,$agence){

	global $Conn;

	$erreur=false;
	$juridique=array();
	$age_structure=0;

	$responsable=0;

	if($agence>0){
		$sql="SELECT age_nom,age_ad1,age_ad2,age_ad3,age_cp,age_ville,age_tel,age_fax,age_mail,age_site
		,age_siret,age_siren,age_tva,age_ape,age_rcs,age_capital,age_type,age_structure,age_logo_1,age_logo_2,age_qualite_1,age_qualite_2,age_responsable
		FROM Agences WHERE age_id=" . $agence;
		$req=$Conn->query($sql);
		$d_agence=$req->fetch();
		if(!empty($d_agence)){
			$age_structure=$d_agence["age_structure"];
			if($age_structure==1){
				$juridique["nom"]=$d_agence["age_nom"];
				$juridique["agence"]="";
				$juridique["siren"]=$d_agence["age_siren"];
				$juridique["tva"]=$d_agence["age_tva"];
				$juridique["ape"]=$d_agence["age_ape"];
				$juridique["rcs"]=$d_agence["age_rcs"];
				$juridique["capital"]=$d_agence["age_capital"];
				$juridique["type"]=$d_agence["age_type"];
			}else{
				$juridique["nom"]="";
				$juridique["agence"]=$d_agence["age_nom"];
			}
			$juridique["ad1"]=$d_agence["age_ad1"];
			$juridique["ad2"]=$d_agence["age_ad2"];
			$juridique["ad3"]=$d_agence["age_ad3"];
			$juridique["cp"]=$d_agence["age_cp"];
			$juridique["ville"]=$d_agence["age_ville"];
			$juridique["tel"]=$d_agence["age_tel"];
			$juridique["fax"]=$d_agence["age_fax"];
			$juridique["mail"]=$d_agence["age_mail"];
			$juridique["site"]=$d_agence["age_site"];
			if(!empty($d_agence["age_logo_1"])){
				$juridique["logo_1"]="documents/societes/logos/" . $d_agence["age_logo_1"];
			}
			if(!empty($d_agence["age_logo_2"])){
				$juridique["logo_2"]="documents/societes/logos/" . $d_agence["age_logo_2"];
			}
			if(!empty($d_agence["age_qualite_1"])){
				$juridique["qualite_1"]="documents/societes/logos/" . $d_agence["age_qualite_1"];
			}
			if(!empty($d_agence["age_qualite_2"])){
				$juridique["qualite_2"]="documents/societes/logos/" . $d_agence["age_qualite_2"];
			}
			if(!empty($d_agence["age_responsable"])){
				$juridique["responsable"]=$d_agence["age_responsable"];
			}
			$juridique["siret"]=$d_agence["age_siret"];




		}else{
			$erreur=true;
		}
	}

	if(!$erreur){

		if($age_structure==0){

			$sql="SELECT soc_nom,soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville,soc_tel,soc_fax,soc_mail,soc_site
			,soc_type,soc_capital,soc_rcs,soc_ape,soc_siret,soc_siren,soc_tva,soc_num_existence
			,soc_logo_1,soc_logo_2,soc_qualite_1,soc_qualite_2,soc_region_nom,soc_responsable,soc_cnaps
			FROM Societes WHERE soc_id=" . $societe;
			$req=$Conn->query($sql);
			$d_societe=$req->fetch();
			if(!empty($d_societe)){

				$juridique["nom"]=$d_societe["soc_nom"];
				if($agence==0){
					$juridique["ad1"]=$d_societe["soc_ad1"];
					$juridique["ad2"]=$d_societe["soc_ad2"];
					$juridique["ad3"]=$d_societe["soc_ad3"];
					$juridique["cp"]=$d_societe["soc_cp"];
					$juridique["ville"]=$d_societe["soc_ville"];
					$juridique["tel"]=$d_societe["soc_tel"];
					$juridique["fax"]=$d_societe["soc_fax"];
					$juridique["mail"]=$d_societe["soc_mail"];
					$juridique["site"]=$d_societe["soc_site"];
					if(!empty($d_societe["soc_logo_1"])){
						$juridique["logo_1"]="documents/societes/logos/" . $d_societe["soc_logo_1"];
					}
					if(!empty($d_societe["soc_logo_2"])){
						$juridique["logo_2"]="documents/societes/logos/" . $d_societe["soc_logo_2"];
					}
					if(!empty($d_societe["soc_qualite_1"])){
						$juridique["qualite_1"]="documents/societes/logos/" . $d_societe["soc_qualite_1"];
					}
					if(!empty($d_societe["soc_qualite_2"])){
						$juridique["qualite_2"]="documents/societes/logos/" . $d_societe["soc_qualite_2"];
					}

					$juridique["siret"]=$d_societe["soc_siret"];

				}


				$juridique["siren"]=$d_societe["soc_siren"];
				$juridique["tva"]=$d_societe["soc_tva"];
				$juridique["ape"]=$d_societe["soc_ape"];
				$juridique["rcs"]=$d_societe["soc_rcs"];
				$juridique["capital"]=$d_societe["soc_capital"];
				$juridique["type"]=$d_societe["soc_type"];
				$juridique["num_existence"]=$d_societe["soc_num_existence"];
				$juridique["region_nom"]=$d_societe["soc_region_nom"];
				$juridique["cnaps"]=$d_societe["soc_cnaps"];

				if(empty($juridique["responsable"])){
					if(!empty($d_societe["soc_responsable"])){
						$juridique["responsable"]=$d_societe["soc_responsable"];
					}
				}
			}else{
				$erreur=true;
			}
		}
	}

	if(!$erreur){
		if(!empty($juridique["site"])){
			$juridique["site"]=str_replace("https://","",$juridique["site"]);
			$juridique["site"]=str_replace("http://","",$juridique["site"]);
		}

	}
	if(!$erreur AND !empty($juridique["responsable"])){

		$sql="SELECT uti_id,uti_nom,uti_prenom,uti_fonction FROM Utilisateurs WHERE uti_id=" . $juridique["responsable"] . ";";
		$req=$Conn->query($sql);
		$d_utilisateur=$req->fetch();

		if(!empty($d_utilisateur)){
			$juridique["resp_ident"]=$d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"];
			$juridique["resp_fonction"]=$d_utilisateur["uti_fonction"];
			$responsable = $d_utilisateur["uti_id"];
			if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/Signatures/tampon_" . $societe . "_" . $agence . "_" . $responsable . ".png")){
				// systeme fichier unique signature et tampon
				$juridique["resp_signature"]="Documents/Societes/Signatures/tampon_" . $societe . "_" . $agence . "_" . $responsable . ".png";

			}elseif(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Utilisateurs/Signatures/signature_" . $juridique["responsable"] . ".png")){
				// systeme superposition signature et tampon
				$juridique["resp_signature"]="Documents/Utilisateurs/Signatures/signature_" . $juridique["responsable"] . ".png";
				if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/Signatures/tampons/age_" . $agence . ".png")){
					$juridique["resp_signature_tampon"]="Documents/Societes/Signatures/tampons/age_" . $agence . ".png";
				}elseif(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/Signatures/tampons/soc_" . $societe . ".png")){
					$juridique["resp_signature_tampon"]="Documents/Societes/Signatures/tampons/soc_" . $societe . ".png";
				}
			}
		}
	}


	if($erreur){
		return $erreur;
	}else{

		// construction du footer

		$footer=$juridique["nom"];
		if(!empty($juridique["agence"])){
			$footer.=" - Agence " . $juridique["agence"];
		}
		$footer.=" - " . $juridique["type"] . " au capital de " . $juridique["capital"] . " - R.C.S. " . $juridique["rcs"];
		if(!empty($juridique["holding_nom"])){
			$footer.=" - Filiale de " . $juridique["holding_nom"] . " " . $juridique["holding_type"] . " au capital de " . $juridique["holding_capital"];
		}
		$footer.="<br/> APE " . $juridique["ape"] . " - Siret " . $juridique["siret"] . " - n° TVA " . $juridique["tva"] . "<br/>Déclaration d'activité enregistrée sous le numéro " . $juridique["num_existence"] . " auprès du préfet de région de " . $juridique["region_nom"];
		if(!empty($juridique["cnaps"])){
			$footer.="<br/>N° CNAPS : " . $juridique["cnaps"] . ". L’autorisation d’exercice ne confère aucune prérogative de puissance publique à l’entreprise ou aux personnes qui en bénéficient.";
		}

		$juridique["footer"]=$footer;

		return $juridique;
	}
}
