<?php


function get_erreur_txt($erreur){

	$erreur_txt="";

	switch ($erreur) {
		
		// valeur negative = message correct
		case -990:
			$erreur_txt="Le fichier a bien été enregistré";
			break;

		case 0:
		$erreur_txt="OK";
		break;

		case 1:
		$erreur_txt="Paramètres insufisants. Les données n'ont pas été enregistrées.";
		break;

		case 2:
		$erreur_txt="Le dossier existe déjà.";
		break;

		case 3:
		$erreur_txt="Le fichier existe déjà.";
		break;
		case 4:
		$erreur_txt="Impossible de coller dans le même dossier.";
		break;
		case 5:
		$erreur_txt="Veuillez sélectionner au moins un élément.";
		break;
		case 6:
		$erreur_txt="Veuillez compléter les champs du formulaire.";
		break;
		case 7:
		$erreur_txt="Veuillez sélectionner un ou plusieurs éléments.";
		break;
			// erreur lié au planning
		
		case 970:
		$erreur_txt="Cet intervenant existe déjà";
		break;
		
			// erreur lié aux utilisateurs
		
		case 980:
		$erreur_txt="L'utilisateur existe déjà";
		break;
		
			// erreur lié à l'upload
		case 990:
		$erreur_txt="Le fichier n'a pas été chargé";
		break;

		case 991:
		$erreur_txt="Format de fichier incompatible";
		break;

		case 992:
		$erreur_txt="Le fichier est trop lourd";
		break;

		case 993:
		$erreur_txt="Le dossier de destination n'existe pas";				
		break;
		
		case 994:
		$erreur_txt="Le fichier existe déjà sur le serveur";				
		break;
	};	

	return $erreur_txt;
};




?>	
