<?php

/*--------------------------------------------
	FONCTION D'INSERTION 
----------------------------------------------*/

function set_new_passe($acc_ref,$acc_ref_id,$acc_mail,$modele_mail){
	
	date_default_timezone_set('Europe/Paris');
	
	include_once 'mod_envoi_mail.php';
	
	global $Conn;
	
	$erreur=0;
	
	if(!empty($acc_ref)){
		$acc_ref=intval($acc_ref);
	}
	if(!empty($acc_ref_id)){
		$acc_ref_id=intval($acc_ref_id);
	}
	if($acc_ref==0 OR $acc_ref_id==0){
		$erreur=1;
	}
	
	//echo("erreur : " . $erreur);
	if($erreur==0){
		
		$car_spe=array("?","@",".",";",":","!","_","-");

		$password=chr(rand(65,90)); // MAJ
		$password.=chr(rand(97,122)); // MIN
		$password.=chr(rand(97,122)); // MIN
		$password.=chr(rand(97,122)); // MIN
		$password.=chr(rand(97,122)); // MIN
		$password.=rand(0,9); // CHIFFRE
		$password.=rand(0,9); // CHIFFRE
		$password.=$car_spe[rand(0,7)]; //caractère spécial
		$password=str_shuffle($password);

		if(!empty($password)){
			
			if($acc_ref==1){
			
				// utilisateur -> on genere un password temporaire
				
				$timestamp_validite=mktime(date("H")+1, date("i"), date("s") , date("m"), date("d"),   date("Y"));
				$validite=date("Y-m-d H:i:s",$timestamp_validite);
				
			}else{
				
				$timestamp_validite=mktime(23, 59, 59, date("m"), date("d")+90,   date("Y"));
				$validite=date("Y-m-d H:i:s",$timestamp_validite);
			}					

			try{
				$Conn->query("UPDATE Acces SET acc_uti_passe='" . $password . "', 
				acc_passe_modif='" . $validite . "'
				WHERE acc_ref=" . $acc_ref . " AND acc_ref_id=". $acc_ref_id . ";");
			}Catch(Exception $e){
				$erreur=1;
				echo($e->getMessage());
			}
		}else{
			$erreur=1;
		}
	}
	
	//echo("erreur : " . $erreur . "<br/>");
	// mod de passe genere on evoie par mail
	
	if($erreur==0){
		
		if($acc_ref==1){

			$sql="SELECT uti_nom,uti_prenom FROM Utilisateurs WHERE uti_id=" . $acc_ref_id . ";";
			$req=$Conn->query($sql);
			$d_destinataire=$req->fetch();
			
		}elseif($acc_ref==2){
			
			$sql="SELECT con_nom,con_prenom FROM Contacts WHERE con_id=" . $acc_ref_id . ";";
			$req=$Conn->query($sql);
			$d_destinataire=$req->fetch();
			
		}elseif($acc_ref==3){
			
			$sql="SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id=" . $acc_ref_id . ";";
			$req=$Conn->query($sql);
			$d_destinataire=$req->fetch();
			
		}
		
		if(isset($d_destinataire)){
			
			if(!empty($d_destinataire)){
				
				$param_mail=array(
					"sujet" => "Mot de passe",
					"identite" => $d_destinataire[1] . " " . $d_destinataire[0],
					"password" => $password
				);
				
				$adr_mail=array(
					"0" => array(
						"adresse" => $acc_mail,
						"nom" => $d_destinataire[1] . " " . $d_destinataire[0]
					)
				);
				
				$mail=envoi_mail($modele_mail,$param_mail,$adr_mail);
				if($mail!==True){
					$erreur=1;
				}
			}else{
				
			}
		}else{
			$erreur=1;
		}
	}
	// FIN ENVOIE MAIL
	
	if($erreur==0){
		return $password;	
	}else{
		return false;
	}
	
}

?>
	


