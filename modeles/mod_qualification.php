<?php
//include_once '/connexion.php';
function get_qualifications()
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM qualifications");
    $req->execute();

    $qualifications = $req->fetchAll();

    return $qualifications;
}
function get_qualification($id)
{

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM qualifications WHERE qua_id = " . $id);
    $req->execute();

    $qualifications = $req->fetch();

    return $qualifications;
}
function get_qualification_select($selected)
{

    global $Conn;

    $service_select = "";
    $req            = $Conn->query("SELECT * FROM qualifications ORDER BY qua_libelle");
    while ($donnees = $req->fetch()) {
        if($selected == 0){
            $service_select = $service_select . "<option value='" . $donnees["qua_id"] . "' >" . $donnees["qua_libelle"] . "</option>";
        }else{
            if($donnees['qua_id'] == $selected){
                $service_select = $service_select . "<option selected value='" . $donnees["qua_id"] . "' >" . $donnees["qua_libelle"] . "</option>";
            }else{
                $service_select = $service_select . "<option value='" . $donnees["qua_id"] . "' >" . $donnees["qua_libelle"] . "</option>";
            }
           
        }
        
    }
    ;
    return $service_select;

}