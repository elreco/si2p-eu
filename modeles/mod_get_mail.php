<?php

//  PERMET DE GENERER LE CONTENU HTML UN MODELE

function get_mail($modele,$param){
	
	$body_mail=file_get_contents($_SERVER['DOCUMENT_ROOT']."/email-templates/" . $modele .".php");
	if(!empty($param)){
		foreach($param as $par_k=>$par_v){
			$body_mail = str_replace('%'. $par_k . '%',$par_v, $body_mail); 
		}
	}
	
	return $body_mail;
}