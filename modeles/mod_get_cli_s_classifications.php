<?php 

function get_cli_s_classifications($classification){
	
	global $Conn;

	if(!empty($classification)){
		$req = $Conn->query("SELECT * FROM Clients_Sous_Classifications WHERE csc_classification=" . $classification . " ORDER BY csc_libelle;");
		$s_classifications = $req->fetchAll();
		return $s_classifications;
	}

}
?>