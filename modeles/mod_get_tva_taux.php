<?php


// retourne un tableau dont les clés sont les TVA en vigeur à date_ref
function get_tva_taux($date_ref,$tva_id){
	
	global $Conn;
	
	$tva=array();
	
	if(!empty($date_ref)){
	
		$dateTime_date_ref=date_create_from_format('Y-m-d',$date_ref);
		if(is_bool($dateTime_date_ref)){
			$dateTime_date_ref=date_create_from_format('d/m/Y',$date_ref);
		}
	
		$sql="SELECT tpe_id,tpe_taux,tpe_tva FROM Tva_Periodes WHERE 
		(ISNULL(tpe_date_deb) OR tpe_date_deb<='" . $dateTime_date_ref->format("Y-m-d") . "') 
		AND 
		(ISNULL(tpe_date_fin) OR tpe_date_fin>='" . $dateTime_date_ref->format("Y-m-d") . "')";	
		if(!empty($tva_id)){
			$sql.=" AND tpe_tva=" . $tva_id;
		}
		$req=$Conn->query($sql);
		$tva_periode=$req->fetchAll();
		if(!empty($tva_periode)){
			foreach($tva_periode as $tp){
				$tva[$tp["tpe_tva"]]=array(
					"tpe_id" => $tp["tpe_id"],
					"tpe_taux" => $tp["tpe_taux"]
				);
			}
		}
	}
	
	return $tva;
}

?>