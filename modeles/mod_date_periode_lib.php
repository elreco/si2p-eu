 <?php
 
	function date_periode_lib($arr_periode,$format){
		
		$periode="";
		
		/*$arr_periode[]=array(
			"date_deb" => ,
			"demi_deb" => ,
			"h_deb" => ,
			"date_fin" => ,
			"demi_fin" => ,
			"h_fin" => 	
		);*/
		
		foreach($arr_periode as $p){
			
			$dtime = DateTime::createFromFormat("Y-m-d",$p["date_deb"]);
			$date_f_fr=$dtime->getTimestamp();
			$lib=utf8_encode(strftime($format,$date_f_fr));
			
			if($p["date_deb"]==$p["date_fin"]){
				// même journée
				$periode="Le  " . $lib;
				if($p["demi_deb"]==$p["demi_fin"]){
					if(!empty($p["h_deb"]) AND !empty($p["h_fin"])){
						$periode.=" de " . $p["h_deb"] . " à " . $p["h_fin"];
					}elseif($p["demi_fin"]==2){
						$periode.=" après-midi";
					}else{
						$periode.=" matin";
					}
				}elseif(!empty($p["h_deb"]) AND !empty($p["h_fin"])){
					$periode.=" de " . $p["h_deb"] . " à " . $p["h_fin"];				
				}
				
			}else{
		
				$periode="Du ";
			
				$periode.=$lib;
				if($p["demi_deb"]==2){
					$periode.=" après-midi";
				}
				$periode.=" au ";
				
				$dtime = DateTime::createFromFormat("Y-m-d",$p["date_fin"]);
				$date_f_fr=$dtime->getTimestamp();
				$lib=utf8_encode(strftime($format,$date_f_fr));
				$periode.=$lib;
				if($p["demi_fin"]==1){
					$periode.=" matin";
				}
			}
		}
		return  $periode;
	}
?>