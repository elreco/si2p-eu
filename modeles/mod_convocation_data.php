<?php 

function convocation_data($action_client,$stagiaire){

	global $ConnSoc;
	global $Conn;
	global $acc_societe;

	$erreur=false;
	$data=array();

	if(!empty($action_client)){
		
		$sql="SELECT acl_pro_libelle,acl_produit,acl_client,acl_facturation
		,act_id,act_agence,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville
		,con_id,con_cli_nom,con_cli_service,con_cli_ad1,con_cli_ad2,con_cli_ad3,con_cli_cp,con_cli_ville 
		FROM Actions_Clients INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
		LEFT JOIN Conventions ON (Actions_Clients.acl_id=Conventions.con_action_client)
		WHERE acl_id=" . $action_client . ";";
		$req=$ConnSoc->query($sql);
		$d_action=$req->fetch(PDO::FETCH_ASSOC);
		if(!empty($d_action)){
			
			$data["action"]=$d_action["act_id"];
			
			$data["acl_pro_libelle"]=$d_action["acl_pro_libelle"];
			if(!empty($d_action["con_id"])){
				$data["adresse_client"]=$d_action["con_cli_nom"];
				if(!empty($d_action["con_cli_service"])){
					$data["adresse_client"].="<br/>" . $d_action["con_cli_service"];
				}
				if(!empty($d_action["con_cli_ad1"])){
					$data["adresse_client"].="<br/>" . $d_action["con_cli_ad1"];
				}
				if(!empty($d_action["con_cli_ad2"])){
					$data["adresse_client"].="<br/>" . $d_action["con_cli_ad2"];
				}
				if(!empty($d_action["con_cli_ad3"])){
					$data["adresse_client"].="<br/>" . $d_action["con_cli_ad3"];
				}
				$data["adresse_client"].="<br/>" . $d_action["con_cli_cp"] . " " . $d_action["con_cli_ville"];				
			}
			
			// lieu d'intervention 
			$data["lieu"]=$d_action["act_adr_nom"];
			if(!empty($d_action["act_adr_service"])){
				$data["lieu"].="<br/>" . $d_action["act_adr_service"];
			}
			if(!empty($d_action["act_adr1"])){
				$data["lieu"].="<br/>" . $d_action["act_adr1"];
			}
			if(!empty($d_action["act_adr2"])){
				$data["lieu"].="<br/>" . $d_action["act_adr2"];
			}
			if(!empty($d_action["act_adr3"])){
				$data["lieu"].="<br/>" . $d_action["act_adr3"];
			}
			$data["lieu"].="<br/>" . $d_action["act_adr_cp"] . " " . $d_action["act_adr_ville"];	

		}else{
			$erreur=true;
		}
	}
	
	if(empty($erreur)){
		
		// PAS DE CONVENTION ON PREND ADRESSE DE FAC PAR DEFAUT
		if(empty($d_action["con_id"])){
			
			$sql="SELECT * FROM Adresses WHERE adr_type=1 AND adr_defaut AND adr_ref=1 AND adr_ref_id=" . $d_action["acl_client"] . ";";
			$req=$Conn->query($sql);
			$d_adresse=$req->fetch(PDO::FETCH_ASSOC);
			if(!empty($d_adresse)){	
				
				$data["adresse_client"]=$d_adresse["adr_nom"];
				if(!empty($d_adresse["adr_service"])){
					$data["adresse_client"].="<br/>" . $d_adresse["adr_service"];
				}
				if(!empty($d_adresse["adr_ad1"])){
					$data["adresse_client"].="<br/>" . $d_adresse["adr_ad1"];
				}
				if(!empty($d_adresse["adr_ad2"])){
					$data["adresse_client"].="<br/>" . $d_adresse["adr_ad2"];
				}
				if(!empty($d_adresse["adr_ad3"])){
					$data["adresse_client"].="<br/>" . $d_adresse["adr_ad3"];
				}
				$data["adresse_client"].="<br/>" . $d_adresse["adr_cp"] . " " . $d_adresse["adr_ville"];				
			}
		}
		
		// INFO JURIDIQUE
		if($d_action["acl_facturation"]==1){
			$data["juridique"]=get_juridique(4,4);
		}else{
			$data["juridique"]=get_juridique($acc_societe,$d_action["act_agence"]);
		}
		
		// INFO CONVOC
		$sql="SELECT pro_reference_sta,pso_reference_sta FROM Produits LEFT JOIN Produits_Societes 
		ON (Produits.pro_id=Produits_Societes.pso_produit AND pso_societe=" . $acc_societe . ") 
		WHERE pro_id=" . $d_action["acl_produit"] . ";";
		$req=$Conn->query($sql);
		$d_produit=$req->fetch(PDO::FETCH_ASSOC);
		if(!empty($d_produit)){	
			if(!empty($d_produit["pso_reference_sta"])){
				$data["reference_sta"]=$d_produit["pso_reference_sta"];
			}else{
				$data["reference_sta"]=$d_produit["pro_reference_sta"];
			}
		}
		
		// Date du doc
		
		$data["date_doc"]=strftime("%A %d %B %Y",time());
		
		
		// DONNE STAGIARES
		
		$data["pages"]=array();
		
		$data["pdf"]="";
		
		if(!empty($stagiaire)){
			$stagiaires=array(
				"0" => array(
					"ast_stagiaire" => $stagiaire
				)
			);
			$data["pdf"]="convoc_" . $d_action["act_id"] . "_" . $stagiaire;
		}else{
		
			$req=$ConnSoc->query("SELECT ast_stagiaire FROM Actions_Stagiaires WHERE ast_action_client=" . $action_client . ";");
			$stagiaires=$req->fetchAll();
			
			$data["pdf"]="convoc_" . $action_client;
			
		}
		
		if(!empty($stagiaires)){
			foreach($stagiaires as $k => $sta){
				
				$data["pages"][$k]=array(
					"sta_nom" => "",
					"sta_prenom" => "",
					"date_deb" => "",
					"date_fin" => "",
					"duree" => "",
					"dates" => "",
					"date_deb" => "",
					"heure_deb" => "",
					"horaire" => ""
				);
				
				$req_sta=$Conn->query("SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id=" . $sta["ast_stagiaire"] . ";");
				$d_stagiaire=$req_sta->fetch();
				if(!empty($d_stagiaire)){
					
					$data["pages"][$k]["sta_nom"]=$d_stagiaire["sta_nom"];
					$data["pages"][$k]["sta_prenom"]=$d_stagiaire["sta_prenom"];
					
				}
				
				$calcul_duree=true;
				$h_deb="";
				$h_fin="";
				$pda_id=0;
				
				$req_sta=$ConnSoc->query("SELECT pda_id,pda_date,pda_demi,ase_h_deb,ase_h_fin FROM Plannings_Dates,Actions_Sessions,Actions_Stagiaires_Sessions 
				WHERE pda_id=ase_date AND ase_id=ass_session AND ass_stagiaire=" . $sta["ast_stagiaire"] . " AND pda_type=1 AND pda_ref_1=" . $d_action["act_id"] . "
				ORDER BY pda_date,pda_demi,ase_h_deb;");
				$d_dates=$req_sta->fetchAll();
				if(!empty($d_dates)){
					
					$dt_deb=date_create_from_format('Y-m-d',$d_dates[0]["pda_date"]);					
					if(!is_bool($dt_deb)){
						$tt_deb=$dt_deb->getTimestamp();
						$data["pages"][$k]["date_deb"]=strftime("%A %d %B %Y",$tt_deb);
					}
					$dt_fin=date_create_from_format('Y-m-d',$d_dates[count($d_dates)-1]["pda_date"]);					
					if(!is_bool($dt_fin)){
						$tt_fin=$dt_fin->getTimestamp();
						$data["pages"][$k]["date_fin"]=strftime("%A %d %B %Y",$tt_fin);
					}
					
					if(!empty($d_dates[0]["ase_h_deb"])){
						$data["pages"][$k]["heure_deb"]=$d_dates[0]["ase_h_deb"];
						$h_deb=$d_dates[0]["ase_h_deb"];
					}
					
					if(!empty($d["ase_h_fin"]) ){
						$h_fin=$d["ase_h_fin"];
					}
					
					$duree="";
					
					
					$date_deb=$d_dates[0]["pda_date"];
					$demi_deb=$d_dates[0]["pda_demi"];
					$date_fin=$d_dates[0]["pda_date"];
					$demi_fin=$d_dates[0]["pda_demi"];
					$pda_id=$d_dates[0]["pda_id"];
					$periode="";

					foreach($d_dates as $d){
						
						// nouvelle demi-journée
						if($d["pda_id"]!=$pda_id){
							
							if($d["pda_demi"]==2){
						
								if($demi_fin!=1 OR ($d["pda_date"]!=$date_fin)){
									if(!empty($periode)){
										$periode.="<br/>";
									}
									$periode.=planning_periode_lib($date_deb,$demi_deb,$date_fin,$demi_fin);
									$date_deb=$d["pda_date"];
									$demi_deb=$d["pda_demi"];
									$date_fin=$d["pda_date"];
									$demi_fin=$d["pda_demi"];
								}else{
									$date_fin=$d["pda_date"];
									$demi_fin=$d["pda_demi"];
								}
							}else{
								
								// c'est un matin
								
								$DT_periode=date_create_from_format('Y-m-d',$d["pda_date"]);						
								$j=$DT_periode->format("N");
								if($j==1){
									// C'est un lundu matin
									$DT_periode->sub(new DateInterval('P3D'));							
								}else{
									$DT_periode->sub(new DateInterval('P1D'));			
								}
								$d_compare=$DT_periode->format("Y-m-d");
								
								if($demi_fin!=2 OR $date_fin!=$d_compare){
									if(!empty($periode)){
										$periode.="<br/>";
									}
									$periode.=planning_periode_lib($date_deb,$demi_deb,$date_fin,$demi_fin);
									$date_deb=$d["pda_date"];
									$demi_deb=$d["pda_demi"];
									$date_fin=$d["pda_date"];
									$demi_fin=$d["pda_demi"];					
								}else{
									$date_fin=$d["pda_date"];
									$demi_fin=$d["pda_demi"];
								}
								
							}
							
						}
						
						if(!empty($d["ase_h_deb"]) AND !empty($d["ase_h_fin"]) ){
							if($d["ase_h_deb"]<$h_deb){
								$h_deb=$d["ase_h_deb"];
							}
							if($d["ase_h_fin"]>$h_fin){
								$h_fin=$d["ase_h_fin"];
							}
							
							$DT_deb =date_create_from_format('Y-m-d H\hi:s', $d["pda_date"] . " " . $d["ase_h_deb"] . ":00");
							$DT_fin =date_create_from_format('Y-m-d H\hi:s', $d["pda_date"] . " " . $d["ase_h_fin"] . ":00");
							//$duree->add($DT_deb->diff($DT_fin));

							$interval = $DT_deb->diff($DT_fin);

							//$duree->add(date_diff($DT_deb, $DT_fin));
							/* var_dump($duree);
							die(); */
							if(!empty($duree)){
								$duree_before = $duree;
							}else{
								$duree_before = 0;
							}
							
							$duree = sprintf(
								'%d:%02d',
								($interval->d * 24) + $interval->h,
								$interval->i
							);
							if(!empty($duree_before)){
								$duree_add = explode_time($duree) + explode_time($duree_before);
							}else{
								$duree_add = explode_time($duree);
							}
							
							$duree = second_to_hhmm($duree_add);
							
						}else{
							$calcul_duree=false;
						}						
					}
					if(!empty($periode)){
						$periode.="<br/>";
					}
					$periode.=planning_periode_lib($date_deb,$demi_deb,$date_fin,$demi_fin);
					
					if(!empty($h_deb) AND !empty($h_fin) ){
						$data["pages"][$k]["horaire"]=$h_deb . " / " . $h_fin;
					}
					if($calcul_duree){
						$data["pages"][$k]["duree"]=str_replace(":", "h", $duree);
					}
					if(!empty($periode)){
						$data["pages"][$k]["dates"]=$periode;
					}
					
					
					
				}	
				
			}
		}
	} 
	
	if(!empty($erreur)){
		return false;	
	}else{
		return $data;
	}
}?>
