<?php

// suppression d'une correspondance et traitement associé
// retourne true en cas de résussite false en cas d'echec

function del_correspondance($correspondance){
	
	
	if(empty($correspondance)){	
		return false;
	}
	
	global $Conn;
	
	// SUR LA CORRESPONDANCE
	
	$sql="SELECT cor_contact,cor_utilisateur,cor_date FROM Correspondances WHERE cor_id=" . $correspondance . ";";
	$req=$Conn->query($sql);
	$d_corresp=$req->fetch();
	if(empty($d_corresp)){
		return false;
	}

	if($d_corresp["cor_contact"]>0 AND $d_corresp["cor_utilisateur"]>0 AND !empty($d_corresp["cor_date"])){
		
		// on recherche s'il y a une correspondance plus récente	
		$sql="SELECT cor_id FROM Correspondances WHERE cor_contact=" . $d_corresp["cor_contact"] . " AND cor_utilisateur=" . $d_corresp["cor_utilisateur"];
		$sql.=" AND cor_date>'" . $d_corresp["cor_date"] . "';";
		$req=$Conn->query($sql);
		$d_recente=$req->fetch();
		if(empty($d_recente)){
			
			// on tente de supprimer la correspondance la plus récente => la précédente ne sera plus traité
			
			// on recherche la corresp précédente pour la noté non traitée
			$sql="SELECT cor_id FROM Correspondances WHERE cor_contact=" . $d_corresp["cor_contact"] . " AND cor_utilisateur=" . $d_corresp["cor_utilisateur"];
			$sql.=" AND cor_date<'" . $d_corresp["cor_date"] . "' ORDER BY cor_date DESC;";
			$req=$Conn->query($sql);
			$d_precedente=$req->fetchAll();
			if(!empty($d_precedente)){
				
				$sql="UPDATE Correspondances SET cor_rappel = 0 WHERE cor_id=" . $d_precedente[0]["cor_id"] . ";";
				try{
					$req=$Conn->query($sql);
				}catch (Exception $e) {
					echo 'Exception reçue : ',  $e->getMessage(), "\n";
					return false;
				}
			}
		}
	}

	// suppression de la correspondance
	try {
		$req=$Conn->query("DELETE FROM Correspondances WHERE cor_id=" . $correspondance . " ;");
		$retour=array(
			"correspondance" => $correspondance,
			"non_traite" => 0
		);
		if(isset($d_precedente)){
			if(!empty($d_precedente["cor_id"])){
				$retour["non_traite"]=$d_precedente["cor_id"];
			}
		}
		return $retour;
	}
	catch( PDOException $Exception ) {
		$erreur=$Exception->getMessage();
		return false;
	}
}
?>