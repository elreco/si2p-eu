<?php

// RETOURNE LES FAMILLES DE PRODUITS
// AUTEUR FG

function get_pro_s_familles($famille){

	global $Conn;
	
	$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles";
	if(!empty($famille)){
		$sql.=" WHERE psf_pfa_id=" . $famille;
	}
	$sql.=" ORDER BY psf_libelle";
	$req = $Conn->query($sql);
	$req->execute();
	$s_familles = $req->fetchAll();

    return $s_familles;
}

function get_pro_s_s_familles($sous_famille){

	global $Conn;
	
	$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles";
	if(!empty($famille)){
		$sql.=" WHERE pss_psf_id=" . $sous_famille;
	}
	$sql.=" ORDER BY pss_libelle";
	$req = $Conn->query($sql);
	$req->execute();
	$s_familles = $req->fetchAll();

    return $s_familles;
}
