<?php

	// VERIFIE Si UN CLIENT EXISTE DEJA

function check_suspect($suspect,$code,$nom,$siren =""){
	
	global $ConnSoc;

	$result=array(
		"doublon_code" => false,
		"doublon_nom" => false,
		"doublon_siren" => false
	);
	
	// retourne uniquement la présence de boublon et les societe associé au doublon
	// les conséquence liées à la présence de doublon sont a gérer dans le script d'appel

	
	$sql="SELECT DISTINCT sus_code,sus_nom,sus_siren FROM Suspects";
	$mil="";
	if(!empty($code)){
		$mil.=" OR sus_code=:code";
	}
	if(!empty($nom)){
		$mil.=" OR sus_nom=:nom";
	}
	if(!empty($siren)){
		$mil.=" OR sus_siren=:siren";
	}
	if(!empty($mil)){
		$sql.=" WHERE (" . substr($mil, 4) . ")";
	}
	if(!empty($suspect)){
		$sql.=" AND sus_id=:suspect";
	}
	$req=$ConnSoc->prepare($sql);
	// param
	if(!empty($code)){
		$req->bindParam(":code",$code);
	}
	if(!empty($nom)){
		$req->bindParam(":nom",$nom);
	}
	if(!empty($siren)){
		$req->bindParam(":siren",$siren);
	}
	if(!empty($suspect)){
		$req->bindParam(":suspect",$suspect);
	}
	$req->execute();
	$suspect=$req->fetchAll();
	if(!empty($suspect)){
		foreach($suspect as $suspect){
			
			if(strcasecmp($suspect["sus_code"],$code)==0 AND !empty($code)){		
				$result["doublon_code"]=true;			
			}
			
			if(strcasecmp($suspect["sus_nom"],$nom)==0 AND !empty($nom)){
				$result["doublon_nom"]=true;
			}
			
			if($suspect["sus_siren"]==$siren AND !empty($siren)){
				$result["doublon_siren"]=true;			
			}
		}
		
	}
	return $result;
}
?>
