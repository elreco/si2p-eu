<?php
// return la liste des adresses du suspect ref_id 
// type permet de ne retourner qu'un type d'adresse intervention par exemple
// ATENTION L'adresse par defaut doit etre la première

function get_suspect_adresses($ref_id, $type = 1){
	
	global $ConnSoc;

	$req_sql="SELECT * FROM suspects_adresses WHERE sad_ref_id=:ref_id";
	if($type>0){
		$req_sql.=" AND sad_type=:type";
	}
	$req_sql.=" ORDER BY sad_defaut DESC";

	$req=$ConnSoc->prepare($req_sql);
	$req->bindParam(":ref_id",$ref_id);
	if($type>0){
		$req->bindParam(":type",$type);
	};
	$req->execute();
	$get_suspect_adresses = $req->fetchAll();
	
	return $get_suspect_adresses;
	
}

?>