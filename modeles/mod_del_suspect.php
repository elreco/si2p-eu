<?php


// SUPPRESSION D'UN SUSPECT

function del_suspect($suspect){
	
	Global $ConnSoc;
	
	if(!is_int($suspect)){
		return false;
	}
	
	try{
		$ConnSoc->query("DELETE FROM suspects_correspondances WHERE sco_suspect =" . $suspect . ";");
		$ConnSoc->query("DELETE FROM suspects_contacts WHERE sco_ref_id =" . $suspect . ";");
		$ConnSoc->query("DELETE FROM suspects_adresses WHERE sad_ref_id =" . $suspect . ";");
		$ConnSoc->query("DELETE FROM suspects WHERE sus_id =" . $suspect . ";");		
		return true;
	}catch(Exception $e){
		echo($e->getMessage());		
		return false;
	}
	
}
?>