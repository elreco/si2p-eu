<?php

// RETOURNE LA TABLE Clients_Infos qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_facturation(){

	global $Conn;
	
	$result[0]=array(
		"fac_libelle" => "",
		"fac_pourcentage" => "",
		"fac_societe" => ""
	);
	
	$sql="SELECT fac_id,fac_libelle,fac_pourcentage,fac_societe FROM Facturations ORDER BY fac_id";
	$req=$Conn->prepare($sql);
	$req->execute();
	$pro_famille = $req->fetchAll();
	if(!empty($pro_famille)){
		foreach($pro_famille as $pf){
			$result[$pf["fac_id"]]=array(
				"fac_libelle" => $pf["fac_libelle"],
				"fac_pourcentage" => $pf["fac_pourcentage"],
				"fac_societe" => $pf["fac_societe"]
			);
		}
	}

    return $result;
}
