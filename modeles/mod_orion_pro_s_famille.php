<?php

// RETOURNE UNE LIGNE LA TABLE Produits_Sous_Familles qui se trouve dans ORION

// AUTEUR FG

function orion_pro_s_famille($psf_id){

	global $Conn;
	
	$sql="SELECT psf_libelle,psf_couleur FROM Produits_Sous_Familles WHERE psf_id=:psf_id";
	$req=$Conn->prepare($sql);
	$req->bindParam(":psf_id",$psf_id);
	$req->execute();
	$pro_s_famille = $req->fetch();
	if(!empty($pro_s_famille)){
		 return $pro_s_famille;
	}

   
}
