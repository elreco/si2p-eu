<?php


setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


function devis_data($devis_id){

	$data=array();
	
	global $acc_societe;
	global $acc_agence;
	global $acc_utilisateur;
	global $ConnSoc;
	global $Conn;

	$sql="SELECT * FROM Devis LEFT OUTER JOIN Clients ON (Devis.dev_client=Clients.cli_id AND Devis.dev_agence=Clients.cli_agence) WHERE dev_id=" . $devis_id;
	if($acc_agence>0){
		$sql.=" AND dev_agence=" . $acc_agence;
	}
	if(!$_SESSION["acces"]["acc_droits"][6]){	
		$sql.=" AND (dev_utilisateur=" . $acc_utilisateur . " OR cli_utilisateur=" . $acc_utilisateur . ")";
	}
	$req=$ConnSoc->query($sql);
	$data=$req->fetch();
	if(!empty($data)){
		
		
		
		$data["reg_delai"]="";
		switch ($data["dev_reg_formule"]) {
			case 1:
				if($data["dev_reg_nb_jour"]==0){
					$data["reg_delai"].="Comptant";
				}else{
					$data["reg_delai"].=$data["dev_reg_nb_jour"] . " jours";
				}
				break;
			case 2:
				$data["reg_delai"].="45 jours fin de mois";
				break;
			case 3:
				$data["reg_delai"].="fin de mois + 45 jours";
				break;
			case 4:
				if($data["dev_reg_nb_jour"]>0){
					$data["reg_delai"].=$data["dev_reg_nb_jour"] . " jours ";
				}
				if($data["dev_reg_fdm"]>0){
					$data["reg_delai"].="fin de mois le " . $data["dev_reg_fdm"];
				}
				break;
		}
		
		$dateTime_dev_date=date_create_from_format('Y-m-d',$data["dev_date"]);
		$data["dev_date_texte"]=utf8_encode(strftime("%A %#d %B %Y",$dateTime_dev_date->getTimestamp()));

		// Si lié à un utilisateur 
		if(!empty($data["dev_utilisateur"])){
			
			$sql="SELECT uti_tel,uti_mobile,uti_fax,uti_mail FROM Utilisateurs WHERE uti_id=" . $data["dev_utilisateur"] . ";";
			$req=$Conn->query($sql);
			$utilisateur=$req->fetch();
			if(!empty($utilisateur)){
				$data["dev_uti_tel"]=$utilisateur["uti_tel"];
				$data["dev_uti_mobile"]=$utilisateur["uti_mobile"];
				$data["dev_uti_fax"]=$utilisateur["uti_fax"];
				$data["dev_uti_mail"]=$utilisateur["uti_mail"];				
			}
		}
		
		// LE CLIENT
		
		$req=$Conn->query("SELECT cli_code,cli_reference FROM clients WHERE cli_id=" . $data["dev_client"] . ";");
		$d_client=$req->fetch();
		if(!empty($d_client)){
			$data["dev_cli_code"]=$d_client["cli_code"];
			$data["dev_cli_reference"]=$d_client["cli_reference"];
		}

		// TVA
		// on est obligé de connaitre tous les taux de tva au cas ou on modifie une ligne
		$data["tva"]=array();
		$req=$Conn->query("SELECT DISTINCT tpe_taux FROM Tva_periodes ORDER BY tpe_taux;");
		$d_tva_taux=$req->fetchAll();
		if(!empty($d_tva_taux)){
			foreach($d_tva_taux as $tt){
				$data["tva"][$tt["tpe_taux"]]=0;
			}
			
		}
	
		// ADRESSE RESEAU
		$data["juridique"]=get_juridique($acc_societe,$data["dev_agence"]);
		
		// LES LIGNES DE DEVIS

		$sql="SELECT * FROM Devis_Lignes WHERE dli_devis=" . $devis_id . ";";
		$req=$ConnSoc->query($sql);
		$data["lignes"]=$req->fetchAll();
	
	}

	if(empty($erreur_txt)){
		return $data;
	}else{
		return $erreur_txt;
	}
}
?>