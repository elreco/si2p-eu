<?php


function get_suspect_contacts($suspect){
		
	global $ConnSoc;	
	
	$req_sql="SELECT * FROM suspects_contacts WHERE sco_ref_id = :suspect";
	$req_sql.=" ORDER BY sco_nom,sco_prenom;";
	$req=$ConnSoc->prepare($req_sql);
	$req->bindParam(":suspect",$suspect);
	$req->execute();
	
	$suspect_contacts = $req->fetchAll();
	return $suspect_contacts;
}

?>