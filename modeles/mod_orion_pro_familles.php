<?php

// RETOURNE LA TABLE Clients_Infos qui se trouve dans ORION
// la clé correspond à l'ID da la table


// AUTEUR FG

function orion_produits_familles(){

	global $Conn;
	
	/*$result[0]=array(
		"pfa_libelle" => "",
		"pfa_couleur" => ""
	);*/
	
	$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles ORDER BY pfa_id";
	$req=$Conn->prepare($sql);
	$req->execute();
	$pro_famille = $req->fetchAll();
	if(!empty($pro_famille)){
		foreach($pro_famille as $pf){
			$result[$pf["pfa_id"]]=array(
				"pfa_libelle" => $pf["pfa_libelle"]
			);
		}
	}

    return $result;
}
