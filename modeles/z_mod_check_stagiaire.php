<?php

/*
	VERIF SI UN STAGAIRE EXISTE DEJA
- transfert suspect stagiaire

- retourne 0 si libre
- id sta si on connait nom prenom naiss et qu'il n'y a qu'un seul resultat
- retourne false si existe sans etre sur que c'est la même personne

*/
function check_stagiaire($stagiaire,$nom,$prenom,$naissance,$mail){

	global $Conn;

	if(empty($nom) OR empty($prenom)){
		return false;
	}
	
	$date_naissance_sql=null;
	if(!empty($naissance)){
		$date_naissance=date_create_from_format('j/m/Y',$naissance);
		if(!is_bool($date_naissance)){
			$date_naissance_sql=$date_naissance->format("Y-m-d");	
		}
	}

	$nom=strtoupper($nom);
	$prenom=ucfirst($prenom);
		
	$sql="SELECT sta_id FROM Stagiaires WHERE sta_nom=:nom AND sta_prenom=:prenom";
	if(!empty($stagiaire)){
		$sql.=" AND NOT sta_id=:stagiaire";	
	}
	if(!empty($mail_perso)){
		$sql.=" AND (sta_mail_perso=:mail_perso OR ISNULL(sta_mail_perso) )";
	}
	if(!empty($date_naissance_sql)){
		$sql.=" AND (sta_naissance=:naissance OR ISNULL(sta_naissance) )";
	}
	$sql.=" ORDER BY sta_id";
	$req=$Conn->prepare($sql);
	$req->bindParam(":nom",$nom);
	$req->bindParam(":prenom",$prenom);
	if(!empty($stagiaire)){
		$req->bindParam(":stagiaire",$stagiaire);
	}
	if(!empty($mail_perso)){
		$req->bindParam(":mail_perso",$mail_perso);
	}
	if(!empty($date_naissance_sql)){
		$req->bindParam(":naissance",$date_naissance_sql);
	}
	$req->execute();
	$doublons=$req->fetchAll();
	if(!empty($doublons)){
		if(!empty($date_naissance_sql) AND count($doublons)==1){
			return $doublons["sta_id"];
		}else{
			return false;
		}
	}else{
		return 0;
	}
}
?>
