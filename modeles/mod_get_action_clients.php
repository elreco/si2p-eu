<?php

/* RETOURNES LES CLIENTS QUI PARTICIPENT A UNE ACTION

option
date >0 retourne seulement les clients qui participent a une date précise
session >0 retourne seulement les clients qui participent a session date précises

AUTEUR FG
*/
function get_action_clients($action,$date,$session){

	global $ConnSoc;
	

	$sql="SELECT DISTINCT acl_id,cli_code,cli_nom,acl_pro_reference";
	$sql.=" FROM Clients,Actions_Clients,Actions_Clients_Dates,Actions_Sessions";
	$sql.=" WHERE cli_id=acl_client AND acl_id=acd_action_client AND acd_date=ase_date";
	$sql.=" AND acl_action=:action";
	if($date>0){
		$sql.=" AND acd_date=:date";
	}
	if($session>0){
		$sql.=" AND ase_id=:session";
	}
	$sql.=" ORDER BY cli_code,cli_nom,acl_id;";
	$req = $ConnSoc->prepare($sql);
	$req->bindParam(":action",$action);
	$req->bindParam(":action",$action);
	if($date>0){
		$req->bindParam(":date",$date);
	}
	if($session>0){
		$req->bindParam(":session",$session);
	}
	$req->execute();
	$clients = $req->fetchAll();
	
    return $clients;
}
