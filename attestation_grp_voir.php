<?php 

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_get_juridique.php');

include('modeles/mod_date_periode_lib.php');

include('modeles/mod_action_attestation_data.php');


// VISUALISATION D'UNE CONVENTION

$erreur_txt="";

$action_id=0;
if(!empty($_GET["action"])){
	$action_id=intval($_GET["action"]);
}
$action_client_id=0;
if(!empty($_GET["action_client"])){
	$action_client_id=intval($_GET["action_client"]);
}
$modele=1;
if(!empty($_GET["modele"])){
	$modele=intval($_GET["modele"]);
}

if(empty($action_id) OR empty($action_client_id)){
	$erreur_txt="impossible d'afficher la page";
}else{
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}
	if($acc_societe!=$conn_soc_id){
		$acc_societe=$conn_soc_id;
	}
	
	// MODELE DATA CONCU POUR UN STAGIAIRE
	$data=action_attestation_data($action_id,$action_client_id,$modele);
	/*echo("<pre>");
		print_r($data);
	echo("</pre>");
	die();*/
}
 ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">
		

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#container_print{
				width:21cm;
			}
			.stagiaire{
				list-style:none;
				text-align:center;
				font-size:12pt;
				color:#e31936;
			}
			.head section{
				padding-top:50px!important;
			}
			.foot section{
				padding-bottom:50px;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm">
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">	
		<?php 		if(empty($erreur_txt)){ ?>
		
							<div class="row" >
							
								<!-- zone d'affichage du devis -->
								
								<div class="col-md-8"  >	
								
									<div id="container_print" >
								
										<div id="page_print" >
										
								<?php		foreach($data["pages"] as $p => $page){
												
												if(!empty($page["stagiaires"])){
												
										
													/*echo("<pre>");
														print_r($page);
													echo("</pre>"); */ ?>
										
													<div class="page" id="header_<?=$p?>" data-page="<?=$p?>" >
														<header>
															<div>
																<img src="../<?=$data["juridique"]["logo_1"]?>" />
															</div>	
															<table>
																<tr>
																	<td class="w-50" >
															<?php		echo("<b>" . $data["juridique"]["nom"] . "</b>");
																		if(!empty($data["juridique"]["agence"])){
																			echo("<br/><b>" . $data["juridique"]["agence"] . "</b>");
																		};
																		if(!empty($data["juridique"]["ad1"])){
																			echo("<br/>" . $data["juridique"]["ad1"]);
																		};
																		if(!empty($data["juridique"]["ad2"])){
																			echo("<br/>" . $data["juridique"]["ad2"]);
																		};
																		if(!empty($data["juridique"]["ad3"])){
																			echo("<br/>" . $data["juridique"]["ad3"]);
																		};
																		echo("<br/>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"]); 
																		
																		if(!empty($data["juridique"]["tel"])){
																			echo("<br/>Tél : " . $data["juridique"]["tel"]);
																		};																		
																		if(!empty($data["juridique"]["fax"])){
																			echo("<br/>Fax : " . $data["juridique"]["fax"]);
																		};
																		if(!empty($data["juridique"]["site"])){
																			echo("<br/>" . $data["juridique"]["site"]);
																		};
																		
																		?>
																	</td>
																	
																	<td class="w-50 pt50" >
															<?php		echo($data["adresse_client"]); ?>
																	</td>																
																</tr>													
															</table>
															<h1 class="text-center mb25" >ATTESTATION DE FORMATION</h1>														
														</header>
													</div>
													
													<div id="head_<?=$p?>" class="head" >
															<p>
																Je soussigné,
														<?php	if(!empty($data["juridique"]["resp_ident"])){
																	echo(" " . $data["juridique"]["resp_ident"] . ",");
																}
																if(!empty($data["juridique"]["resp_fonction"])){
																	echo(" " . $data["juridique"]["resp_fonction"] . ",");
																} ?> atteste que 
															</p>
														
													</div>
													
													<div class="ligne-body-<?=$p?>" >														
															<table>
																<tr>
												<?php				$nbBcl=ceil(count($page["stagiaires"])/8);
																	if($nbBcl==1){
																		$class="w-100";
																	}elseif($nbBcl==2){
																		$class="w-50";
																	}else{
																		$class="w-33";
																	} 
																	
																	$nb_sta_col=intval(count($page["stagiaires"])/$nbBcl);
																	if( count($page["stagiaires"]) % $nbBcl>0){
																		$nb_sta_col++;
																	}
																	$num_sta=0;
																	for($bcl=1;$bcl<=$nbBcl;$bcl++){ 
																		$fin_sta=$nb_sta_col*$bcl; ?>																
																		<td class="<?=$class?>" >
										<?php								for($num_sta;$num_sta<$fin_sta;$num_sta++){																			
																				echo("<div class='stagiaire' >" . $page["stagiaires"][$num_sta]["sta_nom"] . " " . $page["stagiaires"][$num_sta]["sta_prenom"] . "</div>");
																				if($num_sta>=count($page["stagiaires"])-1){
																					break;
																				}
																			} ?>
																		</td>
										<?php						}	?>
																</tr>
															</table>
														
													</div>

													<div class="ligne-body-<?=$p?>" >
														<section>ont suivi la formation suivante :</section>
													</div>
													
													
													<div class="ligne-body-<?=$p?>" >
														<table class="w-100" >
															<tr>
																<td class="w-33 text-right pt5" ><b>Intitulé</b> :</td>
																<td class="w-67 text-strong pt5" ><?=$data["acl_pro_libelle"]?></td>
															</tr>
															<tr>
																<td class="w-33 text-right pt5"><b>Date</b> :</td>
																<td class="w-67 text-strong pt5" >
														<?php		if(!empty($page["dates"])){
																		echo(date_periode_lib($page["dates"],"%A %d %B %Y"));
																	} ?>										
																</td>
															</tr>
															<tr>
																<td class="w-33 text-right pt5" ><b>Durée</b> :</td>
																<td class="w-67 text-strong pt5" >
														<?php		echo($page["duree"]); ?>																
																</td>
															</tr>
															<tr>
																<td class="w-33 text-right pt5" ><b>Formateur</b> :</td>
																<td class="w-67 text-strong pt5" >
														<?php		if(!empty($page["formateurs"])){
																		$liste_form=implode($page["formateurs"],",");
																		echo($liste_form);
																	} ?>
																</td>
															</tr>
												<?php		if(!empty($page["testeurs"])){	 ?>
																<tr>
																	<td class="w-33 text-right pt5" ><b>Testeurs</b> :</td>
																	<td class="w-67 text-strong pt5" >
														<?php			$liste_form=implode($page["testeurs"],",");
																		echo($liste_form); ?>
																	</td>
																</tr>
												<?php		} ?>
															<tr>
																<td class="w-33 text-right pt5" ><b>Lieu</b> :</td>
																<td class="w-67 text-strong pt5" ><?=$data["act_adr_ville"] . " (" . substr ($data["act_adr_cp"],0,2) . ")"?></td>
															</tr>
														</table>
														
															<p>Fait pour servir et valoir ce que de droit.</p>
														
													</div>
													
													<div id="foot_<?=$p?>" class="foot" >														
															<p>Fait à <?=$data["juridique"]["ville"]?>, le <?=utf8_encode($data["date_doc"])?></p>
															
															<table>
																<tr>
																	<td class="w-60" >&nbsp;</td>
																	<td>
															<?php		if(!empty($data["juridique"]["resp_signature"])){ 
																			echo("<br/>"); ?>
																			<img src="../<?=$data["juridique"]["resp_signature"]?>" class="img-responsive" />
																<?php	}?>
																	</td>
																</tr>
															</table>
														
													</div>
													
													<div id="footer_<?=$p?>" >
														<footer class="text-center" >
															<?=$data["juridique"]["footer"]?>
														</footer>
													</div>
									<?php		} 
											} ?>
										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->
								
								<div class="col-md-4" >
									<!-- DEBUT ANNEXE -->
									
									<!-- FIN ANNEXE -->
								</div>
								
							</div>
						

		<?php		}else{ ?>
						
						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>
					
		<?php		} ?>
									
				</section>
				
				<!-- End: Content -->
			</section>
			
			
		</div>
	
		<!-- End: Main -->
		
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >		
					<a href="action_voir.php?action=<?=$action_id?>&onglet=3&societ=<?=$acc_societe?>" class="btn btn-default btn-sm" >
						Retour
					</a>					
				</div>
				<div class="col-xs-6 footer-middle text-center" >
					<button type="button" class="btn btn-sm btn-info ml15" id="print" >
						<i class="fa fa-print"></i> Imprimer
					</button>
				</div>
				<div class="col-xs-3 footer-right" ></div>
			</div>
		</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript" src="assets/js/print.js"></script>
		<script type="text/javascript">
			var l_page=21;
			var h_page=28;
		</script>

	</body>
</html>
