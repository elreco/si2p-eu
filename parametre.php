<?php
include "includes/controle_acces.inc.php";
include "includes/connexion.php";

// GESTION DU MENU
$menu_actif="99-99";

$_SESSION["retour"] = "parametre.php";

unset($_SESSION['tri']);

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - Paramètres</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS PAR DEFAUT -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<!-- PERSONALISATION DU THEME -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">


	<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" id="page_no_scroll" >

		<!-- Start: Main -->
		<div id="main">
			<?php
			include "includes/header_def.inc.php";
			?>

			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">
				<!-- Begin: Content -->

				<section id="content" class="animated fadeIn">

			<?php	$nbBloc=0; ?>
					<div class="row">

				<?php	$nbBloc++; ?>
						<div class="col-md-4">
							<div class="panel panel-primary">
								<div class="panel-heading panel-head-sm bbn pn">
									<span class="panel-title"><b>Divers</b></span>
								</div>
								<!--<div class="panel-intro" >
										Gérer les sociétés et les agences
									</div>-->
								<div class="panel-body pn">
									<table class="table table-hover">
										<tbody>
								<?php		if($_SESSION['acces']["acc_profil"]==13){ ?>
												<tr>
													<td><a href="param_case_liste.php">Planning - Cases "Autres"</a></td>
												</tr>
									<?php 	} ?>
											<tr>
												<td><a href="notification.php">Notifications</a></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>

			<?php		// TECH
						if($_SESSION['acces']["acc_service"][3]==1 OR $_SESSION['acces']["acc_profil"]==13){
							if($nbBloc==3){
								echo("</div>");
								echo("<div class='row'>");
								$nbBloc=0;
							}
							$nbBloc++; ?>
							<div class="col-md-4">
								<div class="panel panel-primary">
									<div class="panel-heading panel-head-sm bbn" >
										<span class="panel-title"><b>Technique</b></span>
									</div>
									<div class="panel-body pn">
										<table class="table table-hover">
											<tbody>
												<tr>
													<td><a href="diplome_liste.php">Diplômes</a></td>
												</tr>
												<tr>
													<td><a href="competence_liste.php">Compétences</a></td>
												</tr>
												<tr>
													<td><a href="qualification_liste.php">Qualifications</a></td>
												</tr>
												<tr>
													<td><a href="qualif_cat_liste.php">Catégories de qualification</a></td>
												</tr>
												<tr>
													<td><a href="attestation_liste.php">Attestations</a></td>
												</tr>
												<tr>
													<td><a href="param_avis_liste.php">Avis de stage</a></td>
												</tr>
												<tr>
													<td><a href="param_avis_question_liste.php">Avis de stage : base questions</a></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							
					<?php	if($nbBloc==3){
								echo("</div>");
								echo("<div class='row'>");
								$nbBloc=0;
							}
							$nbBloc++; ?>
							<div class="col-md-4">
								<div class="panel panel-primary">
									<div class="panel-heading panel-head-sm bbn" >
										<span class="panel-title"><b>Rapports d'évacuation</b></span>
									</div>
									<div class="panel-body pn">
										<table class="table table-hover">
											<tbody>
												<tr>
													<td><a href="evac_theme_liste.php">Thèmes</a></td>
												</tr>
												<tr>
													<td><a href="evac_chrono_liste.php">Chronologie</a></td>
												</tr>
												<tr>
													<td><a href="evac_remarque_liste.php">Remarques</a></td>
												</tr>
												<tr>
													<td><a href="evac_param_alerte.php">Paramètre d'alerte</a></td>
												</tr>
												<tr>
													<td><a href="evac_gestion_alerte.php">Gestion des alertes</a></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
				<?php	}

						// COMPTA
						if($_SESSION['acces']["acc_profil"]==13 OR $_SESSION['acces']["acc_service"][2]==1){  
							if($nbBloc==3){
								echo("</div>");
								echo("<div class='row'>");
								$nbBloc=0;
							}
							$nbBloc++; ?>							
							<div class="col-md-4">
								<div class="panel panel-primary">
									<div class="panel-heading panel-head-sm bbn" >
										<span class="panel-title"><b>Fournisseurs / Commandes</b></span>
									</div>
									<div class="panel-intro" >
										Gestion des fourniseurs
									</div>
									<div class="panel-body pn">
										<table class="table table-hover">
											<tbody>

												<tr>
													<td><a href="fournisseur_famille_liste.php">Familles de fournisseurs</a></td>
												</tr>
												<tr>
													<td><a href="validations_bc.php">Grilles de validations</a></td>
												</tr>
												<tr>
													<td><a href="fournisseur_document_liste.php">Documents des fournisseurs</a></td>
												</tr>
												<tr>
													<td><a href="param_marge_st_voir.php">Marges sous-traitance</a></td>
												</tr>
											</tbody>
										</table>

									</div>
								</div>
							</div>
				
				<?php		if($nbBloc==3){
								echo("</div>");
								echo("<div class='row'>");
								$nbBloc=0;
							}
							$nbBloc++; ?>
							<div class="col-md-4">
								<div class="panel panel-primary">
									<div class="panel-heading panel-head-sm bbn" >
										<span class="panel-title"><b>Compta</b></span>
									</div>
									<div class="panel-intro" >
										Gestions des données et paramètres comptables
									</div>
									<div class="panel-body pn">
										<table class="table table-hover">
											<tbody>
												<tr>
													<td><a href="societe_liste.php">Sociétés</a></td>
												</tr>
												<tr>
													<td><a href="agence_liste.php">Agences</a></td>
												</tr>
												<tr>
													<td><a href="rib_liste.php">RIB</a></td>
												</tr>
												<tr>
													<td><a href="compte_liste.php" >Comptes</a></td>
												</tr>

												<tr>
													<td><a href="param_tva_liste.php">TVA</a></td>
												</tr>

												<tr>
													<td><a href="param_produit_liste.php">Produits</a></td>
												</tr>

												<tr>
													<td><a href="param_transfert_sage.php">Transfert des factures vers SAGE</a></td>
												</tr>

												<tr>
													<td><a href="affacturage_liste.php">Remise affacturage</a></td>
												</tr>
												<tr>
													<td><a href="demandes_de_garanties.php">Demandes de garanties</a></td>
												</tr>
												<tr>
													<td><a href="bpf.php">Bilan pédagogique et financier</a></td>
												</tr>
											</tbody>
										</table>

									</div>
								</div>
							</div>
			<?php		}

						if(in_array($_SESSION['acces']["acc_profil"], [7,8,11,12,13,15,10])){ 

							// INFORMATIQUE						
							if($nbBloc==3){
								echo("</div>");
								echo("<div class='row'>");
								$nbBloc=0;
							}
							$nbBloc++; ?>
							<div class="col-md-4">
								<div class="panel panel-primary">
									<div class="panel-heading panel-head-sm bbn" >
										<span class="panel-title"><b>Notes de frais</b></span>
									</div>
									<div class="panel-intro" >
										Gestions des notes de frais
									</div>
									<div class="panel-body pn">
										<table class="table table-hover">
											<tbody>

												<tr>
													<td><a href="ndf_liste.php?retour_param=1">Liste des NDF</a></td>
												</tr>
												<tr>
													<td><a href="ndf_categories_liste.php">Liste des Catégories</a></td>
												</tr>
												<tr>
													<td><a href="ndf_details.php">Détails des frais</a></td>
												</tr>
												<tr>
													<td><a href="ndf_categories_profils.php">Catégories par profils</a></td>
												</tr>
												<tr>
													<td><a href="ndf_bareme_liste.php">Barèmes kilométriques</a></td>
												</tr>
												<tr>
													<td><a href="ndf_export_sage.php">Export SAGE</a></td>
												</tr>
												<tr>
													<td><a href="ndf_controle_depassements.php">Contrôle des dépassements</a></td>
												</tr>
												<tr>
													<td><a href="ndf_controle_invitations.php">Contrôle des invitations internes</a></td>
												</tr>
												<tr>
													<td><a href="ndf_controle_hebergements.php">Contrôle des hébergements</a></td>
												</tr>
											</tbody>
										</table>

									</div>
								</div>
							</div>
				<?php	}

						// DIR TECH
						if($_SESSION['acces']["acc_profil"]==13 OR $_SESSION['acces']["acc_profil"]==9){ 
							if($nbBloc==3){
								echo("</div>");
								echo("<div class='row'>");
								$nbBloc=0;
							}
							$nbBloc++; ?>
							<div class="col-md-4">
								<div class="panel panel-primary">
									<div class="panel-heading panel-head-sm bbn" >
										<span class="panel-title"><b>Informatique</b></span>
									</div>
									<div class="panel-intro" >
										Gérer Orion
									</div>
									<div class="panel-body pn">
										<table class="table table-hover">
											<tbody>
												<tr>
													<td><a href="droit_liste.php">Droits</a></td>
												</tr>
												<tr>
													<td><a href="droit_profil_voir.php">Droits par profil</a></td>
												</tr>
												<tr>
													<td><a href="modeles_fonctions_liste.php">Fonctions des modèles</a></td>
												</tr>
												<tr>
													<td><a href="bugs_liste.php">Liste des bugs</a></td>
												</tr>
												<tr>
													<td><a href="client_import.php">Import de filiales</a></td>
												</tr>
											</tbody>
										</table>

									</div>
								</div>
							</div>
				<?php	}

						// COMMERCE
						if($_SESSION['acces']["acc_service"][1]==1){ 
							if($nbBloc==3){
								echo("</div>");
								echo("<div class='row'>");
								$nbBloc=0;
							}
							$nbBloc++; ?>
							<!-- COMMERCE -->
							<div class="col-md-4">
								<div class="panel panel-primary">
									<div class="panel-heading panel-head-sm bbn" >
										<span class="panel-title"><b>Commerce</b></span>
									</div>
									<div class="panel-intro" >
										Paramètres liés au commerce.
									</div>
									<div class="panel-body pn">
										<table class="table table-hover">
											<tbody>
												<tr>
													<td><a href="classification_liste.php">Classifications</a></td>
												</tr>
												<tr>
													<td><a href="sous_classification_liste.php">Sous-classifications</a></td>
												</tr>
												<tr>
													<td><a href="client_info_liste.php">Infos Clients</a></td>
												</tr>
												<tr>
													<td><a href="param_com_secteur.php">Répartition géographique des vendeurs</a></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
				<?php	}
						
						// RH
						if($_SESSION['acces']["acc_service"][4]==1 OR $_SESSION['acces']["acc_profil"]==13){ 
							if($nbBloc==3){
								echo("</div>");
								echo("<div class='row'>");
								$nbBloc=0;
							}
							$nbBloc++; ?>
							<!-- RH -->
							<div class="col-md-4">
								<div class="panel panel-primary">
									<div class="panel-heading panel-head-sm bbn" >
										<span class="panel-title"><b>Ressources humaines</b></span>
									</div>
									<div class="panel-intro" >
										Paramètres liés à la ressources humaines.
									</div>
									<div class="panel-body pn">
										<table class="table table-hover">
											<tbody>										
												<tr>
													<td><a href="conges_integration.php">Intégration en paye</a></td>
												</tr>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						
				<?php	} 

						// MARKET
						if($_SESSION['acces']["acc_service"][5]==1){ 
							if($nbBloc==3){
								echo("</div>");
								echo("<div class='row'>");
								$nbBloc=0;
							}
							$nbBloc++; ?>
							<!-- MARKET -->
							<div class="col-md-4">
								<div class="panel panel-primary">
									<div class="panel-heading panel-head-sm bbn" >
										<span class="panel-title"><b>Marketing</b></span>
									</div>
									<div class="panel-intro" >
										Paramètres liés au marketing.
									</div>
									<div class="panel-body pn">
										<table class="table table-hover">
											<tbody>
												<tr>
													<td><a href="param_com_secteur.php">Répartition géographique des vendeurs</a></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
				<?php	} ?>

					</div>


				</section>
			</section>

			<!-- End: Main -->
			<footer id="content-footer" >
				<div class="row">
					<div class="col-xs-3 text-center" style="border-right: 1px solid #DDD; height:44px;padding:7px 15px;">
					</div>
					<div class="col-xs-6 footer-middle text-center" style=" ">
					</div>
					<div class="col-xs-3" style="border-left: 1px solid #DDD; height:44px;padding:12px 15px;">
					</div>
				</div>
			</footer>

		</div>
<?php	include "includes/footer_script.inc.php"; ?>
	</body>
</html>
