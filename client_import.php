<?php
$menu_actif = 1;
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");


// sur la personne connecte

$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"]; 
	}
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
  $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
  $acc_agence=$_SESSION['acces']["acc_agence"];  
}

if($_SESSION['acces']["acc_profil"]!=13){
	echo("Accès refusé!");
	die();
}


$req=$Conn->query("SELECT cli_id,cli_code,cli_nom FROM Clients WHERE cli_groupe AND cli_categorie=2 AND cli_niveau=0 ORDER BY cli_code,cli_nom;");
$d_clients=$req->fetchAll();


?>
<!DOCTYPE html> 
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8"> 
		<title>SI2P - Orion - Paramètres</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	   
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	  
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>


	<body class="sb-top sb-top-sm">
	
		<form action="client_import_result.php" enctype="multipart/form-data" method="POST" >
			<div id="main">
		<?php	include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" >
					<section id="content" class="" >
					
						<div class="admin-form theme-primary ">
							<div class="panel heading-border panel-primary">
								<div class="panel-body bg-light">
												
									<div class="content-header">
										<h2>Import <b class="text-primary">de filiales</b></h2>
									</div>
									
									<p><b>Important :</b> cet outil est configuré pour importer uniquement des filiales GC.</p>
									
									<div class="row">							
										<div class="col-md-4">
											<label for="agence" >Entité de rattachement :</label>
											<select class="select2" name="client" required="required" >											
												<option value="" >Entité de rattachement ...</option>
							<?php				if(!empty($d_clients)){
													foreach($d_clients as $d_client){
														echo("<option value='" . $d_client["cli_id"] . "' >" . $d_client["cli_code"] . " - " . $d_client["cli_nom"] . "</option>");
													}
												}	?>
											</select>
										</div>
										<div class="col-md-4">
											<label for="nom" >Suffixe code :</label>
											<input type="text" name="code" class="gui-input" id="code" placeholder="Nom" />	
											<small>Le nom sera utilisé si vide.</small>
										</div>
										<div class="col-md-4">
											<label for="nom" >Nom des entités à importer :</label>
											<input type="text" name="nom" class="gui-input" id="nom" placeholder="Nom" required />	
										</div>
									</div>
									<div class="row">
										
										
										<div class="col-md-4">
											<label for="agence" >Fichier .csv :</label>
											<label class="field prepend-icon file">
												<span class="button btn-primary">Choisir</span>
												<input type="file" class="gui-file" name="fichier" id="fichier" />
												<input type="text" class="gui-input" id="fichier_champ" placeholder="Importez votre document" />
												<label class="field-icon">
													<i class="fa fa-upload"></i>
												</label>
											</label>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-12">
											<div class="section-divider mb40">
												<span>Option</span>
											</div>
										</div>
									</div>

									<div class="row">	
										<div class="col-md-4">
											<div class="option-group field">
												<label class="option option-dark">
													<input type="checkbox" name="fil_intervention_mm" value="on">
													<span class="checkbox"></span>Dupliquer les filiales en intervention MM.
												</label>
											</div>
										</div>
									</div>

									
								</div>
							</div>
						</div>

					</section>				
				</section>		
			</div>
		

			<footer id="content-footer" class="affix">
				<div class="row">				
					<div class="col-xs-3 footer-left" >
						<a href="parametre.php" class="btn btn-default btn-sm" >
							<i class="fa fa-arrow-left" ></i> Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right" >
						  <button type="submit" name="submit" id="upload-btn" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						  </button>					 
					</div>
				</div>
			</footer>
		</form>

<?php 	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
			});		
		</script>
	</body>
</html>
