<?php

// SELECTION DES FACTURES

$menu_actif = "5-5";
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';

if(isset($_SESSION['cli_tri'])){ 
    unset($_SESSION['cli_tri']);
}
// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];    
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
    $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];  
}

$_SESSION["retourFacture"]="affacturage_accueil.php";
$_SESSION["retour"]="affacturage_accueil.php";

// securité

if($_SESSION['acces']['acc_service'][2]!=1){
	header("location : deconnect.php");
	die();
}

// LES AGENCES
if($acc_agence==0){
	$sql="SELECT age_id,age_code FROM Agences LEFT JOIN Societes ON (Agences.age_societe=Societes.soc_id) 
	WHERE soc_id=" . $acc_societe. " AND soc_agence ORDER BY age_code";
	$req = $Conn->query($sql);
	$d_agences=$req->fetchAll();
	if(empty($d_agences)){
		unset($d_agences);
	}
}
// LES REMISES

$sql="SELECT aff_id,DATE_FORMAT(aff_date,'%d/%m/%Y') AS aff_date_fr FROM Affacturages ORDER BY aff_id;";
$req = $ConnSoc->query($sql);
$d_remises=$req->fetchAll();
	


// LES CATEGORIES DE CLIENTS
$sql="SELECT cca_id,cca_libelle FROM Clients_Categories ORDER BY cca_libelle;";
$req = $Conn->query($sql);
$d_categories=$req->fetchAll();


?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="affacturage_fac_liste.php" id="formulaire" >			
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">											
											<div class="content-header">
												<h2>Module <b class="text-primary title-suscli">Affacturage</b></h2>
												
											</div>							
											<div class="col-md-10 col-md-offset-1">											
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Facture</span>
														</div>
													</div>
												</div>												
												<div class="row">
											<?php	if(isset($d_agences)){ ?>
														<div class="col-md-6">
															<div class="section">
																<label for="fac_agence" >Agence</label>
																<select name="fac_agence" id="fac_agence" class="select2" >
																	<option value="">Agence...</option>
														<?php 		if(!empty($d_agences)){
																		foreach($d_agences as $age){
																			echo("<option value='" . $age["age_id"] . "' >" . $age["age_code"] . "</option>");
																		}
																	} ?>
																</select>																
															</div>														
														</div>
											<?php	} ?>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="fac_date_deb" >Facturé entre le</label>
															<span  class="field prepend-icon">
																<input type="text" id="fac_date_deb" name="fac_date_deb" class="gui-input datepicker" placeholder="Facturé entre le" value="" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">															
															<label for="fac_date_fin" >et le</label>
															<span  class="field prepend-icon">
																<input type="text" id="fac_date_fin" name="fac_date_fin" class="gui-input datepicker" placeholder="Et le" value="" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>
												</div>
												<div class="row">	
													<div class="col-md-6">
														<div class="section">
															<label for="fac_aff_remise" >Remise</label>
															<select name="fac_aff_remise" id="fac_aff_remise" class="select2" >
																<option value="0">Remise...</option>
													<?php 		if(!empty($d_remises)){
																	foreach($d_remises as $rem){
																		echo("<option value='" . $rem["aff_id"] . "' >N° " . $rem["aff_id"] . " du " . $rem["aff_date_fr"] . "</option>");
																	}
																} ?>
															</select>																
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">
															<label for="fac_chrono" >Numéro de chrono</label>
															<input type="text" name="fac_chrono" id="fac_chrono" class="gui-input" placeholder="Numéro de chrono" />
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Client</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="fac_cli_categorie" >Catégorie</label>
															<select name="fac_cli_categorie" id="fac_cli_categorie" class="select2 select2-cli-cat" data-cli_s_cat="fac_cli_s_categorie" >
																<option value="0">Catégorie ...</option>
													<?php 		if(!empty($d_categories)){
																	foreach($d_categories as $cat){
																		echo("<option value='" . $cat["cca_id"] . "' >" . $cat["cca_libelle"] . "</option>");
																	}
																} ?>
															</select>
														</div>
													</div>
													<div class="col-md-6" id="bloc_fac_cli_s_categorie" style="display:none;" >
														<div class="section">
															<label for="fac_cli_s_categorie" >Sous-catégorie</label>
															<select name="fac_cli_s_categorie" id="fac_cli_s_categorie" class="select2" >
																<option value="0">Sous-catégorie ...</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="fac_client" >Client</label>
															<select name="fac_client" id="fac_client" class="select2-client-n" >
																<option value="0">Client...</option>
															</select>
														</div>
													</div>
													<div class="col-md-6 pt25 text-center">
														<div class="checkbox-custom">
															<input type="checkbox" name="fac_filiale" id="fac_filiale" value="on" />
															<label for="fac_filiale">et filiale</label>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Règlement</span>
														</div>
													</div>
												</div>				
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="fac_reg_deb" >Règlement entre le</label>
															<span  class="field prepend-icon">
																<input type="text" id="fac_reg_deb" name="fac_reg_deb" class="gui-input datepicker" placeholder="Règlement entre le" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">															
															<label for="fac_reg_fin" >et le</label>
															<span  class="field prepend-icon">
																<input type="text" id="fac_reg_fin" name="fac_reg_fin" class="gui-input datepicker" placeholder="Et le" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>												
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="fac_banque" >Banque :</label>
															<span class="field select">
																<select id="fac_banque" name="fac_banque" >
																	<option value="">Banque ...</option>
																	<option value="1">FACTOR</option>
																	<option value="2">Autre banque</option>
																</select>
																<i class="arrow simple"></i>
															</span>

														</div>
													</div>
													<div class="col-md-6">
														<div class="section">															
															<label for="fac_etat_date" >Etat au :</label>
															<span  class="field prepend-icon">
																<input type="text" id="fac_etat_date" name="fac_etat_date" class="gui-input datepicker" placeholder="Etat au" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>											
												</div>	
												
												<div class="row">												
													<div class="col-md-3 mt15" >															
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_recherche opt1" name="option_recherche" value="1">
																<span class="checkbox"></span>Remboursement à réaliser
															</label>
														</div>													
													</div>
													<div class="col-md-3 mt15" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_recherche opt2" name="option_recherche" value="2">
																<span class="checkbox"></span>Remboursement à percevoir
															</label>
														</div>	
													</div>
												</div>
											</div>																						
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left"></div>
					<div class="col-xs-6 footer-middle text-center">
						<a href="affacturage_liste.php" class="btn btn-info btn-sm">
							<i class='fa fa-eye'></i> Liste des remises
						</a>
					</div>
					<div class="col-xs-3 footer-right">			
						<button type="submit" name="search" class="btn btn-primary btn-sm">
							<i class='fa fa-search'></i> Rechercher
						</button>
					</div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function (){
				$(".option_recherche").click(function(){
					if($(this).prop("checked")){
						val=$(this).val();
						$(".option_recherche").each(function( index ){
							if($(this).val()!=val){
								$(this).prop("checked",false);
							}
						});
					}
				});
			});
		</script>
	</body>
</html>
