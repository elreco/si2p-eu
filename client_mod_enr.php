<?php

// MISE à JOUR D'UN client

include "includes/controle_acces.inc.php";
include "includes/connexion.php";
include "includes/connexion_soc.php";
include "includes/connexion_fct.php";
require("modeles/mod_check_siret.php");
require("modeles/mod_get_groupe_filiales.php");
require("modeles/mod_add_notification.php");
/*require("modeles/mod_get_commercial_uti.php");

require("modeles/mod_client.php");*/

$erreur="";
$warning_txt=""; // permet d'informer l'U que certaines valeurs n'ont pas été enregistrées sans bloquer toute la maj de la fiche


$client=0;
if(!empty($_POST["client"])){
	$client=intval($_POST["client"]);
}

if($client==0){
	$_SESSION['message'][] = array(
		"titre" => "Attention",
		"type" => "danger",
		"message" => "Paramètre absent"
	);
	Header("Location: client_mod.php?client=" . $client);
	die();
}


	// PERSONNE CONNECTE

	// l'utilisateur
	$acc_utilisateur=0;
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}

	// la société
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	$acc_agence = 0;
	if(!empty($_SESSION['acces']['acc_agence'])){
		$acc_agence = $_SESSION['acces']['acc_agence'];
	}

	// agence pour maj Clients Societes

	$tab_agence=array();

	$sql="SELECT age_id FROM Agences WHERE age_societe=" . $acc_societe . ";";
	$req=$Conn->query($sql);
	$result=$req->fetchAll();
	if(!empty($result)){
		foreach($result as $r){
			$tab_agence[]=array(
				"agence" => $r["age_id"],
				"commercial" => 0,
				"utilisateur" => 0,
				"archive" => 0,
				"update" => false
			);
		}
	}else{

		$tab_agence[]=array(
			"agence" => 0,
			"commercial" => 0,
			"utilisateur" => 0,
			"archive" => 0,
			"update" => 0
		);
	}

	// client avant modif

	$aff_restreint=false;
	$edit_categorie=true;
	$cli_facture=0;
	$sql="SELECT cli_code,cli_nom,cli_filiale_de,cli_niveau,cli_fil_de,cli_categorie,cli_sous_categorie,cli_groupe,cli_contrat,cca_gc,cli_blackliste,cli_stagiaire
	,cli_affacturable,cli_affacturable_txt,cli_affacturage,cli_affacturage_iban,cli_fac_groupe,cli_interco_soc,cli_interco_age,cli_blackliste,cli_opca,cli_facture_opca
	,cli_prescripteur,cli_prescripteur_info,cli_first_facture
	FROM Clients,Clients_Categories WHERE cli_categorie=cca_id AND cli_id=" . $client . ";";
	$req=$Conn->query($sql);
	$d_client=$req->fetch();
	if(!empty($d_client)){

		if($d_client["cli_categorie"]==2){
			// GC
			if(!$_SESSION['acces']["acc_droits"][8] AND $_SESSION['acces']["acc_service"][1]!=1){
				$aff_restreint=true;
				$edit_categorie=false;
			}elseif($_SESSION['acces']["acc_service"][1]!=1){
				$edit_categorie=false;
			}
		}elseif($d_client["cli_categorie"]==5){
			// NATIONAL
			if($_SESSION['acces']["acc_service"][1]!=1){
				$edit_categorie=false;
				$aff_restreint=true;
			}
		}
		if(!empty($d_client["cli_first_facture"])){
			$cli_facture=1;
		}
	}

	// adresse de facturation avant modif

	$adr_fac=0;

	$sql="SELECT adr_id,adr_type,adr_siret FROM Adresses WHERE adr_ref=1 AND adr_ref_id=" . $client . " AND adr_type=2 AND adr_defaut;";
	$req=$Conn->query($sql);
	$d_ad_fac_def=$req->fetch();
	if(!empty($d_ad_fac_def)){

		$adr_fac=$d_ad_fac_def["adr_id"];
	}

	// prescripteur avant modif
	if(!empty($d_client["cli_prescripteur"])){

		$sql="SELECT cpr_archive FROM Clients_Prescripteurs WHERE cpr_id=" . $d_client["cli_prescripteur"] . ";";
		$req=$Conn->query($sql);
		$d_prescripteur=$req->fetch();

	}

	/******************************************
		TRAITEMENT DU FORM
	*******************************************/

	$changement_groupe=false;

	if(!$aff_restreint){

		//echo("NOT RESTREINT<br/>");

		if(!empty($_POST["cli_code"])){
			$cli_code=$_POST["cli_code"];
		}else{
			$cli_code = "";
		}
		if(!empty($_POST["cli_nom"])){
			$cli_nom=$_POST["cli_nom"];
		}else{
			$cli_nom = "";
		}

		// CATEGORIE

		if(!$edit_categorie){
			$cli_categorie=$d_client["cli_categorie"];
		}else{
			$cli_categorie=0;
			if(!empty($_POST["cli_categorie"])){
				$cli_categorie=intval($_POST["cli_categorie"]);
			}else{
				$cli_categorie=1;
			}
		}

		$cli_sous_categorie=0;
		if(!empty($_POST["cli_sous_categorie"])){
			$cli_sous_categorie=intval($_POST["cli_sous_categorie"]);
		}

		$cli_contrat=0;
		if(!empty($_POST["cli_contrat"])){
			$cli_contrat=1;
		}

		/****************************
		// GROUPE
		/****************************/

		// Attention cli_niveau et cli_fil_de ne sont pas géré dans le form il doivent etre calculé

		$cli_niveau=0;
		$cli_fil_de=0;
		$cli_filiale_de=0;
		$cli_groupe=0;

		if(!empty($_POST["cli_groupe"])){
			$cli_groupe=1;
		}else{
			// le fait q'il n'y ai pas de filiale doit avoir été verifie plus haut
			$cli_groupe=0;
		}
		if($cli_groupe==1){
			if(!empty($_POST["cli_filiale_de"])){
				$cli_filiale_de=intval($_POST["cli_filiale_de"]);
			}else{
				$cli_filiale_de=0;
			}
		}

		if($cli_groupe==0){

			// c'est une entreprise seule
			$cli_filiale_de=0;
			$cli_fac_groupe=0;

		}elseif($cli_filiale_de>0){

			if($d_client["cli_filiale_de"]!=$cli_filiale_de){

				// changement de groupe

				// 1 - $d_client["cli_filiale_de"] = 0 | $cli_filiale_de = X -> ancien mm qui deveint filiale de X
				// 3 - $d_client["cli_filiale_de"] = Y | $cli_filiale_de = X -> ancienne filiale de Y qui devient  filiale de X

				// en cas de changement de groupe (cas 1,3), la fiche est obligatoirement inserée comme filiale de niveau 1

				$cli_fil_de=$cli_filiale_de;

				if($d_client["cli_fac_groupe"]!=$client){
					// si $d_client["cli_fac_groupe"]==$client -> la fiche portait de la facturation groupée -> elle peut continuer à la faire pour elle-meme et ces filiales
					// sinon il faut réinitialiser le factureur
					$cli_fac_groupe=0;
				}

				$changement_groupe=true;

			}else{
				// reste associé à la même MM -> on conserve les données associées avant MAJ
				if(!empty($d_client["cli_niveau"])){
					$cli_niveau=$d_client["cli_niveau"];
				}
				if(!empty($d_client["cli_fil_de"])){
					$cli_fil_de=$d_client["cli_fil_de"];
				}
			}
		}else{

			// sinon c'est la MM donc fil_de et niveau valent 0
			// $d_client["cli_filiale_de"] = X | $cli_filiale_de = 0 -> ancienne filiale de X qui devient  MM
			if(!empty($d_client["cli_filiale_de"])){
				if($d_client["cli_fac_groupe"]!=$client){
					// si $d_client["cli_fac_groupe"]==$client -> la fiche portait de la facturation groupée -> elle peut continuer à la faire en tant que MM
					// sinon il faut réinitialiser le factureur
					$cli_fac_groupe=0;
				}

			}
		}


	}else{
		$cli_code=$d_client["cli_code"];
		$cli_nom=$d_client["cli_nom"];
		$cli_categorie=$d_client["cli_categorie"];
		$cli_sous_categorie=$d_client["cli_sous_categorie"];
		$cli_contrat=$d_client["cli_contrat"];
		$cli_groupe=$d_client["cli_groupe"];
		$cli_filiale_de=$d_client["cli_filiale_de"];
		$cli_niveau=$d_client["cli_niveau"];
		$cli_fil_de=$d_client["cli_fil_de"];
	}

	// la facturation groupee n'est disponible que pour les CN et les accords mixtes
	if($cli_sous_categorie!=1 AND $cli_sous_categorie!=3){
		$cli_fac_groupe=0;
	}


	// LE OU LES COMMERCIAUX

	if(!empty($tab_agence)){
		foreach($tab_agence as $num_age => $tab_age){

			if(!empty($_POST["cso_agence_" . $tab_age["agence"]]) OR $tab_age["agence"]==0){

				$cso_commercial=0;
				$cso_utilisateur=0;
				if(!empty($_POST["cso_commercial_" . $tab_age["agence"]])){
					$cso_commercial=intval($_POST["cso_commercial_" . $tab_age["agence"]]);
				}
				if($cso_commercial>0){
					$sql="SELECT com_ref_1 FROM Commerciaux WHERE com_id=" . $cso_commercial;
					$req=$ConnSoc->query($sql);
					$d_utilisateur=$req->fetch();
					if(!empty($d_utilisateur)){
						if(!empty($d_utilisateur["com_ref_1"])){
							$cso_utilisateur=$d_utilisateur["com_ref_1"];
						}
					}
				}

				$cso_archive=0;
				if(!empty($_POST["cso_archive_" . $tab_age["agence"]])){
					$cso_archive=1;
				}

				$tab_agence[$num_age]["commercial"]=$cso_commercial;
				$tab_agence[$num_age]["utilisateur"]=$cso_utilisateur;
				$tab_agence[$num_age]["archive"]=$cso_archive;
				$tab_agence[$num_age]["update"]=1;

			}
		}
	}




	// SIREN / SIRET
	$cli_siren="";
	$adr_siret="";
	if(!empty($_POST["cli_siren"])){
		if(strlen($_POST["cli_siren"])==11){
			$cli_siren=$_POST["cli_siren"];
			if(!empty($_POST["adr_siret"])){
				if(strlen($_POST["adr_siret"])==6){
					$adr_siret=$_POST["adr_siret"];
				}
			}
		}
	}
	$check_siret=true;
	if(!empty($_SESSION['acces']['acc_droits'][36])){
		if(!empty($_POST["siret_no_check"])){
			$check_siret=false;
		}
	}


	$cli_facture_opca=0;
	$cli_opca=0;
	$cli_opca_type=0;
	if($cli_categorie!=4){
		if(!empty($_POST["cli_facture_opca"])){
			$cli_facture_opca=1;
			if(!empty($_POST["cli_opca_type"])){
				$cli_opca_type=intval($_POST["cli_opca_type"]);
			}
			if($cli_opca_type>0){
				if(!empty($_POST["cli_opca"])){
					$cli_opca=intval($_POST["cli_opca"]);
				}
			}
		}
	}


	$cli_releve=0;
	if(!empty($_POST["cli_releve"]) && $cli_categorie!=3){
		$cli_releve=1;
	}

	// MODE DE REG

	$cli_reg_type=0;
	if(!empty($_POST["cli_reg_type"])){
		$cli_reg_type=intval($_POST["cli_reg_type"]);
	}
	$cli_reg_formule=1;
	$cli_reg_nb_jour=0;
	$cli_reg_fdm=0;
	if(!empty($_POST["reg_formule"])){
		$cli_reg_formule=intval($_POST["reg_formule"]);
	}
	if(!empty($_POST["reg_nb_jour_" . $cli_reg_formule])){
		$cli_reg_nb_jour=intval($_POST["reg_nb_jour_" . $cli_reg_formule]);
	}
	if(!empty($_POST["reg_fdm_" . $cli_reg_formule])){
		$cli_reg_fdm=intval($_POST["reg_fdm_" . $cli_reg_formule]);
	}

	// DONNEE FACTURAION RESERVE A LA COMPTE

	if($_SESSION['acces']["acc_droits"][9]){

		// affacturage
		$cli_affacturable=1;
		$cli_affacturable_txt="";
		$cli_affacturage=0;

		if(!empty($_POST["cli_non_affacturable"])){
			$cli_affacturable=2;
			$cli_affacturable_txt=$_POST["cli_affacturable_txt"];
		}elseif(!empty($_POST["cli_affacturage"])){
			$cli_affacturage=1;
		}

		$cli_blackliste=0;
		if(!empty($_POST["cli_blackliste"])){
			$cli_blackliste=1;
		}

		$cli_interco_soc=0;
		if(!empty($_POST["cli_interco_soc"])){
			$cli_interco_soc=intval($_POST["cli_interco_soc"]);
		}

		$cli_interco_age=0;
		if(!empty($_POST["cli_interco_age"])){
			$cli_interco_age=intval($_POST["cli_interco_age"]);
		}


		$cso_rib=0;
		if(!empty($_POST["cso_rib"])){
			$cso_rib=intval($_POST["cso_rib"]);
		}
		$cso_rib_change=0;
		if(!empty($_POST["cso_rib_change"])){
			$cso_rib_change=1;
		}

	}else{


		$cli_affacturable=$d_client["cli_affacturable"];
		$cli_affacturable_txt=$d_client["cli_affacturable_txt"];
		$cli_affacturage=$d_client["cli_affacturage"];

		$cli_blackliste=$d_client["cli_blackliste"];
		$cli_interco_soc=$d_client["cli_interco_soc"];
		$cli_interco_age=$d_client["cli_interco_age"];
		$cso_rib=0;	// si cso rib -> pas de maj du client
		$cso_rib_change=0;
	}

	// FG arret aff au 01/04/2020
	// on conserve cette instruction pour ne pas écraser l'info.
	//if($acc_societe==17){
	//	$cli_affacturage_iban=$_POST["cli_affacturage_iban"];
	//}else{
		$cli_affacturage_iban=$d_client["cli_affacturage_iban"];
	//}

	// peut importe l'accès -> OPCA PARTICULIER ET CLIENT SANS SIREN NE SONT PAS AFFACTURABLE
	if(empty($cli_siren) OR $cli_categorie==3 OR $cli_sous_categorie==5 OR $cli_sous_categorie==6 OR $cli_interco_soc>0){
		$cli_affacturable=0;
		$cli_affacturable_txt="";
		$cli_affacturage=0;
	}

	// FIN DONNEE RESERVE A LA COMPTA


	// AUTRES INFOS

	$cli_classification=0;
	if(!empty($_POST["cli_classification"])){
		$cli_classification=intval($_POST["cli_classification"]);
	}

	$cli_classification_type=0;
	if(!empty($_POST["cli_classification_type"])){
		$cli_classification_type=intval($_POST["cli_classification_type"]);
	}
	$cli_classification_categorie=0;
	if(!empty($_POST["cli_classification_categorie"])){
		$cli_classification_categorie=intval($_POST["cli_classification_categorie"]);
	}

	$cli_stagiaire_type=0;
	if(!empty($_POST["cli_stagiaire_type"])){
		$cli_stagiaire_type=intval($_POST["cli_stagiaire_type"]);
	}


	if(isset($_POST["cli_prescripteur"])){

		$cli_prescripteur=0;
		$cli_prescripteur_info="";

		if(!empty($_POST["cli_prescripteur"])){
			$cli_prescripteur=intval($_POST["cli_prescripteur"]);
			if(!empty($cli_prescripteur)){
				if(!empty($_POST["cli_prescripteur_info"])){
					$cli_prescripteur_info=$_POST["cli_prescripteur_info"];
				}
			}
		}elseif(isset($d_prescripteur)){
			// si donnee sur le prescripteur avant modif
			if($d_prescripteur["cpr_archive"]==1){
				// le prescripteur est archive, donc non selectionnable -> il faut conserver l'ancienne valeur.
				$cli_prescripteur=$d_client["cli_prescripteur"];
				$cli_prescripteur_info="";
			}
		}
	}else{
		$cli_prescripteur=$d_client["cli_prescripteur"];
		$cli_prescripteur_info=$d_client["cli_prescripteur_info"];
	}

	$cli_ape=0;
	if(!empty($_POST["cli_ape"])){
		$cli_ape=intval($_POST["cli_ape"]);
	}

	$cli_capital="";
	$cli_soc_type="";
	$cli_immat_lieu="";
	$cli_tel="";
	if($cli_categorie!=3){
		if(!empty($_POST["cli_capital"])){
			$cli_capital=$_POST["cli_capital"];
		}
		if(!empty($_POST["cli_soc_type"])){
			$cli_soc_type=$_POST["cli_soc_type"];
		}
		if(!empty($_POST["cli_immat_lieu"])){
			$cli_immat_lieu=$_POST["cli_immat_lieu"];
		}
		if(!empty($_POST["cli_tel"])){
			$cli_tel=$_POST["cli_tel"];
		}
	}

	$cli_ape=0;
	if(!empty($_POST["cli_ape"])){
		$cli_ape=intval($_POST["cli_ape"]);
	}

	$cli_ident_tva=$_POST["cli_ident_tva"];

	$cli_reference=$_POST["cli_reference"];

	$cli_important=0;
	if(!empty($_POST["cli_important"])){
		$cli_important=1;
	}

	// adresse intervention

	$adr_int=0;
	if(!empty($_POST["adr_int"])){
		$adr_int=intval($_POST["adr_int"]);
	}

	$adr_nom_int=$_POST["adr_nom1"];
	$adr_service_int=$_POST["adr_service1"];
	$adr_ad1_int=$_POST["adr_ad11"];
	$adr_ad2_int=$_POST["adr_ad21"];
	$adr_ad3_int=$_POST["adr_ad31"];
	$adr_cp_int=$_POST["adr_cp1"];
	$adr_ville_int=$_POST["adr_ville1"];
	$adr_geo_int=1;
	if(!empty($_POST["adr_geo1"])){
		$adr_geo_int=intval($_POST["adr_geo1"]);
	}

	// adresse de facturation

	$adr_geo_fac=1;
	if(!empty($_POST["adr_geo2"])){
		$adr_geo_fac=intval($_POST["adr_geo2"]);
	}

	// contact

	$contact=0;
	if(!empty($_POST["contact"])){
		$contact=intval($_POST["contact"]);
	}

	$con_nom="";
	$con_fonction=0;
	$con_fonction_nom="";
	$con_titre=0;
	$con_prenom=0;
	$con_tel="";
	$con_fax="";
	$con_portable="";
	$con_mail="";
	$con_compta=0;
	$sta_naissance=null;
	$sta_ville_naiss=null;
	$sta_cp_naiss=null;

	if(!empty($_POST["con_nom"])){

		$con_nom=$_POST["con_nom"];

		if(!empty($_POST["con_fonction"])){
			$con_fonction=intval($_POST["con_fonction"]);
			if(!empty($_POST["con_fonction_nom"]) AND $con_fonction==0){
				$con_fonction_nom=$_POST["con_fonction_nom"];
			}
		}

		if(!empty($_POST["con_titre"])){
			$con_titre=intval($_POST["con_titre"]);
		}
		$con_prenom=$_POST["con_prenom"];
		$con_tel=$_POST["con_tel"];
		$con_fax=$_POST["con_fax"];
		$con_portable=$_POST["con_portable"];
		$con_mail=$_POST["con_mail"];

		if(!empty($_POST["con_compta"])){
			$con_compta=1;
		}

		if($cli_categorie==3){
			if(!empty($_POST["sta_naissance"])){
				$stagiaire_naiss=date_create_from_format('d/m/Y',$_POST["sta_naissance"]);
				if(!is_bool($stagiaire_naiss)){
					$sta_naissance=$stagiaire_naiss->format("Y-m-d");
				}
				if(!empty($_POST["sta_ville_naiss"])){
					$sta_ville_naiss=$_POST["sta_ville_naiss"];
				}
				if(!empty($_POST["sta_cp_naiss"])){
					$sta_cp_naiss=$_POST["sta_cp_naiss"];
				}
			}
		}
	}

	if(empty($cli_code) OR empty($cli_nom)){
		$_SESSION['message'][] = array(
			"titre" => "Attention",
			"type" => "danger",
			"message" => "Paramètre absent"
		);
		Header("Location: client_mod.php?client=" . $client);
		die();
	}

	// FIN TRAITEMENT FORM
	//********************************************


	/******************************************
		CONTROLE
	*******************************************/

	// toutes les fiches doivent avoir une adresse de fac
	if(empty($_POST["adr_cp2"]) OR empty($_POST["adr_ville2"])){
		$_SESSION['message'][] = array(
			"titre" => "Attention",
			"type" => "danger",
			"message" => "Vous devez renseigner une adresse de facturation par défaut"
		);
		Header("Location: client_mod.php?client=" . $client);
		die();
	}

	// toutes les fiches doivent avoir un contact par défaut lié à l'adresse par defaut
	if(empty($con_nom)){

		$_SESSION['message'][] = array(
			"titre" => "Attention",
			"type" => "danger",
			"message" => "Vous devez renseigner un contact par défaut."
		);
		Header("Location: client_mod.php?client=" . $client);
		die();

	}

	if($cli_categorie!=3){

		// controle autre que particulier

		if(empty($adr_cp_int) OR empty($adr_ville_int)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "danger",
				"message" => "Vous devez renseigner une adresse d'intervention par défaut"
			);
			Header("Location: client_mod.php?client=" . $client);
			die();
		}
		if(!empty($d_client["cli_siren"]) AND empty($cli_siren)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "danger",
				"message" => "Le SIREN est obligatoire pour ce client"
			);
			Header("Location: client_mod.php?client=" . $client);
			die();
		}
		if(!empty($d_ad_fac_def["adr_siret"]) AND empty($adr_siret)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "danger",
				"message" => "Le SIRET est obligatoire pour ce client"
			);
			Header("Location: client_mod.php?client=" . $client);
			die();
		}
		if(!empty($cli_siren) AND !empty($adr_siret) AND $check_siret){
			if(!empty(check_siret($cli_siren,$adr_siret,$adr_fac))){
				$_SESSION['message'][] = array(
					"titre" => "Attention",
					"type" => "danger",
					"message" => "Le siret " . $cli_siren . " " . $adr_siret . " existe déjà"
				);
				Header("Location: client_mod.php?client=" . $client);
				die();
			}
			// VERIF SIRET VIA API
			$siret = $cli_siren . $adr_siret;
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			// UTILISER LA V3
			curl_setopt($ch,CURLOPT_URL, "https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/" . preg_replace('/\s+/', '', $siret));
			$resultat_json=curl_exec($ch);
			curl_close($ch);
			$result_api=json_decode($resultat_json);
			if(empty($result_api->etablissement)){
				$texte = "Le client " . $cli_code  . " " . $cli_nom . " a été modifié mais n'est pas reconnu par l'api du gouvernement.";
				add_notifications("<i class='fa fa-times' ></i>",$texte,"bg-warning","","","","191,13",$acc_societe,$acc_agence,1);

				$erreur_txt="le siret " . $cli_siren . " " . $adr_siret . " n'est pas reconnu par l'api du gouvernement mais la fiche client a été actualisée";
				$_SESSION['message'][] = array(
					"titre" => "Attention",
					"type" => "warning",
					"message" => $erreur_txt
				);
				// Header("Location: client_mod.php?client=" . $client);
				// die();
			}
		}


		// on supp le lien client / stagiaire
		$cli_stagiaire=0;

	}else{

		// PARTICULIER

		if(empty($con_nom) OR empty($con_prenom) OR empty($sta_naissance)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "danger",
				"message" => "Vous devez renseigner le nom, le prénom et la date de naissance du stagiaire dans la partie contact."
			);
			Header("Location: client_mod.php?client=" . $client);
			die();
		}

		if($d_client["cli_categorie"]!=3){

			// client qui devient particulier -> il ne doit avoir qu'une seul adresse et un seul contact

			$sql="SELECT * FROM contacts WHERE con_ref_id = " . $client;
			$req=$Conn->prepare($sql);
			$req->execute();
			$client_contact_count = $req->fetchAll();
			if(count($client_contact_count) > 1){
				$_SESSION['message'][] = array(
					"titre" => "Attention",
					"type" => "danger",
					"message" => "Vous ne pouvez pas passer votre client en particulier car vous avez plus d'un contact"
				);
				Header("Location: client_mod.php?client=" . $client);
				die();
			}

			$sql="SELECT * FROM adresses WHERE adr_ref_id = " . $client;
			$req=$Conn->prepare($sql);
			$req->execute();
			$client_adresse_count = $req->fetchAll();
			if(count($client_adresse_count) > 1){
				$_SESSION['message'][] = array(
					"titre" => "Attention",
					"type" => "danger",
					"message" => "Vous ne pouvez pas passer votre client en particulier car vous avez plus d'une adresse"
				);
				Header("Location: client_mod.php?client=" . $client);
				die();
			}
		}

		// on verifie que les données contact ne vont pas créer un doublon
		// si on arrive ici -> sta_naissance est forcément connu car obligatoire dans les controle au dessus

		$cli_stagiaire=$d_client["cli_stagiaire"];

		$sql="SELECT sta_id FROM Stagiaires WHERE sta_nom=:nom AND sta_prenom=:prenom";
		$sql.=" AND sta_id!=:stagiaire";
		$sql.=" AND (sta_naissance=:naissance OR ISNULL(sta_naissance) )";
		// la date de naissance est obligatoire pour un particulier sinon impossible de gérer les doublon
		$sql.=" AND sta_naissance=:naissance";
		$sql.=" ORDER BY sta_id";
		$req=$Conn->prepare($sql);
		$req->bindParam(":nom",$con_nom);
		$req->bindParam(":prenom",$con_prenom);
		$req->bindParam(":stagiaire",$cli_stagiaire);
		$req->bindParam(":naissance",$sta_naissance);
		$req->execute();
		$doublons_stagiaires=$req->fetchAll();
		if(!empty($doublons_stagiaires)){

			// il existe un doublon différent de celui déjà lié à la fiche ou il existe plusieur doublon
			if(!empty($cli_stagiaire) OR count($doublons_stagiaires)>1){
				$_SESSION['message'][] = array(
					"titre" => "Attention",
					"type" => "danger",
					"message" => "Plusieurs fiches stagiaires correspondent au contact client que vous tentez d'enregistrer."
				);
				Header("Location: client_mod.php?client=" . $client);
				die();

			}else{
				// le lien cli / sta n'existe pas et il n'y a qu'une occurence doublon
				// on verif que le doublon n'est pas déja lié à une fiche client

				$sql="SELECT cli_id FROM Clients WHERE cli_stagiaire=:stagiaire;";
				$req=$Conn->prepare($sql);
				$req->bindParam(":stagiaire",$doublons_stagiaires[0]["sta_id"]);
				$req->execute();
				$cli_sta=$req->fetch();
				if(!empty($cli_sta)){
					$_SESSION['message'][] = array(
						"titre" => "Attention",
						"type" => "danger",
						"message" => "Une fiche particulier est déjà associée à ce stagiaire."
					);
					Header("Location: client_mod.php?client=" . $client);
					die();
				}else{
					$cli_stagiaire=$doublons_stagiaires[0]["sta_id"];
				}
			}
		}

		// init val non utiliser par particulier

		$adr_siret="";
		$cli_groupe=0;
	}

	// le client appartenait à un groupe avant modif => il peut avoir des filiales
	if(!empty($d_client["cli_groupe"])){
		$filiales=get_groupe_filiales($client);
		if(!empty($filiales) AND empty($cli_groupe)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "danger",
				"message" => "Ce client dispose de filiale. Il ne peut pas passer en 'Entreprise seule'"
			);
			Header("Location: client_mod.php?client=" . $client);
			die();

		}
	}

	// si c'est une filiale, on s'assure que les categorie et sous catégorie correspondent.
	// on en profite pour récupérer les données MM qui nous serviront pour gérer l'héritage sur les filiales

	if(!empty($cli_filiale_de)){
		$sql_mm="SELECT cli_id,cli_blackliste,cli_opca,cli_facture_opca,cli_fac_groupe,cli_opca_type FROM Clients WHERE cli_id=:maison_mere AND cli_categorie=:cli_categorie AND cli_sous_categorie=:cli_sous_categorie;";
		$req_mm=$Conn->prepare($sql_mm);
		$req_mm->bindParam(":maison_mere",$cli_filiale_de);
		$req_mm->bindParam(":cli_categorie",$cli_categorie);
		$req_mm->bindParam(":cli_sous_categorie",$cli_sous_categorie);
		$req_mm->execute();
		$d_maison_mere=$req_mm->fetch();
		if(empty($d_maison_mere)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "danger",
				"message" => "La classification de la filiale est différente de celle de la maison mère."
			);
			Header("Location: client_mod.php?client=" . $client);
			die();
		}else{

			if(isset($cli_fac_groupe) AND ($cli_sous_categorie==1 OR $cli_sous_categorie==3)){
				// la modif de U déclenche la réinitialisation de la factuartion groupée
				if(empty($cli_fac_groupe) AND !empty($d_maison_mere["cli_fac_groupe"])){
					// il y a une facturation groupée sur la mm, on l'affecte a la filiale
					$cli_fac_groupe=$d_maison_mere["cli_fac_groupe"];
				}
			}
			if($changement_groupe){
			// lors d'un changement de groupe, on force la valeurs des données suivantes avec les valeurs de la nouvelle MM.
			// U peut les personaliser dans un second temps en re-modifiant la filiale.

				if(empty($cli_opca) AND !empty($d_maison_mere["cli_opca"])){
					// si le client n'a pas de financeur et qu'il intègre un groupe avec financeur

					$actu_financeur=true;

					$cli_opca=$d_maison_mere["cli_opca"];
					$cli_opca_type=$d_maison_mere["cli_opca_type"];
					$cli_facture_opca=$d_maison_mere["cli_facture_opca"];
				}

			}
		}

	}


	/* fin de controle
	***********************************************/

	/*var_dump($cli_siren);
	echo("<br/>");
	var_dump($con_nom);
	die();*/

	/******************************************
		ENREGISTREMENT
	*******************************************/

	// ON TRAITE D'ABORD LES ENREGISTREMENT ANNEXE POUR METTRE A JOUR LA FICHE

	// ADRESSES D'INTERVENTION

	if($cli_categorie!=3){

		// AUTRE QUE PARTICULIER

		if($adr_int>0){

			// MAJ

			$sql="UPDATE adresses SET
			adr_nom=:adr_nom,
			adr_service=:adr_service,
			adr_ad1=:adr_ad1,
			adr_ad2=:adr_ad2,
			adr_ad3=:adr_ad3,
			adr_cp=:adr_cp,
			adr_ville=:adr_ville,
			adr_libelle=:adr_libelle,
			adr_geo=:adr_geo
			WHERE adr_id=:adresse AND adr_defaut=1;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":adr_nom",$adr_nom_int);
			$req->bindParam(":adr_service",$adr_service_int);
			$req->bindParam(":adr_ad1",$adr_ad1_int);
			$req->bindParam(":adr_ad2",$adr_ad2_int);
			$req->bindParam(":adr_ad3",$adr_ad3_int);
			$req->bindParam(":adr_cp",$adr_cp_int);
			$req->bindParam(":adr_ville",$adr_ville_int);
			$req->bindParam(":adr_libelle",$_POST["adr_libelle1"]);
			$req->bindParam(":adr_geo",$adr_geo_int);
			$req->bindParam(":adresse",$adr_int);
			try{
				$req->execute();
			}catch(Exception $e){
				$erreur="E - " . $e->getMessage();
			}


		}else{
			// ADD

			$sql="INSERT INTO adresses
			(adr_ref,adr_ref_id,adr_type,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_libelle,adr_geo,adr_defaut)
			VALUES (1,:adr_ref_id,1,:adr_nom,:adr_service,:adr_ad1,:adr_ad2,:adr_ad3,:adr_cp,:adr_ville,:adr_libelle,:adr_geo,1);";
			$req=$Conn->prepare($sql);
			$req->bindParam(":adr_ref_id",$client);
			$req->bindParam(":adr_nom",$adr_nom_int);
			$req->bindParam(":adr_service",$adr_service_int);
			$req->bindParam(":adr_ad1",$adr_ad1_int);
			$req->bindParam(":adr_ad2",$adr_ad2_int);
			$req->bindParam(":adr_ad3",$adr_ad3_int);
			$req->bindParam(":adr_cp",$adr_cp_int);
			$req->bindParam(":adr_ville",$adr_ville_int);
			$req->bindParam(":adr_libelle",$_POST["adr_libelle1"]);
			$req->bindParam(":adr_geo",$adr_geo_int);
			try{
				$req->execute();
				$adr_int=$Conn->lastInsertId();
			}catch(Exception $e){
				$erreur="B - " . $e->getMessage();
			}
		}

		// on memorise l'adresse d'intervention sur la fiche client

		$cli_adresse=$adr_int;
		$cli_adr_nom=$adr_nom_int;
		$cli_adr_service=$adr_service_int;
		$cli_adr_ad1=$adr_ad1_int;
		$cli_adr_ad2=$adr_ad2_int;
		$cli_adr_ad3=$adr_ad3_int;
		$cli_adr_cp=$adr_cp_int;
		$cli_adr_ville=$adr_ville_int;

	}else{

		// un particulier ne peut pas avoir d'adresse d'intervention on delete

		$sql_del_adr="DELETE FROM Adresses WHERE adr_id=:adresse;";
		$req_del_adr=$Conn->prepare($sql_del_adr);

		$sql_del_con="DELETE FROM Adresses_Contacts WHERE aco_adresse=:adresse;";
		$req_del_con=$Conn->prepare($sql_del_con);

		$sql_get="SELECT adr_id FROM Adresses WHERE adr_ref=1 AND adr_type=1 AND adr_ref_id=:adr_ref_id;";
		$req=$Conn->prepare($sql_get);
		$req->bindParam(":adr_ref_id",$client);
		$req->execute();
		$d_adresses_del=$req->fetchAll();
		if(!empty($d_adresses_del)){
			foreach($d_adresses_del as $del){

				// del lien contact
				$req_del_con->bindParam(":adresse",$del["adr_id"]);
				$req_del_con->execute();

				// del adresse
				$req_del_adr->bindParam(":adresse",$del["adr_id"]);
				$req_del_adr->execute();
			}
		}
	}

	// ADRESSE DE FACTURATION

	if(empty($erreur)){

		if($adr_fac>0){

			// MAJ

			$sql="UPDATE adresses SET
			adr_nom=:adr_nom,
			adr_service=:adr_service,
			adr_ad1=:adr_ad1,
			adr_ad2=:adr_ad2,
			adr_ad3=:adr_ad3,
			adr_cp=:adr_cp,
			adr_ville=:adr_ville,
			adr_libelle=:adr_libelle,
			adr_geo=:adr_geo,
			adr_siret=:adr_siret
			WHERE adr_id=:adresse AND adr_defaut=1;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":adr_nom",$_POST["adr_nom2"]);
			$req->bindParam(":adr_service",$_POST["adr_service2"]);
			$req->bindParam(":adr_ad1",$_POST["adr_ad12"]);
			$req->bindParam(":adr_ad2",$_POST["adr_ad22"]);
			$req->bindParam(":adr_ad3",$_POST["adr_ad32"]);
			$req->bindParam(":adr_cp",$_POST["adr_cp2"]);
			$req->bindParam(":adr_ville",$_POST["adr_ville2"]);
			$req->bindParam(":adr_libelle",$_POST["adr_libelle2"]);
			$req->bindParam(":adr_geo",$adr_geo_fac);
			$req->bindParam(":adr_siret",$adr_siret);
			$req->bindParam(":adresse",$adr_fac);
			try{
				$req->execute();
			}catch(Exception $e){
				$erreur="D - " . $e->getMessage();
			}

		}else{

			// ADD

			$sqla="INSERT INTO adresses
			(adr_ref,adr_ref_id,adr_type,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_libelle,adr_geo,adr_siret,adr_defaut)
			VALUES (1,:adr_ref_id,2,:adr_nom,:adr_service,:adr_ad1,:adr_ad2,:adr_ad3,:adr_cp,:adr_ville,:adr_libelle,:adr_geo,:adr_siret,1);";
			$req=$Conn->prepare($sqla);
			$req->bindParam(":adr_ref_id",$client);
			$req->bindParam(":adr_nom",$_POST["adr_nom2"]);
			$req->bindParam(":adr_service",$_POST["adr_service2"]);
			$req->bindParam(":adr_ad1",$_POST["adr_ad12"]);
			$req->bindParam(":adr_ad2",$_POST["adr_ad22"]);
			$req->bindParam(":adr_ad3",$_POST["adr_ad32"]);
			$req->bindParam(":adr_cp",$_POST["adr_cp2"]);
			$req->bindParam(":adr_ville",$_POST["adr_ville2"]);
			$req->bindParam(":adr_libelle",$_POST["adr_libelle2"]);
			$req->bindParam(":adr_geo",$adr_geo_fac);
			$req->bindParam(":adr_siret",$adr_siret);
			try{
				$req->execute();
				$adr_fac = $Conn->lastInsertId();
			}catch(Exception $e){
				$erreur="G - " . $e->getMessage();
				echo($erreur);
				die();
			}
		}

		if($cli_categorie==3){
			$cli_adresse=$adr_fac;
			$cli_adr_nom=$_POST["adr_nom2"];
			$cli_adr_service=$_POST["adr_service2"];
			$cli_adr_ad1=$_POST["adr_ad12"];
			$cli_adr_ad2=$_POST["adr_ad22"];
			$cli_adr_ad3=$_POST["adr_ad32"];
			$cli_adr_cp=$_POST["adr_cp2"];
			$cli_adr_ville=$_POST["adr_ville2"];
		}
	}

	// CONTACT PAR DEFAUT

	if(empty($erreur)){
		if($contact>0){

			// MAJ

			$sql="UPDATE contacts SET
			con_fonction=:con_fonction,
			con_fonction_nom=:con_fonction_nom,
			con_titre=:con_titre,
			con_nom=:con_nom,
			con_prenom=:con_prenom,
			con_tel=:con_tel,
			con_portable=:con_portable,
			con_fax=:con_fax,
			con_mail=:con_mail,
			con_compta=:con_compta
			WHERE con_id=:contact";
			$req=$Conn->prepare($sql);
			$req->bindParam(":con_fonction",$con_fonction);
			$req->bindParam(":con_fonction_nom",$con_fonction_nom);
			$req->bindParam(":con_titre",$con_titre);
			$req->bindParam(":con_nom",$con_nom);
			$req->bindParam(":con_prenom",$con_prenom);
			$req->bindParam(":con_tel",$con_tel);
			$req->bindParam(":con_portable",$con_portable);
			$req->bindParam(":con_fax",$con_fax);
			$req->bindParam(":con_mail",$con_mail);
			$req->bindParam(":con_compta",$con_compta);
			$req->bindParam(":contact",$contact);
			try{
				$req->execute();
			}catch(Exception $e){
				$erreur="B - " . $e->getMessage();
			}
		}else{
			// ADD

			$sql="INSERT INTO contacts (
			con_ref,con_ref_id,con_fonction,con_fonction_nom,con_titre,con_nom,con_prenom,con_tel,con_portable,con_fax,
			con_mail,con_compta)
			VALUES (1,:client,:con_fonction,:con_fonction_nom,:con_titre,:con_nom,:con_prenom,:con_tel,:con_portable,:con_fax,
			:con_mail,:con_compta);";
			$req=$Conn->prepare($sql);
			$req->bindParam(":client",$client);
			$req->bindParam(":con_fonction",$con_fonction);
			$req->bindParam(":con_fonction_nom",$con_fonction_nom);
			$req->bindParam(":con_titre",$con_titre);
			$req->bindParam(":con_nom",$con_nom);
			$req->bindParam(":con_prenom",$con_prenom);
			$req->bindParam(":con_tel",$con_tel);
			$req->bindParam(":con_portable",$con_portable);
			$req->bindParam(":con_fax",$con_fax);
			$req->bindParam(":con_mail",$con_mail);
			$req->bindParam(":con_compta",$con_compta);
			try{
				$req->execute();
				$contact=$Conn->lastInsertId();
			}catch(Exception $e){
				$erreur="F - " . $e->getMessage();
			}
		}
	}

	// LIEN ADRESSE PAR DEFAUT ET CONTACT PAR DEFAUT

	if(empty($erreur)){
		// on utilise cli_adresse car contact par defaut pour etre lié à adresse int pour entreprise et adr fac pour particulier

		$con_def_enr=FALSE;

		$req_up_ad_con=$Conn->prepare("UPDATE Adresses_Contacts SET aco_defaut = :aco_defaut WHERE aco_adresse=:cli_adresse AND aco_contact=:aco_contact;");

		$req=$Conn->prepare("SELECT aco_contact FROM Adresses_Contacts WHERE aco_adresse=:cli_adresse AND (aco_defaut=1 OR aco_contact=:contact);");
		$req->bindParam(":cli_adresse",$cli_adresse);
		$req->bindParam(":contact",$contact);
		$req->execute();
		$d_adresse_contact=$req->fetchAll();
		if(!empty($d_adresse_contact)){
			foreach($d_adresse_contact as $d_adr_con){

				if($d_adr_con["aco_contact"]!=$contact){
					$req_up_ad_con->bindParam(":cli_adresse",$cli_adresse);
					$req_up_ad_con->bindParam(":aco_contact",$d_adr_con["aco_contact"]);
					$req_up_ad_con->bindValue(":aco_defaut",0);
					$req_up_ad_con->execute();
				}else{
					$req_up_ad_con->bindParam(":cli_adresse",$cli_adresse);
					$req_up_ad_con->bindParam(":aco_contact",$d_adr_con["aco_contact"]);
					$req_up_ad_con->bindValue(":aco_defaut",1);
					$req_up_ad_con->execute();
					$con_def_enr=TRUE;
				}
			}
		}
		if(!$con_def_enr){
			$req=$Conn->prepare("INSERT INTO Adresses_Contacts (aco_adresse,aco_contact,aco_defaut) VALUES (:aco_adresse,:aco_contact,1);");
			$req->bindParam(":aco_adresse",$cli_adresse);
			$req->bindParam(":aco_contact",$contact);
			$req->execute();
		}
	}

	// LIEN FICHE STAGIAIRE FICHE CLIENT

	if(empty($erreur)){

		if($cli_categorie==3){

			// le client peut pas déjà lié à une fiche sta ou controle n'a trouver q'une seule occurrence doublon

			if(!empty($cli_stagiaire)){

				// on recupère les info sur la fiche stagiaire

				$sql="SELECT sta_client,scl_client FROM Stagiaires LEFT OUTER JOIN Stagiaires_Clients ON (Stagiaires.sta_id=Stagiaires_Clients.scl_stagiaire AND Stagiaires_Clients.scl_client=:client)
				WHERE sta_id=:stagiaire;";
				$req = $Conn->prepare($sql);
				$req->bindParam("stagiaire",$cli_stagiaire);
				$req->bindParam("client",$client);
				$req->execute();
				$d_stagiaire=$req->fetch();

				if(!empty($d_stagiaire)){

					$sql="UPDATE Stagiaires SET
					sta_titre = :sta_titre,
					sta_nom = :sta_nom,
					sta_prenom = :sta_prenom,
					sta_naissance = :sta_naissance,
					sta_cp_naiss = :sta_cp_naiss,
					sta_ville_naiss = :sta_ville_naiss,
					sta_ad1 = :sta_ad1,
					sta_ad2 = :sta_ad2,
					sta_ad3 = :sta_ad3,
					sta_cp = :sta_cp,
					sta_ville = :sta_ville,
					sta_tel = :sta_tel,
					sta_portable = :sta_portable,
					sta_mail_perso = :sta_mail_perso";
					if($d_stagiaire["sta_client"]==$client){
						$sql.=",sta_client_code = :sta_client_code,
						sta_client_nom = :sta_client_nom";
					}
					$sql.=" WHERE sta_id = :sta_id";
					$req = $Conn->prepare($sql);
					$req->bindParam("sta_titre",$con_titre);
					$req->bindParam("sta_nom",$con_nom);
					$req->bindParam("sta_prenom",$con_prenom);
					$req->bindParam("sta_naissance",$sta_naissance);
					$req->bindParam("sta_cp_naiss",$sta_cp_naiss);
					$req->bindParam("sta_ville_naiss",$sta_ville_naiss);
					$req->bindParam("sta_ad1",$cli_adr_ad1);
					$req->bindParam("sta_ad2",$cli_adr_ad2);
					$req->bindParam("sta_ad3",$cli_adr_ad3);
					$req->bindParam("sta_cp",$cli_adr_cp);
					$req->bindParam("sta_ville",$cli_adr_ville);
					$req->bindParam("sta_tel",$con_tel);
					$req->bindParam("sta_portable",$con_portable);
					$req->bindParam("sta_mail_perso",$con_mail);
					if($d_stagiaire["sta_client"]==$client){
						$req->bindParam("sta_client_code",$cli_code);
						$req->bindParam("sta_client_nom",$cli_nom);
					}
					$req->bindParam("sta_id",$cli_stagiaire);
					$req->execute();



					if(empty($d_stagiaire["scl_client"])){
						// le client est le stagiaire ne sont pas encore lié
						$sql="INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:stagiaire,:client);";
						$req = $Conn->prepare($sql);
						$req->bindParam("stagiaire",$cli_stagiaire);
						$req->bindParam("client",$client);
						$req->execute();
					}

				}
			}else{

				// creation de la fiche stagiaire

				$sql="INSERT INTO Stagiaires (sta_titre,sta_nom,sta_prenom,sta_naissance,sta_cp_naiss,sta_ville_naiss,sta_ad1,sta_ad2,sta_ad3,sta_cp,sta_ville,sta_tel,sta_portable,sta_mail_perso,sta_client,sta_client_code,sta_client_nom)
				VALUES (:sta_titre,:sta_nom,:sta_prenom,:sta_naissance,:sta_cp_naiss,:sta_ville_naiss,:sta_ad1,:sta_ad2,:sta_ad3,:sta_cp,:sta_ville,:sta_tel,:sta_portable,:sta_mail_perso,:sta_client,:sta_client_code,:sta_client_nom);";
				$req = $Conn->prepare($sql);
				$req->bindParam("sta_titre",$con_titre);
				$req->bindParam("sta_nom",$con_nom);
				$req->bindParam("sta_prenom",$con_prenom);
				$req->bindParam("sta_naissance",$sta_naissance);
				$req->bindParam("sta_cp_naiss",$sta_cp_naiss);
				$req->bindParam("sta_ville_naiss",$sta_ville_naiss);
				$req->bindParam("sta_ad1",$cli_adr_ad1);
				$req->bindParam("sta_ad2",$cli_adr_ad2);
				$req->bindParam("sta_ad3",$cli_adr_ad3);
				$req->bindParam("sta_cp",$cli_adr_cp);
				$req->bindParam("sta_ville",$cli_adr_ville);
				$req->bindParam("sta_tel",$con_tel);
				$req->bindParam("sta_portable",$con_portable);
				$req->bindParam("sta_mail_perso",$con_mail);
				$req->bindParam("sta_client",$client);
				$req->bindParam("sta_client_code",$cli_code);
				$req->bindParam("sta_client_nom",$cli_nom);
				$req->execute();
				$cli_stagiaire=$Conn->lastInsertId();

				$sql="INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:scl_stagiaire,:scl_client);";
				$req = $Conn->prepare($sql);
				$req->bindParam("scl_client",$client);
				$req->bindParam("scl_stagiaire",$cli_stagiaire);
				$req->execute();

			}
		}
	}

	// MISE A JOUR DE LA FICHE

	if(empty($erreur)){
		$sql="UPDATE clients SET
		cli_code = :cli_code,
		cli_nom = :cli_nom,
		cli_reference = :cli_reference,
		cli_important = :cli_important,
		cli_categorie = :cli_categorie,
		cli_sous_categorie = :cli_sous_categorie,
		cli_contrat = :cli_contrat,
		cli_groupe = :cli_groupe,
		cli_filiale_de = :cli_filiale_de,
		cli_niveau = :cli_niveau,
		cli_fil_de = :cli_fil_de,";
		if(isset($cli_fac_groupe)){
			$sql.="cli_fac_groupe = :cli_fac_groupe,";
		}
		$sql.="cli_adresse = :cli_adresse,
		cli_adr_nom = :cli_adr_nom,
		cli_adr_service = :cli_adr_service,
		cli_adr_ad1 = :cli_adr_ad1,
		cli_adr_ad2 = :cli_adr_ad2,
		cli_adr_ad3 = :cli_adr_ad3,
		cli_adr_cp = :cli_adr_cp,
		cli_adr_ville = :cli_adr_ville,
		cli_contact = :cli_contact,
		cli_con_fct = :cli_con_fct,
		cli_con_fct_nom = :cli_con_fct_nom,
		cli_con_nom = :cli_con_nom,
		cli_con_prenom = :cli_con_prenom,
		cli_con_tel = :cli_con_tel,
		cli_con_mail = :cli_con_mail,
		cli_siren = :cli_siren,
		cli_facture_opca = :cli_facture_opca,
		cli_opca_type = :cli_opca_type,
		cli_opca = :cli_opca,
		cli_releve = :cli_releve,
		cli_ident_tva = :cli_ident_tva,
		cli_reg_type = :cli_reg_type,
		cli_reg_formule = :cli_reg_formule,
		cli_reg_nb_jour = :cli_reg_nb_jour,
		cli_reg_fdm= :cli_reg_fdm,
		cli_blackliste = :cli_blackliste,
		cli_interco_soc = :cli_interco_soc,
		cli_interco_age = :cli_interco_age,
		cli_classification = :cli_classification,
		cli_classification_type = :cli_classification_type,
		cli_classification_categorie = :cli_classification_categorie,
		cli_stagiaire=:cli_stagiaire,
		cli_stagiaire_type = :cli_stagiaire_type,
		cli_ape = :cli_ape,
		cli_prescripteur = :cli_prescripteur,
		cli_prescripteur_info = :cli_prescripteur_info,
		cli_capital = :cli_capital,
		cli_soc_type = :cli_soc_type,
		cli_immat_lieu = :cli_immat_lieu,
		cli_affacturable= :cli_affacturable,
		cli_affacturable_txt= :cli_affacturable_txt,
		cli_affacturage=:cli_affacturage,
		cli_affacturage_iban=:cli_affacturage_iban,
		cli_tel=:cli_tel
		WHERE cli_id = :cli_id";

		$req = $Conn->prepare($sql);
		$req->bindParam("cli_code",$cli_code);
		$req->bindParam("cli_nom",$cli_nom);
		$req->bindParam("cli_reference",$cli_reference);
		$req->bindParam("cli_important",$cli_important);
		$req->bindParam("cli_categorie",$cli_categorie);
		$req->bindParam("cli_sous_categorie",$cli_sous_categorie);
		$req->bindParam("cli_contrat",$cli_contrat);
		$req->bindParam("cli_groupe",$cli_groupe);
		$req->bindParam("cli_filiale_de",$cli_filiale_de);
		$req->bindParam("cli_niveau",$cli_niveau);
		$req->bindParam("cli_fil_de",$cli_fil_de);
		if(isset($cli_fac_groupe)){
			$req->bindParam("cli_fac_groupe",$cli_fac_groupe);
		}
		// adresse d'intervention par défaut
		$req->bindParam("cli_adresse",$cli_adresse);
		$req->bindParam("cli_adr_nom",$cli_adr_nom);
		$req->bindParam("cli_adr_service",$cli_adr_service);
		$req->bindParam("cli_adr_ad1",$cli_adr_ad1);
		$req->bindParam("cli_adr_ad2",$cli_adr_ad2);
		$req->bindParam("cli_adr_ad3",$cli_adr_ad3);
		$req->bindParam("cli_adr_cp",$cli_adr_cp);
		$req->bindParam("cli_adr_ville",$cli_adr_ville);
		// contact par defaut
		$req->bindParam("cli_contact",$contact);
		$req->bindParam("cli_con_fct",$con_fonction);
		$req->bindParam("cli_con_fct_nom",$con_fonction_nom);
		$req->bindParam("cli_con_nom",$con_nom);
		$req->bindParam("cli_con_prenom",$con_prenom);
		$req->bindParam("cli_con_tel",$con_tel);
		$req->bindParam("cli_con_mail",$con_mail);

		$req->bindParam("cli_siren",$cli_siren);
		$req->bindParam("cli_facture_opca",$cli_facture_opca);
		$req->bindParam("cli_opca_type",$cli_opca_type);
		$req->bindParam("cli_opca",$cli_opca);
		$req->bindParam("cli_releve",$cli_releve);
		$req->bindParam("cli_ident_tva",$cli_ident_tva);
		$req->bindParam("cli_reg_type",$cli_reg_type);
		$req->bindParam("cli_reg_formule",$cli_reg_formule);
		$req->bindParam("cli_reg_nb_jour",$cli_reg_nb_jour);
		$req->bindParam("cli_reg_fdm",$cli_reg_fdm);
		$req->bindParam("cli_blackliste",$cli_blackliste);
		$req->bindParam("cli_interco_soc",$cli_interco_soc);
		$req->bindParam("cli_interco_age",$cli_interco_age);
		$req->bindParam("cli_affacturable",$cli_affacturable);
		$req->bindParam("cli_affacturable_txt",$cli_affacturable_txt);
		$req->bindParam("cli_affacturage",$cli_affacturage);
		$req->bindParam("cli_affacturage_iban",$cli_affacturage_iban);

		// autre info
		$req->bindParam("cli_classification",$cli_classification);
		$req->bindParam("cli_classification_type",$cli_classification_type);
		$req->bindParam("cli_classification_categorie",$cli_classification_categorie);
		$req->bindParam("cli_stagiaire",$cli_stagiaire);
		$req->bindParam("cli_stagiaire_type",$cli_stagiaire_type);
		$req->bindParam("cli_ape",$cli_ape);
		$req->bindParam("cli_prescripteur",$cli_prescripteur);
		$req->bindParam("cli_prescripteur_info",$cli_prescripteur_info);
		$req->bindParam("cli_capital",$cli_capital);
		$req->bindParam("cli_soc_type",$cli_soc_type);
		$req->bindParam("cli_immat_lieu",$cli_immat_lieu);
		$req->bindParam("cli_tel",$cli_tel);


		$req->bindParam("cli_id",$client);
		$req->execute();
		try{
			$req->execute();
		}catch(Exception $e){
			$erreur="C-" . $e->getMessage();
		}
	}

	// MAJ DE LA TABLE CLIENTS SOCIETES

	// rib sur toutes les agences de connsoc

	if($_SESSION['acces']["acc_droits"][9] AND $cso_rib>0 ){
		$req=$Conn->prepare("UPDATE Clients_Societes SET cso_rib=:cso_rib,cso_rib_change=:cso_rib_change WHERE cso_client=:cso_client AND cso_societe=:cso_societe;");
		$req->bindValue("cso_rib",$cso_rib);
		$req->bindValue("cso_rib_change",$cso_rib_change);
		$req->bindValue("cso_client",$client);
		$req->bindValue("cso_societe",$acc_societe);
		$req->execute();
	}

	// com et archive
	$req=$Conn->prepare("UPDATE Clients_Societes SET cso_commercial=:cso_commercial, cso_utilisateur=:cso_utilisateur, cso_archive=:cso_archive
	WHERE cso_client=:cso_client AND cso_societe=:cso_societe AND cso_agence=:cso_agence;");
	foreach($tab_agence as $tab_age){
		if($tab_age["update"]==1){
			$req->bindParam("cso_commercial",$tab_age["commercial"]);
			$req->bindParam("cso_utilisateur",$tab_age["utilisateur"]);
			$req->bindParam("cso_archive",$tab_age["archive"]);
			$req->bindValue("cso_client",$client);
			$req->bindValue("cso_societe",$acc_societe);
			$req->bindParam("cso_agence",$tab_age["agence"]);
			$req->execute();
		}
	}

	// si client a porté nationnal -> on s'assure que cli est dispo sur GFC GC
	if( ($cli_categorie==2 OR ($cli_categorie==5 AND $cli_groupe==1 AND $cli_filiale_de==0) ) AND ($acc_societe!=4 OR $acc_agence!=4) ){

		$req=$Conn->prepare("SELECT cso_commercial FROM Clients_Societes WHERE cso_client=:cso_client AND cso_societe=4 AND cso_agence=4;");
		$req->bindParam("cso_client",$client);
		$req->execute();
		$existe_gfc=$req->fetch();
		if(empty($existe_gfc)){

			// le client n'est pas sur GFC => on l'ajout en automatique

			// on recupere le RIB par defaut de GFC

			$req=$Conn->query("SELECT rib_id,rib_change FROM Rib WHERE rib_societe=4 AND rib_defaut;");
			$d_rib=$req->fetch();

			// si filiale gc on va cherche le com de la maison mere

			$gc_commercial=0;
			$gc_utilisateur=0;
			if($cli_categorie==2 AND $cli_groupe==1 AND $cli_filiale_de>0){
				$req=$Conn->prepare("SELECT cso_commercial,cso_utilisateur FROM Clients_Societes WHERE cso_client=:maison_mere AND cso_societe=4 AND cso_agence=4;");
				$req->bindParam("maison_mere",$cli_filiale_de);
				$req->execute();
				$d_com_mm=$req->fetch();
				if(!empty($d_com_mm)){
					$gc_commercial=$d_com_mm["cso_commercial"];
					$gc_utilisateur=$d_com_mm["cso_utilisateur"];
				}
			}

			$req=$Conn->prepare("INSERT INTO Clients_Societes (cso_societe,cso_agence,cso_client,cso_commercial,cso_utilisateur,cso_rib,cso_rib_change)
			VALUES (4,4,:cso_client,:cso_commercial,:cso_utilisateur,:cso_rib,:cso_rib_change);");
			$req->bindParam("cso_client",$client);
			$req->bindParam("cso_commercial",$gc_commercial);
			$req->bindParam("cso_utilisateur",$gc_utilisateur);
			$req->bindParam("cso_rib",$d_rib["rib_id"]);
			$req->bindParam("cso_rib_change",$d_rib["rib_change"]);
			$req->execute();

		}

	}

	// MAJ DES BASE N


	$sql_get_n="SELECT * FROM Clients WHERE cli_id = :cli_id AND cli_agence = :cli_agence;";

	$sql_add_n="INSERT INTO Clients
	(cli_id,cli_code,cli_nom,cli_agence,cli_commercial, cli_utilisateur,cli_categorie, cli_groupe, cli_filiale_de,cli_archive,cli_sous_categorie
	,cli_rib,cli_rib_change,cli_blackliste,cli_cp,cli_interco_soc,cli_interco_age,cli_affacturage,cli_affacturable,cli_facture";
	if(isset($cli_fac_groupe)){
		$sql_add_n.=",cli_fac_groupe";
	}
	$sql_add_n.=") VALUES (:cli_id,:cli_code,:cli_nom,:cli_agence,:cli_commercial, :cli_utilisateur,:cli_categorie, :cli_groupe, :cli_filiale_de,:cli_archive,:cli_sous_categorie
	,:cli_rib,:cli_rib_change,:cli_blackliste,:cli_cp,:cli_interco_soc,:cli_interco_age,:cli_affacturage,:cli_affacturable,:cli_facture";
	if(isset($cli_fac_groupe)){
		$sql_add_n.=",:cli_fac_groupe";
	}
	$sql_add_n.=")";

	$sql_up_n="UPDATE Clients SET cli_code=:cli_code,cli_nom=:cli_nom,cli_commercial=:cli_commercial, cli_utilisateur=:cli_utilisateur
	,cli_categorie=:cli_categorie,cli_groupe=:cli_groupe, cli_filiale_de=:cli_filiale_de,cli_archive=:cli_archive,cli_sous_categorie=:cli_sous_categorie
	,cli_rib=:cli_rib, cli_rib_change=:cli_rib_change, cli_blackliste=:cli_blackliste,cli_cp=:cli_cp,cli_interco_soc=:cli_interco_soc,cli_interco_age=:cli_interco_age
	,cli_affacturage=:cli_affacturage,cli_affacturable=:cli_affacturable,cli_facture=:cli_facture";
	if(isset($cli_fac_groupe)){
		$sql_up_n.=",cli_fac_groupe=:cli_fac_groupe";
	}
	$sql_up_n.=" WHERE cli_id=:cli_id AND cli_agence = :cli_agence";


	$soc_n=0;
	$sql="SELECT * FROM Clients_Societes WHERE cso_client=:cso_client ORDER BY cso_societe;";
	$req=$Conn->prepare($sql);
	$req->bindValue("cso_client",$client);
	$req->execute();
	$d_client_societe=$req->fetchAll();
	if(!empty($d_client_societe)){
		foreach($d_client_societe as $d_cli_soc){

			if($d_cli_soc["cso_societe"]!=$soc_n){

				$ConnFct=connexion_fct($d_cli_soc["cso_societe"]);
				$soc_n=$d_cli_soc["cso_societe"];

				$req_get_n=$ConnFct->prepare($sql_get_n);

				$req_add_n=$ConnFct->prepare($sql_add_n);

				$req_up_n=$ConnFct->prepare($sql_up_n);
			}

			$req_get_n->bindValue("cli_id",$client);
			$req_get_n->bindValue("cli_agence",$d_cli_soc["cso_agence"]);
			$req_get_n->execute();
			$d_client_n=$req_get_n->fetch();
			if(empty($d_client_n)){

				// securite ne doit jamais etre execute l'add en base N se fait via client réseaux

				$req_add_n->bindValue("cli_code",$cli_code);
				$req_add_n->bindValue("cli_nom",$cli_nom);
				$req_add_n->bindValue("cli_agence",$d_cli_soc["cso_agence"]);
				$req_add_n->bindValue("cli_commercial",$d_cli_soc["cso_commercial"]);
				$req_add_n->bindValue("cli_utilisateur",$d_cli_soc["cso_utilisateur"]);
				$req_add_n->bindValue("cli_categorie",$cli_categorie);
				$req_add_n->bindValue("cli_groupe",$cli_groupe);
				$req_add_n->bindValue("cli_filiale_de",$cli_filiale_de);
				$req_add_n->bindValue("cli_archive",$d_cli_soc["cso_archive"]);
				$req_add_n->bindValue("cli_sous_categorie",$cli_sous_categorie);
				$req_add_n->bindValue("cli_rib",$d_cli_soc["cso_rib"]);
				$req_add_n->bindValue("cli_rib_change",$d_cli_soc["cso_rib_change"]);
				$req_add_n->bindValue("cli_blackliste",$cli_blackliste);
				$req_add_n->bindValue("cli_cp",$cli_adr_cp);
				$req_add_n->bindValue("cli_interco_soc",$cli_interco_soc);
				$req_add_n->bindValue("cli_interco_age",$cli_interco_age);
				$req_add_n->bindValue("cli_affacturage",$cli_affacturage);
				$req_add_n->bindValue("cli_affacturable",$cli_affacturable);
				$req_add_n->bindValue("cli_facture",$cli_facture);
				if(isset($cli_fac_groupe)){
					$req_add_n->bindValue("cli_fac_groupe",$cli_fac_groupe);
				}
				$req_add_n->bindValue("cli_id",$client);
				$req_add_n->execute();
				// INSERT

			}else{
				// SI EXISTE ON UPDATE

				$req_up_n->bindValue("cli_code",$cli_code);
				$req_up_n->bindValue("cli_nom",$cli_nom);
				$req_up_n->bindValue("cli_agence",$d_cli_soc["cso_agence"]);
				$req_up_n->bindValue("cli_commercial",$d_cli_soc["cso_commercial"]);
				$req_up_n->bindValue("cli_utilisateur",$d_cli_soc["cso_utilisateur"]);
				$req_up_n->bindValue("cli_categorie",$cli_categorie);
				$req_up_n->bindValue("cli_groupe",$cli_groupe);
				$req_up_n->bindValue("cli_filiale_de",$cli_filiale_de);
				$req_up_n->bindValue("cli_archive",$d_cli_soc["cso_archive"]);
				$req_up_n->bindValue("cli_sous_categorie",$cli_sous_categorie);
				$req_up_n->bindValue("cli_rib",$d_cli_soc["cso_rib"]);
				$req_up_n->bindValue("cli_rib_change",$d_cli_soc["cso_rib_change"]);
				$req_up_n->bindValue("cli_blackliste",$cli_blackliste);
				$req_up_n->bindValue("cli_cp",$cli_adr_cp);
				$req_up_n->bindValue("cli_interco_soc",$cli_interco_soc);
				$req_up_n->bindValue("cli_interco_age",$cli_interco_age);
				$req_up_n->bindValue("cli_affacturage",$cli_affacturage);
				$req_up_n->bindValue("cli_affacturable",$cli_affacturable);
				$req_up_n->bindValue("cli_facture",$cli_facture);
				if(isset($cli_fac_groupe)){
					$req_up_n->bindValue("cli_fac_groupe",$cli_fac_groupe);
				}
				$req_up_n->bindValue("cli_id",$client);
				try{
					$req_up_n->execute();
				}Catch(Exception $e){
					echo($e->getMessage());
					var_dump($d_cli_soc["cso_societe"]);
					die();
				}
			}
		}
	}

	// TRAITEMENT AUTO EFFET DE BORD

	if($d_client["cli_categorie"]!=$cli_categorie){

			if($cli_categorie==3){

				// le client est bascule en particulier

				// le fait de n'avoir q'une suel adresse et qu'un seul contact est géré dans la partie controle

				// supression de la base produit
				$sql="DELETE FROM Clients_Produits WHERE cpr_client=" . $client . ";";
				$Conn->query($sql);

			}
	}

	/* IMPACT FILIALE

	filiales est issu des controles si client groupe avant modif $filiales existe
	si il passe groupe il ne pouvait pas avoir de filiales
	LA FICHE A DEJA ETE MODIFIE on ne maj que les filiale*/

	if(isset($filiales)){

		if(!empty($filiales)){

			if($changement_groupe){

				// CHANGEMENT DE GROUPE

				// Attention cette partie n'actualise que les filiales -> il faudra penser a appliquer les même traitements à la fiche modifiée

				// NOTE FG : en cas de changement de groupe, les filiales conserves les données telles quelles ont été enregistré par l'utilisateur sur la fiche modifié.
				// si ça doit changer, utiliser $d_maison_mere en fonction des cas de figures.

				// FG 03/12/2019
				// -> exception au commentaire précédent
				// Laurent souhaite que la maj d'un financeur sur la maison mère actualise toutes les filiales

				if(empty($d_client['cli_filiale_de']) AND !empty($cli_filiale_de)){

					// c'était une maison mere qui passe filiale

					$actu_niv=1;
					$filiale_de=$cli_filiale_de;


				}elseif(!empty($d_client['cli_filiale_de']) AND empty($cli_filiale_de)){

					// c'était une filiale qui passe maison mere

					$actu_niv=-1-$d_client['cli_niveau'];
					$filiale_de=$client;

					$fil_blackliste=$cli_blackliste;



				}elseif($d_client['cli_filiale_de']!=$cli_filiale_de){

					echo("Je suis une filiale et je change de groupe!<br/>");

					// c'est une filiale qui change de groupe

					if($d_client['cli_niveau']!=0){
						// on actualise le niveau que si ce n'etait pas une filiale de premier niveau
						$actu_niv=-1-$d_client['cli_niveau'];
					}else{
						$actu_niv=0;
					}
					$filiale_de=$cli_filiale_de;

				}else{
					$warning_txt="Les filiales du clients n'ont pas été actualisées!";
				}
				if(empty($warning_txt)){

					$sql_up_fil="UPDATE Clients SET cli_filiale_de=:cli_filiale_de,cli_niveau=:cli_niveau,cli_categorie=:cli_categorie,cli_sous_categorie=:cli_sous_categorie";

					// FG 06/12/2019. ajout dans le cadre suite merge plusieurs features.
					if($cli_blackliste!=$d_client["cli_blackliste"]){
						$sql_up_fil.=",cli_blackliste=:cli_blackliste";
					}
					if($actu_financeur){
						$sql_up_fil.=",cli_opca=:cli_opca, cli_facture_opca=:cli_facture_opca,cli_opca_type=:cli_opca_type";
					}
					if(isset($cli_fac_groupe)){
						$sql_up_fil.=",cli_fac_groupe=:cli_fac_groupe";
					}
					$sql_up_fil.=" WHERE cli_id=:filiale;";
					$req_up_fil=$Conn->prepare($sql_up_fil);

					$sql_get_fil_soc="SELECT cso_societe,cso_agence FROM Clients_Societes WHERE cso_client=:filiale ORDER BY cso_societe;";
					$req_get_fil_soc=$Conn->prepare($sql_get_fil_soc);

					$sql_up_fil_soc="UPDATE Clients SET cli_filiale_de=:cli_filiale_de, cli_categorie=:cli_categorie, cli_sous_categorie=:cli_sous_categorie";
					if($cli_blackliste!=$d_client["cli_blackliste"]){
						$sql_up_fil_soc.=",cli_blackliste=:cli_blackliste";
					}
					if(isset($cli_fac_groupe)){
						$sql_up_fil_soc.=",cli_fac_groupe=:cli_fac_groupe";
					}
					$sql_up_fil_soc.=" WHERE cli_id=:filiale;";

					foreach($filiales as $filiale){

						$fil_niveau=$filiale["cli_niveau"]+$actu_niv;

						$req_up_fil->bindParam(":cli_filiale_de",$filiale_de);
						$req_up_fil->bindParam(":cli_niveau",$fil_niveau);
						$req_up_fil->bindParam(":cli_categorie",$cli_categorie);
						$req_up_fil->bindParam(":cli_sous_categorie",$cli_sous_categorie);
						if($cli_blackliste!=$d_client["cli_blackliste"]){
							$req_up_fil->bindParam(":cli_blackliste",$cli_blackliste);
						}
						if($actu_financeur){
							$req_up_fil->bindParam(":cli_facture_opca",$cli_facture_opca);
							$req_up_fil->bindParam(":cli_opca",$cli_opca);
							$req_up_fil->bindParam(":cli_opca_type",$cli_opca_type);
						}
						if(isset($cli_fac_groupe)){
							$req_up_fil->bindParam(":cli_fac_groupe",$cli_fac_groupe);
						}
						$req_up_fil->bindParam(":filiale",$filiale["cli_id"]);
						try{
							$req_up_fil->execute();
						}Catch(Exception $e){
							die();
						}

						$conn_soc_id=0;
						$req_get_fil_soc->bindParam(":filiale",$filiale["cli_id"]);
						$req_get_fil_soc->execute();
						$d_clients_soc=$req_get_fil_soc->fetchAll();
						if(!empty($d_clients_soc)){

							foreach($d_clients_soc as $d_cli_soc){

								if($conn_soc_id!=$d_cli_soc["cso_societe"]){
									$conn_soc_id=$d_cli_soc["cso_societe"];
									$ConnFct=connexion_fct($conn_soc_id);
									$req_up_fil_soc=$ConnFct->prepare($sql_up_fil_soc);
								}

								$req_up_fil_soc->bindParam(":cli_filiale_de",$filiale_de);
								$req_up_fil_soc->bindParam(":cli_categorie",$cli_categorie);
								$req_up_fil_soc->bindParam(":cli_sous_categorie",$cli_sous_categorie);
								if($cli_blackliste!=$d_client["cli_blackliste"]){
									$req_up_fil_soc->bindParam(":cli_blackliste",$cli_blackliste);
								}
								if(isset($cli_fac_groupe)){
									$req_up_fil_soc->bindParam(":cli_fac_groupe",$cli_fac_groupe);
								}
								$req_up_fil_soc->bindParam(":filiale",$filiale["cli_id"]);
								$req_up_fil_soc->execute();
							}

						}
					}
				}

			}else{

				// MEME GROUPE

				// c'est une maison mere qui reste maison mere ou filiale qui reste filiale de la meme maison mère

				$sql_up_fil="UPDATE Clients SET cli_categorie=:cli_categorie,cli_sous_categorie=:cli_sous_categorie";
				if($cli_blackliste!=$d_client["cli_blackliste"]){
					$sql_up_fil.=",cli_blackliste=:cli_blackliste";
				}
				if($cli_facture_opca!=$d_client["cli_facture_opca"] OR $cli_opca!=$d_client["cli_opca"]){
					$sql_up_fil.=",cli_facture_opca=:cli_facture_opca, cli_opca=:cli_opca, cli_opca_type=:cli_opca_type";
				}
				if(isset($cli_fac_groupe)){
					$sql_up_fil.=",cli_fac_groupe=:cli_fac_groupe";
				}
				$sql_up_fil.=" WHERE cli_id=:filiale;";
				$req_up_fil=$Conn->prepare($sql_up_fil);

				$sql_get_fil_soc="SELECT cso_societe,cso_agence FROM Clients_Societes WHERE cso_client=:filiale ORDER BY cso_societe;";
				$req_get_fil_soc=$Conn->prepare($sql_get_fil_soc);

				$sql_up_fil_soc="UPDATE Clients SET cli_categorie=:cli_categorie, cli_sous_categorie=:cli_sous_categorie";
				if($cli_blackliste!=$d_client["cli_blackliste"]){
					$sql_up_fil_soc.=", cli_blackliste=:cli_blackliste";
				}
				if(isset($cli_fac_groupe)){
					$sql_up_fil_soc.=", cli_fac_groupe=:cli_fac_groupe";
				}
				$sql_up_fil_soc.=" WHERE cli_id=:filiale;";

				foreach($filiales as $filiale){


					$req_up_fil->bindParam(":cli_categorie",$cli_categorie);
					$req_up_fil->bindParam(":cli_sous_categorie",$cli_sous_categorie);
					if($cli_blackliste!=$d_client["cli_blackliste"]){
						$req_up_fil->bindParam(":cli_blackliste",$cli_blackliste);
					}
					if($cli_facture_opca!=$d_client["cli_facture_opca"] OR $cli_opca!=$d_client["cli_opca"]){
						$req_up_fil->bindParam(":cli_facture_opca",$cli_facture_opca);
						$req_up_fil->bindParam(":cli_opca",$cli_opca);
						$req_up_fil->bindParam(":cli_opca_type",$cli_opca_type);
					}
					if(isset($cli_fac_groupe)){
						$req_up_fil->bindParam(":cli_fac_groupe",$cli_fac_groupe);
					}
					$req_up_fil->bindParam(":filiale",$filiale["cli_id"]);
					try{
						$req_up_fil->execute();
					}Catch(Exception $e){
						die();
					}

					$conn_soc_id=0;
					$req_get_fil_soc->bindParam(":filiale",$filiale["cli_id"]);
					$req_get_fil_soc->execute();
					$d_clients_soc=$req_get_fil_soc->fetchAll();
					if(!empty($d_clients_soc)){

						foreach($d_clients_soc as $d_cli_soc){

							if($conn_soc_id!=$d_cli_soc["cso_societe"]){
								$conn_soc_id=$d_cli_soc["cso_societe"];
								$ConnFct=connexion_fct($conn_soc_id);
								$req_up_fil_soc=$ConnFct->prepare($sql_up_fil_soc);
							}

							$req_up_fil_soc->bindParam(":cli_categorie",$cli_categorie);
							$req_up_fil_soc->bindParam(":cli_sous_categorie",$cli_sous_categorie);
							if($cli_blackliste!=$d_client["cli_blackliste"]){
								$req_up_fil_soc->bindParam(":cli_blackliste",$cli_blackliste);
							}
							if(isset($cli_fac_groupe)){
								$req_up_fil_soc->bindParam(":cli_fac_groupe",$cli_fac_groupe);
							}
							$req_up_fil_soc->bindParam(":filiale",$filiale["cli_id"]);
							$req_up_fil_soc->execute();
						}

					}
				}
			}
		}
	}


/*
	if($cli_groupe != $d_client['cli_groupe']){

		if($cli_groupe == 0){
			if($cli_filiale_de == 0){
				$req=$Conn->query("SELECT * FROM Clients_Produits WHERE cpr_client = " . $client);
				$d_client_produits=$req->fetchAll();
			}else{
				$req=$Conn->query("SELECT * FROM Clients_Produits WHERE cpr_client = " . $cli_filiale_de);
				$d_client_produits=$req->fetchAll();
			}

			// AJOUT DE TOUS LES PRODUITS DE LA MM SUR LE CLIENT ACTUEL
			foreach($d_client_produits as $dcp){
				$sql_add="INSERT INTO Clients_Produits (cpr_client,cpr_produit,cpr_intra,cpr_libelle_intra,cpr_ht_intra,cpr_inter,cpr_libelle_inter,cpr_ht_inter,cpr_devis_txt,
				cpr_comment_txt,cpr_alerte_jour,cpr_confirmation_gc,cpr_valide,cpr_cn,cpr_archive)
				VALUES (:cpr_client, :cpr_produit, :cpr_intra, :cpr_libelle, :cpr_ht_intra, :cpr_inter, :cpr_libelle_inter, :cpr_ht_inter,
				:cpr_devis_txt, :cpr_comment_txt, :cpr_alerte_jour, :cpr_confirmation_gc, :cpr_valide, :cpr_cn, 0);";
				$req_add=$Conn->prepare($sql_add);
				$req_add->bindParam(":cpr_intra",$dcp['cpr_intra']);
				$req_add->bindParam(":cpr_libelle",$dcp['cpr_libelle']);
				$req_add->bindParam(":cpr_ht_intra",$dcp['cpr_ht_intra']);
				$req_add->bindParam(":cpr_inter",$dcp['cpr_inter']);
				$req_add->bindParam(":cpr_libelle_inter",$dcp['cpr_libelle_inter']);
				$req_add->bindParam(":cpr_ht_inter",$dcp['cpr_ht_inter']);
				$req_add->bindParam(":cpr_devis_txt",$dcp['cpr_devis_txt']);
				$req_add->bindParam(":cpr_comment_txt",$dcp['cpr_comment_txt']);
				$req_add->bindParam(":cpr_alerte_jour",$dcp['cpr_alerte_jour']);
				$req_add->bindParam(":cpr_confirmation_gc",$dcp['cpr_confirmation_gc']);
				$req_add->bindParam(":cpr_valide",$dcp['cpr_valide']);
				$req_add->bindParam(":cpr_cn",$dcp['cpr_cn']);
				$req_add->bindParam(":cpr_client",$client);
				$req_add->bindParam(":cpr_produit",$dcp['cpr_produit']);
				$req_add->execute();
			}
		}else{
			// AJOUT DE TOUS LES PRODUITS SUR LA MM
			if($cli_filiale_de != 0){
				$req=$Conn->query("SELECT * FROM Clients_Produits WHERE cpr_client = " . $client);
				$d_client_produits=$req->fetchAll();

				// AJOUT DE TOUS LES PRODUITS DE LA MM SUR LE CLIENT ACTUEL
				foreach($d_client_produits as $dcp){

					$req=$Conn->query("SELECT * FROM Clients_Produits WHERE cpr_client = " . $cli_filiale_de);
					$d_client_produit_mm=$req->fetch();
					if(empty($d_client_produit_mm)){
						$sql_add="INSERT INTO Clients_Produits (cpr_client,cpr_produit,cpr_intra,cpr_libelle_intra,cpr_ht_intra,cpr_inter,cpr_libelle_inter,cpr_ht_inter,cpr_devis_txt,
						cpr_comment_txt,cpr_alerte_jour,cpr_confirmation_gc,cpr_valide,cpr_cn,cpr_archive)
						VALUES (:cpr_client, :cpr_produit, :cpr_intra, :cpr_libelle, :cpr_ht_intra, :cpr_inter, :cpr_libelle_inter, :cpr_ht_inter,
						:cpr_devis_txt, :cpr_comment_txt, :cpr_alerte_jour, :cpr_confirmation_gc, :cpr_valide, :cpr_cn, 0);";
						$req_add=$Conn->prepare($sql_add);
						$req_add->bindParam(":cpr_intra",$dcp['cpr_intra']);
						$req_add->bindParam(":cpr_libelle",$dcp['cpr_libelle']);
						$req_add->bindParam(":cpr_ht_intra",$dcp['cpr_ht_intra']);
						$req_add->bindParam(":cpr_inter",$dcp['cpr_inter']);
						$req_add->bindParam(":cpr_libelle_inter",$dcp['cpr_libelle_inter']);
						$req_add->bindParam(":cpr_ht_inter",$dcp['cpr_ht_inter']);
						$req_add->bindParam(":cpr_devis_txt",$dcp['cpr_devis_txt']);
						$req_add->bindParam(":cpr_comment_txt",$dcp['cpr_comment_txt']);
						$req_add->bindParam(":cpr_alerte_jour",$dcp['cpr_alerte_jour']);
						$req_add->bindParam(":cpr_confirmation_gc",$dcp['cpr_confirmation_gc']);
						$req_add->bindParam(":cpr_valide",$dcp['cpr_valide']);
						$req_add->bindParam(":cpr_cn",$dcp['cpr_cn']);
						$req_add->bindParam(":cpr_client",$client);
						$req_add->bindParam(":cpr_produit",$dcp['cpr_produit']);
						$req_add->execute();
					}

				}
			}
		}

	}*/

	if(!empty($erreur)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur
		);
	}elseif(!empty($warning_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Attention!",
			"type" => "warning",
			"message" => $warning_txt
		);
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Succès",
			"type" => "success",
			"message" => "Votre client a été actualisé"
		);
	}
	header("Location: client_voir.php?client=" . $client);
?>
