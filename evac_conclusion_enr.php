<?php 
// ENREGISTREMENT D'UN DEVIS
	
include "includes/controle_acces.inc.php";
include_once("includes/connexion_soc.php");
include_once("includes/connexion.php");
include('modeles/mod_parametre.php');

// FIN DES CONTROLES

// ENREGISTREMENT
if(!empty($_POST['eva_id'])){
	$_POST['eva_conclusion']= str_replace('\n', '<br />', nl2br($_POST['eva_conclusion']));
	$sql_edit="UPDATE Evacuations SET 
eva_conclusion = :eva_conclusion 
 WHERE eva_id = :eva_id;";
$req_edit=$Conn->prepare($sql_edit);
$req_edit->bindValue(":eva_conclusion", $_POST['eva_conclusion']);
$req_edit->bindValue(":eva_id",$_POST['eva_id']);

$req_edit->execute();

$id = $_POST['eva_id'];
}
$_SESSION['message'][] = array( 
	"titre" => "Succès",
	"type" => "success",
	"message" => "Votre conclusion a été enregistrée"
);
Header("Location: evac_voir.php?id=" . $_POST['eva_id']);
die();



