<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";
include('modeles/mod_get_juridique.php');
include('modeles/mod_parametre.php');
include('includes/connexion.php');
include('includes/connexion_soc.php');

// VISUALISATION D'UN DEVIS

$erreur=0;

$eva_id=0;
if(!empty($_GET["id"])){
	$eva_id=intval($_GET["id"]);
}
$_SESSION['retour'] = "evac_voir.php?id=".$_GET['id'];
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
}

// recherche du devis
if($eva_id>0){

	$eva_pdf=false;
	$sql="SELECT * FROM Evacuations WHERE eva_id=" . $eva_id;
	$req=$Conn->query($sql);
	$eva=$req->fetch();
	/*if(!empty($eva)){

		$eva_pdf=file_exists("documents/Evacuations/Evac_" . $acc_societe . "/" . $eva["eva_id"] . ".pdf");

	}else{
		$erreur=1;
	}*/
}

if($erreur==0){

	// ADRESSE RESEAU
	$juridique=get_juridique($eva["eva_societe"],$eva["eva_agence"]);
	/*echo("<pre>");
		print_r($juridique);
	echo("</pre>");
	die();*/

	// CLASSIFICATION PRODUIT
}
$sql="SELECT * FROM Evacuations_Chronologies LEFT JOIN evacuations_chronologies_param ON (evacuations_chronologies_param.ecp_id = Evacuations_Chronologies.ech_chronologie) WHERE ech_evacuation=" . $eva_id . " ORDER BY ecp_position";
$req=$Conn->query($sql);
$chronologies =$req->fetchAll();


$sql="SELECT * FROM Evacuations_Bilans LEFT JOIN evacuations_bilans_param ON (evacuations_bilans_param.ebp_id = Evacuations_Bilans.ebi_bilan) WHERE ebi_evacuation=" . $eva_id. " ORDER BY ebi_position";
$req=$Conn->query($sql);
$remarques =$req->fetchAll();

ob_start(); ?>

<style type="text/css" >

	table{
		width:100%;
		color:#333333;
		font-size:11pt;
		font-family:helvetica;
		line-height:6mm;
		border-collapse:collapse;
	}
	.text-right{
		text-align:right;
	}
	.text-center{
		text-align:center;
	}

	table.border{

	}
	table.border th{
		background-color:#e31936;
		color:#FFFFFF;
		text-align:center;
		/*border:1px solid #FFFFFF;*/
	}
	table.border td{
		/*border:1px solid #7e8082;*/
	}

	h3{
		font-size:12pt;
	}
	.cadre{
		border:1px solid #333333;
	}

	.juridique{
		font-size:7pt;
		line-height:3mm;
		text-align:center;
	}


	h4{
		font-size:12pt;
		margin:0px;

	}
	h5{
		font-size:8pt;
		color:#E31936;
		margin:5px 0px 0px 0px;
	}
	h6{
		font-size:8pt;
		margin:5px 0px 0px 0px;
	}
	.cgv-intro{
		color:#E31936;
		font-size:8pt;
		text-align:justify;
		margin:5px 0px 0px 0px;
	}
	.cgv{
		text-align:justify;
		font-size:7pt;
		margin:5px 0px 0px 0px;
	}


</style>

<page backtop="30mm" backleft="5mm" backright="5mm" backbottom="15mm" >
	<page_header>
		<table>
			<tr>
				<td style="width:30%;" >
		<?php		if(!empty($juridique["logo_1"])){
						echo("<img style='max-width:100%;' src='" . $juridique["logo_1"] . "' />");
					} ?>
				</td>
				<td style="width:40%;" ><h1 class="text-center" >RAPPORT D'EVACUATION</h1></td>
				<td style="width:30%;" class="text-right" >
		<?php		if(!empty($juridique["logo_1"])){
						echo("<img style='max-width:100%;' src='" . $juridique["logo_1"] . "' />");
					} ?>
				</td>
			</tr>
		</table>
    </page_header>

	<page_footer>
		<hr/>
		<table>
			<tr>
				<td style="width:15%;" >
		<?php		if(!empty($juridique["qualite_1"])){
						echo("<img style='max-width:100%;' src='" . $juridique["qualite_1"] . "' />");
					} ?>
				</td>
				<td style="width:70%;" class="juridique" ><?=$juridique["footer"]?></td>
				<td style="width:15%;" class="text-right" >
		<?php		if(!empty($juridique["qualite_2"])){
						echo("<img style='max-width:100%;' src='" . $juridique["qualite_2"] . "' />");
					} ?>
				</td>
			</tr>
		</table>
	</page_footer>

	<table>
		<tr>
			<td style="width:50%;" >
	<?php		echo("<strong>" . $eva["eva_client_code"] . "</strong>");
				echo("<br/> " . $eva["eva_client_nom"]);
				echo("<br/> " . $eva["eva_adr_nom"]);
				echo("<br/> " . $eva["eva_adr_ad1"]);
				if(!empty($eva["eva_adr_ad2"])){
					echo("<br/>" . $eva["eva_adr_ad2"]);
				}
				if(!empty($eva["eva_adr_ad3"])){
					echo("<br/>" . $eva["eva_adr_ad3"]);
				}
				echo("<br/>" . $eva["eva_adr_cp"] . " " . $eva["eva_adr_ville"]);

				?>
			</td>
			<td style="width:50%;" >
	<?php
															if(!empty($eva["eva_contact_nom"])){
																echo("<br/>Responsable Client : " . $eva["eva_contact_nom"]);
															}
															if(!empty($eva["eva_for_nom"])){
																echo("<br/> Exercice réalisé par : " . $eva["eva_for_nom"]);
															}
															if(!empty($eva["eva_com_nom"])){
																echo("<br/> Affaire suivie par : " . $eva["eva_com_nom"]);
															}
															if(!empty($eva["eva_date_exer"])){
																echo("<br/> Date de l'exercice : " . convert_date_txt($eva["eva_date_exer"]));
															}

															?>
			</td>
		</tr>
	</table>

	<table class="border" >
		<thead>
			<tr>
				<th style="width:100%;" >Thème de l'exercice</th>

			</tr>
		</thead>
		<tbody>
				<tr>
					<td style="width:100%;" ><?= nl2br(strip_tags($eva['eva_theme'])) ?></td>
				</tr>
		</tbody>
	</table>
	<table class="border" >
		<thead>
			<tr>
				<th colspan="2">Chronologie</th>
			</tr>
		</thead>
		<tbody>

			<?php foreach($chronologies as $c){ ?>
				<tr>
					<td style="width:20%;" ><?= $c['ecp_libelle'] ?></td>
					<td style="width:80%;" ><?= nl2br(strip_tags($c['ech_valeur'])) ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<table class="border" >
		<thead>
			<tr>
				<th colspan="2">Bilan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
					<td><strong>Remarques</strong> </td>
					<td><strong>Préconisations</strong></td>
				</tr>

			<?php foreach($remarques as $r){
				if(!empty($r['ebi_position'])){?>
				<tr>
					<td style="width:20%;" ><?= nl2br(strip_tags($r['ebi_remarque'])) ?></td>
					<td style="width:80%;" ><?= nl2br(strip_tags($r['ebi_preconisation'])) ?></td>
				</tr>
			<?php }
		}?>
		</tbody>
	</table>
	<table class="border" >
		<thead>
			<tr>
				<th style="width:100%;" >Conclusion</th>

			</tr>
		</thead>
		<tbody>
				<tr>
					<td style="width:100%;" ><?= nl2br(strip_tags($eva['eva_conclusion'])) ?></td>
				</tr>
		</tbody>
	</table>

</page>


<?php

$content=ob_get_clean();
echo $content;
die();
//echo(dirname(__FILE__));
//die();


/* require_once('html2pdf/html2pdf.class.php');

require_once(dirname(__FILE__).'/html2pdf/vendor/autoload.php');
try{
	$pdf=new HTML2PDF('P','A4','fr');
	$pdf->pdf->SetDisplayMode('fullwidth');
	//echo($content);
	$pdf->writeHTML($content);
	if($pdf){
		$pdf->Output(dirname(__FILE__) . '/documents/rapports/Rapport_' . $eva["eva_id"] . '.pdf', 'F');
		header("location : devis_voir.php?devis=" . $eva["eva_id"]);
	}else{
		$pdf->Output($eva["eva_id"] . '.pdf');
	}



}catch(HTML2PDF_exception $e){
	die($e);
}*/

?>
