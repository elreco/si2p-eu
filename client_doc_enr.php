<?php 

// Mise a jour d'un document client

include('includes/connexion.php');
include("modeles/mod_parametre.php");
include('modeles/mod_upload.php');

$erreur_txt="";

if(!empty($_POST)){
	
	
	
	$client=0;
	if(!empty($_POST["client"])){
		$client=intval($_POST["client"]);
	}

	if($client==0 OR empty($_POST["cdo_nom"])){
		$erreur_txt="B - Erreur paramètre!";	
	}
	
	$document=0;
	if(!empty($_POST["document"])){
		$document=intval($_POST["document"]);
	}
	
	// pour nouveau doc le fichier est obligatoire
	if($document==0 AND empty($_FILES["cdo_doc"]["name"])){
		$erreur_txt="A - Erreur paramètre!";	
	}
	
	if(empty($erreur_txt)){
	
		$cdo_pro_famille=0;
		if(!empty($_POST["cdo_pro_famille"])){
			$cdo_pro_famille=intval($_POST["cdo_pro_famille"]);
		}
		$cdo_pro_sous_famille=0;
		if(!empty($_POST["cdo_pro_sous_famille"])){
			$cdo_pro_sous_famille=intval($_POST["cdo_pro_sous_famille"]);
		}
		$cdo_pro_sous_sous_famille=0;
		if(!empty($_POST["cdo_pro_sous_sous_famille"])){
			$cdo_pro_sous_sous_famille=intval($_POST["cdo_pro_sous_sous_famille"]);
		}
		$cdo_type=0;
		if(!empty($_POST["cdo_type"])){
			$cdo_type=intval($_POST["cdo_type"]);
		}
		
		if(!empty($_FILES["cdo_doc"]["name"])){	
			$path = $_FILES['cdo_doc']['name'];
			$cdo_ext = pathinfo($path, PATHINFO_EXTENSION);		
		}
		
		// ON COMMENCE PAR METTRE A JOUR LA TABLE DOC 
		// id intervient dans le nom physique du fichier
		
		// actualisation du fichier
		
			
		if($document>0){
			if(!empty($_FILES["cdo_doc"]["name"])){	
				
				$dossier="clients/client". $client . "/";
				if (!file_exists("documents/" . $dossier)) {
					mkdir("documents/" . $dossier, 0777, true);
					
				}
	
				$extension=array("pdf","jpg");
				$erreur=upload("cdo_doc",$dossier,"doc" . $document,5242880,$extension,1);
				
				if(!empty($erreur)){
					
	
					$erreur_txt="Le fichier n'a pas été chargé. Vérifiez l'extension et la taille du fichier.";
					header("Location: client_voir.php?client=" . $client . "&erreur=" . $erreur_txt . "&tab7");
					die();
				}
			}
			$sql="UPDATE clients_documents SET 
			cdo_nom=:cdo_nom, cdo_type=:cdo_type, cdo_pro_famille=:cdo_pro_famille, cdo_pro_sous_famille=:cdo_pro_sous_famille, cdo_pro_sous_sous_famille = :cdo_pro_sous_sous_famille";
			if(isset($cdo_ext)){
				$sql.=", cdo_ext=:cdo_ext";
			}
			$sql.=" WHERE cdo_id=:document;";
			$req = $Conn->prepare($sql);
			$req->bindParam(':document', $document);
			$req->bindParam(':cdo_type', $cdo_type);
			$req->bindParam(':cdo_nom', $_POST['cdo_nom']);
			if(isset($cdo_ext)){
				$req->bindParam(':cdo_ext', $cdo_ext);
			}
			$req->bindParam(':cdo_pro_famille', $cdo_pro_famille);
			$req->bindParam(':cdo_pro_sous_famille', $cdo_pro_sous_famille);
			$req->bindParam(':cdo_pro_sous_sous_famille', $cdo_pro_sous_sous_famille);
			try{
				$req->execute();
			} catch (Exception $e){
				$erreur_txt='Exception reçue : '. $e->getMessage();
				echo($erreur_txt);
				die();
			}
			
			
		}else{
			$sql="INSERT INTO clients_documents 
			(cdo_client, cdo_nom, cdo_type, cdo_pro_famille, cdo_pro_sous_famille, cdo_pro_sous_sous_famille, cdo_ext) 
			VALUES 
			(:cdo_client, :cdo_nom, :cdo_type, :cdo_pro_famille, :cdo_pro_sous_famille,:cdo_pro_sous_sous_famille, :cdo_ext)";
			$req = $Conn->prepare($sql);
			$req->bindParam(':cdo_client', $client);
			$req->bindParam(':cdo_type', $cdo_type);
			$req->bindParam(':cdo_nom', $_POST['cdo_nom']);
			//$req->bindParam(':cdo_fichier', clean($_POST['cdo_nom'])); 
			$req->bindParam(':cdo_ext', $cdo_ext);
			$req->bindParam(':cdo_pro_famille', $cdo_pro_famille);
			$req->bindParam(':cdo_pro_sous_famille', $cdo_pro_sous_famille);
			$req->bindParam(':cdo_pro_sous_sous_famille', $cdo_pro_sous_sous_famille);
			try{
				$req->execute();
				$document=$Conn->lastInsertId(); 
			} catch (Exception $e) {
				$erreur_txt='Exception reçue : '. $e->getMessage();
				echo($erreur_txt);
				die();
			}
			if(!empty($_FILES["cdo_doc"]["name"])){	
				
				$dossier="clients/client". $client . "/";
				if (!file_exists("documents/" . $dossier)) {
					mkdir("documents/" . $dossier, 0777, true);
					
				}
	
				$extension=array("pdf","jpg");
				$erreur=upload("cdo_doc",$dossier,"doc" . $document,5242880,$extension,1);
				
				if(!empty($erreur)){
					$sql="DELETE FROM clients_documents WHERE cdo_id = " . $document;
					$req = $Conn->prepare($sql);
					$req->execute();
					
					$erreur_txt="Le fichier n'a pas été chargé. Vérifiez l'extension et la taille du fichier.";
					header("Location: client_voir.php?client=" . $client . "&erreur=" . $erreur_txt . "&tab7");
					die();
				}
			}
		}

		
		
	}
}else{
	$erreur_txt="Erreur paramètre!";
}
if(!empty($erreur_txt)){
	header("Location: client_voir.php?client=" . $client . "&erreur=" . $erreur_txt . "&tab7");
}else{
	header("Location: client_voir.php?client=" . $client . "&tab7");
}


?>