<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include "modeles/mod_qualification.php";

$q = array();
if (isset($_GET['id'])) {
    $q = get_qualification($_GET['id']);
} else {
    $q = array();
}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Paramètres</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">

	<form action="qualification_enr.php" method="POST" id="admin-form">
	
<?php 	if (isset($_GET['id'])): ?>
			<input type="hidden" name="qua_id" value="<?= $q['qua_id'] ?>"></input>
<?php 	endif;?>

		<div id="main">
  <?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" class="">
				
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary ">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										
										<div class="content-header">
								<?php		if(isset($_GET['id'])){ ?>
												<h2>Edition d'une <b class="text-primary">qualification</b></h2>
								<?php		}else{ ?>				
												<h2>Ajouter une <b class="text-primary">qualification</b></h2>
								<?php		} ?>	
										</div>

										<div class="col-md-10 col-md-offset-1">
										
									
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label for="qua_libelle" >Libellé :</label>
														<div class="field prepend-icon">
															<input type="text" name="qua_libelle" class="gui-input" id="qua_libelle" placeholder="Libellé" 
													<?php 	if(isset($_GET['id'])): ?>
																value="<?=$q['qua_libelle']?>"
													<?php 	endif; ?>>
															<span class="field-icon">
																<i class="fa fa-book"></i>
															</span>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label for="qua_libelle" >Code :</label>
														<div class="field prepend-icon">
															<input type="text" name="qua_code" class="gui-input" id="qua_code" placeholder="Code" 
													<?php 	if(isset($_GET['id'])): ?>
																value="<?=$q['qua_code']?>"
													<?php 	endif; ?>>
															<span class="field-icon">
																<i class="fa fa-book"></i>
															</span>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label for="qua_duree" >Durée de validité :</label>
														<div class="field prepend-icon">
															<input type="number" name="qua_duree" class="gui-input" id="qua_duree" placeholder="Durée (en mois)" 
													<?php 	if(isset($_GET['id'])): ?>
																value="<?=$q['qua_duree']?>"
													<?php 	endif; ?>>
															<span class="field-icon">
																<i class="fa fa-book"></i>
															</span>
														</div>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label for="qua_libelle" >Option 1 :</label>														
														<input type="text" name="qua_opt_1" class="gui-input" id="qua_opt_1" placeholder="Option 1" 
												<?php 	if(isset($_GET['id'])): ?>
															value="<?=$q['qua_opt_1']?>"
												<?php 	endif; ?> />
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label for="qua_libelle" >Option 2 :</label>														
														<input type="text" name="qua_opt_2" class="gui-input" id="qua_opt_2" placeholder="Option 2" 
												<?php 	if(isset($_GET['id'])): ?>
															value="<?=$q['qua_opt_2']?>"
												<?php 	endif; ?> />
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section">
														<label for="qua_libelle" >Option 3 :</label>														
														<input type="text" name="qua_opt_3" class="gui-input" id="qua_opt_3" placeholder="Option 3" 
												<?php 	if(isset($_GET['id'])): ?>
															value="<?=$q['qua_opt_3']?>"
												<?php 	endif; ?> />
													</div>
												</div>
											</div>
										</div>


									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="qualification_liste.php" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
					<button type="submit" class="btn btn-success btn-sm">
						<i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php	
	include "includes/footer_script.inc.php"; ?>	
	<!-- validation inputs -->
	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<!-- plugin pour les masques formulaires -->

	<script src="vendor/plugins/holder/holder.min.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->
	<script src="assets/js/custom.js"></script>

</body>
</html>
