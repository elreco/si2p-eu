<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_compte.php');
	
	$erreur=0;
	
	$cpt_id=0;
	if(!empty($_POST["cpt_id"])){
		$cpt_id=$_POST["cpt_id"];
	};
	
	$cpt_numero="";
	if(!empty($_POST["cpt_numero"])){
		$cpt_numero=$_POST["cpt_numero"];
	}else{
		$erreur=1;
	};

	$cpt_libelle="";
	if(!empty($_POST["cpt_libelle"])){
		$cpt_libelle=$_POST["cpt_libelle"];
	}else{
		$erreur=1;
	};

	// ENREGISTREMENT DU COMPTE
	
	
	if($erreur==0){
		
	
		if($cpt_id>0){
			
			// MISE A JOUR D'UN COMPTE
			update_compte($cpt_id,$cpt_numero,$cpt_libelle);
					
		}else{
			
			$cpt_id=insert_compte($cpt_numero,$cpt_libelle);
			if($cpt_id==0){
				$erreur=1;
			};
	
		};	
	};

	header('Location: compte_liste.php?erreur=' . $erreur);      
	
?>

