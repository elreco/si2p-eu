<?php

// ENREGISTREMENT D'UN NOUVEAU SUSPECT
// La maj est géré par suspect_mod et suspect_mod_enr

include "includes/controle_acces.inc.php";
include "includes/connexion.php";
include "includes/connexion_soc.php";

$erreur="";
$warning_fac="";
$warning_int="";
$warning_con="";
$warning_cli="";

$suspect=0;
if(!empty($_POST["suspect"])){
	$suspect=intval($_POST["suspect"]);
}

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}

if(!empty($_POST)){
	$sus_categorie=0;
	if(!empty($_POST["sus_categorie"])){
		$sus_categorie=intval($_POST["sus_categorie"]);
	}
	// ON CREER LE SUSPECT
	if($sus_categorie != 3){
		if(empty($_POST['sus_code']) OR empty($_POST['sus_nom'])){
			$erreur="Paramètres absents";
		}

		if(strlen($_POST['sus_code']) > 200 OR strlen($_POST['sus_nom']) > 200){
			$erreur="Vérifiez le nombre de caractères du code et du nom";
		}
	}


	if(empty($erreur)){

		// DONNEE POUR CONTROLE

		// l'utilisateur

		$acc_utilisateur=0;
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}

		// la société
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);
		}

		$sql="SELECT soc_agence FROM Societes WHERE soc_id=" . $acc_societe . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();

		// CONTROLE DU FORM

		$sus_agence=0;
		if($d_societe["soc_agence"]){
			if(!empty($_POST["sus_agence"])){
				$sus_agence=intval($_POST["sus_agence"]);
			}
		}

		// le commercial
		$sus_commercial=0;
		$sus_utilisateur=0;
		if(!empty($_POST["sus_commercial"])){
			$sus_commercial=intval($_POST["sus_commercial"]);
		}
		if($sus_commercial>0){
			$sql="SELECT com_ref_1 FROM Commerciaux WHERE com_id=" . $sus_commercial . " AND com_type=1;";
			$req=$ConnSoc->query($sql);
			$d_commercial=$req->fetch();
			if(!empty($d_commercial)){
				$sus_utilisateur=$d_commercial["com_ref_1"];
			}

		}



		$sus_sous_categorie=0;
		if(!empty($_POST["sus_sous_categorie"])){
			$sus_sous_categorie=intval($_POST["sus_sous_categorie"]);
		}

		// INTERCO
		$sus_interco_soc=0;
		$sus_interco_age=0;
		if($sus_categorie==5){
			if(!empty($_POST["sus_interco_soc"])){
				$sus_interco_soc=intval($_POST["sus_interco_soc"]);
			}
			if(!empty($_POST["sus_interco_age"])){
				$sus_interco_age=intval($_POST["sus_interco_age"]);
			}
		}

		// SIREN / SIRET
		$sus_siren="";
		$sus_siret="";	// on enregistre le siret dans la table suspect pour ne pas le perdre si pas d'adresse de facturation
		$adr_siret="";
		if($sus_categorie!=3){
			if(!empty($_POST["sus_siren"])){
				if(strlen($_POST["sus_siren"])==11){
					$sus_siren=$_POST["sus_siren"];
				}
			}
			if(!empty($_POST["sus_nic"])){
				if(strlen($_POST["sus_nic"])==6){
					$adr_siret=$_POST["sus_nic"];
				}
			}
			/*if(!empty($sus_siren) AND !empty($adr_siret)){
				$sus_siret=$sus_siren . " " . $adr_siret;
			}*/
		}


		// MODE DE REG

		$sus_reg_type=0;
		if(!empty($_POST["sus_reg_type"])){
			$sus_reg_type=intval($_POST["sus_reg_type"]);
		}
		$sus_reg_formule=0;
		$sus_reg_nb_jour=0;
		$sus_reg_fdm=0;
		if(!empty($_POST["reg_formule"])){
			$sus_reg_formule=intval($_POST["reg_formule"]);
		}
		if(!empty($_POST["reg_nb_jour_" . $sus_reg_formule])){
			$sus_reg_nb_jour=intval($_POST["reg_nb_jour_" . $sus_reg_formule]);
		}
		if(!empty($_POST["reg_fdm_" . $sus_reg_formule])){
			$sus_reg_fdm=intval($_POST["reg_fdm_" . $sus_reg_formule]);
		}

		// AUTRE INFO

		$sus_opca_type=0;
		$sus_opca=0;
		$sus_facture_opca=0;
		if($sus_categorie!=4){
			if(!empty($_POST["sus_facture_opca"])){
				$sus_facture_opca=1;
				if(!empty($_POST["sus_opca_type"])){
					$sus_opca_type=intval($_POST["sus_opca_type"]);
					if($sus_opca_type>0){
						$sus_opca=intval($_POST["sus_opca"]);
					}
				}
			}
		}

		$sus_releve=0;
		if(!empty($_POST["sus_releve"]) && $sus_categorie!=3){
			$sus_releve=1;
		}

		$sus_classification=0;
		if(!empty($_POST["sus_classification"])){
			$sus_classification=intval($_POST["sus_classification"]);
		}

		$sus_classification_type=0;
		if(!empty($_POST["sus_classification_type"])){
			$sus_classification_type=intval($_POST["sus_classification_type"]);
		}
		$sus_classification_categorie=0;
		if(!empty($_POST["sus_classification_categorie"])){
			$sus_classification_categorie=intval($_POST["sus_classification_categorie"]);
		}

		$sus_stagiaire_type=0;
		if(!empty($_POST["sus_stagiaire_type"])){
			$sus_stagiaire_type=intval($_POST["sus_stagiaire_type"]);
		}

		$sus_secteur=0;
		if(!empty($_POST["sus_secteur"])){
			$sus_secteur=intval($_POST["sus_secteur"]);
		}

		$sus_sous_secteur=0;
		if(!empty($_POST["sus_sous_secteur"])){
			$sus_sous_secteur=intval($_POST["sus_sous_secteur"]);
		}

		$sus_prescripteur=0;
		$sus_prescripteur_info="";
		if(!empty($_POST["sus_prescripteur"])){
			$sus_prescripteur=intval($_POST["sus_prescripteur"]);
		}

		if(!empty($sus_prescripteur)){
			if(!empty($_POST["sus_prescripteur_info"])){
				$sus_prescripteur_info=$_POST["sus_prescripteur_info"];
			}
		}

		$sus_capital="";
		$sus_soc_type="";
		$sus_immat_lieu="";
		$sus_tel="";

		if($sus_categorie!=3){
			if(!empty($_POST["sus_capital"])){
				$sus_capital=$_POST["sus_capital"];
			}
			if(!empty($_POST["sus_soc_type"])){
				$sus_soc_type=$_POST["sus_soc_type"];
			}
			if(!empty($_POST["sus_immat_lieu"])){
				$sus_immat_lieu=$_POST["sus_immat_lieu"];
			}
			if(!empty($_POST["sus_tel"])){
				$sus_tel=$_POST["sus_tel"];
			}
		}


		// CONTACT

		$add_contact=false;
		$sus_contact=0;

		$sus_con_fct=0;
		$sus_con_fct_nom="";
		$sus_con_titre=0;
		$sus_con_prenom="";
		$sus_con_nom="";
		$sus_con_tel="";
		$sus_con_mail="";

		if(!empty($_POST["con_fonction"])){
			$sus_con_fct=intval($_POST["con_fonction"]);
			if(!empty($sus_con_fct)){
				$add_contact=true;
			}
		}
		if($sus_con_fct==0){
			if(!empty($_POST["con_fonction_nom"])){
				$sus_con_fct_nom=$_POST["con_fonction_nom"];
				$add_contact=true;
			}
		}
		if(!empty($_POST["con_titre"])){
			$sus_con_titre=intval($_POST["con_titre"]);
		}
		if(!empty($_POST["con_nom"])){
			$sus_con_nom=$_POST["con_nom"];
			$add_contact=true;
		}
		if(!empty($_POST["con_prenom"])){
			$sus_con_prenom=$_POST["con_prenom"];
			$add_contact=true;
		}
		if(!empty($_POST["con_tel"])){
			$sus_con_tel=$_POST["con_tel"];
			$add_contact=true;
		}
		if(!empty($_POST["con_mail"])){
			$sus_con_mail=$_POST["con_mail"];
			$add_contact=true;
		}
		$sco_compta=0;
		if(!empty($_POST["con_compta"])){
			$sco_compta=1;
		}
		$sus_stagiaire_naiss=null;
		$sus_sta_naiss_cp=null;
		$sus_sta_naiss_ville=null;
		if($sus_categorie==3){
			if(!empty($_POST["sus_stagiaire_naiss"])){
				$stagiaire_naiss=date_create_from_format('d/m/Y',$_POST["sus_stagiaire_naiss"]);
				if(!is_bool($stagiaire_naiss)){
					$sus_stagiaire_naiss=$stagiaire_naiss->format("Y-m-d");
				}
			}
			if(!empty($_POST["sus_stagiaire_naiss_cp"])){
				$sus_sta_naiss_cp=$_POST["sus_stagiaire_naiss_cp"];
			}
			if(!empty($_POST["sus_stagiaire_naiss_ville"])){
				$sus_sta_naiss_ville=$_POST["sus_stagiaire_naiss_ville"];
			}
			$_POST["sus_code"] = "P-" . preg_replace('/\s+/', '', $sus_con_prenom) . preg_replace('/\s+/', '', $sus_con_nom) . str_replace('/', '', $stagiaire_naiss->format("d/m/Y"));
			$_POST["sus_nom"] = $sus_con_prenom . " " . $sus_con_nom;
		}

		// TRANSFERT EN PROSPECT

		$prospect=0;
		if(!empty($_POST["prospect"])){
			$prospect=1;
		}

		/*****************************
				ENREGISTREMENT
		******************************/

		// FICHE SUSPECT

		$sql_suspect="INSERT INTO Suspects (sus_code,sus_nom,sus_agence,sus_commercial,sus_utilisateur,sus_categorie,sus_sous_categorie
		,sus_siren,sus_reg_type,sus_reg_formule,sus_reg_nb_jour,sus_reg_fdm,sus_facture_opca,sus_opca_type,sus_opca,sus_releve, sus_ident_tva, sus_classification,
		sus_classification_type, sus_classification_categorie, sus_stagiaire_type, sus_stagiaire_naiss,sus_sta_naiss_cp,sus_sta_naiss_ville, sus_ape, sus_prescripteur,sus_prescripteur_info,
		sus_capital,sus_soc_type,sus_immat_lieu,sus_date_creation,sus_uti_creation,sus_tel)
		VALUES (:sus_code, :sus_nom, :sus_agence, :sus_commercial, :sus_utilisateur, :sus_categorie, :sus_sous_categorie
		, :sus_siren, :sus_reg_type, :sus_reg_formule, :sus_reg_nb_jour, :sus_reg_fdm, :sus_facture_opca, :sus_opca_type, :sus_opca, :sus_releve, :sus_ident_tva, :sus_classification,
		:sus_classification_type, :sus_classification_categorie, :sus_stagiaire_type, :sus_stagiaire_naiss,:sus_sta_naiss_cp,:sus_sta_naiss_ville, :sus_ape, :sus_prescripteur,:sus_prescripteur_info,
		:sus_capital, :sus_soc_type, :sus_immat_lieu, NOW(), :sus_uti_creation,:sus_tel);";
		$sql = $sql_suspect;
		$req=$ConnSoc->prepare($sql);
		// DU SUSPECT
		$req->bindParam(":sus_code",$_POST["sus_code"]);
		$req->bindParam(":sus_nom",$_POST["sus_nom"]);
		$req->bindParam(":sus_agence",$sus_agence);
		$req->bindParam(":sus_commercial",$sus_commercial);
		$req->bindParam(":sus_utilisateur",$sus_utilisateur);
		$req->bindParam(":sus_categorie",$sus_categorie);
		$req->bindParam(":sus_sous_categorie",$sus_sous_categorie);
		$req->bindParam(":sus_siren",$sus_siren);
		//$req->bindParam(":sus_siret",$sus_siret);
		$req->bindParam(":sus_reg_type",$sus_reg_type);
		$req->bindParam(":sus_reg_formule",$sus_reg_formule);
		$req->bindParam(":sus_reg_nb_jour",$sus_reg_nb_jour);
		$req->bindParam(":sus_reg_fdm",$sus_reg_fdm);
		$req->bindParam(":sus_facture_opca",$sus_facture_opca);
		$req->bindParam(":sus_opca_type",$sus_opca_type);
		$req->bindParam(":sus_opca",$sus_opca);
		$req->bindParam(":sus_releve",$sus_releve);
		$req->bindParam(":sus_ident_tva",$_POST["sus_ident_tva"]);
		$req->bindParam(":sus_classification",$sus_classification);
		$req->bindParam(":sus_classification_type",$sus_classification_type);
		$req->bindParam(":sus_classification_categorie",$sus_classification_categorie);
		$req->bindParam(":sus_stagiaire_type",$sus_stagiaire_type);
		$req->bindParam(":sus_stagiaire_naiss",$sus_stagiaire_naiss);
		$req->bindParam(":sus_sta_naiss_cp",$sus_sta_naiss_cp);
		$req->bindParam(":sus_sta_naiss_ville",$sus_sta_naiss_ville);
		$req->bindParam(":sus_ape",$_POST["sus_ape"]);
		$req->bindParam(":sus_prescripteur",$sus_prescripteur);
		$req->bindParam(":sus_prescripteur_info",$sus_prescripteur_info);
		$req->bindParam(":sus_capital",$sus_capital);
		$req->bindParam(":sus_soc_type",$sus_soc_type);
		$req->bindParam(":sus_immat_lieu",$sus_immat_lieu);
		$req->bindParam(":sus_tel",$sus_tel);
		$req->bindParam(":sus_uti_creation",$acc_utilisateur);
		try{
			$req->execute();
			$suspect=$ConnSoc->lastInsertId();
		}Catch(Exception $e){
			$erreur=$e->getMessage();
		}
	}

	if(empty($erreur)){

		// ADRESSSE

		$adresse_int=0;
		$adresse_fac=0;


		// donnee a memorise sur la fiche suspect

		$sus_adresse=0;
		$sus_adr_nom="";
		$sus_adr_service="";
		$sus_adr_ad1="";
		$sus_adr_ad2="";
		$sus_adr_ad3="";
		$sus_adr_cp="";
		$sus_adr_ville="";
		$sus_adr_geo=1;


			// ADRESSE D'INTERVENTION

		if(!empty($_POST["adr_cp1"]) AND !empty($_POST["adr_ville1"]) AND $sus_categorie!=3){

			$adr_geo=1;
			if(!empty($_POST["adr_geo1"])){
				$adr_geo=intval($_POST["adr_geo1"]);
			}

			$warning_int="";

			$sql="INSERT INTO Suspects_Adresses (sad_ref,sad_ref_id,sad_type,sad_nom,sad_service,sad_ad1,sad_ad2,sad_ad3,sad_cp,sad_ville,sad_defaut,sad_libelle,sad_geo)
			VALUES (1, :suspect, 1, :sad_nom, :sad_service, :sad_ad1, :sad_ad2, :sad_ad3, :sad_cp, :sad_ville, 1, :sad_libelle, :sad_geo);";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":suspect",$suspect);
			$req->bindParam(":sad_nom",$_POST["adr_nom1"]);
			$req->bindParam(":sad_service",$_POST["adr_service1"]);
			$req->bindParam(":sad_ad1",$_POST["adr_ad11"]);
			$req->bindParam(":sad_ad2",$_POST["adr_ad21"]);
			$req->bindParam(":sad_ad3",$_POST["adr_ad31"]);
			$req->bindParam(":sad_cp",$_POST["adr_cp1"]);
			$req->bindParam(":sad_ville",$_POST["adr_ville1"]);
			$req->bindParam(":sad_libelle",$_POST["adr_libelle1"]);
			$req->bindParam(":sad_geo",$adr_geo);
			try{
				$req->execute();
				$adresse_int=$ConnSoc->lastInsertId();
			}catch(Exception $e){
				$warning_int="Adresse d'intervention non enregistrée<br/>"; //. $e->getMessage();
			}

			if(empty($warning_int) AND $sus_categorie!=3){
				$sus_adresse=$adresse_int;
				$sus_adr_nom=$_POST["adr_nom1"];
				$sus_adr_service=$_POST["adr_service1"];
				$sus_adr_ad1=$_POST["adr_ad11"];
				$sus_adr_ad2=$_POST["adr_ad21"];
				$sus_adr_ad3=$_POST["adr_ad31"];
				$sus_adr_cp=$_POST["adr_cp1"];
				$sus_adr_ville=$_POST["adr_ville1"];
			}
		}

		// ADRESSE DE FACTURATION

		IF(!empty($_POST["adr_cp2"]) AND !empty($_POST["adr_ville2"])){

			$adr_geo2=1;
			if(!empty($_POST["adr_geo2"])){
				$adr_geo2=intval($_POST["adr_geo2"]);
			}

			$sql="INSERT INTO Suspects_Adresses (sad_ref,sad_ref_id,sad_type,sad_nom,sad_service,sad_ad1,sad_ad2,sad_ad3,sad_cp,sad_ville,sad_defaut,sad_libelle,
			sad_geo,sad_siret)
			VALUES (1, :suspect, 2, :sad_nom, :sad_service, :sad_ad1, :sad_ad2, :sad_ad3, :sad_cp, :sad_ville, 1, :sad_libelle,
			:sad_geo, :sad_siret);";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":suspect",$suspect);
			$req->bindParam(":sad_nom",$_POST["adr_nom2"]);
			$req->bindParam(":sad_service",$_POST["adr_service2"]);
			$req->bindParam(":sad_ad1",$_POST["adr_ad12"]);
			$req->bindParam(":sad_ad2",$_POST["adr_ad22"]);
			$req->bindParam(":sad_ad3",$_POST["adr_ad32"]);
			$req->bindParam(":sad_cp",$_POST["adr_cp2"]);
			$req->bindParam(":sad_ville",$_POST["adr_ville2"]);
			$req->bindParam(":sad_libelle",$_POST["adr_libelle2"]);
			$req->bindParam(":sad_geo",$adr_geo2);
			$req->bindParam(":sad_siret",$adr_siret);
			try{
				$req->execute();
				$adresse_fac=$ConnSoc->lastInsertId();
			}catch(Exception $e){
				$warning_fac="Adresse de facturation non enregistrée."; //. $e->getMessage();
			}

			if(empty($warning_fac) AND $sus_categorie==3){
				$sus_adresse=$adresse_fac;
				$sus_adr_nom=$_POST["adr_nom2"];
				$sus_adr_service=$_POST["adr_service2"];
				$sus_adr_ad1=$_POST["adr_ad12"];
				$sus_adr_ad2=$_POST["adr_ad22"];
				$sus_adr_ad3=$_POST["adr_ad32"];
				$sus_adr_cp=$_POST["adr_cp2"];
				$sus_adr_ville=$_POST["adr_ville2"];
			}

		}


		// CONTACT

		$sus_contact=0;
		if($add_contact){
			$sql="INSERT INTO Suspects_Contacts (sco_ref,sco_ref_id,sco_fonction,sco_fonction_nom,sco_titre,sco_nom,sco_prenom,sco_tel,sco_portable,sco_fax,
			sco_mail,sco_compta)
			VALUES (1, :suspect, :sco_fonction, :sco_fonction_nom, :sco_titre, :sco_nom, :sco_prenom, :sco_tel, :sco_portable, :sco_fax,
			:sco_mail, :sco_compta);";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":suspect",$suspect);
			$req->bindParam(":sco_fonction",$sus_con_fct);
			$req->bindParam(":sco_fonction_nom",$sus_con_fct_nom);
			$req->bindParam(":sco_titre",$sus_con_titre);
			$req->bindParam(":sco_nom",$sus_con_nom);
			$req->bindParam(":sco_prenom",$sus_con_prenom);
			$req->bindParam(":sco_tel",$sus_con_tel);
			$req->bindParam(":sco_portable",$_POST["con_portable"]);
			$req->bindParam(":sco_fax",$_POST["con_fax"]);
			$req->bindParam(":sco_mail",$sus_con_mail);
			$req->bindParam(":sco_compta",$sco_compta);
			try{
				$req->execute();
				$sus_contact=$ConnSoc->lastInsertId();
			}catch(Exception $e){
				$warning_con="Contact non enregistré"; //. $e->getMessage();
			}

			if(empty($warning_con) AND !empty($sus_contact)){

				$req = $ConnSoc->prepare("INSERT INTO Suspects_Adresses_Contacts (sac_adresse, sac_contact, sac_defaut ) VALUES (:sac_adresse, :sac_contact, 1);");

				// LE CONTACT EST APR DEFAUT ASSOCIE AUX ADRESSES
				if(!empty($adresse_int)){
					$req->bindParam("sac_adresse",$adresse_int);
					$req->bindParam("sac_contact",$sus_contact);
					$req->execute();
				}
				if(!empty($adresse_fac)){
					$req->bindParam("sac_adresse",$adresse_fac);
					$req->bindParam("sac_contact",$sus_contact);
					$req->execute();
				}
			}
		}

		// MAJ DU SUSPECT AVEC DONNEE ADRESSE ET CONTACT


		if($sus_contact>0 OR $sus_adresse>0){

			$sql="UPDATE Suspects SET sus_adresse=:sus_adresse, sus_adr_nom=:sus_adr_nom, sus_adr_service=:sus_adr_service, sus_adr_ad1=:sus_adr_ad1, sus_adr_ad2=:sus_adr_ad2,sus_adr_ad3=:sus_adr_ad3
			,sus_adr_cp=:sus_adr_cp, sus_adr_ville=:sus_adr_ville,sus_contact=:sus_contact, sus_con_prenom=:sus_con_prenom, sus_con_nom=:sus_con_nom, sus_con_fct=:sus_con_fct, sus_con_fct_nom=:sus_con_fct_nom
			,sus_con_tel=:sus_con_tel, sus_con_mail=:sus_con_mail";
			$sql.=" WHERE sus_id=:suspect;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":sus_adresse",$sus_adresse);
			$req->bindParam(":sus_adr_nom",$sus_adr_nom);
			$req->bindParam(":sus_adr_service",$sus_adr_service);
			$req->bindParam(":sus_adr_ad1",$sus_adr_ad1);
			$req->bindParam(":sus_adr_ad2",$sus_adr_ad2);
			$req->bindParam(":sus_adr_ad3",$sus_adr_ad3);
			$req->bindParam(":sus_adr_cp",$sus_adr_cp);
			$req->bindParam(":sus_adr_ville",$sus_adr_ville);
			$req->bindParam(":sus_contact",$sus_contact);
			$req->bindParam(":sus_con_prenom",$sus_con_prenom);
			$req->bindParam(":sus_con_nom",$sus_con_nom);
			$req->bindParam(":sus_con_fct",$sus_con_fct);
			$req->bindParam(":sus_con_fct_nom",$sus_con_fct_nom);
			$req->bindParam(":sus_con_tel",$sus_con_tel);
			$req->bindParam(":sus_con_mail",$sus_con_mail);
			$req->bindParam(":suspect",$suspect);
			try{
				$req->execute();
			}catch(Exception $e){
				$warning_cli="Adresse et contact par défaut non enregistrés"; // . $e->getMessage();
			}
		}
	}

	if(empty($erreur) AND empty($warning_int) AND empty($warning_fac) AND empty($warning_con) AND empty($warning_cli) AND !empty($prospect)){

		// L'UTILISATEUR A DEMANDE DE PASSER LE SUSPECT DIRECTEMENT EN PROSPECT

		$url="suspect_transfert.php?id=" . $suspect;
		if(!empty($_SESSION['acces']['acc_droits'][36])){
			if(!empty($_POST["option_verif"])){
				$url.="&option_verif=1";
			}
		}
		header("location : " . $url);
		die();
	}
}else{
	$erreur="Paramètres absents";
}

if(!empty($erreur)){

	$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur
		);
		header("location : " . $_SESSION["retour"]);
		die();

}else{

	$warning_txt="";

	if(!empty($warning_int)){
		$warning_txt="<li>" . $warning_int . "</li>";
	}
	if(!empty($warning_fac)){
		$warning_txt.="<li>" . $warning_fac . "</li>";
	}
	if(!empty($warning_con)){
		$warning_txt.="<li>" . $warning_con . "</li>";
	}
	if(!empty($warning_cli)){
		$warning_txt.="<li>" . $warning_cli . "</li>";
	}
	if(!empty($warning_txt)){

		$warning="Enregistrement partiel de votre fiche en tant que suspect.";
		$warning.="<ul>";
			$warning.=$warning_txt;
		$warning.="</ul>";

		$_SESSION['message'] = array(
			"aff" => "modal",
			"titre" => "Enregistrement incomplet !",
			"type" => "warning",
			"message" => $warning
		);
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Enregistrement terminé !",
			"type" => "success",
			"message" => "Votre fiche a bien été enregistrée en tant que <b>suspect</b>."
		);
	}
	header("location : suspect_voir.php?suspect=" . $suspect);
	die();
}
