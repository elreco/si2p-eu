<?php 
include "includes/controle_acces.inc.php";
include('includes/connexion.php');

$req = $Conn->prepare("SELECT * FROM fournisseurs_societes WHERE fso_fournisseur = " . $_GET['fournisseur'] . " AND fso_societe =" . $_POST['societe'] . " AND fso_agence = " . $_POST['agence']);

$req->execute();

$fournisseurs_societes = $req->fetch();

if(isset($_POST['submitajout'])){ // ajout
	if(!empty($_POST['societe'])){


		if(empty($fournisseurs_societes)){

			$req = $Conn->prepare("INSERT INTO fournisseurs_societes (fso_fournisseur, fso_societe, fso_agence) VALUES (:fso_fournisseur, :fso_societe, :fso_agence)");
			$req->bindParam(':fso_fournisseur', $_GET['fournisseur']);
			$req->bindParam(':fso_societe', $_POST['societe']);
			$req->bindParam(':fso_agence', $_POST['agence']);
			$req->execute();

		}
		$req = $Conn->prepare("SELECT * FROM societes_departements WHERE sde_societe = " . $_POST['societe'] . " AND sde_agence =" . $_POST['agence']);
		$req->execute();
		$societe_departements = $req->fetchAll();

		foreach($societe_departements as $s){

			$req = $Conn->prepare("SELECT * FROM fournisseurs_interventions WHERE fin_fournisseur = " . $_GET['fournisseur'] . " AND fin_departement =" . $s['sde_departement']);
			$req->execute();
			$sde_existe = $req->fetch();
			if(empty($sde_existe)){
				$req = $Conn->prepare("INSERT INTO fournisseurs_interventions (fin_fournisseur, fin_departement) VALUES (:fin_fournisseur, :fin_departement)");
				$req->bindParam(':fin_fournisseur', $_GET['fournisseur']);
				$req->bindParam(':fin_departement', $s['sde_departement']);
				$req->execute();
			}

		}
		Header("Location: fournisseur_voir.php?fournisseur=" . $_GET['fournisseur'] . "&tab=3&succes=10");
	}else{
		Header("Location: fournisseur_voir.php?fournisseur=" . $_GET['fournisseur'] . "&tab=3&error=1");
	}
}elseif(isset($_POST['submitretire'])){

	$req = $Conn->prepare("DELETE FROM fournisseurs_societes WHERE fso_fournisseur = " . $_GET['fournisseur'] . " AND fso_societe =" . $_POST['societe'] . " AND fso_agence = " . $_POST['agence']);

	$req->execute();

	$req = $Conn->prepare("SELECT * FROM societes_departements WHERE sde_societe = " . $_POST['societe'] . " AND sde_agence =" . $_POST['agence']);
	$req->execute();
	$societe_departements = $req->fetchAll();

	foreach($societe_departements as $s){

		$req = $Conn->prepare("DELETE FROM fournisseurs_interventions WHERE fin_fournisseur = " . $_GET['fournisseur'] . " AND fin_departement =" . $s['sde_departement']);

	$req->execute();

	}
Header("Location: fournisseur_voir.php?fournisseur=" . $_GET['fournisseur'] . "&tab=3&succes=11");
}
?>