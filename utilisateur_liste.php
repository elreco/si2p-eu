<?php
		$menu_actif = "3-1";
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";

	include "modeles/mod_get_utilisateur_societes.php";
	/*include "modeles/mod_societe.php";
	include "modeles/mod_agence.php";
	include "modeles/mod_profil.php";
	//include "modeles/mod_utilisateur.php";
	include "modeles/mod_droit.php";
	include "modeles/mod_erreur.php";*/

	$_SESSION['retour'] = "utilisateur_liste.php";

	if($_SESSION["acces"]["acc_droits"]["22"]!=1){
		header("location : deconnect.php");
		die();
	}

	if(empty($_SESSION['acces']['acc_ref_id'])){
		echo("Impossible d'afficher la page");
		die();
	}

	$user_connect=$_SESSION['acces']['acc_ref_id'];

	$filt_societe=0;
	$filt_agence=0;
	$filt_profil=0;
	$filt_archive=false;
	$filt_nom="";
	$mil="";
	if(isset($_POST["filt_societe"])){

		$filt_societe=$_POST["filt_societe"];
		$_SESSION['filt_societe']=$filt_societe;

		$filt_agence=$_POST["filt_agence"];
		$_SESSION['filt_agence']=$filt_agence;

		$filt_profil=0;
		if(!empty($_POST["filt_profil"])){
			$filt_profil=$_POST["filt_profil"];
		}

		$_SESSION['filt_profil']=$filt_profil;

		$filt_nom=$_POST["filt_nom"];
		$_SESSION['filt_nom']=$filt_nom;

		if(isset($_POST["filt_archive"])){
			if($_POST["filt_archive"]=="on"){
				$filt_archive=true;
			}
		}
		$_SESSION['filt_archive']=$filt_archive;

	}

	if(!empty($_SESSION["filt_societe"])){
		$filt_societe=$_SESSION['filt_societe'];
		$mil.=" AND uti_societe = " . $filt_societe;
	}
	if(!empty($_SESSION["filt_agence"])){
		$filt_agence=$_SESSION['filt_agence'];
		$mil.=" AND uti_agence = " . $filt_agence;
	}
	if(!empty($_SESSION["filt_profil"])){
		$filt_profil=$_SESSION['filt_profil'];
		$mil.=" AND uti_profil = " . $filt_profil;
	}
	if(!empty($_SESSION["filt_nom"])){
		$filt_nom=$_SESSION['filt_nom'];
		$mil.=" AND uti_nom LIKE '%" . $filt_nom . "%'";
	}
	if(!empty($_SESSION["filt_archive"])){
		$filt_archive=$_SESSION['filt_archive'];
		$mil.=" AND uti_archive = " . $filt_archive;
	}else{
		$mil.=" AND uti_archive = 0";
	}

	$mil.= " AND uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'];
	if($_SESSION['acces']["acc_profil"]==10){
		// cas particulier de RE
		/* ils ont accès à GFC GC pour la gestions de GC mais seul le RE des GC doit avoir accès a la variable du RA GC
		*/
		if($user_connect==3){
			$mil.= " AND uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'];
		}else{
			$mil.= " AND (uti_id = " . $user_connect . " OR NOT uti_societe=4)";
		}
	}

	$sql="SELECT DISTINCT maj_id,maj_version,uti_id,uti_version_tab, uti_nom,uti_prenom,uti_matricule,uti_tel,uti_mobile,uti_mail
	,soc_nom,age_nom,pro_libelle,uti_profil
	FROM Utilisateurs
	LEFT JOIN Utilisateurs_Societes ON (Utilisateurs.uti_societe=uso_societe AND Utilisateurs.uti_agence=uso_agence)
	LEFT OUTER JOIN Societes ON (Utilisateurs.uti_societe=Societes.soc_id)
	LEFT OUTER JOIN Agences ON (Utilisateurs.uti_agence=Agences.age_id)
	LEFT OUTER JOIN Profils ON (Utilisateurs.uti_profil=Profils.pro_id)
	LEFT JOIN maj ON (maj.maj_id = Utilisateurs.uti_version_tab) ";

	if($mil!=""){
      $sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
    }
    $sql .= " ORDER BY uti_nom,uti_prenom;";
	/*echo($sql);
	die(); */
	$req=$Conn->query($sql);
	$d_utilisateurs=$req->fetchAll();
	/*echo("<pre>");
		print_r($d_utilisateurs);
	echo("</pre>");
	die();*/

	/*echo("AGENCE");
	var_dump($filt_agence);
	die();*/
	// SOCIETES DISPO

	$d_societes=get_utilisateur_societes($user_connect);

	// AGENCES DISPO

	if(!empty($filt_societe)){

		$sql="SELECT age_id,age_nom FROM Agences LEFT JOIN Utilisateurs_Societes ON (Agences.age_id=Utilisateurs_Societes.uso_agence)
		WHERE uso_utilisateur=" . $user_connect . " AND age_societe=" . $filt_societe . " ORDER BY age_nom;";
		$req=$Conn->query($sql);
		$d_agences=$req->fetchAll();


	}

	// reserve RH

	if($_SESSION["acces"]["acc_service"][4]==1){
		$req=$Conn->query("SELECT pro_id,pro_libelle FROM Profils ORDER BY pro_libelle;");
		$d_profils=$req->fetchAll();
	}

	// LA PERSONNE CONNECTE
/*
	$acc_service=0;
	$acc_profil=0;
	$acc_drt_competence=false;
	if($_SESSION['acces']['acc_ref']==1){
		$acc_profil=$_SESSION['acces']["acc_profil"];
		$acc_service=$_SESSION['acces']['acc_service'];
		if(!empty(get_droit_utilisateur(3,$_SESSION['acces']['acc_ref_id']))){
			$acc_drt_competence=true;
		}
	}*/
	// DROITS SUR LE PROGRAMME
/*
	$drt_modif=false;
	if($acc_service==2 OR $acc_service==4 OR $acc_profil==13 OR $acc_profil==14 OR $acc_drt_competence){
		$drt_modif=true;
	}*/


	$_SESSION["retour"]="utilisateur_liste.php";
	$_SESSION["retourUtilisateur"]="utilisateur_liste.php";
	?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="UTF-8">
    <title>Si2P - Utilisateur</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="Si2P">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Theme CSS PAR DEFAUT -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

    <link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css">
	  <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	 <!-- PERSONALISATION DU THEME -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

    <link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="sb-top sb-top-sm no-scroll">

<!-- Start: Main -->
<div id="main">
<?php
    include "includes/header_def.inc.php"; ?>
    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
        <section id="content" class="animated fadeIn">
            <div class="row row-42">
                <div class="col-md-12 text-center">
                    <div class="admin-form theme-primary ">
                        <form action="utilisateur_liste.php" method="post" >
                            <div class="col-md-3">
                                    <select name="filt_societe" id="filt_societe" class="select2 select2-societe" data-acc_drt="1">
                                        <option value="">Sélectionner une société...</option>
								<?php	if(!empty($d_societes)){
											foreach($d_societes as $s){
												if($s["soc_id"]==$filt_societe){
													echo("<option value='" . $s["soc_id"] . "' selected >" . $s["soc_nom"] . "</option>");
												}else{
													echo("<option value='" . $s["soc_id"] . "' >" . $s["soc_nom"] . "</option>");
												}
											}
										} ?>
									</select>

                            </div>
                            <div class="col-md-3">
                                    <select id="filt_agence" name="filt_agence"  class="select2 select2-societe-agence"  >
                                        <option value="0">Sélectionner une agence...</option>
								<?php	if(!empty($d_agences)){
											foreach($d_agences as $a){
												if($a["age_id"]==$filt_agence){
													echo("<option value='" . $a["age_id"] . "' selected >" . $a["age_nom"] . "</option>");
												}else{
													echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
												}
											}
										} ?>
                                    </select>
                            </div>
					<?php	if(isset($d_profils)){ ?>
								<div class="col-md-2">
									<label class="field select">
										<select id="filt_profil" name="filt_profil" >
											<option value="">Sélectionner un profil...</option>
					<?php					if(!empty($d_profils)){
												foreach($d_profils as $p){
													if($p["pro_id"]==$filt_profil){
														echo("<option value='" . $p["pro_id"] . "' selected >" . $p["pro_libelle"] . "</option>");
													}else{
														echo("<option value='" . $p["pro_id"] . "' >" . $p["pro_libelle"] . "</option>");
													}
												}
											} ?>
										</select>
										<i class="arrow simple"></i>
									</label>
								</div>
					<?php	} ?>
                            <div class="col-md-2">
								<div class="field prepend-icon">
									<input type="text" name="filt_nom" id="filt_nom" class="gui-input nom" placeholder="Nom" value="<?=$filt_nom?>" >
									<label for="filt_nom" class="field-icon">
										<i class="fa fa-user"></i>
									</label>
								</div>
                            </div>
							<div class="col-md-1 pt10">
								<label class="option">
									<input type="checkbox" name="filt_archive" value="on" <?php if($filt_archive) echo("checked"); ?> >
									<span class="checkbox"></span> Archivé
								</label>
							</div>
                            <div class="col-md-1">
								<button type="submit" data-toggle="tooltip" data-placement="bottom" title="Filtrer" class="btn btn-primary">
									<i class="fa fa-search"></i>
								</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row-100 pn" id="tableUtiCont">
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="tableUtiFix">
                        <thead id="tableUtiHead">
							<tr class="dark">
						<?php	if($_SESSION["acces"]["acc_service"][4]==1){
									echo("<th>Matricule</th>");
								} ?>
								<th>Nom</th>
								<th>Prénom</th>
								<th>Société</th>
								<th>Agence</th>
						<?php	if($_SESSION["acces"]["acc_service"][4]==1){
									echo("<th>Profil</th>");
								} ?>
								<th>Tél</th>
								<th>Mobile</th>
								<th>Mail</th>
								<th>Version Tab.</th>
								<th class="no-sort">Action(s)</th>
							</tr>
                        </thead>
						<tbody>
			<?php
							if(!empty($d_utilisateurs)){
								foreach($d_utilisateurs as $u){ ?>
									<tr>
							<?php		if($_SESSION["acces"]["acc_service"][4]==1){
											echo("<td>" . $u["uti_matricule"] . "</td>");
										} ?>
										<td><?=$u["uti_nom"]?></td>
										<td><?=$u["uti_prenom"]?></td>
										<td><?=$u["soc_nom"]?></td>
										<td><?=$u["age_nom"]?></td>
							<?php		if($_SESSION["acces"]["acc_service"][4]==1){
											echo("<td>" . $u["pro_libelle"] . "</td>");
										} ?>
										<td><?=$u["uti_tel"]?></td>
										<td><?=$u["uti_mobile"]?></td>
										<td><?=$u["uti_mail"]?></td>
										<td data-order="<?= $u['maj_id'] ?>">
												<?php if(!empty($u['uti_version_tab'])){ ?>

													<?= $u['maj_version'] ?>
												<?php } ?>
										</td>
										<td class="text-center">
										<?php
										/*echo("<pre>");
										var_dump($_SESSION["acces"]["acc_droits"]);
										echo("</pre>");
										die();*/ ?>
									<?php	if($_SESSION["acces"]["acc_droits"][22]==1){ ?>
												<a href="utilisateur_voir.php?utilisateur=<?=$u["uti_id"]?>" class="btn btn-info btn-sm" data-toggle="tooltip"
												   data-placement="bottom" title="Consulter la fiche de cet utilisateur" role="button">
													<span class="fa fa-eye"></span>
												</a>
									<?php	}
											if($_SESSION["acces"]["acc_droits"][21]==1){ ?>
												<a href="utilisateur.php?utilisateur=<?=$u["uti_id"]?>" class="btn btn-warning btn-sm" data-toggle="tooltip"
												   data-placement="bottom" title="Modifier la fiche de cet utilisateur">
													<span class="fa fa-pencil"></span>
												</a>
									<?php 	}
											$acces_rem=false;
											if( ($u["uti_profil"]==3 OR $u["uti_profil"]==15) AND $_SESSION["acces"]["acc_droits"][26]==1){
												// PROFIL AVEC PART VARIABLE (com,RA) et U a le droit rem variable

												if($_SESSION["acces"]["acc_profil"]==3){
													if($u["uti_id"]==$_SESSION['acces']['acc_ref_id']){
														// rem de uti connec
														$acces_rem=true;
													}
												}elseif($u["uti_id"]==$_SESSION['acces']['acc_ref_id']){
													// rem de uti connec
													$acces_rem=true;
												}elseif($_SESSION["acces"]["acc_profil"]==15){
													if($_SESSION["acces"]["acc_service"][1]==1){
														// marion resp agence mais membre du Service COM
														if($u["uti_profil"]==3 OR $u["uti_profil"]==15){
															$acces_rem=true;
														}
													}else{
														if($u["uti_profil"]==3){
															$acces_rem=true;
														}
													}
												}else{
													$acces_rem=true;
												}
											}
											if($acces_rem){ ?>
												<a href="utilisateur_rem.php?utilisateur=<?=$u["uti_id"]?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="bottom" title="Rémunération variable">
													<span class="fa fa-eur"></span>
												</a>
									<?php 	} ?>
										</td>
									</tr>
					<?php		}
							} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </section>
</div>
<!-- End: Main -->

<footer id="content-footer">
    <div class="row">
        <div class="col-xs-3 footer-left"></div>
        <div class="col-xs-6 footer-middle text-center">
<?php		if($_SESSION['acces']['acc_profil']==13){ ?>
				<a href="import/sync_utilisateur.php" class="btn btn-default btn-sm" >
					<i class="fa fa-refresh"></i>
					Importer local
				</a>
				<a href="import/sync_utilisateur.php?srv=1" class="btn btn-default btn-sm" >
					<i class="fa fa-refresh"></i>
					Importer srv
				</a>
				<a href="import/sync_acces.php" class="btn btn-default btn-sm" >
					<i class="fa fa-refresh"></i>
					accès local
				</a>
				<a href="import/sync_acces.php?srv=1" class="btn btn-default btn-sm" >
					<i class="fa fa-refresh"></i>
					accès srv
				</a>
<?php		} ?>
		</div>
        <div class="col-xs-3 footer-right">
<?php		if($_SESSION["acces"]["acc_droits"][21]==1){ ?>
				<a href="utilisateur.php" class="btn btn-success btn-sm" role="button">
					<i class="fa fa-plus"></i>
					Nouvel utilisateur
				</a>
<?php		} ?>
        </div>
    </div>
</footer>
<?php
	include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script src="vendor/plugins/mask/jquery.mask.js"></script>

<script src="assets/js/custom.js"></script>
<script type="text/javascript">

    jQuery(document).ready(function () {

        $(".clickable-row").click(function () {
            window.document.location = $(this).data("href");
        });

        var calcDataTableHeight = function (elt_content, elt_head) {
            return $(elt_content).height() - $(elt_head).height() - 6;
        };
        var tableDefFix = $('#tableUtiFix').dataTable({
            "language": {
                "url": "/vendor/plugins/DataTables/media/js/French.json"
            },
            paging: false,
            searching: false,

            info: false,
            scrollY: calcDataTableHeight("#tableUtiCont", "#tableUtiHead"),
            scrollCollapse: true,
            order: [[1, "asc"], [2, "asc"]],
            columnDefs: [
  				{ targets: 'no-sort', orderable: false }
			]
        });
        $(window).resize(function () {
            var tableDefFixParam = tableDefFix.fnSettings();
            tableDefFixParam.oScroll.sY = calcDataTableHeight("#tableUtiCont", "#tableUtiHead");
            tableDefFix.fnDraw();
        });
        $(window).load(function () {
            setTimeout(function () {
                $(".dataTables_scrollBody").mCustomScrollbar({
                    theme: "dark"
                });
            }, 100);
        });

		/*$("#filt_societe").change(function (){
			actu_select_agence(this,"filt_agence");
		});*/
		if($('#modal-error')){
			$('#modal-error').modal('show')
		};
    });
    (jQuery);

	function actu_select_agence(liste_societe,cible){
		$.ajax({
			type:'POST',
			url: '/ajax/ajax_agence.php',
			data : 'field=' + liste_societe.value,
			dataType: 'json',
			success: function(data)
			{	$('#' + cible).find("option:gt(0)").remove();
				if (data['0'] == undefined){
				}else{
					$.each(data, function(key, value){
						$('#' + cible)
						.append($("<option></option>")
						.attr("value",value["age_id"])
						.text(value["age_nom"]));
					});
				}
			}
		});
	}
</script>
</body>
</html>
