﻿<?php 

session_start(); 

include "includes/connexion.php";

require "modeles/mod_get_passe_validite.php";
require "modeles/mod_get_user_acces.php";

$erreur=1;
if(isset($_SESSION["acces"])){
	if(!empty($_SESSION["acces"]["acc_ref"]) AND !empty($_SESSION["acces"]["acc_ref_id"])){
		$erreur=0;	
		
	}
}


if($erreur==1){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => "Changement de mot de passe impossible" 
	);
	header("location : index.php");
	die();
	
}else{
	

	$sql="SELECT acc_ref_id FROM Acces WHERE acc_ref=:acc_ref AND acc_ref_id=:acc_ref_id AND acc_uti_passe=:acc_uti_passe;";
	$req=$Conn->prepare($sql);
	$req->bindParam("acc_ref",$_SESSION["acces"]["acc_ref"]);
	$req->bindParam("acc_ref_id",$_SESSION["acces"]["acc_ref_id"]);
	$req->bindParam("acc_uti_passe",$_POST["passe_old"]);
	try{
		$req->execute();
		$d_acces=$req->fetch();
	}Catch(Exception $e){
		$erreur=1;	
		//echo($e->getMessage());

	}
	
	if($erreur==0){
		if(!empty($d_acces)){
			
			if($_SESSION["acces"]["acc_ref"]=="1"){
				$validite=get_passe_validite();
				if(is_bool($validite)){
					$erreur=1;	
				}
			}else{
				$timestamp_validite=mktime(23, 59, 59, date("m"), date("d")+90,   date("Y"));
				$validite=date("Y-m-d H:i:s",$timestamp_validite);
			}
			
			if($erreur==0){
				
				try{
				
					$Conn->query("UPDATE Acces SET acc_uti_passe='" . $_POST["passe"] . "', 
					acc_passe_modif='" . $validite . "'
					WHERE acc_ref=" . $_SESSION["acces"]["acc_ref"] . " AND acc_ref_id=". $_SESSION["acces"]["acc_ref_id"] . ";");
				}Catch(Exception $e){
					$erreur=1;

					echo($e->getMessage());
				}
			}
		}else{
			$erreur=1;	
		}
	}
}

if($erreur==0){

	$d_access=get_user_acces($_SESSION["acces"]["acc_ref"],$_SESSION["acces"]["acc_ref_id"]);
	
	print_r($d_access);
	if(is_array($d_access)){
		
		$_SESSION["acces"]=$d_access;
		
		
	}else{
		$erreur=1;

	}
}
if($erreur==0){
	header("location:accueil.php");	
}else{
	$_SESSION['message'][] = array(
		"titre" => "Echec",
		"type" => "danger",
		"message" => "Impossible de changer le mot de passe." 
	);
	header("location:index.php");
}
?>