<?php

 include "includes/controle_acces.inc.php";
 include_once("includes/connexion_soc.php");
 include_once("includes/connexion.php");
 include_once("includes/connexion_fct.php");
 // ECRAN DE MAJ DES DONNEE DATES


 $erreur_txt="";

 $action_id=0;
 if(isset($_GET["action"])){
     if(!empty($_GET["action"])){
         $action_id=intval($_GET["action"]);
     }else{
         $erreur_txt="Formulaire incomplet";
     }
 }else{
     $erreur_txt="Formulaire incomplet";
 }

 $action_client_id=0;
 if(isset($_GET["action_client"])){
     if(!empty($_GET["action_client"])){
         $action_client_id=$_GET["action_client"];
     }
 }

 if(empty($erreur_txt)){

     // sur la personne connecte

     $acc_agence=0;
     if(isset($_SESSION['acces']["acc_agence"])){
         $acc_agence=$_SESSION['acces']["acc_agence"];
     }
     $acc_societe=0;
     if(isset($_SESSION['acces']["acc_societe"])){
         $acc_societe=$_SESSION['acces']["acc_societe"];
    }
    if($conn_soc_id!=$acc_societe){
        $acc_societe=$conn_soc_id;
        $acc_agence=0;
    }

     // info sur l'action

     $max_session=4;

     $sql="SELECT act_multi_session,act_date_h_def_1,act_date_h_def_2,act_agence,act_pro_sous_famille FROM Actions WHERE act_id=:action;";
     $req=$ConnSoc->prepare($sql);
     $req->bindParam(":action",$action_id);
     $req->execute();
     $d_action=$req->fetch();
     if(empty($d_action)){
         $erreur_txt="Impossible de récupérer les données de formation.";
     }else{
        if($d_action["act_pro_sous_famille"]==2 OR $d_action["act_pro_sous_famille"]==3 OR $d_action["act_pro_sous_famille"]==4 OR $d_action["act_pro_sous_famille"]==6 OR $d_action["act_pro_sous_famille"]==13 OR $d_action["act_pro_sous_famille"]==21 OR $d_action["act_pro_sous_famille"]==26){
            $max_session=15;
        }
     }
     // LES DATES

     $sql_ase="SELECT pda_date,pda_categorie,pda_salle,pda_vehicule,pda_demi,pda_id
     ,int_label_1
     ,ase_h_deb,ase_h_fin,ase_id";
     if(!empty($action_client_id)){
         $sql_ase.=",acd_action_client";
     }
     $sql_ase.=" FROM Plannings_Dates INNER JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)
     INNER JOIN Actions_Sessions ON (Plannings_Dates.pda_id=Actions_Sessions.ase_date)";
     if(!empty($action_client_id)){
         $sql_ase.=" LEFT OUTER JOIN Actions_Clients_Dates ON (Plannings_Dates.pda_id=Actions_Clients_Dates.acd_date AND acd_action_client=" . $action_client_id . ")";
     }
     $sql_ase.=" WHERE pda_type=1 AND pda_ref_1=" . $action_id . " ORDER BY pda_date,pda_demi,ase_h_deb,int_label_1;";
     $req=$ConnSoc->query($sql_ase);
     //var_dump($sql);
     $d_action_dates=$req->fetchAll();
     if(empty($d_action_dates)){
        $sql="SELECT pda_id, pda_ref_1 FROM Plannings_Dates WHERE pda_type = 1 AND pda_ref_1=:action;";
        $req=$ConnSoc->prepare($sql);
        $req->bindParam(":action",$action_id);
        $req->execute();
        $d_planning_dates=$req->fetchAll();

        foreach($d_planning_dates as $pda){
            $sql="INSERT INTO actions_sessions (ase_date, ase_action) VALUES (:pda_id,:action);";
            $req=$ConnSoc->prepare($sql);
            $req->bindParam(":action",$action_id);
            $req->bindParam(":pda_id",$pda['pda_id']);
            $req->execute();
        }
        $req=$ConnSoc->query($sql_ase);
        //var_dump($sql);
        $d_action_dates=$req->fetchAll();
        if(empty($d_action_dates)){
            $erreur_txt="Les dates n'ont pas pu être chargées.";
        }

     }
 }

 if(!empty($erreur_txt)){
     if(!empty($action_id)){
         $_SESSION['message'][] = array(
             "titre" => "Erreur",
             "type" => "danger",
             "message" => $erreur_txt
         );
         var_dump($erreur_txt);
         //header("location: action_voir.php?action=" . $action_id);
         die();
     }else{
         echo($erreur_txt);
         die();
     }
 }

 // AUTRE DONNEE POUR MAJ

 // CLIENT SELECTIONNE

 if(!empty($action_client_id)){

     $sql="SELECT cli_code,cli_nom FROM Clients,Actions_Clients WHERE cli_id=acl_client AND acl_id=:action_client_id;";
     $req=$ConnSoc->prepare($sql);
     $req->bindParam(":action_client_id",$action_client_id);
     $req->execute();
     $d_action_client=$req->fetch();
 }

 // INTERCO
 $display_msg = false;
 $sql="SELECT * FROM Actions_Clients_Intercos WHERE aci_action_client=:action_client";
 $req=$ConnSoc->prepare($sql);
 $req->bindParam(":action_client",$action_client_id);
 $req->execute();
 $d_actions_clients_intercos=$req->fetch();

 if(!empty($d_actions_clients_intercos)){
    $display_msg = true;

    // Selection des actions
    $sql="SELECT soc_id, soc_nom FROM Societes WHERE soc_archive = 0 AND soc_id = " . $d_actions_clients_intercos['aci_action_client_interco_soc'];
    $req=$Conn->prepare($sql);
    $req->execute();
    $d_societe = $req->fetch();

    $d_actions_clients_intercos_ok_array[] = $d_societe['soc_nom'];
 }
 // Selection des actions
 $sql="SELECT soc_id, soc_nom FROM Societes WHERE soc_archive = 0";
 $req=$Conn->prepare($sql);
 $req->execute();
 $d_societes = $req->fetchAll();
 // Suppression interco
 foreach($d_societes as $s){
     $ConnFct=connexion_fct($s['soc_id']);
     $sql="SELECT * FROM Actions_Clients_Intercos WHERE aci_action_interco=:aci_action_client_interco AND aci_action_client_interco_soc = :soc;";
     $req=$ConnFct->prepare($sql);
     $req->bindParam(":aci_action_client_interco",$action_id);
     $req->bindParam(":soc",$_SESSION['acces']['acc_societe']);
     $req->execute();
     $d_actions_clients_intercos_ok = $req->fetch();
     if(!empty($d_actions_clients_intercos_ok)){
        $display_msg = true;
        $d_actions_clients_intercos_ok_array[] = $s['soc_nom'];
     }
 }

 ?>
 <!DOCTYPE html>
 <html>
     <head>
         <meta charset="utf-8">
         <title>SI2P - Orion</title>
         <meta name="keywords" content=""/>
         <meta name="description" content="">
         <meta name="author" content="">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <!-- CSS THEME -->
         <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
         <link href="assets/admin-tools/admin-forms/css/admin-forms.css" rel="stylesheet" type="text/css">

         <!-- CSS PLUGINS -->
         <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
         <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

         <!-- CSS Si2P -->
         <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

         <!-- Favicon -->
         <link rel="shortcut icon" href="assets/img/favicon.png">

         <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
         <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
         <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
         <![endif]-->
     </head>
     <body class="sb-top sb-top-sm ">

         <form method="post" action="action_date_enr.php" >
             <div>
                 <input type="hidden" name="action" value="<?=$action_id?>" />
                 <input type="hidden" name="action_client" value="<?=$action_client_id?>" />
                 <input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
             </div>
             <div id="main">
     <?php		include "includes/header_def.inc.php"; ?>
                 <section id="content_wrapper">
                     <section id="content" class="animated">

                         <div class="row" >

                             <div class="col-md-12">

                                 <div class="admin-form theme-primary ">

                                     <div class="panel heading-border panel-primary">

                                         <div class="panel-body bg-light">

                                             <div class="content-header mtn mbn">
                                     <?php		echo("<h2>Action N°". $action_id . " - <b class='text-primary' >Modification des horaires</b></h2>"); ?>
                                             </div>
                                             <div class="row" >

                                                 <table class="table table-striped">
                                                     <thead>
                                                         <tr>
                                                              <th rowspan="2" >&nbsp;</th>
                                                             <th rowspan="2" colspan="2" >Date</th>
                                                             <th rowspan="2" >Intervenant</th>
                                                     <?php	if(isset($d_action_client)){ ?>
                                                                 <th rowspan="2" >Participation de<br/><?=$d_action_client["cli_code"]?></th>
                                                     <?php	} ?>
                                                             <th rowspan="2" >Valeur par défaut</th>
                                                             <th colspan="3" >Type de formation</th>
                                                             <th rowspan="2" >Salle</th>
                                                             <th rowspan="2" >Véhicule</th>
                                                             <th colspan="2" >Horaires</th>
                                                         </tr>
                                                         <tr>
                                                             <th>Th</th>
                                                             <th>Pr</th>
                                                             <th>Ev</th>
                                                             <th>
                                                                 <div class="row" >
                                                                     <div class="col-md-5" >Début</div>
                                                                     <div class="col-md-5" >Fin</div>
                                                                     <div class="col-md-2" >&nbsp;</div>
                                                                 </div>
                                                             </th>
                                                             <th>&nbsp;</th>
                                                         </tr>
                                                     </thead>
                                                     <tbody>
                                                 <?php	$date_id=0;
                                                         /*echo("<pre>");
                                                             print_r($d_action_dates);
                                                         echo("</pre>");*/
                                                         foreach($d_action_dates as $date){

                                                             // on passe sur une nouvelle date
                                                             if($date_id!=$date["pda_id"]){

                                                                 // on termine "d'écrire" la ligne précédente
                                                                 if(!empty($date_id)){
                                                                     echo("</td>");
                                                                         echo("<td style='vertical-align:top;' >");
                                                                         //if($d_action["act_multi_session"]){ ?>
                                                                             <button type='button' class="btn-add btn btn-sm btn-success" id="btn_add_<?=$date_id?>" data-date_id="<?=$date_id?>" data-toggle="tooltip" title="Ajouter une session" <?php if($session_num==4) echo("disabled") ?> >
                                                                                 <i class="fa fa-plus" ></i>
                                                                             </button>
                                                 <?php					//}else{
                                                                         //	echo("&nbsp;");
                                                                         //}
                                                                     echo("</tr>");
                                                                 }
                                                                 // on fixe les valeurs pour la ligne en cours

                                                                 $session_num=0;
                                                                 $date_id=$date["pda_id"];
                                                                 $demi_lid="matin";
                                                                 if($date["pda_demi"]==2){
                                                                     $demi_lid="après-midi";
                                                                 }

                                                                 $pda_date="";
                                                                 if(!empty($date["pda_date"])){
                                                                     $DT_periode=date_create_from_format('Y-m-d',$date["pda_date"]);
                                                                     if(!is_bool($DT_periode)){
                                                                         $pda_date=$DT_periode->format("d/m/Y");
                                                                     }
                                                                 }


                                                                 // on recupère les ressources disponible sur la date

                                                                 // VEHICULE

                                                                 // attention vehicules et dates ne sont pas sur la même base
                                                                 $veh_pas_dispo="";
                                                                 $sql="SELECT DISTINCT pda_vehicule FROM Plannings_Dates WHERE pda_date='" . $date["pda_date"] . "' AND pda_demi=" . $date["pda_demi"] . " AND NOT pda_id=" . $date_id . " AND NOT pda_archive;";
                                                                 $req=$ConnSoc->query($sql);
                                                                 $d_veh_non_dispo=$req->fetchAll(PDO::FETCH_ASSOC);
                                                                 if(!empty($d_veh_non_dispo)){
                                                                     foreach($d_veh_non_dispo as $vpl){
                                                                         $veh_pas_dispo.=$vpl["pda_vehicule"] . ",";
                                                                     }
                                                                     $veh_pas_dispo=substr($veh_pas_dispo,0,-1);
                                                                 }


                                                                 $sql="SELECT veh_id,veh_libelle FROM Vehicules WHERE NOT veh_archive AND veh_societe=". $acc_societe;
                                                                 if(!empty($d_action["act_agence"])){
                                                                     $sql.=" AND (veh_agence=" . $d_action["act_agence"] . " OR veh_agence=0 OR ISNULL(veh_agence))";
                                                                 }
                                                                 if(!empty($veh_pas_dispo)){
                                                                     $sql.=" AND NOT veh_id IN (" . $veh_pas_dispo . ")";
                                                                 }
                                                                 $sql.=" ORDER BY veh_libelle;";
                                                                 $req=$Conn->query($sql);
                                                                 $d_vehicules=$req->fetchAll();

                                                                 // SALLES
                                                                 $sql="SELECT sal_id,sal_nom,pda_id FROM Salles LEFT OUTER JOIN Plannings_Dates
                                                                 ON (Salles.sal_id=Plannings_Dates.pda_salle AND pda_date='". $date["pda_date"] . "' AND pda_demi=" . $date["pda_demi"] . " AND NOT pda_id=" . $date_id . " AND NOT pda_archive)";
                                                                 $sql.=" WHERE ISNULL(pda_id)";
                                                                 if(!empty($d_action["act_agence"])){
                                                                     $sql.=" AND (sal_agence=" . $d_action["act_agence"] . " OR sal_agence=0)";
                                                                 }
                                                                 $sql.=" ORDER BY sal_nom;";
                                                                 $req=$ConnSoc->query($sql);
                                                                 $d_salles=$req->fetchAll();	?>
                                                                 <tr id="tr_date_<?=$date["pda_id"]?>">
                                                                     <td>
                                                                        <button type='button' class="btn-suppr-date btn btn-sm btn-danger" id="btn_del_<?=$date["pda_id"]?>" data-toggle="modal" data-target="#modal_suppr_date" data-action_id="<?=  $action_id ?>" data-pda_id="<?=$date["pda_id"]?>" data-toggle="tooltip" title="Supprimer une date">
                                                                            <i class="fa fa-times"></i>
                                                                        </button>
                                                                     </td>
                                                                     <td><?=$pda_date?></td>
                                                                     <td><?=$demi_lid?></td>
                                                                     <td><?=$date["int_label_1"]?></td>
                                                             <?php	if(isset($d_action_client)){ ?>
                                                                         <td>
                                                                             <div class="option-group field">
                                                                                 <label class="option option-dark">
                                                                                     <input type="checkbox" name="present_<?=$date_id?>" value="on" <?php if(!empty($date["acd_action_client"])) echo("checked"); ?> >
                                                                                     <span class="checkbox"></span>
                                                                                 </label>
                                                                             </div>
                                                                         </td>
                                                             <?php	}?>
                                                                     <td>
                                                                         <div class="radio-custom mb5">
                                                                             <input id="defaut_<?=$date["pda_id"]?>" name="act_date_h_def_<?=$date["pda_demi"]?>" type="radio" value="<?=$date["pda_id"]?>" <?php if($d_action["act_date_h_def_" . $date["pda_demi"]]==$date["pda_id"]) echo("checked"); ?> >
                                                                             <label for="defaut_<?=$date["pda_id"]?>" ></label>
                                                                         </div>
                                                                     </td>
                                                                     <td>
                                                                         <div class="radio-custom mb5">
                                                                             <input id="th_<?=$date["pda_id"]?>" name="pda_categorie_<?=$date["pda_id"]?>" type="radio" value="0" <?php if($date["pda_categorie"]==0) echo("checked"); ?> />
                                                                             <label for="th_<?=$date["pda_id"]?>"></label>
                                                                         </div>
                                                                     </td>
                                                                     <td>
                                                                         <div class="radio-custom mb5">
                                                                             <input id="pr_<?=$date["pda_id"]?>" name="pda_categorie_<?=$date["pda_id"]?>" type="radio" value="1" <?php if($date["pda_categorie"]==1) echo("checked"); ?> />
                                                                             <label for="pr_<?=$date["pda_id"]?>"></label>
                                                                         </div>
                                                                     </td>
                                                                     <td>
                                                                         <div class="radio-custom mb5">
                                                                             <input id="ev_<?=$date["pda_id"]?>" name="pda_categorie_<?=$date["pda_id"]?>" type="radio" value="2" <?php if($date["pda_categorie"]==2) echo("checked"); ?> />
                                                                             <label for="ev_<?=$date["pda_id"]?>"></label>
                                                                         </div>
                                                                     </td>
                                                                     <td>
                                                                         <select id="salle_<?=$date["pda_id"]?>" name="pda_salle_<?=$date["pda_id"]?>" class="select2" >
                                                                             <option value="0" >Salle ...</option>
                                                                 <?php		if(!empty($d_salles)){
                                                                                 foreach($d_salles as $salle){
                                                                                     if($date["pda_salle"]==$salle["sal_id"]){
                                                                                         echo("<option value='" . $salle["sal_id"] . "' selected >" . $salle["sal_nom"] . "</option>");
                                                                                     }else{
                                                                                         echo("<option value='" . $salle["sal_id"] . "' >" . $salle["sal_nom"] . "</option>");
                                                                                     }
                                                                                 }
                                                                             } ?>
                                                                         </select>
                                                                     </td>
                                                                     <td>
                                                                         <select id="vehicule_<?=$date["pda_id"]?>" name="pda_vehicule_<?=$date["pda_id"]?>" class="select2" >
                                                                             <option value="0" >Véhicule ...</option>
                                                                 <?php		if(!empty($d_vehicules)){
                                                                                 foreach($d_vehicules as $vehicule){
                                                                                     if($date["pda_vehicule"]==$vehicule["veh_id"]){
                                                                                         echo("<option value='" . $vehicule["veh_id"] . "' selected >" . $vehicule["veh_libelle"] . "</option>");
                                                                                     }else{
                                                                                         echo("<option value='" . $vehicule["veh_id"] . "' >" . $vehicule["veh_libelle"] . "</option>");
                                                                                     }
                                                                                 }
                                                                             } ?>
                                                                         </select>
                                                                     </td>
                                                                     <td id="session_<?=$date_id?>" class="date-demi-<?=$date["pda_demi"]?>" data-date_id="<?=$date_id?>" >

                                                 <?php		}
                                                             $session_num++; ?>
                                                             <div class="row session-<?=$date_id?>" data-session_num="<?=$session_num?>" id="session_<?=$date_id?>_<?=$session_num?>" >
                                                                 <div class="col-md-5" >
                                                                     <input type="hidden" name="ase_date_<?=$date["ase_id"]?>" value="<?=$date["pda_id"]?>" />
                                                                     <input type="text" id="h_deb_<?=$date_id?>_<?=$session_num?>" size="5" name="ase_h_deb_<?=$date["ase_id"]?>" class="gui-input heure" value="<?=$date["ase_h_deb"]?>" />
                                                                 </div>
                                                                 <div class="col-md-5" >
                                                                     <input type="text" id="h_fin_<?=$date_id?>_<?=$session_num?>" size="5" name="ase_h_fin_<?=$date["ase_id"]?>" class="gui-input heure" value="<?=$date["ase_h_fin"]?>" />
                                                                 </div>
                                                                 <div class="col-md-2" >
                                                                     <button type='button' class="btn btn-sm btn-danger btn-supp" data-date_id="<?=$date_id?>" data-session_num="<?=$session_num?>" data-toggle="tooltip" title="Supprimer la session" >
                                                                         <i class="fa fa-minus" ></i>
                                                                     </button>
                                                                 </div>
                                                             </div>
                                                 <?php	} ?>
                                                             </td>
                                                             <td style="vertical-align:top;" >
                                                 <?php			//if($d_action["act_multi_session"]){ ?>
                                                                     <button type='button' class="btn btn-sm btn-success btn-add" id="btn_add_<?=$date_id?>" data-date_id="<?=$date_id?>" data-toggle="tooltip" title="Ajouter une session" <?php if($session_num==$max_session) echo("disabled") ?> >
                                                                         <i class="fa fa-plus" ></i>
                                                                     </button>
                                         <?php					//}else{
                                                                 //	echo("&nbsp;");
                                                                 //} ?>
                                                             </td>
                                                         </tr>
                                                     </tbody>
                                                 </table>
                                             </div>

                                         </div>
                                         <!-- fin panel body -->
                                     </div>
                                     <!-- fin panel -->
                                 </div>
                                 <!-- fin admin form -->
                             </div>

                         </div>

                     </section>

                 </section>
             </div>
             <footer id="content-footer" class="affix">
                 <div class="row">
                     <div class="col-xs-3 footer-left">
                         <a href="action_voir.php?action=<?=$action_id?>&action_client=<?=$action_client_id?>&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
                             Retour
                         </a>
                     </div>
                     <div class="col-xs-6 footer-middle text-center" >
                         <button type='button' class="btn btn-sm btn-info btn-duplique" data-demi="1" data-toggle="tooltip" title="Dupliquer la matinée par défaut" >
                             <i class="fa fa-files-o" ></i> Dupliquer la matinée par défaut
                         </button>
                         <button type='button' class="btn btn-sm btn-info btn-duplique" data-demi="2" data-toggle="tooltip" title="Dupliquer l'après-midi par défaut" >
                             <i class="fa fa-files-o" ></i> Dupliquer l'après-midi par défaut
                         </button>
                     </div>
                     <div class="col-xs-3 footer-right">
                         <button type="submit" class="btn btn-success btn-sm" >
                             <i class="fa fa-save" ></i> Enregistrer
                         </a>
                     </div>
                 </div>
             </footer>
         </form>
        <!-- MODAL SUPPRESSION DATE -->
        <div id="modal_suppr_date" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Supprimer une date</h4>
                    </div>

                    <div class="modal-body" >
                        <p>
                            Si la date est liée à un sous-traitant disposant d'un BC, ce dernier va être annulé.</br>
                            Le CA de chaque inscription va être recalculé en fonction du nombre de jours restants.
                        </p>
                        <p>
                            <b>Êtes-vous sûr de vouloir supprimer cette date ?</b>
                        </p>
                        <?php if($display_msg){ ?>
                            <br>
                            <strong style="color:red;">Attention, cette action est liée à une interco !
                            <?php if(!empty($d_actions_clients_intercos_ok_array)){ ?>
                                <br>
                                <strong>Vous devez prévenir les assistantes des sociétés suivantes que vous supprimez des dates : </strong>
                                <ul>

                                <?php foreach($d_actions_clients_intercos_ok_array as $soc_nom){ ?>
                                    <li><?= $soc_nom ?></li>
                                <?php } ?>

                                </ul>
                            <?php } ?>
                        <?php } ?>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-sm btn-supp-date-modal" data-action_id="" data-pda_id=""> <i class="fa fa-check"></i> Oui</a>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- FIN MODAL SUPPRESSION DATE -->
 <?php
         include "includes/footer_script.inc.php"; ?>
         <script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
         <script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
         <script type="text/javascript" src="/assets/js/custom.js"></script>
         <script type="text/javascript" >
             var tab_session=new Array();
            var max_session=parseInt(<?=$max_session?>);

             jQuery(document).ready(function(){

                 $(".btn-add").click(function(){
                     date_id=$(this).data("date_id");
                     ajouter_session(date_id,"","");
                 });
                 $(".btn-supp").click(function(){
                     date_id=$(this).data("date_id");
                     session_num=$(this).data("session_num");
                     supprimer_session(date_id,session_num);
                 });

                 $(".btn-duplique").click(function(){
                     demi=$(this).data("demi");
                     date_defaut=$('input[name=act_date_h_def_' + demi + ']:checked').val();
                     duppliquer_date(date_defaut,demi);
                 });

                 $(".btn-suppr-date").click(function(){
                     pda_id=$(this).data("pda_id");
                     action_id=$(this).data("action_id");
                     $(".btn-supp-date-modal").data("action_id", action_id);
                     $(".btn-supp-date-modal").data("pda_id", pda_id);
                 });

                 $(".btn-supp-date-modal").click(function(){
                    pda_id=$(this).data("pda_id");
                    action_id=$(this).data("action_id");

                    supprimer_date(pda_id, action_id);
                 });

             });
             (jQuery);

             function ajouter_session(date,h_deb,h_fin){
                 nb_session=$(".session-" + date).length;
                 if(nb_session<max_session){
                     last_session=$(".session-" + date).last();
                     session_num=last_session.data("session_num")+1;
                     code_html="<div class='row session-" + date + "' data-session_num='" + session_num + "' id='session_" + date + "_" + session_num + "' >";
                         code_html=code_html + "<div class='col-md-5' >";
                             code_html=code_html + "<input type='hidden' name='ase_date[]' value='" + date + "' />";
                             code_html=code_html + "<input type='text' size='5' id='h_deb_" + date + "_" + session_num + "' name='ase_h_deb[]' class='gui-input heure' value='" + h_deb + "' />";
                         code_html=code_html + "</div>";
                         code_html=code_html + "<div class='col-md-5' >";
                             code_html=code_html + "<input type='text' size='5' id='h_fin_" + date + "_" + session_num + "' name='ase_h_fin[]' class='gui-input heure' value='" + h_fin + "' />";
                         code_html=code_html + "</div>";
                         code_html=code_html + "<div class='col-md-2' >";
                             code_html=code_html + "<button type='button' class='btn btn-sm btn-danger btn-supp' data-date_id='" + date + "' data-session_num='" + session_num + "' data-toggle='tooltip' title='Supprimer la session'  >";
                                 code_html=code_html + "<i class='fa fa-times' ></i>";
                             code_html=code_html + "</button>";
                         code_html=code_html + "</div>";
                     code_html=code_html + "</div>";

                     $("#session_" + date).append(code_html);

                     $(".btn-supp").click(function(){
                         date_id=$(this).data("date_id");
                         session_num=$(this).data("session_num");
                         supprimer_session(date_id,session_num);
                     });

                     if(nb_session==(max_session-1)){
                         $("#btn_add_" + date).prop("disabled",true);
                     }
                 }else{
                     $("#btn_add_" + date).prop("disabled",true);
                 }
             }

             function supprimer_session(date,session){
                 nb_session=$(".session-" + date).length;
                 if(nb_session>1){
                     $("#session_" + date + "_" + session).remove();
                     $("#btn_add_" + date).prop("disabled",false);
                 }
             }

             function duppliquer_date(date,demi){

                 tab_session=new Array();
                 var nb_session_ref=0;
                 var nb_session_dup=0;
                 /*tab_session["h_deb"]=new Array();
                 tab_session["h_fin"]=new Array();*/

                 // on recupère les donne de la sssion de référence
                 $(".session-" + date).each(function(index){
                     session_num=$(this).data("session_num");
                     if($("#h_deb_" + date + "_" + session_num).val()!="" && $("#h_fin_" + date + "_" + session_num).val()!=""){
                         nb_session_ref=nb_session_ref+1;
                         tab_session[nb_session_ref]=new Array($("#h_deb_" + date + "_" + session_num).val(),$("#h_fin_" + date + "_" + session_num).val());
                     }
                 });
                 if(nb_session_ref==0){
                     nb_session_ref=1;
                     tab_session[nb_session_ref]=new Array("","");
                 }

                 var categorie_ref=$('input[name=pda_categorie_' + date + ']:checked').val();
                 var vehicule_ref=$('#vehicule_' + date).val();
                 var salle_ref=$('#salle_' + date).val();

                 // on applique au date de la meme demi journée
                 $(".date-demi-" + demi).each(function(index){

                     date_dup=$(this).data("date_id");

                     // on duplique les donnee "date"

                     if(categorie_ref==2){
                         $('#ev_' + date_dup).prop("checked",true);
                     }else if(categorie_ref==1){
                         $('#pr_' + date_dup).prop("checked",true);
                     }else{
                         $('#th_' + date_dup).prop("checked",true);
                     }
                     $('#salle_' + date_dup).val(salle_ref).trigger("change");
                     $('#vehicule_' + date_dup).val(vehicule_ref).trigger("change");

                     nb_session_dup=0;
                     // on parcours les sessions de chaque date
                     $(".session-" + date_dup).each(function(index){
                         session_dup=$(this).data("session_num");
                         nb_session_dup=nb_session_dup+1;
                         if(nb_session_dup<=nb_session_ref){

                             // on maj
                             $("#h_deb_" + date_dup + "_" + session_dup).val(tab_session[nb_session_dup][0]);
                             $("#h_fin_" + date_dup + "_" + session_dup).val(tab_session[nb_session_dup][1]);
                         }else{
                             supprimer_session(date_dup,session_dup);
                         }
                     });
                     if(nb_session_ref>nb_session_dup){
                         for(var bcl_add=nb_session_dup+1;bcl_add<=nb_session_ref;bcl_add++){
                             nb_session_dup=nb_session_dup+1;
                             ajouter_session(date_dup,tab_session[nb_session_dup][0],tab_session[nb_session_dup][1]);
                         }
                     };
                 });
             }
            // supprimer une date
             function supprimer_date(pda_id,action_id){
                $.ajax({
                    type:'POST',
                    url: 'ajax/ajax_supp_planning_date.php',
                    data : 'pda_id=' + pda_id + "&action_id=" + action_id,
                    dataType: 'json',
                    success: function(data){

                        $("#tr_date_" + pda_id).remove();
                        $("#modal_suppr_date").modal("hide");

                    },
                    error: function(data){
                        $("#modal_suppr_date").modal("hide");
                        afficher_message("Erreur","danger",data.responseText);
                    }
                });
             }
         </script>
     </body>
 </html>
