<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");

include 'modeles/mod_parametre.php';
include 'modeles/mod_agence.php';
include 'modeles/mod_get_intervenant.php';

// EDITION D'UN INTERVENANT (ligne de planning) et données associés

	// PERSONNE CONNECTE
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];	
	}
	/*if($acc_societe!=3){
		$acc_agence=0;
	}*/
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref_id"])){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];	
	}
	
	$intervenant_id=0;
	if(!empty($_GET["intervenant"])){
		$intervenant_id=intval($_GET["intervenant"]);	
	}
	
	// MODIF
	
	if($intervenant_id>0){
		
		$d_intervenant=get_intervenant($intervenant_id);
	}else{
		$d_intervenant=array(
			"int_type" => 1,
			"int_ref_1" => 0,
			"int_ref_2" => 0,
			"int_agence" => 0,
			"int_label_1" => "",
			"int_label_1" => "",
			"int_option" => 0
		);
	}
	
	

	
	// SUR LA Societes
	
	if($acc_agence==0){
		$sql="SELECT soc_agence FROM Societes WHERE soc_id=:acc_societe;";
		$req=$Conn->prepare($sql);
		$req->bindParam("acc_societe",$acc_societe);
		$req->execute();
		$societe=$req->fetch();
		if(!empty($societe)){
			if($societe["soc_agence"]){ 
				// gestion d'agence on liste celles auquelle à accès l'uti
				
				$sql="SELECT age_id,age_nom FROM Agences,Utilisateurs_Societes WHERE age_id=uso_agence AND uso_utilisateur=:acc_utilisateur AND uso_societe=:acc_societe";
				$sql.=" ORDER BY age_nom";
				$req=$Conn->prepare($sql);
				$req->bindParam("acc_societe",$acc_societe);
				$req->bindParam("acc_utilisateur",$acc_utilisateur);
				$req->execute();
				$acc_agence_liste=$req->fetchAll();
				if(empty($acc_agence_liste)){
					unset($acc_agence_liste);
				}
			}
		}
	}

	// LES FORMATEUR INTERNE DISPO
	$sql="SELECT uti_id,uti_nom,uti_prenom FROM Utilisateurs WHERE uti_societe=:acc_societe AND NOT uti_archive";
	if($acc_agence>0){
		$sql.=" AND uti_agence=:acc_agence";
	}
	$sql.=" ORDER BY uti_nom,uti_prenom;";
	$req=$Conn->prepare($sql);
	$req->bindParam("acc_societe",$acc_societe);
	if($acc_agence>0){
		$req->bindParam("acc_agence",$acc_agence);
	}
	$req->execute();
	$utilisateurs=$req->fetchAll();
	if(!empty($utilisateurs)){
		$sql="SELECT int_ref_1 FROM Intervenants WHERE int_type=1";
		if($d_intervenant["int_type"]==1 AND $d_intervenant["int_ref_1"]>0){
			// en cas de modif l'uti select doit être repropose
			$sql.=" AND NOT int_ref_1=" . $d_intervenant["int_ref_1"];

		}
		$req=$ConnSoc->query($sql);
		$req->execute();
		$uti_deja_inscrit=$req->fetchAll();
		if(!empty($uti_deja_inscrit)){
			foreach($uti_deja_inscrit as $u){
				$cle=array_search($u["int_ref_1"],array_column($utilisateurs,"uti_id"),true);
				if(!is_bool($cle)){
					array_splice($utilisateurs, $cle, 1);
				};
			}
		}
	}
	
	// LES INTERCO

	$sql="SELECT soc_id,soc_code FROM Societes WHERE (NOT soc_id=:acc_societe OR soc_agence) AND NOT soc_archive";
	$req = $Conn->prepare($sql);
	$req->bindParam(":acc_societe",$acc_societe);
	$req->execute();
	$intercos=$req->fetchAll();
	
	$d_intercos_age=array();
	if($d_intervenant["int_type"]==2 AND !empty($d_intervenant["int_ref_1"]) ){
		
		$sql="SELECT age_id,age_nom FROM Agences WHERE age_societe=" . $d_intervenant["int_ref_1"] . " ORDER BY age_nom;";
		$req = $Conn->query($sql);
		$d_intercos_age=$req->fetchAll();
		
		
	}
	
	// LES ST

	$multi_agence=0;
	
	if($d_intervenant["int_type"]==3){
		
		if(!empty($d_intervenant["int_ref_1"])){
			
			$sql="SELECT fou_code,fou_nom,fou_type FROM Fournisseurs WHERE fou_id=" . $d_intervenant["int_ref_1"] . ";";
			$req = $Conn->query($sql);
			$d_fournisseur=$req->fetch();
			if(empty($d_fournisseur)){
				unset($d_fournisseur);
			}
			
			// ON VA CHERCGER LES INTERVENANT DU ST
			
			$sql="SELECT fin_id,fin_nom,fin_prenom FROM Fournisseurs_Intervenants WHERE fin_fournisseur=" . $d_intervenant["int_ref_1"] . " ORDER BY fin_nom,fin_prenom;";
			$req = $Conn->query($sql);
			$d_fou_intervenants=$req->fetchAll();
			if(empty($d_fou_intervenants)){
				unset($d_fou_intervenants);
			}
			
			// ON REGARDE SI LE FOURNISSEUR EST MULTO AGENCE
			
			/*
			Réunion MM JP du 10/12/2018 -> fournisseur est réseau
			if(empty($acc_agence) AND empty($d_intervenant["int_agence"])){
				
				$sql="SELECT fso_agence FROM Fournisseurs_Societes WHERE fso_fournisseur=:fso_fournisseur AND fso_societe=:acc_societe;";
				$req = $Conn->prepare($sql);
				$req -> bindParam(":fso_fournisseur",$d_intervenant["int_ref_1"]);
				$req -> bindParam(":acc_societe",$acc_societe);
				$req ->execute();
				$d_fournisseur_soc=$req->fetchAll();
				if(!empty($d_fournisseur_soc)){
					if(count($d_fournisseur_soc)>1){
						$multi_agence=1;
					}elseif($d_fournisseur_soc[0]["fso_agence"]==0){
						$multi_agence=1;
					}
				}
			}*/
		}
	}
	
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<form method="post" action="intervenant_enr.php" id="form_int" >
			<div>
				<input type="hidden" name="intervenant" value="<?=$intervenant_id?>" />
			</div>
			<!-- Start: Main -->
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">	

				<?php	if(!empty($intervenant_id)){ ?>
							<h1><small>Intervenant</small> <?=$d_intervenant["int_label_1"]?> <?=$d_intervenant["int_label_2"]?></h1>					
				<?php	} ?>

						<div class="panel" > 
							<div class="panel-heading panel-head-sm" >
								<span class="panel-title">Type d'intervenant</span>								
							</div>
							<div class="panel-body">
								<div class="admin-form" >
									<div class="row" >
										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="int_type" value="1" <?php if($d_intervenant["int_type"]==1) echo("checked"); ?> />
												<span class="radio"></span>Formateur interne
											</label>
										</div>
										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="int_type" value="2" <?php if($d_intervenant["int_type"]==2) echo("checked"); ?> >
												<span class="radio"></span>Interco
											</label>
										</div>
										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="int_type" value="3" <?php if($d_intervenant["int_type"]==3) echo("checked"); ?> >
												<span class="radio"></span>Sous-Traitant
											</label>
										</div>
										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="int_type" value="0" <?php if($d_intervenant["int_type"]==0) echo("checked"); ?> >
												<span class="radio"></span>Autre
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--FORMATEUR INTERNE -->	
						<div class="panel" id="form_interne" <?php if($d_intervenant["int_type"]!=1) echo("style='display:none;'"); ?> > 
							<div class="panel-heading" >
								<span class="panel-title">Formateur interne</span>
							</div>
							<div class="panel-body">
							
								<div class="admin-form" >
							
									<div class="row" >
										<div class="col-md-5" >
											<select id="utilisateur" name="utilisateur" placeholder="Compte utilisateur existants" >
												<option value="0" >&nbsp;</option>
									<?php		if(!empty($utilisateurs)){
													foreach($utilisateurs as $u){
														if($d_intervenant["int_type"]==1 AND $d_intervenant["int_ref_1"]==$u["uti_id"]){
															echo("<option value='" . $u["uti_id"] . "' selected >" . $u["uti_nom"] ." " . $u["uti_prenom"] . "</option>");											
														}else{
															echo("<option value='" . $u["uti_id"] . "' >" . $u["uti_nom"] ." " . $u["uti_prenom"] . "</option>");
														}
													}
												} ?>
											</select>
										</div>
										<div class="col-md-2 text-center" >
											OU
										</div>
										<div class="col-md-5" >
											<div class="option-group field">
												<label class="option">
													<input type="checkbox" id="utilisateur_add" name="utilisateur_add" value="on" />	
													Créer un nouveau compte utilisateur : 													
													<span class="checkbox"></span>													
												</label>
											</div>									
										</div>
										
									</div>
									<!--utilisateur_add_form -->
									<div id="utilisateur_add_form" style="display:none" >
									
									
										<div class="row" >
											<div class="col-md-12" >
												<div class="section-divider mb40" >
													<span>Identité</span>
												</div>
											</div>
										</div>
											
							<?php		if(isset($acc_agence_liste)){
											if(!empty($acc_agence_liste)){ ?>
												<div class="row" >
													<div class="col-md-4" >
														<div class="section">
															<label class="field select">
																<select name="uti_agence" id="uti_agence" >
																	<option value="">Agence...</option>
														<?php		foreach($acc_agence_liste as $a){
																		echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																	};?>																																	
																</select>
																<i class="arrow simple"></i>
															</label>
														</div>
													</div>
												</div>
							<?php			}
										} ?>
										<div class="row" >			
											<div class="col-md-4" >
												<div class="section">
													<label class="field select">
														<select name="uti_titre" id="uti_titre" >
															<option value="">Civilité...</option>
												<?php		foreach($base_civilite as $cle => $valeur){
																if($cle>0){
																	echo("<option value='" . $cle . "' >" . $valeur . "</option>");
																};																
															}; ?>																																	
														</select>
														<i class="arrow simple"></i>
													</label>
												</div>
											</div>
											<div class="col-md-4">
												<div class="section">
													<div class="field prepend-icon">
														<input type="text" name="uti_nom" id="uti_nom" class="gui-input nom" placeholder="Nom" >
														<label for="uti_nom" class="field-icon">
															<i class="fa fa-user"></i>
														</label>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="section">
													<div class="field prepend-icon">
														<input type="text" name="uti_prenom" id="uti_prenom" class="gui-input prenom" placeholder="Prénom" >
														<label for="uti_prenom" class="field-icon">
															<i class="fa fa-user"></i>
														</label>
													</div>
												</div>
											</div>
											
										</div>
										
										<div class="row">
										
											<div class="col-md-8" >
												<div class="section-divider mb40" >
													<span>Adresse</span>
												</div>
												<div class="col-md-12">
													<div class="section">
														<div class="field prepend-icon">
															<input type="text" name="uti_ad1" id="uti_ad1" class="gui-input" placeholder="Adresse" >
															<label for="uti_ad1" class="field-icon">
																<i class="fa fa-map-marker"></i>
															</label>
														</div>
													</div>
													<div class="section">
														<div class="field prepend-icon">
															<input type="text" name="uti_ad2" class="gui-input" id="uti_ad2" placeholder="Adresse (Complément 1)">
															<label for="uti_ad2" class="field-icon">
																<i class="fa fa-map-marker"></i>
															</label>
														</div>
													</div>
													<div class="section">
														<div class="field prepend-icon">
															<input type="text" name="uti_ad3" id="uti_ad3" class="gui-input" placeholder="Adresse (Complément 2)">
															<label for="uti_ad3" class="field-icon">
																<i class="fa fa-map-marker"></i>
															</label>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="section">
														<div class="field prepend-icon">
															<input type="text" id="uti_cp" name="uti_cp" class="gui-input code-postal" placeholder="Code postal" >
															<label for="uti_cp" class="field-icon">
																<i class="fa fa fa-certificate"></i>
															</label>
														</div>
													</div>
												</div>
												<div class="col-md-9">
													<div class="section">
														<div class="field prepend-icon">
															<input type="text" name="uti_ville" id="uti_ville" class="gui-input nom" placeholder="Ville" >
															<label for="uti_ville" class="field-icon">
																<i class="fa fa fa-building"></i>
															</label>
														</div>
													</div>
												</div>
											</div>
											<!--fin bloc adresse -->
											<!--bloc contact -->
											<div class="col-md-4" >
												<div class="section-divider mb40" >
													<span>Contact Pro.</span>
												</div>
												<div class="section">
													<div class="field prepend-icon">
														<input type="tel" name="uti_tel" id="uti_tel" class="gui-input telephone" placeholder="Numéro de téléphone" >
														<label for="uti_tel" class="field-icon">
															<i class="fa fa fa-phone"></i>
														</label>
													</div>
												</div>
												<div class="section">
													<div class="field prepend-icon">
														<input name="uti_fax" id="uti_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax" >
														<label for="uti_fax" class="field-icon">
															<i class="fa fa-fax"></i>
														</label>
													</div>
												</div>
												<div class="section">
													<div class="field prepend-icon">
														<input type="tel" name="uti_mobile" id="uti_mobile" class="gui-input telephone" placeholder="Numéro de mobile" >
														<label for="uti_mobile" class="field-icon">
															<i class="fa fa-mobile"></i>
														</label>
													</div>
												</div>
												<div class="section">
													<div class="field prepend-icon">
														<input type="email" name="uti_mail" id="uti_mail" class="gui-input" placeholder="Adresse Email" >
														<label for="uti_mail" class="field-icon">
															<i class="fa fa-envelope"></i>
														</label>
													</div>
												</div>
											</div>
											<!--fin bloc contact -->
										</div>								
									</div>
									<!--FIN DE utilisateur_add_form -->
								</div>
								
							</div>
						</div>
						<!--FIN DE FORMATEUR INTERNE -->
						
						
						<!--INTERCO -->	
						<div class="panel" id="form_interco" <?php if($d_intervenant["int_type"]!=2) echo("style='display:none;'"); ?> > 
							<div class="panel-heading" >
								<span class="panel-title">Interco</span>
							</div>
							<div class="panel-body">
							
								<div class="admin-form" >
								
						<?php		if(isset($acc_agence_liste)){
										//if(!empty($acc_agence_liste)){ ?>
											<div class="row" >
												<div class="col-md-4" >
													<div class="section">
														<label for="interco_agence" >Agence :</label>
														<span class="field select">
															<select name="interco_agence" id="interco_agence" required >
																<option value="">Agence...</option>
													<?php		foreach($acc_agence_liste as $a){
																	if($d_intervenant["int_agence"]==$a["age_id"]){
																		echo("<option value='" . $a["age_id"] . "' selected >" . $a["age_nom"] . "</option>");
																	}else{
																		echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");	
																	}
																};?>																																	
															</select>
															<i class="arrow simple"></i>
														</span>
													</div>
												</div>
											</div>
						<?php		//	}
									} ?>
							
									<div class="row" id="cont_interco" <?php if(isset($acc_agence_liste) AND empty($d_intervenant["int_agence"])) echo("style='display:none;'"); ?> >
										<div class="col-md-4">
											<label for="societe" >Société INTERCO :</label>
											<span class="field select">
												<select id="societe" name="societe" >
													<option value="">Sélectionner la société...</option>
										<?php		if(!empty($intercos)){
														foreach($intercos as $i){
															if($d_intervenant["int_type"]==2 AND $d_intervenant["int_ref_1"]==$i["soc_id"]){
																echo("<option value='" . $i["soc_id"] . "' selected >" . $i["soc_code"] . "</option>");
															}else{
																echo("<option value='" . $i["soc_id"] . "' >" . $i["soc_code"] . "</option>");	
															}
														}										
													} ?>
												</select>
												<i class="arrow simple"></i>
											</span>											
										</div>                                           
										<div class="col-md-4" >	
											<div id="cont_agence" <?php if(empty($d_intercos_age)) echo("style='display:none;'"); ?> >
												<label for="societe" >Agence INTERCO :</label>
												<span class="field select" >
													<select id="agence" name="agence" >
														<option value="">Sélectionner une agence...</option>
												<?php	if(!empty($d_intercos_age)){
															foreach($d_intercos_age as $int_age){
																if($int_age["age_id"]==$d_intervenant["int_ref_2"]){
																	echo("<option value='" . $int_age["age_id"] . "' selected >" . $int_age["age_nom"] . "</option>");
																}else{
																	echo("<option value='" . $int_age["age_id"] . "' >" . $int_age["age_nom"] . "</option>");
																}
															}
														} ?>
													</select>											
													<i class="arrow simple"></i>
												</span>	
											</div>
										</div>								
										<div class="col-md-4">
											<label for="interco_label" >Libellé de la fiche INTERCO :</label>
											<input type="text" name="interco_label" class="gui-input" id="interco_label" placeholder="Libellé" value="<?=$d_intervenant["int_label_1"]?>" />											
										</div>																	
									</div>									
								</div>
								
							</div>
						</div>
						<!--FIN DE INTERCO -->
						
						<!-- SOUS-TRAITANT -->
						<div class="panel" id="form_st" <?php if($d_intervenant["int_type"]!=3) echo("style='display:none;'"); ?> > 
							<div class="panel-heading" >
								<span class="panel-title">Sous-Traitant</span>
							</div>
							<div class="panel-body">
							
								<div class="admin-form" >
								
									<div class="row" >									
										<div class="col-md-6">
											<label for="fournisseur" >Fournisseur :</label>										
											<select id="fournisseur" name="fournisseur" class="select2 select2-fournisseur" data-famille_type="2" required >
										<?php	if(isset($d_fournisseur)){
													echo("<option value='" . $d_intervenant["int_ref_1"] . "' >" . $d_fournisseur["fou_nom"] . " (". $d_fournisseur["fou_code"] . ")</option>");
												}else{
													echo("<option value='' >Sélectionner un founisseur</option>");
												}	 ?>
											</select>																			
										</div>
										<div class="col-md-6" id="cont_fou_int" <?php if(!isset($d_fou_intervenants)) echo("style='display:none;'"); ?> >
											<label for="fou_int" >Intervenant :</label>	
											<select id="fou_int" name="fou_int" class="select2" <?php if($d_intervenant["int_type"]==3 AND $d_fournisseur["fou_type"]==3) echo("disabled"); ?> >
												<option value="" >Sélectionner un intervenant</option>
									<?php 		if(isset($d_fou_intervenants)){
													foreach($d_fou_intervenants as $d_int){
														if($d_int["fin_id"]==$d_intervenant["int_ref_2"]){
															echo("<option value='" . $d_int["fin_id"] . "' selected >" . $d_int["fin_nom"] . " " . $d_int["fin_prenom"] . "</option>");
														}else{
															echo("<option value='" . $d_int["fin_id"] . "' >" . $d_int["fin_nom"] . " " . $d_int["fin_prenom"] . "</option>");
														}	
													}
												} ?>
											</select>
										</div>
									</div>
									<div class="row mt15" >	
							<?php		if(isset($acc_agence_liste)){
											//if(!empty($acc_agence_liste)){ ?>												
												<div class="col-md-6" id="cont_st_agence" <?php if(!empty($d_intervenant["int_ref_2"]) OR $d_intervenant["int_type"]!=3) echo("style='display:none;'"); ?> >
													<div class="section">
														<label for="st_agence" >Agence :</label>
														<span class="field select">
															<select name="st_agence" id="st_agence" required >
																<option value="">Agence...</option>
													<?php		foreach($acc_agence_liste as $a){
																	if($d_intervenant["int_agence"]==$a["age_id"]){
																		echo("<option value='" . $a["age_id"] . "' selected >" . $a["age_nom"] . "</option>");
																	}else{
																		echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");	
																	}
																};?>																																	
															</select>
															<i class="arrow simple"></i>
														</span>
													</div>
												</div>
							<?php			//}
										} ?>
										
										<div class="col-md-6" id="cont_st_label" <?php if(!empty($d_intervenant["int_ref_2"]) OR $d_intervenant["int_type"]!=3) echo("style='display:none;'"); ?> >
											<label for="autre_label" >Libellé de la fiche sous-traitant :</label>
											<input type="text" name="st_label" class="gui-input" id="st_label" placeholder="Libellé" value="<?=$d_intervenant["int_label_1"]?>" required />														
										</div>																																			
									</div>	
									
								</div>
								
							</div>
						</div>
						
						<!--AUTRE -->	
						<div class="panel" id="form_autre" <?php if($d_intervenant["int_type"]!=0) echo("style='display:none;'"); ?> > 
							<div class="panel-heading" >
								<span class="panel-title">Autre</span>
							</div>
							<div class="panel-body">							
								<div class="admin-form" >						
									<div class="row" >
							<?php		if(isset($acc_agence_liste)){
											//if(!empty($acc_agence_liste)){ ?>												
												<div class="col-md-4" >
													<div class="section">
														<label for="autre_agence" >Agence :</label>
														<span class="field select">
															<select name="autre_agence" id="autre_agence" required >
																<option value="">Agence...</option>
													<?php		foreach($acc_agence_liste as $a){
																	if($d_intervenant["int_agence"]==$a["age_id"]){
																		echo("<option value='" . $a["age_id"] . "' selected >" . $a["age_nom"] . "</option>");
																	}else{
																		echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");	
																	}
																};?>																																	
															</select>
															<i class="arrow simple"></i>
														</span>
													</div>
												</div>
							<?php			//}
										} ?>
										<div class="col-md-4">
											<label for="autre_label" >Libellé de la fiche intervenant :</label>
											<input type="text" name="autre_label" class="gui-input" id="autre_label" placeholder="Libellé" value="<?=$d_intervenant["int_label_1"]?>" />														
										</div>       
										<div class="col-md-4" >
											<div class="section">
												<label for="autre_annul" >Annulation / Report :</label>
												<span class="field select">
													<select name="autre_annul" id="autre_annul" >
														<option value="">Ligne classique</option>
														<option value="1" <?php if($d_intervenant["int_option"]==1) echo ("selected") ?> >Ligne annulation</option>
														<option value="2" <?php if($d_intervenant["int_option"]==2) echo ("selected") ?> >Ligne report</option>																								
													</select>
													<i class="arrow simple"></i>
												</span>
											</div>
										</div>                                    
															
									</div>									
								</div>
								
							</div>
						</div>
						<!--FIN DE AUTRE -->
					
				<?php 	if($intervenant_id==0){ ?>
							<!--BLOC INSCRIPTION -->	
							<div class="panel" id="form_interne" > 
								<div class="panel-heading" >
									<span class="panel-title">Inscription</span>
								</div>
								<div class="panel-body">
									<div class="admin-form" >
										<div class="row" >
											<div class="col-md-6">
												Du							
												<div class="field prepend-icon">
													<input type="text" id="periode_deb" name="periode_deb" class="gui-input datepicker" placeholder="Selectionner une date" value="<?php if(isset($_SESSION["pla_periode_deb"])) echo($_SESSION["pla_periode_deb"]);?>" />						
													<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
												</div>									
											</div>
											<div class="col-md-6">
												Au
												<div class="field prepend-icon">
													<input type="text" id="periode_fin" name="periode_fin" class="gui-input datepicker" placeholder="Selectionner une date" value="<?php if(isset($_SESSION["pla_periode_fin"])) echo($_SESSION["pla_periode_fin"]);?>" />
													<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
												</div>
											</div>						
										</div>
									</div>
								</div>
							</div>					
				<?php 	}else{ ?>
							<div class="panel" > 
								<div class="panel-body">						
									<div class="admin-form" >						
										<div class="row" >
											<div class="col-md-3" >
												<label class="option">
													<input type="checkbox" id="int_archive" name="int_archive" value="on" <?php if($d_intervenant["int_archive"]==1) echo("checked"); ?> />	
													<span class="checkbox"></span> Archivé													
												</label>
											</div>									
										</div>									
									</div>
								</div>
							</div>					
				<?php	} ?>		
					</section>
					<!-- End: Content -->
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
			<?php		if(!empty($_SESSION["retour"])){ ?>
							<a href="<?=$_SESSION["retour"]?>" class="btn btn-sm btn-default" >
								Retour
							</a>
			<?php 		}?> 	
					</div>
					<div class="col-xs-6 footer-middle" >&nbsp;</div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm" >
							<i class="fa fa-save"></i>
							Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
		<?php
		include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>
		<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script type="text/javascript" >
			
			var acc_agence="<?=$acc_agence?>";
			var acc_societe="<?=$acc_societe?>";
			var interco_age=0;
			var multi_agence=parseInt("<?=$multi_agence?>");
				
			jQuery(document).ready(function(){
				
				
				// CHOIX DU BLOC A AFFICHER
				$('input[name=int_type]').click(function(){	
					switch($('input[name=int_type]:checked').val()) {
						case "1":
							$('#form_interne').show();
							$('#form_interco').hide();
							$('#form_autre').hide();
							$('#form_st').hide();
							break;
						case "2":
							$('#form_interne').hide();   
							$('#form_interco').show();
							$('#form_autre').hide();
							$('#form_st').hide();
							break;
						case "3":
							$('#form_interne').hide(); 
							$('#form_interco').hide();
							$('#form_autre').hide();
							$('#form_st').show();
							break;
						default:
							$('#form_interne').hide();    
							$('#form_interco').hide();
							$('#form_autre').show();
							$('#form_st').hide();							
					}
				});

				// BLOC FORMATEUR INTERNE
				$("#utilisateur").select2();
				$("#utilisateur").change(function(){
					if($(this).val()>0){
						$("#utilisateur_add").prop("checked",false);
						$("#utilisateur_add_form").hide();									
					}
				});
				$("#utilisateur_add").click(function(){				
					if($(this).is(":checked")){
						$("#utilisateur").select2("val",0);
						$("#utilisateur_add_form").show();
						if($("#int_archive").length>0){
							$("#int_archive").prop("checked",false);
						}
					}else{
						$("#utilisateur_add_form").hide();			
					}
					
				});

				$("#int_archive").click(function(){	
					if($("#utilisateur_add").is(":checked")){
						$("#utilisateur_add").trigger("click");						
					}			
				});
				
				//BLOC INTERCO
				if($("#interco_agence").length>0){
					$("#interco_agence").change(function(){							
						if(acc_societe==$("#societe").val()){
							get_agences($("#societe").val(),"",$(this).val(),afficher_agence_interco,"","");
						}	
						if($(this).val()>0){
							$("#cont_interco").show();
						}else{
							$("#cont_interco").hide();
						}
					});
				}
				$("#societe").change(function(){
					if($(this).val()!=""){
						$("#interco_label").val($("#societe option:selected").text());
					}
					
					if($("#interco_agence").length>0){
						interco_age=$("#interco_agence").val();
					}else{
						interco_age=acc_agence;
					}
					get_agences($(this).val(),"",interco_age,afficher_agence_interco,"","");										
				});
				$("#agence").change(function(){	
					if($(this).val()!=""){
						$("#interco_label").val($("#agence option:selected").text());
					}							
				});
				
				
				// BLOC ST
				
				$("#fournisseur").change(function(){
					if($(this).val()>0){
						get_fournisseur($(this).val(),afficher_st,"","");
					}else{
						afficher_st();	
					}
				});
				
				$("#fou_int").change(function(){
					console.log("#fou_int -> change");
					if($(this).val()>0){	
						
						$("#cont_st_label").hide();
						if($("#cont_st_agence").length>0){
							$("#cont_st_agence").hide();
						}
					}else{
						if($("#cont_st_agence").length>0){
							$("#cont_st_agence").show();
						};
						$("#cont_st_label").show();
					}
					//alert($("#fou_int option:selected").text());
					
				});
				
				// VALIDATION DU FORMULAIRE
				$("#form_int").submit(function(){
					switch($('input[name=int_type]:checked').val()) {
						case "1":	
							
							if($("#utilisateur_add").is(":checked")){
								$("#uti_nom").rules("add", {
									required: true
								});	
								$("#uti_prenom").rules("add", {
									required: true
								});							
								
							}else{
								if($("#utilisateur").select2("val")==0){
									return false;
								};							
							}
						break;
						
						case "2":	
							$("#societe").rules("add", {
								required: true
							});
							$("#interco_label").rules("add", {
								required: true
							});
							if($("#cont_agence").is(":visible")){									
								$("#agence").rules("add", {
									required: true
								});
							}
						break;
						
						default:			
							$("#autre_label").rules("add", {
								required: true
							});	
								
						break;
					}
					
				});
				$("#form_int").validate();
			});
			
			function afficher_agence_interco(json){
				if(json.length>0){				
					$("#agence option[value!='0'][value!='']").remove();
					
					$.each(json, function(key, value){
						$("#agence").append($("<option></option>").attr("value",value["id"]).text(value["text"]));
					});
					$("#cont_agence").show();
				}else{
					$("#agence option[value!='0'][value!='']").remove();
					$("#agence").val(0);
					$("#cont_agence").hide();
				}
				
			}
		
			function afficher_st(json){
				console.log("afficher_st()");
				$("#fou_int option[value!='0'][value!='']").remove();
				if(json){	
					
					/*if(json.agence==-1){
						multi_agence=1;
					}else{
						multi_agence=0;
						if(json.agence>0){
							if($("#st_agence").length>0){
								$("#st_agence").val(json.agence);	
							}
						}
					}*/
					
					if(json.intervenants){
						$("#fou_int").select2({
							data: json.intervenants,
							closeOnSelect: true			
						});
						if(json.fou_type==3){
							$("#fou_int").val(json.intervenants[0]["id"]).trigger("change");
							$("#fou_int").prop("disabled",true);
						}else{
							$("#fou_int").prop("disabled",false);
						}
						$("#cont_fou_int").show();	
						
					}else{
						
						if(json.fou_type==3){
							modal_alerte_user("Cet auto-entrepreneur ne dispose pas de fiche intervenant!");
							$("#fournisseur").select2("val",0);
						}
						$("#cont_fou_int").hide();
						$("#cont_st_label").hide();	
						$("#st_label").val("");	
						$("#cont_st_agence").hide();	
						$("#st_agence").val("");	
					}
					
					if($("#fou_int").select2("val")==0){
						$("#cont_st_label").show();	
						$("#cont_st_agence").show();	
					}
	
				}else{
					
					// PAS DE ST
					$("#cont_fou_int").hide();	
					$("#cont_st_label").hide();	
					$("#st_label").val("");	
					$("#cont_st_agence").hide();	
					$("#st_agence").val("");	
				}
			}
		</script>
	</body>
</html>