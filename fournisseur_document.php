<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
if(isset($_GET['id'])){
    $req = $Conn->prepare("SELECT * FROM fournisseurs_documents_Param WHERE fdp_id = :fdp_id");
    $req->bindParam(':fdp_id', $_GET['id']);
    $req->execute();
    $doc = $req->fetch();
}else{
    $doc=array(
        "fdp_libelle" => "",
        "fdp_type" => 1,
        "fdp_entreprise" => 1,
        "fdp_indiv" => 1,
        "fdp_obligatoire" => 1,
        "fdp_periodicite_mois" => 0,
        "fdp_une_seule_fois" => 1
    );
}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Paramètres</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <form action="fournisseur_document_enr.php" method="POST" id="admin-form">
        <div id="main">
            <?php
            include "includes/header_def.inc.php";
            ?>


            <!-- Start: Content-Wrapper -->
            <section id="content_wrapper" class="">
                
                <section id="content" class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="admin-form theme-primary ">
                                <div class="panel heading-border panel-primary">
                                    <div class="panel-body bg-light">
                                    

                                        <div class="content-header">
                                            <h2>Ajouter un <b class="text-primary">document</b></h2>

                                        </div>

                                        <div class="col-md-10 col-md-offset-1">
                                    
                                    <?php   if (isset($_GET['id'])): ?>
                                                <input type="hidden" name="fdp_id" value="<?=$doc['fdp_id']?>"></input>
                                    <?php    endif;?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="section">
                                                        <label for="fdp_libelle" >Nom du document</label>
                                                        <input type="text" name="fdp_libelle" class="gui-input" id="fdp_libelle" placeholder="Nom du document" required 
                                                        <?php if(isset($_GET['id'])): ?>
                                                            value="<?=$doc['fdp_libelle']?>"
                                                        <?php endif; ?> >
                                                    </div>
                                                </div>
                                            </div>
                                    <?php   if(!isset($_GET['id'])){ ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="section-divider mb40">
                                                            <span>Type de document</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 text-center" >		
                                                        <div class="radio-custom mb5">
                                                            <input id="fdp_type_1" name="fdp_type" type="radio" value="1" <?php if($doc["fdp_type"]==1) echo("checked"); ?> >
                                                            <label for="fdp_type_1">Document fournisseur</label>
                                                        </div>
                                                        <small>Le document sera géré au niveau de la fiche fournisseur.</small>
                                                    </div>
                                                    <div class="col-md-6 text-center" >		
                                                        <div class="radio-custom mb5">
                                                            <input id="fdp_type_2" name="fdp_type" type="radio" value="2" <?php if($doc["fdp_type"]==2) echo("checked"); ?> >
                                                            <label for="fdp_type_2">Document intervenant</label>
                                                        </div>
                                                        <small>Le document sera géré au niveau de chaque intervenant de chaque fournisseur.</small>
                                                    </div>
                                                </div>
                                    <?php   } ?>
                                            <div class="row doc-fournisseur" <?php if($doc["fdp_type"]==2) echo("style='display:none;'"); ?> >
                                                <div class="col-md-12">
                                                    <div class="section-divider mb40">
                                                        <span>Type de fournisseur</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row doc-fournisseur" <?php if($doc["fdp_type"]==2) echo("style='display:none;'"); ?> >
                                                <div class="col-md-6 text-center" >	
                                                    <div class="option-group field">
                                                        <label class="option option-dark">
                                                            <input type="checkbox" name="fdp_entreprise" id="fdp_entreprise" <?php if($doc["fdp_entreprise"]) echo("checked"); ?> >
                                                            <span class="checkbox"></span>Demander le document aux entreprises
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-center" >	
                                                    <div class="option-group field">
                                                        <label class="option option-dark">
                                                            <input type="checkbox" name="fdp_indiv" id="fdp_indiv" <?php if($doc["fdp_indiv"]) echo("checked"); ?> >
                                                            <span class="checkbox"></span>Demander le document aux auto-entrepreneurs
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row doc-fournisseur" <?php if($doc["fdp_type"]==2) echo("style='display:none;'"); ?> >
                                                <div class="col-md-12">
                                                    <div class="section-divider mb40">
                                                        <span>Réseau Si2P</span>
                                                    </div>
                                                </div>
                                            </div>                            
                                            <div class="row doc-fournisseur" <?php if($doc["fdp_type"]==2) echo("style='display:none;'"); ?> >
                                                <div class="col-md-6 text-center" >		
                                                    <div class="radio-custom mb5">
                                                        <input id="doc_reseau" name="fdp_une_seule_fois" type="radio" value="1"  <?php if($doc["fdp_une_seule_fois"]==1) echo("checked"); ?> >
                                                        <label for="doc_reseau">Demander le document une seule fois pour l'ensemble du réseau Si2P</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-center" >		
                                                    <div class="radio-custom mb5">
                                                        <input id="doc_societe" name="fdp_une_seule_fois" type="radio" value="0"  <?php if($doc["fdp_une_seule_fois"]!=1) echo("checked"); ?> >
                                                        <label for="doc_societe">Demander le document pour chaque entreprise du réseau</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="section-divider mb40">
                                                        <span>Autres</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 text-center" >
                                                    <div class="option-group field mt15">
                                                        <label class="option option-dark">
                                                            <input type="checkbox" name="fdp_obligatoire" id="fdp_obligatoire" 
                                                <?php      if(isset($_GET['id'])){
                                                                if($doc['fdp_obligatoire']) echo("checked");
                                                            } ?>
                                                            >
                                                            <span class="checkbox"></span>Document obligatoire
                                                        </label>
                                                    </div>
                                                </div>                               
                                                <div class="col-md-6">
                                                    <label for="fdp_periodicite_mois">Validité (en mois) :</label>
                                                        <div class="section">
                                                            <div class="field prepend-icon">
                                                                <input type="text" name="fdp_periodicite_mois" id="fdp_periodicite_mois" class="gui-input input-int" placeholder="Validité" 
                                                                <?php if(isset($_GET['id'])): ?>
                                                                value="<?= $doc['fdp_periodicite_mois']; ?>"
                                                                <?php endif; ?>

                                                                >
                                                                <label for="fdp_periodicite_mois" class="field-icon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </label>
                                                            </div>
                                                            <small>Saisir 0 si le document n'expire jamais</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            

                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </section>
        </section>
    </div>

    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left" >
                <a href="fournisseur_document_liste.php" class="btn btn-default btn-sm">
                    <i class="fa fa-long-arrow-left"></i>
                    Retour
                </a>
            </div>
            <div class="col-xs-6 footer-middle" ></div>
            <div class="col-xs-3 footer-right" >
                <button type="submit" class="btn btn-success btn-sm">
                    <i class='fa fa-save'></i> Enregistrer
                </button>
            </div>
        </div>
    </footer>
</form>

<?php
include "includes/footer_script.inc.php"; ?>

<script type="text/javascript">
    $(document).ready(function(){ 

        $('input[name=fdp_type]').change(function(){ 
          
            if($("#fdp_type_1").is(":checked")){
                $(".doc-fournisseur").show();
            }else{
               
                $("#fdp_entreprise").prop("checked",false);
                $("#fdp_indiv").prop("checked",false);
                $(".doc-fournisseur").hide();
                
            }

        });


    });

</script>

</body>
</html>
