<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');

	$erreur=0;
	
	$pca_id=0;
	if(!empty($_POST["pca_id"])){
		$pca_id=$_POST["pca_id"];
	};
	
	$pca_libelle="";
	if(!empty($_POST["pca_libelle"])){
		$pca_libelle=$_POST["pca_libelle"];
	}else{
		$erreur=1;
	};
	$pca_couleur="";
	if(!empty($_POST["pca_couleur"])){
		$pca_couleur=$_POST["pca_couleur"];
	};
	
	// MISE A JOUR D'UNE CASE PLANNING
	
	if($erreur==0){
		
		if($pca_id>0){
			
			$req = $Conn->prepare("UPDATE plannings_cases SET pca_libelle = :pca_libelle, pca_couleur = :pca_couleur WHERE pca_id = :pca_id");
			$req->bindParam(":pca_id",$pca_id);
			$req->bindParam(":pca_libelle",$pca_libelle);
			$req->bindParam(":pca_couleur",$pca_couleur);	
			$req->execute();
		}else{
			$req = $Conn->prepare("INSERT INTO plannings_cases (pca_libelle, pca_couleur) VALUES (:pca_libelle, :pca_couleur);");
			$req->bindParam(":pca_libelle",$pca_libelle);
			$req->bindParam(":pca_couleur",$pca_couleur);
			$req->execute();
		};
				
	};	

	header('Location: param_case_liste.php?erreur=' . $erreur);      
	
?>

