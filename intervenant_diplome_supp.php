<?php

	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "modeles/mod_diplome.php";
	include "modeles/mod_competence.php";



	$erreur=0;
	
	$ref=0;
	if(isset($_GET["ref"])){
		$ref=$_GET["ref"];
	}else{
		$erreur=1;
	}
	
	$ref_id=0;
	if(isset($_GET["ref_id"])){
		$ref_id=$_GET["ref_id"];
	}else{
		$erreur=1;
	}
	
	$diplome=0;
	if(isset($_GET["diplome"])){
		$diplome=$_GET["diplome"];
	}else{
		$erreur=1;
	}

	if($erreur==0){
		
		$erreur=delete_diplome_intervenant($ref,$ref_id,$diplome);
		
		$idi_fichier="diplome_" . $ref_id . "_" . $diplome . ".pdf";
		
		if($ref==1){
			if(file_exists("documents/utilisateurs/diplomes/" . $idi_fichier)){
				unlink ("documents/utilisateurs/diplomes/" . $idi_fichier);
			}
		}
	
	}
	if($erreur==0){
		
		// on recupere la liste de toutes les compétences affectées au diplome
		$sql="SELECT * FROM diplomes_competences WHERE dco_interne = 1 AND dco_obligatoire = 1";
		$req=$Conn->query($sql);
		$diplomes_competences = $req->fetchAll();
		
		$add_competence = 1;
		foreach($diplomes_competences as $d){
			$sql="SELECT * FROM intervenants_diplomes WHERE (idi_date_fin > NOW() OR idi_date_fin IS NULL) AND idi_ref=" . $ref . " AND idi_ref_id =" . $ref_id . " AND idi_diplome = " . $d['dco_diplome'];
			$req=$Conn->query($sql);
			$intervenants_diplomes = $req->fetch();
			// on ajoute pas la compétence
			if(!empty($intervenants_diplomes)){
				$no_add_competence[$d['dco_competence']]=1;
			}else{
				$no_add_competence[$d['dco_competence']]=0;
			}
		}

		foreach($no_add_competence as $k=>$n){
			if($n == 0){
				$req = $Conn->prepare("DELETE FROM  Intervenants_Competences WHERE ico_ref = :ico_ref AND ico_ref_id=:ico_ref_id AND ico_competence=:ico_competence");
				$req -> bindParam(":ico_ref",$ref);
				$req -> bindParam(":ico_ref_id",$ref_id);
				$req -> bindParam(":ico_competence",$k);
				$req->execute();
			}
		}
	}
	$retour=$_SESSION['retour_diplome'];
	if($erreur>0){
		if(strpos($retour,"?")===FALSE){
			$retour.="?erreur=" . $erreur; 
		}else{
			$retour.="&erreur=" . $erreur; 
		}
	}
	header("location : " . $retour);

