<?php
// ENREGISTREMENT D'UN DEVIS

include "includes/controle_acces.inc.php";
include_once("includes/connexion_soc.php");
include_once("includes/connexion.php");
include('modeles/mod_parametre.php');

$client=0;
if(isset($_POST["client"])){
	if(!empty($_POST["client"])){
		$client=intval($_POST["client"]);
	}
}
// CHERCHER LE CLIENT
$sql="SELECT * FROM Clients WHERE cli_id=" . $client;
$req=$Conn->query($sql);
$d_client=$req->fetch();

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}else{
	if(!empty($_POST['agence'])){
		$acc_agence=$_POST['agence'];
	}else{
		$acc_agence = 0;
	}
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}

$eva_societe=0;
if(isset($_POST['societe'])){
	$eva_societe=$_POST['societe'];
}

$eva_agence=0;
if(isset($_POST['agence'])){
	$eva_agence=$_POST['agence'];
}

$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
}

$acc_nom = $_SESSION['acces']['acc_prenom'] . " " . $_SESSION['acces']['acc_nom'];
// VERIF SUR LA DATE
$erreur = 0;
if(empty($_POST['eva_date_exer'])){
 $erreur_txt = "Veuillez renseigner une date d'exercice";
}else{
	$eva_date_exer = convert_date_sql($_POST['eva_date_exer']);
}



// SELECTIONNER L'ASSISTANTE

$assist_region_id = 0;
// FG 16/12/2019. Rapports GC validés uniquement par Yannick
// VERIF GC

if($d_client['cli_categorie'] == 2 && $d_client['cli_sous_categorie'] == 1){
	// CONTRAT NATIONAL

		// JOSIANE
		$assist_region_id = 73;

}else{
	$req = $Conn->prepare("SELECT uti_id, uti_nom, uti_prenom FROM Utilisateurs WHERE uti_archive = 0 AND uti_profil = 4 AND uti_societe = " . $eva_societe . " AND uti_agence = " . $eva_agence);
	$req->execute();
	$assist_region = $req->fetch();
	if(!empty($assist_region)){
		// // CAS PARTICULIER CARTE 1539
		// // INES SIMAL
		// if($assist_region['uti_id'] == 494){
		// 	//STEPHANIE REZZE
		// 	$assist_region_id = 24;
		// }elseif($assist_region['uti_id'] == 567){
		// 	// BARBARA STEPHANOU
		// 	$assist_region_id = 268;
		// }else{
		//
		// }
		$assist_region_id = $assist_region['uti_id'];
	}
}

// SELECTIONNER LE COMMERCIAL

$commercial = 0;
$req = $Conn->prepare("SELECT cso_commercial FROM Clients_Societes WHERE cso_client =" . $client . " AND cso_societe =" . $_SESSION['acces']['acc_societe']);
$req->execute();
$cso_commercial = $req->fetch();


if(!empty($cso_commercial)){
	$req = $ConnSoc->prepare("SELECT com_id, com_label_1, com_label_2 FROM Commerciaux WHERE com_ref_1 =" . $cso_commercial['cso_commercial']);
    $req->execute();
    $cli_commercial = $req->fetch();
    $commercial = $cli_commercial['com_id'];
    $eva_com_nom = $cli_commercial['com_label_1'] . " " . $cli_commercial['com_label_2'];
}else{
	$commercial = 0;
	$eva_com_nom = "";
}

// REVENDEUR ?

$revendeur = 0;
if(!empty($_POST['eva_revendeur'])){
	$revendeur = 1;
}
// THEME
if(empty($_POST['eva_theme_id'])){
	$erreur_txt = "Merci de sélectionner un thème";
}

// DETAIL DU THEME

if(empty($_POST['eva_theme'])){
	$erreur_txt = "Merci de renseigner les détails du thème";
}

// CONTROLE ADRESSE

if(empty($_POST['adr_ad1']) OR empty($_POST['adr_nom']) OR empty($_POST['adr_cp']) OR empty($_POST['adr_ville'])){
	$erreur_txt = "Veuillez renseigner au moins une adresse, un cp et une ville";
}
// CONTROLE CONTACT
if(empty($_POST['con_nom']) OR empty($_POST['con_prenom'])){
	$erreur_txt = "Veuillez renseigner au moins un nom et prénom pour le contact";
}

if(!empty($erreur_txt)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur_txt
	);
	Header("Location: evac.php");
	die();
}

// FIN DES CONTROLES

// ENREGISTREMENT
if(empty($_POST['eva_id'])){
$sql_add="INSERT INTO Evacuations (
eva_societe,
eva_agence,
eva_commercial,
eva_com_nom,
eva_formateur,
eva_for_nom,
eva_client_societe,
eva_client_agence,
eva_client,
eva_client_code,
eva_client_nom,
eva_contact,
eva_contact_nom,
eva_contact_prenom,
eva_adresse,
eva_adr_nom,
eva_adr_service,

eva_adr_ad1,
eva_adr_ad2,
eva_adr_ad3,
eva_adr_cp,
eva_adr_ville,
eva_date_creation,
eva_date_termine,
eva_date_exer,
eva_theme_id,
eva_theme,
eva_conclusion,
eva_etat_valide,
eva_etat_controle,
eva_temps,
eva_controle_theme,
eva_controle_chrono,
eva_controle_conclusion,
eva_revendeur,
eva_assist,
eva_tech
) VALUES (
:eva_societe,
:eva_agence,
:eva_commercial,
:eva_com_nom,
:eva_formateur,
:eva_for_nom,
:eva_client_societe,
:eva_client_agence,
:eva_client,
:eva_client_code,
:eva_client_nom,
:eva_contact,
:eva_contact_nom,
:eva_contact_prenom,
:eva_adresse,
:eva_adr_nom,
:eva_adr_service,
:eva_adr_ad1,
:eva_adr_ad2,
:eva_adr_ad3,
:eva_adr_cp,
:eva_adr_ville,
:eva_date_creation,
:eva_date_termine,
:eva_date_exer,
:eva_theme_id,
:eva_theme,
:eva_conclusion,
:eva_etat_valide,
:eva_etat_controle,
:eva_temps,
:eva_controle_theme,
:eva_controle_chrono,
:eva_controle_conclusion,
:eva_revendeur,
:eva_assist,
:eva_tech);";
$req_add=$Conn->prepare($sql_add);
$req_add->bindValue(":eva_societe", $eva_societe);
$req_add->bindValue(":eva_agence", $eva_agence);
$req_add->bindValue(":eva_commercial", $commercial);
$req_add->bindValue(":eva_com_nom", $eva_com_nom);
$req_add->bindValue(":eva_formateur", $acc_utilisateur);
$req_add->bindValue(":eva_for_nom", $acc_nom);
$req_add->bindValue(":eva_client_societe", $eva_societe);
$req_add->bindValue(":eva_client_agence", $eva_agence);
$req_add->bindValue(":eva_client", $client);
$req_add->bindValue(":eva_client_code", $d_client['cli_code']);
$req_add->bindValue(":eva_client_nom", $d_client['cli_nom']);
$req_add->bindValue(":eva_contact", intval($_POST['contact']));
$req_add->bindValue(":eva_contact_nom", $_POST['con_nom']);
$req_add->bindValue(":eva_contact_prenom", $_POST['con_prenom']);
$req_add->bindValue(":eva_adresse", intval($_POST['adresse']));
$req_add->bindValue(":eva_adr_nom", $_POST['adr_nom']);
$req_add->bindValue(":eva_adr_service", $_POST['adr_service']);
$req_add->bindValue(":eva_adr_ad1", $_POST['adr_ad1']);
$req_add->bindValue(":eva_adr_ad2", $_POST['adr_ad2']);
$req_add->bindValue(":eva_adr_ad3", $_POST['adr_ad3']);
$req_add->bindValue(":eva_adr_cp", $_POST['adr_cp']);
$req_add->bindValue(":eva_adr_ville", $_POST['adr_ville']);
$req_add->bindValue(":eva_date_creation", date('Y-m-d'));
$req_add->bindValue(":eva_date_termine", null);
$req_add->bindValue(":eva_date_exer", $eva_date_exer);
$req_add->bindValue(":eva_theme_id", $_POST['eva_theme_id']);
$req_add->bindValue(":eva_theme", $_POST['eva_theme']);
$req_add->bindValue(":eva_conclusion", "");
$req_add->bindValue(":eva_etat_valide", 2);
$req_add->bindValue(":eva_etat_controle", 0);
$req_add->bindValue(":eva_temps", "");
$req_add->bindValue(":eva_controle_theme", 0);
$req_add->bindValue(":eva_controle_chrono", 0);
$req_add->bindValue(":eva_controle_conclusion", 0);
$req_add->bindValue(":eva_revendeur", $revendeur);
$req_add->bindValue(":eva_assist", $assist_region_id);
$req_add->bindValue(":eva_tech", 198);
$req_add->execute();
$id = $Conn->lastInsertId();

// C'EST UNE EDITION

$req=$Conn->query("SELECT * FROM Evacuations_Bilans_Param");
$remarques=$req->fetchAll();

foreach($remarques as $r){
	$req=$Conn->prepare(
		"INSERT INTO Evacuations_Bilans
		(ebi_bilan,ebi_evacuation,ebi_position,ebi_remarque,ebi_preconisation)
		VALUES ( :ebi_bilan
		,:ebi_evacuation,:ebi_position,
		:ebi_remarque,:ebi_preconisation )"
	);
	$req->bindValue(":ebi_bilan", $r['ebp_id']);
	$req->bindValue(":ebi_evacuation", $id);
	$req->bindValue(":ebi_position", 0);
	$req->bindValue(":ebi_remarque", $r['ebp_remarque']);
	$req->bindValue(":ebi_preconisation", $r['ebp_preconisation']);

	$req->execute();
}

}else{
	$sql_edit="UPDATE Evacuations SET
	eva_contact = :eva_contact,
	eva_contact_nom = :eva_contact_nom,
	eva_contact_prenom = :eva_contact_prenom,
	eva_adresse = :eva_adresse,
	eva_adr_nom = :eva_adr_nom,
	eva_adr_service = :eva_adr_service,
	eva_adr_ad1 = :eva_adr_ad1,
	eva_adr_ad2 = :eva_adr_ad2,
	eva_adr_ad3 = :eva_adr_ad3,
	eva_adr_cp = :eva_adr_cp,
	eva_adr_ville = :eva_adr_ville,
	eva_date_exer = :eva_date_exer,
	eva_theme_id = :eva_theme_id,
	eva_theme = :eva_theme,
	eva_revendeur = :eva_revendeur
	 WHERE eva_id = :eva_id;";
	$req_edit=$Conn->prepare($sql_edit);
	$req_edit->bindValue(":eva_contact", $_POST['contact']);
	$req_edit->bindValue(":eva_contact_nom", $_POST['con_nom']);
	$req_edit->bindValue(":eva_contact_prenom", $_POST['con_prenom']);
	$req_edit->bindValue(":eva_adr_nom", $_POST['adr_nom']);
	$req_edit->bindValue(":eva_adr_service", $_POST['adr_service']);
	$req_edit->bindValue(":eva_adresse", intval($_POST['adresse']));
	$req_edit->bindValue(":eva_adr_ad1", $_POST['adr_ad1']);
	$req_edit->bindValue(":eva_adr_ad2", $_POST['adr_ad2']);
	$req_edit->bindValue(":eva_adr_ad3", $_POST['adr_ad3']);
	$req_edit->bindValue(":eva_adr_cp", $_POST['adr_cp']);
	$req_edit->bindValue(":eva_adr_ville", $_POST['adr_ville']);
	$req_edit->bindValue(":eva_date_exer", $eva_date_exer);
	$req_edit->bindValue(":eva_theme_id", $_POST['eva_theme_id']);
	$req_edit->bindValue(":eva_theme", $_POST['eva_theme']);
	$req_edit->bindValue(":eva_id", $_POST['eva_id']);
	$req_edit->bindValue(":eva_revendeur", $revendeur);
	$req_edit->execute();
	$id = $_POST['eva_id'];
}






$_SESSION['message'][] = array(
	"titre" => "Succès",
	"type" => "success",
	"message" => "Votre rapport a été enregistré"
);
Header("Location: evac_voir.php?id=" . $id);
die();
