 <?php

	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");
	include_once("includes/connexion_fct.php");
    include_once("modeles/mod_parametre.php");
	// EXTRACTION DES DONNEES POUR BPF

	$erreur_txt="";

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	$_SESSION["retour_action"]="bpf.php";

	if(!empty($_POST)){

		if(!empty($_POST["bpf_deb"])){
			$bpf_deb=$_POST["bpf_deb"];
		}
		if(!empty($_POST["bpf_fin"])){
			$bpf_fin=$_POST["bpf_fin"];
		}

		$bpf_tableau=1;
		if(!empty($_POST["bpf_tableau"])){
			$bpf_tableau=$_POST["bpf_tableau"];
		}

		$_SESSION["bpf"]=array(
			"bpf_deb" => $bpf_deb,
			"bpf_fin" => $bpf_fin,
			"bpf_tableau" => $bpf_tableau
		);

	}elseif(isset($_SESSION["bpf"])){

		$bpf_deb=$_SESSION["bpf"]["bpf_deb"];
		$bpf_fin=$_SESSION["bpf"]["bpf_fin"];
		$bpf_tableau=$_SESSION["bpf"]["bpf_tableau"];

	}else{

		if(intval(date("m"))<4){

			$exercice=intval(date("Y"))-2;



		}else{
			$exercice=intval(date("Y"))-1;
		}
		$bpf_deb="01/04/" . $exercice;
		$bpf_fin="31/03/" . intval($exercice+1);
		$bpf_tableau=1;
	}

	$bpf_deb_req="";
	if(!empty($bpf_deb)){
		$DT_periode_deb=date_create_from_format('d/m/Y',$bpf_deb);
		if(!is_bool($DT_periode_deb)){
			$bpf_deb_req=$DT_periode_deb->format("Y-m-d");
		}

	}
	$bpf_fin_req="";
	if(!empty($bpf_fin)){
		$DT_periode_fin=date_create_from_format('d/m/Y',$bpf_fin);
		if(!is_bool($DT_periode_fin)){
			$bpf_fin_req=$DT_periode_fin->format("Y-m-d");
		}

	}
	if(empty($bpf_deb_req) OR empty($bpf_fin_req)){

		$erreur_txt="Vous devez selectionner une période.";

	}


	if(empty($erreur_txt)){

		// PRODUITS ELIGIBLES BPF

		$liste_pro_bpf="";
		$sql="SELECT pro_id FROM Produits WHERE pro_deductible ORDER BY pro_id;";
		$req=$Conn->query($sql);
		$src_pro=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($src_pro)){
			$tab_pro=array_column ($src_pro,"pro_id");
			$liste_pro_bpf=implode($tab_pro,",");
		}else{
			$erreur_txt="Aucun produit n'est éligible au BPF";
		}

	}


	if(empty($erreur_txt)){

		if($bpf_tableau==1 OR $bpf_tableau==3){

			//note
			// act_date_fin n'est pas fonctionnel

			$donnee=array();
			$donnee_client=array();

			$action_client=null;

			$sql_cli_get="SELECT cli_code,cli_nom,cli_ape,cli_classification,cli_classification_type,cli_classification_categorie,cli_interco_soc
			,ape_code
			,ccl_libelle
			,ccc_libelle
			,cct_code
			,cca_libelle
			,csc_libelle
			FROM clients
			LEFT JOIN Ape ON (clients.cli_ape=Ape.ape_id)
			LEFT JOIN clients_classifications ON (clients.cli_classification=clients_classifications.ccl_id)
			LEFT JOIN clients_classifications_categories ON (clients.cli_classification_categorie=clients_classifications_categories.ccc_id)
			LEFT JOIN clients_classifications_types ON (clients.cli_classification_type=clients_classifications_types.cct_id)
			LEFT JOIN clients_categories ON (clients.cli_categorie=clients_categories.cca_id)
			LEFT JOIN clients_sous_categories ON (clients.cli_sous_categorie=clients_sous_categories.csc_id)
			WHERE cli_id=:client;";
			$req_cli_get=$Conn->prepare($sql_cli_get);


			// DONNEE FINANCIERE

			$sql="SELECT fac_date,fac_numero,fac_cli_nom,fac_client,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date_fr
			,fli_code_produit,fli_montant_ht
			,int_type
			,COUNT(pda_id) AS nb_demi_j
			,acl_id,acl_action
			,total_demi_j";
			if($bpf_tableau==1){
				$sql.=",int_id,int_label_1,int_label_2";
			}
			$sql.=" FROM Factures
			INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
			INNER JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id AND Factures_Lignes.fli_action_cli_soc=" . $acc_societe . ")
			INNER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
			INNER JOIN Plannings_Dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
			INNER JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)
			INNER JOIN (SELECT COUNT(acd_date) AS total_demi_j,acd_action_client FROM Actions_Clients_Dates GROUP BY acd_action_client) AS Actions_Dates ON (Actions_Clients.acl_id=Actions_Dates.acd_action_client)";

			$sql.=" WHERE fli_produit IN (" . $liste_pro_bpf . ")";
			$sql.=" AND fac_date>='" . $bpf_deb_req . "' AND fac_date<='" . $bpf_fin_req . "'";
			if($bpf_tableau==1){
				$sql.=" GROUP BY fac_id,fac_date,fac_numero,fac_cli_nom,fac_client,fac_date_fr,fli_code_produit,int_type,int_id,acl_id,acl_action,fli_montant_ht";
			}else{
				$sql.=" GROUP BY fac_id,fac_date,fac_numero,fac_cli_nom,fac_client,fac_date_fr,fli_code_produit,int_type,acl_id,acl_action,fli_montant_ht";
			}
			$sql.=" ORDER BY fac_date,fac_id";
			$req=$ConnSoc->query($sql);
			$factures=$req->fetchAll();


		}elseif($bpf_tableau==2){


			$acl_id=null;
			$ass_session=null;
			$ast_stagiaire=null;
			$for_id=null;

			$donnee_intervenant=array();

			$sql="SELECT act_id,acl_ca,acl_id,ast_stagiaire,ass_session,ase_h_deb,ase_h_fin,Intervenants.int_type
			,fac_nb_stagiaires,for_id
			FROM Actions_Clients
			INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
			LEFT JOIN Actions_Stagiaires ON (Actions_Clients.acl_id=Actions_Stagiaires.ast_action_client)
			LEFT JOIN Actions_Stagiaires_Sessions ON (Actions_Stagiaires.ast_stagiaire=Actions_Stagiaires_Sessions.ass_stagiaire AND Actions.act_id=Actions_Stagiaires_Sessions.ass_action)
			LEFT JOIN Actions_Sessions ON (Actions_Stagiaires_Sessions.ass_session=Actions_Sessions.ase_id)
			LEFT JOIN Plannings_Dates ON (Actions_Sessions.ase_date=Plannings_Dates.pda_id)
			LEFT JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)
			LEFT JOIN Formations_Actions ON (Actions_Clients.acl_id=Formations_Actions.fac_action_client)
			LEFT JOIN Formations ON (Formations_Actions.fac_formation=Formations.for_id)
			LEFT JOIN Intervenants AS Rapports_Intervenants ON (Formations.for_intervenant=Rapports_Intervenants.int_id)
			WHERE Actions_Clients.acl_produit IN (" . $liste_pro_bpf . ")
			AND Actions.act_date_deb>='" . $bpf_deb_req . "' AND Actions.act_date_deb<='" . $bpf_fin_req . "'
			AND NOT act_archive AND NOT acl_archive
			ORDER BY act_date_deb,act_id,acl_id,ast_stagiaire,ass_session;";
			$req=$ConnSoc->query($sql);
			$resultats=$req->fetchAll(PDO::FETCH_ASSOC);

			/*echo("<pre>");
				print_r($resultats);
			echo("</pre>");*/
			//die();
			foreach($resultats as $r){
				
				

				if($r["acl_id"]!=$acl_id){

					// nouvelle action client

					$acl_id=$r["acl_id"];
					$ast_stagiaire=null;
					$ass_session=null;


					$donnee_intervenant[$acl_id]=array(
                        "acl_id" =>$acl_id,
                        "act_id" => $r['act_id'],
						"nb_demi_j" => 0,
						"stagiaires" => array(),
                        "ca" => $r['acl_ca']
					);
				}
				
				// il y a des inscrits 
				if(!empty($r["ast_stagiaire"])){
					
					

					if($r["ast_stagiaire"]!=$ast_stagiaire){
						// nouveau stagiaire

						$ast_stagiaire=$r["ast_stagiaire"];
						$ass_session=null;

						$donnee_intervenant[$acl_id]["stagiaires"][$ast_stagiaire]=array(
							"nb_demi_j" => 0,
							"nb_h_salarie" => 0,
							"nb_h_interco" => 0,
							"nb_h_st" => 0,
							"nb_h_autre" => 0,
							"nb_sta" => 1
						);
					}

					if($r["ass_session"]!=$ass_session){
						
						$ass_session=$r["ass_session"];

						// nouvelle session

						$donnee_intervenant[$acl_id]["nb_demi_j"]++;
						$donnee_intervenant[$acl_id]["stagiaires"][$ast_stagiaire]["nb_demi_j"]++;
						
						
						if(!empty($r['ase_h_deb']) && !empty($r['ase_h_fin'])){
							$nb_minutes=nb_minutes(time_sql($r['ase_h_deb']),time_sql($r['ase_h_fin']));
						}else{
							$nb_minutes=210;
						}
						
						//var_dump($nb_minutes);
					
						if($r['int_type'] == 1){						
							 $donnee_intervenant[$acl_id]["stagiaires"][$ast_stagiaire]["nb_h_salarie"]+=$nb_minutes;
						}elseif($r['int_type'] == 2){
							$donnee_intervenant[$acl_id]["stagiaires"][$ast_stagiaire]["nb_h_interco"]+=$nb_minutes;             
						}elseif($r['int_type'] == 3){
							$donnee_intervenant[$acl_id]["stagiaires"][$ast_stagiaire]["nb_h_st"]+=$nb_minutes;             
						}else{
							$donnee_intervenant[$acl_id]["stagiaires"][$ast_stagiaire]["nb_h_autre"]+=$nb_minutes;             
						}
					}


				}else{
					
					/*if($r["for_id"]!=$for_id){
						$for_id=$r["for_id"];
					}*/
					
				}
				
			}
			
		}

	}
	//die();

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<!-- PERSO Si2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

	   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
			<div id="main">
    <?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">

						<div class="row" >
							<div class="col-md-offset-2 col-md-8" >
								<div class="panel" >
									<div class="panel-heading panel-head-sm">Période du BPF</div>
									<div class="panel-body">

										<form method="post" action="bpf.php" >
											<div class="admin-form theme-primary ">
												<div class="row" >
													<div class="col-md-3" >
														<label for="bpf_deb" >Date de début :</label>
														<span  class="field prepend-icon">
															<input type="text" id="bpf_deb" name="bpf_deb" class="gui-input datepicker" placeholder="BPF du" value="<?=$bpf_deb?>" />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
													</div>
													<div class="col-md-3" >
														<label for="bpf_fin" >Date de fin :</label>
														<span  class="field prepend-icon">
															<input type="text" id="bpf_fin" name="bpf_fin" class="gui-input datepicker" placeholder="au" value="<?=$bpf_fin?>" />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
													</div>
													<div class="col-md-3" >
														<label for="bpf_fin" >Tableau :</label>
														<span class="field select">
															<select id="bpf_tableau" name="bpf_tableau" >
																<option value="1" <?php if($bpf_tableau==1) echo("selected") ?> >Tableau 1</option>
																<option value="2" <?php if($bpf_tableau==2) echo("selected") ?> >Tableau 2</option>	
																<option value="3" <?php if($bpf_tableau==3) echo("selected") ?> >Tableau 3</option>																
															</select>
															<i class="arrow simple"></i>
														</span>

													</div>
													<div class="col-md-3 pt15 text-center" >
														<button type="submit" class="btn btn-info" >
															Afficher les donées
														</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>

				<?php	if(empty($erreur_txt)){

							if($bpf_tableau==1 OR $bpf_tableau==3){

								if(!empty($factures)){ ?>

									<h1>BPF du <?=$bpf_deb?> au <?=$bpf_fin?></h1>

									<h2>Bilan financier</h2>

									<div class="table-responsive" >
										<table class="table table-striped table-hover" id="tabAction" data-tri_col="7" data-tri_sens="asc">
											<thead id="tabRappelHead" >
												<tr class="dark" >
													<th>Date de facture</th>
													<th>N° de facture</th>
													<th>Code client</th>
													<th>Catégorie client</th>
													<th>Sous-catégorie client</th>
													<th>INTERCO</th>
													<th>Classification</th>
													<th>Type</th>
													<th>Catégorie</th>
													<th>APE</th>
													<th>Produit</th>
													<th>Nom de facturation</th>
													<th>Formation</th>
													<th>Type d'intervenant</th>
											<?php	$total_cols=14;
													if($bpf_tableau==1) {
														$total_cols=15;
														echo("<th>Nom de l'intervenant</th>");
													} ?>
													<th>CA</th>
												</tr>
											</thead>
											<tbody>
									<?php		$total_ca=0;
												foreach($factures as $f){

													if(empty($donnee_client[$f["fac_client"]])){
														// on va recupérer les données clients en base 0
														$req_cli_get->bindParam("client",$f["fac_client"]);
														$req_cli_get->execute();
														$d_client=$req_cli_get->fetch();
														if(!empty($d_client)){
															$donnee_client[$f["fac_client"]]=$d_client;

														}
													}

													switch ($f["int_type"]) {
														case 1:
															$int_type="Salarié";
															break;
														case 2:
															$int_type="Interco";
															break;
														case 3:
															$int_type="Sous-Traitant";
															break;
														default:
														  $int_type="Autre";
													}
													$ca=0;
													if(!empty($f["total_demi_j"])){
														$ca=$f["fli_montant_ht"]*($f["nb_demi_j"]/$f["total_demi_j"]);
													}
													$total_ca=$total_ca+$ca;


													?>
													<tr>
														<td><?=$f["fac_date_fr"]?></td>
														<td><?=$f["fac_numero"]?></td>
														<td><?=$donnee_client[$f["fac_client"]]["cli_code"]?></td>
														<td><?=$donnee_client[$f["fac_client"]]["cca_libelle"]?></td>
														<td><?=$donnee_client[$f["fac_client"]]["csc_libelle"]?></td>
														<td>
													<?php	if(!empty($donnee_client[$f["fac_client"]]["cli_interco_soc"])){
																echo("OUI");
															} ?>
														</td>
														<td><?=$donnee_client[$f["fac_client"]]["ccl_libelle"]?></td>
														<td><?=$donnee_client[$f["fac_client"]]["cct_code"]?></td>
														<td><?=$donnee_client[$f["fac_client"]]["ccc_libelle"]?></td>
														<td><?=$donnee_client[$f["fac_client"]]["ape_code"]?></td>
														<td><?=$f["fli_code_produit"]?></td>
														<td><?=$f["fac_cli_nom"]?></td>
														<td>
															<a href="action_voir.php?action=<?=$f["acl_action"]?>&action_client=<?=$f["acl_id"]?>" >
																<?=$f["acl_action"] . "-" . $f["acl_id"]?>
															</a>
														</td>
														<td><?=$int_type?></td>
												<?php	if($bpf_tableau==1) {
															echo("<td>" . $f["int_label_1"] . " " . $f["int_label_2"] . "</td>");
														} ?>
														<td class="text-right" ><?=number_format($ca,2,","," ")?></td>
													</tr>
									<?php		} ?>
											</tbody>
											<tfoot>
												<tr>
													<th colspan="<?=$total_cols?>" >Total :</th>
													<td class="text-right" ><?=number_format($total_ca,2,","," ")?></td>
												</tr>
											</tfoot>
										</table>
									</div>

					<?php		}else{ ?>
									<p class="alert alert-warning" >
										Aucune donnée.
									</p>
					<?php		}

							}elseif($bpf_tableau==2){

								if(!empty($donnee_intervenant)){ ?>

									<h1>BPF du <?=$bpf_deb?> au <?=$bpf_fin?></h1>

									<h2>Données liées aux intervenants</h2>

									<div class="table-responsive" >
										<table class="table table-striped table-hover" >
											<thead id="tabRappelHead" >
												<tr class="dark" >
                                                    <th>ID Action</th>
                                                    <th>ID Action Client</th>
													<th>Nb. sta.</th>
													<th>CA</th>
													<th>Nb H salarié</th>
													<th>Nb H interco</th>
													<th>Nb H sous-traitant</th>
													<th>Nb H autre</th>
												</tr>
											</thead>
											<tbody>
									<?php		foreach($donnee_intervenant as $action){ 
											
													foreach($action["stagiaires"] as $sta){?>
														<tr>
															<td><?= $action['act_id'] ?></td>
															<td>
																<!--<a href="action_voir.php?action=<?=$action['act_id']?>&action_client=<?=$action['acl_id']?>" >-->
																	<?= $action['acl_id'] ?>
																<!--</a>-->
															</td>
															<td><?=$sta["nb_sta"]?></td>
															<td>
												<?php			if(!empty($action["nb_demi_j"])){
																	echo(($action["ca"]*$sta["nb_demi_j"])/$action["nb_demi_j"]);
																} ?>
															</td>
															<td><?=hoursandmins($sta['nb_h_salarie'])?></td>
															<td><?=hoursandmins($sta['nb_h_interco'])?></td>
															<td><?=hoursandmins($sta['nb_h_st'])?></td>
															<td><?=hoursandmins($sta['nb_h_autre'])?></td>
														</tr>
										<?php		} 
												} ?>
											</tbody>
										</table>
									</div>

					<?php		}else{ ?>
									<p class="alert alert-warning" >
										Aucune donnée.
									</p>
					<?php		}
							}

						}else{
							echo("<p class='alert alert-danger' >" . $erreur_txt . "</p>");
						} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="parametre.php" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i> Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>


<?php
		include "includes/footer_script.inc.php"; ?>

		<script type="text/javascript">

			jQuery(document).ready(function (){

			});


		</script>
	</body>
</html>
