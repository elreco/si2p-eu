<?php

	//LISTE DES VARIATIONS DE PRIX DANS UNE BASE PRODUITS
	
	include "includes/controle_acces.inc.php";
	
	include_once("includes/connexion.php");

	include('modeles/mod_orion_utilisateur.php');
	

	// $client correspond à l'ID du client consulter // différent de cpr_client qui est l'ID du client qui mémorise la base produit
	$client=0;
	if(!empty($_GET['client'])){
		$client=intval($_GET['client']);
	}
	if($client==0){
		echo("Erreur : client inconnu !");
		die();	
	}
	
	$produit=0;
	if(!empty($_GET['produit'])){
		$produit=intval($_GET['produit']);
	}
	
	
	// SUR LE CLIENT 
	
	$cph_client=$client;
	$req=$Conn->query("SELECT cli_nom,cli_categorie,cli_sous_categorie,cca_gc,cca_libelle,cli_groupe,cli_filiale_de FROM Clients,Clients_Categories WHERE cli_categorie=cca_id AND cli_id=" . $client . ";");
	$d_client=$req->fetch();
	if(!empty($d_client)){
		$lib_groupe=$d_client["cli_nom"];
		if($d_client["cli_groupe"] AND $d_client["cli_filiale_de"]>0){
			$cph_client=$d_client["cli_filiale_de"];
		}
	}else{
		echo("Erreur : client inconnu !");
		die();	
	}
	
	// sur la MM
	if($cph_client!=$client){
		$req=$Conn->query("SELECT cli_nom FROM Clients WHERE cli_id=" . $cph_client . ";");
		$result=$req->fetch();
		if(!empty($result)){
			$lib_groupe=$result["cli_nom"];
		}	
	}
	
	
	$utilisateurs=orion_utilisateurs();
	
	// HISTORIQUE DES PRODUITS
	
	$sql="SELECT pro_id,pro_code_produit,cpr_intra,cpr_ht_intra,cpr_inter,cpr_ht_inter,cpr_derogation,cpr_nego
	,cph_utilisateur,cph_date,cph_intra,cph_ht_intra,cph_inter,cph_ht_inter,cph_derogation,cph_nego
	FROM Produits,Clients_Produits,Clients_Produits_Histo WHERE pro_id=cpr_produit AND cpr_client=" . $cph_client . " AND pro_id=cph_produit AND cph_client=" . $cph_client;
	if(!empty($produit)){
		$sql.=" AND cpr_produit=" . $produit;
	}
	$sql.=" ORDER BY pro_code_produit,pro_id,cph_date DESC,cph_id DESC;";
	$req=$Conn->query($sql);
	$d_produits=$req->fetchAll();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css" >
		
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	   
	   <!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
	
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" class="" >
					
					<header id="topbar">
						<h4><small>Historique des prix appliqués pour</small> <b><?=$lib_groupe?></b></h4>
					</header>
					
					<section id="content" class="animated fadeIn">
				<?php	if(!empty($d_produits)){ ?>
							<table class="table table-striped table-hover datatable" >
								<thead>								
									<tr class="dark" >
										<th rowspan="2" >Produit</th>
										<th rowspan="2" >Changé par</th>
										<th rowspan="2" >Changé le</th>
										<th colspan="2" class="text-center" >INTRA</th>
										<th colspan="2" class="text-center" >HT INTRA</th>
										<th colspan="2" class="text-center" >INTER</th>
										<th colspan="2" class="text-center" >HT INTER</th>
										<th colspan="2" class="text-center" >Dérogation</th>
										<th colspan="2" class="text-center" >Négociation</th>										
									</tr>
									<tr>
										<th class="text-center" >Avant</th>
										<th class="text-center">Après</th>
										<th class="text-center" >Avant</th>
										<th class="text-center">Après</th>
										<th class="text-center" >Avant</th>
										<th class="text-center">Après</th>
										<th class="text-center" >Avant</th>
										<th class="text-center">Après</th>
										<th class="text-center" >Avant</th>
										<th class="text-center">Après</th>
										<th class="text-center" >Avant</th>
										<th class="text-center">Après</th>
									</tr>
								</thead>
								<tbody>
						<?php		$produit=0;
									foreach($d_produits as $cle_p => $dp){

										$uti_lib="";
										if(!empty($dp["cph_utilisateur"])){
											$uti_lib=$utilisateurs[$dp["cph_utilisateur"]]["identite"];
										}										
										$date_lib="";
										if(!empty($dp["cph_date"])){
											$date = date_create_from_format('Y-m-d',$dp["cph_date"]);
											$date_lib=$date->format("d/m/Y");
										} 
										
										$intra_lib="NON";
										if(!empty($dp["cph_intra"])){
											$intra_lib="OUI";
										}
										$inter_lib="NON";
										if(!empty($dp["cph_inter"])){
											$inter_lib="OUI";
										}
										$derogation_lib="Aucune";
										if(!empty($dp["cph_derogation"])){
											$derogation_lib="Niv. " . $dp["cph_derogation"];
										}
										$nego_lib="NON";
										if(!empty($dp["cph_nego"])){
											$nego_lib="OUI";
										}
										
										$ap_intra_lib="NON";
										$ap_inter_lib="NON";
										$ap_derogation_lib="Aucune";
										$ap_nego_lib="NON";
										
										if($dp["pro_id"]!=$produit){ 
											$produit=$dp["pro_id"];											
											$ap_ht_intra=$dp["cpr_ht_intra"];											
											$ap_ht_inter=$dp["cpr_ht_inter"];																													
											if(!empty($dp["cpr_intra"])){
												$ap_intra_lib="OUI";
											}										
											if(!empty($dp["cpr_inter"])){
												$ap_inter_lib="OUI";
											}										
											if(!empty($dp["cpr_derogation"])){
												$ap_derogation_lib="Niv. " . $dp["cpr_derogation"];
											}
											if(!empty($dp["cpr_derogation"])){
												$ap_nego_lib="OUI";
											}											
										}else{
											$ap_ht_intra=$d_produits[$cle_p-1]["cph_ht_intra"];
											$ap_ht_inter=$d_produits[$cle_p-1]["cph_ht_inter"];
											if(!empty($d_produits[$cle_p-1]["cph_intra"])){
												$ap_intra_lib="OUI";
											}										
											if(!empty($d_produits[$cle_p-1]["cph_inter"])){
												$ap_inter_lib="OUI";
											}										
											if(!empty($d_produits[$cle_p-1]["cph_derogation"])){
												$ap_derogation_lib="Niv. " . $d_produits[$cle_p-1]["cph_derogation"];
											}	
											if(!empty($d_produits[$cle_p-1]["cph_nego"])){
												$ap_nego_lib="OUI";
											}
										} ?>
							
										<tr>
											<td><?=$dp["pro_code_produit"]?></td>
											<td><?=$uti_lib?></td>
											<td><?=$date_lib?></td>
											<td class="text-center" ><?=$intra_lib?></td>
											<td class="text-center" ><?=$ap_intra_lib?></td>											
											<td class="text-right" ><?=$dp["cph_ht_intra"]?> €</td>
											<td class="text-right" ><?=$ap_ht_intra?> €</td>
											<td class="text-center" ><?=$inter_lib?></td>
											<td class="text-center" ><?=$ap_inter_lib?></td>	
											<td class="text-right" ><?=$dp["cph_ht_inter"]?> €</td>
											<td class="text-right" ><?=$ap_ht_inter?> €</td>	
											<td class="text-center" ><?=$derogation_lib?></td>
											<td class="text-center" ><?=$ap_derogation_lib?></td>
											<td class="text-center" ><?=$nego_lib?></td>
											<td class="text-center" ><?=$ap_nego_lib?></td>
										</tr>
						<?php		} ?>
								</tbody>
							</table>
				<?php	} ?>		
					</section>				
				</section>
			</div>
			
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-4 footer-left" >
						<a href="client_voir.php?client=<?=$client?>&onglet=13" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-4 footer-middle" ></div>
					<div class="col-xs-4 footer-right" ></div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="/vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" >
			
		</script>			
	</body>
</html>
