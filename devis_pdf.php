<?php


setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

//include('modeles/mod_get_droit_utilisateur.php');
include('modeles/mod_get_juridique.php');

// VISUALISATION D'UN DEVIS EN PDF

$erreur=0;

$devis_id=0;
if(!empty($_GET["devis"])){
	$devis_id=intval($_GET["devis"]);
}


$pdf=false;
if(!empty($_GET["pdf"])){
	$pdf=boolval($_GET["pdf"]);
}

// la personne logue
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}

$acc_drt_base_client=true;
/*if($_SESSION['acces']['acc_ref']==1){
	$acc_utilisateur=intval($_SESSION['acces']['acc_ref_id']);
	if(!empty(get_droit_utilisateur(6,$_SESSION['acces']['acc_ref_id']))){
		$acc_drt_base_client=true;
	}
}*/

// recherche du devis
if($devis_id>0){

	$sql="SELECT * FROM Devis WHERE dev_id=" . $devis_id;
	if($acc_agence>0){
		$sql.=" AND dev_agence=" . $acc_agence;
	}
	if(!$acc_drt_base_client){
		$sql.=" AND dev_utilisateur=" . $acc_utilisateur;
	}
	$req=$ConnSoc->query($sql);
	$devis=$req->fetch();
	if(empty($devis)){
		$erreur=1;
	}
}

if($erreur==0){

	// le devis peut etre affiche
	// recherche des infos necessaire
	
	$base_civilite=array(
		0 => "&nbsp;",
		1 => "Monsieur",
		2 => "Madame"
	);
	
	$juridique=get_juridique($acc_societe,$devis["dev_agence"]);
	if(!is_array($juridique)){
		$erreur=1;
	}
	
	$dateTime_dev_date=date_create_from_format('Y-m-d',$devis["dev_date"]);
	$date_dev_date=strftime("%A %#d %B %Y",$dateTime_dev_date->getTimestamp());

	// Si lié à un utilisateur 
	if(!empty($devis["dev_utilisateur"])){
		
		$sql="SELECT uti_tel,uti_mobile,uti_fax,uti_mail FROM Utilisateurs WHERE uti_id=" . $devis["dev_utilisateur"] . ";";
		$req=$Conn->query($sql);
		$utilisateur=$req->fetch();
		if(!empty($utilisateur)){
			if(!empty($utilisateur["uti_mobile"])){
				$com_mobile=$utilisateur["uti_mobile"];
			}
			if(!empty($utilisateur["uti_tel"])){
				$juridique["tel"]=$utilisateur["uti_tel"];
			}
			if(!empty($utilisateur["uti_fax"])){
				$juridique["fax"]=$utilisateur["uti_fax"];
			}
			if(!empty($utilisateur["uti_mail"])){
				$juridique["mail"]=$utilisateur["uti_mail"];
			}
		}
		
	}

}

ob_start(); ?>

<style type="text/css" >

	table{
		width:100%;
		color:#333333;
		font-size:11pt;
		font-family:helvetica;
		line-height:6mm;
		border-collapse:collapse;
	}
	.text-right{
		text-align:right;
	}
	.text-center{
		text-align:center;
	}
	
	table.border{
		
	}
	table.border th{
		background-color:#333333;
		color:#FFFFFF;
		text-align:center;
		border:1px solid #FFFFFF;
	}
	table.border td{
		border:1px solid #7e8082;
	}
	
	h3{
		font-size:12pt;
	}
	.cadre{
		border:1px solid #333333;
	}
	
	.juridique{
		font-size:7pt;
		line-height:3mm;
		text-align:center;
	}
	
	
	h4{
		font-size:12pt;
		margin:0px;
		
	}
	h5{
		font-size:8pt;
		color:#E31936;
		margin:5px 0px 0px 0px;
	}
	h6{
		font-size:8pt;
		margin:5px 0px 0px 0px;
	}
	.cgv-intro{
		color:#E31936;
		font-size:8pt;
		text-align:justify;
		margin:5px 0px 0px 0px;
	}
	.cgv{
		text-align:justify;
		font-size:7pt;
		margin:5px 0px 0px 0px;
	}
	
	
</style>

<page backtop="30mm" backleft="5mm" backright="5mm" backbottom="15mm" >
	<page_header> 
		<table>
			<tr>
				<td style="width:30%;" >
		<?php		if(!empty($juridique["logo_1"])){
						echo("<img style='max-width:100%;' src='" . $juridique["logo_1"] . "' />");
					} ?>	
				</td>
				<td style="width:40%;" ><h1 class="text-center" >DEVIS</h1></td>
				<td style="width:30%;" class="text-right" >
		<?php		if(!empty($juridique["logo_1"])){
						echo("<img style='max-width:100%;' src='" . $juridique["logo_1"] . "' />");
					} ?>
				</td>
			</tr>
		</table>            
    </page_header> 
	
	<page_footer>
		<hr/>
		<table>
			<tr>
				<td style="width:15%;" >
		<?php		if(!empty($juridique["qualite_1"])){
						echo("<img style='max-width:100%;' src='" . $juridique["qualite_1"] . "' />");
					} ?>	
				</td>
				<td style="width:70%;" class="juridique" ><?=$juridique["footer"]?></td>
				<td style="width:15%;" class="text-right" >
		<?php		if(!empty($juridique["qualite_2"])){
						echo("<img style='max-width:100%;' src='" . $juridique["qualite_2"] . "' />");
					} ?>
				</td>
			</tr>
		</table>			
	</page_footer>
	
	<table>
		<tr>
			<td style="width:70%;" >
	<?php		echo("<strong>" . $juridique["nom"] . "</strong>");
				if(!empty($juridique["agence"])){
					echo("<br/><strong>Agence " . $juridique["agence"]. "</strong>");	
				}
				if(!empty($juridique["ad1"])){
					echo("<br/>" . $juridique["ad1"]);	
				}
				if(!empty($juridique["ad2"])){
					echo("<br/>" . $juridique["ad2"]);	
				}
				if(!empty($juridique["ad3"])){
					echo("<br/>" . $juridique["ad3"]);	
				}
				echo("<br/>" . $juridique["cp"] . " " . $juridique["ville"]);
				echo("<br/>" . $juridique["mail"]);
				echo("<br/>" . $juridique["site"]);
				echo("<br/>Affaire suivi par : " . $devis["dev_com_identite"]);
				if(!empty($com_mobile)){
					echo("<br/><i class='fa fa-mobile' ></i> " . $com_mobile);	
				} 
				if(!empty($devis["dev_cli_code"])){
					echo("<br/>Code client : " . $devis["dev_cli_code"]);	
				}?>
			</td>
			<td style="width:30%;" >
	<?php
				echo("<strong>" . $devis["dev_adr_nom"] . "</strong>");

				if(!empty($devis["dev_con_nom"])){
					echo("<br/>A l'attention de ");
					if(!empty($devis["dev_con_titre"])){
						echo($base_civilite[$devis["dev_con_titre"]] . " ");	
					}
					if(!empty($devis["dev_con_nom"])){
						echo($devis["dev_con_nom"] . " ");	
					}
					echo($devis["dev_con_nom"]);
				} 
				if(!empty($devis["dev_adr_service"])){
					echo("<br/>" . $devis["dev_adr_service"]);	
				}
				if(!empty($devis["dev_adr_ad1"])){
					echo("<br/>" . $devis["dev_adr_ad1"]);	
				}
				if(!empty($devis["dev_adr_ad2"])){
					echo("<br/>" . $devis["dev_adr_ad2"]);	
				}
				if(!empty($devis["dev_adr_ad3"])){
					echo("<br/>" . $devis["dev_adr_ad3"]);	
				}												
				echo("<br/>" . $devis["dev_adr_cp"] . " " . $devis["dev_adr_ville"]); 

				?>
			</td>
		</tr>
	</table>
	<table>
		<tr>
			<td style="width:50%;" >
				<strong>Numéro : <?=$devis["dev_numero"]?></strong>
			</td>
			<td class="text-right" style="width:50%;" >
				Le <?=$date_dev_date?>	
			</td>
		</tr>
	</table>
	
	<table class="border" >
		<thead>
			<tr>
				<th style="width:20%;" >Référence</th>
				<th style="width:46%;" >Désignation</th>
				<th style="width:14%;" >Prix HT</th>
				<th style="width:6%;" >Qté</th>
				<th style="width:14%;" >Montant HT</th>
				
			</tr>
		</thead>	
		<tbody>
	<?php	for($bcl=1;$bcl<11;$bcl++){ ?>
				<tr>
					<td style="width:20%;" >PEMP3BEXPINTRA</td>
					<td style="width:46%;" >
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
					Sed non laoreet arcu, vitae euismod justo. 
					Praesent vulputate tincidunt orci vitae pellentesque. 
					Curabitur laoreet interdum sem. 
					Curabitur sem risus, tincidunt ac ligula in, auctor luctus nulla. 
					In feugiat metus sit amet lacus fermentum, et pulvinar magna fermentum. 
					Integer tristique est justo, sit amet pellentesque diam varius ut. 
					Morbi felis velit, faucibus vitae orci a, consequat faucibus dolor.
					</td>
					<td style="width:14%;" >100 000.00 €</td>
					<td style="width:6%;" >99.99</td>
					<td style="width:14%;" >100 000.00 €</td>
						
				</tr>
	<?php	} ?>
		</tbody>
	</table>
	
	<div style="position:absolute;bottom:10px;left:0;border:1px solid white;" >
		<table>	
			<tr>
				<td style="width:25%;" >
					<div class="cadre" >
						<h3>Condition de reglement</h3>
						Chèque 60 jours Date facture
					</div>
				</td>
				<td style="width:50%;" >
					<div class="cadre" >
						<h3>Cachet, Nom et Signature du client</h3>
					</div>
				</td>
				<td style="width:25%;" >
					<div class="cadre" >
						Validité : 3 mois
					</div>
				</td>
			</tr>
		</table>
	</div>
	
</page>

<page backtop="5mm" backleft="5mm" backright="5mm" backbottom="5mm" >

	<h4>CONDITIONS GÉNÉRALES DE VENTE GÉNÉRALITÉS</h4> 
	<p class="cgv-intro" >
		LES VENTES ET INTERVENTiONS RELATIVES AUX FOURNITURES ET SERVICES DE Si2P SONT RÉGIES PAR LES PRÉSENTES CONDITIONS 
		GÉNÉRALES, SOUS RÉSERVE D’ACCORDS PARTICULIERS OU COMMERCIAUX SPÉCIFIQUES. 
		LES CATALOGUES, NOTICES, PROSPECTUS, DÉPLIANTS ET MATERIELS EXPOSÉS NE CONSTITUENT PAS DES OFFRES FERMES DE 
		FOURNITURES ET SERVICES DE Si2P. 
		CELLE-CI SE RÉSERVE LA POSSIBILITÉ D’Y APPORTER À TOUT MOMENT LES AMÉLIORATIONS ET MODIFICATIONS QU’ELLE 
		JUGERAIT UTILES, SANS ÊTRE TENUE CEPENDANT, DE LES APPORTER AUX FOURNITURES ET/OU SERVICES, DÉJÀ LIVRÉS OU 
		EFFECTUÉS, OU EN COURS DE COMMANDE. 
		LE CONTRAT DE VENTE N’EST PARFAIT QU’APRÈS ACCEPTATION DE La COMMANDE DU CLIENT PAR Si2P. 
	</p>
	<h5>LES PRIX</h5>
	<p class="cgv" >
		Le prix des matériels, fournitures et/ou services sont ceux fixés par le tarif en vigueur au jour de la commande des matériels et/ou des fournitures ou de la 
		réalisation de la prestation. Les prix indiqués sur le tarif sont des prix hors taxes.
	</p>
	<p class="cgv" >
		Les prix de matériels, fournitures et/ou services ne comprennent pas les frais éventuels de port, déplacement, vacation, installation, taxes, recouvrement, 
		enregistrement et timbres qui sont en sus.
		Les prix des matériels, fournitures et servicessont payables à trente (30) jours date de facture par chèque ou virement bancaire.
		En cas de retard de paiement, des pénalités égales à trois (3) fois le taux d’intérêt légal en vigueur à la date de la facture seront appliqués à compter du 
		premier jour de retard.
		Ces pénalités sont exigibles de plein droit sans qu’un rappel soit nécessaire ainsi qu‘une indemnité forfaitaire pour frais de recouvrement d’un montant de 
		quarante (40) euros. 
	</p>
	<p class="cgv" >
		Dans ce cas, les effets tirés sur le client le sont à ses frais et ne constituent ni novation, ni dérogation aux autres clauses des présentes conditions générales. 
	</p>

	<h5>LA LIVRAISON</h5>
	<p class="cgv" >
		Les matériels et/ou fournitures sont livrés au mieux des possibilités de Si2P et aux frais du client. ils voyagent toujours aux risques et périls de ce dernier.
	</p> 

	<h5>SÉANCES D’INSTRUCTION</h5>

	<p class="cgv" >Conditions d’inscription et de règlement</p>

	<h6>Dispositions générales</h6> 

	<p class="cgv" >
		Les séances modulaires de formation se déroulent chez le client aux jours ouvrables, aux heures, dates et lieux fixés d’un commun accord entre le client et Si2P. 
		Chaque séance, animée par un moniteur, comprend au maximum 15 participants et dure une heure minimum. Un seul thème est traité par séance. 
		Ce thème peut être fixé soit d’avance, soit d’une séance à l’autre. Si2P s’oblige à convenir avec le client et à l’informer d’une date pour la séance, trois semaines à l’avance. 
	</p>
	<p class="cgv" >
		Le client mettra à la disposition de l’animateur de Si2P un local pouvant servir de salle de cours, le terrain, ainsi que les produits combustibles liquides nécessaires 
		à l’exercice sur feux réels. Pour sa part, Si2P délègue un moniteur et s’engage à fournir lorsque nécessaire : le matériel audiovisuel, les extincteurs 
		portatifs pour quinze participants maximum, les bacs pour les feux de combustibles liquides, le combustible gazeux pour les feux de gaz. Si2P peut également, 
		sur demande du client et selon disponibilité et tarif en vigueur, mettre un terrain à disposition de celui-ci.
	</p>

	<h6>conditions d’annulation</h6> 

	<p class="cgv" >
		Si2P arrêtera avec le client sa date d’intervention d’un commun accord. Toute demande de report ou d’annulation devra parvenir à Si2P au moins dix jours ouvrables 
		avant la date fixée. Faute de quoi, Si2P se réserve le droit de réclamer une indemnité dont le montant correspondra à 50 % de la valeur de la prestation prévue. 
		Toute annulation sur place ou le jour de la formation fera l’objet d’une facturation équivalente à la totalité du montant de la prestation.
	</p>

	<h5>LES STAGES DE FORMATION</h5>
	<h6>Formulaire d’inscription au stage de formation</h6>
	<p class="cgv" >
		Les inscriptions sont enregistrées dans leur ordre d’arrivée, et donnent lieu à un accusé de réception. Le formulaire d’inscription, daté et signé par un responsable 
		dûment habilité et portant le cachet du client, devra obligatoirement être retourné au moins trois semaines avant la date du premier stage retenu. 
		a défaut d’avoir reçu ce formulaire d’inscription accompagné de l’acompte dans les délais sus mentionnés, Si2P, en cas d’impossibilité d’accueillir les stagiaires, 
		se réserve la faculté de retarder la date de formation souhaitée par le client et de fixer en accord avec ce dernier une date ultérieure de formation. 
		Pour éviter ensuite toute erreur, le donneur d’ordre précise si son budget de formation est confié à un organisme gestionnaire (OPCa, FaF, ...).
		Sauf mention particulière, le donneur d’ordre accepte de figurer sur les listes de référence Si2P.
	</p>
	<h6>conditions d’inscription et de règlement</h6>
	<p class="cgv" >
		Si2P propose des stages de formation professionnelle dont le montant peut venir en déduction de la contribution patronale au financement de la formation professionnelle continue 
		sous l’une des réserves suivantes :
	</p> 
	<p class="cgv" >
		-l’entreprise bénéficiaire s’engage à faire participer à ce type de formation, à l’une des catégories professionnelles suivantes : chefs de sécurité ou adjoints, 
		agents de surveillance, sapeurs pompiers d’entreprise, gardiens, membres du CHSCT, équipiers de deuxième intervention.
	</p> 
	<p class="cgv" >
		-L’entreprise s’engage à intégrer la formation dispensée par Si2P comme partie d’un stage de formation professionnelle de personnel dont la fonction implique nécessairement 
		une bonne connaissance de la sécurité. Si2P peut établir une convention de formation professionnelle avec les entreprises qui lui en font la demande. 
		L’entreprise désireuse de s’inscrire à un ou des stages de formation complètera le bon de commande valant bulletin d’inscription qu’elle remettra à Si2P dûment daté,
		signé par un responsable et portant le cachet de l’entreprise. Au cas où elle fait également une demande de convention avec Si2P, elle précisera si elle conserve ou 
		si elle confie à un organisme tiers -en tout ou partie -la gestion de sa participation obligatoire. 
	</p>
	<p class="cgv" >
		Les natures, objet(s), durée(s) du ou des stage(s), le nombre des bénéficiaires, la nature de la sanction, les moyens pédagogiques, techniques mis en oeuvre, 
		ainsi que les modalités de contrôle des connaissances sont déterminés par le document général de présentation dont l’entreprise inscrite a pris connaissance. 
		L’entreprise inscrite donnera toutes facilités aux salariés bénéficiaires de la formation pour suivre celle-ci. Elle transmettra à Si2P le nom et la fonction des stagiaires, 
		au plus tard une semaine avant la date du stage. Toute inscription de l’entreprise à un stage ne sera considérée comme définitive par Si2P qu’après versement 
		des frais d’inscription correspondants.
	</p>
	
	<p class="cgv" > 
		Dans le cas où il s’avérerait que les participants inscrits soient d’un niveau ou d’une qualification professionnelle insuffisants, Si2P se réserve le droit d’en informer 
		l’entreprise et de demander le remplacement des dits candidats à la dite formation.
	</p> 
	<p class="cgv" >
		Si2P arrêtera avec le client, la date de son intervention d’un commun accord. Toute demande de report ou d’annulation devra parvenir à Si2P au moins dix jours ouvrables 
		avant la date fixée. Faute de quoi, Si2P se réserve le droit de réclamer une indemnité dont le montant correspondra à 50% de la valeur de la prestation prévue. 
		Toute annulation sur place ou le jour de la formation fera l’objet d’une facturation équivalente à la totalité du montant de la prestation. 
	</p>

	<h5>RESPONSABILITÉ CIVILE ASSURANCES</h5>

	<h6>Incidents matériels -accidents du travail</h6> 

	<p class="cgv" >
		Chacune des parties fera son affaire de sa responsabilité d’employeur vis-à-vis des dégâts matériels et/ou accidents corporels qui pourraient survenir à l’occasion 
		des séances et/ou des divers stages de formation, et ce, dans le respect des dispositions réglementaires en matière de responsabilité civile et de la législation du travail 
		sauf s’il ressortait et était démontré que la responsabilité de l’autre partie était engagée.
	</p>

	<h6>Assurances</h6> 
	<p class="cgv" >
		Si la responsabilité de Si2P était démontrée et engagée en cas d’accident matériel ou corporel, la réparation des dommages s’opérerait dans les limites fixées 
		par la police d’assurances Responsabilité Civile que Si2P aura souscrite auprès d’une compagnie notoirement solvable.
	</p>

	<h5>CONTESTATIONS</h5>

	<p class="cgv" >
		À défaut d’accord amiable, toutes contestations de quelques natures qu’elles soient, seront de la compétence exclusive des tribunaux du siège social de Si2P, 
		de convention expresse, et nonobstant toutes autres clauses attributives de juridiction, même en cas de pluralité de défendeurs ou d’appel en garantie.
	</p>

</page>


<?php

$content=ob_get_clean();


require_once(dirname(__FILE__).'/html2pdf/vendor/autoload.php');

try{
	$pdf=new HTML2PDF('P','A4','fr');
	$pdf->pdf->SetDisplayMode('fullwidth');
	//echo($content);
	$pdf->writeHTML($content);
	if($pdf){
		$pdf->Output(dirname(__FILE__) . '/Documents/Devis/Devis_' . $acc_societe . '/'. $devis["dev_numero"] . '.pdf', 'F');
		header("location : devis_voir.php?devis=" . $devis_id);
	}else{
		$pdf->Output($devis["dev_numero"] . '.pdf');	
	}
	
	
	
}catch(HTML2PDF_exception $e){
	die($e);
}

?>