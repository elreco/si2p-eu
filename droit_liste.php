<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	
	include('modeles/mod_droit.php');
	include('modeles/mod_service.php');
	include('modeles/mod_erreur.php');
	
	$filt_service=0;
	if(!empty($_POST["filt_service"])){
		$filt_service=$_POST["filt_service"];
	};
	
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link href="assets/admin-tools/admin-forms/css/admin-forms-orion.css" rel="stylesheet" type="text/css">
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php	
				include "includes/header_def.inc.php";
			?>	
				
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
							
				<section id="content" class="animated fadeIn">
				
					<div class="row mb10">
						<div class="col-md-6 col-md-offset-3">
							<div class="admin-form theme-primary">
								<form action="droit_liste.php" method="post" id="admin-form">
									<div class="col-md-10">
										<label class="field select">
											<select name="filt_service" id="filt_service" >
												<option value="">Filtrer par service...</option>
												<?=get_service_select($filt_service);?>											
											</select>
											<i class="arrow simple"></i>
										</label>
									</div>	
									
									<div class="col-md-2">
										<button type="submit" class="btn btn-primary btn-sm" >
											<i class="fa fa-search"></i>										
										</button>
									
									</div>
								</form>
							</div>
						</div>
					</div>
					
					<div class="row" >
						<div class="col-md-12" >
							<div class="table-responsive" >
								<table class="table table-hover table-striped" >
									<thead>
										<tr class="dark" >
											<th>ID</th>
											<th>Nom</th>
											<th>Descriptif</th>
											<th>Métier</th>
											<th>&nbsp;</th>
										</tr>															
									</thead>
									<tbody>
								<?php	$donnees=get_droit($filt_service);												
										$service=get_services();
										if(!empty($donnees)){										
											foreach($donnees as $value){ 
												$service_nom="";
												if($value["drt_service"]){											
													$cle_service = array_search($value["drt_service"], array_column($service, 'ser_id'));												
													$service_nom=$service[$cle_service]["ser_libelle"];
												}; ?>
												<tr>
													<td><?=$value["drt_id"]?></td>
													<td><?=$value["drt_nom"]?></td>
													<td><?=$value["drt_descriptif"]?></td>	
													<td><?=$service_nom?></td>
													<td class="text-center" >
														<span data-toggle="modal" data-target="#formEdit" >
															<a href="#" class="btn btn-warning btn-xs open-formEdit" data-id="<?=$value["drt_id"]?>" data-nom="<?=$value["drt_nom"]?>" data-descriptif="<?=$value["drt_descriptif"]?>" data-service="<?=$value["drt_service"]?>" data-toggle="tooltip" data-placement="bottom" title="Modifier" >
																<i class="fa fa-pencil"></i>
															</a>
														</span>
													</td>									
												</tr>	
								<?php		
											};
										}; ?>																			
									</tbody>
								</table>
							</div>
						</div>
					</div>
	
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >
				</div>
				<div class="col-xs-3 footer-right" >
					<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#formEdit" >
						<i class="fa fa-plus" ></i>
						Nouveau droit
					</button>
				</div>
			</div>
		</footer>
									
		<div id="formEdit" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
				
					<form method="post" action="droit_enr.php" >
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title" >Nouveau droit</h4>
						</div>
						<div class="modal-body">
						
							<div>
								<input type="hidden" name="drt_id" id="drt_id" value="0" >
							</div>
							
							<div class="row" >
								<div class="col-md-6" >
								
									<div class="form-group">
										<label for="drt_nom" class="sr-only" >Nom</label>
										<div>
											<input type="text" class="form-control" name="drt_nom" id="drt_nom" placeholder="Nom" required >
										</div>
									</div>
								</div>
								<div class="col-md-6" >								  
									<div class="form-group" >
										<label class="sr-only" for="drt_service">Métier</label>
										<select name="drt_service" id="drt_service" class="form-control" required >
											<option value="" ></option>
											<?=get_service_select(0);?>
										</select>
									</div>	
								</div>
							</div>
							<div class="row" >
								<div class="col-md-12 text-center" >
									<textarea rows="5" cols="60" id="drt_descriptif" name="drt_descriptif" placeholder="Descriptif des fonctions associées" required ></textarea>
								</div>
							</div>							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php
		include "includes/footer_script.inc.php"; ?>	                         
<script type="text/javascript">
			jQuery(document).ready(function() {
				

				$(document).on("click", ".open-formEdit", function () {
					
					 var drt_id = $(this).data('id');
					 var drt_service = $(this).data('service');
					 var drt_nom = $(this).data('nom');
					 var drt_descriptif = $(this).data('descriptif');
					
					 $(".modal-body #drt_id").val( drt_id );					  
					 $(".modal-body #drt_service").val( drt_service );					
					 $(".modal-body #drt_nom").val( drt_nom );
					 $(".modal-body #drt_descriptif").val( drt_descriptif );
					
					if(drt_id==0){
						$("#formEdit .modal-title").html('Nouveau droit');
					}else{
						$("#formEdit .modal-title").html('Edition d\'un droit');
					};
				});		
			});
			(jQuery);
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
