<?php
include "includes/controle_acces.inc.php";

include("includes/connexion.php");

// edition contact
if(isset($_POST['submit1'])){
	if($_POST['con_fonction'] == 'autre'){
		$_POST['con_fonction'] =0;
	}
	if(isset($_POST['con_defaut']) && $_POST['con_defaut'] == "con_defaut"){
		$con_defaut = 1;
	}else{
		$con_defaut = 0;
	}

	$req = $Conn->prepare("UPDATE fournisseurs_contacts SET fco_defaut = :fco_defaut, fco_fonction = :fco_fonction, fco_fonction_nom = :fco_fonction_nom, fco_titre = :fco_titre, fco_nom = :fco_nom, fco_prenom = :fco_prenom, fco_tel =:fco_tel, fco_fax = :fco_fax, fco_portable = :fco_portable, fco_mail = :fco_mail WHERE fco_id = :fco_id;");
	$req->bindParam(':fco_id', $_GET['contact']);
	$req->bindParam(':fco_defaut', $con_defaut);
	$req->bindParam(':fco_fonction', $_POST['con_fonction']);
	$req->bindParam(':fco_fonction_nom', $_POST['con_fonction_nom']);
	$req->bindParam(':fco_titre', $_POST['con_titre']);
	$req->bindParam(':fco_nom', $_POST['con_nom']);
	$req->bindParam(':fco_prenom', $_POST['con_prenom']);
	$req->bindParam(':fco_tel', $_POST['con_tel']);
	$req->bindParam(':fco_fax', $_POST['con_fax']);
	$req->bindParam(':fco_portable', $_POST['con_portable']);
	$req->bindParam(':fco_mail', $_POST['con_mail']);
	$req->execute();

	if(isset($_POST['con_defaut']) && $_POST['con_defaut'] == "con_defaut"){

		$req = $Conn->prepare("SELECT fco_id, fco_defaut FROM fournisseurs_contacts WHERE fco_fournisseur = :fou_id AND fco_id <> :fco_id");
		$req->bindParam(':fou_id', $_GET['fournisseur']);
		$req->bindParam(':fco_id', $_GET['contact']);
		$req->execute();
		$contacts_fournisseur = $req->fetchAll();

		if(!empty($contacts_fournisseur)){
			foreach($contacts_fournisseur as $c){
				$req = $Conn->prepare("UPDATE fournisseurs_contacts SET fco_defaut = 0 WHERE fco_id = :fco_id;");
				$req->bindParam(':fco_id', $c['fco_id']);
				$req->execute();
			}
		}
		

	}

	Header("Location: fournisseur_voir.php?fournisseur=" . $_GET['fournisseur'] . "&succes=3");
}
// fin edition contact
// ajout contact
if(isset($_POST['submit2'])){
	if($_POST['con_fonction'] == 'autre'){
		$_POST['con_fonction'] =0;
	}
	if(isset($_POST['con_defaut']) && $_POST['con_defaut'] == "con_defaut"){
		$con_defaut = 1;
	}else{
		$con_defaut = 0;
	}

	$req = $Conn->prepare("SELECT fco_id, fco_defaut FROM fournisseurs_contacts WHERE fco_fournisseur = :fou_id");
	$req->bindParam(':fou_id', $_GET['fournisseur']);
	$req->execute();
	$contact_fournisseur = $req->fetch();

	if(empty($contact_fournisseur)){
		$con_defaut = 1;
	}

	$req = $Conn->prepare("INSERT INTO fournisseurs_contacts (fco_fournisseur, fco_defaut, fco_fonction, fco_fonction_nom , fco_titre , fco_nom , fco_prenom, fco_tel , fco_fax, fco_portable, fco_mail) VALUES (:fco_fournisseur, :fco_defaut, :fco_fonction, :fco_fonction_nom , :fco_titre , :fco_nom , :fco_prenom, :fco_tel , :fco_fax, :fco_portable, :fco_mail);");
	$req->bindParam(':fco_fournisseur', $_GET['fournisseur']);
	$req->bindParam(':fco_defaut', $con_defaut);
	$req->bindParam(':fco_fonction', $_POST['con_fonction']);
	$req->bindParam(':fco_fonction_nom', $_POST['con_fonction_nom']);
	$req->bindParam(':fco_titre', $_POST['con_titre']);
	$req->bindParam(':fco_nom', $_POST['con_nom']);
	$req->bindParam(':fco_prenom', $_POST['con_prenom']);
	$req->bindParam(':fco_tel', $_POST['con_tel']);
	$req->bindParam(':fco_fax', $_POST['con_fax']);
	$req->bindParam(':fco_portable', $_POST['con_portable']);
	$req->bindParam(':fco_mail', $_POST['con_mail']);
	$req->execute();

	$contact = $Conn->lastInsertId();

	if(isset($_POST['con_defaut']) && $_POST['con_defaut'] == "con_defaut"){

		$req = $Conn->prepare("SELECT fco_id, fco_defaut FROM fournisseurs_contacts WHERE fco_fournisseur = :fou_id AND fco_id <> :fco_id");
		$req->bindParam(':fou_id', $_GET['fournisseur']);
		$req->bindParam(':fco_id', $contact);
		$req->execute();
		$contacts_fournisseur = $req->fetchAll();

		if(!empty($contacts_fournisseur)){
			foreach($contacts_fournisseur as $c){
				$req = $Conn->prepare("UPDATE fournisseurs_contacts SET fco_defaut = 0 WHERE fco_id = :fco_id;");
				$req->bindParam(':fco_id', $c['fco_id']);
				$req->execute();
			}
		}
		

	}

	Header("Location: fournisseur_voir.php?fournisseur=" . $_GET['fournisseur'] . "&succes=4&tab=1");
}
// fin ajout contact

// ajout / modif adresse

if(isset($_POST['submit3'])){
	$req = $Conn->prepare("SELECT fad_id, fad_fournisseur FROM fournisseurs_adresses WHERE fad_fournisseur = :fad_fournisseur");
	$req->bindParam(':fad_fournisseur', $_GET['fournisseur']);
	$req->execute();
	$adresse = $req->fetch();

	if(!empty($adresse)){
		$req = $Conn->prepare("UPDATE fournisseurs_adresses SET fad_fournisseur = :fad_fournisseur, fad_nom = :fad_nom, fad_ad1 = :fad_ad1, fad_ad2 = :fad_ad2, fad_ad3 = :fad_ad3, fad_cp = :fad_cp, fad_ville = :fad_ville WHERE fad_id = :fad_id");

		$req->bindParam(':fad_id', $adresse['fad_id']);
		$req->bindParam(':fad_fournisseur', $_GET['fournisseur']);
		$req->bindParam(':fad_nom', $_POST['fad_nom']);
		$req->bindParam(':fad_ad1', $_POST['fad_ad1']);
		$req->bindParam(':fad_ad2', $_POST['fad_ad2']);
		$req->bindParam(':fad_ad3', $_POST['fad_ad3']);
		$req->bindParam(':fad_cp', $_POST['fad_cp']);
		$req->bindParam(':fad_ville', $_POST['fad_ville']);


		$req->execute();
		Header("Location: fournisseur_voir.php?fournisseur=" . $_GET['fournisseur'] . "&succes=5&tab=1");

	}else{
		$req = $Conn->prepare("INSERT INTO fournisseurs_adresses (fad_fournisseur, fad_nom, fad_ad1, fad_ad2, fad_ad3, fad_cp, fad_ville) VALUES (:fad_fournisseur, :fad_nom, :fad_ad1, :fad_ad2, :fad_ad3, :fad_cp, :fad_ville)");

		$req->bindParam(':fad_fournisseur', $_GET['fournisseur']);
		$req->bindParam(':fad_nom', $_POST['fad_nom']);
		$req->bindParam(':fad_ad1', $_POST['fad_ad1']);
		$req->bindParam(':fad_ad2', $_POST['fad_ad2']);
		$req->bindParam(':fad_ad3', $_POST['fad_ad3']);
		$req->bindParam(':fad_cp', $_POST['fad_cp']);
		$req->bindParam(':fad_ville', $_POST['fad_ville']);


		$req->execute();
		Header("Location: fournisseur_voir.php?fournisseur=" . $_GET['fournisseur'] . "&succes=6&tab=1");

	}

}
// fin ajout / modif adresse

?>