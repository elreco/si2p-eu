<?php

	$menu_actif = "1-8";
	
	include "includes/controle_acces.inc.php";

	include('includes/connexion.php');

	include "modeles/mod_orion_cli_sous_categories.php";

	$categorie=0;
	if(isset($_GET["categorie"])){
		$categorie=intval($_GET["categorie"]);
	}
	$categorie=1;
	// $req = $Conn->query("SELECT soc_id FROM Societes WHERE soc_gc;");
	// $d_societe=$req->fetch();
	// if(!empty($d_societe)){
	// 	$conn_get_id=$d_societe["soc_id"];
	// }

	$erreur=0;
	if($categorie>0){

		$_SESSION['retour']="client_gc_liste.php";


		$req = $Conn->query("SELECT cca_libelle FROM Clients_Categories WHERE cca_id=2;");
		$d_categorie=$req->fetch();

		$d_sous_categories=orion_cli_sous_categories();

		$sql_client="SELECT DISTINCT cli_id,cli_code,cli_nom,cli_sous_categorie,
		cli_adr_cp,cli_adr_ville
		FROM Clients
		INNER JOIN Clients_Societes ON (Clients_Societes.cso_client = Clients.cli_id)
		WHERE cli_categorie= 2 AND NOT cso_archive  AND cli_groupe AND (NOT cli_groupe OR cli_filiale_de=0 ) AND cli_contrat=:contrat
		ORDER BY cli_code,cli_nom;";

		$req_client=$Conn->prepare($sql_client);


	}else{
		$erreur=1;
	}

	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<!-- PERSO SI2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">


		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			.col-btn{
				width:8%;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm">

		<!-- Start: Main -->
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<!-- CONTENU -->
				<section id="content" class="animated fadeIn">
	<?php 			if($erreur==0){

						// CONTRAT SIGNE

						$contrat=1;
						$req_client->bindParam(":contrat",$contrat);
						$req_client->execute();
						$clients=$req_client->fetchAll();
						if(!empty($clients)){ ?>
							<h1><?=$d_categorie["cca_libelle"]?> - Contrats et accords conclus</h1>

							<div class="table-responsive">
								<table class="table table-striped table-hover datatable" >
									<thead>
										<tr class="dark" >

											<th>Code</th>
											<th>Nom</th>
											<th>Nature</th>
											<th>CP</th>
											<th>Ville</th>
											<th class="col-btn text-center" >Succursales</th>
											<th class="col-btn text-center" >Produits</th>
											<th class="col-btn text-center" >Documents</th>
										</tr>
									</thead>
									<tbody>
		<?php							foreach($clients as $c){

											$cli_sous_categorie=0;
											if(!empty($c["cli_sous_categorie"])){
												$cli_sous_categorie=$c["cli_sous_categorie"];
											}
											if(!empty($d_sous_categories[$cli_sous_categorie]["csc_couleur"])){
												$style="style='color:" . $d_sous_categories[$cli_sous_categorie]["csc_couleur"] . "';";
											}else{
												$style="style='color:#000';";
											} ?>
											<tr <?=$style?> >
												<td>
													<a href="client_voir.php?client=<?=$c["cli_id"]?>" <?=$style?> >
														<?=$c["cli_code"]?>
													</a>
												</td>
												<td><?=$c["cli_nom"]?></td>
												<td><?=$d_sous_categories[$cli_sous_categorie]["csc_libelle"]?></td>
												<td><?=$c["cli_adr_cp"]?></td>
												<td><?=$c["cli_adr_ville"]?></td>
												<td class="text-center" >
													<a class="btn btn-md btn-info" href="client_voir.php?client=<?=$c["cli_id"]?>&tab10" <?=$style?> >
														<i class="fa fa-eye"  style="color:white"></i>
													</a>
												</td>
												<td class="text-center" >
													<a class="btn btn-md btn-info" href="client_voir.php?client=<?=$c["cli_id"]?>&tab13" <?=$style?> >
														<i class="fa fa-eye"  style="color:white"></i>
													</a>
												</td>
												<td class="text-center" >
													<a class="btn btn-md btn-info" href="client_voir.php?client=<?=$c["cli_id"]?>&tab7" <?=$style?> >
														<i class="fa fa-eye"  style="color:white"></i>
													</a>
												</td>
											</tr>
		<?php							} ?>
									</tbody>
								</table>
							</div>
		<?php			}

						// CONTRAT EN NEGO

						$contrat=0;
						$req_client->bindParam(":contrat",$contrat);
						$req_client->execute();
						$clients=$req_client->fetchAll();
						if(!empty($clients)){ ?>
							<h1><?=$d_categorie["cca_libelle"]?> - Contrats et accords en négociation</h1>

							<div class="table-responsive">
								<table class="table table-striped table-hover datatable" >
									<thead>
										<tr class="dark" >
											<th>Code</th>
											<th>Nom</th>
											<th>Nature</th>
											<th>CP</th>
											<th>Ville</th>
											<th class="col-btn text-center" >Succursales</th>
											<th class="col-btn text-center" >Produits</th>
											<th class="col-btn text-center" >Documents</th>
										</tr>
									</thead>
									<tbody>
		<?php							foreach($clients as $c){

											$cli_sous_categorie=0;
											if(!empty($c["cli_sous_categorie"])){
												$cli_sous_categorie=$c["cli_sous_categorie"];
											}
											if(!empty($d_sous_categories[$cli_sous_categorie]["csc_couleur"])){
												$style="style='color:" . $d_sous_categories[$cli_sous_categorie]["csc_couleur"] . "';";
											}else{
												$style="style='color:#000';";
											} ?>
											<tr <?=$style?> >
												<td>
													<a href="client_voir.php?client=<?=$c["cli_id"]?>&societ=4" <?=$style?> >
														<?=$c["cli_code"]?>
													</a>
												</td>
												<td><?=$c["cli_nom"]?></td>
												<td><?=$d_sous_categories[$cli_sous_categorie]["csc_libelle"]?></td>
												<td><?=$c["cli_adr_cp"]?></td>
												<td><?=$c["cli_adr_ville"]?></td>
												<td class="text-center" >
													<a class="btn btn-md btn-info" href="client_voir.php?client=<?=$c["cli_id"]?>&tab10&societ=4" <?=$style?> >
														<i class="fa fa-eye" style="color:white"></i>
													</a>
												</td>
												<td class="text-center" >
													<a class="btn btn-md btn-info" href="client_voir.php?client=<?=$c["cli_id"]?>&tab13&societ=4" <?=$style?> >
														<i class="fa fa-eye" style="color:white"></i>
													</a>
												</td>
												<td class="text-center" >
													<a class="btn btn-md btn-info" href="client_voir.php?client=<?=$c["cli_id"]?>&tab7&societ=4" <?=$style?> >
														<i class="fa fa-eye" style="color:white"></i>
													</a>
												</td>
											</tr>
		<?php							} ?>
									</tbody>
								</table>
							</div>
		<?php			}
					} ?>
				</section>
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left"></div>
				<div class="col-xs-6 footer-middle">&nbsp;</div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>
<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">

		</script>
	</body>
</html>
