<?php

   include "includes/controle_acces.inc.php";
   include_once("includes/connexion_soc.php");
   include_once("includes/connexion.php");
   include_once("modeles/mod_add_notification.php");
   include_once("modeles/mod_get_client_produits.php");
   include('modeles/mod_action_stagiaires_del.php');
   include('modeles/mod_planning_periode_lib.php');
   include('modeles/mod_planning_periode.php');

   // AJOUT OU MAJ D'UN CLIENT SUR UNE ACTION

   $erreur_txt="";
   $warning_txt="";

   // element du form obligatoire

   $action_id=0;
   if(isset($_POST["action"])){
       if(!empty($_POST["action"])){
           $action_id=intval($_POST["action"]);
       }
   }

   $client=0;
   if(isset($_POST["client"])){
       if(!empty($_POST["client"])){
           $client=intval($_POST["client"]);
       }
   }

   $produit=0;
   if(isset($_POST["produit"])){
       if(!empty($_POST["produit"])){
           $produit=intval($_POST["produit"]);
       }
   }

   $commercial=0;
   if(isset($_POST["commercial"])){
       if(!empty($_POST["commercial"])){
           $commercial=intval($_POST["commercial"]);
       }
   }

   $action_client_id=0;
   if(isset($_POST["action_client"])){
       if(!empty($_POST["action_client"])){
           $action_client_id=intval($_POST["action_client"]);
       }
   }

   $id_heracles=0;
   if(isset($_POST["id_heracles"])){
       if(!empty($_POST["id_heracles"])){
           $id_heracles=intval($_POST["id_heracles"]);
       }
   }

   $auto=0;
   if(isset($_GET["auto"])){
       if(!empty($_GET["auto"])){

           $auto=intval($_GET["auto"]);

           $action_id=0;
           if(isset($_GET["action"])){
               if(!empty($_GET["action"])){
                   $action_id=intval($_GET["action"]);
               }
           }

           $devis_ligne_id=0;
           if(isset($_GET["devis"])){
               if(!empty($_GET["devis"])){
                   $devis_ligne_id=intval($_GET["devis"]);
               }
           }

       }
   }


   //if(empty($action_id) OR empty($client) OR empty($produit) OR empty($commercial) OR (empty($action_client_id) AND empty($id_heracles)) ){
   if(empty($action_id) OR ($auto==0 AND (empty($produit) OR empty($commercial))) OR ($auto==1 AND (empty($action_id) OR empty($devis_ligne_id)))  ){

       $_SESSION['message'][] = array(
           "titre" => "Erreur",
           "type" => "danger",
           "message" => "Formulaire incomplet!"
       );
       if(!empty($action_id)){
           header("location: action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id);
           die();
       }else{
           header("location: action_tri.php");
           die();
       }

   }else{

       // PERSONNE CONNECTE
       $acc_agence=0;
       if(isset($_SESSION['acces']["acc_agence"])){
           $acc_agence=$_SESSION['acces']["acc_agence"];
       }
       $acc_societe=0;
       if(isset($_SESSION['acces']["acc_societe"])){
           $acc_societe=$_SESSION['acces']["acc_societe"];
       }
       $acc_utilisateur=0;
       if(!empty($_SESSION['acces']["acc_ref"])){
           if($_SESSION['acces']["acc_ref"]==1){
               $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
           }
       }

       $consultation_gc=false;
       if($conn_soc_id!=$acc_societe){
           $acc_societe=$conn_soc_id;
           $acc_agence=0;
           $consultation_gc=true;
       }

       // niveau de derog du U

       $derog_u=0;
       if($_SESSION["acces"]["acc_droits"][31]){
           $derog_u=2;
       }elseif($_SESSION["acces"]["acc_droits"][27]){
           $derog_u=1;
       }

       // INI DES VAL DE CONTROLE OU DE TRAITEMENT

       $acl_pro_inter=0;	// type de produit selectionné dans le formaulaire

       $archive=0;
       $archive_date=null;
       $archive_uti=0;
       $archive_ok=0;

       $acl_non_fac=0;

       $nb_demi_jour=0;

       $devis_id=0;


       // TRAITEMENT SELON ORIGIN FORM / AUTO
       if(!empty($auto)){

           // INSCRIPTION SANS PASSE PAR LE FORM

           $_POST=array();

           $sql="SELECT dli_type,dli_produit,dev_client,dev_commercial,dli_derogation,dli_rem_famille,dli_rem_ca,dli_ht,dli_montant_ht,dli_qte,dli_libelle,dev_id,dev_numero
           FROM Devis_Lignes,Devis WHERE dli_devis=dev_id AND dli_id=" . $devis_ligne_id . " AND dli_action=" . $action_id . ";";
           $req=$ConnSoc->query($sql);
           $d_devis=$req->fetch();
           if(!empty($d_devis)){

               $client=$d_devis["dev_client"];
               $produit=$d_devis["dli_produit"];
               $commercial=$d_devis["dev_commercial"];
               $dev_numero=$d_devis["dev_numero"];
               $action_client_id=0;

               $devis_id=$d_devis["dev_id"];

               if($d_devis["dli_type"]==2){
                   $acl_pro_inter=1;
               }
               $_POST["confirme"]="on";
               $_POST["confirme_date"]=date("d/m/Y");

               $_POST["acl_attestation"]=0;

               $derog_u=$d_devis["dli_derogation"];
               $_POST["acl_derogation"]=$d_devis["dli_derogation"];
               $_POST["acl_rem_famille"]=$d_devis["dli_rem_famille"];
               $_POST["acl_rem_ca"]=$d_devis["dli_rem_ca"];

               if($acl_pro_inter==0){

                   $_POST["ca_intra"]=$d_devis["dli_montant_ht"];

               }else{

                   $_POST["ca_inter_unit"]=$d_devis["dli_ht"];
                   $_POST["for_nb_sta"]=$d_devis["dli_qte"];

               }

               $_POST["acl_pro_libelle"]=$d_devis["dli_libelle"];

           }

           if(empty($erreur_txt)){

               // ON RECUPERE LES DATES

               $sql="SELECT pda_id FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=" . $action_id . " AND NOT pda_archive;";
               $req=$ConnSoc->query($sql);
               $d_dates=$req->fetchAll();
               if(!empty($d_dates)){
                   foreach($d_dates as $dd){
                       $_POST["date_" . $dd["pda_id"]]="on";
                       $nb_demi_jour++;
                   }
               }
           }

       }else{

           // ELEMENT DU FORM POUR CONTROLE

           if(isset($_POST["produit_type"])){
               if(!empty($_POST["produit_type"])){
                   if($_POST["produit_type"]==2){
                       $acl_pro_inter=1;
                   }
               }
           }

           // archivage

           if(isset($_POST["archive"])){
               if($_POST["archive"]=="on"){
                   $archive=1;
               }
           }

           // nombre de demi jour selectionne

           if(isset($_POST["nb_demi_jour"])){
               $nb_demi_jour=intval($_POST["nb_demi_jour"]);
           }
       }

		//******************************************
		//      CONTROLE
		//******************************************/

       if(empty($erreur_txt)){
           if(empty($nb_demi_jour)){
               $erreur_txt="Aucune date de formation n'est asociée au client";
           }
       }

       $add_particulier=false;

       // nouvelle inscription
       if($action_client_id==0){

           if(!empty($_POST["particulier"])){

               $add_particulier=true;

               $client=0;

               $cli_code="";
               if(!empty($_POST["par_code"])){
                   $cli_code=$_POST["par_code"];
               }

               $adr_ad1="";
               if(!empty($_POST["par_adr1"])){
                   $adr_ad1=$_POST["par_adr1"];
               }
               $adr_cp="";
               if(!empty($_POST["par_cp"])){
                   $adr_cp=$_POST["par_cp"];
               }
               $adr_ville="";
               if(!empty($_POST["par_ville"])){
                   $adr_ville=$_POST["par_ville"];
               }

               $con_titre=0;
               if(!empty($_POST["par_titre"])){
                   $con_titre=intval($_POST["par_titre"]);
               }
               $con_nom="";
               if(!empty($_POST["par_nom"])){
                   $con_nom=$_POST["par_nom"];
               }
               $con_prenom="";
               if(!empty($_POST["par_prenom"])){
                   $con_prenom=$_POST["par_prenom"];
               }
               $sta_naissance="";
               if(!empty($_POST["par_naissance"])){
                   $DT_par_naissance=date_create_from_format('j/m/Y',$_POST["par_naissance"]);
                   $sta_naissance=$DT_par_naissance->format("Y-m-d");
               }

               if(empty($cli_code) OR empty($adr_ad1) OR empty($adr_cp) OR empty($adr_ville) OR empty($con_titre) OR empty($con_nom) OR empty($con_prenom) OR empty($sta_naissance)){
                   $erreur_txt="Formulaire incomplet : impossible de créer le particulier.";
               }
           }
       }

       // SI on ne créer pas de particulier le client doit etre renseigné
       if(empty($erreur_txt)){
           if(!$add_particulier AND empty($client)){
               $erreur_txt="Le client n'a pas pu être identifié.";
           }
       }



       // DONNEE DE L'ACTION

       if(empty($erreur_txt)){

           $sql="SELECT act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,act_produit,act_agence,act_vehicule,act_date_deb
		   ,act_pro_type,act_adr_cp,act_adr_ville
           ,act_marge_ca,act_verrou_bc,act_verrou_admin FROM Actions WHERE act_id=:action_id;";
           $req=$ConnSoc->prepare($sql);
           $req->bindParam(":action_id",$action_id);
           $req->execute();
           $d_action=$req->fetch();
           if(empty($d_action)){
               $erreur_txt="Impossible de charger les données";
           }
       }

       // LE PRODUIT

       if(empty($erreur_txt)){
           $d_produit=get_client_produits($client,$produit,1,0,1,0,0,0,0,0,0);
           if(empty($d_produit)){
               $erreur_txt="Les informations sur le produit n'ont pas pu être chargées.";
           }
       }

       // ACTION CLIENT AVANT MODIF

       $pda_confirme=1;
       $actu_classification=false;
       $derogation=null;
       $acl_non_fac=null;
       $acl_non_fac_gfc=null;

       if(empty($erreur_txt) AND !empty($action_client_id)){

           $sql="SELECT acl_produit,acl_pro_recurrent,acl_act_recurrent,acl_client,acl_archive,acl_archive_date,acl_archive_uti,acl_archive_ok
           ,acl_confirme,acl_for_nb_sta,acl_for_nb_sta_resa,acl_derogation,acl_rem_famille,acl_rem_ca,acl_non_fac,acl_ca,acl_ca_unit,acl_non_fac_gfc,acl_facture_ht,acl_facture_gfc_ht
		   ,acl_pro_inter
           FROM Actions_Clients WHERE acl_id=:action_client;";
           $req=$ConnSoc->prepare($sql);
           $req->bindParam(":action_client",$action_client_id);
           $req->execute();
           $avant_modif=$req->fetch();
           if(!empty($avant_modif)){

               // uti n'a pas le droit d'archivage ou action été archive et le reste
               if(!$_SESSION["acces"]["acc_droits"][10] OR ($avant_modif["acl_archive_ok"] AND !$_SESSION["acces"]["acc_droits"][11]) OR ($archive==1 AND $avant_modif["acl_archive"]==1) ){
                   $archive=$avant_modif["acl_archive"];
                   $archive_date=$avant_modif["acl_archive_date"];
                   $archive_uti=$avant_modif["acl_archive_uti"];
                   $archive_ok=$avant_modif["acl_archive_ok"];
               }elseif($archive==1){
                   $archive_date=date("Y-m-d");
                   $archive_uti=$acc_utilisateur;
                   if($_SESSION["acces"]["acc_droits"][11]){
                       $archive_ok=1;
                   }else{
                       $archive_ok=0;
                   }
               }

               if(!$_SESSION["acces"]["acc_droits"][10] AND !$_SESSION["acces"]["acc_droits"][11]){
                   $acl_non_fac=$avant_modif["acl_non_fac"];
                   $acl_non_fac_gfc=$avant_modif["acl_non_fac_gfc"];

               }

               if($avant_modif["acl_produit"]==$produit){
                   if($avant_modif["acl_derogation"]>$derog_u){
                       $derogation=$avant_modif["acl_derogation"];
                       $rem_famille=$avant_modif["acl_rem_famille"];
                       $rem_ca=$avant_modif["acl_rem_ca"];
                   }

               }
           }else{
               $erreur_txt="Impossible de charger les données liées à l'inscription du client";
           }

           if(empty($erreur_txt)){

			   // #VERIF_QUALIF -> VERIF COHERENCE ENTRE PRODUIT, QUALIFICATION ET DIPLOME.

               $sql="SELECT ast_stagiaire FROM Actions_Stagiaires WHERE ast_action=:action_id AND ast_action_client=:action_client_id";
               if(empty($d_produit["qualification"])){
                   // le nouveau produit ne gere pas de qualification
                   $sql.=" AND ast_diplome>0";
               }else{
                   $sql.=" AND NOT ast_qualification=:qualification AND ast_diplome>0";
               }
               $req=$ConnSoc->prepare($sql);
               $req->bindParam(":action_id",$action_id);
               $req->bindParam(":action_client_id",$action_client_id);
               if(!empty($d_produit["qualification"])){
                   $req->bindParam(":qualification",$d_produit["qualification"]);
               }
               $req->execute();
               $d_action_stagiaires=$req->fetchAll();
               if(!empty($d_action_stagiaires)){
                   $erreur_txt="Le produit sélectionné est incompatible avec les diplômes déjà enregistrés!";
               }

           }

       }




       // PRESENCE D'AUTRE CLIENT

	    $acl_intra=0; // si 1 alors l'action est INTRA par la présence d'un code INTRA sur une AUTRE action_client. -> pas de changement de type ni de texte_survol

       if(empty($erreur_txt)){

           $sql="SELECT acl_id,acl_client,acl_produit,acl_pro_inter,acl_confirme FROM Actions_Clients WHERE acl_action=:action_id AND NOT acl_id=:action_client_id AND NOT acl_archive;";
           $req=$ConnSoc->prepare($sql);
           $req->bindParam(":action_client_id",$action_client_id);
           $req->bindParam(":action_id",$action_id);
           $req->execute();
           $d_autre_clients=$req->fetchAll();
           if(empty($d_autre_clients)){
               unset($d_autre_clients);
           }else{
               foreach($d_autre_clients as $cli){
                   if(empty($cli["acl_pro_inter"])){
                       $acl_intra=1;
                   }
                   if(empty($cli["acl_confirme"])){
                       $pda_confirme=0;
                   }
               }
           }
       }

       // pas changement de famille si plusieurs clients

       if(empty($erreur_txt)){
           if($d_action["act_pro_famille"]!=$d_produit["famille"] OR $d_action["act_pro_sous_famille"]!=$d_produit["sous_famille"] OR $d_action["act_pro_sous_sous_famille"]!=$d_produit["sous_sous_famille"]){
               if(isset($d_autre_clients)){
                   $erreur_txt="Vous ne pouvez pas changer la famille de formation si plusieurs clients sont déjà inscrits à la formation!";
               }else{
                   $actu_classification=true;
               }
           }
       }

       // SI CLIENT
       if(!empty($client)){

           if(empty($erreur_txt)){

               $sql="SELECT cli_categorie,cli_nom
               ,cli_contact,cli_con_fct,cli_con_fct_nom,cli_con_titre,cli_con_nom,cli_con_prenom,cli_con_tel,cli_con_mail
               ,cli_reg_type,cli_reg_formule,cli_reg_nb_jour,cli_reg_fdm,cli_stagiaire
               ,adr_id,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_geo
               FROM Clients INNER JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client)
               LEFT JOIN Adresses ON (Clients.cli_adresse=Adresses.adr_id)
               WHERE cli_id=:client AND cso_societe=:societe AND cso_agence=:agence AND NOT cso_archive;";
               $req=$Conn->prepare($sql);
               $req->bindParam(":client",$client);
               $req->bindParam(":societe",$acc_societe);
               $req->bindParam(":agence",$d_action["act_agence"]);
               $req->execute();
               $d_client=$req->fetch();
               if(empty($d_client)){
                   $erreur_txt="Les informations sur le client n'ont pas pu être chargées.";

                   if($auto==1){
                       if($d_client["cli_facture_opca"]==1){
                           $_POST["opca_fac"]="on";
                       }
                   }
               }
           }
       }

       // SUPERPOSITION INTRA INTER
       if(empty($erreur_txt)){
           if($acl_intra==1 AND $acl_pro_inter==0){
               $erreur_txt="Vous ne pouvez pas utiliser deux codes intra sur la même formation!";
           }
       }

       // FIN DES CONTROLES


		//******************************************
		//      DONNEE POUR ENREGISTREMENT
		//****************************************/

       if(empty($erreur_txt)){

           if(is_null($acl_non_fac) AND is_null($acl_non_fac_gfc)){

               if($d_produit["facturation"]==1 AND $consultation_gc){
                   $acl_non_fac_gfc=0;
                   if(!empty($_POST["acl_non_fac"])){
                       $acl_non_fac_gfc=1;
                   }
                   $acl_non_fac=0;
                   if(isset($avant_modif)){
                       $acl_non_fac=$avant_modif["acl_non_fac"];
                   }
               }else{
                   $acl_non_fac=0;
                   if(!empty($_POST["acl_non_fac"])){
                       $acl_non_fac=1;
                   }
                   $acl_non_fac_gfc=0;
                   if(isset($avant_modif)){
                       $acl_non_fac_gfc=$avant_modif["acl_non_fac_gfc"];
                   }
               }

           }



           if(!isset($avant_modif)){
               if($d_produit["derogation"]>$derog_u){
                   $derogation=$d_produit["derogation"];
                   $rem_famille=$d_produit["rem_famille"];
                   $rem_ca=$d_produit["rem_ca"];
               }

           }elseif($avant_modif["acl_produit"]!=$produit){

			   // FG : ce n'est pas parce qu'on change de produit qu'il faut actualiser act_produit
               /*$sql="SELECT acl_produit FROM Actions_Clients WHERE acl_action = " . $action_id . " AND acl_produit = " . $avant_modif["acl_produit"];
               $req=$Conn->prepare($sql);
               $d_action_client_produit=$req->fetch();

               if(empty($d_action_client_produit)) {
                   $sql="UPDATE actions SET act_produit = " . $produit . " WHERE act_id = " . $action_id;
                   $req=$ConnSoc->prepare($sql);
                   $req->execute();
               }*/

               if($d_produit["derogation"]>$derog_u){
                   $derogation=$d_produit["derogation"];
                   $rem_famille=$d_produit["rem_famille"];
                   $rem_ca=$d_produit["rem_ca"];
               }
           }

           // elment du form

           $confirme=0;
           $confirme_date=null;
           if(isset($_POST["confirme"])){
               if($_POST["confirme"]=="on"){
                   $confirme=1;
                   if(isset($_POST["confirme_date"])){
                       if(!empty($_POST["confirme_date"])){
                           $DT_confirme_date=date_create_from_format('j/m/Y',$_POST["confirme_date"]);
                           $confirme_date=$DT_confirme_date->format("Y-m-d");
                       }
                   }
               }
           }


           $opca_fac=0;
           if(isset($_POST["opca_fac"])){
               if($_POST["opca_fac"]=="on"){
                   $opca_fac=1;
               }
           }

           $acl_attestation=0;
           if(isset($_POST["acl_attestation"])){
               if(!empty($_POST["acl_attestation"])){
                   $acl_attestation=intval($_POST["acl_attestation"]);
               }
           }

           // etat de confirmation de l'action
           if($archive==0){
               if($pda_confirme==1 AND $confirme==0){
                   $pda_confirme=0;
               }
           }

			$acl_reporte=0;
			if(isset($_POST["reporte"])){
				if(!empty($_POST["reporte"])){
					$acl_reporte=1;
				}
			}


           // S'il n'y a pas de vehicule -> le changement de famille impact le style des cases
		   // c'est le vehicule qui colorise la case en priorité donc s'il y a un vehicule, le changement de classification n'aura pas d'impact sur la case
           if(empty($d_action["act_vehicule"]) AND $actu_classification){

               $sql="SELECT psf_libelle,psf_couleur_bg,psf_couleur_txt FROM Produits_Sous_Familles WHERE psf_id=" . $d_produit["sous_famille"] . ";";
               $req=$Conn->query($sql);
               $d_pro_s_fam=$req->fetch();

               $sql="SELECT pss_libelle,pss_couleur_bg,pss_couleur_txt FROM Produits_Sous_Sous_Familles WHERE pss_id=" . $d_produit["sous_sous_famille"] . ";";
               $req=$Conn->query($sql);
               $d_pro_s_s_fam=$req->fetch();

               $pda_style_bg="#FFF";
               $pda_style_txt="#333";

               if(!empty($d_pro_s_s_fam["pss_couleur_bg"])){
                   $pda_style_bg=$d_pro_s_s_fam["pss_couleur_bg"];
                   $pda_style_txt=$d_pro_s_s_fam["pss_couleur_txt"];
               }elseif(!empty($d_pro_s_fam["psf_couleur_bg"])){
                   $pda_style_bg=$d_pro_s_fam["psf_couleur_bg"];
                   $pda_style_txt=$d_pro_s_fam["psf_couleur_txt"];
               }
           }

           // CA NON COMMUNIQUE

           $acl_ca_nc=0;
           if(!empty($_POST["acl_ca_nc"])){
               $acl_ca_nc=1;
           }

           // derogation

           if(is_null($derogation)){
               if($derog_u>0){

                   $derogation=0;
                   if(!empty($_POST["acl_derogation"])){
                       $derogation=intval($_POST["acl_derogation"]);
                   }

                   $rem_famille=0;
                   if(!empty($_POST["acl_rem_famille"])){
                       $rem_famille=1;
                   }
                   $rem_ca=0;
                   if(!empty($_POST["acl_rem_ca"])){
                       $rem_ca=1;
                   }

               }else{
                   $derogation=0;
                   $rem_famille=1;
                   $rem_ca=1;
               }

           }

           // creation d'un devis
           $add_devis=false;
           if($archive==0 AND $acl_ca_nc==0){
               if(!empty($_POST["devis"])){
                   $add_devis=true;
               }
           }

           if($add_particulier OR $add_devis){

               // DONNEE SUR LE COMMERCIAL

               $com_utilisateur=0;
               $com_agence=0;
               $sql_com="SELECT com_ref_1,com_agence,com_label_1,com_label_2 FROM Commerciaux WHERE com_type=1 AND com_id=:commercial;";
               $req_com=$ConnSoc->prepare($sql_com);
               $req_com->bindParam(":commercial",$commercial);
               $req_com->execute();
               $d_commercial=$req_com->fetch();
               if(!empty($d_commercial)){
                   $com_utilisateur=$d_commercial["com_ref_1"];
                   $com_agence=$d_commercial["com_agence"];
               }

               if($add_particulier){

                   // RIB PAR DEFAUT POUR LA SOCIETE

                   $soc_rib=0;
                   $soc_rib_change=0;
                   $sql_com="SELECT rib_id,rib_change FROM Rib WHERE rib_defaut=1 AND rib_societe=:acc_societe;";
                   $req_com=$Conn->prepare($sql_com);
                   $req_com->bindParam(":acc_societe",$acc_societe);
                   $req_com->execute();
                   $d_rib=$req_com->fetch();
                   if(!empty($d_rib)){
                       $soc_rib=$d_rib["rib_id"];
                       $soc_rib_change=$d_rib["rib_change"];
                   }
               }

               if($add_devis){

                   $sql="SELECT soc_tva_exo FROM Societes WHERE soc_id=:acc_societe;";
                   $req=$Conn->prepare($sql);
                   $req->bindParam(":acc_societe",$acc_societe);
                   $req->execute();
                   $d_societe=$req->fetch();

               }
           }


			$acl_commentaire="";
			if(!empty($_POST["acl_commentaire"])){
				$acl_commentaire=str_replace("<br/>",chr(10),$_POST["acl_commentaire"]);
				$acl_commentaire=str_replace("<br>",chr(10),$acl_commentaire);
				$acl_commentaire=str_replace("<","",$acl_commentaire);
				$acl_commentaire=str_replace(">","",$acl_commentaire);
			}


			// TYPE D'ACTION pour UPDATE Actions

			$act_pro_type=null;
			$act_produit=null;

			if($actu_classification){
				// n'est possible que s'il y a qu'une action client
				if($acl_pro_inter==1){
					$act_pro_type=2;
				}else{
					$act_pro_type=1;
				}
				$act_produit=$produit;

			}elseif($acl_intra==0){
				// si l'action est intra via une autre action_client -> rien ne peux impacter act_pro_type et act_produit

				if(isset($avant_modif)){

					// edition

					if($avant_modif["acl_pro_inter"]==1 AND $acl_pro_inter==0){

						// l'action passe intra
						$act_pro_type=1;
						$act_produit=$produit;

					}elseif($avant_modif["acl_pro_inter"]==0 AND $acl_pro_inter==1){

						// l'action passe inter
						$act_pro_type=2;
						$act_produit=$produit;

					}elseif($avant_modif["acl_produit"]!=$produit){

						// l'action conserve son type mais change de produit

						$act_pro_type=$d_action["act_pro_type"];
						$act_produit=$produit;
					}

				}else{

					// ajout

					if($acl_pro_inter==0){
						// l'action passe intra
						$act_pro_type=1;
						$act_produit=$produit;
					}
				}
			}

			if(!is_null($act_pro_type)){
				if($act_pro_type==1){
					$text_survol=$d_produit["reference"] . " " . $d_action["act_adr_ville"];
				}else{
					$text_survol=$d_action["act_adr_cp"] . " " . $d_action["act_adr_ville"];
				}

			}
		}

		//******************************************
		//        ENREGISTREMENT
		//******************************************/

       if(empty($erreur_txt)){


           if($action_client_id>0){

               // EDITION

               $operation=2;
               $ca_unit=0;
               $ca=0;
               $ca_gfc=0;
               if($acl_pro_inter==0){

                   // INTRA

                   if($acl_non_fac==0 AND $acl_ca_nc==0){
                       if(isset($_POST["ca_intra"])){
							if(!empty($_POST["ca_intra"])){
								$ca=floatval($_POST["ca_intra"]);
							}
                       }

                        if($derogation<2){
                          $coeff_ca=0;
                          if($d_produit["nb_demi_jour"]!=0){

                              $coeff_ca=$nb_demi_jour/$d_produit["nb_demi_jour"];
                          }

                          if($derogation==1 AND $ca<$d_produit["min_dr_intra"]*$coeff_ca){
                              $ca=$d_produit["min_dr_intra"]*$coeff_ca;
                          }elseif($derogation==0 AND $ca<$d_produit["min_intra"]*$coeff_ca){

                              $ca=$d_produit["min_intra"]*$coeff_ca;
                          }
                        }


					   $ca=round($ca,2);
                   }
                   // maj intra ne met pas a jour le nb sta -> même si on supprime la participation d'un client à certaine date et par consequent la partcipation de certain stagiaire au session
                   // 		-> la participation des stagiaires à l'action est conserver
                   // 			-> acl_for_nb_sta n'a pas à être actualisé

                   $acl_for_nb_sta=$avant_modif["acl_for_nb_sta"];
                   $acl_for_nb_sta_resa=$avant_modif["acl_for_nb_sta_resa"];

               }else{

                   // INTER
                   if($acl_non_fac==0 AND $acl_ca_nc==0){
                       if(isset($_POST["ca_inter_unit"])){
                           if(!empty($_POST["ca_inter_unit"])){
                               $ca_unit=floatval($_POST["ca_inter_unit"]);
                           }
                       }
                       if($derogation<2){
                           if($derogation==1 AND $ca_unit<$d_produit["min_dr_inter"]){
                               $ca_unit=$d_produit["min_dr_inter"];
                           }elseif($derogation==0 AND $ca_unit<$d_produit["min_inter"]){
                               $ca_unit=$d_produit["min_inter"];
                           }
                       }
                   }

                   $acl_for_nb_sta=0;
                   if(!empty($_POST["for_nb_sta"])){
                       $acl_for_nb_sta=intval($_POST["for_nb_sta"]);
                   }

                   $acl_for_nb_sta_resa=0;
                   if(!empty($_POST["for_nb_sta_resa"])){
                       $acl_for_nb_sta_resa=intval($_POST["for_nb_sta_resa"]);
                   }

                   /*
                   ANCIEN CODE QUI VERROUILLAIT LE NB STA S'IL Y AVAIT DES INSCRIT
                   $sql="SELECT COUNT(ast_stagiaire) as nb_sta FROM Actions_Stagiaires WHERE ast_action_client=:action_client_id AND ast_confirme;";
                   $req=$ConnSoc->prepare($sql);
                   $req->bindParam(":action_client_id",$action_client_id);
                   $req->execute();
                   $d_inscription=$req->fetch();
                   if(!empty($d_inscription)){
                       if(!empty($d_inscription["nb_sta"])){
                           $acl_for_nb_sta=$d_inscription["nb_sta"];
                       }
                   }*/

                   $ca=$ca_unit*$acl_for_nb_sta;
				   $ca=round($ca,2);


               }

               // calcul de l'état de facturation

               if($avant_modif["acl_facture_ht"]==$ca){
                   $acl_facture=1;
               }else{
                   $acl_facture=0;
               }

               // get produit en aff 1 => $ca déja minoré
               $ca_gfc=0;
               $acl_facture_gfc=0;
               if($d_produit["facturation"]==1){
                   $ca_gfc=$ca/0.88;
				   $ca_gfc=round($ca_gfc,2);
                   if($avant_modif["acl_facture_gfc_ht"]==$ca_gfc){
                       $acl_facture_gfc=1;
                   }else{
                       $acl_facture_gfc=0;
                   }
               }

               $sql="UPDATE Actions_Clients SET acl_client=:client ,acl_pro_inter=:pro_inter ,acl_produit=:produit ,acl_pro_reference=:pro_reference
               ,acl_pro_libelle=:pro_libelle ,acl_commercial=:commercial
               ,acl_ca=:ca ,acl_ca_unit=:ca_unit, acl_for_nb_sta=:acl_for_nb_sta, acl_for_nb_sta_resa=:acl_for_nb_sta_resa, acl_confirme=:confirme ,acl_confirme_date=:confirme_date
               ,acl_opca_fac=:opca_fac ,acl_opca_num=:opca_num,acl_bc_num=:acl_bc_num, acl_attestation=:acl_attestation
               ,acl_archive=:archive, acl_archive_date=:archive_date, acl_archive_uti=:archive_uti,acl_archive_ok=:archive_ok,acl_commentaire=:acl_commentaire
               ,acl_derogation=:derogation,acl_rem_ca=:rem_ca,acl_rem_famille=:rem_famille,acl_facturation=:facturation,acl_ca_gfc=:ca_gfc, acl_non_fac=:acl_non_fac
               ,acl_non_fac_gfc=:acl_non_fac_gfc,acl_ca_nc=:acl_ca_nc,acl_facture=:acl_facture,acl_facture_gfc=:acl_facture_gfc,acl_reporte=:acl_reporte";
               $sql.=" WHERE acl_id=:action_client AND acl_action=:action_id;";
               $req=$ConnSoc->prepare($sql);
               $req->bindParam(":client",$client);
               $req->bindParam(":pro_inter",$acl_pro_inter);
               $req->bindParam(":produit",$produit);
               $req->bindParam(":pro_reference",$d_produit["reference"]);
               $req->bindParam(":pro_libelle",$_POST["acl_pro_libelle"]);
               $req->bindParam(":commercial",$commercial);
               $req->bindParam(":ca",$ca);
               $req->bindParam(":ca_unit",$ca_unit);
               $req->bindParam(":acl_for_nb_sta",$acl_for_nb_sta);
               $req->bindParam(":acl_for_nb_sta_resa",$acl_for_nb_sta_resa);
               $req->bindParam(":confirme",$confirme);
               $req->bindParam(":confirme_date",$confirme_date);
               $req->bindParam(":opca_fac",$opca_fac);
               $req->bindParam(":opca_num",$_POST["opca_num"]);
               $req->bindParam(":acl_bc_num",$_POST["acl_bc_num"]);
               $req->bindParam(":acl_attestation",$acl_attestation);
               $req->bindParam(":archive",$archive);
               $req->bindParam(":archive_date",$archive_date);
               $req->bindParam(":archive_uti",$archive_uti);
               $req->bindParam(":archive_ok",$archive_ok);
               $req->bindParam(":acl_commentaire",$acl_commentaire);
               $req->bindParam(":derogation",$derogation);
               $req->bindParam(":rem_ca",$rem_ca);
               $req->bindParam(":rem_famille",$rem_famille);
               $req->bindParam(":facturation",$d_produit["facturation"]);
               $req->bindParam(":ca_gfc",$ca_gfc);
               $req->bindParam(":acl_non_fac",$acl_non_fac);
               $req->bindParam(":acl_non_fac_gfc",$acl_non_fac_gfc);
               $req->bindParam(":acl_ca_nc",$acl_ca_nc);
               $req->bindParam(":action_client",$action_client_id);
               $req->bindParam(":acl_facture",$acl_facture);
               $req->bindParam(":acl_facture_gfc",$acl_facture_gfc);
				$req->bindParam(":acl_reporte",$acl_reporte);
				$req->bindParam(":action_id",$action_id);

               try{
                   $req->execute();
               }Catch(Exception $e){
                   $erreur_txt="EDITION : " . $e->getMessage();
               }

               // TRAITEMENT ANNEXE EDITION
               if(empty($erreur_txt)){

                   if($avant_modif["acl_client"]!=$client){

                       //echo("CHANGEMENT DE CLIENT<br/>");

                       // CHANGEMENT DE CLIENT

                       // suppression des stagiaires

                       // si il y a des signatures

                        $sql="SELECT * FROM Actions_Stagiaires_Sessions
                        LEFT JOIN Actions_Stagiaires ON (Actions_Stagiaires.ast_stagiaire = Actions_Stagiaires_Sessions.ass_stagiaire)
                         WHERE ass_action=:ass_action AND ast_action = :ass_action AND ast_action_client = :ast_action_client;";
                        $req=$ConnSoc->prepare($sql);
                        $req->bindParam(":ass_action", $action_id);
                        $req->bindParam(":ast_action_client", $action_client_id);
                        $req->execute();
                        $actions_stagiaires_sessions=$req->fetchAll();
                        $_del = true;
                        foreach($actions_stagiaires_sessions as $ass) {
                            if (!empty($ass['ass_signature'])) {
                                $_del = false;
                            }

                            $sql="SELECT cli_code,cli_nom FROM Clients WHERE cli_id=:client_id;";
                            $req=$ConnSoc->prepare($sql);
                            $req->bindParam(":client_id",$client);
                            $d_client=$req->fetch();

                            $req=$Conn->prepare("UPDATE Stagiaires SET
                            sta_client_code=:sta_client_code,
                            sta_client_nom=:sta_client_nom,
                            sta_client=:sta_client
                            WHERE sta_id=:stagiaire_id;");
                            $req->bindParam(":sta_client_code",$d_client["cli_code"]);
                            $req->bindParam(":sta_client_nom",$d_client["cli_nom"]);
                            $req->bindParam(":sta_client",$client);
                            $req->bindParam(":stagiaire_id",$ass['ass_stagiaire']);
                            $req->execute();
                            try {
                                $req=$Conn->prepare("INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:scl_stagiaire,:scl_client);");
                            $req->bindParam(":scl_stagiaire",$ass['ass_stagiaire']);
                            $req->bindParam(":scl_client",$client);
                            $req->execute();
                            } catch(Exception $e) {

                            }


                            if (!empty($ass['ast_diplome'])) {
                                $req=$Conn->prepare("UPDATE Diplomes_Caces SET dca_client = :dca_client
                                WHERE dca_id=:dca_id;");
                                $req->bindParam(":dca_client",$client);
                                $req->bindParam(":dca_id",$ass['ast_diplome']);
                                $req->execute();
                            }

                        }

                        if ($_del) {
                            del_action_stagiaires($action_id,$action_client_id);
                        }

                       $sql="SELECT con_numero FROM Conventions WHERE con_action=:action AND con_action_client=:action_client;";
                       $req=$ConnSoc->prepare($sql);
                       $req->bindParam(":action",$action_id);
                       $req->bindParam(":action_client",$action_client_id);
                       $req->execute();
                       $avant_convention = $req->fetch();
                       // suppression de la convention
                       $sql="DELETE FROM Conventions WHERE con_action=:action AND con_action_client=:action_client;";
                       $req=$ConnSoc->prepare($sql);
                       $req->bindParam(":action",$action_id);
                       $req->bindParam(":action_client",$action_client_id);
                       $req->execute();
                       if(file_exists("documents/Societes/". $acc_societe."/Conventions/" . $avant_convention["con_numero"] . ".pdf")){
                           unlink("documents/Societes/". $acc_societe."/Conventions/" . $avant_convention["con_numero"] . ".pdf");
                       }

                       // Suppression interco
                       $sql="DELETE FROM Actions_Clients_Intercos WHERE aci_action_client=:action_client_id;";
                       $req=$ConnSoc->prepare($sql);
                       $req->bindParam(":action_client_id",$action_client_id);
                       $req->execute();

                   }

                   if($avant_modif["acl_ca_unit"]!=$ca_unit OR $avant_modif["acl_ca"]!=$ca){

                       //echo("CHANGEMENT DE CLIENT<br/>");

                       // CHANGEMENT DE CLIENT

                       //select convention

                       $sql="SELECT con_numero FROM Conventions WHERE con_action=:action AND con_action_client=:action_client;";
                       $req=$ConnSoc->prepare($sql);
                       $req->bindParam(":action",$action_id);
                       $req->bindParam(":action_client",$action_client_id);
                       $req->execute();
                       $avant_convention = $req->fetch();
                       // suppression de la convention
                       $sql="DELETE FROM Conventions WHERE con_action=:action AND con_action_client=:action_client;";
                       $req=$ConnSoc->prepare($sql);
                       $req->bindParam(":action",$action_id);
                       $req->bindParam(":action_client",$action_client_id);
                       $req->execute();

                       if(file_exists("documents/Societes/". $acc_societe."/Conventions/" . $avant_convention["con_numero"] . ".pdf")){
                           unlink("documents/Societes/". $acc_societe."/Conventions/" . $avant_convention["con_numero"] . ".pdf");
                       }
                   }
               }

           }else{

               //**************
               //	CREATION
               //**************

               // creation d'une fiche particulier

               if($add_particulier){

                   $cli_adresse=0;
                   $cli_contact=0;
                   $cli_stagiaire=0;

                   $cli_nom="";
                   if($con_titre==1){
                       $cli_nom="M.";
                   }elseif($con_titre==2){
                       $cli_nom="Mme";
                   }
                   $cli_nom.=" " . $con_nom . " " . $con_prenom;

                   // AJOUT DU CLIENT

                   $sql_cli="INSERT INTO Clients (cli_code,cli_nom,cli_categorie,cli_date_creation,cli_reg_type,cli_reg_formule,cli_uti_creation,cli_releve,cli_stagiaire_type
                   ,cli_adr_nom,cli_adr_ad1,cli_adr_ad2,cli_adr_ad3,cli_adr_cp,cli_adr_ville
                   ,cli_con_titre ,cli_con_nom,cli_con_prenom,cli_con_tel,cli_con_mail)
                   VALUES (:cli_code,:cli_nom,3,NOW(),1,1,:cli_uti_creation,1,4
                   ,:cli_adr_nom,:cli_adr_ad1,:cli_adr_ad2,:cli_adr_ad3,:cli_adr_cp,:cli_adr_ville
                   ,:cli_con_titre,:cli_con_nom,:cli_con_prenom,:cli_con_tel,:cli_con_mail);";
                   $req_cli=$Conn->prepare($sql_cli);
                   $req_cli->bindParam(":cli_code",$cli_code);
                   $req_cli->bindParam(":cli_nom",$cli_nom);
                   $req_cli->bindParam(":cli_uti_creation",$acc_utilisateur);
                   $req_cli->bindParam(":cli_adr_nom",$cli_nom);
                   $req_cli->bindParam(":cli_adr_ad1",$adr_ad1);
                   $req_cli->bindParam(":cli_adr_ad2",$_POST["par_adr2"]);
                   $req_cli->bindParam(":cli_adr_ad3",$_POST["par_adr3"]);
                   $req_cli->bindParam(":cli_adr_cp",$adr_cp);
                   $req_cli->bindParam(":cli_adr_ville",$adr_ville);
                   $req_cli->bindParam(":cli_con_titre",$con_titre);
                   $req_cli->bindParam(":cli_con_nom",$con_nom);
                   $req_cli->bindParam(":cli_con_prenom",$con_prenom);
                   $req_cli->bindParam(":cli_con_tel",$_POST["par_tel"]);
                   $req_cli->bindParam(":cli_con_mail",$_POST["par_mail"]);
                   try{
                       $req_cli->execute();
                       $client=$Conn->lastInsertId();
                   }Catch(Exception $e){
                       $erreur_txt="CREATION PARTICULIER " . $e->getMessage();
                   }

                   // CREATION DE L'ADRESSE DE FAC
                   if(empty($erreur_txt)){

                       $sql_cli_adr="INSERT INTO Adresses (adr_ref,adr_ref_id,adr_type,adr_nom,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_defaut,adr_geo)
                       VALUES (1,:adr_ref_id,2,:adr_nom,:adr_ad1,:adr_ad2,:adr_ad3,:adr_cp,:adr_ville,1,1);";
                       $req_cli_adr=$Conn->prepare($sql_cli_adr);
                       $req_cli_adr->bindParam(":adr_ref_id",$client);
                       $req_cli_adr->bindParam(":adr_nom",$cli_nom);
                       $req_cli_adr->bindParam(":adr_ad1",$adr_ad1);
                       $req_cli_adr->bindParam(":adr_ad2",$_POST["par_adr2"]);
                       $req_cli_adr->bindParam(":adr_ad3",$_POST["par_adr3"]);
                       $req_cli_adr->bindParam(":adr_cp",$adr_cp);
                       $req_cli_adr->bindParam(":adr_ville",$adr_ville);
                       $req_cli_adr->bindParam(":adr_cp",$adr_cp);
                       $req_cli_adr->bindParam(":adr_ville",$adr_ville);
                       try{
                           $req_cli_adr->execute();
                           $cli_adresse=$Conn->lastInsertId();
                       }Catch(Exception $e){
                           $erreur_txt="CREATION PAR ADRESSE " . $e->getMessage();
                       }

                   }

                   // CREATION DU CONTACT
                   if(empty($erreur_txt)){

                       $sql_cli_adr="INSERT INTO Contacts (con_ref,con_ref_id,con_titre,con_nom,con_prenom,con_tel,con_portable,con_fax,con_mail,con_compta)
                       VALUES (1,:con_ref_id,:con_titre,:con_nom,:con_prenom,:con_tel,:con_portable,:con_fax,:con_mail,1);";
                       $req_cli_adr=$Conn->prepare($sql_cli_adr);
                       $req_cli_adr->bindParam(":con_ref_id",$client);
                       $req_cli_adr->bindParam(":con_titre",$con_titre);
                       $req_cli_adr->bindParam(":con_nom",$con_nom);
                       $req_cli_adr->bindParam(":con_prenom",$con_prenom);
                       $req_cli_adr->bindParam(":con_tel",$_POST["par_tel"]);
                       $req_cli_adr->bindParam(":con_portable",$_POST["par_portable"]);
                       $req_cli_adr->bindParam(":con_fax",$_POST["par_fax"]);
                       $req_cli_adr->bindParam(":con_mail",$_POST["par_mail"]);
                       try{
                           $req_cli_adr->execute();
                           $cli_contact=$Conn->lastInsertId();
                       }Catch(Exception $e){
                           $erreur_txt="CREATION PAR CONTACT " . $e->getMessage();
                       }

                   }


                   if(empty($erreur_txt)){

                       $d_client=array(
                           "cli_categorie" => 3,
                           "cli_nom" => $cli_nom,
                           "adr_id" => $cli_adresse,
                           "adr_nom" => $cli_nom,
                           "adr_service" => "",
                           "adr_ad1" => $adr_ad1,
                           "adr_ad2" => $_POST["par_adr2"],
                           "adr_ad3" => $_POST["par_adr3"],
                           "adr_cp" => $adr_cp,
                           "adr_ville" => $adr_ville,
                           "adr_geo" => 1,
                           "cli_contact" => $cli_contact,
                           "cli_con_fct" => "",
                           "cli_con_fct_nom" => "",
                           "cli_con_titre" => $con_titre,
                           "cli_con_nom" => $con_nom,
                           "cli_con_prenom" => $con_prenom,
                           "cli_con_tel" => $_POST["par_tel"],
                           "cli_con_mail" => $_POST["par_mail"],
                           "cli_reg_type" => 1,
                           "cli_reg_formule" => 1,
                           "cli_reg_nb_jour" => 0,
                           "cli_reg_fdm" => 0

                       );



                       // CREATION DU STAGIAIRE

                       $sql_get_sta="SELECT sta_id FROM Stagiaires WHERE sta_nom=:sta_nom AND sta_prenom=:sta_prenom AND sta_naissance=:sta_naissance;";
                       $req_get_sta=$Conn->prepare($sql_get_sta);
                       $req_get_sta->bindParam(":sta_nom",$con_nom);
                       $req_get_sta->bindParam(":sta_prenom",$con_prenom);
                       $req_get_sta->bindParam(":sta_naissance",$sta_naissance);
                       $req_get_sta->execute();
                       $d_stagiaire=$req_get_sta->fetch();
                       if(!empty($d_stagiaire)){
                           $cli_stagiaire=$d_stagiaire["sta_id"];
                       }else{

                           // LE STA N'EXISTE PAS ON LE CREE

                           $sql_add_sta="INSERT INTO Stagiaires (sta_nom,sta_prenom,sta_naissance,sta_mail_perso,sta_tel,sta_portable,sta_titre,sta_client,sta_client_code,sta_client_nom)
                           VALUES (:sta_nom,:sta_prenom,:sta_naissance,:sta_mail_perso,:sta_tel,:sta_portable,:sta_titre,:sta_client,:sta_client_code,:sta_client_nom);";
                           $req_add_sta=$Conn->prepare($sql_add_sta);
                           $req_add_sta->bindParam(":sta_nom",$con_nom);
                           $req_add_sta->bindParam(":sta_prenom",$con_prenom);
                           $req_add_sta->bindParam(":sta_naissance",$sta_naissance);
                           $req_add_sta->bindParam(":sta_mail_perso",$_POST["par_mail"]);
                           $req_add_sta->bindParam(":sta_tel",$_POST["par_tel"]);
                           $req_add_sta->bindParam(":sta_portable",$_POST["par_portable"]);
                           $req_add_sta->bindParam(":sta_titre",$sta_titre);
                           $req_add_sta->bindParam(":sta_client",$client);
                           $req_add_sta->bindParam(":sta_client_code",$cli_code);
                           $req_add_sta->bindParam(":sta_client_nom",$cli_nom);
                           try{
                               $req_add_sta->execute();
                               $cli_stagiaire=$Conn->lastInsertId();
                           }Catch(Exception $e){
                               $erreur_txt="CREATION STAGIAIRE " . $e->getMessage();
                           }
                       }
                   }

                   // ON LIE LE STA ET LE CLIENT
                   if(empty($erreur_txt)){

                       $sql_lie_sta_cli="INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:scl_stagiaire,:scl_client);";
                       $req_lie_sta_cli=$Conn->prepare($sql_lie_sta_cli);
                       $req_lie_sta_cli->bindParam(":scl_stagiaire",$cli_stagiaire);
                       $req_lie_sta_cli->bindParam(":scl_client",$client);
                       try{
                           $req_lie_sta_cli->execute();
                       }Catch(Exception $e){
                           $erreur_txt="LIEN STAGIAIRE CLIENT " . $e->getMessage();
                       }
                   }

                   // ON MAJ LE CLIENT

                   $sql_up_cli="UPDATE Clients SET cli_adresse=:cli_adresse,cli_contact=:cli_contact,cli_stagiaire=:cli_stagiaire WHERE cli_id=:cli_id;";
                   $req_up_cli=$Conn->prepare($sql_up_cli);
                   $req_up_cli->bindParam(":cli_adresse",$cli_adresse);
                   $req_up_cli->bindParam(":cli_contact",$cli_contact);
                   $req_up_cli->bindParam(":cli_stagiaire",$cli_stagiaire);
                   $req_up_cli->bindParam(":cli_id",$client);
                   try{
                       $req_up_cli->execute();
                   }Catch(Exception $e){
                       $erreur_txt="MAJ PARTICULIER " . $e->getMessage();
                   }

                   // ON AFFECT LE CLIENT A LA SOCIETE

                   $sql_cli_soc="INSERT INTO Clients_Societes (cso_client,cso_societe,cso_agence,cso_commercial,cso_utilisateur,cso_rib,cso_rib_change ) VALUES (
                   :cso_client,:cso_societe,:cso_agence,:cso_commercial,:cso_utilisateur,:cso_rib,:cso_rib_change);";
                   $req_cli_soc=$Conn->prepare($sql_cli_soc);
                   $req_cli_soc->bindParam(":cso_client",$client);
                   $req_cli_soc->bindParam(":cso_societe",$acc_societe);
                   $req_cli_soc->bindParam(":cso_agence",$com_agence);
                   $req_cli_soc->bindParam(":cso_commercial",$cso_commercial);
                   $req_cli_soc->bindParam(":cso_utilisateur",$com_utilisateur);
                   $req_cli_soc->bindParam(":cso_rib",$soc_rib);
                   $req_cli_soc->bindParam(":cso_rib_change",$soc_rib_change);
                   try{
                       $req_cli_soc->execute();
                   }Catch(Exception $e){
                       $erreur_txt="AFFECTATION CLI SOC " . $e->getMessage();
                   }

                   // ENREGISTREMENT DU CLIENT EN BASE N

                   $sql_cli_n="INSERT INTO Clients (cli_id,cli_agence,cli_code,cli_nom,cli_commercial,cli_utilisateur,cli_categorie,cli_rib,cli_rib_change,cli_cp
                   ) VALUES (:cli_id,:cli_agence,:cli_code,:cli_nom,:cli_commercial,:cli_utilisateur,3,:cli_rib,:cli_rib_change,:cli_cp);";
                   $req_cli_n=$ConnSoc->prepare($sql_cli_n);
                   $req_cli_n->bindParam(":cli_id",$client);
                   $req_cli_n->bindParam(":cli_agence",$com_agence);
                   $req_cli_n->bindParam(":cli_code",$cli_code);
                   $req_cli_n->bindParam(":cli_nom",$cli_nom);
                   $req_cli_n->bindParam(":cli_commercial",$commercial);
                   $req_cli_n->bindParam(":cli_utilisateur",$com_utilisateur);
                   $req_cli_n->bindParam(":cli_rib",$soc_rib);
                   $req_cli_n->bindParam(":cli_rib_change",$soc_rib_change);
                   $req_cli_n->bindParam(":cli_cp",$adr_cp);
                   try{
                       $req_cli_n->execute();
                   }Catch(Exception $e){
                       $erreur_txt="CLIENT BASE N " . $e->getMessage();
                   }

               }
               // FIN AJOUT PARTICULIER


               // CREATION DE L'INSCRIPTION

               if(empty($erreur_txt)){

                   //echo("NOUVELLE INSCRIPTION<br/>");

                   $operation=1;

                   // CALCUL DU CA
                   // creation donc il n'y a pas d'inscrit

                   $acl_for_nb_sta=0;
                   $ca_unit=0;
                   $ca=0;

                   if($acl_pro_inter==0){
                       // INTRA

                       if($acl_ca_nc==0){
                           if(isset($_POST["ca_intra"])){
                               if(!empty($_POST["ca_intra"])){
                                   $ca=floatval($_POST["ca_intra"]);
                               }
                           }

                           if($derogation<2){
                               $coeff_ca=0;
                               if($d_produit["nb_demi_jour"]!=0){
                                   $coeff_ca=$nb_demi_jour/$d_produit["nb_demi_jour"];
                               }

                               if($derogation==1 AND $ca<$d_produit["min_dr_intra"]*$coeff_ca){
                                   $ca=$d_produit["min_dr_intra"]*$coeff_ca;
                               }elseif($derogation==0 AND $ca<$d_produit["min_intra"]*$coeff_ca){
                                   $ca=$d_produit["min_intra"]*$coeff_ca;
                               }
                           }

						    $ca=round($ca,2);
                       }

                   }else{
                       // inter
                       if($acl_ca_nc==0){
                           if(isset($_POST["ca_inter_unit"])){
                               if(!empty($_POST["ca_inter_unit"])){
                                   $ca_unit=floatval($_POST["ca_inter_unit"]);
                               }
                           }
                       }
                       if($derogation<2){
                           if($derogation==1 AND $ca_unit<$d_produit["min_dr_inter"]){
                               $ca_unit=$d_produit["min_dr_inter"];
                           }elseif($derogation==0 AND $ca_unit<$d_produit["min_inter"]){
                               $ca_unit=$d_produit["min_inter"];
                           }
                       }

                       if($d_client["cli_categorie"]==3){
                           $ca=$ca_unit;
                           if($confirme==1){
                               $acl_for_nb_sta=1;
                               $acl_for_nb_sta_resa=0;
                           }else{
                               $acl_for_nb_sta=0;
                               $acl_for_nb_sta_resa=1;
                           }
                       }else{
                           if(!empty($_POST["for_nb_sta"])){
                               $acl_for_nb_sta=intval($_POST["for_nb_sta"]);
                           }
                           if(!empty($_POST["for_nb_sta_resa"])){
                               $acl_for_nb_sta_resa=intval($_POST["for_nb_sta_resa"]);
                           }
                           $ca=$ca_unit*$acl_for_nb_sta;
                       }
					   $ca=round($ca,2);
                   }

                   $ca_gfc=0;
                   if($d_produit["facturation"]==1){
                       $ca_gfc=$ca/0.88;
					   $ca_gfc=round($ca_gfc,2);
                   }

                   $sql="INSERT INTO Actions_Clients (
                   acl_action,
                   acl_client,
                   acl_pro_inter,
                   acl_produit,
                   acl_pro_reference,
                   acl_pro_libelle,
                   acl_commercial,
                   acl_ca,
                   acl_ca_unit,
                   acl_ca_gfc,
                   acl_facturation,
                   acl_confirme,
                   acl_confirme_date,
                   acl_archive,
                   acl_creation_uti,
                   acl_creation_date,
                   acl_opca_fac,
                   acl_opca_num,
                   acl_bc_num,
                   acl_commentaire,
                   acl_for_nb_sta,
                   acl_for_nb_sta_resa,
                   acl_derogation,
                   acl_rem_ca,
                   acl_rem_famille,
                   acl_ca_nc,
                   acl_attestation,
				   acl_reporte
                   ) VALUES (
                   :action,
                   :client,
                   :pro_inter,
                   :produit,
                   :pro_reference,
                   :pro_libelle,
                   :commercial,
                   :ca,
                   :ca_unit,
                   :ca_gfc,
                   :facturation,
                   :confirme,
                   :confirme_date,
                   :archive,
                   :creation_uti,
                   NOW(),
                   :opca_fac,
                   :opca_num,
                   :acl_bc_num,
                   :acl_commentaire,
                   :acl_for_nb_sta,
                   :acl_for_nb_sta_resa,
                   :derogation,
                   :rem_ca,
                   :rem_famille,
                   :acl_ca_nc,
                   :acl_attestation,
				   :acl_reporte);";
                   $req=$ConnSoc->prepare($sql);
                   $req->bindParam(":action",$action_id);
                   $req->bindParam(":client",$client);
                   $req->bindParam(":pro_inter",$acl_pro_inter);
                   $req->bindParam(":produit",$produit);
                   $req->bindParam(":pro_reference",$d_produit["reference"]);
                   $req->bindParam(":pro_libelle",$_POST["acl_pro_libelle"]);
                   $req->bindParam(":commercial",$commercial);
                   $req->bindParam(":ca",$ca);
                   $req->bindParam(":ca_unit",$ca_unit);
                   $req->bindParam(":ca_gfc",$ca_gfc);
                   $req->bindParam(":facturation",$d_produit["facturation"]);
                   $req->bindParam(":confirme",$confirme);
                   $req->bindParam(":confirme_date",$confirme_date);
                   $req->bindParam(":archive",$archive);
                   $req->bindParam(":creation_uti",$acc_utilisateur);
                   $req->bindParam(":opca_fac",$opca_fac);
                   $req->bindParam(":opca_num",$_POST["opca_num"]);
                   $req->bindParam(":acl_bc_num",$_POST["acl_bc_num"]);
                   $req->bindParam(":acl_commentaire",$acl_commentaire);
                   $req->bindParam(":acl_for_nb_sta",$acl_for_nb_sta);
                   $req->bindParam(":acl_for_nb_sta_resa",$acl_for_nb_sta_resa);
                   $req->bindParam(":derogation",$derogation);
                   $req->bindParam(":rem_ca",$rem_ca);
                   $req->bindParam(":rem_famille",$rem_famille);
                   $req->bindParam(":acl_ca_nc",$acl_ca_nc);
                   $req->bindParam(":acl_attestation",$acl_attestation);
				   $req->bindParam(":acl_reporte",$acl_reporte);
                   try{
                       $req->execute();
                       $action_client_id=$ConnSoc->lastInsertId();
                   }Catch(Exception $e){
                       $erreur_txt="CREATION INSCRIPTION " . $e->getMessage();
                   }

                   // C'EST UN PARTCULIER -> INSCRIPTION DU STAGIAIRE

                   if($d_client["cli_categorie"]==3 AND (isset($cli_stagiaire) OR !empty($d_client["cli_stagiaire"]))){

                       //echo("INSCRIPTION STAGIAIRE");
                       if(!isset($cli_stagiaire)){
                           $cli_stagiaire=$d_client["cli_stagiaire"];
                       }
                       $sql="INSERT INTO Actions_Stagiaires (
                       ast_stagiaire,
                       ast_action,
                       ast_action_client,
                       ast_confirme,
                       ast_qualification
                       ) VALUES (
                       :ast_stagiaire,
                       :ast_action,
                       :ast_action_client,
                       :ast_confirme,
                       :ast_qualification);";
                       $req=$ConnSoc->prepare($sql);
                       $req->bindParam(":ast_stagiaire",$cli_stagiaire);
                       $req->bindParam(":ast_action",$action_id);
                       $req->bindParam(":ast_action_client",$action_client_id);
                       $req->bindValue(":ast_confirme",1);
                       $req->bindParam(":ast_qualification",$d_produit["qualification"]);
                       try{
                           $req->execute();
                       }Catch(Exception $e){
                           $warning_txt="Echec de l'insription du stagiaire " . $e->getMessage();
                       }
                   }
               }

           }
           // FIN ADD MOD ACTION CLIENT
		}

		//********************************
		//	 TRAITEMENT ANNEXE
		//********************************

		if(empty($erreur_txt)){

		   // ENREGISTREMENT DES DATES DE PARTICIPATIONS

		   $sql_add_date="INSERT INTO Actions_Clients_Dates (acd_action_client,acd_date) VALUES (:acd_action_client,:acd_date);";
		   $req_add_date=$ConnSoc->prepare($sql_add_date);
		   $req_add_date->bindParam(":acd_action_client",$action_client_id);

		   $sql_del_date="DELETE FROM Actions_Clients_Dates WHERE acd_action_client=:acd_action_client AND acd_date=:acd_date;";
		   $req_del_date=$ConnSoc->prepare($sql_del_date);
		   $req_del_date->bindParam(":acd_action_client",$action_client_id);


		   $sql_sta_ses_get="SELECT ast_stagiaire,ase_id FROM Actions_Stagiaires INNER JOIN Actions_Sessions ON (Actions_Stagiaires.ast_action=Actions_Sessions.ase_action)
		   WHERE ast_action_client=" . $action_client_id . " AND ase_date=:ase_date;";
		   $req_sta_ses_get=$ConnSoc->prepare($sql_sta_ses_get);

		   $sql_sta_ses_del="DELETE FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:ass_stagiaire AND ass_session=:ass_session;";
		   $req_sta_ses_del=$ConnSoc->prepare($sql_sta_ses_del);

		   $sql_date_ses_get="SELECT ase_id FROM Actions_Sessions WHERE ase_date=:ase_date AND ase_action=:ase_action;";
		   $req_date_ses_get=$ConnSoc->prepare($sql_date_ses_get);
		   $req_date_ses_get->bindParam(":ase_action",$action_id);

		   $sql_ses_sta_add="INSERT INTO Actions_Stagiaires_Sessions (ass_stagiaire,ass_session,ass_action) VALUES (:ass_stagiaire,:ass_session,:ass_action);";
		   $req_ses_sta_add=$ConnSoc->prepare($sql_ses_sta_add);
		   $req_ses_sta_add->bindParam("ass_action",$action_id);

		   // Présences sur les dates de formation

		   $qte_pm=0;
		   $periode=array();
		   $sql="SELECT pda_id,pda_date,pda_demi,acd_date FROM Plannings_Dates LEFT JOIN Actions_Clients_Dates ON (Plannings_Dates.pda_id=Actions_Clients_Dates.acd_date AND acd_action_client=:action_client_id)
		   WHERE pda_type=1 AND pda_ref_1=:action_id ORDER BY pda_date,pda_demi,pda_id;";
		   $req=$ConnSoc->prepare($sql);
		   $req->bindParam(":action_id",$action_id);
		   $req->bindParam(":action_client_id",$action_client_id);
		   $req->execute();
		   $d_dates=$req->fetchAll();
		   if(!empty($d_dates)){
			   foreach($d_dates as $d){

				   if(!empty($_POST["date_" . $d["pda_id"]]) AND empty($d["acd_date"])){

					   // AJOUT DE LA DATE
					   $req_add_date->bindParam(":acd_date",$d["pda_id"]);
					   try{
						   $req_add_date->execute();
						   $qte_pm++;

						   $periode[]=array(
							   "pda_date" => $d["pda_date"],
							   "pda_demi" => $d["pda_demi"]
						   );
					   }Catch(Exception $e){
						   $warning_txt=$e->getMessage();
					   }

					   // cli_stagiaire existe si créer depuis action_client ou si client est un particulier
					   if(isset($cli_stagiaire)){

						   $req_date_ses_get->bindParam("ase_date",$d["pda_id"]);
						   $req_date_ses_get->execute();
						   $d_sta_session=$req_date_ses_get->fetchAll();
						   if(!empty($d_sta_session)){
							   foreach($d_sta_session as $sta_ses){
								   $req_ses_sta_check="SELECT * FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire = :ass_stagiaire AND ass_session = :ass_session AND ass_action=:ass_action;";
								   $req_ses_sta_check=$ConnSoc->prepare($req_ses_sta_check);
								   $req_ses_sta_check->bindParam("ass_action",$action_id);
								   $req_ses_sta_check->bindParam("ass_stagiaire",$cli_stagiaire);
								   $req_ses_sta_check->bindParam("ass_session",$sta_ses["ase_id"]);
								   $req_ses_sta_check->execute();

								   $d_ses_sta_check=$req->fetch();
								   if(!empty($d_ses_sta_check)){
									   $req_ses_sta_add->bindParam("ass_stagiaire",$cli_stagiaire);
									   $req_ses_sta_add->bindParam("ass_session",$sta_ses["ase_id"]);
									   $req_ses_sta_add->execute();
								   }


							   }
						   }

					   }


				   }elseif(empty($_POST["date_" . $d["pda_id"]]) AND !empty($d["acd_date"])){
					   // DEL PARTICIPATION DATE
					   $req_del_date->bindParam(":acd_date",$d["pda_id"]);
					   try{
						   $req_del_date->execute();
					   }Catch(Exception $e){
						   $warning_txt=$e->getMessage();
					   }

					   // DEL PARTICIPATION STAGIAIRE SESION

					   $req_sta_ses_get->bindParam(":ase_date",$d["pda_id"]);
					   $req_sta_ses_get->execute();
					   $d_stagiaires=$req_sta_ses_get->fetchAll();
					   if(!empty($d_stagiaires)){
						   foreach($d_stagiaires as $sta){
							   $req_sta_ses_del->bindParam("ass_stagiaire",$sta["ast_stagiaire"]);
							   $req_sta_ses_del->bindParam("ass_session",$sta["ase_id"]);
							   $req_sta_ses_del->execute();
						   }
					   }

				   }else{
					   $qte_pm++;

					   $periode[]=array(
						   "pda_date" => $d["pda_date"],
						   "pda_demi" => $d["pda_demi"]
					   );

				   }
			   }
			   // SI INTRA ON MAJ LE NOM DE DEMI JOUR
			   if($acl_pro_inter==0){

				   $sql="UPDATE Actions_Clients SET acl_qte_pm=:qte_pm WHERE acl_id=:action_client AND acl_action=:action_id;";
				   $req=$ConnSoc->prepare($sql);
				   $req->bindParam(":action_client",$action_client_id);
				   $req->bindParam(":action_id",$action_id);
				   $req->bindParam(":qte_pm",$qte_pm);
				   try{
					   $req->execute();
				   }Catch(Exception $e){
					   $warning_txt=$e->getMessage();
				   }

			   }
		   }

		   // ARCHIVAGE
		   if($archive==1 AND empty($avant_modif["acl_archive"])){

			   // L'uti n'a pas la validation de l'archivage
			   if($archive_ok==0){

			   /*	$uti_notif=0;
				   $uti_ref=$acc_utilisateur;
				   $uti_nom="";
				   $uti_prenom="";

				   $req=$Conn->prepare("SELECT uti_id,uti_nom,uti_prenom,uti_responsable,udr_droit FROM Utilisateurs LEFT JOIN Utilisateurs_Droits
				   ON (Utilisateurs.uti_id=Utilisateurs_Droits.udr_utilisateur AND udr_profil=0 AND udr_droit=11)
				   WHERE uti_id=:uti_ref;");

				   WHILE($uti_notif==0 OR $secur_bcl<4){
					   $req->bindParam(":uti_ref",$uti_ref);
					   $req->execute();
					   $d_responsable=$req->fetch();
					   if(!empty($d_responsable)){
						   if($d_responsable["uti_id"]==$acc_utilisateur){
							   $uti_nom=$d_responsable["uti_nom"];
							   $uti_prenom=$d_responsable["uti_prenom"];
						   }
						   if(!empty($d_responsable["udr_droit"])){
							   $uti_notif=$d_responsable["uti_id"];
						   }else{
							   $uti_ref=$d_responsable["uti_responsable"];
						   }
					   }else{
						   $secur_bcl=5;
						   break;
					   }
					   $secur_bcl++;
				   }

				   if($uti_notif>0){*/

				   $req=$Conn->query("SELECT uti_nom,uti_prenom FROM Utilisateurs WHERE uti_id=" . $acc_utilisateur . ";");
				   $d_utilisateur=$req->fetch();

				   $texte=$d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"] . " a archivé le client " . $d_client["cli_nom"] . " sur l'action N°" . $action_id;
				   $url="action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id;
				   add_notifications("<i class='fa fa-times' ></i>",$texte,"bg-danger",$url,"",11,"",$acc_societe,$d_action["act_agence"],0);
			   /*	}*/
			   }

		   }

		   // ACTUALISATION DU TYPE DE FOMRATION
		   if($actu_classification){

			   //mise a jour de la famille de l'action (possible si un seul client)
			   $sql="UPDATE Actions SET act_pro_categorie=:pro_categorie ,act_pro_famille=:pro_famille ,act_pro_sous_famille=:pro_sous_famille,act_pro_sous_sous_famille=:pro_sous_sous_famille
			   ,act_produit=:produit,act_pro_type=:produit_type WHERE act_id=:action;";
			   $req=$ConnSoc->prepare($sql);
			   $req->bindParam(":action",$action_id);
			   $req->bindParam(":pro_categorie",$d_produit["categorie"]);
			   $req->bindParam(":pro_famille",$d_produit["famille"]);
			   $req->bindParam(":pro_sous_famille",$d_produit["sous_famille"]);
			   $req->bindParam(":pro_sous_sous_famille",$d_produit["sous_sous_famille"]);
			   $req->bindParam(":produit",$act_produit);
			   $req->bindParam(":produit_type",$act_pro_type);
			   $req->execute();

			   // le changement de date impact le style des cases
               if(isset($pda_style_bg))
               {

				   $sql="UPDATE Plannings_Dates SET pda_style_bg=:pda_style_bg ,pda_style_txt=:pda_style_txt WHERE pda_vehicule=0 AND pda_type=1 AND pda_ref_1=:action_id;";
				   $req=$ConnSoc->prepare($sql);
				   $req->bindParam(":action_id",$action_id);
				   $req->bindParam(":pda_style_txt",$pda_style_txt);
				   $req->bindParam(":pda_style_bg",$pda_style_bg);
				   $req->execute();
			   }

                // si empty($d_action) => erreur_txt!="" donc le code ci-dessous est toujours execute ce qui n'est pas pertinent.
                /*}elseif(empty($d_action["act_produit"]) OR $d_action["act_produit"]<1208){

                    $sql="UPDATE Actions SET act_produit=:produit WHERE act_id=:action;";
                    $req=$ConnSoc->prepare($sql);
                    $req->bindParam(":action",$action_id);
                    $req->bindParam(":produit",$produit);
                    $req->execute();

                }*/
            }elseif(!is_null($act_pro_type))
            {
				$sql="UPDATE Actions SET act_produit=:produit,act_pro_type=:produit_type WHERE act_id=:action;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":produit",$act_produit);
				$req->bindParam(":produit_type",$act_pro_type);
				 $req->bindParam(":action",$action_id);
				$req->execute();
			}

			// ACTUALISATION DE L'ETAT DE CONFIRMATION DE LA FORMATION

			$sql="UPDATE Plannings_Dates SET pda_confirme=:pda_confirme";
			if(isset($text_survol)){
				$sql.=",pda_survol=:pda_survol";
			}
			$sql.=" WHERE pda_type=1 AND pda_ref_1=:action_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_id",$action_id);
			$req->bindParam(":pda_confirme",$pda_confirme);
			if(isset($text_survol)){
				$req->bindParam(":pda_survol",$text_survol);
			}
			$req->execute();

           
            // CREATION D'UN DEVIS

            if($add_devis){


			   // on recupere le chrono

			   $dev_chrono=0;
			   $dev_numero="";

			   $sql="SELECT MAX(dev_chrono) FROM Devis;";
			   $req=$ConnSoc->query($sql);
			   $d_chrono=$req->fetch();
			   if(!empty($d_chrono)){
				   $dev_chrono=$d_chrono[0];
			   }
			   $dev_chrono++;

			   if($acc_societe==16){
				   $dev_numero="D" . date("y") . str_pad($dev_chrono,5,0,STR_PAD_LEFT);
			   }else{
				   $dev_numero="DV" . date("y") . "-" . str_pad($dev_chrono,5,0,STR_PAD_LEFT);
			   }

			   // DONNEEES DEVIS

			   $dev_com_identite=$d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"];

			   $dev_esperance="30";
			   $dev_commande=0;
			   $dev_commande_date=null;
			   if($confirme==1){
				   $dev_esperance="100";
				   $dev_commande=1;
				   $dev_commande_date=$confirme_date;
			   }

			   $dev_validite="3 mois";

			   $dev_con_identite="";
			   if(!empty($d_client["cli_con_titre"])){
				   if($d_client["cli_con_titre"]==2){
					   $dev_con_identite.="Mme ";
				   }elseif($d_client["cli_con_titre"]==1){
					   $dev_con_identite.="M. ";
				   }
			   }
			   if(!empty($d_client["cli_con_prenom"])){
				   $dev_con_identite.=$d_client["cli_con_prenom"] . " ";
			   }
			   if(!empty($d_client["cli_con_nom"])){
				   $dev_con_identite.=$d_client["cli_con_nom"] . " ";
			   }
			   $dev_con_identite=trim($dev_con_identite);

			   // LIGNE DE DEVIS

			   // PRODUITS

			   $dev_total_ht=0;
			   $dev_total_ttc=0;

			   if(!isset($d_produits)){
				   $d_produit=get_client_produits($client,$produit,1,0,1,0,0,0,0,0,0);
			   }

			   // GESTION DE LA TVA

			   $dli_tva_id=0;
			   $dli_exo=0;
			   if(isset($d_societe)){
				   if($d_societe["soc_tva_exo"] AND $d_produit["deductible"]){
					   $dli_tva_id=3;
					   $dli_tva_periode=3;
					   $dli_tva_taux=0;
					   $dli_exo=1;
				   }
			   }
			   if(empty($dli_exo)){
				   $dli_tva_id=$d_produit["tva"];

				   $sql="SELECT tpe_id,tpe_taux FROM Tva_Periodes WHERE tpe_tva=:dli_tva_id AND tpe_date_deb<=:act_date_deb AND (tpe_date_fin>:act_date_deb OR ISNULL(tpe_date_fin) );";
				   $req=$Conn->prepare($sql);
				   $req->bindParam(":dli_tva_id",$dli_tva_id);
				   $req->bindParam(":act_date_deb",$d_action["act_date_deb"]);
				   $req->execute();
				   $d_tva=$req->fetch();
				   if(!empty($d_tva)){
					   $dli_tva_periode=$d_tva["tpe_id"];
					   $dli_tva_taux=$d_tva["tpe_taux"];
				   }else{
					   $dli_tva_id=3;
					   $dli_tva_periode=3;
					   $dli_tva_taux=0;
				   }


			   }

			   // CALCUL DU CA
			   if($acl_pro_inter==1){

				   // iNTER

				   $dli_type=2;
				   $dli_tg=$d_produit["tg_inter"];

				   $dli_qte=$acl_for_nb_sta;
				   $dli_ht=$ca_unit;
				   $dli_montant_ht=$dli_ht*$dli_qte;

				   if($derogation<2){
					   if($derogation==1){
						   $dli_ht_min=$d_produit["min_dr_inter"];
					   }elseif($derogation==0){
						   $dli_ht_min=$d_produit["min_inter"];
					   }
				   }

			   }else{

				   // INTRA

				   $dli_type=1;
				   $dli_tg=$d_produit["tg_intra"];
				   $dli_qte=$coeff_ca;
				   $dli_montant_ht=$ca;
				   $dli_ht=0;
				   if(!empty($dli_qte)){
					   $dli_ht=$dli_montant_ht/$dli_qte;
				   }

				   if($derogation<2){
					   if($derogation==1){
						   $dli_ht_min=$d_produit["min_dr_intra"]*$dli_qte;
					   }elseif($derogation==0){
						   $dli_ht_min=$d_produit["min_intra"]*$dli_qte;
					   }
				   }
			   }

			   $dli_remise=$dli_tg-$dli_ht;
			   $dli_taux_remise=0;
			   if(!empty($dli_tg)){
				   $dli_taux_remise=($dli_remise*100)/$dli_tg;
			   }

			   $dev_total_ttc=$dli_montant_ht*(1+($dli_tva_taux/100));

			   $dli_dates=planning_periode($periode);


			   // ENREGISTREMENT DU DEVIS

			   $sql_add="INSERT INTO DEVIS (dev_agence,dev_commercial,dev_com_identite,dev_numero,dev_chrono,dev_date,dev_prospect,dev_client,
			   dev_adresse,dev_adr_service,dev_adr_nom,dev_adr1,dev_adr2,dev_adr3,dev_adr_cp,dev_adr_ville,dev_adr_geo,dev_contact,dev_con_identite,dev_con_titre,dev_con_nom,dev_con_prenom,
			   dev_con_tel,dev_con_mail,dev_reg_type,dev_reg_formule,dev_reg_nb_jour,dev_reg_fdm,dev_total_ht,dev_total_ttc,
			   dev_validite,dev_esperance,dev_commande,dev_commande_date,dev_utilisateur) VALUES (
			   :dev_agence,:dev_commercial,
			   :dev_com_identite,:dev_numero,:dev_chrono,:dev_date,:dev_prospect,:dev_client,:dev_adresse,:dev_adr_service,:dev_adr_nom,:dev_adr1,:dev_adr2,:dev_adr3,:dev_adr_cp,:dev_adr_ville,
			   :dev_adr_geo,:dev_contact,:dev_con_identite,:dev_con_titre,:dev_con_nom,:dev_con_prenom,:dev_con_tel,:dev_con_mail,:dev_reg_type,:dev_reg_formule,
			   :dev_reg_nb_jour,:dev_reg_fdm,:dev_total_ht,:dev_total_ttc,:dev_validite,:dev_esperance,:dev_commande,:dev_commande_date,
			   :dev_utilisateur);";
			   $req_add=$ConnSoc->prepare($sql_add);
			   $req_add->bindValue(":dev_agence",$com_agence);
			   $req_add->bindValue(":dev_commercial",$commercial);
			   $req_add->bindValue(":dev_com_identite",$dev_com_identite);
			   $req_add->bindValue(":dev_numero",$dev_numero);
			   $req_add->bindValue(":dev_chrono",$dev_chrono);
			   $req_add->bindValue(":dev_date",date("Y-m-d"));
			   $req_add->bindValue(":dev_prospect",1);
			   $req_add->bindValue(":dev_client",$client);
			   $req_add->bindValue(":dev_adresse",$d_client["adr_id"]);
			   $req_add->bindValue(":dev_adr_service",$d_client["adr_service"]);
			   $req_add->bindValue(":dev_adr_nom",$d_client["adr_nom"]);
			   $req_add->bindValue(":dev_adr1",$d_client["adr_ad1"]);
			   $req_add->bindValue(":dev_adr2",$d_client["adr_ad2"]);
			   $req_add->bindValue(":dev_adr3",$d_client["adr_ad3"]);
			   $req_add->bindValue(":dev_adr_cp",$d_client["adr_cp"]);
			   $req_add->bindValue(":dev_adr_ville",$d_client["adr_ville"]);
			   $req_add->bindValue(":dev_adr_geo",$d_client["adr_geo"]);
			   $req_add->bindValue(":dev_contact",$d_client["cli_contact"]);
			   $req_add->bindValue(":dev_con_titre",$d_client["cli_con_titre"]);
			   $req_add->bindValue(":dev_con_nom",$d_client["cli_con_nom"]);
			   $req_add->bindValue(":dev_con_prenom",$d_client["cli_con_prenom"]);
			   $req_add->bindValue(":dev_con_identite",$dev_con_identite);
			   $req_add->bindValue(":dev_con_tel",$d_client["cli_con_tel"]);
			   $req_add->bindValue(":dev_con_mail",$d_client["cli_con_mail"]);
			   $req_add->bindValue(":dev_reg_type",$d_client["cli_reg_type"]);
			   $req_add->bindValue(":dev_reg_formule",$d_client["cli_reg_formule"]);
			   $req_add->bindValue(":dev_reg_nb_jour",$d_client["cli_reg_nb_jour"]);
			   $req_add->bindValue(":dev_reg_fdm",$d_client["cli_reg_fdm"]);
			   $req_add->bindValue(":dev_total_ht",$dli_montant_ht);
			   $req_add->bindValue(":dev_total_ttc",$dev_total_ttc);
			   $req_add->bindValue(":dev_validite",$dev_validite);
			   $req_add->bindValue(":dev_esperance",$dev_esperance);
			   $req_add->bindValue(":dev_commande",$dev_commande);
			   $req_add->bindValue(":dev_commande_date",$dev_commande_date);
			   $req_add->bindValue(":dev_utilisateur",$com_utilisateur);
			   try{
				   $req_add->execute();
				   $devis_id=$ConnSoc->lastInsertId();
			   }Catch(Exception $e){
				   $erreur_txt="ADD DEVIS" . $e->getMessage();
			   }

			   if(empty($erreur_txt)){

				   $sql_add="INSERT INTO Devis_Lignes (dli_devis,dli_produit,dli_categorie,dli_famille,dli_sous_famille,dli_sous_sous_famille,dli_type,dli_reference
				   ,dli_libelle,dli_texte,dli_dates,dli_tg,dli_ht,dli_ht_min,dli_remise,dli_taux_remise,dli_qte,dli_montant_ht,dli_tva_id,dli_tva_periode
				   ,dli_tva_taux,dli_action,dli_action_client,dli_action_cli_soc,dli_exo,dli_stagiaires_txt,dli_derogation,dli_rem_famille,dli_rem_ca
				   ) VALUES (
				   :dli_devis,
				   :dli_produit,
				   :dli_categorie,
				   :dli_famille,
				   :dli_sous_famille,
				   :dli_sous_sous_famille,
				   :dli_type,
				   :dli_reference,
				   :dli_libelle,
				   :dli_texte,
				   :dli_dates,
				   :dli_tg,
				   :dli_ht,
				   :dli_ht_min,
				   :dli_remise,
				   :dli_taux_remise,
				   :dli_qte,
				   :dli_montant_ht,
				   :dli_tva_id,
				   :dli_tva_periode,
				   :dli_tva_taux,
				   :dli_action,
				   :dli_action_client,
				   :dli_action_cli_soc,
				   :dli_exo,
				   :dli_stagiaires_txt,
				   :dli_derogation,
				   :dli_rem_famille,
				   :dli_rem_ca);";
				   $req_add=$ConnSoc->prepare($sql_add);
				   $req_add->bindParam(":dli_devis",$devis_id);
				   $req_add->bindParam(":dli_produit",$produit);
				   $req_add->bindParam(":dli_categorie",$d_produit["categorie"]);
				   $req_add->bindParam(":dli_famille",$d_produit["famille"]);
				   $req_add->bindParam(":dli_sous_famille",$d_produit["sous_famille"]);
				   $req_add->bindParam(":dli_sous_sous_famille",$d_produit["sous_sous_famille"]);
				   $req_add->bindParam(":dli_type",$dli_type);
				   $req_add->bindParam(":dli_reference",$d_produit["reference"]);
				   $req_add->bindParam(":dli_libelle",$d_produit["libelle"]);
				   $req_add->bindParam(":dli_texte",$d_produit["devis_txt"]);
				   $req_add->bindParam(":dli_action",$action_id);
				   $req_add->bindParam(":dli_action_client",$action_client_id);
				   $req_add->bindParam(":dli_action_cli_soc",$acc_societe);
				   $req_add->bindParam(":dli_dates",$dli_dates);
				   $req_add->bindValue(":dli_stagiaires_txt","");
				   $req_add->bindParam(":dli_tg",$dli_tg);
				   $req_add->bindParam(":dli_ht",$dli_ht);
				   $req_add->bindParam(":dli_ht_min",$dli_ht_min);
				   $req_add->bindParam(":dli_remise",$dli_remise);
				   $req_add->bindParam(":dli_taux_remise",$dli_taux_remise);
				   $req_add->bindParam(":dli_qte",$dli_qte);
				   $req_add->bindParam(":dli_montant_ht",$dli_montant_ht);
				   $req_add->bindParam(":dli_tva_id",$dli_tva_id);
				   $req_add->bindParam(":dli_tva_periode",$dli_tva_periode);
				   $req_add->bindParam(":dli_tva_taux",$dli_tva_taux);
				   $req_add->bindParam(":dli_exo",$dli_exo);
				   $req_add->bindParam(":dli_derogation",$derogation);
				   $req_add->bindParam(":dli_rem_famille",$rem_famille);
				   $req_add->bindParam(":dli_rem_ca",$rem_ca);
				   try{
					   $req_add->execute();
					   $devis_ligne_id=$ConnSoc->lastInsertId();
				   }Catch(Exception $e){
					   $erreur_txt="ADD DEVIS LIGNE" . $e->getMessage();
				   }
			   }

		   }
		   // FIN CREATION DEVIS

		   // MAJ DE L'ACTION ACLIENT AVEC LE DEVIS
		   if(empty($erreur_txt) AND !empty($devis_id)){

			   // on lie l'inscription au devis

			   $sql="UPDATE Actions_Clients SET acl_devis=:devis_id,acl_devis_ligne=:devis_ligne_id,acl_devis_numero=:dev_numero,acl_devis_soc=:acc_societe WHERE acl_id=:action_client_id;";
			   $req=$ConnSoc->prepare($sql);
			   $req->bindParam(":action_client_id",$action_client_id);
			   $req->bindParam(":devis_id",$devis_id);
			   $req->bindParam(":devis_ligne_id",$devis_ligne_id);
			   $req->bindParam(":dev_numero",$dev_numero);
			   $req->bindParam(":acc_societe",$acc_societe);
			   $req->execute();

			   if(!$add_devis){

				   // le devis existatit deja => on maj la ligne de devis

				   $sql="UPDATE Devis_Lignes SET dli_action_client=:action_client_id WHERE dli_id=:devis_ligne_id;";
				   $req=$ConnSoc->prepare($sql);
				   $req->bindParam(":action_client_id",$action_client_id);
				   $req->bindParam(":devis_ligne_id",$devis_ligne_id);
				   $req->execute();

			   }

		   }


		   // MAJ DES QUALIFICATIONS
		   if(!empty($d_produit["qualification"])){
			   $sql="UPDATE Actions_Stagiaires SET ast_qualification=:qualification WHERE ast_action=:action_id AND (ast_qualification=0 OR ISNULL(ast_qualification) );";
			   $req=$ConnSoc->prepare($sql);
			   $req->bindParam(":action_id",$action_id);
			   $req->bindParam(":qualification",$d_produit["qualification"]);
			   $req->execute();
		   }

		   // MAJ DES ATTESTATIONS
		   if(!empty($acl_attestation)){
			   $sql="UPDATE Actions_Stagiaires SET ast_attestation=:attestation,ast_attest_ok=1 WHERE ast_action=:action_id AND ast_action_client=:action_client_id AND ast_attestation=0;";
			   $req=$ConnSoc->prepare($sql);
			   $req->bindParam(":action_id",$action_id);
			   $req->bindParam(":action_client_id",$action_client_id);
			   $req->bindParam(":attestation",$acl_attestation);
			   $req->execute();

           }
           
           // MARGE ET BLOCAGE ADMINISTRATIF
            if(!empty($d_action["act_marge_ca"])){

                $act_verrou_marge=1;
                $act_verrou_admin=1;

                $sql="SELECT SUM(acl_ca) FROM Actions_Clients WHERE acl_confirme AND NOT acl_archive AND acl_action=:action;";
                $req=$ConnSoc->prepare($sql);
                $req->bindParam(":action",$action_id);
                $req->execute();
                $d_action_ca=$req->fetch();
                if(!empty($d_action_ca)){
                    if($d_action_ca[0]>=$d_action["act_marge_ca"]){
                        // la marge est bonne (>= a ce qui a été validé dans les BC)
                        $act_verrou_marge=0;
                    }
                }
                if($act_verrou_marge==0 AND $d_action["act_verrou_bc"]==0){
                    $act_verrou_admin=0;
                }

                // MAJ DE L'ACTION 

                $sql="UPDATE Actions SET act_verrou_marge=:act_verrou_marge, act_verrou_admin=:act_verrou_admin WHERE act_id=:action;";
                $req=$ConnSoc->prepare($sql);
                $req->bindParam(":act_verrou_marge",$act_verrou_marge);
                $req->bindParam(":act_verrou_admin",$act_verrou_admin);
                $req->bindParam(":action",$action_id);
                $req->execute();

                // MAJ DES DATES

                if($d_action["act_verrou_admin"]!=$act_verrou_admin){

                    $sql="UPDATE Plannings_Dates SET pda_alert=:pda_alert WHERE pda_type=1 AND pda_ref_1=:action;";
                    $req=$ConnSoc->prepare($sql);
                    $req->bindParam(":action",$action_id);
                    $req->bindParam(":pda_alert",$act_verrou_admin);
                    $req->execute();

                }
           }


		}
       // FIN ENREGISTREMENT
   }

   if(!empty($erreur_txt)){

       $_SESSION['message'][] = array(
           "titre" => "Erreur",
           "type" => "danger",
           "message" => $erreur_txt
       );
       if(!empty($action_id)){
           header("location: action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id . "&societ=" . $conn_soc_id);
           die();
       }else{
           header("location: action_tri.php");
           die();
       }

   }else{

       if(!empty($warning_txt)){
           $_SESSION['message'][] = array(
               "titre" => "Attention",
               "type" => "warning",
               "message" => $warning_txt
           );
       }
       header("location: action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id . "&societ=" . $conn_soc_id);
       die();
   }
?>
