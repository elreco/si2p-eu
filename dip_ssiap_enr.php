<?php
 
 include "includes/controle_acces.inc.php";
 
 include_once("includes/connexion_soc.php");
 include_once("includes/connexion_fct.php");
 include_once("includes/connexion.php");
 include_once("modeles/mod_parametre.php");
 
 // EDITION D'UN DIPLOME SSIAP
 
 $erreur_txt="";
 
 $dss_diplome=0;
 
if(!empty($_POST)){

	$dss_action=0;
	if(!empty($_POST["dss_action"])){
		$dss_action=intval($_POST["dss_action"]);
	}
	$dss_stagiaire=0;
	if(!empty($_POST["dss_stagiaire"])){
		$dss_stagiaire=intval($_POST["dss_stagiaire"]);
	}
	$dss_qualification=0;
	if(!empty($_POST["dss_qualification"])){
		$dss_qualification=intval($_POST["dss_qualification"]);
	}
	
	$dss_type=0;
	if(!empty($_POST["dss_type"])){
		$dss_type=intval($_POST["dss_type"]);
	}
	if(empty($dss_action) OR empty($dss_stagiaire) OR empty($dss_qualification) OR empty($dss_type) ){
		$erreur_txt="A Formulaire incomplet!";
	}
	
}else{
	$erreur_txt="B Formulaire incomplet!";
}
if(empty($erreur_txt)){

	if(!empty($_POST["dss_sta_nom"])){
		$dss_sta_nom=$_POST["dss_sta_nom"];
	}else{
		$erreur_txt=" 1Formulaire incomplet!";
	}
	if(!empty($_POST["dss_sta_prenom"])){
		$dss_sta_prenom=$_POST["dss_sta_prenom"];
	}else{
		$erreur_txt="2Formulaire incomplet!";
	}
	$dss_sta_naissance=null;
	if(!empty($_POST["dss_sta_naissance"])){
		$dss_sta_naissance = convert_date_sql($_POST['dss_sta_naissance']);
	}
	if(empty($dss_sta_naissance)){
		$erreur_txt="Formulaire incomplet!";
	}
	
	$dss_form_date=null;
	if(!empty($_POST["dss_form_date"])){
		$dss_form_date = convert_date_sql($_POST['dss_form_date']);
	}
	if(empty($dss_form_date)){
		$erreur_txt="Formulaire incomplet!";
	}
}

if(empty($erreur_txt)){
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}
	if($acc_societe!=$conn_soc_id){
		$acc_societe=$conn_soc_id;  
	}
	
	// INFO SUR LA SOCIETE POUR LA GESTION DU CHRONO
	$sql="SELECT soc_agre_ssiap,soc_ville FROM societes WHERE soc_id=:soc_id;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":soc_id",$acc_societe);
	$req->execute();
	$societe=$req->fetch();
	if(!empty($societe)){
		if(empty($societe["soc_agre_ssiap"])){
			$erreur_txt="Les paramètres actuels ne permettent pas d'enregistrer les diplômes SSIAP.";
		}else{
			$dss_ville=$societe["soc_ville"];
		}
	}else{
		$erreur_txt="Les paramètres actuels ne permettent pas d'enregistrer les diplômes SSIAP.";
	}
}
// SUR L'ACTION POUR IDENTIFIER Si AGENCE
if(empty($erreur_txt)){
	$sql="SELECT act_agence FROM Actions WHERE act_id=:dss_action AND act_agence>0;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":dss_action",$dss_action);
	$req->execute();
	$d_action=$req->fetch();
	if(!empty($d_action)){
		
		$sql="SELECT age_ville FROM Agences WHERE age_id=:agence;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":agence",$d_action["act_agence"]);
		$req->execute();
		$d_agence=$req->fetch();
		if(!empty($d_agence)){
			$dss_ville=$d_agence["age_ville"];
		}else{
			$erreur_txt="Impossible d'identifier l'agence liée au diplôme.";
		}
	}
}

// ON S'ASSURE QU'IL N'Y A PAS DE DOUBLON DE STAGIAIRE 
if(empty($erreur_txt)){	
	$sql="SELECT sta_id FROM Stagiaires WHERE sta_nom=:dss_sta_nom AND sta_prenom=:dss_sta_prenom AND sta_naissance=:dss_sta_naissance AND NOT sta_id=:dss_stagiaire;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":dss_sta_nom",$dss_sta_nom);
	$req->bindParam(":dss_sta_prenom",$dss_sta_prenom);
	$req->bindParam(":dss_sta_naissance",$dss_sta_naissance);
	$req->bindParam(":dss_stagiaire",$dss_stagiaire);
	$req->execute();
	$d_sta_doublon=$req->fetch();
	if(!empty($d_sta_doublon)){
		$erreur_txt="Doublon stagiaire! Merci de contacter l'administateur système";
	}
}


// CONTROLE LIE AU DIPLOME
if(empty($erreur_txt)){	

	if(!empty($_POST['diplome'])){
		$dss_diplome=intval($_POST['diplome']);
	}
	
	if($dss_diplome>0){
		$sql="SELECT * FROM diplomes_ssiap WHERE dss_id=:dss_id;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":dss_id",$dss_diplome);
		$req->execute();
		$d_diplome=$req->fetch();
		if(!empty($d_diplome)){
			if($d_diplome["dss_qualification"]!=$dss_qualification){
				$erreur_txt="La qualification d'un diplôme ne peut pas être changée!";
			}
		}else{
			$erreur_txt="Impossible de mettre à jour le diplôme.";
		}
	}elseif($dss_type!=3){
		
		// CREATION D'UN DIPLOME INI OU MC -> calcul du chrono
		
		$dss_chrono=0;
		$dss_numero="";
		
		$annee=substr($dss_form_date,0,4);
		
		
		$sql="SELECT MAX(dss_chrono) AS max_chrono FROM diplomes_ssiap WHERE dss_numero LIKE '" . $societe["soc_agre_ssiap"] . "%' AND YEAR(dss_form_date)=" . $annee . ";";
		$req=$Conn->query($sql);
		var_dump($sql);
		$chrono=$req->fetch();
		if(!empty($chrono)){
			
			
			$dss_chrono=$chrono["max_chrono"]+1;
			$dss_numero = $societe['soc_agre_ssiap'] . "-" . $dss_qualification . "-" . $annee . "-" .  str_pad($dss_chrono,5,"0",STR_PAD_LEFT);
			
		}else{
			$erreur_txt="Merci de contacter votre administrateur système pour initialiser la chronologie des diplômes SSIAP";
		}
    }
}
	
//*********************************************
// 		CONTROLE OK -> ON ENREGISTRE
//*********************************************
if(empty($erreur_txt)){	

	// MAJ D'UN DIPLOME
	if($dss_diplome>0){

		$sql="UPDATE Diplomes_Ssiap SET 
		dss_sta_nom=:dss_sta_nom,dss_sta_prenom=:dss_sta_prenom,dss_sta_naissance=:dss_sta_naissance,dss_sta_ville=:dss_sta_ville,
		dss_sta_cp=:dss_sta_cp,dss_form_titre=:dss_form_titre,dss_form_libelle=:dss_form_libelle,
		dss_form_texte_1=:dss_form_texte_1,dss_ville=:dss_ville,dss_form_texte_2=:dss_form_texte_2,dss_form_texte_3=:dss_form_texte_3,dss_type=:dss_type  
		WHERE dss_id = :dss_id";
		$req=$Conn->prepare($sql);
		$req->bindParam(":dss_sta_nom",$dss_sta_nom);
		$req->bindParam(":dss_sta_prenom",$dss_sta_prenom);
		$req->bindParam(":dss_sta_naissance",$dss_sta_naissance);
		$req->bindParam(":dss_sta_ville",$_POST['dss_sta_ville']);
		$req->bindParam(":dss_sta_cp",$_POST['dss_sta_cp']);
		$req->bindParam(":dss_form_titre",$_POST['dss_form_titre']);
		$req->bindParam(":dss_form_libelle",$_POST['dss_form_libelle']);
		$req->bindParam(":dss_form_texte_1",$_POST['dss_form_texte_1']);
		$req->bindParam(":dss_ville",$dss_ville);
		$req->bindParam(":dss_form_texte_2",$_POST['dss_form_texte_2']);
		$req->bindParam(":dss_form_texte_3",$_POST['dss_form_texte_3']);
		$req->bindParam(":dss_type",$dss_type);
		$req->bindParam(":dss_id",$dss_diplome);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur_txt="UP : " . $e->getMessage();
		}
		
	}else{

		$sql="INSERT INTO Diplomes_Ssiap
		(dss_action,dss_societe,dss_qualification,dss_type,dss_stagiaire,dss_sta_nom,dss_sta_prenom,dss_sta_naissance,dss_sta_ville,dss_sta_cp,dss_form_titre,dss_form_libelle,dss_form_date,dss_form_texte_1,dss_numero,dss_chrono,dss_date,dss_ville,dss_form_texte_2,dss_form_texte_3)
		VALUES 
		(:dss_action,:dss_societe,:dss_qualification,:dss_type,:dss_stagiaire,:dss_sta_nom,:dss_sta_prenom,:dss_sta_naissance,:dss_sta_ville,:dss_sta_cp,:dss_form_titre,:dss_form_libelle,:dss_form_date,:dss_form_texte_1,:dss_numero,:dss_chrono,NOW(),:dss_ville,:dss_form_texte_2,:dss_form_texte_3);";
		$req=$Conn->prepare($sql);
		$req->bindParam(":dss_action",$dss_action);
		$req->bindParam(":dss_societe",$acc_societe);
		$req->bindParam(":dss_qualification",$dss_qualification);
		$req->bindParam(":dss_type",$dss_type);
		$req->bindParam(":dss_stagiaire",$dss_stagiaire);
		$req->bindParam(":dss_sta_nom",$dss_sta_nom);
		$req->bindParam(":dss_sta_prenom",$dss_sta_prenom);
		$req->bindParam(":dss_sta_naissance",$dss_sta_naissance);
		$req->bindParam(":dss_sta_ville",$_POST['dss_sta_ville']);
		$req->bindParam(":dss_sta_cp",$_POST['dss_sta_cp']);
		$req->bindParam(":dss_form_titre",$_POST['dss_form_titre']);
		$req->bindParam(":dss_form_libelle",$_POST['dss_form_libelle']);
		$req->bindParam(":dss_form_date",$dss_form_date);
		$req->bindParam(":dss_form_texte_1",$_POST['dss_form_texte_1']);
		$req->bindParam(":dss_numero",$dss_numero);
		$req->bindParam(":dss_chrono",$dss_chrono);
		$req->bindParam(":dss_ville",$dss_ville);
		$req->bindParam(":dss_form_texte_2",$_POST['dss_form_texte_2']);
		$req->bindParam(":dss_form_texte_3",$_POST['dss_form_texte_3']);
		try{
			$req->execute();
			$dss_diplome=$Conn->lastInsertId();
		}Catch(Exception $e){
			$erreur_txt="ADD : " . $e->getMessage();
		}
		
		
		if(empty($erreur_txt)){
			
			$sql="UPDATE Actions_Stagiaires SET ast_diplome=:diplome,ast_diplome_statut=:diplome_statut WHERE ast_stagiaire=:ast_stagiaire AND ast_action=:ast_action;";
			$req=$ConnSoc->prepare($sql);	
			
			$req->bindParam(":diplome",$dss_diplome);
			$req->bindValue(":diplome_statut",1);
			$req->bindParam(":ast_stagiaire",$dss_stagiaire);
			$req->bindParam(":ast_action",$dss_action);
			$req->execute();
		}
		
		
	}
	
	if(empty($erreur_txt)){
		
		// MAJ DE LA FICHE STAGIAIRE AU CAS OU CHANGEMENT DEPUIS LE DIPLOMES
		
		$sql="UPDATE Stagiaires SET sta_nom=:dss_sta_nom,sta_prenom=:dss_sta_prenom,sta_naissance=:dss_sta_naissance,sta_ville_naiss=:dss_sta_ville,sta_cp_naiss=:dss_sta_cp
		WHERE sta_id=:dss_stagiaire;";
		$req=$Conn->prepare($sql);	
		$req->bindParam(":dss_sta_nom",$dss_sta_nom);
		$req->bindValue(":dss_sta_prenom",$dss_sta_prenom);
		$req->bindParam(":dss_sta_naissance",$dss_sta_naissance);
		$req->bindParam(":dss_sta_ville",$_POST['dss_sta_ville']);
		$req->bindParam(":dss_sta_cp",$_POST['dss_sta_cp']);
		$req->bindParam(":dss_stagiaire",$dss_stagiaire);
		$req->execute();
		
	}
	
	// TRAITEMENT ANNEXE
	
}

if(!empty($erreur_txt)){
    $_SESSION['message'][] = array(
        "titre" => "Echec de l'enregistrement",
        "type" => "danger",
        "message" => $erreur_txt
    );
}else{
    $_SESSION['message'][] = array(
        "titre" => "Enregistrement terminé",
        "type" => "success",
        "message" => "Diplôme enregistré avec succes!" 
    );
}
if(empty($erreur_txt)){
    header("location : dip_ssiap_voir.php?diplome=" . $dss_diplome . "&societ=" . $conn_soc_id);
}else{
    header("location : dip_ssiap.php?action=" . $dss_action . "&stagiaire=" . $dss_stagiaire . "&qualification=" . $dss_qualification . "&diplome=" . $dss_diplome . "&societ=" . $conn_soc_id);
}
die();