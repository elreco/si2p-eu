 <?php
 
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");
	include_once("includes/connexion_soc.php");
	
	include "modeles/mod_get_action_clients.php";
	include 'modeles/mod_get_action_sessions.php';
	include 'modeles/mod_parametre.php';
	include 'modeles/mod_erreur.php';

	// ECRAN D'IMPORTATION DES STAGIAIRES
	
	$erreur=0;
	
	$action_id=0;
	if(isset($_GET["action"])){
		if(!empty($_GET["action"])){
			$action_id=$_GET["action"];
		}	
	}
	
	$session_id=0;
	if(isset($_GET["session"])){
		if(!empty($_GET["session"])){
			$session_id=$_GET["session"];
		}	
	}
		
	if($action_id>0){
		
		$sql="SELECT act_gest_sta FROM Actions WHERE act_id=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$action_id);
		$req->execute();
		$action=$req->fetch();
		//echo("act_gest_sta : ". $action["act_gest_sta"]. "<br/>");
		//die();
 		if($action["act_gest_sta"]==2){
			// IMPORT A LA SESSION
			$sessions=get_action_sessions($action_id,0);
		}
		
		// LES CLIENTS QUI PARTICIPENT
		$clients=get_action_clients($action_id,0,$session_id);
		
	}else{
		$erreur=1;
	}

	
	if($erreur==0){
?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<title>SI2P - Orion</title>
				<meta name="keywords" content=""/>
				<meta name="description" content="">
				<meta name="author" content="">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<!-- CSS THEME -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
				<link href="assets/admin-tools/admin-forms/css/admin-forms.css" rel="stylesheet" type="text/css">

				<!-- CSS PLUGINS -->
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
				<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />
				
				<!-- CSS Si2P -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
				
				<!-- Favicon -->
				<link rel="shortcut icon" href="assets/img/favicon.png">
			   
				<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
				<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
			</head>
			<body class="sb-top sb-top-sm ">
				<form method="post" action="action_sta_import_up.php" enctype="multipart/form-data" id="form_modal_client" >
					<div>
						<input type="hidden" name="action" value="<?=$action_id?>" />
					</div>
					<div id="main">
			<?php		include "includes/header_def.inc.php"; ?>
						<section id="content_wrapper">
							
							<section id="content" class="animated">	
								<div class="row" >
							
									<div class="col-md-12">
										
										<div class="admin-form theme-primary ">												
											
											<div class="panel heading-border panel-primary">
											
												<div class="panel-body bg-light">

													<div class="content-header mtn mbn">
												<?php	echo("<h2>Importation des stagiaires <b class='text-primary' >Action N°". $action_id . "</b></h2>"); ?>
													</div>
													
									<?php			if($action["act_gest_sta"]==2){ ?>
														<div class="row" >
															<div class="col-md-12" >
																<select name="session" class="select2" >
														<?php		foreach($sessions as $s){
																		if(!empty($s["ase_h_deb"])){
																			$horraire=time_txt($s["ase_h_deb"]) . " / " . time_txt($s["ase_h_fin"]);
																		}elseif($s["pda_demi"]==1){
																			$horraire="Matin";
																		}else{
																			$horraire="Après-Midi";
																		}
																		
																		if($s["ase_id"]==$session_id){
																			echo("<option value='" . $s["ase_id"] . "' selected >" . $s["int_label_1"] . " " . $s["pda_date"] . " " . $horraire . "</option>");
																		}else{
																			echo("<option value='" . $s["ase_id"] . "' >" . $s["int_label_1"] . " " . $s["pda_date"] . " " . $horraire . "</option>");	
																		}
														
																	} ?>
																</select>
															</div>
														</div>
									<?php			} ?>
													<div class="row mt10" >
														<div class="col-md-12" >
															<select name="action_client" class="select2" >
													<?php		foreach($clients as $c){
																	echo("<option value='" . $c["acl_id"] . "' >" . $c["cli_code"] . "-" . $c["cli_nom"] . " (" . $c["acl_id"] . ")</option>");													
																} ?>
															</select>
														</div>
													</div>

													<div class="row mt10" >
														<div class="col-md-12" >
															<label class="field prepend-icon file">
																<span class="button btn-primary">Sélectionner un fichier</span>
																<input type="file" class="gui-file" name="fichier" onChange="document.getElementById('up_fichier').value = this.value;">
																<input type="text" class="gui-input" id="up_fichier" placeholder="Merci de selectionner un fichier">
																<label class="field-icon">
																	<i class="fa fa-download"></i>
																</label>
															</label>
														</div>
													</div>														

													
												</div>
											</div>
										</div>
									</div>
								<div>
							</section>
						</section>
					</div>
					<footer id="content-footer" class="affix">
						<div class="row">
							<div class="col-xs-3 footer-left">
								<a href="action_voir.php?action=<?=$action_id?>" class="btn btn-default" >
									Retour
								</a>
							</div>
							<div class="col-xs-6 footer-middle"></div>
							<div class="col-xs-3 footer-right">
								<button type="submit" class="btn btn-sm btn-success" >
									<i class="fa fa-save" ></i> Importer
								</button>
							</div>
						</div>
					</footer>
				</form>

		<?php	
				include "includes/footer_script.inc.php"; ?>
				<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
				<script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>
				<script type="text/javascript">				
					jQuery(document).ready(function (){
						$(".select2").select2();
						
						// ----- FIN DOC READY -----
					});				
				</script>
			</body>
		</html>
<?php
	} ?>