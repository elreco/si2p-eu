<?php  
//////////////// INCLUDES /////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php'); 
require "modeles/mod_droit.php";
require "modeles/mod_contact.php";
require "modeles/mod_get_commerciaux.php";
require "modeles/mod_erreur.php";

//////////////// REQUETES VERS LE SERVEUR /// ///////////////

$_SESSION['retour'] = "commande_liste.php";
if(isset($_SESSION["retourCommande"])){
	unset($_SESSION["retourCommande"]);
}

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
  $acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
$acc_profil=0;
if(isset($_SESSION['acces']["acc_profil"])){
    $acc_profil=$_SESSION['acces']["acc_profil"];  
}




	// initialisation des variables pour la requete recherche

	$com_annule = 0; 
	// si la recherche est activée

	if(isset($_POST['search'])){
		
		$com_agence=$acc_agence;
		if(isset($_POST['com_agence'])){
			$com_agence=intval($_POST['com_agence']);
		}
		
		$com_fournisseur=0;
		if(!empty($_POST['com_fournisseur'])){
			$com_fournisseur=intval($_POST['com_fournisseur']);
		}
		
		$com_id=0;
		if(!empty($_POST['com_numero'])){
			$com_id=intval($_POST['com_numero']);
		}
		
		$com_donneur_ordre=0;
		if(!empty($_POST['com_donneur_ordre'])){
			$com_donneur_ordre=intval($_POST['com_donneur_ordre']);
		}
		
		$cli_famille=0;
		if(!empty($_POST['com_famille'])){
			$cli_famille=intval($_POST['com_famille']);
		}
		
		$com_date_deb="";
		if(!empty($_POST['com_date_deb'])){		
			$com_date_deb=convert_date_sql($_POST['com_date_deb']);
		}
		$com_date_fin="";
		if(!empty($_POST['com_date_fin'])){		
			$com_date_fin=convert_date_sql($_POST['com_date_fin']);
		}
		
		// statut
		$com_etat=null;
		for($i=0;$i<=4;$i++){
			if(!empty($_POST['com_etat_' . $i])){
				$com_etat=$i;
				break;
			}
		}
		// ope
		
		$com_ope=null;
		for($i=1;$i<=2;$i++){
			if(!empty($_POST['com_ope_' . $i])){
				$com_ope=$i;
				break;
			}
		}
		
		$_SESSION['com_tri'] = array(
			"com_agence" => $com_agence,
			"com_fournisseur" => $com_fournisseur,
			"com_id" => $com_id,
			"com_date_deb"   => $com_date_deb,
			"com_date_fin"   => $com_date_fin,
			"com_donneur_ordre" => $com_donneur_ordre,
			"cli_famille" => $cli_famille,
			"com_etat" => $com_etat,
			"com_ope" => $com_ope
		);
	}

	// ON CONSTRUIT LA REQUETE

	if(isset($_SESSION['com_tri'])){
		
        $rechercheParLigne=false;
        $rechercheParValide=false;
		
		$critere=array();
		$mil="";
		
		 if(!empty($_SESSION['com_tri']['com_agence'])){
			$mil.=" AND com_agence = :com_agence"; 
			$critere["com_agence"]= $_SESSION['com_tri']['com_agence']; 
		};
		if(!empty($_SESSION['com_tri']['com_fournisseur'])){
			$mil.=" AND com_fournisseur = :com_fournisseur";
			$critere["com_fournisseur"]=$_SESSION['com_tri']['com_fournisseur'];
		};
		if(!empty($_SESSION['com_tri']['com_id'])){
            
			$mil.=" AND com_id = :com_id";
            $critere["com_id"]=$_SESSION['com_tri']['com_id'];
            
        };
		if(!empty($_SESSION['com_tri']['com_donneur_ordre'])){
			$mil.=" AND com_donneur_ordre = :com_donneur_ordre"; 
			$critere["com_donneur_ordre"]= $_SESSION['com_tri']['com_donneur_ordre'];
		};
		
		if(!empty($_SESSION['com_tri']['cli_famille'])){
			$mil.=" AND cli_famille = :cli_famille"; 
			$critere["cli_famille"]= $_SESSION['com_tri']['cli_famille'];
			$rechercheParLigne=true;    
        };
		if(!empty($_SESSION['com_tri']['com_date_deb'])){
			$mil.=" AND com_date >= :com_date_deb";
			$critere["com_date_deb"]=$_SESSION['com_tri']['com_date_deb'];
		};
		if(!empty($_SESSION['com_tri']['com_date_fin'])){
			$mil.=" AND com_date <= :com_date_fin";
			$critere["com_date_fin"]=$_SESSION['com_tri']['com_date_fin'];
        };
        
		if($_SESSION['com_tri']['com_etat']==4){
			$mil.=" AND com_annule = 1";
		}else{
			$mil.=" AND (com_annule = 0 OR com_annule IS NULL)";
			if(!is_null($_SESSION['com_tri']['com_etat'])){
				$mil.=" AND com_etat=:com_etat";
				$critere["com_etat"]=$_SESSION['com_tri']['com_etat'];
			}
        }


        if(!empty($_SESSION['com_tri']['com_ope'])){

            if($_SESSION['com_tri']['com_ope']==1){
                // Commande à valider
                $rechercheParValide=true;
                $critere["com_uti_valide"]=$acc_utilisateur;
                $critere["com_uti_profil"]=$acc_profil;
                $critere["com_etat"]=1;
                $critere["udr_utilisateur"]=$acc_utilisateur;

               
                $mil.=" AND com_etat=:com_etat AND (cva_utilisateur=:com_uti_valide OR cva_profil=:com_uti_profil OR NOT isnull(udr_droit) ) AND ISNULL(cva_date)";

            }   
        }
        
		
		// critere auto
		$mil.=" AND com_societe = :com_societe"; 
		$critere["com_societe"]= $acc_societe; 

		$sql="SELECT DISTINCT com_id,com_date,com_annule, com_donneur_ordre, uti_nom, uti_prenom, com_numero,com_fou_nom, com_fou_code, com_ht, com_ttc, com_etat";
		$sql.=" FROM Commandes LEFT JOIN utilisateurs ON utilisateurs.uti_id = commandes.com_donneur_ordre";
		if(!empty($rechercheParLigne)){
			$sql=$sql . " LEFT JOIN commandes_lignes ON Commandes.com_id = commandes_lignes.cli_commande";
        }
        if(!empty($rechercheParValide)){
            $sql=$sql . " LEFT JOIN Commandes_Validations ON (Commandes.com_id = Commandes_Validations.cva_commande)";
            $sql=$sql . " LEFT JOIN Utilisateurs_Droits ON (Commandes_Validations.cva_droit=Utilisateurs_Droits.udr_droit AND udr_utilisateur=:udr_utilisateur)";
            
        }

		if($mil!=""){
			$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		}
        $sql.=" ORDER BY com_numero";
      
		$req = $Conn->prepare($sql); 
		if(!empty($critere)){
			foreach($critere as $c => $v){
			  $req->bindValue($c,$v);
			}
        };
        $req->execute();
		$commandes = $req->fetchAll();
    }
    /*echo($sql);
    var_dump($commandes);
    die();*/
?>

<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>Si2P - ORION</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="Si2P">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->

<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

<link rel="shortcut icon" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="sb-top sb-top-sm">
    <div id="main">
<?php 	include "includes/header_def.inc.php";  ?>
        <section id="content_wrapper">
            <section id="content" class="animated fadeIn">
				<h1>Liste de bons de commande</h1>
	<?php 		if(!empty($commandes)){ ?>
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="table_id_2">
							<thead>
								<tr class="dark">
									<th>Numéro</th>
									<th>Date</th> 
									<th>Fournisseur</th>
									<th>H.T. (€)</th>
									<th>T.T.C. (€)</th>
									<th>Ordre</th>
									<th>Statut</th>
									<?php if(!empty($_SESSION['com_tri_2'])){ ?>
										<th>Détails/Validation</th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
                   <?php 	foreach($commandes as $c){ ?>
                       
                            <tr data-id="<?= $c['com_id'] ?>">
                                <td>
                                     <a href="commande_voir.php?commande=<?= $c['com_id'] ?>"><?= $c['com_numero'] ?></a>
                                </td>
                                <td>
                                    <?= convert_date_txt($c['com_date']) ?>
                                </td>
                                <td>
                                    <?= $c['com_fou_nom'] ?> (<?= $c['com_fou_code'] ?>)
                                </td>
                                <td>

                                    <?= number_format($c['com_ht'], 2, ',', ' ') ?>
                                </td> 
                                <td>
                                    
                                    <?php 
                                        if(!empty($c['com_annule'])){
                                            echo "-";
                                        }else{
                                            switch ($c['com_etat']) {
                                                case 0:
                                                    echo "-";
                                                    break;
                                                case 1:
                                                    echo number_format($c['com_ttc'], 2, ',', ' ');
                                                    break;
                                                case 2:
                                                    echo number_format($c['com_ttc'], 2, ',', ' ');
                                                    break;
                                                case 3:
                                                    echo number_format($c['com_ttc'], 2, ',', ' ');
                                                    break;
                                            }
                                        }
                                        
                                    ?>
                                    
                                </td>
                                <td>
                                    <?= $c['uti_prenom'] ?> <?= $c['uti_nom'] ?>
                                </td>  
                                
                                <td>
                                    <?php 
                                        if(!empty($c['com_annule'])){
                                            echo "Annulé";
                                        }else{
                                            switch ($c['com_etat']) {
                                                case 0:
                                                    echo "En cours de saisie";
                                                    break;
                                                case 1:
                                                    echo "En cours de validation";
                                                    break;
                                                case 2:
                                                    echo "Validé";
                                                    break;
                                                case 3:
                                                    echo "Bon à payer";
                                                    break;
                                            }
                                        }
                                        
                                    ?>
                                </td> 
                                <?php if(!empty($_SESSION['com_tri_2'])){ ?>
                                    <td class="text-center">
                                        <button type="button" id="btn-voir" class="btn btn-primary btn-sm" data-id="<?= $c['com_id'] ?>" 
                                        <?php if(!empty($c['cva_profil'])){ ?> 
                                            data-profil ="<?= $c['cva_profil'] ?>"
                                        <?php } ?> data-toggle="modal" data-target="#modalvoir"><i class="fa fa-eye"></i></button>
                                    </td>
                                <?php } ?>
                            </tr>

                        <?php } ?>

                        </tbody>
                    </table>

                </div>
            <?php }else{ ?>
                <div class="col-md-12 text-center" style="padding:0;" >
                    <div class="alert alert-warning" style="border-radius:0px;">
                      Aucune commande correspondant à votre recherche.
                    </div>
                </div>
            <?php } ?>
            </section>
        <!-- End: Content -->
        </section>
    </div>
    <!-- Modal pour l'ajout/edition d'une ligne -->
<div id="modalvoir" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 90%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title_ligne">Détails du bon de commande</h4>
            </div>
            <div class="modal-body pn">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-hover" id="table_id">
                            <thead>
                                <tr class="dark">
                                    <th>Famille</th>
                                    <th>Produit</th> 
                                    <th>Référence</th>
                                    <th>Descriptif</th>
                                    <th>Prix unitaire (€)</th>
                                    <th>Quantité</th>
                                    <th>Prix HT</th>
                                    <th>TVA</th>
                                    <th>Date de livraison</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="table_bc_voir">
                                
                            </tbody>
                        </table>
                       
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3 col-md-offset-9">
                            <table class="table mbn tc-icon-2 tc-med-2 tc-bold-last">
                                <thead>
                                    <tr class="hidden">
                                        <th>Total HT</th>

                                        <th>TVA</th>
                                        <th>Total TTC</th>
                                    </tr>
                                </thead>
                                <tbody id="tva_table">

                                    <tr>
                                        <td>
                                            <strong>Total HT</strong>
                                        </td>
                                        <td class="total_ht_txt"></td>
                                        
                                    </tr>
                                    <tr class="tva_table">
                                        
                                    </tr>
                                    <tr class="tva_table">
                                        
                                    </tr>
                                       
                                     
                                    <tr>
                                        <td>
                                            <strong>Total TTC</strong>
                                        </td>
                                        <td class="table_ttc"></td>
                                        
                                           
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <?php if(isset($_SESSION['com_tri_2']['com_etat']) && $_SESSION['com_tri_2']['com_etat'] == 1){ ?>
                    <button type="button" class="btn btn-success btn-sm" id="btn-valide" data-etat="1"><i class="fa fa-check"></i> Valider le BC</button>
                <?php }elseif(isset($_SESSION['com_tri_2']['com_etat']) && $_SESSION['com_tri_2']['com_etat'] == 2){?>
                    <button type="button" class="btn btn-success btn-sm" id="btn-valide" data-etat="2"><i class="fa fa-check"></i> Bon à payer</button>
                <?php } ?>
                <a href="" class="btn btn-info btn-sm" id="btn-voirbc" target="_blank"><i class="fa fa-eye"></i> Voir le BC</a>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>
<!-- Fin modal ajout/suppression de ligne -->
    <!-- End: Main -->
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">
              
                <a href="commande_tri.php" class="btn btn-primary btn-sm" role="button">
                    <span class="fa fa-search"></span>
                    <span class="hidden-xs">Nouvelle recherche</span>
                </a>
              
            </div>
            <div class="col-xs-6 footer-middle">&nbsp;</div>
            <div class="col-xs-3 footer-right">
            
                <a href="commande.php" class="btn btn-success btn-sm" role="button">
                    <span class="fa fa-plus"></span>
                    <span class="hidden-xs">Nouvelle demande d'achat</span>
                </a>
            

            </div>
        </div>
    </footer>
<?php
include "includes/footer_script.inc.php"; ?>  
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="vendor/plugins/jquery.numberformat.js"></script>
<script type="text/javascript">
/////////////////////// INTERACTIONS UTILISATEUR /////////////////////////
jQuery(document).ready(function () {
    $(document).on("click", "#btn-voir", function () { // si on clique sur le bouton voir
        var com_id = $(this).data("id");
        $("#btn-voirbc").attr("href", "commande_voir.php?commande=" + com_id);
        $("#btn-valide").data("id", com_id);
        voir_bc(com_id);
        calcul_tva(com_id);
        calcul_ttc(com_id);
        get_total(com_id);
    });

    $(document).on("click", "#btn-valide", function () { // si on clique sur le bouton valider ou bon à payer
        var com_id = $(this).data("id");
        
        actualiser_bc(com_id, $(this).data("etat"), $(this).data("profil"));
    });
});

////////////////////////////// FONCTIONS ////////////////////////////////
function voir_bc(com_id){
    $.ajax({
        type: 'POST',
        url: 'ajax/ajax_voir_bc.php',                   
        data : {
            "com_id":com_id
        },       
        dataType: 'json',              
        success: function(value)       
        {
            $('#table_bc_voir:last-child').html("");  
            $.each(value, function(key, data) {
                
                if(data['uti_libelle'] != " "){
                    $('#table_bc_voir:last-child').append(
                    "<tr><td>" + data['famille'] + "<br><strong>Utilisateur : </strong>" + data['uti_libelle'] + "</td><td>" + data['produit'] + "</td><td>" + data['cli_reference'] + "</td><td><strong>" + data['cli_libelle'] + "</strong><br>" + data['cli_descriptif'] + "</td><td class='td_prix_ht'>" + $.number( data['prix_ht'], 2, ',', ' ' ) + "</td><td class='td_prix_qte'>" + data['cli_qte']  + "</td><td>" + $.number( data['cli_qte']  * data['prix_ht'], 2, ',', ' ' ) + "</td><td>" + data['tva'] + "</td><td>" + data['date_livraison'] + "</td></tr>"
                    );
                }else{

                    $('#table_bc_voir:last-child').append(
                    "<tr><td>" + data['famille'] + "<br><strong>Véhicule : </strong>" + data['veh_libelle'] + "</td><td>" + data['produit'] + "</td><td>" + data['cli_reference'] + "</td><td><strong>" + data['cli_libelle'] + "</strong><br>" + data['cli_descriptif'] + "</td><td class='td_prix_ht'>" + $.number( data['prix_ht'], 2, ',', ' ' ) + "</td><td class='td_prix_qte'>" + data['cli_qte'] + "</td><td>" + $.number( data['cli_qte']  * data['prix_ht'], 2, ',', ' ' ) + "</td><td>" + data['tva'] + "</td><td>" + data['date_livraison'] + "</td></tr>"
                    );
                }

            });
        }

    });
}

// calcul tva

function calcul_tva(com_id){

    $.ajax({
        type: 'POST',
        url: 'ajax/ajax_commande_tva.php',                  
        data : {
            "commande":com_id,
        },       
        dataType: 'json',              
        success: function(data)     
          
        {

            $('.tva_table').remove();
            $.each(data, function(key, value) {
                $('#tva_table tr:first-child').after("<tr class='tva_table'><td><strong>TVA " + key + "</strong></td><td>" + value + " %</td></tr>");
            });
        }

    });
}
// fin calcul tva

function calcul_ttc(com_id){

    $.ajax({
        type: 'POST',
        url: 'ajax/ajax_commande_ttc.php',                  
        data : {
            "commande":com_id,
        },       
        dataType: 'html',              
        success: function(data)       
        {
            
            $('.table_ttc').html("");
        
            $('.table_ttc').html(data + " €");
            
        }

    });
}

function actualiser_bc(com_id, com_etat, com_profil){
    $.ajax({
        type: 'POST',
        url: 'ajax/ajax_actualise_etat.php',                   
        data : {
            "com_id":com_id,
            "com_etat":com_etat,
            "com_profil": com_profil,
            "com_utilisateur":<?= $_SESSION['acces']['acc_ref_id'] ?>
        },       
        dataType: 'html',              
        success: function(data)       
        {
            $("#table_id_2 > tbody > tr").each(function() {
                if($(this).data('id') == com_id){
                    $(this).remove();
                }
            });
            $("#modalvoir").modal("hide");

        }

    });
}

function get_total(com_id){


    $.ajax({
        type:'POST',
        url: 'ajax/ajax_commande_get_total.php',
        data : {
            "com_id":com_id,
        },
        dataType: 'html',
        success: function(data)         
        {     
            $(".total_ht_txt").text($.number(data, 2, ",", " ") + " €");  
        }
    });
    
    
}
</script>
</body>
</html>
