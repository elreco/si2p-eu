<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_get_juridique.php');
include('modeles/mod_orion_pro_categories.php');
include('modeles/mod_orion_pro_familles.php');

include('modeles/mod_parametre.php');

include('modeles/mod_devis_data.php');

// VISUALISATION D'UN DEVIS

$erreur=0;

$devis_id=0;
if(!empty($_GET["devis"])){
	$devis_id=intval($_GET["devis"]);
}

if(empty($devis_id)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();

}else{

	// la personne logue
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}

	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	// ON RECUPERE LES DONNES DU DEVIS A AFFICHER

	$data=devis_data($devis_id);
	if(empty($data)){
		$erreur_txt="impossible d'afficher la page";
		echo($erreur_txt);
		die();
	}

	// DONNE COMPLEMENTAIRE A PLUS DE L'AFFICHAGE (pas utile dans la version PDF donc pas dans data).

	$devis_pdf=file_exists("documents/Societes/" . $acc_societe . "/Devis/" . $data["dev_numero"] . ".pdf");

	// CLASSIFICATION PRODUIT
	$categorie_produit=orion_produits_categories();
	$famille_produit=orion_produits_familles();

	$sql="SELECT act_id,dli_action_client,DATE_FORMAT(act_date_deb,'%d/%m/%Y') AS act_date_deb_fr,dli_id,dli_reference FROM Devis_Lignes INNER JOIN Actions ON (Devis_Lignes.dli_action=Actions.act_id)
	LEFT JOIN Actions_Clients ON (Actions_Clients.acl_id=Devis_Lignes.dli_action_client AND Actions_Clients.acl_action=Devis_Lignes.dli_action)
	WHERE dli_devis=" . $devis_id . ";";
	$req=$ConnSoc->query($sql);
	$d_devis_actions=$req->fetchAll();

} ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title><?=$data["dev_numero"]?></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#container_print{
				width:21cm;
			}
			.ligne-col-1{
				width:20%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-2{
				width:52%;
				padding:1px 5px;
				font-size:10px;
			}
			.ligne-col-3{
				width:11%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-4{
				width:6%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-5{
				width:11%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.cachet{
				padding:5px 5px 20px 5px;
				border:1px solid #333;
			}
			.cachet small{
				font-size:7pt;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
		</style>

		<!--
		<style type="text/css" >
			#zone_print{
				display:none;
				margin:0px!important;
			}


			#controleH{
				background-color:red;
				border:1px solid blue;
				page-break-after:always;
			}

			/* GENERAL */

			#container_print{
				width:21cm;
				margin:auto;
				padding:0.5cm; /* remplace marge d'impression */
				background-color:#fff;
			}

			#page_print{
				width:100%;
			}

			#page_print table{
				width:100%;
			}

			/* HEADER */

			#page_print header{

			}
			#page_print header .print-logo{
				width:4cm;
			}
			#page_print header img{
				max-width:4cm;
				max-height:2.5cm;
			}

			/* FOOTER */

			#page_print footer{
				font-size:7pt;
			}
			#page_print footer .print-logo{
				width:3cm;
			}
			#page_print footer img{
				max-width:3cm;
				max-height:2cm;
			}
			#page_print #foot{
				margin-bottom:30px;
			}
			#page_print .saut{
				page-break-after:always;
			}

			/* GRILLE PRINT */
			.print-col{
				display:inline-block;
				vertical-align:top;
			}
			.print-col-33{
				width:32%;
			}
			.print-col-50{
				width:49%;
			}
			.print-col-100{
				width:99%;
			}

			/* SPE EDITION DEVIS */

			.ligne{
				display:table;
				border-bottom:1px solid #333;
				border-left:1px solid #333;
				border-right:1px solid #333;
				background-color:#FFF!important;
				width:100%;
			}
			.ligne>div{
				display:table-cell;
				vertical-align:middle;
				border-left:1px solid #333;
				height:100%!important;
				font-size:9pt;
			}


			.ligne-col-1{
				width:20%;
				border-left:0px!important;
				padding-left:5px;
			}
			.ligne-col-2{
				width:50%;
				padding:5px;
			}
			.ligne-col-3{
				width:10%;
				padding-right:5px;
			}
			.ligne-col-4{
				width:5%;
				padding-right:5px;
			}
			.ligne-col-5{
				width:13%;
				padding-right:5px;
			}
			.ligne-head{
				color:#FFF;
				background-color:#333!important;
			}


		</style>-->
		<!--
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
			html{
				background-color:#fff!important;
				margin:0px!important;
				padding:0px!important;;
			}
			body{
				background-color:#fff!important;
				margin:0px!important;;
				padding:0px!important;;
			}

		</style>-->
	</head>
	<body class="sb-top sb-top-sm document-affiche">
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php 		if($erreur==0){ ?>

							<div class="row" >

								<!-- zone d'affichage du devis -->

								<div class="col-md-8"  >

									<div id="container_print" >

										<div id="page_print" >

											<!--<div id="controleH" ></div>-->

											<div class="page" id="header_0" data-page="0" >
												<header>
													<table>
														<tr>
															<td class="w-25" >
													<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
																	<img src="<?=$data["juridique"]["logo_1"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td class="w-50 text-center" ><h1>DEVIS <?=$data["dev_numero"]?></h1></td>
															<td class="w-25" >
													<?php		if(!empty($data["juridique"]["logo_2"])){ ?>
																	<img src="<?=$data["juridique"]["logo_2"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
														</tr>
													</table>
												</header>
											</div>

											<div id="head_0" class="head" >
												<section>
													<table>
														<tr>
															<td class="w-50 va-t" >
													<?php		echo($data["juridique"]["nom"]);
																if(!empty($data["juridique"]["agence"])){
																	echo("<br/>Agence " . $data["juridique"]["agence"]);
																}
																if(!empty($data["juridique"]["ad1"])){
																	echo("<br/>" . $data["juridique"]["ad1"]);
																}
																if(!empty($data["juridique"]["ad2"])){
																	echo("<br/>" . $data["juridique"]["ad2"]);
																}
																if(!empty($data["juridique"]["ad3"])){
																	echo("<br/>" . $data["juridique"]["ad3"]);
																}
																echo("<br/>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"]);
																echo("<br/>" . $data["juridique"]["mail"]);
																echo("<br/>" . $data["juridique"]["site"]);
																echo("<br/>Affaire suivie par : " . $data["dev_com_identite"]);
																if(!empty($data["dev_uti_mobile"])){
																	echo("<br/><i class='fa fa-mobile' ></i> " . $data["dev_uti_mobile"]);
																}
																if(!empty($data["dev_uti_mail"])){
																	echo("<br/><i class='fa fa-envelope-o' ></i> " . $data["dev_uti_mail"]);
																}
																if(!empty($data["dev_reference"])){
																	echo("<br/>Référence : " . $data["dev_reference"]);
																}
																if(!empty($data["dev_cli_code"])){  ?>
																	<div >Code : <strong><?=$data["dev_cli_code"]?></strong></div>
														<?php 	}
																if(!empty($data["dev_cli_reference"])){  ?>
																	<div>Référence interne : <?=$d_client["dev_cli_reference"]?></div>
														<?php 	} ?>
															</td>
															<td class="w-50" >
													<?php		echo("<b>" . $data["dev_adr_nom"] . "</b>");
																if(!empty($data["dev_con_identite"])){
																	echo("<br/>A l'attention de " . $data["dev_con_identite"]);
																}
																if(!empty($data["dev_adr_service"])){
																	echo("<br/>" . $data["dev_adr_service"]);
																}
																if(!empty($data["dev_adr1"])){
																	echo("<br/>" . $data["dev_adr1"]);
																}
																if(!empty($data["dev_adr2"])){
																	echo("<br/>" . $data["dev_adr2"]);
																}
																if(!empty($data["dev_adr3"])){
																	echo("<br/>" . $data["dev_adr3"]);
																}
																echo("<br/>" . $data["dev_adr_cp"] . " " . $data["dev_adr_ville"]); ?>
															</td>
														</tr>
													</table>

													<table>
														<tr>
															<td class="text-right" >
																Le <?=$data["dev_date_texte"]?>
															</td>
														</tr>
													</table>

												</section>
											</div>

											<div id="ligne_header_0" >
												<div class="table-div" >
													<div class="table-div-th ligne-col-1 text-center" >Référence</div>
													<div class="table-div-th ligne-col-2 text-center" >Désignation</div>
													<div class="table-div-th ligne-col-3 text-center" >Prix HT</div>
													<div class="table-div-th ligne-col-4 text-center" >Qté</div>
													<div class="table-div-th ligne-col-5 text-center" >Montant HT</div>
												</div>
											</div>

									<?php	if(!empty($data["lignes"])){
												foreach($data["lignes"] as $l){


													if($l["dli_revente"]==1){
														$revente=1;
													}

													$designation="";
													if(!empty($l["dli_libelle"])){
														$designation=$l["dli_libelle"];
													}
													if(!empty($l["dli_texte"])){
														if(!empty($designation)){
															$designation.="<br/>";
														}
														$designation.=$l["dli_texte"];
													}
													if(!empty($l["dli_dates"])){
														if(!empty($designation)){
															$designation.="<br/>";
														}
														$designation.=$l["dli_dates"];
													}

													$stagiaires_txt=$l["dli_stagiaires_txt"];


													if($l["dli_exo"]){
														if(!empty($designation)){
															$designation.="<br/>";
														}
														$designation.="<b class='text-danger' >Exonération de TVA article 261-4-4°a du CGI</b>";
													}

													$montant_tva=($l["dli_montant_ht"]*$l["dli_tva_taux"]/100);

													if(!isset($data["tva"][$l["dli_tva_taux"]])){
														$data["tva"][$l["dli_tva_taux"]]=0;
													}
													$data["tva"][$l["dli_tva_taux"]]+=$montant_tva;
													?>

													<div class="table-div ligne-body-0" id="ligne_<?=$l["dli_id"]?>" >
														<div class="ligne-col-1" >
															<?=$l["dli_reference"]?>
															<div>
																<button class="btn btn-defaut btn-sm btn-stagiaire" data-toggle="tooltip" title="Gestion des stagiaires" data-ligne="<?=$l["dli_id"]?>" >
																	<i class="fa fa-user" ></i>
																</button>
															<!--
																<button class="btn btn-warning btn-sm btn-ligne-set" data-toggle="tooltip" title="Modifier la ligne" data-ligne="<?=$l["dli_id"]?>" >
																	<i class="fa fa-pencil" ></i>
																</button>-->
																<button class="btn btn-danger btn-sm btn-ligne-del" data-toggle="tooltip" title="Supprimer la ligne" data-ligne="<?=$l["dli_id"]?>" >
																	<i class="fa fa-times" ></i>
																</button>
															</div>
														</div>
														<div class="ligne-col-2" >
															<div><?=$designation?></div>
															<div id="stagiaires_txt_<?=$l["dli_id"]?>" ><?=$stagiaires_txt?></div>
														</div>
														<div class="text-right ligne-col-3" ><?=$l["dli_ht"]?> €</div>
														<div class="text-right ligne-col-4 " ><?=$l["dli_qte"]?></div>
														<div class="text-right ligne-col-5" ><?=$l["dli_montant_ht"]?> €</div>
													</div>


										<?php	}
											} ?>

											<div id="foot_0" >
										<?php	if(!empty($data["dev_libelle"])){ ?>
													<p><?=$data["dev_libelle"]?></p>
										<?php	}?>
												<table>
													<tr>
														<td class="w-33" >
											<?php			if(!empty($data["dev_reg_formule"])){
																echo("Mode de paiement : " . $base_reglement[$data["dev_reg_type"]] . "<br/>");
															}
															echo("Délais de paiement :	" . $data["reg_delai"]); ?>
														</td>
														<td class="w-33" >

											<?php		if(empty($data["dev_bordereau"])){
																if($data["dev_adr_geo"]!=1){
																	echo("<b class='text-danger' >Exonération de TVA article 262 1° du CGI</b>");
																} ?>
																<div class="cachet" >
																	<b>Cachet, Nom et Signature du Client</b>
																	<small>
																		Signature du devis après acceptation des conditions générales et particulières CONSULTABLES SUR NOTRE SITE INTERNET
																		ET SUR NOTRE CATALOGUE, et ce compris les dispositions à la clause de réserve de propriété
																		et prise de connaissance des prestations annexées au présent contrat.
																	</small>
																	<p class="text-center" >
																		Bon pour accord
																	</p>
																</div>
											<?php			}else{
																echo("&nbsp;");
															} ?>
														</td>
														<td class="w-33 va-t" >
											<?php			if(empty($data["dev_bordereau"])){	?>
																<table class="tab-border" >
																	<tr>
																		<th class="text-right" >Total HT :</th>
																		<td class="text-right" id="devis_ht" ><?=number_format($data["dev_total_ht"],2,","," ")?> €</td>
																	</tr>
															<?php	if(!empty($data["tva"])){
																		foreach($data["tva"] as $tk => $tv){ ?>
																			<tr <?php if(empty($tv)) echo("style='display:none;'"); ?> id="devis_tva_<?=$tk?>" >
																				<th class="text-right" >Tva <?=$tk?>% :</th>
																				<td class="text-right" id="devis_tva_<?=$tk?>_val" ><?=number_format($tv,2,","," ")?> €</td>
																			</tr>
															<?php		}
																	} ?>
																	<tr>
																		<th class="text-right" >Total TTC :</th>
																		<td class="text-right" id="devis_ttc" ><?=number_format($data["dev_total_ttc"],2,","," ")?> €</td>
																	</tr>
																</table>
											<?php			}else{
																echo("&nbsp;");
															} ?>
														</td>
													</tr>
												</table>
											</div>
											<div id="footer_0" >
												<footer>
													<table>
														<tr>
															<td class="w-15" >
													<?php		if(!empty($data["juridique"]["qualite_1"])){ ?>
																	<img src="<?=$data["juridique"]["qualite_1"]?>" class="img-responsive" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td class="text-center w-70" >
																<?=$data["juridique"]["footer"]?>
																<br>
																<strong>MESURES COVID-19</strong><br>
																<strong>Les mesures sanitaires de toutes nos formations  ont été renforcées dans le respect des mesures barrières en vigueur. Nos formateurs habitués au secours à personne ont reçu une formation spécifique adaptée pour garantir la sécurité de tous nos stagiaires.</strong>

														</td>
															<td class="text-right w-15" >
													<?php		if(!empty($data["juridique"]["qualite_2"])){ ?>
																	<img src="<?=$data["juridique"]["qualite_2"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
														</tr>
													</table>
												</footer>
											</div>

										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->

								<div class="col-md-4" >
						<?php		if(!empty($data["dev_att_derogation"])){ ?>
										<p class="alert alert-danger" >
											<strong>Attention :</strong> Ce devis nécessite une dérogation de <b>niveau <?=$data["dev_att_derogation"]?></b> pour pouvoir être imprimé.
										</p>
						<?php		} ?>

									<!-- DEBUT ANNEXE -->
									<div class="panel" id="bloc_pdf" <?php if(!$devis_pdf) echo("style='display:none;'"); ?> >
										<div class="panel-body">
											<div class="row"  >
												<div class="col-md-6 pt5" >
													<a href="documents/Societes/<?=$acc_societe?>/Devis/<?=$data["dev_numero"]?>.pdf?<?=time()?>" target="_blank" >
														<i class="fa fa-file-pdf-o" ></i> <?=$data["dev_numero"]?>
													</a>
												</div>
												<div class="col-md-3" >
													<a href="mail_prep.php?devis=<?=$devis_id?>" class="btn btn-sm btn-info" data-toggle="tooltip" title="Envoyer par mail" >
														<i class="fa fa-envelope-o" ></i>
													</a>
												</div>
												<div class="col-md-3" >
													<a href="devis_pdf_supp.php?devis=<?=$devis_id?>" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Supprimer le fichier PDF" >
														<i class="fa fa-times" ></i>
													</a>
												</div>
											</div>
										</div>
									</div>

									<div class="panel">
										<div class="panel-heading">
											<span class="panel-title">Contact</span>
										</div>
										<div class="panel-body">
											<div class="row" >
												<div class="col-md-12" ><b><?=$data["dev_con_identite"]?></b></div>
											</div>
											<div class="row" >
												<div class="col-md-6" >
							<?php					if(!empty($data["dev_con_tel"])){
														echo("<i class='fa fa-phone' ></i> " . $data["dev_con_tel"]);
													} ?>
												</div>
												<div class="col-md-6" >
							<?php					if(!empty($data["dev_con_portable"])){
														echo("<i class='fa fa-mobile' ></i> " . $data["dev_con_portable"]);
													} ?>
												</div>
											</div>
							<?php			if(!empty($d_devis["dev_con_mail"])){ ?>
												<div class="row" >
													<div class="col-md-12" >
														<i class="fa fa-envelope-o"></i>
														<a href="mailto:<?=$d_devis["dev_con_mail"]?>" >
															<?=$d_devis["dev_con_mail"]?>
														</a>
													</div>
												</div>
							<?php			} ?>
										</div>
									</div>

									<div class="panel" <?php if(empty($data["dev_bc"])) echo("id='bloc_revente'");?> <?php if(empty($revente)) echo("style='display:none;'");?> >
										<div class="panel-heading">
											<span class="panel-title">Achat pour revente</span>
										</div>
										<div class="panel-body">

										</div>
									</div>

							<?php	if(!empty($d_devis_actions)){ ?>
										<div class="panel" >
											<div class="panel-heading">
												<span class="panel-title">Formations liéés au devis</span>
											</div>
											<div class="panel-body">
												<table class="table" >
													<tr>
														<th>Action</th>
														<th class="text-center" >Date de début</th>
														<th class="text-center" >Produit</th>
														<th class="text-center" >Inscription</th>
													</tr>
											<?php	foreach($d_devis_actions as $action){ ?>
														<tr>
															<td>N° <?=$action["act_id"]?></td>
															<td class="text-center" ><?=$action["act_date_deb_fr"]?></td>
															<td class="text-center" ><?=$action["dli_reference"]?></td>
															<td class="text-center" >
														<?php	if(!empty($action["dli_action_client"])){ ?>
																	<a href="action_voir.php?action=<?=$action["act_id"]?>&action_client=<?=$action["dli_action_client"]?>" class="btn btn-info btn-sm" >
																		<i class="fa fa-eye" ></i>
																	</a>
														<?php	}else{ ?>
																	<a href="action_client_enr.php?devis=<?=$action["dli_id"]?>&action=<?=$action["act_id"]?>&auto=1" class="btn btn-success btn-sm" >
																		<i class="fa fa-plus" ></i>
																	</a>
														<?php	} ?>
															</td>
														</tr>
											<?php	} ?>
												</table>
											</div>
										</div>
							<?php	} ?>

									<!-- FIN ANNEXE -->
								</div>

							</div>


		<?php		}else{ ?>

						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>

		<?php		} ?>

				</section>

				<!-- End: Content -->
			</section>


		</div>
		<!-- End: Main -->

		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
		<?php		if(isset($_SESSION["retourDevis"])){
						if(!empty($_SESSION["retourDevis"])){ ?>
							<a href="<?=$_SESSION["retourDevis"]?>" class="btn btn-sm btn-default" >
								<i class="fa fa-left-arrow"></i> Retour
							</a>
		<?php			}
					} ?>

				</div>
				<div class="col-xs-6 footer-middle text-center" >
		<?php		if(empty($data["dev_att_derogation"])){ ?>
						<button type="button" class="btn btn-sm btn-info ml15" id="print" >
							<i class="fa fa-print"></i> Imprimer
						</button>

						<a href="#" class="btn btn-success btn-sm" id="pdf_add" >
							<i class="fa fa-save" ></i> Sauvegarder en PDF
						</a>
		<?php		} ?>
				</div>
				<div class="col-xs-3 footer-right" >
				<!---	<button type="button" class="btn btn-sm btn-success" id="btn_ligne_add" data-toggle="tooltip" title="Ajouter une nouvelle ligne" >
						<i class="fa fa-plus"></i> Produit
					</button>-->
					<a href="devis.php?devis=<?=$devis_id?>" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Modifier le devis" >
						<i class="fa fa-pencil"></i> Modifier
					</a>
				</div>
			</div>
		</footer>


		<!-- modal stagiaire -->
		<div id="modal_stagiaire" class="modal fade" role="dialog" >
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Stagiaires</h4>
					</div>
					<div class="modal-body">

						<table class="table" id="table_stagiaire" >
							<thead class="bg-dark" >
								<tr>
									<th>&nbsp;</th>
									<th>Nom</th>
									<th>Prénom</th>
									<th>Date de Naissance</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>

						<div class="admin-form theme-primary ">
							<div class="row">
								<div class="col-md-12">
									<div class="section-divider mb40">
										<span>Inscrire un stagiaire</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-11">
									<select class="select2 select2-stagiaire" data-client="<?=$data["dev_client"]?>" data-client_actif="0" id="sta_select" >
										<option value="" >Stagiaire ...</option>
									</select>
								</div>
								<div class="col-md-1">
									<button class="btn btn-sm btn-info" data-toggle="tooltip" title="Ajouter sur le devis" id="btn_sta_inscrire" >
										<i class="fa fa-plus" ></i>
									</button>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="section-divider mb40">
										<span>Enregistrer un nouveau stagiaire</span>
									</div>
								</div>
							</div>
							<div class="row" >
								<div class="col-md-4" >
									<label for="" >Nom :</label>
									<input type="text" id="sta_nom" name="sta_nom nom" class="gui-input nom" id="" placeholder="Nom" />

								</div>
								<div class="col-md-4" >
									<label for="" >Prénom :</label>
									<input type="text" name="sta_prenom prenom" id="sta_prenom" class="gui-input prenom" id="" placeholder="Prénom" />
								</div>
								<div class="col-md-3" >
									<label for="" >Naissance :</label>
									<input type="text" name="sta_naissance" id="sta_naissance" class="gui-input datepicker" id="" placeholder="Date de naissance" />
								</div>
								<div class="col-md-1 pt15">
									<button class="btn btn-sm btn-success" data-toggle="tooltip" title="Enregistrer et inscrire" id="btn_sta_add" >
										<i class="fa fa-save" ></i>
									</button>
								</div>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-success btn-sm" id="btn_sta_valide" >
							Terminer
						</button>
					</div>
				</div>
			</div>
		</div>

		<!-- modal ligne de devis -->
		<div id="modal_ligne" class="modal fade" role="dialog" aria-hidden="true" >
			<div class="modal-dialog modal-lg" >
				<div class="modal-content" style="overflow-y:scroll;" >
					<div class="modal-header">
						<h4 class="modal-title">Ligne de devis</h4>
					</div>
					<div class="modal-body">
						<div class="admin-form theme-primary" >
							<form method="post" action="#" id="form_ligne" >
								<div>
									<input type="hidden" name="dli_devis" value="<?=$devis_id?>" />
									<input type="hidden" id="dli_id" name="dli_id" value="" />
								</div>
								<div class="row" >
									<div class="col-md-6 text-center" >
										<div class="radio-custom mb5">
											<input id="dli_intra" name="dli_type" type="radio" class="select2-produit-intra" value="1" >
											<label for="dli_intra">INTRA</label>
										</div>
									</div>
									<div class="col-md-6 text-center" >
										<div class="radio-custom mb5">
											<input id="dli_inter" name="dli_type" type="radio" class="select2-produit-inter" value="2" >
											<label for="dli_inter">INTER</label>
										</div>
									</div>
								</div>
								<div class="row mt10" >
									<div class="col-md-12" >
										<label for="dli_categorie" >Catégorie de produit</label>
										<select name="dli_categorie" id="dli_categorie" class="select2 select2-produit-cat" >
											<option value="" >Catégorie de produit ...</option>
								<?php		if(!empty($categorie_produit)){
												foreach($categorie_produit as $cle_cat => $cat_p){
													echo("<option value='" . $cle_cat . "' >" . $cat_p["pca_libelle"] . "</option>");
												}
											} ?>
										</select>
									</div>
								</div>
								<div class="row mt10" >
									<div class="col-md-4" >
										<label for="dli_pro_famille" >Famille de produit</label>
										<select name="dli_pro_famille" id="dli_pro_famille" class="select2 select2-famille select2-produit-fam" >
											<option value="" >Famille de produit ...</option>
								<?php		if(!empty($famille_produit)){
												foreach($famille_produit as $cle_fam => $fam_p){
													echo("<option value='" . $cle_fam . "' >" . $fam_p["pfa_libelle"] . "</option>");
												}
											} ?>
										</select>
									</div>
									<div class="col-md-4" >
										<label for="dli_pro_s_famille" >Sous-Famille de produit</label>
										<select name="dli_pro_s_famille" id="dli_pro_s_famille" class="select2 select2-famille-sous-famille select2-produit-sous-fam" >
											<option value="" >Sous-Famille de produit ...</option>
										</select>
									</div>
									<div class="col-md-4" >
										<label for="dli_pro_s_s_famille" >Sous Sous-Famille</label>
										<select name="dli_pro_s_s_famille" id="dli_pro_s_s_famille" class="select2 select2-famille-sous-sous-famille select2-produit-sous-sous-fam" >
											<option value="" >Sous sous-Famille de produit ...</option>
										</select>
									</div>
								</div>
								<div class="row mt10" >
									<div class="col-md-12" >
										<label for="dli_produit" >Produit</label>
										<select name="dli_produit" id="dli_produit" class="select2 select2-produit" data-client="<?=$data["dev_client"]?>" required >
											<option value="" >Produit ...</option>
										</select>
										<small class="text-danger texte_required" id="dli_produit_required">Merci de selectionner un produit</small>
									</div>
								</div>
								<div class="row mt10" >
									<div class="col-md-12" >
										<label for="dli_libelle" >Libellé :</label>
										<input type="text" name="dli_libelle" class="gui-input" id="dli_libelle" placeholder="Libellé" required />
										<small class="text-danger texte_required" id="dli_libelle_required"></small>
									</div>
								</div>
								<div class="row mt10" >
									<div class="col-md-4" >
										<label for="dli_ht" >Prix unitaire :</label>
										<div class="field append-icon">
											<input type="text" name="dli_ht" class="gui-input input-float" id="dli_ht" placeholder="Prime produit" />
											<span class="field-icon">
												<i class="fa fa-eur"></i>
											</span>
										</div>
										<div id="text_pu" ></div>
									</div>
									<div class="col-md-4" >
										<label for="dli_ht" >Quantité :</label>
										<input type="text" name="dli_qte" class="gui-input" id="dli_qte" placeholder="Quantité" />
									</div>
									<div class="col-md-4" >
										<label for="dli_montant_ht" >Total HT :</label>
										<div class="field append-icon">
											<input type="text" name="dli_montant_ht" class="gui-input input-float" id="dli_montant_ht" placeholder="Prime produit" />
											<span class="field-icon">
												<i class="fa fa-eur"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="row mt10" >
									<div class="col-md-12" >
										<div class="panel" >
											<div class="panel-heading panel-head-sm" >
												<span class="panel-title">Texte</span>
												<span class="pull-right" >
													<button type="button" class="btn btn-default btn-sm btn-aff-mas" data-cible="aff_texte" >
														<i class="fa fa-minus" ></i>
													</button>
												</span>
											</div>
											<div class="panel-body pn" id="aff_texte" >
												<textarea id="dli_texte" class="summernote-img"  rows="1" ></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="panel mt10" >
									<div class="panel-heading panel-head-sm" >
										<span class="panel-title">Dates</span>
										<span class="pull-right" >
											<button type="button" class="btn btn-default btn-sm btn-aff-mas" data-cible="aff_dates" >
												<i class="fa fa-plus" ></i>
											</button>
										</span>
									</div>
									<div class="panel-body pn" id="aff_dates" style="display:none;" >
										<textarea id="dli_dates" class="summernote-img" rows="1"  ></textarea>
									</div>
								</div>
								<div class="panel mt10" >
									<div class="panel-heading panel-head-sm" >
										<span class="panel-title">Stagiaires</span>
										<span class="pull-right" >
											<button type="button" class="btn btn-default btn-sm btn-aff-mas" data-cible="aff_stagiaire" >
												<i class="fa fa-plus" ></i>
											</button>
										</span>
									</div>
									<div class="panel-body pn" id="aff_stagiaire" style="display:none;" >
										<textarea id="dli_stagiaires" class="summernote-img" rows="1"  ></textarea>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-success btn-sm" id="btn_ligne_valide" >
							Terminer
						</button>
					</div>
				</div>
			</div>
		</div>

		<!-- modal confirmation -->
		<div id="modal_confirmation_user" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Confirmer ?</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							Annuler
						</button>
						<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
							Confirmer
						</button>
					</div>
				</div>
			</div>
		</div>


<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>

		<script type="text/javascript" src="assets/js/print.js"></script>

		<script type="text/javascript">

			var l_page=21;
			var h_page=28;

			var devis_ligne=0;
			var client=parseInt("<?=$data["dev_client"]?>");
			var devis=parseInt("<?=$devis_id?>");
			var stagiaires_txt="";
			var ht_min=0;

			jQuery(document).ready(function (){

				//********************
				//	MODAL STAGIAIRE
				//*********************

				// affichage du modal
				$(".btn-stagiaire").click(function(){
					devis_ligne=$(this).data("ligne");
					get_devis_stagiaires(devis_ligne,afficher_stagiaires,"","");
					$("#modal_stagiaire").modal("show");

				});

				// enregistrement d'un nouveau stagiaire
				$("#btn_sta_add").click(function(){
					var nom=$("#sta_nom").val();
					var prenom=$("#sta_prenom").val();
					var naissance=$("#sta_naissance").val();
					if(nom!=""&&prenom!=""){
						add_stagiaire(client,nom,prenom,naissance,1,inscription,"","");
					}else{
						afficher_message("Erreur","danger","Vous devez renseigner le nom et le prénom du stagiaire");
					}


				});

				// inscription d'un stagiaire déjà lié au client
				$("#btn_sta_inscrire").click(function(){
					if($("#sta_select").val()>0){
						inscription($("#sta_select").val());
						$("#sta_select").select("val","");
					}else{
						afficher_message("Erreur","danger","Vous devez selectionner un stagiaire");
					}

				});

				$("#btn_sta_valide").click(function(){
					donnee="&dli_stagiaires_txt=" + stagiaires_txt;
					set_devis_ligne(devis,devis_ligne,donnee,fermer_modal_stagiaire);
				});

				//********************
				//	GESTION DES LIGNES
				//*********************

				//supp d'une ligne existante
				$(".btn-ligne-del").click(function(){
					devis_ligne=$(this).data("ligne");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer cette ligne?",supprimer_ligne,devis_ligne,"","");

				});

				// ouvre le modal pour un ajout de ligne
				/*$("#btn_ligne_add").click(function(){
					console.log("#btn_ligne_add -> click");
					devis_ligne=0;
					H=$(window).height()-75;
					$("#modal_ligne .modal-content").css("max-height",H + "px");
					$("#form_ligne").prop("action","ajax/ajax_devis_ligne_add.php");
					$("#dli_id").val(0);
					$("#modal_ligne").modal("show");

				});*/

				//modification d'une ligne existante
				/*$(".btn-ligne-set").click(function(){

				});*/





				// validation du modal ligne
				/*$("#btn_ligne_valide").click(function(){
					console.log("#btn_ligne_valide -> click");
					if(valider_form("#form_ligne")){
						if($("#dli_id").val()==0){
							envoyer_form_ajax($("#form_ligne"),ajouter_ligne);
						}else{
							envoyer_form_ajax($("#form_ligne"),modifier_ligne);
						}
					}
				});*/

				// calcul
				/*$("#dli_ht").blur(function(){
					calculer_ht(1);
				});*/
				/*$("#dli_qte").blur(function(){
					calculer_ht(1);
				});*/
				/*$("#dli_montant_ht").blur(function(){
					calculer_ht(2);
				});*/

				//********************
				//	GESTION DU PDF
				//*********************
				$("#pdf_add").click(function(){
					$.ajax({
						type:'POST',
						url: 'ajax/ajax_devis_pdf.php',
						data : 'devis=' + devis,
						dataType: 'JSON',
						success: function(data){
							$("#bloc_pdf").show();

						},
						error: function(data){
							afficher_message("Erreur","danger",data.responseText);
						}
					});
				});


			});



			/****************************
				FONCTION
			****************************/

			//********************
			//	MODAL STAGIAIRE
			//*********************

			// affiche la liste des stagiaires recuperés en JSON
			function afficher_stagiaires(json){
				$("#table_stagiaire > tbody").empty();
				stagiaires_txt="";
				if(json){
					$.each(json, function(i, obj){

						td_stagiaire="<tr>";
							td_stagiaire=td_stagiaire+"<td id='td_sta_"+ obj.sta_id +"' >";
								td_stagiaire=td_stagiaire+"<button class='btn btn-sm btn-danger' id='btn_sta_supp_" + obj.sta_id  + "' >";
									td_stagiaire=td_stagiaire+"<i class='fa fa-times' ></i>";
								td_stagiaire=td_stagiaire+"</button>";
							td_stagiaire=td_stagiaire+"</td>";
							td_stagiaire=td_stagiaire+"<td>" + obj.sta_nom + "</td>";
							td_stagiaire=td_stagiaire+"<td>" + obj.sta_prenom + "</td>";
							td_stagiaire=td_stagiaire+"<td>";
								if(obj.sta_naissance!=null){
									td_stagiaire=td_stagiaire+obj.sta_naissance;
								}
							td_stagiaire=td_stagiaire+ "</td>";
						td_stagiaire=td_stagiaire+"</tr>";

						stagiaires_txt=stagiaires_txt + "<li>" + obj.sta_prenom + " " + obj.sta_nom + "</li>";

						$("#table_stagiaire > tbody:last").append(td_stagiaire);

						$("#btn_sta_supp_" + obj.sta_id).click(function(){
							del_devis_stagiaire(obj.sta_id,devis,devis_ligne,actualiser_stagiaires,"","");
						});
					});
					if(stagiaires_txt!=""){
						stagiaires_txt="Stagiaire(s) :<ul>" + stagiaires_txt + "</ul>";
					}
				}

			}

			function inscription(stagiaire){
				//retour add_stagiaire() pour déclencher inscription auto
				console.log("inscription()");
				if(stagiaire){
					$("#sta_nom").val("");
					$("#sta_prenom").val("");
					$("#sta_naissance").val("");
					add_devis_stagiaire(stagiaire,devis,devis_ligne,actualiser_stagiaires,"","");
				}
			}

			// declenche la mise à jour des stagiaires
			function actualiser_stagiaires(){
				console.log("actualiser_stagiaires()");
				get_devis_stagiaires(devis_ligne,afficher_stagiaires,"","");
			}

			// Maj du devis en fonction des modif
			function fermer_modal_stagiaire(json){
				$("#stagiaires_txt_" + devis_ligne).html(stagiaires_txt);

				mise_en_page();

				$("#modal_stagiaire").modal("hide");
			}

			// liste des stagiaires liés à une ligne de devis
			function get_devis_stagiaires(devis_ligne,callback,param1,param2){
				console.log("get_devis_stagiaires(" + devis_ligne + ",callback,param1,param2)");
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_devis_stagiaires_get.php',
					data : 'devis_ligne=' + devis_ligne,
					dataType: 'JSON',
					success: function(data){
						// tab JSON
						// tab JSON
						callback(data,param1,param2);
					},
					error: function(data) {
						modal_alerte_user(data.responseText);
					}
				});
			}

			// Enregistre un nouveau stagiaire
			function add_stagiaire(client,nom,prenom,naissance,doublon,callback,param1,param2){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_stagiaire_add.php',
					data : 'sta_client=' + client + '&sta_nom=' + nom + '&sta_prenom=' + prenom + '&sta_naissance=' + naissance + "&devis_ligne=" + devis_ligne + "&doublon=" + doublon,
					dataType: 'JSON',
					success: function(data){
						// data = id stagiaire
						callback(data,param1,param2);

					},
					error: function(data) {
						afficher_message("Erreur","danger",data.responseText);
					}
				});
			}

			// Inscription d'un stagiaire sur une ligne de devis
			function add_devis_stagiaire(stagiaire,devis,devis_ligne,callback,param1,param2){

				console.log("add_devis_stagiaire(" + stagiaire + "," + devis + "," + devis_ligne + "," + callback + "," + param1 + "," + param2+")");
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_devis_stagiaire_add.php',
					data : 'stagiaire=' + stagiaire + '&devis=' + devis + '&devis_ligne=' + devis_ligne,
					dataType: 'JSON',
					success: function(data){
						// data = id stagiaire
						callback(data,param1,param2);

					},
					error: function(data){
						afficher_message("Erreur","danger",data.responseText);
					}
				});
			}

			// Désinscription d'un stagiaire sur une ligne de devis
			function del_devis_stagiaire(stagiaire,devis,devis_ligne,callback,param1,param2){

				console.log("add_devis_stagiaire(" + stagiaire + "," + devis + "," + devis_ligne + "," + callback + "," + param1 + "," + param2+")");
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_devis_stagiaire_del.php',
					data : 'stagiaire=' + stagiaire + '&devis=' + devis + '&devis_ligne=' + devis_ligne,
					dataType: 'JSON',
					success: function(data){
						// data = id stagiaire
						callback(data,param1,param2);

					},
					error: function(data){
						afficher_message("Erreur","danger",data.responseText);
					}
				});
			}

			// Mise à jour de la ligne de devis en fonction de la siaisi U
			function set_devis_ligne(dli_devis,dli_id,donnee,callback,param1,param2){
				console.log("set_devis_ligne()");
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_devis_ligne_set.php',
					data : 'dli_devis=' + dli_devis + '&dli_id=' + dli_id + "&" + donnee,
					dataType: 'JSON',
					success: function(data){
						// data json tab[sta_txt]
						callback(data,param1,param2);

					},
					error: function(data){
						afficher_message("Erreur","danger",data.responseText);
					}
				});
			}

			//*******************************
			//	GESTION DES LIGNES
			//*******************************

			// FONCTION SPE DEVIS VOIR
			// ini la suppression d'une ligne
			function supprimer_ligne(ligne_supp){
				if(ligne_supp>0){
					del_devis_ligne(devis,ligne_supp,ligne_supprimee,"","");
				}
			}

			// modifie la page suite à la suppression
			function ligne_supprimee(json){
				if(json){

					devis_ht=new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR',useGrouping:true }).format(json.ht);
					devis_ttc=new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR',useGrouping:true }).format(json.ttc);

					$("#ligne_" + json.ligne).remove();

					$("#devis_ht").html(devis_ht);
					$("#devis_ttc").html(devis_ttc);
					$.each(json.tva, function(i, obj){
						if(obj==0){
							$("#devis_tva_" + i).hide();
						}else{
							tva_montant=new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR',useGrouping:true }).format(obj);
							$("#devis_tva_" + i + "_val").html(tva_montant);
							$("#devis_tva_" + i).show();
						}
					});
					if(json.warning!=""){
						afficher_message("Attention","warning","/" + json.warning + "/");
					}
					if(json.revente==0 && $("#bloc_revente").length>0){
						//afficher_message("Attention","warning",json.warning);
					}
					mise_en_page();
				}

			}

			/*function actualiser_produit(json){

				if(json){

					$("#dli_ht").val(json.ht_intra);
					if($("#dli_intra").is(":checked")){
						$("#dli_ht").val(json.ht_intra);
						ht_min=json.min_intra;
					}else{
						$("#dli_ht").val(json.ht_inter);
						ht_min=json.min_inter;
					}
					$("#dli_libelle").val(json.libelle);
					$("#dli_texte").code(json.devis_txt);
					calculer_ht(1);
				}
			}*/

			/*function calculer_ht(source){
				if($("#dli_qte").val()!=""){
					qte=parseFloat($("#dli_qte").val());
				}else{
					qte=1;
					$("#dli_qte").val(qte);
				}
				if(source==1){
					ht=parseFloat($("#dli_ht").val());
					if(ht<ht_min){
						ht=ht_min;
						$("#dli_ht").val(ht_min);
						afficher_txt_user($("#text_pu"),"Prix minimum à " + ht_min + " €","danger",4000);
					}
					montant_ht=ht*qte;
					$("#dli_montant_ht").val(montant_ht);
				}else{
					montant_ht=parseFloat($("#dli_montant_ht").val());
					ht=montant_ht/qte;
					if(ht<ht_min){
						ht=ht_min;
						$("#dli_ht").val(ht_min);
						afficher_txt_user($("#text_pu"),"Prix minimum à " + ht_min + " €","danger",4000);
						calculer_ht(1);
					}
				}
			}
			*/

			/*function ajouter_ligne(){
				console.log("ajouter_ligne");
			}*/

			/*function modifier_ligne(){
				console.log("modifier_ligne");
			}*/

			// FONCTION GENERALISABLE

			// supp d'une ligne de devis
			function del_devis_ligne(devis,ligne_supp,callback,param1,param2){

				console.log("del_devis_ligne()");
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_del_devis_ligne.php',
					data : 'devis=' + devis + '&devis_ligne=' + ligne_supp,
					dataType: 'JSON',
					success: function(data){
						// data = id stagiaire
						callback(data,param1,param2);

					},
					error: function(data){
						afficher_message("Erreur","danger",data.responseText);
					}
				});

			}



		</script>
	</body>
</html>
