<?php

	// STAT VENTILATION CA CLIENT PAR FAMILLE


	include "includes/controle_acces.inc.php";
	include "modeles/mod_parametre.php";
	include "includes/connexion.php";
	include "includes/connexion_soc.php";

    /* NOTE SCRIPT
    en consultation agence, on ne s'occupe que de l'agence.
    un client facturé N-1 sur IDF OUEST et facturé N sur IDF EST sera considéré comme perdu sur IDF OUEST et ancien sur EST
    => acté réunion du 06/01/2021 Maud et Laurent.

    statut prospect / client 
    suite à cette stat la regle prospect -> client a été modifié.
    La facturation d'une filiale pour la première fois n'affecte plus la maison mere ni les autres filiales.
    Ces dernières restent prospect
    => acté télpéhone Maud 07/01/2021 suite échange Eric
    */

	// DONNEE FORM
	//$_SESSION['retour'] = "stat_com_cli_pfam.php";
	//$_SESSION["retourFacture"]="stat_com_cli_pfam.php";
	//$_SESSION["retourClient"]="stat_com_cli_pfam.php";
    //$_SESSION["retour_action"]="stat_com_cli_pfam.php";

    $erreur_txt="";

    // CONTROLE D'ACCESS

    if ($_SESSION['acces']['acc_profil'] != 14 AND $_SESSION['acces']['acc_profil'] != 11) {
        // stat prospection -> DG
        $erreur_txt="Accè refusé. Vous n'êtes pas autorisé à accéder à cette page.";
    
    }else{
    
        if(!empty($_POST)){

            //print_r($_POST);

            $exercice=0;
            if(!empty($_POST["exercice"])){
                $exercice=intval($_POST["exercice"]);       
            }
            $_SESSION["crit_stat"]["exercice"]=$exercice;

            $cli_categorie=0;
            if(!empty($_POST["cli_categorie"])){
                $cli_categorie=intval($_POST["cli_categorie"]);
               
            }
            $_SESSION["crit_stat"]["cli_categorie"]=$cli_categorie;

            if(empty($exercice)){
                $erreur_txt="Formulaire incomplet!";
            }
        }elseif(isset($_SESSION["crit_stat"])){

            $exercice=$_SESSION["crit_stat"]["exercice"];
            $cli_categorie=$_SESSION["crit_stat"]["cli_categorie"];

        }else{
            $erreur_txt="Formulaire incomplet!";

        }

    }

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : stat_commercial.php");
		die();
	}


	// LE PERSONNE CONNECTE

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
    }

    // Info sur la societe
    $sql="SELECT soc_agence FROM Societes WHERE soc_id=" . $acc_societe . " AND soc_agence";
    $req=$Conn->query($sql);
    if(!empty($req->fetch())){
        $d_societe=$req->fetch();
    }

    // tableau pour affichage de la page 
    $data=Array();

    
    $data[0]=Array(
        "com_identite" => "Non affecté",
        "0" => array(   //exercice N
            "ca" => 0,
            "clients" => array()
        ),
        "1" => array(   //exercice N-1
            "ca" => 0,
            "clients" => array()
        ),
        "nb_renouv" => 0,
        "ca_renouv" => 0,
        "nb_perdu" => 0,
        "ca_perdu" => 0,
        "nb_gagne" => 0,
        "ca_gagne" => 0,
        "nb_ancien" => 0,
        "ca_ancien" => 0

    );
    

    // 
    // - ne prend que la facturation formation, Audit et Communication.


    $sql="SELECT SUM(fli_montant_ht) as ca ,MONTH(fac_date) AS mois,YEAR(fac_date) as annee,cli_commercial,cli_agence,cli_id
    ,com_label_1,com_label_2
    FROM Factures
    INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
    INNER JOIN Clients ON (Factures.fac_client=clients.cli_id AND Factures.fac_agence=clients.cli_agence)
    LEFT JOIN Commerciaux ON (Clients.cli_commercial=Commerciaux.com_id)
    WHERE fac_date>='" . intval($exercice-1) . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'
    AND fli_categorie<4";
    if( !empty($acc_agence)) {
        $sql.=" AND fac_agence=" . $acc_agence;
    }
    if ($cli_categorie == 1) {
        $sql.=" AND NOT cli_categorie=2";
    } elseif ($cli_categorie == 2) {
        $sql.=" AND cli_categorie=2";
    }
    //$sql.=" AND cli_commercial=198 AND cli_id=8921";
    $sql.=" GROUP BY annee,mois,cli_id,cli_commercial,cli_agence,com_label_1,com_label_2;"; 
    $sql.=" ORDER BY annee,mois,cli_id;"; 

    $req=$ConnSoc->query($sql);
    //var_dump($sql);
    $d_client_fac=$req->fetchAll(PDO::FETCH_ASSOC);
    $req->closeCursor();
    /*echo("<pre>");
        print_r($d_client_fac);
    echo("</pre>");
    die();*/
    if( !empty($d_client_fac) ){
        foreach($d_client_fac as $cli_fac){

            

            if($cli_fac["mois"]<4){
                $exercice_fac=$cli_fac["annee"]-1;
            } else {
                $exercice_fac=$cli_fac["annee"];
            }
            $cle_exe=$exercice-$exercice_fac;

            /*if($cle_exe==1){
                echo("<pre>");
                    print_r($cli_fac);
                echo("</pre>");
            }*/
           
            $ca=0;
            if(!empty($cli_fac["ca"])){
                $ca=floatval($cli_fac["ca"]);
            }

            if(!empty($ca)){

                //echo($cli_fac["cli_commercial"] . "<br/>");

                if(empty($data[$cli_fac["cli_commercial"]])){
                    $data[$cli_fac["cli_commercial"]]=Array(
                        "com_identite" => $cli_fac["com_label_1"] . " " . $cli_fac["com_label_2"],
                        "0" => array(
                            "ca" => 0,
                            "clients" => array()
                        ),
                        "1" => array(
                            "ca" => 0,
                            "clients" => array()
                        ),
                        "nb_renouv" => 0,
                        "ca_renouv" => 0,
                        "nb_perdu" => 0,
                        "ca_perdu" => 0,
                        "nb_gagne" => 0,
                        "ca_gagne" => 0,
                        "nb_ancien" => 0,
                        "ca_ancien" => 0
                    );
                }

                // CA total du commercial sur l'exercice N(0) ou N-1(1)
                $data[$cli_fac["cli_commercial"]][$cle_exe]["ca"]=$data[$cli_fac["cli_commercial"]][$cle_exe]["ca"] + $ca;

                $cle_client=$cli_fac["cli_id"];
                if (isset($d_societe) AND empty($cli_fac["cli_commercial"])) {
                    $cle_client=$cli_fac["cli_id"] . "-" . $cli_fac["cli_agence"];
                }


                // on somme le CA pour le client sur l'exercice concernée

                if(!isset($data[$cli_fac["cli_commercial"]][$cle_exe]["clients"][$cle_client])){

                    $data[$cli_fac["cli_commercial"]][$cle_exe]["clients"][$cle_client]=$ca;

                    // AJOUT D'UN RENOUVELEMENT
                    /*if($cle_exe==0){

                        // je suis sur exercice N -> l'ordre de tri de ma requete me permet d'etre sur que data contient déjà ma facturation N-1
                        if(!empty($data[$cli_fac["cli_commercial"]][1]["clients"][$cli_fac["cli_id"]]) ) {

                            // le client a du CA sur N-1 -> c'est un renouvelé
                            $data[$cli_fac["cli_commercial"]]["nb_renouv"]++;
                            $data[$cli_fac["cli_commercial"]]["ca_renouv"]+=$ca;

                        }
                    }*/

                }else{

                    //echo("CHAT");

                    $data[$cli_fac["cli_commercial"]][$cle_exe]["clients"][$cle_client]=$data[$cli_fac["cli_commercial"]][$cle_exe]["clients"][$cle_client] + $ca;

                    // si apprès avoir sommé le CA, on obtient 0, alors c'est qu'il y a eu FC - AC, donc le client ne doit pas être considéré dans la stat

                   if(empty($data[$cli_fac["cli_commercial"]][$cle_exe]["clients"][$cle_client])){

                        unset($data[$cli_fac["cli_commercial"]][$cle_exe]["clients"][$cle_client]);

                        /*if($cle_exe==0){

                            if(isset($data[$cli_fac["cli_commercial"]][1]["clients"][$cli_fac["cli_id"]]) ) {

                                // le client a du CA sur N-1 -> c'est un renouvelé
                                $data[$cli_fac["cli_commercial"]]["nb_renouv"]--;

                            }
                        }*/

                   }
                   /*else{

                        // j'ai tjr du CA

                        if($cle_exe==0){

                            if(!empty($data[$cli_fac["cli_commercial"]][1]["clients"][$cli_fac["cli_id"]]) ) {

                                // le client a du CA sur N-1 -> c'est un renouvelé
                                $data[$cli_fac["cli_commercial"]]["ca_renouv"]+=$ca;

                            }
                        }

                    }*/

                }
            }

            /*if($cle_exe==0){
                echo("<pre>");
                    print_r($data[199]);
                echo("</pre>");

                echo("-----------------------------------------------<br/><br/>");
            }*/
            
        }
    }

    /*echo("<pre>");
        print_r($data[0]);
    echo("</pre>");
    die();*/

    // CALCUL DES NOUVEAUX et DES PERDUS
    /* pas possibles avec juste le tableau data car 

        LES NOUVEAUX CLIENTS
        un client peut ne pas avoir été facturé en N-1 et facture en N sans pour autant être un nouveau client
        on est obligé d'aller chercher les nouveau clients en base 0

        LES CLIENTS PERDUS
        le nb perdu pourrait être calculé avec la formule (nb_client N-1) - (nb renouve) = perdu
        mais ça ne marche pas pour le CA car le client ne renouvelle forcement a prix égal.

    */
   
    //die();

    

    foreach($data as $com_id => $d){


            // LES CLIENTS N-1

            // SOIT RENOUVELES SUR N SOIT PERDU

            // on part des clients N-1 et on regarde ceux qui ont été renouvelés sur N
            // s'il n'y a pas d'agence il suffit de comparer le CA N-1 du client avec les CA N
            // s'il y a une agence un client facturé sur les autres agences doit quand même etre considéré comme renouvelé.

           /* if($com_id==198){
                echo("<pre>");
                    print_r($data[$com_id]);
                echo("</pre>");
                die();
            }
          */

            $tab_key=array_keys($d[1]["clients"]);
            if(!empty($tab_key)){
                foreach($tab_key as $cli_id){

                    if(!empty($data[$com_id][0]["clients"][$cli_id]))
                    {
                        // le client a du CA sur N -> il est renouvelé
                        $data[$com_id]["nb_renouv"] = $data[$com_id]["nb_renouv"] + 1;
                        $data[$com_id]["ca_renouv"] = $data[$com_id]["ca_renouv"] + $data[$com_id][0]["clients"][$cli_id];

                        /*if($com_id==198){
                            echo($cli_id . "<br/>");
                        }*/

                    }else{

                        $data[$com_id]["nb_perdu"] = $data[$com_id]["nb_perdu"] + 1;
                        $data[$com_id]["ca_perdu"] = $data[$com_id]["ca_perdu"] + $data[$com_id][1]["clients"][$cli_id];
                        
                    }
                }

            }
            // CLIENTS N

            /*  - soit nouveaux 
                - soit renouveles (déjà géré avec les client N-1) 
                - soit anciens.

            NOUVEAU CLIENTS
            */

            // on prend la liste des clients facturés sur N
            $tab_key=array_keys($d[0]["clients"]);

            if(!empty($tab_key)){
                foreach($tab_key as $cli_id){

                    if(empty($data[$com_id][1]["clients"][$cli_id])){

                        // le client n'a pas de CA sur N-1 (si c'était le cas le renouv a été géré sur la boucle des clients N-1)

                        // on regarde s'il est facturé pour la première fois sur N

                        $sql_cli="SELECT cli_id FROM Clients WHERE cli_id=:client
                        AND cli_first_facture_date>='" . intval($exercice) . "-04-01' AND cli_first_facture_date<='" . intval($exercice+1) . "-03-31';";
                        $req_cli=$Conn->prepare($sql_cli);
                        
                        $req_cli->bindParam(":client",$cli_id);
                        $req_cli->execute();
                        $d_nb_client=$req_cli->fetch();
                        if(!empty($d_nb_client)){
                            
                            $data[$com_id]["nb_gagne"]=$data[$com_id]["nb_gagne"]+1;
                            $data[$com_id]["ca_gagne"]=$data[$com_id]["ca_gagne"] + $data[$com_id][0]["clients"][$cli_id];
                            
                        }else{
                            
                            // client avait déjà été facturé avant l'exercice N
                            
                            /*if($com_id==198){
                                echo($cli_id . "<br/>");
                            }*/
                            $data[$com_id]["nb_ancien"]=$data[$com_id]["nb_ancien"]+1;
                            $data[$com_id]["ca_ancien"]=$data[$com_id]["ca_ancien"] + $data[$com_id][0]["clients"][$cli_id];

                        }

                    }
                }
            }
    }

    //die();
    // PARAMETRE VARIABLE STAT
    
	$param_stat=array(
		"titre" => "",
		"agence" => 0
	);

    if($acc_agence>0){

		$sql="SELECT age_nom FROM Agences WHERE age_id=" . $acc_agence . ";";
		$req=$Conn->query($sql);
		$d_agence=$req->fetch();
		if(empty($d_agence)){

			$_SESSION['message'][] = array(
				"titre" => "Erreur",
				"type" => "danger",
				"message" => "Formulaire incomplet!"
			);
			header("location : stat_commercial.php");
			die();
		}else{
			$param_stat["titre"]=$d_agence["age_nom"];
			$param_stat["agence"]=$acc_agence;
		}

	}else{

		$sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();
		if(empty($d_societe)){

			$_SESSION['message'][] = array(
				"titre" => "Erreur",
				"type" => "danger",
				"message" => "Formulaire incomplet!"
			);
			header("location : stat_commercial.php");
			die();
		}else{
			$param_stat["titre"]=$d_societe["soc_nom"];
			$param_stat["agence"]=0;
		}
    }
    
    // TITRE
    if($cli_categorie==1){
        $titre="Prospection géographique " . $exercice . "/" . intval($exercice+1);
    }elseif($cli_categorie==2){
        $titre="Prospection grands-comptes " . $exercice . "/" . intval($exercice+1);
    } else {
        $titre="Prospection " . $exercice . "/" . intval($exercice+1);
    }

	/*echo("<pre>");
		print_r($cle_x);
	echo("</pre>");
	die();*/

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css">

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#zone_print{
				display:none;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 landscape;
				margin:5mm;
			}
			html{
				background-color:#fff!important
			}
			body{
				background-color:#fff!important
			}
			#zone_print{
				background-color:#fff!important;
				font-size:8pt;
			}
		</style>
	</head>

	<body class="sb-top sb-top-sm no-scroll" >

		<div id="zone_print" ></div>

		<div id="main" >

<?php       include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" style="overflow:scroll;" >

					<div id="page_print" >
						<h1 class="text-center" ><?=$titre?></h1>

                        <div class="table-responsive">
                            <table class="table table-striped table-hover" >
                                <thead>
                                    <tr class="dark">
                                        <th rowspan="2" >Commercial</th>
                                        <th colspan="2" class="text-center" >Exercice <?=$exercice-1?>/<?=$exercice?></th>
                                        <th colspan="2" class="text-center" >Exercice <?=$exercice?>/<?=$exercice+1?></th>
                                        <th colspan="2" class="text-center" >Nouveaux clients</th>
                                        <th colspan="2" class="text-center" >Clients renouvelés</th>
                                        <th colspan="2" class="text-center" >Clients anciens ou à faible récurrence</th>                                           
                                        <th colspan="2" class="text-center" >Clients non renouvelés ou perdus</th>
                                    </tr>
                                    <tr>											
                                        <th class="text-center" >Nb Clients</th>
                                        <th class="text-center">CA</th>
                                        <th class="text-center">Nb Clients</th>
                                        <th class="text-center">CA</th>
                                        <th class="text-center">Nb Clients</th>
                                        <th class="text-center">CA</th>
                                        <th class="text-center">Nb Clients</th>
                                        <th class="text-center">CA</th>
                                        <th class="text-center">Nb Clients</th>
                                        <th class="text-center">CA</th>
                                        <th class="text-center">Nb Clients</th>
                                        <th class="text-center">CA</th>
                                    </tr>
                                </thead>
                <?php           if(!empty($data)){  ?>
                                    <tbody>
                        <?php			$total_cli_n=0;
                                        $total_ca_n=0;
                                        $total_cli_n1=0;
                                        $total_ca_n1=0;
                                        $total_nb_gagne=0;
                                        $total_ca_gagne=0;
                                        $total_nb_renouv=0;
                                        $total_ca_renouv=0;
                                        $total_nb_perdu=0;
                                        $total_ca_perdu=0;

                                        $total_nb_ancien=0;
                                        $total_ca_ancien=0;

                                        foreach($data as $k => $d){ 
                                            if($k!=0) { 
                                                
                                                $total_cli_n=$total_cli_n + count($d[0]["clients"]);
                                                $total_ca_n=$total_ca_n + round($d[0]["ca"],2);

                                                $total_cli_n1=$total_cli_n1 + count($d[1]["clients"]);
                                                $total_ca_n1=$total_ca_n1 + round($d[1]["ca"],2);

                                                $total_nb_gagne=$total_nb_gagne + $d["nb_gagne"];
                                                $total_ca_gagne=$total_ca_gagne + round($d["ca_gagne"],2);

                                                $total_nb_renouv=$total_nb_renouv + $d["nb_renouv"];
                                                $total_ca_renouv=$total_ca_renouv + round($d["ca_renouv"],2);

                                                $total_nb_perdu=$total_nb_perdu + $d["nb_perdu"];
                                                $total_ca_perdu=$total_ca_perdu + round($d["ca_perdu"],2);

                                                $total_nb_ancien=$total_nb_ancien + $d["nb_ancien"];
                                                $total_ca_ancien=$total_ca_ancien + round($d["ca_ancien"],2);

                                                ?>
                                                <tr>
                                                    <td><?=$d["com_identite"]?></td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=5" >
                                                            <?=count($d[1]["clients"])?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=5" >
                                                            <?=number_format($d[1]["ca"],2,","," ")?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=1" >
                                                            <?=count($d[0]["clients"])?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=1" >
                                                            <?=number_format($d[0]["ca"],2,","," ")?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=2" >
                                                            <?=$d["nb_gagne"]?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=2" >
                                                            <?=number_format($d["ca_gagne"],2,","," ")?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=3" >
                                                            <?=$d["nb_renouv"]?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=3" >
                                                            <?=number_format($d["ca_renouv"],2,","," ")?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=6" >
                                                            <?=$d["nb_ancien"]?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=6" >
                                                            <?=number_format($d["ca_ancien"],2,","," ")?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=4" >
                                                            <?=$d["nb_perdu"]?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="stat_com_prospection_detail.php?commercial=<?=$k?>&detail=4" >
                                                            <?=number_format($d["ca_perdu"],2,","," ")?>
                                                        </a>
                                                    </td>
                                                </tr>
                        <?php			    }
                                        }
                                        $total_cli_n=$total_cli_n + count($data[0][0]["clients"]);
                                        $total_ca_n=$total_ca_n + round($data[0][0]["ca"],2);

                                        $total_cli_n1=$total_cli_n1 + count($data[0][1]["clients"]);
                                        $total_ca_n1=$total_ca_n1 + round($data[0][1]["ca"],2);

                                        $total_nb_gagne=$total_nb_gagne + $data[0]["nb_gagne"];
                                        $total_ca_gagne=$total_ca_gagne + round($data[0]["ca_gagne"],2);

                                        $total_nb_renouv=$total_nb_renouv + $data[0]["nb_renouv"];
                                        $total_ca_renouv=$total_ca_renouv + round($data[0]["ca_renouv"],2);

                                        $total_nb_perdu=$total_nb_perdu + $data[0]["nb_perdu"];
                                        $total_ca_perdu=$total_ca_perdu + round($data[0]["ca_perdu"],2);

                                        $total_nb_ancien=$total_nb_ancien + $data[0]["nb_ancien"];
                                        $total_ca_ancien=$total_ca_ancien + round($data[0]["ca_ancien"],2);

                                        ?>
                                        <tr>
                                            <td><?=$data[0]["com_identite"]?></td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=5" >
                                                    <?=count($data[0][1]["clients"])?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=5" >
                                                    <?=number_format($data[0][1]["ca"],2,","," ")?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=1" >
                                                    <?=count($data[0][0]["clients"])?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=1" >
                                                    <?=number_format($data[0][0]["ca"],2,","," ")?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=2" >
                                                    <?=$data[0]["nb_gagne"]?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=2" >
                                                    <?=number_format($data[0]["ca_gagne"],2,","," ")?>
                                                </a>
                                            </td>                                              
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=3" >
                                                    <?=$data[0]["nb_renouv"]?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=3" >
                                                    <?=number_format($data[0]["ca_renouv"],2,","," ")?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=6" >
                                                    <?=$data[0]["nb_ancien"]?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=6" >
                                                    <?=number_format($data[0]["ca_ancien"],2,","," ")?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=4" >
                                                    <?=$data[0]["nb_perdu"]?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?commercial=-1&detail=4" >
                                                    <?=number_format($data[0]["ca_perdu"],2,","," ")?>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Total :</td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=5" >
                                                    <?=$total_cli_n1?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=5" >
                                                    <?=number_format($total_ca_n1,2,","," ")?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=1" >
                                                    <?=$total_cli_n?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=1" >
                                                    <?=number_format($total_ca_n,2,","," ")?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=2" >
                                                    <?=$total_nb_gagne?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=2" >
                                                    <?=number_format($total_ca_gagne,2,","," ")?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=3" >
                                                    <?=$total_nb_renouv?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=3" >
                                                    <?=number_format($total_ca_renouv,2,","," ")?>
                                                </a>   
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=6" >
                                                    <?=$total_nb_ancien?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=6" >
                                                    <?=number_format($total_ca_ancien,2,","," ")?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=4" >
                                                    <?=$total_nb_perdu?>
                                                </a>
                                            </td>
                                            <td class="text-right" >
                                                <a href="stat_com_prospection_detail.php?detail=4" >
                                                    <?=number_format($total_ca_perdu,2,","," ")?>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                        <?php	} ?>
                            </table>
                        </div>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
            <div class="row">
                <div class="col-xs-3 footer-left">
                    <a href="stat_commercial.php" class="btn btn-sm btn-default"  >
                        <i class="fa fa-left-arrow" ></i>Retour
                    </a>
                    <!--<button type="button" class="btn btn-sm btn-info ml15 btn-print" >
                        <i class="fa fa-print"></i> Imprimer
                    </button>-->
                </div>
                <div class="col-xs-6 footer-middle text-center" style="">
                    <button type="button" class="btn btn-system btn-sm" data-toggle="modal" data-target="#modal_aide" >
                        <i class="fa fa-info" ></i>
                    </button>
                </div>
                <div class="col-xs-3 footer-right"></div>
            </div>
        </footer>

        <div id="modal_aide" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header bg-system">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="mdl_doc_titre" >
							<i class="fa fa-info" ></i> - <span>Aide</span>
						</h4>
					</div>
					<div class="modal-body" >
                        <p>
                            <b>Détail des colonnes :</b><br>
                            <ul>
                                <li><b>Nouveaux clients :</b> clients facturés pour la première fois sur l'exercice N.</li>
                                <li><b>Clients renouvelés :</b> clients facturés sur l'exercice N-1 et sur l'exercice N.</li>
                                <li>
                                    <b>Clients anciens ou à faible récurrence :</b> 
                                    clients facturés sur l'exercice N 
                                    mais qui ne sont ni nouveaux clients (car déjà facturés avant l'excercice N) ni renouvelés (car non facturés sur l'excercice 
                                    N-1).
                                </li>
                                <li>
                                    <b>Clients non renouvelés ou perdus :</b> 
                                    clients ayants fait l'objet d'une facturation sur l'exercice N-1 qui n'a pas été reconduite 
                                    sur l'exercice N.</li>
                            </ul>
                        </p>
                        <!--<p>
                            <b>Détail des calculs :</b><br>
                            Nb clients <?=intval($exercice-1) . "/" . $exercice?> = Nb. clients renouvelés + Nb. clients non renouvelés ou perdus<br/>
                            Nb clients <?=$exercice . "/" . intval($exercice+1)?> = Nb. nouveaux clients + Nb. clients anciens ou à faible récurrence + Nb. clients renouvelés.<br/>
                            CA clients <?=$exercice . "/" . intval($exercice+1)?> = CA nouveaux clients +CA clients anciens ou à faible récurrence + CA clients renouvelés.
                        </p>
                        <p>
                            <b>Important :</b><br>
                            <ul>
                                <li>
                                    <b>Facturation nulle.</b> 
                                    Les clients dont la sommes des pièces (factures et avoirs) est égale à 0 sur un exercice sont considérés comme non facturés 
                                    et par consequent, ils sont exclus de cette statistique.
                                </li>
                                <li>
                                    <b>Consultation agence et changement de secteur.</b> 
                                    Sur une même société, si un client est facturé par une agence (exemple IDF EST) sur l'exercice <?=intval($exercice-1) . "/" . $exercice?> 
                                    puis par une autre agence (exemple IDF OUEST) sur l'exercice <?=$exercice . "/" . intval($exercice+1)?>, le client sera :<br/>
                                    <ul>
                                        <li>- non renouvelé ou perdu sur IDF EST</li>
                                        <li>- ancien ou à faible récurrence sur IDF OUEST</li>
                                    </ul>
                                </li>
                            </ul>                    
                        </p>-->
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
							<i class="fa fa-close" ></i>
							Fermer
						</button>
					</div>
				</div>
			</div>
		</div>

<?php	include "includes/footer_script.inc.php"; ?>

		<script src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script src="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function () {

				/*

				var calcDataTableHeight = function (elt_content, elt_head) {
					return $(elt_content).height() - $(elt_head).height() - 6;
				};
				var tableDefFix = $('#tableFix').dataTable({
					"language": {
						"url": "/vendor/plugins/DataTables/media/js/French.json"
					},
					paging: false,
					searching: false,

					info: false,
					scrollY: calcDataTableHeight("#tableCont", "#tableHead"),
					scrollCollapse: true,
					order: [[1, "asc"], [2, "asc"]],
					columnDefs: [
						{ targets: 'no-sort', orderable: false }
					]
				});
				$(window).resize(function () {
					var tableDefFixParam = tableDefFix.fnSettings();
					tableDefFixParam.oScroll.sY = calcDataTableHeight("#tableCont", "#tableHead");
					tableDefFix.fnDraw();
				});
				$(window).load(function () {
					setTimeout(function () {
						$(".dataTables_scrollBody").mCustomScrollbar({
							theme: "dark"
						});
					}, 100);
				});
		*/

			});
			(jQuery);
		</script>
	</body>
</html>
