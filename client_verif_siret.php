<?php

// VISUALISATION D'UNE FICHE CLIENT
//Tous les clients sont stockés sur la base ORION
// les suspect sont consulter via un autre script

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');

// VISU D'UNE FICHE CLIENT

$erreur=0;

$client=0;
if(!empty($_GET['client'])){
    $client=intval($_GET['client']);
}
if($client==0){
    echo("Erreur : client inconnu !");
    die();
}

/******************************
DONNEE PARAMETRE
*******************************/

// personne connecte

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
    if($_SESSION['acces']["acc_ref"]==1){
        $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
    }
}

$adr_id = 0;
if(!empty($_GET['adresse'])){
    $adr_id = $_GET['adresse'];
}

// DONNEE DU CLIENT

// donnée client

$req = $Conn->prepare("SELECT * FROM clients WHERE cli_id=" . $client . ";");
$req->execute();
$d_client = $req->fetch();

// CODE APE
$sql="SELECT * FROM Ape ORDER BY ape_code,ape_libelle";
$req = $Conn->query($sql);
$d_apes = $req->fetchAll();

if(empty($d_client)){
    echo("Erreur : client inconnu !");
    die();
}

// donnee client societe
// on utilise Clients_N pour lien avec table commerciaux

$sql="SELECT cli_agence,cli_rib,cli_rib_change,cli_archive,com_label_1,com_label_2 FROM Clients LEFT OUTER JOIN Commerciaux ON (Clients.cli_commercial=Commerciaux.com_id)
WHERE cli_id = " . $client;
if($acc_agence>0){
    $sql.=" AND cli_agence=" . $acc_agence;
}
if(!$_SESSION['acces']["acc_droits"][6]){
    $sql.=" AND cli_utilisateur=" . $acc_utilisateur;
}
$sql.=";";
$req=$ConnSoc->query($sql);
$d_client_societe=$req->fetchAll();

// ALLER CHERCHER LE SIRET DU CLIENT
if(!empty($adr_id)){
    $req = $Conn->prepare("SELECT * FROM adresses WHERE adr_id = " . $adr_id);
}else{
    $req = $Conn->prepare("SELECT * FROM adresses WHERE adr_type = 2 AND adr_ref_id=" . $client . ";");
}

$req->execute();
$d_adr_fac = $req->fetch();
// TEST SIRET
if(!empty($d_adr_fac['adr_siret'])){
    $siret = $d_client['cli_siren'] . " " . $d_adr_fac['adr_siret'];
}else{
    $siret = "";
}

$cli_siren = "";
$adr_siret = "";
// variable message
$message = "";
if(empty($siret)){
    $message = "Aucun résultat trouvé sur l'API Entreprises Data Gouv !";
}else{
    $cli_siren = $d_client['cli_siren'];
    $adr_siret = $d_adr_fac['adr_siret'];
    // REQUETE VERS l'API
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    // UTILISER LA V3
    curl_setopt($ch,CURLOPT_URL, "https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/" . preg_replace('/\s+/', '', $siret));
    $resultat_json=curl_exec($ch);
    curl_close($ch);
    $result=json_decode($resultat_json);
    // SI JE NE TROUVE RIEN AVEC LE SIRET, JE CHERCHE PAR NOM
    if(empty($result->etablissement)){
        $message = "Aucun résultat trouvé sur l'API Entreprises Data Gouv !";
    }else{
        // CHERCHER LE NUMERO DE TVA
        // REQUETE VERS l'API
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        // UTILISER LA V3
        curl_setopt($ch,CURLOPT_URL, "https://entreprise.data.gouv.fr/api/sirene/v1/siren/" . preg_replace('/\s+/', '', $cli_siren));
        $resultat_json=curl_exec($ch);
        curl_close($ch);
        $result2=json_decode($resultat_json);
    }

}


?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - <?=$d_client['cli_nom']?></title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >

    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" >

    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
<form action="client_verif_siret_enr.php" method="POST">
    <div id="main">
        <?php include "includes/header_def.inc.php"; ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">
            <section id="content" class="">

                <div class="row">
                    <div class="col-md-12">
                        <h1><?="[" . $d_client['cli_code'] . "] -" . $d_client['cli_nom']?></h1>
                    </div>

                    <div class="col-md-12">
                        <?php if(!empty($message)){ ?>
                            <div class="alert alert-warning">
                                <?= $message ?>
                            </div>
                        <?php }else{ ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            Données de la fiche client
                                        </div>
                                        <div class="panel-body">
                                            <input type="hidden" name="client" value="<?= $client ?>">
                                            <input type="hidden" class="siren" name="cli_siren" value="<?= $cli_siren ?>">
                                            <input type="hidden" class="siret" name="adr_siret" value="<?= $adr_siret ?>">
                                            <?php if(!empty($adr_id)){ ?>
                                                <input type="hidden" class="adresse" name="adresse" value="<?= $adr_id ?>">
                                                <input type="hidden" name="onglet" value="<?= $_GET['onglet'] ?>">
                                            <?php } ?>

                                            <!-- DONNEES DE DATA GOUV -->
                                            <h3 class="mb20">Informations générales

                                            </h3>
                                            <div class="form-group">
                                                <label for="cli_siret" class="control-label">Siret</label>
                                                <input id="cli_siret" readonly name="cli_siret" placeholder="Siret du client" type="text" class="form-control siretlong"
                                                    value="<?= $cli_siren ?><?= $adr_siret ?>"
                                                >
                                            </div>
                                            <?php if(empty($adr_id)){ ?>
                                                <?php if(empty($adr_id)){?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="button" data-api="general" class="btn btn-sm btn-success pull-right copier_api"> <i class="fa fa-long-arrow-left"></i> Copier les données de l'API</button>
                                                    </div>
                                                </div>
                                                <?php }?>
                                                <div class="form-group">
                                                    <label for="cli_nom" class="control-label">Nom</label>
                                                    <input id="cli_nom" name="cli_nom" placeholder="Pas de nom" type="text" class="form-control"
                                                    <?php if(!empty($d_client['cli_nom'])){ ?>
                                                        value="<?= $d_client['cli_nom'] ?>"
                                                    <?php } ?>
                                                    >
                                                </div>
                                                <?php if(empty($adr_id)){?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <button type="button" data-api="ape" class="btn btn-sm btn-success pull-right copier_api"> <i class="fa fa-long-arrow-left"></i> Copier les données de l'API</button>
                                                        </div>

                                                    </div>
                                                <?php }?>
                                                <div class="form-group">

                                                    <label for="ape" class="control-label">Code APE :</label>
                                                    <select id="ape"  name="cli_ape" class="select2" <?php if(!empty($d_client['cli_ape'])) echo("required"); ?> >
                                                        <option value="">Pas de code APE...</option>
                                                <?php	if(!empty($d_apes)){
                                                            foreach($d_apes as $d_ape){
                                                                if($d_client['cli_ape']==$d_ape["ape_id"]){
                                                                    echo("<option value='" . $d_ape["ape_id"] . "' selected >" . $d_ape["ape_code"] . "-" . $d_ape["ape_libelle"] . "</option>");
                                                                }else{
                                                                    echo("<option value='" . $d_ape["ape_id"] . "' >" . $d_ape["ape_code"] . "-" . $d_ape["ape_libelle"] . "</option>");
                                                                }
                                                            }
                                                        } ?>
                                                    </select>
                                                </div>

                                            <?php } ?>
                                            <hr>
                                            <div class="getHeight">

                                                <div class="row ">

                                                    <div class="col-md-6">
                                                        <h3 class="mb20" style="margin-top:0px">Adresse de facturation </h3>
                                                        <button type="button" data-api="facturation" class="btn mb10  btn-sm text-right btn-success copier_api"> <i class="fa fa-long-arrow-left"></i> Copier les données de l'API</button>
                                                        <div class="form-group">
                                                            <label for="cli_adr_ad1" class="control-label">Nom</label>
                                                            <input id="cli_adr_nom" name="cli_adr_nom" placeholder="Pas de nom" type="text" class="form-control"
                                                                value="<?php if(!empty($d_adr_fac['adr_nom'])){ ?><?= $d_adr_fac['adr_nom'] ?><?php } ?>"
                                                            >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="cli_adr_ad1" class="control-label">Adresse</label>
                                                            <input id="cli_adr_ad1" name="cli_adr_ad1" placeholder="Pas d'adresse de facturation" type="text" class="form-control"
                                                                value="<?php if(!empty($d_adr_fac['adr_ad1'])){ ?><?= $d_adr_fac['adr_ad1'] ?><?php } ?>"
                                                            >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="cli_adr_ad2" class="control-label">Complément d'adresse</label>
                                                            <input id="cli_adr_ad2" name="cli_adr_ad2" placeholder="Pas de complément d'adresse" type="text" class="form-control"
                                                            <?php if(!empty($d_adr_fac['adr_ad2'])){ ?>
                                                                value="<?= $d_adr_fac['adr_ad2'] ?>"
                                                            <?php } ?>
                                                            >
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label for="cli_adr_cp" class="control-label">Code postal</label>
                                                                <input id="cli_adr_cp" name="cli_adr_cp" placeholder="Pas de code postal" type="text" class="form-control code-postal"
                                                                <?php if(!empty($d_adr_fac['adr_cp'])){ ?>
                                                                    value="<?= $d_adr_fac['adr_cp'] ?>"
                                                                <?php } ?>
                                                                >
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="cli_adr_ville" class="control-label">Ville</label>
                                                                <input id="cli_adr_ville" name="cli_adr_ville" placeholder="Pas de ville" type="text" class="form-control"
                                                                <?php if(!empty($d_adr_fac['adr_ville'])){ ?>
                                                                    value="<?= $d_adr_fac['adr_ville'] ?>"
                                                                <?php } ?>
                                                                >
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <?php if(empty($adr_id)){ ?>
                                                    <div class="col-md-6">
                                                        <h3 class="mb20"  style="margin-top:0px">Adresse d'intervention </h3>
                                                        <button type="button" data-api="intervention" class="btn mb10  btn-sm text-right btn-success copier_api"> <i class="fa fa-long-arrow-left"></i> Copier les données de l'API</button>
                                                        <div class="form-group">
                                                            <label for="cli_adr_ad1_int" class="control-label">Nom</label>
                                                            <input id="cli_adr_nom_int" name="cli_adr_nom_int" placeholder="Pas de nom" type="text" class="form-control"
                                                                value="<?php if(!empty($d_client['cli_adr_nom'])){ ?><?= $d_client['cli_adr_nom'] ?><?php } ?>"
                                                            >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="cli_adr_ad1_int" class="control-label">Adresse</label>
                                                            <input id="cli_adr_ad1_int" name="cli_adr_ad1_int" placeholder="Pas d'adresse de facturation" type="text" class="form-control"
                                                                value="<?php if(!empty($d_client['cli_adr_ad1'])){ ?><?= $d_client['cli_adr_ad1'] ?><?php } ?>"
                                                            >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="cli_adr_ad2_int" class="control-label">Complément d'adresse</label>
                                                            <input id="cli_adr_ad2_int" name="cli_adr_ad2_int" placeholder="Pas de complément d'adresse" type="text" class="form-control"
                                                            <?php if(!empty($d_client['cli_adr_ad2'])){ ?>
                                                                value="<?= $d_client['cli_adr_ad2'] ?>"
                                                            <?php } ?>
                                                            >
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label for="cli_adr_cp_int" class="control-label">Code postal</label>
                                                                <input id="cli_adr_cp_int" name="cli_adr_cp_int" placeholder="Pas de code postal" type="text" class="form-control code-postal"
                                                                <?php if(!empty($d_client['cli_adr_cp'])){ ?>
                                                                    value="<?= $d_client['cli_adr_cp'] ?>"
                                                                <?php } ?>
                                                                >
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="cli_adr_ville_int" class="control-label">Ville</label>
                                                                <input id="cli_adr_ville_int" name="cli_adr_ville_int" placeholder="Pas de ville" type="text" class="form-control"
                                                                <?php if(!empty($d_client['cli_adr_ville'])){ ?>
                                                                    value="<?= $d_client['cli_adr_ville'] ?>"
                                                                <?php } ?>
                                                                >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php if(empty($adr_id)){ ?>

                                                <hr>
                                                <h3 class="mb20">Comptabilité <button type="button" data-api="compta" class="btn btn-sm btn-success pull-right copier_api"> <i class="fa fa-long-arrow-left"></i> Copier les données de l'API</button></h3>
                                                <div class="form-group">
                                                    <label for="cli_ident_tva" class="control-label">N° de TVA intracommunautaire</label>
                                                    <input id="cli_ident_tva" name="cli_ident_tva" placeholder="Pas de TVA intracommunautaire" type="text" class="form-control nom"

                                                    value="<?= $d_client['cli_ident_tva'] ?>"
                                                    >
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <!-- DONNEES DE LA FICHE CLIENT -->
                                    <div class="panel">

                                        <div class="panel-heading">
                                            Données importées depuis <strong>l'API <a href="https://entreprise.data.gouv.fr" target="_blank">entreprise.data.gouv.fr</a></strong>
                                        </div>
                                        <div class="panel-body">
                                            <!-- DONNEES DE DATA GOUV -->
											<h3 class="mb20">Informations générales</h3>
                                            <div class="form-group">
                                                <label for="cli_siret_api" class="control-label">Siret</label>
                                                <input id="cli_siret_api" readonly placeholder="Pas de siret" type="text" class="form-control siretlong" value="<?= $cli_siren ?><?= $adr_siret ?>">
                                            </div>
                                            <div class="form-group" <?php if(!empty($adr_id)){ ?> style="display:none;" <?php }else{  ?> style="margin-top:42px;" <?php }?>>
                                                <label for="cli_nom_api" class="control-label">Nom</label>
                                                <input id="cli_nom_api" readonly placeholder="Pas de nom" type="text" class="form-control" <?php if(!empty($result->etablissement->unite_legale->denomination)){ ?>
                                                    value="<?= $result->etablissement->unite_legale->denomination ?>"
                                                <?php } ?>>
                                            </div>
                                            <?php if(empty($adr_id)){ ?>

                                                <div class="form-group" style="margin-top:42px;">
                                                    <label for="cli_ape_api" class="control-label">Code APE</label>
                                                    <input type="hidden" id="cli_ape_api"
                                                    <?php foreach($d_apes as $d_ape){
                                                        if(!empty($result->etablissement->activite_principale) && str_replace(".", "", $result->etablissement->activite_principale) == $d_ape['ape_code']){

                                                        ?>
                                                        value="<?= $d_ape['ape_id'] ?>"
                                                    <?php }
                                                    } ?>
                                                    >
                                                    <input readonly placeholder="Pas de code APE" type="text" class="form-control"
                                                    <?php foreach($d_apes as $d_ape){
                                                        if(!empty($result->etablissement->activite_principale) && str_replace(".", "", $result->etablissement->activite_principale) == $d_ape['ape_code']){

                                                        ?>
                                                        value="<?= $d_ape['ape_code'] ?>-<?= $d_ape['ape_libelle'] ?>"
                                                    <?php }
                                                    } ?>
                                                    >
                                                </div>
                                            <?php } ?>
                                            <!-- DONNEES DE DATA GOUV -->
                                                <hr>
                                            <div class="setHeight">


												<h3 class="mb20">Adresse</h3>
                                                <p><?= $result->etablissement->unite_legale->denomination ?></p>
                                                <p><?php if(!empty($result->etablissement->numero_voie)){ ?><?= $result->etablissement->numero_voie ?><?php } ?><?php if(!empty($result->etablissement->type_voie)){ ?>&nbsp;<?= $result->etablissement->type_voie ?><?php } ?>&nbsp;<?php if(!empty($result->etablissement->libelle_voie)){ ?><?= $result->etablissement->libelle_voie ?><?php } ?></p>
                                                <?php if(!empty($result->etablissement->complement_adresse)){ ?>
                                                    <p><?= $result->etablissement->complement_adresse ?></p>
                                                <?php } ?>
                                                <p>
                                                <?php if(!empty($result->etablissement->code_postal)){ ?>
                                                    <?= $result->etablissement->code_postal ?>
                                                <?php } ?>
                                                <?php if(!empty($result->etablissement->libelle_commune)){ ?>
                                                     <?= $result->etablissement->libelle_commune ?>
                                                <?php } ?>
                                                </p>
                                                    <input id="cli_adr_ad1_api" readonly placeholder="Pas d'adresse" type="hidden" class="form-control"

                                                    value="<?php if(!empty($result->etablissement->numero_voie)){ ?><?= $result->etablissement->numero_voie ?><?php } ?><?php if(!empty($result->etablissement->type_voie)){ ?>&nbsp;<?= $result->etablissement->type_voie ?><?php } ?>&nbsp;<?php if(!empty($result->etablissement->libelle_voie)){ ?><?= $result->etablissement->libelle_voie ?><?php } ?>"
                                                    >
                                                    <input id="cli_adr_ad2_api" readonly placeholder="Pas de complément d'adresse" type="hidden" class="form-control"

                                                    <?php if(!empty($result->etablissement->complement_adresse)){ ?>
                                                        value="<?= $result->etablissement->complement_adresse ?>"
                                                    <?php } ?>
                                                    >
                                                    <input id="cli_adr_cp_api" readonly placeholder="Pas de code postal" type="hidden" class="form-control code-postal"
                                                    <?php if(!empty($result->etablissement->code_postal)){ ?>
                                                        value="<?= $result->etablissement->code_postal ?>"
                                                    <?php } ?>
                                                    >

                                                    <input id="cli_adr_ville_api" readonly placeholder="Pas de ville" type="hidden" class="form-control nom"
                                                    <?php if(!empty($result->etablissement->libelle_commune)){ ?>
                                                        value="<?= $result->etablissement->libelle_commune ?>"
                                                    <?php } ?>
                                                    >
                                            </div>
                                            <?php if(empty($adr_id)){ ?>
                                                <!-- DONNEES DE DATA GOUV -->
                                                <div class="section-divider mt40">
                                                    <hr>
    												<h3 class="mb20">Comptabilité</h3>
    											</div>
                                                <div class="form-group">
                                                    <label for="cli_ident_tva_api" class="control-label">N° de TVA intracommunautaire</label>
                                                    <input id="cli_ident_tva_api" readonly placeholder="Pas de TVA Intracommunautaire" type="text" class="form-control nom"
                                                    <?php if(!empty($result2->numero_tva_intra)){?>
                                                        value="<?= $result2->numero_tva_intra ?>"
                                                    <?php } ?>>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                    </div>
                </div>

            </section>
            <!-- End: Content -->
        </section>

    </div>

    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-4 footer-left">
                <?php if(!empty($_GET['onglet'])){ ?>
                <a href="client_voir.php?client=<?= $d_client['cli_id'] ?>&onglet=<?= $_GET['onglet'] ?>" class="btn btn-default btn-sm">
                <?php }else{ ?>
                <a href="client_voir.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-default btn-sm">
                <?php } ?>
                    <i class="fa fa-long-arrow-left"></i>
                    Retour
                </a>
            </div>
            <div class="col-xs-8 footer-right">
                <button type="submit" class="btn btn-success btn-sm" >
                    <i class='fa fa-floppy-o'></i> Enregistrer
                </a>
            </div>
        </div>
    </footer>
</form>


    <?php include "includes/footer_script.inc.php"; ?>
    <script type="text/javascript" src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
    <script type="text/javascript" src="assets/admin-tools/admin-forms/js/jquery-ui-monthpicker.min.js"></script>

    <script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
    <script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>

    <script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
    <script type="text/javascript" src="/assets/js/custom.js"></script>

    <script type="text/javascript" src="/assets/js/sync_heracles.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            var data = "";
            $(".copier_api").click(function() {
                data = $(this).data('api');
                if(data == "compta"){
                    $("#cli_ident_tva").val($("#cli_ident_tva_api").val());
                }else if(data == "ape"){
                    $("#ape").val($("#cli_ape_api").val());
                    $("#ape").select2();
                    $("#ape").trigger("change");
                }else if(data == "general"){
                    $("#cli_nom").val($("#cli_nom_api").val());

                }else if(data == "facturation"){
                    $("#cli_adr_nom").val($("#cli_nom_api").val());
                    $("#cli_adr_ad1").val($("#cli_adr_ad1_api").val());
                    $("#cli_adr_ad2").val($("#cli_adr_ad2_api").val());
                    $("#cli_adr_cp").val($("#cli_adr_cp_api").val());
                    $("#cli_adr_ville").val($("#cli_adr_ville_api").val());
                }else if(data == "intervention"){
                    $("#cli_adr_nom_int").val($("#cli_nom_api").val());
                    $("#cli_adr_ad1_int").val($("#cli_adr_ad1_api").val());
                    $("#cli_adr_ad2_int").val($("#cli_adr_ad2_api").val());
                    $("#cli_adr_cp_int").val($("#cli_adr_cp_api").val());
                    $("#cli_adr_ville_int").val($("#cli_adr_ville_api").val());
                }
            });

            $("#adr_int_ok").click(function() {
                if($(this).is(":checked")){
                    $("#cli_adr_nom_int").val($("#cli_adr_nom").val());
                    $("#cli_adr_ad1_int").val($("#cli_adr_ad1").val());
                    $("#cli_adr_ad2_int").val($("#cli_adr_ad2").val());
                    $("#cli_adr_cp_int").val($("#cli_adr_cp").val());
                    $("#cli_adr_ville_int").val($("#cli_adr_ville").val());
                }
            });

            // CALCULER LA HAUTEUR

            var divHeight = $(".getHeight").height();

            $('.setHeight').css('height', divHeight+'px');
        });


    </script>
</body>
</html>
