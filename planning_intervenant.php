<?php
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");

include "modeles/mod_parametre.php";


// AJOUT OU SUPPRESSION D'UN INTERVENANT AU PLANNING

// sur la personne connecte

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);
}

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
/*if($acc_societe!=3){
	$acc_agence=0;
}*/

// Attention les dates sont recu en format jj/mm/aaaa
// elle doivent etre convertie en aaaa-mm-jj

if(!empty($_POST["ref_deb"])){

	$ref_deb=$_POST["ref_deb"];
	$periode_deb=$_POST["periode_deb"];
	$periode_fin=$_POST["periode_fin"];
	//echo("POST<br/>");

}elseif(!empty($_SESSION["pla_periode_deb"])){

	// en provenance du planning
	$ref_deb=$_SESSION["pla_periode_deb"];
	$periode_deb=$_SESSION["pla_periode_deb"];
	$periode_fin=$_SESSION["pla_periode_fin"];
	//echo("SESSION<br/>");
}
//echo("SESSION<br/>");
//var_dump($_SESSION["pla_periode_deb"]);
//die();
$c_ref_deb=convert_date_sql($ref_deb);

$date_ref = new DateTime($c_ref_deb);
$semaine=$date_ref->format("W");
$annee=$date_ref->format("Y");

$date_ref->add(new DateInterval('P5D'));
$ref_fin=$date_ref->format("d/m/Y");



?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<form method="post" action="planning_intervenant_enr.php" id="form_int" >
			<div>
				<input type="hidden" name="organisation" id="organisation" value="" />
			</div>
			<!-- Start: Main -->
			<div id="main">
				<?php
					include "includes/header_def.inc.php";
				?>

				<section id="content_wrapper">

					<section id="content" class="animated fadeIn">

						<h3 class="text-center" >Organisation du planning</h3>

						<!--BLOC INSCRIPTION -->
						<div class="row" >
							<div class="col-md-6">
								<div class="panel" id="form_interne" >
									<div class="panel-heading" >
										<span class="panel-title">Référence : Semaine <?=$date_ref->format("W") . " " . $date_ref->format("Y")?></span>
									</div>
									<div class="panel-body">
										<div class="admin-form" >
											<div class="row" >
												<div class="col-md-6">
													Du
													<div class="field prepend-icon">
														<input type="text" id="ref_deb" name="ref_deb" class="gui-input" placeholder="Selectionner une date" value="<?=$ref_deb?>" required />
														<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
													</div>
												</div>
												<div class="col-md-6">
													Au
													<div class="field prepend-icon">
														<input type="text" id="ref_fin" name="ref_fin" class="gui-input" value="<?=$ref_fin?>" readonly />
														<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel" id="form_interne" >
									<div class="panel-heading head-sm" >
										<span class="panel-title">Dupliquer cette organisation :</span>
									</div>
									<div class="panel-body">
										<div class="admin-form" >
											<div class="row" >
												<div class="col-md-6">
													Du
													<div class="field prepend-icon">
														<input type="text" id="periode_deb" name="periode_deb" class="gui-input" placeholder="Selectionner une date" value="<?=$periode_deb?>" />
														<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
													</div>
												</div>
												<div class="col-md-6">
													Au
													<div class="field prepend-icon">
														<input type="text" id="periode_fin" name="periode_fin" class="gui-input" placeholder="Selectionner une date" value="<?=$periode_fin?>" />
														<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
			<?php		$sql="SELECT * FROM Intervenants LEFT OUTER JOIN Plannings_Intervenants";
						$sql.=" ON (Intervenants.int_id=Plannings_Intervenants.pin_intervenant	AND pin_semaine=:semaine AND pin_annee=:annee AND pin_agence=:acc_agence)";
						$sql.=" WHERE (NOT int_archive OR NOT ISNULL(pin_place))";
						if($acc_agence>0){
							$sql.=" AND (
								(
									(int_agence=:acc_agence OR int_agence=0) AND ISNULL(pin_agence)
								)
								 OR
								 (
									pin_agence=:acc_agence
								 )
							 )";
						}
						$sql.=" ORDER BY pin_place,int_label_1,int_label_2;";
						$req=$ConnSoc->prepare($sql);
						$req->bindValue(":semaine",ltrim($semaine, '0'));
						$req->bindParam(":annee",$annee);
						$req->bindParam(":acc_agence",$acc_agence);
						$req->execute();
						$intervenants=$req->fetchAll();
						if(!empty($intervenants)){ ?>
							<select class="inscrit" multiple="multiple" size="10" name="inscrit" id="inscrit" >
			<?php				foreach($intervenants as $i){
									if(!empty($i["pin_place"])){
										echo("<option value='" . $i["int_id"] . "' selected >". $i["int_label_1"] . " " . $i["int_label_2"] . "</option>");
									}else{
										echo("<option value='" . $i["int_id"] . "' >". $i["int_label_1"] . " " . $i["int_label_2"] . "</option>");
									}
								}	?>
							</select>
			<?php		} ?>
					</section>
					<!-- End: Content -->
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="planning.php" class="btn btn-default btn-sm" >
							<i class="fa fa-long-arrow-left" ></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" >&nbsp;</div>
					<div class="col-xs-3 footer-right" >
						<button type="button" class="btn btn-success btn-sm" id="btn_submit" >
							<span class="fa fa-save"></span>
							Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
		<?php
		include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/duallistbox/jquery.bootstrap-duallistbox.js"></script>
		<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<script type="text/javascript" >
			 jQuery(document).ready(function() {

				 //PERIODE DE REFERENCE
				$("#ref_deb").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=1) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},
					onSelect: function (input, inst){
						$("#form_int").attr("action", "planning_intervenant.php");
						$("#form_int").submit();
					}
				});
				// PERIODE D'AFFECTATION
				$("#periode_deb").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=1) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},
					onSelect: function (input, inst){
						var date_fin=$('#periode_deb').datepicker( "getDate" );
						date_fin.setDate(date_fin.getDate() + 5);
						var max_fin=new Date();
						max_fin.setDate(date_fin.getDate() + 84);
						$("#periode_fin").datepicker( "setDate",date_fin);
						$("#periode_fin").datepicker( "option", "minDate", date_fin);
						$("#periode_fin").datepicker( "option", "maxDate", max_fin);
					}
				});
				$("#periode_fin").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=6) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},

				});

				// DUAL LISTE

				var $inscrit = $('.inscrit').bootstrapDualListbox({
					filterPlaceHolder: 'Rechercher',
					selectedListLabel: 'Intervenants inscrits au planning',
					nonSelectedListLabel: 'Intervenants disponibles',
					infoText: '',
					moveOnSelect: false,
					upDown:true
				});

				// ENVOIE DU FORM

				$("#btn_submit").click(function(){
					var liste_tri=$('.inscrit').bootstrapDualListbox("getValueOrder");
					$('#organisation').val(liste_tri);
					$("#form_int").attr("action", "planning_intervenant_enr.php");
					$("#form_int").submit();
				});
			 });
		</script>
	</body>
</html>
