<?php 
// CRON JOB : RUN EVERY DAY AT 12H PM

include("../includes/connexion.php");
include("../modeles/mod_envoi_mail.php");
// SELECT TOUS LES DOCUMENTS NOUVEAUX
$req=$Conn->prepare("SELECT  doc_id,doc_date_creation, doc_nouveau FROM documents WHERE doc_nouveau = 1");
$req->execute();
$documents = $req->fetchAll();

foreach($documents as $d){
	// SI LA DATE DE CREATION EST SUPERIEURE A 6 MOIS, ALORS CE N'EST PLUS UN NOUVEAU DOCUMENT
	$date = $d['doc_date_creation'];
	$date = strtotime(date("Y-m-d", strtotime($date)) . " +6 month");
	$date = date("Y-m-d",$date);

	if($date <= date("Y-m-d")){
		// UPDATE LE DOCUMENT
		$req=$Conn->prepare("UPDATE documents SET doc_nouveau = 0 WHERE doc_id = " . $d['doc_id']);
		$req->execute();
	}

}

$param_mail=array(
	"sujet" => "Tâche récurrente ORION : etech_actualise_nouveau_doc.php",
	"identite" => "Informatique",
	"message" => "La tâche planifiée s'est super bien passée, niquel, bon travail."
);

$adr_mail=array(
	"0" => array(
		"adresse" => "informatique@si2p.mobi",
		"nom" => "Informatique"
	)
);

$mail=envoi_mail("si2p",$param_mail,$adr_mail);

var_dump($mail);
die();



?>