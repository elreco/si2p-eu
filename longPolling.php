<?php
$minute = date("i");
 
while(1) {
  // Retourne des données uniquement
  // si la minute change, sinon boucle
  if(isMinuteChanged($minute)) {
    echo json_encode(date("G").":".date("i"));
    break;
  }
 
  // Pause de 3s
  sleep(3);
}
 
function isMinuteChanged(&$savedMinute) {
  $currentMinute = date("i");
  if ( $savedMinute != $currentMinute ) {
    $savedMinute = $currentMinute;
    return true;
  }
  return false;
}
?>