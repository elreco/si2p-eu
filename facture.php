 <?php

	// EDITIONS D'UNE FACTURE

	include "includes/controle_acces.inc.php";

	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");

	include_once("modeles/mod_parametre.php");

	include 'modeles/mod_get_commerciaux.php';

	include_once("modeles/mod_facture_actions_get.php");
	include_once("modeles/mod_planning_periode_lib.php");
	include_once("modeles/mod_planning_periode.php");
	include_once("modeles/mod_get_client_produits.php");
	include_once("modeles/mod_get_contacts.php");
	include_once('modeles/mod_get_client_infos.php');
	include_once 'modeles/mod_orion_utilisateur.php';
	include_once 'modeles/mod_get_groupe_filiales.php';
	include_once 'modeles/mod_clients_get.php';


	//include 'modeles/mod_droit.php';
	//include 'modeles/mod_get_societe.php';
	//include 'modeles/mod_get_cli_categories.php';
	//include 'modeles/mod_get_pro_familles.php';
	//include 'modeles/mod_get_adresses.php';


	/********************************************
		PARAMETRE
	********************************************/

	if(!$_SESSION["acces"]["acc_droits"][29]){
		echo("Impossible d'afficher la page!");
		die();
	}

	$erreur_txt="";

	$facture_id=0;
	if(isset($_GET["facture"])){
		if(!empty($_GET["facture"])){
			$facture_id=$_GET["facture"];
		}
	}

	$action_client_id=0;
	if(isset($_GET["action_client"])){
		if(!empty($_GET["action_client"])){
			$action_client_id=$_GET["action_client"];
		}
	}

	$avoir_id=0;
	if(isset($_GET["avoir"])){
		if(!empty($_GET["avoir"])){
			$avoir_id=intval($_GET["avoir"]);
		}
	}

	$client_id=0;
	if(isset($_GET["client"])){
		if(!empty($_GET["client"])){
			$client_id=intval($_GET["client"]);
		}
	}

	/********************************************
		DONNEE POUR TRAITEMENT
	********************************************/

	// la personne logue

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}

	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	$action_soc=0;
	if($acc_agence==4 AND $_SESSION["acces"]["acc_droits"][8]){
		if(isset($_GET["action_soc"])){
			if(!empty($_GET["action_soc"])){
				$action_soc=intval($_GET["action_soc"]);
			}
		}
	}
	if(empty($action_soc)){
		$action_soc=$acc_societe;
	}
	$ConnFct=connexion_fct($action_soc);


	if($facture_id>0){

		$client_verrou=true;

		// EDITION D'UNE FACTURE EXISTANTE

		$sql="SELECT * FROM Factures WHERE fac_id=" . $facture_id . ";";
		$req=$ConnSoc->query($sql);
		$d_facture=$req->fetch();
		if(!empty($d_facture)){

			$DTime_fac_date = date_create_from_format('Y-m-d',$d_facture["fac_date"]);

		}else{
			$erreur_txt="Impossible d'afficher la page!";
		}

		// LES LIGNES DEJA ENREGISTREE

		$sql_lignes="SELECT * FROM Factures_Lignes WHERE fli_facture=" . $facture_id . ";";
		$req_lignes=$ConnSoc->query($sql_lignes);
		$d_lignes=$req_lignes->fetchAll();
		if(!empty($d_lignes)){
			$client_verrou=true;
		}

	}else{

		// CREATION DE FACTURE

		$client_verrou=false;

		$d_facture=array(
			"fac_nature" => 1,
			"fac_agence" => $acc_agence,
			"fac_adresse" => 0,
			"fac_client" => 0,
			"fac_cli_suc" => 0,
			"fac_cli_nom" => "",
			"fac_cli_service" => "",
			"fac_cli_ad1" => "",
			"fac_cli_ad2" => "",
			"fac_cli_ad3" => "",
			"fac_cli_cp" => "",
			"fac_cli_ville" => "",
			"fac_cli_geo" => 1,
			"fac_contact" => null,
			"fac_commercial" => 0,
			"fac_reference" => "",
			"fac_reg_formule" => 0,
			"fac_reg_nb_jour" => 0,
			"fac_reg_fdm" => 0,
			"fac_libelle" => "",
			"fac_total_ht" => 0,
			"fac_con_nom" => "",
			"fac_con_prenom" => "",
			"fac_con_tel" => "",
			"fac_con_portable" => "",
			"fac_con_mail" => "",
			"fac_env_adresse" => 0,
			"fac_reg_type" => 0,
			"fac_opca_type" => 0,
			"fac_opca" => 0,
			"fac_env_nom" => "",
			"fac_env_cp" => "",
			"fac_env_ville" => "",
			"fac_con_titre" => 0,
			"fac_convention" => 0
		);

		$DTime_fac_date=new DateTime();

		// permet de figer la date de facture en fin de mois
		/* if(date("m")==12 AND date("Y")==2020 AND $acc_societe==5 ){
			$DTime_fac_date=DateTime::createFromFormat('Y-m-d',"2020-11-30");
		} */

		// CREATION D'UNE FACTURE A PARTIR D'UNE ACTION CLIENT

		if($action_client_id>0){

			// ON VERIFIE QUE L'ACTION N'EST PAS DEJA LIE A UNE FACTURE NON BLOQUE

			$sql="SELECT fac_id,fac_numero FROM Factures,Factures_Lignes WHERE fac_id=fli_facture AND NOT fac_blocage AND fli_action_client=" . $action_client_id . " AND fli_action_cli_soc=" . $action_soc . ";";
			$req=$ConnSoc->query($sql);
			$d_fac_ouverte=$req->fetch();
			if(!empty($d_fac_ouverte)){

				$_SESSION['message'][] = array(
					"titre" => "Erreur",
					"type" => "warning",
					"message" => "L'action " . $action_client_id . " est déjà liée à la facture " . $d_fac_ouverte["fac_numero"] . "."
				);
				header("location :facture_voir.php?facture=" . $d_fac_ouverte["fac_id"]);
				die();

			}

			$sql="SELECT acl_client,acl_commercial,acl_facturation,act_agence,acl_opca_fac FROM Actions_Clients,Actions WHERE acl_action=act_id AND acl_id=" . $action_client_id . ";";
			$req=$ConnFct->query($sql);
			$d_action_client=$req->fetch();
			if(!empty($d_action_client)){

				$client_verrou=true;

				// FG ne sert a rien doublon avec la ligne 178.
				//$DTime_fac_date=new DateTime();

				$fac_client=0;
				$fac_cli_suc=0;
				if($acc_societe!=4 AND $d_action_client["acl_facturation"]==1){
					// facturation d'une action à GFC
					$fac_client=8891;
					$fac_cli_suc=$d_action_client["acl_client"];
				}else{
					// facturation à un client
					$fac_client=$d_action_client["acl_client"];

				}

				$d_facture["fac_client"]=$fac_client;
				$d_facture["fac_cli_suc"]=$fac_cli_suc;
				$d_facture["fac_date"]=$DTime_fac_date->format("Y-m-d");
				//$d_facture["fac_reference"]=$d_action_client["acl_bc_num"];

				if($action_soc==$acc_societe){
					$d_facture["fac_commercial"]=$d_action_client["acl_commercial"];
					$d_facture["fac_agence"]=$d_action_client["act_agence"];
				}



			}else{

				$erreur_txt="Impossible de charger les données de l'action à facturer";
				$d_facture=array(
					"fac_client" => 0
				);
			}

		}elseif($avoir_id>0){

			$client_verrou=true;

			// CREATION D'UN AVOIR EN PARTANT DE LA FACTURE $avoir_id

			$sql="SELECT * FROM Factures WHERE fac_id=" . $avoir_id . ";";
			$req=$ConnSoc->query($sql);
			$d_facture=$req->fetch();
			if(empty($d_facture)){
				$erreur_txt="Impossible d'afficher la page!";
			}else{
				$d_facture["fac_nature"]=2;
				$d_facture["fac_libelle"]="Avoir se référant à la facture N° " . $d_facture["fac_numero"];
				$d_facture["fac_total_ht"]=0;
			}

			// LES LIGNES DEJA ENREGISTREE

			$sql_lignes_avoir="SELECT * FROM Factures_Lignes WHERE fli_facture=" . $avoir_id . ";";
			$req_lignes_avoir=$ConnSoc->query($sql_lignes_avoir);
			$d_lignes_avoir=$req_lignes_avoir->fetchAll();

		}elseif($client_id>0){

			$d_facture["fac_client"]=$client_id;
			$d_facture["fac_date"]=$DTime_fac_date->format("Y-m-d");
		}

	}

	$base_tva=array(
		0 => "&nbsp;",
		1 => "Normale",
		2 => "Réduit alimentaire",
		3 => "Sans TVA",
		4 => "Réduit",
		5 => "Spé"
	);

	$d_ad_opca=array();
	$d_ad_client=array();
	$fac_groupe=null;
	$liste_fil=null;

	// DONNEE LIE AU CLIENT

	if(empty($erreur_txt) AND !empty($d_facture["fac_client"])){

		// info sur le client
		$sql="SELECT cli_id,cli_code,cli_nom,cli_groupe,cli_filiale_de,cli_categorie,
		cli_reg_type,cli_reg_formule,cli_reg_fdm,cli_reg_nb_jour,cli_opca,cli_opca_type,cli_fac_groupe,cli_affacturage,cli_affacturage_iban
		,cso_rib,cso_commercial FROM Clients,Clients_Societes WHERE cli_id=cso_client AND cli_id=:client AND cso_societe=:cso_societe";
		if(!empty($d_facture["fac_agence"])){
			$sql.=" AND cso_agence=" . $d_facture["fac_agence"];
		}
		if(!$_SESSION["acces"]["acc_droits"][6]){
			$sql.=" AND cso_utilisateur=" . $acc_utilisateur;
		}
		$sql.=";";
		$req=$Conn->prepare($sql);
		$req->bindParam(":client",$d_facture["fac_client"]);
		$req->bindParam(":cso_societe",$acc_societe);
		$req->execute();
		$d_client=$req->fetch(PDO::FETCH_ASSOC);
		if(!empty($d_client)){

			$d_facture["fac_reg_type"]=$d_client["cli_reg_type"];
			$d_facture["fac_reg_formule"]=$d_client["cli_reg_formule"];
			$d_facture["fac_reg_fdm"]=$d_client["cli_reg_fdm"];
			$d_facture["fac_reg_nb_jour"]=$d_client["cli_reg_nb_jour"];

			if(empty($d_facture["fac_commercial"])){
				$d_facture["fac_commercial"]=$d_client["cso_commercial"];
			}

			if(isset($d_action_client)){
				if(!empty($d_action_client["acl_opca_fac"]) AND empty($fac_cli_suc)){
					$d_facture["fac_opca"]=$d_client["cli_opca"];
					$d_facture["fac_opca_type"]=$d_client["cli_opca_type"];
				}
			}
			// activation de la facturation groupée
			if(!empty($d_client["cli_fac_groupe"] AND $d_facture["fac_nature"]==1)){
				if(empty($facture_id) OR $d_client["cli_fac_groupe"]==$d_facture["fac_client"]){
					$fac_groupe=$d_client["cli_fac_groupe"];
				}
			}
			if($client_verrou AND empty($d_client["cso_rib"])){
				$erreur_txt="Le RIB du client n'est pas renseigné";
			}
		}else{

		}

		// FACTURATION GROUPEE
		if(!empty($fac_groupe)){
			// FG le 28/11/2019
			/* Le fait de scinder la facturation groupée et la factuartion classique fait que dans le cadre d'une facturation groupée, $fac_groupe est toujours = à $d_facture["fac_client"]
			donc le ci-dessous en sert plus à rien */

			// si le factureur n'est pas le client -> on actualise les données de la facture en fonction de la fiche du factureur
			/*if($fac_groupe!=$d_facture["fac_client"]){

				$sql="SELECT cli_id,cli_code,cli_nom,cli_groupe,cli_filiale_de,cli_reg_type,cli_reg_formule,cli_reg_fdm,cli_reg_nb_jour,cli_opca,cli_categorie
				,cli_affacturage,cli_affacturage_iban
				,cso_rib,cso_commercial FROM Clients,Clients_Societes WHERE cli_id=cso_client AND cli_id=:client AND cso_societe=:cso_societe";
				if(!empty($d_facture["fac_agence"])){
					$sql.=" AND cso_agence=" . $d_facture["fac_agence"];
				}
				if(!$_SESSION["acces"]["acc_droits"][6]){
					$sql.=" AND cso_utilisateur=" . $acc_utilisateur;
				}
				$sql.=";";
				$req=$Conn->prepare($sql);
				$req->bindParam(":client",$fac_groupe);
				$req->bindParam(":cso_societe",$acc_societe);
				$req->execute();
				$d_client=$req->fetch(PDO::FETCH_ASSOC);
				if(!empty($d_client)){
					if($client_verrou AND empty($d_client["cso_rib"])){
						$erreur_txt="Le RIB du client n'est pas renseigné";
					}else{
						$d_facture["fac_client"]=$fac_groupe;

						$d_facture["fac_reg_type"]=$d_client["cli_reg_type"];
						$d_facture["fac_reg_formule"]=$d_client["cli_reg_formule"];
						$d_facture["fac_reg_fdm"]=$d_client["cli_reg_fdm"];
						$d_facture["fac_reg_nb_jour"]=$d_client["cli_reg_nb_jour"];

						$d_facture["fac_commercial"]=$d_client["cso_commercial"];
					}
				}
			}*/

			// FG le 28/11/2019
			/* il est plus rapide d'utiliser le champ cli_fac_groupe que de reconstituer l'arbo*/

			// on recherche les filiales pouvant etre facture
			/*$filiales=get_groupe_filiales($fac_groupe);
			$tab_fil=array_column ($filiales,"cli_id");
			$liste_fil=implode($tab_fil,",");
			if(!empty($liste_fil)){
				$liste_fil.="," . $fac_groupe;
			}else{
				$liste_fil=$fac_groupe;
			}*/

			$sql="SELECT cli_id FROM Clients WHERE cli_fac_groupe=" . $fac_groupe . ";";
			$req=$Conn->query($sql);
			$d_cli_facturable=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($d_cli_facturable)){
				$tab_cli_facturable=array_column ($d_cli_facturable,"cli_id");
				$liste_fil=implode($tab_cli_facturable,",");
			}

		}

		// GESTION DE L'AFFACTURAGE

		// FG plus de controle
		// si la société dispose d'un rib d'affacturage et que le client remplit les conditions (hors opca onterco partciculier et client non affacturable)
		//		-> on enregiste la facture en affacturage

		/*if($d_client["cli_affacturage"] AND $acc_societe!=17){

			$sql_rib_aff="SELECT rib_id FROM Rib WHERE rib_societe=:acc_societe AND rib_affacturage;";
			$req_rib_aff=$Conn->prepare($sql_rib_aff);
			$req_rib_aff->bindParam(":acc_societe",$acc_societe);
			$req_rib_aff->execute();
			$d_rib_aff=$req_rib_aff->fetchAll();
			if(!empty($d_rib_aff)){
				if(count($d_rib_aff)>1){
					$erreur_txt="Le RIB d'affacturage n'est pas identifiable. Merci de contacter le service comptabilité.";
				}
			}else{
				$erreur_txt="Votre société ne dispose pas d'un RIB d'affacturage. Merci de contacter le service comptabilité.";
			}

		}*/


		// requete adresse

		$adresses=array();

		$sql_ad="SELECT * FROM Adresses WHERE adr_ref=1 AND adr_ref_id=:client AND adr_type=:adr_type ORDER BY adr_libelle,adr_cp,adr_ville;";
		$req_ad=$Conn->prepare($sql_ad);

		// ADRESSES DE FACTURATION DU CLIENT

		$req_ad->bindParam(":client",$d_facture["fac_client"]);
		$req_ad->bindValue(":adr_type",2);
		$req_ad->execute();
		$cli_ad_fac=$req_ad->fetchAll();
		if(!empty($cli_ad_fac)){
			foreach($cli_ad_fac as $ad){
				if(!empty($ad["adr_libelle"])){
					$ad_text=$ad["adr_libelle"];
				}else{
					$ad_text=$ad["adr_nom"] . " " . $ad["adr_cp"] . " " . $ad["adr_ville"];
				}
				$d_ad_client[]=array(
					"id" => $ad["adr_id"],
					"text" => $ad_text,
					"nom" => $ad["adr_nom"],
					"service" => $ad["adr_service"],
					"ad1" => $ad["adr_ad1"],
					"ad2" => $ad["adr_ad2"],
					"ad3" => $ad["adr_ad3"],
					"cp" => $ad["adr_cp"],
					"ville" => $ad["adr_ville"],
					"defaut" => $ad["adr_defaut"],
					"geo" => $ad["adr_geo"],
				);

				if(empty($d_facture["fac_adresse"]) AND $ad["adr_defaut"] AND empty($d_facture["fac_opca"])){
					$d_facture["fac_adresse"]=$ad["adr_id"];
					$d_facture["fac_cli_nom"]=$ad["adr_nom"];
					$d_facture["fac_cli_service"]=$ad["adr_service"];
					$d_facture["fac_cli_ad1"]=$ad["adr_ad1"];
					$d_facture["fac_cli_ad2"]=$ad["adr_ad2"];
					$d_facture["fac_cli_ad3"]=$ad["adr_ad3"];
					$d_facture["fac_cli_cp"]=$ad["adr_cp"];
					$d_facture["fac_cli_ville"]=$ad["adr_ville"];


				}
			}
		}

		// ADRESSE DE FAC DE LA MM
		if($d_client["cli_groupe"]==1 AND !empty($d_client["cli_filiale_de"])){
			$req_ad->bindParam(":client",$d_client["cli_filiale_de"]);
			$req_ad->bindValue(":adr_type",2);
			$req_ad->execute();
			$cli_ad_fac=$req_ad->fetchAll();
			if(!empty($cli_ad_fac)){
				foreach($cli_ad_fac as $ad){
					if(!empty($ad["adr_libelle"])){
						$ad_text=$ad["adr_libelle"];
					}else{
						$ad_text=$ad["adr_nom"] . " " . $ad["adr_cp"] . " " . $ad["adr_ville"];
					}
					$d_ad_client[]=array(
						"id" => $ad["adr_id"],
						"text" => $ad_text,
						"nom" => $ad["adr_nom"],
						"service" => $ad["adr_service"],
						"ad1" => $ad["adr_ad1"],
						"ad2" => $ad["adr_ad2"],
						"ad3" => $ad["adr_ad3"],
						"cp" => $ad["adr_cp"],
						"ville" => $ad["adr_ville"],
						"defaut" => $ad["adr_defaut"],
						"geo" => $ad["adr_geo"],
					);

					if(empty($d_facture["fac_adresse"]) AND $ad["adr_defaut"] AND empty($d_facture["fac_opca"])){
						$d_facture["fac_adresse"]=$ad["adr_id"];
						$d_facture["fac_cli_nom"]=$ad["adr_nom"];
						$d_facture["fac_cli_service"]=$ad["adr_service"];
						$d_facture["fac_cli_ad1"]=$ad["adr_ad1"];
						$d_facture["fac_cli_ad2"]=$ad["adr_ad2"];
						$d_facture["fac_cli_ad3"]=$ad["adr_ad3"];
						$d_facture["fac_cli_cp"]=$ad["adr_cp"];
						$d_facture["fac_cli_ville"]=$ad["adr_ville"];
					}
				}
			}
		}


		// ADRESSE DE FAC DE L'OPCA
		if(!empty($d_facture["fac_opca"])){
			$req_ad->bindParam(":client",$d_facture["fac_opca"]);
			$req_ad->bindValue(":adr_type",2);
			$req_ad->execute();
			$opca_ad_fac=$req_ad->fetchAll();
			if(!empty($opca_ad_fac)){
				foreach($opca_ad_fac as $ad){
					if(!empty($ad["adr_libelle"])){
						$ad_text=$ad["adr_libelle"];
					}else{
						$ad_text=$ad["adr_nom"] . " " . $ad["adr_cp"] . " " . $ad["adr_ville"];
					}

					$d_ad_opca[]=array(
						"id" => $ad["adr_id"],
						"text" => $ad_text,
						"nom" => $ad["adr_nom"],
						"service" => $ad["adr_service"],
						"ad1" => $ad["adr_ad1"],
						"ad2" => $ad["adr_ad2"],
						"ad3" => $ad["adr_ad3"],
						"cp" => $ad["adr_cp"],
						"ville" => $ad["adr_ville"],
						"defaut" => $ad["adr_defaut"],
						"geo" => $ad["adr_geo"],
					);
					if(empty($d_facture["fac_adresse"]) AND $ad["adr_defaut"]){
						$d_facture["fac_adresse"]=$ad["adr_id"];
						$d_facture["fac_cli_nom"]=$ad["adr_nom"];
						$d_facture["fac_cli_service"]=$ad["adr_service"];
						$d_facture["fac_cli_ad1"]=$ad["adr_ad1"];
						$d_facture["fac_cli_ad2"]=$ad["adr_ad2"];
						$d_facture["fac_cli_ad3"]=$ad["adr_ad3"];
						$d_facture["fac_cli_cp"]=$ad["adr_cp"];
						$d_facture["fac_cli_ville"]=$ad["adr_ville"];
					}
				}
			}
			$adresses=$d_ad_opca;
		}else{
			$adresses=$d_ad_client;
		}

		// ADRESSE D'ENVOI -> toijours au client
		$d_ad_envoi=array();
		$req_ad->bindParam(":client",$d_facture["fac_client"]);
		$req_ad->bindValue(":adr_type",3);
		$req_ad->execute();
		$d_ad_envoi=$req_ad->fetchAll();

		// ADRESSE D'ENVOI DE LA MM
		if($d_client["cli_groupe"]==1 AND !empty($d_client["cli_filiale_de"])){
			$req_ad->bindParam(":client",$d_client["cli_filiale_de"]);
			$req_ad->bindValue(":adr_type",3);
			$req_ad->execute();
			$cli_ad_env=$req_ad->fetchAll();
			if(!empty($cli_ad_env)){
				foreach($cli_ad_env as $ad){
					$d_ad_envoi[]=$ad;
				}
			}
		}

		// CONTACTS
		if(!empty($d_facture["fac_opca"])){
			if(empty($d_facture["fac_env_adresse"])){
				$d_contacts=get_contacts(1,$d_facture["fac_opca"],$d_facture["fac_adresse"],null);
			}else{
				// fac OPCA mais envoie au client
				$d_contacts=get_contacts(1,$d_facture["fac_client"],$d_facture["fac_env_adresse"],null);
			}
		}else{

			$d_contacts=get_contacts(1,$d_facture["fac_client"],$d_facture["fac_adresse"],null);
		}


		// LIGNES POUVANT ETRE AJOUTE = ACTIONS DE FORMATIONS
		if($d_facture["fac_nature"]==1){
			$d_actions=get_facture_actions($d_facture["fac_client"],$d_facture["fac_commercial"],$d_facture["fac_date"],$facture_id,$d_facture["fac_cli_suc"],$d_facture["fac_nature"],$d_client["cli_categorie"],$liste_fil);
			if(!empty($d_actions)){
				// FG 28/11/2019
				/* pas de rechargement de page en cas de changement de client.
				sans cette sécurité U pourrait liste les actions du client A et les facturer au client. B */
				$client_verrou=true;
			}
		}

	}

	// **********************************
	//	AUTRE DONNEE
	//***********************************

	if(empty($erreur_txt)){

		// sur la societe
		if($acc_agence==0){
			$req=$Conn->query("SELECT age_id,age_nom FROM Agences,Societes WHERE age_societe=soc_id AND soc_agence AND soc_id=" . $acc_societe . ";");
			$d_agences=$req->fetchAll();
			if(empty($d_agences)){
				unset($d_agences);
			}
		}


		// FINANCEUR

		$sql="SELECT csc_id,csc_libelle FROM Clients_Sous_Categories WHERE csc_categorie=4 ORDER BY csc_libelle;";
		$req=$Conn->query($sql);
		$d_opca_type=$req->fetchAll();

		if(!empty($d_facture["fac_opca_type"])){
			$d_opcas=get_clients(4,$d_facture["fac_opca_type"],0,null);
		}





		// commerciaux accéssible

		if(!isset($d_agences) OR $d_facture["fac_agence"]>0){
			$com_source=get_commerciaux($acc_societe,$d_facture["fac_agence"],0);
		}

		// CATEGORIE DE PRODUIT POUVANT ÊTRE AJOUTE
		$sql="SELECT pca_id,pca_libelle FROM Produits_Categories WHERE NOT pca_planning";
		if(!$_SESSION["acces"]["acc_droits"][9]){
			$sql.=" AND NOT pca_compta";
		}
		$sql.=" ORDER BY pca_libelle;";
		$req=$Conn->query($sql);
		$result_cat=$req->fetchAll();
		if(!empty($result_cat)){
			$d_pro_categories=array();
			foreach($result_cat as $cat){
				$d_pro_categories[$cat["pca_id"]]=$cat["pca_libelle"];
			}
		}

		// LES PRODUITS DE CHAQUES CATEGORIES pour ini ligne

		if(!empty($facture_id)){

			if(!empty($d_facture["fac_cli_suc"])){
				$client_produit=$d_facture["fac_cli_suc"];
			}else{
				$client_produit=$d_facture["fac_client"];
			}
			$maison_mere=$client_produit;

			$sql="SELECT cli_categorie,cli_groupe,cli_filiale_de,cca_gc FROM Clients,Clients_Categories WHERE cli_categorie=cca_id AND cli_id=" . $maison_mere . ";";
			$req=$Conn->query($sql);
			$d_client_produit=$req->fetch();
			if(empty($d_client_produit)){
				echo("Erreur paramètre!");
				die();
			}else{
				if($d_client_produit["cli_groupe"] AND $d_client_produit["cli_filiale_de"]>0){
					$maison_mere=$d_client_produit["cli_filiale_de"];
				}
			}

			$sql="SELECT pro_id,pro_code_produit,pro_libelle,pro_categorie
			FROM Produits LEFT JOIN Clients_Produits ON (Produits.pro_id=Clients_Produits.cpr_produit AND cpr_client=:maison_mere)
			LEFT JOIN Produits_Categories ON (Produits.pro_categorie=Produits_Categories.pca_id)";
			$sql.=" WHERE NOT pro_archive AND NOT pca_planning";
			if(!$_SESSION["acces"]["acc_droits"][9]){
				$sql.=" AND NOT pca_compta";
			}
			$sql.=" AND (pro_intra OR cpr_intra)";
			if($d_client_produit["cli_categorie"]==2 AND !$_SESSION['acces']["acc_droits"][8]){
				$sql.=" AND cpr_valide=1";
			}
			if(!$_SESSION["acces"]["acc_droits"][9]){
				$sql.=" AND NOT pca_compta";
			}
			$sql.=" ORDER BY pro_code_produit";
			//echo($sql);
			$req = $Conn->prepare($sql);
			if(!empty($param)){
				$req->bindParam(":param",$param);
			}
			$req->bindParam(":maison_mere",$maison_mere);
			$req->execute();
			$d_produits = $req->fetchAll();
		}

		// LES UTILISATEURS
		$orion_utilisateurs=orion_utilisateurs();

		// TYPE D'INFO
		$infos_types=array();
		$sql="SELECT * FROM Clients_Infos ORDER BY cin_id";
		$req=$Conn->prepare($sql);
		$req->execute();
		$results = $req->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$infos_types[$r["cin_id"]]=$r["cin_libelle"];
			}
		}

		// INFO DE FACTURATION
		$client_info=get_client_infos($d_facture["fac_client"],2,null,null,null);

	}



    ?>

	<!DOCTYPE html>
	<html lang="fr" >
		<head>
			<meta charset="utf-8">
			<title>SI2P - Orion</title>
			<meta name="keywords" content=""/>
			<meta name="description" content="">
			<meta name="author" content="">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<!-- CSS THEME -->
			<link href="assets/skin/si2p/css/theme.css" rel="stylesheet" type="text/css" >
			<link href="assets/admin-tools/admin-forms/css/admin-forms.css" rel="stylesheet" type="text/css">


			<!-- CSS PLUGINS -->
			<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
			<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
			<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >

			<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />

			<!-- CSS Si2P -->
			<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

			<!-- Favicon -->
			<link rel="shortcut icon" href="assets/img/favicon.png">

			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
			<![endif]-->



		</head>
		<body class="sb-top sb-top-sm ">

			<form method="post" action="facture_enr.php" id="form_facture" >
				<div>
					<input type="hidden" id="id_heracles" name="id_heracles" value="0" />
					<input type="hidden" id="facture" name="facture" value="<?=$facture_id?>" />
					<input type="hidden" id="avoir" name="avoir" value="<?=$avoir_id?>" />
                    <input type="hidden" id="action_client_id" name="action_client_id" value="<?=$action_client_id?>" />
				</div>
				<div id="main">
		<?php		include "includes/header_def.inc.php"; ?>
					<section id="content_wrapper">

						<section id="content" class="animated">

				<?php		if(!empty($erreur_txt)){
								echo("<p class='alert alert-danger' >" . $erreur_txt . "</p>");
							}else{ ?>

								<div class="row" >

									<div class="col-md-12">

										<div class="admin-form theme-primary ">

											<div class="panel heading-border panel-primary">

												<div class="panel-body bg-light">

													<div class="content-header mtn mbn">
												<?php	if($facture_id==0){
															if(!empty($avoir_id)){
																echo("<h2>Nouvel <b class='text-primary' >avoir</b></h2>");
															}else{
																echo("<h2>Nouvelle <b class='text-primary' >facture</b></h2>");
															}
														}else{
															echo("<h2>Modification <b class='text-primary' >" . $d_facture["fac_numero"] . "</b></h2>");
														}?>
													</div>

													<!-- AFFICHAGE FACTURE -->

													<div class="row" >

														<!-- COLONNE DE GAUCHE -->
														<div class="col-md-4" >



												<?php		if(!$client_verrou){
																// si client verrouillé -> l'agence l'est aussi

																if(isset($d_agences)){
																	if(!empty($d_agences)){ ?>
																		<div class="row" >
																			<div class="col-md-12" >
																				<div class="select2-sm" >
																					<label for="agence" >Agence :</label>
																					<select name="fac_agence" id="agence" class="select2 select2-com-agence client-n-agence get-client-agence" data-com_archive="0" required >
																						<option value="0" >Sélectionnez une agence</option>
																				<?php	foreach($d_agences as $a){
																							if($a["age_id"]==$d_facture["fac_agence"]){
																								echo("<option value='" . $a["age_id"] . "' selected >" . $a["age_nom"] . "</option>");
																							}else{
																								echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																							}
																						}?>
																					</select>
																				</div>
																				<p class="text-danger" id="text_agence" <?php if(!isset($d_agences) OR !empty($d_facture["fac_agence"])) echo("style='display:none;'"); ?> >
																					Merci de selectionner une agence pour accéder à la liste des clients.
																				</p>
																			</div>
																		</div>
													<?php			}
																}else{ ?>
																	<div>
																		<input type="hidden" name="fac_agence" id="agence" class="client-n-agence get-client-agence" value="<?=$d_facture["fac_agence"]?>" />
																	</div>
													<?php		} ?>
																<div class="row mt5" >
																	<div class="col-md-12" >
																		<div class="select2-sm" >
																			<label for="client" >Client :</label>
																			<select id="client" name="fac_client" required class="select2-client-n get-client" <?php if(isset($d_agences) AND empty($d_facture["fac_agence"])) echo("disabled"); ?> data-acces="1" data-adr_type="92" data-archive="0" data-blackliste="0" >
																		<?php	if(!empty($d_client)){ ?>
																					<option value='<?=$d_client["cli_id"]?>' selected='selected' ><?=$d_client["cli_nom"]?> (<?=$d_client["cli_code"]?>)</option>
																		<?php	}else{ ?>
																					<option value='0' selected='selected' >Client ...</option>
																		<?php	} ?>
																			</select>
																			<small class="text-danger texte_required" id="client_required">
																				Merci de selectionner un client
																			</small>
																			<input type="hidden" name="fac_cli_suc" value="<?=$d_facture["fac_cli_suc"]?>" />
																		</div>
																	</div>
																</div>
												<?php		}else{ ?>
																<div class="row mt5" >
																	<div class="col-md-12" >
																		Client : <?=$d_client["cli_nom"]?> (<?=$d_client["cli_code"]?>)
																		<input type="hidden" name="fac_cli_suc" value="<?=$d_facture["fac_cli_suc"]?>" />
																		<input type="hidden" id="client" name="fac_client" value="<?=$d_facture["fac_client"]?>" />
																		<input type="hidden" id="agence" name="fac_agence" value="<?=$d_facture["fac_agence"]?>" />
																	</div>
																</div>
												<?php		} ?>

															<div class="row mt5" >
																<div class="col-md-12 section-divider" >
																	<span>Adresse</span>
																</div>
															</div>


															<div class="row mt15" >

																<!-- ADRESSE GAUCHE -->
																<div class="col-md-6" >

																	<div class="row" >
																		<div class="col-md-6 text-center" >
																			<div class="radio-custom mb5">
																				<input id="ad_client" type="radio" name="ad_opca" value="0" <?php if(empty($d_facture["fac_opca"])) echo("checked"); ?> />
																				<label for="ad_client">Facturation client</label>
																			</div>
																		</div>
																		<div class="col-md-6 text-center" >
																			<div class="radio-custom mb5">
																				<input id="ad_opca" type="radio" name="ad_opca" value="1" <?php if(!empty($d_facture["fac_opca"])) echo("checked"); ?> >
																				<label for="ad_opca">Facturation financeur</label>
																			</div>
																		</div>
																	</div>

																	<div class="row mt15 bloc-opca" <?php if(empty($d_facture["fac_opca"])) echo("style='display:none'"); ?> >
																		<div class="col-md-12" >
																			<label for="opca_type" >Type de financeur :</label>
																			<select id="opca_type" name="fac_opca_type" class="select2" >
																				<option value='0' selected='selected' >Type de financeur ...</option>
																		<?php	if(!empty($d_opca_type)){
																					foreach($d_opca_type as $opca_type){
																						if($opca_type["csc_id"]==$d_facture["fac_opca_type"]){
																							echo("<option value='" . $opca_type["csc_id"] . "' selected >" . $opca_type["csc_libelle"] . "</option>");
																						}else{
																							echo("<option value='" . $opca_type["csc_id"] . "' >" . $opca_type["csc_libelle"] . "</option>");
																						}
																					}
																				} ?>
																			</select>
																		</div>
																	</div>

																	<div class="row mt15 bloc-opca" <?php if(empty($d_facture["fac_opca"])) echo("style='display:none'"); ?> >
																		<div class="col-md-12" >
																			<label for="opca" >Financeur :</label>
																			<select id="opca" name="fac_opca" class="select2" >
																				<option value='0' selected='selected' >Financeur ...</option>
																		<?php	if(!empty($d_opcas)){
																					foreach($d_opcas as $opca){
																						if($opca["id"]==$d_facture["fac_opca"]){
																							echo("<option value='" . $opca["id"] . "' selected >" . $opca["text"] . "</option>");
																						}else{
																							echo("<option value='" . $opca["id"] . "' >" . $opca["text"] . "</option>");
																						}
																					}
																				} ?>
																			</select>
																		</div>
																	</div>

																	<div class="row mt15" >
																		<div class="col-md-12" >
																			<label for="adresse" >Adresse de facturation</label>
																			<div class="select2-sm" >
																				<select name="fac_adresse" id="adresse" class="select2" required >
																					<option value="0" >Selectionnez une adresse</option>
																		<?php		if(!empty($adresses)){
																						foreach($adresses as $ad){
																							if($d_facture["fac_adresse"]==$ad["id"]){
																								echo("<option value='" . $ad["id"] . "' selected >" . $ad["text"] . "</option>");
																							}else{
																								echo("<option value='" . $ad["id"] . "' >" .  $ad["text"] . "</option>");
																							}
																						}
																					}?>
																				</select>
																				<small class="text-danger texte_required" id="adresse_required">
																					Merci de selectionner une adresse de facturation
																				</small>
																			</div>
																		</div>
																	</div>
																	<div class="row mt15 bloc-adr-fac" <?php if(empty($d_facture["fac_adresse"])) echo("style='display:none;'"); ?> >
																		<div class="col-md-12" >
																			<p>
																				<i>Adresse de facturation</i><br/>
																				<div id="cont_adr_fac" >
																		<?php		echo($d_facture["fac_cli_nom"]);
																					if(!empty($d_facture["fac_cli_service"])){
																						echo("<br/>" . $d_facture["fac_cli_service"]);
																					}
																					if(!empty($d_facture["fac_cli_ad1"])){
																						echo("<br/>" . $d_facture["fac_cli_ad1"]);
																					}
																					if(!empty($d_facture["fac_cli_ad2"])){
																						echo("<br/>" . $d_facture["fac_cli_ad2"]);
																					}
																					if(!empty($d_facture["fac_cli_ad3"])){
																						echo("<br/>" . $d_facture["fac_cli_ad3"]);
																					}
																					echo("<br/>" . $d_facture["fac_cli_cp"] . " " . $d_facture["fac_cli_ville"]); ?>
																				</div>
																			</p>
																		</div>
																	</div>

																</div>
																<!-- ADRESSE DROITE -->
																<div class="col-md-6" >

																	<div class="row" >
																		<div class="col-md-12" >
																			<div class="option-group field">
																				<label class="option option-dark">
																					<input type="checkbox" id="envoi_ad" name="fac_envoi" value="on" <?php if(!empty($d_facture["fac_env_adresse"])) echo("checked"); ?>>
																					<span class="checkbox"></span>Utiliser une autre adresse pour l'envoi de facture
																				</label>
																			</div>
																		</div>
																	</div>

																	<div class="row mt15 bloc-env-adresse" <?php if(empty($d_facture["fac_env_adresse"])) echo("style='display:none'"); ?> >
																		<div class="col-md-12" >
																			<label for="env_adresse" >Adresse d'envoi</label>
																			<div class="select2-sm" >
																				<select name="fac_env_adresse" id="env_adresse" class="select2" >
																					<option value="0" >Selectionnez une adresse</option>
																		<?php		if(!empty($d_ad_envoi)){
																						foreach($d_ad_envoi as $ad_envoi){
																							if(!empty($ad_envoi["adr_libelle"])){
																								$ad_libelle=$ad_envoi["adr_libelle"];
																							}else{
																								$ad_libelle=$ad_envoi["adr_nom"] . " " . $ad_envoi["adr_cp"] . " " . $ad_envoi["adr_ville"];
																							}
																							if($ad_envoi["adr_id"]==$d_facture["fac_env_adresse"]){
																								echo("<option value='" . $ad_envoi["adr_id"] . "' selected >" . $ad_libelle . "</option>");
																							}else{
																								echo("<option value='" . $ad_envoi["adr_id"] . "' >" . $ad_libelle . "</option>");
																							}
																						}
																					} ?>
																				</select>
																				<small class="text-danger texte_required" id="env_adresse_required">
																					Merci de selectionner une adresse d'envoi de facture
																				</small>
																			</div>
																		</div>
																	</div>

																	<div class="row mt15 bloc-env-adr-aff" <?php if(empty($d_facture["fac_env_adresse"])) echo("style='display:none'"); ?> >
																		<div class="col-md-12" >
																			<p>
																				<i>Adresse d'envoi</i><br/>
																				<div id="cont_env_adresse" >
																					<b><?=$d_facture["fac_env_nom"]?></b></br>
																			<?php	if(!empty($d_facture["fac_env_service"])){
																						echo($d_facture["fac_env_service"] . "<br/>");
																					}
																					if(!empty($d_facture["fac_env_ad1"])){
																						echo($d_facture["fac_env_ad1"] . "<br/>");
																					}
																					if(!empty($d_facture["fac_env_ad2"])){
																						echo($d_facture["fac_env_ad2"] . "<br/>");
																					}
																					if(!empty($d_facture["fac_env_ad3"])){
																						echo($d_facture["fac_env_ad3"] . "<br/>");
																					}
																					echo($d_facture["fac_env_cp"] . " " . $d_facture["fac_env_ville"]); ?>

																				</div>
																			</p>
																		</div>
																	</div>

																</div>
															</div>
														</div>
														<!-- FIN COLONNE DE GAUCHE -->

														<!-- COLONNE DU MILIEU -->
														<div class="col-md-4" >

															<div class="row mt5" >
																<div class="col-md-4 mt5 text-right" >Commercial :</div>
																<div class="col-md-8" >
																	<div class="select2-sm" >
																		<select class="select2 select2-commercial actu-client-select2-com" id="commercial" name="fac_commercial" required >
																			<option value="" >&nbsp;</option>
																	<?php	if(!empty($com_source)){
																				foreach($com_source as $com){
																					if($com["com_id"]==$d_facture["fac_commercial"]){
																						echo("<option value='" . $com["com_id"] . "' selected >" . $com["com_label_1"] . " " . $com["com_label_2"] . "</option>");
																					}else{
																						echo("<option value='" . $com["com_id"] . "' >" . $com["com_label_1"] . " " . $com["com_label_2"] . "</option>");
																					}
																				};
																			}; ?>
																		</select>
																		<small class="text-danger texte_required" id="commercial_required">
																			Merci de selectionner un commercial
																		</small>

																	</div>
																</div>
															</div>

															<div class="row mt5" >
																<div class="col-md-4 mt10 text-right" >Référence :</div>
																<div class="col-md-8" >
																	<input type="text" name="fac_reference" class="gui-input gui-input-sm" placeholder="Référence" value="<?=$d_facture["fac_reference"]?>" />
																</div>
															</div>

															<div class="row mt5 mb15" >
																<div class="col-md-4 mt10 text-right" >Date :</div>
																<div class="col-md-8" >
																	<span class="field prepend-icon prepend-icon-sm">
																		<input type="text" size="10" name="fac_date" id="date_facture" class="gui-input datepicker" placeholder="Selectionner une date" value="<?=$DTime_fac_date->format("d/m/Y")?>" />
																		<label class="field-icon" for="date_facture" ><i class="fa fa-calendar-o"></i></label>
																	</span>
																</div>
															</div>
															<div class="row mt5 mb15" >
																<div class="col-md-4 mt10 text-right" >Facture valant Convention :</div>
																<div class="col-md-8 pt5" >
																	<div class="option-group field">
																		<label class="option option-dark">
																			<input type="checkbox" name="fac_convention" value="on" <?php if(!empty($d_facture["fac_convention"])) echo("checked"); ?>>
																			<span class="checkbox"></span>
																		</label>
																	</div>
																</div>
															</div>

															<div class="section-divider mbn" >
																<span>Modalité de règlement</span>
															</div>
															<div class="row mt15" >
																<div class="col-md-12">
																	<label for="reg_type" >Paiement :</label>
																	<select name="fac_reg_type" id="reg_type" class="select2" >
															<?php		foreach($base_reglement as $reg_type => $reg_lib){
																			if($reg_type!=0){
																				if($reg_type==$d_facture["fac_reg_type"]){
																					echo("<option value='" . $reg_type . "' selected >" . $reg_lib . "</option>");
																				}else{
																					echo("<option value='" . $reg_type . "' >" . $reg_lib . "</option>");
																				}
																			}
																		} ?>
																	</select>
																</div>
															</div>
															<div class="row mt15" >
																<div class="col-md-12">
															<?php	if($d_facture["fac_reg_formule"]==1){ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_1" class="reg-formule" name="reg_formule" value="1" <?php if($d_facture["fac_reg_formule"]==1) echo("checked"); ?> >
																			<span class="radio"></span>

																		</label>
																		Date + <input type="number" max="60" size="2" id="reg_nb_jour_1" name="reg_nb_jour_1" value="<?=$d_facture["fac_reg_nb_jour"]?>" class="champ-reg-formule reg-formule-1" /> jours
															<?php	}else{ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_1" class="reg-formule" name="reg_formule" value="1" />
																			<span class="radio"></span>
																		</label>
																		Date + <input type="number" max="60" size="2" id="reg_nb_jour_1" name="reg_nb_jour_1" value="0" class="champ-reg-formule reg-formule-1" disabled /> jours
															<?php	} ?>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
																	<label class="option">
																		<input type="radio" id="reg_formule_2" class="reg-formule" name="reg_formule" value="2" <?php if($d_facture["fac_reg_formule"]==2) echo("checked"); ?> >
																		<span class="radio"></span>
																	</label>
																	Date + 45j + fin de mois
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
																	<label class="option">
																		<input type="radio" id="reg_formule_3" class="reg-formule" name="reg_formule" value="3" <?php if($d_facture["fac_reg_formule"]==3) echo("checked"); ?> >
																		<span class="radio"></span>
																	</label>
																	Date + Fin de mois + 45j
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
															<?php	if($d_facture["fac_reg_formule"]==4){ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_4" class="reg-formule" name="reg_formule" value="4" checked />
																			<span class="radio"></span>
																		</label>
																		Date + <input type="number" max="30" size="2" id="reg_nb_jour_4" name="reg_nb_jour_4" value="<?=$d_facture["fac_reg_nb_jour"]?>" class="champ-reg-formule reg-formule-4" /> jours
																		+ Fin de mois +  <input type="number" size="2" max="20" id="reg_fdm_4" name="reg_fdm_4" value="<?=$d_facture["fac_reg_fdm"]?>" class="champ-reg-formule reg-formule-4" /> jours
															<?php	}else{ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_4" class="reg-formule" name="reg_formule" value="4" />
																			<span class="radio"></span>
																		</label>
																		Date + <input type="number" max="30" size="2" id="reg_nb_jour_4" name="reg_nb_jour_4" value="0" disabled class="champ-reg-formule reg-formule-4" /> jours
																		+ Fin de mois +  <input type="number" size="2" max="20" id="reg_fdm_4" name="reg_fdm_4" value="0" disabled class="champ-reg-formule reg-formule-4" /> jours
															<?php	} ?>
																</div>
															</div>

															<!-- CONTACT -->
															<div class="row mt5" >
																<div class="col-md-12 section-divider" >
																	<span>Contact</span>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12" >
																	<div class="select2-sm" >
																		<select name="fac_contact" id="contact" class="select2 actu-contacts aff-contact" >
																			<option value="0" >Selectionnez un contact client</option>
																<?php		if(!empty($d_contacts)){
																				foreach($d_contacts as $c){
																					if(is_null($d_facture["fac_contact"])){
																						// contact n'a pas ete initialisé

																						if(!empty($c["aco_defaut"])){
																							$d_facture["fac_contact"]=$c["con_id"];
																							$d_facture["fac_con_titre"]=$c["con_titre"];
																							$d_facture["fac_con_nom"]=$c["con_nom"];
																							$d_facture["fac_con_prenom"]=$c["con_prenom"];
																							$d_facture["fac_con_tel"]=$c["con_tel"];
																							$d_facture["fac_con_portable"]=$c["con_portable"];
																							$d_facture["fac_con_mail"]=$c["con_mail"];
																						}

																					}

																					if($d_facture["fac_contact"]==$c["con_id"]){
																						echo("<option value='" . $c["con_id"] . "' selected >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
																					}else{
																						echo("<option value='" . $c["con_id"] . "' >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
																					}
																				}
																			}?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-6" >
																	<div class="select2-sm" >
																		<select name="fac_con_titre" id="con_titre" class="select2" >
																			<option value="0" >Civilité ...</option>
																			<option value="1" <?php if($d_facture["fac_con_titre"]==1) echo("selected") ?> >M.</option>
																			<option value="2" <?php if($d_facture["fac_con_titre"]==2) echo("selected") ?> >Mme.</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-6" >
																	<input type="text" id="con_nom" name="fac_con_nom" class="gui-input gui-input-sm nom champ-contact" placeholder="Nom" value="<?=$d_facture["fac_con_nom"]?>" />
																</div>
																<div class="col-md-6" >
																	<input type="text" id="con_prenom" name="fac_con_prenom"  class="gui-input gui-input-sm prenom champ-contact" placeholder="Prénom" value="<?=$d_facture["fac_con_prenom"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-6" >
																	<input type="text" id="con_tel" name="fac_con_tel"  class="gui-input gui-input-sm telephone champ-contact" placeholder="Téléphone" value="<?=$d_facture["fac_con_tel"]?>" />
																</div>
																<div class="col-md-6" >
																	<input type="text" id="con_portable" name="fac_con_portable"  class="gui-input gui-input-sm telephone champ-contact" placeholder="Portable" value="<?=$d_facture["fac_con_portable"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-6" >
																	<input type="text" id="con_mail" name="fac_con_mail"  class="gui-input gui-input-sm champ-contact" placeholder="Mail" value="<?=$d_facture["fac_con_mail"]?>" />
																</div>
																<div class="col-md-6" >
																	<div class="option-group field" id="bloc_con_add" <?php if(!empty($d_facture["fac_contact"])) echo("style='display:none;'"); ?> >
																		<label class="option option-dark">
																			<input type="checkbox" name="con_add" value="1">
																			<span class="checkbox"></span>Enregistrer comme nouveau contact
																		</label>
																	</div>
																	<div class="option-group field" id="bloc_con_maj" <?php if(empty($d_facture["fac_contact"])) echo("style='display:none;'"); ?> >
																		<label class="option option-dark">
																			<input type="checkbox" name="con_maj" value="1" >
																			<span class="checkbox"></span>Mettre à jour le contact
																		</label>
																	</div>
																</div>
															</div>

														</div>
														<!-- FIN COLONNE DE GAUCHE -->

														<!-- COLONNE DE DROITE INFO DE FACTURATION -->
														<div class="col-md-4" >
													<?php	if(!empty($client_info)){ ?>
																<div class="panel panel-primary">
																	<div class="panel-heading panel-head-sm" style="padding:5px 15px;" >
																		<span class="panel-title" style="color:#fff;" >Infos</span>
																	</div>
																	<div class="panel-body">
															<?php		foreach($client_info as $i){
																			$date_info="";
																			if(!empty($i["inf_date"])){
																				$DT_periode=date_create_from_format('Y-m-d',$i["inf_date"]);
																				if(!is_bool($DT_periode)){
																					$date_info=$DT_periode->format("d/m/Y");
																				}
																			}


																			echo("<div class='mb25' >");
																				echo("<p>");
																					echo("<b>" . $infos_types[$i["inf_type"]] . "</b><br/>");
																					echo($i["inf_description"] . "<br/>");
																					echo($orion_utilisateurs[$i["inf_auteur"]]["identite"] . " le " . $date_info);
																				echo("</p>");
																			echo("</div>");
																		} ?>
																	</div>
																</div>
												<?php		} ?>
														</div>
													</div>

													<div class="table-responsive mt10" >
														<table class="table" id="ligne_fac" >
															<thead>
																<tr class="dark" >
																	<th>&nbsp;</th>
																	<th class="text-center" >Référence</th>
																	<th class="text-center" >Désignation</th>
																	<th class="text-center" >Reste à facturer</th>
																	<th class="text-center" >Prix HT</th>
																	<th class="text-center" >Qté</th>
																	<th class="text-center" >Montant HT</th>
															<?php	$cols=6;
																	if($_SESSION['acces']["acc_droits"][9]){
																		$cols=7; ?>
																		<th id="gest_tva" class="text-center" >TVA</th>
															<?php	} ?>
																</tr>
															</thead>
															<tbody>
												<?php			$num_l=null;

																// LIGNE DEJA ENREGISTREE
																if(!empty($d_lignes)){
																	foreach($d_lignes as $num_l => $l){

																		$reste=0;
																		if(!empty($l["fli_action_client"])){

																			$acl_ca=0;
																			$sql_act_cli_get="SELECT acl_ca,acl_ca_gfc FROM Actions_Clients WHERE acl_id=:fli_action_client;";
																			$ConnFct=connexion_fct($l["fli_action_cli_soc"]);
																			$req_act_cli_get=$ConnFct->prepare($sql_act_cli_get);
																			$req_act_cli_get->bindParam(":fli_action_client",$l["fli_action_client"]);
																			$req_act_cli_get->execute();
																			$d_action_client=$req_act_cli_get->fetch();
																			if(!empty($d_action_client)){
																				if($l["fli_action_cli_soc"]!=$acc_societe){
																					$acl_ca=$d_action_client["acl_ca_gfc"];
																				}else{
																					$acl_ca=$d_action_client["acl_ca"];
																				}
																			}

																			$acl_facture_ht=0;
																			$sql_ca_get="SELECT SUM(fli_montant_ht) FROM Factures_Lignes WHERE fli_action_client=:fli_action_client AND fli_action_cli_soc=:fli_action_cli_soc
																			AND NOT fli_id=:fli_id;";
																			$req_ca_get=$ConnSoc->prepare($sql_ca_get);
																			$req_ca_get->bindParam(":fli_action_client",$l["fli_action_client"]);
																			$req_ca_get->bindParam(":fli_action_cli_soc",$l["fli_action_cli_soc"]);
																			$req_ca_get->bindParam(":fli_id",$l["fli_id"]);
																			$req_ca_get->execute();
																			$d_action_ca=$req_ca_get->fetch();
																			if(!empty($d_action_ca)){
																				$acl_facture_ht=$d_action_ca[0];
																			}
																			if($d_facture["fac_nature"]==1){
																				$reste=$acl_ca-$acl_facture_ht;
																			}else{
																				$reste=-$acl_facture_ht;
																			}

																		} ?>
																		<tr class="ligne-fac" data-num_ligne="<?=$num_l?>" >
																			<td>
																				<div class="option-group field">
																					<label class="option option-dark">
																						<input type="checkbox" name="choix_<?=$num_l?>" id="choix_<?=$num_l?>" class="check-ligne" value="on" checked >
																						<span class="checkbox"></span>
																					</label>
																				</div>
																				<input type="hidden" name="num_ligne[]" value="<?=$num_l?>" />
																				<input type="hidden" name="fli_id_<?=$num_l?>" value="<?=$l["fli_id"]?>" />
																				<input type="hidden" name="fli_action_client_<?=$num_l?>" value="<?=$l["fli_action_client"]?>" />
																				<input type="hidden" name="fli_action_cli_soc_<?=$num_l?>" value="<?=$l["fli_action_cli_soc"]?>" />
																				<input type="hidden" name="fli_intra_inter_<?=$num_l?>" value="<?=$l["fli_intra_inter"]?>" />
																			</td>
																			<td>

																		<?php	if(isset($d_pro_categories[$l["fli_categorie"]])){ ?>
																					<div>
																						<select class="select2 categorie" id="categorie_<?=$num_l?>" data-num_ligne="<?=$num_l?>" >
																							<option value='0' >Catégorie</option>";
																					<?php	foreach($d_pro_categories as $cat => $cat_lib){
																								if($cat==$l["fli_categorie"]){ ?>
																									<option value="<?=$cat?>" selected ><?=$cat_lib?></option>
																					<?php		}else{ ?>
																									<option value="<?=$cat?>" ><?=$cat_lib?></option>
																					<?php		}
																							} ?>
																						</select>
																					</div>
																					<div>
																						<select class='select2 produit' id='produit_<?=$num_l?>' name='fli_produit_<?=$num_l?>' data-num_ligne='<?=$num_l?>' >
																							<option value='0' >Produit</option>
																					<?php	foreach($d_produits as $p){
																								if($p["pro_categorie"]==$l["fli_categorie"]){
																									if($p["pro_id"]==$l["fli_produit"]){?>
																										<option value="<?=$p["pro_id"]?>" selected ><?=$p["pro_code_produit"] . " - " . $p["pro_libelle"]?></option>
																						<?php		}else{ ?>
																										<option value="<?=$p["pro_id"]?>" ><?=$p["pro_code_produit"] . " - " . $p["pro_libelle"]?></option>
																						<?php		}
																								}
																							} ?>
																						</select>
																					</div>

																		<?php	}else{ ?>
																					<?=$l["fli_code_produit"]?>
																					<input type="hidden" name="fli_produit_<?=$num_l?>" value="<?=$l["fli_produit"]?>" />
																		<?php	} ?>


																			</td>
																			<td>
																				<label for="libelle_<?=$num_l?>" >Libelle :</label>
																				<input type="text" name="fli_libelle_<?=$num_l?>" class="gui-input" id="libelle_<?=$num_l?>" placeholder="Désignation" value="<?=$l["fli_libelle"]?>" />

																				<div>
																					<div class="pull-right" >
																						<button type="button" class="btn btn-sm btn-warning btn-designation" data-num_ligne="<?=$num_l?>" >
																							<i class="fa fa-pencil" ></i>
																						</button>
																					</div>
																					<div id="text_formation_<?=$num_l?>" >
																			<?php		if(!empty($l["fli_formation"])){
																							echo($l["fli_formation"]);
																						} ?>
																					</div>
																					<div id="text_complement_<?=$num_l?>" >
																			<?php		if(!empty($l["fli_complement"])){
																							echo($l["fli_complement"]);
																						} ?>
																					</div>
																					<div id="text_stagiaire_<?=$num_l?>" >
																			<?php		if(!empty($l["fli_stagiaire"])){
																							echo($l["fli_stagiaire"]);
																						} ?>
																					</div>
																				</div>
																				<input type="hidden" name="fli_formation_<?=$num_l?>" id="formation_<?=$num_l?>" rows="2" value="<?=$l["fli_formation"]?>" />
																				<input type="hidden" name="fli_complement_<?=$num_l?>" id="complement_<?=$num_l?>" rows="2" value="<?=$l["fli_complement"]?>" />
																				<input type="hidden" name="fli_stagiaire_<?=$num_l?>" id="stagiaire_<?=$num_l?>" rows="2" value="<?=$l["fli_stagiaire"]?>" />
																	<?php		if(!empty($l["fli_action_client"])){ ?>
																					<div class="row mt5" >
																						<div class="col-md-6" >
																							<label for="opca_num_<?=$num_l?>" >Numéro de prise en charge OPCO :</label>

																							<input type="text" name="fli_opca_num_<?=$num_l?>" class="gui-input" id="opca_num_<?=$num_l?>" placeholder="Numéro OPCO" value="<?=$l["fli_opca_num"]?>" />
																						</div>
																						<div class="col-md-6" >
																							<label for="bc_num_<?=$num_l?>" >Numéro de bon de commande :</label>
																							<input type="text" name="fli_bc_num_<?=$num_l?>" class="gui-input" id="bc_num_<?=$num_l?>" placeholder="Numéro de BC" value="<?=$l["fli_bc_num"]?>" />
																						</div>
																					</div>
																	<?php		} ?>
																			</td>
																			<td class="text-center" >
																		<?php	if(!empty($reste)){  ?>
																					<input type="hidden" id="reste_<?=$num_l?>" name="fli_reste_<?=$num_l?>" value="<?=$reste?>" />
																					<?=number_format($reste,2,","," ")?> €
																		<?php	}else{
																					echo("&nbsp;");
																				}?>
																			</td>
																			<td>
																				<input type="text" name="fli_ht_<?=$num_l?>" id="ht_<?=$num_l?>" class="gui-input calcul calcul-ht" data-num_ligne="<?=$num_l?>" placeholder="HT" size="7" value="<?=$l["fli_ht"]?>" />
																				<input type="hidden" id="ht_min_<?=$num_l?>" value="0" />
																			</td>
																			<td>
																				<input type="text" name="fli_qte_<?=$num_l?>" id="qte_<?=$num_l?>" class="gui-input calcul calcul-qte" data-num_ligne="<?=$num_l?>" placeholder="Qté" size="5" value="<?=$l["fli_qte"]?>" />
																			</td>
																			<td>
																				<input type="text" name="fli_montant_ht_<?=$num_l?>" id="montant_ht_<?=$num_l?>" class="gui-input input-float calcul calcul-total" data-num_ligne="<?=$num_l?>" size="9" placeholder="HT" value="<?=$l["fli_montant_ht"]?>" />
																			</td>

																	<?php	if($_SESSION['acces']["acc_droits"][9]){

                                                                        ?>
																				<td>
																					<select name="fli_tva_id_<?=$num_l?>" id="tva_id_<?=$num_l?>" class="select2" >
																			<?php		foreach($base_tva as $id_tva => $libelle_tva){
																							if($id_tva==$l["fli_tva_id"]){
																								echo("<option value='" . $id_tva . "' selected >" . $libelle_tva . "</option>");
																							}else{
																								echo("<option value='" . $id_tva . "' >" . $libelle_tva . "</option>");
																							}
																						} ?>
																					</select>
																				</td>
																	<?php	} ?>
																		</tr>
														<?php		}
																}

																// ACTION POUVANT ETRE AJOUTE
																if(!empty($d_actions)){
																	foreach($d_actions as $action){

																		$formation=$action["planning"];
																		if(!empty($action["intervenants_lib"])){
																			if(!empty($action["intervenants_lib"])){

																				$formation.="<br/><u>Formateur(s) :</u><br/>";
																			}
																			$formation.=$action["intervenants_lib"];
																		}
																		if(!empty($action["testeurs_lib"])){
																			if(!empty($action["testeurs_lib"])){

																				$formation.="<br/><u>Testeur(s) :</u><br/>";
																			}
																			$formation.=$action["testeurs_lib"];
																		}

																		if(!empty($action["lieu"])){
																			if(!empty($action["lieu"])){

																				$formation.="<br/><u>Lieu de formation :</u><br/>";
																			}
																			$formation.=$action["lieu"];
																		}

																		if($action["action_client"]==$action_client_id){
																			$d_facture["fac_total_ht"]=$d_facture["fac_total_ht"]+$action["ht"];
																		};

																		$action_complement=$action["complement"];
																		if(!empty($action_complement)){
																			$action_complement=str_replace(chr(10),"<br/>",$action_complement);
																		}

																		$intra_inter=1;
																		if(!empty($action["inter"])){
																			if(!empty($action["inter"]==1)){
																				$intra_inter=2;
																			}
																		}


																		$num_l++; 	?>
																		<tr class="ligne-fac" data-num_ligne="<?=$num_l?>" >
																			<td>
																				<div class="option-group field">
																					<label class="option option-dark">
																						<input type="checkbox" name="choix_<?=$num_l?>" id="choix_<?=$num_l?>" class="check-ligne" value="on" data-num_ligne="<?=$num_l?>" <?php if($action["action_client"]==$action_client_id OR !empty($fac_groupe)) echo("checked") ?> />
																						<span class="checkbox"></span>
																					</label>
																				</div>
																				<input type="hidden" name="num_ligne[]" value="<?=$num_l?>" />
																				<input type="hidden" name="fli_id_<?=$num_l?>" value="0" />
																				<input type="hidden" name="fli_action_client_<?=$num_l?>" value="<?=$action["action_client"]?>" />
																				<input type="hidden" name="fli_action_cli_soc_<?=$num_l?>" value="<?=$action["action_client_soc"]?>" />
																				<input type="hidden" name="fli_intra_inter_<?=$num_l?>" value="<?=$intra_inter?>" />
																			</td>
																			<td>
																				<?=$action["reference"]?>
																				<br/>
																				<?=$action["action"] . "-" . $action["action_client"]?>
																				<input type="hidden" name="fli_produit_<?=$num_l?>" value="<?=$action["produit"]?>" />
																			</td>
																			<td>
																				<label for="libelle_<?=$num_l?>" >Libelle :</label>
																				<input type="text" name="fli_libelle_<?=$num_l?>" class="gui-input" id="libelle_<?=$num_l?>" placeholder="Désignation" value="<?=$action["libelle"]?>" />

																				<div>
																					<div class="pull-right" >
																						<button type="button" class="btn btn-sm btn-warning btn-designation" data-num_ligne="<?=$num_l?>" >
																							<i class="fa fa-pencil" ></i>
																						</button>
																					</div>
																					<div id="text_formation_<?=$num_l?>" ><?=$formation?></div>
																					<div id="text_complement_<?=$num_l?>" ><?=$action_complement?></div>
																					<div id="text_stagiaire_<?=$num_l?>" ><?=$action["stagiaires"]?></div>
																				</div>
																				<input type="hidden" name="fli_formation_<?=$num_l?>" id="formation_<?=$num_l?>" rows="2" value="<?=$formation?>" />
																				<input type="hidden" name="fli_complement_<?=$num_l?>" id="complement_<?=$num_l?>" rows="2" value="<?=$action_complement?>" />
																				<input type="hidden" name="fli_stagiaire_<?=$num_l?>" id="stagiaire_<?=$num_l?>" rows="2" value="<?=$action["stagiaires"]?>" />
																				<div class="row mt5" >
																					<div class="col-md-6" >
																						<label for="opca_num_<?=$num_l?>" >Numéro de prise en charge OPCO :</label>
																						<input type="text" name="fli_opca_num_<?=$num_l?>" class="gui-input" id="opca_num_<?=$num_l?>" placeholder="Numéro OPCO" value="<?=$action["opca_num"]?>" />
																					</div>
																					<div class="col-md-6" >
																						<label for="bc_num_<?=$num_l?>" >Numéro de bon de commande :</label>
																						<input type="text" name="fli_bc_num_<?=$num_l?>" class="gui-input" id="bc_num_<?=$num_l?>" placeholder="Numéro de BC" value="<?=$action["bc_num"]?>" />
																					</div>
																				</div>

																			</td>
																			<td class="text-center" >
																				<input type="hidden" id="reste_<?=$num_l?>" name="fli_reste_<?=$num_l?>" value="<?=$action["ht"]?>" />
																				<?=number_format($action["ht"],2,","," ")?> €

																			</td>
																			<td>
																				<input type="text" name="fli_ht_<?=$num_l?>" id="ht_<?=$num_l?>" class="gui-input calcul calcul-ht" data-num_ligne="<?=$num_l?>" placeholder="HT" size="7" value="<?=$action["pu"]?>" />
																			</td>
																			<td>
																				<input type="text" name="fli_qte_<?=$num_l?>" id="qte_<?=$num_l?>" class="gui-input calcul calcul-qte" data-num_ligne="<?=$num_l?>" placeholder="Qté" size="5" value="<?=$action["qte"]?>" />
																			</td>
																			<td>
																				<input type="text" name="fli_montant_ht_<?=$num_l?>" id="montant_ht_<?=$num_l?>" class="gui-input input-float calcul calcul-total" data-num_ligne="<?=$num_l?>" size="9" placeholder="HT" value="<?=$action["ht"]?>" />
																			</td>
																	<?php	if($_SESSION['acces']["acc_droits"][9]){ ?>
																				<td>
																					<select name="fli_tva_id_<?=$num_l?>" id="tva_id_<?=$num_l?>" class="select2" >
																			<?php		foreach($base_tva as $id_tva => $libelle_tva){
                                                                                            if($id_tva == 1){
                                                                                                echo("<option selected value='" . $id_tva . "' >" . $libelle_tva . "</option>");
                                                                                            }else{
                                                                                                echo("<option value='" . $id_tva . "' >" . $libelle_tva . "</option>");
                                                                                            }
																						} ?>
																					</select>
																				</td>
																	<?php	} ?>
																		</tr>
													<?php			}
																} ?>

												<?php			// LIGNE POUVANT ETRE ASSOCIE A L'Avoir

																if(!empty($d_lignes_avoir)){

																	foreach($d_lignes_avoir as $num_l => $l){

																		$reste=0;
																		if(!empty($l["fli_action_client"])){
																			$sql_ca_get="SELECT SUM(fli_montant_ht) FROM Factures_Lignes WHERE fli_action_client=:fli_action_client AND fli_action_cli_soc=:fli_action_cli_soc;";
																			$req_ca_get=$ConnSoc->prepare($sql_ca_get);
																			$req_ca_get->bindParam(":fli_action_client",$l["fli_action_client"]);
																			$req_ca_get->bindParam(":fli_action_cli_soc",$l["fli_action_cli_soc"]);
																			$req_ca_get->execute();
																			$d_action_ca=$req_ca_get->fetch();
																			if(!empty($d_action_ca)){
																				$reste=-$d_action_ca[0];
																				$l["fli_montant_ht"]=-$d_action_ca[0];
																			}
																		}else{
																			// produit comm
																			$l["fli_montant_ht"]=-$l["fli_montant_ht"];
																		}

																		if(!empty($l["fli_qte"])){
																			$l["fli_ht"]=$l["fli_montant_ht"]/$l["fli_qte"];
																		}

																		$d_facture["fac_total_ht"]=$d_facture["fac_total_ht"]+$l["fli_montant_ht"];

																		?>

																		<tr class="ligne-fac" data-num_ligne="<?=$num_l?>" >
																			<td>
																				<div class="option-group field">
																					<label class="option option-dark">
																						<input type="checkbox" name="choix_<?=$num_l?>" id="choix_<?=$num_l?>" class="check-ligne" value="on" checked >
																						<span class="checkbox"></span>
																					</label>
																				</div>
																				<input type="hidden" name="num_ligne[]" value="<?=$num_l?>" />
																				<input type="hidden" name="fli_id_<?=$num_l?>" value="0" />
																				<input type="hidden" name="fli_action_client_<?=$num_l?>" value="<?=$l["fli_action_client"]?>" />
																				<input type="hidden" name="fli_action_cli_soc_<?=$num_l?>" value="<?=$l["fli_action_cli_soc"]?>" />
																			</td>
																			<td>
																				<?=$l["fli_code_produit"]?>
																				<input type="hidden" name="fli_produit_<?=$num_l?>" value="<?=$l["fli_produit"]?>" />
																			</td>
																			<td>
																				<label for="libelle_<?=$num_l?>" >Libelle :</label>
																				<input type="text" name="fli_libelle_<?=$num_l?>" class="gui-input" id="libelle_<?=$num_l?>" placeholder="Désignation" value="<?=$l["fli_libelle"]?>" />

																				<div>
																					<div class="pull-right" >
																						<button type="button" class="btn btn-sm btn-warning btn-designation" data-num_ligne="<?=$num_l?>" >
																							<i class="fa fa-pencil" ></i>
																						</button>
																					</div>
																					<div id="text_formation_<?=$num_l?>" >
																			<?php		if(!empty($l["fli_formation"])){
																							echo($l["fli_formation"]);
																						} ?>
																					</div>
																					<div id="text_complement_<?=$num_l?>" >
																			<?php		if(!empty($l["fli_complement"])){
																							echo($l["fli_complement"]);
																						} ?>
																					</div>
																					<div id="text_stagiaire_<?=$num_l?>" >
																			<?php		if(!empty($l["fli_stagiaire"])){
																							echo($l["fli_stagiaire"]);
																						} ?>
																					</div>
																			<?php	if(!empty($l["fli_action_client"])){ ?>
																						<div class="row mt5" >
																							<div class="col-md-6" >
																								<label for="opca_num_<?=$num_l?>" >Numéro de prise en charge OPCO :</label>
																								<input type="text" name="fli_opca_num_<?=$num_l?>" class="gui-input" id="opca_num_<?=$num_l?>" placeholder="Numéro OPCO" value="<?=$l["fli_opca_num"]?>" />
																							</div>
																							<div class="col-md-6" >
																								<label for="bc_num_<?=$num_l?>" >Numéro de bon de commande :</label>
																								<input type="text" name="fli_bc_num_<?=$num_l?>" class="gui-input" id="bc_num_<?=$num_l?>" placeholder="Numéro de BC" value="<?=$l["fli_bc_num"]?>" />
																							</div>
																						</div>
																			<?php	} ?>
																				</div>
																				<input type="hidden" name="fli_formation_<?=$num_l?>" id="formation_<?=$num_l?>" rows="2" value="<?=$l["fli_formation"]?>" />
																				<input type="hidden" name="fli_complement_<?=$num_l?>" id="complement_<?=$num_l?>" rows="2" value="<?=$l["fli_complement"]?>" />
																				<input type="hidden" name="fli_stagiaire_<?=$num_l?>" id="stagiaire_<?=$num_l?>" rows="2" value="<?=$l["fli_stagiaire"]?>" />
																			</td>
																			<td class="text-center" >
																				<input type="hidden" id="reste_<?=$num_l?>" name="fli_reste_<?=$num_l?>" value="<?=$l["fli_montant_ht"]?>" />
																				<?=number_format($l["fli_montant_ht"],2,","," ")?> €
																			</td>
																			<td>
																				<input type="text" name="fli_ht_<?=$num_l?>" id="ht_<?=$num_l?>" class="gui-input calcul calcul-ht" data-num_ligne="<?=$num_l?>" placeholder="HT" size="7" value="<?=$l["fli_ht"]?>" />
																				<input type="hidden" id="ht_min_<?=$num_l?>" value="0" />
																			</td>
																			<td>
																				<input type="text" name="fli_qte_<?=$num_l?>" id="qte_<?=$num_l?>" class="gui-input calcul calcul-qte" data-num_ligne="<?=$num_l?>" placeholder="Qté" size="5" value="<?=$l["fli_qte"]?>" />
																			</td>
																			<td>
																				<input type="text" name="fli_montant_ht_<?=$num_l?>" id="montant_ht_<?=$num_l?>" class="gui-input input-float calcul calcul-total" data-num_ligne="<?=$num_l?>" size="9" placeholder="HT" value="<?=$l["fli_montant_ht"]?>" />
																			</td>
																	<?php	if($_SESSION['acces']["acc_droits"][9]){ ?>
																				<td>
																					<select name="fli_tva_id_<?=$num_l?>" id="tva_id_<?=$num_l?>" class="select2" >
																			<?php		foreach($base_tva as $id_tva => $libelle_tva){
																							if($id_tva==$l["fli_tva_id"]){
																								echo("<option value='" . $id_tva . "' selected >" . $libelle_tva . "</option>");
																							}else{
																								echo("<option value='" . $id_tva . "' >" . $libelle_tva . "</option>");
																							}
																						} ?>
																					</select>
																				</td>
																	<?php	} ?>
																		</tr>
													<?php			}
																} ?>
															</tbody>
															<tfoot>
																<tr>
																	<td colspan="<?=$cols?>" >
																		<input type="text" name="fac_libelle" class="gui-input" placeholder="Info complémentaire" value="<?=$d_facture["fac_libelle"]?>" />
																	</td>
																	<td class="text-right" >
																		<input type="text" id="montant_total" class="text-right" readonly size="9" maxlength="9" value="<?=$d_facture["fac_total_ht"]?>" />&nbsp;€
																	</td>
																</tr>
															</tfoot>
														</table>
													</div>

													<!-- FIN AFFICHAGE DE LA FACTURE -->

												</div>
											</div>
										</div>
									</div>
								</div>
				<?php		} ?>
						</section>
					</section>
				</div>
				<footer id="content-footer" class="affix">
					<div class="row">
						<div class="col-xs-3 footer-left">
				<?php		if($facture_id>0){ ?>
								<a href="facture_voir.php?facture=<?=$facture_id?>" class="btn btn-default btn-sm" >
									Retour
								</a>
				<?php		}elseif(isset($_SESSION["retourFacture"])){ ?>
								<a href="<?=$_SESSION["retourFacture"]?>" class="btn btn-default btn-sm" >
									Retour
								</a>
				<?php		} ?>
						</div>
						<div class="col-xs-6 footer-middle"></div>
						<div class="col-xs-3 footer-right">
				<?php		if(empty($erreur_txt)){
								if($d_facture["fac_nature"]==1){ ?>
									<button type="button" class="btn btn-sm btn-success" id="add_ligne" >
										<i class="fa fa-plus" ></i> Ajouter produit
									</button>
					<?php		} ?>

								<button type="button" id="save_facture" class="btn btn-sm btn-success" >
									<i class="fa fa-save" ></i> Enregistrer
								</button>
				<?php		} ?>
						</div>
					</div>
				</footer>
			</form>

			<!-- MAJ DU TEXTE D'UNE LIGNE DE FACTURE -->
			<div id="modal_designation" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg" style="width:1400px;" >
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
							×
							 </button>
							<h4 class="modal-title">Désignation</h4>
						</div>
						<div class="modal-body">
							<div class="admin-form theme-primary ">
								<div class="row mt5" >
									<div class="col-md-4" >
										<label for="formation" >Formation</label>
										<div id="cont_formation" >
											<textarea id="text_formation" ></textarea>
										</div>
									</div>
									<div class="col-md-4">
										<label for="complement" >Descriptif</label>
										<div id="cont_complement" >
											<textarea id="text_complement" ></textarea>
										</div>
									</div>
									<div class="col-md-4">
										<label for="stagiaire" >Stagiaires</label>
										<div id="cont_stagiaire" >
											<textarea id="text_stagiaire" ></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success btn-sm" id="sub_designation"  >
								Valider
							</button>
							<button type="button" class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
								Annuler
							</button>
						</div>
					</div>
				</div>
			</div>

	<?php
			include "includes/footer_script.inc.php"; ?>
			<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
			<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
			<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

			<script src="vendor/plugins/mask/jquery.mask.js"></script>
			<script src="assets/js/custom.js"></script>


			<script type="text/javascript" src="/assets/js/sync_heracles.js"></script>

			<script type="text/javascript">

				var contact_client=0;
				var contact_adresse=0;

				var json_ad_opca=<?=json_encode($d_ad_opca)?>;
				var json_ad_client=<?=json_encode($d_ad_client)?>;

				var tab_fac_adresse=new Array();
		<?php	if(!empty($adresses)){
					foreach($adresses as $ad){ ?>
						tab_fac_adresse[<?=$ad["id"]?>]=new Array(8);
						tab_fac_adresse[<?=$ad["id"]?>]["nom"]="<?=str_replace('"', "'", $ad["nom"]);?>";
						tab_fac_adresse[<?=$ad["id"]?>]["service"]="<?=str_replace('"', "'", $ad["service"])?>";
						tab_fac_adresse[<?=$ad["id"]?>]["ad1"]="<?=preg_replace( "/\r|\n/", "", str_replace('"', "'", $ad["ad1"]));?>";
						tab_fac_adresse[<?=$ad["id"]?>]["ad2"]="<?=str_replace('"', "'", $ad["ad2"]);?>";
						tab_fac_adresse[<?=$ad["id"]?>]["ad3"]="<?=str_replace('"', "'", $ad["ad3"]);?>";
						tab_fac_adresse[<?=$ad["id"]?>]["cp"]="<?=$ad["cp"]?>";
						tab_fac_adresse[<?=$ad["id"]?>]["ville"]="<?=str_replace('"', "'", $ad["ville"]);?>";
						tab_fac_adresse[<?=$ad["id"]?>]["geo"]="<?=$ad["geo"]?>";
				<?php	if($ad["defaut"]==1){ ?>
							defaut="<?=$ad["id"]?>";
		<?php			}
					}
				} ?>

				var tab_env_adresse=new Array();
				var adr_env_defaut=0;
		<?php	// on memo direct au format js
				if(!empty($d_ad_envoi)){
					foreach($d_ad_envoi as $ad){ ?>
						tab_env_adresse[<?=$ad["adr_id"]?>]=new Array(8);
						tab_env_adresse[<?=$ad["adr_id"]?>]["nom"]="<?=str_replace('"', "'", $ad["adr_nom"]);?>";
						tab_env_adresse[<?=$ad["adr_id"]?>]["service"]="<?=str_replace('"', "'", $ad["adr_service"]);?>";
						tab_env_adresse[<?=$ad["adr_id"]?>]["ad1"]="<?=str_replace('"', "'", $ad["adr_ad1"]);?>";
						tab_env_adresse[<?=$ad["adr_id"]?>]["ad2"]="<?=str_replace('"', "'", $ad["adr_ad2"]);?>";
						tab_env_adresse[<?=$ad["adr_id"]?>]["ad3"]="<?=str_replace('"', "'", $ad["adr_ad3"]);?>";
						tab_env_adresse[<?=$ad["adr_id"]?>]["cp"]="<?=$ad["adr_cp"]?>";
						tab_env_adresse[<?=$ad["adr_id"]?>]["ville"]="<?=str_replace('"', "'", $ad["adr_ville"]);?>";
						tab_env_adresse[<?=$ad["adr_id"]?>]["geo"]="<?=$ad["adr_geo"]?>";
				<?php	if($ad["adr_defaut"]==1){ ?>
							adr_env_defaut="<?=$ad["adr_id"]?>";
		<?php			}
					}
				} ?>

				var tab_contact=new Array();
		<?php	if(!empty($d_contacts)){
					foreach($d_contacts as $con){ ?>
						tab_contact[<?=$con["con_id"]?>]=new Array(8);
						tab_contact[<?=$con["con_id"]?>]["titre"]="<?=$con["con_titre"]?>";
						tab_contact[<?=$con["con_id"]?>]["nom"]="<?=$con["con_nom"]?>";
						tab_contact[<?=$con["con_id"]?>]["prenom"]="<?=$con["con_prenom"]?>";
						tab_contact[<?=$con["con_id"]?>]["tel"]="<?=$con["con_tel"]?>";
						tab_contact[<?=$con["con_id"]?>]["portable"]="<?=$con["con_portable"]?>";
						tab_contact[<?=$con["con_id"]?>]["mail"]="<?=$con["con_mail"]?>";
		<?php		}
				} ?>

				var client_categorie=0;
		<?php	if(!empty($d_facture["fac_cli_suc"])){ ?>
					var client_produit="<?=$d_facture["fac_cli_suc"]?>";
		<?php	}else{ ?>
					var client_produit="<?=$d_facture["fac_client"]?>";
		<?php	} ?>

				var fac_nature="<?=$d_facture["fac_nature"]?>";

				var ligne_focus=null;

				jQuery(document).ready(function (){

					//*****************************************
					//	INTERACTION HEADER FAC
					//******************************************/

					$("#agence").change(function(){
						if($(this).val()>0){
							$("#text_agence").hide();
							$("#client").prop("disabled",false);
						}else{
							$("#text_agence").show();
							$("#client").prop("disabled",true);
						}
						$("#client").val(0).trigger("change");

					});

					$("#client").change(function(){
						client_produit=$(this).val();
					});

					// type de facturation
					$("#ad_opca").click(function(){
						actualiser_adresse(json_ad_opca);
						$(".bloc-opca").show();
					});

					$("#ad_client").click(function(){
						actualiser_adresse(json_ad_client);
						$(".bloc-opca").hide();

					});


					// FINANCEUR

					$("#opca_type").change(function(){
						if($(this).val()>0){
							get_clients(4,$(this).val(),0,null,actualiser_select2,"#opca",0);
						}else{
							actualiser_select2("","#opca",0);
						}
						memoriser_opca("");
					});

					// adresse de facturation de l'OPCA
					$("#opca").change(function(){
						if($(this).val()>0){
							// on passe groupe à 0 pour les OPCA -> pas de récupération des adresses de la MM
							get_adresses($(this).val(),1,2,0,0,memoriser_opca,"","");
						}else{
							memoriser_opca("");
						}
					});

					// adresse de facturation de l'OPCA
					$("#adresse").change(function(){
						afficher_adresse($(this).val());
						recuperer_contacts();
					});

					// ADRESSE D'ENVOI

					// utilisation d'une adresse d'envoi
					$("#envoi_ad").click(function(){
						if($(this).is(":checked")){
							$(".bloc-env-adresse").show();
							$("#env_adresse").val(adr_env_defaut).trigger("change");

						}else{
							$("#env_adresse").val(0).trigger("change");
							$(".bloc-env-adresse").hide();
						}
					});

					$("#env_adresse").change(function(){
						afficher_adr_envoi($(this).val());
						recuperer_contacts();
					});


					// GESTION DES MODALITES DE REGLEMENTS
					$("input[name=reg_formule]").click(function (){
						reg_formule=$("input[name=reg_formule]:checked").val();
						actualiser_formule_reg(reg_formule);
					});

					$(".champ-reg-formule").blur(function(){
						if( ($(this).val()*1)>($(this).prop("max")*1) ){
							$(this).val($(this).prop("max"));
						}
					});

					// CONTACT
					$("#contact").change(function(){
						contact=$(this).val();
						if(contact>0){
							$("#con_titre").select2("val",tab_contact[contact]["titre"]);
							$("#con_nom").val(tab_contact[contact]["nom"]);
							$("#con_prenom").val(tab_contact[contact]["prenom"]);
							$("#con_tel").val(tab_contact[contact]["tel"]);
							$("#con_portable").val(tab_contact[contact]["portable"]);
							$("#con_mail").val(tab_contact[contact]["mail"]);

							$("#bloc_con_maj").show();

							$("#con_add").prop("checked",false);
							$("#bloc_con_add").hide();

						}else{
							$("#con_titre").select2("val",0);
							$(".champ-contact").val("");
							$("#con_maj").prop("checked",false);
							$("#bloc_con_maj").hide();
							$("#bloc_con_add").show();
						}
					});

					$("#save_facture").click(function(){
						if($("#envoi_ad").is(":checked")){
							$("#env_adresse").prop("required",true);
						}else{
							$("#env_adresse").prop("required",false);
						}
						if(valider_form("#form_facture")){
				<?php		if(!empty($facture_id)){ ?>
								$("#form_facture").submit();
				<?php		}else{ ?>
								facture_sync("<?=$acc_societe?>","#form_facture","#id_heracles","","","");
				<?php		} ?>
						}
					});


					//*****************************************
					//	INTERACTION LIGNE
					//******************************************/
					$(document).on("click",".check-ligne",function(){
						num_ligne=$(this).data("num_ligne");
						if($(this).is(":checked")){
							if($("#taux_remise_" + num_ligne).hasClass("calcul-form")){
								calcul_form($("#taux_remise_" + num_ligne));
							}
						}
						calculer_total();
					});


					$(document).on("click",".btn-designation",function(){
						num_ligne=$(this).data("num_ligne");
						ini_modal_designation(num_ligne);
					});

					$(document).on("blur",".calcul",function(){
						calcul($(this));
					});

					// ajout d'un ligne de facture
					$("#add_ligne").click(function(){
						if($("#client").val()>0){
							ecrire_ligne();
						}else{
							modal_alerte_user("Merci de selectionner un client avant d'ajouter une ligne");
						}
					});

					$(document).on("change",".categorie",function(){
						categorie=$(this).val();
						ligne_focus=$(this).data("num_ligne");
						get_produits(client_produit,categorie,0,0,0,1,0,actualiser_select2,"#produit_" + ligne_focus,0);
					});

					$(document).on("change",".produit",function(){
						produit=$(this).val();
						ligne_focus=$(this).data("num_ligne");
						if(produit>0){
							get_produit(client_produit,produit,1,0,afficher_produit,"","");
						}else{
							$("#libelle_" + ligne_focus).val("");

							$("#text_formation_" + ligne_focus).html("");
							$("#formation_" + ligne_focus).val("");

							$("#text_complement_" + ligne_focus).html("");
							$("#complement_" + ligne_focus).val("");

							$("#text_stagiaire_" + ligne_focus).html("");
							$("#stagiaire_" + ligne_focus).val("");

							$("#ht_" + ligne_focus).val(0);
							$("#ht_min_" + ligne_focus).val(0);

							$("#qte_" + ligne_focus).val(0);

							$("#taux_remise_" + ligne_focus).prop("readonly",false);


					<?php	if($_SESSION['acces']["acc_droits"][9]){ ?>
								$("#tva_id_" + ligne_focus).select2("val",0);
					<?php	} ?>

							calcul($("#qte_" + ligne_focus));
						}
					});



					//*****************************************
					//	INTERACTION MODAL
					//******************************************/

					// applique la maj des textes à la ligne concerne
					$("#sub_designation").click(function (){
						if(ligne_focus!=null){
							formation=$("#text_formation").code();
							$("#text_formation_" + ligne_focus).html(formation);
							$("#formation_" + ligne_focus).val(formation);
							complement=$("#text_complement").code();
                            complement = stripHtml(complement);
							$("#text_complement_" + ligne_focus).html(complement);
							$("#complement_" + ligne_focus).val(complement);

							stagiaire=$("#text_stagiaire").code();
							$("#text_stagiaire_" + ligne_focus).html(stagiaire);
							$("#stagiaire_" + ligne_focus).val(stagiaire);

						}
						$("#modal_designation").modal("hide");
						ligne_focus=null;
					});


				});
                function stripHtml(html)
                {
                    // create a new dov container
                 var div = document.createElement('div');

                 // assing your HTML to div's innerHTML
                 div.innerHTML = html;

                 // get all <a> elements from div
                 var elements = div.getElementsByTagName('a');

                 // remove all <a> elements
                 while (elements[0])
                    elements[0].parentNode.removeChild(elements[0])

                 // get div's innerHTML into a new variable
                 var repl = div.innerHTML;
                   return repl;
                }
				//*****************************************
				//	FONCTION HEADER
				//******************************************/

				// retour de la requete get_client
				function actualiser_get_client(json,param1,param2){
					if(json){
						if(json.erreur_txt!=""){
							afficher_message("Erreur","danger",json.erreur_txt);
						}else{

							client_categorie=json.categorie;

							// commercial
							$("#commercial").val(json.commercial).trigger("change");

							// actu des modalite de reglement
							$("#reg_type").val(json.reg_type);
							$("#reg_type").select2("val",json.reg_type);
							$("#reg_formule_" + json.reg_formule).prop("checked",true);
							actualiser_formule_reg(json.reg_formule);
							if($("#reg_nb_jour_" + json.reg_formule).length>0){
								$("#reg_nb_jour_" + json.reg_formule).val(json.reg_nb_jour);
							}
							if($("#reg_fdm_" + json.reg_formule).length>0){
								$("#reg_fdm_" + json.reg_formule).val(json.reg_fdm);
							}

							// ADRESSE DE FACTURATION

							// on memorise les adresses de facturation
							json_ad_client=json["adr_fac"];

							// type de facturation par defaut
							if(json.facture_opca==1){
								$("#ad_opca").prop("checked",true);
								$("#ad_client").prop("checked",false);

								$(".bloc-opca").show();

								// on declenche #opca->change pour recuperer les adresses
								$("#opca").val(json.opca).trigger("change");
							}else{
								actualiser_adresse(json_ad_client);
							}

							// ADRESSE D'ENVOIE
							get_adresses($("#client").val(),1,3,0,1,actualiser_envoi,"","");
						}
					}else{



					}

				}


				// recuperation des données OPCA
				function memoriser_opca(json){
					json_ad_opca=json;
					actualiser_adresse(json);
				}

				// GESTION DES ADRESSES DE FACTURATION
				function actualiser_adresse(json){
					tab_fac_adresse=new Array();
					// on memorise le tableau
					var defaut=0;
					$("#adresse option[value!='0'][value!='']").remove();
					if(json!=""){
						$.each(json, function(i, ad){
							tab_fac_adresse[ad.id]=new Array(7);
							tab_fac_adresse[ad.id]["nom"]=ad.nom;
							tab_fac_adresse[ad.id]["service"]=ad.service;
							tab_fac_adresse[ad.id]["ad1"]=ad.ad1;
							tab_fac_adresse[ad.id]["ad2"]=ad.ad2;
							tab_fac_adresse[ad.id]["ad3"]=ad.ad3;
							tab_fac_adresse[ad.id]["cp"]=ad.cp;
							tab_fac_adresse[ad.id]["ville"]=ad.ville;
							tab_fac_adresse[ad.id]["geo"]=ad.geo;
							if(ad.defaut==1){
								defaut=ad.id;
							}
						});

						// on actualise le select
						$("#adresse").select2({
							data: json,
							closeOnSelect: true
						});
					}
					// on affecte la valeur par defaut
					$("#adresse").val(defaut).trigger("change");
				}

				function afficher_adresse(adresse){
					ad_html="";
					if(adresse!="" && adresse!=0){
						ad_html="<b>" + tab_fac_adresse[adresse]["nom"] + "</b></br>";
						if(tab_fac_adresse[adresse]["service"]!=""){
							ad_html=ad_html + "<br/>" + tab_fac_adresse[adresse]["service"];
						}
						if(tab_fac_adresse[adresse]["ad1"]!=""){
							ad_html=ad_html + "<br/>" + tab_fac_adresse[adresse]["ad1"];
						}
						if(tab_fac_adresse[adresse]["ad2"]!=""){
							ad_html=ad_html + "<br/>" + tab_fac_adresse[adresse]["ad2"];
						}
						if(tab_fac_adresse[adresse]["ad3"]!=""){
							ad_html=ad_html + "<br/>" + tab_fac_adresse[adresse]["ad3"];
						}
						ad_html=ad_html + "<br/>" + tab_fac_adresse[adresse]["cp"] + " " + tab_fac_adresse[adresse]["ville"];
					}
					$("#cont_adr_fac").html(ad_html);
					if(adresse!=0){
						$(".bloc-adr-fac").show();
					}else{
						$(".bloc-adr-fac").hide();
					}
				}

				// ADRESSE D'ENVOI

				function actualiser_envoi(json){
					// on ne peux pas utiliser actualiser select2 car il faut aussi memoriser les adresses

					tab_env_adresse=new Array();
					// on memorise le tableau
					adr_env_defaut=0;

					$("#env_adresse option[value!='0'][value!='']").remove();

					if(json!=""){
						$.each(json, function(i, ad){
							tab_env_adresse[ad.id]=new Array(7);
							tab_env_adresse[ad.id]["nom"]=ad.nom;
							tab_env_adresse[ad.id]["service"]=ad.service;
							tab_env_adresse[ad.id]["ad1"]=ad.ad1;
							tab_env_adresse[ad.id]["ad2"]=ad.ad2;
							tab_env_adresse[ad.id]["ad3"]=ad.ad3;
							tab_env_adresse[ad.id]["cp"]=ad.cp;
							tab_env_adresse[ad.id]["ville"]=ad.ville;
							tab_env_adresse[ad.id]["geo"]=ad.geo;
							if(ad.defaut==1){
								adr_env_defaut=ad.id;
							}
						});

						// on actualise le select
						$("#env_adresse").select2({
							data: json,
							closeOnSelect: true
						});
					}

					// on affecte la valeur par defaut
					if($("#envoi_ad").is(":checked")){
						$("#env_adresse").val(adr_env_defaut).trigger("change");
					}
				}

				function afficher_adr_envoi(adresse){
					ad_html="";
					if(adresse!="" && adresse!=0){

						ad_html="<b>" + tab_env_adresse[adresse]["nom"] + "</b></br>";
						if(tab_env_adresse[adresse]["service"]!=""){
							ad_html=ad_html + "<br/>" + tab_env_adresse[adresse]["service"];
						}
						if(tab_env_adresse[adresse]["ad1"]!=""){
							ad_html=ad_html + "<br/>" + tab_env_adresse[adresse]["ad1"];
						}
						if(tab_env_adresse[adresse]["ad2"]!=""){
							ad_html=ad_html + "<br/>" + tab_env_adresse[adresse]["ad2"];
						}
						if(tab_env_adresse[adresse]["ad3"]!=""){
							ad_html=ad_html + "<br/>" + tab_env_adresse[adresse]["ad3"];
						}
						ad_html=ad_html + "<br/>" + tab_env_adresse[adresse]["cp"] + " " + tab_env_adresse[adresse]["ville"];

						$("#cont_env_adresse").html(ad_html);
						$(".bloc-env-adr-aff").show();
					}else{
						$("#cont_env_adresse").html("");
						$(".bloc-env-adr-aff").hide();
					}

				}

				// GESTION DE DELAI DE REG
				function actualiser_formule_reg(formule){
					$('.champ-reg-formule').each(function(){
						if($(this).hasClass("reg-formule-" + formule)){
							$(this).prop("disabled",false);
						}else{
							$(this).val(0);
							$(this).prop("disabled",true);
						}
					});
				}


				// GESTION DES CONTACTS

				function recuperer_contacts(){
					contact_client=0;
					contact_adresse=0;
					if($("#envoi_ad").is(":checked")){
						contact_client=$("#client").val();
						contact_adresse=$("#env_adresse").val();
					}else{
						if($("#ad_opca").is(":checked")){
							// on facture l'OPCA -> contact de l'OPCA
							contact_client=$("#opca").val();
						}else{
							contact_client=$("#client").val();
						}
						contact_adresse=$("#adresse").val();
					}

					if(contact_adresse!=0){
						get_contacts(1,0,contact_adresse,"",actualiser_contacts,"","");
					}else{
						actualiser_contacts();
					}
				}

				// traitement suite à la recuperation des contacts liés à l'adresse
				function actualiser_contacts(json){
					if(json){
						// on memorie les conatcts
						var con_defaut=0;
						tab_contact=new Array();
						$.each(json, function(i, con){
							tab_contact[con.id]=new Array();
							tab_contact[con.id]["titre"]=con.titre;
							tab_contact[con.id]["nom"]=con.nom;
							tab_contact[con.id]["prenom"]=con.prenom;
							tab_contact[con.id]["tel"]=con.tel;
							tab_contact[con.id]["portable"]=con.portable;
							tab_contact[con.id]["mail"]=con.mail;
							if(con.defaut==1){
								 con_defaut=con.id;
							}
						});
						// actualisation du select contact
						actualiser_select2(json,"#contact",con_defaut);
						if(con_defaut==0){
							// securite si 0 pas #contact.change
							$("#contact").val(0).trigger("change");
						}
					}else{
						actualiser_select2("","#contact",0);
						$("#contact").val(0).trigger("change");
						$("#contact").prop("disabled",true);
					}
					if(client_categorie==3){
						$("#contact").prop("disabled",true);
					}else{
						$("#contact").prop("disabled",false);
					}
				}


				//*****************************************
				//	FONCTION LIGNES
				//******************************************/
				function ini_modal_designation(num_ligne){


					// textarea formation
					formation=$("#formation_" + num_ligne).val();
					$("#cont_formation").append("");
					html_formation="<textarea id='text_formation' >" + formation + "</textarea>";
					$("#cont_formation").html(html_formation);
					$('#text_formation').summernote({
						lang: 'fr-FR',
						height:250,
						toolbar: [
							// [groupName, [list of button]]
							['style', ['bold', 'italic', 'underline', 'clear']],
							['font', ['strikethrough', 'superscript', 'subscript']],
							['fontsize', ['fontsize']],
							['color', ['color']],
							['para', ['ul', 'ol', 'paragraph']],
							['height', ['height']],
							['picture', ['picture']]
						]
					});

					// textarea complement
					complement=$("#complement_" + num_ligne).val();
					$("#cont_complement").append("");
					html_complement="<textarea id='text_complement' >" + complement + "</textarea>";
					$("#cont_complement").html(html_complement);
					$('#text_complement').summernote({
						lang: 'fr-FR',
						height:250,
						toolbar: [
							// [groupName, [list of button]]
							['style', ['bold', 'italic', 'underline', 'clear']],
							['font', ['strikethrough', 'superscript', 'subscript']],
							['fontsize', ['fontsize']],
							['color', ['color']],
							['para', ['ul', 'ol', 'paragraph']],
							['height', ['height']],
							['picture', ['picture']]
						]
					});

					// textarea stagiaire
					stagiaire=$("#stagiaire_" + num_ligne).val();
					$("#cont_stagiaire").append("");
					html_stagiaire="<textarea id='text_stagiaire' >" + stagiaire + "</textarea>";
					$("#cont_stagiaire").html(html_stagiaire);
					$('#text_stagiaire').summernote({
						lang: 'fr-FR',
						height:250,
						toolbar: [
							// [groupName, [list of button]]
							['style', ['bold', 'italic', 'underline', 'clear']],
							['font', ['strikethrough', 'superscript', 'subscript']],
							['fontsize', ['fontsize']],
							['color', ['color']],
							['para', ['ul', 'ol', 'paragraph']],
							['height', ['height']],
							['picture', ['picture']]
						]
					});

					ligne_focus=num_ligne;
					$("#modal_designation").modal("show");
				}

				function calcul(elt){
					//console.log("calcul");
					num_ligne=elt.data("num_ligne");
					if(elt.hasClass("calcul-ht")){
						// calcul en partant du pu ht
						ht=elt.val();
						ht=parseFloat(ht);
						qte=$("#qte_" + num_ligne).val();
						montant=ht*qte;
						montant=parseFloat(Math.round(montant * 100) / 100).toFixed(2);

					}else if(elt.hasClass("calcul-qte")){
						// calcul en partant de la qte
						qte=elt.val();
						ht=$("#ht_" + num_ligne).val();
						ht=parseFloat(Math.round(ht * 1000) / 1000);
						montant=ht*qte;
						montant=parseFloat(Math.round(montant * 100) / 100).toFixed(2);

					}else if(elt.hasClass("calcul-total")){
						// calcul en partant du montant total
						montant=elt.val();
						montant=parseFloat(Math.round(montant * 100) / 100).toFixed(2);
						qte=$("#qte_" + num_ligne).val();
						if(qte!=0){
							ht=montant/qte;
						}
						ht=parseFloat(Math.round(ht * 1000) / 1000).toFixed(3);
					}

					montant_min=0;
					if($("#ht_min_" + num_ligne).length>0){
						montant_min=$("#ht_min_" + num_ligne).val();
					};

					reste=0;
					if($("#reste_" + num_ligne).length>0){
						reste=$("#reste_" + num_ligne).val();
						reste=parseFloat(Math.round(reste * 100) / 100).toFixed(2);
					};
					if( (fac_nature==1 && (ht<0 || montant<0) ) || (fac_nature==2 && (ht>0 || montant>0) ) ){
						$("#ht_" + num_ligne).val(0);
						calcul($("#ht_" + num_ligne));

					}else if(montant_min>0 && ht<montant_min){
						$("#ht_" + num_ligne).val(montant_min);
						calcul($("#ht_" + num_ligne));

						$("#bloc_alert_ht_" + num_ligne).html(montant_min + " € min");
						$("#bloc_alert_ht_" + num_ligne).fadeIn().delay(5000).fadeOut();

					}else if( (reste>0 && 1*montant>1*reste) || (reste<0 && 1*montant<1*reste) ){
						$("#montant_ht_" + num_ligne).val(reste);
						calcul($("#montant_ht_" + num_ligne));

						/*$("#bloc_alert_ht_" + num_ligne).html(montant_min + " € min");
						$("#bloc_alert_ht_" + num_ligne).fadeIn().delay(5000).fadeOut();*/

					}else{
						$("#ht_" + num_ligne).val(ht);
						$("#qte_" + num_ligne).val(qte);
						$("#montant_ht_" + num_ligne).val(montant);
						calculer_total();
					}

				}

				function calculer_total(){
					var total=0;
					$(".calcul-total").each(function(){
						num_ligne=$(this).data("num_ligne");
						if($("#choix_" + num_ligne).is(":checked")&&$(this).val()!=""){
							total=1*total + 1*parseFloat($(this).val());
						}
					});

					$("#montant_total").val(total.toFixed(2));
				}

				function ecrire_ligne(){

					num_ligne=$('.ligne-fac').length;
					num_ligne=num_ligne+1;

					html="<tr class='ligne-fac' data-num_ligne='" + num_ligne + "' >";
						html=html + "<td>";
							html=html + "<div class='option-group field' >";
								html=html + "<label class='option option-dark' >";
									html=html + "<input type='checkbox' name='choix_" + num_ligne + "' id='choix_" + num_ligne + "' value='on' checked class='check-ligne' data-num_ligne='" + num_ligne + "' >";
									html=html + "<span class='checkbox' ></span>";
								html=html + "</label>";
							html=html + "</div>";
							html=html + "<input type='hidden' name='num_ligne[]' value='" + num_ligne + "' />";
							html=html + "<input type='hidden' name='fli_id_" + num_ligne + "' value='0' />";
							html=html + "<input type='hidden' name='fli_action_client_" + num_ligne + "' value='' />";
						html=html + "</td>";
						html=html + "<td>";
							html=html + "<div>";
								html=html + "<select class='categorie' id='categorie_" + num_ligne + "' data-num_ligne='" + num_ligne + "' >";
									html=html + "<option value='0' >Catégorie</option>";
							<?php	if(isset($d_pro_categories)){
										foreach($d_pro_categories as $cat => $cat_lib){ ?>
											html=html + "<option value='<?=$cat?>' ><?=$cat_lib?></option>";
							<?php		}
									} ?>
								html=html + "</select>";
							html=html + "</div>";
							html=html + "<div>";
								html=html + "<select class='produit' id='produit_" + num_ligne + "' name='fli_produit_" + num_ligne + "' data-num_ligne='" + num_ligne + "' >";
									html=html + "<option value='0' >Produit</option>";
								html=html + "</select>";
							html=html + "</div>";
						html=html + "</td>";

						html=html + "<td>";
							html=html + "<label for='libelle_" + num_ligne + "' >Libelle :</label>";
							html=html + "<input type='text' name='fli_libelle_" + num_ligne + "' class='gui-input' id='libelle_" + num_ligne + "' placeholder='Désignation' value='' />";

							html=html + "<div>";
								html=html + "<div class='pull-right' >";
									html=html + "<button type='button' class='btn btn-sm btn-warning btn-designation' data-num_ligne='" + num_ligne + "' >";
										html=html + "<i class='fa fa-pencil' ></i>";
									html=html + "</button>";
								html=html + "</div>";
								html=html + "<div id='text_formation_" + num_ligne + "' ></div>";
								html=html + "<div id='text_complement_" + num_ligne + "' ></div>";
								html=html + "<div id='text_stagiaire_" + num_ligne + "' ></div>";
							html=html + "</div>";
							html=html + '<input type="hidden" name="fli_formation_' + num_ligne + '" id="formation_' + num_ligne + '" rows="2" value="" />';
							html=html + '<input type="hidden" name="fli_complement_' + num_ligne + '" id="complement_' + num_ligne + '" rows="2" value="" />';
							html=html + '<input type="hidden" name="fli_stagiaire_' + num_ligne + '" id="stagiaire_' + num_ligne + '" rows="2" value="" />';
						html=html + "</td>";

						html=html + "<td>&nbsp;</td>";

						html=html + "<td>";

							html=html + "<input type='text' name='fli_ht_" + num_ligne + "' id='ht_" + num_ligne + "' class='gui-input calcul calcul-ht' data-num_ligne='" + num_ligne + "' placeholder='HT' size='5' value='0' />";

							html=html + "<input type='hidden' id='ht_min_" + num_ligne + "' value='0' />";

							html=html + "<div id='bloc_alert_ht_" + num_ligne + "' class='text-danger' ></div>";

						html=html + "</td>";

						html=html + "<td>";
							html=html + "<input type='text' name='fli_qte_" + num_ligne + "' id='qte_" + num_ligne + "' class='gui-input calcul calcul-qte' data-num_ligne='" + num_ligne + "' placeholder='Qté' size='5' value='0' />";
						html=html + "</td>";

						html=html + "<td>";
							html=html + "<input type='text' name='fli_montant_ht_" + num_ligne + "' id='montant_ht_" + num_ligne + "' class='gui-input input-float calcul calcul-total' data-num_ligne='" + num_ligne + "' size='9' placeholder='HT' value='0' />";
						html=html + "</td>";
				<?php	if($_SESSION['acces']["acc_droits"][9]){ ?>
							html=html + "<td>";
								html=html + "<select id='tva_id_" + num_ligne + "' name='fli_tva_id_" + num_ligne + "' >";
							<?php	foreach($base_tva as $id_tva => $libelle_tva){ ?>
                                    <?php if($id_tva == 1){ ?>
										html=html + "<option selected value='<?=$id_tva?>' ><?=$libelle_tva?></option>";
                                    <?php }else{ ?>
                                        html=html + "<option value='<?=$id_tva?>' ><?=$libelle_tva?></option>";
                                    <?php } ?>
							<?php	} ?>
								html=html + "</select>";
							html=html + "</td>";
				<?php	} ?>
					html=html + "</tr>";

					if($('.ligne-fac:last').length>0){
						$('.ligne-fac:last').after(html);
					}else{
						$('#ligne_fac tbody').html(html);
					}


					$('#categorie_' + num_ligne).select2();
					$('#produit_' + num_ligne).select2();
					$('#tva_id_' + num_ligne).select2();


				}

				function afficher_produit(json,param1,param2){
					if(ligne_focus>0){
						if(json){

							$("#libelle_" + ligne_focus).val(json.libelle);

							$("#text_formation_" + ligne_focus).html("");
							$("#formation_" + ligne_focus).val("");

							$("#text_complement_" + ligne_focus).html(json.devis_txt);
							$("#complement_" + ligne_focus).val(json.devis_txt);

							$("#text_stagiaire_" + ligne_focus).html("");
							$("#stagiaire_" + ligne_focus).val("");

							$("#ht_" + ligne_focus).val(json.ht_intra);
							$("#ht_min_" + ligne_focus).val(json.cpr_min_intra);

							if($("#qte_" + ligne_focus).val()==0){
								$("#qte_" + ligne_focus).val(1);
							};

							if(json.ca_verrou_intra){
								$("#ht_" + ligne_focus).prop("readonly",true);
								$("#montant_ht_" + ligne_focus).prop("readonly",true);
							}else{
								$("#ht_" + ligne_focus).prop("readonly",false);
								$("#montant_ht_" + ligne_focus).prop("readonly",true);
							}

					<?php	if($_SESSION['acces']["acc_droits"][9]){ ?>
								$("#tva_id_" + ligne_focus).select2("val",json.tva);
					<?php	} ?>

							calcul($("#ht_" + ligne_focus));

						}
					}
				}

			</script>
		</body>
	</html>
