 <?php

	// ENREGISTREMENT D'UNE FACTURE

	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");
	include 'modeles/mod_get_tva_taux.php';
	include 'modeles/mod_get_client_produits.php';


	$erreur_txt="";

	/*******************************
		ELEMENT POUR CONTROLE
	********************************/

		// FORM

	$facture_id=0;
	if(isset($_POST["facture"])){
		if(!empty($_POST["facture"])){
			$facture_id=intval($_POST["facture"]);
		}
	}

	$avoir_id=0;
	if(isset($_POST["avoir"])){
		if(!empty($_POST["avoir"])){
			$avoir_id=intval($_POST["avoir"]);
		}
	}

	$id_heracles=0;
	if(isset($_POST["id_heracles"])){
		if(!empty($_POST["id_heracles"])){
			$id_heracles=intval($_POST["id_heracles"]);
		}
	}
	$chrono_heracles=0;
	if(isset($_POST["chrono_heracles"])){
		if(!empty($_POST["chrono_heracles"])){
			$chrono_heracles=intval($_POST["chrono_heracles"]);
		}
	}

	$fac_client=0;
	if(isset($_POST["fac_client"])){
		if(!empty($_POST["fac_client"])){
			$fac_client=intval($_POST["fac_client"]);
		}
	}

	$auto=0;// creation d'un devis sans passer par devis.php (ex : fiche client)
	if(isset($_POST["auto"])){
		if(!empty($_POST["auto"])){
			$auto=intval($_POST["auto"]);
		}
	}

	$fac_commercial=0;
	if(isset($_POST["fac_commercial"])){
		if(!empty($_POST["fac_commercial"])){
			$fac_commercial=intval($_POST["fac_commercial"]);
		}
	}

	$fac_adresse=0;
	if(isset($_POST["fac_adresse"])){
		if(!empty($_POST["fac_adresse"])){
			$fac_adresse=intval($_POST["fac_adresse"]);
		}
	}

	$DT_fac_date=null;
	$fac_date_sql="";
	if(isset($_POST["fac_date"])){
		if(!empty($_POST["fac_date"])){
			$DT_fac_date=date_create_from_format('d/m/Y',$_POST["fac_date"]);
			$fac_date_sql=$DT_fac_date->format("Y-m-d");
		}
	}

	// opca impact le choix du rib -> le rib fait partie des données controlées
	$fac_opca=0;
	$fac_opca_type=0;
	$fac_opca_compte=null;
	if($_POST["ad_opca"]==1){
		if(!empty($_POST["fac_opca_type"])){
			$fac_opca_type=intval($_POST["fac_opca_type"]);
			if($fac_opca_type>0){
				if(!empty($_POST["fac_opca"])){
					$fac_opca=intval($_POST["fac_opca"]);
				}
			}
		}
	}


	// PERSONNE CONNECTE

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}
	/*******************************
			CONTROLE
	*******************************/
	if(empty($fac_client) OR empty($acc_societe) OR empty($fac_commercial) OR empty($fac_adresse) OR empty($DT_fac_date) ){
		$erreur_txt="Formulaire incomplet!";
	}
    if(empty($erreur_txt) && !empty($_POST['num_ligne'])){
        foreach($_POST["num_ligne"] as $num_ligne){
            if(!empty($_POST["fli_action_client_" . $num_ligne])){
                $fli_action_client=intval($_POST["fli_action_client_" . $num_ligne]);
                if(!empty($_POST["fli_action_cli_soc_" . $num_ligne])){
                    $fli_action_cli_soc=intval($_POST["fli_action_cli_soc_" . $num_ligne]);
                }
            }
        // ON VERIFIE QUE L'ACTION N'EST PAS DEJA LIE A UNE FACTURE NON BLOQUE
            if(!empty($fli_action_client) && !empty($fli_action_cli_soc) && !empty($_POST['action_client_id'])){
                if(!empty($facture_id)){
                    // MODIFICATION DE FACTURE
                    $sql="SELECT fac_id,fac_numero FROM Factures,Factures_Lignes WHERE fac_id=fli_facture AND NOT fac_blocage AND fac_id != " . $facture_id . " AND fli_action_client=" . $fli_action_client . " AND fli_action_cli_soc=" . $fli_action_cli_soc . ";";
                }else{
                    $sql="SELECT fac_id,fac_numero FROM Factures,Factures_Lignes WHERE fac_id=fli_facture AND NOT fac_blocage AND fli_action_client=" . $fli_action_client . " AND fli_action_cli_soc=" . $fli_action_cli_soc . ";";
                }
                $req=$ConnSoc->query($sql);
                $d_fac_ouverte=$req->fetch();
                if(!empty($d_fac_ouverte)){

                    $_SESSION['message'][] = array(
                        "titre" => "Erreur",
                        "type" => "warning",
                        "message" => "L'action " . $_POST['action_client_id'] . " est déjà liée à la facture " . $d_fac_ouverte["fac_numero"] . "."
                    );
                    header("location :facture_voir.php?facture=" . $d_fac_ouverte["fac_id"]);
                    die();

                }
            }

        }
    }
	// SUR LA FACTURE
	if(!empty($facture_id)){

		$sql="SELECT fac_chrono,fac_nature,fac_soc_ad1,fac_soc_ad2,fac_soc_ad3,fac_soc_cp,fac_soc_ville,fac_soc_siret FROM Factures WHERE fac_id=:fac_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("fac_id",$facture_id);
		$req->execute();
		$d_facture=$req->fetch();
		if(empty($d_facture)){
			$erreur_txt="Impossible d'identifier la facture à modifier";
		}else{
			$fac_nature=$d_facture["fac_nature"];

			if($fac_nature==2){

				$fac_soc_ad1=$d_facture["fac_soc_ad1"];
				$fac_soc_ad2=$d_facture["fac_soc_ad2"];
				$fac_soc_ad3=$d_facture["fac_soc_ad3"];
				$fac_soc_cp=$d_facture["fac_soc_cp"];
				$fac_soc_ville=$d_facture["fac_soc_ville"];
				$fac_soc_siret=$d_facture["fac_soc_siret"];

			}
		}
	}elseif(!empty($avoir_id)){
		$fac_nature=2;

	}else{
		$fac_nature=1;
	}

	// SOCEIETE / AGENCE
	if(empty($erreur_txt)){

		if(!empty($acc_agence)){
			$fac_agence=$acc_agence;
		}else{
			$fac_agence=0;
			if(!empty($_POST["fac_agence"])){
				$fac_agence=intval($_POST["fac_agence"]);
			}
		}

		$fac_compta_change=0;

		$sql="SELECT soc_agence,soc_nom,soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville,soc_tel,soc_fax,soc_mail,soc_site,soc_logo_1,soc_logo_2,soc_qualite_1,soc_qualite_2
		,soc_compta,soc_compta_age,soc_compta_tel,soc_compta_fax,soc_compta_mail,soc_tva_exo,soc_siret FROM Societes WHERE soc_id=" . $acc_societe . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();
		if(!empty($d_societe)){
			if($d_societe["soc_agence"] AND empty($fac_agence)){
				$erreur_txt="La facture doit-être associé à une agence.";
			}else{

				$fac_soc_nom=$d_societe["soc_nom"];
				$fac_soc_agence="";
				if($fac_nature!=2){

					$fac_soc_ad1=$d_societe["soc_ad1"];
					$fac_soc_ad2=$d_societe["soc_ad2"];
					$fac_soc_ad3=$d_societe["soc_ad3"];
					$fac_soc_cp=$d_societe["soc_cp"];
					$fac_soc_ville=$d_societe["soc_ville"];

					$fac_soc_siret=$d_societe["soc_siret"];

				}
				$fac_soc_tel=$d_societe["soc_tel"];
				$fac_soc_fax=$d_societe["soc_fax"];
				$fac_soc_mail=$d_societe["soc_mail"];
				$fac_soc_site=$d_societe["soc_site"];
				

				$fac_compta_tel=$d_societe["soc_compta_tel"];
				$fac_compta_fax=$d_societe["soc_compta_fax"];
				$fac_compta_mail=$d_societe["soc_compta_mail"];

				$fac_logo_1=$d_societe["soc_logo_1"];
				$fac_logo_2=$d_societe["soc_logo_2"];
				$fac_qualite_1=$d_societe["soc_qualite_1"];
				$fac_qualite_2=$d_societe["soc_qualite_2"];

				$fac_compta=$d_societe["soc_compta"];
				$fac_compta_age=$d_societe["soc_compta_age"];

			}
		}else{
			$erreur_txt="Impossible d'identifier le société utilisée pour la facturation!";
		}
	}

	// AGENCES
	if(empty($erreur_txt)){

		if(!empty($fac_agence)){

			$sql="SELECT age_nom,age_ad1,age_ad2,age_ad3,age_cp,age_ville,age_tel,age_fax,age_mail,age_site,age_logo_1,age_logo_2,age_qualite_1,age_qualite_2
			,age_compta,age_compta_age,age_compta_tel,age_compta_fax,age_compta_mail,age_siret FROM Agences WHERE age_id=" . $fac_agence . ";";
			$req=$Conn->query($sql);
			$d_agence=$req->fetch();
			if(!empty($d_agence)){

				$fac_soc_agence=$d_agence["age_nom"];

				if($fac_nature!=2){
					$fac_soc_ad1=$d_agence["age_ad1"];
					$fac_soc_ad2=$d_agence["age_ad2"];
					$fac_soc_ad3=$d_agence["age_ad3"];
					$fac_soc_cp=$d_agence["age_cp"];
					$fac_soc_ville=$d_agence["age_ville"];

					$fac_soc_siret=$d_agence["age_siret"];
				}
				$fac_soc_tel=$d_agence["age_tel"];
				$fac_soc_fax=$d_agence["age_fax"];
				$fac_soc_mail=$d_agence["age_mail"];
				$fac_soc_site=$d_agence["age_site"];
				

				$fac_compta_tel=$d_agence["age_compta_tel"];
				$fac_compta_fax=$d_agence["age_compta_fax"];
				$fac_compta_mail=$d_agence["age_compta_mail"];

				$fac_logo_1=$d_agence["age_logo_1"];
				$fac_logo_2=$d_agence["age_logo_2"];
				$fac_qualite_1=$d_agence["age_qualite_1"];
				$fac_qualite_2=$d_agence["age_qualite_2"];

				$fac_compta=$d_agence["age_compta"];
				$fac_compta_age=$d_agence["age_compta_age"];

			}else{
				$erreur_txt="Impossible d'identifier l'agence utilisée pour la facturation!";
			}

		}
	}

	
	if(!empty($avoir_id)){
		// CREATION D'AVOIR 
		// on prend les données juridiques de la facture au cas ou il y aurait eu un changement d'adresse entre facture et avoir

		$sql="SELECT fac_soc_ad1,fac_soc_ad2,fac_soc_ad3,fac_soc_cp,fac_soc_ville,fac_soc_siret FROM Factures WHERE fac_id=:avoir_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("avoir_id",$avoir_id);
		$req->execute();
		$d_avoir=$req->fetch();
		if(empty($d_avoir)){
			$erreur_txt="Impossible d'identifier la facture à l'origine de l'avoir";
		}else{
			$fac_soc_ad1=$d_avoir["fac_soc_ad1"];
			$fac_soc_ad2=$d_avoir["fac_soc_ad2"];
			$fac_soc_ad3=$d_avoir["fac_soc_ad3"];
			$fac_soc_cp=$d_avoir["fac_soc_cp"];
			$fac_soc_ville=$d_avoir["fac_soc_ville"];

			$fac_soc_siret=$d_avoir["fac_soc_siret"];
		}

	}



	// CLIENT
	if(empty($erreur_txt)){

		$sql="SELECT cli_code,cli_nom,cli_categorie,cli_sous_categorie,cli_nom,cli_affacturage_iban,cli_affacturable,cli_interco_soc,cli_filiale_de,cli_groupe,cli_first_facture,
		cso_rib,cso_rib_change FROM Clients,Clients_Societes
		WHERE cli_id=cso_client AND cso_societe=" . $acc_societe . " AND cli_id=" . $fac_client;
		if(!empty($fac_agence)){
			$sql.=" AND cso_agence=" . $fac_agence;
		}
		if(!$_SESSION["acces"]["acc_droits"][6]){
			$sql.=" AND cso_utilisateur=" . $acc_utilisateur;
		}
		$sql.=";";
		$req=$Conn->query($sql);
		$d_client=$req->fetch();
		if(empty($d_client)){
			$erreur_txt="Impossible d'identifier le client utilisé pour la facturation!";
		}else{

			if(empty($d_client["cso_rib"])){
				$erreur_txt="Impossible d'identifier le RIB!";
			}

			$fac_affacturage=0;
			$fac_affacturage_iban="";

			if($acc_societe==17 AND $DT_fac_date->format("Y-m-d")<"2020-04-01"){
				if(!empty($d_client["cli_affacturage_iban"]) AND empty($fac_opca)){
					$fac_affacturage=1;
					$fac_affacturage_iban=$d_client["cli_affacturage_iban"];
				}
			}
			
			if(!empty($fac_opca)){
				$fac_opca_compte=$d_client["cli_nom"];
			}
			/*else{
				$fac_affacturage=$d_client["cli_affacturage"];
			}*/
		}
	}

	// CHRONO
	if(empty($erreur_txt)){

		if(empty($facture_id)){

			// CERATION DE FACTURE

			$fac_chrono=0;
			$sql="SELECT fac_chrono,fac_date FROM Factures WHERE fac_id = (SELECT MAX(fac_id) AS fac_id FROM Factures";
				if($acc_societe==11){
					// ACB
					$sql.=" WHERE fac_nature=" . $fac_nature;
				}elseif($acc_societe==16){
					// MB
					$sql.=" WHERE YEAR(fac_date)=" . $DT_fac_date->format("Y");
				}
			$sql.=");";
			$req=$ConnSoc->query($sql);
			$d_fac_prec=$req->fetch();
			if(!empty($d_fac_prec)){

				$fac_chrono=intval($d_fac_prec["fac_chrono"]);
				$DT_fac_prec=date_create_from_format('Y-m-d',$d_fac_prec["fac_date"]);


				if($d_fac_prec["fac_date"]>$fac_date_sql){
					$erreur_txt="La date de facture ne peut pas être inférieure au " .  $DT_fac_prec->format("d/m/Y");
				}
			}
			$fac_chrono++;
			if(!empty($id_heracles) AND $acc_societe!=11 AND $acc_societe!=3){
				$fac_chrono=$id_heracles;
			}
		}else{

			$sql="SELECT fac_chrono,fac_date FROM Factures WHERE (fac_chrono= " . intval($d_facture["fac_chrono"]-1) . " OR fac_chrono=" .  intval($d_facture["fac_chrono"]+1) . ")";
			if($acc_societe==11){
				// ACB
				$sql.=" AND fac_nature=" . $fac_nature;
			}elseif($acc_societe==16){
				$sql.=" AND YEAR(fac_date)=" . $DT_fac_date->format("Y");
			}
			$sql.=";";
			$req=$ConnSoc->query($sql);
			$d_fac_chrono=$req->fetchAll();
			if(!empty($d_fac_chrono)){
				/*echo("<pre>");
					print_r($d_fac_chrono);
				echo("</pre>");
				die();*/
				foreach($d_fac_chrono as $fc){
					$DT_fac_chrono=date_create_from_format('Y-m-d',$fc["fac_date"]);

					if($fc["fac_chrono"]==$d_facture["fac_chrono"]-1 AND $fac_date_sql<$fc["fac_date"]){
						$erreur_txt="La date de facture ne peut pas être inférieur au " .  $DT_fac_chrono->format("d/m/Y") . "Err001";
					}elseif($fc["fac_chrono"]==$d_facture["fac_chrono"]+1 AND $fac_date_sql>$fc["fac_date"]){
						$erreur_txt="La date de facture ne peut pas être supérieur au " .  $DT_fac_chrono->format("d/m/Y");
					}
				}
			}
			$fac_chrono=$d_facture["fac_chrono"];
		}

		if($acc_societe==16){
			$fac_numero=$DT_fac_date->format("y") . str_pad($fac_chrono,5,"0",STR_PAD_LEFT);
		}else{
			if($fac_nature==1){
				$fac_numero="FC" . $DT_fac_date->format("y") . "-" . str_pad($fac_chrono,5,"0",STR_PAD_LEFT);
			}else{
				$fac_numero="AC" . $DT_fac_date->format("y") . "-" . str_pad($fac_chrono,5,"0",STR_PAD_LEFT);
			}
		}
	}

	// LE RIB

	if(empty($d_client["cli_interco_soc"]) AND $d_client["cli_categorie"]!=3 AND $d_client["cli_sous_categorie"]!=5 AND $d_client["cli_sous_categorie"]!=6 AND $d_client["cli_affacturable"]!=2 AND (empty($fac_opca) OR ($fac_opca_type!=5 AND $fac_opca_type!=6) ) ){

		// si le client n'est ni un interco ni un opca, ni un pole emploi ni un particulier et qu'il n'est pas spécifiquement noté non affacturable -> alors on regarde s'il y a un rib d'affacturage

		$sql="SELECT rib_id,rib_change,rib_iban,rib_bic FROM Rib WHERE rib_affacturage AND rib_societe=" . $acc_societe . ";";
		$req=$Conn->query($sql);
		$d_r_rib=$req->fetchAll();
		if(!empty($d_r_rib)){
			if(count($d_r_rib)>1){
				$erreur_txt="La société dispose de plusieurs RIB d'affacturage. Merci de contacter le service comptabilité.";
			}else{
				// la société dispose d'un rib affacturage -> on enregistre la facture en affacturage
				$fac_affacturage=1;
				$d_rib=$d_r_rib[0];
			}
		}
	}
	


	// s'il n'y a pas d'affacturage ou si nous sommes sur ADEQ -> on utilise le rib du client
	if(!$fac_affacturage OR ($acc_societe==17 AND $DT_fac_date->format("Y-m-d")<"2020-04-01")){
		$sql="SELECT rib_id,rib_change,rib_iban,rib_bic FROM Rib WHERE rib_id=" . $d_client["cso_rib"] . ";";
		$req=$Conn->query($sql);
		$d_rib=$req->fetch();
		if(empty($d_rib)){
			$erreur_txt="Le RIB n'a pas été identifié!";
		}else{
			// si le rib cahnge on niveau 
			if(!empty($d_client["cso_rib_change"])){
				$d_rib["rib_change"]=$d_client["cso_rib_change"];
				
			}
		}
	}
	/*******************************
		DONNEE COMPLEMENTAIRE
	*******************************/

	if(empty($erreur_txt)){

		// FORM

		$fac_cli_suc=0;
		// FG 28/11/2019 
		/* $_POST["fac_cli_suc"] est = à l'ID du client qui à déclencher une facturation à GFC 
		il n'est jamais = 1 sur si le client est 3SQE (ID 1)*/
		//if($_POST["fac_cli_suc"]==1){
			if(!empty($_POST["fac_cli_suc"])){
				$fac_cli_suc=intval($_POST["fac_cli_suc"]);
			}
		//}

		$fac_convention=0;
		if(!empty($_POST["fac_convention"])){
			$fac_convention=1;
		}


		// ADRESSE DE FACTURATION
		$sql="SELECT adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_geo,cli_nom
		FROM Adresses,Clients WHERE adr_ref_id=cli_id AND adr_ref=1 AND adr_id=" . $fac_adresse;
		if($fac_opca>0){
			$sql.=" AND adr_ref_id=" . $fac_opca;
		}elseif(!empty($d_client["cli_filiale_de"])){
			$sql.=" AND (adr_ref_id=" . $fac_client . " OR adr_ref_id=" . $d_client["cli_filiale_de"] . ")";
		}else{
			$sql.=" AND adr_ref_id=" . $fac_client;
		}
		$sql.=";";
		$req=$Conn->query($sql);
		$d_adresse_fac=$req->fetch();
		if(!empty($d_adresse_fac)){
			if(empty($d_adresse_fac["adr_nom"])){
				$d_adresse_fac["adr_nom"]=$d_client["cli_nom"];
			}
			$d_adresse_fac["adr_service"]=$d_adresse_fac["adr_service"];
		}
		/*echo("<pre>");
			print_r($d_adresse_fac);
		echo("</pre>");
		var_dump($d_adresse_fac["adr_service"]);
		die();*/


		// ADRESSE D'ENVOI

		$fac_env_adresse=0;
		$d_adresse_env=array(
			"adr_nom" => "",
			"adr_service" => "",
			"adr_ad1" => "",
			"adr_ad2" => "",
			"adr_ad3" => "",
			"adr_cp" => "",
			"adr_ville" => ""
		);
		if(!empty($_POST["fac_envoi"])){
			if(!empty($_POST["fac_env_adresse"])){

				$fac_env_adresse=intval($_POST["fac_env_adresse"]);

				if(!empty($d_client["cli_filiale_de"])){
					$sql="SELECT * FROM Adresses WHERE adr_id=" . $fac_env_adresse . " AND (adr_ref_id=" . $fac_client . " OR adr_ref_id=" . $d_client["cli_filiale_de"] . ");";
				}else{
					$sql="SELECT * FROM Adresses WHERE adr_id=" . $fac_env_adresse . " AND adr_ref_id=" . $fac_client . ";";
				}
				$req=$Conn->query($sql);
				$d_adresse_env=$req->fetch();
			}
		}

		// LE CONTACT

		$fac_contact=0;
		if(!empty($_POST["fac_contact"])){
			$fac_contact=intval($_POST["fac_contact"]);
		}

		$fac_con_titre=0;
		if(!empty($_POST["fac_con_titre"])){
			$fac_con_titre=intval($_POST["fac_con_titre"]);
		}

		$fac_con_nom="";
		if(!empty($_POST["fac_con_nom"])){
			$fac_con_nom=$_POST["fac_con_nom"];
		}

		$con_add=0;
		$con_maj=0;
		if(!empty($fac_con_nom)){
			if(isset($_POST["con_add"])){
				if(!empty($_POST["con_add"])){
					$con_add=1;
				}
			}

			if(isset($_POST["con_maj"])){
				if(!empty($_POST["con_maj"])){
					$con_maj=1;
				}
			}

			// client associé au contact
			$con_ref_id=0;
			if(empty($fac_opca) OR !empty($fac_env_adresse)){
				if($d_client["cli_categorie"]==3){
					// pas de maj pour les particuliers => trop complexe lien stagiaire
					$con_add=0;
					$con_maj=0;
				}else{
					$con_ref_id=$fac_client;
					if(!empty($fac_env_adresse)){
						$con_adresse=$fac_env_adresse;
					}else{
						$con_adresse=$fac_adresse;
					}
				}
			}else{
				$con_ref_id=$fac_opca;
				$con_adresse=$fac_adresse;
			}
		}





		// MODALITE DE REGLEMENT

		$fac_reg_type=0;
		if(!empty($_POST["fac_reg_type"])){
			$fac_reg_type=intval($_POST["fac_reg_type"]);
		}

		$fac_reg_formule=1;
		$fac_reg_fdm=0;
		$fac_reg_nb_jour=0;
		if(!empty($_POST["reg_formule"])){
			$fac_reg_formule=intval($_POST["reg_formule"]);
		}
		switch ($fac_reg_formule) {
			case 1:
				if(!empty($_POST["reg_nb_jour_1"])){
					$fac_reg_nb_jour=intval($_POST["reg_nb_jour_1"]);
				}
				break;
			case 2:
				$fac_reg_nb_jour=45;
				break;
			case 3:
				$fac_reg_fdm=45;
				break;
			case 4:
				if(!empty($_POST["reg_nb_jour_4"])){
					$fac_reg_nb_jour=intval($_POST["reg_nb_jour_4"]);
				}
				if(!empty($_POST["reg_fdm_4"])){
					$fac_reg_fdm=intval($_POST["reg_fdm_4"]);
				}
				break;
		}

		// DATE DE REGLEMENT


		$DT_reg=date_create_from_format('Y-m-d',$DT_fac_date->format("Y-m-d"));
		if($fac_reg_nb_jour>0){
			$DT_reg->add(new DateInterval('P' . $fac_reg_nb_jour. 'D'));
		}
		if($fac_reg_formule==2 OR $fac_reg_fdm>0){
			$ms=intval($DT_reg->format("m"));
			$ms++;
			$DT_reg=date_create_from_format('Y-m-d',$DT_reg->format("Y") . "-" . $ms . "-01");
			$DT_reg->sub(new DateInterval('P1D'));
			if($fac_reg_fdm>0){
				$DT_reg->add(new DateInterval('P' . $fac_reg_fdm. 'D'));
			}
		}

		// IDENTITE DU COMMERCIAL

		$fac_com_identite="";
		$sql="SELECT com_label_1,com_label_2 FROM Commerciaux WHERE com_id=:fac_commercial;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("fac_commercial",$fac_commercial);
		$req->execute();
		$d_commercial=$req->fetch();
		if(!empty($d_commercial)){
			if(!empty($d_commercial["com_label_2"])){
				$fac_com_identite=$d_commercial["com_label_2"] . " ";
			}
			$fac_com_identite.=$d_commercial["com_label_1"];
		}


		// ADRESSE SERVICE COMPTA

		$sql="SELECT soc_nom,soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville,age_nom,age_ad1,age_ad2,age_ad3,age_cp,age_ville
		FROM Societes LEFT JOIN Agences ON (Societes.soc_id=Agences.age_societe AND Agences.age_id=" . $fac_compta_age . ")
		WHERE soc_id=" . $fac_compta . ";";
		$req=$Conn->query($sql);
		$d_soc_compta=$req->fetch();
		if(!empty($d_soc_compta)){

			$fac_compta_nom=$d_soc_compta["soc_nom"];
			if(!empty($d_soc_compta["age_nom"])){
				$fac_compta_agence=$d_soc_compta["age_nom"];
				$fac_compta_ad1=$d_soc_compta["age_ad1"];
				$fac_compta_ad2=$d_soc_compta["age_ad2"];
				$fac_compta_ad3=$d_soc_compta["age_ad3"];
				$fac_compta_cp=$d_soc_compta["age_cp"];
				$fac_compta_ville=$d_soc_compta["age_ville"];
			}else{
				$fac_compta_agence="";
				$fac_compta_ad1=$d_soc_compta["soc_ad1"];
				$fac_compta_ad2=$d_soc_compta["soc_ad2"];
				$fac_compta_ad3=$d_soc_compta["soc_ad3"];
				$fac_compta_cp=$d_soc_compta["soc_cp"];
				$fac_compta_ville=$d_soc_compta["soc_ville"];
			}
		}

		// BASE PRODUIT DU CLIENT

		$tab_produit=get_client_produits($fac_client,0,0,0,1,0,0,0,0,0,null);

		// LES TVA EN VIGEUR

		// tva en vigeur
		$tab_tva=get_tva_taux($DT_fac_date->format("Y-m-d"),0);
	}

	/*******************************
		ENREGISTREMENT
	*******************************/

	if(empty($erreur_txt)){

		// AJOUT / MAJ D'UN CONTACT

		if($con_add==1 AND empty($fac_contact) AND !empty($con_ref_id) ){

			$sql="INSERT INTO Contacts (con_ref,con_ref_id,con_titre,con_nom,con_prenom,con_tel,con_portable,con_mail
			) VALUES (1,:con_ref_id,:con_titre,:con_nom,:con_prenom,:con_tel,:con_portable,:con_mail);";
			$req=$Conn->prepare($sql);
			$req->bindValue(":con_ref_id",$con_ref_id);
			$req->bindValue(":con_titre",$fac_con_titre);
			$req->bindValue(":con_nom",$fac_con_nom);
			$req->bindValue(":con_prenom",$_POST["fac_con_prenom"]);
			$req->bindValue(":con_tel",$_POST["fac_con_tel"]);
			$req->bindValue(":con_portable",$_POST["fac_con_portable"]);
			$req->bindValue(":con_mail",$_POST["fac_con_mail"]);
			$req-> execute();
			$fac_contact=$Conn->lastInsertId();

			// on associe le contact à l'adresse
			if(!empty($con_adresse)){
				$sql="INSERT INTO Adresses_Contacts (aco_contact,aco_adresse) VALUES (:aco_contact,:aco_adresse);";
				$req=$Conn->prepare($sql);
				$req->bindValue(":aco_contact",$fac_contact);
				$req->bindValue(":aco_adresse",$con_adresse);
				$req-> execute();
			}

		}elseif($con_maj==1 AND !empty($fac_contact)){

			$sql="UPDATE Contacts SET
			con_titre=:con_titre,
			con_nom=:con_nom,
			con_prenom=:con_prenom,
			con_tel=:con_tel,
			con_portable=:con_portable,
			con_mail=:con_mail
			WHERE con_id=:con_id AND con_ref_id=:con_ref_id;";
			$req=$Conn->prepare($sql);
			$req->bindValue(":con_id",$fac_contact);
			$req->bindValue(":con_ref_id",$con_ref_id);
			$req->bindValue(":con_titre",$fac_con_titre);
			$req->bindValue(":con_nom",$fac_con_nom);
			$req->bindValue(":con_prenom",$_POST["fac_con_prenom"]);
			$req->bindValue(":con_tel",$_POST["fac_con_tel"]);
			$req->bindValue(":con_portable",$_POST["fac_con_portable"]);
			$req->bindValue(":con_mail",$_POST["fac_con_mail"]);
			$req-> execute();
		}

		// ADD / UP FACTURES
		if($facture_id>0){

			// UPDATE DE FACTURE

			$sql="UPDATE Factures SET
			fac_agence=:fac_agence,
			fac_client=:fac_client,
			fac_cli_categorie=:fac_cli_categorie,
			fac_cli_sous_categorie=:fac_cli_sous_categorie,
			fac_cli_code=:fac_cli_code,
			fac_cli_suc=:fac_cli_suc,
			fac_commercial=:fac_commercial,
			fac_com_identite=:fac_com_identite,
			fac_date=:fac_date,
			fac_reference=:fac_reference,
			fac_libelle=:fac_libelle,
			fac_opca=:fac_opca,
			fac_opca_type=:fac_opca_type,
			fac_adresse=:fac_adresse,
			fac_cli_nom=:fac_cli_nom,
			fac_opca_compte=:fac_opca_compte,
			fac_cli_service=:fac_cli_service,
			fac_cli_ad1=:fac_cli_ad1,
			fac_cli_ad2=:fac_cli_ad2,
			fac_cli_ad3=:fac_cli_ad3,
			fac_cli_cp=:fac_cli_cp,
			fac_cli_ville=:fac_cli_ville,
			fac_cli_geo=:fac_cli_geo,
			fac_env_adresse=:fac_env_adresse,
			fac_env_nom=:fac_env_nom,
			fac_env_service=:fac_env_service,
			fac_env_ad1=:fac_env_ad1,
			fac_env_ad2=:fac_env_ad2,
			fac_env_ad3=:fac_env_ad3,
			fac_env_cp=:fac_env_cp,
			fac_env_ville=:fac_env_ville,
			fac_soc_nom=:fac_soc_nom,
			fac_soc_agence=:fac_soc_agence,
			fac_soc_ad1=:fac_soc_ad1,
			fac_soc_ad2=:fac_soc_ad2,
			fac_soc_ad3=:fac_soc_ad3,
			fac_soc_cp=:fac_soc_cp,
			fac_soc_ville=:fac_soc_ville,
			fac_soc_tel=:fac_soc_tel,
			fac_soc_fax=:fac_soc_fax,
			fac_soc_mail=:fac_soc_mail,
			fac_soc_site=:fac_soc_site,
			fac_soc_siret=:fac_soc_siret,
			fac_contact=:fac_contact,
			fac_con_titre=:fac_con_titre,
			fac_con_nom=:fac_con_nom,
			fac_con_prenom=:fac_con_prenom,
			fac_con_tel=:fac_con_tel,
			fac_con_portable=:fac_con_portable,
			fac_con_mail=:fac_con_mail,
			fac_reg_type=:fac_reg_type,
			fac_reg_formule=:fac_reg_formule,
			fac_reg_fdm=:fac_reg_fdm,
			fac_reg_nb_jour=:fac_reg_nb_jour,
			fac_date_reg_prev=:fac_date_reg_prev,
			fac_rib=:fac_rib,
			fac_rib_change=:fac_rib_change,
			fac_rib_iban=:fac_rib_iban,
			fac_rib_bic=:fac_rib_bic,
			fac_compta_nom=:fac_compta_nom,
			fac_compta_agence=:fac_compta_agence,
			fac_compta_ad1=:fac_compta_ad1,
			fac_compta_ad2=:fac_compta_ad2,
			fac_compta_ad3=:fac_compta_ad3,
			fac_compta_cp=:fac_compta_cp,
			fac_compta_ville=:fac_compta_ville,
			fac_compta_tel=:fac_compta_tel,
			fac_compta_fax=:fac_compta_fax,
			fac_compta_mail=:fac_compta_mail,
			fac_compta_change=:fac_compta_change,
			fac_logo_1=:fac_logo_1,
			fac_logo_2=:fac_logo_2,
			fac_qualite_1=:fac_qualite_1,
			fac_qualite_2=:fac_qualite_2,
			fac_convention=:fac_convention,
			fac_affacturage=:fac_affacturage,
			fac_affacturage_iban=:fac_affacturage_iban
			WHERE fac_id=:fac_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam("fac_id",$facture_id);
			$req->bindParam("fac_agence",$fac_agence);
			$req->bindParam("fac_client",$fac_client);
			$req->bindParam("fac_cli_categorie",$d_client["cli_categorie"]);
			$req->bindParam("fac_cli_sous_categorie",$d_client["cli_sous_categorie"]);
			$req->bindParam("fac_cli_code",$d_client["cli_code"]);
			$req->bindParam("fac_cli_suc",$fac_cli_suc);
			$req->bindParam("fac_commercial",$fac_commercial);
			$req->bindParam("fac_com_identite",$fac_com_identite);
			$req->bindValue("fac_date",$DT_fac_date->format("Y-m-d"));
			$req->bindParam("fac_reference",$_POST["fac_reference"]);
			$req->bindParam("fac_libelle",$_POST["fac_libelle"]);
			$req->bindParam("fac_opca",$fac_opca);
			$req->bindParam("fac_opca_type",$fac_opca_type);
			$req->bindParam("fac_adresse",$fac_adresse);
			$req->bindParam("fac_cli_nom",$d_adresse_fac["adr_nom"]);
			$req->bindParam("fac_opca_compte",$fac_opca_compte);
			$req->bindParam("fac_cli_service",$d_adresse_fac["adr_service"]);
			$req->bindParam("fac_cli_ad1",$d_adresse_fac["adr_ad1"]);
			$req->bindParam("fac_cli_ad2",$d_adresse_fac["adr_ad2"]);
			$req->bindParam("fac_cli_ad3",$d_adresse_fac["adr_ad3"]);
			$req->bindParam("fac_cli_cp",$d_adresse_fac["adr_cp"]);
			$req->bindParam("fac_cli_ville",$d_adresse_fac["adr_ville"]);
			$req->bindParam("fac_cli_geo",$d_adresse_fac["adr_geo"]);
			$req->bindParam("fac_env_adresse",$fac_env_adresse);
			$req->bindParam("fac_env_nom",$d_adresse_env["adr_nom"]);
			$req->bindParam("fac_env_service",$d_adresse_env["adr_service"]);
			$req->bindParam("fac_env_ad1",$d_adresse_env["adr_ad1"]);
			$req->bindParam("fac_env_ad2",$d_adresse_env["adr_ad2"]);
			$req->bindParam("fac_env_ad3",$d_adresse_env["adr_ad3"]);
			$req->bindParam("fac_env_cp",$d_adresse_env["adr_cp"]);
			$req->bindParam("fac_env_ville",$d_adresse_env["adr_ville"]);
			$req->bindParam("fac_soc_nom",$fac_soc_nom);
			$req->bindParam("fac_soc_agence",$fac_soc_agence);
			$req->bindParam("fac_soc_ad1",$fac_soc_ad1);
			$req->bindParam("fac_soc_ad2",$fac_soc_ad2);
			$req->bindParam("fac_soc_ad3",$fac_soc_ad3);
			$req->bindParam("fac_soc_cp",$fac_soc_cp);
			$req->bindParam("fac_soc_ville",$fac_soc_ville);
			$req->bindParam("fac_soc_tel",$fac_soc_tel);
			$req->bindParam("fac_soc_fax",$fac_soc_fax);
			$req->bindParam("fac_soc_mail",$fac_soc_mail);
			$req->bindParam("fac_soc_site",$fac_soc_site);
			$req->bindParam("fac_soc_siret",$fac_soc_siret);
			$req->bindParam("fac_contact",$fac_contact);
			$req->bindParam("fac_con_titre",$fac_con_titre);
			$req->bindParam("fac_con_nom",$fac_con_nom);
			$req->bindParam("fac_con_prenom",$_POST["fac_con_prenom"]);
			$req->bindParam("fac_con_tel",$_POST["fac_con_tel"]);
			$req->bindParam("fac_con_portable",$_POST["fac_con_portable"]);
			$req->bindParam("fac_con_mail",$_POST["fac_con_mail"]);
			$req->bindParam("fac_reg_type",$fac_reg_type);
			$req->bindParam("fac_reg_formule",$fac_reg_formule);
			$req->bindParam("fac_reg_fdm",$fac_reg_fdm);
			$req->bindParam("fac_reg_nb_jour",$fac_reg_nb_jour);
			$req->bindValue("fac_date_reg_prev",$DT_reg->format("Y-m-d"));
			$req->bindParam("fac_rib",$d_rib["rib_id"]);
			$req->bindParam("fac_rib_change",$d_rib["rib_change"]);
			$req->bindParam("fac_rib_iban",$d_rib["rib_iban"]);
			$req->bindParam("fac_rib_bic",$d_rib["rib_bic"]);
			$req->bindParam("fac_compta_nom",$fac_compta_nom);
			$req->bindParam("fac_compta_agence",$fac_compta_agence);
			$req->bindParam("fac_compta_ad1",$fac_compta_ad1);
			$req->bindParam("fac_compta_ad2",$fac_compta_ad2);
			$req->bindParam("fac_compta_ad3",$fac_compta_ad3);
			$req->bindParam("fac_compta_cp",$fac_compta_cp);
			$req->bindParam("fac_compta_ville",$fac_compta_ville);
			$req->bindParam("fac_compta_tel",$fac_compta_tel);
			$req->bindParam("fac_compta_fax",$fac_compta_fax);
			$req->bindParam("fac_compta_mail",$fac_compta_mail);
			$req->bindParam("fac_compta_change",$fac_compta_change);
			$req->bindParam("fac_logo_1",$fac_logo_1);
			$req->bindParam("fac_logo_2",$fac_logo_2);
			$req->bindParam("fac_qualite_1",$fac_qualite_1);
			$req->bindParam("fac_qualite_2",$fac_qualite_2);
			$req->bindParam("fac_convention",$fac_convention);
			$req->bindParam("fac_affacturage",$fac_affacturage);
			$req->bindParam("fac_affacturage_iban",$fac_affacturage_iban);
			try{
				$req->execute();
			}Catch(Exception $e){
				$erreur_txt="UPDATE FAC : " . $e->getMessage();
			}

		}else{

			// CREATION D'UNE FACTURE
            //VERIFICATION DE doublon DE CHRONO
            $sql="SELECT fac_id FROM factures WHERE fac_numero = :fac_numero;";
            $req=$ConnSoc->prepare($sql);
            $req->bindValue(":fac_numero",$fac_numero);
            $req-> execute();
            $fac_chrono_exists = $req->fetch();

            if(!empty($fac_chrono_exists)){
                // DIE IMPOSSIBLE DE CREER LA FACTURE
                $_SESSION['message'][] = array(
                    "titre" => "Erreur",
                    "type" => "danger",
                    "message" => "Impossible de créer la facture : doublon de chrono"
                );
                header("location:" . $_SESSION["retourFacture"]);
                die();
            }




            // FIN
			$sql="INSERT INTO Factures (fac_agence,fac_client,fac_cli_categorie,fac_cli_sous_categorie,fac_cli_code,fac_cli_suc,fac_commercial,fac_com_identite,fac_nature,fac_chrono,fac_numero,fac_date,fac_reference
			,fac_opca,fac_opca_type,fac_adresse,fac_cli_nom,fac_opca_compte,fac_cli_service,fac_cli_ad1,fac_cli_ad2,fac_cli_ad3,fac_cli_cp,fac_cli_ville,fac_cli_geo
			,fac_env_adresse,fac_env_nom,fac_env_service,fac_env_ad1,fac_env_ad2,fac_env_ad3,fac_env_cp,fac_env_ville
			,fac_contact,fac_con_titre,fac_con_nom,fac_con_prenom,fac_con_tel,fac_con_portable,fac_con_mail
			,fac_reg_type,fac_reg_formule,fac_reg_fdm,fac_reg_nb_jour,fac_date_reg_prev,fac_rib,fac_rib_change,fac_rib_iban,fac_rib_bic,fac_libelle
			,fac_soc_nom,fac_soc_agence,fac_soc_ad1,fac_soc_ad2,fac_soc_ad3,fac_soc_cp,fac_soc_ville,fac_soc_tel,fac_soc_fax,fac_soc_mail,fac_soc_site,fac_soc_siret
			,fac_compta_nom,fac_compta_agence,fac_compta_ad1,fac_compta_ad2,fac_compta_ad3,fac_compta_cp,fac_compta_ville,fac_compta_tel,fac_compta_fax,fac_compta_mail,fac_compta_change
			,fac_logo_1,fac_logo_2,fac_qualite_1,fac_qualite_2,fac_avoir,fac_convention,fac_affacturage,fac_affacturage_iban,fac_creation_date";
			if(!empty($id_heracles)){
				$sql.=",fac_id";
			}
			$sql.=")
			VALUES (:fac_agence,:fac_client,:fac_cli_categorie,:fac_cli_sous_categorie,:fac_cli_code,:fac_cli_suc,:fac_commercial,:fac_com_identite,:fac_nature,:fac_chrono,:fac_numero,:fac_date,:fac_reference
			,:fac_opca,:fac_opca_type,:fac_adresse,:fac_cli_nom,:fac_opca_compte,:fac_cli_service,:fac_cli_ad1,:fac_cli_ad2,:fac_cli_ad3,:fac_cli_cp,:fac_cli_ville,:fac_cli_geo
			,:fac_env_adresse,:fac_env_nom,:fac_env_service,:fac_env_ad1,:fac_env_ad2,:fac_env_ad3,:fac_env_cp,:fac_env_ville
			,:fac_contact,:fac_con_titre,:fac_con_nom,:fac_con_prenom,:fac_con_tel,:fac_con_portable,:fac_con_mail
			,:fac_reg_type,:fac_reg_formule,:fac_reg_fdm,:fac_reg_nb_jour,:fac_date_reg_prev,:fac_rib,:fac_rib_change,:fac_rib_iban,:fac_rib_bic,:fac_libelle
			,:fac_soc_nom,:fac_soc_agence,:fac_soc_ad1,:fac_soc_ad2,:fac_soc_ad3,:fac_soc_cp,:fac_soc_ville,:fac_soc_tel,:fac_soc_fax,:fac_soc_mail,:fac_soc_site,:fac_soc_siret
			,:fac_compta_nom,:fac_compta_agence,:fac_compta_ad1,:fac_compta_ad2,:fac_compta_ad3,:fac_compta_cp,:fac_compta_ville,:fac_compta_tel,:fac_compta_fax,:fac_compta_mail,:fac_compta_change
			,:fac_logo_1,:fac_logo_2,:fac_qualite_1,:fac_qualite_2,:fac_avoir,:fac_convention,:fac_affacturage,:fac_affacturage_iban,NOW()";
			if(!empty($id_heracles)){
				$sql.=",:fac_id";
			}
			$sql.=");";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam("fac_agence",$fac_agence);
			$req->bindParam("fac_client",$fac_client);
			$req->bindParam("fac_cli_categorie",$d_client["cli_categorie"]);
			$req->bindParam("fac_cli_sous_categorie",$d_client["cli_sous_categorie"]);
			$req->bindParam("fac_cli_code",$d_client["cli_code"]);
			$req->bindValue("fac_cli_suc",$fac_cli_suc);
			$req->bindParam("fac_commercial",$fac_commercial);
			$req->bindParam("fac_com_identite",$fac_com_identite);
			$req->bindValue("fac_nature",$fac_nature);
			$req->bindParam("fac_chrono",$fac_chrono);
			$req->bindParam("fac_numero",$fac_numero);
			$req->bindValue("fac_date",$DT_fac_date->format("Y-m-d"));
			$req->bindParam("fac_reference",$_POST["fac_reference"]);
			$req->bindParam("fac_opca",$fac_opca);
			$req->bindParam("fac_opca_type",$fac_opca_type);
			$req->bindParam("fac_adresse",$fac_adresse);
			$req->bindParam("fac_cli_nom",$d_adresse_fac["adr_nom"]);
			$req->bindParam("fac_opca_compte",$fac_opca_compte);
			$req->bindParam("fac_cli_service",$d_adresse_fac["adr_service"]);
			$req->bindParam("fac_cli_ad1",$d_adresse_fac["adr_ad1"]);
			$req->bindParam("fac_cli_ad2",$d_adresse_fac["adr_ad2"]);
			$req->bindParam("fac_cli_ad3",$d_adresse_fac["adr_ad3"]);
			$req->bindParam("fac_cli_cp",$d_adresse_fac["adr_cp"]);
			$req->bindParam("fac_cli_ville",$d_adresse_fac["adr_ville"]);
			$req->bindParam("fac_cli_geo",$d_adresse_fac["adr_geo"]);
			$req->bindParam("fac_env_adresse",$fac_env_adresse);
			$req->bindParam("fac_env_nom",$d_adresse_env["adr_nom"]);
			$req->bindParam("fac_env_service",$d_adresse_env["adr_service"]);
			$req->bindParam("fac_env_ad1",$d_adresse_env["adr_ad1"]);
			$req->bindParam("fac_env_ad2",$d_adresse_env["adr_ad2"]);
			$req->bindParam("fac_env_ad3",$d_adresse_env["adr_ad3"]);
			$req->bindParam("fac_env_cp",$d_adresse_env["adr_cp"]);
			$req->bindParam("fac_env_ville",$d_adresse_env["adr_ville"]);
			$req->bindParam("fac_contact",$fac_contact);
			$req->bindParam("fac_con_titre",$fac_con_titre);
			$req->bindParam("fac_con_nom",$fac_con_nom);
			$req->bindParam("fac_con_prenom",$_POST["fac_con_prenom"]);
			$req->bindParam("fac_con_tel",$_POST["fac_con_tel"]);
			$req->bindParam("fac_con_portable",$_POST["fac_con_portable"]);
			$req->bindParam("fac_con_mail",$_POST["fac_con_mail"]);
			$req->bindParam("fac_reg_type",$fac_reg_type);
			$req->bindParam("fac_reg_formule",$fac_reg_formule);
			$req->bindParam("fac_reg_fdm",$fac_reg_fdm);
			$req->bindParam("fac_reg_nb_jour",$fac_reg_nb_jour);
			$req->bindValue("fac_date_reg_prev",$DT_reg->format("Y-m-d"));
			$req->bindParam("fac_rib",$d_rib["rib_id"]);
			$req->bindParam("fac_rib_change",$d_rib["rib_change"]);
			$req->bindParam("fac_libelle",$_POST["fac_libelle"]);
			$req->bindParam("fac_soc_nom",$fac_soc_nom);
			$req->bindParam("fac_soc_agence",$fac_soc_agence);
			$req->bindParam("fac_soc_ad1",$fac_soc_ad1);
			$req->bindParam("fac_soc_ad2",$fac_soc_ad2);
			$req->bindParam("fac_soc_ad3",$fac_soc_ad3);
			$req->bindParam("fac_soc_cp",$fac_soc_cp);
			$req->bindParam("fac_soc_ville",$fac_soc_ville);
			$req->bindParam("fac_soc_tel",$fac_soc_tel);
			$req->bindParam("fac_soc_fax",$fac_soc_fax);
			$req->bindParam("fac_soc_mail",$fac_soc_mail);
			$req->bindParam("fac_soc_site",$fac_soc_site);
			$req->bindParam("fac_soc_siret",$fac_soc_siret);
			$req->bindParam("fac_rib_iban",$d_rib["rib_iban"]);
			$req->bindParam("fac_rib_bic",$d_rib["rib_bic"]);
			$req->bindParam("fac_compta_nom",$fac_compta_nom);
			$req->bindParam("fac_compta_agence",$fac_compta_agence);
			$req->bindParam("fac_compta_ad1",$fac_compta_ad1);
			$req->bindParam("fac_compta_ad2",$fac_compta_ad2);
			$req->bindParam("fac_compta_ad3",$fac_compta_ad3);
			$req->bindParam("fac_compta_cp",$fac_compta_cp);
			$req->bindParam("fac_compta_ville",$fac_compta_ville);
			$req->bindParam("fac_compta_tel",$fac_compta_tel);
			$req->bindParam("fac_compta_fax",$fac_compta_fax);
			$req->bindParam("fac_compta_mail",$fac_compta_mail);
			$req->bindParam("fac_compta_change",$fac_compta_change);
			$req->bindParam("fac_logo_1",$fac_logo_1);
			$req->bindParam("fac_logo_2",$fac_logo_2);
			$req->bindParam("fac_qualite_1",$fac_qualite_1);
			$req->bindParam("fac_qualite_2",$fac_qualite_2);
			$req->bindParam("fac_avoir",$avoir_id);
			$req->bindParam("fac_convention",$fac_convention);
			$req->bindParam("fac_affacturage",$fac_affacturage);
			$req->bindParam("fac_affacturage_iban",$fac_affacturage_iban);
			if(!empty($id_heracles)){
				$req->bindParam("fac_id",$id_heracles);
			}
			try{
				$req->execute();
				$facture_id=$ConnSoc->lastInsertId();
			}Catch(Exception $e){
				$erreur_txt="ADD FAC : " . $e->getMessage();
			}

			// la memo de la première facture se fait au moment de la génération du PDF -> avant la date peut etre modifié
			// donc cli_first_facture_date peut changer
		}


		// MISE A JOUR DES LIGNES
		if(empty($erreur_txt)){

			$fac_total_ht=0;
			$fac_total_ttc=0;

			$produit_manquant=false;

			if(!empty($_POST["num_ligne"])){

				// PREP REQ

				$sql_add_ligne="INSERT INTO Factures_Lignes (fli_facture,fli_produit,fli_code_produit,fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,fli_action_client,fli_action_cli_soc
				,fli_intra_inter,fli_libelle,fli_formation,fli_complement,fli_stagiaire,fli_ht,fli_qte,fli_remise,fli_taux_remise,fli_montant_ht,fli_tva_id,fli_tva_taux,fli_tva_per,fli_exo,fli_vehicule,fli_opca_num,fli_bc_num
				) VALUES (
				:fli_facture,:fli_produit,:fli_code_produit,:fli_categorie,:fli_famille,:fli_sous_famille,:fli_sous_sous_famille,:fli_action_client,:fli_action_cli_soc
				,:fli_intra_inter,:fli_libelle,:fli_formation,:fli_complement,:fli_stagiaire,:fli_ht,:fli_qte,:fli_remise,:fli_taux_remise,:fli_montant_ht,:fli_tva_id,:fli_tva_taux,:fli_tva_per,:fli_exo,:fli_vehicule,:fli_opca_num,:fli_bc_num);";
				$req_add_ligne=$ConnSoc->prepare($sql_add_ligne);

				$sql_up_ligne="UPDATE Factures_Lignes SET
				fli_produit=:fli_produit,
				fli_code_produit=:fli_code_produit,
				fli_categorie=:fli_categorie,
				fli_famille=:fli_famille,
				fli_sous_famille=:fli_sous_famille,
				fli_sous_sous_famille=:fli_sous_sous_famille,
				fli_action_client=:fli_action_client,
				fli_action_cli_soc=:fli_action_cli_soc,
				fli_intra_inter=:fli_intra_inter,
				fli_libelle=:fli_libelle,
				fli_formation=:fli_formation,
				fli_complement=:fli_complement,
				fli_stagiaire=:fli_stagiaire,
				fli_ht=:fli_ht,
				fli_qte=:fli_qte,
				fli_remise=:fli_remise,
				fli_taux_remise=:fli_taux_remise,
				fli_montant_ht=:fli_montant_ht,
				fli_tva_id=:fli_tva_id,
				fli_tva_taux=:fli_tva_taux,
				fli_tva_per=:fli_tva_per,
				fli_exo=:fli_exo,
				fli_vehicule=:fli_vehicule,
				fli_opca_num=:fli_opca_num,
				fli_bc_num=:fli_bc_num
				WHERE fli_id=:fli_id AND fli_facture=:fli_facture;";
				$req_up_ligne=$ConnSoc->prepare($sql_up_ligne);

				$sql_del_ligne="DELETE FROM Factures_Lignes WHERE fli_id=:fli_id AND fli_facture=:fli_facture;";
				$req_del_ligne=$ConnSoc->prepare($sql_del_ligne);

				foreach($_POST["num_ligne"] as $num_ligne){

					$fli_id=0;
					if(!empty($_POST["fli_id_" . $num_ligne])){
						$fli_id=intval($_POST["fli_id_" . $num_ligne]);
					}

					$choix=false;
					if(isset($_POST["choix_" . $num_ligne])){
						if(!empty($_POST["choix_" . $num_ligne])){
							$choix=true;
						}
					}

					$fli_action_client=0;
					$fli_action_cli_soc=0;
					if(!empty($_POST["fli_action_client_" . $num_ligne])){
						$fli_action_client=intval($_POST["fli_action_client_" . $num_ligne]);
						if(!empty($_POST["fli_action_cli_soc_" . $num_ligne])){
							$fli_action_cli_soc=intval($_POST["fli_action_cli_soc_" . $num_ligne]);
						}
					}

					$actualiser=false;


					if($choix){

						// LA LIGNE EST COCHE
						$fli_produit=0;
						if(!empty($_POST["fli_produit_" . $num_ligne])){
							$fli_produit=intval($_POST["fli_produit_" . $num_ligne]);
						}

						IF(!empty($fli_produit)){

							if(empty($tab_produit[$fli_produit])){
								$produit_manquant=true;
							}else{

								$fli_intra_inter=1;
								if(!empty($_POST["fli_intra_inter_" . $num_ligne])){
									$fli_intra_inter=intval($_POST["fli_intra_inter_" . $num_ligne]);
								}


								// gestion de la tva
								$fli_exo=0;
								if($d_societe["soc_tva_exo"]==1 AND $tab_produit[$fli_produit]["deductible"]){
									$fli_exo=1;
									$fli_tva_id=3;
									$fli_tva_per=3;
									$fli_tva_taux=0;
								}else{

									if($_SESSION['acces']["acc_droits"][9]){
										$fli_tva_id=0;
										if(!empty($_POST["fli_tva_id_" . $num_ligne])){
											$fli_tva_id=intval($_POST["fli_tva_id_" . $num_ligne]);
										}
										/*var_dump($fli_tva_id);
										echo("<br/>");*/
									}else{
										$fli_tva_id=$tab_produit[$fli_produit]["tva"];
									}


									$fli_tva_per=$tab_tva[$fli_tva_id]["tpe_id"];
									$fli_tva_taux=$tab_tva[$fli_tva_id]["tpe_taux"];

								}

								//die();

								$fli_ht=0;
								if(!empty($_POST["fli_ht_" . $num_ligne])){
									$fli_ht=floatval($_POST["fli_ht_" . $num_ligne]);
								}
								$fli_qte=0;
								if(!empty($_POST["fli_qte_" . $num_ligne])){
									$fli_qte=floatval($_POST["fli_qte_" . $num_ligne]);
								}
								$fli_montant_ht=$fli_ht*$fli_qte;
								$fli_montant_ht=round($fli_montant_ht,2);

								$fac_total_ht=$fac_total_ht+$fli_montant_ht;

								$tva=$fli_montant_ht*($fli_tva_taux/100);
								$tva=round($tva,2);

								$fac_total_ttc=$fac_total_ttc+$fli_montant_ht+$tva;

								// texte ligne

								$fli_formation="";
								if(!empty($_POST["fli_formation_" . $num_ligne])){
									$fli_formation=trim($_POST["fli_formation_" . $num_ligne]);
									$fli_formation=str_replace('"',"''",$fli_formation);
                                    $fli_formation = str_replace('', "'", $fli_formation);
								}
								$fli_complement="";
								if(!empty($_POST["fli_complement_" . $num_ligne])){
									$fli_complement=trim($_POST["fli_complement_" . $num_ligne]);
									$fli_complement=str_replace('"',"''",$fli_complement);
                                    $fli_complement = str_replace('', "'", $fli_complement);
								}
								$fli_stagiaire="";
								if(!empty($_POST["fli_stagiaire_" . $num_ligne])){
									$fli_stagiaire=trim($_POST["fli_stagiaire_" . $num_ligne]);
									$fli_stagiaire=str_replace('"',"''",$fli_stagiaire);
                                    $fli_stagiaire = str_replace('', "'", $fli_stagiaire);
								}


								$fli_vehicule=0;

								if(!empty($_POST["fli_id_" . $num_ligne])){

									// LA LIGNE ETAIT DEJA LIE A LA FAC -> UPDATE

									//echo("UPDATE<br/>");

									$req_up_ligne->bindParam(":fli_produit",$fli_produit);
									$req_up_ligne->bindParam(":fli_code_produit",$tab_produit[$fli_produit]["reference"]);
									$req_up_ligne->bindParam(":fli_categorie",$tab_produit[$fli_produit]["categorie"]);
									$req_up_ligne->bindParam(":fli_famille",$tab_produit[$fli_produit]["famille"]);
									$req_up_ligne->bindParam(":fli_sous_famille",$tab_produit[$fli_produit]["sous_famille"]);
									$req_up_ligne->bindParam(":fli_sous_sous_famille",$tab_produit[$fli_produit]["sous_sous_famille"]);
									$req_up_ligne->bindParam(":fli_action_client",$fli_action_client);
									$req_up_ligne->bindParam(":fli_action_cli_soc",$fli_action_cli_soc);
									$req_up_ligne->bindParam(":fli_intra_inter",$fli_intra_inter);
									$req_up_ligne->bindParam(":fli_libelle",$_POST["fli_libelle_" . $num_ligne]);
									$req_up_ligne->bindParam(":fli_formation",$fli_formation);
									$req_up_ligne->bindParam(":fli_complement",$fli_complement);
									$req_up_ligne->bindParam(":fli_stagiaire",$fli_stagiaire);
									$req_up_ligne->bindParam(":fli_ht",$fli_ht);
									$req_up_ligne->bindParam(":fli_qte",$fli_qte);
									$req_up_ligne->bindValue(":fli_remise",0);
									$req_up_ligne->bindValue(":fli_taux_remise",0);
									$req_up_ligne->bindParam(":fli_montant_ht",$fli_montant_ht);
									$req_up_ligne->bindParam(":fli_tva_id",$fli_tva_id);
									$req_up_ligne->bindParam(":fli_tva_taux",$fli_tva_taux);
									$req_up_ligne->bindParam(":fli_tva_per",$fli_tva_per);
									$req_up_ligne->bindParam(":fli_exo",$fli_exo);
									$req_up_ligne->bindParam(":fli_vehicule",$fli_vehicule);
									$req_up_ligne->bindParam(":fli_id",$fli_id);
									$req_up_ligne->bindParam(":fli_opca_num",$_POST["fli_opca_num_" . $num_ligne]);
									$req_up_ligne->bindParam(":fli_bc_num",$_POST["fli_bc_num_" . $num_ligne]);
									$req_up_ligne->bindParam(":fli_id",$fli_id);
									$req_up_ligne->bindParam(":fli_facture",$facture_id);

									$req_up_ligne->execute();

									$actualiser=true;


								}else{

									// LIGNE AJOUTE PAR U -> ADD

								//	echo("ADD<br/>");

									$req_add_ligne->bindParam("fli_facture",$facture_id);
									$req_add_ligne->bindParam("fli_produit",$fli_produit);
									$req_add_ligne->bindParam("fli_code_produit",$tab_produit[$fli_produit]["reference"]);
									$req_add_ligne->bindParam("fli_categorie",$tab_produit[$fli_produit]["categorie"]);
									$req_add_ligne->bindParam("fli_famille",$tab_produit[$fli_produit]["famille"]);
									$req_add_ligne->bindParam("fli_sous_famille",$tab_produit[$fli_produit]["sous_famille"]);
									$req_add_ligne->bindParam("fli_sous_sous_famille",$tab_produit[$fli_produit]["sous_sous_famille"]);
									$req_add_ligne->bindParam("fli_action_client",$fli_action_client);
									$req_add_ligne->bindParam("fli_action_cli_soc",$fli_action_cli_soc);
									$req_add_ligne->bindParam("fli_intra_inter",$fli_intra_inter);
									$req_add_ligne->bindParam("fli_libelle",$_POST["fli_libelle_" . $num_ligne]);
									$req_add_ligne->bindParam(":fli_formation",$fli_formation);
									$req_add_ligne->bindParam(":fli_complement",$fli_complement);
									$req_add_ligne->bindParam(":fli_stagiaire",$fli_stagiaire);
									$req_add_ligne->bindParam("fli_ht",$fli_ht);
									$req_add_ligne->bindParam("fli_qte",$fli_qte);
									$req_add_ligne->bindValue("fli_remise",0);
									$req_add_ligne->bindValue("fli_taux_remise",0);
									$req_add_ligne->bindParam("fli_montant_ht",$fli_montant_ht);
									$req_add_ligne->bindParam("fli_tva_id",$fli_tva_id);
									$req_add_ligne->bindParam("fli_tva_taux",$fli_tva_taux);
									$req_add_ligne->bindParam("fli_tva_per",$fli_tva_per);
									$req_add_ligne->bindParam("fli_exo",$fli_exo);
									$req_add_ligne->bindParam("fli_vehicule",$fli_vehicule);
									$req_add_ligne->bindParam(":fli_opca_num",$_POST["fli_opca_num_" . $num_ligne]);
									$req_add_ligne->bindParam(":fli_bc_num",$_POST["fli_bc_num_" . $num_ligne]);
									$req_add_ligne->execute();

									$actualiser=true;


								}
							}


						}
					}elseif(!empty($_POST["fli_id_" . $num_ligne])){

						//echo("DEL<br/>");

						// LA LIGNE ETAIT LIE A LA FAC ET U LA SUPP

						$req_del_ligne->bindParam("fli_id",$fli_id);
						$req_del_ligne->bindParam("fli_facture",$facture_id);
						$req_del_ligne->execute();

						$actualiser=true;

					}

					// MAJ DU CA DE L'ACTION CLIENT
					if($actualiser AND !empty($fli_action_client)){

						/*var_dump($fli_action_client);
						echo("<br/>");*/

						$acl_facture_ht=0;

						// montant deja facturé
						$sql_ligne_get="SELECT SUM(fli_montant_ht) as ht FROM Factures_Lignes WHERE fli_action_client=:fli_action_client";
						if(!empty($fli_action_cli_soc)){
							$sql_ligne_get.=" AND fli_action_cli_soc=" . $fli_action_cli_soc;
						}
						$sql_ligne_get.=";";
						$req_ligne_get=$ConnSoc->prepare($sql_ligne_get);
						$req_ligne_get->bindParam("fli_action_client",$fli_action_client);
						$req_ligne_get->execute();
						$d_facturation=$req_ligne_get->fetch();
						if(!empty($d_facturation)){
							if(!empty($d_facturation["ht"])){
								$acl_facture_ht=$d_facturation["ht"];
								$acl_facture_ht=round($acl_facture_ht,2);
							}
						}

						/*var_dump($acl_facture_ht);
						echo("<br/>");*/

						// montant devant etre facturée

						$acl_facture=0;

						if(empty($fli_action_cli_soc) OR $fli_action_cli_soc==$acc_societe){

							$sql_act_cli_get="SELECT acl_ca, acl_id FROM Actions_Clients WHERE acl_id=:fli_action_client;";
							$req_act_cli_get=$ConnSoc->prepare($sql_act_cli_get);
							$req_act_cli_get->bindParam("fli_action_client",$fli_action_client);
							$req_act_cli_get->execute();
							$d_action_client=$req_act_cli_get->fetch();
							if(!empty($d_action_client)){
								if($d_action_client["acl_ca"]==$acl_facture_ht){
									$acl_facture=1;
								}
							}

							$sql_action_up="UPDATE Actions_Clients SET acl_facture_ht=:acl_facture_ht,acl_facture=:acl_facture WHERE acl_id=:acl_id;";
							$req_action_up=$ConnSoc->prepare($sql_action_up);
							$req_action_up->bindParam("acl_facture_ht",$acl_facture_ht);
							$req_action_up->bindParam("acl_facture",$acl_facture);
							$req_action_up->bindParam("acl_id",$fli_action_client);
							$req_action_up->execute();

						}else{

							//echo("FAC GFC<br/>");

							// FACTURATION SUR GFC D'UNE ACTION REGION

							$ConnFct=connexion_fct($fli_action_cli_soc);

							$sql_act_cli_get="SELECT acl_ca_gfc, acl_id FROM Actions_Clients WHERE acl_id=:fli_action_client;";
							$req_act_cli_get=$ConnFct->prepare($sql_act_cli_get);
							$req_act_cli_get->bindParam("fli_action_client",$fli_action_client);
							$req_act_cli_get->execute();
							$d_action_client=$req_act_cli_get->fetch();
							if(!empty($d_action_client)){
								if($d_action_client["acl_ca_gfc"]==$acl_facture_ht){
									$acl_facture=1;
								}
							}

							$sql_action_up="UPDATE Actions_Clients SET acl_facture_gfc_ht=:acl_facture_gfc_ht,acl_facture_gfc=:acl_facture_gfc WHERE acl_id=:acl_id;";
							$req_action_up=$ConnFct->prepare($sql_action_up);
							$req_action_up->bindParam("acl_facture_gfc_ht",$acl_facture_ht);
							$req_action_up->bindParam("acl_facture_gfc",$acl_facture);
							$req_action_up->bindParam("acl_id",$fli_action_client);
							$req_action_up->execute();

						}

					}
				}
				// fin de la boucle sur les lignes
			}


			// MAJ DE LA FACTURE EN FONCTION DES LIGNES

			$sql="UPDATE Factures SET
			fac_total_ht=:fac_total_ht,
			fac_total_ttc=:fac_total_ttc
			WHERE fac_id=:fac_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam("fac_total_ht",$fac_total_ht);
			$req->bindParam("fac_total_ttc",$fac_total_ttc);
			$req->bindParam("fac_id",$facture_id);
			try{
				$req->execute();
			}Catch(Exception $e){
				$erreur_txt="FAC TOTAL : " . $e->getMessage();
			}

		}



    }
	// FIN TRAITEMENt

	//die();

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
	}elseif($produit_manquant){
		$_SESSION['message'][] = array(
			"titre" => "Attention",
			"type" => "warning",
			"message" => "Certaines lignes factures n'ont pas été enregistrées car les données produit sont indisponibles!"
		);
	}

	if(!empty($erreur_txt)){

		header("location:" . $_SESSION["retourFacture"]);
	}else{
		header("location: facture_voir.php?facture=" . $facture_id);
	}

	die();
?>
