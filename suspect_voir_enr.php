<?php
include "includes/controle_acces.inc.php";

include("includes/connexion.php");
include("connexion_soc.php");
include("modeles/mod_client.php");
include("modeles/mod_contact.php");
include("modeles/mod_adresse.php");
include("modeles/mod_parametre.php");
include("modeles/mod_correspondance.php");

// modification adresse facturation par défaut

if(isset($_POST['submit1'])){
$client = get_client($_GET['client'], "sus"); 
if(!empty($_POST['cli_siret2'])){


	    $sql3="SELECT * FROM suspects_adresses WHERE sad_siret = :siret AND sad_ref_id != " . $_GET['client'];

	    $req3 = $ConnSoc->prepare($sql3);
	    $req3->bindParam("siret",$_POST['cli_siret2']);
	    $req3->execute();
	    $sirets = $req3->fetchAll();

	    $sql4="SELECT * FROM adresses WHERE adr_siret = :sireti";

	    $req4 = $ConnSoc->prepare($sql4);
	    $req4->bindParam("sireti",$_POST['cli_siret2']);
	    $req4->execute();
	    $sirets2 = $req4->fetchAll();

	    if(empty($sirets) && empty($sirets2)){
	        
	    }else{
	        Header("Location: suspect_voir.php?client=" . $_GET['client'] . "&error=15&search_adresse");
	        exit();
	    }
	}

if($client['sus_categorie'] == 3){
	$_POST['cli_siret2'] = 0;
	$_POST['cli_siren'] = 0;
}
	if(empty($client['sus_siren']) && isset($_POST['cli_siren'])){
		update_suspect_siren($_GET['client'], $_POST['cli_siren']);
	}
	update_adresse_suspect($_GET['adresse'],$_POST['sad_nom'],$_POST['sad_service'],$_POST['sad_ad1'],$_POST['sad_ad2'],$_POST['sad_ad3'],$_POST['sad_cp'],$_POST['sad_ville'],1,$_POST['sad_libelle'], $_POST['cli_geo'] ,$_POST['cli_siret2']);

	Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&succes=1");

}
// delete correspondance 

if(isset($_GET['submit6'])){
	delete_correspondance($_GET['cor']);
	Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&search_cor&succes=14");
}
// insertion correspondance

if(isset($_POST['submit2'])){
	// ajout correspondance
	if(!isset($_POST['sco_id'])){



		if($_POST['sco_contact'] == "nouveau"){

			if($_POST['con_fonction'] == 'autre'){
				$_POST['con_fonction'] = 0;
			}
			$contact_existe = get_contact_suspect($_GET['client'], 1);

			if(!empty($contact_existe)){
				$contact = insert_suspect_contact($_GET['client'],$_GET['categorie'], $_POST['con_fonction'], $_POST['con_fonction_nom'],$_POST['sco_titre'],$_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail'],0,1);
			}else{
				$contact = insert_suspect_contact($_GET['client'],$_GET['categorie'], $_POST['con_fonction'], $_POST['con_fonction_nom'],$_POST['sco_titre'],$_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail'],1,1);
			}
			
			$rap = convert_date_sql($_POST['sco_rappeler_le']);
			$date = convert_date_sql($_POST['sco_date']);
			insert_correspondance($_SESSION['acces']['acc_ref_id'], $_POST['sco_commentaire'], $_POST['sco_tel'], $contact, "", $rap, $_GET['client'], $date);


		}elseif($_POST['sco_contact'] == 0){

			$rap = convert_date_sql($_POST['sco_rappeler_le']);
			$date = convert_date_sql($_POST['sco_date']);
			insert_correspondance($_SESSION['acces']['acc_ref_id'], $_POST['sco_commentaire'], $_POST['sco_tel'], 0, $_POST['sco_nom'], $rap, $_GET['client'], $date );
		}else{
			$rap = convert_date_sql($_POST['sco_rappeler_le']);
			$date = convert_date_sql($_POST['sco_date']);

			$req = $ConnSoc->prepare("SELECT * FROM suspects_correspondances WHERE sco_suspect = :sco_suspect AND sco_contact = :sco_contact;");

			$req->bindParam("sco_suspect", $_GET['client']);
			$req->bindParam("sco_contact", $_POST['sco_contact']);
			$req->execute();

			$corr = $req->fetchAll();

			if(!empty($corr)){
				foreach($corr as $c){
					$req = $ConnSoc->prepare("UPDATE suspects_correspondances SET sco_date=:sco_date, sco_rappel = 1 WHERE sco_id = :sco_id;");

					$req->bindParam("sco_date",$date);
					$req->bindParam("sco_id",$c['sco_id']);
					$req->execute();
				}
			}

			
			insert_correspondance($_SESSION['acces']['acc_ref_id'], $_POST['sco_commentaire'], $_POST['sco_tel'], $_POST['sco_contact'], $_POST['sco_nom'], $rap, $_GET['client'], $date);
			if($_POST['con_fonction'] == 'autre'){
				$_POST['con_fonction'] = 0;
			}
			update_contact_suspect_2($_POST['sco_contact'], $_POST['con_fonction'], $_POST['con_fonction_nom'], $_POST['sco_titre'], $_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail']);
		}

		Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&search_cor&succes=12");
	// edition correspondance
	}else{
		if($_POST['sco_contact'] == "nouveau"){
			if($_POST['con_fonction'] == 'autre'){
				$_POST['con_fonction'] = 0;
			}
			$contact = insert_suspect_contact($_GET['client'],$_GET['categorie'], $_POST['con_fonction'], $_POST['con_fonction_nom'],$_POST['sco_titre'],$_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail'],0,0);
			$rap = convert_date_sql($_POST['sco_rappeler_le']);
			$date = convert_date_sql($_POST['sco_date']);
			if(isset($_POST['sco_rappel']) && $_POST['sco_rappel'] == "sco_rappel"){
				$sco_rappel = 1;
			}else{
				$sco_rappel = 0;
			}
			update_correspondance($_POST['sco_id'], $_SESSION['acces']['acc_ref_id'], $_POST['sco_commentaire'], $_POST['sco_tel'], $contact, "", $rap, $_GET['client'],$sco_rappel, $date);

		}elseif($_POST['sco_contact'] == 0){

			$rap = convert_date_sql($_POST['sco_rappeler_le']);
			if(isset($_POST['sco_rappel']) && $_POST['sco_rappel'] == "sco_rappel"){
				$sco_rappel = 1;
			}else{
				$sco_rappel = 0;
			}
			$date = convert_date_sql($_POST['sco_date']);
			update_correspondance($_POST['sco_id'], $_SESSION['acces']['acc_ref_id'], $_POST['sco_commentaire'], $_POST['sco_tel'], 0, $_POST['sco_nom'], $rap, $_GET['client'], $sco_rappel, $date);
		}else{

			$date = convert_date_sql($_POST['sco_date']);
			$rap = convert_date_sql($_POST['sco_rappeler_le']);
			if(isset($_POST['sco_rappel']) && $_POST['sco_rappel'] == "sco_rappel"){
				$sco_rappel = 1;
			}else{
				$sco_rappel = 0;
			}
			update_correspondance($_POST['sco_id'], $_SESSION['acces']['acc_ref_id'], $_POST['sco_commentaire'], $_POST['sco_tel'], $_POST['sco_contact'], $_POST['sco_nom'], $rap, $_GET['client'], $sco_rappel, $date);
			if($_POST['con_fonction'] == 'autre'){
				$_POST['con_fonction'] = 0;
			}
			update_contact_suspect_2($_POST['sco_contact'], $_POST['con_fonction'], $_POST['con_fonction_nom'], $_POST['sco_titre'], $_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail']);
		}

		Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&search_cor&succes=13");
	}

}
// update contact
if(isset($_POST['submit3'])){
	if($_POST['sco_fonction'] == "autre"){
		$_POST['sco_fonction'] = 0;
	}
	update_contact_suspect($_GET['contact'],$_POST['sco_fonction'],$_POST['sco_fonction_nom'],$_POST['sco_titre'],$_POST['sco_nom'],$_POST['sco_prenom'],$_POST['sco_tel'],$_POST['sco_portable'],$_POST['sco_fax'], $_POST['sco_mail'],1);

	Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&succes=2");
}
// ajout d'adresse
if(isset($_POST['submit4'])){

	if(!empty($_POST['cli_siret2'])){


	    $sql3="SELECT * FROM suspects_adresses WHERE sad_siret = :siret AND sad_ref_id != " . $_GET['client'];

	    $req3 = $ConnSoc->prepare($sql3);
	    $req3->bindParam("siret",$_POST['cli_siret2']);
	    $req3->execute();
	    $sirets = $req3->fetchAll();

	    $sql4="SELECT * FROM adresses WHERE adr_siret = :sireti";

	    $req4 = $ConnSoc->prepare($sql4);
	    $req4->bindParam("sireti",$_POST['cli_siret2']);
	    $req4->execute();
	    $sirets2 = $req4->fetchAll();

	    if(empty($sirets) && empty($sirets2)){
	        
	    }else{
	        Header("Location: suspect_voir.php?client=" . $_GET['client'] . "&error=15&search_adresse");
	        exit();
	    }
	}
	$client = get_client($_GET['client'], "sus");

	if($client['sus_categorie'] == 3){
	$_POST['cli_siret2'] = 0;
	$_POST['cli_siren'] = 0;
	}

	if(empty($client['sus_siren'])){
			update_suspect_siren($_GET['client'], $_POST['cli_siren']);
	}
	if(isset($_POST['sad_defaut']) && $_POST['sad_defaut'] == "sad_defaut"){
		$adresses = get_adresses_suspects($_GET['client'], $_POST['sad_type']);
		foreach($adresses as $a){
			update_adresse_suspect_defaut($a['sad_id'],0);
		}

		

		insert_suspect_adresse($_GET['categorie'],$_GET['client'], $_POST['sad_type'], $_POST['sad_nom'],$_POST['sad_service'],$_POST['sad_ad1'],$_POST['sad_ad2'],$_POST['sad_ad3'],$_POST['sad_cp'],$_POST['sad_ville'],1,$_POST['sad_libelle'] , $_POST['cli_geo'] ,$_POST['cli_siret2']);
	}else{
		insert_suspect_adresse($_GET['categorie'],$_GET['client'], $_POST['sad_type'], $_POST['sad_nom'],$_POST['sad_service'],$_POST['sad_ad1'],$_POST['sad_ad2'],$_POST['sad_ad3'],$_POST['sad_cp'],$_POST['sad_ville'],0,$_POST['sad_libelle'], $_POST['cli_geo'] ,$_POST['cli_siret2']);
	}
	Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&search_adresse&succes=3");
}

// ajout contact
if(isset($_POST['submit5'])){
	if($_POST['sco_fonction'] == 'autre'){
			$_POST['sco_fonction'] =0;
		}
	if(isset($_POST['sco_defaut']) && $_POST['sco_defaut'] == "sco_defaut"){
		$contacts = get_contacts_suspects($_GET['client']);
		foreach($contacts as $c){
			update_contact_suspect_defaut($c['sco_id'],0);
		}

		insert_suspect_contact($_GET['client'],$_GET['categorie'], $_POST['sco_fonction'], $_POST['sco_fonction_nom'],$_POST['sco_titre'],$_POST['sco_nom'],$_POST['sco_prenom'],$_POST['sco_tel'],$_POST['sco_portable'],$_POST['sco_fax'],$_POST['sco_mail'],1,1);
	}else{
		insert_suspect_contact($_GET['client'],$_GET['categorie'], $_POST['sco_fonction'], $_POST['sco_fonction_nom'],$_POST['sco_titre'],$_POST['sco_nom'],$_POST['sco_prenom'],$_POST['sco_tel'],$_POST['sco_portable'],$_POST['sco_fax'],$_POST['sco_mail'],0,1);
	}

	
	Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&succes=4&search_contact");
}
// editier adresse normale
if(isset($_POST['submit6'])){
	$client = get_client($_GET['client'], "sus");

	if(!empty($_POST['cli_siret2'])){


	    $sql3="SELECT * FROM suspects_adresses WHERE sad_siret = :siret AND sad_ref_id != " . $_GET['client'];

	    $req3 = $ConnSoc->prepare($sql3);
	    $req3->bindParam("siret",$_POST['cli_siret2']);
	    $req3->execute();
	    $sirets = $req3->fetchAll();

	    $sql4="SELECT * FROM adresses WHERE adr_siret = :sireti";

	    $req4 = $ConnSoc->prepare($sql4);
	    $req4->bindParam("sireti",$_POST['cli_siret2']);
	    $req4->execute();
	    $sirets2 = $req4->fetchAll();

	    if(empty($sirets) && empty($sirets2)){
	        
	    }else{
	        Header("Location: suspect_voir.php?client=" . $_GET['client'] . "&error=15&search_adresse");
	        exit();
	    }
	}
	if(empty($client['sus_siren'])){
			update_suspect_siren($_GET['client'], $_POST['cli_siren']);
	}
	if(isset($_POST['sad_defaut']) && $_POST['sad_defaut'] == "sad_defaut"){
		$adresses = get_adresses_suspects($_GET['client'], $_POST['sad_type']);
		foreach($adresses as $a){
			update_adresse_suspect_defaut($a['sad_id'], 0);
		}
		$defaut = 1;
	}else{
		$defaut = 0;
	}
	update_adresse_suspect($_GET['adresse'],$_POST['sad_nom'],$_POST['sad_service'],$_POST['sad_ad1'],$_POST['sad_ad2'],$_POST['sad_ad3'],$_POST['sad_cp'],$_POST['sad_ville'],$defaut,$_POST['sad_libelle'], $_POST['cli_geo'] ,$_POST['cli_siret2']);

	Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&succes=1&search_adresse");

}
// update contact
if(isset($_POST['submit7'])){
	if($_POST['sco_fonction'] == "autre"){
		$_POST['sco_fonction'] = 0;
	}
	if(isset($_POST['sco_defaut']) && $_POST['sco_defaut'] == "sco_defaut"){
		$contacts = get_contacts_suspects($_GET['client']);
		foreach($contacts as $c){
			update_contact_suspect_defaut($c['sco_id'],0);
		}
		$defaut = 1;
	}else{
		$defaut = 0;
	}


	update_contact_suspect($_GET['contact'],$_POST['sco_fonction'],$_POST['sco_fonction_nom'],$_POST['sco_titre'],$_POST['sco_nom'],$_POST['sco_prenom'],$_POST['sco_tel'],$_POST['sco_portable'],$_POST['sco_fax'], $_POST['sco_mail'],$defaut);

	Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&succes=5&search_contact=1");
}
// supprimer adresse
if(isset($_GET['submit8'])){
	delete_suspect_adresse($_GET['adresse']);
	Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&succes=7&search_adresse");
}
// supprimer contact
if(isset($_GET['submit9'])){
	delete_suspect_contact($_GET['contact']);
	Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&succes=8&search_contact");
}

// ajouter/editer info
if(isset($_POST['submit10'])){
	if(!isset($_POST['sin_id'])){
		insert_client_info($_POST['sin_type'], $_POST['sin_description'], $_POST['sin_famille'], $_POST['sin_sous_famille'], $_SESSION['acces']['acc_ref_id'], $_GET['client']);
		Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&succes=9&tab6");
	}else{
		update_client_info($_POST['sin_id'], $_POST['sin_type'], $_POST['sin_description'], $_POST['sin_famille'], $_POST['sin_sous_famille'], $_GET['client']);
		Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&succes=10&tab6");
	}
	
}
// supprimer info
if(isset($_GET['submit11'])){
	delete_suspect_info($_GET['info']);
	Header("Location: suspect_voir.php?client=" . $_GET['client'] ."&succes=11&tab6");
}
