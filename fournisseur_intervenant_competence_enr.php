<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "modeles/mod_competence.php";
	include "modeles/mod_upload.php";
	include "modeles/mod_parametre.php";



	$erreur=0;
	
	$ref=0;
	if(isset($_POST["ref"])){
		$ref=$_POST["ref"];
	}else{
		$erreur=1;
	}
	
	$ref_id=0;
	if(isset($_POST["ref_id"])){
		$ref_id=$_POST["ref_id"];
	}else{
		$erreur=1;
	}
	
	$competence=0;
	if(isset($_POST["competence"])){
		$competence=$_POST["competence"];
	}else{
		$erreur=1;
	}

	$fournisseur=0;
	if(isset($_POST["fournisseur"])){
		$fournisseur=$_POST["fournisseur"];
	}else{
		$erreur=1;
	}
	$valide=0;
	if(!empty($_POST["ico_valide"])){
		$valide=1;
	}else{
		$valide=0;
	}
	


	
	if($erreur==0){

		// ON INSERT LA COMPETENCE POUR L'UTILISATEUR SEULEMENT SI ON A FORCE LA COMPETENCE
		if($valide == 1){ // si ico_valide = 1

			$sql="SELECT * FROM intervenants_competences WHERE ico_competence = " . $competence . " AND ico_ref_id = " . $ref_id . " AND ico_ref = " . $ref;
			$req=$Conn->query($sql);
			$intervenant_competence = $req->fetch();
			if(empty($intervenant_competence)){
				// on insert la competence liée à l'intervenant
				$sql="INSERT INTO Intervenants_competences  (ico_ref,ico_ref_id,ico_competence,ico_valide)
				 VALUES(" . $ref . ", " . $ref_id . ", " . $competence . ", 1)";
				$req=$Conn->prepare($sql);
				$req->execute();

			}
			
		}else{
			$sql="SELECT * FROM intervenants_competences WHERE ico_competence = " . $competence;
			$req=$Conn->query($sql);
			$intervenant_competence = $req->fetch();

			if(!empty($intervenant_competence)){
				$sql="DELETE FROM Intervenants_competences  WHERE ico_ref =" . $ref . " AND ico_ref_id = " . $ref_id . " AND ico_competence = " . $competence . " AND ico_valide = 1";
				$req=$Conn->prepare($sql);
				$req->execute();
			}
		}
	}
	$retour="fournisseur_voir.php?fournisseur=" . $fournisseur . "&intervenant=" . $ref_id . "&tab=5";
	if(!empty($erreur)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur !",
			"type" => "danger",
			"message" => $erreur
		);
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Ajout d'un diplôme",
			"type" => "success",
			"message" => "La compétence a bien été enregistrée"
		);
	}
	header("location : " . $retour);

?>
