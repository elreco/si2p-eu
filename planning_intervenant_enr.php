<?php
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");

include "modeles/mod_parametre.php";
include "modeles/mod_del_planning_intervenant.php";

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);
}
/*if($acc_societe!=3){
	$acc_agence=0;
}*/

// LES PARAMETRES

$erreur=0;

if(!empty($_POST["periode_deb"])){
	$periode_deb=$_POST["periode_deb"];
}else{
	$erreur=1;
}
if(!empty($_POST["periode_fin"])){
	$periode_fin=$_POST["periode_fin"];
}else{
	$erreur=1;
}

if($erreur==0){



	$c_periode_deb=convert_date_sql($periode_deb);
	$date_deb = new DateTime($c_periode_deb);

	$c_periode_fin=convert_date_sql($periode_fin);
	$date_fin = new DateTime($c_periode_fin);

	if(!empty($_POST["organisation"])){
		$organisation=$_POST["organisation"];
		$tab_org=explode(",",$organisation);
	}
	if(isset($tab_org)){
		echo("<pre>");
			print_r($tab_org);
		echo("</pre>");
	}

	// preparation des requetes

	// selection d'une ligne
	$sql_select="SELECT pin_place FROM Plannings_Intervenants WHERE pin_intervenant=:intervenant AND pin_semaine=:semaine AND pin_annee=:annee AND pin_agence=:acc_agence";
	$req_select=$ConnSoc->prepare($sql_select);


	// ajout d'une ligne
	$sql_add="INSERT INTO Plannings_Intervenants (pin_intervenant, pin_annee, pin_semaine, pin_place,pin_agence)";
	$sql_add.=" VALUES (:intervenant, :annee, :semaine, :place, :acc_agence);";
	$req_add=$ConnSoc->prepare($sql_add);

	// update d'une ligne
	$sql_update="UPDATE Plannings_Intervenants SET pin_place = :place WHERE pin_intervenant = :intervenant AND pin_semaine=:semaine AND pin_annee=:annee AND pin_agence=:acc_agence;";
	$req_update=$ConnSoc->prepare($sql_update);

	// FERIES
	$sql_ferie_get="SELECT fer_date FROM Feries WHERE fer_date>=:bcl_deb AND fer_date<=:bcl_fin AND (fer_type=0 OR (fer_type=1 AND fer_societe=:acc_societe) OR (fer_type=2 AND NOT fer_societe=:acc_societe));";
	$req_ferie_get=$Conn->prepare($sql_ferie_get);

	// ECRITURE PLANNING

	$sql_date_get="SELECT pda_id FROM Plannings_Dates WHERE pda_date=:pda_date AND pda_demi=:pda_demi AND pda_intervenant=:pda_intervenant;";
	$req_date_get=$ConnSoc->prepare($sql_date_get);

	$sql_date_add="INSERT INTO Plannings_Dates (pda_intervenant,pda_date, pda_demi,pda_type,pda_ref_1,pda_texte,pda_style_bg,pda_style_txt)";
	$sql_date_add.=" VALUES (:pda_intervenant, :pda_date, :pda_demi, 0, 0, 'férié', '#F00','#FFF');";
	$req_date_add=$ConnSoc->prepare($sql_date_add);


	// ON RECUPERE LES INTERVENANTS DE LA SOCIETE

	$sql="SELECT int_id,int_type FROM Intervenants";
	if($acc_agence>0){
		$sql.=" WHERE (int_agence=:acc_agence OR int_agence=0)";
	}
	$req=$ConnSoc->prepare($sql);
	if($acc_agence>0){
		$req->bindParam(":acc_agence",$acc_agence);
	}
	$req->execute();
	$intervenants=$req->fetchAll();

	if(!empty($intervenants)){
		//echo("intervenant existe<br/>");
		foreach($intervenants as $i){

			if(isset($tab_org)){
				$place=array_search($i["int_id"],$tab_org);
				$place++;
				echo($place);

			}


			$bcl_deb = new DateTime($date_deb->format('Y-m-d'));

			while($bcl_deb<$date_fin){

				$bcl_fin = new DateTime($bcl_deb->format('Y-m-d'));
				$bcl_fin->add(new DateInterval('P5D'));

				$semaine=$bcl_deb->format("W");
				$annee=$bcl_deb->format("Y");

				$inscrit=false;
				//echo("semaine : " . $semaine . "<br/>");
				//echo("annee : " . $annee . "<br/>");

				$req_select->bindValue(":semaine",ltrim($semaine, '0'));
				$req_select->bindParam(":annee",$annee);
				$req_select->bindParam(":acc_agence",$acc_agence);
				$req_select->bindParam(":intervenant",$i["int_id"]);
				$req_select->execute();
				$ligne=$req_select->fetch();

				if(!empty($ligne)){

					//echo("INSCRIT<br/>");
					// L'INTERVENANT EST INSCRIT

					if(!empty($place)){

						// l'intervenant reste inscrit on maj place si besoin
						if($ligne["pin_place"]!=$place){

							//echo("update<br/>");
							//echo("place : " . $place . "<br/>");
							$req_update->bindValue(":semaine",ltrim($semaine, '0'));
							$req_update->bindParam(":annee",$annee);
							$req_update->bindParam(":acc_agence",$acc_agence);
							$req_update->bindParam(":intervenant",$i["int_id"]);
							$req_update->bindParam(":place",$place);
							$req_update->execute();


						}
						$inscrit=true;
					}else{

						// l'intervenant doit être supprimé du planning
						// utilisation modele car traitement possible en AJAX

						del_planning_intervenant($i["int_id"],$bcl_deb,$bcl_fin,$acc_agence);


					}
				}elseif(!empty($place)){

					// inscription

					//echo ("inscription<br/>");
					$req_add->bindValue(":semaine",ltrim($semaine, '0'));
					$req_add->bindParam(":annee",$annee);
					$req_add->bindParam(":acc_agence",$acc_agence);
					$req_add->bindParam(":intervenant",$i["int_id"]);
					$req_add->bindParam(":place",$place);
					$req_add->execute();

					$inscrit=true;
				}

				if($inscrit AND $i["int_type"]==1){

					// l'intervenant est inscrit sur la semaine on regarde s'il y a des feriés

					$req_ferie_get->bindValue(":bcl_deb",$bcl_deb->format("Y-m-d"));
					$req_ferie_get->bindValue(":bcl_fin",$bcl_fin->format("Y-m-d"));
					$req_ferie_get->bindParam(":acc_societe",$acc_societe);
					$req_ferie_get->execute();
					$d_feries=$req_ferie_get->fetchAll();
					if(!empty($d_feries)){
						foreach($d_feries as $ferie){

							// pour chaque fériés si case planning vide on ajoute le férié

							for($demi=1;$demi<=2;$demi++){
								$req_date_get->bindParam("pda_date",$ferie["fer_date"]);
								$req_date_get->bindParam("pda_demi",$demi);
								$req_date_get->bindParam("pda_intervenant",$i["int_id"]);
								$req_date_get->execute();
								$date_planning=$req_date_get->fetch();
								if(empty($date_planning)){

									$req_date_add->bindParam("pda_date",$ferie["fer_date"]);
									$req_date_add->bindParam("pda_demi",$demi);
									$req_date_add->bindParam("pda_intervenant",$i["int_id"]);
									$req_date_add->execute();
								}
							}

						}


					}
				}

				$bcl_deb->add(new DateInterval('P7D'));
			}
			unset($place);
		}
	}
	//die();
	header("Location : planning.php");
}
?>
