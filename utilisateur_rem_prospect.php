<?php 

// AFFICHE LA LISTE DES NOUVEAU CLIENTS D'UN COMMERCIAL POUR LE SYSTEME DE REM
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


// DONNEE UTILE AU PROGRAMME

// personne connecté
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	} 
}

// param get
$utilisateur=0;
if(isset($_GET["utilisateur"])){
	if(!empty($_GET["utilisateur"])){
		$utilisateur=intval($_GET["utilisateur"]);
	}
}

$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}
if($exercice==0){
	if(date("n")<4){
		$exercice=date("Y")-1;
	}else{
		$exercice=date("Y");
	}
}
$exercice_fin=$exercice+1;

// init var

$erreur_txt="";

// CONTROLE ACCES

if($_SESSION["acces"]["acc_droits"][26]){
	if($utilisateur>0 AND $exercice>0){
		$sql="SELECT uti_nom,uti_prenom,uti_profil,uti_agence,uti_societe FROM Utilisateurs LEFT JOIN Utilisateurs_Societes 
		ON (Utilisateurs.uti_societe=Utilisateurs_Societes.uso_societe AND Utilisateurs.uti_agence=Utilisateurs_Societes.uso_agence)
		WHERE uti_id=" . $utilisateur . " AND uso_utilisateur=" . $acc_utilisateur;
		if($_SESSION['acces']["acc_profil"]==3){
			$sql.=" AND uti_id=" . $acc_utilisateur;
		}elseif($_SESSION['acces']["acc_profil"]==5 OR $_SESSION['acces']["acc_service"][1]==1){
			$sql.=" AND (uti_id=" . $acc_utilisateur . " OR uti_profil IN (3,15) )";
		}elseif($_SESSION['acces']["acc_profil"]==15){
			$sql.=" AND (uti_id=" . $acc_utilisateur . " OR uti_profil=3)";
		}
		$sql.=";";
		$req=$Conn->query($sql);
		$d_utilisateur=$req->fetch();
		if(empty($d_utilisateur)){
			$erreur_txt="Impossible d'afficher la page (err:001)!";
		}
	}else{
		$erreur_txt="Impossible d'afficher la page (err:002)!";
	}
}else{
	$erreur_txt="Impossible d'afficher la page (err:003)!";
}

// PARAMETRE DE LA REM
IF(empty($erreur_txt)){
	$sql="SELECT upr_rem_type FROM Utilisateurs_Rem_Param WHERE urp_utilisateur=" . $utilisateur . " AND urp_exercice=" . $exercice . ";";
	$req=$Conn->query($sql);
	$d_rem=$req->fetch();
	if(empty($d_rem)){
		$erreur_txt="Paramètres de rémunération non renseignés!";
	}elseif(empty($d_rem["upr_rem_type"])){
		$erreur_txt="Le type de rémunération n'est pas renseigné!";		
	}
}

if(empty($erreur_txt)){
	
	// DONNEE PARAMETRE
	
	// TITRE DE LA PAGE
	$titre="Liste des nouveaux clients de " . $d_utilisateur["uti_nom"] . " " . $d_utilisateur["uti_prenom"]; 
	$sous_titre="pour l'exercice " . $exercice . "/" . $exercice_fin; 
	
	// LISTE DES CLIENTS
	
	$d_propects=array();
	$sql="SELECT DISTINCT cli_id,cli_code,cli_nom,cli_adr_cp,cli_adr_ville,cli_filiale_de FROM Clients INNER JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client)
	WHERE cli_first_facture_date>='" . $exercice . "-04-01' AND cli_first_facture_date<='" . intval($exercice+1) . "-03-31' 
	AND cso_societe=" . $d_utilisateur["uti_societe"];
	if($d_rem["upr_rem_type"]==1){
		// com facture perso
		$sql.=" AND cso_utilisateur=" . $utilisateur;
	}elseif($d_utilisateur["uti_agence"]>0){
		//autre facturation agence / societe
		$sql.=" AND cso_agence=" . $d_utilisateur["uti_agence"];
	}
	$sql.=" ORDER BY cli_filiale_de,cli_id,cli_code;";
	$req=$Conn->query($sql);
	$d_resultats=$req->fetchAll();
	if(!empty($d_resultats)){
		foreach($d_resultats as $p){
			if(!empty($p["cli_filiale_de"])){
				if(!isset($d_propects[$p["cli_filiale_de"]])){
					$d_propects[$p["cli_filiale_de"]]=array(
						"cli_id" => $p["cli_id"],
						"cli_nom" => $p["cli_nom"],
						"cli_code" => $p["cli_code"],
						"cli_adr_cp" => $p["cli_adr_cp"],
						"cli_adr_ville" => $p["cli_adr_ville"]
					);
				}
			}else{
				if(!isset($d_propects[$p["cli_id"]])){
					$d_propects[$p["cli_id"]]=array(
						"cli_id" => $p["cli_id"],
						"cli_code" => $p["cli_code"],
						"cli_nom" => $p["cli_nom"],
						"cli_adr_cp" => $p["cli_adr_cp"],
						"cli_adr_ville" => $p["cli_adr_ville"]
					);
				}
			}
		}
	}
}
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">

		<?php 			if(!empty($erreur_txt)){ ?>
							<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-danger" style="border-radius:0px;"><?=$erreur_txt?></div>
							</div>
		<?php			}else{
			
							echo("<h1>" . $titre . "</h1>");
							echo("<h2>" . $sous_titre. "</h2>");
							if(!empty($d_propects)){  ?>
				
								<div class="table-responsive">
									<table class="table table-striped" >
										<thead>
											<tr class="dark2" >	
												<th>ID</th> 
												<th>Code</th>										
												<th>Nom</th>
												<th>CP</th>
												<th>Ville</th>		
												<th>&nbsp;</th>	
											</tr>
										</thead>
										<tbody>
								<?php		foreach($d_propects as $mm => $p){	?>
												<tr>		
													<td><?= $p['cli_id']?></td>
													<td><?= $p['cli_code'] ?></td>											
													<td><?=	$p['cli_nom'] ?></td>
													<td><?= $p['cli_adr_cp'] ?></td>																					
													<td><?= $p['cli_adr_ville'] ?></td>		
													<td>
														<a href="utilisateur_rem_prospect_fac.php?client=<?=$mm?>&utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>" >
															voir les factures
														</a>
													</td>	
												</tr>
					<?php					}  ?>
										</tbody>
										<tfoot>
											<tr>
												<th colspan="4" class="text-right" >Nombre de nouveaux clients :</th>
												<td class="text-right" ><?=count($d_propects)?></td>	
												<th colspan="6" class="text-right" >&nbsp;</th>
											</tr>
										</tfoot>
									</table>
								</div>
			<?php 			}else{ ?>
								<div class="col-md-12 text-center" style="padding:0;" >
									<div class="alert alert-warning" style="border-radius:0px;">
										Aucune facture correspondant à votre recherche.
									</div>
								</div>
			<?php			} 
						}?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="utilisateur_rem.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>" class="btn btn-default btn-sm" role="button">
							<i class="fa fa-left"></i> Retour
						</a>	</div>
					<div class="col-xs-6 footer-middle">&nbsp;</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
			
			});						
		</script>
	</body>
</html>
