<?php

include "includes/controle_acces.inc.php";
include('includes/connexion_soc.php');


// SUPP LE FICHIER PDF SUR LE SERVEUR

$erreur=0;

$devis_id=0;
if(!empty($_GET["devis"])){
	$devis_id=intval($_GET["devis"]);
}else{
	$erreur=1;
}

// la personne logue
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}

// recherche du devis
if($erreur==0){
	$sql="SELECT dev_numero FROM Devis WHERE dev_id=" . $devis_id;
	$req=$ConnSoc->query($sql);
	$devis=$req->fetch();
	if(!empty($devis) && file_exists("documents/Societes/" . $acc_societe . "/Devis/" . $devis["dev_numero"] . ".pdf")){
		unlink("documents/Societes/" . $acc_societe . "/Devis/" . $devis["dev_numero"] . ".pdf");
	}
}

header("location : devis_voir.php?devis=" . $devis_id);

?>
