<?php

// VISUALISATION D'UNE FICHE SUSPECT

// Les suspects sont associés à une société et stocké en base N
// les clients sont consulter via un autre script

	include "includes/controle_acces.inc.php";

	// CONTROLE DROITS ACCESS
	if(!$_SESSION["acces"]["acc_droits"][23]){
		header("location: deconnect.php");
		die();
	}

	include('includes/connexion.php');
	include('includes/connexion_soc.php');
	include('modeles/mod_orion_utilisateur.php');
	include('modeles/mod_get_suspect_adresses.php');
	include('modeles/mod_get_suspect_correspondances.php');
	include('modeles/mod_parametre.php');


	// PERSONNE CONNECTE

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}

	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}
	// CONTROLE PARAM
	$erreur=0;
	$suspect=0;
	if(!empty($_GET['suspect'])){
		$suspect=intval($_GET['suspect']);
	}
	if($suspect==0){
		echo("Erreur : suspect inconnu !");
		die();
	}

	/***************************
		ONGLET GENERAL
	*****************************/

	$sql="SELECT * FROM Suspects WHERE sus_id=" . $suspect . ";";
	$req=$ConnSoc->query($sql);
	$d_suspect=$req->fetch();
	if(empty($d_suspect)){

		echo("Erreur : client inconnu !");
		die();

	}else{

		// adresse d'intervention par défaut duplique sur suspect et suspect adresse pour simplifié requete
		// ici on la lit sur table suspect

		$adresse_int="";
		$adresse_int_id=0;
		if(!empty($d_suspect["sus_adresse"])){
			$adresse_int_id=$d_suspect["sus_adresse"];

			if(!empty($d_suspect["sus_adr_nom"])){
				$adresse_int=$d_suspect["sus_adr_nom"];
			}
			if(!empty($d_suspect["sus_adr_service"])){
				if(!empty($adresse_int)){
					$adresse_int.="<br/>";
				}
				$adresse_int.=$d_suspect["sus_adr_service"];
			}
			if(!empty($d_suspect["sus_adr_ad1"])){
				if(!empty($adresse_int)){
					$adresse_int.="<br/>";
				}
				$adresse_int.=$d_suspect["sus_adr_ad1"];
			}
			if(!empty($d_suspect["sus_adr_ad2"])){
				if(!empty($adresse_int)){
					$adresse_int.="<br/>";
				}
				$adresse_int.=$d_suspect["sus_adr_ad2"];
			}
			if(!empty($d_suspect["sus_adr_ad3"])){
				if(!empty($adresse_int)){
					$adresse_int.="<br/>";
				}
				$adresse_int.=$d_suspect["sus_adr_ad3"];
			}
			if(!empty($adresse_int)){
				$adresse_int.="<br/>";
			}
			$adresse_int.=$d_suspect["sus_adr_cp"] . " " . $d_suspect["sus_adr_ville"];
		}

		$sus_stagiaire_naiss="";
		if($d_suspect["sus_categorie"]==3){

			if(!empty($d_suspect["sus_stagiaire_naiss"])){

				$stagiaire_naiss=date_create_from_format('Y-m-d',$d_suspect["sus_stagiaire_naiss"]);
				if(!is_bool($stagiaire_naiss)){
					$sus_stagiaire_naiss=$stagiaire_naiss->format("d/m/Y");
				}
			}
		}
	}
	// les utilisateurs

	$utilisateurs=orion_utilisateurs();

	// commercial
	if(!empty($d_suspect["sus_commercial"])){
		$req = $ConnSoc->prepare("SELECT * FROM commerciaux WHERE com_id = " . $d_suspect['sus_commercial']);
		$req->execute();
		$d_commercial = $req->fetch();
		if(empty($d_commercial)){
			unset($d_commercial);
		}
	}

	// info création

	$creator="";
	if(!empty($d_suspect['sus_uti_creation'])){
		$creator = $utilisateurs[$d_suspect['sus_uti_creation']]["identite"];
	}
	$date_crea = convert_date_txt($d_suspect['sus_date_creation']);

	// sur la categorie du client

	$sql="SELECT cca_gc,cca_libelle,cca_couleur FROM clients_categories WHERE cca_id=" . $d_suspect["sus_categorie"] . ";";
	$req=$Conn->query($sql);
	$d_categorie=$req->fetch();

	// sous categorie
	if(!empty($d_suspect["sus_sous_categorie"])){
		$sql="SELECT csc_libelle,csc_couleur FROM clients_Sous_categories WHERE csc_id=" . $d_suspect["sus_sous_categorie"] . ";";
		$req=$Conn->query($sql);
		$d_sous_categorie=$req->fetch();
	}

	$couleur="";
	if(isset($d_sous_categorie)){
		if(!empty($d_sous_categorie["csc_couleur"])){
			$couleur=$d_sous_categorie["csc_couleur"];
		}
	}elseif(!empty($d_categorie["cca_couleur"])){
		$couleur=$d_categorie["cca_couleur"];
	}

	// l'agence

	if(!empty($d_suspect['sus_agence'])){
		$sql="SELECT age_nom FROM Agences WHERE age_id=" . $d_suspect["sus_agence"] . ";";
		$req=$Conn->query($sql);
		$d_agence=$req->fetch();
	}





	// adresse de fac par defaut
	// -> voir onglet carnet $adresse_fac_def

	//contact par defaut
	$d_con_fonction_nom=array(
		"0" => ""
	);
	$sql="SELECT * FROM contacts_fonctions ORDER BY cfo_libelle";
	$req=$Conn->query($sql);
	$d_contact_fonctions=$req->fetchAll();
	foreach($d_contact_fonctions as $d){
		$d_con_fonction_nom[$d["cfo_id"]]=$d["cfo_libelle"];
	}

	$contact_def="";
	$contact_def_id=0;
	if(!empty($d_suspect["sus_contact"])){



		$sql="SELECT * FROM Suspects_Contacts WHERE sco_id = " . $d_suspect['sus_contact'];

		$req=$ConnSoc->query($sql);
		$d_contact_def=$req->fetch();

		if(!empty($d_contact_def)){
			foreach($base_civilite as $k => $v){
				if($k == $d_contact_def['sco_titre']){
					$contact_def=$v . " ";
				}
			}
			$contact_def.=$d_contact_def['sco_prenom'] . " " . $d_contact_def['sco_nom'] . " - ";
			if(!empty($d_contact_def['sco_fonction_nom'])){
				$contact_def.=$d_contact_def['sco_fonction_nom'];
			}elseif(!empty($d_contact_def['sco_fonction'])){
				$contact_def.=$d_con_fonction_nom[$d_contact_def['sco_fonction']];
			}
			$contact_def.="<br>";
			if(!empty($d_contact_def['sco_tel'])){
				$contact_def.="Tél: " . $d_contact_def['sco_tel'];
			};
			if(!empty($d_contact_def['sco_portable'])){
				$contact_def.=" - Portable: " . $d_contact_def['sco_portable'];
			};
			if(!empty($d_contact_def['sco_fax'])){
				$contact_def.=" - Fax: " . $d_contact_def['sco_fax'];
			};
			if(!empty($d_contact_def['sco_comment'])){
				$contact_def.="<br> - Commentaire: " . $d_contact_def['sco_comment'];
			};
			$contact_def.="<br>";
			$contact_def.=$d_contact_def['sco_mail'];
			if($d_suspect["sus_categorie"]==3){
				if(!empty($sus_stagiaire_naiss)){
					$contact_def.="<br/><strong>Date de naissance : </strong>" .$sus_stagiaire_naiss;
				}
				if(!empty($d_suspect["sus_sta_naiss_cp"])){
					$contact_def.="<br/><strong>Département de naissance : </strong>" . $d_suspect["sus_sta_naiss_cp"];
				}
				if(!empty($d_suspect["sus_sta_naiss_ville"])){
					$contact_def.="<br/><strong>Ville de naissance : </strong>" . $d_suspect["sus_sta_naiss_ville"];
				}
			}
		}
	}

	// derniere correspondance
	// -> voir onglet correspondance


	// classification
	if(!empty($d_suspect["sus_classification"])){
		$sql="SELECT ccl_libelle FROM clients_Classifications WHERE ccl_id=" . $d_suspect["sus_classification"] . ";";
		$req=$Conn->query($sql);
		$d_classification=$req->fetch();
	}

	if(!empty($d_suspect["sus_classification_categorie"])){
		$sql="SELECT ccc_libelle FROM Clients_classifications_categories WHERE ccc_id=" . $d_suspect["sus_classification_categorie"] . ";";
		$req=$Conn->query($sql);
		$d_classification_categorie=$req->fetch();
	}

	if(!empty($d_suspect["sus_classification_type"])){
		$sql="SELECT cct_libelle FROM Clients_classifications_types WHERE cct_id=" . $d_suspect["sus_classification_type"] . ";";
		$req=$Conn->query($sql);
		$d_classification_type=$req->fetch();
	}

	// opca
	if(!empty($d_suspect["sus_categorie"]!=4)){
		if(!empty($d_suspect['sus_opca_type'])){

			$opca_type=intval($d_suspect['sus_opca_type']);

			$sql="SELECT csc_libelle FROM clients_Sous_categories WHERE csc_id=" . $opca_type . ";";
			$req=$Conn->query($sql);
			$d_opca_type=$req->fetch();

		}
		if(!empty($d_suspect['sus_opca'])){

			$opca=intval($d_suspect['sus_opca']);

			$sql="SELECT cli_code FROM Clients WHERE cli_id=" . $opca . ";";
			$req=$Conn->query($sql);
			$d_opca=$req->fetch();
		}
	}

	// mode de reglement

	$mod_de_reg="<i>Pas de mode de règlement</i>";
	if(!empty($d_suspect['sus_reg_type'])){
		$mod_de_reg=$base_reglement[$d_suspect['sus_reg_type']];
	}
	$delai_de_reg="<i>Pas de délai de règlement</i>";
	if($d_suspect['sus_reg_formule'] == 2){
		$delai_de_reg="45 jours fin de mois";
	}elseif($d_suspect['sus_reg_formule'] == 3){
		$delai_de_reg="fin de mois plus 45 jours";
	}elseif($d_suspect['sus_reg_formule'] == 1){
		if(!empty($d_suspect['sus_reg_nb_jour'])){
			$delai_de_reg=$d_suspect['sus_reg_nb_jour'] ." jours";
		}else{
			$delai_de_reg="Comptant";
		}
	}elseif($d_suspect['sus_reg_formule'] == 4){
		if(!empty($d_suspect['sus_reg_nb_jour'])){
			$delai_de_reg=$d_suspect['sus_reg_nb_jour'] ." jours ";
		}
		$delai_de_reg.="fin de mois";
		if(!empty($d_suspect['sus_reg_fdm'])){
			$delai_de_reg.=" le " . $d_suspect['sus_reg_fdm'];
		}
	}


	// prescripteur
	$prescripteur_lib="<i>Pas de prescripteur</i>";
	$prescripteur_info="";
	if(!empty($d_suspect['sus_prescripteur'])){

		if($d_suspect['sus_prescripteur']==-1){
			$prescripteur_lib="Autre";
		}else{
			$sql="SELECT cpr_libelle FROM Clients_Prescripteurs WHERE cpr_id=" . $d_suspect['sus_prescripteur'] . ";";
			$req=$Conn->query($sql);
			$d_prescripteur=$req->fetch();
			if(!empty($d_prescripteur)){
				$prescripteur_lib=$d_prescripteur["cpr_libelle"];
			}
		}

		$prescripteur_info=$d_suspect['sus_prescripteur_info'];
	}

	// code APE
	$ape_lib="<i>Pas de code APE</i>";
	if(!empty($d_suspect['sus_ape'])){
		$sql="SELECT ape_code,ape_libelle FROM Ape WHERE ape_id=" . $d_suspect['sus_ape'] . ";";
		$req=$Conn->query($sql);
		$d_ape=$req->fetch();
		if(!empty($d_ape)){
			$ape_lib=$d_ape["ape_code"] . "-" . $d_ape["ape_libelle"];
		}
	}

	// ONGLET ADRESSE

	$adresses_int = get_suspect_adresses($suspect,1);

	$adresses_fac = get_suspect_adresses($suspect,2);
	if(!empty($adresses_fac)){
		$adresse_fac_def_id=$adresses_fac[0]["sad_id"];
		$adresse_fac_def="";
		if(!empty($adresses_fac[0]["sad_nom"])){
			$adresse_fac_def=$adresses_fac[0]["sad_nom"];
		};
		if(!empty($adresses_fac[0]["sad_service"])){
			if(!empty($adresse_fac_def)){
				$adresse_fac_def.="<br/>";
			};
			$adresse_fac_def.=$adresses_fac[0]["sad_service"];
		};
		if(!empty($adresses_fac[0]["sad_ad1"])){
			if(!empty($adresse_fac_def)){
				$adresse_fac_def.="<br/>";
			};
			$adresse_fac_def.=$adresses_fac[0]["sad_ad1"];
		};
		if(!empty($adresses_fac[0]["sad_ad2"])){
			if(!empty($adresse_fac_def)){
				$adresse_fac_def.="<br/>";
			};
			$adresse_fac_def.=$adresses_fac[0]["sad_ad2"];
		};
		if(!empty($adresses_fac[0]["sad_ad3"])){
			if(!empty($adresse_fac_def)){
				$adresse_fac_def.="<br/>";
			};
			$adresse_fac_def.=$adresses_fac[0]["sad_ad3"];
		};
		if(!empty($adresse_fac_def)){
			$adresse_fac_def.="<br/>";
		};
		$adresse_fac_def.=$adresses_fac[0]["sad_cp"] . " " . $adresses_fac[0]["sad_ville"];
	}

	$adresses_env = get_suspect_adresses($suspect,3);

	// ONGLET CONTACTS
	// différents du contact par defaut
	$sql="SELECT * FROM Suspects_Contacts WHERE sco_ref_id=" . $suspect . ";";
	$req=$ConnSoc->query($sql);
	$contacts=$req->fetchAll();


	//**************************************
	// 	ONGLET CORRESPONDANCES		
	//**************************************
	
	$class_panel="panel-dark";
	$correspondances = get_suspect_correspondances($suspect);
	if(!empty($correspondances)){
		$last_cor=$correspondances[0];
		$class_panel="panel-success";
	}

	// types de correspondances
	$d_cor_types=array(
		"0" => "&nbsp;"
	);
	$sql="SELECT cty_id,cty_libelle FROM Correspondances_Types ORDER BY cty_id;";
	$req=$Conn->query($sql);
	$d_results=$req->fetchAll();
	if(!empty($d_results)){
		foreach($d_results as $r){
			$d_cor_types[$r["cty_id"]]=$r["cty_libelle"];
		}
	}

	// types de correspondances
	$d_cor_raisons=array(
		"0" => "&nbsp;"
	);
	$sql="SELECT cra_id,cra_libelle FROM Correspondances_Raisons ORDER BY cra_id;";
	$req=$Conn->query($sql);
	$d_results=$req->fetchAll();
	if(!empty($d_results)){
		foreach($d_results as $r){
			$d_cor_raisons[$r["cra_id"]]=$r["cra_libelle"];
		}
	}

	
	// GESTION DES DROITS SUR LA FICHE

	$drt_modifier=true;
	if($d_categorie["cca_gc"] OR ($d_suspect["sus_categorie"]==7 AND $maison_mere_id=$suspect)){
		if(!$_SESSION['acces']["acc_droits"][8]){
			$drt_modifier=false;
		}
	}

	// ONGLET ACTIF ET Retour

	$_SESSION['retourCorresp'] = "suspect_voir.php?suspect=" . $suspect;
	$_SESSION['retourContact'] = "suspect_voir.php?suspect=" . $suspect;
	$_SESSION['retourAdresse'] = "suspect_voir.php?suspect=" . $suspect;

	$onglet=1;
	if(isset($_GET['onglet'])){
		$onglet=intval($_GET['onglet']);
	}elseif(isset($_GET['tab1'])){
		$onglet=1;
	}elseif(isset($_GET['tab5'])){
		$onglet=5;
	}elseif(isset($_GET['tab11'])){
		$onglet=11;
	}elseif(isset($_GET['tab12'])){
		$onglet=12;
	}

	// SYSTEME DE PAGINATION
	if(isset($_SESSION['client_tableau'])){
		if(is_array($_SESSION['client_tableau'])){
			$cle=array_search ($suspect . "-1",$_SESSION['client_tableau']);
			if(!is_bool($cle)){
				if($cle>0){					
					$tab_prec=explode("-",$_SESSION['client_tableau'][$cle-1]);
				}				
				if($cle<count($_SESSION['client_tableau'])-1){					
					$tab_suiv=explode("-",$_SESSION['client_tableau'][$cle+1]);
				}
			}
		}
	}


?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion - <?=$d_suspect['sus_nom']?></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >

		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper" class="">
			<header id="topbar">
	            <div class="text-center">
	                <h4 style="margin:0;"><?= $d_suspect['sus_code'] ?> <small><?= $d_suspect['sus_nom'] ?></small></h4>
	            </div>


	       </header>
				<section id="content" class="">
		<?php		if($erreur==0){ ?>
						<div class="row">
							<div class="col-md-12">

								<div class="tab-block">
									<ul class="nav nav-tabs responsive">
										<li <?php if($onglet==1) echo("class='active'");?> >
										  <a href="#tab1" data-toggle="tab" class="onglet clic-general" aria-expanded="true" data-onglet="general" >Général</a>
										</li>
										<?php if($d_suspect["sus_categorie"]!=3){ ?>
											<li <?php if($onglet==11) echo("class='active'");?> >
												<a href="#tab11" data-toggle="tab" class="onglet clic-adresse" aria-expanded="false" data-onglet="adresse" >Carnet d'adresses</a>
											</li>

											<li <?php if($onglet==12) echo("class='active'");?> >
												<a href="#tab12" data-toggle="tab" class="onglet clic-contact" aria-expanded="false" data-onglet="contact" >Carnet de contacts</a>
											</li>
										<?php } ?>
											<li <?php if($onglet==5) echo("class='active'");?> >
												<a href="#tab5" data-toggle="tab" class="onglet clic-cor" aria-expanded="false" data-onglet="cor" >Correspondances</a>
											</li>

									</ul>

									<div class="tab-content responsive p30" style="min-height:500px;">

										<!-- DEBUT TAB GENERALE 1 -->
										<div id="tab1" class="tab-pane <?php if($onglet==1) echo("active"); ?>" >

											<div class="row">
												<div class="col-md-12">

													<h2 style="margin-top:0px;">Général</h2>
													<div class="panel <?=$class_panel?>">
														<div class="panel-heading  panel-head-sm" <?php if(!empty($couleur)) echo("style='background-color:" . $couleur . ";'") ?> >
															<span class="panel-icon">
																<i class="fa fa-file"></i>
															</span>
															<span class="panel-title">Informations principales</span>
														</div>
														<div class="panel-body">
															<div class="row mb15" >
																<div class="col-md-3">
																	<strong>Code:</strong> <?= $d_suspect['sus_code']; ?>
																</div>
																<div class="col-md-3">
																	<strong>Nom:</strong> <?= $d_suspect['sus_nom']; ?>
																</div>
																<div class="col-md-3"><strong>Catégorie:</strong>
														<?php 		if(!empty($d_categorie['cca_libelle'])){
																		echo($d_categorie['cca_libelle']);
																	}else{
																		echo("<i>Pas de catégorie</i>");
																	} ?>
																</div>
																<div class="col-md-3">
																	<strong>Sous catégorie:</strong>
															<?php 	if(isset($d_sous_categorie)){
																		echo($d_sous_categorie['csc_libelle']);
																	}else{
																		echo("<i>Pas de Sous-catégorie</i>");
																	} ?>
																</div>
															</div>
															<div class="row" style="margin-bottom:15px;">
												<?php			if(isset($d_agence)){ ?>
																	<div class="col-md-3">
																		<strong>Agence :</strong><?=$d_agence['age_nom']?>
																	</div>
												<?php			}  ?>
																<div class="col-md-3">
																	<strong>Commercial :</strong>
															<?php 	if(isset($d_commercial)){
																		echo($d_commercial['com_label_1'] . " " . $d_commercial['com_label_2']);
																	}else{
																		echo("<i>Pas de commercial</i>");
																	} ?>
																</div>
														<?php 	if($d_suspect['sus_categorie'] != 3): ?>
																	<div class="col-md-3">
																	  <strong>Siren: </strong> <?php if(empty($d_suspect['sus_siren'])): ?><i> Pas de siren</i><?php else: ?> <?= $d_suspect['sus_siren'] ?><?php endif; ?>
																	</div>
													  <?php		endif; ?>																
																<div class="col-md-3">
																
														<?php		if(empty($correspondances)){
																		echo("<strong>Statut: </strong> suspect");
																	}else{
																		echo("<strong>Statut: </strong> prospect non validé");
																	} ?>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>

											<div class="row">

												<div class="col-md-4" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'");?> >
													<!-- INTERVENTION -->
													<div class="panel <?=$class_panel?>" >
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-home"></i>
															</span>
															<span class="panel-title">
																Adresse du lieu d'intervention par défaut
													<?php		if($drt_modifier AND !empty($adresse_int)){ ?>
																	<span class="pull-right">
																		<a href="suspect_adresse.php?suspect=<?=$suspect?>&adresse=<?=$adresse_int_id?>" class="btn btn-sm btn-warning">
																			<i class="fa fa-pencil"></i>
																		</a>
																	</span>
													<?php		} ?>
															</span>
														</div>
														<div class="panel-body">
													<?php 	if(empty($d_suspect["sus_adresse"])){ ?>
																<div class="alert alert-warning" style="margin-bottom:0px;">
																	Pas d'adresse d'intervention
																</div>
													<?php	}else{
																echo($adresse_int);
															} ?>
														</div>
													</div>
												</div>
										<?php 	if($d_suspect["sus_categorie"]==3){ ?>
													<div class="col-md-6">
										<?php 	}else{ ?>
													<div class="col-md-4">
										<?php 	} ?>
													<div class="panel <?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-home"></i>
															</span>
															<span class="panel-title">Adresse de facturation par défaut
													<?php		if($drt_modifier AND isset($adresse_fac_def)){ ?>
																	<span class="pull-right">
																		<a href="suspect_adresse.php?suspect=<?=$suspect?>&adresse=<?=$adresse_fac_def_id?>" class="btn btn-sm btn-warning">
																			<i class="fa fa-pencil"></i>
																		</a>
																	</span>
													<?php		} ?>
															</span>
														</div>
														<div class="panel-body">
													<?php 	if(!isset($adresse_fac_def)){ ?>
																<div class="alert alert-warning" style="margin-bottom:0px;">
																	Pas d'adresse de facturation
																</div>
													<?php	}else{
																echo($adresse_fac_def);
																if($d_suspect['sus_categorie'] != 3){
																	if(!empty($d_suspect['sus_siren']) AND !empty($adresses_fac[0]["sad_siret"])){
																		echo("<br>Siret: " . $d_suspect['sus_siren'] . " " . $adresses_fac[0]["sad_siret"]);
																	}
																}
															} ?>
														</div>
													</div>
												</div>
												<?php if($d_suspect["sus_categorie"]==3){ ?>
													<div class="col-md-6">
												<?php }else{ ?>
													<div class="col-md-4">
												<?php }?>
													<div class="panel <?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-comments-o"></i>
															</span>
															<span class="panel-title">
																Dernière correspondance
																<span class="pull-right">
															<?php 	if($drt_modifier){
																		if(!empty($last_cor) && $_SESSION['acces']['acc_ref_id'] == $last_cor['sco_utilisateur']){ ?>
																			<a href="suspect_correspondance.php?suspect=<?= $d_suspect['sus_id'] ?>&id=<?= $last_cor['sco_id'] ?>" class="btn btn-sm btn-warning">
																				<i class="fa fa-pencil"></i>
																			</a>
																<?php 	}
																	} ?>
																</span>
															</span>
														</div>
														<div class="panel-body">
												<?php 		if(isset($last_cor)){ ?>
																<ul>
																	<li>
																		Créée
															<?php		if(!empty($last_cor['sco_utilisateur'])){
																			echo("par : " . $utilisateurs[$last_cor['sco_utilisateur']]["identite"]);
																		}?>
																		le <strong><?= convert_date_txt($last_cor['sco_date']); ?></strong>
																	</li>
																	<li>
																		<strong>Correspondant: </strong>
																		<?=$last_cor["sco_contact_prenom"] . " " . $last_cor["sco_contact_nom"]?>
																	</li>
																	<li>
																		<strong>Commentaires: </strong> <?=$last_cor['sco_commentaire']?>
																	</li>
																	<li>
																		<strong>Téléphone: </strong> <?=$last_cor['sco_contact_tel']?>
																	</li>
																	<li>
																		<strong>Rappeler le:</strong> <?= convert_date_txt($last_cor['sco_rappeler_le']); ?>
																	</li>
																</ul>
												<?php 		}else{ ?>
																<div class="alert alert-warning" style="margin-bottom:0px;">
																	Il n'y a pas encore de correspondance pour ce client.
																</div>
												<?php 		} ?>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<!-- CONTACT -->
													<div class="panel <?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-map-marker"></i>
															</span>
															<span class="panel-title">
																Contact par défaut
												<?php 			if($drt_modifier AND !empty($contact_def)){ ?>
																	<span class="pull-right">
																		<a href="suspect_contact.php?suspect=<?=$suspect?>&contact=<?=$d_contact_def["sco_id"]?>" class="btn btn-sm btn-warning">
																			<i class="fa fa-pencil"></i>
																		</a>
																	</span>
												<?php			} ?>
															</span>
														</div>
														<div class="panel-body">
												<?php 		if(!empty($contact_def)){
																echo($contact_def);
															}else{
																echo("<div class='alert alert-warning' style='margin-bottom:0px;' >");
																	echo("Pas de contact");
																echo("</div>");
															} ?>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="panel <?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-file"></i>
															</span>
															<span class="panel-title">Informations complémentaires</span>
														</div>
														<div class="panel-body">
															<div class="row" style="margin-bottom:15px;">
																<div class="col-md-4">
																	<strong>Source : </strong>
																	<?=$prescripteur_lib?>
																</div>
																<div class="col-md-4">
														<?php		if(!empty($prescripteur_info)){
																		echo("<strong>" . $prescripteur_lib . ": </strong>");
																		echo($prescripteur_info);
																	} ?>
																</div>
																<div class="col-md-4">
																	<strong>Code APE: </strong>
																	<?=$ape_lib?>
																</div>
															</div>
															<div class="row" style="margin-bottom:15px;">
																<div class="col-md-8">
																	<strong>Type de stagiaires:</strong>
														<?php 		foreach($base_type_stagiaire as $k => $n){
																		if($k == $d_suspect['sus_stagiaire_type']){
																			$stagiaire_type = $n;
																		}
																	}
																	if(empty($stagiaire_type) OR $stagiaire_type == "&nbsp;"){
																		echo("<i>Pas de type de stagiaire</i>");
																	}else{
																		echo($stagiaire_type);
																	} ?>
																</div>
																
														<?php	if($d_suspect["sus_categorie"]!=3){ ?>
																	<div class="col-md-4">
																		<strong>Téléphone (standard) :</strong>
														<?php 			if(empty($d_suspect['sus_tel'])){
																			echo("<i>non renseigné</i>");
																		}else{
																			echo($d_suspect['sus_tel']);
																		} ?>
																	</div>
														<?php	} ?>
															</div>
															<div class="row" style="margin-bottom:15px;">
																<div class="col-md-4">
																	<strong>Classification: </strong>
															<?php 	if(isset($d_classification)){
																		echo($d_classification['ccl_libelle']);
																	}else{
																		echo("<i>Pas de classification</i>");
																	} ?>
																</div>
																<div class="col-md-4">
																	<strong>Catégorie de classification :</strong>
															<?php 	if(isset($d_classification_categorie)){
																		echo($d_classification_categorie['ccc_libelle']);
																	}else{
																		echo("<i>Pas de catégorie de classification</i>");
																	} ?>
																</div>
																<div class="col-md-4">
																	<strong>Type de classification :</strong>
															<?php 	if(isset($d_classification_type)){
																		echo($d_classification_type['cct_libelle']);
																	}else{
																		echo("<i>Pas de type de classification</i>");
																	} ?>
																</div>
															</div>
													<?php	if($d_suspect["sus_categorie"]!=3){ ?>
																<div class="row" style="margin-bottom:15px;">
																	<div class="col-md-6">
																		<strong>Capital : </strong>
																<?php 	if(!empty($d_suspect["sus_capital"])){
																			echo($d_suspect["sus_capital"]);
																		}else{
																			echo("<i>non renseigné</i>");
																		} ?>
																	</div>
																	<div class="col-md-6">
																		<strong>Type de société :</strong>
																<?php 	if(!empty($d_suspect["sus_soc_type"])){
																			echo($d_suspect["sus_soc_type"]);
																		}else{
																			echo("<i>non renseigné</i>");
																		} ?>
																	</div>
																</div>
																<div class="row" style="margin-bottom:15px;">
																	<div class="col-md-6">
																		<strong>Lieu d'immatriculation :</strong>
																<?php 	if(!empty($d_suspect["sus_immat_lieu"])){
																			echo($d_suspect["sus_immat_lieu"]);
																		}else{
																			echo("<i>non renseigné</i>");
																		} ?>
																	</div>
																</div>
													<?php	} ?>

															<div class="row" style="margin-bottom:15px;">
																<div class="col-md-12">
																	<strong>Créé le </strong> <?= $date_crea ?>
																	<strong>par </strong> <?=$creator?>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-2">
													<div class="panel <?=$class_panel?>">
														<div class="panel-heading panel-head-sm">
															<span class="panel-icon">
																<i class="fa fa-file"></i>
															</span>
															<span class="panel-title">Compta</span>
														</div>
														<div class="panel-body">

													<?php	if(!empty($d_suspect["sus_categorie"]!=4)){
																if($d_suspect['sus_facture_opca'] == 1){ ?>
																	<div class="col-md-12 mb15" >
																		<strong>Catégorie de financeur :</strong>
														<?php			if(isset($d_opca_type)){
																			echo($d_opca_type["csc_libelle"]);
																		}else{
																			echo("<i>Non renseigné</i>");
																		} ?>
																	</div>
																	<div class="col-md-12 mb15" >
																		<strong>Financeur :</strong>
																<?php	if(isset($d_opca)){
																			echo($d_opca["cli_code"]);
																		}else{
																			echo("<i>Non renseigné</i>");
																		} ?>
																	</div>
													<?php		}else{ ?>															
																	<div class="col-md-12 mb15" >
																		<strong>Financeur :</strong>
																		<i>aucun financeur</i>
																	</div>
														<?php	} 
															} ?>
												
															<div class="col-md-12" style="margin-bottom:15px;">
																<strong>Mode de règlement : </strong>
																<?=$mod_de_reg?>
															</div>
															<div class="col-md-12" style="margin-bottom:15px;">
																<strong>Délai de règlement:</strong>
																<?=$delai_de_reg?>
															</div>
															<div class="col-md-12" style="margin-bottom:15px;">
																<strong>Identification TVA:</strong>
															  <?php if(empty($d_suspect['sus_ident_tva'])): ?>
																<i>Pas d'identification TVA</i>
															  <?php else: ?>
																<?= $d_suspect['sus_ident_tva'] ?>
															  <?php endif; ?>
															</div>
															<div class="col-md-12" style="margin-bottom:15px;">
															  <strong>Relevés de factures :</strong>
															  <?php if(empty($d_suspect['sus_releve'])): ?>
																Non
															  <?php else: ?>
																Oui
															  <?php endif; ?>
															</div>

														</div>
													</div>
												</div>
											</div>

										</div>

										<!-- TAB5 correspondances -->
										<div id="tab5" class="tab-pane <?php if($onglet==5) echo("active"); ?>" >
											<h2 style="margin-top:0px;">Correspondances</h2>
											<div class="admin-form" >
												<table class="table table-striped table-hover" >
													<thead>
														<tr class="dark">
															<th>ID</th>
															<th>Date</th>
															<th>Enregistré par</th>
															<th>Méthode</th>
															<th>Raison</th>
															<th>Correspondant</th>
															<th>Tel</th>
															<th>Commentaires</th>
															<th>Rappeler le</th>
															<th>Rappel traité</th>
															<th>Actions</th>
														</tr>
													</thead>
											<?php 	if(!empty($correspondances)){ ?>
														<tbody>
												<?php 		foreach($correspondances as $c){

																$sco_date_aff="";
																if(!empty($c['sco_date'])){
																	$sco_date=date_create_from_format('Y-m-d',$c['sco_date']);
																	$sco_date_aff=$sco_date->format("d/m/Y");
																}
																$sco_rappeler_le_aff="";
																if(!empty($c['sco_rappeler_le'])){
																	$sco_rappeler_le=date_create_from_format('Y-m-d',$c['sco_rappeler_le']);
																	$sco_rappeler_le_aff=$sco_rappeler_le->format("d/m/Y");
																}

																$sco_auteur="";
																$sco_responsable=0;
																if(!empty($c['sco_utilisateur'])){
																	$sco_auteur=$utilisateurs[$c['sco_utilisateur']]["identite"];
																	$sco_responsable=$utilisateurs[$c['sco_utilisateur']]["responsable"];
																}

																$sco_raison="";
																if($c['sco_raison']==-1){
																	$sco_raison="Autre : " . $c['sco_raison_info'];
																}elseif(!empty($c['sco_raison'])){
																	$sco_raison=$d_cor_raisons[$c['sco_raison']];
																}

																?>
																<tr class="correspondance" id="correspondance_<?=$c['sco_id']?>" >
																	<td><?= $c['sco_id']; ?></td>
																	<td><?= $sco_date_aff ?></td>
																	<td><?= $sco_auteur ?></td>
																	<td><?= $d_cor_types[$c['sco_type']] ?></td>
																	<td><?= $sco_raison ?></td>
																	<td><?= $c['sco_contact_prenom'] . " " .$c['sco_contact_nom']?></td>
																	<td><?= $c['sco_contact_tel']?></td>
																	<td><?= $c['sco_commentaire']?></td>
																	<td><?=$sco_rappeler_le_aff?></td>
															<?php	if($c['sco_utilisateur']==$acc_utilisateur OR $sco_responsable==$acc_utilisateur){ ?>
																		<td class="text-center" >
																			<label class="option">
																				<input type="checkbox" id="corresp_rappel_<?=$c['sco_id']?>" class="corresp-rappel" data-corresp="<?=$c['sco_id']?>" <?php if($c['sco_rappel']==1) echo("checked"); ?> />
																				<span class="checkbox"></span>
																			</label>
																		</td>
																		<td>
																			<a href="suspect_correspondance.php?suspect=<?= $d_suspect['sus_id'] ?>&id=<?=$c['sco_id']?>&tab5" class="btn btn-warning btn-sm">
																				<i class="fa fa-pencil"></i>
																			</a>
																			<a href="#" class="btn btn-danger btn-sm corresp-supp" data-corresp="<?= $c['sco_id'] ?>">
																				<i class="fa fa-times"></i>
																			</a>
																		</td>
															<?php	}else{ ?>
																		<td>
															<?php			if($c['sco_rappel']==1){
																				echo("OUI");
																			}else{
																				echo("NON");
																			} ?>
																		</td>
																		<td>&nbsp;</td>
															<?php	} ?>
																</tr>
												<?php 		} ?>
														</tbody>
											<?php	} ?>
												</table>
											</div>
											<div class="alert alert-warning" id="no_corresp" <?php if(!empty($correspondances)) echo("style='display:none;'")?> >
												Il n'y a pas de correspondance pour ce client
											</div>
										</div>
										<!-- fin correspondances -->

										<!-- adresses TAB11 -->
										<div id="tab11" class="tab-pane <?php if($onglet==11) echo("active"); ?>" >

											<h2 style="margin-top:0px;">Carnet d'adresses</h2>
											<div class="row">
										<?php 	if($d_suspect['sus_categorie'] != 3){ ?>
													<div class="col-md-4">
														<blockquote class="blockquote-success">
															<p>Adresses d'intervention.</p>
														</blockquote>
											<?php 		if(!empty($adresses_int)){
															foreach($adresses_int as $a){
																$text_adresse="";
																if(!empty($a['sad_nom'])){
																	$text_adresse=$a['sad_nom'];
																}
																if(!empty($a['sad_service'])){
																	if(!empty($text_adresse)){
																		$text_adresse.="<br/>";
																	}
																	$text_adresse.=$a['sad_service'];
																}
																if(!empty($a['sad_ad1'])){
																	if(!empty($text_adresse)){
																		$text_adresse.="<br/>";
																	}
																	$text_adresse.=$a['sad_ad1'];
																}
																if(!empty($a['sad_ad2'])){
																	if(!empty($text_adresse)){
																		$text_adresse.="<br/>";
																	}
																	$text_adresse.=$a['sad_ad2'];
																}
																if(!empty($a['sad_ad3'])){
																	if(!empty($text_adresse)){
																		$text_adresse.="<br/>";
																	}
																	$text_adresse.=$a['sad_ad3'];
																}
																$text_adresse.="<br/>" . $a['sad_cp'] . " " . $a['sad_ville']; ?>

																<div class="panel adresse-1" id="adresse_<?=$a['sad_id']?>" >
																	<div class="panel-heading panel-head-sm">
																		<span class="panel-title">
																		<?php if($a['sad_defaut'] == 1){ ?>
																		<strong><?=$a['sad_libelle']?></strong>
																		<?php }else{ ?>
																			<?=$a['sad_libelle']?>
																		<?php }?>

																		</span>
																	</div>
																	<div class="panel-body">
																		<?php if($a['sad_defaut'] == 1){ ?>
																			Adresse par défaut<br>
																		<?php }?>
																		<?=$text_adresse?>
																	</div>
																	<div class="panel-footer">

																			<a href="suspect_adresse.php?suspect=<?=$suspect?>&adresse=<?=$a['sad_id']?>&onglet=11" class="btn btn-warning btn-sm" >
																				<i class="fa fa-pencil"></i>
																			</a>
																<?php 		if($a['sad_defaut'] != 1){ ?>
																				<div class="pull-right" >
																					<button type="button" data-adresse="<?= $a['sad_id']?>" class="btn btn-danger btn-sm deladresse text-right">
																						<i class="fa fa-times"></i>
																					</button>
																				</div>
																<?php 		} ?>

																	</div>
																</div>
													<?php	}
														} ?>
														<div class="alert alert-warning" id="no_adresse_1" <?php if(!empty($adresses_int)) echo("style='display:none;'"); ?> >
															Pas d'adresse d'intervention pour ce client.
														</div>
													</div>
										<?php 	} ?>
												<div class="col-md-4">
													<blockquote class="blockquote-primary">
														<p>Adresses de facturation.</p>
													</blockquote>
										  <?php		if(!empty($adresses_fac)){
														foreach($adresses_fac as $a){
															$text_adresse="";
															if(!empty($a['sad_nom'])){
																$text_adresse=$a['sad_nom'];
															}
															if(!empty($a['sad_service'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['sad_service'];
															}
															if(!empty($a['sad_ad1'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['sad_ad1'];
															}
															if(!empty($a['sad_ad2'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['sad_ad2'];
															}
															if(!empty($a['sad_ad3'])){
																if(!empty($text_adresse)){
																	$text_adresse.="<br/>";
																}
																$text_adresse.=$a['sad_ad3'];
															}
															$text_adresse.="<br/>" . $a['sad_cp'] . " " . $a['sad_ville'];

															if(!empty($a['sad_siret']) AND !empty($d_suspect["sus_siren"])){
																$text_adresse.="<br/>" . $d_suspect["sus_siren"] . " " . $a['sad_siret'];
															} ?>
															<div class="panel adresse-2" id="adresse_<?=$a['sad_id']?>" >
																<div class="panel-heading panel-head-sm">
																	<span class="panel-title">
																	<?php if($a['sad_defaut'] == 1){ ?>
																		<strong><?=$a['sad_libelle']?></strong>
																	<?php }else{ ?>
																		<?=$a['sad_libelle']?>
																	<?php }?>
																	</span>
																</div>
																<div class="panel-body">
																	<?php if($a['sad_defaut'] == 1){ ?>
																		Adresse par défaut<br>
																	<?php }?>
																	<?=$text_adresse?>
																</div>
																<div class="panel-footer" >
																	<a href="suspect_adresse.php?suspect=<?=$suspect?>&adresse=<?=$a['sad_id']?>&onglet=11" class="btn btn-warning btn-sm" >
																		<i class="fa fa-pencil"></i>
																	</a>
														<?php 		if($a['sad_defaut'] != 1){ ?>
																		<div class="pull-right" >
																			<button type="button" data-adresse="<?= $a['sad_id']?>" class="btn btn-danger btn-sm deladresse">
																				<i class="fa fa-times"></i>
																			</button>
																		</div>
														<?php 		} ?>

																</div>
															</div>
												<?php	}
													} ?>
													<div class="alert alert-warning" id="no_adresse_2" <?php if(!empty($adresses_fac)) echo("style='display:none;'"); ?> >
														Pas d'adresse de facturation pour ce client.
													</div>
												</div>

										<?php 	if($d_suspect['sus_categorie'] != 3){ ?>
													<div class="col-md-4">
														<blockquote class="blockquote-danger">
															<p>Adresses d'envoi de facture.</p>
														</blockquote>
											  <?php 	if(!empty($adresses_env)){
															foreach($adresses_env as $a){

																$text_adresse="";
																if(!empty($a['sad_nom'])){
																	$text_adresse=$a['sad_nom'];
																}
																if(!empty($a['sad_service'])){
																	if(!empty($text_adresse)){
																		$text_adresse.="<br/>";
																	}
																	$text_adresse.=$a['sad_service'];
																}
																if(!empty($a['sad_ad1'])){
																	if(!empty($text_adresse)){
																		$text_adresse.="<br/>";
																	}
																	$text_adresse.=$a['sad_ad1'];
																}
																if(!empty($a['sad_ad2'])){
																	if(!empty($text_adresse)){
																		$text_adresse.="<br/>";
																	}
																	$text_adresse.=$a['sad_ad2'];
																}
																if(!empty($a['sad_ad3'])){
																	if(!empty($text_adresse)){
																		$text_adresse.="<br/>";
																	}
																	$text_adresse.=$a['sad_ad3'];
																}
																$text_adresse.="<br/>" . $a['sad_cp'] . " " . $a['sad_ville']; ?>

																<div class="panel adresse-3" id="adresse_<?=$a['sad_id']?>" >
																	<div class="panel-heading panel-head-sm">
																		<span class="panel-title">
																		<?php if($a['sad_defaut'] == 1){ ?>
																		<strong><?=$a['sad_libelle']?></strong>
																	<?php }else{ ?>
																		<?=$a['sad_libelle']?>
																	<?php }?>

																		</span>
																	</div>
																	<div class="panel-body">
																		<?php if($a['sad_defaut'] == 1){ ?>
																			Adresse par défaut<br>
																		<?php }?>
																		<?=$text_adresse?>
																	</div>
																	<div class="panel-footer">
																		<a href="suspect_adresse.php?suspect=<?=$suspect?>&adresse=<?=$a['sad_id']?>&onglet=11" class="btn btn-warning btn-sm" >
																			<i class="fa fa-pencil"></i>
																		</a>
																<?php 	if($a['sad_defaut'] != 1){ ?>
																			<div class="pull-right" >
																				<button type="button" data-adresse="<?= $a['sad_id']?>" class="btn btn-danger btn-sm deladresse">
																					<i class="fa fa-times"></i>
																				</button>
																			</div>
																<?php 	} ?>
																	</div>
																</div>
													<?php	}
														} ?>
														<div class="alert alert-warning" id="no_adresse_3" <?php if(!empty($adresses_env)) echo("style='display:none;'"); ?> >
															Pas d'adresse d'envoi de facture pour ce client.
														</div>
													</div>
										<?php	} ?>
											</div>
										</div>
										<!-- fin adresses -->

										<!-- contacts -->
										<div id="tab12" class="tab-pane <?php if($onglet==12) echo("active"); ?>" >

											<div class="row">
												<h2 style="margin-top:0px;">Carnet de contacts</h2>
										<?php 	if(!empty($contacts)){ ?>
													<table class="table table-striped table-hover" id="tab_contact" >
														<thead>
															<tr class="dark">
																<th class="text-center" >Fonction</th>
																<th class="text-center" >Nom</th>
																<th class="text-center" >Tel</th>
																<th class="text-center" >Portable</th>
																<th class="text-center" >Fax</th>
																<th class="text-center" >Mail</th>
																<th class="text-center" >Commentaire</th>
																<th class="text-center" >Contact "impayé"</th>
														<?php	if($drt_modifier){ ?>
																	<th class="text-center" >Actions</th>
														<?php	} ?>
															</tr>
														</thead>
														<tbody>
												<?php 		foreach($contacts as $c){
																$sql="SELECT * FROM Suspects_adresses
																LEFT JOIN suspects_adresses_contacts ON (suspects_adresses_contacts.sac_adresse = Suspects_adresses.sad_id)
																 WHERE sac_defaut AND sac_contact = " . $c['sco_id'];
																$req = $ConnSoc->query($sql);
																$d_contact_suppr=$req->fetch();

																$comment="";
																if(!empty($c['sco_comment'])){
																	$comment=str_replace(CHR(13),"<br/>",$c['sco_comment']);
																}?>
																<tr id="contact_<?=$c['sco_id']?>" class="contact" >
																	<td class="text-center">
												<?php 					if(!empty($c['sco_fonction'])){
																			echo($d_con_fonction_nom[$c['sco_fonction']]);
																		}else{
																			echo($c['sco_fonction_nom']);
																		} ?>
																	</td>
																	<td class="text-center">
																	<?php foreach($base_civilite as $k => $v): ?>
																	  <?php if($k == $c['sco_titre']): ?>
																		<?= $v; ?>
																	  <?php endif; ?>
																	<?php endforeach; ?>
																	<?= $c['sco_prenom']; ?> <?= $c['sco_nom']; ?>
																	</td>
																	<td class="text-center"><?= $c['sco_tel']; ?></td>
																	<td class="text-center"><?= $c['sco_portable']; ?></td>
																	<td class="text-center"><?= $c['sco_fax']; ?></td>
																	<td class="text-center"><?= $c['sco_mail']; ?></td>

																	<td class="text-center"><?=$comment?></td>
																	<td class="text-center">
																	<?php if(!empty($c['sco_compta'])){ ?>
																		<i class="fa fa-check"></i>
																	<?php } ?>
																	</td>
													<?php 			if($drt_modifier){ ?>
																		<td class="text-center">
																			<a href="suspect_contact.php?suspect=<?=$suspect?>&contact=<?=$c['sco_id']?>&onglet=12" class="btn btn-warning btn-sm" >
																				<i class="fa fa-pencil"></i>
																			</a>
																	<?php 	if(empty($d_contact_suppr)){ ?>
																				<button type="button" class="btn btn-danger btn-sm delcontact" data-contact="<?=$c['sco_id']?>" >
																					<i class="fa fa-times"></i>
																				</button>
																	<?php 	} ?>
																		</td>
													<?php 			} ?>
																</tr>
												<?php 		} ?>
														</tbody>
													</table>
										<?php 	} ?>

												<div class="alert alert-warning" id="no_contact" <?php if(!empty($contacts)) echo("style='display:none;'"); ?> >
													Pas de contact pour ce client.
												</div>

											</div>
										</div>
										<!-- fin contacts -->

									</div>
								</div>
							</div>
						</div>
		<?php		}else{ ?>
						<h1 class="bg-danger text-center pt25 pb25" >Impossible d'afficher la page!</h1>
		<?php		} ?>
				</section>
			<!-- End: Content -->
			</section>

		</div>
<?php	if($erreur==0){ ?>

			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-4 footer-left">
			<?php 		if(!empty($_SESSION['retour'])){ ?>
							<a href="<?= $_SESSION['retour']; ?>" class="btn btn-default btn-sm">
								<i class="fa fa-long-arrow-left"></i>
								Retour
							</a>
			<?php 		}
						if(isset($tab_prec)){
							if($tab_prec[1]==1){ ?>
								<a href="suspect_voir.php?suspect=<?=$tab_prec[0]?>" class="btn btn-default btn-sm">
									<i class="fa fa-long-arrow-left"></i>
								</a>
			<?php 			}else{ ?>
								<a href="client_voir.php?client=<?=$tab_prec[0]?>" class="btn btn-default btn-sm">
									<i class="fa fa-long-arrow-left"></i>
								</a>
			<?php			}
						} 
						//if($_SESSION["acces"]["acc_profil"]==13){ ?>
							<a href="suspect_transfert.php?id=<?= $d_suspect['sus_id'] ?>" class="btn btn-info btn-sm" >
								<i class="fa fa-check" ></i> Valider la fiche
							</a>
			<?php		//}
						if(!empty($_SESSION['acces']['acc_droits'][36])){ ?>
							<a href="suspect_transfert.php?id=<?= $d_suspect['sus_id']?>&option_verif=1" class="btn btn-danger btn-sm" >
								<i class="fa fa-check" ></i> forcer la fiche
							</a>
			<?php		}
						if(isset($tab_suiv)){ 
							if($tab_suiv[1]==1){ ?>
								<a href="suspect_voir.php?suspect=<?=$tab_suiv[0]?>" class="btn btn-default btn-sm">
									<i class="fa fa-long-arrow-right"></i>
								</a>
			<?php 			}else{ ?>
								<a href="client_voir.php?client=<?=$tab_suiv[0]?>" class="btn btn-default btn-sm">
									<i class="fa fa-long-arrow-right"></i>
								</a>
			<?php			}
						}?>
					</div>
					<div class="col-xs-4 footer-middle">
			<?php		if(!empty($_SESSION['acces']['acc_droits'][36])){ ?>
							<button type="button" class="btn btn-system btn-sm" data-toggle="modal" data-target="#modal_aide" >
								<i class="fa fa-info" ></i>
							</button>
			<?php		} ?>		
					</div>
					<div class="col-xs-4 footer-right">
			<?php 		if($drt_modifier){ ?>

							<!-- onglet général -->
							<div class="btn-onglet btn-onglet-general" <?php if($onglet!=1) echo("style='display:none;'");?> >
						<?php	if($d_suspect['sus_categorie']!=3){ ?>
									<a href="suspect_adresse.php?suspect=<?=$suspect?>" class="btn btn-success btn-sm" >
										<i class='fa fa-plus'></i> Adresse
									</a>
									<a href="suspect_contact.php?suspect=<?=$suspect?>" class="btn btn-success btn-sm" >
										<i class='fa fa-plus'></i> Contact
									</a>
						<?php 	} ?>
								<a href="suspect_correspondance.php?suspect=<?=$suspect?>" class="btn btn-success btn-sm" >
									<i class='fa fa-plus'></i> Correspondance
								</a>
								<a href="suspect_mod.php?suspect=<?=$suspect?>" class="btn btn-warning btn-sm" >
									<i class='fa fa-pencil'></i> Editer
								</a>
							</div>
							<!-- fin onglet général -->

							<!-- onglet adresse -->
							<a href="suspect_adresse.php?suspect=<?=$suspect?>&onglet=11" class="btn btn-success btn-sm btn-onglet btn-onglet-adresse" <?php if($onglet!=11) echo("style='display:none;'");?> >
								<i class='fa fa-plus'></i> Adresse
							</a>

							<!-- onglet contact -->
							<a href="suspect_contact.php?suspect=<?=$suspect?>&onglet=12" class="btn btn-success btn-sm btn-onglet btn-onglet-contact" <?php if($onglet!=12) echo("style='display:none;'");?> >
								<i class='fa fa-plus'></i> Contact
							</a>

							<!-- onglet correspondances -->
							<a href="suspect_correspondance.php?suspect=<?=$suspect?>&tab5" class="btn btn-success btn-sm btn-onglet btn-onglet-cor" <?php if($onglet!=5) echo("style='display:none;'");?> >
								<i class='fa fa-plus'></i> Correspondance
							</a>
				<?php 	} ?>
					</div>
				</div>
			</footer>

			<!-- MODAL -->

			<!-- modal de confirmation -->
			<div id="modal_confirmation_user" class="modal fade" role="dialog" >
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title">Confirmer ?</h4>
						</div>
						<div class="modal-body">
							<p>Message</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								Annuler
							</button>
							<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
								Confirmer
							</button>
						</div>
					</div>
				</div>
			</div>
			
			<div id="modal_aide" class="modal fade" role="dialog" >
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
							<h4 class="modal-title" id="mdl_doc_titre" >
								<i class="fa fa-info" ></i> - <span>Aide</span>
							</h4>
						</div>
						<div class="modal-body" >
							<p>
					<?php		if(!empty($_SESSION['acces']['acc_droits'][36])){ ?>
									Le bouton 
									<button class="btn btn-danger btn-sm" >
										<i class="fa fa-check" ></i> forcer la fiche
									</button>
									permet de valider une fiche même si le SIRET existe déjà dans la base.<br/>
									<b>Attention :</b> tous les autres contrôles sont maintenus et le SIRET reste quand même <b>obligatoire</b>. 
					<?php		} ?>
							</p>
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Fermer
							</button>
						</div>
					</div>
				</div>
			</div>
	
			<!-- FIN MODAL -->

<?php	}
		// DEB JS

		include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<script type="text/javascript" src="assets/admin-tools/admin-forms/js/jquery-ui-monthpicker.min.js"></script>

		<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>

		<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
		<script type="text/javascript" src="/assets/js/custom.js"></script>

		<script type="text/javascript">

			var suspect="<?=$d_suspect["sus_id"]?>";

			jQuery(document).ready(function (){
				// INTERACTION ONGLET

				$(".onglet").click(function(){

					onglet=$(this).data("onglet");
					console.log("onglet " + onglet);
					$(".btn-onglet").each(function(){
						if($(this).hasClass("btn-onglet-" + onglet)){
							$(this).show();
						}else{
							$(this).hide();
						}
					});
				});

				// INTERACTION CORRESPONDANCE

				$(".corresp-supp").click(function(){
					corresp=$(this).data("corresp");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer cette correspondance ?",preparer_supp_corresp,corresp);
				});

				$(".corresp-rappel").click(function(){
					corresp=$(this).data("corresp");
					sco_rappel=0;
					if($(this).is(":checked")){
						sco_rappel=1;
					}
					set_base_champ(<?=$acc_societe?>,"Suspects_Correspondances","sco_rappel",sco_rappel,"sco_id",corresp,actualiser_rappel,corresp);
				});

				// INTERACTION CONTACT

				$(".delcontact").click(function(){
					contact=$(this).data("contact");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer ce contact?",confirmer_contact_supp,contact);
				});

				// INTERACTION LIE AUX ADRESSES

				$(".deladresse").click(function(){
					adresse=$(this).data("adresse");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer cette adresse?",confirmer_adresse_supp,adresse);
				});

				// TRANSFERT EN PROSPECT
				/*	plus utilise
				$("#transfert").click(function(){
					check_transfert_suspect(suspect);
				});*/

				$("#sus_groupe").click(function(){
					if($(this).is(":checked")){
						$("#bloc_groupe").show();
					}else{
						$("#sus_holding").select2("val",0);
						$("#bloc_groupe").hide();
					}

				});
			});
			// FIN DE DOCUMENT READY

			// **** FONCTION ADRESSE ****

			function confirmer_adresse_supp(adresse){
				del_suspect_adresse(adresse,suspect,supprimer_adresse_suspect,adresse);
			}

			function supprimer_adresse_suspect(json){
				if(json.adresse){
					$("#adresse_" + json.adresse).remove();
				}

				if($(".adresse-" + json.type).length==0){
					$("#no_adresse_" + json.type).show();
				}
			}


			// **** FONCTION CONTACT ****

			function confirmer_contact_supp(contact){
				del_suspect_contact(contact,suspect,supprimer_contact_suspect,contact);
			}

			// modif de l'affichage suite à supp
			function supprimer_contact_suspect(json){
				if(json.contact){
					$("#contact_" + json.contact).remove();
				}
				if($(".contact").length==0){
					$("#tab_contact").remove();
					$("#no_contact").show();
				}
			}

			// *** FONCTION LIE A L'ONGLET COORESPONDANCE ***

			function preparer_supp_corresp(corresp){
				console.log("preparer_supp_corresp(" + corresp + ")");
				del_suspect_corresp(corresp,supprimer_corresp,corresp,"");
			}

			function supprimer_corresp(json){
				console.log("supprimer_corresp(json)");
				if(json){
					if(json.correspondance>0){
						$("#correspondance_" + json.correspondance).remove();
					}
					if($(".correspondance").size()==0){
						$("#no_corresp").show();
					}else{
						if(json.non_traite>0){
							$("#corresp_rappel_" + json.non_traite).prop("checked",false);
						}
					}
				}
			}

			function actualiser_rappel(json,corresp){
				if(json){
					if(json.erreur==0){
						// reussite
						afficher_txt_user($(".footer-middle"),"Mise à jour terminée","text-success",5000);
					}else{
						// erreur
						if($("corresp_rappel_" + corresp).is(":checked")){
							$(this).prop("checked",false);
						}else{
							$(this).prop("checked",true);
						}
						modal_alerte_user(json.erreur_txt);
					}
				}
			}

			// TRANSFERT EN PROSPECT
			/* PAS UTILISE
			on pas par un lien classique et plus par une requete ajax
			function check_transfert_suspect(suspect){
				console.log("suspect " + suspect);
				if(suspect>0){
					$.ajax({
						type:'POST',
						url: 'ajax/ajax_check_transfert_suspect.php',
						data : "suspect=" + suspect,
						dataType: 'json',
						success: function(data){

							$("#modal_transfert").modal("show");
                            console.log("JSON");
						},
						error: function(data) {

							modal_alerte_user(data.responseText);
						}
					});
				}
			}*/

		</script>
	</body>
</html>
