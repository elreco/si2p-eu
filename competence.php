<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_competence.php');
	include('modeles/mod_diplome.php');
	
	$com_id=0;
	$com_libelle="";
	if(!empty($_GET["competence"])){
		$com_id=$_GET["competence"];
		$competence=get_competence($com_id);
		$com_libelle=$competence["com_libelle"];
	};
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
		<form method="post" action="competence_enr.php" >
			<div>
				<input type="hidden" name="competence" value="<?=$com_id?>" />
			</div>
			<!-- Start: Main -->
			
			<div id="main">
				<?php	
					include "includes/header_def.inc.php";
				?>	
					
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper">		 
					<!-- Begin: Content -->
								
					<section id="content" class="animated fadeIn">
					
					
						<div class="admin-form theme-primary ">
						
							<div class="panel heading-border panel-primary">
								<div class="panel-body bg-light">
								
									<div class="row" >
									
										<div class="col-md-10 col-md-offset-1" >
								
											<div class="row" >
												<div class="col-md-12" >							
													<div class="text-center">								
														<div class="content-header">
												<?php	if($com_id){ ?>
															<h2>Mise à jour d'une <b class="text-primary">compétence</b></h2>
												<?php	}else{ ?>
															<h2>Nouvelle <b class="text-primary">compétence</b></h2>	
												<?php	} ?>
															
														</div>
													</div>
												</div>
											</div>									
													
											<div class="row">
												<div class="col-md-12">
													<div class="field">								 
														<input type="text" class="gui-input" name="com_libelle" id="com_libelle" placeholder="Libellé de la compétence" required="" value="<?=$com_libelle?>" />
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span>Diplômes associés</span>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-12">

													<table class="table table-striped table-hover" >
														<thead>
															<tr class="dark" >
																<th>Diplôme</th>																																
																<th>Demander aux intervenants Si2P</th>
																<th>Demander aux sous-traitants</th>
																<th>Obligatoire</th>
															</tr>
														</thead>
												<?php	$diplome=get_diplomes_competence($com_id);
														if(!empty($diplome)){ ?>
															<tbody>
												<?php			foreach($diplome as $value){ 
																	
																	$interne=false;
																	if(!empty($value["dco_interne"])){
																		$interne=true;
																	}
																	$externe=false;
																	if(!empty($value["dco_externe"])){
																		$externe=true;
																	}
																	$obligatoire=false;
																	if(!empty($value["dco_obligatoire"])){
																		$obligatoire=true;
																	} ?>
																	<tr>
																		<td><?=$value["dip_libelle"]?></td>																													
																		<td class="text-center" >
																			<label class="option">
																				<input type="checkbox" name="interne_<?=$value["dip_id"]?>" id="interne_<?=$value["dip_id"]?>" value="on" <?php if($interne) echo("checked") ; ?> >
																				<span class="checkbox"></span>
																			</label>
																		</td>
																		<td class="text-center" >
																			<label class="option">
																				<input type="checkbox" name="externe_<?=$value["dip_id"]?>" id="externe_<?=$value["dip_id"]?>" value="on" <?php if($externe) echo("checked") ; ?> >
																				<span class="checkbox"></span>
																			</label>
																		</td>
																		<td class="text-center" >
																			<label class="option">
																				<input type="checkbox" name="obligatoire_<?=$value["dip_id"]?>" id="obligatoire_<?=$value["dip_id"]?>" value="on" <?php if($obligatoire) echo("checked") ; ?> >
																				<span class="checkbox"></span>
																			</label>
																		</td>													
																	</tr>
												<?php 			}; ?>
															
															</tbody>																												
												<?php 	}; ?>
														
													</table>
													
												</div>
											</div>
											
											
									
										</div>
										
									</div>
										
								</div>
							</div>
							
						</div>

					</section>
					<!-- End: Content -->
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix"  >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="competence_liste.php" class="btn btn-default btn-sm" >
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" >
					</div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm" >
							<i class="fa fa-add" ></i>
							Enregistrer
						</button>
					</div>
				</div>
			</footer>
	</form>
									
		
	<?php
		include "includes/footer_script.inc.php"; ?>	                         

		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
	</body>
</html>
