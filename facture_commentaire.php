<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';
include_once 'modeles/mod_parametre.php';

if (isset($_GET['id'])){
    $req=$ConnSoc->query("SELECT * FROM factures_commentaires WHERE fco_id =" . $_GET['id']);
	$com=$req->fetch();
}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Commentaire facture</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
<form action="facture_commentaire_enr.php" method="POST" id="admin-form">
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">
            
            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-left">

                                        <div class="content-header">
                                            <?php if(!empty($_GET['id'])){ ?>

                                                <h2>Modifier le commentaire <b class="text-primary"><?= $_GET['id'] ?></b></h2>
                                            <?php } ?>
                                        </div>

                                        <div class="col-md-10 col-md-offset-1">
                                         

                                                <?php if (isset($_GET['id'])): ?>
                                                    <input type="hidden" name="fco_id" value="<?=$com['fco_id']?>"></input>
                                                <?php endif;?>
                                                
                                          <div class="row">
                                                <div class="col-md-12">
                                                    <div class="alert alert-info">
                                                        Si vous ne mettez rien, le commentaire sera supprimé
                                                    </div>
                                                </div>
                                            <div class="col-md-6">
                                                    <div class="section">
                                                    <label class="field prepend-icon" style="width:100%">
                                                            <textarea class="summernote-img" id="commentaire" name="commentaire" placeholder="Texte du commentaire"><?= $com['fco_comment'] ?></textarea>																
                                                        </label>
                                                    </div>
                                            </div>
                                          </div>
                                         
                                      </div>

                                  </div>
                              </div>


                          </div>
                      </div>

                  </div>
              </div>

          </div>
      </section>
      <!-- End: Content -->
  </section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="<?= $_SESSION['retourFacture'] ?>" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-save'></i> Enregistrer
            </button>
        </div>
    </div>
</footer>
</form>
<?php
	include "includes/footer_script.inc.php"; ?>	

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script> 

</body>
</html>
