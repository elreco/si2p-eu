<?php

	// STATISTIQUE CA PAR PRODUIT

	include "includes/controle_acces.inc.php";

	include "includes/connexion.php";
	include "includes/connexion_soc.php";


	// CONTROLE ACCES

	if($_SESSION['acces']['acc_service'][1]!=1 AND $_SESSION['acces']['acc_service'][2]!=1 AND $_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']['acc_service'][5]!=1){
		echo("Impossible d'afficher la page!");
		die();
	}

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}


	// TRAITEMENT DU FORM

	$crit_stat=array();
	if(!empty($_POST)){

		$periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$periode_deb_fr=$_POST["periode_deb"];
			$DT_periode=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode)){
				$periode_deb=$DT_periode->format("Y-m-d");

			}
		}
		$crit_stat["periode_deb"]=$periode_deb;

		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$periode_fin_fr=$_POST["periode_fin"];
			$DT_periode=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode)){
				$periode_fin=$DT_periode->format("Y-m-d");
			}
		}
		$crit_stat["periode_fin"]=$periode_fin;

		$qualification=0;
		if(!empty($_POST["qualification"])){
			$qualification=intval($_POST["qualification"]);
		}
		$crit_stat["qualification"]=$qualification;

		$_SESSION["crit_stat"]=$crit_stat;

	}elseif(isset($_SESSION["crit_stat"])){

		$crit_stat=$_SESSION["crit_stat"];

		$DT_periode=date_create_from_format('Y-m-d',$crit_stat["periode_deb"]);
		if(!is_bool($DT_periode)){
			$periode_deb_fr=$DT_periode->format("d/m/Y");
		}

		$DT_periode=date_create_from_format('Y-m-d',$crit_stat["periode_fin"]);
		if(!is_bool($DT_periode)){
			$periode_fin_fr=$DT_periode->format("d/m/Y");
		}

	}

	if(empty($crit_stat)){
		echo("Formulaire incomplet");
		die();
	}elseif(empty($crit_stat["qualification"])){
		echo("Formulaire incomplet");
		die();
	}

	// TRAITEMENT

	$sql="SELECT qua_libelle FROM Qualifications WHERE qua_id=:qualification;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":qualification",$crit_stat["qualification"]);
	$req->execute();
	$d_qualification=$req->fetch(PDO::FETCH_ASSOC);
	if(empty($d_qualification)){
		echo("Impossible d'identifier la qualification");
		die();
	}


	// ON LISTE LES PRODUITS CONCERNE PAR LA Statistiques

	$sql="SELECT pro_id FROM Produits WHERE pro_qualification=:qualification;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":qualification",$crit_stat["qualification"]);
	$req->execute();
	$d_results=$req->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($d_results)){
		$tab_result=array_column($d_results,"pro_id");
		$liste_produit=implode($tab_result,",");
	}

	if(!empty($liste_produit)){

		// LISTE DES FACTURES

		$sql="SELECT cli_id,cli_code,cli_nom,SUM(fli_montant_ht) AS ca FROM Factures_Lignes,Factures,Clients
		WHERE fli_facture=fac_id AND fac_client=cli_id AND fac_agence=cli_agence
		AND fli_produit IN (" . $liste_produit . ") AND fac_date>='". $crit_stat["periode_deb"] . "' AND fac_date<='" . $crit_stat["periode_fin"] . "'";
		if(!empty($acc_agence)){
			$sql.="	AND fac_agence=" . $acc_agence;
		}
		$sql.=" GROUP BY cli_id,cli_code,cli_nom
		ORDER BY cli_code;";
		$req=$ConnSoc->query($sql);
		$d_factures=$req->fetchAll();


		/*$sql_contact="SELECT con_nom,con_prenom,con_mail FROM Adresses,Adresses_Contacts,Contacts
		WHERE adr_id=aco_adresse AND aco_contact=con_id
		AND adr_ref=1 AND adr_ref_id=:client AND adr_type=2 AND adr_defaut AND aco_defaut;";
		$req_contact=$Conn->prepare($sql_contact);*/

		$sql_contact="SELECT cli_con_nom,cli_con_prenom,cli_con_mail FROM Clients WHERE cli_id=:client;";
		$req_contact=$Conn->prepare($sql_contact);


	}

	$_SESSION["retourFacture"]="stat_com_qualif.php";
	$_SESSION['retour'] = "stat_com_qualif.php";
	$_SESSION["retourClient"]="stat_com_qualif.php";
	$_SESSION["retour_action"]="stat_com_qualif.php";
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">

<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" >
					<div class="row ">

						<h1 class="text-center" >
					<?php
							echo("CA " . $d_qualification["qua_libelle"]);
							echo("<br/>du " . $periode_deb_fr . " au " . $periode_fin_fr);

							?>
						</h1>

						<div class="table-responsive">
							<table class="table table-striped table-hover" >
								<thead>
									<tr class="dark">
										<th>Code</th>
										<th>Nom</th>
										<th>Contact</th>
										<th>Mail</th>
										<th>CA</th>
									</tr>
								</thead>
								<tbody>
						<?php		if(!empty($d_factures)){
										$total_ca=0;
										foreach($d_factures as $fac){


											$req_contact->bindParam(":client",$fac["cli_id"]);
											$req_contact->execute();
											$d_contact=$req_contact->fetch();


											$total_ca=$total_ca+$fac["ca"]; ?>
											<tr>
												<td>
													<a href="client_voir.php?client=<?= $fac["cli_id"] ?>">
														<?=$fac["cli_code"]?>
													</a>
												</td>
												<td><?=$fac["cli_nom"]?></td>
												<td><?=$d_contact["cli_con_nom"] . " " . $d_contact["cli_con_prenom"]?></td>
												<td><?=$d_contact["cli_con_mail"]?></td>
												<td class="text-right" >
													<a href="stat_com_qualif_fac.php?client=<?=$fac["cli_id"]?>" >
														<?=number_format($fac["ca"],2,","," ")?>
													</a>
												</td>
											</tr>
						<?php
										} ?>
										<tr>
											<th colspan="4" class="text-right" >Total :</th>
											<td class="text-right" >
												<a href="stat_com_qualif_fac.php" >
													<?=number_format($total_ca,2,","," ")?>
												</a>
											</td>
										</tr>
						<?php		} ?>
								</tbody>
							</table>
						</div>

					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="stat_commercial.php" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" style="">
					<button type="button" class="btn btn-system btn-sm" data-toggle="modal" data-target="#modal_aide" >
						<i class="fa fa-info" ></i>
					</button>
				</div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>
		<div id="modal_aide" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="mdl_doc_titre" >
							<i class="fa fa-info" ></i> - <span>Aide</span>
						</h4>
					</div>
					<div class="modal-body" >
						<p>
							<b>Contact :</b> le contact affiché est le contact par défaut du lieu d'intervention par défaut.
						</p>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
							<i class="fa fa-close" ></i>
							Fermer
						</button>
					</div>
				</div>
			</div>
		</div>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){



			});
		</script>
	</body>
</html>
