<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');

$erreur_txt="";

if(!empty($_POST)){
	if(empty($_SESSION['acces']['acc_agence'])){
		$_SESSION['acces']['acc_agence'] = 0;
	}
	$cgr_id=0;
	if(!empty($_POST['grille'])){
		$cgr_id=intval($_POST['grille']);
	}

	$cgr_famille=0;
	if(!empty($_POST['cgr_famille'])){
		$cgr_famille=intval($_POST['cgr_famille']);
	}

	$cgr_montant_min=0;
	if(!empty($_POST['cgr_montant_min'])){
		$cgr_montant_min=intval($_POST['cgr_montant_min']);
	}

	$type_1=0;
	$cgr_utilisateur_1=0;
	$cgr_profil_1=0;
	$cgr_responsable_1=0;
	$cgr_droit_1=0;
	if(!empty($_POST['type_1'])){
		$type_1=intval($_POST['type_1']);
		switch ($type_1) {
			case 2:
				$cgr_utilisateur_1=intval($_POST['utilisateur_1']);
				break;
			case 1:
				$cgr_profil_1=intval($_POST['profil_1']);
				break;
			case 3:
				$cgr_responsable_1=1;
				break;
			case 4:
				$cgr_droit_1=intval($_POST['droit_1']);
				break;
		}
	}
	$type_2=0;
	$cgr_utilisateur_2=0;
	$cgr_profil_2=0;
	$cgr_droit_2=0;
	if(!empty($_POST['type_2'])){
		$type_2=intval($_POST['type_2']);
		switch ($type_2) {
			case 2:
				$cgr_utilisateur_2=intval($_POST['utilisateur_2']);
				break;
			case 1:
				$cgr_profil_2=intval($_POST['profil_2']);
				break;
			case 4:
				$cgr_droit_2=intval($_POST['droit_2']);
				break;

		}
	}
	$type_3=0;
	$cgr_utilisateur_3=0;
	$cgr_profil_3=0;
	$cgr_droit_3=0;
	if(!empty($_POST['type_3'])){
		$type_3=intval($_POST['type_3']);
		switch ($type_3) {
			case 2:
				$cgr_utilisateur_3=intval($_POST['utilisateur_3']);
				break;
			case 1:
				$cgr_profil_3=intval($_POST['profil_3']);
				break;
			case 4:
				$cgr_droit_3=intval($_POST['droit_3']);
				break;
	}
	}
	$type_4=0;
	$cgr_utilisateur_4=0;
	$cgr_profil_4=0;
	$cgr_droit_4=0;
	if($cgr_famille!=2){
		// si ST, il n'y a que 3 niveau de validation
		if(!empty($_POST['type_4'])){
			$type_4=intval($_POST['type_4']);
			switch ($type_4) {
				case 2:
					$cgr_utilisateur_4=intval($_POST['utilisateur_4']);
					break;
				case 1:
					$cgr_profil_4=intval($_POST['profil_4']);
					break;
			}
		}
	}

	if(empty($cgr_id) AND empty($cgr_famille)){
		$erreur_txt="Formulaire incomplet";
	}
}else{
	$erreur_txt="Formulaire incomplet";
}
if(empty($erreur_txt)){

	if(!empty($cgr_id)){
		$req = $Conn->prepare("SELECT cgr_famille FROM commandes_grilles WHERE cgr_id = " . $cgr_id);
		$req->execute();
		$d_grille = $req->fetch();
		if(!empty($d_grille)){
			$cgr_famille=$d_grille["cgr_famille"];
		}else{
			$erreur_txt="Impossible de charger la grille";
		}
	}
}
if(empty($erreur_txt)){

	if(empty($cgr_id)){

		// CREATION

		$req = $Conn->prepare("INSERT INTO commandes_grilles (cgr_societe,cgr_agence,cgr_montant_min,cgr_famille,
		cgr_profil_1, cgr_utilisateur_1,cgr_responsable_1, cgr_droit_1, cgr_profil_2, cgr_utilisateur_2, cgr_droit_2, cgr_profil_3, cgr_utilisateur_3, cgr_droit_3,
		cgr_profil_4, cgr_utilisateur_4, cgr_droit_4
		) VALUES (:cgr_societe,:cgr_agence,:cgr_montant_min,:cgr_famille,
		:cgr_profil_1, :cgr_utilisateur_1,:cgr_responsable_1, :cgr_profil_2, :cgr_utilisateur_2, :cgr_profil_3, :cgr_utilisateur_3, :cgr_profil_4, :cgr_utilisateur_4);");
		$req->bindParam(":cgr_montant_min",$cgr_montant_min);
		$req->bindParam(":cgr_famille",$cgr_famille);
		$req->bindParam(":cgr_profil_1",$cgr_profil_1);
		$req->bindParam(":cgr_utilisateur_1",$cgr_utilisateur_1);
		$req->bindParam(":cgr_responsable_1",$cgr_responsable_1);
		$req->bindParam(":cgr_droit_1",$cgr_droit_1);
		$req->bindParam(":cgr_profil_2",$cgr_profil_2);
		$req->bindParam(":cgr_utilisateur_2",$cgr_utilisateur_2);
		$req->bindParam(":cgr_droit_2",$cgr_droit_2);
		$req->bindParam(":cgr_profil_3",$cgr_profil_3);
		$req->bindParam(":cgr_utilisateur_3",$cgr_utilisateur_3);
		$req->bindParam(":cgr_droit_3",$cgr_droit_3);
		$req->bindParam(":cgr_profil_4",$cgr_profil_4);
		$req->bindParam(":cgr_utilisateur_4",$cgr_utilisateur_4);
		$req->bindParam(":cgr_droit_4",$cgr_droit_4);
		$req->bindParam(":cgr_societe",$_SESSION['acces']['acc_societe']);
		$req->bindParam(":cgr_agence",$_SESSION['acces']['acc_agence']);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}

	}else{
		$req = $Conn->prepare("UPDATE commandes_grilles SET
		cgr_montant_min=:cgr_montant_min,
		cgr_profil_1=:cgr_profil_1,
		cgr_utilisateur_1=:cgr_utilisateur_1,
		cgr_responsable_1=:cgr_responsable_1,
		cgr_droit_1=:cgr_droit_1,
		cgr_profil_2=:cgr_profil_2,
		cgr_utilisateur_2=:cgr_utilisateur_2,
		cgr_droit_2=:cgr_droit_2,
		cgr_profil_3=:cgr_profil_3,
		cgr_utilisateur_3=:cgr_utilisateur_3,
		cgr_droit_3=:cgr_droit_3,
		cgr_profil_4=:cgr_profil_4,
		cgr_utilisateur_4=:cgr_utilisateur_4,
		cgr_droit_4=:cgr_droit_4
		WHERE cgr_id=:cgr_id;");
		$req->bindParam(":cgr_montant_min",$cgr_montant_min);
		$req->bindParam(":cgr_id",$cgr_id);
		$req->bindParam(":cgr_profil_1",$cgr_profil_1);
		$req->bindParam(":cgr_utilisateur_1",$cgr_utilisateur_1);
		$req->bindParam(":cgr_responsable_1",$cgr_responsable_1);
		$req->bindParam(":cgr_droit_1",$cgr_droit_1);
		$req->bindParam(":cgr_profil_2",$cgr_profil_2);
		$req->bindParam(":cgr_utilisateur_2",$cgr_utilisateur_2);
		$req->bindParam(":cgr_droit_2",$cgr_droit_2);
		$req->bindParam(":cgr_profil_3",$cgr_profil_3);
		$req->bindParam(":cgr_utilisateur_3",$cgr_utilisateur_3);
		$req->bindParam(":cgr_droit_3",$cgr_droit_3);
		$req->bindParam(":cgr_profil_4",$cgr_profil_4);
		$req->bindParam(":cgr_utilisateur_4",$cgr_utilisateur_4);
		$req->bindParam(":cgr_droit_4",$cgr_droit_4);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}
	}
}
if(empty($erreur_txt)){

	$req_max = $Conn->prepare("UPDATE commandes_grilles SET
	cgr_montant_max=:cgr_montant_max
	WHERE cgr_id=:cgr_id;");

	/* $req = $Conn->query("SELECT cgr_id,cgr_montant_min,cgr_montant_max FROM commandes_grilles WHERE cgr_societe = " . $_SESSION['acces']['acc_societe'] . " AND cgr_famille= " . $cgr_famille . " ORDER BY cgr_montant_min"); */
	$req = $Conn->query("SELECT cgr_id,cgr_montant_min,cgr_montant_max FROM commandes_grilles WHERE cgr_agence = " . $_SESSION['acces']['acc_agence'] . " AND cgr_societe = " . $_SESSION['acces']['acc_societe'] . " AND cgr_famille= " . $cgr_famille . " ORDER BY cgr_montant_min");
	$d_tranches = $req->fetchAll();
	if(!empty($d_tranches)){
		foreach($d_tranches as $k => $t){
			if($k<count($d_tranches)-1 && !empty($d_tranches[$num]["cgr_montant_min"])){
				$num=intval($k+1);
				$cgr_montant_max=floatval($d_tranches[$num]["cgr_montant_min"]);
				$cgr_montant_max=$cgr_montant_max-0.01;
			}else{
				$cgr_montant_max=99999999;
			}
			$req_max->bindParam(":cgr_id",$t["cgr_id"]);
			$req_max->bindParam(":cgr_montant_max",$cgr_montant_max);
			$req_max->execute();
		}
	}
}
if(!empty($erreur_txt)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur_txt
	);
}

Header('Location: validations_bc.php?famille=' . $cgr_famille);
die();
?>
