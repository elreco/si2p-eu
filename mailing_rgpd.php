<?php
include("includes/connexion.php");

include_once 'modeles/mod_get_mail.php';
require('vendor/mailin-smtp-api-master/Mailin.php');

$erreur_txt="";

$sujet="RGPD : choisissez les communications que vous souhaitez recevoir";

$sql="SELECT con_id,con_ref_id,con_nom,con_prenom,con_mail,con_hash_public FROM Contacts WHERE NOT con_mail_doublon=1 AND NOT con_mail='' AND NOT ISNULL(con_mail) AND NOT con_rgpd=1;";
//AND con_id>5443;";
$req=$Conn->query($sql);
$d_mailing=$req->fetchAll();
if(!empty($d_mailing)){

	foreach($d_mailing as $mail){
		
		
		$param=array(
			"cle_public" => $mail["con_hash_public"]
		);
		
		$html=get_mail("rgpd",$param);
			
		$mailin = new Mailin('marketing@si2p.fr', 'I0SCGEMhN57yfVPv');
		$mailin->
		addTo($mail["con_mail"], $mail["con_prenom"] . " " . $mail["con_nom"])->
		setFrom('info@reseau.si2p.fr', 'Si2P')->
		setReplyTo('dpo@si2p.fr','DPO Si2P')->
		setSubject($sujet)->
		setHtml($html);
		$res = $mailin->send();	
		$res_tab=json_decode($res);
	
		if(is_bool($res_tab->result)){
			
			if($res_tab->result==true){
		
				$sql="UPDATE contacts SET con_rgpd = 1,con_hash_public = :con_hash_public WHERE con_id=:contact AND con_ref_id=:client;";
				$req = $Conn->prepare($sql);
				$req->bindParam("con_hash_public",$mail["con_hash_public"]);
				$req->bindParam("contact",$mail["con_id"]);
				$req->bindParam("client",$mail["con_ref_id"]);					
				try{
					$req->execute();
				}catch( PDOException $Exception ){
					$erreur_txt.=$Exception->getMessage() . "</br>";
					$erreur_txt.="Erreur 002! Le contact ". $mail["con_id"] ." n'a pas été mis à jour. Contacter l'administrateur du site.<br/>";
				}
			}else{
				$erreur_txt.="Erreur 003! Mail " . $mail["con_mail"] . " du contact N°". $mail["con_id"] ." n'a pas été envoyé.<br/>";
			}
			
		}else{			
			$erreur_txt.="Erreur 001! Mail " . $mail["con_mail"] . " du contact N°". $mail["con_id"] ." n'a pas été envoyé.<br/>";

		}
		
	}
}else{
	$erreur_txt="Pas de mail en attente!";
}


if(!empty($erreur_txt)){
	echo($erreur_txt);
}else{
	echo("TERMINE");
}
?>
