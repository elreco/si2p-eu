<?php
////////////////// MENU ACTIF HEADER ///////////////////
$menu_actif = 3;
setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_get_juridique.php');
include('modeles/mod_parametre.php');

include('modeles/mod_conge_data.php');

// VISUALISATION D'UN DEVIS

$erreur=0;

$conge_id=0;
if(!empty($_GET["conge"])){
	$conge_id=intval($_GET["conge"]);
	$_SESSION['retour_conge'] = "conges_voir.php?conge=" . $_GET['conge'];
}


if(empty($conge_id)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();

}else{

	// la personne logue
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}

	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	// ON RECUPERE LES DONNES DU DEVIS A AFFICHER

	$data=conge_data($conge_id);

	if(empty($data)){
		$erreur_txt="impossible d'afficher la page";
		echo($erreur_txt);
		die();
	}
	if($_SESSION['acces']['acc_ref_id'] != $data['con_utilisateur'] && $_SESSION['acces']['acc_ref_id'] != $data['con_responsable'] && $_SESSION['acces']['acc_profil'] != 7 && $_SESSION['acces']['acc_profil'] != 12 && $_SESSION['acces']['acc_profil'] != 10 && $_SESSION['acces']['acc_profil'] != 15 && $_SESSION['acces']['acc_profil'] != 13 &&  $_SESSION['acces']['acc_ref_id'] != $data['con_creation_uti']){
		$erreur_txt="impossible d'afficher la page";
		$erreur = 1;
		// ALLER CHERCHE LE RESPONSABLE DU RESPONSABLE
		if(!empty($data['con_responsable'])){
			$sql_com="SELECT uti_id, uti_nom, uti_prenom FROM utilisateurs WHERE uti_id = " . $data['con_responsable'];
			$req_com=$Conn->query($sql_com);
			$d_responsable_responsable=$req_com->fetch();

			if(!empty($d_responsable_responsable) && $_SESSION['acces']['acc_ref_id'] == $d_responsable_responsable['uti_id']){
				$erreur = 0;
			}
		}

	}
	// DONNE COMPLEMENTAIRE A PLUS DE L'AFFICHAGE (pas utile dans la version PDF donc pas dans data).

	/* $devis_pdf=file_exists("documents/Societes/" . $acc_societe . "/Devis/" . $data["dev_numero"] . ".pdf"); */

	// CLASSIFICATION PRODUIT
	/* $categorie_produit=orion_produits_categories();
	$famille_produit=orion_produits_familles(); */

} ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Demande de congés <?=$data["con_id"]?></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >

    #container_print{
                    width:21cm;
                }
    table{
        width:100%;
        color:#333333;
        font-size:11pt;
        font-family:helvetica;
        line-height:6mm;
        border-collapse:collapse;
    }


    .text-right{
        text-align:right;
    }
    .text-center{
        text-align:center;
    }

    table.border{

    }
    table.border th{
        background-color:#333333;
        color:#FFFFFF;
        text-align:center;
    }

    table.border td{
        border:1px solid #7e8082;
    }



    h3{
        font-size:12pt;
    }
    .cadre{
        border:1px solid #333333;
        padding:5px;
    }

    .juridique{
        font-size:7pt;
        line-height:3mm;
        text-align:center;
    }

    h2{
        color:#333333;
        font-size: 24px;
        margin-top: 9.5px;
        margin-bottom: 9.5px;
    }
    .text-primary{
        color: #E31936;
    }
    h4{
        font-size:12pt;
        margin:0px;

    }
    h5{
        font-size:8pt;
        color:#E31936;
        margin:5px 0px 0px 0px;
    }
    h6{
        font-size:8pt;
        margin:5px 0px 0px 0px;
    }
    .cgv-intro{
        color:#E31936;
        font-size:8pt;
        text-align:justify;
        margin:5px 0px 0px 0px;
    }
    .cgv{
        text-align:justify;
        font-size:7pt;
        margin:5px 0px 0px 0px;
    }
    .titre{

        background-color:#333333;
        color:white;
    }
    .mt20{

        margin-top:20px;
    }

	.ligne-col-1{
				width:20%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-2{
				width:52%;
				padding:1px 5px;
				font-size:10px;
			}
			.ligne-col-3{
				width:11%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-4{
				width:6%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-5{
				width:11%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.cachet{
				padding:5px 5px 20px 5px;
				border:1px solid #333;
			}
			.cachet small{
				font-size:7pt;
			}
    </style>
    <style type="text/css" media="print" >
        @page{
            size:A4 portrait;
            margin:5mm;
        }
    </style>
	</head>
	<body class="sb-top sb-top-sm document-affiche">
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php 		if($erreur==0){

			?>

							<div class="row" >

								<!-- zone d'affichage du devis -->

								<div class="col-md-8"  >

									<div id="container_print" >

										<div id="page_print" >

											<!--<div id="controleH" ></div>-->

											<div class="page" id="header_0" data-page="0" >
												<header>
													<table>
														<tr>
															<td class="w-25" >
													<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
																	<img src="<?=$data["juridique"]["logo_1"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td class="w-50 text-center" ><h1>FICHE DE CONGÉS INDIVIDUELS</h1></td>
															<td class="w-25" >
													<?php		if(!empty($data["juridique"]["logo_2"])){ ?>
																	<img src="<?=$data["juridique"]["logo_2"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
														</tr>
													</table>
												</header>
											</div>

											<div id="head_0" class="head" >
												<section>
													<table>
														<tr>
															<td class="text-right" >
																Le <?=convert_date_txt($data["con_creation_date"])?>
															</td>
														</tr>
													</table>

												</section>


											<table>
												<tr>
													<td style="width:100%;" >
														<div class="cadre">
															<h4 style="margin:0;font-size:12px;text-align:center;font-size:16px;margin-bottom:10px" class="mt20" >SIGNALÉTIQUE DU SALARIÉ</h4>
															<div style="width:35%;padding:15px;display:inline-block;">

																Nom : <strong><?= $data['con_nom'] ?></strong>

															</div>
															<div style="width:35%;display:inline-block;padding:15px;">

																Prénom : <strong><?= $data['con_prenom'] ?></strong>

															</div>
															<div style="width:100%;display:inline-block;padding:15px;">

																Matricule : <strong><?= $data['con_matricule'] ?></strong>

															</div>
														</div>
													</td>

												</tr>
												<tr>
													<td style="width:100%;" >
														<div class="cadre">
															<h4 style="margin:0;font-size:12px;text-align:center;font-size:16px;margin-bottom:10px" class="mt20" >CONGÉS</h4>
															<div style="width:100%;padding:15px;display:inline-block;">

																Type : <strong><?= $data['categories'][$data['con_categorie']] ?></strong>

															</div>
															<div style="width:100%;display:inline-block;padding:15px;">

																Du : <strong><?= convert_date_fr_le($data['con_date_deb']) ?>
																<?php if($data['con_demi_deb']==1){ ?>
																	matin
																<?php }else{ ?>
																	après-midi
																<?php } ?>
																 inclus</strong>

															</div>
															<div style="width:100%;display:inline-block;padding:15px;">

															Au : <strong><?= convert_date_fr_le($data['con_date_fin']) ?>
																<?php if($data['con_demi_fin']==1){ ?>
																	matin
																<?php }else{ ?>
																	après-midi
																<?php } ?>
																 inclus</strong>

															</div>
															<div style="width:100%;display:inline-block;padding:15px;">

															Soit : <strong><?= number_format($data['con_nb_jour'], 1, ',', ' ')?>
																<?php if($data['con_nb_jour']>1){ ?>
																	jours
																<?php }else{ ?>
																	jour
																<?php } ?>
																</strong>

															</div>
														</div>
													</td>

												</tr>
												<tr>
												<td>
													<p>Informations complémentaires</p>
													<p><?= $data['con_comment'] ?></p>
												</td>

												</tr>
											</table>
											</div>
											<div id="footer_0" >
												<footer>
													<table>
														<tr>
															<?php if(empty($data['con_annul'])){ ?>
															<!-- <td class="w-50">
															<div class="cadre"  style="min-height:115px;">
																<h4 style="margin-bottom:10px;">Visa du demandeur</h4>
																<p style="text-align:center;"><?=	$data['con_prenom'] ?> <?=	$data['con_nom'] ?> </p>
																	<?php if((($_SESSION['acces']['acc_profil'] == 7 OR $_SESSION['acces']['acc_profil'] == 12) OR ($_SESSION['acces']['acc_ref_id'] == $data['con_utilisateur'])) && empty($data['con_uti_valide'])){?>
																		<p style="text-align:center;">
																			<a href="conges_valide.php?conge=<?= $data['con_id'] ?>&uti=<?= $_SESSION['acces']['acc_ref_id'] ?>&ok" class="btn btn-sm btn-success">
																				<i class="fa fa-check"></i> Valider
																			</a> <a href="conges_valide.php?conge=<?= $data['con_id'] ?>&uti=<?= $_SESSION['acces']['acc_ref_id'] ?>&refus" class="btn btn-sm btn-danger">
																				<i class="fa fa-times"></i> Refuser
																			</a>
																		</p>
																	<?php }?>
																	<?php if(!empty($data['con_uti_valide'])){
																		if($data['con_uti_valide'] == 1){
																		?>
																		<p style="text-align:center"><span  style="color:green">Validé</span><br>
																		<strong><?= convert_date_fr($data['con_uti_valide_date']); ?></strong></p>
																		<?php }elseif($data['con_uti_valide'] == 2){ ?>
																			<p style="text-align:center"><span  style="color:red">Refusé</span><br>
																		<strong><?= convert_date_fr($data['con_uti_valide_date']); ?></strong></p>
																		<?php }elseif($data['con_uti_valide'] == 3){ ?>
																			<p style="text-align:center"><span  style="color:green">Validation automatique</span><br>
																		</p>
																		<?php }?>
																	<?php }?>
																</div>
															</td> -->
															<td class="text-right w-100">
																<div class="cadre"  style="min-height:115px;">
																	<h4 style="margin-bottom:10px;">Visa du responsable</h4>



																	<?php if(!empty($data['con_resp_po'])){ ?>
																				<p style="text-align:center;"><?=$data['po']['uti_prenom'] ?> <?=	$data['po']['uti_nom'] ?> <br>PO <?=	$data['responsable']['uti_prenom'] ?> <?=	$data['responsable']['uti_nom'] ?></p>

																			<?php }else{ ?>
																				<p style="text-align:center;"><?=$data['responsable']['uti_prenom'] ?> <?=	$data['responsable']['uti_nom'] ?> </p>
																			<?php }?>
																		<?php if(!empty($data['con_resp_valide'])){ ?>

																			<?php
																				if($data['con_resp_valide'] == 1){
																				?>
																				<p style="text-align:center"><span  style="color:green">Validé</span><br>
																				<strong><?= convert_date_fr($data['con_resp_valide_date']); ?></strong></p>
																				<?php }elseif($data['con_resp_valide'] == 2){ ?>
																					<p style="text-align:center"><span  style="color:red">Refusé</span><br>
																				<strong><?= convert_date_fr($data['con_resp_valide_date']); ?></strong></p>
																				<?php }elseif($data['con_resp_valide'] == 3){ ?>
																					<p style="text-align:center"><span  style="color:green">Validation automatique</span><br>
																				</p>
																				<?php }?>
																		<?php }?>
																		<?php if((($_SESSION['acces']['acc_profil'] == 7 OR $_SESSION['acces']['acc_profil'] == 12) OR ($_SESSION['acces']['acc_ref_id'] == $data['con_responsable'])) && empty($data['con_resp_valide'])){?>

																		<p style="text-align:center;">
																			<a href="conges_valide.php?conge=<?= $data['con_id'] ?>&resp=<?= $_SESSION['acces']['acc_ref_id'] ?>&ok" class="btn btn-sm btn-success">
																				<i class="fa fa-check"></i> Valider
																			</a> <a href="conges_valide.php?conge=<?= $data['con_id'] ?>&resp=<?= $_SESSION['acces']['acc_ref_id'] ?>&refus" class="btn btn-sm btn-danger">
																				<i class="fa fa-times"></i> Refuser
																			</a>
																		</p>
																	<?php }?>
																</div>
															</td>
															<?php }else{ ?>
																<td class="text-center w-100">
																	<div class="cadre"  style="min-height:115px;">
																		<h4 style="margin-top:25px;margin-bottom:10px;color:red">Demande annulée</h4>
																		<?php if(!empty($data['annul'])){ ?>
																		<p><strong><?= convert_date_fr($data['con_annul_date']); ?></strong> par <strong><?= $data['annul']['uti_prenom'] ?> <?= $data['annul']['uti_nom'] ?></strong></p>
																		<?php } ?>
																	</div>
																</td>
															<?php }?>
														</tr>
													</table>
												</footer>
											</div>
										</div>
									</div>
								</div>
								<?php if(!empty($data['con_integre'])){ ?>
								<div class="col-md-2">
									<div class="alert alert-danger">
										Cette demande de congés est intégrée en comptabilité et ne peut plus être modifiée.
									</div>
								</div>
								<?php } ?>
							</div>

		<?php		}else{ ?>

						<p class="alert alert-danger text-center" >
							Vous n'êtes pas autorisé à afficher ce congé !
						</p>

		<?php		} ?>

				</section>

				<!-- End: Content -->
			</section>


		</div>

		<!-- End: Main -->

		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >

					 		<a
							<?php if(!empty($_SESSION['retour'])){ ?>
							 href="<?= $_SESSION['retour'] ?>"
							<?php }else{ ?>
								href="conges_liste.php"
							<?php } ?>
							 class="btn btn-sm btn-default" >
								<i class="fa fa-left-arrow"></i> Retour
							</a>
				</div>

				<div class="col-xs-6 footer-middle text-center" >
					<?php if($data['con_annul'] == 0  && $erreur == 0){ ?>
						<?php if($data['con_resp_valide'] != 0 && $data['con_uti_valide'] != 0){ ?>
							<button type="button" class="btn btn-sm btn-info ml15" id="print" >
								<i class="fa fa-print"></i> Imprimer
							</button>
						<?php } ?>
					<?php } ?>
				</div>

				<div class="col-xs-3 footer-right" >
				<!---	<button type="button" class="btn btn-sm btn-success" id="btn_ligne_add" data-toggle="tooltip" title="Ajouter une nouvelle ligne" >
						<i class="fa fa-plus"></i> Produit
					</button>-->
					<?php
					if($data['con_integre'] == 1 && $_SESSION["acces"]["acc_profil"] != 7){
						$afficher_actions = 0;
					}else{
						$afficher_actions = 1;
					}
					if($data['con_annul'] == 1){
						$afficher_actions_annul = 0;
					}else{
						$afficher_actions_annul = 1;
					} ?>

					<?php if($afficher_actions_annul == 1 && $afficher_actions == 1 && $erreur == 0){ ?>
						<a href="conge.php?id=<?=$conge_id?>" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Modifier la demande" >
							<i class="fa fa-pencil"></i> Modifier
						</a>
					<?php }?>
					<?php if($afficher_actions_annul == 1 && $erreur == 0 && $afficher_actions == 1){ ?>
						<button id="annule" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Annuler la demande" >
							<i class="fa fa-times"></i> Annuler
						</a>
					<?php }?>
				</div>
			</div>
		</footer>


		<!-- modal confirmation -->
		<div id="modal_confirmation_user" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Confirmer ?</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							Annuler
						</button>
						<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
							Confirmer
						</button>
					</div>
				</div>
			</div>
		</div>


<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>

		<script type="text/javascript" src="assets/js/print.js"></script>

		<script type="text/javascript">

			var l_page=21;
			var h_page=29;



			jQuery(document).ready(function (){


				//********************
				//	GESTION DU PDF
				//*********************
				$("#pdf_add").click(function(){
					$.ajax({
						type:'POST',
						url: 'ajax/ajax_devis_pdf.php',
						data : 'devis=' + devis,
						dataType: 'JSON',
						success: function(data){
							$("#bloc_pdf").show();

						},
						error: function(data){
							afficher_message("Erreur","danger",data.responseText);
						}
					});
				});

				$("#annule").click(function(){
					modal_confirmation_user("Êtes-vous sûr de vouloir annuler cette demande ?",supprimer_ligne,"","","");

				});

			});

			function supprimer_ligne(){
				window.location.replace("conges_annul.php?conge=<?= $conge_id ?>");
			}


		</script>
	</body>
</html>
