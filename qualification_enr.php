<?php
include "includes/controle_acces.inc.php";
include "includes/connexion.php";

 if (isset($_POST['qua_id'])) {
	$req = $Conn->prepare("UPDATE qualifications SET qua_libelle=:qua_libelle, qua_duree=:qua_duree, qua_code=:qua_code, qua_opt_1=:qua_opt_1, qua_opt_2=:qua_opt_2, qua_opt_3=:qua_opt_3
	WHERE qua_id=:qua_id;");
	$req->bindParam("qua_libelle",$_POST['qua_libelle']);
	$req->bindParam("qua_duree",$_POST['qua_duree']);
	$req->bindParam("qua_code",$_POST['qua_code']);
	$req->bindParam("qua_opt_1",$_POST['qua_opt_1']);
	$req->bindParam("qua_opt_2",$_POST['qua_opt_2']);
	$req->bindParam("qua_opt_3",$_POST['qua_opt_3']);
	$req->bindParam("qua_id",$_POST['qua_id']);
	$req->execute();
}else{
	$req = $Conn->prepare("INSERT INTO qualifications (qua_libelle, qua_duree,qua_code,qua_opt_1,qua_opt_2,qua_opt_3) VALUES (:qua_libelle, :qua_duree,:qua_code,:qua_opt_1,:qua_opt_2,:qua_opt_3);");
	$req->bindParam("qua_libelle",$_POST['qua_libelle']);
	$req->bindParam("qua_duree",$_POST['qua_duree']);
	$req->bindParam("qua_code",$_POST['qua_code']);
	$req->bindParam("qua_opt_1",$_POST['qua_opt_1']);
	$req->bindParam("qua_opt_2",$_POST['qua_opt_2']);
	$req->bindParam("qua_opt_3",$_POST['qua_opt_3']);
	$req->execute();
}
header('Location: /qualification_liste.php');  
?>
