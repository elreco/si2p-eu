<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');
include('modeles/mod_get_juridique.php');
include('modeles/mod_orion_pro_categories.php');
include('modeles/mod_orion_pro_familles.php');

include('modeles/mod_parametre.php');
include('modeles/mod_get_adresses.php');
include('modeles/mod_relance_data.php');


// VISUALISATION D'UN DEVIS

$erreur=0;

$relance=0;
if(!empty($_GET["relance"])){
	$relance=intval($_GET["relance"]);
}

if(empty($relance)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();

}else{

	// la personne logue
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}

	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	// ON RECUPERE LES DONNES DU DEVIS A AFFICHER

	$data=relance_data($relance);

	if(empty($data)){
		$erreur_txt="impossible d'afficher la page";
		echo($erreur_txt);
		die();
    }

	foreach($data["rel_adresse_facturation"] as $ad){
		if(!empty($ad["adr_libelle"])){
			$ad_text=$ad["adr_libelle"];
		}else{
			$ad_text=$ad["adr_nom"] . " " . $ad["adr_cp"] . " " . $ad["adr_ville"];
		}
		$adresses[]=array(
			"id" => $ad["adr_id"],
			"text" => $ad_text,
			"nom" => $ad["adr_nom"],
			"service" => $ad["adr_service"],
			"ad1" => $ad["adr_ad1"],
			"ad2" => $ad["adr_ad2"],
			"ad3" => $ad["adr_ad3"],
			"cp" => $ad["adr_cp"],
			"ville" => $ad["adr_ville"],
			"defaut" => $ad["adr_defaut"],
			"geo" => $ad["adr_geo"],
		);
	}
    $sql="UPDATE Relances SET rel_courrier=:rel_envoie,rel_courrier_uti=:rel_courrier_uti,
        rel_courrier_date=NOW()
            WHERE (rel_courrier = 0 OR rel_courrier IS NULL) AND rel_id =" . $relance;
	$req = $Conn->prepare($sql);
    $req->bindValue(":rel_envoie",true);
    $req->bindValue(":rel_courrier_uti",$_SESSION['acces']['acc_ref_id']);
	$req->execute();

	$_SESSION["retourFacture"] = "facture_relance_lr_voir.php?relance=" . $relance;

} ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Relance</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#container_print{
				width:21cm;
			}
			.ligne-col-1{
                width:20%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-2{
                width:15%;
				padding:1px 5px;
				font-size:10px;
                vertical-align:middle;
			}
			.ligne-col-3{
                width:15%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-4{
                width:15%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-5{
                width:20%;
				padding:1px 5px;
				vertical-align:middle;
			}
            .ligne-col-6{
                width:15%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.cachet{
				padding:5px 5px 20px 5px;
				border:1px solid #333;
			}
			.cachet small{
				font-size:7pt;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
		</style>


	</head>
	<body class="sb-top sb-top-sm document-affiche">
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php 		if($erreur==0){ ?>

							<div class="row" >

								<!-- zone d'affichage du devis -->

								<div class="col-md-8"  >

									<div id="container_print" >

										<div id="page_print" >

											<!--<div id="controleH" ></div>-->

											<div class="page" id="header_0" data-page="0" >
												<header>
													<table>
														<tr>
															<td class="w-25" >
													<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
																	<img src="<?=$data["juridique"]["logo_1"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td class="w-50 text-center" ><h1>LETTRE RECOMMANDEE AVEC AR</h1></td>
															<td class="w-25" >
													<?php		if(!empty($data["juridique"]["logo_2"])){ ?>
																	<img src="<?=$data["juridique"]["logo_2"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
														</tr>
													</table>
												</header>
											</div>

											<div id="head_0" class="head" >
												<section>
													<table>
														<tr>
															<td class="w-50 va-t" >
													<?php		echo($data["juridique"]["nom"]);
																if(!empty($data["juridique"]["agence"])){
																	echo("<br/>Agence " . $data["juridique"]["agence"]);
																}
																if(!empty($data["juridique"]["ad1"])){
																	echo("<br/>" . $data["juridique"]["ad1"]);
																}
																if(!empty($data["juridique"]["ad2"])){
																	echo("<br/>" . $data["juridique"]["ad2"]);
																}
																if(!empty($data["juridique"]["ad3"])){
																	echo("<br/>" . $data["juridique"]["ad3"]);
																}
																echo("<br/>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"]);
																echo("<br/>" . $data["juridique"]["mail"]);
																echo("<br/>" . $data["juridique"]["site"]);

																if(!empty($data["rel_cli_code"])){  ?>
																	<div >Code : <strong><?=$data["rel_cli_code"]?></strong></div>
														<?php 	}
																if(!empty($data["rel_cli_reference"])){  ?>
																	<div>Référence interne : <?=$d_client["rel_cli_reference"]?></div>
														<?php 	} ?>
															</td>
															<td class="w-50 adr_fac" >
																<p>
																	<div id="cont_adr_fac" >
																	</div>
																</p>
															</td>
														</tr>
													</table>

													<table>
                                                            <tr>
															<td class="text-right" >
																Le <?=$data["rel_date_texte"]?>
															</td>
														</tr>
													</table>
													<p>Madame, Monsieur, <br> Nous constatons malheureusement que votre société n'a toujours pas réglé le solde des factures détaillées ci dessous, arrivées à échéance.</p>
												</section>
											</div>

											<div id="ligne_header_0" >
												<div class="table-div" >
													<div class="table-div-th ligne-col-1 text-center" >Agence</div>
													<div class="table-div-th ligne-col-2 text-center" >Numéro de facture</div>
													<div class="table-div-th ligne-col-3 text-center" >Date</div>
													<div class="table-div-th ligne-col-4 text-center" >Montant</div>
													<div class="table-div-th ligne-col-5 text-center" >Date échéance</div>
                                                    <div class="table-div-th ligne-col-6 text-center" >Reste dû</div>
												</div>
											</div>

                                            <?php
                                            $total_reste_du = 0;
                                            foreach($data['rel_factures'] as $soc=>$fr){
												foreach($fr as $f){


                                                if(!empty($f['fac_agence'])){
                                                    $req=$Conn->query("SELECT soc_nom FROM societes WHERE soc_id=" . $soc . ";");
                                                    $d_societe=$req->fetch();
                                                    $req=$Conn->query("SELECT age_code FROM agences WHERE age_id=" . $f['fac_agence'] . ";");
                                                    $d_agence=$req->fetch();
                                                    $agence =$d_societe['soc_nom'] . " ". $d_agence['age_code'];
                                                }else{
                                                    $req=$Conn->query("SELECT soc_code FROM societes WHERE soc_id=" . $soc . ";");
                                                    $d_societe=$req->fetch();
                                                    $agence = $d_societe['soc_code'];
                                                }
                                                $total_reste_du = $total_reste_du + ($f["fac_total_ttc"]-$f["fac_regle"]);

                                                ?>
                                                <div class="table-div ligne-body-0" id="ligne_<?=$f["fac_id"]?>" >
														<div class="ligne-col-1" >
															<?=$agence?>
														</div>
														<div class="ligne-col-2" >
															<div><?=$f["fac_numero"]?></div>
														</div>
														<div class="text-right ligne-col-3" ><?=convert_date_txt($f["fac_date"])?></div>
														<div class="text-right ligne-col-4 " ><?=number_format($f["fac_total_ttc"],2,","," ")?> €</div>
														<div class="text-right ligne-col-5" ><?=convert_date_txt($f["fac_date_reg_prev"])?></div>
                                                        <div class="text-right ligne-col-6" ><?=number_format($f["fac_total_ttc"]-$f["fac_regle"],2,","," ")?> €</div>
													</div>
											<?php
											}
										}?>

											<div id="foot_0" >
                                                <p>
                                                Aussi par la présente, nous vous mettons en demeure de nous verser la somme de <?=number_format($total_reste_du,2,","," ")?> €uros.<br>
                                                Cette somme sera majorée des intérêts au taux légal dus en vertu de l'article 1153 du code civil.<br>
                                                Nous vous informons que ces pénalités courent dès réception de cette lettre.<br>
                                                Si dans un délai de 7 jours à compter de la première présentation de cette lettre recommandée,<br>
                                                vous ne vous êtes toujours pas acquittés de votre obligation, nous saisirons la juridiction<br>
                                                compétente afin d'obtenir le paiement des sommes susvisées.<br>
                                                Nous vous prions de croire, Messieurs, en l'assurance de notre considération distinguée.
                                                </p>
												<table>
													<tr>
														<td class="w-33" >

														</td>
														<td class="w-33" >


														</td>
														<td class="w-33 va-t" >
                                                                <?= $data['rel_uti_prenom'] ?> <?= $data['rel_uti_nom'] ?><br>
                                                                Tél : <?= $data['rel_uti_tel'] ?><br>
                                                                Mail : <?= $data['rel_uti_mail'] ?>

														</td>
													</tr>
												</table>
											</div>
											<div id="footer_0" >
												<footer>
													<table>
														<tr>
															<td class="w-15" >
													<?php		if(!empty($data["juridique"]["qualite_1"])){ ?>
																	<img src="<?=$data["juridique"]["qualite_1"]?>" class="img-responsive" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td class="text-center w-70" ><?=$data["juridique"]["footer"]?></td>
															<td class="text-right w-15" >
													<?php		if(!empty($data["juridique"]["qualite_2"])){ ?>
																	<img src="<?=$data["juridique"]["qualite_2"]?>" />
													<?php		}else{
																	echo("&nbsp;");
																} ?>
															</td>
														</tr>
													</table>
												</footer>
											</div>

										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->
								<div class="col-md-4">

										<div class="panel"  >
											<div class="panel-body">
												<?php  foreach($data['rel_factures'] as $soc=>$fr){
												foreach($fr as $f){ ?>
												<div class="row mb10 mt10"  >
													<div class="col-md-6 pt5" >
														<?php if(file_exists("documents/Societes/". $soc . "/Factures/". $f["fac_numero"] . ".pdf")){ ?>
														<a href="documents/Societes/<?=$soc?>/Factures/<?=$f["fac_numero"]?>.pdf" target="_blank" >
															<i class="fa fa-file-pdf-o" ></i> <?=$f["fac_numero"]?>
														</a>
														<a href="documents/Societes/<?=$soc?>/Factures/<?=$f["fac_numero"]?>.pdf" download="<?=$f["fac_numero"]?>.pdf" class="download_files" style="display:none;"></a>
														<?php }else{?>
														<a href="facture_voir.php?facture=<?=$f["fac_id"]?>&societ=<?=$soc?>">
															<?=$f["fac_numero"]?> - <strong class="text-primary">Aller générer la facture</strong>
														</a>
														<?php }?>
													</div>
													<div class="col-md-6" >
														<a href="mail_prep.php?facture=<?=$f["fac_id"]?>" class="btn btn-sm btn-info" data-toggle="tooltip" title="Envoyer par mail" >
															<i class="fa fa-envelope-o" ></i>
														</a>
													</div>
												</div>
												<?php }
												} ?>
											</div>
										</div>

								</div>
								<div class="col-md-4">

										<div class="panel"  >
											<div class="panel-body">
												<select name="fac_adresse" id="adresse" class="select2" required >
										<?php		if(!empty($adresses)){
														foreach($adresses as $ad){
																echo("<option value='" . $ad["id"] . "' >" .  $ad["text"] . "</option>");

														}
													}?>
												</select>
											</div>
										</div>

								</div>

							</div>


		<?php		}else{ ?>

						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>

		<?php		} ?>

				</section>

				<!-- End: Content -->
			</section>


		</div>
		<!-- End: Main -->

		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
		<?php		if(isset($_SESSION["retourRelance"])){
						if(!empty($_SESSION["retourRelance"])){ ?>
							<a href="<?=$_SESSION["retourRelance"]?>" class="btn btn-sm btn-default" >
								<i class="fa fa-left-arrow"></i> Retour
							</a>
		<?php			}
					} ?>

				</div>
				<div class="col-xs-6 footer-middle text-center" >
						<button type="button" class="btn btn-sm btn-info ml15" id="print" >
							<i class="fa fa-print"></i> Imprimer
						</button>
						<button type="button" class="btn btn-sm btn-success download" >
							<i class="fa fa-download"></i> Télécharger toutes les factures
						</button>

				</div>
			</div>
		</footer>




		<!-- modal confirmation -->
		<div id="modal_confirmation_user" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Confirmer ?</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							Annuler
						</button>
						<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
							Confirmer
						</button>
					</div>
				</div>
			</div>
		</div>


<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>

		<script type="text/javascript" src="assets/js/print.js"></script>

		<script type="text/javascript">

			var l_page=21;
			var h_page=29;


			$(document).ready(function () {
				$(".download").click(function(e) {
					// download `a.txt`, `b.txt`
					$(".download_files").each(function() {
						this.click();
					})
				});


			});

			var tab_adresse=new Array();
	<?php	if(!empty($adresses)){
				foreach($adresses as $ad){ ?>
					tab_adresse[<?=$ad["id"]?>]=new Array(8);
					tab_adresse[<?=$ad["id"]?>]["nom"]="<?=str_replace('"', "'", $ad["nom"]);?>";
					tab_adresse[<?=$ad["id"]?>]["service"]="<?=str_replace('"', "'", $ad["service"])?>";
					tab_adresse[<?=$ad["id"]?>]["ad1"]="<?=preg_replace( "/\r|\n/", "", str_replace('"', "'", $ad["ad1"]));?>";
					tab_adresse[<?=$ad["id"]?>]["ad2"]="<?=str_replace('"', "'", $ad["ad2"]);?>";
					tab_adresse[<?=$ad["id"]?>]["ad3"]="<?=str_replace('"', "'", $ad["ad3"]);?>";
					tab_adresse[<?=$ad["id"]?>]["cp"]="<?=$ad["cp"]?>";
					tab_adresse[<?=$ad["id"]?>]["ville"]="<?=str_replace('"', "'", $ad["ville"]);?>";
					tab_adresse[<?=$ad["id"]?>]["geo"]="<?=$ad["geo"]?>";
			<?php	if($ad["defaut"]==1){ ?>
						defaut="<?=$ad["id"]?>";
	<?php			}
				}
			} ?>
			function afficher_adresse(adresse){
				ad_html="";
				if(adresse!="" && adresse!=0){
					ad_html="<b>" + tab_adresse[adresse]["nom"] + "</b></br>";
					if(tab_adresse[adresse]["service"]!=""){
						ad_html=ad_html + "<br/>" + tab_adresse[adresse]["service"];
					}
					if(tab_adresse[adresse]["ad1"]!=""){
						ad_html=ad_html + "<br/>" + tab_adresse[adresse]["ad1"];
					}
					if(tab_adresse[adresse]["ad2"]!=""){
						ad_html=ad_html + "<br/>" + tab_adresse[adresse]["ad2"];
					}
					if(tab_adresse[adresse]["ad3"]!=""){
						ad_html=ad_html + "<br/>" + tab_adresse[adresse]["ad3"];
					}
					ad_html=ad_html + "<br/>" + tab_adresse[adresse]["cp"] + " " + tab_adresse[adresse]["ville"];
				}
				$("#cont_adr_fac").html(ad_html);
				if(adresse!=0){
					$(".bloc-adr-fac").show();
				}else{
					$(".bloc-adr-fac").hide();
				}
			}

			// adresse de facturation de l'OPCA
			$("#adresse").change(function(){
				afficher_adresse($(this).val());
			});
			afficher_adresse($("#adresse").val());



		</script>
	</body>
</html>
