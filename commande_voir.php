<?php
////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');

// session retour
if(isset($_GET['menu_retour'])){
	unset($_SESSION['retour']);
}
///////////////////// Contrôles des parametres ////////////////////
$erreur_txt="";

$c_id = 0;
if(isset($_GET)){
	if(!empty($_GET['commande'])){
		$c_id = intval($_GET['commande']);
	}
}
if(empty($c_id)){
	echo("Impossible d'afficher la page!");
	die();
}

$action_id = 0;
if(isset($_GET)){
	if(!empty($_GET['action'])){
		$action_id = intval($_GET['action']);
	}
}

///////////////////// FIN Contrôles des parametres ////////////////////
////////////////// TRAITEMENTS SERVEUR ///////////////////

	// rechercher la commande
	$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = " . $c_id);
	$req->execute();
	$c = $req->fetch();
	if(empty($c)){
		echo("Impossible d'afficher la page!");
		die();
	}

	// PERSONNEE CONNECTE

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}


	// rechercher le fournisseur de la commande
	$req = $Conn->query("SELECT fou_nom,fou_code FROM fournisseurs WHERE fou_id = " . $c['com_fournisseur']);
	$req->execute();
	$fournisseur = $req->fetch();
	// rechercher l'adresse du fournisseur
	$req = $Conn->prepare("SELECT * FROM fournisseurs_adresses WHERE fad_fournisseur = " . $c['com_fournisseur']);
	$req->execute();
	$fad = $req->fetch();
	// rechercher le donneur d'ordres
	$req = $Conn->prepare("SELECT uti_id,uti_nom, uti_prenom, uti_societe, uti_agence,uti_responsable FROM utilisateurs WHERE uti_id = " . $c['com_donneur_ordre']);
	$req->execute();
	$ordre = $req->fetch();

	// rechercher les lignes du BC
	$req = $Conn->prepare("SELECT cli_ht,cli_id,cli_livraison,pro_code_produit,cli_libelle, ffa_libelle, cli_reference, cli_descriptif, cli_pu, cli_qte, cli_tva, veh_libelle, uti_nom, uti_prenom, cli_utilisateur, cli_vehicule, ffa_id FROM commandes_lignes
	LEFT JOIN produits ON produits.pro_id = commandes_lignes.cli_produit_si2p
	LEFT JOIN fournisseurs_familles ON fournisseurs_familles.ffa_id = commandes_lignes.cli_famille
	LEFT JOIN vehicules ON vehicules.veh_id = commandes_lignes.cli_vehicule
	LEFT JOIN utilisateurs ON utilisateurs.uti_id = commandes_lignes.cli_utilisateur
	WHERE cli_commande = " . $c['com_id'] . " ORDER BY cli_id");
	$req->execute();
	$lignes = $req->fetchAll();

	// rechercher les lignes du BC
	// rechercher les familles du fournisseur
	$req = $Conn->prepare("SELECT * FROM fournisseurs_familles_jointure WHERE ffj_fournisseur = :ffj_fournisseur");
	$req->bindParam(':ffj_fournisseur', $c['com_fournisseur']);
	$req->execute();
	$fournisseurs_familles_jointure = $req->fetchAll();

	foreach($fournisseurs_familles_jointure as $f){
		if(empty($fam)){
		$fam = $f['ffj_famille'];
		}else{
		$fam = $fam . "," . $f['ffj_famille'];
		}
	}
	if(!empty($fournisseurs_familles_jointure)){

		// rechercher les familles de $c['com_fournisseur']
		$req = $Conn->prepare("SELECT ffa_id, ffa_libelle, ffa_type, ffa_droit FROM fournisseurs_familles WHERE ffa_id IN (" . $fam . ")");
		$req->execute();
		$fournisseurs_familles = $req->fetchAll();
	}else{
		// rechercher les familles de $c['com_fournisseur']
		$req = $Conn->prepare("SELECT ffa_id, ffa_libelle, ffa_type, ffa_droit FROM fournisseurs_familles WHERE ffa_id = 0");
		$req->execute();
		$fournisseurs_familles = $req->fetchAll();
	}
	// rechercher les familles de produit
	/*$req = $Conn->prepare("SELECT pfa_id, pfa_libelle FROM produits_familles");
	$req->execute();
	$produits_familles = $req->fetchAll();*/

	// toutes les tva
	$req = $Conn->prepare("SELECT * FROM tva_periodes");
	$req->execute();
	$tva = $req->fetchAll();

	// tous les véhicules
	$req = $Conn->prepare("SELECT veh_id,veh_libelle FROM vehicules");
	$req->execute();
	$vehicules = $req->fetchAll();
	//donneur d'ordre gfc tt le monde, sinon societe agence...(pour les utilisateurs)
	if($ordre['uti_societe'] == 4){
		$req = $Conn->prepare("SELECT uti_id,uti_prenom, uti_nom FROM utilisateurs");
		$req->execute();
		$utilisateurs = $req->fetchAll();
	}else{
		$req = $Conn->prepare("SELECT uti_id,uti_prenom, uti_nom FROM utilisateurs WHERE uti_societe =" . $ordre['uti_societe'] . " AND uti_agence = " . $ordre['uti_agence']);
		$req->execute();
		$utilisateurs = $req->fetchAll();
	}

	// tous les validants du BC
	if($c['com_etat'] > 0){
		$req = $Conn->prepare("SELECT * FROM commandes_validations WHERE cva_commande = " . $c['com_id']);
		$req->execute();
		$cs_validations = $req->fetchAll();

	}

	$req = $Conn->prepare("SELECT * FROM commandes_histo WHERE chi_commande = " . $c['com_id']  . " ORDER BY chi_id DESC");
	$req->execute();
	$cs_histo = $req->fetch();

	// CALCULER TTC
	foreach($lignes as $k=>$l){ // pour toutes les lignes calculer la tva
		$date = $c['com_date'];
		if(empty($l['cli_tva'])){
			$lignes[$k]["cli_tva"] = 1;
			$l["cli_tva"] = 1;
			$req = $Conn->prepare("UPDATE commandes_lignes SET cli_tva = 1 WHERE cli_commande = " . $l['cli_id']);
			$req->execute();
		}
		$req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = " . $l['cli_tva'] . " AND tpe_date_deb <= '" . $date . "' AND (tpe_date_fin >= '"  . $date . "' OR tpe_date_fin IS NULL)");
		$req->execute();
		$tva = $req->fetch();

		$req = $Conn->prepare("UPDATE commandes_lignes SET cli_tva_taux = " . $tva['tpe_taux'] . " WHERE cli_commande = " . $l['cli_id']);
		$req->execute();

		$valeur = (($l['cli_ht'])*$tva['tpe_taux'])/100;
		$valeur = $l['cli_ht'] + $valeur;

		if(!empty($total)){
			$total = $total + $valeur;
		}else{
			$total = $valeur;
		}
	}


	if(!empty($lignes)){
		$req = $Conn->prepare("UPDATE commandes SET com_ttc = " . $total . " WHERE com_id = " . $c['com_id']);
		$req->execute();
	}

	if(!empty($action_id)){

		// Nouveau BC avec ouverture du modal ligne
		// la création du BC se fait forcément sur la société de l'action

		$req = $ConnSoc->query("SELECT act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,act_produit FROM Actions WHERE act_id=" . $action_id . ";");
		$d_action=$req->fetch();
		if(!empty($d_action)){

			$req = $Conn->query("SELECT pca_id,pca_libelle FROM Produits_Categories ORDER BY pca_libelle;");
			$d_produit_cat=$req->fetchAll();

			$req = $Conn->query("SELECT pfa_id,pfa_libelle FROM Produits_Familles ORDER BY pfa_libelle;");
			$d_produit_fam=$req->fetchAll();

			$req = $Conn->query("SELECT psf_id,psf_libelle FROM Produits_Sous_Familles WHERE psf_pfa_id=" . $d_action["act_pro_famille"]. ";");
			$d_produit_s_fam=$req->fetchAll();

			$req = $Conn->query("SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles WHERE pss_psf_id=" . $d_action["act_pro_sous_famille"]. ";");
			$d_produit_s_s_fam=$req->fetchAll();

			$req = $Conn->query("SELECT pro_id,pro_code_produit FROM Produits WHERE pro_id=" . $d_action["act_produit"]. ";");
			$d_produit=$req->fetch();
		}
	}
	
	if (!empty($c["com_action"]) AND $c["com_etat"]>0){

		if(!empty($c["com_societe"])){

			$ConnFct=connexion_fct($c["com_societe"]);
		
			if($_SESSION["menu"]["rubrique"]==5){
				$_SESSION["retour_action"]="commande_voir.php?commande=" . $c_id;
			}
			
			$data_marge=array(
				"ca" => 0,
				"charge" => 0,
				"marge" => 0,
				"marge_taux" =>0,
				"erreur" => ""
			);

			$sql="SELECT SUM(acl_ca) FROM Actions_Clients WHERE acl_confirme AND not acl_archive AND acl_action=:action;";
			$req=$ConnFct->prepare($sql);
			$req->bindParam(":action",$c["com_action"]);
			$req->execute();
			$d_action_clients=$req->fetch();
			if(!empty($d_action_clients)){
				if(!empty($d_action_clients[0])){
					$data_marge["ca"]=$d_action_clients[0];
				}
			}

			$demi_j_total=0;
			$demi_j_st=0;

			$sql="SELECT COUNT(pda_id) as nb_demi_j,int_type FROM Plannings_Dates,Intervenants WHERE pda_intervenant=int_id AND pda_type=1 AND pda_ref_1=:action AND NOT pda_archive
			GROUP BY int_type";
			$req=$ConnFct->prepare($sql);
			$req->bindParam(":action",$c['com_action']);
			$req->execute();
			$d_dates=$req->fetchAll();
			if (!empty($d_dates)) {
	
				foreach($d_dates as $d){
	
					$demi_j_total=$demi_j_total + $d["nb_demi_j"];
	
					if ($d["int_type"]==3 OR $d["int_type"]==2) {
	
						$demi_j_st=$demi_j_st + $d["nb_demi_j"];
					}
				}

				if($demi_j_total!=$demi_j_st){
					$data_marge["ca"]=($data_marge["ca"]/$demi_j_total)*$demi_j_st;
				}


			}else{
				$data_marge["erreur"]="Aucune date n'est liée à un sous-traitant.";
			}

			// on proratise le CA
			if(!empty($demi_j_total))

	
			

			// charges (l'action peut être liée à N BC)

			$sql="SELECT SUM(com_ht) FROM Commandes WHERE NOT com_annule AND com_societe=:societe AND com_action=:action;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":action",$c["com_action"]);
			$req->bindParam(":societe",$acc_societe);
			$req->execute();
			$d_action_commandes=$req->fetch();
			$data_marge["charge"]=$d_action_commandes[0];


			$data_marge["marge"]=$data_marge["ca"]-$data_marge["charge"];

			if (!empty($data_marge["charge"])) {

				$data_marge["marge_taux"]=round(($data_marge["marge"]/$data_marge["charge"])*100,2);

			}else{

				$data_marge["marge_taux"]="100";

			}

		}else{
			$data_marge=array(
				"erreur" => "L'action n'a pas pu être identifiée. Calcul de la marge impossible!"
			);
		}

	}
?>
<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>SI2P - Orion - <?= $c['com_numero'] ?></title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<style type="text/css">
	.panel-tabs > li > a {
		color: #AAA;
		font-size: 14px;
		letter-spacing: 0.2px;
		line-height: 30px;
		/*padding: 9px 20px 11px;*/
		border-radius: 0;
		border-left: 1px solid transparent;
		border-right: 1px solid transparent;
	}
	.nav2 > li > a {
		position: relative;
		display: block;
		/* padding: 10px 15px; */
	}
</style>
<body class="sb-top sb-top-sm">


	<form method="post" action="commande_enr.php" enctype="multipart/form-data">
		<div id="main">
<?php		 include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<h1 class="text-center" ><?= $c['com_numero']?></h1>
				<section id="content" class="animated fadeIn pr20 pl20">

					<div id="com_annule_panel" class="panel bg-danger light of-h mb10" <?php if($c['com_annule'] == 0) echo("style='display:none;'"); ?> >
						<div class="pn pl20 p5" style="min-height: 100px;">
							<div class="icon-bg">
								<i class="fa fa-times" aria-hidden="true"></i>
							</div>
							<h2 class="mt15 lh15">
								<b>Demande d'achat annulée</b>
							</h2>
							<div id="com_annule_title">
					<?php 		if(!empty($c['com_annule_txt'])){ ?>
									<h5 class="text-muted">Cette demande d'achat a été annulée pour la raison suivante : <?= $c['com_annule_txt'] ?></h5>
					<?php 		} ?>
							</div>
						</div>
					</div>

			<?php 	if(!empty($cs_histo)){ ?>
						<div class="panel bg-info light of-h mb10 com_histo_panel">
							<div class="pn pl20 p5" style="min-height: 100px;">
								<div class="icon-bg">
									<i class="fa fa-info-circle" aria-hidden="true"></i>
								</div>
								<h2 class="mt15 lh15">
									<b>Cette demande d'achat a été réouverte pour
									<?php
									switch ($cs_histo['chi_type']) {
										case 1:
											echo("changement de prix");
											break;

										case 2:
											echo("changement de produit");
											break;
									}
									?>
									</b>
								</h2>

									<?php if(!empty($cs_histo['chi_txt'])){ ?>
										<h5 class="text-muted"><?= $cs_histo['chi_txt'] ?></h5>
									<?php } ?>

							</div>
						</div>
			<?php 	} ?>

		                <div class="row">
		                	<div class="col-md-4">
		                		<div class="panel mb10">
		                            <div class="panel-heading panel-head-sm">
		                                <span class="panel-icon">
		                                    <i class="fa fa-truck" aria-hidden="true"></i>
		                                </span>
		                                <span class="panel-title"> Fournisseur</span>
		                            </div>
		                            <div class="panel-body p10">
		                                <h4  class="text-left"><?= $fournisseur['fou_nom'] ?> (<?= $fournisseur['fou_code'] ?>)</h4>
				                        <p class="text-muted">
				                        	<?= $fad['fad_ad1'] ?>
				                        	<?php if(!empty($fad['fad_ad2'])){ ?>
				                            	<br> <?= $fad['fad_ad2'] ?>
				                            <?php } ?>
				                            <?php if(!empty($fad['fad_ad3'])){ ?>
				                            	<br> <?= $fad['fad_ad3'] ?>
				                            <?php } ?>
				                            <br><?= $fad['fad_cp'] ?> <?= $fad['fad_ville'] ?>
				                        </p>
				                        <hr class="short br-lighter">
				                        <h4  class="text-left">Contact</h4>

				                        <p class="text-muted">
				                        <strong>
											<?php if(!empty($base_civilite[$c['com_contact_titre']])){ ?>
												<?= $base_civilite[$c['com_contact_titre']] ?>
											<?php }?>
											<?= $c['com_contact_prenom'] ?> <?= $c['com_contact_nom'] ?></strong><br>
								<?php	if(!empty($c['com_contact_fonction_nom']) OR !empty($c['com_contact_fonction'])){ ?>
				                        	Fonction :
								<?php 		if(!empty($c['com_contact_fonction_nom'])){
				                        		echo($c['com_contact_fonction_nom']);
				                        	}else{
				                        		$fonction_nom = get_fonction($c['com_contact_fonction']);
				                        		echo($fonction_nom['cfo_libelle']);
				                        	}

				                        	?>
				                        <br>
				                        <?php } ?>
				                        Tel : <?= $c['com_contact_tel'] ?> <br>
				                        Fax : <?= $c['com_contact_fax'] ?> <br>
				                        Mail : <?= $c['com_contact_mail'] ?> <br>
				                        Portable : <?= $c['com_contact_portable'] ?> <br>


				                        </p>
		                            </div>

		                        </div>
		                	</div>
		                	<div class="col-md-4">
		                		<div class="panel mb10">
		                            <div class="panel-heading panel-head-sm">
		                                <span class="panel-icon">
		                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
		                                </span>
		                                <span class="panel-title"> Adresse de facturation</span>
		                            </div>
		                            <div class="panel-body p10">
		                                <h4 class="text-left"><?= $c['com_fac_nom'] ?></h4>
				                        <p class="text-muted">
				                        	<?= $c['com_fac_adr1'] ?>
				                        	<?php if(!empty($c['com_fac_adr2'])){ ?>
				                            	<br> <?= $c['com_fac_adr2'] ?>
				                            <?php } ?>
				                            <?php if(!empty($c['com_fac_adr3'])){ ?>
				                            	<br> <?= $c['com_fac_adr3'] ?>
				                            <?php } ?>
				                            <br><?= $c['com_fac_cp'] ?> <?= $c['com_fac_ville'] ?>
				                        </p>
				                        <hr class="short br-lighter">
				                        <h6>Affaire suivie par</h6>

				                        <h4  class="text-left"><?= $ordre['uti_prenom'] ?> <?= $ordre['uti_nom'] ?></h4>

		                            </div>

		                        </div>
		                	</div>
		                	<div class="col-md-4">
		                		<div class="panel mb10">
		                            <div class="panel-heading panel-head-sm">
		                                <span class="panel-icon">
		                                    <i class="fa fa-file-text" aria-hidden="true"></i>
		                                </span>
		                                <span class="panel-title"> Adresse de livraison</span>
		                            </div>
		                            <div class="panel-body p10">
				                        <h4 class="text-left"><?= $c['com_liv_nom'] ?></h4>
				                        <p class="text-muted">
				                        	<?= $c['com_liv_adr1'] ?>
				                        	<?php if(!empty($c['com_liv_adr2'])){ ?>
				                            	<br> <?= $c['com_liv_adr2'] ?>
				                            <?php } ?>
				                            <?php if(!empty($c['com_liv_adr3'])){ ?>
				                            	<br> <?= $c['com_liv_adr3'] ?>
				                            <?php } ?>
				                            <br><?= $c['com_liv_cp'] ?> <?= $c['com_liv_ville'] ?>
				                        </p>
		                            </div>

		                        </div>
		                	</div>
		                </div>

						<div class="row">
		                	<div class="col-md-12">
		                		<span class="tm-tag tm-tag-inverse mr5 mb10 ib" style="border-radius:0;background:#4C4A49;">
                                    <span>Numéro : <?= $c['com_numero'] ?></span>
                                </span>

                                <span class="tm-tag tm-tag-inverse mr5 mb10 ib pull-right" style="border-radius:0;background:#4C4A49;">
                                    <span><?= convert_date_fr($c['com_date']) ?></span>
                                </span>
		                	</div>
		                </div>

						<div class="row">
		                	<div class="col-md-12">
		                		<table class="table table-striped table-hover" id="table_id" style="border:1px solid #e2e2e2;">
			                        <thead>
			                            <tr class="dark">
			                                <th>Famille</th>
			                                <th>Produit</th>
			                                <th>Référence</th>
			                                <th style="width:20%">Descriptif</th>
			                                <th>Prix unitaire (€)</th>
			                                <th>Quantité</th>
			                                <th>Prix HT (€)</th>
			                                <th>TVA (%)</th>
									<?php 	if($c['com_etat'] > 1){ ?>
			                                	<th style="width:10%;">Date de livraison <input type="text" class="form-control datepicker date com_date_livraison_all" placeholder="Toutes les dates"></th>
			                        <?php 	}
											if(($acc_utilisateur == $ordre['uti_id'] OR $acc_utilisateur == $ordre['uti_responsable']) && $c['com_etat'] == 0  && $c['com_annule'] == 0){ ?>
			                                	<th>Actions</th>
									<?php 	} ?>
			                            </tr>
			                        </thead>
			                        <tbody>
						<?php 			$bap = true;
										foreach($lignes as $l){
											// TVA

												$date = $c['com_date'];
												$req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = " . $l['cli_tva'] . " AND tpe_date_deb <= '" . $date . "' AND (tpe_date_fin >= '"  . $date . "' OR tpe_date_fin IS NULL)");
												$req->execute();
												$tva = $req->fetch();
												$req = $Conn->prepare("UPDATE commandes_lignes SET cli_tva_taux = " . $tva['tpe_taux'] . " WHERE cli_commande = " . $l['cli_id']);
												$req->execute();


												$tva_nom = $base_tva[$l['cli_tva']];
												$total_ligne[$tva_nom]['nom'] = $tva['tpe_taux'];

												$valeur = (($l['cli_ht'])*$tva['tpe_taux'])/100;

												if(!empty($total_ligne[$tva_nom]['montant'])){
													$total_ligne[$tva_nom]['montant'] = floatval(str_replace(',', '.', $total_ligne[$tva_nom]['montant'])) + $valeur;
												}else{
													$total_ligne[$tva_nom]['montant'] = $valeur;
												}
												$total_ligne[$tva_nom]['montant'] = number_format($total_ligne[$tva_nom]['montant'], 2, ',', ' ');


											if($l['ffa_id'] == 1){ // mettre a jour $bap pour bon à payer
												if(!empty($l['cli_livraison'])){
													$bap = true;
												}else{
													$bap = false;
												}
											} ?>
											<tr class="ligne_<?= $l['cli_id'] ?>">
												<td class="td_famille"><?= $l['ffa_libelle'] ?><br>

													<?php if(!empty($l['cli_utilisateur'])){ ?>
														<strong>Utilisateur : </strong><?= $l['uti_prenom'] ?> <?= $l['uti_nom'] ?>
													<?php }elseif(!empty($l['veh_libelle'])){?>
														<strong>Véhicule : </strong><?= $l['veh_libelle'] ?>
													<?php } ?>
												</td>
												<td><?= $l['pro_code_produit'] ?></td>
												<td><?= $l['cli_reference'] ?></td>
												<td><strong><?= $l['cli_libelle'] ?></strong><br><?= nl2br($l['cli_descriptif']) ?></td>
												<td class="td_prix_ht">
												<?php ?>
													<?= number_format($l['cli_pu'], 2, ',', ' ')  ?></td>
												<td class="td_prix_qte"><?= $l['cli_qte'] ?></td>
												<td>
												<?= number_format($l['cli_ht'], 2, ',', ' ') ?></td>

												<td><?php foreach($base_tva as $b => $v){

														if($b > 0){

															if($b == $l['cli_tva']){
																$req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = " . $l['cli_tva'] . " AND tpe_date_deb <= '" . $c['com_date'] . "' AND (tpe_date_fin >= '"  . $c['com_date'] . "' OR tpe_date_fin IS NULL)");
																$req->execute();
																$tva = $req->fetch();
																echo($tva['tpe_taux']);
															}

														}
													}  ?>
												</td>

												<?php if($c['com_etat'] > 1){ ?>
													<td>
														<input type="text" class="form-control datepicker date com_date_livraison" data-id="<?= $l['cli_id'] ?>" name="com_date_livraison" placeholder="Date de livraison" value="<?= convert_date_txt($l['cli_livraison']) ?>">
													</td>
												<?php } ?>
												<?php  if(($acc_utilisateur == $ordre['uti_id'] OR $acc_utilisateur == $ordre['uti_responsable']) && $c['com_etat'] == 0  && $c['com_annule'] == 0){ ?>
													<td class="btn-actions">

														<button type="button" class="btn btn-sm btn-warning editer_ligne" data-toggle="modal" data-target="#modal_ligne" data-id="<?= $l['cli_id'] ?>"><i class="fa fa-pencil"></i></button>
														<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal_suppr" data-id="<?= $l['cli_id'] ?>" id="delete_ligne"><i class="fa fa-times"></i></button>



													</td>
												<?php } ?>
											</tr>
			                    <?php 	} ?>
									</tbody>
			                    </table>
		                	</div>
		                </div>

		                <div class="row mt10">
		                	<div class="col-md-9">
				<?php 		if (isset($data_marge)){
								if(!empty($data_marge["erreur"])) { ?>	
									<p class="alert alert-danger" ><?=$data_marge["erreur"]?></p>
				<?php			}else {  ?>
									<div class="panel mb10">
										<div class="panel-heading panel-head-sm">
											<span class="panel-icon">
												€
											</span>
											<span class="panel-title"> Marge</span>
										</div>
										<div class="panel-body p10">
											<div class="row" >
												<div class="col-md-2">
													Action : <a href="action_voir.php?action=<?=$c["com_action"]?>" >N° <?=$c["com_action"]?></a>
												</div>
												<div class="col-md-2">
													CA confirmé : <?=$data_marge["ca"]?> €
												</div>
												<div class="col-md-2">
													Charge : <?=$data_marge["charge"]?> €
												</div>
												<div class="col-md-2">
													Marge : <?=$data_marge["marge"]?> €
												</div>
												<div class="col-md-2">
													Taux de marge : <?=$data_marge["marge_taux"]?> %
												</div>
											</div>

										</div>
									</div>
				<?php 			}
							}

							if($c['com_etat'] != 0){

									// si la commande n'est pas en cours de saisie ?>
			                		<div class="panel mb10">
			                            <div class="panel-heading panel-head-sm">
			                                <span class="panel-icon">
			                                    <i class="fa fa-check"></i>
			                                </span>
			                                <span class="panel-title"> Validations</span>
			                            </div>
			                            <div class="panel-body p10">
									<?php	foreach($cs_validations as $cv){ ?>
												<div class="col-md-3">
													<div class="panel panel-tile text-center br-a br-light">
												<?php 	if(empty($cv['cva_date'])){ ?>
															<div class="panel-body bg-danger">
																<i class="fa fa-spinner text-muted fs20 br64 bg-danger dark p15 ph20"></i>
														<?php 	if(!empty($cv['cva_profil'])){
																	if($cv['cva_profil'] == $_SESSION['acces']['acc_profil']){ ?>
																		<div class="text-center mt15">
																			<a href="commande_valide_date.php?commande=<?= $_GET['commande'] ?>&profil=<?= $_SESSION['acces']['acc_profil'] ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Valider la DA"><i class="fa fa-check"></i></a>
																		</div>
														<?php 		}
																}elseif(!empty($cv['cva_utilisateur'])){ 
																	if($cv['cva_utilisateur'] == $_SESSION['acces']['acc_ref_id']){ ?>
																		<div class="text-center mt15">
																			<a href="commande_valide_date.php?commande=<?= $_GET['commande'] ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Valider la DA"><i class="fa fa-check"></i></a>
																		</div>
														<?php 		}
																}elseif(!empty($cv['cva_droit'])){ 
																	if($_SESSION["acces"]["acc_droits"][$cv['cva_droit']]){ ?>
																		<div class="text-center mt15">
																			<a href="commande_valide_date.php?commande=<?= $_GET['commande'] ?>&droit=<?=$cv['cva_droit']?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Valider la DA"><i class="fa fa-check"></i></a>
																		</div>
														<?php 		}
																}  ?>
																<h1 class="fs20 mbn"><?= $cv['cva_nom_prenom'] ?></h1>
																<h6 class="text-white">EN COURS DE VALIDATION</h6>
															</div>
												<?php 	}else{ ?>
															<div class="panel-body bg-success">
																<i class="fa fa-check fa-check text-muted fs20 br64 bg-success dark p15 ph20 mt5"></i>
																<h1 class="fs20 mbn"><?= $cv['cva_nom_prenom'] ?></h1>
																<h6 class="text-white">VALIDÉ LE : <?= convert_date_txt($cv['cva_date']); ?></h6>
															</div>
												<?php 	} ?>
													</div>
												</div>
									<?php 	} ?>
										</div>
									</div>
			            <?php 	} ?>

		                        <div class="panel mb10">
		                            <div class="panel-heading panel-head-sm">
		                                <span class="panel-icon">
		                                    <i class="fa fa-check"></i>
		                                </span>
		                                <span class="panel-title"> Informations complémentaires</span>
		                            </div>
		                            <div class="panel-body p10">
		                            	<div class="row">
											<div class="col-md-3">
												<strong>Mode de règlement :</strong>
												<?php if(empty($c['com_reg_type'])){ ?>
													<i>Pas de mode de règlement</i>
												<?php }else{ ?>
													<?php foreach($base_reglement as $k => $n){ ?>
														<?php if($k > 0){ ?>
															<?php if($c['com_reg_type'] == $k){ ?><?= $n ?><?php } ?>
														<?php } ?>
													<?php } ?>
												<?php } ?>
											</div>
											<div class="col-md-3">
												<strong>Type de règlement :</strong>
												<!-- TYPE DE REGLEMENT -->
												<?php if($c['com_reg_formule'] == 1){ ?>

													Date + <?php if(!empty($c['com_reg_formule'])){ ?><?= $c['com_reg_nb_jour'] ?><?php }else{ ?>0<?php } ?> jours

												<?php } ?>
												<?php if($c['com_reg_formule'] == 2){ ?>
													Date + 45j + fin de mois
												<?php } ?>
												<?php if($c['com_reg_formule'] == 3){ ?>
													Date + fin de mois + 45j
												<?php } ?>
												<?php if($c['com_reg_formule'] == 4){ ?>
													Date + <?php if(!empty($c['com_reg_nb_jour'])){ ?><?= $c['com_reg_nb_jour'] ?><?php }else{ ?>0<?php } ?> jours + fin de mois + <?php if(!empty($c['com_reg_fdm'])){ ?><?= $c['com_reg_fdm'] ?><?php }else{ ?>0<?php } ?> jour(s)
												<?php } ?>
											</div>
											<div class="col-md-5">
												<strong>Statut de la commande :</strong>
												  <?php
												  if($c['com_annule'] == 0){
						                        	 	switch ($c['com_etat']) {
							                                case 0:
							                                    echo "<i class='fa fa-pencil'></i>DA en cours de saisie";
							                                    break;
							                                case 1:
							                                    echo "<i class='fa fa-spinner fa-spin fa-fw'></i>DA en cours de validation";
							                                    break;
							                                case 2:
							                                    echo "<i class='fa fa-check'></i>BC Validé";
							                                    break;
							                                case 3:
							                                    echo "<i class='fa fa-check'></i> Bon à payer";
							                                    break;
							                            }
					                            }else{
					                            	echo "<i class='fa fa-times'></i> Annulé";
					                            }
				                        	?>
											</div>

										</div>




		                            </div>

		                        </div>
		                	</div>

		                	<div class="col-md-3 text-center mt10">

		                		<div class="panel-body pn">
			                		<table class="table mbn tc-icon-2 tc-med-2 tc-bold-last">
	                                    <thead>
	                                        <tr class="hidden">
	                                            <th>Total HT</th>

	                                            <th>TVA</th>
	                                            <th>Total TTC</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody id="tva_table">

	                                        <tr>
	                                            <td>
	                                                <strong>Total HT</strong>
	                                            </td>
	                                            <td class="total_ht_txt"><?= number_format($c['com_ht'], 2, ',', ' ') ?> €</td>

	                                        </tr>
											<?php
											if(!empty($total_ligne)){
												foreach($total_ligne as $t){
											?>
												<tr class='tva_table'>
													<td><strong>TVA (<?= $t['nom'] ?> %)</strong></td><td><?= $t['montant'] ?> €</td>
												</tr>
											<?php }
											}
											  ?>

		                                        <tr>
		                                            <td>
		                                                <strong>Total TTC</strong>
		                                            </td>
		                                            <td class="table_ttc">

														<?= number_format($c['com_ttc'], 2, ',', ' ') ?> €
		                                            </td>


		                                        </tr>

	                                    </tbody>
	                                </table>
                                </div>
                                <?php if($c['com_etat'] == 2){ ?>
	                                <div class="panel mt10">
			                            <div class="panel-heading panel-head-sm">
			                                <span class="panel-icon">
			                                    <i class="fa fa-file-pdf-o"></i>
			                                </span>
			                                <span class="panel-title"> PDF et Impression</span>
			                            </div>
			                            <div class="panel-body p10">

			                                <div class="col-md-4">
												<h6 class="text-system" style="font-size:50px;"><a href="commande_pdf.php?commande=<?= $c['com_id'] ?>">
												<i class="fa fa-download" aria-hidden="true"  data-toggle="tooltip" data-placement="bottom" title="Télécharger le pdf"></i>
												</a>
												</h6>
											</div>
											<div class="col-md-4">
												<h6 class="text-system" style="font-size:50px;"><a href="commande_imprim.php?commande=<?= $c['com_id'] ?>">
												<i class="fa fa-print" aria-hidden="true"  data-toggle="tooltip" data-placement="bottom" title="Imprimer"></i>
												</a>
												</h6>
											</div>
											<div class="col-md-4">
												<h6 class="text-system" style="font-size:50px;"><a href="mail_prep.php?commande=<?= $c['com_id'] ?>">
												<i class="fa fa-envelope" aria-hidden="true"  data-toggle="tooltip" data-placement="bottom" title="Envoyer par mail"></i>
												</a>
												</h6>
											</div>



			                            </div>

			                        </div>
		                        <?php } ?>
		                	</div>
		                </div>
					</div>

				</section>
			</section>

		<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
			<div class="row">
				<div class="col-xs-4 footer-left pt7" >
		<?php 		if(isset($_SESSION['retourCommande'])){ ?>
						<a href="<?= $_SESSION['retourCommande']; ?>" class="btn btn-default btn-sm">
				          	<i class="fa fa-long-arrow-left"></i>
				          	Retour
				        </a>
		<?php		}elseif(!empty($_SESSION['retour'])){ ?>
				        <a href="<?= $_SESSION['retour']; ?>" class="btn btn-default btn-sm">
				          	<i class="fa fa-long-arrow-left"></i>
				          	Retour
				        </a>
		<?php 		} ?>
				</div>
				<div class="col-xs-4 footer-middle pt7"></div>
				<div class="col-xs-4 footer-right pt7">
		<?php  		// si c'est le donneur d'ordre ou le responsable
					if( ($acc_utilisateur==$ordre['uti_id'] OR $acc_utilisateur==$ordre['uti_responsable']) AND $c['com_annule'] == 0){
						if($c['com_etat'] == 0 ){?>
							<button type="button" class="btn btn-success btn-sm editer_ligne btn-actions" data-toggle="modal" data-target="#modal_ligne" data-id="0">
								  <i class="fa fa-plus"></i>
									Ligne
							</button>

							<a href="commande.php?id=<?= $c['com_id'] ?>" class="btn btn-warning btn-sm btn-actions">
								  <i class="fa fa-pencil"></i>
								  Editer
							</a>
							<a href="commande_valide.php?commande=<?= $_GET['commande'] ?>" class="btn btn-success btn-sm btn-actions">
								  <i class="fa fa-check"></i>
								  Clôturer la DA
							</a>
							<a href="#" data-toggle="modal" data-target="#modal_annule" class="btn btn-danger btn-sm btn-actions">
								  <i class="fa fa-times"></i>
								  Annuler la DA
							</a>
		<?php	 		}elseif($c['com_etat']!=3){
							// LE BC N'EST PAS BAP ?>
							 <a href="commande_reouvrir.php?commande=<?= $_GET['commande'] ?>" class="btn btn-success btn-sm btn-actions" >
								<i class="fa fa-folder-open" aria-hidden="true"></i>
								Réouvrir la DA
							</a>
		<?php			}elseif($c['com_etat'] == 2){ ?>
							<a href="commande_bap.php?commande=<?= $_GET['commande'] ?>" class="btn btn-success btn-sm" id="bap">
								<i class="fa fa-arrow-up" aria-hidden="true"></i>
								 Bon à payer
							</a>
		<?php			}
					}?>
				</div>
			</div>
		</footer>
	</form>

<!-- Modal pour l'ajout/edition d'une ligne -->
<?php
$masquer_pro="style='display:none;'";
if(!empty($action_id)){
	$masquer_pro="";
} 
?>

<div id="modal_ligne" class="modal fade" role="dialog">
  	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title title_ligne">Ajouter une ligne</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<input type="hidden" name="cli_id" id="cli_id" value="0">
						<label for="cli_famille" class="text-left">Type de produit :</label>
						<select name="cli_famille" id="fpr_type" class="select2" >
					<?php 	foreach($fournisseurs_familles as $f){
								if($f['ffa_droit'] == 0 OR (!empty($_SESSION['acces']['acc_droits'][$f['ffa_droit']]))){ ?>
									<option value="<?= $f['ffa_id'] ?>" data-revente="<?= $f['ffa_type']; ?>"><?= $f['ffa_libelle'] ?></option>
					<?php 		}
							}; ?>
						</select>
					</div>


	        	<div class="col-md-12 mb5 fpr_categorie mb5" <?=$masquer_pro?> >
	        		<label for="fpr_categorie" class="text-left">Catégorie de produit :</label>

					<select name="cli_categorie_si2p" id="fpr_categorie" class="select2 select2-produit-cat" >
						<option value="0">Catégorie de produit...</option>
				<?php	if(isset($d_produit_cat)){
							foreach($d_produit_cat as $p_cat){
								if($p_cat["pca_id"]==1){
									echo("<option value='" . $p_cat["pca_id"] . "' selected >" . $p_cat["pca_libelle"] . "</option>");
								}else{
									echo("<option value='" . $p_cat["pca_id"] . "' >" . $p_cat["pca_libelle"] . "</option>");
								}
							}
						} ?>
					</select>
				</div>
				<div class="col-md-12 pro_famille mb5" <?=$masquer_pro?> >
	            	<label for="pro_famille" class="text-left">Famille :</label>
	                <div class="section">
	                    <select id="cli_famille_si2p" name="pro_famille" class="select2 select2-famille select2-produit-fam">
							<option value="0">Famille de produit...</option>
				<?php 		if(isset($d_produit_fam)){
								foreach($d_produit_fam as $p_fam){ 
									if($p_fam['pfa_id']==$d_action["act_pro_famille"]){
										echo("<option value='" . $p_fam['pfa_id'] . "' selected >" . $p_fam['pfa_libelle'] . "</option>");
									}else{
										echo("<option value='" . $p_fam['pfa_id'] . "' >" . $p_fam['pfa_libelle'] . "</option>");
									} 
								}
							} ?>
	                    </select>

	                </div>
	            </div>
	            <div class="col-md-6 pro_sous_famille mb5" <?=$masquer_pro?> >
	            	<label for="pro_sous_famille" class="text-left">Sous-famille :</label>
	                <div class="section">
	                    <select id="pro_sous_famille" name="cli_sous_famille_si2p" class="select2 select2-famille-sous-famille select2-famille-sous-fam select2-produit-sous-fam">
	                        <option value="0">Sous-famille...</option>
							<?php if(isset($d_produit_s_fam)){
								foreach($d_produit_s_fam as $p_s_fam){ 
									if($p_s_fam['psf_id']==$d_action["act_pro_sous_famille"]){
										echo("<option value='" . $p_s_fam['psf_id'] . "' selected >" . $p_s_fam['psf_libelle'] . "</option>");
									}else{
										echo("<option value='" . $p_s_fam['psf_id'] . "' >" . $p_s_fam['psf_libelle'] . "</option>");
									}
								}
							} ?>
	                    </select>
	                </div>
	            </div>
				<div class="col-md-6 pro_sous_famille mb5" <?=$masquer_pro?> >
	            	<label for="pro_sous_sous_famille" class="text-left">Sous-Sous-famille :</label>
	                <div class="section">

	                    <select id="pro_sous_sous_famille" name="cli_sous_sous_famille_si2p" class="select2 select2-famille-sous-sous-famille select2-famille-sous-sous-fam select2-produit-sous-sous-fam">
	                        <option value="0">Sous-Sous-famille...</option>
							<?php if(isset($d_produit_s_s_fam)){
								foreach($d_produit_s_s_fam as $p_s_s_fam){ 
									if($p_s_s_fam['pss_id']==$d_action["act_pro_sous_sous_famille"]){
										echo("<option value='" . $p_s_s_fam['pss_id'] . "' selected >" . $p_s_s_fam['pss_libelle'] . "</option>");
									}else{
										echo("<option value='" . $p_s_s_fam['pss_id'] . "' >" . $p_s_s_fam['pss_libelle'] . "</option>");
									}
								}
							} ?>
	                    </select>

	                </div>
	            </div>
	            <div class="col-md-12 mb5 produit_si2p" <?=$masquer_pro?> >
	            	<label for="fpr_pro_id" class="text-left">Produit SI2P :</label>
	              	<div class="section">
	                    <select id="fpr_pro_id" name="cli_produit_si2p" class="select2 select2-produit">
	                        <option value="0">Produit SI2P...</option>
							<?php if(isset($d_produit)){
								if(!empty($d_produit)){
									echo("<option value='" . $d_produit['pro_id'] . "' selected >" . $d_produit['pro_code_produit'] . "</option>");
								}
							} ?>
	                    </select>
	              	</div>
	            </div>
	            <div class="col-md-12 mb5 cli_produit_fournisseur" <?=$masquer_pro?> >
	            	<label for="cli_produit_fournisseur" class="text-left">Produit du fournisseur :</label>
	              	<div class="section">
	                    <select id="cli_produit_fournisseur" name="cli_produit_fournisseur" class="select2">
	                        <option value="0">Autre produit...</option>
	                    </select>
	              	</div>
	            </div>
				<div class="col-md-12 base-fournisseur">
					<div class="option-group field p15">
							<input type="checkbox" class="com-etat" id="base-fournisseur" name="ajout_fournisseur" value="on" >
							Ajouter dans la base du fournisseur
					</div>
				</div>
	        	<div class="col-md-12 mb5">
	                <input type="text" name="cli_reference" id="fpr_reference" class="form-control nom" placeholder="Référence">
	        	</div>
				<div class="col-md-12 mb5">
	                <input type="text" name="cli_libelle" id="cli_libelle" class="form-control mb5" placeholder="Libellé">
	        	</div>
	    		<div class="col-md-12 mb5">
	            	<textarea class="form-control" style="height:100px;" id="fpr_descriptif" name="cli_descriptif" placeholder="Descriptif"></textarea>
	    		</div>
	    		<div class="col-md-12 mb5">
	                <label for="cli_pu" class="text-left">Prix unitaire (€):</label>
	                <input type="text"  name="cli_pu" id="cli_pu"  value="0" class="form-control input-float" placeholder="Tarif">
	            </div>
	    		<div class="col-md-12 mb5">
	            	<label for="cli_qte" class="text-left">Quantité :</label>
	                    <input type="number" min="0" name="cli_qte" id="cli_qte" class="form-control" value="1" placeholder="Quantité">
	            </div>
	    		<div class="col-md-12 mb5 text-center">
	                   <strong>Prix HT : </strong> <span id="prix_ht">0 €</span>
	            </div>
	    		<input type="hidden" name="cli_pu_input" id="cli_pu_input" class="form-control" value="0">


	      		<div class="col-md-12">
					<!-- FAMILLE -->
					<label for="tva" class="text-left">TVA :</label>
	        		<select name="cli_tva" id="cli_tva" class="select2" >

						<?php foreach($base_tva as $b => $v){

							if($b > 0){
	                        	?>
	                        	<option value="<?= $b ?>" <?php if($b == 1){ ?>selected<?php }?>><?= $v ?></option>
	                        <?php } ?>
	                    <?php } ?>

					</select>
				</div>
				<div class="col-md-12 cli_vehicule mb5" style="display:none;">
	            	<label for="cli_vehicule" class="text-left">Véhicule :</label>
	                <div class="section">

	                    <select id="cli_vehicule" name="cli_vehicule" class="select2">
	                     	<option value="0">Véhicule...</option>
	                        <?php foreach($vehicules as $v): ?>
	                          <option value="<?= $v['veh_id'] ?>"><?= $v['veh_libelle']; ?></option>
	                        <?php endforeach; ?>
	                    </select>

	                </div>
	            </div>
	            <div class="col-md-12 cli_utilisateur mb5" style="display:none;">
	            	<label for="cli_utilisateur" class="text-left">Utilisateur :</label>
	                <div class="section">

	                    <select id="cli_utilisateur" name="cli_utilisateur" class="select2">
	                     	<option value="0">Utilisateur...</option>
	                        <?php foreach($utilisateurs as $u): ?>
	                          <option value="<?= $u['uti_id'] ?>"><?= $u['uti_prenom']; ?> <?= $u['uti_nom']; ?></option>
	                        <?php endforeach; ?>
	                    </select>

	                </div>
	            </div>

            </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-sm" id="ajouter_ligne"><i class="fa fa-floppy-o"></i> Enregistrer</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
			</div>
		</div>
	</div>
</div>
<!-- Fin modal ajout/suppression de ligne -->

<!-- MODAL SUPPRESSION LIGNE -->
<div id="modal_suppr" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Supprimer une ligne</h4>
			</div>

			<div class="modal-body">
				Êtes-vous sûr de vouloir supprimer cette ligne ?

			</div>

			<div class="modal-footer">
				<button type="button" id="ligne_supprimer" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</a>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
			</div>
		</div>

	</div>
</div>
<!-- FIN MODAL SUPPRESSION LIGNE -->

	<!-- MODAL ANNULATION BC -->
	<div id="modal_annule" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Annuler la demande d'achat</h4>
				</div>

				<div class="modal-body">
					<div class="alert alert-sm alert-border-left alert-danger">
						<i class="fa fa-warning"></i>
						<strong>Attention :</strong> Vous ne pourrez plus réouvrir la demande d'achat si vous l'annulez.
					</div>
					Raison de l'annulation de la demande d'achat :
					<textarea id="com_annule_txt" name="com_annule_txt" class="summernote" ></textarea>
					<div id="com_annule_txt_required" ></div>
				</div>
				<div class="modal-footer">
					<button type="button" id="bc_annule" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Annuler la DA</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
				</div>
			</div>

		</div>
	</div>
	<!-- FIN MODAL SUPPRESSION LIGNE -->

<?php
	include "includes/footer_script.inc.php"; ?>
	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<!-- plugin pour les masques formulaires -->
	<script src="assets/js/custom.js"></script>
	<script src="vendor/plugins/holder/holder.min.js"></script>
	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script src="vendor/plugins/summernote/summernote.min.js"></script>
	<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
	<script src="vendor/plugins/jquery.numberformat.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->
	<script>
		// DOCUMENT READY //
		jQuery(document).ready(function () {
<?php 		if(!empty($action_id)){ ?>
				$('#modal_ligne').modal('show');
<?php 		}else{ ?>

				// actualisation produits
				actualiser_select_produit($("#fpr_type").find(':selected').data("revente"));
<?php		} ?>


			$.fn.modal.Constructor.prototype.enforceFocus = $.noop;
			///////////////// INTERRACTIONS UTILISATEUR ///////////////////
			if($("#cli_produit_fournisseur").val() == 0 && !$("#fpr_type").find(':selected').data("revente")){
				afficher_ajouter_fournisseur();
			}else{
				masquer_ajouter_fournisseur();
			}
			$("#cli_produit_fournisseur").change(function () {

				if($(this).val() == 0 && !$("#fpr_type").find(':selected').data("revente")){
					afficher_ajouter_fournisseur();
				}else{
					masquer_ajouter_fournisseur();
				}

			})
			afficher_ajouter_fournisseur();
			// lorsque l'on annule le bc
			$(document).on("click", "#bc_annule", function (){
				if($('#com_annule_txt').code()!=""){
					annule_bc($('#com_annule_txt').code());
					$("#modal_annule").hide();
				}else{
					afficher_txt_user($("#com_annule_txt_required"),"Merci d'indiquer la raison de l'annulation","text-danger",4000);
				}
			});

	<?php 	if($c['com_etat'] >= 1){ ?>
				calcul_tva();
				calcul_ttc();
	<?php 	} ?>

			

			// SI CEST UN VEHICULE
			actualiser_select_vehicule();

			$("#fpr_pro_id").change(function() {
				actualiser_ligne_ajout($(this).val(), 1);
			});
			$("#fpr_pro_id").change(function() {
				actualiser_ligne_ajout($(this).val(), 1);
			});
			$( "#cli_produit_fournisseur" ).change(function() {
				actualiser_ligne_ajout($(this).val(), 0);
			});

			// fin actualisation produits
			// only accept nombres
			$('.number').keypress(function(event) {
			  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
				event.preventDefault();
			  }
			});
			// interdire la valeur 0 sur la qte
			$("body").delegate('#cli_qte', 'focusout', function(){
				if($(this).val() < 0){
					$(this).val('0');
				}
			});

			$("body").delegate('#cli_pu', 'focusout', function(){
				if($(this).val() < 0){
					$(this).val('0');
				}
			});
			// fin interdire la valeur 0 sur la qte

			// mise à jour date de livraison
			$(".com_date_livraison").change(function(){
				if($(this).val() != ""){
					actualise_date_livraison($(this).data("id"), $(this).val());
					/*calcul_tva();
					calcul_ttc();*/
				}

			});

			$(".com_date_livraison_all").change(function() {
				if($(this).val() != ""){
					$(".com_date_livraison").each(function() {
						$(this).val($(".com_date_livraison_all").val());
						actualise_date_livraison($(this).data("id"), $(this).val());
					});
					/*calcul_tva();
					calcul_ttc();*/
				}
			});
			// fin mise à jour date de livraison

			// actualiser le prix ht
				$(document).on('keyup', '#cli_qte', function () {
					if($(this).val() != 0 && $("#cli_pu").val() != 0){
						actualiser_ht($("#cli_pu").val(),$(this).val());
					}
				});
				$(document).on('keyup', '#cli_pu', function () {
					if($(this).val() != 0 && $("#cli_qte").val() != 0){
						actualiser_ht($(this).val(),$("#cli_qte").val());
					}
				});
			// fin actualiser le prix ht
			// actualisation des  select de recherche produit
			$(document).on("change", "#fpr_type", function () {
				var fpr_type = $(this).find(':selected').data("revente");
				actualiser_select_produit(fpr_type);
				actualiser_select_vehicule();
			});
			// fin actualisation des  select de recherche produit

			// passer le parametre de suppression du modal vers le modal
			$(document).on("click", "#delete_ligne", function () {
				var delete_ligne = $(this).data("id");
				$("#ligne_supprimer").data("id", delete_ligne);
			});
			// fin passer le parametre de suppression du modal vers le modal

			// passer le parametre de suppression du modal vers le modal
			$(document).on("click", "#ligne_supprimer", function () {
				var ligne_supprimer = $(this).data("id");

				delete_ligne(ligne_supprimer);
				$("#modal_suppr").modal("hide");
			});
			// fin passer le parametre de suppression du modal vers le modal


			// fin lorsque l'on annule le bc

			// savoir si le modal est edition ou ajout de lignes
			$(document).on("click", ".editer_ligne", function () {

				$("#cli_id").val($(this).data("id"));
				if($(this).data("id") != 0){
					$(".title_ligne").text("Modifier une ligne");
					get_ligne($(this).data("id"));
				}else{
					// remettre a zéro les champs du formulaire
					$(".title_ligne").text("Ajouter une ligne");
					$("#fpr_type").val($("#fpr_type option:first").val());
					$("#fpr_type").trigger("change");
					$("#fpr_reference").val("");
					$("#cli_libelle").val("");
					$("#fpr_descriptif").val("");
					$("#cli_pu").val(0);
					$("#cli_qte").val(0);
					$("#cli_tva").val(1);
					$("#cli_tva").trigger("change");
					$("#fpr_categorie").val(0);
					$("#fpr_categorie").trigger("change");
					$("#cli_famille_si2p").val(0);
					$("#cli_famille_si2p").trigger("change");
					$("#cli_sous_famille_si2p").val(0);
					$("#cli_sous_famille_si2p").trigger("change");
					$("#cli_sous_sous_famille_si2p").val(0);
					$("#cli_sous_sous_famille_si2p").trigger("change");
					$("#cli_utilisateur").val(0);
					$("#cli_utilisateur").trigger("change");
					$("#cli_vehicule").val(0);
					$("#cli_vehicule").trigger("change");
					$("#prix_ht").html("0 €");
					$("#fpr_pro_id").val(0);
					$("#fpr_pro_id").trigger("change");
					$('#base-fournisseur').prop('checked', false); // Unchecks it
				}
			});
			// fin savoir si le modal est edition ou ajout de lignes
			// submit le modal d'ajout de ligne
			$(document).on("click", "#ajouter_ligne", function () {
				if($("#cli_id").val() != 0){
					modifier_ligne($("#cli_id").val(), $("#fpr_type").val(), $("#fpr_categorie").val(), $("#pro_sous_sous_famille").val(),$("#cli_famille_si2p").val(), $("#pro_sous_famille").val(),$("#fpr_pro_id").val(), $("#fpr_reference").val(), $("#cli_libelle").val(), $("#fpr_descriptif").val(), $("#cli_pu").val(), $("#cli_qte").val(), $("#cli_tva").val(), $("#cli_vehicule").val(), $("#cli_utilisateur").val(), $("#base-fournisseur").val());
				}else{
					enregistrer_ligne($("#fpr_categorie").val(),$("#fpr_type").val(), $("#pro_sous_sous_famille").val(),$("#cli_famille_si2p").val(), $("#pro_sous_famille").val(),$("#fpr_pro_id").val(), $("#fpr_reference").val(), $("#cli_libelle").val(), $("#fpr_descriptif").val(), $("#cli_pu").val(), $("#cli_qte").val(), $("#cli_tva").val(), $("#cli_vehicule").val(), $("#cli_utilisateur").val(),$("#base-fournisseur").val());
				}
			});



			////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
		});

		///////////// FONCTIONS //////////////

		// annule le bon de commande
		function annule_bc(txt){
			txt = encodeURIComponent(txt);
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_commande_annule.php',
				data : {
					"com_id":<?=$_GET['commande']?>,
					"com_annule_txt": txt,
				},
				dataType: 'JSON',
				success: function(data)
				{	$("#com_annule_panel").show(400);
					if(data != ""){
						$("#com_annule_title").html("<h5 class='text-muted'>Ce bon de commande a été annulé pour la raison suivante : " + data.com_annule_txt + "</h5>");
						$(".btn-actions").hide();
					}

				},
				error: function(data)
				{	modal_alerte_user(data.responseText,"");

				}
			});

		}

///	/
//ACTUALISER SELECT VEHICULES
/////
function actualiser_select_vehicule(){
	if($("#fpr_type").val() == 1){
		$(".cli_vehicule").show();
		$(".cli_utilisateur").hide();
		$(".cli_utilisateur").val(0);
		$(".cli_utilisateur").trigger("change");
	}else if($("#fpr_type").val() != 1 && $("#fpr_type").val() != 7){
		$(".cli_vehicule").hide();
		$(".cli_utilisateur").hide();
		$(".cli_vehicule").val(0);
		$(".cli_vehicule").trigger("change");
	}else{
		$(".cli_vehicule").hide();
		$(".cli_utilisateur").hide();
		$(".cli_vehicule").val(0);
		$(".cli_vehicule").trigger("change");
		$(".cli_utilisateur").val(0);
		$(".cli_utilisateur").trigger("change");
	}
}

/// ajouter une ligne

function enregistrer_ligne(categorie, type,famille, sous_sous_famille,sous_famille, produit, reference, libelle, descriptif, prix_ht, prix_qte, tva, vehicule, utilisateur, ajout_fournisseur){
	reference = encodeURIComponent(reference);
	libelle = encodeURIComponent(libelle);
	descriptif = encodeURIComponent(descriptif);
	if(prix_ht && prix_qte && prix_ht != 0 && prix_qte != 0){
		$.ajax({
			type: 'POST',
			url: 'ajax/ajax_commande_ajout.php',
			data : {
				"categorie":categorie,
				"type":type,
				"famille":famille,
				"sous_famille":sous_famille,
				"sous_sous_famille":sous_sous_famille,
				"produit":produit,
				"reference":reference,
				"libelle":libelle,
				"descriptif":descriptif,
				"prix_ht":prix_ht,
				"prix_qte":prix_qte,
				"tva":tva,
				"commande": <?= $_GET['commande'] ?>,
				"vehicule": vehicule,
				"utilisateur": utilisateur,
				"ajout_fournisseur":ajout_fournisseur,
				"fournisseur":<?=  $c['com_fournisseur'] ?>
			},
			dataType: 'json',
			success: function(data)
			{
				if(data != undefined){
					if(data['ffa_libelle'] != "Véhicules"){
						if(data['ffa_libelle'] == "Informatique et Télécom"){
							$('#table_id > tbody:last-child').append(
						"<tr class='ligne_" + data['cli_id'] + "'><td>" + data['famille'] + "<br><strong>Utilisateur : </strong>" + data['uti_libelle'] + "</td><td>" + data['produit'] + "</td><td>" + data['cli_reference'] + "</td><td><strong>" + data['cli_libelle'] + "</strong><br>" + nl2br(data['cli_descriptif']) + "</td><td class='td_prix_ht'>" + $.number( prix_ht, 2, ',', ' ' ) + "</td><td class='td_prix_qte'>" + prix_qte + "</td><td>" + $.number( prix_qte * prix_ht, 2, ',', ' ' ) + "</td><td>" + data['tva'] + "</td><td><button type='button' class='btn btn-sm btn-warning editer_ligne' data-toggle='modal' data-target='#modal_ligne' data-id='" + data['cli_id'] + "'><i class='fa fa-pencil'></i></button> <button type='button' class='btn btn-sm btn-danger' id='delete_ligne' data-id='" + data['cli_id'] + "' data-target='#modal_suppr' data-toggle='modal'><i class='fa fa-times'></i></button></td></tr>"
						);
						}else{
							$('#table_id > tbody:last-child').append(
						"<tr class='ligne_" + data['cli_id'] + "'><td>" + data['famille'] + "</td><td>" + data['produit'] + "</td><td>" + data['cli_reference'] + "</td><td><strong>" + data['cli_libelle'] + "</strong><br>" + nl2br(data['cli_descriptif']) + "</td><td class='td_prix_ht'>" + $.number( prix_ht, 2, ',', ' ' ) + "</td><td class='td_prix_qte'>" + prix_qte + "</td><td>" + $.number( prix_qte * prix_ht, 2, ',', ' ' ) + "</td><td>" + data['tva'] + "</td><td><button type='button' class='btn btn-sm btn-warning editer_ligne' data-toggle='modal' data-target='#modal_ligne' data-id='" + data['cli_id'] + "'><i class='fa fa-pencil'></i></button> <button type='button' class='btn btn-sm btn-danger' id='delete_ligne' data-id='" + data['cli_id'] + "' data-target='#modal_suppr' data-toggle='modal'><i class='fa fa-times'></i></button></td></tr>"
						);
						}

					}else{
						$('#table_id > tbody:last-child').append(
						"<tr class='ligne_" + data['cli_id'] + "'><td>" + data['famille'] + "<br><strong>Véhicule : </strong>" + data['veh_libelle'] + "</td><td>" + data['produit'] + "</td><td>" + data['cli_reference'] + "</td><td><strong>" + data['cli_libelle'] + "</strong><br>" + nl2br(data['cli_descriptif']) + "</td><td class='td_prix_ht'>" + $.number( prix_ht, 2, ',', ' ' ) + "</td><td class='td_prix_qte'>" + prix_qte + "</td><td>" + $.number( prix_qte * prix_ht, 2, ',', ' ' ) + "</td><td>" + data['tva'] + "</td><td><button type='button' class='btn btn-sm btn-warning editer_ligne' data-toggle='modal' data-target='#modal_ligne' data-id='" + data['cli_id'] + "'><i class='fa fa-pencil'></i></button> <button type='button' class='btn btn-sm btn-danger' id='delete_ligne' data-id='" + data['cli_id'] + "' data-target='#modal_suppr' data-toggle='modal'><i class='fa fa-times'></i></button></td></tr>"
						);
					}
					actualiser_total();
					$("#modal_ligne").modal("hide");
				}
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}

		});
	}else{
		alert("Veuillez remplir le prix et la quantité svp.");
	}
}

/// modifier une ligne

function modifier_ligne(cli_id, type,categorie,sous_sous_famille, famille, sous_famille, produit, reference, libelle, descriptif, prix_ht, prix_qte, tva,  vehicule, utilisateur, ajout_fournisseur){
	reference = encodeURIComponent(reference);
	libelle = encodeURIComponent(libelle);
	descriptif = encodeURIComponent(descriptif);

	$.ajax({
		type: 'POST',
		url: 'ajax/ajax_commande_ajout.php',
		data : {
			"cli_id":cli_id,
			"categorie":categorie,
			"type":type,
			"famille":famille,
			"sous_famille":sous_famille,
			"sous_sous_famille":sous_sous_famille,
			"produit":produit,
			"reference":reference,
			"libelle":libelle,
			"descriptif":descriptif,
			"prix_ht":prix_ht,
			"prix_qte":prix_qte,
			"tva":tva,
			"commande" : <?= $_GET['commande'] ?>,
			"vehicule": vehicule,
			"utilisateur": utilisateur,
			"ajout_fournisseur" : ajout_fournisseur,
			"fournisseur":<?=  $c['com_fournisseur'] ?>
		},
		dataType: 'json',
		success: function(data)
		{
			if(data != undefined){
				if(!data['veh_libelle']){
					$('.ligne_' + cli_id).html(
						"<td>" + data['famille'] + "</td><td>" + data['produit'] + "</td><td>" + data['cli_reference'] + "</td><td><strong>" + data['cli_libelle'] + "</strong><br>" + nl2br(data['cli_descriptif']) + "</td><td class='td_prix_ht'>" + $.number( prix_ht, 2, ',', ' ' ) + "</td><td class='td_prix_qte'>" + prix_qte + "</td><td>" + $.number( prix_qte * prix_ht, 2, ',', ' ' ) + "</td><td>" + data['tva'] + "</td><td><button type='button' class='btn btn-sm btn-warning editer_ligne' data-toggle='modal' data-target='#modal_ligne' data-id='" + data['cli_id'] + "'><i class='fa fa-pencil'></i></button> <button type='button' class='btn btn-sm btn-danger' id='delete_ligne' data-id='" + cli_id + "' data-target='#modal_suppr' data-toggle='modal' data-id='" + data['cli_id'] + "'><i class='fa fa-times'></i></button></td>"
					);
				}else{
					$('.ligne_' + cli_id).html(
						"<td>" + data['famille'] + "<br><strong>Véhicule : </strong>" + data['veh_libelle'] + "</td><td>" + data['produit'] + "</td><td>" + data['cli_reference'] + "</td><td><strong>" + data['cli_libelle'] + "</strong><br>" + nl2br(data['cli_descriptif']) + "</td><td class='td_prix_ht'>" + $.number( prix_ht, 2, ',', ' ' ) + "</td><td class='td_prix_qte'>" + prix_qte + "</td><td>" + $.number( prix_qte * prix_ht, 2, ',', ' ' ) + "</td><td>" + data['tva'] + "</td><td><button type='button' class='btn btn-sm btn-warning editer_ligne' data-toggle='modal' data-target='#modal_ligne' data-id='" + data['cli_id'] + "'><i class='fa fa-pencil'></i></button> <button type='button' class='btn btn-sm btn-danger' id='delete_ligne' data-id='" + cli_id + "' data-target='#modal_suppr' data-toggle='modal' data-id='" + data['cli_id'] + "'><i class='fa fa-times'></i></button></td>"
					);
				}
				/* if(data['ffa_libelle'] != "Véhicules"){
					if(data['ffa_libelle'] == "Informatique et Télécom"){
						$('.ligne_' + cli_id).html(
					"<tr class='ligne_" + data['cli_id'] + "'><td>" + data['famille'] + "<br><strong>Utilisateur : </strong>" + data['uti_libelle'] + "</td><td>" + data['produit'] + "</td><td>" + data['cli_reference'] + "</td><td><strong>" + data['cli_libelle'] + "</strong><br>" + data['cli_descriptif'] + "</td><td class='td_prix_ht'>" + $.number( prix_ht, 2, ',', ' ' ) + "</td><td class='td_prix_qte'>" + prix_qte + "</td><td>" + $.number( prix_qte * prix_ht, 2, ',', ' ' ) + "</td><td>" + data['tva'] + "</td><td><button type='button' class='btn btn-sm btn-warning editer_ligne' data-toggle='modal' data-target='#modal_ligne' data-id='" + data['cli_id'] + "'><i class='fa fa-pencil'></i></button> <button type='button' class='btn btn-sm btn-danger' id='delete_ligne' data-id='" + data['cli_id'] + "' data-target='#modal_suppr' data-toggle='modal'><i class='fa fa-times'></i></button></td></tr>"
					);
					}else{
						$('.ligne_' + cli_id).html(
					"<tr class='ligne_" + data['cli_id'] + "'><td>" + data['famille'] + "</td><td>" + data['produit'] + "</td><td>" + data['cli_reference'] + "</td><td><strong>" + data['cli_libelle'] + "</strong><br>" + data['cli_descriptif'] + "</td><td class='td_prix_ht'>" + $.number( prix_ht, 2, ',', ' ' ) + "</td><td class='td_prix_qte'>" + prix_qte + "</td><td>" + $.number( prix_qte * prix_ht, 2, ',', ' ' ) + "</td><td>" + data['tva'] + "</td><td><button type='button' class='btn btn-sm btn-warning editer_ligne' data-toggle='modal' data-target='#modal_ligne' data-id='" + data['cli_id'] + "'><i class='fa fa-pencil'></i></button> <button type='button' class='btn btn-sm btn-danger' id='delete_ligne' data-id='" + data['cli_id'] + "' data-target='#modal_suppr' data-toggle='modal'><i class='fa fa-times'></i></button></td></tr>"
					);
					}

				}else{
					$('.ligne_' + cli_id).html(
					"<tr class='ligne_" + data['cli_id'] + "'><td>" + data['famille'] + "<br><strong>Véhicule : </strong>" + data['veh_libelle'] + "</td><td>" + data['produit'] + "</td><td>" + data['cli_reference'] + "</td><td><strong>" + data['cli_libelle'] + "</strong><br>" + data['cli_descriptif'] + "</td><td class='td_prix_ht'>" + $.number( prix_ht, 2, ',', ' ' ) + "</td><td class='td_prix_qte'>" + prix_qte + "</td><td>" + $.number( prix_qte * prix_ht, 2, ',', ' ' ) + "</td><td>" + data['tva'] + "</td><td><button type='button' class='btn btn-sm btn-warning editer_ligne' data-toggle='modal' data-target='#modal_ligne' data-id='" + data['cli_id'] + "'><i class='fa fa-pencil'></i></button> <button type='button' class='btn btn-sm btn-danger' id='delete_ligne' data-id='" + data['cli_id'] + "' data-target='#modal_suppr' data-toggle='modal'><i class='fa fa-times'></i></button></td></tr>"
					);
				} */
				actualiser_total();
				$("#modal_ligne").modal("hide");
			}
		}

	});
}

/// retourne une commande

function get_ligne(cli_id){
	$.ajax({
		type: 'POST',
		url: 'ajax/ajax_commande_ligne.php',
		data : {
			"cli_id":cli_id,
		},
		dataType: 'json',
		success: function(data)
		{
			if(data != undefined){
				$("#fpr_type").val(data['cli_famille']);
				$("#fpr_type").trigger("change");
				$("#fpr_reference").val(data['cli_reference']);
				$("#cli_libelle").val(data['cli_libelle']);
				$("#cli_produit_fournisseur").val(data['cli_libelle']);
				$("#fpr_descriptif").val(data['cli_descriptif']);
				$("#cli_pu").val(data['cli_pu']);
				$("#cli_qte").val(data['cli_qte']);
				$("#cli_tva").val(data['cli_tva']);
				$("#cli_tva").trigger("change");
				$("#fpr_categorie").val(data['cli_categorie_si2p']);
				$("#fpr_categorie").trigger("change");
				$("#cli_famille_si2p").val(data['cli_famille_si2p']);
				$("#cli_famille_si2p").trigger("change");
				$("#cli_sous_famille_si2p").val(data['cli_sous_famille_si2p']);
				$("#cli_sous_famille_si2p").trigger("change");
				$("#cli_sous_sous_famille_si2p").val(data['cli_sous_sous_famille_si2p']);
				$("#cli_sous_sous_famille_si2p").trigger("change");
				if(data['cli_utilisateur'] != ""){
					$("#cli_utilisateur").val(data['cli_utilisateur']);
					$("#cli_utilisateur").trigger("change");
					$("#cli_utilisateur").show();
				}else{
					$("#cli_utilisateur").hide();
				}


				$("#cli_vehicule").val(data['cli_vehicule']);
				$("#cli_vehicule").trigger("change");
				$("#prix_ht").html(data['cli_qte']*data['cli_pu'] + " €");
			}
		}

	});
}
// calcul tva

function calcul_tva(){

	$.ajax({
		type: 'POST',
		url: 'ajax/ajax_commande_tva.php',
		data : {
			"commande":<?= $_GET['commande'] ?>,
		},
		dataType: 'json',
		success: function(data)

		{

			$('.tva_table').remove();
			$.each(data, function(key, value) {
	            $('#tva_table tr:first-child').after("<tr class='tva_table'><td><strong>TVA (" + value["nom"] + " %)</strong></td><td>" + value["montant"] + " €</td></tr>");
	        });
		}

	});
}
// fin calcul tva

// calcul tva

function calcul_ttc(){

	$.ajax({
		type: 'POST',
		url: 'ajax/ajax_commande_ttc.php',
		data : {
			"commande":<?= $_GET['commande'] ?>,
		},
		dataType: 'html',
		success: function(data)
		{

			$('.table_ttc').html("");

            $('.table_ttc').html(data + " €");
			console.log(data);

		}

	});
}
function get_ttc(){
	$.ajax({
		type: 'POST',
		url: 'ajax/ajax_get_ttc_all.php',
		data : {
			"commande":<?= $_GET['commande'] ?>,
		},
		dataType: 'html',
		success: function(data)
		{

			$('.table_ttc').html("");

			$('.table_ttc').html(data + " €");
			console.log(data);

		}

	});
}
// fin calcul tva


// actualiser les select dans la recherche produits

function actualiser_select_produit(type){

	console.log("actualiser_select_produit");

	// savoir si
	$.ajax({
		type:'POST',
		url: 'ajax/ajax_fournisseur_traitement_produit.php',
		data : 'type=' + type,
		dataType: 'json',
		success: function(data)
		{

			if(data == 1 ){// si pas sous-traitance ni achat pour revente


				$(".fpr_categorie").hide();
				$(".pro_famille").hide();
				$(".pro_sous_famille").hide();
				$("#fpr_categorie").val(0);
				$("#fpr_categorie").trigger("change");
				$(".produit_si2p").hide();
				$("#pro_famille").val(0);
				$("#pro_famille").trigger("change");
				$("#cli_famille_si2p").val(0);
				$("#cli_famille_si2p").trigger("change");
				$("#pro_sous_famille").val(0);
				$("#pro_sous_famille").trigger("change");
				$(".cli_produit_fournisseur").show();
				$("#cli_produit_fournisseur").find("option:gt(0)").remove();


				// aller chercher le produit du fournisseur
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_fournisseur_get_produit.php',
					data : 'type=' + $("#fpr_type").val() + "&fournisseur=<?= $c['com_fournisseur'] ?>",
					dataType: 'json',
					success: function(data2)
					{
						 $.each(data2, function(key, value) {
				            $('#cli_produit_fournisseur').append($("<option></option>").attr("value",value["fpr_id"]).text(value["fpr_reference"]));
				        });
					}
				});


			}else{ // si achat pour revente OU sous_traitance

				// actualisation du select categorie
				$("#fpr_categorie").find("option:gt(0)").remove();
		        $.each(data, function(key, value) {
		            $('#fpr_categorie').append($("<option></option>").attr("value",value["pca_id"]).text(value["pca_libelle"]));
		        });
				$(".fpr_categorie").show();
				$(".pro_famille").show();
				$(".pro_sous_famille").show();
				$(".produit_si2p").show();
				$(".cli_produit_fournisseur").hide();

			}

		}

	});

}

// fin actualiser les select dans la recherche produits
 // produits SI2P en fonction de la catégorie
function produits(){
	famille=$("#cli_famille_si2p").val();
	sous_famille=$("#pro_sous_famille").val();
	sous_sous_famille=$("#pro_sous_sous_famille").val();
    type=$("#fpr_type").val();
    categorie=$("#fpr_categorie").val();
        selected = 0;

    $.ajax({
      type:'POST',
      url: 'ajax/ajax_fournisseur_produit.php',
      data : 'categorie=' + categorie + "&famille=" + famille + "&sous_famille=" + sous_famille+"&sous_sous_famille=" + sous_sous_famille +"&type=" + type,
      dataType: 'json',
      success: function(data)
      {
        if (data['0'] == undefined){

            $("#fpr_pro_id").find("option:gt(0)").remove();
            $("#fpr_pro_code_produit").val("");
        }else{
          $("#fpr_pro_id").find("option:gt(0)").remove();
          // on recupere pro_code_produit intra

            $.each(data, function(key, value) {
                if(value['pro_id'] == selected){

					$('#fpr_pro_id')
					.append($("<option></option>")
					.attr("value",value["pro_id"])
					.text(value["pro_code_produit"])
					.attr("selected",true));

                }else{
                  	$('#fpr_pro_id')
                  	.append($("<option></option>")
                  	.attr("value",value["pro_id"])
                  	.text(value["pro_code_produit"]));
                }

            });
        }
      }
    });
  }

// actualiser les inputs à la création
function actualiser_ligne_ajout(produit, si2p){

  	$.ajax({
		type:'POST',
		url: 'ajax/ajax_commande_actualise.php',
		data : 'produit=' + produit + "&si2p=" + si2p,
		dataType: 'json',
		success: function(data)
		{
			if(data['pro_id'] === undefined){
				$("#fpr_reference").val(data['fpr_reference']);
				$("#cli_libelle").val(data['fpr_libelle']);
				$("#fpr_descriptif").val(data['fpr_descriptif']);
				$("#cli_pu").val(data['fpr_tarif']);
			}else{
				$("#fpr_reference").val(data['pro_code_produit']);
				$("#cli_libelle").val(data['pro_libelle']);
				$("#cli_pu").val(data['pro_ht_intra']);
			}

		}
    });

}
// actualiser prix hors taxe
function actualiser_ht(pu,quantite){

	prix_ht = quantite * pu;


  	prix_ht = $("#prix_ht").number(prix_ht, 2, ",", " ") + " €";
  	$("#cli_pu_input").val(prix_ht);

}

// actualiser date de livraison
function actualise_date_livraison(cli_id, cli_livraison){
	$.ajax({

		type: 'POST',
		url: 'ajax/ajax_commande_date_livraison.php',
		data : {
			"cli_id": cli_id,
			"date": cli_livraison,
		},
		dataType: 'html',
		success: function(data)
		{
			if(data == 1){
				<?php if($c['com_etat'] == 2 && $bap == true && (($_SESSION['acces']['acc_ref_id'] == $c['com_donneur_ordre'] && $_SESSION['acces']['acc_droits'][16]==true))){ ?>
					$("#bap").show();
				<?php } ?>
			}else{
				$("#bap").hide();
			}
		}

	});
}
// fin actualiser date de livraison

// supprimer une ligne
function delete_ligne(cli_id){

	$.ajax({
		type:'POST',
		url: 'ajax/ajax_del_commande.php',
		data : 'cli_id=' + cli_id,
		dataType: 'html',
		success: function(data)
		{
			$(".ligne_" + data).remove();
			actualiser_total();
		}
    });

}



// actualiser total
function actualiser_total(){
	var total = 0;
	var ttc = 0;
  	/* $('#table_id > tbody  > tr').each(function() {
  		var qte = $(this).find(".td_prix_qte").text();
  		var prix_ht = $.trim($(this).find(".td_prix_ht").text());
  		ht = parseFloat(prix_ht.replace(',','.').replace(' ',''));
  		if(total == 0){
  			total = qte * ht;
  		}else{
  			total = total + qte * ht;
  		}
  	}); */

  	$.ajax({
		type:'POST',
		url: 'ajax/ajax_commande_actualise_total.php',
		data : {
			"com_id":<?= $_GET['commande'] ?>
		},
		dataType: 'html',
		success: function(data)
		{
			$(".total_ht_txt").text($.number(data, 2, ",", " ") + " €");
		}
    });

	$.ajax({
		type:'POST',
		url: 'ajax/ajax_get_ttc.php',
		data : {
			"commande": <?= $_GET['commande'] ?>
		},
		dataType: 'json',
		success: function(data_ttc)
		{
			$('.tva_table').remove();
			$.each(data_ttc, function(key, value) {
				$('#tva_table tr:first-child').after("<tr class='tva_table'><td><strong>TVA (" + value["nom"] + " %)</strong></td><td>" + value["montant"] + " €</td></tr>");
			});
			get_ttc();
		}
	});


}
//////////// FIN FONCTIONS //////////

function afficher_ajouter_fournisseur(){
	$(".base-fournisseur").show();
}
function masquer_ajouter_fournisseur(){
	$(".base-fournisseur").show();
	$('#base-fournisseur').prop('checked', false); // Unchecks it
}

</script>

</body>
</html>
