<?php
	
	include "includes/controle_acces.inc.php";
	
	include_once 'includes/connexion.php';
	
	include("modeles/mod_upload.php");
	
	$erreur="";
	$warning="";

	if(!empty($_POST)){

		$age_societe=0;
		if(isset($_POST['age_societe'])){
			if(intval($_POST['age_societe'])){
				$age_societe=intval($_POST['age_societe']);
			}
		}
		
	}else{
		$erreur="Formulaire incomplet";
	}
	
	IF(empty($erreur)){
		
		$age_id=0;
		if(isset($_POST['age_id'])){
			if(intval($_POST['age_id'])){
				$age_id=intval($_POST['age_id']);
			}
		}
		
		$age_compta=0;
		if(isset($_POST['age_compta'])){
			if(!empty($_POST['age_compta'])){
				$age_compta=$_POST['age_compta'];
			};
		}
		
		$age_compta_age=0;
		if(isset($_POST['age_compta_age'])){
			if(!empty($_POST['age_compta_age'])){
				$age_compta_age=$_POST['age_compta_age'];
			};
		}
		
		$age_structure=0;
		if(isset($_POST['age_structure'])){
			if(!empty($_POST['age_structure'])){
				$age_structure=$_POST['age_structure'];
			};
		}
		
		$age_responsable=0;
		if(isset($_POST['age_responsable'])){
			if(!empty($_POST['age_responsable'])){
				$age_responsable=$_POST['age_responsable'];
			};
		}
		
		$age_interco_intra=0;
		if(isset($_POST['age_interco_intra'])){
			if(!empty($_POST['age_interco_intra'])){
				$age_interco_intra=intval($_POST['age_interco_intra']);
			};
		}
		
		$age_interco_inter=0;
		if(isset($_POST['age_interco_inter'])){
			if(!empty($_POST['age_interco_inter'])){
				$age_interco_inter=intval($_POST['age_interco_inter']);
			};
		}
		
		if($age_id==0){
			
			// NOUVELLE AGENCE
		
			$req = $Conn->prepare("INSERT INTO agences (age_societe,age_compta,age_compta_age,age_responsable, age_nom, age_code, age_ad1, age_ad2, age_ad3, age_cp, age_ville,
			age_tel,age_fax, age_mail, age_site, age_siret,age_siren,age_tva,age_num_existence,age_ape,age_rcs,age_capital,age_type,age_structure,age_compta_tel,age_compta_fax,age_compta_mail,age_tva_type
			,age_interco_intra,age_interco_inter,age_h_deb_am,age_h_deb_matin,age_h_fin_am,age_h_fin_matin)
			VALUES (:age_societe, :age_compta, :age_compta_age, :age_responsable, :age_nom, :age_code, :age_ad1, :age_ad2, :age_ad3, :age_cp, :age_ville,
			:age_tel, :age_fax, :age_mail, :age_site, :age_siret, :age_siren, :age_tva, :age_num_existence, :age_ape, :age_rcs, :age_capital, :age_type, :age_structure, :age_compta_tel, :age_compta_fax,
			:age_compta_mail, :age_tva_type, :age_interco_intra, :age_interco_inter,:age_h_deb_am,:age_h_deb_matin,:age_h_fin_am,:age_h_fin_matin)");
			$req->bindParam(":age_societe",$_POST['age_societe']);
			$req->bindParam(":age_compta",$age_compta);
			$req->bindParam(":age_compta_age",$age_compta_age);
			$req->bindParam(":age_responsable",$age_responsable);
			$req->bindParam(":age_nom",$_POST['age_nom']);
			$req->bindParam(":age_code",$_POST['age_code']);
			$req->bindParam(":age_ad1",$_POST['age_ad1']);
			$req->bindParam(":age_ad2",$_POST['age_ad2']);
			$req->bindParam(":age_ad3",$_POST['age_ad3']);
			$req->bindParam(":age_cp",$_POST['age_cp']);
			$req->bindParam(":age_ville",$_POST['age_ville']);
			$req->bindParam(":age_tel",$_POST['age_tel']);
			$req->bindParam(":age_fax",$_POST['age_fax']);
			$req->bindParam(":age_mail",$_POST['age_mail']);
			$req->bindParam(":age_site",$_POST['age_site']);
			$req->bindParam(":age_siret",$_POST['age_siret']);
			$req->bindParam(":age_siren",$_POST['age_siren']);
			$req->bindParam(":age_tva",$_POST['age_tva']);
			$req->bindParam(":age_num_existence",$_POST['age_num_existence']);
			$req->bindParam(":age_ape",$_POST['age_ape']);
			$req->bindParam(":age_rcs",$_POST['age_rcs']);
			$req->bindParam(":age_capital",$_POST['age_capital']);
			$req->bindParam(":age_type",$_POST['age_type']);
			$req->bindParam(":age_structure",$age_structure);
			$req->bindParam(":age_compta_tel",$_POST['age_compta_tel']);
			$req->bindParam(":age_compta_fax",$_POST['age_compta_fax']);
			$req->bindParam(":age_compta_mail",$_POST['age_compta_mail']);
			$req->bindParam(":age_tva_type",$_POST['age_tva_type']);
			$req->bindParam(":age_interco_intra",$age_interco_intra);
			$req->bindParam(":age_interco_inter",$age_interco_inter);
			$req->bindParam(":age_h_deb_am",$_POST['age_h_deb_am']);
			$req->bindParam(":age_h_deb_matin",$_POST['age_h_deb_matin']);
			$req->bindParam(":age_h_fin_am",$_POST['age_h_fin_am']);
			$req->bindParam(":age_h_fin_matin",$_POST['age_h_fin_matin']);
			try{
				$req->execute();
				$age_id = $Conn->lastInsertId();
			}Catch(Exeception $e){
				$erreur=$e->getMessage();
			}
			
		}else{
			
			$sql ="UPDATE agences SET age_societe=:age_societe, age_compta=:age_compta, age_compta_age=:age_compta_age, age_responsable=:age_responsable";
			$sql =$sql . ",age_nom=:age_nom,age_code=:age_code, age_ad1=:age_ad1, age_ad2=:age_ad2, age_cp=:age_cp, age_ville=:age_ville, age_ad3=:age_ad3"; //
			$sql =$sql . ",age_tel=:age_tel, age_fax=:age_fax, age_mail=:age_mail, age_site=:age_site, age_siret=:age_siret, age_siren=:age_siren, age_tva=:age_tva, age_num_existence=:age_num_existence,
			age_ape=:age_ape, age_rcs=:age_rcs, age_capital=:age_capital, age_type=:age_type, age_structure=:age_structure, age_compta_tel=:age_compta_tel, age_compta_fax=:age_compta_fax,
			age_compta_mail=:age_compta_mail, age_tva_type=:age_tva_type, age_interco_intra=:age_interco_intra, age_interco_inter=:age_interco_inter,
			age_h_deb_am=:age_h_deb_am, age_h_deb_matin=:age_h_deb_matin, age_h_fin_am=:age_h_fin_am, age_h_fin_matin=:age_h_fin_matin";
			$sql = $sql . " WHERE age_id=:age_id";
			$req = $Conn->prepare($sql);
		
			$req->bindParam(":age_id",$age_id);
			$req->bindParam(":age_societe",$age_societe);
			$req->bindParam(":age_compta",$age_compta);
			$req->bindParam(":age_compta_age",$age_compta_age);
			$req->bindParam(":age_responsable",$age_responsable);
			$req->bindParam(":age_nom",$_POST['age_nom']);
			$req->bindParam(":age_code",$_POST['age_code']);
			$req->bindParam(":age_ad1",$_POST['age_ad1']);
			$req->bindParam(":age_ad2",$_POST['age_ad2']);
			$req->bindParam(":age_ad3",$_POST['age_ad3']);
			$req->bindParam(":age_cp",$_POST['age_cp']);
			$req->bindParam(":age_ville",$_POST['age_ville']);
			$req->bindParam(":age_tel",$_POST['age_tel']);
			$req->bindParam(":age_fax",$_POST['age_fax']);
			$req->bindParam(":age_mail",$_POST['age_mail']);
			$req->bindParam(":age_site",$_POST['age_site']);
			$req->bindParam(":age_siret",$_POST['age_siret']);
			$req->bindParam(":age_siren",$_POST['age_siren']);
			$req->bindParam(":age_tva",$_POST['age_tva']);
			$req->bindParam(":age_num_existence",$_POST['age_num_existence']);
			$req->bindParam(":age_ape",$_POST['age_ape']);
			$req->bindParam(":age_rcs",$_POST['age_rcs']);
			$req->bindParam(":age_capital",$_POST['age_capital']);
			$req->bindParam(":age_type",$_POST['age_type']);
			$req->bindParam(":age_structure",$age_structure);
			$req->bindParam(":age_compta_tel",$_POST['age_compta_tel']);
			$req->bindParam(":age_compta_fax",$_POST['age_compta_fax']);
			$req->bindParam(":age_compta_mail",$_POST['age_compta_mail']);
			$req->bindParam(":age_tva_type",$_POST['age_tva_type']);	
			$req->bindParam(":age_interco_intra",$age_interco_intra);
			$req->bindParam(":age_interco_inter",$age_interco_inter);
			$req->bindParam(":age_h_deb_am",$_POST['age_h_deb_am']);
			$req->bindParam(":age_h_deb_matin",$_POST['age_h_deb_matin']);
			$req->bindParam(":age_h_fin_am",$_POST['age_h_fin_am']);
			$req->bindParam(":age_h_fin_matin",$_POST['age_h_fin_matin']);
			try{
				$req->execute();
			}Catch(Exeception $e){
				$erreur=$e->getMessage();
			}
				
		}
	}else{
		$erreur="Formulaire incomplet";
	}
	
	// MISE A JOUR DES LOGOS
  
	if(empty($erreur)){

		$logo1 = null;
		$logo2 = null;
		$qualite1 = null;
		$qualite2 = null;
		
		if (!empty($_FILES['age_logo_1']['name'])) {

			$er_logo_1 = upload('age_logo_1', "societes/logos/","age_logo_1_" . $age_id . "_" . date('m-d-Y'), 0, array(), 1);
			if($er_logo_1==0){
				$logo1 = "age_logo_1_" . $age_id . "_" . date('m-d-Y') . "." . pathinfo($_FILES['age_logo_1']['name'], PATHINFO_EXTENSION);
			}else{
				$warning="Certains logos n'ont pas été chargés.";
			}
		}

		if (!empty($_FILES['age_logo_2']['name'])) {
			$er_logo_2 = upload('age_logo_2', "societes/logos/","age_logo_2_" . $age_id . "_" . date('m-d-Y'), 0, array(), 1);
			if($er_logo_2==0){
				$logo2 = "age_logo_2_" . $age_id . "_" . date('m-d-Y') . "." . pathinfo($_FILES['age_logo_2']['name'], PATHINFO_EXTENSION);
			}else{
				$warning="Certains logos n'ont pas été chargés.";
			}
		}
		if (!empty($_FILES['age_qualite_1']['name'])) {
			$er_qualite_1 = upload('age_qualite_1', "societes/logos/","age_qualite_1_" . $age_id . "_" . date('m-d-Y'), 0, array(), 1);
			if($er_qualite_1==0){
				$qualite1 = "age_qualite_1_" . $age_id . "_" . date('m-d-Y') . "." . pathinfo($_FILES['age_qualite_1']['name'], PATHINFO_EXTENSION);
			}else{
				$warning="Certains logos n'ont pas été chargés.";
			}
		}
		if (!empty($_FILES['age_qualite_2']['name'])) {
			$er_qualite_2 = upload('age_qualite_2', "societes/logos/","age_qualite_2_" . $age_id . "_" . date('m-d-Y'), 0, array(), 1);
			if($er_qualite_2==0){
				$qualite2 = "age_qualite_2_" . $age_id . "_" . date('m-d-Y') . "." . pathinfo($_FILES['age_qualite_2']['name'], PATHINFO_EXTENSION);
			}else{
				$warning="Certains logos n'ont pas été chargés.";
			}
		}
		
		if(!empty($logo1) OR !empty($logo2) OR !empty($qualite1) OR !empty($qualite2)){
			
			$sql="UPDATE agences SET";
			if(!empty($logo1)){
				$sql.=" age_logo_1 ='" . $logo1 . "',";
			}
			if(!empty($logo2)){
				$sql.=" age_logo_2 ='" . $logo2 . "',";
			}
			if(!empty($qualite1)){
				$sql.=" age_qualite_1 ='" . $qualite1 . "',";
			}
			if(!empty($qualite2)){
				$sql.=" age_qualite_2 ='" . $qualite2 . "',";
			}
			$sql=substr($sql,0,-1);
			$sql.=" WHERE age_id=" . $age_id . ";";
			try{
				$req = $Conn->query($sql);
			}Catch(Exception $e){
				$warning=$sql;
				$warning.="B Certains logos n'ont pas été chargés.";
			}
		}
	}
	
	if(!empty($erreur)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur 
		);
		header('Location: agence.php?id=' . $age_id);
	}else{
		if(!empty($warning)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "warning",
				"message" => $warning
			);
		}else{
			$_SESSION['message'][] = array(
				"titre" => "Enregistrement terminé",
				"type" => "success",
				"message" => "Le formulaire a bien été enregistré." 
			);
		}
		header('Location: agence_liste.php');
	}
 ?>
