<?php
	include "includes/controle_acces.inc.php";
	
	include_once("includes/connexion.php");
	include_once("includes/connexion_soc.php");

	include 'modeles/mod_parametre.php';
	include 'modeles/mod_utilisateur.php';
	include 'modeles/mod_acces.php';
	include 'modeles/mod_droit.php';

	// ENREGISTREMENT D'UN NOUVELLE INTERVENANT = LIGNE DE PLANNING

	// PERSONNE CONNECTE
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	$com_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$com_agence=$_SESSION['acces']["acc_agence"];	
	}
	$acc_profil=0;
	if(isset($_SESSION['acces']["acc_profil"])){
		$acc_profil=$_SESSION['acces']["acc_profil"];	
	}

	// TRAITEMENT DES PARAMETRES

	$erreur=0;

	$com_type=0;
	if(isset($_POST["com_type"])){
		$com_type=$_POST["com_type"];
	}

	if(!empty($_POST["utilisateur_add"])){
		if(empty($_POST["uti_nom"])){
			$erreur=1;	
		}else{
			$com_label_1=$_POST["uti_nom"];
		}	
		if(empty($_POST["uti_prenom"])){
			$erreur=1;	
		}else{
			$com_label_2=$_POST["uti_prenom"];
		}
	}
		

	if($erreur==0){
		switch ($com_type){
			
			case "1":	
				// FORMATEUR INTERNE
				
				$com_ref_2=0;
			
				if(!empty($_POST["utilisateur"])){
					
						// UTILISATEUR EXISTANT
					
					$com_ref_1=$_POST["utilisateur"];

					$sql="SELECT uti_nom,uti_prenom,uti_agence FROM Utilisateurs WHERE uti_id=:utilisateur;";
					$req=$Conn->prepare($sql);
					$req->bindParam("utilisateur",$com_ref_1);
					$req->execute();
					$info_uti=$req->fetch();
					if(!empty($info_uti)){
						$com_agence=$info_uti["uti_agence"];
						$com_label_1=$info_uti["uti_nom"];
						$com_label_2=$info_uti["uti_prenom"];
					}
					
				}elseif(!empty($_POST["utilisateur_add"])){
					
					// AJOUT d'UN UTILISATEUR
					
					// controle pour eviter un doublon
					$sql="SELECT uti_id FROM Utilisateurs WHERE uti_nom LIKE :nom AND uti_prenom LIKE :prenom;";
					$req=$Conn->prepare($sql);
					$req->bindParam("nom",$com_label_1);
					$req->bindParam("prenom",$com_label_2);
					$req->execute();
					$doublon=$req->fetch();
					if(!empty($doublon)){
						$erreur=980;
					};
					
					if($erreur==0){
					
						$uti_titre=0;
						if(!empty($_POST["uti_titre"])){
							$uti_titre=$_POST["uti_titre"];	
						}
						$uti_profil=3;
						
						// le connecte à accès à l'ensemble de la société, il a précisé l'agence de le form de création
						if(isset($_POST["uti_agence"])){
							$com_agence=$_POST["uti_agence"];
						}
						
						$uti_responsable=0;
						$sql="SELECT uti_id FROM Utilisateurs WHERE uti_societe=:societe AND uti_agence=:agence AND uti_profil=5;";
						$req=$Conn->prepare($sql);
						$req->bindParam("societe",$acc_societe);
						$req->bindParam("agence",$com_agence);
						$req->execute();
						$responsable=$req->fetch();
						if(!empty($responsable)){
							$uti_responsable=$responsable["uti_id"];	
						};

						$com_ref_1=insert_utilisateur($uti_titre,$com_label_1,$com_label_2,$_POST["uti_ad1"],$_POST["uti_ad2"],$_POST["uti_ad3"],$_POST["uti_cp"],$_POST["uti_ville"],"","","",$_POST["uti_tel"],$_POST["uti_fax"],$_POST["uti_mobile"],$_POST["uti_mail"],$acc_societe,$com_agence,$uti_profil,0,"Formateur",$uti_responsable);

						// par defaut on affecte l'acces à la société auquel l'utilisateur est rattaché.
					
						insert_acces_societe($com_ref_1,$acc_societe,$com_agence);
					
						// on lui affect les droits par defaut lié à son profil
					
						$droit_par_defaut=get_liste_droit_profil($uti_profil);
						if(!empty($droit_par_defaut)){
							
							foreach($droit_par_defaut as $d){
								
								if(!empty($d["udr_droit"])){
									insert_droit_utilisateur($d["udr_droit"],$com_ref_1,$d["udr_reaffecte"]);
								}
							}
						}
						
						IF($acc_profil!=7 AND $acc_profil!=12){
							
							// CREATION HORS SERVICE RH
							
							$sql="SELECT uti_id FROM utilisateurs WHERE uti_profil IN (7,12) AND NOT uti_archive;";
							$req=$Conn->query($sql);
							$rh=$req->fetchAll();
							if(!empty($rh)){
								
								$not_icone="<i class='fa fa-user' ></i>";
								$not_texte="Le compte ". $com_label_1 . " " . $com_label_2 . " a été crée";
								$not_classe="bg-success";
								$not_url="utilisateur_voir.php?utilisateur=" . $com_ref_1;
								
								$sql="INSERT INTO Notifications (not_utilisateur,not_icone,not_texte,not_vu,not_classe,not_url,not_date)";
								$sql.=" VALUE (:not_utilisateur, :not_icone, :not_texte, false, :not_classe, :not_url, NOW())";
								$req = $Conn->prepare($sql);
								foreach($rh as $r){
									$req ->bindParam(":not_utilisateur",$r["uti_id"]);
									$req ->bindParam(":not_icone",$not_icone);
									$req ->bindParam(":not_texte",$not_texte);
									$req ->bindParam(":not_classe",$not_classe);
									$req ->bindParam(":not_url",$not_url);
									$req->execute();
								}
							}
						}
					}
				}else{
					$erreur=1;
				}
				
				break;
				
			case "2": 
				
				// REVENDEUR
				
				if(!empty($_POST["revendeur_label_1"])){
					$com_label_1=$_POST["revendeur_label_1"];
				}else{
					$erreur=0;
				}
				$com_agence=0;
				if(!empty($_POST["revendeur_agence"])){
					$com_agence=$_POST["revendeur_agence"];
				}
				
			case "3": 
				
				// INDIVI
				
				if(!empty($_POST["indivi_label_1"])){
					$com_label_1=$_POST["indivi_label_1"];
				}else{
					$erreur=0;
				}
				$com_agence=0;
				if(!empty($_POST["indivi_agence"])){
					$com_agence=$_POST["indivi_agence"];
				}
				
				
			case "0":
		
				$$erreur==1;
				break;
		}
		
		// FIN GESTION Type
		
		// ON AJOUTE LE COMMERCIAL
		
		if($erreur==0){
			
			$sql="INSERT INTO Commerciaux (com_type,com_ref_1,com_ref_2,com_agence,com_label_1,com_label_2)";
			$sql.=" VALUE (:com_type, :com_ref_1, :com_ref_2, :com_agence, :com_label_1, :com_label_2)";
			$req = $ConnSoc->prepare($sql);
			$req ->bindParam(":com_type",$com_type);
			$req ->bindParam(":com_ref_1",$com_ref_1);
			$req ->bindParam(":com_ref_2",$com_ref_2);
			$req ->bindParam(":com_agence",$com_agence);
			$req ->bindParam(":com_label_1",$com_label_1);
			$req ->bindParam(":com_label_2",$com_label_2);
			$req->execute();
			$com_id=$ConnSoc->lastInsertId();

		}

	}
	header('Location: commercial_liste.php?erreur=' . $erreur);
?>