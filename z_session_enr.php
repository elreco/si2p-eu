<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "modeles/mod_upload.php";
	include "modeles/mod_parametre.php";

	$erreur=0;

	if(isset($_POST)){
		
		$ses_date="";
		if(!empty($_POST['ses_date'])){
			$ses_date = convert_date_sql($_POST['ses_date']);
		}
		
		$ses_demi=0;
		if(!empty($_POST['ses_demi'])){
			$ses_demi = intval($_POST['ses_demi']);
		}
		
		$ses_produit=0;
		if(!empty($_POST['ses_produit'])){
			$ses_produit = intval($_POST['ses_produit']);
		}
		
		$ses_lieu =$_POST['ses_lieu'];
		
	}
	
	if(empty($ses_date) OR empty($ses_demi) OR empty($ses_produit) OR empty($ses_lieu)){
		$erreur=1;

	}else{
		
		$genere_attest=false;
	
		$ses_id=0;
		if(!empty($_POST['ses_id'])){
			$ses_id = intval($_POST['ses_id']);
		}
		
		$nbH=0;
		if(!empty($_POST['nbH'])){
			$nbH = intval($_POST['nbH']);
		}
		
		// LIBELLE DE FORMATION

		$req = $Conn->query("SELECT cpr_libelle FROM Cnrs_Produits WHERE cpr_id=" . $ses_produit . ";");
		$d_produit=$req->fetch();
		
		if($ses_id==0){
			
			// NOUVELLE FORMTION
			
			$req = $Conn->prepare("INSERT INTO sessions (ses_date, ses_demi, ses_groupe, ses_lieu,ses_formation,ses_produit) 
			VALUES (:ses_date, :ses_demi, :ses_groupe, :ses_lieu, :ses_formation, :ses_produit)");
			$req->bindParam(':ses_date', $ses_date);
			$req->bindParam(':ses_demi', $ses_demi);
			//$req->bindParam(':ses_h_deb', $_POST['ses_h_deb']);
			//$req->bindParam(':ses_h_fin', $_POST['ses_h_fin']);
			$req->bindParam(':ses_groupe', $_SESSION['acces']['acc_holding']);
			$req->bindParam(':ses_lieu', $ses_lieu);
			$req->bindParam(':ses_formation',$d_produit["cpr_libelle"]);
			$req->bindParam(':ses_produit', $ses_produit);
			$req->execute();
			$ses_id = $Conn->lastInsertId();

		}else{
			
			$req = $Conn->query("SELECT ses_id FROM Sessions WHERE ses_id=" . $ses_id . "
			AND (NOT ses_lieu='" . $ses_lieu . "' OR NOT ses_date='" . $ses_date . "');");
			$d_session=$req->fetch();
			if(!empty($genere_attest)){
				$genere_attest=true;
			}

			// UPDATE FORMATION
			
			$req = $Conn->prepare("UPDATE sessions SET ses_date = :ses_date, ses_demi = :ses_demi, ses_groupe=:ses_groupe, 
			ses_lieu = :ses_lieu, ses_formation=:ses_formation, ses_produit=:ses_produit WHERE ses_id = :ses_id");
			$req->bindParam(':ses_date', $ses_date);
			$req->bindParam(':ses_demi', $ses_demi);
			//$req->bindParam(':ses_h_deb', $_POST['ses_h_deb']);
			//$req->bindParam(':ses_h_fin', $_POST['ses_h_fin']);
			$req->bindParam(':ses_groupe', $_SESSION['acces']['acc_holding']);
			$req->bindParam(':ses_lieu', $ses_lieu);
			$req->bindParam(':ses_formation', $d_produit['cpr_libelle']);
			$req->bindParam(':ses_produit', $ses_produit);
			$req->bindParam(':ses_id', $ses_id);
			$req->execute();
			
		}
		
		// LES SESSIONS DE FORMATION = HORAIRE
		
		for($k=1;$k<=$nbH;$k++){
			
			$sho_id=0;
			if(!empty($_POST['sho_id_' . $k])){
				$sho_id = intval($_POST['sho_id_' . $k]);
			}
			
			if($sho_id==0 AND !empty($_POST['sho_h_deb_' . $k]) AND !empty($_POST['sho_h_fin_' . $k])){
				
				// NOUVELLE SESSION
				$req = $Conn->prepare("INSERT INTO Sessions_Horaires (sho_session, sho_h_deb, sho_h_fin) 
				VALUES (:sho_session, :sho_h_deb, :sho_h_fin)");
				$req->bindParam(':sho_session', $ses_id);
				$req->bindParam(':sho_h_deb', $_POST['sho_h_deb_' . $k]);
				$req->bindParam(':sho_h_fin', $_POST['sho_h_fin_' . $k]);
				$req->execute();
			
			}elseif($sho_id>0){
				
				if(!empty($_POST['sho_h_deb_' . $k]) AND !empty($_POST['sho_h_fin_' . $k])){
					
					// UPDATE SESSION
					
					$req = $Conn->prepare("UPDATE Sessions_Horaires SET sho_h_deb = :sho_h_deb, sho_h_fin = :sho_h_fin
					WHERE sho_id = :sho_id");
					$req->bindParam(':sho_h_deb', $_POST['sho_h_deb_' . $k]);
					$req->bindParam(':sho_h_fin', $_POST['sho_h_fin_' . $k]);
					$req->bindParam(':sho_id', $sho_id);
					$req->execute();
				
				}else{
					
					// SUPPRESSION DE SESSION
					
					// delete inscription stagiaire
					
					$req = $Conn->prepare("DELETE FROM sessions_stagiaires WHERE sst_session=:sst_session AND sst_horaire=:sst_horaire;");
					$req->bindParam(':sst_session', $ses_id);
					$req->bindParam(':sst_horaire', $sho_id);
					$req->execute();
					
					// delete session
					$req = $Conn->prepare("DELETE FROM Sessions_Horaires WHERE sho_id=:sho_id;");
					$req->bindParam(':sho_id', $sho_id);
					$req->execute();
					
					// SI SUPP SESSION ->SUPP STA -> SUPP ATTEST
					
					$genere_attest=true;
				}
			}
			
		}
		
		
		// FIN GESTION DES HORAIRES

		if(!empty($_FILES['file_presence'])){
			
			$nom = pathinfo($_FILES['file_presence']['name']); // nom du document (infos)
			// construction du nom du  document
			// upload $erreur != 0 si erreur
			$err_upload = upload('file_presence', 'sessions_stagiaires/',$ses_id, 0, 0, 1);
			
			if($err_upload==0){
				$req = $Conn->prepare("UPDATE sessions SET ses_ext = :ses_ext WHERE ses_id = " . $ses_id);
				$req->bindParam(':ses_ext', $nom['extension']);
				$req->execute();
			}
		}
		
		/*
		LES ATTESTATIONS SONT GENEREE AUTOMATIQUEMENT
		$err_upload=0;
		if(!empty($_FILES['file_attestation'])){
			
			$nom = pathinfo($_FILES['file_attestation']['name']); // nom du document (infos)
			// construction du nom du  document
			// upload $erreur != 0 si erreur
			$err_upload = upload('file_attestation', 'sessions_stagiaires/',"attestation_conso_" . $ses_id, 0, 0, 1);
			
			if($err_upload==0){
				$req = $Conn->prepare("UPDATE sessions SET ses_ext_attest = :ses_ext WHERE ses_id = " . $ses_id);
				$req->bindParam(':ses_ext', $nom['extension']);
				$req->execute();
			}
		}*/
		
		IF($genere_attest){
			
			// LE MODIF DE LA SESSION ENGENDRE UNE REGENERATION DES ATTESTATIONS
			
			$files = glob($_SERVER['DOCUMENT_ROOT'] . '/documents/attestations/' . $ses_id . '/*'); // get all file names
			foreach($files as $file){ // iterate files
			  if(is_file($file))
				unlink($file); // delete file
			}
		
			
			$req = $Conn->prepare("SELECT * FROM sessions WHERE ses_id = " . $ses_id . ";");
			$req->execute();
			$attestation_session = $req->fetch();
			
			$req = $Conn->query("SELECT sta_id, sta_nom, sta_prenom, sta_titre 
			FROM sessions_stagiaires LEFT JOIN CNRS_Stagiaires ON (sessions_stagiaires.sst_stagiaire = CNRS_Stagiaires.sta_id)
			WHERE sst_session = " . $ses_id . ";");
			$req->execute();
			$d_stagiaires = $req->fetchAll();
			if(!empty($d_stagiaires)){
				
				setlocale(LC_TIME, "fr");
				date_default_timezone_set('Europe/Paris');
				
				foreach($d_stagiaires as $sta){
					
					$attestation_stagiaires=array(
						0 => $sta
					);

					include("modeles/mod_attestation_pdf.php");

				}
				
				if(count($d_stagiaires)>1){
					
					$attestation_stagiaires=$d_stagiaires;
					
					include("modeles/mod_attestation_pdf.php");
				}
			
			}
			
		}
	}
	if($erreur==0){
		
		if($ses_id>0){
			header("location : session_stagiaire.php?id=" . $ses_id);
		}else{
			header("location : session_liste.php");
		}
		
	}else{
		echo("Erreur : impossible d'enregistrer les modifications.");
		die();
	}
?>