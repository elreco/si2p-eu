<?php

// MISE à JOUR D'UN client

include "includes/controle_acces.inc.php";
include "includes/connexion.php";
include "includes/connexion_soc.php";
include "includes/connexion_fct.php";
require("modeles/mod_check_siret.php");
require("modeles/mod_get_groupe_filiales.php");
/*require("modeles/mod_get_commercial_uti.php");

require("modeles/mod_client.php");*/

$erreur="";
$warning_txt=""; // permet d'informer l'U que certaines valeurs n'ont pas été enregistrées sans bloquer toute la maj de la fiche


$client=0;
if(!empty($_POST["client"])){
	$client=intval($_POST["client"]);
}

if($client==0){
	$_SESSION['message'][] = array(
		"titre" => "Attention",
		"type" => "danger",
		"message" => "Paramètre absent"
	);
	Header("Location: client_verif_siret.php?client=" . $client);
	die();
}


	// PERSONNE CONNECTE

	// l'utilisateur
	$acc_utilisateur=0;
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}

	// la société
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	$acc_agence = 0;
	if(!empty($_SESSION['acces']['acc_agence'])){
		$acc_agence = $_SESSION['acces']['acc_agence'];
	}

	// agence pour maj Clients Societes

	$tab_agence=array();

	$sql="SELECT age_id FROM Agences WHERE age_societe=" . $acc_societe . ";";
	$req=$Conn->query($sql);
	$result=$req->fetchAll();
	if(!empty($result)){
		foreach($result as $r){
			$tab_agence[]=array(
				"agence" => $r["age_id"],
				"commercial" => 0,
				"utilisateur" => 0,
				"archive" => 0,
				"update" => false
			);
		}
	}else{

		$tab_agence[]=array(
			"agence" => 0,
			"commercial" => 0,
			"utilisateur" => 0,
			"archive" => 0,
			"update" => 0
		);
	}

	// client avant modif

	$aff_restreint=false;
	$edit_categorie=true;
	$sql="SELECT cli_code,cli_nom,cli_filiale_de,cli_niveau,cli_fil_de,cli_categorie,cli_sous_categorie,cli_groupe,cli_contrat,cca_gc,cli_blackliste,cli_stagiaire
	,cli_affacturable,cli_affacturable_txt,cli_affacturage,cli_affacturage_iban,cli_fac_groupe,cli_interco_soc,cli_interco_age,cli_blackliste,cli_opca,cli_facture_opca
	,cli_prescripteur,cli_prescripteur_info
	FROM Clients,Clients_Categories WHERE cli_categorie=cca_id AND cli_id=" . $client . ";";
	$req=$Conn->query($sql);
	$d_client=$req->fetch();
	// adresse de facturation avant modif

	$adr_fac=0;
	if(!empty($_POST['adresse'])){
		$adr_id = $_POST['adresse'];
		$sql="SELECT adr_id,adr_type,adr_siret, adr_nom FROM Adresses WHERE adr_id = " . $_POST['adresse'];
	}else{
		$sql="SELECT adr_id,adr_type,adr_siret FROM Adresses WHERE adr_ref=1 AND adr_ref_id=" . $client . " AND adr_type=2 AND adr_defaut;";
	}
	$req=$Conn->query($sql);
	$d_ad_fac_def=$req->fetch();
	if(!empty($d_ad_fac_def)){
		$adr_fac=$d_ad_fac_def["adr_id"];
	}

    $adr_int=0;

	$sql="SELECT adr_id,adr_type,adr_siret, adr_nom FROM Adresses WHERE adr_ref=1 AND adr_ref_id=" . $client . " AND adr_type=1 AND adr_defaut;";
	$req=$Conn->query($sql);
	$d_ad_int_def=$req->fetch();
	if(!empty($d_ad_int_def)){
		$adr_int=$d_ad_int_def["adr_id"];
	}
	/******************************************
		TRAITEMENT DU FORM
	*******************************************/


	$cli_code=$d_client["cli_code"];
	if(!empty($_POST["cli_nom"])){
		$cli_nom=$_POST["cli_nom"];
	}else{
		$cli_nom=$d_client["cli_nom"];
	}

	$cli_categorie=$d_client["cli_categorie"];
	$cli_sous_categorie=$d_client["cli_sous_categorie"];
	$cli_contrat=$d_client["cli_contrat"];
	$cli_groupe=$d_client["cli_groupe"];
	$cli_filiale_de=$d_client["cli_filiale_de"];
	$cli_niveau=$d_client["cli_niveau"];
	$cli_fil_de=$d_client["cli_fil_de"];
	$cli_fac_groupe=$d_client["cli_fac_groupe"];

	// SIREN / SIRET
	$cli_siren="";
	$adr_siret="";
    if(!empty($_POST["cli_siret"])){
        $_POST["cli_siren"] = substr($_POST["cli_siret"], 0, 11);
        $_POST["adr_siret"] = substr($_POST["cli_siret"], 12, 6);
    }
	if(!empty($_POST["cli_siren"])){
		if(strlen($_POST["cli_siren"])==11){
			$cli_siren=$_POST["cli_siren"];
			if(!empty($_POST["adr_siret"])){
				if(strlen($_POST["adr_siret"])==6){
					$adr_siret=$_POST["adr_siret"];
				}
			}
		}
	}
	$cli_ape=0;
	if(!empty($_POST["cli_ape"])){
		$cli_ape=intval($_POST["cli_ape"]);
	}

	$cli_ident_tva="";
	if(!empty($_POST["cli_ident_tva"])){
		$cli_ident_tva=$_POST["cli_ident_tva"];
	}

	$adr_nom=$_POST["cli_adr_nom"];
	$adr_ad1=$_POST["cli_adr_ad1"];
    $adr_ad2=$_POST["cli_adr_ad2"];
	$adr_cp=$_POST["cli_adr_cp"];
	$adr_ville=$_POST["cli_adr_ville"];

	$adr_geo_int=1;
	if(empty($adr_id)){
		$adr_nom_int=$_POST["cli_adr_nom_int"];
		$adr_ad1_int=$_POST["cli_adr_ad1_int"];
		$adr_ad2_int=$_POST["cli_adr_ad2_int"];
		$adr_cp_int=$_POST["cli_adr_cp_int"];
		$adr_ville_int=$_POST["cli_adr_ville_int"];
	}


	// adresse de facturation

	$adr_geo_fac=1;

	// FIN TRAITEMENT FORM
	//********************************************


	/******************************************
		CONTROLE
	*******************************************/
		// controle autre que particulier

	/* fin de controle
	***********************************************/


	// ON TRAITE D'ABORD LES ENREGISTREMENT ANNEXE POUR METTRE A JOUR LA FICHE

	// ADRESSES D'INTERVENTION

	if(empty($adr_id)){

		// AUTRE QUE PARTICULIER

		if($adr_int>0){

			// MAJ

			$sql="UPDATE adresses SET
			adr_nom = :adr_nom,
			adr_ad1=:adr_ad1,
            adr_ad2=:adr_ad2,
			adr_ad3='',
			adr_cp=:adr_cp,
			adr_ville=:adr_ville
			WHERE adr_id=:adresse";
			$req=$Conn->prepare($sql);
			$req->bindParam(":adr_nom",$adr_nom_int);
			$req->bindParam(":adr_ad1",$adr_ad1_int);
            $req->bindParam(":adr_ad2",$adr_ad2_int);
			$req->bindParam(":adr_cp",$adr_cp_int);
			$req->bindParam(":adr_ville",$adr_ville_int);
			$req->bindParam(":adresse",$adr_int);
			try{
				$req->execute();
			}catch(Exception $e){
				$erreur="E - " . $e->getMessage();
			}
		}else{
			// ADD

			$sql="INSERT INTO adresses
			(adr_ref,adr_ref_id,adr_type,adr_nom,adr_ad1,adr_ad2,adr_cp,adr_ville,adr_libelle,adr_geo,adr_defaut)
			VALUES (1,:adr_ref_id,1,:adr_nom,:adr_ad1,:adr_ad2,:adr_cp,:adr_ville,:adr_libelle,:adr_geo,1);";
			$req=$Conn->prepare($sql);
			$req->bindParam(":adr_ref_id",$client);
			$req->bindParam(":adr_nom",$adr_nom_int);
			$req->bindParam(":adr_ad1",$adr_ad1_int);
            $req->bindParam(":adr_ad2",$adr_ad2_int);
			$req->bindParam(":adr_cp",$adr_cp_int);
			$req->bindParam(":adr_ville",$adr_ville_int);
			$req->bindParam(":adr_libelle",$adr_nom);
			$req->bindParam(":adr_geo",$adr_geo_int);
			try{
				$req->execute();
				$adr_int=$Conn->lastInsertId();
			}catch(Exception $e){
				$erreur="B - " . $e->getMessage();
			}
		}

		// on memorise l'adresse d'intervention sur la fiche client

		$cli_adresse=$adr_int;
		$cli_adr_nom=$adr_nom;
		$cli_adr_ad1=$adr_ad1;
        $cli_adr_ad2=$adr_ad2;
		$cli_adr_cp=$adr_cp;
		$cli_adr_ville=$adr_ville;

	}

	// ADRESSE DE FACTURATION

	if(empty($erreur)){

		if($adr_fac>0){

			// MAJ

			$sql="UPDATE adresses SET
			adr_nom=:adr_nom,
			adr_ad1=:adr_ad1,
            adr_ad2=:adr_ad2,
			adr_ad3='',
			adr_cp=:adr_cp,
			adr_ville=:adr_ville
			WHERE adr_id=:adresse";
			$req=$Conn->prepare($sql);
			$req->bindParam(":adr_nom",$adr_nom);
			$req->bindParam(":adr_ad1",$adr_ad1);
            $req->bindParam(":adr_ad2",$adr_ad2);
			$req->bindParam(":adr_cp",$adr_cp);
			$req->bindParam(":adr_ville",$adr_ville);
			$req->bindParam(":adresse",$adr_fac);
			try{
				$req->execute();
			}catch(Exception $e){
				$erreur="D - " . $e->getMessage();
			}

		}else{

			// ADD

			$sqla="INSERT INTO adresses
			(adr_ref,adr_ref_id,adr_type,adr_nom,adr_ad1,adr_ad2,adr_cp,adr_ville,adr_libelle,adr_geo,adr_siret,adr_defaut)
			VALUES (1,:adr_ref_id,2,:adr_nom,:adr_ad1,:adr_ad2,:adr_cp,:adr_ville,:adr_libelle,:adr_geo,:adr_siret,1);";
			$req=$Conn->prepare($sqla);
			$req->bindParam(":adr_ref_id",$client);
			$req->bindParam(":adr_nom",$adr_nom);
			$req->bindParam(":adr_ad1",$adr_ad1);
            $req->bindParam(":adr_ad2",$adr_ad2);
			$req->bindParam(":adr_cp",$adr_cp);
			$req->bindParam(":adr_ville",$adr_ville);
			$req->bindParam(":adr_libelle",$adr_nom);
			$req->bindParam(":adr_geo",$adr_geo_fac);
			$req->bindParam(":adr_siret",$adr_siret);
			try{
				$req->execute();
				$adr_fac = $Conn->lastInsertId();
			}catch(Exception $e){
				$erreur="G - " . $e->getMessage();
				echo($erreur);
				die();
			}
		}

	}


	// MISE A JOUR DE LA FICHE

	if(empty($erreur) && $adr_int > 0 && empty($adr_id)){
		$sql="UPDATE clients SET
		cli_nom = :cli_nom,
		cli_adr_nom = :cli_adr_nom,
		cli_adr_ad1 = :cli_adr_ad1,
        cli_adr_ad2 = :cli_adr_ad2,
		cli_adr_ad3 = '',
		cli_adr_cp = :cli_adr_cp,
		cli_adr_ville = :cli_adr_ville,
		cli_siren = :cli_siren,
		cli_ident_tva = :cli_ident_tva,
		cli_ape = :cli_ape
		WHERE cli_id = :cli_id";

		$req = $Conn->prepare($sql);
		$req->bindParam("cli_nom",$cli_nom);

		// adresse d'intervention par défaut
		$req->bindParam("cli_adr_nom",$adr_nom_int);
		$req->bindParam("cli_adr_ad1",$adr_ad1_int);
        $req->bindParam("cli_adr_ad2",$adr_ad2_int);
		$req->bindParam("cli_adr_cp",$adr_cp_int);
		$req->bindParam("cli_adr_ville",$adr_ville_int);
		$req->bindParam("cli_ident_tva",$cli_ident_tva);
		// contact par defaut

		$req->bindParam("cli_siren",$cli_siren);
		// autre info
		$req->bindParam("cli_ape",$cli_ape);


		$req->bindParam("cli_id",$client);
		$req->execute();
		try{
			$req->execute();
		}catch(Exception $e){
			$erreur="C-" . $e->getMessage();
		}
	}


	// MAJ DES BASE N

	$soc_n=0;
	$sql="SELECT * FROM Clients_Societes WHERE cso_client=:cso_client ORDER BY cso_societe;";
	$req=$Conn->prepare($sql);
	$req->bindValue("cso_client",$client);
	$req->execute();
	$d_client_societe=$req->fetchAll();
	if(!empty($d_client_societe) && $adr_int > 0){
		foreach($d_client_societe as $d_cli_soc){

			if($d_cli_soc["cso_societe"]!=$soc_n){

				$ConnFct=connexion_fct($d_cli_soc["cso_societe"]);
				$soc_n=$d_cli_soc["cso_societe"];

				$req_up_n=$ConnFct->prepare("UPDATE Clients SET cli_nom=:cli_nom, cli_cp=:cli_cp
				WHERE cli_id=:cli_id AND cli_agence = :cli_agence");
			}

			// SI EXISTE ON UPDATE
			if(empty($adr_id)){
				$req_up_n->bindValue("cli_nom",$cli_nom);
				$req_up_n->bindValue("cli_agence",$d_cli_soc["cso_agence"]);
				$req_up_n->bindValue("cli_cp",$cli_adr_cp);
				$req_up_n->bindValue("cli_id",$client);

				try{
					$req_up_n->execute();
				}Catch(Exception $e){
					var_dump($e->getMessage());
					die();
				}
			}
		}
	}



	if(!empty($erreur)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur
		);
        header("Location: client_verif_siret.php?client=" . $client);
	}elseif(!empty($warning_txt)){

		$_SESSION['message'][] = array(
			"titre" => "Attention!",
			"type" => "warning",
			"message" => $warning_txt
		);
        header("Location: client_verif_siret.php?client=" . $client);
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Succès",
			"type" => "success",
			"message" => "Votre client a été actualisé"
		);
		if(!empty($_POST['onglet'])){
			header("Location: client_voir.php?client=" . $client ."&onglet=" . $_POST['onglet']);
		}else{
			header("Location: client_voir.php?client=" . $client);
		}

	}

?>
