<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "modeles/mod_utilisateur.php";
	include "modeles/mod_diplome.php";
	include "modeles/mod_parametre.php";


	$erreur=0;
	
	$ref=0;
	if(isset($_GET["ref"])){
		$ref=$_GET["ref"];
	}else{
		$erreur=1;
	}
	
	$ref_id=0;
	if(isset($_GET["ref_id"])){
		$ref_id=$_GET["ref_id"];
	}else{
		$erreur=1;
	}
	
	$competence=0;
	if(isset($_GET["uti_competence"])){
		$competence=$_GET["uti_competence"];
	}else{
		$erreur=1;
	}

	$fournisseur=0;
	if(isset($_GET["fournisseur"])){
		$fournisseur=$_GET["fournisseur"];
	}else{
		$erreur=1;
	}

	if($erreur>0){
		
		header("location : index.php");
		
	}else{ 
		if($ref==2){
			
			$sql="SELECT * FROM fournisseurs_intervenants WHERE fin_id = " . $ref_id;
	        $req=$Conn->query($sql);
	        $fournisseur_intervenant = $req->fetch();

	        $intervenant_nom = $fournisseur_intervenant['fin_prenom'] . " " . $fournisseur_intervenant['fin_nom'];

			// DIPLOMES REQUIS
			$sql="SELECT * FROM Diplomes_competences  
			LEFT JOIN Diplomes ON(diplomes.dip_id = diplomes_competences.dco_diplome) 
			 WHERE dco_competence = " . $competence . " AND dco_externe = 1";
			$req=$Conn->query($sql);
			$diplomes = $req->fetchAll();

			$sql="SELECT * FROM competences WHERE com_id = " . $competence;
			$req=$Conn->query($sql);
			$c = $req->fetch();

			$sql="SELECT * FROM intervenants_competences WHERE ico_competence = " . $competence . " AND ico_ref = " . $ref . " AND ico_ref_id = " . $ref_id;
			$req=$Conn->query($sql);
			$intervenant_competence = $req->fetch();
			
		}
		
		?>
		
		<!DOCTYPE html>
		<html>
		<head>
			<!-- Meta, title, CSS, favicons, etc. -->
			<meta charset="utf-8">
			<title>SI2P - Fournisseurs</title>
			<meta name="keywords" content=""/>
			<meta name="description" content="">
			<meta name="author" content="">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<!-- Theme CSS -->
			<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
			<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
			<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
			
			<!-- Favicon -->
			<link rel="shortcut icon" href="assets/img/favicon.png">

			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
			<![endif]-->

		</head>
		<body class="sb-top sb-top-sm ">
			
			<form method="post" action="fournisseur_intervenant_competence_enr.php" enctype="multipart/form-data" >
				<div>
					<input type="hidden" name="ref" value="<?=$ref?>" />
					<input type="hidden" name="ref_id" value="<?=$ref_id?>" />
					<input type="hidden" name="competence" value="<?=$competence?>" />
					<input type="hidden" name="fournisseur" value="<?=$fournisseur?>" />
				</div>
				<!-- Start: Main -->
				<div id="main">
				
					<?php
					include "includes/header_def.inc.php";
					?>


					<!-- Start: Content-Wrapper -->
					<section id="content_wrapper" class="">
					
						<section id="content" class="animated fadeIn">
							<div class="row">
								<div class="col-md-12">
									<div class="admin-form theme-primary ">								
										<div class="panel heading-border panel-primary">
											
											<div class="panel-body bg-light">
											
												<div class="text-center">

													<div class="content-header">
														<h2><?=$intervenant_nom?></h2>
														<h2>Compétence <b class="text-primary"><?=$c["com_libelle"]?></b></h2>
													</div>
												</div>
												
												<div class="row">                      
													<div class="col-md-12">
														<div class="section">
															<p>Pour valider cette compétence, il faut charger les diplômes suivants : </p>
															<ul>
																<?php foreach($diplomes as $d){ ?>
																	<li>
																	<?php if($d['dco_obligatoire'] == 1){ ?>
																		<strong>
																	<?php } ?>
																	<?= $d['dip_libelle'] ?> 
																	<?php if($d['dco_obligatoire'] == 1){ ?>
																		</strong>
																	<?php } ?>
																	<?php if($d['dco_obligatoire'] == 0){ ?>
																		(Facultatif)
																	<?php } ?>
																	</li>
																<?php } ?>
															</ul>
															
														</div>
													</div>
												</div>
												<?php if($_SESSION["acces"]["acc_droits"][4]==1){ ?>
												<?php if((!empty($intervenant_competence) && $intervenant_competence['ico_valide'] == 1) OR empty($intervenant_competence)){ ?>
												<div class="row">	
													<div class="col-md-12 text-center">	
														<label class="option option-success">
															<input type="checkbox" name="ico_valide" value="1" 
															<?php if(!empty($intervenant_competence) && $intervenant_competence['ico_valide'] == 1){ ?>
																checked
															<?php } ?>
															/>
															<span class="checkbox"></span>Forcer la validation de cette compétence
														</label>
													</div>
												</div>
												<?php } ?>
												<?php } ?>	
												<?php if(!empty($intervenant_competence) && $intervenant_competence['ico_valide'] == 0){ ?>
												<div class="row text-center">
													<div class="col-md-6 col-md-offset-3">
														<div class="alert alert-success">
															Cette compétence est validée
														</div>
													</div>
												</div>
												<?php } ?>												
											</div>
										</div>
									</div>
								</div>
							</div>					
						</section>
					</section>
				</div>
				<footer id="content-footer" class="affix">
					<div class="row">
						<div class="col-xs-3 footer-left">
							<a href="fournisseur_voir.php?fournisseur=<?= $_GET['fournisseur'] ?>&intervenant=<?= $ref_id ?>&tab=5" class="btn btn-default btn-sm">
								<i class="fa fa-long-arrow-left"></i>
								Retour
							</a>
						</div>
						<div class="col-xs-6 footer-middle"></div>
						<div class="col-xs-3 footer-right">
						<?php if((!empty($intervenant_competence) && $intervenant_competence['ico_valide'] == 1) OR empty($intervenant_competence)){ ?>
							<button class="btn btn-success btn-sm" >
								 <i class='fa fa-floppy-o'></i> Enregistrer
							</button>
						<?php } ?>
							
						</div>
					</div>
				</footer>
			</form>
		<?php
			include "includes/footer_script.inc.php"; ?>	
			<!-- jQuery -->
			<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>

			<script type="text/javascript">

				jQuery(document).ready(function () {
						

				})		
				</script>
			</body>
		</html>
<?php	
	} ?>
