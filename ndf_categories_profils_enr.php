<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');

	if(!empty($_POST)){
		$req = $Conn->prepare("SELECT * FROM ndf_categories_profils WHERE ncp_profil = :ncp_profil AND ncp_type = :ncp_type AND ncp_categorie = :ncp_categorie");
        $req->bindParam(':ncp_profil', $_POST['profil']);
        $req->bindParam(':ncp_type', $_POST['type']);
        $req->bindParam(':ncp_categorie', $_POST['categorie']);
        $req->execute();
        $ndf_categories_profils = $req->fetch();

        if(!empty($_POST['ncp_acces'])){
        	$ncp_acces = 1;
        }else{
        	$ncp_acces = 0;
        }

        if(empty($_POST['ncp_plafond_1'])){
        	$_POST['ncp_plafond_1'] = NULL;
        }
        if(empty($_POST['ncp_plafond_2'])){
        	$_POST['ncp_plafond_2'] = NULL;
        }
        if(empty($ndf_categories_profils)){
        	$req = $Conn->prepare("INSERT INTO ndf_categories_profils (ncp_profil, ncp_type, ncp_categorie, ncp_acces, ncp_plafond_1, ncp_plafond_2) VALUES (:ncp_profil, :ncp_type, :ncp_categorie, :ncp_acces, :ncp_plafond_1, :ncp_plafond_2)");
	        $req->bindParam(':ncp_profil', $_POST['profil']);
	        $req->bindParam(':ncp_type', $_POST['type']);
	        $req->bindParam(':ncp_categorie', $_POST['categorie']);
	        $req->bindParam(':ncp_acces', $ncp_acces);
	        $req->bindParam(':ncp_plafond_1', $_POST['ncp_plafond_1']);
	        $req->bindParam(':ncp_plafond_2', $_POST['ncp_plafond_2']);

	        $req->execute();
        }else{
        	$req = $Conn->prepare("UPDATE ndf_categories_profils SET ncp_acces = :ncp_acces, ncp_plafond_1 = :ncp_plafond_1, ncp_plafond_2 = :ncp_plafond_2 WHERE ncp_profil = :ncp_profil AND ncp_type = :ncp_type AND ncp_categorie = :ncp_categorie");
	       $req->bindParam(':ncp_profil', $_POST['profil']);
	        $req->bindParam(':ncp_type', $_POST['type']);
	        $req->bindParam(':ncp_categorie', $_POST['categorie']);
	        $req->bindParam(':ncp_acces', $ncp_acces);
	        $req->bindParam(':ncp_plafond_1', $_POST['ncp_plafond_1']);
	        $req->bindParam(':ncp_plafond_2', $_POST['ncp_plafond_2']);
	        $req->execute();
        }
	}

Header("Location: ndf_categories_profils.php?profil=" . $_POST['profil']);
die();
?>