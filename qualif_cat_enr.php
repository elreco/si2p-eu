<?php
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';

// MAJ D'UNE CATEGORIE DE QUALIF

include "modeles/mod_qualification.php";

$erreur_txt="";

// CONTRAOLE ACCESS
if($_SESSION['acces']["acc_service"][3]!=1 AND $_SESSION['acces']["acc_profil"]!=13){ 
	$erreur_txt="Enregistrement impossible";
}else{
	
	if(!empty($_POST)){
	
		$qca_id=0;
		if(!empty($_POST["qca_id"])){
			$qca_id=intval($_POST["qca_id"]);	
		}
	
		$qca_qualification=0;
		if(!empty($_POST["qca_qualification"])){
			$qca_qualification=intval($_POST["qca_qualification"]);	
		}
		if( (empty($qca_qualification) AND empty($qca_id) ) OR empty($_POST["qca_libelle"])){
			$erreur_txt="Formulaire incomplet!";
		}
		
	}else{
		$erreur_txt="Formulaire incomplet!";
	}
}
if(empty($erreur_txt)){
	
	$qca_ut=0;
	if(!empty($_POST["qca_ut"])){
		$qca_ut=floatval($_POST["qca_ut"]);
	}
	$qca_competence=0;
	if(!empty($_POST["qca_competence"])){
		$qca_competence=intval($_POST["qca_competence"]);
	}
	
	// gestion des options
	
	$option=array();
	for($bcl=1;$bcl<=3;$bcl++){
		
		$qca_opt=0;
		$qca_opt_up=0;
		$qca_opt_comp=0;
		if(!empty($_POST["qca_opt_" . $bcl])){
			$qca_opt=1;
			
			if(!empty($_POST["qca_ut_opt_" . $bcl])){
				$qca_opt_up=floatval($_POST["qca_ut_opt_" . $bcl]);
			}
			if(!empty($_POST["qca_comp_opt_" . $bcl])){
				$qca_opt_comp=intval($_POST["qca_comp_opt_" . $bcl]);
			}
		}
		$option[$bcl]=array(
			"qca_opt" => $qca_opt,
			"qca_ut" => $qca_opt_up,
			"qca_competence" => $qca_opt_comp
		);
		
	}
	
	$qca_archive=0;
	if(!empty($_POST["qca_archive"])){
		$qca_archive=1;
	}
	
	
	
	if(!empty($qca_id)){
		
		// UPDATE
		
		$sql="UPDATE Qualifications_Categories SET
		qca_libelle=:qca_libelle,
		qca_nom=:qca_nom,
		qca_competence=:qca_competence,
		qca_ut=:qca_ut,
		qca_opt_1=:qca_opt_1,
		qca_ut_opt_1=:qca_ut_opt_1,
		qca_comp_opt_1=:qca_comp_opt_1,
		qca_opt_2=:qca_opt_2,
		qca_ut_opt_2=:qca_ut_opt_2,
		qca_comp_opt_2=:qca_comp_opt_2,
		qca_opt_3=:qca_opt_3,
		qca_ut_opt_3=:qca_ut_opt_3,
		qca_comp_opt_3=:qca_comp_opt_3,
		qca_archive=:qca_archive
		WHERE qca_id=:qca_id;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":qca_libelle",$_POST["qca_libelle"]);
		$req->bindParam(":qca_nom",$_POST["qca_nom"]);
		$req->bindParam(":qca_competence",$qca_competence);
		$req->bindParam(":qca_ut",$qca_ut);
		$req->bindParam(":qca_opt_1",$option[1]["qca_opt"]);
		$req->bindParam(":qca_ut_opt_1",$option[1]["qca_ut"]);
		$req->bindParam(":qca_comp_opt_1",$option[1]["qca_competence"]);
		$req->bindParam(":qca_opt_2",$option[2]["qca_opt"]);
		$req->bindParam(":qca_ut_opt_2",$option[2]["qca_ut"]);
		$req->bindParam(":qca_comp_opt_2",$option[2]["qca_competence"]);
		$req->bindParam(":qca_opt_3",$option[3]["qca_opt"]);
		$req->bindParam(":qca_ut_opt_3",$option[3]["qca_ut"]);
		$req->bindParam(":qca_comp_opt_3",$option[3]["qca_competence"]);
		$req->bindParam(":qca_archive",$qca_archive);
		$req->bindParam(":qca_id",$qca_id);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur_txt="UP : " . $e->getMessage();
		}
	}else{
		
		// AJOUT
		
		$sql="INSERT INTO Qualifications_Categories (qca_qualification,qca_libelle,qca_nom,qca_competence,qca_ut
		,qca_opt_1,qca_ut_opt_1,qca_comp_opt_1,qca_opt_2,qca_ut_opt_2,qca_comp_opt_2,qca_opt_3,qca_ut_opt_3,qca_comp_opt_3) 
		VALUES (:qca_qualification,:qca_libelle,:qca_nom,:qca_competence,:qca_ut
		,:qca_opt_1,:qca_ut_opt_1,:qca_comp_opt_1,:qca_opt_2,:qca_ut_opt_2,:qca_comp_opt_2,:qca_opt_3,:qca_ut_opt_3,:qca_comp_opt_3);";
		$req=$Conn->prepare($sql);
		$req->bindParam(":qca_qualification",$qca_qualification);
		$req->bindParam(":qca_libelle",$_POST["qca_libelle"]);
		$req->bindParam(":qca_nom",$_POST["qca_nom"]);
		$req->bindParam(":qca_competence",$qca_competence);
		$req->bindParam(":qca_ut",$qca_ut);
		$req->bindParam(":qca_opt_1",$option[1]["qca_opt"]);
		$req->bindParam(":qca_ut_opt_1",$option[1]["qca_ut"]);
		$req->bindParam(":qca_comp_opt_1",$option[1]["qca_competence"]);
		$req->bindParam(":qca_opt_2",$option[2]["qca_opt"]);
		$req->bindParam(":qca_ut_opt_2",$option[2]["qca_ut"]);
		$req->bindParam(":qca_comp_opt_2",$option[2]["qca_competence"]);
		$req->bindParam(":qca_opt_3",$option[3]["qca_opt"]);
		$req->bindParam(":qca_ut_opt_3",$option[3]["qca_ut"]);
		$req->bindParam(":qca_comp_opt_3",$option[3]["qca_competence"]);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur_txt="ADD : " . $e->getMessage();
		}
		
	}
}
if(!empty($erreur_txt)){
	$_SESSION['message'][] = array(
		"titre" => "Echec de l'enregistrement",
		"type" => "danger",
		"message" => $erreur_txt
	);
}else{
	$_SESSION['message'][] = array(
		"titre" => "Enregistrement terminé",
		"type" => "success",
		"message" => "Vos modifications ont bien a été enregistrées!" 
	);

}	
header("location : qualif_cat_liste.php");
die();
?>