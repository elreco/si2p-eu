<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	// initialisation des champs
	if(!empty($_POST['mod_menu'])){
		$mod_menu = $_POST['mod_menu'];
	}else{
		$mod_menu = 0;
	}
	// contrôle des champs
	if(!empty($_POST['mod_type'])){

		// récupérer les modèles de la personne connectée
		
		$req = $Conn->prepare("SELECT mod_id, mod_utilisateur,mod_type FROM Modeles WHERE mod_utilisateur = :mod_utilisateur AND mod_menu =:mod_menu");
		$req->bindParam("mod_utilisateur", $_SESSION['acces']['acc_ref_id']);
		$req->bindParam("mod_menu", $mod_menu);
		$req->execute();
		$modele = $req->fetch();
		// EDITION
		if(!empty($modele)){
			$req = $Conn->prepare("UPDATE Modeles SET mod_type=:mod_type, mod_fct_1=:mod_fct_1,mod_fct_2=:mod_fct_2,mod_fct_3=:mod_fct_3,mod_fct_4=:mod_fct_4,mod_fct_5=:mod_fct_5,mod_fct_6=:mod_fct_6 WHERE mod_id=:mod_id ");
			$req->bindParam("mod_id", $modele['mod_id']);
			$req->bindParam("mod_type", $_POST['mod_type']);
			$req->bindParam("mod_fct_1", $_POST['mod_fct_1']);
			$req->bindParam("mod_fct_2", $_POST['mod_fct_2']);
			$req->bindParam("mod_fct_3", $_POST['mod_fct_3']);
			$req->bindParam("mod_fct_4", $_POST['mod_fct_4']);
			$req->bindParam("mod_fct_5", $_POST['mod_fct_5']);
			$req->bindParam("mod_fct_6", $_POST['mod_fct_6']);
			$req->execute();

			$_SESSION['message'][] = array(
				"titre" => "Succès",
				"type" => "success",
				"message" => "Votre modèle a bien été actualisé"
			);
		}else{
		// CREATION
			$req = $Conn->prepare("INSERT INTO Modeles (mod_utilisateur, mod_type, mod_fct_1,mod_fct_2,mod_fct_3,mod_fct_4,mod_fct_5,mod_fct_6, mod_menu) VALUES (:mod_utilisateur, :mod_type, :mod_fct_1,:mod_fct_2,:mod_fct_3,:mod_fct_4,:mod_fct_5,:mod_fct_6, :mod_menu)");
			$req->bindParam("mod_utilisateur", $_SESSION['acces']['acc_ref_id']);
			$req->bindParam("mod_type", $_POST['mod_type']);
			$req->bindParam("mod_menu", $_POST['mod_menu']);
			$req->bindParam("mod_fct_1", $_POST['mod_fct_1']);
			$req->bindParam("mod_fct_2", $_POST['mod_fct_2']);
			$req->bindParam("mod_fct_3", $_POST['mod_fct_3']);
			$req->bindParam("mod_fct_4", $_POST['mod_fct_4']);
			$req->bindParam("mod_fct_5", $_POST['mod_fct_5']);
			$req->bindParam("mod_fct_6", $_POST['mod_fct_6']);
			$req->execute();

			$_SESSION['message'][] = array(
				"titre" => "Succès",
				"type" => "success",
				"message" => "Votre modèle a bien été enregistré"
			);
		}
		
	}
	if(empty($mod_menu)){
		header('Location: accueil.php');  
	}else{
		header('Location: accueil.php?menu=' . $mod_menu);  
	}
	    
	
?>

