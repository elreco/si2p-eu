<?php
    include "includes/controle_acces.inc.php";
    include("modeles/mod_parametre.php");
	include("includes/connexion.php");
	include("includes/connexion_soc.php");
    include("includes/connexion_fct.php");
	include("modeles/mod_set_correspondance.php");

    // VARIABLES
    $client=0;
    if(!empty($_POST['client'])){
        $client = $_POST['client'];
    }

    $opca=0;
    if(!empty($_POST['opca'])){
        $opca = $_POST['opca'];
    }

    $rel_etat_relance=0;
    if(!empty($_POST['rel_etat_relance'])){
        $rel_etat_relance = $_POST['rel_etat_relance'];
    }

    $relance=0;
    if(!empty($_POST['relance'])){
        $relance = $_POST['relance'];
    }

    $acc_agence=0;
    if(isset($_SESSION['acces']["acc_agence"])){
        $acc_agence=$_SESSION['acces']["acc_agence"];
    }
    $acc_societe=0;
    if(isset($_SESSION['acces']["acc_societe"])){
        $acc_societe=$_SESSION['acces']["acc_societe"];
    }
    $acc_utilisateur=0;
    if(!empty($_SESSION['acces']["acc_ref"])){
        if($_SESSION['acces']["acc_ref"]==1){
            $acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
        }
    }

    $sql="SELECT uti_nom, uti_prenom FROM Utilisateurs WHERE uti_id = " . $acc_utilisateur;
    $req = $Conn->query($sql);
    $utilisateur=$req->fetch();

    $rel_con_nom=$_POST['rel_con_nom'];
    $rel_con_prenom=$_POST['rel_con_prenom'];
    $rel_con_tel=$_POST['rel_con_tel'];
    $rel_comment=$_POST['rel_comment'];
    $rel_date_rappel=$_POST['rel_date_rappel'];

    $maj_contact=false;
    if($opca==0 && ($rel_con_nom!="" OR $rel_con_prenom!="" OR $rel_con_tel!="")){
        $maj_contact=true;
    }
    $actu_fac=true;
    $ret_traitement = 0;
    $ret_libelle="";

    $sql="SELECT ret_id,ret_traitement,ret_libelle FROM Relances_Etats ORDER BY ret_j_deb;";
    $req = $Conn->query($sql);
    $d_relances=$req->fetchAll();
    $tab_relance = array();
    $tab_relance[0] = $actu_fac;
    foreach($d_relances as $d){
        if($d['ret_id'] == $rel_etat_relance){
            $actu_fac=false;
            $ret_traitement = $d['ret_traitement'];
            $ret_libelle = $d['ret_libelle'];
        }
        $tab_relance[$d['ret_id']] = $actu_fac;
    }
    if(empty($_POST['rel_date'])){
        $rel_date = date('Y-m-d');
    }else{
        $rel_date = convert_date_sql($_POST['rel_date']);
    }
    if(empty($_POST['rel_date_rappel'])){
        $rel_date_rappel = date('Y-m-d H:i');
    }else{
        $date = $_POST['rel_date_rappel'];
        $rel_date_rappel = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$date)));

    }

    $sql="INSERT INTO Relances (rel_client, rel_opca, rel_utilisateur,rel_uti_identite, rel_etat_relance, rel_date, rel_titre, rel_comment, rel_date_rappel, rel_contact,rel_con_nom,rel_con_tel, rel_con_prenom)
    VALUES (:rel_client, :rel_opca, :rel_utilisateur,:rel_uti_identite, :rel_etat_relance, :rel_date, :rel_titre, :rel_comment, :rel_date_rappel, :rel_contact,:rel_con_nom,:rel_con_tel, :rel_con_prenom)";
    $req = $Conn->prepare($sql);
    $req->bindValue(":rel_client",$client);

    $req->bindValue(":rel_opca",$opca);
    $req->bindValue(":rel_utilisateur",$acc_utilisateur);
    $req->bindValue(":rel_uti_identite",$utilisateur['uti_prenom'] . " " . $utilisateur['uti_nom']);
    $req->bindValue(":rel_etat_relance",$rel_etat_relance);
    $req->bindValue(":rel_date",$rel_date);
    $req->bindValue(":rel_titre",$_POST['rel_titre']);
    $req->bindValue(":rel_comment",$_POST['rel_comment']);
    $req->bindValue(":rel_date_rappel",$rel_date_rappel);
    $req->bindValue(":rel_contact",$_POST['rel_contact']);
    $req->bindValue(":rel_con_nom",$_POST['rel_con_nom']);
    $req->bindValue(":rel_con_prenom",$_POST['rel_con_prenom']);
    $req->bindValue(":rel_con_tel",$_POST['rel_con_tel']);
    $req->execute();
    $relance = $Conn->lastInsertId();
    // TABLEAU CORRESPONDANCE
    if(!empty($_POST['rel_comment'])){
        $edit_correspondance['cor_client'] = $client;
        $edit_correspondance['cor_utilisateur'] = $acc_utilisateur;
        $edit_correspondance['cor_date'] = date('Y-m-d');
        $edit_correspondance['cor_contact'] = $_POST['rel_contact'];
        $edit_correspondance['cor_contact_nom'] = $_POST['rel_con_nom'];
        $edit_correspondance['cor_contact_prenom'] = $_POST['rel_con_prenom'];
        $edit_correspondance['cor_contact_tel'] = $_POST['rel_con_tel'];
        $edit_correspondance['cor_commentaire'] = $_POST['rel_comment'];
        $edit_correspondance['cor_rappeler_le'] = date("Y-m-d",strtotime($rel_date_rappel));
        $edit_correspondance['cor_type'] = 1;
        // INSERER CORRESPONDANCE
        $correspondance=set_correspondance($edit_correspondance);
        if(is_bool($correspondance)){

            echo("Erreur : impossible d'enregsitrer la correspondance!");
            die();
        }
    }
    for ($i=1; $i <= $_POST['nbFac']; $i++) {

        $facture=0;
        if(!empty($_POST['fac_id_' . $i])){
            $facture = $_POST['fac_id_' . $i];
        }

        $facture_societe=0;
        if(!empty($_POST['fac_societe_' . $i])){
            $facture_societe = $_POST['fac_societe_' . $i];
        }

        $facture_numero=0;
        if(!empty($_POST['fac_numero_' . $i])){
            $facture_numero = $_POST['fac_numero_' . $i];
        }

        if($facture>0 && !empty($_POST['ligne_fac'][$i])){



            $ConnFct = connexion_fct($facture_societe);
            $sql="SELECT * FROM Factures  WHERE fac_id =" . $facture;
            $req = $ConnFct->prepare($sql);
            $req->execute();
            $d_facture = $req->fetch();
            if($rel_etat_relance>0){
                $fac_etat_relance = $d_facture['fac_etat_relance'];
                $fac_date_relance = $d_facture['fac_date_relance'];
                if(!empty($tab_relance[$fac_etat_relance]) && $ret_traitement == 1){
                    $fac_etat_relance = $rel_etat_relance;
                    $fac_date_relance = date("Y-m-d");
                }
            }
            if($rel_etat_relance>0){
                $sql="INSERT INTO Relances_Factures (rfa_facture, rfa_facture_soc, rfa_facture_num, rfa_relance, rfa_date, rfa_etat_relance)
                VALUES (:rfa_facture, :rfa_facture_soc, :rfa_facture_num, :rfa_relance, :rfa_date, :rfa_etat_relance)";
                $req = $Conn->prepare($sql);
                $req->bindValue(":rfa_facture",$facture);
                $req->bindValue(":rfa_facture_soc",$facture_societe);
                $req->bindValue(":rfa_facture_num",$d_facture['fac_numero']);
                $req->bindValue(":rfa_relance",$relance);
                if($ret_traitement == 1){
                    $req->bindValue(":rfa_date",$rel_date);
                    $req->bindValue(":rfa_etat_relance",$rel_etat_relance);
                }else{
                    $req->bindValue(":rfa_date",$fac_date_relance);
                    $req->bindValue(":rfa_etat_relance",$fac_etat_relance);
                }
            }else{
                $sql="INSERT INTO Relances_Factures (rfa_facture, rfa_facture_soc, rfa_facture_num, rfa_relance)
                VALUES (:rfa_facture, :rfa_facture_soc, :rfa_facture_num, :rfa_relance)";
                $req = $Conn->prepare($sql);
                $req->bindValue(":rfa_facture",$facture);
                $req->bindValue(":rfa_facture_soc",$facture_societe);
                $req->bindValue(":rfa_facture_num",$d_facture['fac_numero']);
                $req->bindValue(":rfa_relance",$relance);

            }

            if($rel_etat_relance == 6){
                 $sql="UPDATE Factures SET fac_etat_relance=6,fac_date_relance=NOW() WHERE fac_id=" . $d_facture['fac_id'];
                 $req = $ConnFct->prepare($sql);
                 $req->execute();
            }

            $req->execute();
        }
    }

    $_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Votre relance est enregistrée"
    );
    if($rel_etat_relance == 5){
        $adresse="facture_relance_lr_voir.php?relance=" . $relance;
    }elseif(empty($rel_etat_relance) OR $rel_etat_relance == 6){
        $adresse="facture_relance_voir.php?client=" . $client;
    }else{
        $adresse="mail_prep.php?facture_relance=" . $relance;
    }

    header("location : " . $adresse);
die();
