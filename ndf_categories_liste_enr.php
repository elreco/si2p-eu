<?php

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

if (!empty($_POST['nca_vehicule'])) {
	$nca_vehicule = 1;
} else {
	$nca_vehicule = 0;
}
if(!empty($_POST['nca_id'])){
	// select la tva actuelle

	$req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = :tpe_tva AND tpe_date_deb < NOW() AND tpe_date_fin IS NULL");
	$req->bindParam(':tpe_tva', $_POST['nca_tva']);
	$req->execute();
	$tva = $req->fetch();

	$req = $Conn->prepare("UPDATE ndf_categories SET nca_libelle = :nca_libelle, nca_vehicule=:nca_vehicule, nca_tva = :nca_tva,nca_compte = :nca_compte, nca_tva_type = :nca_tva_type  WHERE nca_id = :nca_id");
	$req->bindParam(':nca_libelle', $_POST['nca_libelle']);
	$req->bindParam(':nca_tva', $tva['tpe_taux']);
	$req->bindParam(':nca_tva_type', $_POST['nca_tva']);
	$req->bindParam(':nca_compte', $_POST['nca_compte']);
	$req->bindParam(':nca_vehicule', $nca_vehicule);
	$req->bindParam(':nca_id', $_POST['nca_id']);
	$req->execute();
}else{
	// select la tva actuelle

	$req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = :tpe_tva AND tpe_date_deb < NOW() AND tpe_date_fin IS NULL");
	$req->bindParam(':tpe_tva', $_POST['nca_tva']);
	$req->execute();
	$tva = $req->fetch();
	$req = $Conn->prepare("INSERT INTO ndf_categories (nca_libelle, nca_tva, nca_compte, nca_tva_type, nca_vehicule) VALUES (:nca_libelle, :nca_tva, :nca_compte, :nca_tva_type, :nca_vehicule)");
	$req->bindParam(':nca_libelle', $_POST['nca_libelle']);
	$req->bindParam(':nca_tva', $tva['tpe_taux']);
	$req->bindParam(':nca_compte', $_POST['nca_compte']);
	$req->bindParam(':nca_tva_type', $_POST['nca_tva']);
	$req->bindParam(':nca_vehicule', $nca_vehicule);
	$req->execute();
}

   Header('Location: ndf_categories_liste.php');
	die();
 ?>

