<?php

	// STAT VENTILATION CA CLIENT PAR FAMILLE


	include "includes/controle_acces.inc.php";
	include "modeles/mod_parametre.php";
	include "includes/connexion.php";
	include "includes/connexion_soc.php";


	// DONNEE FORM
	//$_SESSION['retour'] = "stat_com_cli_pfam.php";
	//$_SESSION["retourFacture"]="stat_com_cli_pfam.php";
	//$_SESSION["retourClient"]="stat_com_cli_pfam.php";
    //$_SESSION["retour_action"]="stat_com_cli_pfam.php";

    $erreur_txt="";

    // CONTROLE D'ACCESS

    if ($_SESSION['acces']['acc_profil'] != 14 AND $_SESSION['acces']['acc_profil'] != 11 ) {
        // stat prospection -> DG
        $erreur_txt="Accè refusé. Vous n'êtes pas autorisé à accéder à cette page.";
    
    }else{
    
        if(!empty($_GET)){

			$exercice=0;
			if(isset($_GET["exercice"])){
				if(!empty($_GET["exercice"])){
					$exercice=intval($_GET["exercice"]);
				}
			}else{
				$exercice=intval($_SESSION["crit_stat"]["exercice"]);
			}
            
            $commercial=0;
            if(!empty($_GET["commercial"])){
                $commercial=intval($_GET["commercial"]);
            }
            $detail=0;
            if(!empty($_GET["detail"])){
                $detail=intval($_GET["detail"]);
            }
            $client=0;
            if(!empty($_GET["client"])){
                $client=intval($_GET["client"]);
            }


            if(empty($exercice) OR empty($detail)){
                $erreur_txt="Formulaire incomplet!";
            }
        }else{
            $erreur_txt="Formulaire incomplet!";

        }

    }

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : stat_commercial.php");
		die();
    }
    
    // CRITERE MEMORISE
    $cli_categorie=0;
    if(isset($_SESSION['crit_stat'])){
        $cli_categorie=$_SESSION['crit_stat']["cli_categorie"];
    }

	// LE PERSONNE CONNECTE

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	
	/* $detail
	1 = facturation N
	2 = nouveau client
	3 = client renouvelé
	4 = client perdu
	5 = facturation N-1
	6 = client ancien
	*/

    // tableau pour affichage de la page 

    if($detail==1 OR $detail == 2 OR $detail == 5 OR $detail == 6){
        $sql="SELECT fli_montant_ht,fli_code_produit
        ,fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date,fac_commercial
        ,cli_code,cli_nom,cli_id,cli_agence
        ,Commerciaux.com_label_1,Commerciaux.com_label_2
		,Commerciaux_Clients.com_label_1 as cli_com_label_1,Commerciaux_Clients.com_label_2 as cli_com_label_2
        FROM Factures
        INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
        INNER JOIN Clients ON (Factures.fac_client=clients.cli_id AND Factures.fac_agence=clients.cli_agence)
        INNER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
		LEFT JOIN Commerciaux AS Commerciaux_Clients ON (Clients.cli_commercial=Commerciaux_Clients.com_id)
        WHERE fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'
        AND fli_categorie<4";
        if( !empty($acc_agence)) {
            $sql.=" AND fac_agence=" . $acc_agence;
        }

        if(!empty($commercial)){
            if($commercial==-1){
				$sql.=" AND cli_commercial=0";
			}else{
				$sql.=" AND cli_commercial=" . $commercial;
			}
        }
        if(!empty($client)){
            $sql.=" AND cli_id=" . $client;
        }
        if ($cli_categorie == 1) {
            $sql.=" AND NOT cli_categorie=2";
        } elseif ($cli_categorie == 2) {
            $sql.=" AND cli_categorie=2";
        }
        $sql.=" ORDER BY fac_numero"; 
        $req=$ConnSoc->query($sql);
		$d_factures=$req->fetchAll();

		/*echo("<pre>");
			print_r($d_factures);
		echo("</pre>");*/

		if(!empty($d_factures) AND $detail==2){

			// NOUVEAUX CLIENTS

			$tab_id=array_column($d_factures,"cli_id");
			$list_id=implode(",",$tab_id);

			$sql="SELECT cli_id,cli_first_facture_date FROM Clients WHERE 
			 cli_id IN (" . $list_id . ") AND cli_first_facture_date NOT BETWEEN '" . $exercice . "-04-01' AND '" . intval($exercice+1) . "-03-31';";
			$req=$Conn->query($sql);
			$d_clients_exclus=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($d_clients_exclus)){

				$d_factures_ref=$d_factures;

				foreach($d_clients_exclus as $cli_del){

					//echo($cli_del["cli_id"] . "<br/>");

					$keys = array_keys(array_column($d_factures_ref, 'cli_id'),$cli_del["cli_id"]);
					if(is_array($keys)){		
						foreach($keys as $key)		
						{
							unset($d_factures[$key]);
						}
					}

					/*if($cli_del["cli_id"] == 36953){
						var_dump($key);
					}*/

				}
			}

			/*echo("<pre>");
				print_r($d_factures);
			echo("</pre>");*/
			//die();
		}elseif(!empty($d_factures) AND $detail==6){

			// CLIENTS ANCIENS

			$tab_id=array_column($d_factures,"cli_id");
			$list_id=implode(",",$tab_id);

			/*en partant facture N, il faut exlure 
			- les nouveaux clients 
			- les clients ayants une facturation sur N-1 (renouv)
			*/

			// suppresion clients facturés sur N-1
		
			$sql="SELECT DISTINCT SUM(fli_montant_ht) as ca,cli_id,cli_agence
			FROM Factures
			INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
			INNER JOIN Clients ON (Factures.fac_client=clients.cli_id AND Factures.fac_agence=clients.cli_agence)
			LEFT JOIN Commerciaux ON (Clients.cli_commercial=Commerciaux.com_id) WHERE fac_date>='" . intval($exercice-1) . "-04-01' AND fac_date<='" . $exercice . "-03-31'
			AND fli_categorie<4 AND cli_id IN (" . $list_id . ")";
			if(!empty($acc_agence)){
				$sql.=" AND fac_agence=" . $acc_agence;
			}
			$sql.=" GROUP BY cli_id,cli_agence";
			$req=$ConnSoc->query($sql);
			$d_clients_exclus=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($d_clients_exclus)){

				$d_factures_ref=$d_factures;

				foreach($d_clients_exclus as $cli_del){

					if ( !empty($cli_del["ca"]) ) {
						$keys = array_keys(array_column($d_factures_ref, 'cli_id'),$cli_del["cli_id"]);
						if(is_array($keys)){		
							foreach($keys as $key)		
							{	
								if($d_factures_ref[$key]["cli_agence"]==$cli_del["cli_agence"]){
									unset($d_factures[$key]);
								}
							}
						}
					}

				}
			}

			/*
			réinitialisation des clés 
			*/
			sort($d_factures);

			$tab_id=array_column($d_factures,"cli_id");
			$list_id=implode(",",$tab_id);

			$sql="SELECT cli_id,cli_first_facture_date FROM Clients WHERE 
			cli_id IN (" . $list_id . ") AND cli_first_facture_date BETWEEN '" . $exercice . "-04-01' AND '" . intval($exercice+1) . "-03-31';";
			$req=$Conn->query($sql);
			$d_clients_exclus=$req->fetchAll(PDO::FETCH_ASSOC);

			if(!empty($d_clients_exclus)){

				$d_factures_ref=$d_factures;

				foreach($d_clients_exclus as $cli_del){

					$keys = array_keys(array_column($d_factures_ref, 'cli_id'),$cli_del["cli_id"]);				
					if(is_array($keys)){		
						foreach($keys as $key)		
						{
							unset($d_factures[$key]);
						}
					}
				}
			}

		
		}


		
    } elseif( $detail == 3 ) {

		// CLIENT RENOUVELES

		$sql="SELECT fli_montant_ht,fli_code_produit
        ,fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date,fac_commercial
        ,cli_code,cli_nom
        ,Commerciaux.com_label_1,Commerciaux.com_label_2
		,Commerciaux_Clients.com_label_1 as cli_com_label_1,Commerciaux_Clients.com_label_2 as cli_com_label_2
        FROM Factures
        INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
        INNER JOIN Clients ON (Factures.fac_client=clients.cli_id AND Factures.fac_agence=clients.cli_agence)
        INNER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
		LEFT JOIN Commerciaux AS Commerciaux_Clients ON (Clients.cli_commercial=Commerciaux_Clients.com_id)

		INNER JOIN (
			SELECT SUM(fli_montant_ht) AS ca_prec,fac_client,fac_agence FROM Factures,Factures_Lignes WHERE fac_id=fli_facture AND fli_categorie<4 
			AND fac_date>='" . intval($exercice-1) . "-04-01' AND fac_date<='" . $exercice . "-03-31'";
			if( !empty($acc_agence)) {
				$sql.=" AND fac_agence=" . $acc_agence;
			}
			$sql.=" GROUP BY fac_client,fac_agence) AS Factures_Prec
		ON Clients.cli_id = Factures_Prec.fac_client AND Clients.cli_agence = Factures_Prec.fac_agence AND NOT ISNULL(ca_prec) AND NOT ca_prec=0
		
		WHERE fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'
        AND fli_categorie<4";
        if( !empty($acc_agence)) {
            $sql.=" AND Factures.fac_agence=" . $acc_agence;
        }

        if(!empty($commercial)){
            if($commercial==-1){
				$sql.=" AND cli_commercial=0";
			}else{
				$sql.=" AND cli_commercial=" . $commercial;
			}
        }
        if(!empty($client)){
            $sql.=" AND cli_id=" . $client;
        }
        if ($cli_categorie == 1) {
            $sql.=" AND NOT cli_categorie=2";
        } elseif ($cli_categorie == 2) {
            $sql.=" AND cli_categorie=2";
        }
        $sql.=" ORDER BY fac_numero"; 
        $req=$ConnSoc->query($sql);
		$d_factures=$req->fetchAll();

	} elseif( $detail == 4 ) {

		// CLIENTS PERDUS

		$sql="SELECT fli_montant_ht,fli_code_produit
        ,fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date,fac_commercial
        ,cli_code,cli_nom,cli_commercial,cli_agence
        ,Commerciaux.com_label_1,Commerciaux.com_label_2
		,Commerciaux_Clients.com_label_1 as cli_com_label_1,Commerciaux_Clients.com_label_2 as cli_com_label_2
        FROM Factures
        INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
        INNER JOIN Clients ON (Factures.fac_client=clients.cli_id AND Factures.fac_agence=clients.cli_agence)
        INNER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
		LEFT JOIN Commerciaux AS Commerciaux_Clients ON (Clients.cli_commercial=Commerciaux_Clients.com_id)
       

		LEFT JOIN (
			SELECT SUM(fli_montant_ht) AS ca_suiv,fac_client,fac_agence FROM Factures,Factures_Lignes WHERE fac_id=fli_facture AND fli_categorie<4 
			AND fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'";
			if( !empty($acc_agence)) {
				$sql.=" AND fac_agence=" . $acc_agence;
			}
			$sql.=" GROUP BY fac_client,fac_agence) AS Factures_Suiv
		ON (Clients.cli_id = Factures_Suiv.fac_client AND Clients.cli_agence = Factures_Suiv.fac_agence)
		
		WHERE fac_date>='" . intval($exercice-1) . "-04-01' AND fac_date<='" . $exercice . "-03-31'
		AND fli_categorie<4";

		$sql.=" AND ( ISNULL(ca_suiv) OR ca_suiv=0 )";
        if( !empty($acc_agence)) {
            $sql.=" AND Factures.fac_agence=" . $acc_agence;
        }

        if(!empty($commercial)){
            if($commercial==-1){
				$sql.=" AND cli_commercial=0";
			}else{
				$sql.=" AND cli_commercial=" . $commercial;
			}
        }
        if(!empty($client)){
            $sql.=" AND cli_id=" . $client;
        }
        if ($cli_categorie == 1) {
            $sql.=" AND NOT cli_categorie=2";
        } elseif ($cli_categorie == 2) {
            $sql.=" AND cli_categorie=2";
        }
        $sql.=" ORDER BY fac_numero"; 
        $req=$ConnSoc->query($sql);
		$d_factures=$req->fetchAll();
		

	}
    
    // TITRE

    if(!empty($client)){

        $sql="SELECT cli_nom FROM Clients WHERE cli_id=" . $client . ";";
        $req=$Conn->query($sql);
        $d_client=$req->fetch();
        if(!empty($d_client)){
            $titre="Facturation " . $d_client["cli_nom"];
        }

    }else{

		if($detail==2){
			$titre="Nouveaux clients";
		}elseif($detail==6){
			$titre="Anciens clients";
		}else{
			$titre="Clients";
		}

        if($cli_categorie==1){
            $titre.=" géographiques "; 
        }elseif($cli_categorie==2){
            $titre.=" grands-comptes "; 
        } 
        if(!empty($commercial)){
            if($commercial==-1){
                $titre.=" non affectés";
            }else{
                $sql="SELECT com_label_1,com_label_2 FROM Commerciaux WHERE com_id=" . $commercial . ";";
                $req=$ConnSoc->query($sql);
                $d_commercial=$req->fetch();
                if(!empty($d_commercial)){
                    $titre.=" de " . $d_commercial["com_label_1"] . " " . $d_commercial["com_label_2"];
                }
            }
        }

        if($detail==1 OR $detail == 5){
            $titre.=" facturés";
        }elseif($detail == 3){
			$titre.=" renouvelés";
		}elseif($detail == 4){
			$titre.=" perdus";
		}
    }

    $titre.=" sur l'exercice " . $exercice . "/" . intval($exercice+1);
    

    // CATEGORIES CLIENTS
    $d_categories=array();
    $sql = "SELECT * FROM Clients_Categories ORDER BY cca_id;";
    $req = $Conn->query($sql);
    $results = $req->fetchAll();
    if(!empty($results)){
        foreach($results as $r){
            $d_categories[$r["cca_id"]]=$r["cca_libelle"];
        }
    }

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css">

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#zone_print{
				display:none;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 landscape;
				margin:5mm;
			}
			html{
				background-color:#fff!important
			}
			body{
				background-color:#fff!important
			}
			#zone_print{
				background-color:#fff!important;
				font-size:8pt;
			}
		</style>
	</head>

	<body class="sb-top sb-top-sm no-scroll" >

		<div id="zone_print" ></div>

		<div id="main" >

<?php       include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" style="overflow:scroll;" >

					<div id="page_print" >
						<h1 class="text-center" ><?=$titre?></h1>

							<div class="table-responsive">
								<table class="table table-striped table-hover" >
									<thead>
										<tr class="dark">
											<th>Code</th>
                                            <th>Nom</th>
											<th>Com. client</th>
                                            <th>Facture</th>
                                            <th>Com. facture</th>
                                            <th>Date</th>
                                            <th>Produit</th>
                                            <th>HT</th>
                                        </tr>                                      
									</thead>
					<?php           if(!empty($d_factures)){  ?>
										<tbody>
							<?php			$total_ca=0;

                                            foreach($d_factures as $d){ 

                                                $total_ca=$total_ca + round($d["fli_montant_ht"],2);  ?>
                                                <tr>
                                                    <td><?=$d["cli_code"]?></td>
                                                    <td><?=$d["cli_nom"]?></td>
													<td><?=$d["cli_com_label_1"] . " " . $d["cli_com_label_2"]?></td>
                                                    <td><?=$d["fac_numero"]?></td>
                                                    <td><?=$d["com_label_1"] . " " . $d["com_label_2"]?></td>
                                                    <td><?=$d["fac_date"]?></td>
                                                    <td><?=$d["fli_code_produit"]?></td>                                                   
                                                    <td class="text-right" >
														<?=number_format($d["fli_montant_ht"],2,","," ")?>
                                                    </td>
                                                </tr>
                            <?php		    } ?>
                                            <tr>
                                                <td class="text-right" colspan="7" >Total :</td>
                                                <td class="text-right" ><?=number_format($total_ca,2,","," ")?></td>
                                            </tr>
										</tbody>
							<?php	} ?>
								</table>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="stat_com_prospection_detail.php?commercial=<?=$commercial?>&detail=<?=$detail?>" class="btn btn-sm btn-default"  >
							<i class="fa fa-left-arrow" ></i>Retour
						<a>
						<!--<button type="button" class="btn btn-sm btn-info ml15 btn-print" >
							<i class="fa fa-print"></i> Imprimer
						</button>-->
					</div>
					<div class="col-xs-6 footer-middle text-center" style=""></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>

<?php	include "includes/footer_script.inc.php"; ?>

		<script src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script src="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function () {

				/*

				var calcDataTableHeight = function (elt_content, elt_head) {
					return $(elt_content).height() - $(elt_head).height() - 6;
				};
				var tableDefFix = $('#tableFix').dataTable({
					"language": {
						"url": "/vendor/plugins/DataTables/media/js/French.json"
					},
					paging: false,
					searching: false,

					info: false,
					scrollY: calcDataTableHeight("#tableCont", "#tableHead"),
					scrollCollapse: true,
					order: [[1, "asc"], [2, "asc"]],
					columnDefs: [
						{ targets: 'no-sort', orderable: false }
					]
				});
				$(window).resize(function () {
					var tableDefFixParam = tableDefFix.fnSettings();
					tableDefFixParam.oScroll.sY = calcDataTableHeight("#tableCont", "#tableHead");
					tableDefFix.fnDraw();
				});
				$(window).load(function () {
					setTimeout(function () {
						$(".dataTables_scrollBody").mCustomScrollbar({
							theme: "dark"
						});
					}, 100);
				});
		*/

			});
			(jQuery);
		</script>
	</body>
</html>
