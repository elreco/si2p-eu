<?php
	
    // STAT CACES 1
    /*
    Affiche le nombre de jours de formation et de test par formateur. 
    La stat indique également quelles catégories ont été testées au moins un fois sur la période.
    */
	include "includes/controle_acces.inc.php";
	//include "modeles/mod_parametre.php";
	include "includes/connexion.php";
    include "includes/connexion_soc.php";

    // controle d'accès
    
    if($_SESSION["acces"]["acc_ref"]!=1 OR ($_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']["acc_profil"]!=11 AND $_SESSION['acces']["acc_profil"]!=14 AND $_SESSION['acces']["acc_profil"]!=15 AND $_SESSION['acces']["acc_profil"]!=10) ){
        // SERVICE TECH, DAF, DG, RA et RE
        $erreur="Accès refusé!";
		echo($erreur);
		die();
	}

	
	
	// DONNEE FORM

	$erreur_txt="";
	if(!empty($_POST)){

		$qualification=0;
		if(!empty($_POST["qualification"])){
			$qualification=intval($_POST["qualification"]);
		}
		
		$periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}
		
		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
        }
        
        $_SESSION["stat_tech"]=array(
            "qualification" => $qualification,
            "periode_deb" => $_POST["periode_deb"],
            "periode_fin" => $_POST["periode_fin"],
        );
		
    } elseif (isset($_SESSION["stat_tech"])) {

        $qualification=0;
		if(!empty($_SESSION["stat_tech"]["qualification"])){
			$qualification=intval($_SESSION["stat_tech"]["qualification"]);
		}
		
		$periode_deb="";
		if(!empty($_SESSION["stat_tech"]["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_SESSION["stat_tech"]["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}
		
		$periode_fin="";
		if(!empty($_SESSION["stat_tech"]["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_SESSION["stat_tech"]["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
        }

	}else{
		$erreur_txt="Formulaire incomplet!";
		
    }
    
    if (empty($periode_deb) OR empty($periode_fin) OR empty($qualification)) {
        $erreur_txt="Formulaire incomplet!";
    }
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : stat_technique.php");
		die();
    }
	
	// LE PERSONNE CONNECTE
	
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]); 
	}
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]); 
	}
	
    // QUALIFICATIONS
    
    // qualif select
    
    $sql="SELECT qua_libelle FROM Qualifications WHERE qua_id=" . $qualification . ";";
    $req=$Conn->query($sql);
    $d_qualification=$req->fetch();
    if(!empty($d_qualification)){
        $titre="Qualification " . $d_qualification["qua_libelle"];
        $titre.="<br/> entre le " . $DT_periode_deb->format("d/m/Y") . " et le " . $DT_periode_fin->format("d/m/Y") ;
    }else{
        $titre="";
        $erreur_txt="Qualification non identifié!";
    }

    // liste des qualifications (pour filtre rapide)

    $sql="SELECT qua_id,qua_libelle FROM Qualifications WHERE qua_caces ORDER BY qua_libelle;";
    $req=$Conn->query($sql);
    $d_qualifications=$req->fetchAll();
    


    if (empty($erreur_txt)) {

        // Liste des produits de la qualifications de type CACES
        // exclusion des attestations

        $liste_pro="";
        $sql="SELECT pro_id,pro_code_produit FROM Produits WHERE pro_qualification=" . $qualification . " AND NOT pro_code_produit LIKE 'A-%';";
        $req=$Conn->query($sql);
        $src_pro=$req->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($src_pro)){
            /*echo("<pre>");
                print_r($src_pro);
            echo("</pre>");*/
            $tab_pro=array_column ($src_pro,"pro_id");
            $liste_pro=implode($tab_pro,",");
        }
        if (empty($liste_pro)) {
            $erreur_txt="Aucun produit n'est associé à la qualification demandé.";
        }
    }


    if (empty($erreur_txt)) {

        $data=array(); // tableau final pour le rendu des données.
        $data_cat=array(); // tableau injecté dans data_actions_clients

        // LES CATEGORIES DE QUALIF

        $data_cat=array();
        $sql="SELECT qca_id,qca_libelle FROM Qualifications_Categories WHERE qca_qualification=" . $qualification . " AND NOT qca_archive ORDER BY qca_libelle;";
        $req=$Conn->query($sql);
        $d_categories=$req->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($d_categories)) {
            foreach ($d_categories as $d_cat) {

                $data_cat[$d_cat["qca_id"]]=0;

                $data[$d_cat["qca_id"]]=array(
                    "nom" => $d_cat["qca_libelle"],
                    "intra_ca" => 0,
                    "intra_test" => 0,
                    "inter_ca" => 0,
                    "inter_test" => 0
                );

            }
        }
        $data[0]=array(
            "nom" => "Inscription sans test",
            "intra_ca" => 0,
            "intra_test" => 0,
            "inter_ca" => 0,
            "inter_test" => 0
        );

    
        // LES TEST CACES

        $warning_txt="";

        /* on récupère le nombre de tests pour chaque action client (total et nb test par categorie)
        */

        $data_actions_clients=array();

        $sql="SELECT COUNT(dcc_diplome) AS nb_test,dcc_categorie,dcc_action,dcc_action_client
        FROM diplomes_caces_cat,diplomes_caces
        WHERE dcc_diplome=dca_id";
        $sql.=" AND dca_diplome=2 AND dcc_date>='" . $periode_deb . "' AND dcc_date<='" . $periode_fin . "' AND dca_qualification=" . $qualification;
        $sql.=" AND dcc_action_soc=" . $acc_societe;
        $sql.=" GROUP BY dcc_categorie,dcc_action,dcc_action_client;";
        $req=$Conn->query($sql);
        $d_tests=$req->fetchAll();
        if(!empty($d_tests)){

            foreach($d_tests as $test){


                if (!isset($data_actions_clients[$test["dcc_action_client"]])) {

                    $data_actions_clients[$test["dcc_action_client"]]=array(
                        "total_test" => 0,
                        "test_cat" => $data_cat   
                    );

                }

                // pour chaque action
                // on recupère le nombre de test par categorie et le nombre de test total.
                $data_actions_clients[$test["dcc_action_client"]]["total_test"]=$data_actions_clients[$test["dcc_action_client"]]["total_test"] + $test["nb_test"];
                $data_actions_clients[$test["dcc_action_client"]]["test_cat"][$test["dcc_categorie"]]= $data_actions_clients[$test["dcc_action_client"]]["test_cat"][$test["dcc_categorie"]] + $test["nb_test"];
            }
            if(isset($data_actions_clients[0])) {
                $warning_txt.="Certains tests ne sont associés à aucune action. Consultez le détail des valeurs pour connaître les dossiers concernés.<br/>";
            }
        }else{
            $erreur_txt="Aucun test réalisé sur la période demandée.";
        }

    
        // DONNEES PLANNING POUR LE NOMBRE DE JOUR

        if (empty($erreur_txt)) {

            $data_action_client=array();
           

            $sql="SELECT acl_id,acl_produit,acl_pro_inter,COUNT(pda_id) AS nb_jour,ca,total_jour FROM Actions_Clients 
            INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
            INNER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
            INNER JOIN Plannings_Dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
            LEFT JOIN (
            
                SELECT SUM(fli_montant_ht) as ca, fli_action_client FROM Factures_Lignes GROUP BY fli_action_client

            ) AS Factures ON (Actions_Clients.acl_id = Factures.fli_action_client)
            
            LEFT JOIN (
                SELECT COUNT(acd_date) as total_jour, acd_action_client FROM Actions_Clients_Dates GROUP BY acd_action_client
            ) AS Dates ON (Actions_Clients.acl_id = Dates.acd_action_client)

            WHERE acl_produit IN (" . $liste_pro . ") AND NOT acl_archive AND NOT act_archive AND pda_categorie=2
            AND pda_date>='" . $periode_deb . "' AND pda_date<='" . $periode_fin . "'";
            $sql.=" GROUP BY acl_id,acl_produit,ca,total_jour";
            $req=$ConnSoc->query($sql);
            $d_sources=$req->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($d_sources)) {			
                foreach ($d_sources as $s){

                    $ca_test=0;
                    if (!empty($s["total_jour"])) {
                        $ca_test=($s["ca"]/$s["total_jour"])*$s["nb_jour"];
                    }

                    // l'action a généré des tests.
                    if (!empty($data_actions_clients[$s["acl_id"]])) {

                        if( !empty($data_actions_clients[$s["acl_id"]]["total_test"])) {

                            $ca_test_unit=$ca_test / $data_actions_clients[$s["acl_id"]]["total_test"];
                        }

                        // ventilation INTRA INTER
                        if ($s["acl_pro_inter"]) {
                            // action réalisée en INTER
                            foreach ($data_cat as $q => $d) {
                                // boucle sur tab cat pour ne pas avoir la clé [0] présente sur $data

                                if (!empty($data_actions_clients[$s["acl_id"]]["test_cat"][$q])){                                 
                                    $data[$q]["inter_ca"]= $data[$q]["inter_ca"] + ($data_actions_clients[$s["acl_id"]]["test_cat"][$q]*$ca_test_unit);
                                    $data[$q]["inter_test"]= $data[$q]["inter_test"] + $data_actions_clients[$s["acl_id"]]["test_cat"][$q];                                 
                                }
                            }                
                        } else {

                             // action réalisée en INTRA
                            foreach ($data_cat as $q => $d) {

                                if (!empty($data_actions_clients[$s["acl_id"]]["test_cat"][$q])){                                 
                                    $data[$q]["intra_ca"]= $data[$q]["intra_ca"] + ($data_actions_clients[$s["acl_id"]]["test_cat"][$q]*$ca_test_unit);
                                    $data[$q]["intra_test"]= $data[$q]["intra_test"] + $data_actions_clients[$s["acl_id"]]["test_cat"][$q];                                 
                                }
                            }   
                        }

                    } elseif (!empty($ca_test)) {

                        //$warning_txt.="L'inscription " . $s["acl_id"] . " n'a générée aucun test!<br/>";

                        if ($s["acl_pro_inter"]) {
                            // action réalisée en INTER
                            $data[0]["inter_ca"]= $data[0]["inter_ca"] + $ca_test;                           
                        } else {
                             // action réalisée en INTRA
                             $data[0]["intra_ca"]= $data[0]["intra_ca"] + $ca_test;                 
                        }
                    }
                }

            } else { 
                $erreur_txt="Aucune action ne correspond à votre recherche.";
            }
        }
    }
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<!-- PERSO -->	
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
		
		<div id="main" >
<?php		include "includes/header_def.inc.php";  ?>			
			<section id="content_wrapper" >					
			
				<section id="content" class="animated fadeIn" >


                    <h1 class="text-center" >
                        <?=$titre?>
                    </h1>

                    <div class="row" >
                        <div class="col-md-8 col-md-offset-2" >
                    
                            <div class="panel mt10 panel-info" > 
                                <div class="panel-heading panel-head-sm" >
                                    <span class="panel-title">Recherche rapide.</span>									
                                </div>
                                <div class="panel-body p5" >

                                    <form method="post" action="stat_tech_caces_cdt.php" >
                                        <div>													
                                            <input type="hidden" name="periode_deb" value="<?=$DT_periode_deb->format("d/m/Y")?>" />
                                            <input type="hidden" name="periode_fin" value="<?=$DT_periode_fin->format("d/m/Y")?>" />													
                                        </div>
                                        <div class="admin-form" >
                                            <div class="row mt15" >
                                                <div class="col-md-10" >
                                                    <label for="qualification" >Qualification</label>
                                                    <select class="select2" id="qualification" name="qualification" >                                                      
                                                <?php	foreach($d_qualifications as $qualif){
                                                            if($qualif["qua_id"]==$qualification){
                                                                echo("<option value='" . $qualif["qua_id"] . "' selected >" . $qualif["qua_libelle"] . "</option>");
                                                            }else{
                                                                echo("<option value='" . $qualif["qua_id"] . "' >" . $qualif["qua_libelle"] . "</option>");
                                                            }
                                                        } ?>
                                                
                                                    </select>
                                                </div>
                                                <div class="col-md-2 text-center mt20" >
                                                    <button type="submit" class="btn btn-md btn-info" >
                                                        Afficher
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

            
            <?php	if (!empty($erreur_txt)) {   ?>
                    
                        <p class="alert alert-danger" ><?=$erreur_txt?></p>

        <?php       } else {  ?>

                        <div class="row" >
                            <div class="col-md-12" >
                            
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" >
                                        <thead>
                                            <tr class="dark">
                                                <th rowspan="2" >Catégories</th>
                                                <th colspan="2" class="text-center" >INTRA</th>
                                                <th colspan="2" class="text-center" >INTER</th>    
                                                <th colspan="2" class="text-center" >TOTAL</th> 
                                                <th rowspan="2" class="text-center" >% TEST INTER</th>                                  					
                                            </tr>
                                            <tr>
                                                <td class="text-center">CA</td>
                                                <td class="text-center">Nb. Tests</td>
                                                <td class="text-center">CA</td>
                                                <td class="text-center">Nb. Tests</td>
                                                <td class="text-center">CA</td>
                                                <td class="text-center">Nb. Tests</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php		$total_intra_ca=0;
                                            $total_intra_test=0;
                                            $total_inter_ca=0;
                                            $total_inter_test=0;
                                            foreach($data as $k => $d){ 

                                                if ($k!=0) {
                                                
                                                    $total_intra_ca=$total_intra_ca + round($d["intra_ca"],2);
                                                    $total_intra_test=$total_intra_test + $d["intra_test"];
                                                    $total_inter_ca=$total_inter_ca + round($d["inter_ca"],2);
                                                    $total_inter_test=$total_inter_test + $d["inter_test"];
                                        
                                                    ?>
                                                    <tr>
                                                        <td><?=$d["nom"]?></td>
                                                        <td class="text-right" >
                                                            <a href="stat_tech_caces_cdt_detail.php?categorie=<?=$k?>&intra_inter=1" ><?=round($d["intra_ca"],2)?></a>
                                                        </td>
                                                        <td class="text-right" >
                                                            <a href="stat_tech_caces_cdt_detail.php?categorie=<?=$k?>&intra_inter=1" ><?=$d["intra_test"]?></a>
                                                        </td>
                                                        <td class="text-right" >
                                                            <a href="stat_tech_caces_cdt_detail.php?categorie=<?=$k?>&intra_inter=2" ><?=round($d["inter_ca"],2)?></a>
                                                        </td>
                                                        <td class="text-right" >
                                                            <a href="stat_tech_caces_cdt_detail.php?categorie=<?=$k?>&intra_inter=2" ><?=$d["inter_test"]?></a>
                                                        </td>
                                                        <td class="text-right" >
                                                            <a href="stat_tech_caces_cdt_detail.php?categorie=<?=$k?>" ><?=round($d["intra_ca"]+$d["inter_ca"],2)?></a>
                                                        </td>
                                                        <td class="text-right" >
                                                            <a href="stat_tech_caces_cdt_detail.php?categorie=<?=$k?>" ><?=$d["intra_test"]+$d["inter_test"]?></a>
                                                        </td>
                                                        <td class="text-right" >
                                                    <?php   if(!empty($d["intra_test"]) OR !empty($d["inter_test"])) {
                                                                $taux=($d["inter_test"]*100)/($d["intra_test"]+$d["inter_test"]);
                                                                echo(number_format($taux,2,","," "));
                                                            } ?>
                                                        </td>
                                                    </tr>
                                <?php		    }
                                            } 
                                            $total_intra_ca=$total_intra_ca + round($data[0]["intra_ca"],2);
                                            $total_intra_test=$total_intra_test + $data[0]["intra_test"];
                                            $total_inter_ca=$total_inter_ca + round($data[0]["inter_ca"],2);
                                            $total_inter_test=$total_inter_test + $data[0]["inter_test"];
                                            ?>
                                            <tr>
                                                <td><?=$data[0]["nom"]?></td>
                                                <td class="text-right" ><?=round($data[0]["intra_ca"],2)?></td>
                                                <td class="text-right" ><?=$data[0]["intra_test"]?></td>
                                                <td class="text-right" ><?=round($data[0]["inter_ca"],2)?></td>
                                                <td class="text-right" ><?=$data[0]["inter_test"]?></td>
                                                <td class="text-right" ><?=round($data[0]["intra_ca"]+$data[0]["inter_ca"],2)?></td>
                                                <td class="text-right" ><?=$data[0]["intra_test"]+$data[0]["inter_test"]?></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Total :</th>
                                                <td class="text-right" ><a href="stat_tech_caces_cdt_detail.php?intra_inter=1" ><?=$total_intra_ca?></a></td>
                                                <td class="text-right" ><a href="stat_tech_caces_cdt_detail.php?intra_inter=1" ><?=$total_intra_test?></a></td>
                                                <td class="text-right" ><a href="stat_tech_caces_cdt_detail.php?intra_inter=2" ><?=$total_inter_ca?></a></td>
                                                <td class="text-right" ><a href="stat_tech_caces_cdt_detail.php?intra_inter=2" ><?=$total_inter_test?></a></td>
                                                <td class="text-right" ><a href="stat_tech_caces_cdt_detail.php" ><?=$total_intra_ca + $total_inter_ca?></a></td>
                                                <td class="text-right" ><a href="stat_tech_caces_cdt_detail.php" ><?=$total_intra_test + $total_inter_test?></a></td>
                                            </tr>
                                        </tfoot>
                                    </table>							
                                </div>
                                
                            </div>
						</div>
		<?php	    } ?>

				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
            <div class="row">
                <div class="col-xs-3 footer-left">
                    <a href="stat_technique.php" class="btn btn-sm btn-default"  >
                        <i class="fa fa-left-arrow" ></i>Retour
                    </a>					
                </div>
                <div class="col-xs-6 footer-middle text-center" style=""></div>
                <div class="col-xs-3 footer-right"></div>
            </div>
        </footer>
<?php   if (!empty($warning_txt)) { ?>

            <div id="modal-error" class="modal fade" role="dialog" data-show="true" id="modal_warning">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-warning">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title" id="titreModal" >Avertissement</h4>
                        </div>
                        <div class="modal-body"><?=$warning_txt?></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                <i class="fa fa-close" ></i>
                                Fermer
                            </button>
                        </div>
                    </div>
                </div>
            </div>
<?php	}
        include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function () {

        <?php   if (!empty($warning_txt)) { ?>
                    $("#modal_warning").modal("show");
        <?php   } ?>
			});
			(jQuery);
		</script>
	</body>
</html>
