<?php
    
    /*
    STAT CACES 3
    Affiche la répartition du CA par qualification (formation/test et INTRA/INTER)
    */

	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
    include "includes/connexion_soc.php";
    
    // CONTROLE ACCES
    if ($_SESSION["acces"]["acc_ref"]!=1 OR ($_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']["acc_profil"]!=11 AND $_SESSION['acces']["acc_profil"]!=14 AND $_SESSION['acces']["acc_profil"]!=15 AND $_SESSION['acces']["acc_profil"]!=10) ) {
		// accès service tech, DAF, DG, RA et RE
		$erreur="Accès refusé!";
		echo($erreur);
		die();
	}
	
	// DONNEE FORM

	$erreur_txt="";
	if(!empty($_POST)){
		
		$periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}
		
		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
		}
		
		if(empty($periode_deb) OR empty($periode_fin)){
			$erreur_txt="Formulaire incomplet!";
		}
	}else{
		$erreur_txt="Formulaire incomplet!";
		
	}
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : stat_technique.php");
		die();
	}
	
	
	// LE PERSONNE CONNECTE
	
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]); 
	}
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]); 
	}
	
	// TITRE 
	
    $titre="CA CACES®";
	$titre.=" entre le " . $DT_periode_deb->format("d/m/Y") . " et le " . $DT_periode_fin->format("d/m/Y") ;
	
	// QUALIFICATIONS

	$data=array(
		0 => array(
			"nom" => "Total",
			"ca_form" => 0,
			"ca_test" => 0,
			"ca_intra" => 0,
			"ca_inter" => 0
		)
	);

	$sql="SELECT qua_id,qua_libelle FROM Qualifications WHERE qua_caces ORDER BY qua_libelle;";
	$req=$Conn->query($sql);
	$d_qualifications=$req->fetchAll();
	if(!empty($d_qualifications)){
		foreach($d_qualifications as $qualif){

			$data[$qualif["qua_id"]]=array(
				"nom" => $qualif["qua_libelle"],
				"ca_form" => 0,
				"ca_test" => 0,
				"ca_intra" => 0,
				"ca_inter" => 0
			);
			$tab_qualif[]=$qualif["qua_id"];
		}

		$liste_qualif=implode(",",$tab_qualif);
	}else{
		$erreur_txt="Il n'y a pas de qualification CACES®";
	}


	// LISTE DES PRODUITS "CACES"
	if (empty($erreur_txt)) {

		$liste_pro="";
		$d_produits=array();
		$sql="SELECT pro_id,pro_qualification FROM Produits WHERE pro_qualification IN (" . $liste_qualif . ");";
		$req=$Conn->query($sql);
		$src_pro=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($src_pro)){
			$tab_pro=array_column ($src_pro,"pro_id");
			$liste_pro=implode($tab_pro,",");
			foreach($src_pro as $sp){
				$d_produits[$sp["pro_id"]]=$sp["pro_qualification"];
			}

		}else{
			$erreur_txt="Aucun produit n'est associé aux qualifications CACES®";

		}
	}

	// DONNEE SOURCE POUR LA STAT
	if (empty($erreur_txt)){

		$sql="SELECT acl_id,acl_produit,acl_pro_inter,pda_categorie,COUNT(pda_id) AS nb_jour,ca,total_jour FROM Actions_Clients 
		INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
		INNER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
		INNER JOIN Plannings_Dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
		LEFT JOIN (
			/* avec cette requete pas de facturation = pas de demi jour mais ça ne pose pas de problème puisque la donnée à extraire est le CA*/
			/*SELECT SUM(fli_montant_ht) as ca,COUNT(acd_date) as total_jour, fli_action_client FROM Factures_Lignes,Actions_Clients_Dates
			WHERE Factures_Lignes.fli_action_client=Actions_Clients_Dates.acd_action_client
			GROUP BY fli_action_client */
			
			SELECT SUM(fli_montant_ht) as ca, fli_action_client FROM Factures_Lignes GROUP BY fli_action_client

		) AS Factures ON (Actions_Clients.acl_id = Factures.fli_action_client)
		
		LEFT JOIN (
			SELECT COUNT(acd_date) as total_jour, acd_action_client FROM Actions_Clients_Dates GROUP BY acd_action_client
		) AS Dates ON (Actions_Clients.acl_id = Dates.acd_action_client)

		WHERE acl_produit IN (" . $liste_pro . ") AND NOT acl_archive AND NOT act_archive
		AND pda_date>='" . $periode_deb . "' AND pda_date<='" . $periode_fin . "'";
		//Pour controle
		//$sql.=" AND act_id IN (65902)";
		//$sql.=" AND acl_id IN (67759)";
		$sql.=" GROUP BY acl_id,acl_produit,pda_categorie,ca,total_jour";
		$req=$ConnSoc->query($sql);
		$d_sources=$req->fetchAll(PDO::FETCH_ASSOC);
		if (!empty($d_sources)) {
			
			foreach ($d_sources as $s){

				$qualif_id=$d_produits[$s["acl_produit"]];

				if (!empty($qualif_id)) {

					$cout_demi=0;
					if (!empty($s["total_jour"])) {
						$cout_demi=$s["ca"]/$s["total_jour"];
					}

					if ($s["pda_categorie"]==2) {

						$data[$qualif_id]["ca_test"]=$data[$qualif_id]["ca_test"] + ($cout_demi*$s["nb_jour"]);

					} else {

						$data[$qualif_id]["ca_form"]=$data[$qualif_id]["ca_form"] + ($cout_demi*$s["nb_jour"]);

					}

					if ($s["acl_pro_inter"]) {

						$data[$qualif_id]["ca_inter"]=$data[$qualif_id]["ca_inter"] + ($cout_demi*$s["nb_jour"]);

					} else {

						$data[$qualif_id]["ca_intra"]=$data[$qualif_id]["ca_intra"] + ($cout_demi*$s["nb_jour"]);

					}
				}


			}

		} else { 
			$erreur_txt="Aucune donnée ne correspond à votre recherche.";
		}





		

		
	}	
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

		<!-- PERSO -->	
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
		
		<div id="main" >
<?php	include "includes/header_def.inc.php"; ?>			
			<section id="content_wrapper" >					
				<section id="content" class="animated fadeIn" >

				<h1 class="text-center" >
							<?=$titre?>
						</h1>

				
			<?php	if (!empty($erreur_txt)) { 
						echo("<p class='alert alert-danger' >");
							echo($erreur_txt);
						echo("</p>");

					} else { ?>

						
						<div class="row" >
							<div class="col-md-12" >
							
								<div class="table-responsive">
									<table class="table table-striped table-hover" >
										<thead>
											<tr class="dark">
												<th>Qualification</th>
												<th class="text-right" >CA formation</th>
												<th class="text-right" >CA test</th>
												<th class="text-right" >INTRA</th>
												<th class="text-right" >INTER</th>							
											</tr>
										</thead>
										<tbody>
								<?php		foreach($data as $k => $d){
												if ($k>0) {
													$data[0]["ca_form"]=$data[0]["ca_form"]+ round($d["ca_form"],2); 
													$data[0]["ca_test"]=$data[0]["ca_test"]+ round($d["ca_test"],2);
													$data[0]["ca_intra"]=$data[0]["ca_intra"]+ round($d["ca_intra"],2);
													$data[0]["ca_inter"]=$data[0]["ca_inter"]+ round($d["ca_inter"],2);  ?>
													<tr>
														<td><?=$d["nom"]?></td>
														<td class="text-right" ><?=number_format($d["ca_form"],2,","," ")?></td>
														<td class="text-right" ><?=number_format($d["ca_test"],2,","," ")?></td>
														<td class="text-right" ><?=number_format($d["ca_intra"],2,","," ")?></td>
														<td class="text-right" ><?=number_format($d["ca_inter"],2,","," ")?></td>
													</tr>
								<?php			}
											} ?>
										</tbody>
										<tfoot>
											<tr>
												<th class="text-right" >Total :</th>
												<td class="text-right" ><?=number_format($data[0]["ca_form"],2,","," ")?></td>
												<td class="text-right" ><?=number_format($data[0]["ca_test"],2,","," ")?></td>
												<td class="text-right" ><?=number_format($data[0]["ca_intra"],2,","," ")?></td>
												<td class="text-right" ><?=number_format($data[0]["ca_inter"],2,","," ")?></td>
											</tr>
										</tfoot>
									</table>							
								</div>
								
							</div>
						</div>
		<?php		} ?>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="stat_technique.php" class="btn btn-sm btn-default"  >
							<i class="fa fa-left-arrow" ></i>Retour
						<a>					
					</div>
					<div class="col-xs-6 footer-middle text-center" style=""></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				
			
				
			});
			(jQuery);
		</script>
	</body>
</html>
