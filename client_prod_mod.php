 <?php
	include "includes/controle_acces.inc.php";
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>SI2P - Orion - Alexandre LE CORRE</title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
  <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
  <link rel="stylesheet" type="text/css" href="vendor/plugins/footable/css/footable.core.min.css">
  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>
<body class="sb-top sb-top-sm ">
  <!-- Start: Main -->
  <div id="main">
    <?php
    include "includes/header_def.inc.php";
    ?>


    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper" class="">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li>
              <span class="glyphicon glyphicon-shopping-cart"></span> <a href="parametre.php"> Commerce</a>
            </li>
            <li>
              <a href="client_liste.php">Liste des clients</a>
            </li>
            <li>
              <a href="client_voir.php">Immobilière 3F Groupe SOLENDI</a>
            </li>
            <li class="crumb-trail">Editer un produit</li>
          </ol>
        </div>

      </header>
      <section id="content" class="">

        <!-- Begin .page-heading -->


        <div class="row">

          <div class="col-md-12">
            <div class="panel">
              <div class="panel-heading  panel-head-sm">
                <i class="fa fa-pencil"></i> Editer le document pour le client <strong>Immobilière 3F Groupe SOLENDI</strong>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12">
                      <form id="filtre" action="#" method="post"
                      class="admin-form form-inline form-inline-grid">
                      <div class="col-md-1">

                        <div class="section">
                          <select name="filtre_agence" id="filtre_agence" class="form-control">
                            <option value="">Catégorie...</option>
                          </select>
                        </div>

                      </div>
                      <div class="col-md-1">

                        <div class="section">
                          <select name="filtre_agence" id="filtre_agence" class="form-control">
                            <option value="">Famille...</option>
                          </select>
                        </div>

                      </div>
                      <div class="col-md-1">
                        <div class="section">
                        <select name="filtre_agence" id="filtre_agence" class="form-control">
                          <option value="">Sous-Famille...</option>
                        </select>
                      </div>
                      </div>
                      <div class="col-md-1">
                        <div class="section">
                        <select name="filtre_agence" id="filtre_agence" class="form-control">
                          <option value="">Produit...</option>
                        </select>
                      </div>
                      </div>
                      <div class="col-md-2">
                        <div class="section">
                        <strong>Libellé du produit:</strong> test
                      </div>
                      </div>
                      <div class="col-md-2">
                        <div class="section">
                        <strong>TG:</strong> TG
                      </div>
                      </div>
                      <div class="col-md-1">

                              <input type="number" name="from" id="from" class="gui-input"
                                     placeholder="TC en €">


                      </div>
                      <div class="col-md-2">
                        <div class="section">
                        <strong>Nombre de Demi-Jour:</strong> 5
                      </div>
                      </div>

                    </form>
                  </div>
                </div>

              </div>
            </div>

          </div>
        </div>


      </section>
      <!-- End: Content -->
    </section>
  </div>

  <footer id="content-footer" class="affix">
    <div class="row">
      <div class="col-xs-3 footer-left">
        <a href="client_voir.php" class="btn btn-default btn-sm">
          <i class="fa fa-long-arrow-left"></i>
          Retour
        </a>
      </div>
      <div class="col-xs-6 footer-middle"></div>
      <div class="col-xs-3 footer-right">

        <a href="utilisateur_mod_1.php" class="btn btn-success btn-sm">
          <i class='fa fa-floppy-o'></i> Enregistrer
        </a>
      </div>
    </div>
  </footer>
  <style>
  .content-header {
    margin-bottom: 8px !important;
  }

  .ui-priority-secondary {
    display: none !important;
  }
  </style>
 <?php
		include "includes/footer_script.inc.php"; ?>	
 
  <script src="vendor/plugins/sparkline/jquery.sparkline.min.js"></script>
  <-- plugin pour les graphiques en petit ... -->
  <script src="vendor/plugins/highcharts/highcharts.js"></script>
  <-- plugin pour les graphiques... -->
  <script src="vendor/plugins/circles/circles.js"></script>
  <-- plugin pour les cercles... -->
  <script src="vendor/plugins/jvectormap/jquery.jvectormap.min.js"></script>
  <script src="vendor/plugins/jvectormap/assets/jquery-jvectormap-us-lcc-en.js"></script>
  <-- plugin pour cartes/map -->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
  <-- validation inputs -->
  <script src="assets/admin-tools/admin-forms/js/jquery.steps.min.js"></script>
  <-- pour mettre des étapes sur un formulaire -->
  <script src="vendor/plugins/mask/jquery.mask.js"></script>
  <-- plugin pour les masques formulaires -->
  <script src="vendor/plugins/tagsinput/tagsinput.min.js"></script>
  <-- PAS MAL: pour une liste de tags -->
  <script src="vendor/plugins/holder/holder.min.js"></script>
  <-- pour mettre des images de tests -->
  <script src="vendor/plugins/footable/js/footable.all.min.js"></script>
  <script src="vendor/plugins/date/date.js"></script>
  <script src="vendor/plugins/footable/js/footable.filter.min.js"></script>
  <script src="assets/admin-tools/admin-forms/js/jquery-ui-monthpicker.min.js"></script>
  <script src="assets/admin-tools/admin-forms/js/jquery-ui-datepicker.min.js"></script>
  <script src="assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
  <script src="assets/admin-tools/admin-forms/js/jquery.stepper.min.js"></script>
  <!-- Theme Javascript -->
  <script src="assets/js/custom.js"></script>
  <script src="assets/js/responsive-tabs.js"></script>
  <script type="text/javascript">
  (function ($) {
    fakewaffle.responsiveTabs(['xs', 'sm', 'md']);
  })(jQuery);
  jQuery(document).ready(function () {

    $('.admin-panels').adminpanel({
      grid: '.admin-grid',
      draggable: true,
      mobile: false,
      callback: function () {
        bootbox.confirm('<h3>A Custom Callback!</h3>', function () {
        });
      },
      onFinish: function () {
        $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onload');

        // Init Demo settings
        $('#p0 .panel-control-color').click();

        // Init Demo settings
        $('#p1 .panel-control-title').click();

        // Create an example admin panel filter
        $('#admin-panel-filter a').on('click', function () {
          var This = $(this);
          var Value = This.attr('data-filter');

          // Toggle any elements whos name matches
          // that of the buttons attr value
          $('.admin-filter-panels').find($(Value)).each(function (i, e) {
            if (This.hasClass('active')) {
              $(this).slideDown('fast').removeClass('panel-filtered');
            } else {
              $(this).slideUp().addClass('panel-filtered');
            }
          });
          This.toggleClass('active');
        });

      },
      onSave: function () {
        $(window).trigger('resize');
      }
    });
    // changer l'image
    function readURL1(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#image_upload_preview1').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#inputFile1").change(function () {
      readURL1(this);
    });


    $('.footable').footable();
    // select list dropdowns - placeholder like creation
    var selectList = $('.admin-form select');
    selectList.each(function (i, e) {
      $(e).on('change', function () {
        if ($(e).val() == "0") $(e).addClass("empty");
        else $(e).removeClass("empty")
      });
    });
    selectList.each(function (i, e) {
      $(e).change();
    });

    // Init TagsInput plugin
    $("input#tagsinput").tagsinput({
      tagClass: function (item) {
        return 'label bg-primary light';
      }
    });



    /* @custom validation method (smartCaptcha)
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function (value, element, param) {
      return value == param;
    };

    $('#uti_cp').mask('00 000');
    $('#uti_date_entree').mask('00/00/0000');
    $('#uti_date_sortie').mask('00/00/0000');
    $('#uti_tel').mask('00 00 00 00 00');
    $('#uti_fax').mask('00 00 00 00 00');
    $('#uti_compta_tel').mask('00 00 00 00 00');
    $('#timepicker1').mask('12h34', {
      'translation': {
        1: {pattern: /[0-2]/},
        2: {pattern: /[0-9]/},
        3: {pattern: /[0-5]/},
        4: {pattern: /[0-9]/}
      },
    });
    $('#timepicker2').mask('12h34', {
      'translation': {
        1: {pattern: /[0-2]/},
        2: {pattern: /[0-9]/},
        3: {pattern: /[0-5]/},
        4: {pattern: /[0-9]/}
      },
    });
    $('#timepicker3').mask('12h34', {
      'translation': {
        1: {pattern: /[0-2]/},
        2: {pattern: /[0-9]/},
        3: {pattern: /[0-5]/},
        4: {pattern: /[0-9]/}
      },
    });
    $('#timepicker4').mask('12h34', {
      'translation': {
        1: {pattern: /[0-2]/},
        2: {pattern: /[0-9]/},
        3: {pattern: /[0-5]/},
        4: {pattern: /[0-9]/}
      },
    });
    $('#uti_compta_fax').mask('00 00 00 00 00');
    $('#uti_capital').mask('00000000000', {
      'translation': {
        0: {pattern: /[0-9]/}
      },

    });
    $('#uti_interco').mask('00000000000', {
      'translation': {
        0: {pattern: /[0-9]/}
      },

    });

    $('#uti_nom').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
      'translation': {
        A: {pattern: /[A-Z" "a-z0-9]/}
      },
      onKeyPress: function (value, event) {
        event.currentTarget.value = value.toUpperCase();
      }
    });
    $('#uti_code').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
      'translation': {
        A: {pattern: /[A-Za-z0-9]/}
      }
    });
    $('#uti_ville').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
      'translation': {
        A: {pattern: /[A-Z" "a-z-]/}
      },
      onKeyPress: function (value, event) {
        event.currentTarget.value = value.toUpperCase();
      }
    });


    $("#admin-form").validate({

      /* @validation states + elements
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules
      ------------------------------------------ */

      rules: {
        uti_cp: {
          required: true,
          minlength: 6,
          maxlength: 6

        },
        uti_nom: {
          required: true
        },
        timepicker1: {
          required: true,
          minlength: 5,
          maxlength: 5
        },
        timepicker2: {
          required: true,
          minlength: 5,
          maxlength: 5
        },
        timepicker3: {
          required: true,
          minlength: 5,
          maxlength: 5
        },
        timepicker4: {
          required: true,
          minlength: 5,
          maxlength: 5
        },
        uti_compta_tel: {
          required: true,
          minlength: 14,
          maxlength: 14
        },
        uti_compta_fax: {
          required: true,
          minlength: 14,
          maxlength: 14
        },
        uti_tel: {
          required: true,
          minlength: 14,
          maxlength: 14
        },
        uti_fax: {
          required: true,
          minlength: 14,
          maxlength: 14
        }


      },

      /* @validation error messages
      ---------------------------------------------- */

      messages: {
        uti_compta_tel: {
          minlength: "Veuillez entrer un numéro de téléphone valide."
        },
        uti_compta_fax: {
          minlength: "Veuillez entrer un numéro de téléphone valide."
        },
        uti_tel: {
          minlength: "Veuillez entrer un numéro de téléphone valide."
        },
        uti_fax: {
          minlength: "Veuillez entrer un numéro de téléphone valide."
        },
        timepicker1: {
          minlength: "Veuillez entrer l'heure en respectant la forme XXhXX."
        },
        timepicker2: {
          minlength: "Veuillez entrer l'heure en respectant la forme XXhXX."
        },
        timepicker3: {
          minlength: "Veuillez entrer l'heure en respectant la forme XXhXX."
        },
        timepicker4: {
          minlength: "Veuillez entrer l'heure en respectant la forme XXhXX."
        },
        uti_cp: {
          minlength: "Veuillez entrer un code postal valide."
        }


      },


      /* @validation highlighting + error placement
      ---------------------------------------------------- */

      highlight: function (element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function (error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });


  });

  </script>

  <div id="ui-monthpicker-div" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>
  <!-- END: PAGE SCRIPTS -->
</body>
</html>
