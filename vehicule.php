<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "modeles/mod_agence.php";

	// LA PERSONNE LOGUE
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		if(!empty($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];
		}
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		if(!empty($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];
		}
	}

	// LE Vehicule

	$erreur=0;
	$veh_id=0;
	$veh_code="";
	$veh_libelle="";
	$veh_couleur="#ffffff";
	$veh_societe=$acc_societe;
	$veh_agence=$acc_agence;
	$veh_archive=0;
	$veh_immat="";

	if(isset($_GET["vehicule"])){
		if(!empty($_GET["vehicule"])){
			$sql="SELECT * FROM Vehicules WHERE veh_id=:vehicule;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":vehicule",$_GET["vehicule"]);
			$req->execute();
			$vehicule=$req->fetch();
			if(!empty($vehicule)){
				$veh_id=$vehicule["veh_id"];
				$veh_code=$vehicule["veh_code"];
				$veh_libelle=$vehicule["veh_libelle"];
				$veh_couleur=$vehicule["veh_couleur"];
				$veh_societe=$vehicule["veh_societe"];
				$veh_agence=$vehicule["veh_agence"];
				$veh_archive=$vehicule["veh_archive"];
				$veh_immat=$vehicule["veh_immat"];
			};
		};
	};

	$sql="SELECT soc_agence FROM Societes WHERE soc_id=4";
	$req=$Conn->prepare($sql);
	$req->bindParam(":veh_societe",$veh_societe);
	$req->execute();
	$societe=$req->fetch();



?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion - Véhicules</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="vehicule_enr.php" >
			<div>
				<input type="hidden" name="vehicule" id="vehicule" value="<?=$veh_id?>" >
				<input type="hidden" name="veh_societe" id="veh_societe" value="<?=$veh_societe?>" >
			</div>
			<div id="main">
      <?php		include "includes/header_def.inc.php";  ?>
				<section id="content_wrapper" >


					<section id="content" class="animated fadeIn">

						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											<div class="text-center">
												<div class="content-header">
													<h2>
										<?php 			if(isset($veh_id)){
															if($veh_id>0){
																echo("Editer un véhicule");
															}else{
																echo("Ajouter un <b class='text-primary'>véhicule</b>");
															};
														} ?>
													</h2>
												</div>
												<div class="col-md-10 col-md-offset-1">
													<div class="row">
														<div class="col-md-12">
															<div class="section-divider mb40">
																<span>Informations générales</span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-3">
															<div class="section">
																<label for="veh_code" >Code :</label>
																<div class="field prepend-icon">
																	<input type="text" name="veh_code" id="veh_code" class="gui-input" placeholder="Code" size="4" maxlength="4" value="<?=$veh_code?>" required >
																	<span for="veh_code" class="field-icon">
																		<i class="fa fa-bookmark" aria-hidden="true"></i>
																	</span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="section">
																<label for="veh_libelle" >Libellé :</label>
																<div class="field prepend-icon">
																	<input type="text" name="veh_libelle" id="veh_libelle" class="gui-input" placeholder="Libellé" size="50" maxlength="50" value="<?=$veh_libelle?>" required >
																	<span for="veh_libelle" class="field-icon">
																		<i class="fa fa-bookmark" aria-hidden="true"></i>
																	</span>
																</div>
															</div>
														</div>
														<div class="col-md-3">
															<div class="section">
																<label for="veh_immat" >Immatriculation :</label>
																<div class="field prepend-icon">
																	<input type="text" name="veh_immat" id="veh_immat" class="gui-input" placeholder="Immatriculation" value="<?=$veh_immat?>" >
																	<span for="veh_immat" class="field-icon">
																		<i class="fa fa-bookmark" aria-hidden="true"></i>
																	</span>
																</div>
															</div>
														</div>

													</div>
													<div class="row" >
														<div class="col-md-6" >
															<div class="form-group">
																<div class="section">
																<label class="field sfcolor">
																  <input type="text" name="veh_couleur" id="colorpicker" class="gui-input" placeholder="Couleur" value="<?=$veh_couleur?>" >
																</label>
															  </div>
															</div>
														</div>
												<?php 	if($societe["soc_agence"]){ ?>
															<div class="col-md-6" >
																<label class="field select">
																	<select id="veh_agence" name="veh_agence">
																		<option value="">Agence ...</option>
																		<?=get_agence_select($veh_agence, $veh_societe)?>
																	</select>
																	<i class="arrow"></i>
																</label>
															</div>
												<?php	}; ?>
													</div>
											<?php	if($veh_id>0){  ?>
														<div class="row" >
															<div class="col-md-6" >
																<div class="option-group field">
																	<label class="option option-dark">
																		<input type="checkbox" name="veh_archive" value="veh_archive" <?php if(!empty($veh_archive)) echo("checked"); ?> >
																		<span class="checkbox"></span>Archivé
																	</label>
																</div>
															</div>
														</div>
											<?php	} ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>

				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="vehicule_liste.php?archive=<?=$veh_archive?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" name="submit" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {

				var cPicker2 = $("#colorpicker");

				var cContainer2 = cPicker2.parents('.sfcolor').parent();

				$(cContainer2).addClass('posr');

				$("#colorpicker").spectrum({
					color: bgPrimary,
					appendTo: cContainer2,
					containerClassName: 'sp-left',
					showInput: true,
					showPalette: false,
					showInitial: true,
					preferredFormat: "hex",
					cancelText: "Fermer",         //Texte pour le bouton "cancel"
					chooseText: "Sélectionner",
					//color:<?=$veh_couleur?>,
				});
				var couleur=$("#colorpicker").val();
				console.log("couleur : " . couleur);
				$("#colorpicker").spectrum("set", couleur);

				//$("#colorpicker").val(couleur);
				$("#colorpicker").show();
			});
		</script>
	</body>
</html>
