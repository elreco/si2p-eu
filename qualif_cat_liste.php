<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");

include_once "modeles/mod_qualification.php";

$filt_qualif=0;
$filt_archive=0;
if(!empty($_POST)){
	if(!empty($_POST["filt_qualif"])){
		$filt_qualif=intval($_POST["filt_qualif"]);
	}
	$_SESSION["filt_qualif"]=$filt_qualif;
	
	$filt_archive=0;
	if(!empty($_POST["filt_archive"])){
		$filt_archive=1;
	}
	$_SESSION["filt_archive"]=$filt_archive;
	
}elseif(isset($_SESSION["filt_qualif"])){
	$filt_qualif=intval($_SESSION["filt_qualif"]);
	$filt_archive=intval($_SESSION["filt_archive"]);
}

// LES QUALIF POUR FILTRE
$qualifications = get_qualifications();


$sql="SELECT * FROM Qualifications_Categories INNER JOIN Qualifications ON (Qualifications_Categories.qca_qualification=Qualifications.qua_id)";
if(!empty($filt_archive)){
	$sql.="	WHERE qca_archive";
}else{
	$sql.="	WHERE NOT qca_archive";
}
if(!empty($filt_qualif)){
	$sql.="	AND qca_qualification=" . $filt_qualif;
}

$sql.=" ORDER BY qua_libelle,qca_libelle;";
$req=$Conn->query($sql);
$d_categories=$req->fetchAll();

// LES COMPETENCES

$d_competences=array();
$sql="SELECT com_id,com_libelle FROM Competences ORDER BY com_libelle;";
$sql.=" ORDER BY qua_libelle,qca_libelle;";
$req=$Conn->query($sql);
$d_resultats=$req->fetchAll();
if(!empty($d_resultats)){
	foreach($d_resultats as $r){
		$d_competences[$r["com_id"]]=$r["com_libelle"];
	}
}

// LES COMPETENCES


?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper">
			
				<section id="content" class="animated fadeIn">
				
					<h1>Liste des catégories de qualification</h1>
					
					<div class="row" >
						<div class="col-md-6 col-md-offset-3" >
							<div class="panel" >
								 <div class="panel-heading panel-head-sm">Filtrer</div>
								<div class="panel-body">
									<form method="post" action="#" >
										<div class="admin-form theme-primary ">
											<div class="row" >
												<div class="col-md-6" >
													<span class="field select">
														<select id="filt_qualif" name="filt_qualif" >
															<option value="">Qualification...</option>
													<?php 	foreach($qualifications as $q){
																if($q["qua_id"]==$filt_qualif){
																	echo("<option value='" . $q["qua_id"] . "' selected >" . $q["qua_libelle"] . "</option>");
																}else{
																	echo("<option value='" . $q["qua_id"] . "' >" . $q["qua_libelle"] . "</option>");
																} 
															} ?>
														</select>
														<i class="arrow simple"></i>
													</span>
												</div>
												<div class="col-md-4 pt15" >
													<label class="option option-dark">
														<input type="checkbox" name="filt_archive" value="on" <?php if($filt_archive==1) echo("checked") ?> />
														<span class="checkbox"></span>Catégories archivées
													</label>
												</div>
												<div class="col-md-2" >
													<button type="submit" class="btn btn-primary" >
														<i class="fa fa-search" ></i> Filtrer
													</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
			<?php	if(!empty($d_categories)){ ?>
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<thead>
									<tr class="dark" >
										<th>Qualification</th>
										<th>Nom</th>
										<th>Descriptif</th>
										<th>UT</th>
										<th>Compétence</th>
										<th colspan="9" class="text-center" >Options</th>							
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
						<?php 		foreach ($d_categories as $cat){ ?>
										<tr>
											<td><?= $cat['qua_libelle'] ?></td>
											<td><?= $cat['qca_libelle'] ?></td>
											<td><?= $cat['qca_nom'] ?></td>
											<td><?= $cat['qca_ut'] ?></td>
											<td>
										<?php	if(!empty($cat['qca_competence'])){
													echo($d_competences[$cat['qca_competence']]);
												}else{
													echo("&nbsp;");
												} ?>
											</td>
									<?php	for($bcl=1;$bcl<=3;$bcl++){
												if(!empty($cat["qca_opt_" . $bcl])){ ?>
													<td><?=$cat['qua_opt_' . $bcl]?></td>
									<?php			if(empty($cat["qca_ut_opt_" . $bcl])){ ?>
														<td colspan="2" >incluse dans la catégorie</td>
									<?php			}else{ ?>
														<td><?=$cat["qca_ut_opt_" . $bcl]?> UT</td>
														<td>
													<?php	if(!empty($cat['qca_comp_opt_' . $bcl])){
																echo("Comp. requise <b>" . $d_competences[$cat['qca_comp_opt_' . $bcl]] . "</b>");
															}else{
																echo("&nbsp;");
															} ?>
														</td>
									<?php			}
												}else{
													echo("<td colspan='3' >&nbsp;</td>");
												} 
											} ?>
											<td class="text-center" >
												<a href="qualif_cat.php?cat=<?= $cat['qca_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier cette catégorie">
													<span class="fa fa-pencil"></span>
												</a>
											</td>
										</tr>
						<?php 		} ?>
								</tbody>
							</table>
						</div>
			<?php 	}else{ ?>
						<p class="alert alert-warning" >
							Aucune catégorie n'est associée à la qualification selectionnée.
						</p>
			<?php	} ?>
						
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-long-arrow-left"></span>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="qualif_cat.php?qualification=<?=$filt_qualif?>" class="btn btn-success btn-sm" role="button" >
						<span class="fa fa-plus"></span>
						Nouvelle catégories
					</a>
				</div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>	
	</body>
</html>