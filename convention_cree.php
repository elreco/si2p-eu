 <?php
 
	include "includes/controle_acces.inc.php";	
	
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");
	
	include_once("modeles/mod_planning_periode_lib.php");
	include_once("modeles/mod_planning_periode.php");
	

	
	// GENERE UN CONVENTION
	
	$erreur_txt="";
	$warning_txt="";
	
	// element du form obligatoire
	
	$action_client_id=0;
	if(isset($_GET["action_client"])){
		if(!empty($_GET["action_client"])){
			$action_client_id=intval($_GET["action_client"]);
		}	
	}

	if(empty($action_client_id)){
		
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => "Formulaire incomplet!" 
		);
		header("location: action_tri.php");
		die();
		
	}else{

		/******************************************
					CONTROLE
		******************************************/

		// DONNEE ACTION CLIENT

		$sql="SELECT acl_facturation,acl_action,acl_produit,acl_client,acl_ca,acl_pro_inter,acl_ca_gfc
		,act_agence,act_date_deb FROM Actions_Clients,Actions WHERE acl_action=act_id AND acl_id=:action_client_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_client_id",$action_client_id);
		$req->execute();
		$d_action_client=$req->fetch();
		if(empty($d_action_client)){
			$erreur_txt="Les informations n'ont pas pu être chargées.";
		}else{
			
			$con_annee=date("y");
			if(!empty($d_action_client["act_date_deb"])){
				$DT_periode=date_create_from_format('d/m/Y',$d_action_client["act_date_deb"]);
				if(!is_bool($DT_periode)){
					$con_annee=$DT_periode->format("y");
				}
			}
			if($d_action_client["acl_facturation"]==1){
				$con_ht=$d_action_client["acl_ca_gfc"];
			}else{
				$con_ht=$d_action_client["acl_ca"];
			}
		}
	}
	
	// DONNEE PRODUIT
	if(empty($erreur_txt)){
		
		$sql="SELECT pro_deductible,pro_tva FROM Produits WHERE pro_id=:pro_id;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":pro_id",$d_action_client["acl_produit"]);
		$req->execute();
		$d_produit=$req->fetch();
		if(empty($d_produit)){
			$erreur_txt="Les informations n'ont pas pu être chargées.";
		}
		
	}
	
	
	if(empty($erreur_txt)){
	
		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];  
		}
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
		}
		if($conn_soc_id!=$acc_societe){
			$acc_societe=$conn_soc_id;
		}
		// Note si une convention GC referencement est généré pour une action GN par Josiane depuis GFC -> il faut que la convention sorte en GN

		// SOC DE LA CONVENTION
		if($d_action_client["acl_facturation"]==1){
			
			// FACTURATION GFC
			$con_societe=4;
			$con_agence=4;
			
		}else{
			$con_societe=$acc_societe;
			$con_agence=$d_action_client["act_agence"];
		}
		
		// DONNEE SOC
		
		$sql="SELECT soc_code,soc_tva_exo FROM Societes WHERE soc_id=:con_societe;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":con_societe",$con_societe);
		$req->execute();
		$d_societe=$req->fetch();
		if(!empty($d_societe)){
			if($con_societe!=$conn_soc_id){
				// ENREGISTREMENT D'UNE CONVENTION GFC SUR UNE BASE REGION			
				$con_numero="N°" . $d_societe["soc_code"] . "-" . $con_annee . "-" . $acc_societe . "." . $action_client_id;
			}else{
				$con_numero="N°" . $d_societe["soc_code"] . "-" . $con_annee . "-" . $action_client_id;
			}
		}else{
			$erreur_txt="Les informations n'ont pas pu être chargées.";
		}
	}

	if(empty($erreur_txt)){	
		// SUR LE CLIENT
		$sql="SELECT cli_categorie FROM Clients WHERE cli_id=:client;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":client",$d_action_client["acl_client"]);
		$req->execute();
		$d_client=$req->fetch();
		
	
		// ADRESSE D'INTERVENTION PAR DEFAUT (OU FAC SI PARTICULIER)
		
		$sql="SELECT * FROM Adresses WHERE adr_ref=1 AND adr_ref_id=:adr_ref_id AND adr_defaut";
		if($d_client["cli_categorie"]==3){
			$sql.=" AND adr_type=2";
		}else{
			$sql.=" AND adr_type=1";
		}
		$sql.=";";
		$req=$Conn->prepare($sql);
		$req->bindParam(":adr_ref_id",$d_action_client["acl_client"]);
		$req->execute();
		$d_adresse=$req->fetch();
		
		// UNITE D EREFERENCE

		$con_qte=0;
		if($d_action_client["acl_pro_inter"]){
			$con_unite=5; // stagiaire
		}else{
			$con_unite=2;	// jour
		}

		// CALCUL DE LA QUANTITE
		
		if($con_unite==5){
			
			$con_qte=1;
			$con_unite_txt="stagiaire";
			
			$sql="SELECT COUNT(ast_stagiaire) FROM Actions_Stagiaires WHERE ast_action_client=:action_client_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client_id",$action_client_id);
			$req->execute();
			$d_stagiaires=$req->fetch();
			if(!empty($d_stagiaires)){
				if($d_stagiaires[0]>1){
					$con_qte=$d_stagiaires[0];
					$con_unite_txt="stagiaires";
				}
			}
		}
		
		// CALCUL DES DATES

		$con_form_dates="";
		$sql="SELECT pda_date,pda_demi FROM Plannings_Dates,Actions_Clients_Dates WHERE pda_id=acd_date AND acd_action_client=:action_client_id
		ORDER BY pda_date,pda_demi;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_client_id",$action_client_id);
		$req->execute();
		$d_dates=$req->fetchAll();
		if(!empty($d_dates)){
			
			if(count($d_dates)>2){
				$con_form_duree=count($d_dates)/2 . " jours";	
			}else{
				$con_form_duree=count($d_dates)/2 . " jour";
			}
			
			if($con_unite==2){
				$con_qte=count($d_dates)/2;
				if($con_qte>1){
					$con_unite_txt="jours";
				}else{
					$con_unite_txt="jour";
				}
			}
			$con_form_dates=planning_periode($d_dates);
		}
		
		
		
		// CALCUL DE LA TVA
		
		if(($d_societe["soc_tva_exo"] AND $d_produit["pro_deductible"]) OR ($d_adresse["adr_geo"]!=1)){
			$con_tva_id=3;
			$con_tva_periode=3;
			$con_tva_taux=0;
		}else{
			
			
			
			$con_tva_id=$d_produit["pro_tva"];
			
			/* echo($con_tva_id);
			die(); */
			
			$sql="SELECT tpe_id,tpe_taux FROM Tva_Periodes WHERE tpe_date_deb<=:act_date_deb AND (tpe_date_fin>=:act_date_deb OR ISNULL(tpe_date_fin))  AND tpe_tva=:tva_id;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":tva_id",$con_tva_id);
			$req->bindParam(":act_date_deb",$d_action_client["act_date_deb"]);
			$req->execute();
			$d_tva=$req->fetch();
			if(!empty($d_tva)){
				$con_tva_periode=$d_tva["tpe_id"];
				$con_tva_taux=$d_tva["tpe_taux"];
			}else{
				$erreur_txt="Impossible de calculer la TVA!";
			}		
		}
		
		// COUT TTC
		$con_ttc=$con_ht + ($con_ht * ($con_tva_taux/100));
		$con_ttc=round($con_ttc,2);
	}
	
	if(empty($erreur_txt)){
	
		// CREATION
		
		$sql="INSERT INTO Conventions (
		con_action,con_action_client,con_client,con_societe,con_agence,con_numero,con_cli_adresse,con_cli_nom,con_cli_service,con_cli_ad1,con_cli_ad2,con_cli_ad3,con_cli_cp,con_cli_ville,con_cli_geo
		,con_form_dates,con_form_duree,con_form_effectif,con_form_effectif_text,con_unite,con_unite_txt,con_qte,con_ht,con_tva_id,con_tva_periode,con_tva_taux,con_ttc,con_date) VALUES (
		:con_action,:con_action_client,:con_client,:con_societe,:con_agence,:con_numero,:con_cli_adresse,:con_cli_nom,:con_cli_service,:con_cli_ad1,:con_cli_ad2,:con_cli_ad3,:con_cli_cp,:con_cli_ville,:con_cli_geo
		,:con_form_dates,:con_form_duree,:con_form_effectif,:con_form_effectif_text,:con_unite,:con_unite_txt,:con_qte,:con_ht,:con_tva_id,:con_tva_periode,:con_tva_taux,:con_ttc,NOW())";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":con_action",$d_action_client["acl_action"]);
		$req->bindParam(":con_action_client",$action_client_id);
		$req->bindParam(":con_client",$d_action_client["acl_client"]);
		$req->bindParam(":con_societe",$con_societe);
		$req->bindParam(":con_agence",$con_agence);
		$req->bindParam(":con_numero",$con_numero);
		$req->bindParam(":con_cli_adresse",$d_adresse["adr_id"]);
		$req->bindParam(":con_cli_nom",$d_adresse["adr_nom"]);
		$req->bindParam(":con_cli_service",$d_adresse["adr_service"]);
		$req->bindParam(":con_cli_ad1",$d_adresse["adr_ad1"]);
		$req->bindParam(":con_cli_ad2",$d_adresse["adr_ad2"]);
		$req->bindParam(":con_cli_ad3",$d_adresse["adr_ad3"]);
		$req->bindParam(":con_cli_cp",$d_adresse["adr_cp"]);
		$req->bindParam(":con_cli_ville",$d_adresse["adr_ville"]);
		$req->bindParam(":con_cli_geo",$d_adresse["adr_geo"]);		
		$req->bindParam(":con_form_dates",$con_form_dates);
		$req->bindParam(":con_form_duree",$con_form_duree);
		$req->bindValue(":con_form_effectif",3);
		$req->bindValue(":con_form_effectif_text","");
		$req->bindParam(":con_unite",$con_unite);
		$req->bindParam(":con_unite_txt",$con_unite_txt);
		$req->bindParam(":con_qte",$con_qte);
		$req->bindParam(":con_ht",$con_ht);
		$req->bindParam(":con_tva_id",$con_tva_id);
		$req->bindParam(":con_tva_periode",$con_tva_periode);
		$req->bindParam(":con_tva_taux",$con_tva_taux);
		$req->bindParam(":con_ttc",$con_ttc);
		try{
			$req->execute();
			$con_id=$ConnSoc->lastInsertId(); 				
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}
	}
		
	if(!empty($erreur_txt)){
	
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		if(!empty($action_id)){
			header("location: action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id);
			die();
		}else{
			header("location: action_tri.php");
			die();
		}
		
	}else{
		
		if(!empty($warning_txt)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "warning",
				"message" => $warning_txt
			);
		}
		header("location: convention_voir.php?convention=" . $con_id . "&societ=" . $conn_soc_id);
		die();
	}
?>