<?php

/* variable pour la nav (active) */
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
/* include pour chaque fichier */
include_once 'modeles/mod_produit.php';
include_once 'modeles/mod_famille.php';
include_once 'modeles/mod_sous_famille.php';
include_once 'modeles/mod_qualification.php';
if(isset($_GET['id'])){
    $p = get_produit_modele($_GET['id']);
}

?>
<!DOCTYPE html>
<html>
<!-- Mirrored from admindesigns.com/demos/absolute/1.1/layout_horizontal-sm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 09:52:52 GMT -->
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Créer un modèle de produit</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!--
       <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
       <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
   -->
   <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
   <!-- Favicon -->
   <link rel="shortcut icon" href="assets/img/favicon.png">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<?php
include "includes/header_def.inc.php";
?>

<body class="sb-top sb-top-sm ">
    <section id="content_wrapper">
        <header id="topbar" >
          <div class="topbar-left">
            <ol class="breadcrumb">
              <li>
                <span class="glyphicon glyphicon-cog"></span>
                <a href="parametre.php"> Paramètres</a>
            </li>
            <li>
                <a href="produit_modele.php">Modèles de produits</a>
            </li>
            <li class='crumb-trail'><?php if(isset($_GET['id'])): ?>Editer<?php else: ?>Ajouter<?php endif; ?> un produit</li>


        </ol>
    </div>
</header>
<form class="form-horizontal admin-form" action="produit_modele_enr.php" method="POST" role="form">
    <section id="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel heading-border panel-primary">
                    <div class="panel-body bg-light">
                      <div class="text-center">
                        <div class="content-header">
                            <h2><?php if(isset($_GET['id'])): ?>Editer<?php else: ?>Ajouter<?php endif; ?> un <b class="text-primary">modèle</b></h2>                      
                        </div>
                        <?php if(isset($_GET['id'])): ?>
                            <input type="hidden" name="pmo_id" value="<?= $p['pmo_id'] ?>">
                        <?php endif; ?>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="section">
                              <div class="field prepend-icon">
                                <input type="text" name="pmo_nom" id="pmo_nom" class="gui-input" placeholder="Nom" required="" <?php if(isset($_GET['id'])): ?> value="<?= $p['pmo_nom'] ?>" <?php else: ?> value="" <?php endif; ?>>
                                <label for="soc_nom" class="field-icon">
                                  <i class="fa fa-tag"></i>
                              </label>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                    <div class="section">
                      <div class="field select">
                          <select name="pmo_famille" id="pmo_famille" onchange="sous_famille(this.value)"">
                              <option value="0">Famille ...</option>
                              <?php if (isset($_GET['id'])):
                              echo get_famille_select($p['pmo_famille']);
                              else:
                                echo get_famille_select(0);
                            endif;?>


                        </select>
                        <i class="arrow simple"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section">
                  <div class="field select">
                    <select name="pmo_sous_famille" id="pro_sous_famille" >
                      <option value="0">Sous-famille ...</option>
                      <?php if (isset($_GET['id'])):
                      echo get_sous_famille_select($p['pmo_famille'], $p['pmo_sous_famille']);
                      endif; ?>


                  </select>
                  <i class="arrow simple"></i>
              </div>
          </div>
      </div>
      <div class="col-md-6">
          <div class="section">
            <div class="field select">
                <select name="pmo_qualification" id="pmo_qualification" onchange="qualification(this.value)"  required="">
                    <option value="">Qualification ...</option>
                    <?php if (isset($_GET['id'])):
                    echo get_qualification_select($p['pmo_qualification']);
                    else:
                      echo get_qualification_select(0);
                  endif; ?>


              </select>
              <i class="arrow simple"></i>
          </div>
      </div>
  </div>

</div>

</div>



</div>

</div>
</div>
</div>
</section>
</section>


<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left">
            <a href="produit_modele.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle"></div>
        <div class="col-xs-3 footer-right">

            <button type="submit" <?php if(isset($_GET['id'])): ?> name="edit" <?php else: ?> name="add" <?php endif; ?> class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Enregistrer</button>

        </div>
    </div>
</footer>
</form>
<?php
include "includes/footer_script.inc.php"; 
?>	
<script src="vendor/plugins/mask/jquery.mask.js"></script>
<script src="assets/js/custom.js"></script>
<script type="text/javascript">
   function sous_famille(selected_id){
    //-----------------------------------------------------------------------
    // 2) Send a http request with AJAX http://api.jquery.com/jQuery.ajax/
    //-----------------------------------------------------------------------

    $.ajax({
      type:'POST',
      url: 'ajax/ajax_sous_famille.php',
      //the script to call to get data
      data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
      //for example "id=5&parent=6"

      dataType: 'json',                //data format
      success: function(data)          //on recieve of reply
      {

        if (data['0'] == undefined){

          $("#pro_sous_famille").find("option:gt(0)").remove();

      }else{
          $("#pro_sous_famille").find("option:gt(0)").remove();
          $.each(data, function(key, value) {
            $('#pro_sous_famille')
            .append($("<option></option>")
              .attr("value",value["psf_id"])
              .text(value["psf_libelle"]));
        });
      }
  }
});
}
</script>
</body>
</html>
