<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');
// si la note de frais est déjà créée
if(!empty($_POST['nav'])){
    // MODIFIER L'AVANCE
    $nav_date = convert_date_sql($_POST['nav_date']);

    $req = $Conn->prepare("UPDATE ndf_avances SET nav_date = :nav_date, nav_ttc = :nav_ttc, nav_commentaire = :nav_commentaire WHERE nav_id = :nav_id");
    $req->bindParam(':nav_id', $_POST['nav']);
    $req->bindParam(':nav_date', $nav_date);
    $req->bindParam(':nav_ttc', $_POST['nav_ttc']);
    $req->bindParam(':nav_commentaire', $_POST['nav_commentaire']);
    $req->execute();

}else{
    // INSERER L'AVANCE'
    $nav_date = convert_date_sql($_POST['nav_date']);
    $req = $Conn->prepare("INSERT INTO ndf_avances (nav_ndf, nav_utilisateur, nav_date, nav_ttc, nav_commentaire) VALUES (:nav_ndf, :nav_utilisateur, :nav_date, :nav_ttc, :nav_commentaire)");
    $req->bindParam(':nav_ndf', $_POST['id']);
    $req->bindParam(':nav_utilisateur', $_POST['utilisateur']);
    $req->bindParam(':nav_date', $nav_date);
    $req->bindParam(':nav_ttc', $_POST['nav_ttc']);
    $req->bindParam(':nav_commentaire', $_POST['nav_commentaire']);
    $req->execute();


}
Header("Location: ndf_liste.php");
die();