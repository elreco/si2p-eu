<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_compte.php');
	include('modeles/mod_erreur.php');
	
	$filt_numero="";
	if(isset($_POST["filt_numero"])){
		$filt_numero=$_POST["filt_numero"];
		$_SESSION['filt_numero']=$filt_numero;
	}elseif(!empty($_SESSION['filt_numero'])){
		$filt_numero=$_SESSION['filt_numero'];
	};
	
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link href="assets/admin-tools/admin-forms/css/admin-forms-orion.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php	
				include "includes/header_def.inc.php";
			?>	
				
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
							
				<section id="content" class="animated fadeIn">
				
					<div class="row mb10">
						<div class="col-md-6 col-md-offset-3">
							<div class="admin-form theme-primary">
								<form action="compte_liste.php" method="post" id="admin-form">
									<div class="col-md-10">
										<div class="form-group">
											<label for="filt_numero" class="sr-only" >Compte</label>
										<div>
										<input type="text" class="form-control" name="filt_numero" id="filt_numero" placeholder="Filtrer sur les numéros de compte" value="<?=$filt_numero?>" >
										</div>
										</div>		
									</div>	
									
									<div class="col-md-2">
										<button type="submit" class="btn btn-primary btn-sm" >
											<i class="fa fa-search"></i>										
										</button>
									
									</div>
								</form>
							</div>
						</div>
					</div>
					
					<div class="row" >
						<div class="col-md-12" >
							<div class="table-responsive" >
								<table class="table table-hover table-striped" >
									<thead>
										<tr class="dark" >
											<th>Compte</th>
											<th>Nom</th>										
											<th>&nbsp;</th>
										</tr>															
									</thead>
									<tbody>
								<?php	$donnees=get_comptes($filt_numero);																					
										if(!empty($donnees)){										
											foreach($donnees as $value){  ?>
												<tr>
													<td><?=$value["cpt_numero"]?></td>
													<td><?=$value["cpt_libelle"]?></td>																																			
													<td class="text-center" >
														<span data-toggle="modal" data-target="#formEdit" >
															<a href="#" class="btn btn-warning btn-xs open-formEdit" data-id="<?=$value["cpt_id"]?>" data-numero="<?=$value["cpt_numero"]?>" data-libelle="<?=$value["cpt_libelle"]?>" data-toggle="tooltip" data-placement="bottom" title="Modifier" >
																<i class="fa fa-pencil"></i>
															</a>
														</span>
													</td>									
												</tr>	
								<?php		
											};
										}; ?>																			
									</tbody>
								</table>
							</div>
						</div>
					</div>
	
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >
				</div>
				<div class="col-xs-3 footer-right" >
					<button class="btn btn-success btn-sm open-formEdit" data-toggle="modal" data-target="#formEdit" data-id="0" data-numero="" data-libelle="" >
						<i class="fa fa-plus" ></i>
						Nouveau Compte
					</button>
				</div>
			</div>
		</footer>
									
		<div id="formEdit" class="modal fade" role="dialog">
			
			
			<div class="modal-dialog admin-form theme-primary">
				<div class="modal-content">
				
					<form method="post" action="compte_enr.php" >
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title" >Nouveau Compte</h4>
						</div>
						<div class="modal-body">
						
							<div>
								<input type="hidden" name="cpt_id" id="cpt_id" value="0" >
							</div>
							
							<div class="row" >
								<div class="col-md-12" >
								
									<div class="form-group">
										<label for="cpt_numero" class="sr-only" >Compte</label>
										<div>
											<input type="text" class="form-control" name="cpt_numero" id="cpt_numero" placeholder="Compte" required >
										</div>
									</div>
								</div>																
								<div class="col-md-12 text-center" >
									<div class="form-group">
										<label for="cpt_libelle" class="sr-only" >Nom</label>
										<div>
											<input type="text" class="form-control" name="cpt_libelle" id="cpt_libelle" placeholder="Nom" required >
										</div>
									</div>
								</div>
							</div>
									
						</div>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php
		include "includes/footer_script.inc.php"; ?>	                           

		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {

				$(document).on("click", ".open-formEdit", function () {
					
					var cpt_id = $(this).data('id');
					var cpt_numero = $(this).data('numero');
					var cpt_libelle = $(this).data('libelle');					 
					
					$(".modal-body #cpt_id").val( cpt_id );
					$(".modal-body #cpt_numero").val( cpt_numero );						 
					$(".modal-body #cpt_libelle").val( cpt_libelle );					
					 					
					if(cpt_id==0){
						$("#formEdit .modal-title").html('Nouveau compte');				
					}else{
						$("#formEdit .modal-title").html('Edition d\'un compte');				
					};
				});		
			});
			(jQuery);
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
