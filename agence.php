<?php
error_reporting(error_reporting() & ~E_NOTICE);
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'modeles/mod_upload.php';
include "modeles/mod_societe.php";
require "modeles/mod_utilisateur.php";
require "modeles/mod_agence.php";

$s = array();

$age_compta=0;
$age_compta_age=0;

if (isset($_GET['id'])) {
	
    $a = get_agence($_GET['id']);
	
	if(!empty($a['age_compta'])){
		$age_compta=$a['age_compta'];	
		$age_compta_age=$a['age_compta_age'];	
	}
} else {
    $a = array();
}

// LISTE DES SOCIETES 
$req=$Conn->query("SELECT soc_id,soc_code FROM Societes ORDER BY soc_code;");
$d_societes=$req->fetchAll();


// AGENCES
$d_agences=array();
if(!empty($age_compta)){
	$req=$Conn->query("SELECT age_id,age_code FROM Agences WHERE age_societe=" . $age_compta . " ORDER BY age_code;");
	$d_agences=$req->fetchAll();

}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Paramètres</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
  <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	
<?php 
	if($a['age_structure']!=1){ ?>
		<style type="text/css" >
			.logo-form-agence{
				display:none;
			}
		</style>	
<?php 	
	}; ?>

</head>

<body class="sb-top sb-top-sm ">
	<!-- Start: Main -->
	<form action="agence_enr.php" method="POST" id="admin-form" enctype="multipart/form-data" >
		<div id="main">
	   
			<?php
				include "includes/header_def.inc.php";
			?>

			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper" class="">
				
				<section id="content" class="animated fadeIn">
					<div class="row">
					
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary ">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="text-center">

											<div class="content-header">
									<?php			if(isset($_GET['id'])){
													echo("<h2>Édition d'une <b class='text-primary'>agence</b></h2>");
												}else{
													echo("<h2>Ajouter une <b class='text-primary'>agence</b></h2>");
												} ?>													
											</div>

											<div class="col-md-10 col-md-offset-1">
										   
											
										
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40" >
															<span>Structure</span>
														</div>
													</div>
												</div>
                                           
										  <?php if(isset($_GET['id'])): ?>
													<input type="hidden" name="age_id" value="<?= $a['age_id'] ?>"></input>
                                        <?php 	endif; ?>
										
												<div class="row">
												
													<div class="col-md-12" style="margin-bottom:10px;">
														
														<div class="section">

															<div class="option-group field section">
																<div class="col-md-6 text-left">
																	<label for="age_type_age_1" class="option option-primary">
																		<input type="radio" name="age_structure" id="age_type_age_1" value="1" <?php if($a['age_structure']==1) echo("checked"); ?> >
																		<span class="radio"></span> Structure indépendante                                              
																	</label>
																	<span class="help-block">
																		L'agence disposera des mêmes caractéristiques qu'une société. Elle aura sont propre chrono de facturation et elle pourra facturer les autres agences de la société.
																	</span>
																</div>
																<div class="col-md-6 text-left">
																	<label for="age_type_age_2" class="option option-primary">
																		<input type="radio" name="age_structure" id="age_type_age_2" value="0" <?php if($a['age_structure']!=1) echo("checked"); ?> >
																		<span class="radio"></span>Agence
																	</label>
																	<span class="help-block">&nbsp;</span>
																</div>

															</div>
														
														</div>
													</div>
												</div>
												
												<div class="row">

													<div class="col-md-12">
														<div class="section">
															<label class="field select">
																<select id="age_societe" name="age_societe" required="">
																	<option value="">Selectionner une société...</option>
																	<?php if (isset($_GET['id'])): ?>
																	  <?=get_societe_agence_select($a['age_societe']);?>
																	<?php else: ?>
																	  <?=get_societe_agence_select(0);?>
																	<?php endif;?>
																</select>
																<i class="arrow simple"></i>
															</label>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40" >
															<span>Informations générales</span>
														</div>
													</div>
												</div>


												<div class="row">
													<div class="col-md-8">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_nom" id="age_nom" class="gui-input nom" placeholder="Nom" required="" value="<?= $a['age_nom'] ?>">
																<label for="age_nom" class="field-icon">
																	<i class="fa fa-building-o"></i>
																</label>
															</div>
														</div>
													</div>
													<div class="col-md-4">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_code" id="age_code" class="gui-input nom" placeholder="Code" required="" value="<?= $a['age_code'] ?>">
																<label for="age_code" class="field-icon">
																	<i class="fa fa-code"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_ad1" id="age_ad1" class="gui-input" placeholder="Adresse" required="" value="<?= $a['age_ad1'] ?>">
																<label for="age_ad1" class="field-icon">
																	<i class="fa fa-map-marker"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_ad2" class="gui-input" id="age_ad2" placeholder="Adresse (Complément 1)" value="<?= $a['age_ad2'] ?>">
																<label for="age_ad2" class="field-icon">
																	<i class="fa fa-map-marker"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_ad3" id="age_ad3" class="gui-input" placeholder="Adresse (Complément 2)" value="<?= $a['age_ad3'] ?>">
																<label for="age_ad3" class="field-icon">
																	<i class="fa fa-map-marker"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-3">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" id="age_cp" name="age_cp" class="gui-input code-postal" placeholder="Code postal" required="" value="<?= $a['age_cp'] ?>">
																<label for="age_cp" class="field-icon">
																	<i class="fa fa fa-certificate"></i>
																</label>
															</div>
														</div>
													</div>
													<div class="col-md-9">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_ville" id="age_ville" class="gui-input nom" placeholder="Ville" required="" value="<?= $a['age_ville'] ?>">
																<label for="age_ville" class="field-icon">
																	<i class="fa fa fa-building"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
                                            
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_tel" id="age_tel" class="gui-input telephone" placeholder="Numéro de téléphone" required="" value="<?= $a['age_tel'] ?>">
																<label for="age_tel" class="field-icon">
																	<i class="fa fa fa-phone"></i>
																</label>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_fax" id="age_fax" class="gui-input telephone" placeholder="Numéro de fax" value="<?= $a['age_fax'] ?>">
																<label for="age_fax" class="field-icon">
																	<i class="fa fa-phone-square"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
											
												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<div class="field prepend-icon">
																<input type="email" name="age_mail" id="age_mail" class="gui-input" placeholder="Adresse Email" value="<?= $a['age_mail'] ?>">
																<label for="age_mail" class="field-icon">
																	<i class="fa fa-envelope"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<div class="field prepend-icon">
																<input type="url" name="age_site" id="age_site" class="gui-input" placeholder="Site Internet" value="<?= $a['age_site'] ?>">
																<label for="age_site" class="field-icon">
																	<i class="fa fa-globe"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40" >
															<span>Informations juridiques</span>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<label class="field select">
																<select name="age_responsable" id="age_responsable" >
																	<option value="" >Selectionner un responsable...</option>
																	<?php if (isset($_GET['id'])):
																		echo get_utilisateur_select(0,0,"","",$a['age_responsable']);
																	else:
																		echo get_utilisateur_select(0,0,"","",0);
																	endif;?>

																</select>
																<i class="arrow simple"></i>
															</label>
														</div>
													</div>
												</div>
											
												<div class="row">
													<div class="col-md-6 change-col-siret">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_siret" id="age_siret" class="gui-input siretlong" placeholder="Siret" value="<?= $a['age_siret'] ?>" required>
																<label for="age_siret" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</label>
															</div>
														</div>
													</div>
													<div class="logo-form-agence">
														<div class="col-md-6">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="text" name="age_siren" id="age_siren" class="gui-input siren" placeholder="Siren" value="<?= $a['age_siren'] ?>" <?php if($a['age_structure']==1) echo("required"); ?>  >
																	<label for="age_siren" class="field-icon">
																		<i class="fa fa-barcode"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="logo-form-agence">
													<div class="row">										
														<div class="col-md-6">
															<div class="section">
																<label for="tva" class="field">
																	<input type="text" name="age_tva" id="age_tva" class="gui-input" placeholder="TVA Intracommune" value="<?= $a['age_tva'] ?>" <?php if($a['age_structure']==1) echo("required"); ?> >
																</label>
															</div>
														</div>																						
														<div class="col-md-6">
															<div class="section">
																<label for="age_tva_type" class="field">
																	<input type="text" name="age_tva_type" id="age_tva_type" class="gui-input" placeholder="Type de TVA" value="<?= $a['age_tva_type'] ?>" <?php if($a['age_structure']==1) echo("required"); ?> >
																</label>
															</div>
														</div>
													</div>
												</div>
												<div class="logo-form-agence">
													<div class="row">
														<div class="col-md-12">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="text" name="age_num_existence" id="age_num_existence" class="gui-input" placeholder="Numéro d'existence" value="<?= $a['age_num_existence'] ?>" <?php if($a['age_structure']==1) echo("required"); ?> >
																	<label for="num_existence" class="field-icon" >
																		<i class="fa fa-barcode"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="logo-form-agence">
													<div class="row">
														<div class="col-md-6">
															<div class="section">
																<label class="field">
																	<input type="text" name="age_ape" id="age_ape" class="gui-input" placeholder="APE" value="<?= $a['age_ape'] ?>" <?php if($a['age_structure']==1) echo("required"); ?> >
																</label>
															</div>
														</div>
														<div class="col-md-6">
															<div class="section">
																<label class="field">
																	<input type="text" name="age_rcs" id="age_rcs" class="gui-input" placeholder="RCS" value="<?= $a['age_rcs'] ?>" <?php if($a['age_structure']==1) echo("required"); ?> >
																</label>
															</div>
														</div>
													</div>
												</div>
												<div class="logo-form-agence">
													<div class="row">
														<div class="col-md-6">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="text" name="age_type" id="age_type" class="gui-input" placeholder="Type" value="<?= $a['age_type'] ?>" <?php if($a['age_structure']==1) echo("required"); ?> >
																	<label for="age_type" class="field-icon">
																		<i class="fa fa-gavel"></i>
																	</label>
																</div>
															</div>
														</div>


														<div class="col-md-6">
															<div class="section">

																<div class="smart-widget sm-right smr-80">
																	<div class="field prepend-icon">
																		<input type="text" name="age_capital" id="age_capital" class="gui-input input-int" placeholder="Capital" value="<?= $a['age_capital'] ?>" <?php if($a['age_structure']==1) echo("required"); ?> >
																		<label for="age_capital" class="field-icon">
																			<i class="fa fa-university"></i>
																		</label>
																	</div>
																	<span class="button">€</span>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40" >
															<span>Horaires par défaut</span>
														</div>
													</div>
												</div>
												<div class="section">
													<p> Les horaires sont à entrer sous la forme: <strong>XXhXX</strong></p>
												</div>
												<div class="row">
													<h4 class="text-left">Matin</h4>
													<div class="col-md-6">
														<div class="section">
															<span class="prepend-icon">
																<input type="text" name="age_h_deb_matin" class="gui-input heure" placeholder="Heure de début du matin" value="<?=$a['age_h_deb_matin']?>">
																<span class="field-icon">
																	<i class="fa fa-clock-o"></i>
																</span>
															</span>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">
															<span class="prepend-icon">
																<input type="text" name="age_h_fin_matin" class="gui-input heure" placeholder="Heure de fin du matin" value="<?=$a['age_h_fin_matin']?>">
																<span class="field-icon">
																	<i class="fa fa-clock-o"></i>
																</span>
															</span>
														</div>
													</div>
												</div>
												<div class="row">
													<h4 class="text-left">Après-midi</h4>
													<div class="col-md-6">
														<div class="section">
															<span class="prepend-icon">
																<input type="text" name="age_h_deb_am" class="gui-input heure" placeholder="Heure de début de l'après-midi" value="<?=$a['age_h_deb_am']?>">
																<span class="field-icon">
																	<i class="fa fa-clock-o"></i>
																</span>
															</span>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">
															<span class="prepend-icon">
																<input type="text" name="age_h_fin_am" class="gui-input heure" placeholder="Heure de fin de l'après-midi" value="<?=$a['age_h_fin_am']?>">
																<span class="field-icon">
																	<i class="fa fa-clock-o"></i>
																</span>
															</span>
														</div>
													</div>
												</div>
												
												<div class="row">
													  <div class="col-md-12">
														<div class="section-divider mb40">
														  <span>Logos</span>
														</div>
													  </div>
												</div>
												
												<div class="row">
													<div class="col-md-4 col-md-offset-1">

														<div class="col-md-12">
														  <div class="image-preview image-350x140" >
															<img <?php if (isset($_GET['id'])): ?>src="documents/societes/logos/<?=$a['age_logo_1']?>"<?php else: ?>src="img/preview-350x140.png"<?php endif;?> alt="" id="image_preview1" />
														  </div>
														</div>
														<div class="col-md-12">
														  <div class="section">
															<span class="prepend-icon file">
															  <span class="button btn-primary">Choisir</span>
															  <input type="file" class="gui-file" name="age_logo_1" id="inputFile1" onchange="document.getElementById('uploader0').value = this.value;" >
															  <input type="text" class="gui-input" id="uploader0" placeholder="Logo 1">
															  <span class="field-icon">
																<i class="fa fa-upload"></i>
															  </span>
															</span>
														  </div>
														</div>
													</div>
													<div class="col-md-4 col-md-offset-2">

														  <div class="image-preview image-350x140">
															<img <?php if (isset($_GET['id'])): ?>src="documents/societes/logos/<?=$a['age_logo_2']?>"<?php else: ?>src="img/preview-350x140.png"<?php endif;?> id="image_preview2" alt="" >
														  </div>
														  <div class="col-md-12">
															<div class="section">
															  <span class="field prepend-icon file">
																<span class="button btn-primary">Choisir</span>
																<input type="file" class="gui-file" name="age_logo_2" onchange="document.getElementById('uploader1').value = this.value;" id="inputFile2">
																<input type="text" class="gui-input" id="uploader1" placeholder="Logo 2">
																<span class="field-icon">
																  <i class="fa fa-upload"></i>
																</span>
															  </span>
															</div>
														  </div>
													</div>
												</div>
												
												<div class="row">
											  
													<div class="col-md-4 col-md-offset-1">

														<div class="col-md-12">
														  <div class="image-preview image-350x140" >
															<img <?php if (isset($_GET['id'])): ?>src="documents/societes/logos/<?=$a['age_qualite_1']?>" <?php else: ?>src="img/preview-350x140.png"<?php endif;?> alt="" id="image_preview3" />
														  </div>
														</div>
														<div class="col-md-12">
														  <div class="section">
															<span class="prepend-icon file">
															  <span class="button btn-primary">Choisir</span>
															  <input type="file" class="gui-file" name="age_qualite_1" id="inputFile3" onchange="document.getElementById('uploader2').value = this.value;" >
															  <input type="text" class="gui-input" id="uploader2" placeholder="Qualité 1">
															  <span class="field-icon">
																<i class="fa fa-upload"></i>
															  </span>
															</span>
														  </div>
														</div>
													</div>
													<div class="col-md-4 col-md-offset-2">

														  <div class="image-preview image-350x140">
															<img <?php if (isset($_GET['id'])): ?>src="documents/societes/logos/<?=$a['age_qualite_2']?>" <?php else: ?>src="img/preview-350x140.png"<?php endif;?> id="image_preview4" alt="" />
														  </div>
														  <div class="col-md-12">
															<div class="section">
															  <span class="field prepend-icon file">
																<span class="button btn-primary">Choisir</span>
																<input type="file" class="gui-file" name="age_qualite_2" onchange="document.getElementById('uploader3').value = this.value;" id="inputFile4">
																<input type="text" class="gui-input" id="uploader3" placeholder="Qualité 2">
																<span class="field-icon">
																  <i class="fa fa-upload"></i>
																</span>
															  </span>
															</div>
														  </div>
													</div>
												</div>
					

												<div class="row">

													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Comptabilité</span>
														</div>
													</div>
												</div>
												
												<div class="row mb10" >
													<div class="col-md-6" >
														<select name="age_compta" id="age_compta" class="select2 select2-societe" required >
															<option value="0" >Sélectionnez une société</option>
													<?php	if(!empty($d_societes)){
																foreach($d_societes as $d_soc){
																	if($d_soc["soc_id"]==$age_compta){
																		echo("<option value='" . $d_soc["soc_id"] . "' selected >" . $d_soc["soc_code"] . "</option>");
																	}else{
																		echo("<option value='" . $d_soc["soc_id"] . "' >" . $d_soc["soc_code"] . "</option>");
																	}
																}
															} ?>
														</select>
													</div>
													<div class="col-md-6" >
														<select name="age_compta_age" id="age_compta_age" class="select2 select2-societe-agence" >
															<option value="0" >Sélectionnez une agence</option>
													<?php	if(!empty($d_agences)){
																foreach($d_agences as $d_age){
																	if($d_age["age_id"]==$age_compta_age){
																		echo("<option value='" . $d_age["age_id"] . "' selected >" . $d_age["age_code"] . "</option>");
																	}else{
																		echo("<option value='" . $d_age["age_id"] . "' >" . $d_age["age_code"] . "</option>");
																	}
																}
															} ?>
														</select>
													</div>
												</div>
																	
												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<div class="field prepend-icon">
																<input type="email" name="age_compta_mail" id="age_compta_mail" class="gui-input" placeholder="Adresse Email" value="<?= $a['age_compta_mail'] ?>">
																<label for="age_compta_mail" class="field-icon">
																	<i class="fa fa-envelope"></i>
																</label>
															</div>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_compta_tel" id="age_compta_tel" class="gui-input telephone" placeholder="Numéro de téléphone" value="<?= $a['age_compta_tel'] ?>">
																<label for="age_compta_tel" class="field-icon">
																	<i class="fa fa fa-phone"></i>
																</label>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">
															<div class="field prepend-icon">
																<input type="text" name="age_compta_fax" id="age_compta_fax" class="gui-input telephone" placeholder="Numéro de fax" value="<?= $a['age_compta_fax'] ?>">
																<label for="age_compta_fax" class="field-icon">
																	<i class="fa fa-phone-square"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
											
												<div class="row">
													<div class="col-md-6">
														<div class="section text-left">
															<label for="age_interco_intra" >% INTERCO INTRA</label>
															<div class="smart-widget sm-right smr-80">
																<span class="field">
																	<input type="text" name="age_interco_intra" id="age_interco_intra" class="gui-input input-int" min="0" max="100" placeholder="Pourcentage Interco INTRA" value="<?= $a['age_interco_intra'] ?>">
																</span>
																<span class="button">%</span>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section text-left">
															<label for="age_interco_inter" >% INTERCO INTER</label>
															<div class="smart-widget sm-right smr-80">
																
																<span class="field">
																	<input type="text" name="age_interco_inter" id="age_interco_inter" class="gui-input input-int" min="0" max="100" placeholder="Pourcentage Interco INTER" value="<?= $a['age_interco_inter'] ?>">
																</span>
																<span class="button">%</span>
															</div>
														</div>
													</div>
												</div>
											
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</section>
		  
			</section>
		</div>
		<!-- End: Main -->
		
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="agence_liste.php" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
					<button type="submit" class="btn btn-success btn-sm">
						<i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php
	include "includes/footer_script.inc.php"; ?>	
	
	<!-- validation inputs -->

	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script src="assets/js/custom.js"></script>
	<!-- plugin pour les masques formulaires -->
	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->
	
	<script type="text/javascript">
	
		jQuery(document).ready(function () {
	
			$('#age_type_age_2').click(function() {
			  if ($('#age_type_age_2').is(':checked')) {
					$(".logo-form-agence").hide(400);
					
					$("#age_siren").prop('required',false);
					$("#age_tva").prop('required',false);
					$("#age_tva_type").prop('required',false);
					$("#age_num_existence").prop('required',false);
					$("#age_ape").prop('required',false);
					$("#age_rcs").prop('required',false);
					$("#age_type").prop('required',false);
					$("#age_capital").prop('required',false);
					
					// changer le width de la div
					$( ".change-col-siret" ).switchClass( "col-md-6", "col-md-12", 1000, "easeInOutQuad" );
					
					
				}
			});
			
			$('#age_type_age_1').click(function() {
				if ($('#age_type_age_1').is(':checked')) {
					$(".logo-form-agence").show(400);
					
					$("#age_siren").prop('required',true);
					$("#age_tva").prop('required',true);
					$("#age_tva_type").prop('required',true);
					$("#age_num_existence").prop('required',true);
					$("#age_ape").prop('required',true);
					$("#age_rcs").prop('required',true);
					$("#age_type").prop('required',true);
					$("#age_capital").prop('required',true);
					// changer le width de la div
					$( ".change-col-siret" ).switchClass( "col-md-12", "col-md-6", 1000, "easeInOutQuad" );
				
				}
			});
			
			
			$("#inputFile1").change(function () {
				readURL1(this);
			});
			$("#inputFile2").change(function () {
				readURL2(this);
			});
			$("#inputFile3").change(function () {
				readURL3(this);
			});
			$("#inputFile4").change(function () {
				readURL4(this);
			});
			
			$("#age_compta").change(function (){
				actu_select_agence(this,"age_compta_age");		
			});

    });
	
	
		function actu_select_agence(liste_societe,cible){
			$.ajax({
				type:'POST',
				url: '/ajax/ajax_agence.php',
				data : 'field=' + liste_societe.value,
				dataType: 'json',                
				success: function(data)
				{	
					$('#' + cible).find("option:gt(0)").remove();
					if (data['0'] == undefined){

					}else{
						$.each(data, function(key, value){
							$('#' + cible)
							.append($("<option></option>")
							.attr("value",key)
							.text(value["age_nom"]));
						});
					}
				}
			});
		}

	
		// changer l'image
		function readURL1(input) {

		  if (input.files && input.files[0]) {

			var reader = new FileReader();

			reader.onload = function (e) {
			  $('#image_preview1').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		  }
		}

		// changer l'image
		function readURL2(input) {

		  if (input.files && input.files[0]) {

			var reader = new FileReader();

			reader.onload = function (e) {
			  $('#image_preview2').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		  }
		}

		// changer l'image
		function readURL3(input) {

		  if (input.files && input.files[0]) {

			var reader = new FileReader();

			reader.onload = function (e) {
			  $('#image_preview3').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		  }
		}

		// changer l'image
		function readURL4(input) {

		  if (input.files && input.files[0]) {

			var reader = new FileReader();

			reader.onload = function (e) {
			  $('#image_preview4').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		  }
		}

 
	</script>
</body>
</html>
