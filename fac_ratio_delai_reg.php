<?php

	// DETAIL DU RATION RETARD DE REGLEMENT

	include "includes/controle_acces.inc.php";

	include "includes/connexion.php";
	include "includes/connexion_soc.php";


    // CONTROLE ACCES
    
    if(!$_SESSION["acces"]["acc_droits"][35]){

		$_SESSION['message'][] = array(
			"aff" => "",
			"titre" => "Accès refusé!",
			"type" => "danger",
			"message" => "Vous n'êtes pas autorisé à accéder à cette page!" 
		);
	
		header("location : " . $_SESSION["retour"]);
        die();
        
	}

	$annee=0;
	if(isset($_GET["annee"])){
		if(!empty($_GET["annee"])){
			$annee=intval($_GET["annee"]);
		}
    }
    if(empty($annee)){
        $annee=date("Y");
    }

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}

    // TRAITEMENT
    
	$data=array();
	
	$sql_fac_reglees="SELECT SUM(DATEDIFF(fac_date_reg,fac_date)) as nb_j, COUNT(fac_id) as nb_fac,fac_commercial
    ,com_label_1,com_label_2
    FROM Factures 
	LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
	WHERE fac_nature=1 AND fac_regle=fac_total_ttc AND YEAR(fac_date)=" . $annee;
	
	if(!empty($acc_agence)){
		$sql_fac_reglees.=" AND fac_agence=" . $acc_agence;
    }
    $sql_fac_reglees.=" GROUP BY fac_commercial
    ORDER BY com_label_1";
	$req_fac_reglees=$ConnSoc->query($sql_fac_reglees);
    $d_fac_reglees=$req_fac_reglees->fetchAll(); 

    
	$titre="";

    if(!empty($acc_agence)) {

        $sql="SELECT soc_nom,age_nom FROM Agences,Societes WHERE soc_id=age_societe AND age_id=" . $acc_agence . ";";
        $req=$Conn->query($sql);
        $d_agence=$req->fetch();
        if(!empty($d_agence)){
            $titre="Délai de paiement pour " . $d_agence["soc_nom"] . " " . $d_agence["age_nom"];
        }

    }else{

        $sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
        $req=$Conn->query($sql);
        $d_societe=$req->fetch();
        if(!empty($d_societe)){
            $titre="Délai de paiement pour " . $d_societe["soc_nom"];
        }

    }

    $titre.="<br/>en " . $annee;
	
	

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">

<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" >
					<div class="row ">

                        <div class="row" >
                            <div class="col-md-3 text-center" >
                                <a href="fac_ratio_delai_reg.php?annee=<?=$annee-1?>" class="btn btn-default btn-sm" >
                                    <i class="fa fa-arrow-left" ></i> <?=$annee-1?>
                                </a>
                            </div>
                            <div class="col-md-6" >
                                <h1 class="text-center" ><?=$titre?></h1>
                            </div>
                            <div class="col-md-3 text-center" >
                        <?php   if($annee<date("Y")){  ?>
                                    <a href="fac_ratio_delai_reg.php?annee=<?=$annee+1?>" class="btn btn-default btn-sm" >
                                        <?=$annee+1?> <i class="fa fa-arrow-right" ></i> 
                                    </a>
                        <?php   } ?>
                            </div>

                        </div>
                    

						

						<div class="table-responsive">
                            <table class="table table-striped table-hover" >
                                <thead>
                                    <tr class="dark">
                                        <th>Commercial</th>	
                                        <th class="text-right" >Nb. jours</th>
                                        <th class="text-right" >Nb. factures réglées</th>
                                        <th class="text-right" >Délai de règlement (jours)</th>
                                    </tr>
                                </thead>
                                <tbody>
                        <?php		if(!empty($d_fac_reglees)){

                                        /*echo("<pre>");
                                            print_r($d_fac_reglees);
                                        echo("</pre>");
                                        die();*/
                                        $total_j=0;
                                        $total_fac=0;
                                        foreach($d_fac_reglees as $d){

                                            $total_j=$total_j + $d["nb_j"];
                                            $total_fac=$total_fac + $d["nb_fac"];
                                            

                                            $ratio=0;
                                            if(!empty($d["nb_fac"])){
                                                $ratio=$d["nb_j"]/$d["nb_fac"];                                              
                                            } ?>
                                            <tr>
                                                <td>
                                                    <a href="fac_ratio_delai_reg_detail.php?commercial=<?=$d["fac_commercial"]?>&annee=<?=$annee?>">
                                                        <?=$d["com_label_1"] . " " . $d["com_label_2"]?>
                                                    </a>
                                                </td>
                                                <td class="text-right" ><?=$d["nb_j"]?></td>
                                                <td class="text-right" ><?=$d["nb_fac"]?></td>                                               
                                                <td class="text-right" ><?=number_format($ratio,2,","," ")?></td>
                                            </tr>
                        <?php			}

                                        $ratio=0;
                                        if(!empty($total_fac)){
                                            $ratio=$total_j/$total_fac;
                                        }
                                        ?>
                                        <tr>
                                            <th>
                                                <a href="fac_ratio_delai_reg_detail.php?commercial=-1&annee=<?=$annee?>">
                                                    Total :
                                                </a>
                                            </th>
                                            <td class="text-right" ><?=$total_j?></td>
                                            <td class="text-right" ><?=$total_fac?></td>
                                            <td class="text-right" ><?=number_format($ratio,2,","," ")?></td>
                                        </tr>
                        <?php		} ?>
                                </tbody>
                            </table>
						</div>

					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="<?=$_SESSION["retour"]?>" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){



			});
		</script>
	</body>
</html>
