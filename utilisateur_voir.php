<?php
include "includes/controle_acces.inc.php";
include "includes/connexion.php";
include "modeles/mod_utilisateur.php";
include "modeles/mod_profil.php";
include "modeles/mod_societe.php";
include "modeles/mod_agence.php";
include "modeles/mod_service.php";
include "modeles/mod_droit.php";
include "modeles/mod_parametre.php";
include "modeles/mod_competence.php";
include "modeles/mod_diplome.php";
include('modeles/mod_erreur.php');

$erreur=0;

// ONGLET ACTIF //

$onglet=1;
if(isset($_GET["onglet"])){
	$onglet=$_GET["onglet"];
}

// PARAMETRE

$utilisateur=0;
if(isset($_GET["utilisateur"])){
	if(!empty($_GET["utilisateur"])){
		$utilisateur=$_GET["utilisateur"];
		$u=get_utilisateur($utilisateur);
	}else{
		$erreur=1;
	}
}else{
	$erreur=1;
}

if($erreur==0){


	// PERSONNE CONNECTE

	$acc_utilisateur=0;
	$acc_service=0;
	$acc_profil=0;
	$acc_drt_competence=false;
	$acc_drt_valide_compt=false;
	$drt_rh=false;		// acces aux donnees relevant du RH
	$drt_responsable=false;
	$drt_full=false;

	$acc_societe=$_SESSION['acces']['acc_societe'];
	$acc_agence=$_SESSION['acces']['acc_agence'];
	if($_SESSION['acces']['acc_ref']==1){
		$acc_utilisateur=$_SESSION['acces']['acc_ref_id'];
		$acc_profil=$_SESSION['acces']['acc_profil'];
		$acc_service=$_SESSION['acces']['acc_service'];
		$acc_drt_competence=$_SESSION['acces']['acc_droits'][3];
		$acc_drt_valide_compt=$_SESSION['acces']['acc_droits'][4];
		$drt_rh=$_SESSION['acces']['acc_service'][4];
		if($acc_utilisateur == $u['uti_responsable']){
			$drt_responsable=true;
		}
	}

	if($_SESSION['acces']['acc_profil']==13 OR $_SESSION['acces']['acc_profil']==9 OR $_SESSION['acces']['acc_ref_id']==98){
		$drt_full=true;
	}

	// on verifie que connecte à le droit de consulter

	if($acc_utilisateur!=$utilisateur){

		if(!$_SESSION['acces']['acc_droits'][22]){
			$erreur=1;
		}else{

			if(!$drt_full){
				$sql="SELECT uti_id FROM Utilisateurs,Utilisateurs_Societes
				WHERE uti_id=uso_utilisateur AND uso_societe=" . $u["uti_societe"] . " AND uso_agence=" . $u["uti_agence"] . " AND uso_utilisateur=" . $acc_utilisateur . ";";
				$req=$Conn->query($sql);
				$d_autorise=$req->fetch();
				if(empty($d_autorise)){
					$erreur=1;
				}
			}
		}
	}
}
if($erreur==0){

	$identite=$u["uti_prenom"] . " " . $u["uti_nom"];
	if($u["uti_titre"]>0){
		$identite=$base_civilite[$u["uti_titre"]] . " " . $identite;
	};

	$utilisateur_profil="&nbsp;";
	if($u["uti_profil"]>0){
		$utilisateur_profil=get_profil_nom($u["uti_profil"]);
	}

	$utilisateur_adresse=$u["uti_ad1"];
	if(!empty($u["uti_ad2"])){
		if(!empty($utilisateur_adresse)){
			$utilisateur_adresse=$utilisateur_adresse . "<br/>";
		}
		$utilisateur_adresse=$utilisateur_adresse . $u["uti_ad2"];
	}
	if(!empty($u["uti_ad3"])){
		if(!empty($utilisateur_adresse)){
			$utilisateur_adresse=$utilisateur_adresse . "<br/>";
		}
		$utilisateur_adresse=$utilisateur_adresse . $u["uti_ad3"];
	}
	if(!empty($utilisateur_adresse)){
		$utilisateur_adresse=$utilisateur_adresse . "<br/>";
	}
	$utilisateur_adresse=$utilisateur_adresse . $u["uti_cp"] . " " . $u["uti_ville"];

	$utilisateur_societe="&nbsp;";
	if($u["uti_societe"]>0){
		$societe=get_societe($u["uti_societe"]);
		$utilisateur_societe=$societe["soc_nom"];
	}
	$utilisateur_agence="&nbsp;";
	if($u["uti_agence"]>0){
		$agence=get_agence($u["uti_agence"]);
		$utilisateur_agence=$agence["age_nom"];
	}

	// GESTION DES RETOURS
	$_SESSION['retour_diplome']="utilisateur_voir.php?utilisateur=" . $utilisateur . "&onglet=4";
	$_SESSION['retour_competence']="utilisateur_voir.php?utilisateur=" . $utilisateur . "&onglet=4";

	// GESTION DES NOTIFICATIONS

	if(isset($_GET["notification"])){
		$req = $Conn->prepare("UPDATE Notifications SET not_vu=true WHERE not_id=:notification");
		$req->bindParam(":notification",$_GET["notification"]);
		$req->execute();
	}

	// retourne la liste des droits de l'utilisateur
	$sql="SELECT drt_id,drt_nom,drt_descriptif,udr_droit,udr_reaffecte,ser_id,ser_libelle
	FROM Droits LEFT OUTER JOIN Utilisateurs_Droits ON (Droits.drt_id = Utilisateurs_Droits.udr_droit AND udr_utilisateur=" . $utilisateur . ")
	LEFT JOIN Services ON (Droits.drt_service = Services.ser_id)";
	$sql.=" ORDER BY ser_libelle,drt_nom";

	$req=$Conn->query($sql);
	$d_droits = $req->fetchAll();
	/*echo("<pre>");
		print_r($d_droits);
	echo("</pre>");*/

	// HORS FULL ACCESS
	// SEULS LES DROITS DONT DISPOSE USER EN REAFFECTATION PEUVENT ETRE REAFFECTER
	if(!$drt_full){
		$sql="SELECT drt_id FROM Droits LEFT OUTER JOIN Utilisateurs_Droits ON (Droits.drt_id = Utilisateurs_Droits.udr_droit AND udr_utilisateur=" . $acc_utilisateur . ")
		WHERE (udr_reaffecte=0 OR ISNULL(udr_reaffecte))
		ORDER BY drt_id;";
		$req=$Conn->query($sql);
		$d_droits_exclu = $req->fetchAll();
		if(!empty($d_droits_exclu)){
			foreach($d_droits_exclu as $d_exclu){
				//echo("droit a exclure  : " . $d_exclu["drt_id"] . "<br/>");
				$cle=array_search($d_exclu["drt_id"],array_column($d_droits, 'drt_id'));
				if(!empty($cle) OR $cle==0){
					$d_droits[$cle]["exclu"]=1;
				}


			}
		}
	}
	/*echo("APRES<br/>");
	echo("<pre>");
		print_r($d_droits);
	echo("</pre>");*/

	// LISTE DES DIPLOMES
	$sql="SELECT idi_date_deb, idi_date_fin,dip_libelle,idi_fichier,dip_id FROM diplomes
	LEFT JOIN intervenants_diplomes ON (diplomes.dip_id = intervenants_diplomes.idi_diplome)
	WHERE idi_ref = 1 AND idi_ref_id = " . $utilisateur . " ORDER BY dip_libelle";
	$req=$Conn->query($sql);
	$diplomes = $req->fetchAll();

	// COMPETENCES VALIDEES
	$sql="SELECT * FROM competences
	LEFT JOIN Intervenants_Competences ON(Competences.com_id=Intervenants_Competences.ico_competence)
	 WHERE  Intervenants_Competences.ico_ref=1 AND Intervenants_Competences.ico_ref_id=" . $utilisateur . " ORDER BY com_libelle";
	$req=$Conn->query($sql);
	$competences_validees = $req->fetchAll();
	// COMPETENCES PAS ENCORE ACQUISES
	$sql="SELECT * FROM competences
	LEFT OUTER JOIN Intervenants_Competences ON(Competences.com_id=Intervenants_Competences.ico_competence AND Intervenants_Competences.ico_ref=1 AND Intervenants_Competences.ico_ref_id=" . $utilisateur . ")
	WHERE ico_competence IS NULL ORDER BY com_libelle";
	$req=$Conn->query($sql);
	$competences_non_validees = $req->fetchAll();

	// MAJ TABLETTE
	if(!empty($u['uti_version_tab'])){
		$sql="SELECT * FROM maj
		WHERE maj_id = " . $u['uti_version_tab'];
		$req=$Conn->query($sql);
		$maj = $req->fetch();
	}

}
//die();
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Utilisateurs</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link href="assets/admin-tools/admin-forms/css/admin-forms.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">

	<div id="main">
<?php	include "includes/header_def.inc.php"; ?>
		<section id="content_wrapper" class="">
			<section id="content" class="">

		<?php	if($erreur>0){ ?>
					<h1 class="bg-danger text-center pt25 pb25" >Impossible d'afficher la page!</h1>
		<?php	}else{ ?>

					<h1><?=$identite?></h1>
					<div class="row">

						<div class="col-md-12">

							<div class="tab-block">
								<ul class="nav nav-tabs">
									<li class="<?php if($onglet==1) echo("active") ?>" id="onglet_1" >
										<a href="#tab1" data-toggle="tab" aria-expanded="true">Informations générales</a>
									</li>
							<?php	if($drt_rh OR $drt_responsable OR $drt_full){ ?>
										<li class="<?php if($onglet==2) echo("active") ?>" id="onglet_2" >
											<a href="#tab2" data-toggle="tab" aria-expanded="false">Droits et accès</a>
										</li>
							<?php	} ?>
									<li class="<?php if($onglet==4) echo("active") ?>" id="onglet_4" >
										<a href="#tab4" data-toggle="tab" aria-expanded="false">Tech</a>
									</li>
								</ul>

								<div class="tab-content p30" >

									<div id="tab1" class="tab-pane <?php if($onglet==1) echo("active") ?>" >

										<div class="row" >
											<div class="col-md-3" >
												Fonction : <strong><?=$u["uti_fonction"]?></strong>
											</div>
											<div class="col-md-3" >
												Téléphone : <strong><?=$u["uti_tel"]?></strong>
											</div>
											<div class="col-md-3" >
												Portable : <strong><?=$u["uti_mobile"]?></strong>
											</div>
											<div class="col-md-3" >
												Mail : <strong><?=$u["uti_mail"]?></strong>
											</div>
										</div>

								<?php	if($drt_rh){ ?>
											<hr>
											<h2>Données RH</h2>

											<div class="row" >
												<!-- DONNEE ADMINISTRATIVE -->
												<div class="col-md-8">

													<div class="panel">
														<div class="panel-heading">
															<span class="panel-icon"><i class="fa fa-folder"></i></span>
															<span class="panel-title">Informations administratives</span>
														</div>
														<div class="panel-body pb5">

															<div class="row" >
																<div class="col-md-2 text-right">
																	Matricule :
																</div>
																<div class="col-md-2">
																	<strong><?=$u["uti_matricule"]?></strong>
																</div>
															</div>

															<div class="row mt10" >
																<div class="col-md-2 text-right">
																	Profil :
																</div>
																<div class="col-md-2">
																	<strong><?=$utilisateur_profil?></strong>
																</div>
																<div class="col-md-2 text-right">
																	Société :
																</div>
																<div class="col-md-2">
																	<strong><?=$utilisateur_societe?></strong>
																</div>
																<div class="col-md-2 text-right">
																	Agence :
																</div>
																<div class="col-md-2">
																	<strong><?=$utilisateur_agence?></strong>
																</div>
															</div>

															<div class="row mt10" >
																<div class="col-md-2 text-right">Service :</div>
																<div class="col-md-2" >
															<?php	if(!empty($u["uti_service"])){
																		$ser=get_service($u["uti_service"]);
																		echo("<strong>" . $ser["ser_libelle"] ."</strong>");
																	} ?>
																</div>
																<div class="col-md-2 text-right">
																	Responsable :
																</div>
																<div class="col-md-2" >
														<?php		if(!empty($u["uti_responsable"])){
																		$resp=get_utilisateur($u["uti_responsable"]);
																		echo("<strong>" . $resp["uti_prenom"] . " " . $resp["uti_nom"] ."</strong>");
																	} ?>
																</div>
																<div class="col-md-2 text-right">
																	Contrat :
																</div>
																<div class="col-md-2" >
													<?php			if($u["uti_contrat"]>0){
																		echo("<strong>" . $base_contrat[$u["uti_contrat"]] . "</strong>");
																	}; ?>
																</div>
															</div>

															<div class="row mt10" >
																<div class="col-md-2 text-right">
																	Population:
																</div>
																<div class="col-md-2" >
															<?php	if($u["uti_population"]>0){
																		echo("<strong>" . $base_population[$u["uti_population"]] . "</strong>");
																	}?>
																</div>
																<div class="col-md-2 text-right">
																	Carte affaire:
																</div>
																<div class="col-md-2" >
															<?php	if($u["uti_carte_affaire"]){
																		echo("<strong>OUI</strong> ");
																	}else{
																		echo("<strong>NON</strong> ");
																	} ?>
																</div>
																<div class="col-md-2 text-right">
																	Date d'embauche:
																</div>
																<div class="col-md-2" >
															<?php	if(!empty($u["uti_date_entree"])){
																		echo("<strong>" . convert_date_txt($u["uti_date_entree"]) . "</strong>");
																	}else{
																		echo("<strong>NC</strong> ");
																	} ?>
																</div>
															</div>
															<div class="row mt10">
																<div class="col-md-12">
																<?php	if(!empty($u["uti_saisie_rapport"])){
																		echo("Nombre de semaines pour la saisie des rapports hebdomadaires : <strong>" . $u["uti_saisie_rapport"] . " semaines</strong>");
																	}else{
																		echo("Nombre de semaines pour la saisie des rapports hebdomadaires :  <strong>1 semaine</strong> ");
																	} ?>
																</div>
															</div>
												<?php		if(!empty($u["uti_archive"])){ ?>
																<div class="row mt10" >
																	<div class="col-md-12 text-danger text-center" >
																		<strong>ARCHIV&Eacute;</strong>
																	</div>
																</div>
												<?php		} ?>

														</div>
													</div>

													<div class="panel">
														<div class="panel-heading">
															<span class="panel-icon">
																<i class="fa fa-eur"></i>
															</span>
															<span class="panel-title">Charges</span>
														</div>
														<div class="panel-body pb5">
															<div class="row" >
																<div class="col-md-3 text-right">
																	Charges :
																</div>
																<div class="col-md-3">
																	<?="<strong>" . $u["uti_charge"] . " €</strong>"?>
																</div>
																<div class="col-md-3 text-right">
																	Charges domiciles :
																</div>
																<div class="col-md-3" >
																	<?="<strong>" . $u["uti_charge_dom"] . " €</strong>"?>
																</div>
															</div>
														</div>
													</div>

													<div class="panel">
														<div class="panel-heading">
															<span class="panel-icon">
																<i class="fa fa-bus"></i>
															</span>
															<span class="panel-title">Véhicule</span>
														</div>
														<div class="panel-body pb5">
															<div class="row" >
																<div class="col-md-2 text-right" >
																	Puissance fiscale :
																</div>
																<div class="col-md-2" >
																	<?="<strong>" . $u["uti_veh_cv"] . " CV</strong>"?>
																</div>
																<div class="col-md-2 text-right" >
																	Km annuels :
																</div>
																<div class="col-md-2" >
															<?php	switch($u["uti_tranche_km"]){
																		case 1:
																			echo "<strong>0 - 5 000 km</strong>";
																			break;
																		case 2:
																			echo "<strong>5001 - 20 000 km</strong>";
																			break;
																		case 3:
																			echo "<strong> > 20 000 km</strong>";
																			break;
																	} ?>
																</div>
																<div class="col-md-2 text-right" >
																	Carte grise :
																</div>
																<div class="col-md-2" >
															<?php	if(!empty($u["uti_carte_grise"])){
																		if(file_exists("documents/Utilisateurs/" . $u["uti_carte_grise"])){ ?>
																			<a href="<?="documents/Utilisateurs/" . $u["uti_carte_grise"]?>" target="_blank" download="Carte grise <?=$u["uti_nom"] . ' ' . $u["uti_prenom"]?>" >
																				<i class="fa fa-file-pdf-o" ></i>
																			</a>
																<?php	}else{
																			echo("<strong class='danger' >NC</strong>");
																		}
																	}else{
																		echo("<strong class='danger' >NC</strong>");
																	}	?>
																</div>
															</div>
															<div class="row" >
																<div class="col-md-10 text-right" >
																	Distance Bureau - Domicile :
																</div>
																<div class="col-md-2" >
																	<?="<strong>" . $u["uti_dist_dom"] . " km</strong>"?>
																</div>

															</div>
														</div>
													</div>

												</div>
												<!-- FIN DONNEE ADMINISTRATIVE -->

												<!-- COORDONNEE PERSO -->
												<div class="col-md-4" >

													<div class="panel">
														<div class="panel-heading">
															<span class="panel-icon">
																<i class="fa fa-home"></i>
															</span>
															<span class="panel-title">Adresse</span>
														</div>
														<div class="panel-body pb5"><?=$utilisateur_adresse?></div>
													</div>
													<div class="panel">
														<div class="panel-heading">
															<span class="panel-icon"><i class="fa fa-user"></i></span>
															<span class="panel-title">Contact perso</span>
														</div>
														<div class="panel-body pb5">
												<?php 		if(!empty($u['uti_tel_perso'])){ ?>
																<div class="row" >
																	<div class="col-md-4 text-right">Téléphone :</div>
																	<div class="col-md-8" >
																		<strong><?=$u["uti_tel_perso"]?></strong>
																	</div>
																</div>
												<?php 		}
															if(!empty($u['uti_mobile_perso'])){ ?>
																<div class="row mt10" >
																	<div class="col-md-4 text-right">Portable :</div>
																	<div class="col-md-8" >
																		<strong><?=$u["uti_mobile_perso"]?></strong>
																	</div>
																</div>
												<?php 		}
															if(!empty($u['uti_mail_perso'])){ ?>
																<div class="row mt10" >
																	<div class="col-md-4 text-right">Mail :</div>
																	<div class="col-md-8" >
																		<strong><?=$u["uti_mail_perso"]?></strong>
																	</div>
																</div>
												<?php		} ?>
														</div>
													</div>
												</div>
												<!-- FIN COORDONNEE PERSO -->
											</div>
								<?php	} ?>
									<hr>
									<h2>Données Informatiques</h2>
									<div class="row" >
											<div class="col-md-3" >
												Version de la tablette : <strong>
												<?php if(!empty($u['uti_version_tab'])){ ?>
													<?= $maj['maj_version'] ?>
												<?php } ?></strong>
											</div>
											<div class="col-md-3" >
												Date de la mise à jour : <strong>
												<?php if(!empty($u['uti_version_tab_date'])){ ?>
													<?= convert_date_txt($u['uti_version_tab_date']) ?>
												<?php } ?></strong>
											</div>
											<div class="col-md-3" >
												Date du dernier téléchargement : <strong>
												<?php if(!empty($u['uti_last_dl'])){ ?>
													<?= convert_date_txt($u['uti_last_dl']) ?>
												<?php } ?></strong>
											</div>
											<div class="col-md-3" >
												Date de la dernière synchronisation : <strong>
												<?php if(!empty($u['uti_last_upload'])){ ?>
													<?= convert_date_txt($u['uti_last_upload']) ?>
												<?php } ?></strong>
											</div>
									</div>
									<?php if($_SESSION["acces"]['acc_profil'] == 13 OR $_SESSION["acces"]['acc_service'] == 3){ ?>
										<hr>
										<h2>Données Techniques</h2>
										<div class="row">
												<div class="col-md-3" >
													<input type="checkbox" class="form-check-input" id="valid_auto"
													<?php if(!empty($u['uti_eva_pas_valide_tech'])){ ?>
														checked
													<?php }?>
													>
	    											<label class="form-check-label" for="valid_auto">Validation automatique des rapports d'évacuation </label>
												</div>
										</div>
									<?php }	?>
									</div>

							<?php	if($drt_responsable OR $drt_rh OR $drt_full){ ?>

										<div id="tab2" class="tab-pane <?php if($onglet==2) echo("active") ?>" >

									<?php	if($drt_rh){ ?>
												<h2 style="margin-top:0px;">Accès aux sociétés</h2>
												<div class="panel">
													<div class="panel-heading">
														<span class="panel-title"> Accès aux sociétés</span>
														<span class="panel-controls">
															<a href="#" class="panel-control-loader"></a>
															<a href="#" class="panel-control-collapse"></a>
														</span>
													</div>
													<div class="panel-body bg-light dark pb25">
														<div class="admin-form">
											<?php
															$societes=get_societes_utilisateur($utilisateur,False);
															if(!empty($societes)){
																foreach($societes as $s){ ?>
																	<div class='col-md-3' >
																		<h2 class='text-right mr15' >
																			<?=$s["soc_code"] . "&nbsp;"?>
																			<span class="switch switch-success">
																				<input class="societe_check" id="soc_<?= $s['soc_id'] ?>" type="checkbox" data-societe="<?= $s['soc_id'] ?>"
																				<?php if(!empty($s["uso_utilisateur"])){ ?>
																					checked
																				<?php } ?>>
																				<label data-off="NON" data-on="OUI" for="soc_<?= $s['soc_id'] ?>"></label>
																			</span>
																		</h2>
																<?php 	if($s["soc_agence"]){
																			$agences=get_agences_utilisateur($s['soc_id'],$utilisateur,False);
																			if(!empty($agences)){
																				foreach($agences as $a){
																					echo("<p class='text-right' >");
																						echo($a["age_code"] . "&nbsp;"); ?>
																						<span class="switch switch-success">


																							<input class="agence_check" id="age_<?= $a['age_id'] ?>" type="checkbox" data-societe="<?= $s['soc_id'] ?>" data-agence="<?= $a['age_id'] ?>"
																							<?php if(!empty($a["uso_utilisateur"])){ ?>
																							checked
																							<?php } ?>>
																							<label data-off="NON" data-on="OUI" for="age_<?= $a['age_id'] ?>"></label>
																						</span>

																					<?php echo("</p>");
																				}
																			}
																		} ?>
																	</div>
									<?php						}
															} ?>
														</div>
													</div>
												</div>
									<?php	} ?>

											<h2 style="margin-top:0px;">Droits</h2>

									<?php	if(!empty($d_droits)){
												$service_id=0;

												foreach($d_droits as $d){

													if(!isset($d["exclu"])){

														$droit_affecte=0;
														if(!empty($d["udr_droit"])){
															$droit_affecte=1;
														}

														$droit_reaffecte=0;
														if(!empty($d["udr_reaffecte"])){
															$droit_reaffecte=1;
														}

														if($service_id!=$d["ser_id"]){
															if($service_id>0){

																			echo("</div>");
																		echo("</div>");
																	echo("</div>");
																echo("</div>");
															}
															$service_id=$d["ser_id"]; ?>
															<div class="panel">
																<div class="panel-heading">
																	<span class="panel-title"><?=$d["ser_libelle"]?></span>
																	<span class="panel-controls">
																		<a href="#" class="panel-control-loader"></a>
																		<a href="#" class="panel-control-collapse"></a>
																	</span>
																</div>
																<div class="panel-body bg-light dark pb25">
																	<div class="row">
																		<div class="admin-form" >


										<?php			} ?>

														<div class="row" >
															<div class="col-md-3 text-right mb5">
																<strong><?=$d["drt_nom"]?> :</strong>
																<span class="switch switch-success ml10 mt10 pt10"  style="margin-bottom:0px!important;">
																	<input class="droit_affecte" id="drt_<?=$d["drt_id"]?>" type="checkbox" <?php if($droit_affecte==1) echo("checked") ?> data-droit="<?=$d["drt_id"]?>" >
																	<label data-off="NON" data-on="OUI" for="drt_<?=$d["drt_id"]?>"></label>
																</span>
															</div>
															<div class="col-md-3 text-right mb5">
																<strong>Réaffectation :</strong>
																<span class="switch switch-success ml10 mt10"   style="margin-bottom:0px!important;">
																	<input class="droit_reaffecte" id="affect_<?=$d["drt_id"]?>" type="checkbox" <?php if($droit_reaffecte==1) echo("checked");if($droit_affecte==0) echo("disabled"); ?> data-droit="<?=$d["drt_id"]?>" >
																	<label data-off="NON" data-on="OUI" for="affect_<?=$d["drt_id"]?>"></label>
																</span>
															</div>
															<div class="col-md-6 mt5">
																<p><?=$d["drt_descriptif"]?></p>
															</div>
														</div>
									<?php			}
												}
												if($service_id>0){ ?>
																</div>
															</div>
														</div>
													</div>
									<?php		}

											} ?>
										</div>
							<?php	}?>

									<!-- TECH -->
									<div id="tab4" class="tab-pane <?php if($onglet==4) echo("active") ?>" >
										<h2 style="margin-top:0px;">Tech</h2>

										<div class="col-md-4">
											<div class="panel">
												<div class="panel-heading">
													<span class="panel-title"> Compétences</span>
												</div>
												<div class="panel-body text-center">
													<div class="row">
										<?php
													if(!empty($competences_validees)){
														$numOfCols = 3;
														$rowCount = 0;
														foreach($competences_validees as $c){ ?>
															<div class="col-md-4">
																<p title="Cliquez pour plus d'informations">
																	<a href="intervenant_competence.php?ref=1&ref_id=<?= $utilisateur ?>&uti_competence=<?= $c['com_id'] ?>" id="check_comp_<?=$c["com_id"]?>">
																	<span id='competence_<?=$c["com_id"]?>' class='fa fa-circle text-success' style='font-size:25px;'></span>
																	</a>
																</p>
																<p><?=$c["com_libelle"]?></p>
															</div>
										<?php			$rowCount++;
														if($rowCount % $numOfCols == 0) echo '</div><div class="row mb15">';
														}
													} ?>
													</div>
													<div class="row">
														<form method="GET" action="intervenant_competence.php">
															<div class="col-md-12">
																<input type="hidden" name="ref_id" value="<?= $utilisateur ?>">
																<input type="hidden" name="ref" value="1">
																<div class="section mb15">
																<label for="uti_competence" >Consulter les diplômes liés à une compétence :</label>
																	<select id="uti_competence" class="select2" name="uti_competence" required>
																	<option value="">Choix de la compétence...</option>					<?php foreach($competences_non_validees as $c){ ?>
																			<option value="<?= $c['com_id'] ?>"><?= $c['com_libelle'] ?></option>
																		<?php } ?>
																	</select>
																</div>
															</div>
															<div class="col-md-12">
																<button type="submit" class="btn btn-sm btn-success">Afficher les diplômes</button>
															</div>
														</form>

													</div>
												</div>
											</div>
										</div>

										<!-- LISTE DES DIPLOMES -->
										<div class="col-md-8">
									<?php
											//var_dump($diplomes);

											if(!empty($diplomes)){ ?>
												 <div class="panel">
													<div class="panel-heading panel-md">
														<span class="panel-title"> Diplômes</span>
													</div>
													<div class="panel-body pn">
														<div class="table-responsive">
															<table class="table mbn tc-med-1 tc-bold-2 table-hover pad-right-td">		<thead>
																	<tr class="dark">
																		<th style="text-align:center!important;">Nom du diplôme</th>
																		<th style="text-align:center!important;">Fin de validité</th>
																		<th style="text-align:center!important;">Pdf</th>
															<?php		if($_SESSION['acces']['acc_droits'][3]){ ?>
																			<th style="text-align:center!important;">Actions</th>
															<?php		} ?>
																	</tr>
																</thead>
																<tbody>
													<?php			foreach($diplomes as $d){
																		$classe_doc="";
																		$classe_doc="";
																		$idi_date_fin="&nbsp;";

																		$class_perime="";
																		$style_perime="";
																		if(!empty($d["idi_date_fin"])){
																			$idi_date_fin=convert_date_txt($d["idi_date_fin"]);
																			if($d["idi_date_fin"]<date("Y-m-d")){
																				$class_perime="danger";
																				//$style_perime="style='color:#FFF;'";
																			}
																		}

																		?>
																		<tr class="text-center <?=$class_perime?>">
																			<td><?=$d["dip_libelle"]?></td>
																			<td class="text-center  <?=$style_perime?>" ><?=$idi_date_fin?></td>
													<?php					if(!empty($d["idi_fichier"]) AND file_exists("documents/utilisateurs/diplomes/" . $d["idi_fichier"])){ ?>
																				<td>
																					<a href="documents/utilisateurs/diplomes/<?=$d["idi_fichier"]?>" target="_blank" class="btn btn-success btn-sm">
																						<i class="fa fa-file-pdf-o"></i>
																					</a>
																				</td>
													<?php					}else{
																				echo("<td>&nbsp;</td>");
																			};
																			if($_SESSION['acces']['acc_droits'][3]){ ?>
																				<td>
																					<a href="intervenant_diplome.php?ref=1&ref_id=<?=$utilisateur?>&diplome=<?=$d["dip_id"]?>&onglet=4" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="bottom"  title="Modifier" >
																					   <i class="fa fa-pencil"></i>
																					</a>
																					<button type="button" class="btn btn-sm btn-danger diplome-supp" data-diplome="<?=$d["dip_id"]?>" data-toggle="tooltip" title="Supprimer le diplôme" data-placement="bottom">
																						<i class="fa fa-times"></i>
																					</button>
																				</td>
													<?php					} ?>
																		</tr>
													<?php			} ?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
									<?php	}?>

										</div>
									</div>


								</div>
							</div>
						</div>
					</div>
	<?php		} ?>
			</section>
		</section>
	</div>


<div id="modal_confirmation_user" class="modal fade" role="dialog" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" >
					<i class="fa fa-info" ></i> - <span>Confirmation</span>
				</h4>
			</div>
			<div class="modal-body" >

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
					<i class="fa fa-close" ></i>
					Annuler
				</button>
				<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
					<i class="fa fa-check" ></i>
					Confirmer
				</button>
			</div>
		</div>
	</div>
</div>
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left">
	<?php 	if(isset($_GET['retour_interv'])){ ?>
				<a href="intervenant_liste.php" class="btn btn-default btn-sm">
					<i class="fa fa-long-arrow-left"></i>
					Retour
				</a>
	<?php 	}elseif(isset($_SESSION["retourUtilisateur"])){ ?>
				<a href="<?=$_SESSION["retourUtilisateur"]?>" class="btn btn-default btn-sm">
					<i class="fa fa-long-arrow-left"></i>
					Retour
				</a>
	<?php	}else{ ?>
				<a href="utilisateur_liste.php" class="btn btn-default btn-sm">
					<i class="fa fa-long-arrow-left"></i>
					Retour
				</a>
	<?php 	} ?>


<?php
$_SESSION['retour']="utilisateur_voir.php?utilisateur=" . $utilisateur;
?>
        </div>
        <div class="col-xs-3 footer-middle"></div>
        <div class="col-xs-6 footer-right">
        <?php if($_SESSION["acces"]["acc_droits"][3]==1){ ?>
			<button id="EditDip" class="btn btn-success btn-sm" >
				 <i class='fa fa-plus'></i> Ajouter un diplôme
			</button>
<?php	} ?>
	<?php if($_SESSION["acces"]["acc_droits"][21]==1){ ?>
			<button id="EditUti" class="btn btn-warning btn-sm" >
				 <i class='fa fa-pencil'></i> Editer
			</button>
			<?php
				if ($_ENV['APP_ENV'] && $_ENV['APP_ENV'] == 'local') { ?>
			<a href="utilisateur_plateforme.php?id=<?= $utilisateur ?>" class="btn btn-success btn-sm" >
				 <i class='fa fa-upload'></i> Transfert vers la plateforme
			</a>
				<?php } ?>
<?php	} ?>
        </div>
    </div>
</footer>

<?php
		include "includes/footer_script.inc.php"; ?>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script src="vendor/plugins/select2/js/i18n/fr.js"></script>

<script type="text/javascript">

    jQuery(document).ready(function () {
		var onglet_actif="<?=$onglet?>";
		var utilisateur="<?=$utilisateur?>";
		// GESTION DES ONGLETS

		if(onglet_actif==1){
			$("#EditUti").show();
		}else{
			$("#EditUti").hide();
		}

		if(onglet_actif==4){
			$("#EditDip").show();
		}else{
			$("#EditDip").hide();
		}

		$('#onglet_1').click(function(){
			onglet_actif=1;
			$("#EditUti").show();
			$("#EditDip").hide();
		})
		$('#onglet_2').click(function(){
			onglet_actif=2;
			$("#EditUti").hide();
			$("#EditDip").hide();
		})
		$('#onglet_3').click(function(){
			onglet_actif=3;
			$("#EditUti").hide();
			$("#EditDip").hide();
		})
		$('#onglet_4').click(function(){
			onglet_actif=4;
			$("#EditUti").hide();
			$("#EditDip").show();
		})
		$('#onglet_5').click(function(){
			onglet_actif=5;
			$("#EditUti").hide();
			$("#EditDip").hide();

		})
		$('#EditUti').click(function(){
			editer_utilisateur(utilisateur,onglet_actif);
		});
		$('#EditDip').click(function(){
			ajouter_diplome(utilisateur,onglet_actif);
		});
		$("#valid_auto").click(function(){
			$.ajax({
				url : 'ajax/ajax_uti_pas_valide.php?utilisateur=<?=$utilisateur?>',
				type : 'GET',
				//data : 'droit=' + droit + '&utilisateur=<?=$utilisateur?>&action=1',
				dataType : 'json',

				success : function(data){

				},
				error : function(data){
					console.log(data.responseText)
				}
			});
		});
		// lorqu'on coche / decoche une societe
		$('.societe_check').click(function()
		{	var societe = $(this).data('societe');
			var coche = $(this).is(':checked');
			//if(coche){
				$.ajax({
					url : 'ajax/ajax_societe_utilisateur.php?societe=' + societe + '&utilisateur=<?=$utilisateur?>&coche=' + coche,
					type : 'GET',
					//data : 'droit=' + droit + '&utilisateur=<?=$utilisateur?>&action=1',
					dataType : 'json',

					success : function(data){
						console.log("data : " + data);
						if(data != ""){
							$.each(data, function(key, value) {
								$("#age_" + value['uso_agence']).prop('checked', true);
							});
						}

					},
					error : function(data){
						console.log(data.responseText)
					}
				});
		//	}
		});
		// lorqu'on coche / decoche une agence
		$('.agence_check').click(function()
		{	var societe = $(this).data('societe');
			var agence = $(this).data('agence');
			var coche = $(this).is(':checked');
			//if(!coche){
				$.ajax({
					url : 'ajax/ajax_societe_utilisateur.php?agence=' + agence + '&societe=' + societe + '&utilisateur=<?=$utilisateur?>&coche=' + coche,
					type : 'GET',
					//data : 'droit=' + droit + '&utilisateur=<?=$utilisateur?>&action=1',
					dataType : 'html',

					success : function(code_html, statut){
						console.log("code_html : " + code_html);

						if(code_html != ""){
							$("#soc_" + code_html).prop('checked', false);
						}
					},
					error : function(resultat, statut, erreur){

					}
				});
			//}
		});

		$('.droit_affecte').click(function()
		{	var droit = $(this).data('droit');
			var coche = $(this).is(':checked');
			$.ajax({
				url : 'ajax/ajax_droit.php?droit=' + droit + '&utilisateur=<?=$utilisateur?>&action=1',
				type : 'GET',
				//data : 'droit=' + droit + '&utilisateur=<?=$utilisateur?>&action=1',
				dataType : 'html',

				success : function(code_html, statut){
					if(!coche){
						$("#affect_" + droit).prop('checked', false);
						$("#affect_" + droit).prop('disabled', true);
					}else{
						$("#affect_" + droit).prop('disabled', false);
					};
				},
				error : function(resultat, statut, erreur){
					if(coche){
						$("#drt_" + droit).prop('checked',false);
					}else{
						$("#drt_" + droit).prop('checked',true);
					};
				}
			});
		});


		$('.droit_reaffecte').click(function()
		{	var droit = $(this).data('droit');
			var coche = $(this).is(':checked');
			if(droit>0){
				$.ajax({
					url : 'ajax/ajax_droit.php?droit=' + droit + '&utilisateur=<?=$utilisateur?>&action=2',
					type : 'GET',
					dataType : 'html',
					success : function(code_html, statut){

					},
					error : function(resultat, statut, erreur){
						if(coche){
							$("#affect_" + droit).prop('checked',false);
						}else{
							$("#affect_" + droit).prop('checked',true);
						};
					}
				});
			};
		});

		// SUPRESSION D'UN DIPLOMES


		$(".diplome-supp").click(function(){
			var diplome=$(this).attr("data-diplome");
			modal_confirmation_user("Êtes-vous sûr de vouloir supprimer ce diplôme ?",supprimer_diplome,diplome,utilisateur,"");
		});




    });


	function editer_utilisateur(utilisateur,onglet_actif){
		document.location.href="utilisateur.php?utilisateur=" + utilisateur + "&onglet=" + onglet_actif;
	}
	function ajouter_diplome(utilisateur,onglet_actif){
		document.location.href="intervenant_diplome.php?ref=1&ref_id=" + utilisateur + "&onglet=" + onglet_actif;
	}
	function supprimer_diplome(diplome,utilisateur){

		document.location.href="intervenant_diplome_supp.php?ref=1&ref_id=" + utilisateur + "&diplome=" + diplome + "&onglet=4";
	}


	</script>
</body>
</html>
