<?php

// AFFICHE LA LISTE DES FACTURES

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');

// DONNEE UTILE AU PROGRAMME

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}

if (!empty($_POST)){
	// le formulaire a été soumit
	// on actualise les criteres de recherche


	$type = 0;
	if(!empty($_POST['type'])){
		$type = $_POST['type'];
	}


	$societe = 0;
	if(!empty($_POST['fac_societe'])){
		$societe = $_POST['fac_societe'];
	}
	$fac_chrono=0;
	if(!empty($_POST['fac_chrono'])){
		$fac_chrono=$_POST['fac_chrono'];
	}

	$fac_client=0;
	if(!empty($_POST['fac_client'])){
		$fac_client=$_POST['fac_client'];
	}

	$fac_ttc=0;
	if(!empty($_POST['fac_ttc'])){
		$fac_ttc=floatval($_POST['fac_ttc']);
	}

	$fac_commercial=0;
	if(!empty($_POST['fac_commercial'])){
		$fac_commercial=$_POST['fac_commercial'];
	}

	$fac_relance=null;
	if(!empty($_POST['fac_relance'])){
		$fac_relance=$_POST['fac_relance'];
	}

	if($type>0 AND empty($fac_relance)){
		$err_txt="Vous devez selectionner un statut de relance pour pouvoir utiliser l'outil 'relance en fonction de l'étape'";
	}

	$cli_cp="";
	if(!empty($_POST['cli_cp'])){
		$cli_cp=$_POST['cli_cp'];
	}

	$cli_cp="";
	if(!empty($_POST['cli_cp'])){
		$cli_cp=$_POST['cli_cp'];
	}

	$cli_code="";
	if(!empty($_POST['cli_code'])){
		$cli_code=$_POST['cli_code'];
	}

	$fac_date_reg_prev_deb="";
	if(!empty($_POST['fac_date_reg_prev_deb'])){
		$fac_date_reg_prev_deb=convert_date_sql($_POST['fac_date_reg_prev_deb']);
	}
	$fac_date_reg_prev_fin="";
	if(!empty($_POST['fac_date_reg_prev_fin'])){
		$fac_date_reg_prev_fin=convert_date_sql($_POST['fac_date_reg_prev_fin']);
	}
	$rel_con_nom="";
	if(!empty($_POST['rel_con_nom'])){
		$rel_con_nom=$_POST['rel_con_nom'];
	}
	$rel_con_prenom="";
	if(!empty($_POST['rel_con_prenom'])){
		$rel_con_prenom=$_POST['rel_con_prenom'];
	}

	$filt_stop = false;
	if(!empty($_POST['filt_stop'])){
		$filt_stop=true;
	}

	$filt_non_solde = false;

	if(!empty($_POST['filt_non_solde'])){
		$filt_non_solde=true;
	}
	// mémorisation des critere

	$_SESSION['fac_relance_tri'] = array(
		"fac_chrono" => $fac_chrono,
		"fac_client" => $fac_client,
		"fac_ttc" => $fac_ttc,
		"fac_commercial" => $fac_commercial,
		"fac_relance" => $fac_relance,
		"fac_date_reg_prev_deb"=>$fac_date_reg_prev_deb,
		"fac_date_reg_prev_fin"=>$fac_date_reg_prev_fin,
		"cli_cp" => $cli_cp,
		"filt_stop" => $filt_stop,
		"filt_non_solde" => $filt_non_solde,
		"etape" => $fac_relance,
		"type" => $type,
		"cli_code" => $cli_code,
		"societe" => $societe,
		"rel_con_nom" => $rel_con_nom,
		"rel_con_prenom" => $rel_con_prenom
	);
}
$_SESSION['retourliste'] = "facture_relance_liste.php";
$_SESSION['retour'] = "facture_relance_liste.php";
$_SESSION['retourFacture'] = "facture_relance_liste.php";
$_SESSION['retourFactureRelance'] = "facture_relance_liste.php";
$origine_fac="actvoir";

if(!isset($err_txt)){

	// DONNEES COMPLEMENTAIRES POUR LA REQUETE

	$sql="SELECT * FROM Relances_Etats ORDER BY ret_j_deb";
	$req = $Conn->query($sql);
	$d_results=$req->fetchAll();
	$etapes = array();
	if(!empty($_SESSION['fac_relance_tri']['type'])){
		foreach($d_results as $k=>$r){
			$etapes[$r['ret_id']] = $r['ret_libelle'];

			if($_SESSION['fac_relance_tri']['type'] == 1){
				// en fonction de l'étape
				if($r['ret_id'] == $_SESSION['fac_relance_tri']['fac_relance']){

					if($k==0){
						$_SESSION['fac_relance_tri']['etape'] = 0;
					}else{
						$_SESSION['fac_relance_tri']['etape'] = $d_results[$k-1]['ret_id'];
					}
					$delai = -$r['ret_j_deb'];

					$_SESSION['fac_relance_tri']['fac_date_reg_prev_fin'] = date('Y-m-d', strtotime(date('Y-m-d') . $delai . ' days'));
					$_SESSION['fac_relance_tri']['fac_date_reg_prev_deb'] = null;

				}
			}elseif($_SESSION['fac_relance_tri']['type'] == 2){
				if($r['ret_id'] == $_SESSION['fac_relance_tri']['fac_relance']){
					//$_SESSION['fac_relance_tri']['etape'] = 1;
					$exclure=1;
					$delai_debut = -$r['ret_j_deb'];
					$delai_fin = -$r['ret_j_fin'];
					$_SESSION['fac_relance_tri']['fac_date_reg_prev_fin'] = date('Y-m-d', strtotime(date('Y-m-d') . $delai_debut . ' days'));
					$_SESSION['fac_relance_tri']['fac_date_reg_prev_deb'] = date('Y-m-d', strtotime(date('Y-m-d') . $delai_fin . ' days'));
				}elseif(!empty($exclure)){
					if(empty($_SESSION['fac_relance_tri']['etape'])){
						$_SESSION['fac_relance_tri']['etape'] = $r["ret_id"];
					}else{
						$_SESSION['fac_relance_tri']['etape'] = $_SESSION['fac_relance_tri']['etape'] . "," . $r["ret_id"];
					}
				}

			}
		}
	}

	// ON CONSTRUIT LA REQUETE

	$critere=array();


	$mil="";
	if(!empty($_SESSION['fac_relance_tri']['fac_chrono'])){
		$mil.=" AND fac_chrono =:fac_chrono";
		$critere["fac_chrono"]=$_SESSION['fac_relance_tri']['fac_chrono'];
	}else{
		if(!empty($_SESSION['fac_relance_tri']['rel_con_nom']) OR !empty($_SESSION['fac_relance_tri']['rel_con_prenom'])){
			if(!empty($_SESSION['fac_relance_tri']['rel_con_nom']) && !empty($_SESSION['fac_relance_tri']['rel_con_prenom'])){
				$sql_rel="SELECT rel_con_nom, rel_id FROM Relances WHERE rel_con_nom LIKE :rel_con_nom AND rel_con_prenom LIKE :rel_con_prenom";
				$req_rel = $Conn->prepare($sql_rel);
				$req_rel->bindValue("rel_con_nom","%" . $_SESSION['fac_relance_tri']['rel_con_nom'] . "%");
				$req_rel->bindValue("rel_con_prenom","%" . $_SESSION['fac_relance_tri']['rel_con_prenom'] . "%");
			}elseif(empty($_SESSION['fac_relance_tri']['rel_con_nom']) && !empty($_SESSION['fac_relance_tri']['rel_con_prenom'])){
				$sql_rel="SELECT rel_con_nom, rel_id FROM Relances WHERE rel_con_prenom LIKE :rel_con_prenom";
				$req_rel = $Conn->prepare($sql_rel);
				$req_rel->bindValue("rel_con_prenom","%" . $_SESSION['fac_relance_tri']['rel_con_prenom'] . "%");
			}else{
				$sql_rel="SELECT rel_con_nom, rel_id FROM Relances WHERE rel_con_nom LIKE :rel_con_nom";
				$req_rel = $Conn->prepare($sql_rel);
				$req_rel->bindValue("rel_con_nom","%" . $_SESSION['fac_relance_tri']['rel_con_nom'] . "%");
			}

			$req_rel->execute();
			$d_relances=$req_rel->fetchAll();

			foreach($d_relances as $r){
				$sql_relf="SELECT * FROM Relances_Factures WHERE rfa_relance = :rfa_relance";
				$req_relf = $Conn->prepare($sql_relf);
				$req_relf->bindValue("rfa_relance",$r['rel_id']);
				$req_relf->execute();
				$d_relances_factures=$req_relf->fetchAll();
			}
			$facture_table = array();
			foreach($d_relances_factures as $d_r){
				if((!empty($_SESSION['fac_relance_tri']['societe']) && $_SESSION['fac_relance_tri']['societe'] == $d_r['rfa_facture_soc']) OR empty($_SESSION['fac_relance_tri']['societe'])){
					$facture_table[$d_r['rfa_facture_soc']][] = $d_r['rfa_facture'];
				}
			}

		};
		if(!empty($_SESSION['fac_relance_tri']['fac_client'])){
			$mil.=" AND fac_client=:fac_client";
			$critere["fac_client"]=$_SESSION['fac_relance_tri']['fac_client'];
		};
		if(!empty($_SESSION['fac_relance_tri']['fac_ttc'])){
			$mil.=" AND fac_total_ttc=:fac_ttc";
			$critere["fac_ttc"]=$_SESSION['fac_relance_tri']['fac_ttc'];
		}
		if(!empty($_SESSION['fac_relance_tri']['fac_commercial'])){
			$mil.=" AND fac_commercial=:fac_commercial";
			$critere["fac_commercial"]=$_SESSION['fac_relance_tri']['fac_commercial'];
		}

		if(!empty($_SESSION['fac_relance_tri']['cli_cp'])){
			$mil.=" AND cli_cp LIKE :cli_cp";
			$critere["cli_cp"]="%" . $_SESSION['fac_relance_tri']['cli_cp'] . "%";
		}
		if(!empty($_SESSION['fac_relance_tri']['cli_code'])){
			$mil.=" AND cli_code LIKE :cli_code";
			$critere["cli_code"]=$_SESSION['fac_relance_tri']['cli_code'] . "%";

		};
		if(!empty($_SESSION['fac_relance_tri']['fac_cli_categorie'])){
			$mil.=" AND cli_categorie =:fac_cli_categorie";
			$critere["fac_cli_categorie"]=$_SESSION['fac_relance_tri']['fac_cli_categorie'];
		};

		$mil.=" AND fac_nature != 2";

		if(!is_null($_SESSION['fac_relance_tri']['etape'])){
			if($_SESSION['fac_relance_tri']['type']==1){

				// EN FONCTION DE L'ETAPE

				if($_SESSION['fac_relance_tri']['etape']==0){
					// releve de facture
					$mil.=" AND (fac_etat_relance =0 OR ISNULL(fac_etat_relance) )";

				}elseif($_SESSION['fac_relance_tri']['etape']==1){
					// user demande etape R1 donc ORION calcul etap releve soit ID 1
					// il faut les fac en releve et les fac qui n'ont jamais été relancées (on ne conservera que les fac non relancé pour les client qui ne voulait pas de relevé)
					$mil.=" AND (fac_etat_relance =0 OR ISNULL(fac_etat_relance) OR fac_etat_relance=1)";


				}else{
					$mil.=" AND fac_etat_relance =:etape";
				}


			}elseif($_SESSION['fac_relance_tri']['type']==2){
				// EN FONCTION DU RETARD

				$mil.=" AND fac_etat_relance NOT IN(" . $_SESSION['fac_relance_tri']['etape'] . ")";

			}else{
				// NORMAL
				$mil.=" AND fac_etat_relance =:etape";
			}
			$critere["etape"]=$_SESSION['fac_relance_tri']['etape'];
		}
		if($_SESSION['fac_relance_tri']['type'] > 0){
			$mil.=" AND NOT fac_relance_stop  AND ((fac_total_ttc>fac_regle AND fac_nature=1))";
		}

		if(!empty($_SESSION['fac_relance_tri']['filt_non_solde'])){
			$mil.=" AND ((fac_total_ttc>fac_regle AND fac_nature=1)) AND fac_total_ttc-fac_regle > 0.5";


		};

		if(!empty($_SESSION['acces']['acc_agence'])){
			$mil.=" AND fac_agence = :fac_agence";
			$critere["fac_agence"]=$_SESSION['acces']['acc_agence'];
		};
		if(!empty($_SESSION['fac_relance_tri']['filt_stop'])){
			$mil.=" AND fac_relance_stop != 0";
		}

		if(!empty($_SESSION['fac_relance_tri']['fac_date_reg_prev_deb'])){
			$mil.=" AND ((fac_date_reg_prev >= :fac_date_reg_prev_deb AND fac_date_reg_rel IS NULL) OR fac_date_reg_rel >= :fac_date_reg_prev_deb)";
			$critere["fac_date_reg_prev_deb"]=$_SESSION['fac_relance_tri']['fac_date_reg_prev_deb'];
		};
		if(!empty($_SESSION['fac_relance_tri']['fac_date_reg_prev_fin'])){
			$mil.=" AND ((fac_date_reg_prev <= :fac_date_reg_prev_fin AND fac_date_reg_rel IS NULL) OR fac_date_reg_rel <= :fac_date_reg_prev_fin)";
			$critere["fac_date_reg_prev_fin"]=$_SESSION['fac_relance_tri']['fac_date_reg_prev_fin'];
		};


	}

	$mil.= " AND cli_id=fac_client AND (cli_interco_soc = 0 OR cli_interco_soc IS NULL)";

	// champ a selectionne
	$sql="SELECT DISTINCT fac_aff_remise,cli_affacturable, fac_chrono, cli_id,cli_code,cli_nom,fac_id,fac_numero,fac_date,fac_date_reg_prev,fac_total_ttc,fac_regle, fac_etat_relance,fac_commercial,fac_date_reg_rel
	,fac_relance_stop,fac_nature,fac_opca, fac_total_ht, fac_client
	FROM Clients,Factures ";
	/*if($rech_sur_contact){
		$sql=$sql . ",Contacts";
	}*/

	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY fac_date_reg_rel DESC;";

	//SOCIETE
	if(!empty($_SESSION['fac_relance_tri']['societe'])){
		$sql_soc="SELECT soc_id,soc_nom, soc_code FROM Societes WHERE  soc_id = " . $_SESSION['fac_relance_tri']['societe'];

	}else{
		$sql_soc="SELECT soc_id,soc_nom, soc_code FROM Societes WHERE NOT soc_archive ORDER BY soc_nom;";

	}
	$req_soc = $Conn->query($sql_soc);
	$d_societes=$req_soc->fetchAll();

	foreach($d_societes as $s){
		$ConnFct = connexion_fct($s['soc_id']);
		$req = $ConnFct->prepare($sql);

		if(!empty($critere)){
			foreach($critere as $c => $u){
				$req->bindValue($c,$u);
			}
		};
		$req->execute();

		$factures[$s['soc_id']] = $req->fetchAll();
	}
	//die();

	// CHERCHER LES LIBELLE RELANCES_ETATS POUR SELECT TYPE DE PROCéDURE
	$sql="SELECT * FROM Relances_Etats ORDER BY ret_j_deb";
	$req = $Conn->query($sql);
	$d_statut=$req->fetchAll();

	// chercher les libelles
	$d_statut_libelle=array();
	foreach($d_statut as $ds){
		$d_statut_libelle[$ds['ret_id']]= $ds['ret_libelle'];
	}

	session_write_close();

} ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<form  action="facture_relance_etape.php?etat_relance=<?= $_SESSION['fac_relance_tri']['fac_relance'] ?>" method="POST" class="admin-form">
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">

			<?php		if(!isset($err_txt)){ ?>

							<h1>Liste des factures</h1>

			<?php			foreach($d_societes as $s)
							{
								if(!empty($factures[$s['soc_id']]))
								{
									if(!isset($facture_table) OR (isset($facture_table) && !empty($facture_table[$s['soc_id']]))){

										$total_ht=0;
										$total_ttc=0;
										$total_regle=0;
										$total_reste_du=0;
										$data_order=5; ?>
										<h4 class="text-primary text-left pt15"><?= $s['soc_code'] ?></h4>

										<div class="table-responsive">

											<table class="table dataTable table-striped table-hover" data-tri_col="<?=$data_order?>" data-tri_sens="desc" id="table_id">
												<thead>
													<tr class="dark2" >
														<th>Société</th>
														<th>Code client</th>
														<th>Nom client</th>
														<th style="min-width:100px;">Facture</th>
														<th>Commercial</th>
														<th>Date</th>
														<th>Echéance</th>
														<th>Date ref. relance</th>
														<th class="text-right">T.T.C.</th>
														<th class="text-right">Réglé</th>
														<th class="text-right">Reste Dû</th>
														<th>État Relance</th>
												<?php 	if($_SESSION['fac_relance_tri']['type'] != 0)
														{ ?>
															<th class="text-center no-sort">
																<label class="option mn" data-toggle="tooltip" title="Sélectionner tout">
																	<input type="checkbox" class="selectall" data-soc="<?= $s['soc_id'] ?>" name="mobileos" value="FR">
																	<span class="checkbox mn"></span>
																</label>
															</th>
												<?php 	} ?>
													</tr>
												</thead>
												<tbody>
										<?php		foreach($factures[$s['soc_id']] as $f)
													{

														$continue = false;
														if(isset($facture_table)){
															if(!empty($facture_table[$s['soc_id']])){
																foreach($facture_table[$s['soc_id']] as $fac_ta){
																	if($fac_ta == $f['fac_id']){
																		$continue = true;

																	}
																}
															}else{
																$continue = false;
															}
														}else{
															$continue = true;

														}

														if($continue==true){

															if($_SESSION['fac_relance_tri']['type'] == 1){

																// EN FONCTION DE L'ETAPE

																if($_SESSION['fac_relance_tri']['etape'] == 0){

																	// U demande etape releve


																	$sql_cli="SELECT cli_releve FROM Clients WHERE cli_id = " . $f['cli_id'];
																	$req_cli = $Conn->query($sql_cli);
																	$d_client=$req_cli->fetch();
																	if(empty($d_client['cli_releve'])){
																		// le client ne veut pas de releve -> on exclut la fac
																		$continue = false;
																	}

																}elseif($_SESSION['fac_relance_tri']['etape'] == 1){

																	// U demande etape R1

																	if(empty($f['fac_etat_relance'])){

																		$sql_cli="SELECT cli_releve FROM Clients WHERE cli_id = " . $f['cli_id'];
																		$req_cli = $Conn->query($sql_cli);
																		$d_client=$req_cli->fetch();
																		if(!empty($d_client['cli_releve'])){
																			// le client veut une releve -> on exclut la fac. Elle de doit pas ressortir tant que le releve n'a pas été envoyé.
																			$continue = false;
																		}

																	}
																}

															}elseif($_SESSION['fac_relance_tri']['type'] == 2){

																if($_SESSION['fac_relance_tri']['fac_relance'] == 1){

																	// le retard nécéssitent un relevé -> on s'assure que le client est d'accord pour le recevoir
																	$continue = false;
																	$sql_cli="SELECT cli_releve FROM Clients WHERE cli_id = " . $f['cli_id'];
																	$req_cli = $Conn->query($sql_cli);
																	$d_client=$req_cli->fetch();
																	if(!empty($d_client['cli_releve'])){
																		$continue = true;
																	}
																}
															}
														}

														if($continue == true){

															$continue = false;
															$commercial = "";
															if(!empty($f['fac_commercial'])){
																$ConnFct = connexion_fct($s['soc_id']);
																$sql="SELECT com_label_1, com_label_2 FROM Commerciaux WHERE com_id = " . $f['fac_commercial'];
																$req = $ConnFct->query($sql);
																$d_commercial=$req->fetch();
																$commercial = $d_commercial['com_label_2'] . " " . $d_commercial['com_label_1'];
															}

															$fac_date="";
															if(!empty($f['fac_date'])){
																$dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
																$fac_date=$dt_fac_date->format("d/m/Y");
															}
															$fac_date_reg_prev="";
															if(!empty($f['fac_date_reg_prev'])){
																$dt_fac_date_reg_prev=date_create_from_format("Y-m-d",$f['fac_date_reg_prev']);
																$fac_date_reg_prev=$dt_fac_date_reg_prev->format("d/m/Y");
															}
															$fac_date_reg_rel="";
															if(!empty($f['fac_date_reg_rel'])){
																$dt_fac_date_reg_rel=date_create_from_format("Y-m-d",$f['fac_date_reg_rel']);
																$fac_date_reg_rel=$dt_fac_date_reg_rel->format("d/m/Y");
															}
															$reste_du=$f['fac_total_ttc']-$f['fac_regle'];

															$total_ht+=$f['fac_total_ht'];
															$total_ttc+=$f['fac_total_ttc'];
															$total_regle+=$f['fac_regle'];
															$total_reste_du+=$reste_du;

															// style de la ligne
															$facture_opca=false;
															$fac_opca=0;
															if(!empty($f['fac_opca'])){
																$fac_opca = $f['fac_opca'];
																$facture_opca=true;
															}

															$style_tr="";
															$style_a="";
															if($f['fac_relance_stop']){
																if($f['fac_relance_stop'] == 1){
																	$style_tr="style='color:orange;font-weight:bold;'";
																	$style_a="color:orange;font-weight:bold;";
																}elseif($f['fac_relance_stop'] == 2){
																	$style_tr="style='color:green;font-weight:bold;'";
																	$style_a="color:green;font-weight:bold;";
																}elseif($f['fac_relance_stop'] == 3){
																	$style_tr="style='color:red;font-weight:bold;'";
																	$style_a="color:red;font-weight:bold;";
																}
															}elseif($facture_opca){
																$style_tr="style='color:purple;font-weight:bold;'";
																$style_a="color:purple;font-weight:bold;";
															}
															$class_fac = "";
															if($_SESSION['acces']['acc_service'][2]==1 AND empty($style)){

																if($f['fac_aff_remise']>0){
																	$class_fac="class='system'";
																}elseif($f['cli_affacturable']!=1 OR $f['fac_opca']>0){
																	$class_fac="class='warning'";
																}
															}

															$style_reste ="";
															if($f['fac_relance_stop']==1 AND $reste_du<0){
																$style_reste="style='" . $style_a . "font-weight:bold;'";
															}else{
																$style_reste="style='" . $style_a . "'";
															}

															if(!empty($style_a)){
																$style_a="style='" . $style_a . "'";
															} ?>

															<tr <?= $style_tr ?> <?= $class_fac ?>>
																<td><?=	$s['soc_code']?></td>
																<td>

																	<a href="facture_relance_voir.php?client=<?= $f['cli_id'] ?>" <?=$style_a?>>
																		<?= $f['cli_code']?>
																	</a>
																</td>
																<td><?= $f['cli_nom']?></td>
																<td>
																	<a href="facture_voir.php?facture=<?=$f['fac_id']?>&societ=<?= $s['soc_id'] ?>" <?= $style_a ?>>
																		<?= $f['fac_numero'] ?>
																	</a>
																</td>
																<td>
																	<?= $commercial ?>
																</td>
																<td data-order="<?= $f['fac_date'] ?>"><?=	$fac_date?></td>
																<td data-order="<?= $f['fac_date_reg_prev'] ?>"><?= $fac_date_reg_prev ?></td>
																<td data-order="<?= $f['fac_date_reg_rel'] ?>"><?= $fac_date_reg_rel ?></td>
																<td data-order="<?= $f['fac_total_ttc'] ?>" class="text-right" >
													<?php			if($_SESSION["acces"]["acc_droits"][30]){ ?>
																		<a href="reglement.php?facture=<?=$f['fac_id']?>&societ=<?= $s['soc_id'] ?>" <?=$style_a?>>
																			<?=number_format($f['fac_total_ttc'], 2, ',', ' ');?>
																		</a>
													<?php			}else{
																		echo(number_format($f['fac_total_ttc'], 2, ',', ' '));
																	} ?>
																</td>
																<td data-order="<?= $f['fac_regle'] ?>" class="text-right" >
													<?php			if($_SESSION["acces"]["acc_droits"][30]){ ?>
																		<a href="reglement.php?facture=<?=$f['fac_id']?>&societ=<?= $s['soc_id'] ?>" <?=$style_a?>>
																			<?= number_format($f['fac_regle'], 2, ',', ' ');?>
																		</a>
													<?php			}else{
																		echo(number_format($f['fac_regle'], 2, ',', ' '));
																	} ?>
																</td>
																<td data-order="<?= $reste_du ?>" class="text-right" >
													<?php			if($_SESSION["acces"]["acc_droits"][30]){ ?>
																		<a href="reglement.php?facture=<?=$f['fac_id']?>&societ=<?= $s['soc_id'] ?>" <?=$style_reste?>>
																			<?= number_format($reste_du, 2, ',', ' ');?>
																		</a>
												<?php				}else{
																		echo(number_format($reste_du, 2, ',', ' '));
																	} ?>
																</td>
																<td class="text-center" >
																	<?php if(!empty($f['fac_etat_relance'])){ ?>
																		<a href="facture_relance.php?facture=<?= $f['fac_id'] ?>&societ=<?= $s['soc_id'] ?>" <?=$style_a?>>
																			<?= $d_statut_libelle[$f['fac_etat_relance']] ?>
																		</a>
																	<?php }?>
																</td>
														<?php 	if(!empty($fac_opca))
																{ ?>
																	<input type="hidden" name="facture_opca[<?= $f['cli_id'] ?>]" value="<?= $fac_opca ?>">
														<?php 	}
																if($_SESSION['fac_relance_tri']['type'] != 0)
																{ ?>
																	<td class="text-center">
																		<label class="option block mn">
																			<input type="checkbox" class="checkbox<?= $s['soc_id'] ?>" name="check_list[<?= $s['soc_id'] ?>][]" value="<?= $f['fac_id'] ?>">
																			<span class="checkbox mn"></span>
																		</label>
																	</td>
														<?php 	} ?>
															</tr>
								<?php					}
													} ?>
												</tbody>
												<tfoot>
													<tr>
														<th colspan="8" >Total :</th>
														<td class="text-right" ><?=number_format($total_ttc, 2, ',', ' ');?></td>
														<td class="text-right" ><?=number_format($total_regle, 2, ',', ' ');?></td>
														<td class="text-right" ><?=number_format($total_reste_du, 2, ',', ' ');?></td>
														<td>&nbsp;</td>
													</tr>
												</tfoot>
											</table>
										</div>
	<?php							}
								}
							}
						}else{
							echo("<p class='alert alert-danger' >" . $err_txt . "</p>");
						} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="facture_relance_tri.php" class="btn btn-default btn-sm" role="button">
							<span class="fa fa-search"></span>
							<span class="hidden-xs">Nouvelle recherche</span>
						</a>
					</div>
					<div class="col-xs-6 footer-middle">&nbsp;</div>
					<div class="col-xs-3 footer-right">
			<?php		if(!isset($err_txt)){
							if($_SESSION['fac_relance_tri']['type'] != 0){ ?>
								<button type="submit" class="btn btn-success btn-sm">
									Passer à l'étape d'après
								</button>
			<?php 			}
						} ?>
					</div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function (){
				tri_col=$(".dataTable").data("tri_col");
				tri_sens=$(".dataTable").data("tri_sens");
				$('.dataTable').DataTable({
					"language": {
					"url": "vendor/plugins/DataTables/media/js/French.json"
					},
					"order":[tri_col,tri_sens],
					"paging": false,
					"searching": false,
					"info": false,
					"columnDefs": [ {
						"targets": 'no-sort',
						"orderable": false,
					}]
				});
				// SELECTIONNER TOUT
				$(".selectall").each(function() {
					$(this).change(function(){
						soc = $(this).data('soc');
						$(".checkbox" + soc).prop('checked', $(this).prop("checked"));
					});
				});


			});
		</script>
	</body>
</html>
