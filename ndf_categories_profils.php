<?php
////////////////// MENU ACTIF HEADER ///////////////////
////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
if (isset($_GET['menu_retour'])) {
	unset($_SESSION['retour']);
}
///////////////////// TRAITEMENTS SERVEUR ////////////////////
////////////////////FIN TRAITEMENTS SERVEUR /////////////////
///////////////////// TRAITEMENTS PHP ///////////////////////
// categories ndf
$req = $Conn->prepare("SELECT * FROM profils");
$req->execute();
$profils = $req->fetchAll();
// fin categories ndf
// categories ndf
$req = $Conn->prepare("SELECT * FROM ndf_categories");
$req->execute();
$categories = $req->fetchAll();
// fin categories ndf
////////////////////FIN TRAITEMENTS PHP//////////////////////
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Notes de frais</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
	<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
<style type="text/css">
	.panel-tabs>li>a {
		color: #AAA;
		font-size: 14px;
		letter-spacing: 0.2px;
		line-height: 30px;
		/*padding: 9px 20px 11px;*/
		border-radius: 0;
		border-left: 1px solid transparent;
		border-right: 1px solid transparent;
	}
	.nav2>li>a {
		position: relative;
		display: block;
		/* padding: 10px 15px; */
	}
</style>
<body class="sb-top sb-top-sm">
	<!-- Start: Main -->
	<div id="main">
		<?php include "includes/header_def.inc.php"; ?>
		<!-- Start: Content-Wrapper -->
		<section id="content_wrapper">
			<section id="content" class="animated fadeIn pr20 pl20">
				<h1>Liste des catégories par profil utilisateur</h1>
				<div class="row">
					<div class="col-md-12">
						<div class="tab-block mb25">
							<ul class="nav tabs-right tabs-border">
								<?php foreach ($profils as $p) {
								?>
									<li <?php if ((!empty($_GET['profil']) && $_GET['profil'] == $p['pro_id']) or (empty($_GET['profil']) && $p['pro_id'] == 1)) { ?> class="active" <?php } ?>>
										<a href="#tab<?= $p['pro_id'] ?>" data-toggle="tab"><?= $p['pro_libelle'] ?></a>
									</li>
								<?php } ?>
							</ul>
							<div class="tab-content pn" style="overflow-x: hidden">
								<?php foreach ($profils as $p) { ?>
									<div id="tab<?= $p['pro_id'] ?>" <?php if ((!empty($_GET['profil']) && $_GET['profil'] == $p['pro_id']) or (empty($_GET['profil']) && $p['pro_id'] == 1)) { ?> class="tab-pane active" <?php } else { ?> class="tab-pane" <?php } ?>>
										<div class="row">
											<div class="col-md-6">
												<h5 class="text-center">Frais classiques</h5>
												<table class="table table-striped table-hover outprint table_id">
													<thead>
														<tr class="dark">
															<th>Catégorie</th>
															<th class="no-sort">Accès</th>
															<th class="no-sort">Plafond province</th>
															<th class="no-sort">Plafond région parisienne</th>
															<th class="no-sort">Enregistrer</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($categories as $c) {
															$req = $Conn->prepare("SELECT * FROM ndf_categories_profils WHERE ncp_profil = :ncp_profil AND ncp_categorie = :ncp_categorie AND ncp_type = 1");
															$req->bindParam("ncp_categorie", $c['nca_id']);
															$req->bindParam("ncp_profil", $p['pro_id']);
															$req->execute();
															$cp = $req->fetch();
														?>
															<tr>
																<form action="ndf_categories_profils_enr.php" method="POST">
																	<input type="hidden" name="profil" value="<?= $p['pro_id'] ?>">
																	<input type="hidden" name="categorie" value="<?= $c['nca_id'] ?>">
																	<input type="hidden" name="type" value="1">
																	<td><?= $c['nca_libelle'] ?></td>
																	<td class=" text-center"> <input type="checkbox" name="ncp_acces" <?php if ($cp['ncp_acces'] == 1) { ?> checked="" <?php } ?>></td>
																	<td><input type="number" name="ncp_plafond_1" class="form-control number" value="<?= $cp['ncp_plafond_1'] ?>"></td>
																	<td><input type="number" name="ncp_plafond_2" class="form-control number" value="<?= $cp['ncp_plafond_2'] ?>"></td>
																	<td class=" text-center"><button type="submit" class="btn btn-sm btn-success"><i class="fa fa-floppy-o"></i></button></td>
																</form>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
											<div class="col-md-6">
												<h5 class="text-center">Frais formations</h5>
												<table class="table table-striped table-hover outprint table_id">
													<thead>
														<tr class="dark">
															<th>Catégorie</th>
															<th class="no-sort">Accès</th>
															<th class="no-sort">Plafond province</th>
															<th class="no-sort">Plafond région parisienne</th>
															<th class="no-sort">Enregistrer</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($categories as $c) {
															$req = $Conn->prepare("SELECT * FROM ndf_categories_profils WHERE ncp_profil = :ncp_profil AND ncp_categorie = :ncp_categorie AND ncp_type = 2");
															$req->bindParam("ncp_categorie", $c['nca_id']);
															$req->bindParam("ncp_profil", $p['pro_id']);
															$req->execute();
															$cp = $req->fetch();
														?>
															<tr>
																<form action="ndf_categories_profils_enr.php" method="POST">
																	<input type="hidden" name="profil" value="<?= $p['pro_id'] ?>">
																	<input type="hidden" name="categorie" value="<?= $c['nca_id'] ?>">
																	<input type="hidden" name="type" value="2">
																	<td><?= $c['nca_libelle'] ?></td>
																	<td class=" text-center"> <input type="checkbox" name="ncp_acces" <?php if ($cp['ncp_acces'] == 1) { ?> checked="" <?php } ?>></td>
																	<td><input type="number" name="ncp_plafond_1" class="form-control number" value="<?= $cp['ncp_plafond_1'] ?>"></td>
																	<td><input type="number" name="ncp_plafond_2" class="form-control number" value="<?= $cp['ncp_plafond_2'] ?>"></td>
																	<td class=" text-center"><button type="submit" class="btn btn-sm btn-success"><i class="fa fa-floppy-o"></i></button></td>
																</form>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</section>
		</section>
	</div>
	<!-- End: Main -->
	<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
		<div class="row">
			<div class="col-xs-4 footer-left pt7">
				<a href="parametre.php" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a>
			</div>
			<div class="col-xs-4 footer-middle pt7"></div>
			<div class="col-xs-4 footer-right pt7">
			</div>
		</div>
	</footer>
	<?php
	include "includes/footer_script.inc.php"; ?>
	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
	<!-- plugin pour les masques formulaires -->
	<script src="assets/js/custom.js"></script>
	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script src="vendor/plugins/summernote/summernote.min.js"></script>
	<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
	<script src="vendor/plugins/jquery.numberformat.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->
	<script>
		// DOCUMENT READY //a
		jQuery(document).ready(function() {
			// initilisation plugin datatables
			$('.table_id').DataTable({
				"language": {
					"url": "vendor/plugins/DataTables/media/js/French.json"
				},
				"paging": false,
				"searching": false,
				"info": false,
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});
			////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
		});
		///////////// FONCTIONS //////////////
		//////////// FIN FONCTIONS //////////
	</script>
</body>
</html>