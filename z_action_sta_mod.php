 <?php
 
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");
	
	include "modeles/mod_get_stagiaire.php";
	include "modeles/mod_get_stagiaire_sessions.php";	
	include "modeles/mod_parametre.php";
	include "modeles/mod_erreur.php";

	// MODIFICATION DES DONNES STAGIARES ET DE SON INSCRIPTION A UNE ACTION
	
	$erreur=0;
	
	$stagiaire_id=0;
	if(isset($_GET["stagiaire"])){
		if(!empty($_GET["stagiaire"])){
			$stagiaire_id=intval($_GET["stagiaire"]);
		}	
	}
	
	$action_id=0;
	if(isset($_GET["action"])){
		if(!empty($_GET["action"])){
			$action_id=intval($_GET["action"]);
		}	
	}
	if($stagiaire_id==0 OR $action_id==0){
		$erreur==1;
	}	
	if($erreur==0){
		
		$sta_naiss="";
		
		$stagiaire=get_stagiaire($stagiaire_id);
		if(!empty($stagiaire)){
			$sta_naiss_sql=date_create_from_format("Y-m-d",$stagiaire['sta_naissance']);
			if($sta_naiss_sql!==FALSE){
				$sta_naiss=$sta_naiss_sql->format("d/m/y");
			}
		}
	
	}
	
	if($erreur==0){
?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<title>SI2P - Orion</title>
				<meta name="keywords" content=""/>
				<meta name="description" content="">
				<meta name="author" content="">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<!-- CSS THEME -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
				<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >

				<!-- CSS PLUGINS -->
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
				
				<!-- CSS Si2P -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
				
				<!-- Favicon -->
				<link rel="shortcut icon" href="assets/img/favicon.png">
			   
				<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
				<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
			</head>
			<body class="sb-top sb-top-sm ">
				<form method="post" action="action_sta_mod_enr.php" id="formulaire" >
					<div>
						<input type="hidden" name="action" value="<?=$action_id?>" />
						<input type="hidden" name="stagiaire" value="<?=$stagiaire_id?>" />
					</div>
					<div id="main">
			<?php		include "includes/header_def.inc.php"; ?>
						<section id="content_wrapper">
							

							<section id="content" class="animated">
							
								<div class="row" >
							
									<div class="col-md-10 col-md-offset-1">
										
										<div class="admin-form theme-primary ">												
											
											<div class="panel heading-border panel-primary">
											
												<div class="panel-body bg-light">

													<div class="content-header mtn mbn">
												<?php 
														echo("<h2>Action N°". $action_id . " - <b class='text-primary' >Stagiaire : " . $stagiaire["sta_prenom"] . " " . $stagiaire["sta_nom"] . "</b></h2>");
												?>
													</div>	
													
													<div class="row" >
														<div class="col-md-2" >	
															<label class="field select">
																<select name="titre" id="titre"   >
													<?php			foreach($base_civilite as $k=>$v){
																	
																		if($stagiaire["sta_titre"]==$k){
																			echo("<option value='"  . $k . "' selected >" . $v . "</option>");
																		}else{
																			echo("<option value='"  . $k . "' >" . $v . "</option>");
																		}
																	} ?>
																</select>
																 <i class="arrow"></i>
															</label>
														</div>
														<div class="col-md-5" >	
															<input type="text" class="gui-input nom" name="nom" value="<?=$stagiaire["sta_nom"]?>" placeholder="NOM" />
														</div>
														<div class="col-md-5" >
															<input type="text" class="gui-input prenom" name="prenom" value="<?=$stagiaire["sta_prenom"]?>" placeholder="Prénom" />
														</div>
													</div>

													<div class="row" >
														<!-- DONNEE STAGIAIRE -->
														<div class="col-md-6" >	
															<div class="row mt15" >
																<div class="col-md-12" >
																	<div><input type="text" class="gui-input" name="ad1" value="<?=$stagiaire["sta_ad1"]?>" placeholder="Adresse" /></div>
																	<div><input type="text" class="gui-input" name="ad2" value="<?=$stagiaire["sta_ad2"]?>" placeholder="Adresse (Complément 1)" /></div>
																	<div><input type="text" class="gui-input" name="ad3" value="<?=$stagiaire["sta_ad3"]?>" placeholder="Adresse (Complément 2)" /></div>
																</div>
															</div>
															<div class="row" >
																<div class="col-md-4" >
																	<input type="text" class="gui-input code-postal" name="cp" value="<?=$stagiaire["sta_cp"]?>" placeholder="Code postal" />
																</div>
																<div class="col-md-8" >
																	<input type="text" class="gui-input nom" name="ville" value="<?=$stagiaire["sta_ville"]?>" placeholder="Ville" />
																</div>
															</div>
															
															<div class="row mt15">
																<div class="col-md-6">
																	
																	<div class="field prepend-icon">
																		<input name="tel" type="tel" class="gui-input telephone" placeholder="Numéro de téléphone" value="<?=$stagiaire['sta_tel']?>">
																		<label for="tel" class="field-icon">
																			<i class="fa fa fa-phone"></i>
																		</label>
																	</div>
																	
																</div>
																<div class="col-md-6">
																	
																	<div class="field prepend-icon">
																		<input name="portable" id="portable" type="tel" class="gui-input telephone" placeholder="Numéro de portable" value="<?=$stagiaire['sta_portable']?>">
																		<label for="portable" class="field-icon">
																			<i class="fa fa-mobile"></i>
																		</label>
																	</div>
																	
																</div>
															</div>
															<div class="row mt15">
																<div class="col-md-12">
																	
																	<div class="field prepend-icon">
																		<input type="email" name="mail_perso" id="mail_perso" class="gui-input" placeholder="Adresse email perso" value="<?=$stagiaire['sta_mail_perso']?>">
																		<label for="mail_perso" class="field-icon">
																			<i class="fa fa-envelope"></i>
																		</label>
																	</div>																	
																</div>
															</div>
															<div class="section-divider" >
																<span>Informations diplôme</span>
															</div>
															<div class="row mt15">                      
																<div class="col-md-6">
																	<label for="naissance" class="field prepend-icon">
																		<input type="text" id="naissance" name="naissance" class="gui-input" placeholder="Date de naissance" value="<?=$sta_naiss?>" />
																		<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
																	</label>
																</div>
															</div>
															<div class="row mt15" >
																<div class="col-md-4" >
																	<input type="text" class="gui-input code-postal" name="cp_naiss" value="<?=$stagiaire["sta_cp_naiss"]?>" placeholder="Code postal de naissance" />
																</div>
																<div class="col-md-8" >
																	<input type="text" class="gui-input nom" name="ville_naiss" value="<?=$stagiaire["sta_ville_naiss"]?>" placeholder="Ville de naissance" />
																</div>
															</div>
														</div>
														<!-- FIN DONNEE STAGIAIRE -->
														
														<!-- DONNEE INSCRIPTION -->
														<div class="col-md-6" >	
															<div class="section-divider" >
																<span>Informations Pro.</span>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="email" name="mail" id="mail" class="gui-input" placeholder="Adresse email pro" value="<?=$stagiaire['sta_mail']?>">
																			<label for="mail" class="field-icon">
																				<i class="fa fa-envelope"></i>
																			</label>
																		</div>
																	</div>
																</div>
															</div>
														
															<div class="section-divider mb40" >
																<span>Inscription du stagiaire</span>
															</div>
															<table class="table table-condensed" >
																<thead>
																	<tr>
																		<th>
																			<label class="option">
																				<input type="checkbox" id="session_check_all" class="check-all" />
																				<span class="checkbox"></span>
																			</label>
																		</th>
																		<td colspan="3" >&nbsp;</td>
																		<td class="text-center"  >J. Test</td>																		
																	</tr>
																</thead>
													<?php		$sessions=get_stagiaire_sessions($action_id,$stagiaire_id);
																if(!empty($sessions)){ 
																	//echo("<pre>");
																	//	print_r($sessions);
																	//echo("</pre>"); ?>
																	<tbody>
													<?php				foreach($sessions as $d){ 
																		
																			$date_sql=date_create_from_format("Y-m-d",$d["pda_date"]);
																			$date=$date_sql->format("d/m/Y");
																		
																			if(!empty($d["ase_h_deb"])){
																				$demi_lib=$d["ase_h_deb"] . " " . $d["ase_h_fin"];
																			}elseif($d["pda_demi"]==1){
																				$demi_lib="Matin";
																				
																			}else{
																				$demi_lib="Après-Midi";
																			} ?>
																			<tr>
																				<td>
																					<label class="option">
																						<input type="checkbox" class="session-check" name="session_<?=$d["ase_id"]?>" <?php if(!empty($d["ass_session"])) echo("checked"); ?> />
																						<span class="checkbox"></span>
																					</label>
																				</td>
																				<td><?=$date?></td>
																				<td><?=$demi_lib?></td>
																				<td><?=$d["int_label_1"]?></td>
																		<?php	if($d["pda_test"]){ ?>
																					<td class="text-center" ><i class="fa fa-check-square-o" ></i></td>
																		<?php	}else{ ?>
																					<td class="text-center" ><i class="fa fa-square-o" ></i></td>
																		<?php	} ?>
																			</tr>
													<?php				} ?>
																	</tbody>
													<?php		} ?>
															</table>
														
														</div>
														<!-- FIN DONNEE INSCRIPTION -->
													</div>
												</div>
												<!-- fin panel body -->
											</div>
											<!-- fin panel -->
										</div>
										<!-- fin admin form -->	
									</div>
									
								</div>
									
							</section>
							
						</section>
					</div>
					<footer id="content-footer" class="affix">
						<div class="row">
							<div class="col-xs-3 footer-left">
								<a href="action_voir.php?action=<?=$action_id?>&onglet=1" class="btn btn-default btn-sm" >
									Retour
								</a>							
							</div>
							<div class="col-xs-6 footer-middle"></div>
							<div class="col-xs-3 footer-right">
								<button type="button" class="btn btn-success btn-sm" id="formulaire_submit" >
									<i class="fa fa-save" ></i> Enregistrer
								</a>
							</div>
						</div>
					</footer>
				</form>
				
				<div id="modal_confirmation_user" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title">Confirmer ?</h4>
							</div>
							<div class="modal-body">
								<p>
									Le stagiaire n'est inscrit à aucune session.<br/>
									Sa fiche va être mise à jour mais son inscription à cette formation va être supprimée.<br/><br/>
									Êtes-vous sûr de vouloir continuer ?
								</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
									Annuler
								</button>
								<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
									Confirmer
								</button>
							</div>
						</div>
					</div>
				</div>
			
		<?php	
				include "includes/footer_script.inc.php"; ?>
				<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>	
				<script type="text/javascript" src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>		
				<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>	
		
				<script type="text/javascript" src="assets/js/custom.js"></script>			
				<script type="text/javascript" src="assets/js/orion.js"></script>								
				
				<script type="text/javascript" >
					jQuery(document).ready(function() {
						
						$(".select2").select2();
						
						$(".check-all").click(function(){
							check_all($(this));
						});

						$("#naissance").datepicker({
							prevText: '<i class="fa fa-chevron-left"></i>',
							nextText: '<i class="fa fa-chevron-right"></i>',
							showButtonPanel: false,
							beforeShow: function(input, inst) {
								var newclass = 'admin-form';
								var themeClass = $(this).parents('.admin-form').attr('class');
								var smartpikr = inst.dpDiv.parent();
								if (!smartpikr.hasClass(themeClass)) {
									inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
								}
							}
						});	
						
						$("#formulaire_submit").click(function() {
							if($(".session-check:checked").length==0){
								modal_confirmation_user("",envoyer_form,"")
							}else{
								$("#formulaire").submit();
							};						
						});
                            
						// ----- FIN DOC READY -----
					});
					(jQuery);
					
					function envoyer_form(){
						$("#formulaire").submit();
					}
					
				</script>
			</body>
		</html>
<?php
	} ?>