<?php 

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');
include('modeles/mod_facture_data.php');
include('modeles/mod_facture_pdf.php');



// VISUALISATION D'UNE CONVOCATIONS

$erreur_txt="";

$facture_id=0;
if(!empty($_GET["facture"])){
	$facture_id=intval($_GET["facture"]);
}


if(empty($facture_id)){
	$erreur_txt="impossible d'afficher la page";
}else{
	// personne connecte
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	if(!empty($_GET["societ"])){
		$acc_societe=intval($_GET["societ"]);
	}
	
	$ConnFct=connexion_fct($acc_societe);
	
	
	// DONNEE POUR AFFICHAGE
	$data=array(
		"pages" => array()
	);
	
	$data["pages"][0]=facture_data($facture_id);
	if(empty($data["pages"][0])){
		$erreur_txt="impossible d'afficher la page";
	}else{
		$pdf=model_facture_pdf($data);
		if(!$pdf){
			$erreur_txt="impossible d'afficher la page";
		}else{
			
			if(!empty($facture_id)){
			
				$data_facture=$data["pages"][0];
				
				// ON BLOQUE LA FACTURE
				
				$ConnFct->query("UPDATE Factures SET fac_blocage=1 WHERE fac_id=" . $facture_id . ";");
				
				$sql_get_ca="SELECT SUM(fli_montant_ht) as action_ca FROM Factures_Lignes INNER JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id)
				WHERE fli_action_cli_soc=:societe AND acl_action=:action;";
				$req_get_ca=$ConnFct->prepare($sql_get_ca);
				
				$sql_up_ca="UPDATE Actions SET act_ca=:action_ca WHERE act_id=:action;";
				$req_up_ca=$ConnFct->prepare($sql_up_ca);
				
				$get_action="SELECT DISTINCT acl_action FROM Factures_Lignes INNER JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id)
				WHERE fli_action_cli_soc=" . $acc_societe . " AND fli_facture=" . $facture_id . ";";
				$req_action=$ConnFct->query($get_action);
				$d_actions=$req_action->fetchAll();
				if(!empty($d_actions)){
					foreach($d_actions as $action){
						
						// pour chaque action concerné par la facture 
						
						$action_ca=0;
						
						// on récupère le ca généré par l'action 
						
						$req_get_ca->bindParam(":societe",$acc_societe);
						$req_get_ca->bindParam(":action",$action["acl_action"]);
						$req_get_ca->execute();
						$d_action_ca=$req_get_ca->fetch();
						if(!empty($d_action_ca)){
							if(!empty($d_action_ca["action_ca"])){
								$action_ca=$d_action_ca["action_ca"];	
							}	
						}
						
						// on maj le ca sur l'action
						$req_up_ca->bindParam(":action_ca",$action_ca);
						$req_up_ca->bindParam(":action",$action["acl_action"]);
						$req_up_ca->execute();
						
						
					}	
				}
				
				// MAJ ETAT PROSPECT CLIENT
				
				if(empty($data_facture["cli_first_facture"])){
					
					// jamais facture -> prospect -> on le passe en client
									
					$Conn->query("UPDATE Clients SET cli_first_facture=" . $data_facture["fac_id"] . ", cli_first_facture_date='" . $data_facture["fac_date"] . "'
					,cli_first_facture_soc=" . $acc_societe . " WHERE cli_id=" . $data_facture["fac_client"] . ";");
					
					$req_cli_soc=$Conn->query("SELECT DISTINCT cso_societe FROM Clients_Societes WHERE cso_client=" . $data_facture["fac_client"] . ";");
					$d_cli_soc=$req_cli_soc->fetchAll();
					if(!empty($d_cli_soc)){
						foreach($d_cli_soc as $cli_soc){
							
							$connFct=connexion_fct($cli_soc["cso_societe"]);
							
							$connFct->query("UPDATE Clients SET cli_facture=1 WHERE cli_id=" . $data_facture["fac_client"] . ";");
							
						}
					}
					
					// si filiale 
					
					/*
					FG 07/01/2021 
					l'état client / prospect est limité a la fiche client.
					la facturation n'impacte plus les filialies (cf note trello Orions fetaures / Clients)
					
					if(!empty($data_facture["cli_filiale_de"])){
						
						$Conn->query("UPDATE Clients SET cli_first_facture=" . $data_facture["fac_id"] . ",cli_first_facture_date='" . $data_facture["fac_date"] . "',
						cli_first_facture_soc=" . $acc_societe . " WHERE cli_id=" . $data_facture["cli_filiale_de"] . " AND (ISNULL(cli_first_facture) OR cli_first_facture=0);");
						
						$req_cli_soc=$Conn->query("SELECT DISTINCT cso_societe FROM Clients_Societes WHERE cso_client=" . $data_facture["cli_filiale_de"] . ";");
						$d_cli_soc=$req_cli_soc->fetchAll();
						if(!empty($d_cli_soc)){
							foreach($d_cli_soc as $cli_soc){
								
								$connFct=connexion_fct($cli_soc["cso_societe"]);
								
								$connFct->query("UPDATE Clients SET cli_facture=1 WHERE cli_id=" . $data_facture["cli_filiale_de"] . ";");
								
							}
						}

					}*/
				}
				
				// SI AFFACTURAGE
				
				if($data_facture["fac_affacturage"] AND !empty($data_facture["cli_first_facture"]) AND empty($data_facture["cso_aff_courrier"]) AND $acc_societe!=17){
					// facture en affacturage pour un client déjà facture (avant la facture $facture_id qui n'a pa reçu sont courrier
					// Le courrier a été généré via model_facture_pdf -> on memorise l'info
					// un rib aff par societe donc mal toutes les agences
					$Conn->query("UPDATE Clients_Societes SET cso_aff_courrier=1 WHERE cso_client=" . $data_facture["fac_client"]  . " AND cso_societe=" . $acc_societe . ";");
					
				
				}
				
			}
		}
	}
}
if(!empty($erreur_txt)){
	$_SESSION['message'][] = array(
		"aff" => "",
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur_txt
	);
}
if(!empty($facture_id)){
	if(!empty($_GET['societ'])){
		header("location : facture_voir.php?facture=" . $facture_id . "&societ=" . $_GET['societ']);
	}else{
		header("location : facture_voir.php?facture=" . $facture_id);
	}

}else{
	header("location : facture_tri.php");
}
die();
?>