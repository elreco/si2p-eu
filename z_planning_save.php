<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");

include "modeles/mod_parametre.php";
include "modeles/mod_droit.php";
include "modeles/mod_erreur.php";

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];	
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];	
}
$acc_drt_base_client=false;
if($_SESSION['acces']['acc_ref']==1){
	if(!empty(get_droit_utilisateur(6,$_SESSION['acces']['acc_ref_id']))){
		$acc_drt_base_client=true;
	}
}

/*------------------------------
 GESTION DE L'AFFICHAGE
------------------------------*/

$mois=0;
$annee=0;
if(!isset($_SESSION["pla_affichage"])){
	$_SESSION["pla_affichage"]=0;
}

// L'UTILISATEUR A FILTRE L'AFFICHAGE
if(isset($_POST["filt_aff"])){

	if(!empty($_POST["periode_mois"])){
		// l'utilisateur affiche un mois entier
		
		$tab_mois=explode("/",$_POST["periode_mois"]);
		$mois=$tab_mois[0];
		$annee=$tab_mois[1];		
		
		$jour_deb=date("N", mktime(0, 0, 0, $mois, 1, $annee));	
		$deb_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1), $annee);
		$date_deb=date_create(date("Y-m-d",$deb_mk));						

		$jour_fin=date("N", mktime(0, 0, 0, $mois+1, 0, $annee));	
		$fin_mk=mktime(0, 0, 0, $mois+1, 5-$jour_fin, $annee);
		$date_fin=date_create(date("Y-m-d",$fin_mk));
		
		$_SESSION["pla_affichage"]=0;
		$_SESSION["pla_mois"]=$mois;
		$_SESSION["pla_annee"]=$annee;
		$_SESSION["pla_periode_deb"]=$date_deb->format("d/m/Y");
		$_SESSION["pla_periode_fin"]=$date_fin->format("d/m/Y");
		
		
	}elseif(!empty($_POST["periode_deb"])){
		
		// l'utilisateur affiche une periode perso

		$c_date_deb=convert_date_sql($_POST["periode_deb"]);
		$date_deb = new DateTime($c_date_deb);

		$c_date_fin=convert_date_sql($_POST["periode_fin"]);
		$date_fin = new DateTime($c_date_fin);
		
		$_SESSION["pla_affichage"]=1;
		$_SESSION["pla_mois"]=0;
		$_SESSION["pla_annee"]=0;
		$_SESSION["pla_periode_deb"]=$_POST["periode_deb"];
		$_SESSION["pla_periode_fin"]=$_POST["periode_fin"];
		
		/*echo("POST periode_deb : <br/>");
		echo($_POST["periode_deb"]);
		echo("<br/>");
		echo("POST periode_fin : <br/>");
		echo($_POST["periode_fin"]);
		echo("<br/>");*/
		//die();
		
	}

}else{

	if($_SESSION["pla_affichage"]==1){
		
		// periode perso
		
		if(!empty($_GET["periode_deb"])){
			
			//echo("Source : get<br/>");
			
			$c_date_deb=convert_date_sql($_GET["periode_deb"]);
			$_SESSION["pla_periode_deb"]=$_GET["periode_deb"];
			
			$c_date_fin=convert_date_sql($_GET["periode_fin"]);
			$_SESSION["pla_periode_fin"]=$_GET["periode_fin"];
			
		}elseif(!empty($_SESSION["pla_periode_deb"])){
			
		/*	echo("Source : session<br/>");
			echo("pla_periode_deb : ");
				echo($_SESSION["pla_periode_deb"]);
			echo("<br/>");
			echo("pla_periode_fin : ");
				echo($_SESSION["pla_periode_fin"]);
			echo("<br/>");*/
			//die();
			
			$c_date_deb=convert_date_sql($_SESSION["pla_periode_deb"]);
			$c_date_fin=convert_date_sql($_SESSION["pla_periode_fin"]);
		
		}else{
			
			//echo("Source : defaut<br/>");

			$jour_deb=date("N");	
			$deb_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1), $annee);
			$c_date_deb=date("d/m/Y",$deb_mk);
			$_SESSION["pla_periode_deb"]=$c_date_deb;
			
			$fin_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1)+6, $annee);
			$c_date_fin=date("d/m/Y",$fin_mk);
			$_SESSION["pla_periode_fin"]=$c_date_fin;
			
		}
		$date_deb = new DateTime($c_date_deb);
		$date_fin = new DateTime($c_date_fin);	
		
	}else{
		
		if(isset($_GET["mois"]) AND isset($_GET["annee"])){
			$mois=$_GET["mois"];
			$annee=$_GET["annee"];
		}else{
			$mois=date("n");
			$annee=date("Y");
		}
		$jour_deb=date("N", mktime(0, 0, 0, $mois, 1, $annee));	
		$deb_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1), $annee);
		$date_deb=date_create(date("Y-m-d",$deb_mk));
		
		$jour_fin=date("N", mktime(0, 0, 0, $mois+1, 0, $annee));	
		$fin_mk=mktime(0, 0, 0, $mois+1, 5-$jour_fin, $annee);
		$date_fin=date_create(date("Y-m-d",$fin_mk));
		$_SESSION["pla_periode_deb"]=$date_deb->format("d/m/Y");
		$_SESSION["pla_periode_fin"]=$date_fin->format("d/m/Y");
	}
}
// GESTION DE LA PAGINATION
if($_SESSION["pla_affichage"]==1){

	$interval_date = $date_deb->diff($date_fin);
	
	$prec_fin = new DateTime($date_deb->format("Y-m-d"));
	$prec_fin->sub(new DateInterval('P2D'));
	
	$prec_deb=new DateTime($prec_fin->format("Y-m-d"));
	$prec_deb->sub(new DateInterval('P'. $interval_date->format('%a') .'D'));
	
	$suiv_deb=new DateTime($date_fin->format("Y-m-d"));
	$suiv_deb->add(new DateInterval('P2D'));
	
	$suiv_fin=new DateTime($suiv_deb->format("Y-m-d"));
	$suiv_fin->add(new DateInterval('P'. $interval_date->format('%a') .'D'));

}else{
	$time_mois_suiv=mktime(0, 0, 0, $mois+1, 1, $annee);		
	$mois_suiv=date("n",$time_mois_suiv);
	$annee_suiv=date("Y",$time_mois_suiv);
	
	$time_mois_prec=mktime(0, 0, 0, $mois-1, 1, $annee);	
	$mois_prec=date("n",$time_mois_prec);
	$annee_prec=date("Y",$time_mois_prec);
}

	/*------------------------------
		DONNEE NECESSAIRE 
	------------------------------*/

		//SOCIETES / AGENCES

	$sql="SELECT soc_agence FROM Societes WHERE soc_id=:acc_societe;";
	$req = $Conn->prepare($sql);
	$req->bindParam(":acc_societe",$acc_societe);
	$req->execute();
	$societe = $req->fetch();
	
		// LES COMMERCIAUX
		
	$sql="SELECT com_id,com_label_1,com_label_2 FROM Commerciaux";
	if($acc_agence>0){
		$sql.=" WHERE com_agence=:acc_agence";
	}
	$sql.=" ORDER BY com_label_1,com_label_2";
	$req = $ConnSoc->prepare($sql);
	if($acc_agence>0){
			$req->bindParam(":acc_agence",$acc_agence);
	}
	$req->execute();
	$commerciaux = $req->fetchAll();
	
		// LES VEHICULES
		
	$sql="SELECT veh_id,veh_libelle FROM Vehicules WHERE veh_societe=:acc_societe";
	if($acc_agence>0){
		$sql.=" AND veh_agence=:acc_agence";
	}
	$sql.=" ORDER BY veh_libelle,veh_libelle";
	$req = $Conn->prepare($sql);
	$req->bindParam(":acc_societe",$acc_societe);
	if($acc_agence>0){
			$req->bindParam(":acc_agence",$acc_agence);
	}
	$req->execute();
	$vehicules = $req->fetchAll();
	
	
/*--------------------------------------
		FONCTION 
----------------------------------------*/
// RETOURNE L'ENTETE D'UNE Semaine

Function w_planning_head($semaine,$date_deb){
	
	$f_date=new DateTime($date_deb->format('Y-m-d'));
	
	echo("<div class='row mt15 ml15 mr15 bg-dark'  >");						
		echo("<div class='col-sm-1' >");
			echo("Semaine " . $semaine);
		echo("</div>");
		echo("<div class='col-sm-11' >");						
			echo("<div class='row' >");								
				$f_date->sub(new DateInterval('P1D'));
				for($j=0;$j<=5;$j++){
					$f_date->add(new DateInterval('P1D')); 
					$f_date->setTimezone(new DateTimeZone('Europe/Paris'));
					echo("<div class='col-sm-2 text-center' >");
						$date_f_fr=$f_date->getTimestamp();
						echo(strftime("%A %d",$date_f_fr));
					echo("</div>");
				}									
			echo("</div>");
		
			echo("<div class='row' >");	
			for ($m = 0; $m <= 5; $m++){ 
					echo("<div class='col-sm-1 text-center' >");
						echo("Matin");
					echo("</div>");
					echo("<div class='col-sm-1 text-center' >");
						echo("Après-Midi");
					echo("</div>");
			}; 								
			echo("</div>");
		echo("</div>");
	echo("</div>");
}

function w_planning_ligne($int_id,$int_label,$date_deb,$planning){

	$f_date=new DateTime($date_deb->format('Y-m-d'));
							
	echo("<div class='row ml15 mr15' >");					
		echo("<div class='col-sm-1 planning_intervenant' >");	
			echo($int_label);
		echo("</div>");
		echo("<div class='col-sm-11' >");
			echo("<div class='row' >");
		
				for($j=0;$j<=5;$j++){
					
					$f_date->add(new DateInterval('P1D'));
					$date_lib=$f_date->format('Y-m-d');
					
					
					for($d=1;$d<=2;$d++){
						
						$case_id=$int_id . "_" . $date_lib . "_" . $d;
						
						if(!empty($planning[$date_lib][$d])){
							$case=$planning[$date_lib][$d];
							$class="";
							$class_case="";
							switch ($case["type"]) {
								case 0:
									$class="";
									break;
								case 1:
									$class=" action action_" . $bcl_semaine . "_" . $int_id . "_" . $case["ref"];
									break;
								case 3:
									$class_case=" case_info click";
									break;
							} 
							echo("<div class='col-sm-1 planning_case" . $class_case . "' id='case_" . $case_id . "' >");							
								echo("<div class='drag" . $class . "' style='" . $case["style"] . "' id='contenu_" . $case_id . "' >");
									echo($case["texte"]);
								echo("</div>");
								
							echo("</div>");
						}else{ 
							echo("<div class='col-sm-1 planning_case libre click' id='case_" . $case_id . "' >&nbsp;</div>");										
						}
					}
				}
			echo("</div>");
		echo("</div>");
	echo("</div>");
};
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" >
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

	</head>

	<body class="sb-top sb-top-sm" >
		<!-- Start: Main -->
		<div id="main">
			<?php
				include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<header id="topbar"  >
					<div class="topbar-left">
						<ol class="breadcrumb">							
							<li>
								Planning
							</li>
							<li class="crumb-trail">
					<?php		if($_SESSION["pla_affichage"]==1){ 
									echo("Période du " . strftime("%d %b %Y",$date_deb->getTimestamp()) . " au " . strftime("%d %b %Y",$date_fin->getTimestamp()));
								}else{
									echo(strftime("%B %Y",mktime(0, 0, 0, $mois, 1, $annee)));
								} ?>
							</li>
						</ol>
					</div>
				</header>
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn">

						
				<?php
				
					$planning=array();				
					
					$bcl_date_deb = new DateTime($date_deb->format('Y-m-d'));
					
					while ($bcl_date_deb<$date_fin){
								
						
						$bcl_date_fin = new DateTime($bcl_date_deb->format('Y-m-d'));
						$bcl_date_fin->add(new DateInterval('P5D'));
			
						$semaine=$bcl_date_deb->format("W");
						$annee=$bcl_date_deb->format("Y");

						w_planning_head($semaine,$bcl_date_deb);
						
						$sql="SELECT * FROM ((";
						
						$sql.="Intervenants LEFT JOIN plannings_intervenants ON (Intervenants.int_id=plannings_intervenants.pin_intervenant)";
						$sql.=")";
						
						$sql.=" LEFT OUTER JOIN plannings_dates ON (Intervenants.int_id=plannings_dates.pda_intervenant";
							$sql.=" AND plannings_dates.pda_date>='" . $bcl_date_deb->format('Y-m-d') . "' AND plannings_dates.pda_date<='" . $bcl_date_fin->format('Y-m-d') . "'";
						$sql.="))";
						
						$sql.=" WHERE pin_semaine=" . $semaine . " AND pin_annee=" . $annee;					
						if($acc_agence>0){
							$sql.=" AND int_agence=" . $acc_agence;
						}
						$sql.=" ORDER BY pin_place,int_id,pda_date,pda_demi";
						$req = $ConnSoc->query($sql);
						$result = $req->fetchAll();

						if(!empty($result)){ 
							$int_id=0;		
							foreach($result as $r)
							{	
								if($int_id!=$r["int_id"]){
									
									if($int_id>0){									
										w_planning_ligne($int_id,$int_label,$bcl_date_deb,$planning);
									}
									$int_id=$r["int_id"];
									$int_label=$r["int_label_1"] . " ". $r["int_label_2"];	
									$planning=array();
									
								}							
								if(!empty($r["pda_date"])){
									$planning[$r["pda_date"]][$r["pda_demi"]]=array(
										"type" => $r["pda_type"],
										"ref" => $r["pda_ref_1"],
										"texte" => $r["pda_texte"],
										"style" => $r["pda_style"]
										
									);
								}
							}
							w_planning_ligne($int_id,$int_label,$bcl_date_deb,$planning);
						};				
						$bcl_date_deb->add(new DateInterval('P7D'));											
					}
					// FIN WHILE ?>
					
					
				</section>
				
				<!-- End: Content -->
			</section>
		</div>
		
		
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
			<?php	if($_SESSION["pla_affichage"]==1){ ?>
						<a href="planning.php?periode_deb=<?=$prec_deb->format("d/m/Y")?>&periode_fin=<?=$prec_fin->format("d/m/Y")?>" class="btn btn-default btn-sm" >
							<i class="fa fa-arrow-left"></i>					
							Période précédente
						</a>
			<?php	}else{ ?>
						<a href="planning.php?mois=<?=$mois_prec?>&annee=<?=$annee_prec?>" class="btn btn-default btn-sm" >
							<i class="fa fa-arrow-left"></i>					
							<?=strftime("%B %Y",$time_mois_prec)?>
						</a>	
			<?php	} ?>
					
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-affiche" >
						<i class="fa fa-calendar" aria-hidden="true"></i>
					</button>
			<?php	if($_SESSION["pla_affichage"]==1){ ?>
						<a href="planning.php?periode_deb=<?=$suiv_deb->format("d/m/Y")?>&periode_fin=<?=$suiv_fin->format("d/m/Y")?>" class="btn btn-default btn-sm" >											
							Période suivante
							<i class="fa fa-arrow-right"></i>	
						</a>
			<?php	}else{ ?>
						<a href="planning.php?mois=<?=$mois_suiv?>&annee=<?=$annee_suiv?>" class="btn btn-default btn-sm" >											
							<?=strftime("%B %Y",$time_mois_suiv)?>
							<i class="fa fa-arrow-right"></i>	
						</a>
			<?php	} ?>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="intervenant_cree.php" class="btn btn-success btn-sm" role="button" title="Ajouter une ligne de planning" data-placement="top" data-toggle="tooltip" >
						<i class="fa fa-user" ></i>
					</a>
			<?php	if($acc_agence==0 AND $societe["soc_agence"]!=1){ ?>
						<span title="Connectez-vous sur une agence pour organiser le planning" data-placement="top" data-toggle="tooltip" >
							<button class="btn btn-warning btn-sm disabled" >
								<i class="fa fa-user" ></i>
							</button>	
						</span>
			<?php	}else{ ?>
						<a href="planning_intervenant.php" class="btn btn-warning btn-sm" role="button" title="Organiser les lignes" data-placement="top" data-toggle="tooltip" >
							<i class="fa fa-user" ></i>
						</a>	
			<?php	} ?>
									
				</div>
			</div>
		</footer>
		
		
		<div id="context-add" class="context-menu" >		
			<ul class="dropdown-menu" >				
				<li>
					<a href="#" id="aff_modal-action" >Action</a>							
				</li>
				<li>
					<a tabindex="-1" href="#">Congés</a>					
				</li>
				<li class="dropdown-submenu">
					<a tabindex="-1" href="#">Autres</a>
					<ul class="dropdown-menu " >
				<?php
					$req=$Conn -> query("SELECT pca_id,pca_libelle,pca_couleur FROM Plannings_Cases WHERE pca_type=0 ORDER BY pca_libelle;");
					$case = $req->fetchAll();
					if(!empty($case)){
						foreach($case as $c){ 
							echo("<li>");
								echo("<a href='#' class='add-case' data-case='" . $c["pca_id"] . "' >");
									echo ("<span class='badge' style='background-color:" . $c["pca_couleur"] . "' >&nbsp;</span>");
									echo ($c["pca_libelle"]);									
								echo("</a>");
							echo("</li>");							
						}
					} ?>						
					</ul>
				</li>
			</ul>
		</div>
		<div id="context-info" class="context-menu" >		
			<ul class="dropdown-menu" >			
			<?php
				if(!empty($case)){
					foreach($case as $c){ 
						echo("<li>");
							echo("<a href='#' class='add-case' data-case='" . $c["pca_id"] . "' >");
								echo ("<span class='badge' style='background-color:" . $c["pca_couleur"] . "' >&nbsp;</span>");
								echo ($c["pca_libelle"]);									
							echo("</a>");
						echo("</li>");							
					}
				} ?>
				<li>
					<a href='#' class='supp-info' >
						<i class="fa fa-times" ></i>
						Supprimer					
					</a>
				</li>	
			</ul>
		</div>
		
		<div id="modal-confirme" class="modal fade" role="dialog" >
			<div class="modal-dialog">			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
						<p id="modal-text" ></p>
						<p>Etes-vous sur de vouloir continuer ?</p>
					</div>
					<div class="modal-footer">
						<button type="button" id="confirme_oui" class="btn btn-success btn-sm" data-dismiss="modal" data-confirmation=""  >Oui</button>
						<button type="button" id="confirme_non" class="btn btn-default btn-sm" data-dismiss="modal">Non</button>
					</div>
				</div>
			</div>
		</div>
		
		<div id="modal-action" class="modal fade" role="dialog" >
			<div class="modal-dialog modal-lg">			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Nouvelle action</h4>
					</div>
					
					<form method="post" action="action_cree.php" >
						<div class="modal-body admin-form">
							
							<div class="row" >
								<div class="col-md-12" >	
									<div class="section" >
										<select name="client" id="client" >
											<option value="0" selected >Client</option>					
										</select>	
									</div>
								</div>
							</div>		
							<div class="row" >
								<!-- colonne 1 -->
								<div class="col-md-6" >	
									<div class="section-divider mb40" >
										<span>Lieu d'intervention</span>
									</div>
									<div class="row" >
										<div class="col-md-4" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr_ref" name="adr_ref" id="adr_client" value="1" checked >
													<span class="radio"></span>Client
												</label>
											</div>
										</div>
										<div class="col-md-4" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr_ref" name="adr_ref" id="adr_si2p" value="2" >
													<span class="radio"></span>Agence 
												</label>
											</div>
										</div>
										<div class="col-md-4" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr_ref" name="adr_ref" id="adr_autre" value="3" >
													<span class="radio"></span>Autres 
												</label>
											</div>
										</div>
									</div>	
									<div class="row" id="cont_adr_client"  >
										<div class="col-md-12" >	
											<div class="section" >
												<select name="adr_ref_id" id="adr_ref_id" class="select2" >
													<option value="0" >Selectionnez une adresse</option>										
												</select>
											</div>
										</div>
									</div>
									<div id="cont_adr" >
										<div class="row">
											<div class="col-md-12" >	
												<input type="text" name="act_adr_nom" id="adr_nom" class="gui-input nom champ_adresse" placeholder="Nom" />	
												<input type="text" name="act_adr_service" id="adr_service" class="gui-input champ_adresse" placeholder="Service" />		
												<input type="text" name="act_adr1" id="adr1" class="gui-input champ_adresse" placeholder="Adresse" />		
												<input type="text" name="act_adr2" id="adr2" class="gui-input champ_adresse" placeholder="Complément 1" />		
												<input type="text" name="act_adr3" id="adr3" class="gui-input champ_adresse" placeholder="Complément 2" />		
											</div>
										</div>
										<div class="row">
											<div class="col-md-3" >	
												<input type="text" name="act_adr_cp" id="adr_cp" class="gui-input code-postal champ_adresse" placeholder="CP" />		
											</div>
											<div class="col-md-9" >	
												<input type="text" name="act_adr_ville" id="adr_ville" class="gui-input nom champ_adresse" placeholder="Ville" />		
											</div>
										</div>
									</div>									
								</div>
								<!-- fin colonne 1 -->
								
								<!-- colonne 2 -->
								<div class="col-md-6" >	
									<div class="section-divider mb40" >
										<span>Formation</span>
									</div>								
									<div class="row" >
										
										<div class="col-md-6" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="produit_type" name="produit_type" id="produit_intra" checked value="1" >
													<span class="radio"></span>Produit intra
												</label>
											</div>
										</div>
										
										<div class="col-md-6" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="produit_type" name="produit_type" id="produit_inter" value="2" >
													<span class="radio"></span>Produit inter
												</label>
											</div>
										</div>
										
									</div>
									<div class="row" >
										<div class="col-md-12" >	
											<div class="section" >
												<select name="produit" id="produit" class="select2" >
													<option value="0" >Selectionnez un produit ...</option>					
												</select>
											</div>
										</div>
									</div>
									<div class="row" >
										<div class="col-md-10" >Nombre de session / demi-journée :</div>
										<div class="col-md-2" >	
											<input type="text" name="nb_session" id="nb_session" class="gui-input" type="number" value="1"  min="1" step="1" max="4" />		
										</div>
									</div>
									
									<div class="row" >
										<div class="col-md-6">
											<h4 class="text-left">Matin</h4>
										</div>
										<div class="col-md-6">
											<h4 class="text-left">Après-Midi</h4>
										</div>
									</div>
									<div class="row" >
										<div class="col-md-3">
											<div class="section">
												<span class="prepend-icon">
													<input type="text" id="timepicker1" name="h_deb_matin" class="gui-input" placeholder="Heure de début" value="">
													<span class="field-icon">
														<i class="fa fa-clock-o"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="col-md-3">
											<div class="section">
												<span class="prepend-icon">
													<input type="text" id="timepicker2" name="h_fin_matin" class="gui-input" placeholder="Heure de fin" value="">
													<span class="field-icon">
														<i class="fa fa-clock-o"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="col-md-3">
											<div class="section">
												<span class="prepend-icon">
													<input type="text" id="timepicker1" name="h_deb_ap" class="gui-input" placeholder="Heure de début" value="">
													<span class="field-icon">
														<i class="fa fa-clock-o"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="col-md-3">
											<div class="section">
												<span class="prepend-icon">
													<input type="text" id="timepicker2" name="h_fin_ap" class="gui-input" placeholder="Heure de fin" value="">
													<span class="field-icon">
														<i class="fa fa-clock-o"></i>
													</span>
												</span>
											</div>
										</div>
									</div>
								
									<div class="row" >
										<div class="col-md-12" >	
											<div class="section" >
												<select name="vehicule" id="vehicule" class="select2" >
													<option value="0" >Selectionnez un vehicule ...</option>
									<?php			if(!empty($vehicules)){
														foreach($vehicules as $v){
															echo("<option value='" . $v["veh_id"] . "' >" . $v["veh_libelle"] . "</option>");
														}
													} ?>		
												</select>
											</div>
										</div>
									</div>
									<div class="section-divider mb40" >
										<span>Commerce</span>
									</div>
									<div class="row" >
										<div class="col-md-12" >	
											<div class="section" >
												<select name="commercial" id="commercial" class="select2" >
													<option value="0" >Selectionnez un commercial ...</option>	
									<?php			if(!empty($commerciaux)){
														foreach($commerciaux as $c){
															echo("<option value='" . $c["com_id"] . "' >" . $c["com_label_1"] . " " . $c["com_label_1"] . "</option>");
														}
													} ?>
												</select>
											</div>
										</div>
									</div>
									<div class="row" >
										<div class="col-md-12" >	
											<div class="section" id="cont_ca_intra" >
												<label>CA Formation</label>
												<div class="field prepend-icon">
													<input id="ca_intra" class="gui-input numerique" type="number" value="0" name="ca_intra" min="0.00" step="0.01">
													<label class="field-icon" for="ca_intra">
														 <i class="fa fa-euro" ></i>
													</label>
												</div>
											</div>
											<div class="section" id="cont_ca_inter" style="display:none;"  >
												<label>CA / stagiaire</label>
												<div class="field prepend-icon">
													<input id="ca_inter" class="gui-input numerique" type="number" value="0" name="ca_inter" min="0.00" step="0.01">
													<label class="field-icon" for="ca_inter">
														 <i class="fa fa-euro" ></i>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- fin colonne 2 -->
							</div>
						</div>					
						<div class="modal-footer">
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>	
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<!-- GESTION DE LA PERIODE A AFFICHER -->
		<div id="modal-affiche" class="modal fade" role="dialog" >
			<div class="modal-dialog">			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Affichage</h4>
					</div>
					<form method="post" action="planning.php" >
						<div>
							<input type="hidden" name="filt_aff" value="filt" />
						</div>
						<div class="modal-body admin-form">					
							<div class="row">
								<div class="col-md-12">
									<div class="section-divider mb40">
										<span>Affichage par mois</span>
									</div>
								</div>
							</div>
							<div class="row" >
								<div class="col-md-6">									
									<div class="field prepend-icon">
							<?php		if($_SESSION["pla_affichage"]==1){ ?>
											<input type="text" id="periode_mois" name="periode_mois" class="gui-input champ_date" placeholder="Selectionner un mois" />
							<?php		}else{ ?>
											<input type="text" id="periode_mois" name="periode_mois" class="gui-input champ_date" placeholder="Selectionner un mois" value="<?=$mois."/".$annee?>" />
							<?php		} ?>
										<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
									</div>									
								</div>							
							</div>
						
							
							<div class="row">
								<div class="col-md-12">
									<div class="section-divider mb40">
										<span>Affichage personalisé</span>
									</div>
								</div>
							</div>
							<div class="row" >
								<div class="col-md-6">
									Du
									<div class="field prepend-icon">
							<?php		if($_SESSION["pla_affichage"]==1){ ?>
											<input type="text" id="periode_deb" name="periode_deb" class="gui-input" placeholder="Selectionner une date" value="<?=$date_deb->format("d/m/Y")?>" />
							<?php		}else{ ?>
											<input type="text" id="periode_deb" name="periode_deb" class="gui-input" placeholder="Selectionner une date" />
							<?php		} ?>
										<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
									</div>									
								</div>
								<div class="col-md-6">
									Au
									<div class="field prepend-icon">
										
							<?php		if($_SESSION["pla_affichage"]==1){ ?>
											<input type="text" id="periode_fin" name="periode_fin" class="gui-input" placeholder="Selectionner une date" value="<?=$date_fin->format("d/m/Y")?>" />
							<?php		}else{ ?>
											<input type="text" id="periode_fin" name="periode_fin" class="gui-input" placeholder="Selectionner une date" />
							<?php		} ?>
										<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
									</div>
								</div>						
							</div>					
						</div>					
						<div class="modal-footer">
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-view" ></i>
								Afficher
							</button>
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>	
						</div>
					</form>
				</div>
			</div>
		</div>
		
		
		
		

		
		<?php
		include "includes/footer_script.inc.php"; ?>	
		<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery-ui-monthpicker.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>
		
		<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
		<script type="text/javascript" src="/assets/js/custom.js"></script>
		
		
		
		<script type="text/javascript" >
		
			var elt_source;
			var nb_demi_j_select=0; // nb de demi jour selectionné au moment ou on affiche le modal action
		
			$(document).ready( function() {
				
				$(".select2").select2();

				// PERIODE D'AFFICHAGE PERSO	
		
				$('#periode_mois').monthpicker();
				$('#periode_mois').change(function(){
					$("#periode_deb").val("");
					$("#periode_fin").val("");
				});									
				$("#periode_deb").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=1) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},
					onSelect: function (input, inst){
						$("#periode_mois").val("");
						var date_fin=$('#periode_deb').datepicker( "getDate" );
						date_fin.setDate(date_fin.getDate() + 5);
						var max_fin=new Date();
						max_fin.setDate(date_fin.getDate() + 84);
						$("#periode_fin").datepicker( "setDate",date_fin);
						$("#periode_fin").datepicker( "option", "minDate", date_fin);
						$("#periode_fin").datepicker( "option", "maxDate", max_fin);
					}
				});
				$("#periode_fin").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}		
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=6) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},
					
				});			
				
				// MENU CONTEXTUEL				
				
				// gestion du clique droit et affichage du menu contextuel associé		
				$(document).on("contextmenu",".planning_case",function(e){
					
					if(e.pageX/$(document).width()>0.75){
						$(".dropdown-submenu").addClass( "pull-left");
					}else{
						$(".dropdown-submenu").removeClass( "pull-left");
					}
					
					if($(this).hasClass("libre")&&$(this).hasClass("case_select")){
						
						$("#context-add").css({top:e.pageY,left:e.pageX}).show();
						
					}else if($(this).hasClass( "case_info" )&&$(this).hasClass("case_select")){
						
						$("#context-info").css({top:e.pageY,left:e.pageX}).show();
					}
					// desactive le comportement par défaut du clique droit
					e.preventDefault();
					return false;
				});
				
				// masque les menucontextuels
				$(document).on("contextmenu click","*:not(.planning_case)",function(e){
					$(".context-menu").hide();
					//e.preventDefault();
					//return false;
				});
				
				
				// ini des fonctions de traitement liées au menu contextuel		
				$(".add-case").click(function(){
					ajouter_case($(this));
					$(".context-menu").hide();	
				});				
				$(".supp-info").click(function(){
					
					$("#modal-confirme #modal-text").text("Vous êtes sur le point d'éffacer le contenu des cases sélectionnées");
					$('#modal-confirme').modal('show');
					
					//$("#confirme_oui").off() si plusieur utilisation du modal confirme			
					$("#confirme_oui").click(function(){
						supprimer_info();
					});
					
					$(".context-menu").hide();	
				});
				// FIN MENU CONTEXTUEL
				
				/* --------------------------------------
					INI LIE AUX ACTIONS
				-----------------------------------------*/	
				
				$("#aff_modal-action").click(function(){
					$(".context-menu").hide();
					$('#modal-action').modal('show');
					nb_demi_j_select=$(".case_select").length;					
				});				
				$('#client').select2({
					minimumInputLength: 1,
					ajax: {
						url: "ajax/ajax_client_select.php",
						dataType: 'json',
						delay: 250,
						method: 'GET',
						data: function (params) {
							return {
								q: params.term, // les termes de la requête
							};
						},
						processResults: function (data) {
							// data correspond au tableau json des résultat
							// data.nom permet d'accéder à l'information $retour["nom"]
							
							return {
								results: data,
							};
						},
					},
					templateResult: function(data) {
						// permet de chosir quelles informations seront affichées dans la liste de résultat
						// parmit les éléments de data
						//if (data.loading) return data.text;
						return data.nom;
					},
					templateSelection: function(data) {
						// permet de chosir quelles informations seront affichées dans le select
						// parmit les éléments de data
						// une fois que l'utilisateur aura fait un choix.
						
						//if (data.loading) return data.text;
						// Sinon affichage du placeholder (data.text).
						//if (data.text) return data.text;

						return data.nom;
					},
				});
				$("#client").change(function(){
					actualiser_client($(this).val());
				});
				$(".adr_ref").change(function(){
					afficher_type_adresse();
				});
				$("#adr_ref_id").change(function(){
					afficher_adresse($(this).val());
				});
				$(".produit_type").click(function(){
					if($('input[name=produit_type]:checked').val()==1){
						$("#cont_ca_inter").hide();
						$("#cont_ca_intra").show();
					}else{
						$("#cont_ca_inter").show();
						$("#cont_ca_intra").hide();
					}
					actualiser_produit_client();										
				});
				$("#produit").change(function(){
					actualiser_produit();
				});				
				//--------- FIN INI LIE AUX ACTIONS------------------
			
				// initialisation des event en fonction des classes affectées au case
				case_ini();
				
			});
			//--------- FIN DE DOCUMENT READY ------------------
			
			
			// OPERATION SUR LES CASES DU PLANNING	
			
			// vide une case pour la rendre disponible		
			function case_vide(case_id){
				$("#contenu_" + case_id).remove();
				$("#case_" + case_id).html("&nbsp;");
				// la case devient cliquable
				$("#case_" + case_id).addClass("libre");
				$("#case_" + case_id).addClass("click");			
				
			}
			
			// insertion de donnee dans une case du planning
			function case_donnee(case_id,case_contenu,case_type){
				console.log("case_type : " + case_type);
				$("#case_" + case_id).html(case_contenu);
				$("#case_" + case_id).removeClass("case_select");
				$("#case_" + case_id).removeClass("libre");
				if(case_type==3){
					$("#case_" + case_id).addClass("case_info");
					$("#case_" + case_id).addClass("click");
				}			
				$("#case_" + case_id).off(); // annule les events affecte
			}		
			
			// reinitialise les event lié au case en fonction des classes (evite de recharger)
			function case_ini(){	
			
				$(".click").off();	
				$(".click").click(function(){
					selectionner_case($(this));
				});		
				$(".libre").droppable({
					drop : function(event,ui){							
						var id_source=elt_source.replace("contenu_","");
						var id_cible=(this).id.replace("case_","");
						deplacer_case(id_source,id_cible);
					}
				});	
				$('.drag').draggable({
					snap : ".drag",
					revert : 'invalid', // renvoi les bloc s'il ne sont pas depose en zone drop
					cursor : "move",
					zIndex: 9999,
					start : function(){
						elt_source=(this).id;		
					}
				});
			}

			// select ou deselection d'une case
			function selectionner_case(elt){	
			
				console.log("selectionner_case()");
			
				if(elt.hasClass( "case_select" )){
					// annule select
					elt.removeClass("case_select");	
					if($(".case_select").length==0){
						case_ini();
					}							
				}else{
					// select
					if($(".case_select").length==0){
						if(elt.hasClass( "case_info" )){
							$(".libre").off(); // .libre ne sont plus select	
						}else{
							$(".case_info").off(); // .info ne sont plus select	
						}
					}
					elt.addClass("case_select");	
				}
			}		
			
			// ajouter une ou plusieur case
			function ajouter_case(e){			
				var type_case=e.data("case");
				var liste_case="";
				var ident_case="";
				$(".case_select").each(function(){ 
					ident_case=(this).id.replace("case_", "");
					liste_case=liste_case +  ident_case + ",";
				});			
				$.ajax({			
					type:'GET',
					url: 'ajax/ajax_add_case.php',
					data : 'type_case=' + type_case + "&liste_case=" + liste_case, 
					dataType: 'json',                
					success: function(data)
					{	if(data.length<3){
							console.log("ERREUR DATA");
						}else{
							var case_texte=data[0];		
							var case_couleur=data[1];	
							var case_html="";
							var case_id="";
							var i=0;
							for(i==3;i<=data.length;i++){
								case_id=data[i];
								if($("#case_" + case_id).hasClass("case_select")){
									
									case_contenu="<div class='drag' style='background-color:" + case_couleur + ";' id='contenu_" + case_id + "' >";
									case_contenu+=case_texte;
									case_contenu+="</div>";	
									
									case_donnee(case_id,case_contenu,3);															
								}						
							}
							case_ini();		
						}
						
					},
					error: function() {
						console.log("ERREUR AJAX");		
					}
				});
			};			
			
			// déplace une case
			function deplacer_case(id_source,id_cible){		
				var case_texte=$("#contenu_" + id_source).text();
				var case_bg=$("#contenu_" + id_source).css("background-color");
				
				var case_type=0;
				if($("#case_" + id_source).hasClass( "case_info" )){
					case_type=3;					
				}
				
				$.ajax({			
					type:'GET',
					url: 'ajax/ajax_move_case.php',
					data : 'id_source=' + id_source + "&id_cible=" + id_cible, 
					dataType: 'html',                
					success: function()
					{	
						case_contenu="<div class='drag' style='background-color:" + case_bg + ";' id='contenu_" + id_cible + "' >";
						case_contenu+=case_texte;
						case_contenu+="</div>";	
						
						case_vide(id_source);	
						case_donnee(id_cible,case_contenu,case_type);
						case_ini();
											
					},
					error: function() {
						console.log("ERREUR");		
					}
				});
			}
			
			function supprimer_info(){
				console.log("supprimer_info()");
				var liste_case="";
				var ident_case="";
				$(".case_select").each(function(){ 
					ident_case=(this).id.replace("case_", "");
					liste_case=liste_case +  ident_case + ",";
				});			
				$.ajax({			
					type:'GET',
					url: 'ajax/ajax_supp_case.php',
					data : "&liste_case=" + liste_case, 
					dataType: 'html',                
					success: function()
					{	$(".case_select").each(function(){ 
							ident_case=(this).id.replace("case_", "");
							$(this).removeClass("case_select");
							$(this).removeClass("case_info");	
							case_vide(ident_case);	
												
						});
						case_ini();					
					},
					error: function() {
						console.log("ERREUR AJAX");		
					}
				});			
			}
			
			// l'utilisateur selectionne un client
			function actualiser_client(client){
				
				$.ajax({			
					type:'GET',
					url: 'ajax/ajax_client_get.php',
					data : 'client=' + client, 
					dataType: 'json',                
					success: function(data){
						$("#commercial").select2("val",data.cli_commercial);						
						actualiser_adresse_client(data.adresses);
						if(data.adresses){
							if($('input[name=adr_ref]:checked').val()==1){
								// intervention chez le client
								$("#adr_ref_id").select2("val",data.adresses[0].id);	
								afficher_adresse(data.adresses[0].id);
							}
						}
						actualiser_produit_client();
					},
					error: function() {
						$("#commercial").select2("val",0);
					}
				});	
			}
			
			// actualise la liste des adresses d'interventions disponibles 
			var tab_adresse=new Array();
			function actualiser_adresse_client(adresses){
				if(adresses){
					$("#adr_ref_id").select2({
						data: adresses		
					})
					tab_adresse=new Array();
					for(i=0;i<=adresses.length-1;i++){
						tab_adresse[adresses[i].id]=new Array(6);
						tab_adresse[adresses[i].id][0]=adresses[i].text;
						tab_adresse[adresses[i].id][1]=adresses[i].service;
						tab_adresse[adresses[i].id][2]=adresses[i].ad1;
						tab_adresse[adresses[i].id][3]=adresses[i].ad2;
						tab_adresse[adresses[i].id][4]=adresses[i].ad3;
						tab_adresse[adresses[i].id][5]=adresses[i].cp;
						tab_adresse[adresses[i].id][6]=adresses[i].ville;
					}
					if($('input[name=adr_ref]:checked').val()==1){
						// adresse du client
						$("#adr_ref_id").select2("val",adresses[0].id);
						afficher_adresse(adresses[0].id);
					}					
				
				}else{
					$("#adr_ref_id option[value!='0']").remove();
				}			
			};
			
			// affiche les champs selon le type d'adresse
			function afficher_type_adresse(){				
				switch($('input[name=adr_ref]:checked').val()){
					case "1":
						// client
						$("#cont_adr_client").show();
						$("#cont_adr").show();
						break;
					case "2":
						// agence
						$("#cont_adr_client").hide();
						$("#cont_adr").hide();
						break;
						
					default:
						// autre
						$("#cont_adr_client").hide();
						$("#cont_adr").show();
						afficher_adresse(0);
						break;
				} 
			}
			
			// renseigne les champ lié à une adresse
			function afficher_adresse(adresse){
				if(adresse!=0){
					$("#adr_nom").val(tab_adresse[adresse][0]);
					$("#adr_service").val(tab_adresse[adresse][1]);
					$("#adr_ad1").val(tab_adresse[adresse][2]);
					$("#adr_ad2").val(tab_adresse[adresse][3]);
					$("#adr_ad3").val(tab_adresse[adresse][4]);
					$("#adr_cp").val(tab_adresse[adresse][5]);
					$("#adr_ville").val(tab_adresse[adresse][6]);					
				}else{
					console.log("VIDER");
					$(".champ_adresse").val("");
				}
			}
			
			// actualise la liste des produits pouvent etre utiliser dans l'action
			function actualiser_produit_client(){
				
				var client=$("#client").select2("val");
				var type= $('input[name=produit_type]:checked').val();

				$("#produit option[value!='0']").remove();
				$.ajax({			
					type:'GET',
					url: 'ajax/ajax_produit.php',
					data : 'client=' + client + "&type=" + type, 
					dataType: 'json',                
					success: function(data)
					{	$("#produit").select2({
							data: data		
						})					
					},
					error: function() {
						
					}
				});
			}	
			
			// actualiser les données selon le produit selection 
			function actualiser_produit(){
				var produit=$("#produit").val();
				var type= $('input[name=produit_type]:checked').val();
				var client=$("#client").val();
				
				if(produit>0){
					$.ajax({			
						type:'POST',
						url: 'ajax/ajax_produit_get.php',
						data : 'produit=' + produit + "&type=" + type, 
						dataType: 'json',                
						success: function(data)
						{	if(type==1){
									// intra
								var ca_demi_j=data.ht_intra/data.nb_demi_jour;
								$("#ca_intra").val(ca_demi_j*nb_demi_j_select);
								
							
							
							}else{
									// inter
								$("#ca_inter").val(data.ht_inter);
							}
						},
						error: function() {
							
						}
					});
				}else{
					$("#ca_inter").val(0);
					$("#ca_intra").val(0);
				}
			}
		</script>	
	</body>
</html>