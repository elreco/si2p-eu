<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_diplome.php');

	$erreur=0;
	
	$dip_id=0;
	if(!empty($_POST["dip_id"])){
		$dip_id=$_POST["dip_id"];
	};
	
	$dip_libelle="";
	if(!empty($_POST["dip_libelle"])){
		$dip_libelle=$_POST["dip_libelle"];
	}else{
		$erreur=1;
	};
	
	$dip_validite=0;
	if(!empty($_POST["dip_validite"])){
		$dip_validite=$_POST["dip_validite"];
	};
	// ENREGISTREMENT DU RIB
	
	if($erreur==0){

		if($dip_id>0){
			
			// MISE A JOUR D'UN DIPLOME
			
			update_diplome($dip_id,$dip_libelle,$dip_validite);
				
		
		}else{
		
			// AJOUT D'UN DIPLOME
			
			$dip_id=insert_diplome($dip_libelle,$dip_validite);
			
			if($dip_id==0){
				$erreur=1;
			};
		};	
	};	
		
	header('Location: diplome_liste.php?erreur=' . $erreur);      
	
?>

