<?php

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';


	// TRAITEMENT DU FORM

	if(isset($_GET['liste'])){
		
		$liste=intval($_GET['liste']);
		
		if($liste==1){
			$filtre= array(
				"bug_statut" => 2,
				"date_deb_sql" => date("Y-m-d"),
				"date_fin_sql" => date("Y-m-d"),
				"date_deb" => date("d/m/Y"),
				"date_fin" => date("d/m/Y"),
				"niveau" => 0
			);
			$_SESSION['filtre'] =$filtre;	
		}else{
			header("location:accueil.php");
			die();
		}
	
	}elseif(isset($_POST['search'])){
		
		$bug_statut=intval($_POST['bug_statut']);
		
		$date_deb="";
		if(!empty($_POST["date_deb"])){
			$DT_date_deb=date_create_from_format('d/m/Y',$_POST["date_deb"]);
			if(!is_bool($DT_date_deb)){
				$date_deb=$DT_date_deb->format("Y-m-d");
			}
		}
		$date_fin="";
		if(!empty($_POST["date_fin"])){
			$DT_date_fin=date_create_from_format('d/m/Y',$_POST["date_fin"]);
			if(!is_bool($DT_date_fin)){
				$date_fin=$DT_date_fin->format("Y-m-d");
			}
		}
		
		$bug_niveau=0;
		if(!empty($_POST["bug_niveau"])){
			$bug_niveau=intval($_POST["bug_niveau"]);
		}
		
		$filtre= array(
			"bug_statut" => $bug_statut,
			"date_deb_sql" => $date_deb,
			"date_fin_sql" => $date_fin,
			"date_deb" => $_POST["date_deb"],
			"date_fin" => $_POST["date_fin"],
			"niveau" => $bug_niveau
		);

		$_SESSION['filtre'] =$filtre;

	}elseif(isset($_SESSION['filtre'])){

		$filtre=$_SESSION['filtre'];

	}else{
		$filtre= array(
			"bug_statut" => -1,
			"date_deb_sql" => "",
			"date_fin_sql" => "",
			"date_deb" => "",
			"date_fin" => "",
			"niveau" => 0
		);
		$_SESSION['filtre']=$filtre;
	}
	
	if(isset($filtre)){
		
		$sql="SELECT bug_id,DATE_FORMAT(bug_date,'%d/%m/%Y') AS bug_date_fr,bug_texte,DATE_FORMAT(bug_statut_date,'%d/%m/%Y') AS bug_statut_date_fr,bug_statut,bug_id
		,bug_niveau FROM bugs";
		$mil="";
		if($filtre["bug_statut"]>=0){
			$mil.=" AND bug_statut=" . $filtre["bug_statut"];
		}
		if(!empty($filtre["date_deb_sql"])){
			$mil.=" AND bug_statut_date>='" . $filtre["date_deb_sql"] . "'";
		}
		if(!empty($filtre["date_fin_sql"])){
			$mil.=" AND bug_statut_date<='" . $filtre["date_fin_sql"] . "'";
		}
		if(!empty($filtre["niveau"])){
			$mil.=" AND bug_niveau=" . $filtre["niveau"];
		}
		if($mil!=""){
			$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		}
		$sql.=" ORDER BY bug_date";
		//echo($sql);
		//die();
		$req = $Conn->query($sql);
		$bugs = $req->fetchAll();
	}
	
	
	$lib_statut=array(
		"0" => "Pris en compte",
		"1" => "En cours de traitement",
		"2" => "Traité"
	);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />


		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php
			include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<section id="content" class="animated fadeIn">
				
					<div class="admin-form theme-primary ">
						<div class="panel heading-border panel-primary">
							<div class="panel-body bg-light">
							
								<div class="content-header">
									<h2>Orion : <b class="text-primary">suivi des incidents</b></h2>
								</div>
							
								<form method="post" action="bugs_liste.php" id="form" >
								<?php
									$col=3;
									$col_offset="";
									if($_SESSION['acces']['acc_profil']==13 OR $_SESSION['acces']['acc_profil']==9){ 
										$col=2;
										$col_offset="col-md-offset-1";
									} ?>
									<div class="row">
										<div class="col-md-<?=$col?> <?=$col_offset?>">
											<label for="bug_statut" >Statut :</label>
											<div class="field select">											
												<select name="bug_statut" id="bug_statut" >
													<option value="-1">Statut ...</option>
													<option value="0" <?php if($filtre["bug_statut"]==0) echo("selected"); ?> >Pris en compte</option>
													<option value="1" <?php if($filtre["bug_statut"]==1) echo("selected"); ?> >En cours de traitement</option>
													<option value="2" <?php if($filtre["bug_statut"]==2) echo("selected"); ?> >Traité</option>											
												</select>
												<i class="arrow simple"></i>
											</div>
										</div>	
										<div class="col-md-<?=$col?>">
											<label for="date_deb" >Entre le</label>
											<span  class="field prepend-icon">
												<input type="text" id="date_deb" name="date_deb" class="gui-input datepicker" placeholder="Entre le" value="<?=$filtre["date_deb"]?>" />
												<span class="field-icon"><i
													class="fa fa-calendar-o"></i>
												</span>
											</span>
										</div>
										<div class="col-md-<?=$col?>">
											<label for="date_fin" >et le</label>
											<span  class="field prepend-icon">
												<input type="text" id="date_fin" name="date_fin" class="gui-input datepicker" placeholder="et le" value="<?=$filtre["date_fin"]?>" />
												<span class="field-icon"><i
													class="fa fa-calendar-o"></i>
												</span>
											</span>
										</div>
							<?php		if($_SESSION['acces']['acc_profil']==13 OR $_SESSION['acces']['acc_profil']==9){ 	 ?>	
											<div class="col-md-<?=$col?>">
												<label for="bug_niveau" >Niveau :</label>
												<input type="number" name="bug_niveau" class="gui-input" id="bug_niveau" placeholder="Niveau" min="0" max="3" step="1" value="<?=$filtre["niveau"]?>" />	
											</div>
							<?php		} ?>

										
										<div class="col-md-<?=$col?> text-center pt20">
											<button type="submit" class="btn btn-sm btn-info" name="search" >
												<i class="fa fa-search" ></i> Afficher les incidents
											</button>
										</div>
									</div>
									
									<h1>Liste des incidents</h1>
									
							<?php	if(!empty($bugs)){ ?>
										<div class="table-responsive mt15">
											<table class="table table-striped table-hover">
												<thead>
													<tr class="dark" >
											<?php		if($_SESSION['acces']['acc_profil']==13 OR $_SESSION['acces']['acc_profil']==9){ ?>
															<th>ID</th>
															<th>Niveau</th>
											<?php		} ?>			
														<th>Date</th>
														<th>Incident</th>
														<th>Statut</th>
														<th>Statut date</th>
											<?php		if($_SESSION['acces']['acc_profil']==13 OR $_SESSION['acces']['acc_profil']==9){ ?>	
															<th>&nbsp;</th>
											<?php		} ?>
													</tr>
												</thead>									
												<tbody>
									<?php 			foreach($bugs as $b){
														$bug_texte="";
														if(!empty($b['bug_texte'])){
															$bug_texte=str_replace(chr(10),"<br/>",$b['bug_texte']);
														}?>
														<tr>
									<?php					if($_SESSION['acces']['acc_profil']==13 OR $_SESSION['acces']['acc_profil']==9){ ?>
																<td><?=$b["bug_id"]?></td>
																<td><?=$b["bug_niveau"]?></td>
											<?php			} ?>			
															<td><?=$b["bug_date_fr"]?></td>													
															<td><?=$bug_texte?></td>
															<td><?=$lib_statut[$b['bug_statut']]?></td>
															<td><?=$b["bug_statut_date_fr"]?></td>			
																														
													<?php	if($_SESSION['acces']['acc_profil']==13 OR $_SESSION['acces']['acc_profil']==9){ ?>
																<td class="text-center" >
																	<a href="bugs.php?id=<?=$b["bug_id"]?>" class="btn btn-sm btn-warning" >
																		<i class="fa fa-pencil" ></i>
																	</a>
																</td>
													<?php	} ?>															
														</tr>
							<?php					} ?>
												</tbody>
											</table>						
										</div>
							<?php	}else{ ?>
										<p class="alert alert-warning" >
											Aucun incident ne correspond à votre recherche.
										</p>
								
							<?php	} ?>
								</form>
							</div>
						</div>
					</div>					
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
			<?php	if(!empty($_SESSION["retour"])){ ?>
						<a href="<?=$_SESSION["retour"]?>" class="btn btn-default btn-sm" >
							<span class="fa fa-long-arrow-left"></span>
							Retour
						</a>
			<?php	} ?>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
	<?php			if($_SESSION['acces']['acc_profil']==13 OR $_SESSION['acces']['acc_profil']==9){ ?>
						<a href="bugs.php" class="btn btn-sm btn-success" >
							<span class="fa fa-save"></span>
							Déclarer un incident
						</a>
	<?php			} ?>
				</div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" >
		
			jQuery(document).ready(function (){

			});
		</script>
		
	</body>
</html>
