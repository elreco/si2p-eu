<?php

	// DETAIL DES TAUX DE SATISFACTION

	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
    include "includes/connexion_soc.php";
    include "includes/connexion_fct.php";

	// CONTROLE D'ACCES
	// cf item stat_formation.php stat_2

    if($_SESSION['acces']['acc_service'][1]!=1 AND !in_array($_SESSION['acces']['acc_profil'], array('1', '4', '15', '10', '11', '14'))) {
		echo("Accès refusé!");
		die();
    }
    
    $erreur_txt="";

    if(isset($_SESSION["stat_form"])){

        $periode_deb="";
		if(!empty( $_SESSION["stat_form"]["periode_deb"])){
            $periode_deb=$_SESSION["stat_form"]["periode_deb"];
			$DT_periode_deb=date_create_from_format('Y-m-d',$periode_deb);
		}
		
		$periode_fin="";
		if(!empty($_SESSION["stat_form"]["periode_fin"])){
            $periode_fin=$_SESSION["stat_form"]["periode_fin"];
			$DT_periode_fin=date_create_from_format('Y-m-d',$periode_fin);
		}

        $indicateur=0;
        if(!empty($_SESSION["stat_form"]["indicateur"])){
            $indicateur=intval($_SESSION["stat_form"]["indicateur"]);
        }

        if(empty($periode_deb) OR empty($periode_fin) OR empty($indicateur)){
            $erreur_txt="Paramètres absents! Impossible d'afficher la page.";
        }

    }else{

        $erreur_txt="Paramètres absents! Impossible d'afficher la page.";

    }

    if(!empty($erreur_txt)){
        $erreur_txt="Formulaire incomplet!";

        $_SESSION['message'][] = array(
            "aff" => "",
            "titre" => "Erreur!",
            "type" => "danger",
            "message" => $erreur_txt
        );
        header("location : stat_formation.php");
        die();

    }

    $cle=0;
    if(!empty($_GET["cle"])){
        $cle=intval($_GET["cle"]);
    }

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
    }

    $acc_utilisateur=0;
    if(!empty($_SESSION['acces']["acc_ref"])){
        if($_SESSION['acces']["acc_ref"]==1){
            $acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
        }
    }

    $sql="SELECT COUNT(sar_avis) as nb_question,SUM(sar_reponse) as score,sar_utilisateur
    ,acl_id,acl_pro_reference
    ,act_id,act_date_deb
    ,cli_code,cli_nom";
    $sql.=" FROM Actions_Clients 
    INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
    LEFT JOIN Stagiaires_Avis_Reponses ON (Stagiaires_Avis_Reponses.sar_action_client=Actions_Clients.acl_id)";
    /*$sql.=" FROM Stagiaires_Avis_Reponses 
    INNER JOIN Actions_Clients ON (Stagiaires_Avis_Reponses.sar_action_client=Actions_Clients.acl_id)
    INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)*/
    $sql.=" LEFT JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND Actions.act_agence=Clients.cli_agence)
    WHERE NOT acl_archive AND NOT act_archive
    AND act_date_deb>='" . $periode_deb . "' AND act_date_deb<='" . $periode_fin . "'";
    if($_SESSION['acces']['acc_profil'] == 1){
        $sql.=" AND sar_utilisateur=" . $acc_utilisateur;
    }
    if(!empty($cle)){
        switch ($indicateur) {
            case 1:
                $sql.=" AND act_pro_sous_famille=" . $cle;
                break;
            case 2:
                $sql.=" AND acl_produit=" . $cle;
                break;
            case 3:
                $sql.=" AND sar_utilisateur=" . $cle;
                break;
        }
    }
    if(!empty($acc_agence)){
        $sql.=" AND act_agence=" . $acc_agence;
    }
    if($_SESSION['acces']['acc_profil'] == 1){
        $sql.=" AND sar_utilisateur=" . $acc_utilisateur;
    }
    $sql.=" GROUP BY sar_utilisateur,acl_id,acl_pro_reference,act_id,act_date_deb
    ORDER BY act_date_deb,act_id";
    $req=$ConnSoc->query($sql);
    $d_avis=$req->fetchAll();



    $titre="Détail des taux de satisfaction";
    if(!empty($cle)){
        switch ($indicateur) {
            case 1:

                $sql="SELECT psf_libelle,pfa_libelle FROM Produits_Sous_Familles,Produits_Familles WHERE psf_pfa_id=pfa_id AND psf_id=" . $cle . ";";
                $req=$Conn->query($sql);
                $d_sous_famille=$req->fetch(PDO::FETCH_ASSOC);       
                $titre.=" pour les formations " .  $d_sous_famille["pfa_libelle"] . " " . $d_sous_famille["psf_libelle"];
                break;
            case 2:
                $sql="SELECT pro_code_produit FROM Produits WHERE pro_id=" . $cle . "";
                $req=$Conn->query($sql);
                $d_produit=$req->fetch(PDO::FETCH_ASSOC);    
                $titre.=" pour les formations " .  $d_produit["pro_code_produit"];
                break;
            case 3:
                $sql="SELECT uti_nom,uti_prenom FROM Utilisateurs WHERE uti_id=" . $cle . "";
                $req=$Conn->query($sql);
                $d_utilisateur=$req->fetch(PDO::FETCH_ASSOC);    
                $titre.=" pour les formations de " .  $d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"];
                break;
        }

    }

    

    $titre.="<br/>entre le " . $DT_periode_deb->format("d/m/Y") . " et le " . $DT_periode_fin->format("d/m/Y");

    // UTILISATEURS ORIONS

    $sql="SELECT uti_id,uti_nom,uti_prenom FROM Utilisateurs ORDER BY uti_id";
    $req=$Conn->query($sql);
    $d_utilisateurs=$req->fetchAll(PDO::FETCH_ASSOC);  

    

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<div id="main" >
<?php
			if($_SESSION["acces"]["acc_ref"]==1){
				include "includes/header_def.inc.php";
			} ?>
			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" style="overflow:scroll;" >

                    <h1 class="text-center"><?=$titre?></h1>

       <?php        if (!empty($d_avis)) { ?>
                        <table class="table table-striped table-hover" >
                            <thead>
                                <tr class="dark">
                                    <th>Action</th>
                                    <th>Date de début</th>
                                    <th>Code client</th>
                                    <th>Nom client</th>
                                    <th>Produit</th>
                                    <th>Intervenant</th>
                                    <th>Nb. réponses</th>
                                    <th>Score</th>
                                    <th>Taux de satisfaction</th>
                                </tr>                                    
                            </thead>
                            <tbody>
                <?php           $total_question=0; 
                                $total_score=0;
                                foreach ($d_avis as $avis){

                                    $utilisateur="";
                                    $k=array_search($avis["sar_utilisateur"],array_column ($d_utilisateurs,"uti_id"));
                                    if(!is_bool($k)){
                                        $utilisateur=$d_utilisateurs[$k]["uti_nom"] . " " . $d_utilisateurs[$k]["uti_prenom"];
                                    }     
                                    
                                    $Dt_date_deb=DateTime::createFromFormat('Y-m-d',$avis["act_date_deb"]);
                                    $act_date_deb=$Dt_date_deb->format("d/m/Y");

                                       
                                    $taux=0;
                                    if(!empty($avis["nb_question"])){
                                        $taux=($avis["score"]/$avis["nb_question"]); 
                                    }
                                    $class_ligne="";
                                    if(!empty($taux) AND $taux<=80){
                                        $class_ligne="class='warning'";
                                    }

                                    $total_question=$total_question + $avis["nb_question"]; 
                                    $total_score=$total_score + $avis["score"];  ?>

                                    <tr <?=$class_ligne?> >
                                        <td><?=$avis["act_id"] . "-" . $avis["acl_id"]?></td>
                                        <td><?=$act_date_deb?></td>
                                        <td><?=$avis["cli_code"]?></td>
                                        <td><?=$avis["cli_nom"]?></td>
                                        <td><?=$avis["acl_pro_reference"]?></td>
                                        <td><?=$utilisateur?></td>
                                <?php   if(!empty($avis["nb_question"])){
                                             echo("<td class='text-right' >" . $avis["nb_question"] . "</td>");
                                             echo("<td class='text-right' >" . $avis["score"] . "</td>");
                                        }else{
                                            echo("<td class='text-center text-danger' colspan='2' >Pas d'avis de stage</td>");
                                        } ?>
                                        <td class='text-right' ><?=number_format($taux,2,","," ")?></td>
                                    </tr>
                <?php           } 
                                $taux=0;
                                if(!empty($total_question)){
                                    $taux=($total_score/$total_question); 
                                }
                                ?>
                                <tr>
                                    <th colspan="6" >Total :</td>
                                    <td class="text-right" ><?=number_format($total_question,2,","," ")?></td>
                                    <td class="text-right" ><?=number_format($total_score,2,","," ")?></td>
                                    <td class="text-right" ><?=number_format($taux,2,","," ")?></td>
                                </tr>
                            </tbody>
                        </table>
            <?php   } 
                    ?>               
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 text-center">
                    <a href="stat_form_avis.php" class="btn btn-md btn-default" >
                        Retour
                    </a>
                </div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){

				

			});
		</script>
	</body>
</html>
