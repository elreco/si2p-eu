<?php
include "includes/controle_acces.inc.php";
include("modeles/mod_parametre.php");
include("includes/connexion.php");
include("includes/connexion_soc.php");
include("includes/connexion_fct.php");

// VARIABLES

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
$facture=0;
if(!empty($_POST['facture'])){
    $facture = $_POST['facture'];
}
$societe=0;
if(!empty($_POST['societe'])){
    $societe = $_POST['societe'];
}

$ConnFct = connexion_fct($societe);
$sql="SELECT uti_nom, uti_prenom FROM Utilisateurs WHERE uti_id = " . $acc_utilisateur;
$req = $Conn->query($sql);
$utilisateur=$req->fetch();
// ALLER CHERCHER LA RELANCE
$sql="SELECT * FROM Factures WHERE fac_id =" . $facture;
$req = $ConnFct->query($sql);
$d_facture=$req->fetch();

$comment_auto = 1;
$fac_date_reg_rel = convert_date_sql($_POST['fac_date_reg_rel']);
if(empty($fac_date_reg_rel)){
	$fac_date_reg_rel = NULL;
}
if(!empty($_POST['relance_statut']) && ($_POST['relance_statut'] == 1 OR $_POST['relance_statut'] == 2 OR $_POST['relance_statut'] == 3)){

	$comment_auto = 1;
	$fac_relance_stop = $_POST['relance_statut'];

}else{
	$fac_relance_stop = 0;
}
$sql="UPDATE Factures SET  fac_date_reg_rel = :fac_date_reg_rel, fac_relance_stop=:fac_relance_stop
					 WHERE fac_id = " . $facture;
$req = $ConnFct->prepare($sql);
$req->bindValue(":fac_date_reg_rel",$fac_date_reg_rel);
$req->bindValue(":fac_relance_stop",$fac_relance_stop);
$req->execute();

if($comment_auto){
	if(empty($_POST['fco_id'])){
		$req = $ConnFct->prepare("INSERT INTO Factures_Commentaires (fco_facture, fco_comment, fco_utilisateur, fco_date) VALUES (:fco_facture, :fco_comment, :fco_utilisateur, NOW())");

	}else{
		$req = $ConnFct->prepare("UPDATE Factures_Commentaires SET fco_facture = :fco_facture, fco_comment = :fco_comment, fco_utilisateur = :fco_utilisateur, fco_date = NOW() WHERE fco_id = :fco_id");
		$req->bindValue("fco_id",$_POST['fco_id']);
	}
 	$req->bindValue("fco_facture",$facture);

    if(!empty($_POST['motif_stop'])){
    	$req->bindValue("fco_comment",$_POST['motif_stop']);
    }else{
		if($fac_relance_stop){
			$req->bindValue("fco_comment","suspension du processus de relance");
		}else{
			$req->bindValue("fco_comment","");
		}
    }
    $req->bindValue("fco_utilisateur",$acc_utilisateur);
    $req->execute();
}
// REDIRECTIONS
$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Votre relance est enregistrée"
    );

 $adresse=$_SESSION['retourFacture'];


header("location : " . $adresse);
die();
