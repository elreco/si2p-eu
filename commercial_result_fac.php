<?php 

// AFFICHE LA LISTE DES FACTURES DEPUIS LE TABLEAU RESULTAT

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_parametre.php');

// DONNEE UTILE AU PROGRAMME

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	} 
}

// PARAMETRE GET

//conso
$agence=0;
if(isset($_GET["agence"])){
	if(!empty($_GET["agence"])){
		$agence=intval($_GET["agence"]);
	}
}
$com_type=0;
if(isset($_GET["com_type"])){
	if(!empty($_GET["com_type"])){
		$com_type=intval($_GET["com_type"]);
	}
}
$cli_type=0;
if(isset($_GET["cli_type"])){
	if(!empty($_GET["cli_type"])){
		$cli_type=intval($_GET["cli_type"]);
	}
}
$commercial=0;
if(isset($_GET["commercial"])){
	if(!empty($_GET["commercial"])){
		$commercial=intval($_GET["commercial"]);
	}
}

// periode
$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}
$mois=0;
if(isset($_GET["mois"])){
	if(!empty($_GET["mois"])){
		$mois=intval($_GET["mois"]);
	}
}
$prec=false;
if(isset($_GET["prec"])){
	$prec=true;
}

$vu=0;
if(isset($_GET["vu"])){
	if(!empty($_GET["vu"])){
		$vu=intval($_GET["vu"]);
	}
}

// classification
$categorie="";
if(isset($_GET["categorie"])){
	if(!empty($_GET["categorie"])){
		$categorie=$_GET["categorie"];
	}
}
$famille=0;
if(isset($_GET["famille"])){
	if(!empty($_GET["famille"])){
		$famille=intval($_GET["famille"]);
	}
}
$s_famille=0;
if(isset($_GET["s_famille"])){
	if(!empty($_GET["s_famille"])){
		$s_famille=intval($_GET["s_famille"]);
	}
}
$s_s_famille=0;
if(isset($_GET["s_s_famille"])){
	if(!empty($_GET["s_s_famille"])){
		$s_s_famille=intval($_GET["s_s_famille"]);
	}
}

// CONTROLE D'ACCESS

if(!$_SESSION["acces"]["acc_droits"][35] AND $commercial==0){
	header("location:deconnect.php");
	die();
}elseif($agence>0){
	if($acc_agence>0 AND $agence!=$acc_agence){
		header("location:deconnect.php");
		die();
	}
}elseif(empty($agence) AND empty($commercial)){
	if(!isset($_SESSION["acces"]["acc_liste_societe"][$acc_societe . "-0"])){
		header("location:deconnect.php");
		die();
	}
}

// DONNEE PARAMETRE

if($vu==1 AND !empty($mois)){
	
	if(!empty($agence)){
		
		$sql="SELECT urp_mois FROM Agences,Utilisateurs_Rem_Periode WHERE age_intra_inter=urp_type AND urp_exercice=:exercice AND urp_periode=:mois AND age_id=:agence;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":exercice",$exercice);
		$req->bindParam(":mois",$mois);
		$req->bindParam(":agence",$agence);
		$req->execute();
		$d_periodes_mois=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($d_periodes_mois)){
			$tab_mois=array_column($d_periodes_mois,"urp_mois");
			$mois_liste=implode($tab_mois,",");
		}

	}else{
		
		$sql="SELECT urp_mois FROM Societes,Utilisateurs_Rem_Periode WHERE soc_intra_inter=urp_type AND urp_exercice=:exercice AND urp_periode=:mois AND soc_id=:societe;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":exercice",$exercice);
		$req->bindParam(":mois",$mois);
		$req->bindParam(":societe",$acc_societe);
		$req->execute();
		$d_periodes_mois=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($d_periodes_mois)){
			$tab_mois=array_column($d_periodes_mois,"urp_mois");
			$mois_liste=implode($tab_mois,",");
		}
	}
	
}

// CATEGORIE
$d_categories=array();
$sql="SELECT pca_id,pca_libelle FROM Produits_Categories ORDER BY pca_id;";
$req=$Conn->query($sql);
$d_result_cat=$req->fetchAll();
if(!empty($d_result_cat)){
	foreach($d_result_cat as $cat){
		$d_categories[$cat["pca_id"]]=$cat["pca_libelle"];
	}	
}
// Famille
$d_familles=array();
$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles  ORDER BY pfa_id;";
$req=$Conn->query($sql);
$d_result_fam=$req->fetchAll();
if(!empty($d_result_fam)){
	foreach($d_result_fam as $fam){
		$d_familles[$fam["pfa_id"]]=$fam["pfa_libelle"];
	}	
}
// Sous-Famille
$d_s_familles=array();
$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles ORDER BY psf_id;";
$req=$Conn->query($sql);
$d_result_s_fam=$req->fetchAll();
if(!empty($d_result_s_fam)){
	foreach($d_result_s_fam as $s_fam){
		$d_s_familles[$s_fam["psf_id"]]=$s_fam["psf_libelle"];
	}	
}
// Sous-Sous-Famille
$d_s_s_familles=array();
$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles ORDER BY pss_id;";
$req=$Conn->query($sql);
$d_result_s_s_fam=$req->fetchAll();
if(!empty($d_result_s_s_fam)){
	foreach($d_result_s_s_fam as $s_s_fam){
		$d_s_s_familles[$s_s_fam["pss_id"]]=$s_s_fam["pss_libelle"];
	}	
}

// Famille a exclure
// exemple si on affiche famille formation, il ne faut pas juste filtrer fli_famille=fomration
// mais il faut exclure les sous-éléments qui disposent de leur propre objectifs
// on part du niv le plus bas et on remonte l'arbo
$mil="";
if($categorie!="total"){
	//categorie=999 on affiche tout
	//categorie=0 est reserve pour autre
	if($s_s_famille>0){
		
		$mil.=" AND fli_sous_sous_famille=" . $s_s_famille;
		
	}else{
		
		$sql="SELECT cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille 
		FROM Commerciaux 
		LEFT JOIN Commerciaux_Objectifs ON (Commerciaux.com_id=Commerciaux_Objectifs.cob_commercial)
		WHERE cob_exercice=" . $exercice;
		// critere d'accès
		if(!$_SESSION["acces"]["acc_droits"][35]){
			$sql.=" AND com_ref_1=" . $acc_utilisateur;
		}elseif($acc_agence>0){
			$sql.=" AND com_agence=" . $acc_agence;
		}
		// critère de tri
		if($commercial>0){
			$sql.=" AND com_id=" . $commercial;
		}elseif($com_type>0){
			$sql.=" AND com_type=" . $com_type;
		}elseif($agence>0){
			$sql.=" AND com_agence=" . $agence;
		}
		if($categorie=="conso"){
			
			$sql.=" AND NOT cob_autre";
			
		}else{
			if($s_famille>0){
				$mil.=" AND fli_sous_famille=" . $s_famille;
				$sql.=" AND cob_sous_famille=" . $s_famille . " AND cob_sous_sous_famille>0";
			}elseif($famille>0){
				$mil.=" AND fli_famille=" . $famille;
				$sql.=" AND cob_famille=" . $famille . " AND cob_sous_famille>0";
			}elseif($categorie>0){
				$mil.=" AND fli_categorie=" . $categorie;
				$sql.=" AND cob_categorie=" . $categorie . " AND cob_famille>0";
			}
		}
		$sql.=";";
		$req=$ConnSoc->query($sql);
		$d_exclut=$req->fetchAll();
		if(!empty($d_exclut)){
			foreach($d_exclut as $exclut){
				if($exclut["cob_sous_sous_famille"]>0){
					$mil.=" AND NOT fli_sous_sous_famille=" . $exclut["cob_sous_sous_famille"];	
				}elseif($exclut["cob_sous_famille"]>0){
					$mil.=" AND NOT fli_sous_famille=" . $exclut["cob_sous_famille"];	
				}elseif($exclut["cob_famille"]>0){
					$mil.=" AND NOT fli_famille=" . $exclut["cob_famille"];	
				}elseif($exclut["cob_categorie"]>0){
					$mil.=" AND NOT fli_categorie=" . $exclut["cob_categorie"];	
				}
			}
		}
	}
}
// champ a selectionne
	$sql="SELECT fac_id,fac_chrono,fac_numero,fac_date,fac_cli_code
	,fli_code_produit,fli_montant_ht,fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille
	,cli_code,cli_nom
	,com_label_1,com_label_2
	FROM Factures 
	LEFT JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
	LEFT OUTER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Clients.cli_agence=Factures.fac_agence)
	LEFT OUTER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";
	if($prec){
		$sql.=" WHERE fac_date>='" . intval($exercice-1) . "-04-01' AND fac_date<='" . $exercice . "-03-31'";
	}else{
		$sql.=" WHERE fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'";
	}
	$sql.=" AND fli_categorie<4";
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif($acc_agence>0){
		$sql.=" AND fac_agence=" . $acc_agence;
	}
	
	if($commercial>0){
		$sql.=" AND fac_commercial=" . $commercial;
	}elseif($com_type>0){
		$sql.=" AND com_type=" . $com_type;
	}elseif($agence>0){
		$sql.=" AND fac_agence=" . $agence;
	}
	if($cli_type>0){
		if($cli_type==2){
			$sql.=" AND (fac_cli_categorie=2 OR fac_client=8891)";
		}else{
			$sql.=" AND NOT fac_cli_categorie=2 AND NOT fac_client=8891";
		}
	}

	if($mois>0){
		if($vu==1){
			if(!empty($mois_liste)){
				$sql.=" AND MONTH(fac_date) IN (" . $mois_liste . ")";
			}
			
		}else{
			$sql.=" AND MONTH(fac_date)=" . $mois;
		}
	}
	
	if($mil!=""){
		$sql.=$mil;
	}
	$sql.=" ORDER BY fac_date,fac_chrono,fac_numero;";
	$req = $ConnSoc->prepare($sql);	
	$req->execute();
	$factures = $req->fetchAll();
	
	/*echo("<pre>");
		print_r($factures);
	echo("</pre>");
	die();*/
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">

		<?php 			if(!empty($factures)){ 
							$total_ht=0; ?>
			
							<div class="table-responsive">
								<table class="table table-striped" >
									<thead>
										<tr class="dark2" >											
											<th>Client</th> 
											<th>Commercial</th>
											<th>Facture</th>
											<th>Date</th>
											<th>Code produit</th>
											<th>Catégorie</th>
											<th>Famille</th>
											<th>Sous-Famille</th>
											<th>Sous-Sous-Famille</th>
											<th>H.T.</th>																																						
										</tr>
									</thead>
									<tbody>
							<?php		foreach($factures as $f){
											
											// style de la ligne										
											$style="";
											if(!empty($f['fac_nature'])){
												if(!empty($d_client_sous_categorie[$f['sus_sous_categorie']])){
													$style="background-color:#FED";				
												}
											}
											$fac_date="";
											if(!empty($f['fac_date'])){
												$dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
												$fac_date=$dt_fac_date->format("d/m/Y");
											}

											$total_ht+=$f['fli_montant_ht'];
											
											?>
											<tr>
												<td><?= $f['cli_code']?></td>
												<td><?= $f['com_label_1'] ?></td>
												<td><?= $f['fac_numero'] ?></td>																					
												<td><?=	$fac_date?></td>
												<td><?=	$f['fli_code_produit'] ?></td>
												<td>
										<?php		if(!empty($f['fli_categorie'])){
														echo($d_categories[$f['fli_categorie']]);
													}else{
														echo("&nbsp;");
													} ?>
												</td>
												<td>
										<?php		if(!empty($f['fli_famille'])){
														echo($d_familles[$f['fli_famille']]);
													}else{
														echo("&nbsp;");
													} ?>
												</td>
												<td>
										<?php		if(!empty($f['fli_sous_famille'])){
														echo($d_s_familles[$f['fli_sous_famille']]);
													}else{
														echo("&nbsp;");
													} ?>
												</td>
												<td>
										<?php		if(!empty($f['fli_sous_sous_famille'])){
														echo($d_s_s_familles[$f['fli_sous_sous_famille']]);
													}else{
														echo("&nbsp;");
													} ?>
												</td>
												<td class="text-right" ><?= $f['fli_montant_ht'] ?></td>																		
											</tr>
				<?php					}  ?>
									</tbody>
									<tfoot>
										<tr>
											<th colspan="9" class="text-right" >Total :</th>
											<td class="text-right" ><?=$total_ht?></td>											
										</tr>
									</tfoot>
								</table>
							</div>
		<?php 			}else{ ?>
							<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-warning" style="border-radius:0px;">
									Aucune facture correspondant à votre recherche.
								</div>
							</div>
		<?php			} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="commercial_result.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&vu=<?=$vu?>" class="btn btn-default btn-sm" role="button">
							<i class="fa fa-left"></i> Retour
						</a>	</div>
					<div class="col-xs-6 footer-middle">&nbsp;</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
			
			});						
		</script>
	</body>
</html>
