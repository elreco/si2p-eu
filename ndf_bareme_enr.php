<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');

$req = $Conn->prepare("SELECT * FROM baremes_kilometres WHERE bki_date_deb <= :bki_date_deb AND bki_date_fin >= :bki_date_deb");
$req->bindValue(':bki_date_deb', date("Y-m-d"));
$req->execute();
$bareme_kilometre = $req->fetch();
if (empty($_POST['bkl_bonus'])) {
    $_POST['bkl_bonus'] = 0;
}
if(!empty($_POST['edition'])){
    try {
        $req = $Conn->prepare("UPDATE baremes_kilometres_lignes SET bkl_coeff = :bkl_coeff, bkl_bonus = :bkl_bonus, bkl_puissance = :bkl_puissance, bkl_tranche_km = :bkl_tranche_km
        WHERE bkl_tranche_km = :bkl_tranche_km_before AND bkl_puissance = :bkl_puissance_before AND bkl_bareme = :bkl_bareme");
        $req->bindParam(':bkl_bareme', $_POST['bkl_bareme']);
        $req->bindParam(':bkl_puissance_before', $_POST['bkl_puissance_before']);
        $req->bindParam(':bkl_puissance', $_POST['bkl_puissance']);
        $req->bindParam(':bkl_tranche_km_before', $_POST['bkl_tranche_km_before']);
        $req->bindParam(':bkl_tranche_km', $_POST['bkl_tranche_km']);
        $req->bindParam(':bkl_coeff', $_POST['bkl_coeff']);
        $req->bindParam(':bkl_bonus', $_POST['bkl_bonus']);
        $req->execute();
    } catch (Exception $e) {
        $_SESSION['message'][] = array(
            "titre" => "Note de frais",
            "type" => "warning",
            "message" => "Le barème existe déjà."
        );
        Header("Location: ndf_bareme_liste.php");
        die();
    }
} else {
    try{
        $req = $Conn->prepare("INSERT INTO baremes_kilometres_lignes
        (bkl_bareme, bkl_puissance, bkl_tranche_km, bkl_coeff, bkl_bonus)
        VALUES (:bkl_bareme, :bkl_puissance, :bkl_tranche_km, :bkl_coeff, :bkl_bonus)");
        $req->bindParam(':bkl_bareme', $bareme_kilometre['bki_id']);
        $req->bindParam(':bkl_puissance', $_POST['bkl_puissance']);
        $req->bindParam(':bkl_tranche_km', $_POST['bkl_tranche_km']);
        $req->bindParam(':bkl_coeff', $_POST['bkl_coeff']);
        $req->bindParam(':bkl_bonus', $_POST['bkl_bonus']);
        $req->execute();
    } catch (Exception $e) {
        $_SESSION['message'][] = array(
            "titre" => "Note de frais",
            "type" => "warning",
            "message" => "Le barème existe déjà."
        );
        Header("Location: ndf_bareme_liste.php");
        die();
    }
}

Header("Location: ndf_bareme_liste.php");
die();
?>