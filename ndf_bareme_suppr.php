<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');

$req = $Conn->prepare("SELECT * FROM baremes_kilometres WHERE bki_date_deb <= :bki_date_deb AND bki_date_fin >= :bki_date_deb");
$req->bindValue(':bki_date_deb', date("Y-m-d"));
$req->execute();
$bareme_kilometre = $req->fetch();

$req = $Conn->prepare("DELETE FROM
baremes_kilometres_lignes
 WHERE bkl_bareme = :bkl_bareme AND bkl_puissance = :bkl_puissance AND bkl_tranche_km = :bkl_tranche_km");
$req->bindParam(':bkl_bareme', $bareme_kilometre['bki_id']);
$req->bindParam(':bkl_puissance', $_GET['bkl_puissance']);
$req->bindParam(':bkl_tranche_km', $_GET['bkl_tranche_km']);
$req->execute();


Header("Location: ndf_bareme_liste.php");
die();
?>