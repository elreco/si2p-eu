<?php
// ENREGISTREMENT D'UN DEVIS

include "includes/controle_acces.inc.php";
include_once("includes/connexion_soc.php");
include_once("includes/connexion.php");
include('modeles/mod_parametre.php');

// FIN DES CONTROLES

// ENREGISTREMENT
if(!empty($_POST['eva_id'])){
	$req=$Conn->query("SELECT * FROM Evacuations_Bilans WHERE ebi_evacuation=" . $_POST['eva_id']);
	$remarques=$req->fetchAll();

	foreach($remarques as $r){
		$sql_edit="UPDATE Evacuations_Bilans SET
		ebi_position = :ebi_position, ebi_remarque = :ebi_remarque, ebi_preconisation = :ebi_preconisation
		 WHERE ebi_id = :ebi_id;";
		 $req_edit=$Conn->prepare($sql_edit);


		$req_edit->bindValue(":ebi_position", intval($_POST['ebi_position'][$r['ebi_id']]));
		$req_edit->bindValue(":ebi_remarque", $_POST['ebi_remarque'][$r['ebi_id']]);
		$req_edit->bindValue(":ebi_preconisation", $_POST['ebi_preconisation'][$r['ebi_id']]);
		$req_edit->bindValue(":ebi_id",$r['ebi_id']);

		$req_edit->execute();
	}

}
$_SESSION['message'][] = array(
	"titre" => "Succès",
	"type" => "success",
	"message" => "Votre bilan a été enregistré"
);
Header("Location: evac_voir.php?id=" . $_POST['eva_id']);
die();
