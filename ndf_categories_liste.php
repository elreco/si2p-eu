<?php
////////////////// MENU ACTIF HEADER ///////////////////

////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
if (isset($_GET['menu_retour'])) {
	unset($_SESSION['retour']);
}





///////////////////// TRAITEMENTS SERVEUR ////////////////////

////////////////////FIN TRAITEMENTS SERVEUR /////////////////

///////////////////// TRAITEMENTS PHP ///////////////////////
// categories ndf
$req = $Conn->prepare("SELECT * FROM ndf_categories");
$req->execute();
$categories = $req->fetchAll();
// fin categories ndf

// comptes ndf
$req = $Conn->prepare("SELECT * FROM comptes");
$req->execute();
$comptes = $req->fetchAll();
// comptes ndf


////////////////////FIN TRAITEMENTS PHP//////////////////////
?>
<!DOCTYPE html>
<html>

<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Notes de frais</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
	<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


	<!-- Favicon -->
	<link rel="icon" type="image/png" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<style type="text/css">
	.panel-tabs>li>a {
		color: #AAA;
		font-size: 14px;
		letter-spacing: 0.2px;
		line-height: 30px;
		/*padding: 9px 20px 11px;*/
		border-radius: 0;
		border-left: 1px solid transparent;
		border-right: 1px solid transparent;
	}

	.nav2>li>a {
		position: relative;
		display: block;
		/* padding: 10px 15px; */
	}
</style>

<body class="sb-top sb-top-sm">


	<!-- Start: Main -->
	<div id="main">

		<?php include "includes/header_def.inc.php"; ?>
		<!-- Start: Content-Wrapper -->
		<section id="content_wrapper">

			<section id="content" class="animated fadeIn pr20 pl20 admin-forms">
				<h1>Liste des catégories de notes de frais</h1>
				<div class="row">

					<div class="col-md-8">

						<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
							<thead>
								<tr class="dark">
									<th>Catégorie</th>
									<th class="no-sort">TVA (%)</th>
									<th class="no-sort">Compte</th>
									<th class="no-sort">Véhicule</th>
									<th class="no-sort">Modifier</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($categories as $c) { ?>
									<tr class="editier_ligne" data-id="<?= $c['nca_id'] ?>">
										<td><?= $c['nca_libelle'] ?></td>
										<td><?= $c['nca_tva'] ?>

										</td>
										<td>
										<?php foreach ($comptes as $cpt) {
											if ($c['nca_compte'] == $cpt['cpt_id']) { ?>
												<?= $cpt['cpt_numero'] ?>
										<?php }
										} ?>
										</td>
										<td><?= $c['nca_vehicule'] ? 'Oui' : 'Non' ?></td>
										<td style="width:10%;">
											<button type="button" data-toggle="modal" data-target="#modal" data-id="<?= $c['nca_id'] ?>" data-tva="<?= $c['nca_tva_type'] ?>" data-libelle="<?= $c['nca_libelle'] ?>" data-compte="<?= $c['nca_compte'] ?>" data-vehicule="<?= $c['nca_vehicule'] ?>" class="btn btn-sm btn-warning editer"><i class="fa fa-pencil"></i></button>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>

					<form action="ndf_categories_liste_enr.php" method="POST">
						<div class="col-md-4">
							<div class="panel mb10">
								<div class="panel-heading panel-head-sm">
									<span class="panel-icon">
										<i class="fa fa-plus" aria-hidden="true"></i>
									</span>
									<span class="panel-title"> Ajouter une catégorie</span>
								</div>
								<div class="panel-body p10">
										<div class="row mb20">
											<div class="col-md-12">
												<p class="text-center mb20" style="font-weight:bold;">Catégorie :</p>
												<div class="field prepend-icon">
													<input type="text" name="nca_libelle" class="form-control" required>

												</div>

											</div>
										</div>

										<input type="hidden" name="nca_id" value="0">
										<div class="row">
											<div class="col-md-4">
												<p class="text-center mb20" style="font-weight:bold;">TVA :</p>
												<select name="nca_tva" class="select2">
													<?php foreach ($base_tva as $k => $b) {
														if ($k != 0) {
													?>
															<option value="<?= $k ?>"><?= $b ?></option>
													<?php }
													} ?>
												</select>

											</div>
											<div class="col-md-4">
												<p class="text-center mb20" style="font-weight:bold;">Compte :</p>
												<select name="nca_compte" class="select2">
													<option value="0">Compte non renseigné</option>
													<?php foreach ($comptes as $c) { ?>
														<option value="<?= $c['cpt_id'] ?>"><?= $c['cpt_numero'] ?> (<?= $c['cpt_libelle'] ?>)</option>
													<?php } ?>
												</select>

											</div>
											<div class="col-md-4">
												<p class="text-center mb20" style="font-weight:bold;">Véhicule :</p>
												<label class="option option-dark text-center" style="width:100%">
													<input type="checkbox" name="nca_vehicule" value="1">
												</label>

											</div>
										</div>
								</div>
								<div class="panel-footer text-right">
									<button type="submit" class="btn btn-sm btn-primary">Enregistrer</button>
								</div>

							</div>
						</div>
					</form>
				</div>
			</section>
		</section>
	</div>
	<!-- End: Main -->
	<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
		<div class="row">
			<div class="col-xs-4 footer-left pt7">
				<a href="parametre.php" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a>
			</div>
			<div class="col-xs-4 footer-middle pt7"></div>
			<div class="col-xs-4 footer-right pt7">


			</div>
		</div>
	</footer>
	<form action="ndf_categories_liste_enr.php" method="POST">
		<!-- Modal pour l'ajout/edition d'une ligne -->
		<div id="modal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title title_ligne">Modifier une catégorie</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<p class="text-center mb20" style="font-weight:bold;">Catégorie :</p>
								<div class="field prepend-icon">
									<input type="text" id="nca_libelle" name="nca_libelle" class="form-control" required>

								</div>

							</div>

						</div>
						<div class="row" style="margin-top:10px">
							<input type="hidden" id="nca_id" name="nca_id" value="0">
							<div class="col-md-4">
								<p class="text-center mb20" style="font-weight:bold;">TVA :</p>
								<select id="nca_tva" name="nca_tva" class="select2">
									<?php foreach ($base_tva as $k => $b) {
										if ($k != 0) {
									?>
											<option value="<?= $k ?>"><?= $b ?></option>
									<?php }
									} ?>
								</select>

							</div>

							<div class="col-md-4">
								<p class="text-center mb20" style="font-weight:bold;">Compte :</p>
								<select id="nca_compte" name="nca_compte" class="select2">
									<option value="0">Compte non renseigné</option>
									<?php foreach ($comptes as $c) { ?>
										<option value="<?= $c['cpt_id'] ?>"><?= $c['cpt_numero'] ?> (<?= $c['cpt_libelle'] ?>)</option>
									<?php } ?>
								</select>

							</div>
							<div class="col-md-4">
								<p class="text-center mb20" style="font-weight:bold;">Véhicule :</p>
								<label class="option option-dark text-center" style="width:100%">
									<input type="checkbox" name="nca_vehicule" id="nca_vehicule" value="1">
								</label>

							</div>
						</div>

					</div>




					<div class="modal-footer">
						<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Enregistrer</button>
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Fin modal ajout/suppression de ligne -->
	</form>



	<?php
	include "includes/footer_script.inc.php"; ?>

	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
	<!-- plugin pour les masques formulaires -->
	<script src="assets/js/custom.js"></script>

	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script src="vendor/plugins/summernote/summernote.min.js"></script>
	<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

	<script src="vendor/plugins/jquery.numberformat.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->


	<script>
		// DOCUMENT READY //a
		jQuery(document).ready(function() {

			// initilisation plugin datatables
			initDatatable()
			// quand on veut imprimer
			// edition d'une ligne
			// fin edition d'une ligne
			$(".editer").on('click', function() {
				$("#nca_id").val($(this).data("id"));
				$("#nca_libelle").val($(this).data("libelle"));
				$("#nca_tva").val($(this).data("tva"));
				$("#nca_compte").val($(this).data("compte"));
				if ($(this).data("vehicule") == 1) {
					$("#nca_vehicule").prop("checked", true);
				} else {
					$("#nca_vehicule").prop("checked", false);
				}

				$(".select2").select2();
			});
			////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
		});

		///////////// FONCTIONS //////////////
		function initDatatable() {
			$('#table_id').DataTable({
				"language": {
					"url": "vendor/plugins/DataTables/media/js/French.json"
				},
				"paging": false,
				"searching": false,
				"info": false,
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});
		}
		//////////// FIN FONCTIONS //////////
	</script>

</body>

</html>