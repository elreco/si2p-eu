<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';
include_once 'modeles/mod_parametre.php';

$menu_actif = "5-3";
if(empty($_SESSION['acces']["acc_droits"][33])){
    die("Vous n'avez pas accès au processus de relance");
}
$sql="SELECT * FROM societes WHERE soc_archive=0 ";
$req = $Conn->query($sql);
$societes = $req->fetchAll();
// INITIALISER LES VARIABLES SESSION
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
    $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
}
if(isset($_SESSION['fac_relance_tri'])){
	unset($_SESSION['fac_relance_tri']);
}
// LES AGENCES
if($acc_agence==0){
	$sql="SELECT age_id,age_code FROM Agences LEFT JOIN Societes ON (Agences.age_societe=Societes.soc_id)
	WHERE soc_id=" . $acc_societe. " AND soc_agence ORDER BY age_code";
	$req = $Conn->query($sql);
	$d_agences=$req->fetchAll();
	if(empty($d_agences)){
		unset($d_agences);
	}
}

// CHERCHER LES COMMERCIAUX POUR LE SELECT
$sql="SELECT com_id,com_label_1,com_label_2, com_agence FROM commerciaux";
$mil="";
if($_SESSION['acces']['acc_profil'] == 3 && !$_SESSION['acces']["acc_droits"][6]){
	$mil=" AND com_ref_1 = " . $_SESSION['acces']['acc_ref_id'];
}
if($acc_agence>0){
	$mil.=" AND com_agence= " . $acc_agence;
}
if($mil!=""){
	$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
}
$sql.=" ORDER BY com_label_1,com_label_2";
$req = $ConnSoc->query($sql);
$d_commercial=$req->fetchAll();
// CHERCHER LES LIBELLE RELANCES_ETATS POUR SELECT TYPE DE PROCéDURE
$sql="SELECT * FROM Relances_Etats ORDER BY ret_j_deb";
$req = $Conn->query($sql);
$d_statut=$req->fetchAll();

// chercher les libelles
$d_statut_libelle=array();
foreach($d_statut as $ds){
    $d_statut_libelle[$ds['ret_id']]= $ds['ret_libelle'];
}



// clients à rappeler ajd
$sql = "SELECT cli_id,cli_code,cli_nom,rel_id,rel_comment,rel_date,rel_utilisateur,rel_con_nom,rel_con_prenom,rel_con_tel,rel_etat_relance,rel_date_rappel
 FROM Relances,Clients WHERE cli_id=rel_client AND rel_utilisateur= " . $acc_utilisateur . " AND rel_date_rappel=NOW() ORDER BY rel_date_rappel DESC";
$req = $ConnSoc->query($sql);
$d_rappels=$req->fetchAll();

$data_order = 1;

$sql="SELECT soc_id,soc_nom FROM Societes WHERE NOT soc_archive ORDER BY soc_nom;";
$req = $Conn->query($sql);
$d_societes=$req->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Processus client</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
     <form action="facture_relance_liste.php" method="POST" id="admin-form">
    <!-- Start: Main -->
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">

            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">

                                        <div class="content-header">
                                            <h2>Relance <b class="text-primary">factures</b></h2>
                                        </div>

                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="section">
                                                    <label for="fac_client" >Société : </label>
                                                        <select name="fac_societe" id="fac_societe" class="select2 select2-societe" >
                                                            <option value="0">Société ...</option>
														<?php	if(!empty($d_societes)){
																	foreach($d_societes as $soc){
																		echo("<option value='" . $soc["soc_id"] . "' >" . $soc["soc_nom"] . "</option>");
																	}
																} ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row pt15">
                                                <div class="col-md-4">
                                                    <div class="section">
                                                        <label for="fac_chrono" >Numéro de chrono</label>
                                                        <input type="text" name="fac_chrono" id="fac_chrono" class="gui-input" placeholder="Numéro de chrono" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="section">
                                                        <label for="fac_client" >Client</label>
                                                        <select name="fac_client" id="fac_client" class="select2-client-n" >
                                                            <option value="0">Client...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="section">
                                                        <label for="cli_code" >Code client</label>
                                                        <input type="text" name="cli_code" id="cli_code" class="gui-input" placeholder="Code Client" />
                                                    </div>
                                                </div>

											</div>
                                            <div class="row pt15">
                                                <div class="col-md-6">
                                                    <div class="section">
                                                        <label for="cli_code" >Nom du contact</label>
                                                        <input type="text" name="rel_con_nom" id="rel_con_nom" class="gui-input" placeholder="Nom du contact" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="section">
                                                        <label for="cli_code" >Prénom du contact</label>
                                                        <input type="text" name="rel_con_prenom" id="rel_con_prenom" class="gui-input" placeholder="Prénom du contact" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row pt15">
                                                <div class="col-md-4">
                                                    <label for="fou_montant_max">Montant TTC</label>
                                                    <div class="section">
                                                        <div class="field prepend-icon">
                                                            <input type="text" name="fac_ttc" id="fac_ttc" class="gui-input input-float" placeholder="Montant TTC" />
                                                            <label for="fac_ttc" class="field-icon">
                                                                <i class="fa fa-euro"></i>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="section">
                                                        <label for="fac_commercial" >Commercial</label>
                                                        <select name="fac_commercial" id="fac_commercial" class="select2" >
                                                            <option value="0">Commercial...</option>
                                                <?php 		if(!empty($d_commercial)){
                                                                foreach($d_commercial as $com){
                                                                    $sql="SELECT age_code FROM Agences WHERE age_id =" . $com['com_agence'];
                                                                    $req = $Conn->query($sql);
                                                                    $age=$req->fetch();
                                                                    echo("<option value='" . $com["com_id"] . "' >" . $com["com_label_1"] . " "  . $com["com_label_2"] . " (" . $age['age_code']. ")</option>");
                                                                }
                                                            } ?>
                                                        </select>
                                                    </div>
                                                </div>

											</div>
                                            <div class="row pt15">
                                                <div class="col-md-6">
													<label for="com_date_deb" >Date d'échéance du :</label>
													<div class="field prepend-icon">
														<input type="text" name="fac_date_reg_prev_deb" id="fac_date_reg_prev_deb" class="gui-input datepicker" placeholder="Depuis le" value="01/01/2014">
														<label for="com_date" class="field-icon">
															<i class="fa fa-calendar" aria-hidden="true"></i>
														</label>
													</div>
												</div>
												<div class="col-md-6">
													<label for="com_date_fin" >Jusqu'au :</label>
													<div class="field prepend-icon">
														<input type="text" name="fac_date_reg_prev_fin" id="fac_date_reg_prev_fin" class="gui-input datepicker" placeholder="Jusqu'au">
														<label for="com_date" class="field-icon">
															<i class="fa fa-calendar" aria-hidden="true"></i>
														</label>
													</div>
												</div>
                                            </div>
                                            <div class="row pt15">
                                                <div class="col-md-6">
                                                    <div class="section">
                                                        <label for="fac_commercial" >Code postal</label>
                                                        <div class="field prepend-icon">
                                                            <input name="cli_cp" id="code_postal" type="text"
                                                            class="gui-input code-postal" placeholder="Code postal">
                                                            <label for="cli_cp" class="field-icon">
                                                                <i class="fa fa-certificate"></i>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mt15">
													<div class="option-group field">
														<label class="option option-system">
															<input type="checkbox" class="com-etat com-etat-0" name="filt_stop" value="on" >
															<span class="checkbox"></span> Relances suspendues
														</label>
													</div>
												</div>
												<div class="col-md-3 mt15">
													<div class="option-group field">
														<label class="option option-warning">
															<input type="checkbox" class="com-etat com-etat-1" name="filt_non_solde" id="filt_non_solde" value="on">
															<span class="checkbox"></span> Factures non-soldées
														</label>
													</div>
												</div>

											</div>



                                            <div class="row mt15">
                                                <div class="col-md-2">
                                                    <div class="section">
                                                        <label class="option">
                                                            <input type="radio" class="type" name="type"  value="0" checked />
                                                            <span class="radio"></span>Normal
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="section">
                                                        <label class="option">
                                                            <input type="radio" class="type" name="type" id="type_select" value="1" />
                                                            <span class="radio"></span>En fonction de l'étape
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-md-2">
                                                    <div class="section">
                                                        <label class="option">
                                                            <input type="radio" class="type" name="type" id="type_select" value="2" />
                                                            <span class="radio"></span>En fonction du retard
                                                        </label>
                                                    </div>
                                                </div> -->
                                                <div class="col-md-6">
                                                    <div class="section">
                                                        <select name="fac_relance" id="fac_relance" class="form-control" >
                                                            <option value="0">Statut de relance...</option>
                                                <?php 		if(!empty($d_statut)){
                                                                foreach($d_statut as $sta){
                                                                    echo("<option value='" . $sta["ret_id"] . "' >" . $sta["ret_libelle"] . "</option>");
                                                                }
                                                            } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                               <!--  <div class="col-md-2">
                                                    <div class="section">
                                                        <label class="option">
                                                            <input type="radio" class="type" name="type" id="" value="2" />
                                                            <span class="radio"></span>En fonction du retard
                                                        </label>
                                                    </div>
                                                </div> -->

                                            </div>
                                            <!-- <div class="row">
                                                <div class="col-md-12">
                                                    <div class="section-divider mb40">
                                                        <span>Contact</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="section">
                                                        <div class="field prepend-icon">
                                                            <input name="con_nom" id="con_nom" type="text" class="gui-input nom" placeholder="Nom" >
                                                            <label for="con_nom" class="field-icon">
                                                                <i class="fa fa-user"></i>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="section">
                                                        <div class="field prepend-icon">
                                                            <input name="con_prenom" type="text" class="gui-input prenom" placeholder="Prénom" >
                                                            <label for="con_prenom" class="field-icon">
                                                                <i class="fa fa-user"></i>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>


                            </div>

                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">

                                        <div class="content-header">
                                            <h2>Clients  à rappeler <b class="text-primary">aujourd'hui</b></h2>
                                        </div>

                                        <div class="col-md-10 col-md-offset-1">
                                            <?php if(!empty($d_rappels)){ ?>
                                            <table class="table">

                                            <?php 	foreach($d_rappels as $d){ ?>
                                                <tr>
                                                    <td rowspan="3" >

                                                    </td>
                                                    <th>Code :</th>
                                                    <td><?= $d['cli_code'] ?></td>
                                                    <th>Nom :</th>
                                                    <td><?= $d['cli_nom'] ?></td>
                                                    <th>Date et heure :</th>
                                                    <td>
                                                    <?php $date=new DateTime($d['rel_date_rappel']); ?>
                                                    <?= $date->format('d/m/Y H:i:s'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Nom :</th>
                                                    <td><?= $d['rel_con_nom'] ?></td>
                                                    <th>Prénom :</th>
                                                    <td><?= $d['rel_con_prenom'] ?></td>
                                                    <th>Tél :</th>
                                                    <td><?= $d['rel_con_tel'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" >
                                                        <?php if(!empty($d['rel_etat_relance'])){ ?>
                                                            <?= $d_statut_libelle[$d['rel_etat_relance']] ?>
                                                        <?php } ?>
                                                        <?=$d['rel_comment']?>
                                                        <hr/>
                                                    </td>
                                                </tr>
                                            <?php 	} ?>
                                            </table>
                                            <?php }else{ ?>
                                                <div class="alert alert-info">
                                                    Aucun client à rappeler aujourd'hui.
                                                </div>
                                            <?php }?>

                                        </div>
                                    </div>


                            </div>

                        </div>
                    </div> -->
                </div>
            </section>
      <!-- End: Content -->
        </section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="parametre.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-search'></i> Rechercher
            </button>
        </div>
    </div>
</footer>
<?php
	include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<script>
$(document).ready(function () {
    $("input[type=radio][name=type]").change(function () {

            actu_statut($(this).val(), $(".select2-societe").val());




    });
    $(".select2-societe").change(function () {
        actu_statut($("#type_select:checked").val(), $(this).val());

    });
});
function actu_statut(statut,societe){
    $("#fac_relance").attr('disabled', 'disabled');
    $(document.body).css({ 'cursor': 'wait' });
    $.ajax({
            type: "get",
            url: "ajax/ajax_nb_relances.php",
            data: "type=" + statut + "&societe=" + societe,
            dataType: "json",
            success: function (data) {
                if (data['0'] == undefined){

                    $("#fac_relance").find("option:gt(0)").remove();

                    }else{
                    $("#fac_relance").find("option:gt(0)").remove();
                    $.each(data, function(key, value) {

                    $('#fac_relance')
                    .append($("<option></option>")
                        .attr("value",value["id"])
                        .text(value["nom"]));
                    });
                    $("#fac_relance").removeAttr('disabled', 'disabled');
                    $(document.body).css({ 'cursor': 'default' });
                }
            },
            error : function(data){
                afficher_message("Erreur","danger",data.responseText);
            }
        });
}
// FONCTION POUR LE SELECT

// FONCTION POUR CHECKBOX
/* function choixEtatReg(elt){
    if(elt.id=="filt_non_solde"){
        if(elt.checked){
            document.getElementById("filt_perdu").checked=false;
        };
    }else if(elt.id=="filt_perdu"){
        if(elt.checked){
            document.getElementById("filt_non_solde").checked=false;
        };
    };
} */
</script>
</form>
</body>
</html>
