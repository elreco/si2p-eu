<?php
include "includes/controle_acces.inc.php";

include("includes/connexion.php");
include("includes/connexion_soc.php");
include("includes/connexion_fct.php");

include("modeles/mod_check_client.php");
include("modeles/mod_check_siret.php");


// PROGRAMME DE TRANSFERT D'UN (suspect_voir) OU PLUSIEUR SUSPECT (suspect_liste)

// DONNEE POUR LE TRAITEMENT

$erreur_txt="";	// message de retour général
$warning_txt="";
$success_txt="";

$erreur_sus="";	// message d'erreur qui bloque le transfert
$erreur_transfert="";	// message d'information si transfert est incomplet

// personne connecté
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}

// rib par defaut de la societe
$sql="SELECT * FROM Rib WHERE rib_societe=" . $acc_societe . " AND rib_defaut = 1;";
$req=$Conn->query($sql);
$d_rib=$req->fetch();
if(empty($d_rib)){
	$d_rib['rib_id'] = 0;
}

// SOCIETES

$sql="SELECT soc_agence FROM Societes WHERE soc_id=" . $acc_societe . " ;";
$req=$Conn->query($sql);
$d_societe=$req->fetch();
if(empty($d_societe)){
	$erreur_txt="Formulaire incomplet. Validation impossible";
}


$items_transfert = array();
if(empty($_POST)){
	$items_transfert[0] = $_GET['id'];
}elseif(!empty($_POST["check_list"])){

	$items_transfert = $_POST["check_list"];
}

$option_verif = 1;
if(!empty($_SESSION['acces']['acc_droits'][36])){
	// FG le 27/04/2020
	// on ne traite que le GET car le non controle du SIRET doit rester exceptionnel => donc pas de POST qui correspond à un transfert groupé depuis la liste
	if(!empty($_GET['option_verif']) ){
		$option_verif = 0;
	}
}

if(empty($erreur_txt)){

	foreach($items_transfert as $i)
	{

		$erreur_sus="";
		$blocage_sus="";

		$sql_sus="SELECT * FROM Suspects WHERE sus_id=" . $i . ";";
		$req_sus=$ConnSoc->query($sql_sus);
		$d_suspect=$req_sus->fetch();
		if(!empty($d_suspect)){

			// CONTROLE DES DONNE OBLIGATOIRE SUR LA FICHE SUSPECT

			// on verifie que le code est libre dans la base clients (le siren et le nom peuvent déjà exister)
			// Pour le siren si le client est blackliste on bloque le transfert
			$result=check_client(0,$d_suspect["sus_code"],"",$d_suspect["sus_siren"]);
			if(!empty($result)){
				if($result["doublon_siren"] AND $result["doublon_siren_blacklist"]){
					$blocage_sus="validation impossible car le siren " . $d_suspect["sus_siren"] . " est blacklisté.";
				}elseif($result["doublon_code"]){
					if(!empty($result["doublon_code_soc"])){
						if(!empty($result["doublon_code_soc"][0]["societe"])){
							$sql="SELECT * FROM Societes WHERE soc_id=" . $result["doublon_code_soc"][0]["societe"] . ";";
							$req=$Conn->query($sql);
							$d_societe=$req->fetch();
						}else{
							$d_societe['soc_nom'] = "";
						}

					}else{
						$d_societe['soc_nom'] = "";
					}

					$erreur_sus="<li>le code " . $d_suspect["sus_code"] . " existe déjà dans la base client " . $d_societe["soc_nom"] . "</li>";
				}
			}

			if(!empty($d_societe['soc_agence']) && empty($d_suspect['sus_agence'])){
				$erreur_sus.="<li>l'agence n'est pas renseignée</li>";
			}

			if(empty($d_suspect['sus_commercial'])){
				$erreur_sus.="<li>le commercial n'est pas renseigné</li>";
			}

			if(empty($d_suspect['sus_reg_type'])){
				$erreur_sus.="<li>le mode de règlement n'est pas renseigné</li>";
			}
			if(empty($d_suspect['sus_reg_formule'])){
				$erreur_sus.="<li>le délai de règlement n'est pas renseigné</li>";
			}

			if($d_suspect["sus_categorie"]!=3){

				if(empty($d_suspect["sus_siren"])){
					// le siren doit etre renseigné pour pouvoir controle les siret mais les particulier n'on pas de siren
					$erreur_sus.="<li>Le siren n'est pas renseigné</li>";
				}
				if(empty($d_suspect["sus_ape"])){
					$erreur_sus.="<li>Le code APE n'est pas renseigné</li>";
				}
				if(empty($d_suspect["sus_classification"])){
					$erreur_sus.="<li>La classification n'est pas renseignée</li>";
				}else{

					if($d_suspect["sus_classification"]==1){
						//ERP
						if(empty($d_suspect["sus_classification_type"]) OR empty($d_suspect["sus_classification_categorie"])){
							$erreur_sus.="<li>La classification est incomplète</li>";
						}

					} else {
						if(empty($d_suspect["sus_classification_categorie"])){
							$erreur_sus.="<li>La classification est incomplète</li>";
						}
					}
				}
			}else{
				if(empty($d_suspect["sus_stagiaire_naiss"])){
					// le siren doit etre renseigné pour pouvoir controle les siret mais les particulier n'on pas de siren
					$erreur_sus.="<li>la date de naissance n'est pas renseignée (obligatoire pour les particuliers)</li>";
				}
				// attention le order by est important -> $d_suspect['contacts'] est utilise sur une boucle
				$sql="SELECT * FROM suspects_Contacts WHERE sco_ref_id=" . $i . " ORDER BY sco_id;";
				$req=$ConnSoc->query($sql);
				$contact_particulier=$req->fetch();
				// VOIR SI il EXISTE DES STAGIAIRES
		        $sql="SELECT DISTINCT * FROM Stagiaires
		        LEFT JOIN Stagiaires_Clients ON (Stagiaires_Clients.scl_stagiaire = Stagiaires.sta_id)
		        LEFT JOIN Clients ON (Clients.cli_id = Stagiaires_Clients.scl_client)
		        WHERE sta_nom=:nom AND sta_prenom=:prenom AND sta_naissance=:naissance AND cli_categorie = 3";
		        $sql.=" ORDER BY sta_id";
		        $req=$Conn->prepare($sql);
		        $req->bindParam(":nom",$contact_particulier['sco_nom']);
		        $req->bindParam(":prenom",$contact_particulier['sco_prenom']);
		        $req->bindParam(":naissance",$d_suspect["sus_stagiaire_naiss"]);
		        $req->execute();
		        $doublons_stagiaires=$req->fetch();
				if(!empty($doublons_stagiaires)){
					$erreur_sus.="<li>Le client particulier existe déjà dans la base de données !</li>";
				}
			}


			// ON CONTROLE LES ADRESSES

			$defaut_ad_int=0;
			if($d_suspect["sus_categorie"]==3){
				$defaut_ad_int=-1;
			}
			$defaut_ad_fac=0;

			$sql="SELECT * FROM suspects_adresses WHERE sad_ref_id=" . $i . ";";
			$req=$ConnSoc->query($sql);
			$d_suspect['adresses']=$req->fetchAll();

			if(!empty($d_suspect['adresses'])){

				foreach($d_suspect['adresses'] as $ad){
					if($ad["sad_type"]==1 AND $ad["sad_defaut"]==1){
						$defaut_ad_int=$ad["sad_id"];
						if(empty($ad["sad_cp"]) OR empty($ad["sad_ville"]) OR (empty($ad["sad_ad1"]) AND empty($ad["sad_ad2"]) AND empty($ad["sad_ad3"])) ){
							$erreur_sus.="<li>l'adresse d'intervention par défaut est incomplète</li>";
						}
					}

					if($ad["sad_type"]==2 AND $ad["sad_defaut"]){
						$defaut_ad_fac=$ad["sad_id"];
						if(empty($ad["sad_cp"]) OR empty($ad["sad_ville"]) OR (empty($ad["sad_ad1"]) AND empty($ad["sad_ad2"]) AND empty($ad["sad_ad3"])) ){
							$erreur_sus.="<li>l'adresse de facturation par défaut est incomplète</li>";
						}
					}

					if($d_suspect["sus_categorie"]!=3 AND $ad["sad_type"]==2){

						if(empty($ad["sad_siret"])){
							$erreur_sus.="<li>Certaines adresses de facturation ne disposent pas de siret</li>";
							/* break; */

						}elseif($option_verif){

							$result=check_siret($d_suspect["sus_siren"],$ad["sad_siret"],0);
							if(!empty($result)){
								$erreur_sus.="<li>le siret " . $d_suspect["sus_siren"] . " " . $ad["sad_siret"] . " existe déjà dans la base client</li>";
								/* break; */
							}
							// VERIF SIRET VIA API



							$siret = $d_suspect["sus_siren"] . $ad["sad_siret"];
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
							// UTILISER LA V3
							curl_setopt($ch,CURLOPT_URL, "https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/" . preg_replace('/\s+/', '', $siret));
							$resultat_json=curl_exec($ch);
							curl_close($ch);
							$result_api=json_decode($resultat_json);
							if(empty($result_api->etablissement)){
								$erreur_sus.="<li>le siret " . $d_suspect["sus_siren"] . " " . $ad["sad_siret"] . " n'est pas reconnu par l'api du gouvernement</li>";
							}
							sleep(1);

						}

					}
				}
				if(empty($defaut_ad_int)){
					$erreur_sus.="<li>La fiche ne dispose pas d'adresse intervention par défaut</li>";
				}
				if(empty($defaut_ad_fac)){
					$erreur_sus.="<li>La fiche ne dispose pas d'adresse de facturation par défaut</li>";
				}
			}else{
				$erreur_sus.="<li>La fiche ne dispose d'aucune adresse</li>";
			}

			// ON CONTROLE LES CONTACTS

			$defaut_con_int=0;
			if($d_suspect["sus_categorie"]==3){
				$defaut_con_int=-1;
			}
			$defaut_con_fac=0;
			$defaut_con_compta=0;

			// attention le order by est important -> $d_suspect['contacts'] est utilise sur une boucle
			$sql="SELECT * FROM suspects_Contacts LEFT JOIN Suspects_Adresses_Contacts ON (suspects_Contacts.sco_id=Suspects_Adresses_Contacts.sac_contact)
			WHERE sco_ref_id=" . $i . " ORDER BY sco_id;";
			$req=$ConnSoc->query($sql);
			$d_suspect['contacts']=$req->fetchAll();
			if(!empty($d_suspect['contacts'])){

				if($d_suspect['sus_categorie'] == 3 AND count($d_suspect['contacts'])>1){
					$erreur_sus.="<li>En tant que particulier, la fiche ne peut pas avoir plus d'un contact</li>";
				}elseif($d_suspect['sus_categorie'] == 3 AND ( empty($d_suspect['contacts'][0]["sco_nom"]) OR  empty($d_suspect['contacts'][0]["sco_prenom"]) ) ){
					$erreur_sus.="<li>En tant que particulier, le nom et prénom du contact par defaut doivent-être renseignés</li>";

				}elseif($d_suspect['sus_categorie'] != 3){
					foreach($d_suspect['contacts'] as $con){
						if($con["sac_adresse"]==$defaut_ad_int AND $con["sac_defaut"]){
							$defaut_con_int=$con["sco_id"];
						}
						if($con["sac_adresse"]==$defaut_ad_fac AND $con["sac_defaut"]){
							$defaut_con_fac=$con["sco_id"];
						}
						if($con["sco_compta"] AND empty($defaut_con_compta)){
							$defaut_con_compta=$con["sco_id"];
						}
					}
					if(empty($defaut_con_int)){
						$erreur_sus.="<li>La fiche ne dispose pas d'un contact d'intervention par défaut</li>";
					}elseif(empty($defaut_con_fac)){
						$erreur_sus.="<li>La fiche ne dispose pas d'un contact de facturation par défaut</li>";
					}elseif(empty($defaut_con_compta)){
						$erreur_sus.="<li>La fiche ne dispose pas d'un contact pour le suivi des impayés</li>";
					}
				}

			}else{
				$erreur_sus.="<li>La fiche ne dispose d'aucun contact</li>";
			}
		}else{
			$erreur_sus.="La fiche N° " . $i . " n'a pas été validée!</li>";
		}

		/***********************************
			CONTROLE OK -> ON PEUT BASCULER LE SUSPECT EN PROSPECT
		************************************/

		$client=0;

		if(!empty($blocage_sus)){

			$erreur_txt.=$d_suspect["sus_code"] . " : " . $blocage_sus . "<br/>";

		}elseif(!empty($erreur_sus)){

			// LE SUSPECT NE PASSE PASSE LES CONTROLES
			$erreur_txt.=$d_suspect["sus_code"] . " : échec de la validation.<ul>" . $erreur_sus . "</ul><br/>";

		}else{

			// CALCUL DE VARIABLE AUTO

			$cli_affacturage=0;
			$cli_affacturable=0;	// ne sont pas affacturable les OPCA / POLE-EMPLOI / INTERCO / Particulier
			if($d_suspect["sus_categorie"]!=3 AND $d_suspect["sus_sous_categorie"]!=5 AND $d_suspect["sus_sous_categorie"]!=6){
				// le parametre etant obligatoire, le client est forecement affacturable et mis en affacturage
				$cli_affacturable=1;
			}

			if($acc_societe==17 AND (empty($d_suspect["sus_agence"]) OR empty($d_suspect["sus_commercial"]) ) ){
				$d_suspect["sus_agence"]=19;
				$d_suspect["sus_commercial"]=4;
			}


			// ENREGISTREMENT EN CLIENT

			$sql="INSERT INTO Clients (
			cli_code,
			cli_nom,
			cli_categorie,
			cli_sous_categorie,
			cli_siren,
			cli_ape,
			cli_classification,
			cli_classification_type,
			cli_classification_categorie,
			cli_opca,
			cli_opca_type,
			cli_facture_opca,
			cli_prescripteur,
			cli_prescripteur_info,
			cli_reg_fdm,
			cli_reg_formule,
			cli_reg_nb_jour,
			cli_reg_type,
			cli_capital,
			cli_soc_type,
			cli_ident_tva,
			cli_immat_lieu,
			cli_releve,
			cli_cor_date,
			cli_cor_rappel,
			cli_date_creation,
			cli_date_suspect,
			cli_soc_suspect,
			cli_import_suspect,
			cli_uti_creation,
			cli_stagiaire_type,
			cli_affacturage,
			cli_affacturable,
			cli_tel
			) VALUES (
			:cli_code,
			:cli_nom,
			:cli_categorie,
			:cli_sous_categorie,
			:cli_siren,
			:cli_ape,
			:cli_classification,
			:cli_classification_type,
			:cli_classification_categorie,
			:cli_opca,
			:cli_opca_type,
			:cli_facture_opca,
			:cli_prescripteur,
			:cli_prescripteur_info,
			:cli_reg_fdm,
			:cli_reg_formule,
			:cli_reg_nb_jour,
			:cli_reg_type,
			:cli_capital,
			:cli_soc_type,
			:cli_ident_tva,
			:cli_immat_lieu,
			:cli_releve,
			:cli_cor_date,
			:cli_cor_rappel,
			NOW(),
			:cli_date_suspect,
			:cli_soc_suspect,
			:cli_import_suspect,
			:cli_uti_creation,
			:cli_stagiaire_type,
			:cli_affacturage,
			:cli_affacturable,
			:cli_tel
			);";
			$req=$Conn->prepare($sql);
			$req->bindValue(":cli_code",$d_suspect["sus_code"]);
			$req->bindValue(":cli_nom",$d_suspect["sus_nom"]);
			$req->bindValue(":cli_categorie",$d_suspect["sus_categorie"]);
			$req->bindValue(":cli_sous_categorie",$d_suspect["sus_sous_categorie"]);
			$req->bindValue(":cli_siren",$d_suspect["sus_siren"]);
			$req->bindValue(":cli_ape",$d_suspect["sus_ape"]);
			$req->bindValue(":cli_classification",$d_suspect["sus_classification"]);
			$req->bindValue(":cli_classification_type",$d_suspect["sus_classification_type"]);
			$req->bindValue(":cli_classification_categorie",$d_suspect["sus_classification_categorie"]);
			$req->bindValue(":cli_opca",$d_suspect["sus_opca"]);
			$req->bindValue(":cli_opca_type",$d_suspect["sus_opca_type"]);
			$req->bindValue(":cli_facture_opca",$d_suspect["sus_facture_opca"]);
			$req->bindValue(":cli_prescripteur",$d_suspect["sus_prescripteur"]);
			$req->bindValue(":cli_prescripteur_info",$d_suspect["sus_prescripteur_info"]);
			$req->bindValue(":cli_reg_fdm",$d_suspect["sus_reg_fdm"]);
			$req->bindValue(":cli_reg_formule",$d_suspect["sus_reg_formule"]);
			$req->bindValue(":cli_reg_nb_jour",$d_suspect["sus_reg_nb_jour"]);
			$req->bindValue(":cli_reg_type",$d_suspect["sus_reg_type"]);
			$req->bindValue(":cli_capital",$d_suspect["sus_capital"]);
			$req->bindValue(":cli_soc_type",$d_suspect["sus_soc_type"]);
			$req->bindValue(":cli_ident_tva",$d_suspect["sus_ident_tva"]);
			$req->bindValue(":cli_immat_lieu",$d_suspect["sus_immat_lieu"]);
			$req->bindValue(":cli_releve",$d_suspect["sus_releve"]);
			$req->bindValue(":cli_cor_date",$d_suspect["sus_cor_date"]);
			$req->bindValue(":cli_cor_rappel",$d_suspect["sus_cor_rappel"]);
			$req->bindValue(":cli_date_suspect",$d_suspect["sus_date_creation"]);	// date de creation du suspect
			$req->bindValue(":cli_soc_suspect",$acc_societe);						// societe a l'origin de la création du suspect
			$req->bindValue(":cli_import_suspect",$d_suspect["sus_import"]);
			$req->bindValue(":cli_uti_creation",$d_suspect["sus_uti_creation"]);
			$req->bindValue(":cli_stagiaire_type",$d_suspect["sus_stagiaire_type"]);
			$req->bindValue(":cli_affacturage",$cli_affacturage);
			$req->bindValue(":cli_affacturable",$cli_affacturable);
			$req->bindValue(":cli_tel",$d_suspect["sus_tel"]);

			try{
				$req->execute();
				$client = $Conn->lastInsertID();
			}Catch(Exception $e){
				$erreur_sus="Problème d'enregistrement client<br/>" . $e->getMessage();
			}


			// LE CLIENT EST CREE SI BUG SUR LES ETAPE SUIVANTES ONT GENERE UN WARNING erreur_transfert
			$erreur_transfert="";

			// ON RENSEIGNE LA TABLE Clients_Societes
			if(empty($erreur_sus)){

				$sql="INSERT INTO Clients_Societes (
				cso_client,
				cso_societe,
				cso_agence,
				cso_commercial,
				cso_utilisateur,
				cso_rib
				) VALUES (
				:cso_client,
				:cso_societe,
				:cso_agence,
				:cso_commercial,
				:cso_utilisateur,
				:cso_rib
				);";
				$req=$Conn->prepare($sql);
				$req->bindParam(":cso_client",$client);
				$req->bindParam(":cso_societe",$acc_societe);
				$req->bindParam(":cso_agence",$d_suspect["sus_agence"]);
				$req->bindParam(":cso_commercial",$d_suspect["sus_commercial"]);
				$req->bindParam(":cso_utilisateur",$d_suspect["sus_utilisateur"]);
				$req->bindParam(":cso_rib",$d_rib['rib_id']);
				try{
					$req->execute();
				}Catch(Exception $e){
					$erreur_transfert.="<li>impossible de valider la société. " . $e->getMessage() . "</li>";
				}
			}

			// ON ENREGISTRE LES ADRESSES

			// donne adresse par defaut a update sur client
			$cli_adresse=0;
			$cli_adr_nom="";
			$cli_adr_service="";
			$cli_adr_ad1="";
			$cli_adr_ad2="";
			$cli_adr_ad3="";
			$cli_adr_cp="";
			$cli_adr_ville="";

			if(empty($erreur_sus) AND empty($erreur_transfert)){

				$conversion_ad=array();
				$sql_ad_cli="INSERT INTO Adresses
				(adr_ad1, adr_ad2, adr_ad3, adr_cp, adr_defaut, adr_geo, adr_libelle, adr_nom, adr_ref,adr_ref_id, adr_service,adr_siret, adr_type, adr_ville)
				VALUES
				(:adr_ad1, :adr_ad2, :adr_ad3, :adr_cp, :adr_defaut, :adr_geo, :adr_libelle, :adr_nom, 1, :adr_ref_id,:adr_service,:adr_siret, :adr_type, :adr_ville)
				";
				$req_ad_cli=$Conn->prepare($sql_ad_cli);

				foreach($d_suspect["adresses"] as $d_adresse){

					$adr_id=0;

					$req_ad_cli->bindParam(":adr_ad1",$d_adresse["sad_ad1"]);
					$req_ad_cli->bindParam(":adr_ad2",$d_adresse["sad_ad2"]);
					$req_ad_cli->bindParam(":adr_ad3",$d_adresse["sad_ad3"]);
					$req_ad_cli->bindParam(":adr_cp",$d_adresse["sad_cp"]);
					$req_ad_cli->bindParam(":adr_defaut",$d_adresse["sad_defaut"]);
					$req_ad_cli->bindParam(":adr_geo",$d_adresse["sad_geo"]);
					$req_ad_cli->bindParam(":adr_libelle",$d_adresse["sad_libelle"]);
					$req_ad_cli->bindParam(":adr_nom",$d_adresse["sad_nom"]);
					$req_ad_cli->bindParam(":adr_ref_id",$client);
					$req_ad_cli->bindParam(":adr_service",$d_adresse["sad_service"]);
					$req_ad_cli->bindParam(":adr_siret",$d_adresse["sad_siret"]);
					$req_ad_cli->bindParam(":adr_type",$d_adresse["sad_type"]);
					$req_ad_cli->bindParam(":adr_ville",$d_adresse["sad_ville"]);
					try{
						$req_ad_cli->execute();
						$adr_id=$Conn->lastInsertId();
						$conversion_ad[$d_adresse["sad_id"]]=$adr_id;
					}Catch(Exception $e){
						$erreur_transfert.="<li>impossible de valider le carnet d'adresses. " . $e->getMessage();
					}

					if(!empty($adr_id)){
						// Adresses par defaut d'intervention ou de facturation si c'est un particulier
						if( $d_adresse["sad_defaut"] AND ($d_adresse["sad_type"]==1 OR ($d_suspect['sus_categorie']==3 AND $d_adresse["sad_type"]==2) ) ){
							$cli_adresse=$adr_id;
							$cli_adr_nom=$d_adresse["sad_nom"];
							$cli_adr_service=$d_adresse["sad_service"];
							$cli_adr_ad1=$d_adresse["sad_ad1"];
							$cli_adr_ad2=$d_adresse["sad_ad2"];
							$cli_adr_ad3=$d_adresse["sad_ad3"];
							$cli_adr_cp=$d_adresse["sad_cp"];
							$cli_adr_ville=$d_adresse["sad_ville"];
						}
					}

				}
			}

			// ON ENREGISTRE LES CONTACTS

			// donnee contact par defaut a update sur client
			$cli_contact=0;
			$cli_con_fct=0;
			$cli_con_fct_nom="";
			$cli_con_mail="";
			$cli_con_nom="";
			$cli_con_prenom="";
			$cli_con_tel="";
			$cli_con_portable="";
			$cli_con_titre=0;


			if(empty($erreur_sus) AND empty($erreur_transfert)){

				$sql_add_con="INSERT INTO Contacts (con_ref, con_ref_id, con_fonction, con_fonction_nom, con_titre, con_nom, con_prenom, con_tel, con_portable, con_fax,con_mail, con_compta, con_comment)
				VALUES (1, :con_ref_id, :con_fonction, :con_fonction_nom, :con_titre, :con_nom, :con_prenom, :con_tel, :con_portable, :con_fax, :con_mail, :con_compta, :con_comment)";
				$req_add_con=$Conn->prepare($sql_add_con);

				$sql_add_ad_con="INSERT INTO Adresses_Contacts (aco_adresse, aco_contact, aco_defaut) VALUES (:aco_adresse, :aco_contact, :aco_defaut)";
				$req_add_ad_con=$Conn->prepare($sql_add_ad_con);

				$bcl_contact=0;

				$conversion_con=array();

				foreach($d_suspect['contacts'] as $d_contact){

					$sco_tel=$d_contact["sco_tel"];
					if($sco_tel!=""){
						if(strlen($sco_tel)==9){
							$sco_tel="0" . $sco_tel;
						}
					}

					$sco_fax=$d_contact["sco_fax"];
					if($sco_fax!=""){
						if(strlen($sco_fax)==9){
							$sco_fax="0" . $sco_fax;
						}
					}

					$sco_portable=$d_contact["sco_portable"];
					if($sco_portable!=""){
						if(strlen($sco_portable)==9){
							$sco_portable="0" . $sco_portable;
						}
					}


					// $d_suspect['contacts'] contient une jointure sur les adresses donc un contact peux etre present 2 fois mais il ne faut le créer une seule fois
					if($bcl_contact!=$d_contact["sco_id"]){

						$bcl_contact=$d_contact["sco_id"];
						$con_id=0;

						$req_add_con->bindParam(":con_ref_id",$client);
						$req_add_con->bindParam(":con_fonction",$d_contact["sco_fonction"]);
						$req_add_con->bindParam(":con_fonction_nom",$d_contact["sco_fonction_nom"]);
						$req_add_con->bindParam(":con_titre",$d_contact["sco_titre"]);
						$req_add_con->bindParam(":con_nom",$d_contact["sco_nom"]);
						$req_add_con->bindParam(":con_prenom",$d_contact["sco_prenom"]);
						$req_add_con->bindParam(":con_tel",$sco_tel);
						$req_add_con->bindParam(":con_portable",$sco_portable);
						$req_add_con->bindParam(":con_fax",$sco_fax);
						$req_add_con->bindParam(":con_mail",$d_contact["sco_mail"]);
						$req_add_con->bindParam(":con_compta",$d_contact["sco_compta"]);
						$req_add_con->bindParam(":con_comment",$d_contact["sco_comment"]);
						try{
							$req_add_con->execute();
							$con_id=$Conn->lastInsertId();
							$conversion_con[$d_contact["sco_id"]]=$con_id;
						}Catch(Exception $e){
							$erreur_transfert.="<li>impossible de valider le carnet de contact. " . $e->getMessage() . "</li>";
						}
					}
					if(!empty($con_id)){

						if(!empty($d_contact["sac_adresse"])){
							// le contact suspect était lié à une adresse
							if(!empty($conversion_ad[$d_contact["sac_adresse"]])){

								$con_adresse=$conversion_ad[$d_contact["sac_adresse"]];

								$req_add_ad_con->bindParam(":aco_adresse",$con_adresse);
								$req_add_ad_con->bindParam(":aco_contact",$con_id);
								$req_add_ad_con->bindParam(":aco_defaut",$d_contact["sac_defaut"]);
								try{
									$req_add_ad_con->execute();
								}Catch(Exception $e){
									$erreur_transfert.="<li>impossible de valider le lien contacts / adresses. " . $e->getMessage() . "</li>";
								}

								// il s'agit du contact par defaut de l'adresse par défaut
								if($d_contact["sac_defaut"] AND $con_adresse=$cli_adresse){
									$cli_contact=$con_id;
									$cli_con_fct=$d_contact["sco_fonction"];
									$cli_con_fct_nom=$d_contact["sco_fonction_nom"];
									$cli_con_mail=$d_contact["sco_mail"];
									$cli_con_nom=$d_contact["sco_nom"];
									$cli_con_prenom=$d_contact["sco_prenom"];
									$cli_con_tel=$sco_tel;
									$cli_con_portable=$sco_portable;
									$cli_con_titre=$d_contact["sco_titre"];
								}
							}
						}

					}
				}
			}

			// ON DUPLIQUE LES INFO SUR CLIENTS_N
			// on le fait après les adresses pour avoir $cli_adr_cp -> :cli_cp
			if(empty($erreur_sus) AND empty($erreur_transfert)){
				$sql="INSERT INTO Clients (
				cli_id,
				cli_code,
				cli_nom,
				cli_agence,
				cli_commercial,
				cli_utilisateur,
				cli_categorie,
				cli_sous_categorie,
				cli_archive,
				cli_groupe,
				cli_filiale_de,
				cli_rib,
				cli_rib_change,
				cli_blackliste,
				cli_cp,
				cli_affacturage,
				cli_affacturable)
				VALUES
				(
				:cli_id,
				:cli_code,
				:cli_nom,
				:cli_agence,
				:cli_commercial,
				:cli_utilisateur,
				:cli_categorie,
				:cli_sous_categorie,
				:cli_archive,
				:cli_groupe,
				:cli_filiale_de,
				:cli_rib,
				:cli_rib_change,
				:cli_blackliste,
				:cli_cp,
				:cli_affacturage,
				:cli_affacturable
				);";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":cli_id",$client);
				$req->bindParam(":cli_code",$d_suspect["sus_code"]);
				$req->bindParam(":cli_nom",$d_suspect["sus_nom"]);
				$req->bindParam(":cli_agence",$d_suspect["sus_agence"]);
				$req->bindParam(":cli_commercial",$d_suspect["sus_commercial"]);
				$req->bindParam(":cli_utilisateur",$d_suspect["sus_utilisateur"]);
				$req->bindParam(":cli_categorie",$d_suspect["sus_categorie"]);
				$req->bindParam(":cli_sous_categorie",$d_suspect["sus_sous_categorie"]);
				$req->bindValue(":cli_archive",0);
				$req->bindValue(":cli_groupe",0);
				$req->bindValue(":cli_filiale_de",0);
				$req->bindParam(":cli_rib",$d_rib['rib_id']);
				$req->bindValue(":cli_rib_change",0);
				$req->bindValue(":cli_blackliste",0);
				$req->bindValue(":cli_cp",$cli_adr_cp);
				$req->bindValue(":cli_affacturage",$cli_affacturage);
				$req->bindValue(":cli_affacturable",$cli_affacturable);
				try{
					$req->execute();
				}Catch(Exception $e){
					$erreur_transfert.="<li>impossible de valider les données agence. " . $e->getMessage() . "</li>";
				}
			}


			// S'IL S'AGIT d'UN PARTICULIER -> on crée sa fiche stagiaire

			$cli_stagiaire=0;

			if(empty($erreur_sus) AND empty($erreur_transfert) AND $d_suspect["sus_categorie"]==3){

				// la partie controle à du verifier que le nom, prenom et naissance était renseigné

				$sql="SELECT sta_id FROM Stagiaires WHERE sta_nom=:nom AND sta_prenom=:prenom AND sta_naissance=:naissance";
				$sql.=" ORDER BY sta_id";
				$req=$Conn->prepare($sql);
				$req->bindParam(":nom",$cli_con_nom);
				$req->bindParam(":prenom",$cli_con_prenom);
				$req->bindParam(":naissance",$d_suspect["sus_stagiaire_naiss"]);
				$req->execute();
				$doublons_stagiaires=$req->fetchAll();
				if(!empty($doublons_stagiaires)){
					if(count($doublons_stagiaires)>1){
						$erreur_transfert.="le stagiaire associé à la fiche n'a pas été créé (risque de doublon)";
					}else{
						$cli_stagiaire=$doublons_stagiaires[0]["sta_id"];
					}
				}else{

					$sql_add_sta="INSERT INTO Stagiaires (sta_nom,sta_prenom,sta_naissance,sta_ville_naiss,sta_cp_naiss,sta_ad1,sta_ad2,sta_ad3,sta_cp,sta_ville,sta_tel,sta_portable,sta_mail_perso,sta_titre
					,sta_client,sta_client_code, sta_client_nom)
					VALUES (:sta_nom,:sta_prenom,:sta_naissance,:sta_ville_naiss,:sta_cp_naiss,:sta_ad1,:sta_ad2,:sta_ad3,:sta_cp,:sta_ville,:sta_tel,:sta_portable,:sta_mail_perso,:sta_titre
					,:sta_client,:sta_client_code, :sta_client_nom)";
					$req_add_sta=$Conn->prepare($sql_add_sta);
					$req_add_sta->bindParam(":sta_nom",$cli_con_nom);
					$req_add_sta->bindParam(":sta_prenom",$cli_con_prenom);
					$req_add_sta->bindParam(":sta_naissance",$d_suspect["sus_stagiaire_naiss"]);
					$req_add_sta->bindParam(":sta_ville_naiss",$d_suspect["sus_sta_naiss_ville"]);
					$req_add_sta->bindParam(":sta_cp_naiss",$d_suspect["sus_sta_naiss_cp"]);
					$req_add_sta->bindParam(":sta_ad1",$cli_adr_ad1);
					$req_add_sta->bindParam(":sta_ad2",$cli_adr_ad2);
					$req_add_sta->bindParam(":sta_ad3",$cli_adr_ad3);
					$req_add_sta->bindParam(":sta_cp",$cli_adr_cp);
					$req_add_sta->bindParam(":sta_ville",$cli_adr_ville);
					$req_add_sta->bindParam(":sta_tel",$cli_con_tel);
					$req_add_sta->bindParam(":sta_portable",$cli_con_portable);
					$req_add_sta->bindParam(":sta_mail_perso",$cli_con_mail);
					$req_add_sta->bindParam(":sta_titre",$cli_con_titre);
					$req_add_sta->bindParam(":sta_client",$client);
					$req_add_sta->bindParam(":sta_client_code",$d_suspect["sus_code"]);
					$req_add_sta->bindParam(":sta_client_nom",$d_suspect["sus_nom"]);
					try{
						$req_add_sta->execute();
						$cli_stagiaire=$Conn->lastInsertId();
					}Catch(Exception $e){
						$erreur_transfert.="<li>Erreur lors de la création de la fiche stagiaire." . $e->getMessage() . "</li>";
					}
				}

				if(!empty($cli_stagiaire)){

					// ON CREE UNE ENTREE Clients_Stagaires

					$sql_add_sta_cli="INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:scl_stagiaire,:scl_client);";
					$req_add_sta_cli=$Conn->prepare($sql_add_sta_cli);
					$req_add_sta_cli->bindParam(":scl_stagiaire",$cli_stagiaire);
					$req_add_sta_cli->bindParam(":scl_client",$client);
					try{
						$req_add_sta_cli->execute();
					}Catch(Exception $e){
						$erreur_transfert.="<li>Erreur lors de l'affectation du client au stagiaire</li>";
					}
					// MAJ DU CLIENT PAR DEFAUT
					$sql="UPDATE Stagiaires SET sta_client = :sta_client,sta_cp_naiss=:sta_cp_naiss, sta_ville_naiss = :sta_ville_naiss, sta_client_code = :sta_client_code, sta_client_nom=:sta_client_nom WHERE sta_id = :sta_id";
					$req=$Conn->prepare($sql);

					$req->bindParam(":sta_client",$client);
					$req->bindParam(":sta_client_code",$d_suspect["sus_code"]);
					$req->bindParam(":sta_client_nom",$d_suspect["sus_nom"]);
					$req->bindParam(":sta_cp_naiss",$d_suspect["sus_sta_naiss_cp"]);
					$req->bindParam(":sta_ville_naiss",$d_suspect["sus_sta_naiss_ville"]);
					$req->bindParam(":sta_id",$cli_stagiaire);
					try{
						$req->execute();
					}Catch(Exception $e){
						$erreur_transfert.="<li>Erreur lors de la mise à jour de la fiche stagiaire." . $e->getMessage() . "</li>";
					}
				}
			}


			// ON MET A JOUR LA TABLE CLIENT AVEC LES DONNEES ADRESSES ET CONTACT

			if(empty($erreur_sus) AND empty($erreur_transfert)){

				$sql="UPDATE Clients SET
				cli_adresse = :cli_adresse,
				cli_adr_nom = :cli_adr_nom,
				cli_adr_service = :cli_adr_service,
				cli_adr_ad1 = :cli_adr_ad1,
				cli_adr_ad2 = :cli_adr_ad2,
				cli_adr_ad3 = :cli_adr_ad3,
				cli_adr_cp = :cli_adr_cp,
				cli_adr_ville = :cli_adr_ville,
				cli_contact = :cli_contact,
				cli_con_fct = :cli_con_fct,
				cli_con_fct_nom = :cli_con_fct_nom,
				cli_con_mail = :cli_con_mail,
				cli_con_nom = :cli_con_nom,
				cli_con_prenom = :cli_con_prenom,
				cli_con_tel = :cli_con_tel,
				cli_con_titre = :cli_con_titre,
				cli_stagiaire = :cli_stagiaire
				WHERE cli_id = :cli_id";
				$req=$Conn->prepare($sql);
				$req->bindParam(":cli_adresse",$cli_adresse);
				$req->bindParam(":cli_adr_nom",$cli_adr_nom);
				$req->bindParam(":cli_adr_service",$cli_adr_service);
				$req->bindParam(":cli_adr_ad1",$cli_adr_ad1);
				$req->bindParam(":cli_adr_ad2",$cli_adr_ad2);
				$req->bindParam(":cli_adr_ad3",$cli_adr_ad3);
				$req->bindParam(":cli_adr_cp",$cli_adr_cp);
				$req->bindParam(":cli_adr_ville",$cli_adr_ville);
				$req->bindParam(":cli_contact",$cli_contact);
				$req->bindParam(":cli_con_fct",$cli_con_fct);
				$req->bindParam(":cli_con_fct_nom",$cli_con_fct_nom);
				$req->bindParam(":cli_con_mail",$cli_con_mail);
				$req->bindParam(":cli_con_nom",$cli_con_nom);
				$req->bindParam(":cli_con_prenom",$cli_con_prenom);
				$req->bindParam(":cli_con_tel",$cli_con_tel);
				$req->bindParam(":cli_con_titre",$cli_con_titre);
				$req->bindParam(":cli_stagiaire",$cli_stagiaire);
				$req->bindParam(":cli_id",$client);
				try{
					$req->execute();
				}Catch(Exception $e){
					$erreur_transfert.="<li>impossible de valider l'adresse et le contact par défaut." . $e->getMessage() . "</li>";
				}
			}


			// ON TRANSFERT LES COORESPONDANCE

			if(empty($erreur_sus) AND empty($erreur_transfert)){

				$sql="SELECT * FROM Suspects_Correspondances WHERE sco_suspect=" . $i . ";";
				$req=$ConnSoc->query($sql);
				$d_correspondances=$req->fetchAll();
				if(!empty($d_correspondances)){

					$sql_add_corresp="INSERT INTO Correspondances
					(cor_client, cor_utilisateur, cor_date, cor_contact, cor_contact_nom, cor_contact_prenom, cor_contact_tel, cor_commentaire, cor_rappeler_le,cor_rappel,cor_type,cor_raison,cor_raison_info)
					VALUES
					(:cor_client, :cor_utilisateur, :cor_date, :cor_contact, :cor_contact_nom, :cor_contact_prenom, :cor_contact_tel, :cor_commentaire, :cor_rappeler_le,:cor_rappel,:cor_type,:cor_raison,:cor_raison_info)
					";
					$req_add_corresp=$Conn->prepare($sql_add_corresp);

					foreach($d_correspondances as $d_correspondance){

						$cor_contact=0;
						if(!empty($d_correspondance["sco_contact"])){
							$cor_contact=$conversion_con[$d_correspondance["sco_contact"]];
						}


						$req_add_corresp->bindParam(":cor_client",$client);
						$req_add_corresp->bindParam(":cor_utilisateur",$d_correspondance["sco_utilisateur"]);
						$req_add_corresp->bindParam(":cor_date",$d_correspondance["sco_date"]);
						$req_add_corresp->bindParam(":cor_contact",$cor_contact);
						$req_add_corresp->bindParam(":cor_contact_nom",$d_correspondance["sco_contact_nom"]);
						$req_add_corresp->bindParam(":cor_contact_prenom",$d_correspondance["sco_contact_prenom"]);
						$req_add_corresp->bindParam(":cor_contact_tel",$d_correspondance["sco_contact_tel"]);
						$req_add_corresp->bindParam(":cor_commentaire",$d_correspondance["sco_commentaire"]);
						$req_add_corresp->bindParam(":cor_rappeler_le",$d_correspondance["sco_rappeler_le"]);
						$req_add_corresp->bindParam(":cor_rappel",$d_correspondance["sco_rappel"]);
						$req_add_corresp->bindParam(":cor_type",$d_correspondance["sco_type"]);
						$req_add_corresp->bindParam(":cor_raison",$d_correspondance["sco_raison"]);
						$req_add_corresp->bindParam(":cor_raison_info",$d_correspondance["sco_raison_info"]);
						try{
							$req_add_corresp->execute();
						}Catch(Exception $e){
							$erreur_transfert.="impossible de valider les correspondances</li>";
						}
					}
				}
			}

			// SI LE TRANSFERT A EU LIEU = CLIENT BIEN CREE

			if(empty($erreur_sus)){

				// Si $erreur_transfert n'est pas vide -> la client a été crée mais certaines données annexes ont planté

				if(!empty($erreur_transfert)){

					// pour cohérence statut,on ne peut pas gerer une validation incomplete
					// on delete tout ce qui a fonctionne dans la base client

					$sql="DELETE FROM Clients WHERE cli_id=:cli_id;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":cli_id",$client);
					try{
						$req->execute();
					}Catch(Exception $e){
						$erreur_transfert.="<li>impossible de dévalider la société. " . $e->getMessage() . "</li>";
					}

					$sql="DELETE FROM Clients_Societes WHERE cso_client=:cso_client;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":cso_client",$client);
					try{
						$req->execute();
					}Catch(Exception $e){
						$erreur_transfert.="<li>impossible de dévalider la société. " . $e->getMessage() . "</li>";
					}


					$sql="DELETE FROM Adresses WHERE adr_ref=1 AND adr_ref_id=:adr_ref_id;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":adr_ref_id",$client);
					try{
						$req->execute();
					}Catch(Exception $e){
						$erreur_transfert.="<li>impossible de dévalider le carnet d'adresses. " . $e->getMessage();
					}

					$sql="DELETE FROM Contacts WHERE con_ref=1 AND con_ref_id=:con_ref_id;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":con_ref_id",$client);
					try{
						$req->execute();
					}Catch(Exception $e){
						$erreur_transfert.="<li>impossible de dévalider le carnet de contact. " . $e->getMessage() . "</li>";
					}

					$sql="DELETE FROM Clients WHERE cli_id=:cli_id;";
					$req=$ConnSoc->prepare($sql);
					$req->bindParam(":cli_id",$client);
					try{
						$req->execute();
					}Catch(Exception $e){
						$erreur_transfert.="<li>impossible de dévalider les données agence. " . $e->getMessage() . "</li>";
					}

					if(!empty($cli_stagiaire)){
						$sql="DELETE FROM Stagiaires WHERE sta_id=:sta_id;";
						$req=$Conn->prepare($sql);
						$req->bindParam(":sta_id",$cli_stagiaire);
						try{
							$req->execute();
						}Catch(Exception $e){
							$erreur_transfert.="<li>impossible de supprimer le stagiaire. " . $e->getMessage() . "</li>";
						}

						$sql="DELETE FROM Stagiaires_Clients WHERE scl_stagiaire=:scl_stagiaire AND scl_client=:scl_client;";
						$req=$Conn->prepare($sql);
						$req->bindParam(":scl_stagiaire",$cli_stagiaire);
						$req->bindParam(":scl_client",$client);
						try{
							$req->execute();
						}Catch(Exception $e){
							$erreur_transfert.="<li>impossible de supprimer le stagiaire. " . $e->getMessage() . "</li>";
						}
					}

					$sql="DELETE FROM Correspondances WHERE cor_client=:cor_client;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":cor_client",$client);
					try{
						$req->execute();
					}Catch(Exception $e){
						$erreur_transfert.="<li>impossible de dévalider les correspondances. " . $e->getMessage() . "</li>";
					}

				}else{

					// SI TOUT S'EST BIEN PASSE JUSQU'A PRESENT ON PEUT SUPPRIMER

					// ON DELETE LE SUSPECT ET TOUTES LES TABLES EN RAPPORT AVEC LE SUSPECT
					$sql = "DELETE FROM Suspects WHERE sus_id=" . $i . ";";
					$req=$ConnSoc->prepare($sql);
					$req->execute();

					// ON DELETE LE SUSPECT ET TOUTES LES TABLES EN RAPPORT AVEC LE SUSPECT
					$sql = "DELETE Suspects_Adresses, Suspects_Adresses_Contacts FROM Suspects_Adresses LEFT JOIN Suspects_Adresses_Contacts ON (Suspects_Adresses.sad_id = Suspects_Adresses_Contacts.sac_adresse)
					WHERE sad_ref_id= " . $i . ";";
					$req=$ConnSoc->prepare($sql);
					$req->execute();

					// ON DELETE LE SUSPECT ET TOUTES LES TABLES EN RAPPORT AVEC LE SUSPECT
					$sql = "DELETE FROM Suspects_Contacts WHERE sco_ref_id=" . $i . ";";
					$req=$ConnSoc->prepare($sql);
					$req->execute();

					// ON DELETE LE SUSPECT ET TOUTES LES TABLES EN RAPPORT AVEC LE SUSPECT
					$sql = "DELETE FROM Suspects_Correspondances WHERE sco_suspect=" . $i . ";";
					$req=$ConnSoc->prepare($sql);
					$req->execute();

				}

			}

			/************************************
				FIN DU TRANSFERT
			*************************************/

			if(!empty($erreur_sus)){
				$erreur_txt.=$d_suspect["sus_code"] . " : échec de la validation.<ul>" . $erreur_sus . "</ul>";
			}elseif(!empty($erreur_transfert)){
				$erreur_txt.=$d_suspect["sus_code"] . " : échec de la validation.<ul>" . $erreur_transfert . "</ul>";
			}
		}
		//FIN DE TENTATIVE DE TRANSFERT
	}
	// FIN DE BOUCLE SUR LE SUSPECT
}

/* GESTION DES MESSAGE DE RETOUR POUR L'UTI */

if(!empty($erreur_txt)){
	// le transfert n'a pas eu lieu le suspect est toujours dans la base
	$_SESSION['message'] = array(
		"aff" =>"modal",
		"type" => "danger",
		"message" => $erreur_txt
	);
}elseif(!empty($_GET)){

	$_SESSION['message'][] = array(
		"titre" =>"Fiche validée",
		"type" => "success",
		"message" => "La fiche " . $d_suspect["sus_code"] . " a bien été validée en tant que prospect."
	);

}else{
	$_SESSION['message'][] = array(
		"titre" =>"Fiches validées",
		"type" => "success",
		"message" => "Toutes les fiches ont bien été validées en tant que prospect."
	);
}

if(empty($erreur_txt)){
	// transfert ok
	if(!empty($_GET)){
		// 1 suspect -> on visulaise la fiche
		$retour = "client_voir.php?client=" . $client;
	}else{
		$retour = "suspect_liste.php";
	}
}elseif(!empty($_GET)){
	$retour = "suspect_voir.php?suspect=" . $_GET["id"];
}else{
	$retour = $_SESSION["retour"];
}
Header("Location: " . $retour);
die();
