<?php 
	include "includes/controle_acces.inc.php";

/* 	LISTE LES PROSPECTS
	= suspect avec correspondance
	= client sans facture

	NOTE

	$_SESSION['acces']["acc_droits"][37] : ne pas accorder ce droit à un user sans mon accord (FG)


*/

	include('includes/connexion.php');
	include('includes/connexion_soc.php');
	include('modeles/mod_orion_cli_categories.php');
	include('modeles/mod_orion_cli_sous_categories.php');


	// DONNEE UTILE AU PROGRAMME

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}
	$_SESSION['retour'] = "prospect_liste.php";

	$d_client_categorie=orion_cli_categories();
	$d_client_sous_categorie=orion_cli_sous_categories();

	if (isset($_POST['search'])){
		
		// le formulaire a été soumit
		// on actualise les criteres de recherche
		
		$_SESSION["prospect_tri"] = array();
		
		// critere sur le client
		$_SESSION["prospect_tri"]["cli_categorie"]=0;
		if(!empty($_POST['cli_categorie'])){
			$_SESSION["prospect_tri"]["cli_categorie"]=intval($_POST['cli_categorie']);
		}

		$_SESSION["prospect_tri"]["cli_sous_categorie"]=0;
		if(!empty($_POST['cli_sous_categorie'])){
			$_SESSION["prospect_tri"]["cli_sous_categorie"]=intval($_POST['cli_sous_categorie']);
		}
		
		$_SESSION["prospect_tri"]["cli_id"]=0;
		if(!empty($_POST['cli_id'])){
			$_SESSION["prospect_tri"]["cli_id"]=intval($_POST['cli_id']);
		}
		
		$_SESSION["prospect_tri"]["cli_groupe"]=0;
		if(!empty($_POST['cli_groupe'])){
			$_SESSION["prospect_tri"]["cli_groupe"]=intval($_POST['cli_groupe']); // 1 mm 2 groupe 3 entreprise
		}
		
		$_SESSION["prospect_tri"]["cli_code"]="";
		if(!empty($_POST['cli_code'])){
			$_SESSION["prospect_tri"]["cli_code"]=$_POST['cli_code'];
		}
		
		$_SESSION["prospect_tri"]["cli_nom"]="";
		if(!empty($_POST['cli_nom'])){
			$_SESSION["prospect_tri"]["cli_nom"]=$_POST['cli_nom'];
		}
		$_SESSION["prospect_tri"]["cli_reference"]="";
		if(!empty($_POST['cli_reference'])){
			$_SESSION["prospect_tri"]["cli_reference"]=$_POST['cli_reference'];
		}
		$_SESSION["prospect_tri"]["cli_commercial"]=0;
		if(!empty($_POST['cli_commercial'])){
			$_SESSION["prospect_tri"]["cli_commercial"]=intval($_POST['cli_commercial']);
		}
		
		$_SESSION["prospect_tri"]["cli_creation_date_deb"]="";
		if(!empty($_POST["cli_creation_date_deb"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["cli_creation_date_deb"]);
			if(!is_bool($DT_periode)){
				$_SESSION["prospect_tri"]["cli_creation_date_deb"]=$DT_periode->format("Y-m-d");
			}
		}
		
		$_SESSION["prospect_tri"]["cli_creation_date_fin"]="";
		if(!empty($_POST["cli_creation_date_fin"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["cli_creation_date_fin"]);
			if(!is_bool($DT_periode)){
				$_SESSION["prospect_tri"]["cli_creation_date_fin"]=$DT_periode->format("Y-m-d");
			}
		}
		
		$_SESSION["prospect_tri"]["cli_prescripteur"]=0;
		if(!empty($_POST['cli_prescripteur'])){
			$_SESSION["prospect_tri"]["cli_prescripteur"]=intval($_POST['cli_prescripteur']);
		}
		
		$_SESSION["prospect_tri"]["cli_ape"]=0;
		if(!empty($_POST['cli_ape'])){
			$_SESSION["prospect_tri"]["cli_ape"]=intval($_POST['cli_ape']);
		}
		$_SESSION["prospect_tri"]["cli_classification"]=0;
		if(!empty($_POST['cli_classification'])){
			$_SESSION["prospect_tri"]["cli_classification"]=intval($_POST['cli_classification']);
		}
		
		$_SESSION["prospect_tri"]["cli_classification_type"]=0;
		if(!empty($_POST['cli_classification_type'])){
			$_SESSION["prospect_tri"]["cli_classification_type"]=intval($_POST['cli_classification_type']);
		}
		
		$_SESSION["prospect_tri"]["cli_classification_categorie"]=0;
		if(!empty($_POST['cli_classification_categorie'])){
			$_SESSION["prospect_tri"]["cli_classification_categorie"]=intval($_POST['cli_classification_categorie']);
		}
		
		
		$_SESSION["prospect_tri"]["cli_siren"]="";
		$_SESSION["prospect_tri"]["cli_nic"]="";
		if(!empty($_POST['cli_siren'])){
			$_SESSION["prospect_tri"]["cli_siren"]=$_POST['cli_siren'];
			if(!empty($_POST['cli_nic']) AND strlen($_SESSION["prospect_tri"]["cli_siren"])==11){
				$_SESSION["prospect_tri"]["cli_nic"]=$_POST['cli_nic']; 
			}
		}
		
		// adresse
		
		$_SESSION["prospect_tri"]["adr_type"]=0;
		if(!empty($_SESSION["prospect_tri"]["cli_nic"])){
			$_SESSION["prospect_tri"]["adr_type"]=2; // facturation
		}elseif(!empty($_POST['adr_type'])){
			$_SESSION["prospect_tri"]["adr_type"]=intval($_POST['adr_type']);
		}
		$_SESSION["prospect_tri"]["adr_defaut"]=0;
		if(isset($_POST['adr_defaut'])){
			$_SESSION["prospect_tri"]["adr_defaut"]=1;
		}
		$_SESSION["prospect_tri"]["adr_cp"]="";
		if(!empty($_POST['adr_cp'])){
			$_SESSION["prospect_tri"]["adr_cp"]=$_POST['adr_cp'];
		}
		$_SESSION["prospect_tri"]["adr_ville"]="";
		if(!empty($_POST['adr_ville'])){
			$_SESSION["prospect_tri"]["adr_ville"]=$_POST['adr_ville'];
		}

		// contact
		
		$_SESSION["prospect_tri"]["con_nom"]="";
		if(!empty($_POST['con_nom'])){
			$_SESSION["prospect_tri"]["con_nom"]=$_POST['con_nom'];
		}
		
		$_SESSION["prospect_tri"]["con_prenom"]="";
		if(!empty($_POST['con_prenom'])){
			$_SESSION["prospect_tri"]["con_prenom"]=$_POST['con_prenom'];
		}
		
		$_SESSION["prospect_tri"]["con_tel"]="";
		if(!empty($_POST['con_tel'])){
			$_SESSION["prospect_tri"]["con_tel"]=$_POST['con_tel'];
		}
		
		$_SESSION["prospect_tri"]["con_mail"]=0; // 1 renseigne 2 non renseigné
		if(!empty($_POST['con_mail'])){
			$_SESSION["prospect_tri"]["con_mail"]=intval($_POST['con_mail']);
		}
		$_SESSION["prospect_tri"]["con_fonction"]=0;
		if(!empty($_POST['con_fonction'])){
			$_SESSION["prospect_tri"]["con_fonction"]=intval($_POST['con_fonction']);
		}
		
		
		// correspondance
		
		$_SESSION["prospect_tri"]["cor_rappel_deb"]="";
		if(!empty($_POST["cor_rappel_deb"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["cor_rappel_deb"]);
			if(!is_bool($DT_periode)){
				$_SESSION["prospect_tri"]["cor_rappel_deb"]=$DT_periode->format("Y-m-d");
			}
		}
		
		$_SESSION["prospect_tri"]["cor_rappel_fin"]="";
		if(!empty($_POST["cor_rappel_fin"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["cor_rappel_fin"]);
			if(!is_bool($DT_periode)){
				$_SESSION["prospect_tri"]["cor_rappel_fin"]=$DT_periode->format("Y-m-d");
			}
		}
		
		$_SESSION["prospect_tri"]["cli_correspondance"]=0;
		if(!empty($_POST['cli_correspondance'])){
			$_SESSION["prospect_tri"]["cli_correspondance"]=intval($_POST['cli_correspondance']); // 1 avec //2 sans
		}
		
		$_SESSION["prospect_tri"]["cli_depuis"]="";
		if(!empty($_POST["cli_depuis"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["cli_depuis"]);
			if(!is_bool($DT_periode)){
				$_SESSION["prospect_tri"]["cli_depuis"]=$DT_periode->format("Y-m-d");
			}
		}
		
		
		$_SESSION["prospect_tri"]["aff_cor"]=0;
		if(!empty($_POST['aff_cor'])){
			$_SESSION["prospect_tri"]["aff_cor"]=intval($_POST['aff_cor']);
		}

		// autre info
		
		$_SESSION["prospect_tri"]["cli_important"]=0;
		if(isset($_POST['cli_important'])){
			$_SESSION["prospect_tri"]["cli_important"]=1;
		}
		
		$_SESSION["prospect_tri"]["cli_archive"]=0;
		if(isset($_POST['cli_archive'])){
			$_SESSION["prospect_tri"]["cli_archive"]=1;
		}

		$_SESSION["prospect_tri"]["cli_import"]=0;
		if(!empty($_POST['cli_import'])){
			$_SESSION["prospect_tri"]["cli_import"]=intval($_POST['cli_import']);		
		}
		
		$_SESSION["prospect_tri"]["cli_reseau"]=0;
		if($_SESSION['acces']["acc_profil"]==13){
			if(isset($_POST['cli_reseau'])){
				if(!empty($_POST['cli_reseau'])){
					$_SESSION["prospect_tri"]["cli_reseau"]=1;
				}
			}
		}
	}

	$rechercheParAdresse=false;
	$rechercheParContact=false;
	$rechercheParCorrespondance=false;
	
	// ON CONSTRUIT LES REQUETES
	
	$critere_cli=array();	// critere reserve client
	$critere_sus=array();	// critere reserve suspect
	$critere=array();		// crière commun
	$mil_cli="";
	$mil_sus="";
	
	if(!empty($_SESSION["prospect_tri"]['cli_id'])){
		$mil_cli.=" AND cli_id =:cli_id"; 
		$mil_sus.=" AND sus_id=:cli_id"; 
		$critere["cli_id"]=$_SESSION["prospect_tri"]['cli_id'];
	}else{
		
		if(!empty($_SESSION["prospect_tri"]['cli_categorie'])){
			$mil_sus.=" AND sus_categorie = :cli_categorie"; 
			$mil_cli.=" AND cli_categorie = :cli_categorie"; 
			$critere["cli_categorie"]=$_SESSION["prospect_tri"]['cli_categorie'];
		};
		if(!empty($_SESSION["prospect_tri"]['cli_sous_categorie'])){
			$mil_sus.=" AND sus_sous_categorie = :cli_sous_categorie";
			$mil_cli.=" AND cli_sous_categorie = :cli_sous_categorie";
			$critere["cli_sous_categorie"]=$_SESSION["prospect_tri"]['cli_sous_categorie'];
		};
		
		if(!empty($_SESSION["prospect_tri"]['cli_groupe'])){
			if($_SESSION["prospect_tri"]['cli_groupe']==1){			
				$mil_cli.=" AND cli_groupe = 1 AND cli_filiale_de = 0"; 
			}else if($_SESSION["prospect_tri"]['cli_groupe']==2){
				$mil_cli.=" AND cli_groupe != 0"; 
			}else if($_SESSION["prospect_tri"]['cli_groupe']==3){
				$mil_cli.=" AND cli_groupe = 0"; 
			}
		};
	
		if(!empty($_SESSION["prospect_tri"]['cli_code'])){
			$mil_cli.=" AND cli_code LIKE :cli_code"; 
			$mil_sus.=" AND sus_code LIKE :cli_code"; 
			$critere["cli_code"]="%" . $_SESSION["prospect_tri"]['cli_code']."%";
		};
		if(!empty($_SESSION["prospect_tri"]['cli_nom'])){
			$mil_cli.=" AND cli_nom LIKE :cli_nom"; 
			$mil_sus.=" AND sus_nom LIKE :cli_nom"; 
			$critere["cli_nom"]="%" . $_SESSION["prospect_tri"]['cli_nom'] ."%";
		};
		
		if(!empty($_SESSION["prospect_tri"]['cli_reference'])){
			$mil_cli.=" AND cli_reference LIKE :cli_reference"; 
			$critere_cli["cli_reference"]="%" . $_SESSION["prospect_tri"]['cli_reference'] . "%";
		};
		
		if(!empty($_SESSION["prospect_tri"]['cli_commercial'])){
			$mil_cli.=" AND cso_commercial =:cli_commercial";
			$mil_sus.=" AND sus_commercial =:cli_commercial";
			$critere["cli_commercial"]=$_SESSION["prospect_tri"]['cli_commercial'];
		};
		
		
		if(!empty($_SESSION["prospect_tri"]['cli_creation_date_deb'])){
			$mil_cli.=" AND ( (ISNULL(cli_date_suspect) AND cli_date_creation >=:cli_creation_date_deb) OR cli_date_suspect >=:cli_creation_date_deb)"; 
			$mil_sus.=" AND sus_date_creation >=:cli_creation_date_deb"; 
			$critere["cli_creation_date_deb"]=$_SESSION["prospect_tri"]['cli_creation_date_deb'];
		};
		if(!empty($_SESSION["prospect_tri"]['cli_creation_date_fin'])){
			$mil_cli.=" AND ( (ISNULL(cli_date_suspect) AND cli_date_creation <=:cli_creation_date_fin) OR cli_date_suspect <=:cli_creation_date_fin)"; 
			$mil_sus.=" AND sus_date_creation <=:cli_creation_date_fin"; 
			$critere["cli_creation_date_fin"]=$_SESSION["prospect_tri"]['cli_creation_date_fin'];
		};
		
		if(!empty($_SESSION["prospect_tri"]['cli_prescripteur'])){
			$mil_cli.=" AND cli_prescripteur =:cli_prescripteur"; 
			$mil_sus.=" AND sus_prescripteur =:cli_prescripteur"; 
			$critere["cli_prescripteur"]=$_SESSION["prospect_tri"]['cli_prescripteur'];
		};
		if(!empty($_SESSION["prospect_tri"]['cli_ape'])){
			$mil_cli.=" AND cli_ape =:cli_ape"; 
			$mil_sus.=" AND sus_ape =:cli_ape"; 
			$critere["cli_ape"]=$_SESSION["prospect_tri"]['cli_ape'];
		};
		if(!empty($_SESSION["prospect_tri"]['cli_classification'])){
			$mil_cli.=" AND cli_classification =:cli_classification"; 
			$mil_sus.=" AND sus_classification =:cli_classification"; 
			$critere["cli_classification"]=$_SESSION["prospect_tri"]['cli_classification'];
		};
		if(!empty($_SESSION["prospect_tri"]['cli_classification_categorie'])){
			$mil_cli.=" AND cli_classification_categorie =:cli_classification_categorie"; 
			$mil_sus.=" AND sus_classification_categorie =:cli_classification_categorie"; 
			$critere["cli_classification_categorie"]=$_SESSION["prospect_tri"]['cli_classification_categorie'];
		};
		if(!empty($_SESSION["prospect_tri"]['cli_classification_type'])){
			$mil_cli.=" AND cli_classification_type =:cli_classification_type"; 
			$mil_sus.=" AND sus_classification_type =:cli_classification_type"; 
			$critere["cli_classification_type"]=$_SESSION["prospect_tri"]['cli_classification_type'];
		};

		if(!empty($_SESSION["prospect_tri"]['cli_siren'])){
			$mil_cli.=" AND cli_siren=:cli_siren"; 
			$mil_sus.=" AND sus_siren=:cli_siren"; 
			$critere["cli_siren"]=$_SESSION["prospect_tri"]['cli_siren'];
		};
		
		if(!empty($_SESSION["prospect_tri"]['cli_nic'])){
			$rechercheParAdresse=true;
			$mil_cli.=" AND adr_siret=:cli_nic"; 
			$mil_sus.=" AND sad_siret=:cli_nic"; 
			$critere["cli_nic"]=$_SESSION["prospect_tri"]['cli_nic'];
		};
		
		// critère sur les adresses	
		
		if($_SESSION["prospect_tri"]['adr_type']==1 AND $_SESSION["prospect_tri"]['adr_defaut']==1){			
			// revient a cherche l'adresse par defaut enregistré directement sur l'entité
			// adr_siret adr_type a ete force a 2 si condition vrai adr_siret=""
			if(!empty($_SESSION["prospect_tri"]['adr_cp'])){
				$mil_cli.=" AND cli_adr_cp LIKE :adr_cp"; 
				$mil_sus.=" AND sus_adr_cp LIKE :adr_cp"; 
				$critere["adr_cp"]="%" . $_SESSION["prospect_tri"]['adr_cp'] ."%";
			};
			if(!empty($_SESSION["prospect_tri"]['adr_ville'])){
				$mil_cli.=" AND cli_adr_ville LIKE :adr_ville"; 
				$mil_sus.=" AND sus_adr_ville LIKE :adr_ville"; 
				$critere["adr_ville"]="%" . $_SESSION["prospect_tri"]['adr_ville'] ."%";
			};
			
		}else{
			
			if(!empty($_SESSION["prospect_tri"]['adr_type'])){
				$mil_cli.=" AND adr_type=:adr_type"; 
				$mil_sus.=" AND sad_type=:adr_type" ; 
				$critere["adr_type"]=$_SESSION["prospect_tri"]['adr_type'];
				$rechercheParAdresse=true;
			};
			if(!empty($_SESSION["prospect_tri"]['adr_siret'])){
				$mil_cli.=" AND adr_siret LIKE :adr_siret"; 
				$mil_sus.=" AND sad_siret LIKE :adr_siret"; 
				$critere["adr_siret"]=$_SESSION["prospect_tri"]['adr_siret'];
				$rechercheParAdresse=true;
			};
			if($_SESSION["prospect_tri"]['adr_defaut']==1){
				$mil_cli.=" AND adr_defaut=1";
				$mil_sus.=" AND sad_defaut=1";
				$rechercheParAdresse=true;
			};
			if(!empty($_SESSION["prospect_tri"]['adr_cp'])){
				$mil_cli.=" AND adr_cp LIKE :adr_cp"; 
				$mil_sus.=" AND sad_cp LIKE :adr_cp";
				$critere["adr_cp"]="%" . $_SESSION["prospect_tri"]['adr_cp'] ."%";
				$rechercheParAdresse=true;
			};
			if(!empty($_SESSION["prospect_tri"]['adr_ville'])){
				$mil_cli.=" AND adr_ville LIKE :adr_ville"; 
				$mil_sus.=" AND sad_ville LIKE :adr_ville"; 
				$critere["adr_ville"]="%" . $_SESSION["prospect_tri"]['adr_ville'] ."%";
				$rechercheParAdresse=true;
			};
		}
		
		// critère sur les contacts
		
		if(!empty($_SESSION["prospect_tri"]['con_nom'])){
			$mil_cli.=" AND con_nom LIKE :con_nom"; 
			$mil_sus.=" AND sco_nom LIKE :con_nom"; 
			$critere["con_nom"]="%" . $_SESSION["prospect_tri"]['con_nom'] ."%";
			$rechercheParContact=true;
		};
		if(!empty($_SESSION["prospect_tri"]['con_prenom'])){
			$mil_cli.=" AND con_prenom LIKE :con_prenom"; 
			$mil_sus.=" AND sco_prenom LIKE :con_prenom"; 
			$critere["con_prenom"]="%" . $_SESSION["prospect_tri"]['con_prenom'] ."%";
			$rechercheParContact=true;
		};

		if(!empty($_SESSION["prospect_tri"]['con_mail'])){
			if($_SESSION["prospect_tri"]['con_mail'] == 1){
				$mil_cli.=" AND con_mail IS NOT NULL AND con_mail!=''"; 
				$mil_sus.=" AND sco_mail IS NOT NULL AND sco_mail!=''";
			}else{
				$mil_cli.=" AND (con_mail IS NULL OR con_mail='')"; 
				$mil_sus.=" AND (sco_mail IS NULL OR sco_mail='')"; 
			}
			$rechercheParContact=true;
		};
		if(!empty($_SESSION["prospect_tri"]['con_fonction'])){
			$mil_cli.=" AND con_fonction =:con_fonction"; 
			$mil_sus.=" AND sco_fonction =:con_fonction"; 
			$critere["con_fonction"]=$_SESSION["prospect_tri"]['con_fonction'];
			$rechercheParContact=true;
		};

		if(!empty($_SESSION["prospect_tri"]['con_tel'])){
			$mil_cli.=" AND (con_tel =:con_tel OR con_portable =:con_tel OR con_fax =:con_tel)"; 
			$mil_sus.=" AND (sco_tel =:con_tel OR sco_portable =:con_tel OR sco_fax =:con_tel)"; 
			$critere["con_tel"]=$_SESSION["prospect_tri"]['con_tel'];
			$rechercheParContact=true;
		};

		// sur les correspondances
		
		if(!empty($_SESSION["prospect_tri"]['cor_rappel_deb'])){
			$mil_cli.=" AND NOT cor_rappel AND cor_rappeler_le>=:cor_rappel_deb"; 
			$mil_sus.=" AND NOT sco_rappel AND sco_rappeler_le>=:cor_rappel_deb"; 
			$critere["cor_rappel_deb"]=$_SESSION["prospect_tri"]['cor_rappel_deb'];
			$rechercheParCorrespondance=true;
		};
		if(!empty($_SESSION["prospect_tri"]['cor_rappel_fin'])){
			$mil_cli.=" AND NOT cor_rappel AND cor_rappeler_le<=:cor_rappel_fin"; 
			$mil_sus.=" AND NOT sco_rappel AND sco_rappeler_le<=:cor_rappel_fin";
			$critere["cor_rappel_fin"]=$_SESSION["prospect_tri"]['cor_rappel_fin'];
			$rechercheParCorrespondance=true;
		};
		if(!empty($_SESSION["prospect_tri"]['cli_correspondance'])){
			if($_SESSION["prospect_tri"]['cli_correspondance']>0){
				$rechercheParCorrespondance=true;
				if($_SESSION["prospect_tri"]['cli_correspondance']==1){				
						// avec
					if(!empty($_SESSION["prospect_tri"]['cli_depuis'])){
						$mil_cli.=" AND cor_date>=:cli_depuis";
						$mil_sus.=" AND sco_date>=:cli_depuis";
						$critere["cli_depuis"]=$_SESSION["prospect_tri"]['cli_depuis'];
					}else{
						$mil_cli.=" AND NOT ISNUll(cor_date)";
						$mil_sus.=" AND NOT ISNUll(sco_date)";
					}
				}else{
						// sans
					$mil_cli.=" AND ( ISNUll(cli_cor_date)";
					$mil_sus.=" AND ( ISNUll(sus_cor_date)";
					if(!empty($_SESSION["prospect_tri"]['cli_depuis'])){
						$mil_cli.=" OR cli_cor_date<=:cli_depuis";
						$mil_sus.=" OR sus_cor_date<=:cli_depuis";
						$critere["cli_depuis"]=$_SESSION["prospect_tri"]['cli_depuis'];
					}
					$mil_cli.=" )";
					$mil_sus.=" )";
				}
			}
		}
		
		// autre infos
		
		if($_SESSION["prospect_tri"]['cli_important'] == 1){
			$mil_cli.=" AND cli_important = 1"; 
		};
		
		if($_SESSION["prospect_tri"]['cli_archive'] == 1){
			$mil_cli.=" AND cso_archive = 1"; 
		}else{
			$mil_cli.=" AND cso_archive = 0"; 
		};
		
		if(!empty($_SESSION["prospect_tri"]['cli_import'])){
			$mil_cli.=" AND cli_import_suspect=:cli_import AND cli_soc_suspect=" . $acc_societe; 
			$mil_sus.=" AND sus_import=:cli_import"; 
			$critere["cli_import"]=$_SESSION["prospect_tri"]['cli_import'];
		};
	}
	
	// critère auto
	
	if($_SESSION["prospect_tri"]['cli_reseau']!==1){
		$mil_cli.= " AND cso_societe = " . $acc_societe;
		if(!empty($acc_agence)){
			$mil_cli.= " AND cso_agence = " . $acc_agence;
			$mil_sus.= " AND sus_agence = " . $acc_agence;
		}
		if(!$_SESSION['acces']["acc_droits"][6]){
			$mil_cli.=" AND cso_utilisateur=" . $acc_utilisateur;
			$mil_sus.=" AND sus_utilisateur=" . $acc_utilisateur;
		}
	}
	$mil_cli.=" AND (cli_first_facture=0 OR ISNULL(cli_first_facture) )";
	$mil_sus.=" AND NOT ISNULL(sus_cor_date)";
	
	// CONSTRUCTION DE LA REQUETES CLIENTS

	// champ a selectionne
	$sql_cli="SELECT DISTINCT cli_code AS ent_code,cli_nom AS ent_nom,cli_id AS ent_id,cli_groupe,cli_filiale_de,cli_categorie as ent_categorie,cli_sous_categorie AS ent_sous_categorie,cli_blackliste,cli_first_facture,cli_siren as ent_siren,2 as ent_src";
	if($rechercheParAdresse){
		$sql_cli.=",adr_id,adr_type,adr_service AS ad_service,adr_ad1 AS ad1, adr_ad2 AS ad2, adr_ad3 AS ad3 ,adr_cp AS ad_cp ,adr_ville AS ad_ville";
	}else{
		$sql_cli.=",cli_adr_service AS ad_service,cli_adr_ad1 AS ad1, cli_adr_ad2 AS ad2, cli_adr_ad3 AS ad3, cli_adr_cp AS ad_cp, cli_adr_ville AS ad_ville";
	}
	if($rechercheParContact){
		$sql_cli.=",con_id,con_prenom AS prenom, con_nom AS nom, con_fonction AS fonction, con_fonction_nom AS fonction_nom ,con_tel AS tel, con_mail AS mail";
	}else{
		$sql_cli.=",cli_con_prenom AS prenom, cli_con_nom AS nom, cli_con_fct AS fonction, cli_con_fct_nom AS fonction_nom, cli_con_tel AS tel, cli_con_mail AS mail";
	}
	if($_SESSION["prospect_tri"]['aff_cor']==1){
		$sql_cli.=",cli_cor_date AS corresp_date";
	}else{
		$sql_cli.=",cli_cor_rappel AS corresp_date";
	}
	if($_SESSION["prospect_tri"]['cli_reseau']==1){
		$sql_cli.=",cso_societe";
	}
	// table
	$sql_cli.=" FROM Clients";
	$sql_cli.=" LEFT JOIN Clients_Societes ON (Clients_Societes.cso_client = Clients.cli_id)";
	if($rechercheParAdresse){
		$sql_cli.=" LEFT JOIN Adresses ON (Clients.cli_id = Adresses.adr_ref_id)";
	}
	if($rechercheParContact){
		$sql_cli.=" LEFT JOIN Contacts ON (Clients.cli_id = Contacts.con_ref_id)";
	}
	if($rechercheParCorrespondance){
		$sql_cli.=" LEFT JOIN Correspondances ON (Clients.cli_id = Correspondances.cor_client)";
	}
	if($mil_cli!=""){
		$sql_cli.=" WHERE " . substr($mil_cli, 5, strlen($mil_cli)-5);
	}
	$sql_cli.=" ORDER BY cli_code,cli_nom";
	//var_dump($sql_cli);
	//echo("<br/>");
	$req = $Conn->prepare($sql_cli);	
	if(!empty($critere)){
		foreach($critere as $c => $u){
			//echo($c . ":" . $u . "<br/>");
			$req->bindValue($c,$u);
		}
	};
	if(!empty($critere_cli)){
		foreach($critere_cli as $c => $u){
			//echo($c . ":" . $u . "<br/>");
			$req->bindValue($c,$u);
		}
	};
	$req->execute();
	$clients = $req->fetchAll(PDO::FETCH_ASSOC);

	
	// CONSTRUCTION DE LA REQUETE SUSPECT

	// champ a selectionne
	$sql_sus="SELECT DISTINCT sus_code AS ent_code,sus_nom AS ent_nom,sus_id AS ent_id,sus_categorie AS ent_categorie,sus_sous_categorie AS ent_sous_categorie,sus_siren as ent_siren,1 as ent_src";
	if($rechercheParAdresse){
		$sql_sus.=",sad_id,sad_type,sad_service AS ad_service,sad_ad1 AS ad1, sad_ad2 AS ad2, sad_ad3 AS ad3 ,sad_cp AS ad_cp ,sad_ville AS ad_ville";
	}else{
		$sql_sus.=",sus_adr_service AS ad_service,sus_adr_ad1 AS ad1, sus_adr_ad2 AS ad2, sus_adr_ad3 AS ad3, sus_adr_cp AS ad_cp, sus_adr_ville AS ad_ville";
	}
	if($rechercheParContact){
		$sql_sus.=",sco_id,sco_prenom AS prenom, sco_nom AS nom, sco_fonction AS fonction, sco_fonction_nom AS fonction_nom ,sco_tel AS tel, sco_mail AS mail";
	}else{
		$sql_sus.=",sus_con_prenom AS prenom, sus_con_nom AS nom, sus_con_fct AS fonction, sus_con_fct_nom AS fonction_nom, sus_con_tel AS tel, sus_con_mail AS mail";
	}
	if($_SESSION["prospect_tri"]['aff_cor']==1){
		$sql_sus.=",sus_cor_date AS corresp_date";
	}else{
		$sql_sus.=",sus_cor_rappel AS corresp_date";
	}
	// table
	$sql_sus.=" FROM Suspects";
	if($rechercheParAdresse){
		$sql_sus.=" LEFT JOIN Suspects_Adresses ON (Suspects.sus_id = Suspects_Adresses.sad_ref_id)";
	}
	if($rechercheParContact){
		$sql_sus.=" LEFT JOIN Suspects_Contacts ON (Suspects.sus_id = Suspects_Contacts.sco_ref_id)";
	}
	if($rechercheParCorrespondance){
		$sql_sus.=" LEFT JOIN Suspects_Correspondances ON (Suspects.sus_id = Suspects_Correspondances.sco_suspect)";
	}
	if($mil_sus!=""){
		$sql_sus.=" WHERE " . substr($mil_sus, 5, strlen($mil_sus)-5);
	}
	$sql_sus.=" ORDER BY sus_code,sus_nom";
	$req = $ConnSoc->prepare($sql_sus);	
	//echo($sql_sus . "<br/>");
	if(!empty($critere)){
		foreach($critere as $c => $u){
			//echo($c . ":" . $u . "<br/>");
			$req->bindValue($c,$u);
		}
	};
	if(!empty($critere_sus)){
		foreach($critere_sus as $c => $u){
			//echo($c . ":" . $u . "<br/>");
			$req->bindValue($c,$u);
		}
	};
	$req->execute();
	$suspects = $req->fetchAll(PDO::FETCH_ASSOC);

	if(!empty($suspects) AND !empty($clients)){
		$resultats=array_merge($suspects,$clients);
		usort($resultats, "sortProspect");
	}elseif(!empty($suspects)){
		$resultats=$suspects;
	}else{
		$resultats=$clients;
	}
	
	function sortProspect( $a, $b ) {
		return strcmp($a["ent_code"], $b["ent_code"]);
	}
	//die();
	
?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<!-- <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css"> -->
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
	
		<!-- Start: Main -->
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
	<?php			if(empty($resultats)){ ?>
						<div class="col-md-12 text-center" style="padding:0;" >
							<div class="alert alert-warning" style="border-radius:0px;">
								Aucun prospect correspondant à votre recherche.
							</div>
						</div>
	<?php			}else{ 
	
						$nb_resultat=count($resultats); 
						?>
						
						<h1><?=$nb_resultat?> prospect(s)</h1>
								
						<div class="table-responsive">
							<table class="table" id="table_id">
								<thead>
									<tr class="success" >
										<th>ID</th>
								<?php	if($_SESSION["prospect_tri"]['cli_reseau']==1){ ?>
											<th>Code Société</th> 
								<?php	} ?>
										<th>Code</th> 
										<th>Nom</th>
										<th>Type d'adresse</th>
										<th>Adresse</th>
										<th>CP</th>
										<th>Ville</th>
							<?php		if($_SESSION['acces']["acc_droits"][37]){ ?>
											<th>Contact</th>
											<th>Tél.</th>
											<th>Mail</th>
							<?php		}
										if($_SESSION['acces']["acc_profil"]==13){ ?>
											<th>SIREN</th>
							<?php		}
										if($_SESSION["prospect_tri"]['aff_cor']==1){ ?>
											<th>Date corresp</th>
							<?php		}else{ ?>	
											<th>Date rappel</th>
							<?php		} ?>
									</tr>
								</thead>
								<tbody>							
							<?php	if(!empty($resultats)){ 
										
										foreach($resultats as $c){
											// style de la ligne
											$style="";
											$style_a="";									
											if(!empty($c['cli_blackliste'])){
												$style="background-color:#000;color:#FFF;";
											}elseif(!empty($c['ent_sous_categorie'])){
												if(!empty($d_client_sous_categorie[$c['ent_sous_categorie']])){
													if(!empty($d_client_sous_categorie[$c['ent_sous_categorie']]["csc_couleur"])){
														$style="background-color:" . $d_client_sous_categorie[$c['ent_sous_categorie']]["csc_couleur"] . ";color:#FFF;";
														$style_a="color:#FFF;";
													}
												}
											}elseif(!empty($c['ent_categorie'])){
												if(!empty($d_client_categorie[$c['ent_categorie']])){
													if(!empty($d_client_categorie[$c['ent_categorie']]["cca_couleur"])){
														$style="background-color:" . $d_client_categorie[$c['ent_categorie']]["cca_couleur"] . ";";
													}
												}
											}
											
											if($c['ent_src']==2){
												
												if($c['cli_groupe']==1 AND $c['cli_filiale_de']==0){
													$style.="font-weight:bold;";	
												}
											}
											
											
											$adresse_type="Intervention";
											if(!empty($c['adr_type'])){
												if($c['adr_type']==2){
													$adresse_type="Facturation";
												}elseif($c['adr_type']==3){
													$adresse_type="Envoi de facture";
												}
											}
											$adresse_lib="";
											if(!empty($c['ad_service'])){
												$adresse_lib=$c['ad_service'];
											}
											if(!empty($c['ad1'])){
												if(!empty($adresse_lib)){
													$adresse_lib.="<br/>";
												}
												$adresse_lib.=$c['ad1'];
											}
											if(!empty($c['ad2'])){
												if(!empty($adresse_lib)){
													$adresse_lib.="<br/>";
												}
												$adresse_lib.=$c['ad2'];
											}
											if(!empty($c['ad3'])){
												if(!empty($adresse_lib)){
													$adresse_lib.="<br/>";
												}
												$adresse_lib.=$c['ad3'];
											} 
											
											// fonction
											if(!empty($fonction)){
												
											}else{
												$fonction_nom=$c['fonction_nom'];
											}
											$corresp_date="";
											if(!empty($c['corresp_date'])){
												$dt_corresp_date=date_create_from_format("Y-m-d",$c['corresp_date']);
												$corresp_date=$dt_corresp_date->format("d/m/Y");
											} 
											if($c['ent_src']==2){ ?>
												<tr <?php if(!empty($style)) echo("style='" . $style . "'"); ?> >
													<td><?=$c['ent_id'] ?></td>
										<?php		if($_SESSION["prospect_tri"]['cli_reseau']==1){ ?>
														<td><?=$c['cso_societe']?></td>
										<?php		} ?>
													<td>
														<a href="client_voir.php?client=<?=$c['ent_id']?>" style="<?=$style_a?>" >
															<?=$c['ent_code']?>
														</a>
													</td>
													<td><?=$c['ent_nom']?></td>
													<td><?=$adresse_type?></td>										
													<td><?=$adresse_lib?></td>
													<td><?= $c['ad_cp'] ?></td>
													<td><?= $c['ad_ville'] ?></td>
										<?php		if($_SESSION['acces']["acc_droits"][37]){ ?>
														<td><?= $c['prenom'] . " " . $c['nom']?></td>
														<td><?= $c['tel']?></td>
														<td><?= $c['mail']?></td>
										<?php		}
													if($_SESSION['acces']["acc_profil"]==13){ ?>
														<td><?= $c['ent_siren'] ?></td>
											<?php	} ?>															
													<td data-sort="<?=$c['corresp_date']?>" ><?=$corresp_date?></td>								
												</tr>
								<?php		}else{ ?>
												
												<tr <?php if(!empty($style)) echo("style='" . $style . "'"); ?> >
													<td><?=$c['ent_id'] ?></td>
											<?php	if($_SESSION["prospect_tri"]['cli_reseau']==1){ ?>
														<td><?=$acc_societe?></td>
										<?php		} ?>
													<td>
														<a href="suspect_voir.php?suspect=<?=$c['ent_id']?>" style="<?=$style_a?>" >
															<?=$c['ent_code']?>
														</a>
													</td>
													<td><?=$c['ent_nom']?></td>
													<td><?=$adresse_type?></td>										
													<td><?=$adresse_lib?></td>
													<td><?= $c['ad_cp'] ?></td>
													<td><?= $c['ad_ville'] ?></td>
											<?php	if($_SESSION['acces']["acc_profil"]==13){ ?>
														<td><?= $c['ent_siren'] ?></td>
											<?php	} ?>		
													<td data-sort="<?=$c['corresp_date']?>" ><?=$corresp_date?></td>											
												</tr>
									<?php	}
											$page[]=$c["ent_id"] . "-" . $c["ent_src"];
										}
										
										$_SESSION['client_tableau'] = $page;
									} ?>
						
								</tbody>
							</table>
						</div>
			<?php 	}  ?>
				</section>
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="prospect_tri.php" class="btn btn-primary btn-sm">
						<span class="fa fa-search"></span>
						<span class="hidden-xs">Nouvelle recherche</span>
					</a>					
				</div>
				<div class="col-xs-6 footer-middle">&nbsp;</div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
	<?php		/*	FG 
					20/05/2020 rebloqué à la demande de JP
					13/01/2021 activé à la demande de Maud (elle doit etre la seul à avoir le droit 37)
				*/
				if(!$_SESSION['acces']["acc_droits"][37]){ ?>	
					 //Disable full page
					$('body').bind('cut copy paste', function (e) {
						e.preventDefault();
					});
					//Disable mouse right click
					$("body").on("contextmenu",function(e){
						return false;
					});
	<?php		} ?>
				
				$('#table_id').DataTable( {
					"language": {
						"url": "vendor/plugins/DataTables/media/js/French.json"
					},
					"paging": false,
					"searching": false,
					"info": false,
					"order": [[ 1, "asc" ]]
				} );
				
			});
		</script>
	</body>
</html>
