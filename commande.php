<?php  
////////////////// MENU ACTIF HEADER ///////////////////
$menu_actif = 5;
////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php'); 
$_SESSION['retour_commande'] = "commande.php";
// session retour 
if(isset($_GET['menu_retour'])){
	unset($_SESSION['retour']);
}



$commande_id = 0;
if(isset($_GET['id'])){
	if(!empty($_GET['id'])){
	    $commande_id = intval($_GET['id']);
	}
}

////////////////// TRAITEMENTS SERVEUR ///////////////////

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}
	
	if(!empty($commande_id)){
		
		// EDITION
		
		$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id =" . $_GET['id']);
		$req->execute();
		$commande = $req->fetch();
		if(!empty($commande)){
			
			// rechercher les fournisseurs sur la société actuelle
			$req = $Conn->prepare("SELECT fou_id, fou_code, fou_nom,fou_type FROM fournisseurs WHERE fou_id = " . $commande['com_fournisseur']);
			$req->execute();
			$fournisseur = $req->fetch();
			
			//on recupere les contacts du fournisseur
			
			$d_contacts=array();
			$req = $Conn->prepare("SELECT * FROM fournisseurs_Contacts WHERE fco_fournisseur = " . $commande['com_fournisseur']);
			$req->execute();
			$d_results = $req->fetchAll();
			if(!empty($d_results)){
				foreach($d_results as $r){
					$d_contacts[$r["fco_id"]]=array(
						"fco_titre" => $r["fco_titre"],
						"fco_nom" => $r["fco_nom"],
						"fco_prenom" => $r["fco_prenom"],
						"fco_tel" => $r["fco_tel"],
						"fco_portable" => $r["fco_portable"],
						"fco_fax" => $r["fco_fax"],
						"fco_mail" => $r["fco_mail"],
						"fco_fonction" => $r["fco_fonction"],
						"fco_fonction_nom" => $r["fco_fonction_nom"]
					);
				}
			}

		}
		
		

	}else{
		
		$commande= array(
			"com_societe" => $acc_societe,
			"com_agence" => $acc_agence,
			"com_action" => 0,
			"com_donneur_ordre" => $acc_utilisateur,
			"com_liv_nom" => "",
			"com_liv_adr1" => "",
			"com_liv_adr2" => "",
			"com_liv_adr3" => "",
			"com_liv_cp" => "",
			"com_liv_ville" => "",
			"com_liv_type" => 0,
			"com_liv_ref_1" => 0,
			"com_liv_ref_2" => 0,
			"com_contact" => 0
		);
		
		$fournisseur=array(
			"fou_type" => 0
		);
		
	}
	
	// SUR LA SOCIETE
	$req = $Conn->prepare("SELECT soc_nom FROM societes WHERE soc_id =" . $commande['com_societe']);
	$req->execute();
	$societe = $req->fetch();
	
	// SUR L'AGENCE
	if($acc_agence==0 AND $commande["com_action"]==0){
		
		$req = $Conn->prepare("SELECT age_id,age_nom FROM Agences WHERE age_societe =" . $commande['com_societe']);
		$req->execute();
		$agences = $req->fetchAll();
		if(empty($agences)){
			unset($agences);
		}
	}else{
		
		$req = $Conn->prepare("SELECT age_nom FROM Agences WHERE age_id =" . $commande['com_agence']);
		$req->execute();
		$agence = $req->fetch();
		if(empty($agence)){
			unset($agence);
		}

	}
	
	// LE DONNEUR D'ORDRE
	
	$req = $Conn->prepare("SELECT uti_id, uti_nom, uti_prenom FROM utilisateurs WHERE uti_id = " . $commande['com_donneur_ordre']);
	$req->execute();
	$ordre = $req->fetch();
	
	
	// SOCIETE POUR LIVRAISON
	$req = $Conn->prepare("SELECT * FROM societes WHERE soc_archive = 0");
	$req->execute();
	$societes = $req->fetchAll();
	
	// AGENCE POUR LA LIVRAISON
	if($commande["com_liv_type"]==2 AND $commande["com_liv_ref_1"]!=0){
		
		$req = $Conn->prepare("SELECT age_id,age_nom FROM Agences WHERE age_societe =" . $commande["com_liv_ref_1"] . " ORDER BY age_nom;");
		$req->execute();
		$d_liv_agences = $req->fetchAll();
		if(empty($d_liv_agences)){
			unset($d_liv_agences);
		}
		
	}
	
	// SUR L'ACTION 
	
	if(!empty($commande["com_action"])){
		
		$req = $ConnSoc->prepare("SELECT DATE_FORMAT(act_date_deb,'%d/%m/%Y') AS act_date_deb FROM Actions WHERE act_id =" . $commande["com_action"] . ";");
		$req->execute();
		$d_action = $req->fetch();

	}
	



?>
<!DOCTYPE html>
<html>
	<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Commandes</title>
	<meta name="keywords" content=""/>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	<!--
	<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
	-->


	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	</head>

<body class="sb-top sb-top-sm ">
	<form method="post" action="commande_enr.php" id="formulaire" >
		<div id="main">
<?php 		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper" >		
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary ">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="content-header">
											<h2>
												<?php if(isset($_GET['id'])){?>
													Demande d'achat <b class='text-primary'><?= $commande['com_numero']?></b>
												<?php }else{ ?>
													Nouvelle <b class='text-primary'>demande d'achat</b>
												<?php }?>
											</h2>					
										</div>
										<div class="col-md-10 col-md-offset-1">		
										
									<?php	if(!empty($commande["com_action"])){ ?>
												<p class="alert alert-info" >
													Ce bon de commande est lié à l'action N° <?=$commande["com_action"]?> du <?=$d_action["act_date_deb"]?>.
												</p>									
									<?php	} ?>

											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span>Facturation</span>
													</div>
												</div>
											</div>
											<div class="row">												
												<input type="hidden" name="com_societe" value="<?=$commande['com_societe']?>" /> 
										<?php	if(!isset($agences)){ ?>
													<input type="hidden" name="com_agence" value="<?=$commande['com_agence']?>" >
										<?php	} ?>
												<input type="hidden" id="com_id" name="com_id" value="<?= $commande_id?>">
												<input type="hidden" id="id_heracles" name="id_heracles" value="0" /> 
										<?php	if(!empty($commande['com_action'])){ ?>
													<input type="hidden" id="com_fournisseur" name="com_fournisseur" value="<?=$fournisseur['fou_id']?>" /> 
										<?php	} ?>	
												
												
												<div class="col-md-4 text-center pt25"><strong>Donneur d'ordre : <?=$ordre['uti_prenom'] . " " . $ordre['uti_nom']?></strong></div>
												<div class="col-md-4 text-center pt25"><strong>Société : <?=$societe['soc_nom']?></strong></div>
										<?php	if(isset($agences)){ ?>
													<div class="col-md-4">
														<label for="com_agence" >Agence :</label>
														<select class="select2 get-client-agence" id="com_agence" name="com_agence" required >
															<option value="" >Agence ...</option>
													<?php	foreach($agences as $age){
																if($age["age_id"]==$commande['com_agence']){
																	echo("<option value='" . $age["age_id"] . "' selected >" . $age["age_nom"] . "</option>");
																}else{
																	echo("<option value='" . $age["age_id"] . "' >" . $age["age_nom"] . "</option>");
																}
															} ?>
														</select>
													</div>
										<?php	}elseif(isset($agence)){ ?>
													<div class="col-md-4 text-center pt25"><strong>Agence : <?=$agence['age_nom']?></strong></div>
										<?php	} ?>												
											</div>
													
											<div class="row" id="cont_formulaire" <?php if(isset($agences) AND $commande['com_agence']==0) echo("style='display:none;'"); ?> >
												<div class="col-md-12">	
													<div class="section-divider mb40">
														<span>Fournisseur</span>
													</div>
												</div>												
												<div class="row">
											<?php	if(!empty($commande['com_action'])){ ?>
														<div class="col-md-6 text-center pt25"><strong>Fournisseur : <?= $fournisseur['fou_nom'] ?> (<?= $fournisseur['fou_code'] ?>)</strong></div>
											<?php	}else{ ?>										
														<div class="col-md-6">
															<div class="section">													  
																<select id="com_fournisseur" name="com_fournisseur" class="select2 select2-fournisseur" data-agence="<?=$commande['com_agence']?>" required>
																	<option value="" selected="selected">Fournisseur...</option>
														<?php		if(isset($_GET['id'])){ ?>
																		<option value="<?= $fournisseur['fou_id'] ?>" selected><?= $fournisseur['fou_nom'] ?> (<?= $fournisseur['fou_code'] ?>)</option>
														<?php 		} ?>
																</select>													   
															</div>
														</div>	
											<?php	} ?>			
													<div class="col-md-6 progression" style="display:none;">
														<span>Taux d'usage du fournisseur :</span>
														<div class="progress progress-bar-xl">
															<div class="progress-bar progression_bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 1%;">
																<span>1%</span>
															</div>
														</div>
													</div>
													<!--<div class="col-md-12 depassement">
														<div class="well well-xs">
															<p class="mbn pl10 texte_progression">Veuillez sélectionner un fournisseur.</p>
														</div>
													</div>
													<div class="col-md-12 progression_b" style="display:none;">
														<div class="panel panel-tile text-primary light">
															<div class="panel-body pn pl20 p5">
																<i class="fa fa-arrow-up icon-bg"></i>
																<h2 class="mt15 lh15">
																	<b class="montant_depasse">523 €</b>
																</h2>
																<h5 class="text-muted">dépassés</h5>
															</div>
														</div>
													</div>-->									
												</div>
												
												<!-- CONTACT -->
												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Contact</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">															
																<div class="section">														  
																	<select id="com_contact" name="com_contact" class="select2" <?php if($fournisseur["fou_type"]==3) echo("disabled") ?>>
																		<option value="0" >Saisie libre...</option>
															<?php		if(isset($d_contacts)){
																			foreach($d_contacts as $c_id => $c){
																				if($c_id==$commande["com_contact"]){
																					echo("<option value='" . $c_id . "' selected >" . $c["fco_nom"] . " " . $c["fco_prenom"] . "</option>");
																				}else{
																					echo("<option value='" . $c_id . "' >" . $c["fco_nom"] . " " . $c["fco_prenom"] . "</option>");
																				} 
																			}
																		} ?>
																	</select>
																	<input type="hidden" id="com_contact_disabled" name="com_contact_disabled" value="<?=$commande["com_contact"]?>" /> 
																</div>
															</div>
														</div>
															<div class="row">
																	<div class="col-md-12">
																		<div class="section">
																			<div class="field select">
																				<select name="com_contact_fonction" id="com_contact_fonction" onchange="nomchange(this.value)">
																					<option value="0">Sélectionner une fonction...</option>
																					<?php if (isset($_GET['id'])){ ?>
																						<?=get_contact_fonction_select($commande['com_contact_fonction']);?>
																					<?php }else{ ?>
																						<?=get_contact_fonction_select(0);?>
																					<?php } ?>
																					<option value="autre">Autre fonction...</option>
																				</select>
																				<i class="arrow simple"></i>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-12 com_contact_fonction_nom_style" style="display:none;">
																		<div class="section">
																			<div class="field prepend-icon">
																				<input type="text" name="com_contact_fonction_nom" id="com_contact_fonction_nom" class="gui-input" placeholder="Autre fonction"  
																				<?php 

																					if(isset($_GET['id'])){
																				?>
																					value="<?= $commande['com_contact_fonction_nom'] ?>"
																				<?php }?>
																				>
																				<label for="com_contact_fonction_nom" class="field-icon">
																					<i class="fa fa-tag"></i>
																				</label>
																			</div>
																		</div>
																	</div>
															</div>
															<div class="row">
																	<div class="col-md-12">
																		<div class="section">
																			<div class="field select">
																				<select name="com_contact_titre" id="com_contact_titre" >
																					<option value="0">Civilité...</option>
																					<?php foreach($base_civilite as $b => $c){ ?>
																						<?php if($b > 0){ ?>
																							<option value="<?= $b ?>"
																							<?php 

																								if(isset($_GET['id']) && $b == $commande['com_contact_titre']){
																							?>
																								selected
																							<?php }?>

																							><?= $c ?></option>
																						<?php } ?>
																					<?php } ?>


																				</select>
																				<i class="arrow simple"></i>
																			</div>
																		</div> 
																	</div>
															</div>

															<div class="row">
																	<div class="col-md-6">
																		<div class="section">
																			<div class="field prepend-icon">
																				<input type="text" name="com_contact_nom" id="com_contact_nom" class="gui-input nom" placeholder="Nom"
																				<?php 

																					if(isset($_GET['id'])){
																				?>
																					value="<?= $commande['com_contact_nom'] ?>"
																				<?php }?>
																				>
																				<label for="com_contact_nom" class="field-icon">
																					<i class="fa fa-user"></i>
																				</label>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-6">
																		<div class="section">
																			<div class="field prepend-icon">
																				<input type="text" name="com_contact_prenom" id="com_contact_prenom" class="gui-input prenom" placeholder="Prénom"
																				<?php 

																					if(isset($_GET['id'])){
																				?>
																					value="<?= $commande['com_contact_prenom'] ?>"
																				<?php }?>
																				>
																				<label for="com_contact_prenom" class="field-icon">
																					<i class="fa fa-tag"></i>
																				</label>
																			</div>
																		</div>
																	</div>
															</div>
															<div class="row">
																	<div class="col-md-6">
																		<div class="section">
																			<div class="field prepend-icon">
																				<input name="com_contact_tel" id="com_contact_tel" type="tel" class="gui-input telephone" placeholder="Numéro de téléphone"
																				<?php 

																					if(isset($_GET['id'])){
																				?>
																					value="<?= $commande['com_contact_tel'] ?>"
																				<?php }?>
																				>
																				<label for="com_contact_tel" class="field-icon">
																					<i class="fa fa fa-phone"></i>
																				</label>
																			</div>
																		</div>
																	</div>

																	<div class="col-md-6">
																		<div class="section">
																			<div class="field prepend-icon">
																				<input name="com_contact_fax" id="com_contact_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax"

																				<?php 

																					if(isset($_GET['id'])){
																				?>
																					value="<?= $commande['com_contact_fax'] ?>"
																				<?php }?>
																				>
																				<label for="com_contact_fax" class="field-icon">
																					<i class="fa fa-phone-square"></i>
																				</label>
																			</div>
																		</div>
																	</div>
															</div>
															<div class="row">
																	<div class="col-md-6">
																		<div class="section">
																			<div class="field prepend-icon">
																				<input name="com_contact_portable" id="com_contact_portable" type="tel" class="gui-input telephone" placeholder="Numéro de portable"

																				<?php 

																					if(isset($_GET['id'])){
																				?>
																					value="<?= $commande['com_contact_portable'] ?>"
																				<?php }?>
																				>
																				<label for="com_contact_portable" class="field-icon">
																					<i class="fa fa fa-mobile"></i>
																				</label>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-6">
																		<div class="section">
																			<div class="field prepend-icon">
																				<input type="email" name="com_contact_mail" id="com_contact_mail" class="gui-input" placeholder="Adresse Email"

																				<?php 
																					if(isset($_GET['id'])){
																				?>
																					value="<?= $commande['com_contact_mail'] ?>"
																				<?php } ?>
																				>
																				<label for="com_contact_mail" class="field-icon">
																					<i class="fa fa-envelope"></i>
																				</label>
																			</div>
																		</div>
																	</div>
															</div>
													</div>
													
													<!-- LIVRAISON -->
											<?php	if(empty($commande['com_action'])){ ?>
														<div class="col-md-6">														
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Adresse de livraison</span>
																	</div>
																</div>
															</div>													
															<div class="row">
																<div class="col-md-12">
																	<div class="row">
																		<div class="col-md-12">
																			<div class="section">									                                      
																				<select id="com_liv_type" name="com_liv_type" class="select2">
																					<option value="0" >Saisie libre...</option>
																					<option value="1" <?php if($commande["com_liv_type"]==1) echo("selected='selected'"); ?> >Site de facturation</option>
																					<option value="2" <?php if($commande["com_liv_type"]==2) echo("selected='selected'"); ?> >Autre agence Si2P</option>
																					<option value="3" <?php if($commande["com_liv_type"]==3) echo("selected='selected'"); ?> >Adresse d'intervention d'un client</option>									                                               
																					<option value="5" <?php if($commande["com_liv_type"]==5) echo("selected='selected'"); ?> >Adresse d'un autre fournisseur</option>
																				</select>
																			</div>
																		</div>
																	</div>
																	<div class="row com_societe_adresse" <?php if($commande["com_liv_type"]!=2) echo("style='display:none;'"); ?> >
																		<div class="col-md-6">
																			<div class="section">
																				<label for="com_societe_adresse" >Société :</label>
																				<select id="com_societe_adresse" name="com_societe_adresse" class="select2 select2-societe">
																					<option value="0" >Société ...</option>
																		<?php 		foreach($societes as $s){ 
																						if($s['soc_id']==$commande["com_liv_ref_1"]){ ?>
																							<option value="<?= $s['soc_id'] ?>" selected ><?= $s['soc_nom'] ?></option>
																				<?php 	}else{ ?>
																							<option value="<?= $s['soc_id'] ?>"><?= $s['soc_nom'] ?></option>
																				<?php	}
																					}?>
																				</select>																   
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="section">	
																				<label for="com_agence_adresse" >Agence :</label>
																				<select id="com_agence_adresse" name="com_agence_adresse" class="select2 select2-societe-agence">
																					<option value="0" >Agence ...</option>
																	<?php			if(isset($d_liv_agences)){
																						foreach($d_liv_agences as $liv){
																							if($liv['age_id']==$commande["com_liv_ref_2"]){
																								echo("<option value='" . $liv['age_id'] . "' selected >" . $liv['age_nom'] . "</option>");
																							}else{
																								echo("<option value='" . $liv['age_id'] . "' >" . $liv['age_nom'] . "</option>");
																							}
																						}
																					} ?>																		
																				</select>																	   
																			</div>
																		</div>
																	</div>
																	<div class="row com_client_adresse" <?php if($commande["com_liv_type"]!=3) echo("style='display:none;'"); ?> >
																		<div class="col-md-12">
																			<div class="section">
																				<label for="com_client" >Client :</label>
																				<select id="com_client" name="com_client" class="select2 select2-client-n get-client" data-adr_type="1" data-adr_contact="0" ></select>																   
																			</div>
																		</div>
																	</div>
																	<div class="row com_client_adresse" <?php if($commande["com_liv_type"]!=3) echo("style='display:none;'"); ?> >
																		<div class="col-md-12">
																			<div class="section">
																				<label for="com_client_adresse" >Adresses :</label>
																				<select id="com_client_adresse" name="com_client_adresse" class="select2"></select>																   
																			</div>
																		</div>
																	</div>
																	
																	<div class="row com_fournisseur_adresse" <?php if($commande["com_liv_type"]!=5) echo("style='display:none;'"); ?> >
																		<div class="col-md-12">
																			<div class="section">
																		
																				<select id="com_fournisseur_adresse" name="com_fournisseur_adresse" class="select2 select2-fournisseur" data-agence="<?=$commande["com_agence"]?>" >
																					
																				</select>
																		
																			</div>
																		</div>
																	</div>
																		
																	<div class="row bloc-adresse" <?php if($commande["com_liv_type"]!=0 AND $commande["com_liv_type"]!=3 AND $commande["com_liv_type"]!=5) echo("style='display:none;'"); ?> >
																		<div class="col-md-12">
																			<div class="section">
																				<div class="field prepend-icon">
																					<input type="text" name="com_liv_nom" id="com_liv_nom" class="gui-input champ-adresse" placeholder="Nom" value="<?= $commande['com_liv_nom'] ?>" />
																					<label for="com_liv_nom" class="field-icon">
																						<i class="fa fa-tag"></i>
																					</label>
																				</div>
																			</div>
																		</div>		 
																	</div>
																	<div class="row bloc-adresse" <?php if($commande["com_liv_type"]!=0 AND $commande["com_liv_type"]!=3 AND $commande["com_liv_type"]!=5) echo("style='display:none;'"); ?> >
																		<div class="col-md-12">
																			<div class="section">
																				<div class="field prepend-icon">
																					<input type="text" name="com_liv_adr1" id="com_liv_adr1" class="gui-input champ-adresse" placeholder="Adresse" value="<?= $commande['com_liv_adr1'] ?>" />
																					<label for="com_liv_adr1" class="field-icon">
																						<i class="fa fa-map-marker"></i>
																					</label>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="row bloc-adresse" <?php if($commande["com_liv_type"]!=0 AND $commande["com_liv_type"]!=3 AND $commande["com_liv_type"]!=5) echo("style='display:none;'"); ?> >
																		<div class="col-md-12">
																			<div class="section">
																				<div class="field prepend-icon">
																					<input type="text" name="com_liv_adr2" class="gui-input champ-adresse" id="com_liv_adr2 " placeholder="Adresse (Complément 1)" value="<?= $commande['com_liv_adr2'] ?>" />									
																					<label for="com_liv_adr2" class="field-icon">
																						<i class="fa fa-map-marker"></i>
																					</label>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="row bloc-adresse" <?php if($commande["com_liv_type"]!=0 AND $commande["com_liv_type"]!=3 AND $commande["com_liv_type"]!=5) echo("style='display:none;'"); ?> >
																		<div class="col-md-12">
																			<div class="section">
																				<div class="field prepend-icon">
																					<input type="text" name="com_liv_adr3" id="com_liv_adr3" class="gui-input champ-adresse" placeholder="Adresse (Complément 2)" value="<?= $commande['com_liv_adr3'] ?>" />												
																					<label for="com_liv_adr3" class="field-icon">
																						<i class="fa fa-map-marker"></i>
																					</label>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="row bloc-adresse" <?php if($commande["com_liv_type"]!=0 AND $commande["com_liv_type"]!=3 AND $commande["com_liv_type"]!=5) echo("style='display:none;'"); ?> >
																		<div class="col-md-3">
																			<div class="section">
																				<div class="field prepend-icon">
																					<input type="text" id="com_liv_cp" name="com_liv_cp" class="gui-input code-postal champ-adresse" placeholder="Code postal" value="<?= $commande['com_liv_cp'] ?>" />
																					<label for="com_liv_cp" class="field-icon">
																						<i class="fa fa fa-certificate"></i>
																					</label>
																				</div>
																			</div>
																		</div>
																		<div class="col-md-9">
																			<div class="section">
																				<div class="field prepend-icon">
																					<input type="text" name="com_liv_ville" id="com_liv_ville" class="gui-input nom champ-adresse" placeholder="Ville" value="<?= $commande['com_liv_ville'] ?>" />
																					<label for="com_liv_ville" class="field-icon">
																						<i class="fa fa fa-building"></i>
																					</label>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>

															</div>
														</div>
											<?php	} ?>
										        </div>
											</div>												
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
						
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<?php if(!empty($_GET['id'])){ ?>
				        <a href="commande_voir.php?commande=<?= $_GET['id'] ?>" class="btn btn-default btn-sm">
				          <i class="fa fa-long-arrow-left"></i>
				          Retour
				        </a>
				    <?php }else{ ?>
				   		<a 
							 <?php if(!empty($_SESSION['retour'])){ ?>
								href="<?= $_SESSION['retour'] ?>"
							 <?php }else{ ?>
								href="commande_liste.php"
							 <?php } ?>
							  
							 class="btn btn-default btn-sm">
				          <i class="fa fa-long-arrow-left"></i>
				          Retour
				        </a>
				    <?php } ?>
						
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
					<button type="button" id="btn_sub_form" class="btn btn-success btn-sm">
						<i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php
	include "includes/footer_script.inc.php"; ?>	
	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<!-- plugin pour les masques formulaires -->
	<script src="assets/js/custom.js"></script>
	<script src="vendor/plugins/holder/holder.min.js"></script>
	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script type="text/javascript" src="/assets/js/sync_heracles.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->
	
	<script>

		var contacts_json="";
<?php	if(!empty($d_contacts)){ ?>
			contacts_json=JSON.parse('<?=json_encode($d_contacts)?>');
<?php	} ?>
		
		var tab_adresse=new Array();
		var commande_id="<?=$commande_id?>";

		jQuery(document).ready(function () {
			
			// initialisation du taux d'usge si c'est une édition
<?php 		if(isset($_GET['id'])){ ?>
				taux_usage($("#com_fournisseur").val());
<?php 		}?>

			$("#com_agence").change(function(){
				$("#com_fournisseur").data("agence",$(this).val());
				$("#com_fournisseur_adresse").data("agence",$(this).val());
				if($(this).val()>0){
					$("#cont_formulaire").show();
				}else{
					$("#cont_formulaire").hide();
				}
			});
			
			$("#com_fournisseur").change(function(){		
				get_fournisseur($(this).val(),afficher_fournisseur,"","");
			});
			
			$("#com_contact").change(function(){			
				remplir_contact($(this).val());
			});
			
			// LIVRAISON CLIENT
						
			$("#com_liv_type").change(function(){
				
				$(".champ-adresse").val("");
				$(".com_societe_adresse").hide();
				$(".com_client_adresse").hide();
				$(".com_fournisseur_adresse").hide();
				
				$(".bloc-adresse").hide();
				switch($(this).val()){
					case "0":
						$(".bloc-adresse").show();
						break;					
					case "2":
						$(".com_societe_adresse").show();
						break;
					case "3":
						$(".com_client_adresse").show();
						$(".bloc-adresse").show();
						break;
					case "5":
						$(".com_fournisseur_adresse").show();
						$(".bloc-adresse").show();
						break;				
				}		
			});
		
			
			$("#com_client_adresse").change(function(){
				afficher_adresse_client($(this).val());
			});
			
			// LIVRAISON FOURNISSEUR
			$("#com_fournisseur_adresse").change(function(){
				get_fournisseur($(this).val(),afficher_adresse_fournisseur,"","");
			});
			
			$("#btn_sub_form").click(function(){
				valide=valider_form("#formulaire");
				if(valide){
					if($("#com_id").val()==0){
						console.log("Sync Heracles activé");
						commande_sync($("#com_fournisseur").val(),"#formulaire","#id_heracles","","","");
						//$("#formulaire").submit();
					}else{
						$("#formulaire").submit();
					}
				}
				
			});
			
		});

		///////////// FONCTIONS //////////////

		// fonction contact
		function nomchange(selected){
			if(selected == "autre"){
				$(".com_contact_fonction_nom_style").show(400);
			}else{
				$(".com_contact_fonction_nom_style").hide(400);
			}
		}

		function afficher_fournisseur(json){
			$("#com_contact").find("option:gt(0)").remove();
			if(json){
				if(json.contacts_select!=""){
					$("#com_contact").select2({
						data: json.contacts_select,
						closeOnSelect: true			
					});
					if(json.contacts){
						// on memorise le tableau json des contacts
						contacts_json=json.contacts;
					}
					if(json.contact_def!=""){
						$("#com_contact").val(json.contact_def).trigger("change");
					}
				}
				if(json.fou_type==3){
					$("#com_contact").prop("disabled",true);
				}else{
					$("#com_contact").prop("disabled",false);
				}
				
				taux_usage(json.fou_id);
			}else{
				
			}
		}
		
		function taux_usage(fournisseur){
			$.ajax({
					url: "ajax/ajax_fournisseur_usage.php",
					type: "post",
					dataType: "json",
					data: "fournisseur=" + fournisseur + "&commande=" + commande_id,
					success: function(data) {
						if(data['existe'] != 0){
							if(data['depassement'] == 1){
								$(".progression").show();
								
								$(".progression_bar").attr("aria-valuenow", 100);
								$(".progression_bar").css( "width","100%");
								$(".progression_bar").text("100%");
								if(data['depassement_autorise'] == 0){
									$(".depassement > div > p").text("Le dépassement n'est pas autorisé pour ce fournisseur");
									$(".progression_b").hide();
									$("#submit").attr("disabled", true);
								}else{
									$(".depassement > div > p").text("Le dépassement est autorisé pour ce fournisseur");
									$(".progression_b").show();
									$(".montant_depasse").text(data['pourcentage'] + " €");
									$("#submit").attr("disabled", false);
								}
								
							}else{
								$("#btn_sub_form").attr("disabled", false);
								$(".progression").show();
								$(".progression_b").hide();
								$(".progression_bar").attr("aria-valuenow", data['pourcentage']);
								$(".progression_bar").css( "width",data['pourcentage'] + "%");
								$(".progression_bar").text(data['pourcentage'] + "%");
								$(".depassement > div > p").text("Il n'y a pas de dépassement pour ce fournisseur");
							}
							
						}else{
							$("#btn_sub_form").attr("disabled", false);
							$(".progression").hide();
							$(".progression_b").hide();
							$(".depassement > div > p").text("Le montant maximum de ce fournisseur n'est pas renseigné");
						}

					}
			});
		}

		function actualiser_get_client(json){
			$("#com_client_adresse option[value!='0'][value!='']").remove();		
			if(json.adr_int){
				
				for(i=0;i<=json.adr_int.length-1;i++){
					tab_adresse[json.adr_int[i].id]=new Array(9);
					tab_adresse[json.adr_int[i].id][0]=json.adr_int[i].nom;
					tab_adresse[json.adr_int[i].id][1]=json.adr_int[i].service;
					tab_adresse[json.adr_int[i].id][2]=json.adr_int[i].ad1;
					tab_adresse[json.adr_int[i].id][3]=json.adr_int[i].ad2;
					tab_adresse[json.adr_int[i].id][4]=json.adr_int[i].ad3;
					tab_adresse[json.adr_int[i].id][5]=json.adr_int[i].cp;
					tab_adresse[json.adr_int[i].id][6]=json.adr_int[i].ville;
				}
				
				$("#com_client_adresse").select2({
					data: json.adr_int,
					closeOnSelect: true			
				});
				$("#com_client_adresse").trigger("change");
				
				
			}			
		}
		
		function afficher_adresse_client(adresse){
			if(adresse>0){
				$("#com_liv_nom").val(tab_adresse[adresse][0]);
				$("#com_liv_adr1").val(tab_adresse[adresse][2]);
				$("#com_liv_adr2").val(tab_adresse[adresse][3]);
				$("#com_liv_adr3").val(tab_adresse[adresse][4]);
				$("#com_liv_cp").val(tab_adresse[adresse][5]);
				$("#com_liv_ville").val(tab_adresse[adresse][6]);
			}else{
				$(".champ-adresse").val("");
			}
			
		}
		
		
		function afficher_adresse_fournisseur(json){
			if(json.adresses){
				$("#com_liv_nom").val(json.adresses.fad_nom);
				$("#com_liv_adr1").val(json.adresses.fad_ad1);
				$("#com_liv_adr2").val(json.adresses.fad_ad2);
				$("#com_liv_adr3").val(json.adresses.fad_ad3);
				$("#com_liv_cp").val(json.adresses.fad_cp);
				$("#com_liv_ville").val(json.adresses.fad_ville);
			}else{
				$(".champ-adresse").val("");
			}
		}

		// fonction remplir le formulaire de contacts fournisseurs
		function remplir_contact(contact){
			$("#com_contact_disabled").val(contact);
			if(contact>0){
				console.log(contacts_json[contact]);
				$("#com_contact_titre").val(contacts_json[contact]['fco_titre']);
				$("#com_contact_nom").val(contacts_json[contact]['fco_nom']);
				$("#com_contact_prenom").val(contacts_json[contact]['fco_prenom']);
				$("#com_contact_mail").val(contacts_json[contact]['fco_mail']);
				$("#com_contact_tel").val(contacts_json[contact]['fco_tel']);
				$("#com_contact_portable").val(contacts_json[contact]['fco_portable']);
				$("#com_contact_fax").val(contacts_json[contact]['fco_fax']);
				if(contacts_json[contact]['fco_fonction']>0){
					$("#com_contact_fonction").val(contacts_json[contact]['fco_fonction']);
					$("#com_contact_fonction_nom").val("");
					$(".com_contact_fonction_nom_style").hide();
				}else if(contacts_json[contact]['fco_fonction_nom']!=""){
					$("#com_contact_fonction").val("autre");
					$(".com_contact_fonction_nom_style").show();
					$("#com_contact_fonction_nom").val(contacts_json[contact]['fco_fonction_nom']);
				}else{
					$("#com_contact_fonction").val(0);
					$(".com_contact_fonction_nom_style").hide();
					$("#com_contact_fonction_nom").val("");
				}
			}else{
				$("#com_contact").val(0);
				$("#com_contact_titre").val(0);
				$("#com_contact_nom").val("");
				$("#com_contact_prenom").val("");
				$("#com_contact_mail").val("");
				$("#com_contact_tel").val("");
				$("#com_contact_portable").val("");
				$("#com_contact_fax").val("");
				$("#com_contact_fonction").val(0);
				$("#com_contact_fonction_nom").val("");
				$(".com_contact_fonction_nom_style").hide();
			}
		}
		

	</script>
</body>
</html>