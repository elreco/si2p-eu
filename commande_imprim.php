<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_parametre.php');

include('modeles/mod_commande_data.php');
$data = commande_data($_GET['commande']);
?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title><?=$data["com_numero"]?></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
<style type="text/css" >

    #container_print{
                    width:21cm;
                }
    table{
        width:100%;
        color:#333333;
        font-size:11pt;
        font-family:helvetica;
        line-height:6mm;
        border-collapse:collapse;
    }


    .text-right{
        text-align:right;
    }
    .text-center{
        text-align:center;
    }

    table.border{

    }
    table.border th{
        background-color:#333333;
        color:#FFFFFF;
        text-align:center;
    }

    table.border td{
        border:1px solid #7e8082;
    }



    h3{
        font-size:12pt;
    }
    .cadre{
        border:1px solid #333333;
        padding:5px;
    }

    .juridique{
        font-size:7pt;
        line-height:3mm;
        text-align:center;
    }

    h2{
        color:#333333;
        font-size: 24px;
        margin-top: 9.5px;
        margin-bottom: 9.5px;
    }
    .text-primary{
        color: #E31936;
    }
    h4{
        font-size:12pt;
        margin:0px;

    }
    h5{
        font-size:8pt;
        color:#E31936;
        margin:5px 0px 0px 0px;
    }
    h6{
        font-size:8pt;
        margin:5px 0px 0px 0px;
    }
    .cgv-intro{
        color:#E31936;
        font-size:8pt;
        text-align:justify;
        margin:5px 0px 0px 0px;
    }
    .cgv{
        text-align:justify;
        font-size:7pt;
        margin:5px 0px 0px 0px;
    }
    .titre{

        background-color:#333333;
        color:white;
    }
    .mt20{

        margin-top:20px;
    }
    </style>
    <style type="text/css" media="print" >
        @page{
            size:A4 portrait;
            margin:5mm;
        }
    </style>
</head>

<body class="sb-top sb-top-sm document-affiche">
    <div id="zone_print" ></div>
    <div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">

                <div class="row" >

                    <!-- zone d'affichage du devis -->

                    <div class="col-md-8"  >

                        <div id="container_print" >

                            <div id="page_print" >
                                <div class="page" id="header_0" data-page="0" >
                                        <?php       if(!empty($data['agence'])){ ?>

                                            <div class="text-left" style="width:15%;display:inline-block">
                                            <?php       if(!empty($data['agence']["age_logo_1"])){
                                                echo("<img style='width:100%;' src='/documents/societes/logos/" . $data['agence']["age_logo_1"] . "' />");
                                                } ?>
                                                </div>

                                                <div class="text-center" style="width:70%;display:inline-block;">
                                                    <h1 class="text-center" style="display: inline;">COMMANDE D'ACHAT</h1>
                                                </div>
                                            <div class="text-right" style="width:13%;display:inline-block;">
                                                <?php       if(!empty($data['agence']["age_logo_2"])){
                                                    echo("<img style='width:100%;' src='/documents/societes/logos/" . $data['agence']["age_logo_2"] . "' />");
                                                } ?>
                                            </div>

                                        <?php }elseif(!empty($data['societe'])){ ?>
                                            <div class="text-left" style="width:15%;display:inline-block;">
                                                <?php       if(!empty($data['societe']["soc_logo_1"])){
                                                    echo("<img style='width:100%;' src='/documents/societes/logos/" . $data['societe']["soc_logo_1"] . "' />");
                                                } ?>
                                                </div>

                                                <div class="text-center"  style="width:70%;display:inline-block">
                                                    <h1 class="text-center" >COMMANDE D'ACHAT</h1>
                                                </div>
                                            <div class="text-right" style="width:13%;display:inline-block;">
                                                <?php       if(!empty($data['societe']["soc_logo_2"])){
                                                    echo("<img style='width:100%;' src='/documents/societes/logos/" . $data['societe']["soc_logo_2"] . "' />");
                                                } ?>
                                            </div>

                                        <?php } ?>
                            </div>
                        <div id="head_0" class="head" >

                            <table style="margin-bottom:20px;">
                                <tr>
                                    <td style="width:70%;" >
                                        <h3 style="margin-bottom:0px;">Adresse de facturation</h3>
                                        <div style="margin-bottom:10px;">
                                            <strong><?= $data['com_fac_nom'] ?></strong><br>
                                            <?= $data['com_fac_adr1'] ?>
                                            <?php if(!empty($data['com_fac_adr2'])){ ?>
                                                <br> <?= $data['com_fac_adr2'] ?>
                                            <?php } ?>
                                            <?php if(!empty($data['com_fac_adr3'])){ ?>
                                                <br> <?= $data['com_fac_adr3'] ?>
                                            <?php } ?>
                                            <br><?= $data['com_fac_cp'] ?> <?= $data['com_fac_ville'] ?>
                                        </div>
                                        <h3 style="margin-bottom:0px;">Adresse de livraison</h3>
                                        <div style="margin-bottom:10px;">
                                            <strong><?= $data['com_liv_nom'] ?></strong><br>
                                            <?= $data['com_liv_adr1'] ?>
                                            <?php if(!empty($data['com_liv_adr2'])){ ?>
                                                <br> <?= $data['com_liv_adr2'] ?>
                                            <?php } ?>
                                            <?php if(!empty($data['com_liv_adr3'])){ ?>
                                                <br> <?= $data['com_liv_adr3'] ?>
                                            <?php } ?>
                                            <br><?= $data['com_liv_cp'] ?> <?= $data['com_liv_ville'] ?>
                                        </div>
                                    </td>
                                    <td style="width:30%;">
                                        <h3 style="margin-bottom:0px;"><?= $data['fournisseur']['fou_nom'] ?></h3>
                                        <div style="margin-bottom:40px;">
                                            <?= $data['fournisseur_adresse']['fad_ad1'] ?>
                                            <?php if(!empty($data['fournisseur_adresse']['fad_ad2'])){ ?>
                                                <br> <?= $data['fournisseur_adresse']['fad_ad2'] ?>
                                            <?php } ?>
                                            <?php if(!empty($data['fournisseur_adresse']['fad_ad3'])){ ?>
                                                <br> <?= $data['fournisseur_adresse']['fad_ad3'] ?>
                                            <?php } ?>
                                            <br><?= $data['fournisseur_adresse']['fad_cp'] ?> <?= $data['fournisseur_adresse']['fad_ville'] ?>
                                        </div>
                                        <h3 style="margin-bottom:0px;">Affaire suivie par : </h3>
                                        <div style="margin-bottom:10px;">
                                            <?= $data['ordre']['uti_prenom'] ?> <?= $data['ordre']['uti_nom'] ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                            <table class="mt20">

                                <tr>
                                    <td style="width:30%;background-color:#333333;color:#FFFFFF;text-align:center;" ><strong>Numéro : </strong><?= $data['com_numero'] ?></td>
                                    <td style="width:40%;" > </td>
                                    <td style="width:30%;background-color:#333333;color:#FFFFFF;text-align:center;" ><?= convert_date_fr($data['com_date']) ?></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                <table class="border mt20">
                    <thead   id="ligne_header_0">
                        <tr style="border:1px solid #333333">
                            <th style="width:25%;height:20px;" >Référence</th>
                            <th style="width:35%;height:20px;" >Désignation</th>
                            <th style="width:15%;height:20px;" >Prix HT</th>
                            <th style="width:5%;height:20px;" >Qté</th>
                            <th style="width:20%;height:20px;">Montant HT</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data['lignes'] as $k => $l){ ?>
                            <tr>
                                <td style="width:25%;" ><?php if(!empty($l['pro_code_produit'])){ ?><?= $l['pro_code_produit'] ?><?php }else{ ?><?= $l['cli_reference'] ?><?php } ?></td>
                                <td style="width:35%;" >
                                <strong><?= $l['cli_libelle'] ?></strong><br><?= $l['cli_descriptif'] ?>
                                </td>
                                <td style="width:15%;text-align:right;"><?= number_format($l['cli_pu'], 2, ',', ' ') ?> €</td>
                                <td style="width:5%;text-align:center;">
                                <?=str_replace(",00", "", (string)number_format ($l['cli_qte'], 2, ",", "")); ?>
                                </td>
                                <td style="width:20%;text-align:right;"><?= number_format($l['cli_ht'], 2, ',', ' ') ?> €</td>


                            </tr>
                    <?php   } ?>


                    </tbody>
                </table>

                <div id="foot_0" >
                    <table class="border mt20">

                            <tr>
                                <td style="width:40%;border-left:none;border-bottom:none;border-top:none;" >

                                </td>
                                <td style="width:30%;text-align:center;background-color:#333333;color:#FFFFFF;" >


                                        Total HT

                                </td>
                                <td style="width:30%;text-align:right;" >


                                        <?= number_format($data['com_ht'], 2, ',', ' '); ?> €

                                </td>
                            </tr>
                            <?php foreach($data['total_lignes'] as $k => $v){ ?>
                            <tr>
                                <td style="width:40%;border-left:none;border-bottom:none;border-top:none;" >

                                </td>

                                    <td style="width:30%;text-align:center;background-color:#333333;color:#FFFFFF;" >
                                        TVA (<?= $v ?> %)
                                    </td>
                                    <td style="width:30%;text-align:right;" >
                                        <?= number_format($data['com_ttc'] - $data['com_ht'], 2, ',', ' '); ?> €
                                    </td>

                            </tr>
                            <?php } ?>
                            <tr>
                                <td style="width:40%;border-left:none;border-bottom:none;border-top:none;" >

                                </td>
                                <td style="width:30%;text-align:center;background-color:#333333;color:#FFFFFF;" >
                                        Total TTC
                                </td>
                                <td style="width:30%;text-align:right;" >


                                        <?= number_format($data['com_ttc'], 2, ',', ' '); ?> €

                                </td>
                            </tr>
                    </table>
                <!-- <div style="position:absolute;bottom:10px;left:0;border:1px solid white;" > -->
                    <table>
                        <tr>
                            <td style="width:30%;" >
                                <div class="cadre" >
                                    <h4 style="margin:0;font-size:12px;">Condition de règlement</h4>
                                    <?php if(!empty($data['com_reg_type'])){ ?>

                                        <?php foreach($data['base_reglement'] as $k => $n){ ?>
                                            <?php if($k > 0){ ?>
                                                <?php if($data['com_reg_type'] == $k){ ?><?= $n ?><?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if($data['com_reg_formule'] == 1){ ?>

                                        Date + <?php if(!empty($data['com_reg_formule'])){ ?><?= $data['com_reg_nb_jour'] ?><?php }else{ ?>0<?php } ?> jours

                                    <?php } ?>
                                    <?php if($data['com_reg_formule'] == 2){ ?>
                                        Date + 45j + fin de mois
                                    <?php } ?>
                                    <?php if($data['com_reg_formule'] == 3){ ?>
                                        Date + fin de mois + 45j
                                    <?php } ?>
                                    <?php if($data['com_reg_formule'] == 4){ ?>
                                        Date + <?php if(!empty($data['com_reg_nb_jour'])){ ?><?= $data['com_reg_nb_jour'] ?><?php }else{ ?>0<?php } ?> jours + fin de mois + <?php if(!empty($data['com_reg_fdm'])){ ?><?= $data['com_reg_fdm'] ?><?php }else{ ?>0<?php } ?> jour(s)
                                    <?php } ?>
                                </div>
                            </td>
                            <td style="width:35%;">
                                <div class="cadre" style="height:100px;">
                                    <h4 style="margin:0;width:100%;font-size:12px;">Signature et cachet du fournisseur</h4>

                                </div>
                            </td>
                            <td style="width:35%;" >
                                <div class="cadre" style="height:100px;text-align:center;position: relative;top: 0;left: 0;">
                                    <h4 style="margin:0;width:100%;font-size:12px;">Signature et cachet du donneur d'ordre</h4>
						<?php       if(!empty($data['agence']) && file_exists("documents/societes/signatures/tampons/age_".$data['agence']['age_id'] . ".png")){ ?>
                                        <img style='max-width:60%;position: relative;top: 0;left: 0;' src='documents/societes/signatures/tampons/age_<?=$data['agence']['age_id']?>.png'/>
                                        <img style="position:absolute;top:10px;left:35px;max-width:70%;" src="documents/utilisateurs/signatures/signature_<?=$data['com_donneur_ordre']?>.png">
                        <?php 		}elseif(file_exists("documents/societes/signatures/tampons/soc_".$data['societe']['soc_id'] . ".png")){ ?>
                                        <img style='max-width:60%;position: relative;top: 0;left: 0;' src='documents/societes/signatures/tampons/soc_<?=$data['societe']['soc_id']?>.png'/>
                                        <img style="position:absolute;top:10px;left:35px;max-width:70%;" src="documents/utilisateurs/signatures/signature_<?=$data['com_donneur_ordre']?>.png">
						<?php 		} ?>
                                </div>
                            </td>
                        </tr>
                    </table>
               <!--  </div> -->
               </div>
               <div id="footer_0" >
                    <footer>
                    <hr/>
                    <table>
                        <tr>
                        <?php if(!empty($data['agence'])){ ?>
                                <td style="width:10%;max-height:35px" >
                        <?php       if(!empty($data['agence']["age_qualite_1"])){
                                        echo("<img style='max-width:100%;' src='/documents/societes/logos/" . $data['agence']["age_qualite_1"] . "' />");
                                    } ?>
                                </td>
                                <td style="width:80%;text-align:center;font-size:10px;line-height:12px;" >

                                    <?= $data['societe']['soc_nom'] ?> <?= $data['agence']['age_nom'] ?> - <?= $data['societe']['soc_type'] ?> au capital de <?= $data['societe']['soc_capital'] ?> € - APE <?= $data['societe']['soc_ape'] ?> - RCS <?= $data['societe']['soc_rcs'] ?><br>
                                    Siret <?= $data['agence']['age_siret'] ?> - N° TVA <?= $data['societe']['soc_tva'] ?><br>
                                    N° déclaration Existence <?= $data['societe']['soc_num_existence'] ?>

                                </td>
                                <td style="width:10%;text-align:right;max-height:35px" >
                        <?php       if(!empty($data['agence']["age_qualite_2"])){
                                        echo("<img style='max-width:100%;' src='/documents/societes/logos/" . $data['agence']["age_qualite_2"] . "' />");
                                    } ?>
                                </td>

                        <?php       }elseif(!empty($data['societe'])){ ?>
                                <td style="width:10%;max-height:35px" >
                        <?php       if(!empty($data['societe']["soc_qualite_1"])){
                                        echo("<img style='max-width:100%;' src='/documents/societes/logos/" . $data['societe']["soc_qualite_1"] . "' />");
                                    } ?>
                                </td>
                                <td style="width:80%;text-align:center;font-size:10px;line-height:12px;" >

                                    <?= $data['societe']['soc_nom'] ?> - <?= $data['societe']['soc_type'] ?> au capital de <?= $data['societe']['soc_capital'] ?> € - APE <?= $data['societe']['soc_ape'] ?> - RCS <?= $data['societe']['soc_rcs'] ?><br>
                                    Siret <?= $data['societe']['soc_siren'] ?> <?= $data['societe']['soc_siret'] ?> - N° TVA <?= $data['societe']['soc_tva'] ?><br>
                                    N° déclaration Existence <?= $data['societe']['soc_num_existence'] ?>

                                </td>
                                <td style="width:10%;text-align:right;max-height:35px;" >
                        <?php       if(!empty($data['societe']["soc_qualite_2"])){
                                        echo("<img style='max-width:100%;' src='/documents/societes/logos/" . $data['societe']["soc_qualite_2"] . "' />");
                                    } ?>
                                </td>

                                <?php }

                                ?>

                            </tr>
                        </table>
                    </footer>
                </div>
            </div>
        </section>

        <footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
							<a href="commande_voir.php?commande=<?= $_GET['commande'] ?>" class="btn btn-sm btn-default" >
								<i class="fa fa-left-arrow"></i> Retour
							</a>

				</div>
				<div class="col-xs-6 footer-middle text-center" >

					<button type="button" class="btn btn-sm btn-info ml15" id="print" >
						<i class="fa fa-print"></i> Imprimer
					</button>

				</div>
				<div class="col-xs-3 footer-right" >
				</div>
			</div>
		</footer>
                    <?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>

		<script type="text/javascript" src="assets/js/print.js"></script>
        <script>
            var l_page=21;
            var h_page=29;


            var _
        </script>
    </body>
</html>