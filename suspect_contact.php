<?php

// EDITION D'UN CONTACT SUSPECT

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_parametre.php');
include('modeles/mod_orion_con_fonctions.php');

include('modeles/mod_get_suspect_adresses.php');


if(!empty($_GET["suspect"])){
	$suspect=intval($_GET["suspect"]);
}else{
	echo("Erreur!");
	die();
}

$contact=0;
if(!empty($_GET["contact"])){
	$contact=intval($_GET["contact"]);	
};

// sur le suspect
$sql="SELECT sus_code,sus_categorie FROM Suspects WHERE sus_id=" . $suspect . ";";
$req=$ConnSoc->query($sql);
$d_suspect=$req->fetch();
if(empty($d_suspect)){
	echo("Erreur!");
	die();
}

// les fonctions du contact
$d_contact_fonctions=orion_con_fonctions();

$d_contact=array(
	"sco_titre" => 0,
	"sco_nom" => "",
	"sco_prenom" => "",
	"sco_fonction" => 0,
	"sco_fonction_nom" => "",
	"sco_tel" => "",
	"sco_portable" => "",
	"sco_fax" => "",
	"sco_mail" => "",
	"sco_comment" => "",
	"sco_defaut" => 0,
	"sco_compta" => 0,
	"sco_adresse" =>0
);




if($contact>0){
	
	$sql="SELECT * FROM Suspects_Contacts WHERE sco_id=" . $contact . " AND sco_ref_id=" . $suspect . ";";
	$req=$ConnSoc->query($sql);
	$d_contact=$req->fetch();
}

// LES LIEUX d'INTERVENTION

$adresses_int = get_suspect_adresses($suspect,1); 
$_SESSION['retourContact'] = "suspect_voir.php?suspect=" . $suspect;
// gestion du retour
if(isset($_GET["onglet"])){
	$_SESSION['retourContact'].="&onglet=" . $_GET["onglet"];
}
?>
<!DOCTYPE html>
<html>  
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote-bs3.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		
		<form action="suspect_contact_enr.php" method="post" id="form_contact" class="admin-form form-inline form-inline-grid" >
			<div>
				<input type="hidden" name="suspect" value="<?=$suspect?>" />
				<input type="hidden" name="contact" value="<?=$contact?>" />					
			</div>
			<div id="main">
		<?php 	include "includes/header_def.inc.php"; ?>
		
				<section id="content_wrapper" class="">
					<section id="content" class="">						
						<div class="row">
							<div class="col-md-10 col-md-offset-1">							
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
									
										<!-- contenu du form -->
										<div class="panel-body bg-light">
										
											<div class="content-header">
												<h2>
													<b class="text-primary"><?=$d_suspect["sus_code"]?></b> -
											<?php	if($contact>0){ ?>
														 Edition d'un 
											<?php	}else{ ?>
														Nouveau
											<?php	} ?>
													<b class="text-primary">contact</b>
												</h2>
											</div>
											
											<div class="row">
												<div class="col-md-4">
													<label for="sco_titre" >Civilité : </label>
													<select id="sco_titre" name="sco_titre" class="form-control">
											<?php 		foreach($base_civilite as $b => $c){
															if($b > 0){ ?>
																<option value="<?= $b ?>"><?= $c ?></option>
											<?php			}
														} ?>
													</select>
												</div>																			
												<div class="col-md-4">
													<label for="sco_nom" >Nom : </label>
													<input type="text" id="sco_nom" name="sco_nom" class="gui-input nom" placeholder="Nom" required value="<?=$d_contact['sco_nom']?>" />
												</div>
												<div class="col-md-4">
													<label for="sco_prenom" >Prénom : </label>
													<input type="text" id="sco_prenom" name="sco_prenom" class="gui-input prenom" placeholder="Prénom" value="<?=$d_contact['sco_prenom']?>"  />												
												</div>
											</div>
											<div class="row mt15">											
												<div class="col-md-4">
													<label for="sco_titre" >Fonction : </label>
													<select id="sco_fonction" name="sco_fonction" class="form-control" >
														<option value="0">Sélectionner une fonction...</option>
											<?php		foreach($d_contact_fonctions as $fk => $fv){
															if($d_contact["sco_fonction"]==$fk){
																echo("<option value='" . $fk . "' selected >" . $fv["cfo_libelle"] . "</option>");
															}else{
																echo("<option value='" . $fk . "' >" . $fv["cfo_libelle"] . "</option>");
															}
														} ?>
														<option value="autre" <?php if(!empty($d_contact['sco_fonction_nom'])) echo("selected"); ?> >Autre...</option>
													</select>
												</div>
												<div class="col-md-4" id="autre_fonction" <?php if(empty($d_contact['sco_fonction_nom'])) echo("style='display:none;'"); ?> >
													<label for="autre_fonction" >Autre fonction : </label>
													<div class="field prepend-icon">
														<input type="text" name="sco_fonction_nom" id="sco_fonction_nom" class="gui-input" placeholder="Nouvelle fonction" value="<?=$d_contact['sco_fonction_nom']?>" />
														<label for="sco_fonction_nom" class="field-icon">
															<i class="fa fa-tag"></i>
														</label>
													</div>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-4">
													<label class="field prepend-icon">
														<input type="text" name="sco_tel" id="sco_tel" class="gui-input telephone" placeholder="Tél" value="<?=$d_contact['sco_tel']?>" />
														<label for="sco_tel" class="field-icon">
															<i class="fa fa-phone"></i>
														</label>
													</label>
												</div>
												<div class="col-md-4">
													<label class="field prepend-icon">
														<input type="text" name="sco_portable" id="sco_portable" class="gui-input telephone" placeholder="Portable" value="<?=$d_contact['sco_portable']?>" />
														<label for="sco_portable" class="field-icon">
															<i class="fa fa-mobile"></i>
														</label>
													</label>
												</div>
												<div class="col-md-4">
													<label class="field prepend-icon">
														<input type="text" name="sco_fax" id="sco_fax" class="gui-input telephone" placeholder="Fax" value="<?=$d_contact['sco_fax']?>" />
														<label for="sco_fax" class="field-icon">
															<i class="fa fa-fax"></i>
														</label>
													</label>
												</div>
											</div>
											<div class="row mt15 mb15">
												<div class="col-md-12">									
													<label class="field prepend-icon">
														<input type="email" name="sco_mail" id="sco_mail" class="gui-input" placeholder="Mail" value="<?=$d_contact['sco_mail']?>" />
														<label for="sco_mail" class="field-icon">
															<i class="fa fa-envelope-o"></i>
														</label>
													</label>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<label class="option block mn">
														  <input type="checkbox" name="sco_compta" value="on" <?php if($d_contact['sco_compta']==1) echo("checked"); ?> />
														  <span class="checkbox mn"></span> Contact "impayé"
													</label>
												</div>
											</div>
												
											
											<div class="section-divider" >
												<span>Commentaire</span>
											</div>
											<div class="row mt15">
												<div class="col-md-12">
													 <textarea class="gui-textarea summernote" id="sco_comment" name="sco_comment" placeholder="Commentaire"><?=$d_contact['sco_comment']?></textarea>
												</div>
											</div>
					
										</div>
										<!-- fin de contenu form -->
										
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-4 footer-left">
						<a href="<?=$_SESSION['retourContact']?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-4 footer-middle"></div>
					<div class="col-xs-4 footer-right">
						<button type="submit" class="btn btn-success btn-sm">
							<i class='fa fa-floppy-o'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php
		include "includes/footer_script.inc.php"; ?>   
		
		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script> 
		<script type="text/javascript" src="/vendor/plugins/mask/jquery.mask.js" ></script>
		<script type="text/javascript" src="/assets/js/custom.js" ></script>
		<script type="text/javascript">		
			jQuery(document).ready(function(){
				
				$("#sco_fonction").change(function(){
					if($(this).val()=="autre"){
						$("#autre_fonction").show();
					}else{
						$("#autre_fonction").hide();
					}
				});
				
				$("#sco_adresse").change(function(){
					if($(this).val()>0){
						$("#sco_defaut").prop("disabled",false);
					}else{
						$("#sco_defaut").prop("checked",false);
						$("#sco_defaut").prop("disabled",true);
					}
				});
				
			});
			
		</script>
	</body>
</html>
