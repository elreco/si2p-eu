<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_presence_conso_data.php');
include('modeles/mod_get_juridique.php');




// VISUALISATION D'UNE CONVOCATIONS

$erreur_txt="";

$action_id=0;
if(!empty($_GET["action"])){
	$action_id=intval($_GET["action"]);
}

$action_client_id=0;
if(!empty($_GET["action_client"])){
	$action_client_id=intval($_GET["action_client"]);
}

if(empty($action_id)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();

}else{

	// personne connecte
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	if($conn_soc_id!=$acc_societe){
		$acc_societe=$conn_soc_id;
	}

	$data=presence_conso_data($action_id,$action_client_id);

	if(empty($data)){
		$erreur_txt="impossible d'afficher la page";
		echo($erreur_txt);
		die();
	}
}
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >

			/* GENERIQUE */

			#zone_print{
				display:none;
				margin:0px!important;
			}
			#container_print{
				margin:auto;
				padding:0.5cm; /* remplace marge d'impression */
				background-color:#fff;
			}
			#page_print{
				width:100%;
				font-family: Arial;
			}

			#page_print h1{
				font-family: Arial;
				font-size: 16pt;
				color: #e31936;
			}
			#page_print h2{
				font-family: Arial;
				font-size: 14pt;
				color: #7e8082;
			}
			#page_print .txt-strong{
				color: #e31936;
				font-weight: bold;
			}
			#page_print table{
				width:100%;
			}

			/* HEADER */

			#page_print header img{
				max-width:4cm;
				max-height:2.5cm;
			}

			/* CONTENU */

			.ligne{
				display:table;
				width:100%;
				border-bottom:1px solid #333;
				border-spacing:0px;
				border-collapse:0px;
			}
			.ligne > div{
				display:table-cell;
				border-spacing:0px;
				border-collapse:0px;
				border-right:1px solid #333;

			}
			.ligne > div:first-child{
			  border-left:1px solid #333;
			}
			.ligne-head{
				background-color:#333;
				color:#FFF;
			}

			/* FOOTER */

			#page_print footer{
				font-size:7pt;
			}
			#page_print footer p,hr{
				margin:0px;
				padding:0px;
			}
			#page_print .saut{
				page-break-after:always;
			}
			#page_print .saut-page{
				page-break-after:always;
			}

			/* MISE EN PAGE */

			.w25{
				width:25%;
			}
			.w50{
				width:50%;
			}
			.w30{
				width:30%!important;
			}
			.w40{
				width:40%!important;
			}
			p5{
				padding:5px;
			}

			/* SPE FEUILLE DE PRESENCE */
			#container_print{
				width:29.7cm;
			}
			.ligne-col-1{
				width:25%;
			}
			.ligne-col-2{
				width:15%;
			}
			.ligne-col-3{
				width:12%;
			}
			.ligne-signature{
				height:45px;
			}
			.ligne-signature > div{
				vertical-align:middle;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 landscape;
				margin:5mm;
			}
			html{
				background-color:#fff!important;
				margin:0px!important;
				padding:0px!important;;
			}
			body{
				background-color:#fff!important;
				margin:0px!important;;
				padding:0px!important;;
			}

		</style>
	</head>
	<body class="sb-top sb-top-sm document-affiche">
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php 		if(empty($erreur_txt)){ ?>

							<div class="row" >

								<!-- zone d'affichage du devis -->

								<div class="col-md-8"  >

									<div id="container_print" >

										<div id="page_print" >

								<?php
											/*echo("<pre>");
												print_r($data);
											echo("</pre>");
											die();*/
											foreach($data["pages"] as $p => $page){ ?>

												<div class="page" id="header_<?=$p?>" data-page="<?=$p?>" >
													<header>
														<table>
															<tr>
																<td class="w30" >
														<?php		if(!empty($page["client_logo"])){  ?>
																		<img src="<?=$page["client_logo"]?>" />
														<?php		}elseif(!empty($data["juridique"]["logo_1"])){ ?>
																		<img src="<?=$data["juridique"]["logo_1"]?>" />
														<?php		}else{
																		echo("&nbsp;");
																	} ?>
																</td>
																<td class="w40" >
																	<h1 class="text-center" >FEUILLE D'EMARGEMENT</h1>
																</td>
																<td class="w30" >
														<?php		if(!empty($data['action']["act_adr_nom"])){
																		echo("<br/>" . $data['action']["act_adr_nom"]);
																	}
																	if(!empty($data['action']["act_adr_service"])){
																		echo("<br/>" . $data['action']["act_adr_service"]);
																	}
																	if(!empty($data['action']["act_adr1"])){
																		echo("<br/>" . $data['action']["act_adr1"]);
																	}
																	if(!empty($data['action']["act_adr2"])){
																		echo(", " . $data['action']["act_adr2"]);
																	}
																	if(!empty($data['action']["act_adr3"])){
																		echo(", " . $data['action']["act_adr3"]);
																	}
																	echo("<br/>" . $data['action']["act_adr_cp"] . " " . $data['action']["act_adr_ville"]); ?>
																</td>
															</tr>
														</table>
														<table>
															<tr>
																<td class="w25" >
																	Client : <strong><?=$page["cli_nom"]?></strong>
																</td>
																<td class="w50" >
																	Formation : <strong><?=$page["acl_pro_libelle"]?></strong>
																</td>
																<td class="w25" >
																	Durée : <strong><?=$page["duree"]?></strong>
																</td>
															</tr>
														</table>
													</header>
												</div>

												<div id="ligne_header_<?=$p?>" >
													<div class="ligne ligne-head" >
														<div class="ligne-col-1 text-center" >
												<?php		if($page["pda_categorie"]==2){
																echo("Testeur (1)");
															}else{
																echo("Formateur (1)");
															} ?>
														</div>
														<div class="ligne-col-2" >&nbsp;</div>
									<?php				$col=0;
														for($col=0;$col<=4;$col++){
															if(!empty($page["sessions"][$col])){
																echo("<div class='ligne-col-3 text-center' >" . $page["sessions"][$col]["h"] . "</div>");
															}else{
																echo("<div class='ligne-col-3' >&nbsp;</div>");
															}
														} ?>
													</div>
													<div class="ligne ligne-signature" >
														<div class="ligne-col-1 p5" ><?=$page["int_identite"]?></div>
														<div class="ligne-col-2" >&nbsp;</div>


									<?php				$col=0;
														for($col=0;$col<=4;$col++){
															if(!empty($page["sessions"][$col]["int_signature"])){
																$marge_left=rand(-25,25);
																$marge_top=rand(-20,0); ?>

																<div class='ligne-col-3' >
																	<div style="height:44px;text-align:center;max-width:100%;" >
																		<img src="<?=$page["sessions"][$col]["int_signature"]?>" style="max-height:100%!important;max-width:100%;" >
																	</div>
																</div>
									<?php					}else{
																echo("<div class='ligne-col-3' >&nbsp;</div>");
															}

														} ?>
													</div>
													<div class="ligne ligne-head" >
														<div class="ligne-col-1 text-center" >Stagiaires (2)</div>
														<div class="ligne-col-2 text-center" >Date de naiss.</div>
									<?php				$col=0;
														for($col=0;$col<=4;$col++){
															if(!empty($col_date[$col])){
																echo("<div class='ligne-col-3' >" . $page["sessions"][$col] . "</div>");
															}else{
																echo("<div class='ligne-col-3' >&nbsp;</div>");
															}
														} ?>
													</div>
												</div>
												<?php if(!empty($page['acl_annule']) OR !empty($page['ase_annule'])){ ?>
														<h1 class="text-center" style="color:#e31936;position:absolute; opacity:0.5;left:30%;font-size:50px;margin-top:13%;">FORMATION ANNULÉE</h1>
												<?php } ?>
										<?php	if(!empty($page["stagiaires"])){

													asort($page["stagiaires"]);

													foreach($page["stagiaires"] as $sta){
														$sta_naissance="";
														$DT_naiss=date_create_from_format('Y-m-d',$sta["sta_naissance"]);
														if(!is_bool($DT_naiss)){
															$sta_naissance=$DT_naiss->format("d/m/Y");
														} ?>
														<div class="ligne ligne-signature ligne-body-<?=$p?>" >
															<div class="ligne-col-1 p5" ><?=$sta["sta_nom"] . " " . $sta["sta_prenom"]?></div>
															<div class="ligne-col-2 text-center" ><?=$sta_naissance?></div>
										<?php				for($col=0;$col<=4;$col++){
																if(!empty($sta[$col])){
																	$marge_left=rand(-25,25);
																	$marge_top=rand(-20,0); ?>

																	<div class='ligne-col-3' >
																		<div style="height:44px;text-align:center;max-width:100%;" >
																			<img src="<?=$sta[$col]?>" style="max-height:100%!important;max-width:100%;" >
																		</div>
																	</div>
										<?php					}else{
																	echo("<div class='ligne-col-3' >&nbsp;</div>");
																}
															} ?>
														</div>
										<?php		}
												}
												// on complete avec des ligne vide
												if(empty($page["stagiaires"])){
													$bcl=0;
												}elseif(count($page["stagiaires"])<10){
													$bcl=count($page["stagiaires"]);
												}elseif(count($page["stagiaires"])>10){
													$bcl=count($page["stagiaires"]) % 10;
												}else{
													$bcl=11;
												}
												for($bcl;$bcl<10;$bcl++){ ?>
													<div class="ligne ligne-signature ligne-body-<?=$p?>" >
														<div class="ligne-col-1 p5" >&nbsp;</div>
														<div class="ligne-col-2 text-center" >&nbsp;</div>
									<?php				for($col=0;$col<=4;$col++){
															echo("<div class='ligne-col-3' >&nbsp;</div>");
														} ?>
													</div>
									<?php		} ?>


												<div id="footer_<?=$p?>" <?php if($p<count($data["pages"])-1) echo("class='saut-page'"); ?> >
													<footer>
														<p class="text-center" >
												<?php		if($page["pda_categorie"]==2){
																echo("(1) - Par ma signature, j'atteste avoir reçu le test ci-dessus référencé et avoir reçu en main propre une attestation de formation.");
																echo("(2) - Par ma signature, j'atteste avoir dispensé le test ci-dessus.");
															}else{
																echo("(1) - Par ma signature, j'atteste avoir reçu la formation ci-dessus référencée et il m'a été remis en main propre une attestation de formation.");
																echo("(2) - Par ma signature, j'atteste avoir dispensé la formation ci-dessous.");
															} ?>
														</p>
														<hr/>
														<p class="text-center" >
															<?=$data["juridique"]["nom"]?> <span class="footer-bar" >|</span>
													<?php	if(!empty($data["juridique"]["agence"])){ ?>
																<?=$data["juridique"]["agence"]?> <span class="footer-bar" >|</span>
													<?php	} ?>
															<?=$data["juridique"]["ad1"] . " " .$data["juridique"]["ad2"] . " " . $data["juridique"]["ad3"]?> <span class="footer-bar" >|</span>
															<?=$data["juridique"]["cp"] . " " . $data["juridique"]["ville"]?> <span class="footer-bar" >|</span>
															TEL. <?=$data["juridique"]["tel"]?> <span class="footer-bar" >|</span>
															FAX. <?=$data["juridique"]["fax"]?>
														</p>
													</footer>
												</div>

							<?php			} ?>
										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->

								<div class="col-md-4" >
									<!-- DEBUT ANNEXE -->

									<!-- FIN ANNEXE -->
								</div>

							</div>


		<?php		}else{ ?>

						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>

		<?php		} ?>

				</section>

				<!-- End: Content -->
			</section>


		</div>
		<!-- End: Main -->

		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="action_voir.php?action=<?=$action_id?>&onglet=3&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
					<button type="button" class="btn btn-sm btn-info ml15" id="print" >
						<i class="fa fa-print"></i> Imprimer
					</button>
				</div>
				<div class="col-xs-3 footer-right" ></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="assets/js/print.js"></script>
		<script type="text/javascript">
			var l_page=29.7;
			var h_page=19.5;
		</script>
	</body>
</html>
