<?php

	// ENREGISTREMENT D'UNE NOUVELLE QUESTION OU DES MAJ D'UNE QUESTION EXISTANTE
	
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	
	$erreur_txt="";
	
	
	if(!empty($_POST)){
		
		$question=0;
		if(!empty($_POST["question"])){
			$question=intval($_POST["question"]);	
		}
		
		$aqu_texte="";
		if(!empty($_POST["aqu_texte"])){
			$aqu_texte=$_POST["aqu_texte"];	
		}else{
			$erreur_txt="1-Formulaire incomplet!";	
		}
		
		$aqu_min_lib="";
		if(!empty($_POST["aqu_min_lib"])){
			$aqu_min_lib=$_POST["aqu_min_lib"];	
		}else{
			$erreur_txt="2-Formulaire incomplet!";	
		}
		
		$aqu_min_val=0;
		if(!empty($_POST["aqu_min_val"])){
			$aqu_min_val=intval($_POST["aqu_min_val"]);	
		}
		
		$aqu_max_lib="";
		if(!empty($_POST["aqu_max_lib"])){
			$aqu_max_lib=$_POST["aqu_max_lib"];	
		}else{
			$erreur_txt="3-Formulaire incomplet!";	
		}
		
		$aqu_max_val=0;
		if(!empty($_POST["aqu_max_val"])){
			$aqu_max_val=intval($_POST["aqu_max_val"]);	
		}
		if($aqu_max_val==0){
			$erreur_txt="4-Formulaire incomplet!";		
		}
		
		$aqu_rubrique=0;
		if(!empty($_POST["aqu_rubrique"])){
			$aqu_rubrique=intval($_POST["aqu_rubrique"]);	
		}
		if($aqu_rubrique==0){
			$erreur_txt="4-Formulaire incomplet!";		
		}
		
		$aqu_valide=0;
		if(!empty($_POST["aqu_valide"])){
			$aqu_valide=1;	
		}
		
		function get_max_place($rubrique){
			
			global $Conn;
			
			$place=1;
			$sql="SELECT MAX(aqu_place) FROM Avis_Questions WHERE aqu_rubrique=" . $rubrique . ";"; 
			$req=$Conn->query($sql);
			$result=$req->fetch();
			if(!empty($result)){
				$place=$result[0]+1;
			}
			
			return $place;
		}
		
		
		if(empty($erreur_txt)){
			
			if($question==0){
				
				// recherche de la place
				
				$place=get_max_place($aqu_rubrique);
	
				// nouvelle question
				
				$sql="INSERT INTO Avis_Questions (aqu_texte,aqu_min_lib,aqu_min_val,aqu_max_lib,aqu_max_val,aqu_valide,aqu_rubrique,aqu_place) 
				VALUES(:aqu_texte,:aqu_min_lib,:aqu_min_val,:aqu_max_lib,:aqu_max_val,:aqu_valide,:aqu_rubrique,:aqu_place);";
				$req=$Conn->prepare($sql);
				$req->bindParam("aqu_texte",$aqu_texte);
				$req->bindParam("aqu_min_lib",$aqu_min_lib);
				$req->bindParam("aqu_min_val",$aqu_min_val);
				$req->bindParam("aqu_max_lib",$aqu_max_lib);
				$req->bindParam("aqu_max_val",$aqu_max_val);
				$req->bindParam("aqu_valide",$aqu_valide);
				$req->bindParam("aqu_rubrique",$aqu_rubrique);
				$req->bindParam("aqu_place",$place);
				try{
					$req->execute();
				}Catch(Exception $e){
					$erreur_txt=$e->getMessage();
				}
				
			}else{
				
				// donnee sur la question avant modif
				
				$sql="SELECT aqu_rubrique,aqu_place FROM Avis_Questions WHERE aqu_id=" . $question . ";"; 
				$req=$Conn->query($sql);
				$d_question=$req->fetch();
				if(!empty($d_question)){
					if($d_question["aqu_rubrique"]!=$aqu_rubrique){
						
						// changement de rubrique
						
						// calcul de la nouvelle place						
						$place=get_max_place($aqu_rubrique);
						
						// inutile de recalculer les places de l'ancienne rubrique
						
						
					}else{
						$place=$d_question["aqu_place"];
					}
				}
				
				
				// maj question
				
				$sql="UPDATE Avis_Questions SET 
				aqu_texte=:aqu_texte,
				aqu_min_lib=:aqu_min_lib,
				aqu_min_val=:aqu_min_val,
				aqu_max_lib=:aqu_max_lib,
				aqu_max_val=:aqu_max_val,				
				aqu_valide=:aqu_valide,
				aqu_rubrique=:aqu_rubrique,
				aqu_place=:aqu_place   				
				WHERE aqu_id=:question AND aqu_valide=0";
				$req=$Conn->prepare($sql);
				$req->bindParam("aqu_texte",$aqu_texte);
				$req->bindParam("aqu_min_lib",$aqu_min_lib);
				$req->bindParam("aqu_min_val",$aqu_min_val);
				$req->bindParam("aqu_max_lib",$aqu_max_lib);
				$req->bindParam("aqu_max_val",$aqu_max_val);
				$req->bindParam("aqu_valide",$aqu_valide);
				$req->bindParam("aqu_rubrique",$aqu_rubrique);
				$req->bindParam("aqu_place",$place);
				$req->bindParam("question",$question);
				try{
					$req->execute();					
				}Catch(Exception $e){
					$erreur_txt=$e->getMessage();
				}
				
			}
		}

	}else{
		$erreur_txt="A Formulaire incomplet!";	
	}
	
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur!",
			"type" => "danger",
			"message" => $erreur_txt 
		);
		print_r($_SESSION['message']);
		//die();
		header("location : param_avis_question.php?question=" . $question);
		
	}else{
		header("location : param_avis_question_liste.php");
	}

	
?>