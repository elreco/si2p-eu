<?php
include "includes/controle_acces.inc.php";

include("includes/connexion.php");
include("includes/connexion_fct.php");
include("modeles/mod_check_siret.php");

// MAJ D'UNE ADRESSE client

$client=0;
if(!empty($_POST["client"])){
	$client=intval($_POST["client"]);
}
$adresse=0;
if(!empty($_POST["adresse"])){
	$adresse=intval($_POST["adresse"]);
}

if(empty($client)){
	echo("Erreur!");
	die();
}

$erreur_txt="";

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);
}


if(empty($_POST['adr_cp']) OR empty($_POST['adr_ville'])){

	$erreur_txt="Impossible de mettre à jour l'adresse";

}else{


	// TRAITEMENT DU FORM
	// champ du form particulier dont la valuer peut être forcé selon le context


	$adr_type=1;
	if(!empty($_POST["adr_type"])){
		$adr_type=intval($_POST["adr_type"]);
	}

	$adr_nom="";
	if(!empty($_POST['adr_nom'])){
		$adr_nom=$_POST['adr_nom'];
	}

	$adr_ville="";
	if(!empty($_POST['adr_ville'])){
		$adr_ville=$_POST['adr_ville'];
	}

	$adr_defaut=0;
	if(!empty($_POST["adr_defaut"])){
		$adr_defaut=1;
	}

	$adr_libelle="";
	if(!empty($_POST["adr_libelle"])){
		$adr_libelle=$_POST["adr_libelle"];
	}

	$adr_geo=1;
	if(!empty($_POST["adr_geo"])){
		$adr_geo=intval($_POST["adr_geo"]);
	}

	$adr_siret="";
	if(!empty($_POST["adr_siret"])){
		$adr_siret=$_POST["adr_siret"];
	}

	// champ lié a la fiche client

	$cli_siren="";
	if(!empty($_POST["cli_siren"])){
		$cli_siren=$_POST["cli_siren"];
	}

	$cli_ident_tva="";
	if(!empty($_POST["cli_ident_tva"])){
		$cli_ident_tva=$_POST["cli_ident_tva"];
	}
	
	$check_siret=true;
	if(!empty($_SESSION['acces']['acc_droits'][36])){
		if(!empty($_POST["siret_no_check"])){
			$check_siret=false;
		}
	}

	// FIN TRAITEMENT FORM

	// INFO DB

	// le client
	$sql="SELECT cli_categorie,cli_sous_categorie,cli_nom,cli_siren,cli_ident_tva,cli_affacturable,cli_siret,cli_affacturage,cli_affacturage_iban,cli_interco_soc FROM clients WHERE cli_id=" . $client . ";";
	$req=$Conn->query($sql);
	$d_client=$req->fetch();
	if(!empty($d_client)){

		if(empty($adr_nom)){

			$adr_nom=$d_client["cli_nom"];

			if(!empty($d_client["cli_siren"])){
				$cli_siren=$d_client["cli_siren"];
			}

			if($adr_geo<2){
				$cli_ident_tva=$d_client["cli_ident_tva"];;
			}
		}
	}else{
		$erreur_txt="L'adresse n'a pas été mise à jour!";
	}

	if($adresse>0){
		// modification d'adresse
		$sql="SELECT adr_defaut,adr_type FROM adresses WHERE adr_id=" . $adresse . ";";
		$req=$Conn->query($sql);
		$d_adresse=$req->fetch();
		if(!empty($d_adresse)){

			$adr_type=$d_adresse["adr_type"];
			if($d_adresse["adr_defaut"]==1){
				$adr_defaut=1;
			}


		}else{
			$erreur_txt="L'adresse n'a pas été mise à jour!";
		}
	}else{
		// création

		$sql="SELECT adr_id FROM adresses WHERE adr_type=" . $adr_type . " AND adr_ref_id=" . $client . " AND adr_defaut;";
		$req=$Conn->query($sql);
		$d_adresse=$req->fetch();
		if(empty($d_adresse)){
			// pas d'adresse par defaut
			// on force la nouvelle adresse en adresse defaut
			$adr_defaut=1;

		}
	}


	if($adr_type==2){

		if(!empty($cli_siren) && !empty($adr_siret) AND $check_siret){

			$siret_ok=check_siret($cli_siren,$adr_siret,$adresse);
			
			if(!empty($siret_ok)){
				// FG 27/04/2020
				// check_siret retourne un tableau pas un boolean
				//if(!$siret_ok){
					$erreur_txt="Le siret est déjà affecté à une autre adresse de facturation";
				//}
			}else{

				// VERIF SIRET VIA API
				$siret = $cli_siren . $adr_siret;
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				// UTILISER LA V3
				curl_setopt($ch,CURLOPT_URL, "https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/" . preg_replace('/\s+/', '', $siret));
				$resultat_json=curl_exec($ch);
				curl_close($ch);
				$result_api=json_decode($resultat_json);
				if(empty($result_api->etablissement)){
					$erreur_txt="le siret  " . $cli_siren . " " . $adr_siret . " n'est pas reconnu par l'api du gouvernement";
				}
			}

		}
	}else{
		$adr_siret="";
	}

	if(empty($adr_libelle)){
		$adr_libelle=$adr_nom . " " . $adr_ville;
	}

	// ENREGISTREMENT

	IF(empty($erreur_txt)){

		if($adresse>0){

			// MODIF

			$sql="UPDATE adresses SET
			adr_nom=:adr_nom,
			adr_service=:adr_service,
			adr_ad1=:adr_ad1,
			adr_ad2=:adr_ad2,
			adr_ad3=:adr_ad3,
			adr_cp=:adr_cp,
			adr_ville=:adr_ville,
			adr_defaut=:adr_defaut,
			adr_libelle=:adr_libelle,
			adr_siret=:adr_siret,
			adr_geo=:adr_geo
			WHERE adr_id=" . $adresse . " AND adr_ref_id=" . $client . ";";
			$req = $Conn->prepare($sql);
			$req->bindParam("adr_nom",$adr_nom);
			$req->bindParam("adr_service",$_POST['adr_service']);
			$req->bindParam("adr_ad1",$_POST["adr_ad1"]);
			$req->bindParam("adr_ad2",$_POST["adr_ad2"]);
			$req->bindParam("adr_ad3",$_POST["adr_ad3"]);
			$req->bindParam("adr_cp",$_POST["adr_cp"]);
			$req->bindParam("adr_ville",$adr_ville);
			$req->bindParam("adr_defaut",$adr_defaut);
			$req->bindParam("adr_libelle",$adr_libelle);
			$req->bindParam("adr_siret",$adr_siret);
			$req->bindParam("adr_geo",$adr_geo);
			try{
				$req->execute();
			}catch( PDOException $Exception ){
				$erreur_txt=$Exception->getMessage();
				$erreur_txt.="Erreur, l'adresse n'a pas été mis à jour!";
			}

			// INSERTION clientS ADRESSES CONTACTS
			$req = $Conn->prepare("DELETE FROM adresses_Contacts WHERE aco_adresse = :aco_adresse;");
			$req->bindParam("aco_adresse",$adresse);
			$req->execute();

			if(!empty($_POST['aco_contact'])){
				foreach($_POST['aco_contact'] as $c){
					if(!empty($c)){
						$aco_defaut = 0;
						foreach($_POST['aco_defaut'] as $d){
							if($d == $c){
								$aco_defaut = 1;
							}
						}

						$req = $Conn->prepare("INSERT INTO adresses_Contacts (
						aco_adresse, aco_contact, aco_defaut ) VALUES (
						:aco_adresse, :aco_contact, :aco_defaut);");
						$req->bindParam("aco_adresse",$adresse);
						$req->bindParam("aco_contact",$c);
						$req->bindParam("aco_defaut",$aco_defaut);
						try {
							$req->execute();
						}catch( PDOException $Exception ){
							$erreur_txt=$Exception->getMessage();
						}
					}

				}
			}

		}else{

			// CREATION

			$req = $Conn->prepare("INSERT INTO adresses (
			adr_ref, adr_ref_id, adr_type, adr_nom, adr_service, adr_ad1, adr_ad2, adr_ad3, adr_cp, adr_ville, adr_defaut, adr_libelle, adr_siret
			,adr_geo ) VALUES (
			1, :adr_ref_id, :adr_type, :adr_nom, :adr_service, :adr_ad1, :adr_ad2, :adr_ad3, :adr_cp, :adr_ville, :adr_defaut, :adr_libelle, :adr_siret
			,:adr_geo);");
			$req->bindParam("adr_ref_id",$client);
			$req->bindParam("adr_type",$adr_type);
			$req->bindParam("adr_nom",$adr_nom);
			$req->bindParam("adr_service",$_POST['adr_service']);
			$req->bindParam("adr_ad1",$_POST["adr_ad1"]);
			$req->bindParam("adr_ad2",$_POST["adr_ad2"]);
			$req->bindParam("adr_ad3",$_POST["adr_ad3"]);
			$req->bindParam("adr_cp",$_POST["adr_cp"]);
			$req->bindParam("adr_ville",$adr_ville);
			$req->bindParam("adr_defaut",$adr_defaut);
			$req->bindParam("adr_libelle",$adr_libelle);
			$req->bindParam("adr_siret",$adr_siret);
			$req->bindParam("adr_geo",$adr_geo);
			try {
				$req->execute();
				$adresse = $Conn->lastInsertId();
			}catch( PDOException $Exception ){
				$erreur_txt=$Exception->getMessage();
			}
			// INSERTION clientS ADRESSES CONTACTS
			if(!empty($_POST['aco_contact'])){
				foreach($_POST['aco_contact'] as $c){
					if(!empty($c)){
						$aco_defaut = 0;
						if(!empty($_POST['aco_defaut'])){
							foreach($_POST['aco_defaut'] as $d){
								if($d == $c){
									$aco_defaut = 1;
								}
							}
						}


						$req = $Conn->prepare("INSERT INTO adresses_Contacts (
						aco_adresse, aco_contact, aco_defaut ) VALUES (
						:aco_adresse, :aco_contact, :aco_defaut);");
						$req->bindParam("aco_adresse",$adresse);
						$req->bindParam("aco_contact",$c);
						$req->bindParam("aco_defaut",$aco_defaut);
						try {
							$req->execute();
						}catch( PDOException $Exception ){
							$erreur_txt=$Exception->getMessage();
						}
					}
				}
			}



		}
	}
	// FIN D'enregistrement

	// TRAITEMENT ANNEXE

	$maj_cli_n=false;

	// adresse par défaut
	if($adr_defaut==1){

		$sql="UPDATE adresses SET adr_defaut=0 WHERE adr_type=" . $adr_type . " AND adr_ref_id=" . $client . " AND NOT adr_id=" . $adresse . ";";
		$req = $Conn->query($sql);

		IF($adr_type==1){

			// il s'agit de l'adresse d'intervention par défaut du client => mise à jour de la fiche

			// on recherche le contact par defaut associé à cette adresse
			$sql="SELECT * FROM contacts
			LEFT JOIN adresses_contacts ON (adresses_contacts.aco_contact = contacts.con_id)
			 WHERE aco_adresse=" . $adresse . " AND aco_defaut;";
			$req = $Conn->query($sql);
			$d_contact=$req->fetch();

			if(empty($d_contact)){
				$d_contact["con_id"]=0;
				$d_contact["con_fonction"]=0;
				$d_contact["con_fonction_nom"]="";
				$d_contact["con_titre"]=0;
				$d_contact["con_nom"]="";
				$d_contact["con_prenom"]="";
				$d_contact["con_tel"]="";
				$d_contact["con_mail"]="";
			}


			$sql="UPDATE clients SET
			cli_adresse=:cli_adresse,
			cli_adr_nom=:cli_adr_nom,
			cli_adr_service=:cli_adr_service,
			cli_adr_ad1=:cli_adr_ad1,
			cli_adr_ad2=:cli_adr_ad2,
			cli_adr_ad3=:cli_adr_ad3,
			cli_adr_cp=:cli_adr_cp,
			cli_adr_ville=:cli_adr_ville,
			cli_contact=:cli_contact,
			cli_con_fct=:cli_con_fct,
			cli_con_fct_nom=:cli_con_fct_nom,
			cli_con_titre=:cli_con_titre,
			cli_con_nom=:cli_con_nom,
			cli_con_prenom=:cli_con_prenom,
			cli_con_tel=:cli_con_tel,
			cli_con_mail=:cli_con_mail
			WHERE cli_id=" . $client . ";";
			$req = $Conn->prepare($sql);
			$req->bindParam("cli_adresse",$adresse);
			$req->bindParam("cli_adr_nom",$_POST['adr_nom']);
			$req->bindParam("cli_adr_service",$_POST['adr_service']);
			$req->bindParam("cli_adr_ad1",$_POST["adr_ad1"]);
			$req->bindParam("cli_adr_ad2",$_POST["adr_ad2"]);
			$req->bindParam("cli_adr_ad3",$_POST["adr_ad3"]);
			$req->bindParam("cli_adr_cp",$_POST["adr_cp"]);
			$req->bindParam("cli_adr_ville",$adr_ville);
			$req->bindParam("cli_contact",$d_contact["con_id"]);
			$req->bindParam("cli_con_fct",$d_contact["con_fonction"]);
			$req->bindParam("cli_con_fct_nom",$d_contact["con_fonction_nom"]);
			$req->bindParam("cli_con_titre",$d_contact["con_titre"]);
			$req->bindParam("cli_con_nom",$d_contact["con_nom"]);
			$req->bindParam("cli_con_prenom",$d_contact["con_prenom"]);
			$req->bindParam("cli_con_tel",$d_contact["con_tel"]);
			$req->bindParam("cli_con_mail",$d_contact["con_mail"]);
			try{
				$req->execute();
			}catch( PDOException $Exception ){
				$erreur_txt=$Exception->getMessage();
				$erreur_txt.="<br/>Erreur, l'adresse par défaut n'a pas été mise à jour sur la fiche client.";
			}

			$d_client["cli_adr_cp"]=$_POST["adr_cp"];
			$maj_cli_n=true;

		}else{

			// ADRESSE DE FAC PAR DEFAUT

			if(empty($d_client["cli_siren"]) AND !empty($cli_siren)){
				$d_client["cli_siren"]=$cli_siren;
				$maj_cli_n=true;
			}
			if(!empty($cli_siren) AND !empty($adr_siret)){
				$d_client["cli_siret"]=$cli_siren . " " . $adr_siret;
				$maj_cli_n=true;
			}
		}

	}
	// FIN DE TRAITEMENT SUR AD DEFAUT

	if(!empty($cli_ident_tva) AND $cli_ident_tva!=$d["cli_ident_tva"]){
		$d_client["cli_ident_tva"]=$cli_ident_tva;
		$maj_cli_n=true;
	}

	if($adr_type==2){
		if($d_client["cli_affacturable"]==1 AND empty($adr_siret)){
			$d_client["cli_affacturable"]=0;
			$d_client["cli_affacturage"]=0;
			$d_client["cli_affacturage_iban"]="";
			$maj_cli_n=true;
		}elseif($d_client["cli_affacturable"]==0 AND !empty($adr_siret) AND $d_client["cli_categorie"]!=3 AND $d_client["cli_sous_categorie"]!=5 AND $d_client["cli_interco_soc"]==0){

			// LE CLIENT EST POTENTIELEMENT AFFACTURABLE

			$sql="SELECT adr_id FROM Adresses WHERE adr_ref=1 AND adr_type=2 AND adr_ref_id=" . $client . " AND (ISNULL(adr_siret) OR adr_siret='');";
			$req = $Conn->query($sql);
			$d_adr_sans_siret=$req->fetchAll();
			if(empty($d_adr_sans_siret)){
				$d_client["cli_affacturable"]=1;
			}
		}
	}


	if($maj_cli_n){


		$sql="UPDATE clients SET
		cli_ident_tva=:cli_ident_tva,
		cli_siren=:cli_siren,
		cli_siret=:cli_siret,
		cli_adr_cp=:cli_adr_cp,
		cli_affacturable=:cli_affacturable,
		cli_affacturage=:cli_affacturage,
		cli_affacturage_iban=:cli_affacturage_iban
		WHERE cli_id=:cli_id;";
		$req = $Conn->prepare($sql);
		$req->bindParam("cli_ident_tva",$d_client["cli_ident_tva"]);
		$req->bindParam("cli_siren",$d_client["cli_siren"]);
		$req->bindParam("cli_siret",$d_client["cli_siret"]);
		$req->bindParam("cli_adr_cp",$d_client["cli_adr_cp"]);
		$req->bindParam("cli_affacturable",$d_client["cli_affacturable"]);
		$req->bindParam("cli_affacturage",$d_client["cli_affacturage"]);
		$req->bindParam("cli_affacturage_iban",$d_client["cli_affacturage_iban"]);
		$req->bindParam("cli_id",$client);
		try{
			$req->execute();
		}catch( PDOException $Exception ){
			$erreur_txt=$Exception->getMessage();
			$erreur_txt.="Erreur, la fiche client n'a pas été actualisée";
		}

		if(empty($erreur_txt)){

			// ON MET A JOUR LES BASE N

			$req=$Conn->query("SELECT DISTINCT cso_societe FROM Clients_Societes WHERE cso_client=" . $client);
			$d_client_societes=$req->fetchAll();
			if(!empty($d_client_societes)){

				foreach($d_client_societes as $cli_soc){

					$ConnFct=connexion_fct($cli_soc["cso_societe"]);

					$sql_cli_soc="UPDATE clients SET
					cli_cp=:cli_cp,
					cli_affacturable=:cli_affacturable,
					cli_affacturage=:cli_affacturage
					WHERE cli_id=:cli_id;";
					$req_cli_soc = $ConnFct->prepare($sql_cli_soc);
					$req_cli_soc->bindParam("cli_cp",$d_client["cli_adr_cp"]);
					$req_cli_soc->bindParam("cli_affacturable",$d_client["cli_affacturable"]);
					$req_cli_soc->bindParam("cli_affacturage",$d_client["cli_affacturage"]);
					$req_cli_soc->bindParam("cli_id",$client);
					try{
						$req_cli_soc->execute();
					}catch( PDOException $Exception ){
						$erreur_txt=$Exception->getMessage();
						$erreur_txt.="Erreur, les fiches regionnales n'ont pas été actualisées";
					}
				}
			}
		}
	}
}

if(!empty($_SESSION['retourAdresse'])){
	
	$retour=$_SESSION['retourAdresse'];

}
if(empty($erreur_txt)){
	$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Votre adresse a été enregistrée"
	);
}else{
	$_SESSION['message'] = array(
		"aff" => "modal",
		"titre" => "Erreur!",
		"type" => "danger",
		"message" => $erreur_txt
	);
}

header("location : " . $retour);
die();
