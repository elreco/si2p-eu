<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_diplome.php');
	include('modeles/mod_erreur.php');
	

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link href="assets/admin-tools/admin-forms/css/admin-forms-orion.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php	
				include "includes/header_def.inc.php";
			?>	
				
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
							
				<section id="content" class="animated fadeIn">
				
					<div class="row" >
						<div class="col-md-12" >
							<div class="table-responsive" >
								<table class="table table-hover table-striped" >
									<thead>
										<tr class="dark" >
											<th>ID</th>
											<th>Nom</th>											
											<th>Validité</th>											
											<th>&nbsp;</th>
										</tr>															
									</thead>
									<tbody>
								<?php	$donnees=get_diplomes();												
										if(!empty($donnees)){										
											foreach($donnees as $value){  ?>
												<tr>
													<td><?=$value["dip_id"]?></td>													
													<td><?=$value["dip_libelle"]?></td>
													<td>
											<?php		if(!empty($value["dip_validite"])){
															echo($value["dip_validite"] . " mois");
														}else{
															echo("&nbsp;");
														}; ?>
													</td>																										
													<td class="text-center" >
														<span data-toggle="modal" data-target="#formEdit" >
															<a href="#" class="btn btn-warning btn-xs open-formEdit" data-id="<?=$value["dip_id"]?>" data-libelle="<?=$value["dip_libelle"]?>" data-validite="<?=$value["dip_validite"]?>" data-toggle="tooltip" data-placement="bottom" title="Modifier" >
																<i class="fa fa-pencil"></i>
															</a>
														</span>
													</td>									
												</tr>	
								<?php		
											};
										}; ?>																			
									</tbody>
								</table>
							</div>
						</div>
					</div>
	
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
		<?php		if($_SESSION['acces']['acc_profil']==13){ ?>
						<a href="import/sync_diplome.php" class="btn btn-default btn-sm" >
							<i class="fa fa-refresh"></i>
							Importer
						</a>		
		<?php		} ?>
				</div>
				<div class="col-xs-3 footer-right" >
					<button class="btn btn-success btn-sm open-formEdit" data-toggle="modal" data-target="#formEdit" data-id="0" data-libelle="" data-validite="" >
						<i class="fa fa-plus" ></i>
						Nouveau diplôme
					</button>
				</div>
			</div>
		</footer>
									
		<div id="formEdit" class="modal fade" role="dialog">
			
			
			<div class="modal-dialog admin-form theme-primary">
				<div class="modal-content">
				
					<form method="post" action="diplome_enr.php" enctype="multipart/form-data" >
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title" >Nouveau diplôme</h4>
						</div>
						<div class="modal-body">
						
							<div>
								<input type="hidden" name="dip_id" id="dip_id" value="0" >
							</div>
							<div class="row" >
							
								<div class="col-md-8" >
									<div class="form-group">		
										<label for="dip_libelle" class="sr-only" >Diplôme</label>					
										<input type="text" class="form-control" name="dip_libelle" id="dip_libelle" placeholder="Diplôme" required >								
									</div>	
								</div>
								<div class="col-md-4" >								  
									<div class="input-group" >
										<label class="sr-only" for="dip_validite">Validité</label>
										<input type="number" class="form-control" name="dip_validite" id="dip_validite" placeholder="Validité" >
										<span class="input-group-addon" >mois</span>										
									</div>	
								</div>
							</div>
										
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php
		include "includes/footer_script.inc.php"; ?>	                        

		
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				
				$(document).on("click", ".open-formEdit", function () {
					
					var diplome_id = $(this).data('id');
					var diplome_libelle = $(this).data('libelle');
					var diplome_validite = $(this).data('validite');
							
					$(".modal-body #dip_id").val( diplome_id );
					$(".modal-body #dip_libelle").val( diplome_libelle );						 
					$(".modal-body #dip_validite").val( diplome_validite );					

					if(diplome_id==0){
						$("#formEdit .modal-title").html('Nouveau diplôme');		
					}else{
						$("#formEdit .modal-title").html('Edition d\'un diplôme');				
					};
				});		
			});
			(jQuery);
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
