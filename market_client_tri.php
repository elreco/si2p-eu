<?php
$menu_actif = "6-3";
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';

$_SESSION['retour'] = "market_client_tri.php";

// annul la memorisation de la requete
if(isset($_SESSION['cli_tri'])){ 
    unset($_SESSION['cli_tri']);
}

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];    
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
    $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];  
}

// SOCIETE CONSULTE
$soc_nom="";
if(!empty($_SESSION['acces']["acc_liste_societe"][$acc_societe . "-" . $acc_agence])){
	$soc_nom=$_SESSION['acces']["acc_liste_societe"][$acc_societe . "-" . $acc_agence];
}

// ACTION MARKETING
$sql="SELECT mac_id,mac_libelle FROM Marketing_Actions ORDER BY mac_libelle;";
$req=$Conn->query($sql);
$d_market_action=$req->fetchAll();


// STAT RGPD
$nb_rgpd=0;
$nb_reponse=0;
$rep_oui=0;
$rep_non=0;

$sql="SELECT COUNT(con_id) AS nb_contact,con_market_no FROM Contacts INNER JOIN Clients ON (Contacts.con_ref_id=Clients.cli_id AND Contacts.con_ref=1) 
WHERE con_rgpd=1 AND cli_first_facture>0 GROUP BY con_market_no;";
$req=$Conn->query($sql);
$d_market=$req->fetchAll();
if(!empty($d_market)){
	foreach($d_market as $mar){
		if(!empty($mar["con_market_no"])){
			$rep_non=$rep_non+$mar["nb_contact"];
			$nb_reponse=$nb_reponse+$mar["nb_contact"];
		}
		$nb_rgpd=$nb_rgpd+$mar["nb_contact"];
	}
}


$sql="SELECT COUNT(DISTINCT con_id) AS nb_contact FROM Contacts
INNER JOIN Clients ON (Contacts.con_ref_id=Clients.cli_id AND Contacts.con_ref=1) 
LEFT OUTER JOIN Contacts_Marketing ON (Contacts.con_id=Contacts_Marketing.cma_contact)
WHERE con_rgpd AND (con_market_all OR NOT isnull(cma_contact) ) AND NOT con_mail_doublon AND cli_first_facture>0;";
$req=$Conn->query($sql);
$d_market_reponse=$req->fetch();
if(!empty($d_market_reponse)){
	$rep_oui=$d_market_reponse["nb_contact"];
	$nb_reponse=$nb_reponse+$d_market_reponse["nb_contact"];
}
$taux_reponse=0;
if(!empty($nb_rgpd)){
	$taux_reponse=($nb_reponse*100)/$nb_rgpd;
	$taux_reponse=number_format($taux_reponse,4);	
}


// CATEGORIE CLIENT
$sql="SELECT cca_id,cca_libelle FROM clients_categories ORDER BY cca_libelle;";
$req=$Conn->query($sql);
$d_categories=$req->fetchAll();

// LES COMMERCIAUX
$sql="SELECT com_id,com_label_1,com_label_2 FROM commerciaux";
$mil="";
if($_SESSION['acces']['acc_profil'] == 3 && !$_SESSION['acces']["acc_droits"][6]){
	$mil=" AND com_ref_1 = " . $_SESSION['acces']['acc_ref_id'];
}
if($acc_agence>0){
	$mil.=" AND com_agence= " . $acc_agence;
}
$mil.=" AND com_archive= 0";
if($mil!=""){
	$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
}
$sql.=" ORDER BY com_label_1,com_label_2";
$req = $ConnSoc->query($sql);
$d_commercial=$req->fetchAll();

// CODE APE
$sql="SELECT ape_id,ape_code,ape_libelle FROM Ape
ORDER BY ape_code,ape_libelle";
$req = $Conn->query($sql);
$d_apes=$req->fetchAll();

// CODE APE
$sql="SELECT cpr_id,cpr_libelle FROM Clients_Prescripteurs
ORDER BY cpr_libelle";
$req = $Conn->query($sql);
$d_prescripteurs=$req->fetchAll();

// CODE APE
$sql="SELECT ccl_id,ccl_libelle FROM Clients_Classifications
ORDER BY ccl_libelle";
$req = $Conn->query($sql);
$d_classifications=$req->fetchAll();

// CONTACT FONCTION
$sql="SELECT cfo_id,cfo_libelle FROM Contacts_Fonctions
ORDER BY cfo_libelle";
$req = $Conn->query($sql);
$d_contact_fonctions=$req->fetchAll();

// DEPARTEMENTS
$sql="SELECT dep_numero,dep_nom FROM Departements
ORDER BY dep_numero";
$req = $Conn->query($sql);
$d_departements=$req->fetchAll();

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Marketing client</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="market_client_liste.php" id="formulaire" >
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
					
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="panel">
									<div class="panel-heading panel-head-sm">STATISTIQUES</div>
									<div class="panel-body" >																														
										<div class="row">
											<div class="col-md-4">
												Contacts unique : <?=$nb_rgpd?>
											</div>
											<div class="col-md-4">
												Nombre de réponse : <?=$nb_reponse?>
											</div>
											<div class="col-md-4">														
												Taux de réponse : <?=$taux_reponse?> %
											</div>
										</div>
										<div class="row mt15">
											<div class="col-md-4">
												Nombre de consentements : <?=$rep_oui?>
											</div>
											<div class="col-md-4">
												Nombre de refus : <?=$rep_non?>
											</div>														
										</div>										
										
									</div>
								</div>
							</div>
						</div>
				
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-info">
									<div class="panel heading-border panel-info">
										<div class="panel-body bg-light">
											
											<div class="content-header">
												<h2><?=$soc_nom?> - Recherche de <b class="text-info title-suscli">contacts</b> clients</h2>
											</div>												
											<div class="col-md-10 col-md-offset-1">
											
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Marketing</span>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<label for="market_action" >Action marketing :</label>
															<div class="field select">																	
																<select name="market_action" id="market_action" required >
																	<option value="">Action marketing...</option>	
														<?php		if(!empty($d_market_action)){
																		foreach($d_market_action as $action){
																			echo("<option value='" . $action["mac_id"] . "' >" . $action["mac_libelle"] . "</option>"); 
																		}
																	} ?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Client</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="cli_categorie" >Catégorie :</label>
															<div class="field select">
																<select name="cli_categorie" id="cli_categorie" class="select2-categorie" >
																	<option value="0">Catégorie du client...</option>
														<?php		if(!empty($d_categories)){
																			foreach($d_categories as $d_categorie){
																				echo("<option value='" . $d_categorie["cca_id"] . "' >" . $d_categorie["cca_libelle"] . "</option>");
																			}
																		}?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
													<div class="col-md-6" >
														<div class="section">
															<label for="cli_sous_categorie" >Sous-catégorie :</label>																												
															<select name="cli_sous_categorie" id="cli_sous_categorie" class="select2 select2-categorie-sous-categorie" >
																<option value="0">Sélectionner une sous catégorie...</option>
															</select>																												
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="cli_groupe" >Type de client :</label>
															<div class="field select">
																<select name="cli_groupe" id="cli_groupe">
																	<option value="0">Type...</option>
																	<option value="1">Maison mère</option>
																	<option value="2">Groupe</option>
																	<option value="3">Entreprise seule</option>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<label for="cli_commercial" >Commercial :</label>
															<select name="cli_commercial" id="cli_commercial"  class="select2">
																<option value="">Commercial...</option>
													<?php 		if(!empty($d_commercial)){
																	foreach($d_commercial as $com){
																		echo("<option value='" . $com["com_id"] . "' >" . $com["com_label_1"] . " "  . $com["com_label_2"] . "</option>");
																	}
																} ?>
															</select>
														</div>
													</div>
												</div>											
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="cli_prescripteur" >Prescripteur :</label>
															<span class="select">															
																<select id="cli_prescripteur"  name="cli_prescripteur" >
																	<option value="0">Sélectionner un prescripteur...</option>
															<?php	if(!empty($d_prescripteurs)){
																		foreach($d_prescripteurs as $p){
																			echo("<option value='" . $p["cpr_id"] . "' >" . $p["cpr_libelle"] . "</option>");
																		}
																	} ?>
																</select>
																<i class="arrow"></i>
															</span>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">		
															<label for="cli_ape" >Code APE :</label>
															<select id="cli_ape"  name="cli_ape[]" class="select2" multiple="multiple" >																
														<?php	if(!empty($d_apes)){
																	foreach($d_apes as $ape){
																		echo("<option value='" . $ape["ape_id"] . "' >" . $ape["ape_code"] . "-" . $ape["ape_libelle"] . "</option>");
																	}
																} ?>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<div class="section">
															<label for="cli_classification" >Classification :</label>
															<select id="cli_classification" class="select2 select2-classification"  name="cli_classification" >
																<option value="0">Sélectionner une classification...</option>
														<?php	if(!empty($d_classifications)){
																	foreach($d_classifications as $c){
																		echo("<option value='" . $c["ccl_id"] . "' >" . $c["ccl_libelle"] . "</option>");
																	}
																} ?>
															</select>
														</div>
													</div>
													<div class="col-md-4">
															<div class="section">
																<label for="cli_classification_type" >Type :</label>
																<select id="cli_classification_type" class="select2 select2-classification-type" name="cli_classification_type">
																	<option value="0">Type de classification...</option>
																</option>
															</select>
														</div>
													</div>
													<div class="col-md-4">
														<div class="section">
															<label for="cli_classification_categorie" >Catégorie :</label>
															<select id="cli_classification_categorie" class="select2 select2-classification-categorie" name="cli_classification_categorie">
																<option value="0">Catégorie de classification...</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Lieu d'intervention par défaut</span>
														</div>
													</div>
												</div>													
												<div class="row">											
													<div class="col-md-6">
														<div class="section">		
															<label for="cli_dep" >Département :</label>
															<select id="cli_dep"  name="cli_dep[]" class="select2" multiple="multiple" >																
														<?php	if(!empty($d_departements)){
																	foreach($d_departements as $dep){
																		echo("<option value='" . $dep["dep_numero"] . "' >" . $dep["dep_numero"] . "-" . $dep["dep_nom"] . "</option>");
																	}
																} ?>
															</select>
														</div>
													</div>											
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Contacts</span>
														</div>
													</div>
												</div>		
												<div class="row" >														
													<div class="col-md-6">
														<div class="section">
															<div class="field select">
																<select name="con_fonction" id="con_fonction" >
																	<option value="0">Sélectionner une fonction...</option>
															<?php	if(!empty($d_contact_fonctions)){
																		foreach($d_contact_fonctions as $cf){
																			echo("<option value='" . $cf["cfo_id"] . "' >" . $cf["cfo_libelle"] . "</option>");
																		}
																	} ?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">
															<div class="option-group field">
																<label class="option option-dark">
																	<input type="checkbox" name="con_mail" value="on">
																	<span class="checkbox"></span>Mail renseigné
																</label>
															</div>
														</div>
													</div>													
												</div>													
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
				
					</section>
				</section>
			</div>		
			<!-- End: Main -->
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">

					</div>
					<div class="col-xs-6 footer-middle text-center"></div>
					<div class="col-xs-3 footer-right">
						<button type="submit" name="search" class="btn btn-primary btn-sm">
							<i class='fa fa-search'></i> Rechercher
						</button>

					</div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	

		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
				
			});
		</script>
	</body>
</html>
