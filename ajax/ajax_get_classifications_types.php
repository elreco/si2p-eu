<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

 // RETOURNE LES INFOS SOUS-SECTEURS D'ACTIVITE LiE A UN SECTEUR 
 
 // RETOUR FORMAT JSON
 
$erreur=0;
if(isset($_POST)){
	 
	$classification=0;
	if(!empty($_POST["classification"])){
		$classification=intval($_POST["classification"]); 
	}

	$retour=array();
	if($classification>0){

		$req=$Conn->prepare("SELECT * FROM clients_classifications_types WHERE cct_classification=:cct_classification ORDER BY cct_code,cct_libelle");
		$req->bindParam(":cct_classification",$classification);
		$req->execute();
		$classification = $req->fetchAll();
		if(!empty($classification)){
			foreach($classification as $dss){
				$libelle=$dss["cct_libelle"];
				if(!empty($dss["cct_code"])){
					$libelle=$dss["cct_code"] . " - " . $libelle;
				}
				$retour[]=array(
					"id" => $dss["cct_id"],
					"text" => $libelle
				);	
			}
			
		}
		
	}
	echo json_encode($retour);	
}else{
	echo("Erreur paramètre!");
}
 
?>