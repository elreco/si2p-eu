<?php
include "../includes/controle_acces.inc.php";
include '../connexion_soc.php';
include '../modeles/mod_del_session.php';


 // MAJ DES HOARIRES D'UNE ACTION
  
$erreur=0;
if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]); 
	}
	
	
	$sql="INSERT INTO Actions_Sessions (ase_date, ase_h_deb, ase_h_fin, ase_action)
	VALUES (:date, :h_deb, :h_fin, :action);";
	$req_add=$ConnSoc->prepare($sql);
	
	$sql="UPDATE Actions_Sessions SET ase_h_deb=:h_deb, ase_h_fin=:h_fin WHERE ase_id=:session;";
	$req_up=$ConnSoc->prepare($sql);
	
	$sql="UPDATE Plannings_Dates SET pda_test=:test WHERE pda_id=:date;";
	$req_up_date=$ConnSoc->prepare($sql);

	$sql="SELECT pda_id FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=" . $action_id . ";";
	$req=$ConnSoc->query($sql);
	$d_dates=$req->fetchAll();
	if(!empty($d_dates)){
		foreach($d_dates as $d){
			
			$nb_session=0;
			if(!empty($_POST["nb_session_" . $d["pda_id"]])){
				$nb_session=intval($_POST["nb_session_" . $d["pda_id"]]);
			}
			
			$test=0;
			if(!empty($_POST["test_" . $d["pda_id"]])){
				$test=1;
			}
			
			$req_up_date->bindParam(":test",$test);
			$req_up_date->bindParam(":date",$d["pda_id"]);
			$req_up_date->execute();
			
			for($bcl=1;$bcl<=$nb_session;$bcl++){
				
				$session=0;
				if(!empty($_POST["session_" . $d["pda_id"] . "_" . $bcl])){
					$session=intval($_POST["session_" . $d["pda_id"] . "_" . $bcl]);
				}
				$h_deb="";
				if(!empty($_POST["h_deb_" . $d["pda_id"] . "_" . $bcl])){
					$h_deb=$_POST["h_deb_" . $d["pda_id"] . "_" . $bcl];
				}
				$h_fin="";
				if(!empty($_POST["h_fin_" . $d["pda_id"] . "_" . $bcl])){
					$h_fin=$_POST["h_fin_" . $d["pda_id"] . "_" . $bcl];
				}
				
				if($session>0){
					
					if(!empty($h_deb) AND !empty($h_fin)){
						
						// MAJ
						
						//echo("MAJ<br/>");
						
						$req_up->bindParam(":session",$session);
						$req_up->bindParam(":h_deb",$h_deb);
						$req_up->bindParam(":h_fin",$h_fin);					
						$req_up->execute();
						
					}else{
						
						// suppression
						//echo("<br/>");
						//echo($session);
						//echo("suppresion<br/>");
						del_session($session,$d["pda_id"]);
						
					}
					
					
				}elseif(!empty($h_deb) AND !empty($h_fin)){
					
					//echo("AJOUT<br/>");
					
					$req_add->bindParam(":date",$d["pda_id"]);
					$req_add->bindParam(":h_deb",$h_deb);
					$req_add->bindParam(":h_fin",$h_fin);
					$req_add->bindParam(":action",$action_id);
					$req_add->execute();
					
				}
				
			}
		}
	}	
}else{
	$erreur=1;
}

 
?>