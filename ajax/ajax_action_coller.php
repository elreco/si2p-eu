<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // PERMET DE MEMORISER UNE ACTION A COUPER
 
if(isset($_POST)){

	if(!empty($_POST)){
		
		
		
		$liste_case="";
		if(!empty($_POST['liste_case'])){
			$liste_case=$_POST['liste_case'];
		}
		
		if(!empty($liste_case) AND isset($_SESSION["action_coupe"])){

			// format de retour compatible avec ecrire_case()

			$case_maj=array(
				"cases" =>array(
					"type" => 1,
					"ref_1" => $_SESSION["action_coupe"]["action"],
					"texte" => "",
					"style" => "",
					"survol" => "",
					"date" => array(),
					"erreur" => 0,
					"up_style_act" => false
				),
				"erreur_type" => "",
				"erreur_code" => "",
				"erreur_text" => ""
			);
			
			// CASE SELECT
			$case=explode (",",$liste_case);
			$nb_jour=0;
			
			/*var_dump($liste_case);
			echo("<pre>");
				print_r($case);
			echo("</pre>"); */
			
			$tab_date_colle=array();
			
			foreach($case as $c){
				if(!empty($c)){
					
					$donnee_case=explode ("_",$c);
					
					$tab_date_colle[]=$donnee_case;
					
					$nb_jour++;
					
					$sql_date_check="SELECT pda_id FROM Plannings_Dates WHERE NOT pda_archive AND pda_intervenant=:intervenant AND pda_date=:date AND pda_demi=:demi;";
					$req_date_check=$ConnSoc->prepare($sql_date_check);
					$req_date_check->bindParam("intervenant",$donnee_case[0]);
					$req_date_check->bindParam("date",$donnee_case[1]);
					$req_date_check->bindParam("demi",$donnee_case[2]);
					$req_date_check->execute();
					$pas_libre=$req_date_check->fetch();
					if(!empty($pas_libre)){
						$case_maj["erreur_type"]=1;
						$case_maj["erreur_code"]=2;
						$case_maj["erreur_text"]="Les dates selectionnées ne sont pas libres.";
					}
				}
			}
			
			if(empty($case_maj["erreur_type"])){
				// action liée aux dates

				$sql_action_get="SELECT act_id,act_verrou_admin,act_verrou_bc,act_verrou_marge,act_charge,act_agence FROM Actions WHERE act_id=:action;";
				$req_action_get=$ConnSoc->prepare($sql_action_get);
				$req_action_get->bindParam("action",$_SESSION["action_coupe"]["action"]);
				$req_action_get->execute();
				$d_action=$req_action_get->fetch();
				if(empty($d_action)){
					$case_maj["erreur_type"]=1;
					$case_maj["erreur_code"]=0;
					$case_maj["erreur_text"]="Impossible d'identifier l'action liée aux dates.";
				}
			}
	

			// LES DATES SONT LIBRES et ON A LES DONNEES DE L'ACTION
			if(empty($case_maj["erreur_type"])){
				
				if($nb_jour==$_SESSION["action_coupe"]["nb_demi_j"]){


					$tab_fournisseur=array();	// id des fournisseurs qui interviennent sur l'action
					$tab_warning=array();	// tableau des messages d'avertissements liés aux effets de bords.

					// recherche du nouveau fournisseur
					$sql_fou_int_get="SELECT int_type,int_ref_1,int_ref_3 FROM Intervenants WHERE int_id=:intervenant;";
					$req_fou_int_get=$ConnSoc->prepare($sql_fou_int_get);

					// annulation du BC de l'ancien fournisseur

					$sql_com_annule="UPDATE Commandes SET com_annule=1 WHERE com_fournisseur=:fournisseur AND com_action=:action AND com_agence=:agence;";
					$req_com_annule=$Conn->prepare($sql_com_annule);	
					$req_com_annule->bindParam("action",$d_action["act_id"]);
					$req_com_annule->bindParam("agence",$d_action["act_agence"]);

				

					

					$sql_date_get="SELECT pda_id,pda_texte,pda_survol,pda_style_bg,pda_style_txt,pda_demi,int_type,int_ref_1,int_ref_3,pda_alert,pda_date,pda_demi FROM Plannings_Dates,Intervenants 
					WHERE pda_intervenant=int_id AND NOT pda_archive AND pda_type=1 AND pda_ref_1=:action
					ORDER BY pda_date,pda_demi,pda_intervenant;";
					$req_date_get=$ConnSoc->prepare($sql_date_get);
					$req_date_get->bindParam("action",$_SESSION["action_coupe"]["action"]);
					$req_date_get->execute();
					$d_dates=$req_date_get->fetchAll();
					if(!empty($d_dates)){
						
						$style="background-color:" . $d_dates[0]["pda_style_bg"] . ";";
						if(!empty($d_dates[0]["pda_style_txt"])){
							$style.="color:" . $d_dates[0]["pda_style_txt"] . ";";
						}
						$case_maj["cases"]["texte"]=$d_dates[0]["pda_texte"];
						$case_maj["cases"]["style"]=$style;
						$case_maj["cases"]["survol"]=$d_dates[0]["pda_survol"];
						

						foreach($d_dates as $num => $dd){

							$sql_date_up="UPDATE Plannings_Dates SET
							pda_intervenant=:intervenant,pda_date=:date,pda_demi=:demi
							WHERE pda_id=:date_id;";
							$req_date_up=$ConnSoc->prepare($sql_date_up);						
							$req_date_up->bindParam("intervenant",$tab_date_colle[$num][0]);
							$req_date_up->bindParam("date",$tab_date_colle[$num][1]);
							$req_date_up->bindParam("demi",$tab_date_colle[$num][2]);
							$req_date_up->bindParam("date_id",$dd["pda_id"]);
							$req_date_up->execute();
							try{
								$req_date_up->execute();
							}Catch(Exception $e){
								$case_maj["erreur_type"]=1;
								$case_maj["erreur_code"]=2;
								$case_maj["erreur_text"]="Toutes les dates n'ont pas pû etre déplacées.";	
								break;
							}
							
							// LA CASE A ETE MODIFIEE
							$case_maj["cases"]["date"][]=$tab_date_colle[$num][0] . "_" . $tab_date_colle[$num][1] . "_" . $tab_date_colle[$num][2];

							// TRAITEMENT DES EFFETS DE BORD (warning text)
							
							if($dd["pda_demi"]!= $tab_date_colle[$num][2]){
								
								// demi journee cible ne correspond pas à la source
								// ON EFFACE LES HORRAIRES
								$sql_h_up="UPDATE Actions_Sessions SET
								ase_h_deb=null,ase_h_fin=null
								WHERE ase_date=:date_id;";
								$req_h_up=$ConnSoc->prepare($sql_h_up);						
								$req_h_up->bindParam("date_id",$dd["pda_id"]);
								$req_h_up->execute();
								$tab_warning[0]="Certains horaires ont été supprimés car la demi-journée source ne correspondait pas à la case selectionnée.";								
							}

							// BLOCAGE ADMINISTRATIF : GESTION DES BC

							// ancien fournisseur lié à la case
							$fournisseur_old=0;
							if($dd["int_type"]==2){
								// interco
								if(!empty($dd["int_ref_3"])){
									$fournisseur_old=$dd["int_ref_3"];
								}
							}elseif($dd["int_type"]==3){
								// st
								if(!empty($dd["int_ref_1"])){
									$fournisseur_old=$dd["int_ref_1"];
								}
							}

							// recherche du nouveau fournisseur

							$fournisseur_new=0;
							$req_fou_int_get->bindParam("intervenant",$tab_date_colle[$num][0]);
							$req_fou_int_get->execute();
							$d_intervenant=$req_fou_int_get->fetch();							
							if(!empty($d_intervenant)){
								if($d_intervenant["int_type"]==2){
									if(!empty($d_intervenant["int_ref_3"])){
										$fournisseur_new=$d_intervenant["int_ref_3"];
									}else{
										$tab_warning[1]="Certains intervenants INTERCO ne sont pas liés à un fournisseur.";										
										$act_verrou_admin=1;
										$d_action["act_verrou_bc"]=1;
										$d_action["act_verrou_marge"]=0;
									}
								}elseif($d_intervenant["int_type"]==3){
									if(!empty($d_intervenant["int_ref_1"])){
										$fournisseur_new=$d_intervenant["int_ref_1"];
									}else{
										$tab_warning[2]="Certains intervenants sous-traitants ne sont pas liés à un fournisseur.";										
										$act_verrou_admin=1;
										$d_action["act_verrou_bc"]=1;
										$d_action["act_verrou_marge"]=0;
									}
								}
							}

							if(!empty($fournisseur_new)){
								$tab_fournisseur[$fournisseur_new]=$fournisseur_new;	
							}

							// annulation d'un potentiel BC

							if(!empty($fournisseur_old) AND ($fournisseur_old!=$fournisseur_new OR $tab_date_colle[$num][1]!=$dd["pda_date"] OR $tab_date_colle[$num][2]!=$dd["pda_demi"] )) {

								$req_com_annule->bindParam("fournisseur",$fournisseur_old);
								try{
									$req_com_annule->execute();
								}Catch(Exception $e){
									$tab_warning[3]="Certains BC ou DA n'ont pas été annulés!" . $e->getMessage();	
								}
							}
						}

						// Fin de la bloucle sur les demi-journée coller.
						if(empty($tab_fournisseur)){

							// l'action ne fait pas ou plus appel à des fournisseur!

							$act_verrou_admin=0;
							$d_action["act_verrou_bc"]=0;
							$d_action["act_verrou_marge"]=0;
							$d_action["act_marge_ca"]=0;
							$d_action["act_charge"]=0;

						}else{

						

							$d_action["act_charge"]=0;
							$act_verrou_admin=0;

							// on verifie que chaque fournisseur a un BC (= DA validée)

							$sql_get_bc="SELECT com_ht FROM Commandes WHERE com_action=:action AND com_agence=:agence AND com_fournisseur=:fournisseur 
							AND com_etat>1 AND NOT com_annule;";
							$req_get_bc=$Conn->prepare($sql_get_bc);	
							$req_get_bc->bindParam("action",$d_action["act_id"]);
							$req_get_bc->bindParam("agence",$d_action["act_agence"]);

							foreach($tab_fournisseur as $fou_id){

								$req_get_bc->bindParam("fournisseur",$fou_id);
								$req_get_bc->execute();
								$d_bc=$req_get_bc->fetch();
								
								if(!empty($d_bc)){

									$d_action["act_charge"]=$d_action["act_charge"]+ $d_bc["com_ht"];

								}else{

									$act_verrou_admin=1;
									$d_action["act_verrou_bc"]=1;
									$d_action["act_verrou_marge"]=0;

								}								
							}
						}

					}else{
						$case_maj["erreur_type"]=1;
						$case_maj["erreur_code"]=2;
						$case_maj["erreur_text"]="Impossible de charger les dates de l'action selectionnée.";
					}
				}else{
					$case_maj["erreur_type"]=1;
					$case_maj["erreur_code"]=0;
					$case_maj["erreur_text"]="Le nombre de demi-journées selectionnées ne correspond pas à l'action d'origine.";
				}
				
			}
			if($case_maj["erreur_type"]!=1){
				
				// ON MET A JOUR LA DATE DE DEBUT DE FORMATION
				
				$sql_action_up="UPDATE Actions SET
				act_intervenant=:intervenant,
				act_date_deb=:date_deb,
				act_demi_deb=:demi,
				act_verrou_admin=:act_verrou_admin,
				act_verrou_bc=:act_verrou_bc,
				act_verrou_marge=:act_verrou_marge,
				act_marge_ca=:act_marge_ca,
				act_charge=:act_charge
				WHERE act_id=:action;";
				$req_action_up=$ConnSoc->prepare($sql_action_up);						
				$req_action_up->bindParam("intervenant",$tab_date_colle[$num][0]);
				$req_action_up->bindParam("date_deb",$tab_date_colle[$num][1]);
				$req_action_up->bindParam("demi",$tab_date_colle[$num][2]);
				$req_action_up->bindParam("act_verrou_admin",$act_verrou_admin);
				$req_action_up->bindParam("act_verrou_bc",$d_action["act_verrou_bc"]);
				$req_action_up->bindParam("act_verrou_marge",$d_action["act_verrou_marge"]);
				$req_action_up->bindParam("act_marge_ca",$d_action["act_marge_ca"]);
				$req_action_up->bindParam("act_charge",$d_action["act_charge"]);
				$req_action_up->bindParam("action",$d_action["act_id"]);
				try{
					$req_action_up->execute();
				}Catch(Exception $e){
					$tab_warning[4]="L'action n'a pas été mise à jour.";	
				}

				// on force le style de la case si blocage admin
				if($act_verrou_admin){
					$case_maj["cases"]["style"]="background-color:#FFF;color:#F00;";
				}
				

				unset($_SESSION["action_coupe"]);

				// on actualise les dates
				if($act_verrou_admin!=$d_action["act_verrou_admin"]){
					
					$sql_date_up="UPDATE Plannings_Dates SET pda_alert=:pda_alert WHERE pda_type=1 AND pda_ref_1=:action;";
					$req_date_up=$ConnSoc->prepare($sql_date_up);						
					$req_date_up->bindParam("action",$d_action["act_id"]);
					$req_date_up->bindParam("pda_alert",$act_verrou_admin);
					try{
						$req_date_up->execute();
					}Catch(Exception $e){
						$tab_warning[5]="Le blocage administratif n'a pas été actualisé sur le planning.";	
					}



				}
			}

			if(empty($case_maj["erreur_type"])){
				if(!empty($tab_warning)){
					$case_maj["erreur_type"]=2;
					foreach($tab_warning as $warning){
						$case_maj["erreur_text"].=$warning . "<br/>";
					}
				}
			}
			
			echo(json_encode($case_maj));
			
			
		}else{
			
			echo("Formulaire incomplet.");
			die();
		}
		
	}else{
		echo("Formulaire incomplet!");
		die();
	}
}else{
	echo("Formulaire incomplet!");
	die();
}			
?>