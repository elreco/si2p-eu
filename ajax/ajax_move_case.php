<?php
include("../includes/connexion.php");
include('../includes/connexion_soc.php');

/*$erreur_type
1 => erreur total : pas de maj
2 => erreur partiel*/
$erreur_type=0;

/*Si erreur_type =1 => $erreur_code
2 => declenche recharchement du planning*/
$erreur_code=0;

$erreur_text="";

$warning_text="";

if(isset($_GET['id_source'])&&isset($_GET['id_cible'])){
	
	$donnee_source=explode ("_",$_GET['id_source']);
	$intervenant_source=$donnee_source[0];
	$date_source=$donnee_source[1];
	$demi_source=$donnee_source[2];
	
	$donnee_cible=explode ("_",$_GET['id_cible']);
	$intervenant_cible=$donnee_cible[0];
	$date_cible=$donnee_cible[1];
	$demi_cible=$donnee_cible[2];
	
	
	$sql="SELECT pda_id,pda_type,pda_ref_1,pda_texte,pda_style_bg,pda_style_txt,pda_confirme
	,int_type,int_ref_1,int_ref_2,int_ref_3,int_label_1,int_label_2 FROM Plannings_Dates,Intervenants 
	WHERE pda_intervenant=int_id AND pda_intervenant=:intervenant AND pda_date=:date AND pda_demi=:demi AND NOT pda_archive;";
	$req=$ConnSoc->prepare($sql);
	
	// CONTROLE
	
	// on verifie que la cible est bien libre
	$req->bindParam(":intervenant",$intervenant_cible);
	$req->bindParam(":date",$date_cible);
	$req->bindParam(":demi",$demi_cible);
	$req->execute();
	$cible=$req->fetch();
	if(!empty($cible)){
		$erreur_type=1;
		$erreur_code=2;	// rechargement
		$erreur_text="La date selectionnée n'est pas disponible!";
	}
	
	// DONNEE POUR TRAITEMENT
	
	if(empty($erreur_type)){

		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
		}
	
		// format de retour 
		$case_maj=array(
			"cases" =>array(
				"type" => 0,
				"ref_1" => 0,
				"texte" => "",
				"style" => "",
				"survol" => "",
				"date" => array(),
				"erreur" => 0,
				"up_style_act" => false		// true = toutes les cases de l'action prendront le style de la case
			),
			"erreur_type" => "",
			"erreur_code" => "",
			"erreur_text" => ""
		);

		
		// DONNEE DE LA CASE SOURCE

		$req->bindParam(":intervenant",$intervenant_source);
		$req->bindParam(":date",$date_source);
		$req->bindParam(":demi",$demi_source);
		$req->execute();
		$source=$req->fetch();
		if(!empty($source)){
			
			$style="background-color:" . $source["pda_style_bg"] . ";color:" . $source["pda_style_txt"] . ";";
			$case_maj["cases"]["type"]=$source["pda_type"];
			$case_maj["cases"]["ref_1"]=$source["pda_ref_1"];
			$case_maj["cases"]["texte"]=$source["pda_texte"];
			$case_maj["cases"]["style"]=$style;
			$case_maj["cases"]["date"]=array($_GET['id_cible']);

		}else{
			$erreur_type=1;
			$erreur_text="Impossible de récupérer les informations de la case selectionnée";
		}
	}
	
	/*******************************
		ENREGISTREMENT
	********************************/
	
	// MAJ DE LA DATE
	
	if(empty($erreur_type)){

		$sql="UPDATE Plannings_Dates SET pda_intervenant=:intervenant_cible, pda_date=:date_cible, pda_demi=:demi_cible";
		$sql.=" WHERE pda_id=:date;";
		$req_up=$ConnSoc->prepare($sql);
		$req_up->bindParam(":intervenant_cible",$intervenant_cible);
		$req_up->bindParam(":date_cible",$date_cible);
		$req_up->bindParam(":demi_cible",$demi_cible);
		$req_up->bindParam(":date",$source["pda_id"]);
		try{
			$req_up->execute();
		}Catch(Exception $e){
			$erreur_type=1;
			$erreur_text="La date n'a pas pu être déplacée!" . $e->getMessage();
		}
	}
	
	// TARITEMNT ANNEXE
	
	if(empty($erreur_type)){
			
		// SI ACTION
	
		if($case_maj["cases"]["type"]==1){


			// l'action

			$sql="SELECT act_id,act_charge,act_date_deb,act_demi_deb,act_intervenant,act_pro_sous_famille
			,act_verrou_admin,act_verrou_marge,act_agence
			FROM Actions WHERE act_id=:action;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action",$case_maj["cases"]["ref_1"]);
			$req->execute();
			$d_action=$req->fetch();
			if(!empty($d_action)){
				$act_verrou_admin=$d_action["act_verrou_admin"];			
			}else{
				$erreur_type=2;
				$warning_text.="L'action liée à la demi-journée n'a pas pu être identifiée!";
			}

			// ANNULATION AUTO DES BC
			$annul_bc=false;
			$warning_bc="";

			if ( ($source["int_type"]==3 OR $source["int_type"]==2) ){

				// la case était sur un intervenant ST ou INTERCO

				if ($source["int_type"]==3){
					if(!empty($source["int_ref_1"])){
						$fournisseur_id=$source["int_ref_1"];
					}
				}elseif ($source["int_type"]==2){
					if(!empty($source["int_ref_3"])){
						$fournisseur_id=$source["int_ref_3"];
					}
				}

				if($date_source!=$date_cible OR $demi_source!=$demi_cible){
					// changement de jour ou changement de demi -> le BC n'est plus valable -> ANNUL
					$annul_bc=true;
				}elseif($source["int_type"]!=$cible["int_type"]){
					// on change de type d'intervenant
					$annul_bc=true;
				}elseif($source["int_type"]==3 AND $source["int_ref_1"]!=$cible["int_ref_1"]){
					// même jour tjr ST mais changement de fournisseur
					$annul_bc=true;
				}elseif($source["int_type"]==2 AND $source["int_ref_3"]!=$cible["int_ref_3"]){
					// même jour tjr INTERCO mais changement de fournisseur (agence)
					$annul_bc=true;
				}

				if($annul_bc){

					if(isset($fournisseur_id)){

						$sql_annul_da = "UPDATE Commandes SET com_annule=1,com_annule_txt='Annulation auto. Changement de prestataire' 
						WHERE com_societe=:acc_societe AND com_agence=:act_agence AND com_action=:action AND com_fournisseur=:fournisseur;";
						$req_annul_da = $Conn->prepare($sql_annul_da);						
						$req_annul_da->bindParam(":acc_societe", $acc_societe);
						$req_annul_da->bindParam(":act_agence", $d_action["act_agence"]);
						$req_annul_da->bindParam(":action", $d_action["act_id"]);
						$req_annul_da->bindParam(":fournisseur", $fournisseur_id);
						try{
							$req_annul_da->execute();
						}Catch(Exception $e){

							$erreur_type=2;
							$warning_bc.="Blocage administratif : Le BC n'a pas été annulé!";

						}
					}else{
						$warning_bc="L'intervenant n'est pas lié à un fournisseur. Impossible de verifier le BC!";
					}

					/*if(empty($warning_bc)){

						// annulation de BC = recalcul de la charge

						$sql_com_ht = "SELECT SUM(com_ht) FROM Commandes WHERE com_societe=:acc_societe AND com_agence=:act_agence AND com_action=:action
						AND com_etat>1 AND NOT com_annule;";
						$req_com_ht = $Conn->prepare($sql_com_ht);						
						$req_com_ht->bindParam(":acc_societe", $acc_societe);
						$req_com_ht->bindParam(":act_agence", $d_action["act_agence"]);
						$req_com_ht->bindParam(":action", $d_action["act_id"]);
						$req_com_ht->execute();
						$d_com_ht=$req_com_ht->fetch();
						if(!empty($d_com_ht)){
							$d_action["act_charge"]=$d_com_ht[0];
						}
					}*/
				}

				if(!empty($warning_bc)){
					$erreur_type=2;
					$warning_text.=$warning_bc;
				}
			}

			// BLOCAGE ADMINISTRATIF : BC

			// recherche les dates de l'action
			
			$fournisseurs=array();
			$act_verrou_bc=0;

			$sql="SELECT pda_intervenant,pda_date,pda_demi,int_type,int_ref_1,int_ref_2,int_label_1 FROM Plannings_Dates,Intervenants WHERE pda_intervenant=int_id AND pda_type=1 
			AND pda_ref_1=:action ORDER BY pda_date,pda_demi,pda_id";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action",$case_maj["cases"]["ref_1"]);
			$req->execute();
			$d_dates=$req->fetchAll();
			if (!empty($d_dates)) {
				$d_action["act_date_deb"]=$d_dates[0]["pda_date"];
				$d_action["act_demi_deb"]=$d_dates[0]["pda_demi"];
				$d_action["act_intervenant"]=$d_dates[0]["pda_intervenant"];

				foreach($d_dates as $d){

					if ($d["int_type"]==3) {

						if(empty($d["int_ref_1"])){
							// int non lié à un fournisseur
							$act_verrou_bc=1;
						}elseif (!isset($fournisseurs[$d["int_ref_1"]])) {							
							$fournisseurs[$d["int_ref_1"]]=$d["int_ref_1"];
						}
					} elseif ($d["int_type"]==2) {

						// interco

						if(empty($d["int_ref_3"])){
							// int non lié à un fournisseur
							$act_verrou_bc=1;
						}elseif (!isset($fournisseurs[$d["int_ref_1"]])) {							
							$fournisseurs[$d["int_ref_3"]]=$d["int_ref_3"];
						}
					}
				}

			}else{
				$erreur_type=2;
				$warning_text.="Action sans date!";
			}

			if(!empty($fournisseurs)){

				// il reste des fournisseurs

				$d_action["act_charge"]=0;

				foreach($fournisseurs as $fou_id){

					$sql_get_bc = "SELECT com_ht FROM Commandes WHERE com_societe=:acc_societe AND com_agence=:act_agence AND com_action=:action
					AND com_etat>1 AND NOT com_annule AND com_fournisseur=:fournisseur;";
					$req_get_bc = $Conn->prepare($sql_get_bc);						
					$req_get_bc->bindParam(":acc_societe", $acc_societe);
					$req_get_bc->bindParam(":act_agence", $d_action["act_agence"]);
					$req_get_bc->bindParam(":action", $d_action["act_id"]);
					$req_get_bc->bindParam(":fournisseur", $fou_id);
					$req_get_bc->execute();
					$d_commande=$req_get_bc->fetch();
					if(!empty($d_commande)){
						$d_action["act_charge"]=$d_action["act_charge"] + $d_commande["com_ht"];
					}else{
						$act_verrou_bc=1;
						$act_verrou_admin=1;
						$act_verrou_marge=0;
					}

				}

				if($act_verrou_bc==0){

					// il y a des ST mais ils ont tous des BC

					// on controle la marge

					$sql="SELECT SUM(acl_ca) AS ca FROM Actions_Clients WHERE acl_action=" . $d_action["act_id"] . " AND NOT acl_archive AND acl_confirme;";
					$req=$ConnSoc->query($sql);
					$d_action_clients=$req->fetch();
					if(!empty($d_action_clients)) {
						if($d_action_clients["ca"]<$d_action["act_marge_ca"]){
							$act_verrou_marge=1;
							$act_verrou_admin=1;
						}
					}else{
						$act_verrou_marge=1;
						$act_verrou_admin=1;
					}
				}

			}else{
				$act_verrou_bc=0;
				$act_verrou_admin=0;
				$act_verrou_marge=0;
				$d_action["act_charge"]=0;
			}

			// MAJ DE L'ACTION

			$sql="UPDATE Actions SET act_intervenant=:act_intervenant, act_date_deb=:act_date_deb,act_demi_deb=:act_demi_deb,act_verrou_admin=:act_verrou_admin";
			$sql.=",act_verrou_bc=:act_verrou_bc,act_verrou_marge=:act_verrou_marge,act_charge=:act_charge";
			$sql.=" WHERE act_id=:action;";
			$req_act=$ConnSoc->prepare($sql);
			$req_act->bindParam(":action",$d_action["act_id"]);
			$req_act->bindParam(":act_intervenant",$d_action["act_intervenant"]);
			$req_act->bindParam(":act_date_deb",$d_action["act_date_deb"]);
			$req_act->bindParam(":act_demi_deb",$d_action["act_demi_deb"]);
			$req_act->bindParam(":act_verrou_admin",$act_verrou_admin);
			$req_act->bindParam(":act_verrou_bc",$act_verrou_bc);
			$req_act->bindParam(":act_verrou_marge",$act_verrou_marge);
			$req_act->bindParam(":act_charge",$d_action["act_charge"]);
			try{
				$req_act->execute();
			}Catch(Exception $e){
				$warning_text.=$e->getMessage();
				$erreur_type=2;
				$warning_text.="L'action n'a pas été mise à jour!<br/>";
			}

			// l'état act_verrou_admin affecte l'affichage du planning -> on actualise les dates
			if($act_verrou_admin!=$d_action["act_verrou_admin"]){

				$sql="UPDATE Plannings_Dates SET pda_alert=:pda_alert WHERE pda_type=1 AND pda_ref_1=:action;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":action",$d_action["act_id"]);
				$req->bindParam(":pda_alert",$act_verrou_admin);			
				try{
					$req->execute();

				}Catch(Exception $e){
					$erreur_type=2;
					$warning_text.="Blocage administratif : les dates n'ont pas été actualisées!";
				}

				$case_maj["cases"]["up_style_act"]=true;
				

			}
			if ($act_verrou_admin) {
				$case_maj["cases"]["style"]="background-color:#FFF;color:#F00;";
			}
			
			// passage d'un matin en ap ou inverssement
			
			if($demi_source!=$demi_cible){
			
				// on supp les horraires
				
				$sql="SELECT * FROM Actions_Sessions WHERE ase_date=" . $source["pda_id"] . " ORDER BY ase_id;";
				$req=$ConnSoc->query($sql);
				$d_session=$req->fetchAll();
				if(!empty($d_session)){
					
					// on supp les horaires sur la première session pour repasser en matin / après midi
					$sql="UPDATE Actions_Sessions SET ase_h_deb='', ase_h_fin='' WHERE ase_id=" . $d_session[0]["ase_id"] . ";"; 
					$req=$ConnSoc->query($sql);
			
					$sql="DELETE FROM Actions_Sessions WHERE ase_date=" . $source["pda_id"] . " AND NOT ase_id=" . $d_session[0]["ase_id"] . ";"; 
					$req=$ConnSoc->query($sql);
				}						
			}

			
		}
	}
}else{
	
	$erreur_type=1;
	$erreur_text="Formulaire incomplet";
}

if($erreur_type==1){
	$case_maj=array(
		"erreur_type" => $erreur_type,
		"erreur_code" => $erreur_code,
		"erreur_text" => $erreur_text
	);
}elseif($erreur_type==2){
	$case_maj["erreur_type"]=2;
	$case_maj["erreur_text"]=$warning_text;
}
echo json_encode($case_maj);
?>
