<?php
include("../includes/connexion.php");
include_once('../modeles/mod_parametre.php');
if(isset($_POST)){

     $date = convert_date_sql($_POST['date']);

	$req = $Conn->prepare("UPDATE commandes_lignes SET cli_livraison = :cli_livraison WHERE cli_id = :cli_id");
    $req->bindParam("cli_livraison",$date);
    $req->bindParam("cli_id", $_POST['cli_id']);
    $req->execute();

    $req = $Conn->prepare("SELECT cli_commande FROM commandes_lignes WHERE cli_id = :cli_id");
    $req->bindParam("cli_id", $_POST['cli_id']);
    $req->execute();
    $ligne = $req->fetch();

    $req = $Conn->prepare("SELECT cli_famille, cli_livraison FROM commandes_lignes WHERE cli_commande = :cli_id");
    $req->bindParam("cli_id", $ligne['cli_commande']);
    $req->execute();
    $lignes = $req->fetchAll();
    $bap = 1;
    foreach($lignes as $l){
    	if($l['cli_famille'] == 1){
    		if(!empty($l['cli_livraison'])){
    			$bap = 1;
    		}else{
    			$bap = 0;
    		}
    	}
    }

    echo $bap;


}
?>