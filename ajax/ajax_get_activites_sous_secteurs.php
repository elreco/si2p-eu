<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../modeles/mod_get_activites_sous_secteurs.php';

 // RETOURNE LES INFOS SOUS-SECTEURS D'ACTIVITE LiE A UN SECTEUR 
 
 // RETOUR FORMAT JSON
 
$erreur=0;
if(isset($_POST)){
	 
	$secteur=0;
	if(!empty($_POST["secteur"])){
		$secteur=intval($_POST["secteur"]); 
	} 
	
	$retour=array();
	if($secteur>0){
		$s_secteur=get_activites_sous_secteurs($secteur);
		if(!empty($s_secteur)){
			foreach($s_secteur as $dss){
				$retour[]=array(
					"id" => $dss["ass_id"],
					"text" => $dss["ass_libelle"]
				);	
			}
			echo json_encode($retour);	
		}
	}else{
		echo("Erreur paramètre!");
	}
}else{
	echo("Erreur paramètre!");
}
 
?>