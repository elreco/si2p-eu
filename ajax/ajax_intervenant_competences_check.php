<?php

	// VERIFIE SI INTERVENANT DISPOSE DE COMPETENCES LISTEES
	
	include("../includes/connexion.php");
	
	$ref=0;
	if(!empty($_POST["ref"])){
		$ref=intval($_POST["ref"]);
	}
	
	$ref_id=0;
	if(!empty($_POST["ref_id"])){
		$ref_id=intval($_POST["ref_id"]);
	}
	
	$liste_comp="";
	if(!empty($_POST["liste_comp"])){
		$liste_comp=$_POST["liste_comp"];
	}
	
	if(!empty($ref) AND !empty($ref_id) AND !empty($liste_comp)){
		
		$retour=array(
			"statut" => 0,
			"non_valides" => array()
		);
		
		$tab_comp=explode(",",$liste_comp);
		
		$sql="SELECT com_libelle,ico_competence FROM Competences LEFT JOIN Intervenants_Competences ON (Competences.com_id=Intervenants_Competences.ico_competence AND ico_ref=:ref AND ico_ref_id=:ref_id)
		WHERE com_id=:com_id;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":ref",$ref);
		$req->bindParam(":ref_id",$ref_id);
		foreach($tab_comp as $comp){
			
			if(!empty($comp)){
			
				$req->bindParam(":com_id",$comp);
				$req->execute();
				$resultat=$req->fetch();
				/*echo("<pre>");
				print_r($resultat);
				echo("</pre>");*/
				if(!empty($resultat)){					
					if(empty($resultat["ico_competence"])){
						$retour["statut"]=2;
						$retour["non_valides"][]=$resultat["com_libelle"];
					}
				}
			}
		}
		
		echo(json_encode($retour));
		die();
	}else{
		echo("Paramètres absents!");
		die();
	}
?>	
