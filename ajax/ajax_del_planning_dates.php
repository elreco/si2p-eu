<?php
include("../includes/connexion.php");
include('../includes/connexion_soc.php');
include('../modeles/mod_actions_clients.php');

// PERMET DE SUPPRIMER DES DATES DU PLANNINGS (enregistrer dans plannings_dates)

// format de retour 
$case_maj=array(
	"cases" =>array(),
	"erreur_type" => "",
	"erreur_code" => "",
	"erreur_text" => ""
);

if(isset($_POST['liste_case'])){
	
	$case=explode (",",$_POST['liste_case']);	

	$req_select=$ConnSoc->prepare("SELECT * FROM Plannings_Dates WHERE pda_intervenant=:intervenant AND pda_date=:date AND pda_demi=:demi AND NOT pda_archive;");

	$req_supp=$ConnSoc->prepare("DELETE FROM Plannings_Dates WHERE pda_id=:date_id;");
	
	$req_select_acd=$ConnSoc->prepare("SELECT acd_action_client FROM Actions_Clients_Dates WHERE acd_date=:date_id;");
	
	$req_supp_acd=$ConnSoc->prepare("DELETE FROM Actions_Clients_Dates WHERE acd_date=:date_id;");
	
	
	$action_client_maj=Array();
	
	foreach($case as $c){
		
		if(!empty($c)){
			
			$donnee_case=explode ("_",$c);
			$intervenant=$donnee_case[0];
			$date=$donnee_case[1];
			$demi=$donnee_case[2];

			$req_select->bindParam(":intervenant",$intervenant);
			$req_select->bindParam(":date",$date);
			$req_select->bindParam(":demi",$demi);
			$req_select->execute();
			$d_date=$req_select->fetch();
			if(!empty($d_date)){
				
				if($d_date["pda_type"]==1){
					
					$case_maj["erreur_type"]=1;
					$case_maj["erreur_text"]="Vous ne pouvez pas supprimer des dates de formations";
				
					
					// Actions
					
					// ON RECUPERE LES ACTIONS CLIENTS IMPACTE PAR LA SUPRRESSION					
					/*$req_select_acd->bindParam(":date_id",$d_date["pda_id"]);
					$req_select_acd->execute();
					$acd=$req_select_acd->fetchAll();
					if(!empty($acd)){
						foreach($acd as $v){
							if(!in_array( $v["acd_action_client"], $action_client_maj)){
								$action_client_maj[]=$v["acd_action_client"];
							}
						}
					}*/
					
					// SUPRESION DU LIEN ENTRE ACTIONS_CLIENTS ET DATES
					
					/*$req_supp_acd->bindParam(":date_id",$d_date["pda_id"]);
					$req_supp_acd->execute();*/
					
				}elseif($d_date["pda_type"]==2){
					
					$case_maj["erreur_type"]=1;
					$case_maj["erreur_text"]="Vous ne pouvez pas supprimer des dates de congés";
					
				}else{
					
					// DATE LIBRE
					
					$req_supp->bindParam(":date_id",$d_date["pda_id"]);
					try{
						$req_supp->execute();
						array_push($case_maj["cases"],$c);			
					}Catch(Exception $e){
						if(empty($case_maj["erreur_type"])){
							$case_maj["erreur_type"]=2;
							$case_maj["erreur_text"]="Certaines dates n'ont pas été supprimées!";
						}
					}
				}
				
			}

		}
	}
}else{
	$case_maj["erreur_type"]=1;
	$case_maj["erreur_text"]="Formulaire incomplet!";
}
echo json_encode($case_maj);
?>
