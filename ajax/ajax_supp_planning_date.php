<?php
include("../includes/connexion.php");
include('../includes/connexion_soc.php');

if(isset($_POST['pda_id']) && isset($_POST['action_id'])){

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}

	// ON CHECK SI IL RESTE AU MOINS UNE DATE
	$req_supp=$ConnSoc->prepare("SELECT pda_id,pda_intervenant FROM Plannings_Dates WHERE pda_ref_1 = :action_id AND pda_type = 1;");
	$req_supp->bindParam(":action_id",$_POST['action_id']);
	$req_supp->execute();
	$pda= $req_supp->fetchAll();
	if(count($pda) > 1){

		

		// info de la date supprimé
	

		$req_date=$ConnSoc->prepare("SELECT pda_intervenant FROM Plannings_Dates WHERE pda_ref_1 = :action_id AND pda_type = 1 AND pda_id=:date;");
		$req_date->bindParam(":action_id",$_POST['action_id']);
		$req_date->bindParam(":date",$_POST['pda_id']);
		$req_date->execute();
		$d_date= $req_date->fetch();
		if(empty($d_date)){
			echo("Impossible de récupérer la date à supprimer!");
			die();
		}

		// RECUPERATION DE L'ID DE LA SESSION*
		// tu peux avoir plusieur session sur une date au moment de la supp
		$req_supp=$ConnSoc->prepare("SELECT ase_id FROM actions_sessions WHERE ase_date = :pda_id AND ase_action = :action_id;");
		$req_supp->bindParam(":pda_id",$_POST['pda_id']);
		$req_supp->bindParam(":action_id",$_POST['action_id']);
		$req_supp->execute();
		$action_sessions= $req_supp->fetchAll();
		
		// DELETE LA SESSION
		// idem planning_dates supp actions_stagiaires_sessions avant actions_sessions

		foreach($action_sessions as $ase){
			// DELETE LES STAGIAIRES INSCRITS
			$req_supp=$ConnSoc->prepare("DELETE FROM actions_stagiaires_sessions WHERE ass_session = :ase_id AND ass_action = :action_id;");
			$req_supp->bindParam(":action_id",$_POST['action_id']);
			$req_supp->bindParam(":ase_id",$ase['ase_id']);
			$req_supp->execute();
		}

		$req_supp=$ConnSoc->prepare("DELETE FROM actions_sessions WHERE ase_date = :pda_id AND ase_action = :action_id;");
		$req_supp->bindParam(":pda_id",$_POST['pda_id']);
		$req_supp->bindParam(":action_id",$_POST['action_id']);
		$req_supp->execute();

		// aller chercher les clients sur actions_clients
		$req_supp=$ConnSoc->prepare("SELECT acl_id,acl_qte_pm,acl_pro_inter, acl_ca, acl_facture, acl_facture_ht, acl_facturation, acl_facture_gfc, acl_ca_gfc, acl_facture_gfc_ht 
		FROM actions_clients 
		INNER JOIN actions_clients_dates ON (actions_clients_dates.acd_action_client = actions_clients.acl_id)
		WHERE acl_action = :action_id AND acd_date = :pda_id;");
		$req_supp->bindParam(":action_id",$_POST['action_id']);
		$req_supp->bindParam(":pda_id",$_POST['pda_id']);
		$req_supp->execute();
		$actions_clients= $req_supp->fetchAll();

		foreach($actions_clients as $acl){
			if($acl['acl_qte_pm'] != 0 AND $acl['acl_pro_inter'] == 0){


				$acl_qte_pm = $acl['acl_qte_pm'] - 1;

				// NORMAL
				if($acl['acl_ca']!=0){
					$acl_ca = $acl['acl_ca']/$acl['acl_qte_pm'];
					$acl_ca = $acl['acl_ca'] - $acl_ca;
					// ACL FACTURE BOOLEAN
					if($acl['acl_facture_ht'] == $acl_ca){
						$acl_facture = 1;
					}else{
						$acl_facture = 0;
					}
				}else{
					$acl_facture = $acl['acl_facture'];
					$acl_ca = 0;
				}
				// GFC
				if($acl['acl_ca_gfc']!=0){
					$acl_ca_gfc = $acl['acl_ca_gfc']/$acl['acl_qte_pm'];
					$acl_ca_gfc = $acl['acl_ca_gfc'] - $acl_ca_gfc;
					// ACL FACTURE GFC BOOLEAN
					if($acl['acl_facture_gfc_ht'] == $acl_ca_gfc){
						$acl_facture_gfc = 1;
					}else{
						$acl_facture_gfc = 0;
					}
				}else{
					$acl_facture_gfc = $acl['acl_facture_gfc'];
					$acl_ca_gfc = 0;
				}

				/////// JSPR QUE C BON !! /////

				// SI C'EST UN CLIENT INTRA
				// JE MET A JOUR LES CHAMPS ACTIONS_CLIENTS AVEC ACL_ID
				$req_supp=$ConnSoc->prepare("UPDATE actions_clients SET acl_qte_pm = :acl_qte_pm, acl_ca=:acl_ca, acl_facture = :acl_facture, acl_ca_gfc=:acl_ca_gfc,acl_facture_gfc = :acl_facture_gfc WHERE acl_id = :acl_id");
				$req_supp->bindParam(":acl_id",$acl['acl_id']);
				$req_supp->bindParam(":acl_qte_pm",$acl_qte_pm);
				$req_supp->bindParam(":acl_ca",$acl_ca);
				$req_supp->bindParam(":acl_facture",$acl_facture);
				$req_supp->bindParam(":acl_ca_gfc",$acl_ca_gfc);
				$req_supp->bindParam(":acl_facture_gfc",$acl_facture_gfc);
				$req_supp->execute();
			}
		}
		
		// SUPPRESSION DE ACTIONS_CLIENTS_DATES
		$req_supp=$ConnSoc->prepare("DELETE FROM actions_clients_dates WHERE acd_date = :pda_id;");
		$req_supp->bindParam(":pda_id",$_POST['pda_id']);
		$req_supp->execute();
		
		// SUPPRESSION DE LA DATE DU PLANNING
		// supp la date à la fin -> si plantage tu n'auras plus de lien les sessions 
		$req_supp=$ConnSoc->prepare("DELETE FROM Plannings_Dates WHERE pda_id = :pda_id AND pda_ref_1 = :action_id AND pda_type = 1;");
		$req_supp->bindParam(":pda_id",$_POST['pda_id']);
		$req_supp->bindParam(":action_id",$_POST['action_id']);
		$req_supp->execute();

		// METTRE A JOUR LA DATE DE DEBUT DE l'ACTION
		// SELECTIONNER LA PREMIERE DATE
		$req_supp=$ConnSoc->prepare("SELECT pda_date, pda_demi, pda_intervenant FROM Plannings_Dates WHERE pda_ref_1 = :action_id AND pda_type = 1 ORDER BY pda_date ASC, pda_demi ASC;");
		$req_supp->bindParam(":action_id",$_POST['action_id']);
		$req_supp->execute();
		$premiere_date= $req_supp->fetch();

		if(!empty($premiere_date)){
			$req_supp=$ConnSoc->prepare("UPDATE actions SET act_date_deb= :act_date_deb, act_demi_deb =:act_demi_deb, act_intervenant=:act_intervenant WHERE act_id = :act_id;");
			$req_supp->bindParam(":act_id",$_POST['action_id']);
			$req_supp->bindParam(":act_demi_deb",$premiere_date['pda_demi']);
			$req_supp->bindParam(":act_intervenant",$premiere_date['pda_intervenant']);
			$req_supp->bindParam(":act_date_deb",$premiere_date['pda_date']);
			$req_supp->execute();
		}
		
		// ANNULATION DU BC

		// l'intervenant lié à la date delete
		$req_int=$ConnSoc->prepare("SELECT int_type,int_ref_1,int_ref_2,int_ref_3 FROM Intervenants WHERE int_id = :intervenant AND int_type IN (2,3);");
		$req_int->bindParam(":intervenant",$d_date['pda_intervenant']);
		$req_int->execute();
		$d_int= $req_int->fetch();

		if(!empty($d_int)){

			// recherche du fournisseur

			if($d_int["int_type"]==3){

				$fournisseur=$d_int["int_ref_1"];

			}else{

				$fournisseur=$d_int["int_ref_3"];
			}

			if(!empty($fournisseur)){

				// annulation du BC

				$sql_annul_bc = "UPDATE Commandes SET com_annule=1 WHERE com_action=:action AND com_societe=:societe AND com_fournisseur=:fournisseur;";
				$req_annul_bc = $Conn->prepare($sql_annul_bc);	
				$req_annul_bc -> bindParam("action",$_POST['action_id']);
				$req_annul_bc -> bindParam("societe",$acc_societe);
				$req_annul_bc -> bindParam("fournisseur",$fournisseur);
				$req_annul_bc ->execute();
				
			}
		}

		// on regarde s'il reste des demi-journées ST et si il on des BC

		$sql_get_bc="SELECT com_id,com_ht FROM Commandes WHERE com_action=:action AND com_societe=:societe AND com_fournisseur=:fournisseur AND com_etat>1 AND NOT com_annule;";
		$req_get_bc=$Conn->prepare($sql_get_bc);
		$req_get_bc->bindParam(":action",$_POST['action_id']);
		$req_get_bc->bindParam(":societe",$acc_societe);

		$sql_get_four_interco = "SELECT fou_id FROM Fournisseurs WHERE fou_interco_soc=:interco_soc AND fou_interco_age=:interco_age AND NOT fou_archive;";
		$req_get_four_interco = $Conn->prepare($sql_get_four_interco);	

		$req_intervenants=$ConnSoc->prepare("SELECT int_type,int_ref_1,int_ref_2,int_ref_3 FROM Plannings_Dates,Intervenants WHERE pda_intervenant=int_id AND
		pda_ref_1 = :action_id AND pda_type = 1 AND int_type IN (2,3);");
		$req_intervenants->bindParam(":action_id",$_POST['action_id']);
		$req_intervenants->execute();
		$d_intervenants= $req_intervenants->fetchAll();
		if(!empty($d_intervenants)){

			$act_charge=0;
			$act_verrou_bc=0;

			// données de l'action liées au blocage administratif

			$req_action=$ConnSoc->prepare("SELECT act_marge_ca FROM Actions WHERE act_id=:action;");
			$req_action->bindParam(":action",$_POST['action_id']);
			$req_action->execute();
			$d_action= $req_action->fetch();

			// IL RESTE DES ST
				
			foreach($d_intervenants as $int){

				if ($int["int_type"]==3) {

					// sous-traitant

					$req_get_bc->bindParam(":fournisseur",$int["int_ref_1"]);
					$req_get_bc->execute();
					$d_bc=$req_get_bc->fetch();
					if (empty($d_bc)) {
						$act_verrou_bc=1;							
					}else{
						$act_charge=$act_charge + $d_bc["com_ht"];
					}
				} elseif ($int["int_type"]==2) {

					// interco
					if(empty($d["int_ref_3"])){

						// int non lié à un fournisseur
						$act_verrou_bc=1;

					}else {	
						
						$req_get_bc->bindParam(":fournisseur",$int["int_ref_3"]);
						$req_get_bc->execute();
						$d_bc=$req_get_bc->fetch();
						if (empty($d_bc)) {
							$act_verrou_bc=1;								
						}else{
							$act_charge=$act_charge + $d_bc["com_ht"];
						}
								
					}
				}
			}

			if($act_verrou_bc==1){
				// certains ST n'ont pas de BC
				$act_verrou_admin=1;
				$act_verrou_marge=0;
			
			}else{

				// TOUS les ST restants dispose d'un BC
				
				// comme le verrou BC est levé, le verrou marge peut être caluclé

				$act_verrou_marge=1;
				$act_verrou_admin=1;

				$sql_action_clients="SELECT SUM(acl_ca) AS ca FROM Actions_Clients WHERE acl_action=:action AND NOT acl_archive AND acl_confirme;";
				$req_action_clients=$ConnSoc->prepare($sql_action_clients);
				$req_action_clients->bindParam("action",$_POST['action_id']);
				$req_action_clients->execute();
				$d_action_clients=$req_action_clients->fetch();
				if(!empty($d_action_clients)){
					if(!empty($d_action_clients)){
						
						if(round($d_action_clients["ca"],2)>=round($d_action["act_marge_ca"],2) )
						{
							$act_verrou_marge=0;
							// il n'y a plus de blocage bc ni de blocage marge -> on leve le blocage admin
							$act_verrou_admin=0;
						}
					}
				}

			}
		}else{

			// IL N'Y A PLUS DE ST
			$act_verrou_admin=0;
			$act_verrou_marge=0;
			$act_verrou_bc=0;
			$act_marge_ca=0;
			$act_charge=0;
		}
		
		$sql_supp_verrou="UPDATE actions SET act_verrou_admin=:act_verrou_admin, act_verrou_bc=:act_verrou_bc, act_verrou_marge=:act_verrou_marge, act_charge=:act_charge";
		if(isset($act_marge_ca)){
			$sql_supp_verrou.=",act_marge_ca=:act_marge_ca";
		}
		$sql_supp_verrou.=" WHERE act_id = :act_id;";
		$req_supp_verrou=$ConnSoc->prepare($sql_supp_verrou);
		$req_supp_verrou->bindParam(":act_id",$_POST['action_id']);
		$req_supp_verrou->bindParam(":act_verrou_admin",$act_verrou_admin);
		$req_supp_verrou->bindParam(":act_verrou_bc",$act_verrou_bc);
		$req_supp_verrou->bindParam(":act_verrou_marge",$act_verrou_marge);
		if(isset($act_marge_ca)){
			$req_supp_verrou->bindParam(":act_marge_ca",$act_marge_ca);
		}
		$req_supp_verrou->bindParam(":act_charge",$act_charge);
		$req_supp_verrou->execute();
		
		
		$req_supp_verrou_date=$ConnSoc->prepare("UPDATE Plannings_Dates SET pda_alert=:act_verrou_admin WHERE pda_type=1 AND pda_ref_1 = :act_id;");
		$req_supp_verrou_date->bindParam(":act_id",$_POST['action_id']);
		$req_supp_verrou_date->bindParam(":act_verrou_admin",$act_verrou_admin);
		$req_supp_verrou_date->execute();

		echo json_encode("ok"); // 1 = OK 2 = ERREUR : IL RESTE PLUS qu UNE DATE

		
	
	}else{
         echo "Vous ne pouvez pas supprimer la dernière date de l'action.";
	}
}else{
	echo "ERREUR PARAMETRE";
}
?>
