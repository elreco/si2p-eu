<?php
include("../includes/connexion.php");
include_once('../modeles/mod_parametre.php');
if(isset($_POST)){
    $req = $Conn->prepare("SELECT nli_pris_en_charge FROM ndf_lignes WHERE nli_id = :nli_id");
    $req->bindParam("nli_id", $_POST['nli_id']);
    $req->execute();
    $nli = $req->fetch();

    $nli_pris_en_charge = $_POST['val'] + $nli['nli_pris_en_charge'];

	$req = $Conn->prepare("UPDATE ndf_lignes SET nli_pris_en_charge = :nli_pris_en_charge, nli_depassement_accorde = :nli_depassement_accorde WHERE nli_id = :nli_id");
    $req->bindParam("nli_depassement_accorde",$_POST['val']);
    $req->bindParam("nli_pris_en_charge",$nli_pris_en_charge);
    $req->bindParam("nli_id", $_POST['nli_id']);
    $req->execute();

    echo $nli_pris_en_charge;
}
?>