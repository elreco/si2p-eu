<?php 

include("../includes/connexion.php");
// edition du contact

if($_POST['con_type_envoi'] == "edition"){
	if($_POST['con_fonction'] == 'autre'){
		$_POST['con_fonction'] =0;
	}
	if(isset($_POST['con_defaut']) && $_POST['con_defaut'] == "con_defaut"){
		$con_defaut = 1;
	}else{
		$con_defaut = 0;
	}
	// update du contact
	$req = $Conn->prepare("UPDATE fournisseurs_contacts SET fco_defaut = :fco_defaut, fco_fonction = :fco_fonction, fco_fonction_nom = :fco_fonction_nom, fco_titre = :fco_titre, fco_nom = :fco_nom, fco_prenom = :fco_prenom, fco_tel =:fco_tel, fco_fax = :fco_fax, fco_portable = :fco_portable, fco_mail = :fco_mail WHERE fco_id = :fco_id;");
	$req->bindParam(':fco_id', $_POST['con_id']);
	$req->bindParam(':fco_defaut', $con_defaut);
	$req->bindParam(':fco_fonction', $_POST['con_fonction']);
	$req->bindParam(':fco_fonction_nom', $_POST['con_fonction_nom']);
	$req->bindParam(':fco_titre', $_POST['con_titre']);
	$req->bindParam(':fco_nom', $_POST['con_nom']);
	$req->bindParam(':fco_prenom', $_POST['con_prenom']);
	$req->bindParam(':fco_tel', $_POST['con_tel']);
	$req->bindParam(':fco_fax', $_POST['con_fax']);
	$req->bindParam(':fco_portable', $_POST['con_portable']);
	$req->bindParam(':fco_mail', $_POST['con_mail']);
	$req->execute();
	// Editer le contact par défaut
	if(isset($_POST['con_defaut']) && $_POST['con_defaut'] == "con_defaut"){

		$req = $Conn->prepare("SELECT fco_id, fco_defaut FROM fournisseurs_contacts WHERE fco_fournisseur = :fou_id AND fco_id <> :fco_id");
		$req->bindParam(':fou_id', $_GET['fournisseur']);
		$req->bindParam(':fco_id', $_GET['contact']);
		$req->execute();
		$contacts_fournisseur = $req->fetchAll();

		if(!empty($contacts_fournisseur)){
			foreach($contacts_fournisseur as $c){
				$req = $Conn->prepare("UPDATE fournisseurs_contacts SET fco_defaut = 0 WHERE fco_id = :fco_id;");
				$req->bindParam(':fco_id', $c['fco_id']);
				$req->execute();
			}
		}
		

	}
	$req = $Conn->prepare("SELECT * FROM fournisseurs_contacts WHERE fco_id = :fco_id;");
	$req->bindParam(':fco_id', $_POST['con_id']);
	$req->execute();
	$contact = $req->fetch();
	// renvoi du tableau json
	echo json_encode($contact);


}
// fin edition

?>