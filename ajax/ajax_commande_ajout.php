<?php 
include "../includes/controle_acces.inc.php";
include("../includes/connexion.php");
include('../modeles/mod_tva.php');

if(isset($_POST)){
	$reference = rawurldecode($_POST['reference']);
	$libelle = rawurldecode($_POST['libelle']);
	$descriptif = rawurldecode($_POST['descriptif']);
	$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = " . $_POST['commande']);
	$req->execute();
	$commande = $req->fetch();
	if(empty($_POST['cli_id'])){
		
		$_POST['prix_ht'] = number_format($_POST['prix_ht'], 2, '.', '');
		$_POST['prix_ht'] = floatval($_POST['prix_ht']);
		if(!empty($_POST['ajout_fournisseur'])){
			$req = $Conn->prepare("SELECT * FROM fournisseurs_produits WHERE fpr_fournisseur = " . $_POST['fournisseur'] . " AND fpr_reference = '" . $reference . "'");
			$req->execute();
			$reference_fournisseur = $req->fetch();

			if(empty($reference_fournisseur)){
				$req = $Conn->prepare("INSERT INTO fournisseurs_produits (fpr_fournisseur,fpr_type,fpr_reference, fpr_libelle,fpr_descriptif, fpr_tarif) 
				VALUES (:fpr_fournisseur,:fpr_type,:fpr_reference, :fpr_libelle,:fpr_descriptif, :fpr_tarif)");
				$req->bindValue(':fpr_fournisseur', $_POST['fournisseur']);
				$req->bindValue(':fpr_type', $_POST['type']);
				$req->bindValue(':fpr_reference', $reference);
				$req->bindValue(':fpr_libelle', $libelle);
				$req->bindValue(':fpr_descriptif', $descriptif);
				$req->bindValue(':fpr_tarif', $_POST['prix_ht']);
				$req->execute();
			}else{
				$reference_fournisseur = array();
			}
		}else{
			$reference_fournisseur = array();
		}
		$req = $Conn->prepare("INSERT INTO commandes_lignes (cli_pu, cli_famille, cli_categorie_si2p, cli_famille_si2p,cli_sous_famille_si2p, cli_sous_sous_famille_si2p, cli_produit_si2p, cli_reference, cli_libelle, cli_descriptif, cli_ht, cli_qte, cli_tva, cli_commande, cli_utilisateur, cli_vehicule) VALUES (:cli_pu,:cli_famille, :cli_categorie_si2p, :cli_famille_si2p, :cli_sous_famille_si2p, :cli_sous_sous_famille_si2p, :cli_produit_si2p, :cli_reference, :cli_libelle, :cli_descriptif, :prix_ht, :prix_qte, :tva, :cli_commande, :cli_utilisateur, :cli_vehicule)");
		$req->bindValue(':cli_famille', $_POST['type']);
	    $req->bindValue(':cli_categorie_si2p', $_POST['categorie']);
	    $req->bindValue(':cli_famille_si2p', $_POST['famille']);
		$req->bindValue(':cli_sous_famille_si2p', $_POST['sous_famille']);
		$req->bindValue(':cli_sous_sous_famille_si2p', $_POST['sous_sous_famille']);
	    $req->bindValue(':cli_produit_si2p', $_POST['produit']);
	    $req->bindValue(':cli_reference', $reference);
	    $req->bindValue(':cli_libelle', $libelle);
	    $req->bindValue(':cli_descriptif', $descriptif);
	    $req->bindValue(':cli_pu', $_POST['prix_ht']);
		$req->bindValue(':prix_qte', $_POST['prix_qte']);
		$req->bindValue(':prix_ht', $_POST['prix_ht']*$_POST['prix_qte']);
	    $req->bindValue(':tva', $_POST['tva']);
	    $req->bindValue(':cli_commande', $_POST['commande']);
	    $req->bindValue(':cli_vehicule', $_POST['vehicule']);
	    $req->bindValue(':cli_utilisateur', $_POST['utilisateur']);
		$req->execute();
		
		$last = $Conn->lastInsertId();

	}else{
		if(!empty($_POST['ajout_fournisseur'])){
			$req = $Conn->prepare("SELECT * FROM fournisseurs_produits WHERE fpr_fournisseur = " . $_POST['fournisseur'] . " AND fpr_reference =  '" . $reference . "'");
			$req->execute();
			$reference_fournisseur = $req->fetch();

			if(empty($reference_fournisseur)){
				$req = $Conn->prepare("INSERT INTO fournisseurs_produits (fpr_fournisseur,fpr_type,fpr_reference, fpr_libelle,fpr_descriptif, fpr_tarif) 
				VALUES (:fpr_fournisseur,:fpr_type,:fpr_reference, :fpr_libelle,:fpr_descriptif, :fpr_tarif)");
				$req->bindValue(':fpr_fournisseur', $_POST['fournisseur']);
				$req->bindValue(':fpr_type', $_POST['type']);
				$req->bindValue(':fpr_reference', $reference);
				$req->bindValue(':fpr_libelle', $libelle);
				$req->bindValue(':fpr_descriptif', $descriptif);
				$req->bindValue(':fpr_tarif', $_POST['prix_ht']);
				$req->execute();
			}else{
				$reference_fournisseur = array();
			}
		}else{
			$reference_fournisseur = array();
		}
		$req = $Conn->prepare("UPDATE commandes_lignes SET cli_pu = :cli_pu, cli_famille = :cli_famille, cli_categorie_si2p = :cli_categorie_si2p, cli_famille_si2p = :cli_famille_si2p, cli_sous_sous_famille_si2p= :cli_sous_sous_famille_si2p,cli_sous_famille_si2p= :cli_sous_famille_si2p, cli_produit_si2p = :cli_produit_si2p, cli_reference = :cli_reference, cli_libelle = :cli_libelle, cli_descriptif = :cli_descriptif, cli_ht = :prix_ht, cli_qte = :prix_qte, cli_tva = :tva, cli_commande = :cli_commande, cli_vehicule = :cli_vehicule, cli_utilisateur = :cli_utilisateur  WHERE cli_id = :cli_id");
		$req->bindValue(':cli_famille', $_POST['type']);
	    $req->bindValue(':cli_categorie_si2p', $_POST['categorie']);
	    $req->bindValue(':cli_famille_si2p', $_POST['famille']);
		$req->bindValue(':cli_sous_famille_si2p', $_POST['sous_famille']);
		$req->bindValue(':cli_sous_sous_famille_si2p', $_POST['sous_sous_famille']);
	    $req->bindValue(':cli_produit_si2p', $_POST['produit']);
	    $req->bindValue(':cli_reference', $reference);
	    $req->bindValue(':cli_libelle', $libelle);
	    $req->bindValue(':cli_descriptif', $descriptif);
	    $req->bindValue(':cli_pu', $_POST['prix_ht']);
		$req->bindValue(':prix_qte', $_POST['prix_qte']);
		$req->bindValue(':prix_ht', $_POST['prix_ht']*$_POST['prix_qte']);
	    $req->bindValue(':tva', $_POST['tva']);
	    $req->bindValue(':cli_commande', $_POST['commande']);
	    $req->bindValue(':cli_id', $_POST['cli_id']);
	    $req->bindValue(':cli_vehicule', $_POST['vehicule']);
	    $req->bindValue(':cli_utilisateur', $_POST['utilisateur']);
		$req->execute();
		
		$last = $_POST['cli_id'];
	}
	

	$req = $Conn->prepare("SELECT * FROM commandes_lignes WHERE cli_id = " . $last);
	$req->execute();
	$cli = $req->fetch();

	$req = $Conn->prepare("SELECT ffa_libelle FROM fournisseurs_familles WHERE ffa_id = " . $cli['cli_famille']);
	$req->execute();
	$ffa = $req->fetch();

	if(!empty($cli['cli_produit_si2p'])){
		$req = $Conn->prepare("SELECT pro_code_produit FROM produits WHERE pro_id = " . $cli['cli_produit_si2p']);
		$req->execute();
		$pro = $req->fetch();
	}
	if(!empty($cli['cli_vehicule'])){
		$req = $Conn->prepare("SELECT veh_libelle FROM vehicules WHERE veh_id = " . $cli['cli_vehicule']);
		$req->execute();
		$veh = $req->fetch();
	}
	if(!empty($cli['cli_utilisateur'])){
		$req = $Conn->prepare("SELECT uti_nom, uti_prenom FROM utilisateurs WHERE uti_id = " . $cli['cli_utilisateur']);
		$req->execute();
		$uti = $req->fetch();
	}
	if(empty($cli['cli_vehicule'])){
		$veh['veh_libelle'] = "";
	}
	if(empty($cli['cli_utilisateur'])){
		$uti['uti_prenom'] = "";
		$uti['uti_nom'] = "";
	}
	foreach($base_tva as $b => $v){ 
        
		if($b > 0){
        	
        	if($b == $cli['cli_tva']){
				$tva = $v;
				$req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = " . $cli['cli_tva'] . " AND tpe_date_deb <= '" . $commande['com_date'] . "' AND (tpe_date_fin >= '"  . $commande['com_date'] . "' OR tpe_date_fin IS NULL)");
				$req->execute();
				$tva = $req->fetch();
				$tva = $tva['tpe_taux'];
        	}
        	
        } 
    } 
    if(!empty($pro)){
    	$retour = array(
    		"cli_id" => $last,
    		"cli_reference" => $reference,
    		"cli_descriptif" => $descriptif,
    		"cli_libelle" => $libelle,
			"famille" => $ffa['ffa_libelle'],
			"tva" => $tva,
			"produit" => $pro['pro_code_produit'],
			"uti_libelle" => $uti['uti_prenom'] . " " . $uti['uti_nom'],
			"veh_libelle" => $veh['veh_libelle'],
		);
    }else{
    	$retour = array(
    		"cli_id" => $last,
    		"cli_reference" => $reference,
    		"cli_descriptif" => $descriptif,
    		"cli_libelle" => $libelle,
			"famille" => $ffa['ffa_libelle'],
			"tva" => $tva,
			"produit" => "",
			"uti_libelle" => $uti['uti_prenom'] . " " . $uti['uti_nom'],
			"veh_libelle" => $veh['veh_libelle'],
		);
    }
	


	echo json_encode($retour);
}

?>