<?php 
include "../includes/controle_acces.inc.php";
include("../includes/connexion.php");
include('../modeles/mod_tva.php');

if(isset($_POST)){
	$req = $Conn->prepare("SELECT * FROM commandes_lignes WHERE cli_commande = :com_id");
	$req->bindParam(':com_id', $_POST['com_id']);
	$req->execute();
	$cli = $req->fetchAll();
	$total=0;
	foreach($cli as $c){
		if(empty($total)){
			$total = $c['cli_qte'] * $c['cli_pu'];
		}else{
			$total = $total + ($c['cli_qte'] * $c['cli_pu']);
		}
		
	}
	$req = $Conn->prepare("UPDATE commandes SET com_ht = :com_ht  WHERE com_id = :com_id");
	$req->bindParam(':com_id', $_POST['com_id']);
	$req->bindParam(':com_ht', $total);
	$req->execute();
	
	echo $total;
}

?>