<?php
include "../includes/controle_acces.inc.php";

include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // RETOURNE LES INFOS CONCERNANT LES CLIENTS QUI PARTICICPENT A UNE ACTION
 
 // RETOUR FORMAT JSON
 
$erreur=0;
if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=$_POST["action"]; 
	}else{
		$erreur=1;
	}	

	$session_id=0;
	if(!empty($_POST["session"])){
		$session_id=intval($_POST["session"]); 
	}	
	
	if($erreur==0){
		
		// SUR L'ACTION DE REF
		
		$sql="SELECT act_adr_ref_id";
		$sql.=" FROM Actions WHERE act_id=:action_id AND act_adr_ref=1";
		$sql.=";";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$action=$req->fetch();
		
	}
	if($erreur==0){	

		// DONNEES ACTIONS CLIENTS
		
		
		$client_action=array();
		$sql="SELECT cli_id,cli_code,cli_nom";
		$sql.=",acl_id,acl_pro_reference,acl_pro_libelle";
		$sql.=" FROM Clients INNER JOIN Actions_Clients ON (Clients.cli_id=Actions_Clients.acl_client)";
		if(!empty($session_id)){
			$sql.="INNER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
			INNER JOIN Actions_Sessions ON (Actions_Sessions.ase_date=Actions_Clients_Dates.acd_date AND ase_id=:session_id)";
		}
		$sql.=" WHERE acl_action=:action_id AND NOT acl_archive
		ORDER BY cli_code,cli_nom,cli_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		if(!empty($session_id)){
			$req->bindParam(":session_id",$session_id);
		}
		$req->execute();
		$result=$req->fetchAll();
		if(!empty($result)){
			foreach($result as $r){
				$intervention=0;
				if(!empty($action)){
					if($r["cli_id"]==$action["act_adr_ref_id"]){
						$intervention=1;
					}
				}
				$client_action[]=array(
					"id" => $r["acl_id"],
					"text" => $r["cli_code"] . " " . $r["cli_nom"],
					"action_client" => $r["acl_id"],
					"client" => $r["cli_id"],
					"code" => $r["cli_code"],
					"nom" => $r["cli_nom"],
					"pro_reference" => $r["acl_pro_reference"],
					"pro_libelle" => $r["acl_pro_libelle"],
					"intervention" => $intervention					
				);
				
				
			}			
		}
		
	}
}else{
	$erreur=10;
}

if($erreur==0){
	echo json_encode($client_action);
}else{
	header("HTTP/1.0 500 ");
};
 
 
?>