<?php 


include '../includes/connexion.php';

if($_POST['etat'] == 1){
	$ico_valide = 1;
}else{
	$ico_valide = 0;
}
$req = $Conn->prepare("UPDATE intervenants_competences SET ico_valide = :ico_valide WHERE ico_ref = 2 AND ico_ref_id = :ico_ref_id AND ico_competence = :ico_competence;");
$req->bindParam(':ico_ref_id', $_POST['intervenant']);
$req->bindParam(':ico_competence', $_POST['competence']);
$req->bindParam(':ico_valide', $ico_valide);
$req->execute();

if($ico_valide == 1){
	echo 1;
}else{
	$req = $Conn->prepare("SELECT * FROM intervenants_competences WHERE ico_ref = 2 AND ico_ref_id = :ico_ref_id AND ico_competence = :ico_competence;");
	$req->bindParam(':ico_ref_id', $_POST['intervenant']);
	$req->bindParam(':ico_competence', $_POST['competence']);
	$req->execute();
	$competences_intervenant = $req->fetch();
	
	if($competences_intervenant["ico_controle_dt"]==3 && $competences_intervenant["ico_valide"] !=1){
		echo 3;
	}elseif($competences_intervenant["ico_controle_dt"]==2 && $competences_intervenant["ico_valide"] !=1){
		echo 2;
	}else{ 
		echo 1;
	} 
	
}
