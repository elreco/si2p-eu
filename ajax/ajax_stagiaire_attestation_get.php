<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // RETOURNE LE DONNEE D'UNE ATTESTION STAGIAIRE DANS LE CADRE D'UNE ACTION

 // retour JSON compatible select2
 
 
$erreur_txt="";
if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]); 
	}	
	$stagiaire_id=0;
	if(!empty($_POST["stagiaire"])){
		$stagiaire_id=intval($_POST["stagiaire"]); 
	}
	
	if(empty($action_id) OR empty($stagiaire_id)){
		
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();
		
	}else{

		$sql="SELECT ast_attestation FROM Actions_Stagiaires WHERE ast_action=:action_id AND ast_stagiaire=:stagiaire_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("action_id",$action_id);
		$req->bindParam("stagiaire_id",$stagiaire_id);
		$req->execute();
		$d_sta_attestation=$req->fetch();
		if(empty($d_sta_attestation)){
			$erreur_txt="A Impossible de récupérer l'attestation";
			echo($erreur_txt);
			die();
		}
		
		$donnees=array();
		
		// on recup l'attestation

		$sql="SELECT att_titre FROM Attestations WHERE att_id=" . $d_sta_attestation["ast_attestation"] . ";";
		$req=$Conn->query($sql);
		$d_attestation=$req->fetch();
		if(empty($d_attestation)){
			$erreur_txt="B Impossible de récupérer l'attestation";
			echo($erreur_txt);
			die();
		}else{
			$donnees["titre"]=$d_attestation["att_titre"];
		}
		
		
		// on recup la liste des competences
		$sql="SELECT aco_id,aco_competence FROM Attestations_competences WHERE aco_attestation=" . $d_sta_attestation["ast_attestation"] . ";";
		$req=$Conn->query($sql);
		$d_competences=$req->fetchAll();
		if(!empty($d_competences)){
			foreach($d_competences as $comp){
				$donnees["competences"][$comp["aco_id"]]=array(
					"competence" => $comp["aco_competence"],
					"acquise" => 1
				);
			}	
		}
		
		// on recupère l'état de validation de chaque compétence
		
		$sql="SELECT sac_attest_competence,sac_acquise FROM Stagiaires_Attestations_Competences WHERE sac_stagiaire=:stagiaire_id AND sac_action=:action_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("action_id",$action_id);
		$req->bindParam("stagiaire_id",$stagiaire_id);
		$req->execute();
		$d_sta_competences=$req->fetchAll();
		if(!empty($d_sta_competences)){
			foreach($d_sta_competences as $s_comp){
				$donnees["competences"][$s_comp["sac_attest_competence"]]["acquise"]=$s_comp["sac_acquise"];
			}	
		}
		echo json_encode($donnees);
		
	}
}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}
?>