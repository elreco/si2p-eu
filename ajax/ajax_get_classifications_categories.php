<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

 // RETOURNE LES INFOS SOUS-SECTEURS D'ACTIVITE LiE A UN SECTEUR 
 
 // RETOUR FORMAT JSON
 
$erreur=0;
if(isset($_POST)){
	 
	$classification=0;
	if(!empty($_POST["classification"])){
		$classification=intval($_POST["classification"]); 
	}
	
	$retour=array();
	
	if($classification>0){

		$req=$Conn->prepare("SELECT * FROM clients_classifications_categories WHERE ccc_classification=:ccc_classification");
		$req->bindParam(":ccc_classification",$classification);
		$req->execute();
		$classification = $req->fetchAll();
		if(!empty($classification)){
			foreach($classification as $dss){
				$retour[]=array(
					"id" => $dss["ccc_id"],
					"text" => $dss["ccc_libelle"]
				);	
			}
			
		}
	}
	echo json_encode($retour);	
}else{
	echo("Erreur paramètre!");
}
 
?>