<?php
	include "../includes/controle_acces.inc.php";

	include '../includes/connexion.php';
	include '../includes/connexion_soc.php';

	 // RETOURNE LA LISTE DES ACTIONS COMPATIBLE
	 
	 
	$erreur="";
	if(isset($_POST)){
		if(!empty($_POST)){
			
			$client=0;
			if(!empty($_POST["client"])){
				$client=intval($_POST["client"]); 
			}else{
				$erreur="Paramètres absent";
			}
			
			$action=0;
			if(!empty($_POST["action"])){
				$action=intval($_POST["action"]); 
			}
			
			$famille=0;
			if(!empty($_POST["famille"])){
				$famille=intval($_POST["famille"]); 
			}
			
			$sous_famille=0;
			if(!empty($_POST["sous_famille"])){
				$sous_famille=intval($_POST["sous_famille"]); 
			}
			
			$sous_sous_famille=0;
			if(!empty($_POST["sous_sous_famille"])){
				$sous_sous_famille=intval($_POST["sous_sous_famille"]); 
			}
			
			$type=0;
			if(!empty($_POST["type"])){
				$type=intval($_POST["type"]); 
			}
		}
	}

	if(empty($erreur)){
			
		$d_devis=array();
		
		$sql_pro="SELECT pro_id FROM Produits WHERE pro_archive=1;";
		$req_pro=$Conn->query($sql_pro);
		$src_produits=$req_pro->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($src_produits)){
			$tab_pro=array_column ($src_produits,"pro_id");
			$liste_pro=implode($tab_pro,",");
		}
			
		$sql="SELECT dev_id,dev_numero,dli_id,dli_reference,dli_libelle,dli_qte,dli_ht,dli_montant_ht,dli_derogation,dli_produit,dli_famille,dli_sous_famille,dli_sous_sous_famille,dli_type 
		FROM Devis,Devis_Lignes WHERE dev_id=dli_devis
		AND dev_client=" . $client . " AND (dli_action_client=0 OR dli_action=" . $action . ") AND dli_categorie=1 AND dev_esperance=100";
		if(!empty($liste_pro)){
			$sql.=" AND NOT dli_produit IN (" . $liste_pro . ")";	
		}
		if($famille>0){
			$sql.=" AND dli_famille=" . $famille;
		}
		if($sous_famille>0){
			$sql.=" AND dli_sous_famille=" . $sous_famille;
		}
		if($sous_sous_famille>0){
			$sql.=" AND dli_sous_sous_famille=" . $sous_sous_famille;
		}
		if($type>0){
			$sql.=" AND dli_type=" . $type;
		}
		$sql.=" ORDER BY dev_numero,dli_reference;";
		$req=$ConnSoc->query($sql);
		$d_result=$req->fetchAll();
		if(!empty($d_result)){
			foreach($d_result as $r){
				
				$text=$r["dev_numero"] . " - " . $r["dli_reference"];
				if($r["dli_type"]==1){
					$text.=" (INTRA)";
				}else{
					$text.=" (INTER)";	
				}
				$d_devis[]=array(
					"id" => $r["dli_id"],
					"text" => $text,
					"famille" => $r["dli_famille"],
					"sous_famille" => $r["dli_sous_famille"],
					"sous_sous_famille" => $r["dli_sous_sous_famille"],
					"type" => $r["dli_type"],
					"produit" => $r["dli_produit"]
				);
			}
		}
		echo json_encode($d_devis);
		die();

	}
	if(!empty($erreur)){
		echo($erreur);
		die();
	}
?>