<?php

if(date("n")<=6){
    $data['date_debut'] = "01/06/" . date('Y', strtotime('-1 year'));
    $data['date_fin'] = "01/05/" . date("Y");
}else{
    $data['date_debut'] = "01/06/" . date("Y");
    $data['date_fin'] = "01/05/" . date('Y', strtotime('+1 year'));
}

echo json_encode($data);

?>