<?php
include("../includes/connexion.php");
include('../modeles/mod_document.php');

session_start();

if(!isset($erreur)){
	$erreur=0;
}

$url_dossier_cible = $_POST['coller'];

foreach($_SESSION['etech_couper_id_all'] as $check) {

	/* DEBUT COLLER */
	$doc = get_document($check);
	// chercher la liste des utilisateurs ayant accès au document
	$req=$Conn->prepare("SELECT uti_id FROM Utilisateurs LEFT JOIN documents_utilisateurs ON (Utilisateurs.uti_id = documents_utilisateurs.dut_utilisateur) WHERE dut_document = " . $doc['doc_id']);
	$req->execute();
	$utilisateurs = $req->fetchAll();
	
	$url_dossier_source = $doc['doc_url'] . $doc['doc_nom_aff'] . "/";
	
	$position = strpos($url_dossier_cible, $url_dossier_source);

	if((is_int($position) && $position > 0) OR is_bool($position)){

			// MAJ DU DOCUMENT
			update_document_coller($doc['doc_id'],$url_dossier_cible);
			
			/*on verifie que u doit encore avori accès au dossier source
			Attention toujour effectuer cette requete après maj sinon doc est toujours dans source 
			et delete_affichage_dossier ne marche pas */
			delete_affichage_dossier($utilisateurs, $url_dossier_source);
			
			set_affichage_dossier($utilisateurs, $url_dossier_cible);
			
			$sous_docs = get_documents_like($doc['doc_url'] . $doc['doc_nom_aff'] . "/", $doc['doc_id']);
			foreach($sous_docs as $s){
				
				$url_sous_dossier=str_replace($doc['doc_url'],$url_dossier_cible,$s['doc_url']);
				
				update_document_coller($s['doc_id'],$url_sous_dossier);
			}
		}else{
			$erreur=1;
		}
	}
	
	unset($_SESSION['etech_couper_id_all']);
	echo $erreur;
	/* FIN COLLER */
?>