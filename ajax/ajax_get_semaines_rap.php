<?php
 // RECHERCHE DE SUSPECT VIA SELECT2

session_start();
include('../includes/connexion.php');
include('../includes/connexion_soc.php');
include('../modeles/mod_parametre.php');
$erreur="";

// DONNEE POUR TRAITEMENT

// sur la personne connecte
$sql="SELECT uti_saisie_rapport FROM Utilisateurs WHERE uti_id = " . $_GET['utilisateur'];
/* $sql="SELECT uti_saisie_rapport FROM Utilisateurs WHERE uti_id = 86"; */
$req = $Conn->query($sql);
$d_uti=$req->fetch();
$_SESSION["rap_nb_semaines"] = $_GET['nb_semaines'];
if($_GET['drt_voir']){
    $nb_semaines = $_GET['nb_semaines'];

    if($nb_semaines == 1 OR empty($nb_semaines)){
        $nb_semaines = 5;
    }else{
        $nb_semaines = datediffInWeeks('1/1/' . $nb_semaines, date("m/d/Y"));
    }
}else{
    $nb_semaines = $d_uti['uti_saisie_rapport'];
}




    $date = new DateTime();

    $currentweek = $date->format("W");
    if($date->format("w") < 3){
        $currentweek = $currentweek - 1;
    }

    $year = date("Y");
    // ALLER CHERCHER LES TROIS SEMAINES SUIVANTES
    $week_number  = intval(sprintf("%02d", $currentweek));

    for ($i=$currentweek; $i < $currentweek + 3; $i++) {

        if($week_number == 0){
            $week_number = 1;

        }

        if(date("Y") == $year && $date->format("W") == sprintf("%02d", $week_number)){
            if($week_number == 1){
                $year = $year;
            }
            $weeks[$week_number . "-" . $year]["current"] = true;

        }else{

            $weeks[$week_number . "-" . $year]["current"] = false;
        }
        $weeks[$week_number . "-" . $year]['semaine'] = $week_number;
        $weeks[$week_number . "-" . $year]['annee'] = $year;
        $week_number = $week_number + 1;
    }

    if(!empty($nb_semaines)){
        $week_number  = intval(sprintf("%02d", $currentweek));
        $year = date("Y");
        for ($i=$currentweek; $i > $currentweek - $nb_semaines; $i--) {
            $i = intval(sprintf("%02d", $i));
                if($week_number == 0){
                    $week_number = 52;
                }

                 if(date("Y") == $year && $date->format("W") == sprintf("%02d", $week_number)){

                    $weeks[$week_number . "-" . $year]["current"] = true;
                }else{
                    if($week_number == 52){
                        $year = $year -1;
                    }
                    $weeks[$week_number . "-" . $year]["current"] = false;
                }
                $weeks[$week_number . "-" . $year]['semaine'] = $week_number;
                $weeks[$week_number . "-" . $year]['annee'] = $year;

                $week_number = $week_number - 1;


        }
    }
    foreach($weeks as $w){

        $date_get = getStartAndEndDate($w['semaine'], $w['annee']);
        $retour[] = array(
            "current" => $w['current'],
            "semaine" => $w['semaine'],
            "annee" => $w['annee'],
            "dimanche" => convert_date_txt($date_get['week_start']),
            "samedi" => convert_date_txt($date_get['week_end']),

        );
    }
    usort($retour, 'sortByOrder');
function sortByOrder($a, $b) {
    return strtotime(convert_date_sql($b['dimanche'])) - strtotime(convert_date_sql($a['dimanche']));
}

// ENVOIE DU RESULTAT
if(!empty($retour)){
	echo json_encode($retour);
}
?>
