<?php
 // PERMET DE PRE-SELECTIONNER DES UTILISATEURS D'APRES UNE SAISIE UTILISATEUR
 

include("../includes/connexion.php");

session_start();
$erreur="";

/* UTILISATION 
- commande_tri.php
 */

// parametre
$param="";
if($_GET['q']){
	$param=$_GET['q'] . "%";
}

$famille_type=0;
if(!empty($_GET['famille_type'])){
	$famille_type=intval($_GET['famille_type']);
}

$sql="SELECT fou_id, fou_nom, fou_code FROM fournisseurs";
if(!empty($famille_type)){
	$sql.=" INNER JOIN Fournisseurs_Familles_Jointure ON (fournisseurs.fou_id=Fournisseurs_Familles_Jointure.ffj_fournisseur AND ffj_famille=". $famille_type . ")";
}
$sql.=" WHERE (fou_blacklist = 0 OR fou_blacklist IS NULL) AND (fou_archive = 0 OR fou_archive IS NULL) AND (fou_nom LIKE '" . $param . "' OR fou_code LIKE '" . $param . "')";
$req = $Conn->query($sql);
$fournisseurs = $req->fetchAll();
if(!empty($fournisseurs)){
	foreach($fournisseurs as $c){
		$retour[] = array(
			"id"=>$c['fou_id'],
			"nom"=>$c['fou_nom'] . " (" . $c['fou_code'] . ")"
		);
	}
}

// ENVOIE DU RESULTAT
if(!empty($retour)){
	echo json_encode($retour);	
}
?>
