	
<?php
 // PERMET DE PRE-SELECTIONNER DES CLIENTS D'APRES UNE SAISIE UTILISATEUR
 
session_start();
include('../includes/connexion.php');

if(!empty($_POST["utilisateur"])){
	
	$utilisateur=intval($_POST["utilisateur"]);
	
	$sql="SELECT uti_nom,uti_prenom FROM Utilisateurs WHERE uti_id=:utilisateur;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":utilisateur",$utilisateur);
	$req->execute();
	$utilisateur=$req->fetch();
	if(!empty($utilisateur)){
		echo json_encode($utilisateur);	
	}
}
?>
