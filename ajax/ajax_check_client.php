<?php

include("../includes/connexion.php");
include("../includes/connexion_soc.php");
include('../modeles/mod_check_client.php');
include('../modeles/mod_check_siret.php');

// PERMET DE CONTOLE SI DONNEE UNIQUE DE CHAQUE CLIENT EXISTE DEJA

// mod_check_client -> test les données clients (code nom siren) enregistré dans clients ou suspects
// mod_check_siret -> test les données client adresse (code nom siren) enregistré dans Adresses ou suspects_adresses

if(isset($_POST)){

	$code="";
	if(!empty($_POST['code'])){
		$code=$_POST['code'];
	}
	$nom="";
	if(!empty($_POST['nom'])){
		$nom=$_POST['nom'];
	}
	
	$siren="";
	if(!empty($_POST['siren'])){
		$siren=$_POST['siren'];
	}
	
	$siret="";
	if(!empty($_POST['siret'])){
		$siret=$_POST['siret'];
	}
	
	$client=0;
	if(!empty($_POST['client'])){
		$client=intval($_POST['client']);
	}
	
	$suspect=0;
	if(!empty($_POST['suspect'])){
		$suspect=intval($_POST['suspect']);
	}
	
	$adresse=0;
	if(!empty($_POST['adresse'])){
		$adresse=intval($_POST['adresse']);
	}
	
	// la personne conneté
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	
	// Format de retour
	$doublon=array(
		"doublon" => false,	
		"doublon_code" => 0,		
		"doublon_code_text" => "",
		"doublon_nom" => 0,		
		"doublon_nom_text" => "",
		"doublon_siren" => 0,		
		"doublon_siren_text" => "",
		"doublon_siret" => 0,		
		"doublon_siret_text" => "",
	);
	
	if((empty($code) AND empty($nom) AND empty($siren))){
		
		echo("Vérification impossible");
		die();
		
	}else{
		
		// on verifie si les infos existe dans la base clients
		$verif=check_client($client,$suspect,$code,$nom,$siren);	
		if(is_array($verif)){
			$result["resultat"] =$verif["resultat"];
			foreach($verif["resultat_text"] as $message){
				$result["resultat_text"] = $message . "<br/>";		
			}
		}
		
		// on regarde si les données existe déjà dans le base suspect
		$verif=check_suspect_2($acc_societe,$suspect,$adresse,$code,$nom,$siren,$siret);	
		if(is_array($verif)){
			$result["resultat"] =$verif["resultat"];
			foreach($verif["resultat_text"] as $message){
				$result["resultat_text"] = $message . "<br/>";		
			}
		}
		
		
		if($verif
		
	
		
		// CONTROLE DU SIRET
		/*if(!empty($siret)){
			
			$test=check_siret($siren,$siret,0,0);
			if(!empty($test)){
				$message_test = "";
				foreach($test as $t){
					
					$sql="SELECT soc_nom FROM societes WHERE soc_id =" . $t['cso_societe'];
					$req = $Conn->prepare($sql);
					$req->execute();
					$soc_nom = $req->fetch();
					if(empty($message_test)){
						$message_test = $soc_nom['soc_nom'];
					}else{
						$message_test = $message_test . ", " . $soc_nom['soc_nom'];
					}
					
				}
				$result["resultat"] = false;
				$result["resultat_text"] = "Le siret " . $siren . " " . $siret . " est déjà utilisé sur " . $message_test;
			}
		}*/
		
	}
	/*$conn_get_id = $societe;

	include("../includes/connexion_get.php");

	$verif=check_suspect($societe,$code,$nom,"");
	if(is_array($verif)){
		$result["resultat"] =$verif["resultat"];
		foreach($verif["resultat_text"] as $message){
			$result["resultat_text"] = $message . "<br/>";		
		}
		
	}*/
	
	// CONTROLE DU SIRET
	/*if(!empty($siret)){
		
		$test=check_siret_suspect($siren,$siret,0,$societe,0);
		if(!$test){
			$result["resultat"] = false;
			$result["resultat_text"] = "Le siret " . $siren . " " . $siret . " est déjà utilisé sur la société " . $soc_nom['soc_nom'];
		}
	}*/
	echo(json_encode($result));
	

}
?>