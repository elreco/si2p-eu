<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';


	 // MISE A JOUR DES VALEURS OPCA ET NUM BC

	$erreur_txt="";
	if(isset($_POST)){
		 
		$fli_facture=0;
		if(!empty($_POST["fli_facture"])){
			$fli_facture=intval($_POST["fli_facture"]); 
		}
		$fli_id=0;
		if(!empty($_POST["fli_id"])){
			$fli_id=intval($_POST["fli_id"]); 
		}
		if(empty($fli_facture) OR empty($fli_id)){
			$erreur_txt="Formulaire incomplet!";
			echo($erreur_txt);
			die();	
		}
	}else{
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();
	}
	
	// DONNEE POUR MAJ
	
	// personne connecte
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	if($conn_soc_id!=$acc_societe){
		$acc_societe = $conn_soc_id;
	}

	// sur la facture
	$sql="SELECT fac_numero FROM Factures WHERE fac_id=:fli_facture;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":fli_facture",$fli_facture);
	$req->execute();
	$d_facture=$req->fetch();
	if(empty($d_facture)){
		$erreur_txt="Impossible de charger les données de la facture!";
		echo($erreur_txt);
		die();
	}
	
	// UPDATE DE LA LIGNE

	$sql="UPDATE Factures_Lignes SET fli_opca_num=:fli_opca_num,fli_bc_num=:fli_bc_num WHERE fli_id=:fli_id AND fli_facture=:fli_facture;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam("fli_opca_num",$_POST["fli_opca_num"]);
	$req->bindParam("fli_bc_num",$_POST["fli_bc_num"]);
	$req->bindParam("fli_facture",$fli_facture);
	$req->bindParam("fli_id",$fli_id);
	try{
		$req->execute();
	}Catch(Exception $e){
		echo($e->getMessage());
		die();
	}
	
	// MAJ OK TRAITEMENT ANNEXE
	if(file_exists('../documents/Societes/' . $acc_societe . '/Factures/'.$d_facture["fac_numero"] . '.pdf')){	
		unlink('../documents/Societes/' . $acc_societe . '/Factures/'.$d_facture["fac_numero"] . '.pdf');
	}
	$retour=array(
		"opca_num" => $_POST["fli_opca_num"],
		"bc_num" => $_POST["fli_bc_num"],
		"ligne" => $fli_id
	);
	
	echo(json_encode($retour));
	die();
?>