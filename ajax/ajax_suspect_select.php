<?php
 // RECHERCHE DE SUSPECT VIA SELECT2

session_start();
include('../includes/connexion_soc.php');

$erreur="";
$param="";
if($_GET['q']){
	$param=$_GET['q'] . "%";
}
// DONNEE POUR TRAITEMENT

// sur la personne connecte

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];	
}
$acc_utilisateur=0;
if($_SESSION['acces']['acc_ref']==1){
	if(isset($_SESSION['acces']["acc_ref_id"])){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];	
	}
}

$sql="SELECT sus_id,sus_nom,sus_code,sco_id FROM Suspects LEFT JOIN Suspects_Correspondances ON (Suspects.sus_id=Suspects_Correspondances.sco_suspect)
 WHERE ISNULL(sco_id) AND (sus_nom LIKE '" . $param . "' OR sus_code LIKE '" . $param . "')";
if(!$_SESSION['acces']["acc_droits"][6]){
	$sql.=" AND sus_utilisateur=" . $acc_utilisateur;	
}
if($acc_agence>0){
	$sql.=" AND sus_agence=" . $acc_agence;	
}
$sql.=" ORDER BY sus_code,sus_code";
$req = $ConnSoc->query($sql);
$suspects = $req->fetchAll();
if(!empty($suspects)){
	foreach($suspects as $c){
		$retour[] = array("id"=>$c['sus_id'], "nom"=>$c['sus_nom'] . " (" . $c['sus_code'] . ")");
	}
}
// ENVOIE DU RESULTAT
if(!empty($retour)){
	echo json_encode($retour);	
}
?>
