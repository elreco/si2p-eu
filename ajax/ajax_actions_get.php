<?php
include "../includes/controle_acces.inc.php";

include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // RETOURNE LA LISTE DES ACTIONS COMPATIBLE
 
 
$erreur="";
if(isset($_POST)){
	if(!empty($_POST)){
		
		$client=0;
		if(!empty($_POST["client"])){
			$client=intval($_POST["client"]); 
		}else{
			$erreur="Paramètres absent";
		}
		$famille=0;
		if(!empty($_POST["famille"])){
			$famille=intval($_POST["famille"]); 
		}else{
			$erreur="Paramètres absent";
		}
		$type=0;
		if(!empty($_POST["type"])){
			$type=intval($_POST["type"]); 
		}else{
			$erreur="Paramètres absent";
		}
		
		if(empty($erreur)){
			
			$sous_famille=0;
			if(!empty($_POST["sous_famille"])){
				$sous_famille=intval($_POST["sous_famille"]); 
			}
			
			$sous_sous_famille=0;
			if(!empty($_POST["sous_sous_famille"])){
				$sous_sous_famille=intval($_POST["sous_sous_famille"]); 
			}
			
			// ON RECUPERE LA BASE PRODUIT
			
			$d_produits=array();
			$sql="SELECT pro_id,pro_code_produit FROM Produits WHERE pro_categorie=1 AND pro_famille=" . $famille;
			if(!empty($sous_famille)){
				$sql.=" AND pro_sous_famille=" . $sous_famille;	
			}
			if(!empty($sous_sous_famille)){
				$sql.=" AND pro_sous_sous_famille=" . $sous_sous_famille;	
			}
			$sql.=" ORDER BY pro_id;";
			$req=$Conn->query($sql);
			$base_produits=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($base_produits)){
				foreach($base_produits as $b_p){
					$d_produits[$b_p["pro_id"]]=$b_p["pro_code_produit"];
				}
			}
	
			
			$sql="SELECT act_id,act_planning_txt, DATE_FORMAT(act_date_deb,'%d/%m/%Y') AS act_date_deb_fr,act_intervenant,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville
			,int_label_1,act_produit";
			if($type==1){
				$sql.=",acl_id";
			}
			$sql.=" FROM Actions INNER JOIN Intervenants ON (Actions.act_intervenant=Intervenants.int_id)";
			if($type==1){
				$sql.=" LEFT JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action AND NOT acl_archive)";
			}
			$sql.=" WHERE NOT act_archive AND act_date_deb>=NOW() AND act_pro_famille=" . $famille;
			if(!empty($sous_famille)){
				$sql.=" AND act_pro_sous_famille=" . $sous_famille;	
			}
			if(!empty($sous_sous_famille)){
				$sql.=" AND act_pro_sous_sous_famille=" . $sous_sous_famille;	
			}
			if($type==1){
				// INTRA
				$sql.=" AND act_pro_type=1 AND (acl_client=" . $client . " OR act_intra_multi)";		
			}else{
				$sql.=" AND act_pro_type=2";	
			}
			$sql.=" ORDER BY act_date_deb;";
			$req=$ConnSoc->query($sql);
			$d_actions=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($d_actions)){
				foreach($d_actions as $k_act => $action){
					$d_actions[$k_act]["act_pro_reference"]=$d_produits[$action["act_produit"]];
				}
			}
			echo json_encode($d_actions);
		}
	}else{
		$erreur="Paramètres absent";
	}
}else{
	$erreur="Paramètres absent";
}

if(!empty($erreur)){
	echo($erreur);
}
?>