<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

// RETOURNE LES SOUS CATEGORIES ASSOCIES à UNE CATEGORIE DE CLIENT

// retour => tableau JSON (compatible Select2)


$erreur=0;
$sous_categories=array();

if(isset($_POST)){

	$categorie=0;
	if(!empty($_POST["categorie"])){
		$categorie=intval($_POST["categorie"]);
	}else{
		$erreur=1;
	}

	if($erreur==0){

		$sql="SELECT * FROM Clients_Sous_Categories WHERE csc_categorie=" . $categorie . ";";
		$req=$Conn->query($sql);
		$results=$req->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$sous_categories[]=array(
					"id" => $r["csc_id"],
					"text" => $r["csc_libelle"]
				);
			}
		}
	}
}else{
	$erreur=10;
}
if($erreur==0){
	echo json_encode($sous_categories);
};
?>
