<?php
include "../includes/controle_acces.inc.php";

include '../includes/connexion.php';
include '../includes/connexion_soc.php';

// RETOURNE LA LISTE DES ACTIONS COMPATIBLE
$erreur = "";
$montant = 0;
// indemnités kilométriques

$req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id = :uti_id");
$req->bindValue(':uti_id', $_POST['uti_id']);
$req->execute();
$utilisateur = $req->fetch();

$req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id = :ndf_id");
$req->bindValue(':ndf_id', $_POST['id']);
$req->execute();
$ndf = $req->fetch();

if (empty($ndf['ndf_puissance'])) {
    $ndf_puissance = $utilisateur['uti_veh_cv'];
    $ndf_tranche_km = $utilisateur['uti_tranche_km'];

    $req = $Conn->prepare("SELECT * FROM baremes_kilometres WHERE bki_date_deb <= :bki_date_deb AND bki_date_fin >= :bki_date_deb");
    $req->bindValue(':bki_date_deb', date("Y-m-d"));
    $req->execute();
    $bareme_kilometre = $req->fetch();

    if (!empty($bareme_kilometre)) {
        $ndf_bareme = $bareme_kilometre['bki_id'];
    } else {
        $erreur = "Bareme introuvable";
    }
} else {
    $ndf_puissance = $ndf['ndf_puissance'];
    $ndf_tranche_km = $ndf['ndf_tranche_km'];
    $ndf_bareme = $ndf['ndf_bareme'];
}

if (empty($erreur)) {
    $req = $Conn->prepare("SELECT * FROM baremes_kilometres_lignes WHERE
        bkl_bareme = :ndf_bareme
        AND bkl_puissance = :ndf_puissance
        AND bkl_tranche_km = :ndf_tranche_km");
    $req->bindValue(':ndf_puissance', $ndf_puissance);
    $req->bindValue(':ndf_tranche_km', $ndf_tranche_km);
    $req->bindValue(':ndf_bareme', $ndf_bareme);
    $req->execute();
    $bareme_kilometre_ligne = $req->fetch();

    if (!empty($bareme_kilometre_ligne)) {
        $montant = ($_POST['nli_qte'] * $bareme_kilometre_ligne['bkl_coeff']);
        // (cal_qte*km_coeff)+1*km_bonus;
    } else {
        $erreur = "Le barème kilométrique n'est pas pris en compte pour votre véhicule.";
    }
}


if (!empty($erreur)) {
    echo json_encode([
        "erreur_txt" => $erreur
    ]);
    die();
} else {
    echo json_encode($montant);
    die();
}
