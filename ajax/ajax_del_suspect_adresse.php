<?php
include('../includes/connexion_soc.php');


// PERMET DE SUPPRIMER UNE ADRESSE D'UN SUSPECT

$erreur="";
if(isset($_POST)){

	$adresse=0;
	if(!empty($_POST["adresse"])){
		$adresse=intval($_POST["adresse"]);
	}
	
	$suspect=0;
	if(!empty($_POST["suspect"])){
		$suspect=intval($_POST["suspect"]);
	}


	
	if($adresse>0 AND $suspect>0){
		
		// modele pas nécéssaire
		
		$sql="SELECT sad_type FROM Suspects_Adresses WHERE sad_id=" . $adresse . " AND sad_ref_id=" . $suspect . ";";
		$req=$ConnSoc->query($sql);
		$d_adresse=$req->fetch();
		if(empty($d_adresse)){
			$erreur="2-ERREUR DONNEE";
		}
		
		if(empty($erreur)){
			$sql="DELETE FROM Suspects_Adresses WHERE sad_id=" . $adresse . " AND sad_ref_id=" . $suspect . ";";
			try{
				$req=$ConnSoc->query($sql);
			}catch (Exception $e){
				$erreur=$e->getMessage();
			}
		}

	}else{
		$erreur="1-ERREUR PARAMETRE";
	}
}else{
	$erreur="2-ERREUR PARAMETRE";
}

if($erreur==""){
	
	$result_supp=array(
		"adresse" => $adresse,
		"type" => $d_adresse["sad_type"]
	);
	echo json_encode($result_supp);
	
}else{
	echo $erreur;
}
?>