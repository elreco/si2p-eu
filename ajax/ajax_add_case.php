<?php
include("../includes/connexion.php");
include('../includes/connexion_soc.php');

// ENREGISTREMENT D'UNE CASE PLANNING

// format de retour 
$case_maj=array(
	"cases" =>array(
		"type" =>"3",
		"ref_1" => "0",
		"texte" => "",
		"style" => "",
		"survol" => "",
		"date" => array(),
		"erreur" => 0
	),
	"erreur_type" => 0,
	"erreur_code" => 0,
	"erreur_text" => ""
);


if(isset($_GET['type_case'])&&isset($_GET['liste_case'])){
	
	if(isset($_GET['lib_case'])){
		if(!empty($_GET['lib_case'])){
			$case_maj["cases"]["texte"]=$_GET['lib_case'];
		}
	}
	
	// donnee sur la case

	$req=$Conn->prepare("SELECT pca_libelle,pca_couleur FROM Plannings_Cases WHERE pca_id=:type_case;");
	$req->bindParam(":type_case",$_GET['type_case']);
	$req->execute();
	$info_case= $req->fetch();
	if(!empty($info_case)){
		
		$style_bg=$info_case["pca_couleur"];
		
		if(empty($case_maj["cases"]["texte"])){
			$case_maj["cases"]["texte"]=$info_case["pca_libelle"];
		}
		$case_maj["cases"]["style"]="color:#000;background-color:" . $style_bg . ";";
		
		// REQUETE POUR AJOUT OU MAJ DES DATES DU PLANNING
		
		$req_select=$ConnSoc->prepare("SELECT pda_id,pda_type FROM Plannings_Dates WHERE pda_intervenant=:intervenant AND pda_date=:date AND pda_demi=:demi AND pda_archive=0;");
		
		$sql_insert="INSERT INTO Plannings_Dates (pda_intervenant,pda_date,pda_demi,pda_type,pda_ref_1,pda_texte,pda_style_bg,pda_style_txt)";
		$sql_insert.=" VALUES (:intervenant, :date, :demi, 3, :pda_ref_1, :texte, :style_bg, '#000');";		
		$req_insert=$ConnSoc->prepare($sql_insert);
		$req_insert->bindParam(":texte",$case_maj["cases"]["texte"]);
		$req_insert->bindParam(":style_bg",$style_bg);

		$sql_update="UPDATE Plannings_Dates SET pda_texte=:texte, pda_style_bg=:style_bg, pda_style_txt='#000', pda_ref_1=:pda_ref_1 WHERE pda_id=:id;";
		$req_update=$ConnSoc->prepare($sql_update);
		$req_update->bindParam(":style_bg",$style_bg);
		
		// on parcours les dates selectionnées	
		
		$case=explode (",",$_GET['liste_case']);	
		foreach($case as $c){
		
			if(!empty($c)){
				$donnee_case=explode ("_",$c);
				$intervenant=$donnee_case[0];
				$date=$donnee_case[1];
				$demi=$donnee_case[2];
				
				$req_select->bindParam(":intervenant",$intervenant);
				$req_select->bindParam(":date",$date);
				$req_select->bindParam(":demi",$demi);
				$req_select->execute();
				$case_select = $req_select->fetch();
		
				// LA CASE N'EST PAS LIBRE
				if(!empty($case_select)){
					
					// C'EST UNE CASE LIBRE 
					
					if($case_select["pda_type"]==3){
						
						// securite pour evite d'écraser une date 1 action ou 2 conges
						$req_update->bindParam(":id",$case_select["pda_id"]);
						$req_update->bindParam(":texte",$info_case["pca_libelle"]);
						$req_update->bindParam(":pda_ref_1",$_GET['type_case']);	
						$req_update->execute();	
						array_push($case_maj["cases"]["date"],$c);						
						//array_push($case_maj,$c);
						
					}else{
						$case_maj["erreur_type"]=1;
						$case_maj["erreur_text"]="Certaines dates ne sont plus libres";
					}					
				}else{
					// ajout d'une nouvelle date
					$req_insert->bindParam(":intervenant",$intervenant);			
					$req_insert->bindParam(":date",$date);
					$req_insert->bindParam(":demi",$demi);	
					$req_insert->bindParam(":pda_ref_1",$_GET['type_case']);					
					$req_insert->execute();
					array_push($case_maj["cases"]["date"],$c);
					//array_push($case_maj,$c);				
				}
			}
		}
	}
}else{
	$case_maj["erreur_type"]=1;
	$case_maj["erreur_text"]="Paramètres absents";
}
echo json_encode($case_maj);
?>
