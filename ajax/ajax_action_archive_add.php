<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';
include '../includes/connexion_fct.php';
include '../modeles/mod_add_notification.php';

 // ARCHIVE UNE FORMATION COMPLETTE
 // ACTION et ACTIONS CLIENTS

 // RETOUR FORMAT JSON

$erreur_text="";
$retour=array(
	"action" => 0,
	"notif" => ""
);

if(isset($_POST)){

	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]);
	}
	if(empty($action_id)){
		$erreur_text="Formulaire incomplet";
	}

}else{
	$erreur_text="Formulaire incomplet";
}

if(empty($erreur_text)){
	if(!$_SESSION["acces"]["acc_droits"][10]){
		$erreur_text="Vous ne disposez pas des droits nécéssaire!";
	}
}

// CONTROLE
if(empty($erreur_text)){
	$req=$ConnSoc->query("SELECT act_date_deb,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,act_agence FROM Actions WHERE act_id=" . $action_id);
	$d_action=$req->fetch();
	if(empty($d_action)){
		$erreur_text="Impossible de charger les données sur la formation.";
	}
}

// MAJ

if(empty($erreur_text)){

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}

	$archive_ok=0;
	if($_SESSION["acces"]["acc_droits"][11]){
		$archive_ok=1;
	}

	// ON ARCHIVE TOUS LES CLIENTS


	$sql="UPDATE Actions_Clients SET
	acl_archive = 1,
	acl_archive_date=NOW(),
	acl_archive_uti=" . $acc_utilisateur . ",
	acl_archive_ok=" . $archive_ok . "
	WHERE NOT acl_archive AND acl_action=" . $action_id . ";";
	try {
		$req=$ConnSoc->query($sql);
	}catch( PDOException $Exception ) {
		$erreur_text="Impossible d'archiver les inscriptions des clients " . $Exception->getMessage();
	}

	// ARCHIVAGE DE L'ACTION
	if(empty($erreur_text)){
		// archivage de l'action
		$sql="UPDATE Actions SET
		act_archive = 1,
		act_archive_date=NOW(),
		act_archive_uti=" . $acc_utilisateur . ",
		act_archive_ok=" . $archive_ok . "
		WHERE act_id=" . $action_id . ";";
		try {
			$req=$ConnSoc->query($sql);
		}catch( PDOException $Exception ) {
			$erreur_text="Impossible d'archiver l'action " . $Exception->getMessage();
		}
	}

	if(empty($erreur_text)){
		// archivage de les actions client
		$sql="UPDATE Actions_Clients SET
		acl_archive = 1,
		acl_archive_date=NOW(),
		acl_archive_uti=" . $acc_utilisateur . ",
		acl_archive_ok=" . $archive_ok . "
		WHERE acl_action=" . $action_id . ";";
		try {
			$req=$ConnSoc->query($sql);
		}catch( PDOException $Exception ) {
			$erreur_text="Impossible d'archiver l'action client " . $Exception->getMessage();
		}
	}

	// ARCHIVAGE DES DATES DU PLANNING
	if(empty($erreur_text)){

		$sql="UPDATE Plannings_Dates SET pda_archive=1
		WHERE pda_type=1 AND pda_ref_1=" . $action_id . ";";
		try {
			$req=$ConnSoc->query($sql);
		}catch( PDOException $Exception ) {
			$erreur_text="Impossible d'archiver les dates de formations " . $Exception->getMessage();
		}
	}

	if(empty($erreur_text)){

		// on memorise l'action pour le retour

		$retour["action"]=$action_id;

		$form_nom="";
		if(!empty($d_action["act_pro_sous_sous_famille"])){
			$req=$Conn->query("SELECT pss_libelle FROM Produits_Sous_Sous_Familles WHERE pss_id=" . $d_action["act_pro_sous_sous_famille"]);
			$d_fam=$req->fetch();
			if(!empty($d_fam)){
				$form_nom=$d_fam["pss_libelle"];
			}
		}elseif(!empty($d_action["act_pro_sous_famille"])){
			$req=$Conn->query("SELECT psf_libelle FROM Produits_Sous_Familles WHERE psf_id=" . $d_action["act_pro_sous_famille"]);
			$d_fam=$req->fetch();
			if(!empty($d_fam)){
				$form_nom=$d_fam["psf_libelle"];
			}
		}elseif(!empty($d_action["act_pro_famille"])){
			$req=$Conn->query("SELECT pfa_libelle FROM Produits_Familles WHERE pfa_id=" . $d_action["act_pro_famille"]);
			$d_fam=$req->fetch();
			if(!empty($d_fam)){
				$form_nom=$d_fam["pfa_libelle"];
			}
		}

		$dt_notif = DateTime::createFromFormat('Y-m-d',$d_action["act_date_deb"]);
		$notif_date=$dt_notif->format('Y-m-d');

		// message success a afficher
		$retour["notif"]="La formation " . $form_nom . " du " . $notif_date . " N°" . $action_id . " a bie été archivée!";

		// ON GENERE UNE NOTIFICATION
		if(!$archive_ok){

			$texte=$_SESSION["acces"]["acc_prenom"] . " " . $_SESSION["acces"]["acc_nom"] . " a archivé la formation " . $form_nom . " du " . $notif_date . " N°" . $action_id;
			$not_url="action_voir.php?action=" . $action_id;
			$sql="SELECT * FROM utilisateurs WHERE uti_responsable = :uti_id AND uti_id != 98";
			$sql="SELECT uti_responsable FROM utilisateurs WHERE uti_id = :uti_id";
			$req=$Conn->prepare($sql);
			$req->bindParam(":uti_id",$_SESSION["acces"]["acc_ref_id"]);
			$req->execute();
			$uti_co=$req->fetch();

			$sql="SELECT * FROM utilisateurs WHERE uti_id = :uti_id AND uti_id != 98";
			$req=$Conn->prepare($sql);
			$req->bindParam(":uti_id",$uti_co['uti_responsable']);
			$req->execute();
			$responsable=$req->fetch();
			if(!empty($responsable)){
				$sql="SELECT * FROM utilisateurs_droits WHERE udr_utilisateur = :uti_id AND udr_droit = 11";
				$req=$Conn->prepare($sql);
				$req->bindParam(":uti_id",$responsable['uti_id']);
				$req->execute();
				$responsable_droit=$req->fetch();

				if(!empty($responsable_droit)){
					add_notifications("<i class='fa fa-times' ></i>",$texte,"bg-danger",$not_url,"","",$responsable['uti_id'],$acc_societe,$d_action["act_agence"],1);
				}else{
					$sql="SELECT * FROM utilisateurs WHERE uti_responsable = :uti_id AND uti_id != 98";
					$req=$Conn->prepare($sql);
					$req->bindParam(":uti_id",$responsable['uti_id']);
					$req->execute();
					$responsable_bis=$req->fetch();
					add_notifications("<i class='fa fa-times' ></i>",$texte,"bg-danger",$not_url,"","",$responsable_bis['uti_id'],$acc_societe,$d_action["act_agence"],1);
				}
			}
			//add_notifications("<i class='fa fa-times' ></i>",$not_texte,"bg-danger",$not_url,"",11,"",$acc_societe,$d_action["act_agence"],1);
		}

	}

    if(empty($erreur_text)){
        // Selection des actions
        $sql="SELECT * FROM Actions_Clients WHERE acl_action=:action_id";
        $req=$ConnSoc->prepare($sql);
        $req->bindParam(":action_id",$action_id);
        $req->execute();
        $actions_clients_exists = $req->fetchAll();
        // Suppression interco
        foreach($actions_clients_exists as $ace){
            $sql="DELETE FROM Actions_Clients_Intercos WHERE aci_action_client=:aci_action_client;";
            $req=$ConnSoc->prepare($sql);
            $req->bindParam(":aci_action_client",$ace['acl_id']);
            $req->execute();
        }
        // Selection des actions
        $sql="SELECT soc_id FROM Societes WHERE soc_archive = 0";
        $req=$Conn->prepare($sql);
        $req->execute();
        $d_societes = $req->fetchAll();
        // Suppression interco
        foreach($d_societes as $s){
            $ConnFct=connexion_fct($s['soc_id']);
            $sql="DELETE FROM Actions_Clients_Intercos WHERE aci_action_interco=:aci_action_client_interco AND aci_action_client_interco_soc = :soc;";
            $req=$ConnFct->prepare($sql);
            $req->bindParam(":aci_action_client_interco",$action_id);
            $req->bindParam(":soc",$_SESSION['acces']['acc_societe']);
            $req->execute();
        }
    }
}
if(empty($erreur_text)){
	echo json_encode($retour);
}else{
	echo($erreur_text);
};


?>
