<?php

include "../includes/controle_acces.inc.php";
include("../includes/connexion.php");
include("../modeles/mod_parametre.php");
include("../modeles/mod_competence.php");
include("../modeles/mod_diplome.php");

////////////////////////////// MISE A JOUR COMPETENCES ////////////////////////////////
// toutes les compétences
$req = $Conn->prepare("SELECT * FROM competences");
$req->execute();
$competences = $req->fetchAll();
// tous les documents des fournisseurs avec le type 2
$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_type = 2");
$req->execute();
$fournisseurs_documents_intervenants = $req->fetchAll();
// j'actualise les competences de l'intervenant
foreach($competences as $c){
	$req = $Conn->prepare("SELECT * FROM intervenants_competences WHERE ico_competence=" . $c['com_id'] . " AND ico_ref = 2 AND ico_ref_id =" . $_POST['fournisseur']);
	$req->execute();
	$competences_act_intervenant = $req->fetch();

	if(empty($competences_act_intervenant)){
		$req = $Conn->prepare("INSERT INTO intervenants_competences (ico_ref, ico_ref_id, ico_competence) VALUES (2, " . $_POST['fournisseur'] . ", " . $c['com_id'] . ")");
		$req->execute();
	}
}

$ref = 2;
$ref_id = $_POST['intervenant'];
$competences_act=get_competences_intervenant($ref,$ref_id,1);

if(!empty($competences_act)){
	
	foreach($competences_act as $c){
		
		// pour chaque competence on controle la presence des diplomes 
		
		$ico_controle_dt=1;
		$ico_controle_dt_txt="";
		
		$diplomes_act=get_diplomes_competence_inter($ref,$ref_id,$c["com_id"]);		

		if(!empty($diplomes_act)){
			
			foreach($diplomes_act as $d){
				
				
				
				if(empty($d["idi_date_deb"])){
					
					// le diplome n'est pas renseigné
					
					if($ico_controle_dt_txt!=""){
						$ico_controle_dt_txt.="<br>";
					}
					if($d["dco_obligatoire"]){										
						if($ico_controle_dt<3){
							$ico_controle_dt=3;
						}
						$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est pas renseigné (obligatoire).";
					}else{
						if($ico_controle_dt<2){
							$ico_controle_dt=2;
						}
						$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est pas renseigné.";	
					}
					
				}elseif($d["dip_validite"]>0 AND date('Y-m-d', strtotime("+" . $d['dip_validite'] . " months", strtotime($d['idi_date_deb']))) < date("Y-m-d")){
					
					// le diplome est périodique et il est perimé
					
					if($d["dco_obligatoire"]){										
						if($ico_controle_dt<3){
							$ico_controle_dt=3;
						}
						$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est plus valide (obligatoire).";
					}else{
						if($ico_controle_dt<2){
							$ico_controle_dt=2;
						}
						$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est plus valide.";	
					}
				}						
			}
		}
		update_competence_intervenant($ref,$ref_id,$c["com_id"],$ico_controle_dt,$ico_controle_dt_txt);
		
	}
}
////////////////////////////// FIN MISE A JOUR COMPETENCES ////////////////////////////////
//// CONTROLES INTERVENANTS /////
$controle_intervenant_dt = $ico_controle_dt;
$controle_intervenant_dt_txt = $ico_controle_dt_txt;

foreach($fournisseurs_documents_intervenants as $fdo){
	$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur = " . $_POST['fournisseur'] . " AND ffi_fichier =" . $fdo['fdo_id'] . " AND ffi_intervenant =" . $_POST['intervenant']);
	$req->execute();
	$fichier_intervenant = $req->fetch();
				if(empty($fichier_intervenant)){
					
					// le document n'est pas renseigné
					
					if($controle_intervenant_dt_txt!=""){
						$controle_intervenant_dt_txt.="<br>";
					}
					if($fdo["fdo_obligatoire"] == 1){										
						if($controle_intervenant_dt<3){
							$controle_intervenant_dt=3;
						}
						$controle_intervenant_dt_txt.="Le document " . $fdo["fdo_libelle"] . " n'est pas renseigné (obligatoire).";
					}else{
						if($controle_intervenant_dt<2){
							$controle_intervenant_dt=2;
						}
						$controle_intervenant_dt_txt.="Le document " . $fdo["fdo_libelle"] . " n'est pas renseigné.";	
					}
					
				}elseif($fdo["fdo_periodicite_mois"]>0 AND date('Y-m-d', strtotime("+" . $fdo["fdo_periodicite_mois"] . " months", strtotime($fichier_intervenant['ffi_date']))) < date("Y-m-d")){
					
					// le diplome est périodique et il est perimé
					
					if($fdo["fdo_obligatoire"]){										
						if($controle_intervenant_dt<3){
							$controle_intervenant_dt=3;
						}
						$controle_intervenant_dt_txt.="Le document " . $fdo["fdo_libelle"] . " n'est plus valide (obligatoire).";
					}else{
						if($controle_intervenant_dt<2){
							$controle_intervenant_dt=2;
						}
						$controle_intervenant_dt_txt.="Le document " . $fdo["fdo_libelle"] . " n'est plus valide.";	
					}
				}						
			}
//// FIN CONTROLES INTERVENANTS /////
$array = array(
	"ico_controle_dt" => $controle_intervenant_dt, 
	"ico_controle_dt_txt" => $controle_intervenant_dt_txt, 
);

echo json_encode($array);