<?php
include("../includes/connexion.php");
include_once('../modeles/mod_parametre.php');
include_once('../modeles/mod_tva.php');
if(isset($_POST)){

 	$req = $Conn->prepare("SELECT * FROM commandes_lignes WHERE cli_commande = " . $_POST['commande']);
	$req->execute();
	$commandes_lignes = $req->fetchAll();

	$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = " . $_POST['commande']);
	$req->execute();
	$commande = $req->fetch();
	foreach($commandes_lignes as $c){ // pour toutes les lignes calculer la tva
		$date = $commande['com_date'];
		$req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = " . $c['cli_tva'] . " AND tpe_date_deb <= '" . $date . "' AND (tpe_date_fin >= '"  . $date . "' OR tpe_date_fin IS NULL)");
		$req->execute();
		$tva = $req->fetch();

		$req = $Conn->prepare("UPDATE commandes_lignes SET cli_tva_taux = " . $tva['tpe_taux'] . " WHERE cli_commande = " . $c['cli_id']);
		$req->execute();


		$tva_nom = $base_tva[$c['cli_tva']];
		$total_ligne[$tva_nom]['nom'] = $tva['tpe_taux'];

		$valeur = (($c['cli_ht'])*$tva['tpe_taux'])/100;

		if(!empty($total_ligne[$tva_nom]['montant'])){
			$total_ligne[$tva_nom]['montant'] =  floatval(str_replace(',', '.', $total_ligne[$tva_nom]['montant'])) + $valeur;
		}else{
			$total_ligne[$tva_nom]['montant'] = $valeur;
		}
		$total_ligne[$tva_nom]['montant'] = number_format($total_ligne[$tva_nom]['montant'], 2, ',', ' ');
	}


	$req = $Conn->prepare("SELECT com_ttc FROM commandes WHERE com_id = " . $_POST['commande']);
	$req->execute();
	$total_ttc = $req->fetch();
	echo json_encode($total_ligne);

}
?>
