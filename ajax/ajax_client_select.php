<?php
 // PERMET DE PRE-SELECTIONNER DES CLIENTS D'APRES UNE SAISIE UTILISATEUR
 
session_start();
include('../includes/connexion.php');


$erreur="";

/* UTILISATION 

- etech
- produit
- client */

// parametre

$param="";
if($_GET['q']){
	$param=$_GET['q'] . "%";
}

$categorie=0;
if(isset($_GET['categorie'])){
	if($_GET['categorie']){
		$categorie=intval($_GET['categorie']);
	}
}
$sous_categorie=0;
if(isset($_GET['sous_categorie'])){
	if($_GET['sous_categorie']){
		$sous_categorie=intval($_GET['sous_categorie']);
	}
}
$groupe=0;
if(isset($_GET['groupe'])){
	if($_GET['groupe']){
		$groupe=intval($_GET['groupe']);
	}
}
$region=0;
if(isset($_GET['region'])){
	if($_GET['region']){
		$region=intval($_GET['region']);
	}
}
$lib_option=0;
if(isset($_GET['lib_option'])){
	if($_GET['lib_option']){
		$lib_option=intval($_GET['lib_option']);
	}
}
$blackliste = null;
if(isset($_GET['blackliste'])){
	$blackliste=intval($_GET['blackliste']);
}


/*
$cp_ville=0;
if(isset($_GET['cp_ville'])){
	if(!empty($_GET['cp_ville'])){
		$cp_ville=1;
	}
}*/

// DONNEE POUR TRAITEMENT

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}

// REQUETE

$sql="SELECT DISTINCT cli_id,cli_nom,cli_code,cli_adr_cp,cli_adr_ville FROM clients";
if($region==1){
	$sql.=" LEFT JOIN Clients_Categories ON (clients.cli_categorie=Clients_Categories.cca_id)
	LEFT OUTER JOIN Clients_Societes ON (clients.cli_id=Clients_Societes.cso_client AND cso_societe=" . $acc_societe;
	if($acc_agence>0){
		$sql.=" AND cso_agence=" . $acc_agence;
	}
	$sql.=")";
}
$sql.=" WHERE (cli_nom LIKE '" . $param . "' OR cli_code LIKE '" . $param . "')";
if($categorie>0){
	$sql.=" AND cli_categorie=" . $categorie;	
}
if($sous_categorie>0){
	$sql.=" AND cli_sous_categorie=" . $sous_categorie;	
}
if($groupe>0){
	if($groupe==1){
		$sql.=" AND (cli_groupe=0 OR cli_filiale_de=0)";
	}elseif($groupe==2){
		$sql.=" AND cli_groupe=1 AND cli_filiale_de=0";
	} 
}
if($region==1){
	$sql.=" AND (cca_gc OR NOT ISNULL(cso_societe))"; 
}
if(!is_null($blackliste)){
	if($blackliste==1){
		$sql.=" AND cli_blackliste";
	}elseif($blackliste==0){
		$sql.=" AND NOT cli_blackliste";
	}
}
$sql.=" ORDER BY cli_nom,cli_code,cli_adr_cp";
$req = $Conn->query($sql);
$clients = $req->fetchAll();
if(!empty($clients)){
	foreach($clients as $c){
		if($lib_option==1){
			$retour[] = array("id"=>$c['cli_id'], "nom"=>$c['cli_nom'] . " (" . $c['cli_code'] . ") - " . $c['cli_adr_cp'] . " " . $c['cli_adr_ville']);
		}else{
			$retour[] = array("id"=>$c['cli_id'], "nom"=>$c['cli_nom'] . " (" . $c['cli_code'] . ")");
		}
	}
}

// ENVOIE DU RESULTAT
if(!empty($retour)){
	echo json_encode($retour);	
}
?>
