<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../connexion_soc.php';

/* RETOURNE LES DONNEES PRODUITS

// retour => tableau JSON

// NOTE script retourne le minimum d'info
// les données détaillés d'un produit peuvent etre recupere via ajax_get_produit

// Appel :
// planning.php

// auteur : FG 06/09/2016
	MAJ 27/09/2016 utilisation de mod_get_produits_ref()
*/

$erreur=0;
$produits=array();

if(isset($_POST)){
	 
	$type=0;
	if(!empty($_POST["type"])){
		$type=$_POST["type"]; 
	}else{
		$erreur=1;
	}	

	if($erreur==0){
		
		$client=0;
		if(!empty($_POST['client'])){
			$client=$_POST['client'];
		}
		
		// par defaut on demande les produits de type formation
		$categorie="1,2";
		if(!empty($_POST["categorie"])){
			$categorie=$_POST["categorie"]; 
		}	
		
		$famille="";
		if(!empty($_POST["famille"])){
			$famille=$_POST["famille"]; 
		}	
		

		include '../modeles/mod_get_produits_ref.php';
		$results=get_produits_ref($type,$client,$categorie,$famille);
		
		if(!empty($results)){
			foreach($results as $r){
				$produits[]=array(
					"id" => $r["id"],
					"text" => $r["reference"] . " - " . $r["libelle"]
				);
			}
		}
	}
}else{
	$erreur=10;
}
if($erreur==0){
	echo json_encode($produits);
}else{
	header("HTTP/1.0 500 ");
};
?>
