<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

// RETOURNE LES SOUS-FAMILLES ASSOCIES à UNE FAMILLE

// retour => tableau JSON (compatible Select2)


$erreur=0;
$agences=array();

if(isset($_POST)){
	 
	$categorie=0;
	if(!empty($_POST["categorie"])){
		$categorie=intval($_POST["categorie"]); 
	}else{
		$erreur=1;
	}

	
	
	

	if($erreur==0){
		
		
		
		$sql="SELECT veh_id, veh_libelle FROM Vehicules WHERE veh_categorie=" . $categorie;
		$sql.=" ORDER BY veh_libelle;";
		$req=$Conn->query($sql);	
		$results=$req->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$agences[]=array(
					"id" => $r["veh_id"],
					"text" => $r["veh_libelle"]
				);
			}
		}
	}
}else{
	$erreur=10;
}
if($erreur==0){
	echo json_encode($agences);
}else{
	//header("HTTP/1.0 500 " . $erreur);
};
?>
