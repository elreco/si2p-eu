<?php
include "../includes/controle_acces.inc.php";
include '../connexion_soc.php';

 // CONFIRME OU ANNUL CONFIRMATION D'UNE ACTION
 

$erreur=0;
if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]); 
	}	 
	
	$action_client_id=0;
	if(!empty($_POST["action_client"])){
		$action_client_id=intval($_POST["action_client"]); 
	}

	$checked=0;
	if(!empty($_POST["checked"])){
		if($_POST["checked"]=="true"){
			$checked=1; 
		}
	}
	
	if($action_id!=0 AND $action_client_id!=0){
	
		if($checked==0){
			$acl_confirme_date=null;
		}else{
			$date = new DateTime();
			$acl_confirme_date=$date->format('Y-m-d');
		}
		
		$sql="UPDATE Actions_Clients SET acl_confirme=:checked, acl_confirme_date=:confirme_date WHERE acl_id=:action_client;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":checked",$checked);
		$req->bindParam(":confirme_date",$acl_confirme_date);
		$req->bindParam(":action_client",$action_client_id);
		$req->execute();
		$err_code=$ConnSoc->errorCode();
		if($err_code!=="00000"){
			$erreur=12;
		}
	}else{
		$erreur=11;
	}
}else{
	$erreur=10;
}
		
if($erreur!=0){
	header("HTTP/1.0 500 ");
}
 
 
?>