<?php

// TEST SI IL EXISTE UN RISQUE DE BOUBLON

include("../includes/connexion.php");
include("../includes/connexion_soc.php");
include("../modeles/mod_parametre.php");



if(isset($_POST)){

	$suspect="";
	if(!empty($_POST['suspect'])){
		$suspect=intval($_POST['suspect']);
	}

	$nom_particulier="";
	if(!empty($_POST['nom_particulier'])){
		$nom_particulier=$_POST['nom_particulier'];
	}
	$prenom_particulier="";
	if(!empty($_POST['prenom_particulier'])){
		$prenom_particulier=$_POST['prenom_particulier'];
	}

	$naissance="";
	if(!empty($_POST['naissance'])){
		$naissance=convert_date_sql($_POST['naissance']);
	}

	if(empty($nom_particulier) OR empty($prenom_particulier) OR empty($naissance)){
		// il n'y a rien a tester
		echo("Vérification impossible");
		die();

	}else{

		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);
		}

        // tableau de retour
		$result=array(
			"resultat" =>0,
			"resultat_class" => "success",
			"resultat_text" => array()
		);
        // SOCIETE

		$d_societes=array();
		$sql="SELECT soc_id,soc_nom FROM Societes ORDER BY soc_id;";
		$req=$Conn->query($sql);
		$resultat=$req->fetchAll();
		if(!empty($resultat)){
			foreach($resultat as $r){
				$d_societes[$r["soc_id"]]=$r["soc_nom"];
			}
		}

		// AGENCE
		$d_agences=array();
		$sql="SELECT age_id,age_nom FROM Agences ORDER BY age_id;";
		$req=$Conn->query($sql);
		$resultat=$req->fetchAll();
		if(!empty($resultat)){
			foreach($resultat as $r){
				$d_agences[$r["age_id"]]=$r["age_nom"];
			}
		}

        // VOIR SI il EXISTE DES STAGIAIRES
        $sql="SELECT DISTINCT * FROM Stagiaires
        LEFT JOIN Stagiaires_Clients ON (Stagiaires_Clients.scl_stagiaire = Stagiaires.sta_id)
        LEFT JOIN Clients ON (Clients.cli_id = Stagiaires_Clients.scl_client)
        WHERE sta_nom=:nom AND sta_prenom=:prenom AND sta_naissance=:naissance AND cli_categorie = 3";
        $sql.=" ORDER BY sta_id";
        $req=$Conn->prepare($sql);
        $req->bindParam(":nom",$nom_particulier);
        $req->bindParam(":prenom",$prenom_particulier);
        $req->bindParam(":naissance",$naissance);
        $req->execute();
        $doublons_stagiaires=$req->fetchAll();

        $text = "";
        if(!empty($doublons_stagiaires)){
            foreach($doublons_stagiaires as $d){
                $sql="SELECT * FROM Clients_Societes WHERE cso_client = " . $d['scl_client'];
                $req=$Conn->prepare($sql);
                $req->execute();
                $clients_societes=$req->fetchAll();
                foreach($clients_societes as $cso){
                    if(!empty($cso['cso_agence'])){
                        $text .= "<p>Le stagiaire existe déjà dans la base " . $d_societes[$cso['cso_societe']] . " " . $d_agences[$cso['cso_agence']] . " au nom du client : " . $d["cli_nom"] . "</p>";
                    }else{
                        $text .= "<p>Le stagiaire existe déjà dans la base " . $d_societes[$cso['cso_societe']] . " au nom de : " . $d["cli_nom"] . "</p>";
                    }

                }

            }
        }
        if(!empty($text)){
			$result["resultat"] = 2;
			$result["resultat_class"]="danger";
			$result["resultat_text"][]=$text;

		}
		echo(json_encode($result));

	}
}else{
	echo("Vérification impossible");
	die();
} ?>
