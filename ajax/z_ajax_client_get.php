<?php
 // PERMET DE RECUPERER LES INFOS D'UN CLIENT RECU EN PARAMETRE
 
	include('../connexion_soc.php');

	$erreur=0;
	
	$client_id=0;
	if(!empty($_GET['client'])){
		$client_id=$_GET['client'];
	}else{
		$erreur=1;
	}
	
	if($erreur==0){
		
		// DONNEE DU CLIENT          
		$req = $Conn->prepare("SELECT cli_commercial,cli_categorie,cli_id FROM clients WHERE cli_id=:client");
		$req->bindParam(":client",$client_id);
		$req->execute();
		$client = $req->fetch();
		
		// ADRESSE D'INTERVENTION
		$reqAd = $Conn->prepare("SELECT * FROM Adresses WHERE adr_ref=1 AND adr_ref_id=:client ORDER BY adr_defaut DESC");
		$reqAd->bindParam(":client",$client_id);
		$reqAd->execute();
		$results = $reqAd->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$adresses[]=array(
					"id" => $r["adr_id"],
					"text" => $r["adr_nom"],
					"service" => $r["adr_service"],
					"ad1" => $r["adr_ad1"],
					"ad2" => $r["adr_ad2"],
					"ad3" => $r["adr_ad3"],
					"cp" => $r["adr_cp"],
					"ville" => $r["adr_ville"]
				);				
			}
		}
		
		// LES CONTACTS
		$reqAd = $Conn->prepare("SELECT * FROM Contacts WHERE con_ref=1 AND con_ref_id=:client ORDER BY con_defaut DESC");
		$reqAd->bindParam(":client",$client_id);
		$reqAd->execute();
		$results = $reqAd->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$contacts[]=array(
					"id" => $r["con_id"],
					"text" => $r["con_nom"],
					"prenom" => $r["con_prenom"],
					"tel" => $r["con_tel"],
					"portable" => $r["con_portable"]
				);				
			}
		}
		
		if(!empty($client)){
			$retour=array(
				"cli_commercial" => $client["cli_commercial"],
				"cli_categorie" => $client["cli_categorie"]
			);
		}
		if(!empty($adresses)){
			$retour["adresses"]=$adresses;
		}
		if(!empty($contacts)){
			$retour["contacts"]=$contacts;
		}
	}
	/*echo("<pre>");
		print_r($retour);
	echo("</pre>");*/
	
	if($erreur==0){
		echo json_encode($retour);
	}else{
		echo("erreur : " . $erreur);
	}
?>
