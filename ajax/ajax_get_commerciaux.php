<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion_fct.php';
include '../modeles/mod_get_commerciaux.php';

 // RETOURNE LES COMMERCIAUX DISPONIBLE
 
// parametre POST

// ref : INT 1 contact clients
	// ref_id : ID clients 

// retour => tableau JSON

// Appel :
// planning.php

// auteur : FG 06/09/2016
 
 
$erreur=0;
if(isset($_POST)){
	 
	$societe=0;
	if(!empty($_POST["societe"])){
		$societe=$_POST["societe"]; 
	}elseif(!empty($_SESSION['acces']["acc_societe"])){
		$societe=$_SESSION['acces']["acc_societe"];
	}else{
		$erreur=1;
	}	
	
	if($erreur==0){
		
		$agence=0;
		if(!empty($_POST["agence"])){
			$agence=intval($_POST["agence"]); 
		}
		
		$commerciaux=array();

		$results=get_commerciaux($societe,$agence,$_POST["archive"]);
		if(!empty($results)){
			foreach($results as $r){
				$commerciaux[]=array(
					"id" => $r["com_id"],
					"text" => $r["com_label_1"] . " " . $r["com_label_2"]				
				);				
			}
		}
		echo json_encode($commerciaux);
	}else{
		echo("Erreur paramètre!");
	}
}
?>