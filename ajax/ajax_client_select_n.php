<?php
 // PERMET DE PRE-SELECTIONNER DES CLIENTS D'APRES UNE SAISIE UTILISATEUR
 // SOURCE : client sur ORION_$acc_societe
 
session_start();
include('../includes/connexion_soc.php');

$erreur="";
$param="";
if($_GET['q']){
	$param=$_GET['q'] . "%";
}

$categorie=0;
if(isset($_GET['categorie'])){
	if(!empty($_GET['categorie'])){
		$categorie=intval($_GET['categorie']);
	}
}
$sous_categorie=0;
if(isset($_GET['sous_categorie'])){
	if(!empty($_GET['sous_categorie'])){
		$sous_categorie=intval($_GET['sous_categorie']);
	}
}
$agence=0;
if(isset($_GET['agence'])){
	if(!empty($_GET['agence'])){
		$agence=intval($_GET['agence']);
	}
}

if(isset($_GET['archive'])){
	$archive="";
	if($_GET['archive']==1){
		$archive=1;
	}elseif($_GET['archive']==0){
		$archive=0;
	}
}else{
	$archive = 0;
}
if(isset($_GET['blackliste'])){
	$blackliste="";
	if($_GET['blackliste']==1){
		$blackliste=1;
	}elseif($_GET['archive']==0){
		$blackliste=0;
	}
}else{
	$blackliste = 0;
}

if(isset($_GET['first_fac'])){
	$first_fac="";
	if($_GET['first_fac']==1){
		$first_fac=1;
	}
}else{
	$first_fac="";
}

/* $acces 
	1 -> exclut les clients CN
*/

if(isset($_GET['acces'])){
	$acces=intval($_GET['acces']);
}

// DONNEE POUR TRAITEMENT

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];	
}
$acc_utilisateur=0;
if($_SESSION['acces']['acc_ref']==1){
	if(isset($_SESSION['acces']["acc_ref_id"])){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];	
	}
}

$sql="SELECT DISTINCT cli_id,cli_nom,cli_code FROM clients WHERE (cli_nom LIKE '" . $param . "' OR cli_code LIKE '" . $param . "')";
// critère auto selon connection
if(!$_SESSION['acces']["acc_droits"][6]){
	$sql.=" AND cli_utilisateur=" . $acc_utilisateur;	
}
if($acc_agence>0){
	$sql.=" AND cli_agence=" . $acc_agence;	
}elseif($agence>0){
	$sql.=" AND cli_agence=" . $agence;	
}

// parametre 
if($categorie>0){
	$sql.=" AND cli_categorie=" . $categorie;
}
if($sous_categorie>0){
	$sql.=" AND cli_sous_categorie=" . $sous_categorie;
}
if(is_int($archive)){
	if($archive===0){
		$sql.=" AND NOT cli_archive";
	}elseif($archive===1){
		$sql.=" AND cli_archive";
	}
}
if(is_int($blackliste)){
	if($blackliste===0){
		$sql.=" AND NOT cli_blackliste";
	}elseif($blackliste===1){
		$sql.=" AND cli_blackliste";
	}
}
if(is_int($first_fac)){
	if($first_fac===1){
		$sql.=" AND cli_facture=1";
	}
}

if(isset($acces)){
	if($acces===1){
		if(!$_SESSION["acces"]["acc_droits"][8]){
			$sql.=" AND NOT cli_sous_categorie=1";	
		}
	}
}

$sql.=" ORDER BY cli_nom";
$req = $ConnSoc->query($sql);

$clients = $req->fetchAll();
if(!empty($clients)){
	foreach($clients as $c){
		$retour[] = array("id"=>$c['cli_id'], "nom"=>$c['cli_nom'] . " (" . $c['cli_code'] . ")");
	}
}

// ENVOIE DU RESULTAT
if(!empty($retour)){
	echo json_encode($retour);	
}
?>
