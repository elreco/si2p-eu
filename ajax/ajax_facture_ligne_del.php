<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';


include '../modeles/mod_get_client_produits.php';

 // SuppRESSION D'UNE LIGNE DE FACTURE

$erreur_txt="";
if(isset($_POST)){
	 
	$facture=0;
	if(!empty($_POST["facture"])){
		$facture=intval($_POST["facture"]); 
	}
	$ligne=0;
	if(!empty($_POST["ligne"])){
		$ligne=intval($_POST["ligne"]); 
	}
	if(empty($facture) OR empty($ligne) ){
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();	
	}
}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}

// SUPPRESSION DE LA LIGNE
$sql="DELETE FROM Factures_Lignes WHERE fli_facture=:fli_facture AND fli_id=:fli_id;";
$req=$ConnSoc->prepare($sql);
$req->bindParam("fli_facture",$facture);
$req->bindParam("fli_id",$ligne);
try{
	$req->execute();
}Catch(Exception $e){
	$erreur_txt="DELETE " . $e->getMessage();
	echo($erreur_txt);
	die();
}

// MAJ DE LA FACTURE

$fac_total_ht=0;
$tva=array();
$fac_total_ttc=0;

$sql="SELECT SUM(fli_montant_ht) as fli_montant_ht,fli_tva_taux FROM Factures_Lignes WHERE fli_facture=" . $facture . " GROUP BY fli_tva_taux;";
$req=$ConnSoc->query($sql);
$d_lignes=$req->fetchAll();
if(!empty($d_lignes)){
	foreach($d_lignes as $l){
		
		$fac_ht=floatval($l["fli_montant_ht"]);
		$fac_tva_taux=floatval($l["fli_tva_taux"]);
		
		$fac_tva=$fac_ht*($fac_tva_taux/100);
		$fac_tva=round($fac_tva,2);
		
		$fac_ttc=round(($fac_ht+$fac_tva),2);
		
		if(!isset($tva[$fac_tva_taux])){
			$tva[$fac_tva_taux]=0;
		}
		$tva[$fac_tva_taux]=$tva[$fac_tva_taux] + $fac_tva;
		
		
		$fac_total_ht=$fac_total_ht + $fac_ht;
		$fac_total_ttc=$fac_total_ttc + $fac_ttc;
	}
}

// UPDATE DE LA FACTURE
$warning_text="";
$sql="UPDATE Factures SET 
fac_total_ht=:fac_total_ht, 
fac_total_ttc=:fac_total_ttc
WHERE fac_id=:fli_facture;";
$req=$ConnSoc->prepare($sql);
$req->bindParam(":fac_total_ht",$fac_total_ht);
$req->bindParam(":fac_total_ttc",$fac_total_ttc);
$req->bindParam(":fli_facture",$facture);
try{
	$req->execute();
}Catch(Exception $e){
	$warning_text="UPDATE Facture " . $e->getMessage();
}

$retour=array(
	"ligne" => $ligne,
	"total_ht" => $fac_total_ht,
	"tva" => $tva,
	"total_ttc" => $fac_total_ttc,
	"warning_text" => $warning_text
);
echo(json_encode($retour));
die();
?>