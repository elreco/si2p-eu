<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

// RETOURNE LES SOUS-FAMILLES ASSOCIES à UNE FAMILLE

// retour => tableau JSON (compatible Select2)


$erreur=0;
$sous_familles=array();

if(isset($_POST)){
	 
	$famille=0;
	if(!empty($_POST["sous_famille"])){
		$famille=intval($_POST["sous_famille"]); 
	}	

	if($famille>0){
		
		$sql="SELECT * FROM Produits_sous_Sous_Familles WHERE pss_psf_id=" . $famille . ";";
		$req=$Conn->query($sql);	
		$results=$req->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$sous_familles[]=array(
					"id" => $r["pss_id"],
					"text" => $r["pss_libelle"]
				);
			}
		}
	}
}else{
	echo("Erreur paramètre!");
}
if($erreur==0){
	echo json_encode($sous_familles);
};
?>
