<?php
include "../includes/controle_acces.inc.php";

include '../includes/connexion.php';
include '../includes/connexion_soc.php';

include '../modeles/mod_get_action_adresses.php';
include '../modeles/mod_get_contacts.php';



 // RETOURNE LES INFOS D'UNE ACTION ENREGISTRE DANS LA TABLE ACTIONS
 
 // utiliser uniquement sur le planning
 
 // RETOUR FORMAT JSON
 
$erreur=0;

if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=$_POST["action"]; 
	}else{
		$erreur=1;
	}	 
	
	if($erreur==0){
		
		// SUR L'ACTION DE REF
		
		$sql="SELECT act_id,act_pro_famille,act_pro_categorie,act_pro_sous_famille,act_pro_sous_sous_famille";
		$sql.=",act_adr_ref,act_adr_ref_id,act_adresse,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville";
		$sql.=",act_commentaire,act_planning_txt,act_agence";
		$sql.=",act_contact,act_con_ref,act_con_ref_id,act_con_nom,act_con_prenom,act_con_tel,act_con_portable";
		$sql.=" FROM Actions WHERE act_id=:action_id";
		$sql.=";";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$action=$req->fetch(PDO::FETCH_ASSOC);
		
		// ON RECUPERE LES LIEU D'INTERVENTION DISPONIBLE
		
		/*
		$adresses=get_action_adresses($action_id);
		$action["adresses"]=$adresses;
		
		$contacts=array();
		
		// LES CONTACTS SI INTERVENTION CHEZ LE CLIENT
		
		if($action["act_adr_ref"]==1 AND $action["act_adr_ref_id"]>0){
			
			$societe=$_SESSION['acces']["acc_societe"];
			if(!empty($c["act_adr_societe"])){
				$societe=$c["act_adr_societe"];
			}
			
			$ConnSoc=connexion_fct($societe);
			$results=get_contacts(1,$action["act_adr_ref_id"]);
			if(!empty($results)){
				foreach($results as $r){
					$contacts[]=array(
						"id" => $r["con_id"],
						"text" => $r["con_nom"],
						"nom" => $r["con_nom"],
						"prenom" => $r["con_prenom"],
						"tel" => $r["con_tel"],
						"portable" => $r["con_portable"],
						"defaut" => $r["con_defaut"]
					);				
				}
			}
			$action["contacts"]=$contacts;
		}*/
		
	}
	
}else{
	$erreur=10;
}

if($erreur==0){
	echo json_encode($action);
}else{
	header("HTTP/1.0 500 ");
};
 
 
?>