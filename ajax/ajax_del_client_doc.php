<?php
include("../includes/connexion.php");


// PERMET DE SUPPRIMER UNE CORRESPONDANCE

$erreur="";
if(isset($_POST)){

	$document=0;
	if(!empty($_POST["document"])){
		$document=intval($_POST["document"]);
	}

	
	if($document>0){
		
		// modele pas nécéssaire
		
		$sql="SELECT cdo_client,cdo_ext FROM Clients_Documents WHERE cdo_id=" . $document . ";";
		try{
			$req=$Conn->query($sql);
			$d_doc=$req->fetch();
			if(empty($d_doc)){
				$erreur="1-ERREUR TRAITEMENT";
			}
		}catch (Exception $e){
			$erreur=$e->getMessage();
		}
		
		if(empty($erreur)){
			$sql="DELETE FROM Clients_Documents WHERE cdo_id=" . $document . ";";
			try{
				$req=$Conn->query($sql);
			}catch (Exception $e){
				$erreur=$e->getMessage();
			}
		}
		
		if(empty($erreur)){
			// suppression du fichier
			if(file_exists("../documents/clients/client" . $d_doc["cdo_client"] . "/doc" . $document . "." . $d_doc["cdo_ext"])){
				unlink("../documents/clients/client" . $d_doc["cdo_client"] . "/doc" . $document . "." . $d_doc["cdo_ext"]);
			}
			
		}
		
	}else{
		$erreur="1-ERREUR PARAMETRE";
	}
}else{
	$erreur="2-ERREUR PARAMETRE";
}
if($erreur==""){
	
	// index result_supp
	// "document" : id document supp
	$result_supp=array(
		"document" => $document
	);
	echo json_encode($result_supp);
	
}else{
	echo $erreur;
}
?>