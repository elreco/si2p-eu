<?php
include "../includes/controle_acces.inc.php";

include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // RETOURNE LES STAGIAIRES ASSOCIES A UNE LIGNE DE DEVIS
 
$erreur="";
if(isset($_POST)){
	if(!empty($_POST)){
		$devis_ligne=0;
		if(!empty($_POST["devis_ligne"])){
			$devis_ligne=intval($_POST["devis_ligne"]); 
		}else{
			$erreur="Paramètres absent";
		}
		
		if(empty($erreur)){
			
			$d_stagiaires=array();
			
			$sql="SELECT dst_stagiaire FROM Devis_Stagiaires WHERE dst_devis_ligne=" . $devis_ligne . ";";
			$req=$ConnSoc->query($sql);
			$d_sta_id=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($d_sta_id)){
				$liste="0";
				foreach($d_sta_id as $s_id){					
					$liste.="," . $s_id["dst_stagiaire"];
				}

				$sql_sta="SELECT sta_id,sta_nom,sta_prenom,DATE_FORMAT(sta_naissance,'%d/%m/%Y') AS sta_naissance FROM Stagiaires WHERE sta_id IN (" . $liste . ") ORDER BY sta_nom,sta_prenom;";
				$req_sta=$Conn->query($sql_sta);
				$d_stagiaires=$req_sta->fetchAll(PDO::FETCH_ASSOC);
			}
			echo json_encode($d_stagiaires);
		}
	}else{
		$erreur="Paramètres absent";
	}
}else{
	$erreur="Paramètres absent";
}

if(!empty($erreur)){
	echo($erreur);
}
?>