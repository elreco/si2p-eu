<?php

// TEST SI IL EXISTE UN RISQUE DE BOUBLON

include("../includes/connexion.php");
include("../includes/connexion_soc.php");

// CONTROLE DANS LA BSE CLIENT 0

include("../modeles/mod_check_client.php");
include("../modeles/mod_check_siret.php");

// CONTROLE DANS LA BASE SUSPECT N
include("../modeles/mod_check_suspect.php");
include("../modeles/mod_check_suspect_siret.php");


if(isset($_POST)){

	$suspect="";
	if(!empty($_POST['suspect'])){
		$suspect=intval($_POST['suspect']);
	}

	$code="";
	if(!empty($_POST['code'])){
		$code=$_POST['code'];
	}
	$nom="";
	if(!empty($_POST['nom'])){
		$nom=$_POST['nom'];
	}

	$siren="";
	if(!empty($_POST['siren'])){
		$siren=$_POST['siren'];
	}

	$siret="";
	if(!empty($_POST['siret'])){
		$siret=$_POST['siret'];
	}

	if(empty($code) AND empty($nom) AND empty($siren)){
		// il n'y a rien a tester
		echo("Vérification impossible");
		die();

	}else{

		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);
		}

		// on commence par vérifier si les données existe dans la base 0
		$verif_client=check_client(0,$code,$nom,$siren);

		// on regarde si les donnée existe déjà dans la base suspect
		$verif_suspect=check_suspect($suspect,$code,$nom,$siren);


		if(!empty($siret)){
			// on regarde si le siret est libre dans la base client
			$verif_cli_siret=check_siret($siren,$siret,0);

			// on regarde si le siret est libre dans la base client
			$verif_sus_siret=check_suspect_siret($siren,$siret,0);
		}

		// ON CONSTRUIT LE RESULTAT A TRANSMETTRE A L'UTILISATEUR


		// tableau de retour
		$result=array(
			"resultat" =>0,
			"resultat_class" => "success",
			"resultat_text" => array()
		);


		// SOCIETE

		$d_societes=array();
		$sql="SELECT soc_id,soc_nom FROM Societes ORDER BY soc_id;";
		$req=$Conn->query($sql);
		$resultat=$req->fetchAll();
		if(!empty($resultat)){
			foreach($resultat as $r){
				$d_societes[$r["soc_id"]]=$r["soc_nom"];
			}
		}

		// AGENCE
		$d_agences=array();
		$sql="SELECT age_id,age_nom FROM Agences ORDER BY age_id;";
		$req=$Conn->query($sql);
		$resultat=$req->fetchAll();
		if(!empty($resultat)){
			foreach($resultat as $r){
				$d_agences[$r["age_id"]]=$r["age_nom"];
			}
		}


		// ALERT DOUBLON CODE

		$tab_ref=array();

		$text_code="";
		if(!empty($verif_client)){
			if($verif_client["doublon_code"]){
				$text_code="<p>Le code " . $code . " est utilisé dans la base client des sociétés suivantes :<p>";
				$text_code.="<ul>";
					foreach($verif_client["doublon_code_soc"] as $s){
						if(!isset($tab_ref[$s["societe"]][$s["agence"]])){
							if(!empty($s["societe"])){
								$text_code.="<li>" . $d_societes[$s["societe"]];
							}else{
								$text_code.="<li>Sans affectation 'réseau'";
							}
							if(!empty($s["agence"])){
								$text_code.=" " . $d_agences[$s["agence"]];
							}
							if(!empty($s["archive"])){
								$text_code.=" (archivé)";
							}
							$text_code.="</li>";

							$tab_ref[$s["societe"]][$s["agence"]]=true;
						}
					}
				$text_code.="</ul>";
			}
		}
		if(!empty($verif_suspect["doublon_code"])){
			$text_code.="<p>Le code " . $code . " est utilisé dans la base suspect " . $d_societes[$acc_societe]  . "</p>";
		}
		if(!empty($text_code)){
			$result["resultat"] = 2;
			$result["resultat_class"]="danger";
			$result["resultat_text"][]=$text_code;

		}

		// ALERT DOUBLON NOM

		$tab_ref=array();

		$text_nom="";
		if(!empty($verif_client)){
			if($verif_client["doublon_nom"]){
				$text_nom="<p>Le nom " . $nom . " est utilisé dans la base client des sociétés suivantes :<p>";
				$text_nom.="<ul>";
					foreach($verif_client["doublon_nom_soc"] as $s){
						if(!isset($tab_ref[$s["societe"]][$s["agence"]])){
							$text_nom.="<li>" . $d_societes[$s["societe"]];
							if(!empty($s["agence"])){
								$text_nom.=" " . $d_agences[$s["agence"]];
							}
							if(!empty($s["archive"])){
								$text_nom.=" (archivé)";
							}
							$text_nom.="</li>";

							$tab_ref[$s["societe"]][$s["agence"]]=true;
						}
					}
				$text_nom.="</ul>";
			}
		}
		if(!empty($verif_suspect["doublon_nom"])){
			$text_nom.="<p>Le nom " . $nom . " est utilisé dans la base suspect " . $d_societes[$acc_societe]  . "</p>";
		}

		if(!empty($text_nom)){
			if($result["resultat"]<2){
				$result["resultat"] = 1;
				$result["resultat_class"]="warning";
			}
			$result["resultat_text"][]=$text_nom;

		}

		// ALERT DOUBLON SIREN

		$tab_ref=array();

		$text_siren="";

		// CLIENT
		if(!empty($verif_client)){
			if($verif_client["doublon_siren"]){
				if($verif_client["doublon_siren_blacklist"]){
					$text_siren="<p><b>Le siren " . $siren . " est blacklisté. Vous ne pouvez pas créer cette fiche</b><p>";
					$result["resultat"] = 3;
					$result["resultat_class"]="dark";
				}else{
					$text_siren="<p>Le siren " . $siren . " est utilisé dans la base client des sociétés suivantes :<p>";
					$text_siren.="<ul>";
						foreach($verif_client["doublon_siren_soc"] as $s){
							if(!isset($tab_ref[$s["societe"]][$s["agence"]])){
								$text_siren.="<li>" . $d_societes[$s["societe"]];
								if(!empty($s["agence"])){
									$text_siren.=" " . $d_agences[$s["agence"]];
								}
								if(!empty($s["archive"])){
									$text_siren.=" (archivé)";
								}
								$text_siren.="</li>";

								$tab_ref[$s["societe"]][$s["agence"]]=true;
							}
						}
					$text_siren.="</ul>";
				}
			}
		}

		// SUSPECT
		if($result["resultat"]!=3){

			if(!empty($verif_suspect["doublon_siren"])){
				$text_siren.="<p>Le siren " . $siren . " est utilisé dans la base suspect " . $d_societes[$acc_societe]  . "</p>";
			}
		}

		if(!empty($text_siren)){
			if($result["resultat"]<2){
				$result["resultat"] = 1;
				$result["resultat_class"]="warning";
			}
			$result["resultat_text"][]=$text_siren;
		}



		// ALERT DOUBLOIN SIRET
		if($result["resultat"]!=3){
			if(!empty($siret)){

				$text_siret="";

				if(!empty($verif_cli_siret)){
					$text_siret="<p>Le siret " . $siren . " " . $siret . " est enregistré sur les fiches clientes suivantes :</p>";
					$text_siret.="<ul>";
						foreach($verif_cli_siret as $vcs){
							$text_siret.="<li>" . $vcs["code"] . " " . $vcs["nom"] . "</li>";
						}
					$text_siret.="</ul>";

				}

				if(!empty($verif_sus_siret)){
					if($verif_sus_siret["doublon"]){
						$text_siret.="<p>Le siret " . $siren . " " . $siret . " est utilisé dans la base suspect " . $d_societes[$acc_societe]  . "</p>";
					}
				}

				if(!empty($text_siret)){
					if($result["resultat"]<3){
						$result["resultat"] = 2;
						$result["resultat_class"]="danger";
					}
					$result["resultat_text"][]=$text_siret;
				}
			}
		}



		echo(json_encode($result));

	}
}else{
	echo("Vérification impossible");
	die();
} ?>
