<?php

include "../includes/controle_acces.inc.php";

include '../includes/connexion_soc.php';
include '../includes/connexion.php';

include "../modeles/mod_add_stagiaire.php";
include "../modeles/mod_action_stagiaire_add.php";



// MAJ D'UN INSCRIPTION DE STAGIAIRE A UNE FORMATION

$erreur_txt="";

if(!empty($_POST)){

	$action_id=0;
	if (isset($_POST['action'])){
		$action_id=intval($_POST['action']);
	}

	$stagiaire_id=0;
	if(isset($_POST['stagiaire'])){
		$stagiaire_id=intval($_POST['stagiaire']);
	}

	$action_client_id=0;
	if(isset($_POST['action_client'])){
		$action_client_id=intval($_POST['action_client']);
	}

	if(empty($action_id) OR empty($stagiaire_id) OR empty($action_client_id)){
		$erreur_txt="Formulaire incomplet!";
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}
/***********************************
			CONTROLE
***********************************/

if(empty($erreur_txt)){

	$session_id=0;
	if(isset($_POST['session'])){
		$session_id=intval($_POST['session']);
	}

	// INFO SUR L'ACTION
	$req=$ConnSoc->prepare("SELECT act_gest_sta,acl_client,acl_pro_inter,acl_ca,acl_ca_unit FROM Actions,Actions_Clients WHERE act_id=acl_action AND act_id=:action_id AND acl_id=:action_client_id;");
	$req->bindParam(":action_id",$action_id);
	$req->bindParam(":action_client_id",$action_client_id);
	$req->execute();
	$d_action=$req->fetch();
	if(!empty($d_action)){
		if($d_action["act_gest_sta"]==2 AND $session_id==0){
			$erreur_txt="Impossible d'identifier la session pour l'inscription";
		}
	}else{
		$erreur_txt="Impossible de charger les données liées à l'action!";
	}
}
if(empty($erreur_txt)){

	// INSCRIPTION AVANT MODIF

	$req=$ConnSoc->prepare("SELECT ast_action_client,ast_attestation,ast_attest_ok,ast_confirme,acl_client,acl_pro_inter,acl_id,acl_ca,acl_ca_unit
	FROM Actions_Stagiaires,Actions_Clients WHERE ast_action_client=acl_id AND ast_action=:action_id AND ast_stagiaire=:stagiaire_id;");
	$req->bindParam(":action_id",$action_id);
	$req->bindParam(":stagiaire_id",$stagiaire_id);
	$req->execute();
	$d_action_stagiaire=$req->fetch();
	if(empty($d_action_stagiaire)){
		$erreur_txt="Impossible de charger les données liées à l'inscription!";
	}
}
// FIN CONTROLE

/***********************************
	DONNEE POUR TRAITREMENT ET ENREGISTREMENT
***********************************/
if(empty($erreur_txt)){

	// ELEMENT DU FORM

	$ast_attestation=0;
	if (isset($_POST['ast_attestation'])){
		$ast_attestation=intval($_POST['ast_attestation']);
	}

	$ast_confirme=0;
	if(isset($_POST['ast_confirme'])){
		if(!empty($_POST['ast_confirme'])){
			$ast_confirme=1;
		}
	}
	// PERSONNE CONNECTE
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}

	// ID DE LA SESSION GESTION A LA SESSION
	if($d_action["act_gest_sta"]==2){

		$req=$ConnSoc->prepare("SELECT ass_session FROM Actions_Stagiaires_Sessions WHERE ass_action=:action_id AND ass_stagiaire=:stagiaire_id;");
		$req->bindParam(":action_id",$action_id);
		$req->bindParam(":stagiaire_id",$stagiaire_id);
		$req->execute();
		$d_stagiaire_session=$req->fetchAll();

	}

	// tableau de retour
	$retour=array(
		"action_client" => $action_client_id,
		"session" => $session_id,
		"stagiaire" => $stagiaire_id,
		"nom" => "",
		"prenom" => "",
		"confirme" => $ast_confirme,
		"attestation" => $ast_attestation,
		"attest_modif" =>0,
		"suppression" => array(
			"action_client" => 0,
			"session" => 0
		)
	);

	// changement d'attestation

	$ast_attest_ok=$d_action_stagiaire["ast_attest_ok"];

	if($d_action_stagiaire["ast_attestation"]!=$ast_attestation){

		$sql="DELETE FROM Stagiaires_Attestations_Competences WHERE sac_action=" . $action_id . " AND sac_stagiaire=" . $stagiaire_id . ";";
		$ConnSoc->query($sql);

		// on supp les fichiers PDF
		if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Attestations/" . $acc_societe . "/attestation_". $action_id . "_" . $stagiaire_id . ".pdf")){
			unlink($_SERVER["DOCUMENT_ROOT"] . "/Documents/Attestations/" . $acc_societe . "/attestation_". $action_id . "_" . $stagiaire_id . ".pdf");
		}

		$retour["attest_modif"]=1;

		if(!empty($ast_attestation)){
			$ast_attest_ok=1;
		}
	}


	// ENREGISTREMENT
	$req=$ConnSoc->prepare("UPDATE Actions_Stagiaires SET
	ast_action_client=:action_client_id,
	ast_confirme=:ast_confirme,
	ast_attestation=:ast_attestation,
	ast_attest_ok=:ast_attest_ok
	WHERE ast_action=:action_id AND ast_stagiaire=:stagiaire_id;");
	$req->bindParam(":action_id",$action_id);
	$req->bindParam(":stagiaire_id",$stagiaire_id);
	$req->bindParam(":ast_attestation",$ast_attestation);
	$req->bindParam(":ast_confirme",$ast_confirme);
	$req->bindParam(":action_client_id",$action_client_id);
	$req->bindParam(":ast_attest_ok",$ast_attest_ok);
	try{
		$req->execute();
	}Catch(Exception $e){
		$erreur_txt=$e->getMessage();
	}

	// TRAITEMENT ANNEXE
	if(empty($erreur_txt)){

		// GESTION DES DATES ET SESSION

		// si gestion a la session et que l'on change action_client -> inutle de supp l'inscription à la session
		if( ($d_action["act_gest_sta"]==1) OR ($d_action["act_gest_sta"]==2 AND $session_id!=$d_stagiaire_session[0]["ass_session"]) ){

			// on supp l'inscription au session
			// note : meme modal add et set -> la construction du form fait que l'on ne recupère que les session coché
			// -> c'est plus simple de del toutes les inscriptions aux sessions et de ré-enregistrer les nouvelles.
			$sql="SELECT * FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=". $stagiaire_id ." AND ass_action=". $action_id .";";
			$req=$ConnSoc->query($sql);
			$d_ass=$req->fetch();

			$sql="DELETE FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=". $stagiaire_id ." AND ass_action=". $action_id .";";
			$ConnSoc->query($sql);

			// on memorise les nouvelles sessions
			$sql_ses="INSERT INTO Actions_Stagiaires_Sessions (ass_stagiaire,ass_action,ass_session,ass_signature) VALUES (:stagiaire_id,:action_id,:session_id,:ass_signature);";
			$req_ses=$ConnSoc->prepare($sql_ses);
			$req_ses->bindParam(":stagiaire_id",$stagiaire_id);
			$req_ses->bindParam(":action_id",$action_id);
			$req_ses->bindParam(":ass_signature",$d_ass['ass_signature']);
			if($d_action["act_gest_sta"]==1){

				if(isset($_POST["sessions"])){
					if(!empty($_POST["sessions"])){
						foreach($_POST["sessions"] as $sc){
							$req_ses->bindParam(":session_id",$sc["ase_id"]);
							$req_ses->execute();
						}
					}
				}

				if($action_client_id!=$d_action_stagiaire["ast_action_client"]){
					$retour["suppression"]["action_client"]=$d_action_stagiaire["ast_action_client"];
				}

			}else{

				$req_ses->bindParam(":session_id",$session_id);
				$req_ses->execute();

				$retour["suppression"]["session"]=$d_stagiaire_session[0]["ass_session"];
			}
		}

		if(!empty($retour["suppression"]["session"]) OR !empty($retour["suppression"]["action_client"])){
			// cote utilisateur js va supprimer l'ancienne inscription et va creer la nouvelle
			// -> la création nécéssite de connaitre le nom du stagiaire
			$sql="SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id=:stagiaire_id;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":stagiaire_id",$stagiaire_id);
			$req->execute();
			$d_stagiaire=$req->fetch();
			if(!empty($d_stagiaire)){
				$retour["nom"]=$d_stagiaire["sta_nom"];
				$retour["prenom"]=$d_stagiaire["sta_prenom"];
			}
		}


		// TRAITEMENT SPE AU CHANGEMENT DE CLIENT
		if($d_action["acl_client"]!=$d_action_stagiaire["acl_client"]){

			// on associe le stagiaire au client et on le fixe en employeur actuel
			$sql="SELECT cli_code,cli_nom FROM Clients WHERE cli_id=:client_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":client_id",$d_action["acl_client"]);
			$d_client=$req->fetch();

			$req=$Conn->prepare("UPDATE Stagiaires SET
			sta_client_code=:sta_client_code,
			sta_client_nom=:sta_client_nom,
			sta_client=:sta_client,
			WHERE sta_id=:stagiaire_id;");
			$req->bindParam(":sta_client_code",$d_client["cli_code"]);
			$req->bindParam(":sta_client_nom",$d_client["cli_nom"]);
			$req->bindParam(":sta_client",$d_action["acl_client"]);
			$req->bindParam(":stagiaire_id",$stagiaire_id);
			try{
				$req->execute();
			}Catch(Exception $e){

			}

			$req=$ConnSoc->prepare("INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:scl_stagiaire,:scl_client);");
			$req->bindParam(":scl_stagiaire",$stagiaire_id);
			$req->bindParam(":scl_client",$d_action["acl_client"]);
			try{
				$req->execute();
			}Catch(Exception $e){

			}
		}


		/**********************************
			ACTUALISATION DU NOMBRE DE STAGIAIRE
		**************************************/

		// CHANGEMENT DE ACTION_CLIENT
		/* possible de changer d'action client sans changer de client */

		/*
		if($action_client_id!=$d_action_stagiaire["ast_action_client"]){



			// l'inscription était confirme sur l'ancienne inscription

			if($d_action_stagiaire["ast_confirme"]){



				$acl_ca=$d_action_stagiaire["acl_ca"]-$d_action_stagiaire["acl_ca_unit"];
				$acl_ca=round($acl_ca,2);

				$sql="UPDATE Actions_Clients SET acl_for_nb_sta=acl_for_nb_sta-1";
				if($d_action_stagiaire["acl_pro_inter"]==1){
					$sql.=",acl_ca=" . $acl_ca;
				}
				$sql.=" WHERE acl_id=" . $d_action_stagiaire["acl_id"] . ";";
				$ConnSoc->query($sql);

			}

			// l'inscription sur la nouvelle action client est confirme
			if($ast_confirme){

				// ATENTION -> en cas de changement d'action client la cible peut ne pas avoir de stagiaire inscrit -> acl_for_nb_sta = saisie utilisateur
					// -> l'incremenatation ne marche pas


				$req=$ConnSoc->query("SELECT COUNT(ast_stagiaire) AS nb_sta FROM Actions_Stagiaires WHERE ast_action_client=" . $action_client_id . " AND ast_confirme;");
				$d_inscription=$req->fetch();
				if(!empty($d_inscription)){
					$nb_sta=intval($d_inscription["nb_sta"]);
				}else{
					$nb_sta=1;
				}

				$acl_ca=$d_action["acl_ca_unit"]*$nb_sta;
				$acl_ca=round($acl_ca,2);

				$sql="UPDATE Actions_Clients SET acl_for_nb_sta=". $nb_sta;
				if($d_action["acl_pro_inter"]==1){
					$sql.=", acl_ca=" . $acl_ca;
				}
				$sql.=" WHERE acl_id=" . $action_client_id . ";";
				$ConnSoc->query($sql);
			}
		}elseif($d_action_stagiaire["ast_confirme"]==1 AND $ast_confirme==0){
			// l'inscription n'est plus comfirmé -> on ne comptabilise plus le sta

			$acl_ca=$d_action["acl_ca"]-$d_action["acl_ca_unit"];
			$acl_ca=round($acl_ca,2);

			$sql="UPDATE Actions_Clients SET acl_for_nb_sta=acl_for_nb_sta-1";
			if($d_action["acl_pro_inter"]==1){
				$sql.=", acl_ca=" . $acl_ca;
			}
			$sql.=" WHERE acl_id=" . $action_client_id . ";";
			$ConnSoc->query($sql);

		}elseif($d_action_stagiaire["ast_confirme"]==0 AND $ast_confirme==1){

			// l'inscprition se confirme -> on comptabilise le sta

			$acl_ca=$d_action["acl_ca"]+$d_action["acl_ca_unit"];
			$acl_ca=round($acl_ca,2);

			$sql="UPDATE Actions_Clients SET acl_for_nb_sta=acl_for_nb_sta+1";
			if($d_action["acl_pro_inter"]==1){
				$sql.=", acl_ca=" . $acl_ca;
			}
			$sql.=" WHERE acl_id=" . $action_client_id . ";";
			$ConnSoc->query($sql);

		}*/

	}
	// fin traitement annexe
}

if(!empty($erreur_txt)){
	echo($erreur_txt);
}else{
	echo json_encode($retour);
}
?>
