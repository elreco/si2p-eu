<?php
	// MISE A JOUR D'UN CONTACT FOURNISSEUR
	
	include '../includes/connexion.php';
	include '../includes/connexion_fct.php';
	
	$erreur_txt="";
	$warning_txt="";
	
	if(!empty($_POST)){
		
		$fournisseur_id = 0;
		if(!empty($_POST['fournisseur'])){
			$fournisseur_id=intval($_POST['fournisseur']);
		}
		
		if(empty($fournisseur_id) OR empty($_POST['con_prenom']) OR empty($_POST['con_nom'])){
			$erreur_txt="Vous devez renseigner le nom et prénom du contact";
		}
	}else{
		$erreur_txt="Formulaire incomplet";
	}
	
	if(empty($erreur_txt)){
		
		$contact_id = 0;
		if(!empty($_POST['con_id'])){
			$contact_id=intval($_POST['con_id']);
		}
		
		$con_defaut = 0;
		if(!empty($_POST['con_defaut'])){
			$con_defaut = 1;
		}

		$con_fonction=0;
		if($_POST['con_fonction']){
			$con_fonction=intval($_POST['con_fonction']);
		}
		
		$req = $Conn->prepare("SELECT fou_type,fou_code FROM fournisseurs WHERE fou_id = " . $fournisseur_id);
		$req->execute();
		$fournisseur = $req->fetch();
		if(empty($fournisseur)){
			$erreur_txt="Impossible d'identifier le fournisseur!";
		}
	}
	if(empty($erreur_txt)){	
		
		if(!empty($contact_id)){

			$req = $Conn->prepare("SELECT fco_defaut FROM fournisseurs_contacts WHERE fco_id=:contact_id;");
			$req->bindParam(':contact_id',$contact_id);
			$req->execute();
			$d_contact=$req->fetch();
			if(!empty($d_contact)){
				if($d_contact["fco_defaut"]==1){
					$con_defaut=1;
				}
			}else{
				$erreur_txt="Impossible de recupéréer les données du contact!";
			}
			
		}
		
	}
	
	// ENREGISTREMENT
	
	if(empty($erreur_txt)){
		
		if($contact_id>0){
			
			// update du contact
			$req = $Conn->prepare("UPDATE fournisseurs_contacts SET fco_defaut = :fco_defaut, fco_fonction = :fco_fonction, fco_fonction_nom = :fco_fonction_nom, fco_titre = :fco_titre, fco_nom = :fco_nom, fco_prenom = :fco_prenom, fco_tel =:fco_tel, fco_fax = :fco_fax, fco_portable = :fco_portable, fco_mail = :fco_mail WHERE fco_id = :fco_id;");
			$req->bindParam(':fco_id', $contact_id);
			$req->bindParam(':fco_defaut', $con_defaut);
			$req->bindParam(':fco_fonction',$con_fonction);
			$req->bindParam(':fco_fonction_nom', $_POST['con_fonction_nom']);
			$req->bindParam(':fco_titre', $_POST['con_titre']);
			$req->bindParam(':fco_nom', $_POST['con_nom']);
			$req->bindParam(':fco_prenom', $_POST['con_prenom']);
			$req->bindParam(':fco_tel', $_POST['con_tel']);
			$req->bindParam(':fco_fax', $_POST['con_fax']);
			$req->bindParam(':fco_portable', $_POST['con_portable']);
			$req->bindParam(':fco_mail', $_POST['con_mail']);
			try{
				$req->execute();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
			}

		}else{
			
			// ajout du contact
			$req = $Conn->prepare("INSERT INTO fournisseurs_contacts (fco_defaut, fco_fonction, fco_fonction_nom, fco_titre, fco_nom, fco_prenom, fco_tel, fco_fax, fco_portable, fco_mail, fco_fournisseur) VALUES (:fco_defaut, :fco_fonction, :fco_fonction_nom,:fco_titre, :fco_nom, :fco_prenom, :fco_tel, :fco_fax, :fco_portable, :fco_mail, :fco_fournisseur);");
			$req->bindParam(':fco_defaut', $con_defaut);
			$req->bindParam(':fco_fournisseur',$fournisseur_id);
			$req->bindParam(':fco_fonction',$con_fonction);
			$req->bindParam(':fco_fonction_nom', $_POST['con_fonction_nom']);
			$req->bindParam(':fco_titre', $_POST['con_titre']);
			$req->bindParam(':fco_nom', $_POST['con_nom']);
			$req->bindParam(':fco_prenom', $_POST['con_prenom']);
			$req->bindParam(':fco_tel', $_POST['con_tel']);
			$req->bindParam(':fco_fax', $_POST['con_fax']);
			$req->bindParam(':fco_portable', $_POST['con_portable']);
			$req->bindParam(':fco_mail', $_POST['con_mail']);
			try{
				$req->execute();
				$contact_id = $Conn->lastInsertId();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
			}
			
		}
	}
	
	// TRAITEMENT ANNEXE
	IF(empty($erreur_txt)){
		
		if(!empty($con_defaut)){
			
			// SECURITE -> on supp l'ancien contact par defaut
			$req = $Conn->prepare("UPDATE fournisseurs_contacts SET fco_defaut = 0 WHERE fco_fournisseur=:fco_fournisseur AND NOT fco_id = :fco_id;");
			$req->bindParam(':fco_fournisseur', $fournisseur_id);
			$req->bindParam(':fco_id', $contact_id);
			$req->execute();
		}
		
		if($fournisseur['fou_type'] == 3){
			
			// SI AUTO - ENTREPRENEUR
			
			$sql="SELECT fin_id,fin_nom,fin_prenom FROM Fournisseurs_Intervenants WHERE fin_fournisseur=" . $fournisseur_id . ";";
			$req = $Conn->query($sql);
			$d_auto_ent=$req->fetchAll();
			if(!empty($d_auto_ent)){
				if(count($d_auto_ent)>1){				
					$warning_txt="Cet auto-entrepreneur dispose de plusieur intervenants!";				
				}else{
				
					// update de l'intervenant
					$req = $Conn->prepare("UPDATE fournisseurs_intervenants SET fin_nom = :fin_nom, fin_prenom = :fin_prenom, fin_tel = :fin_tel, fin_fax = :fin_fax, fin_mail = :fin_mail, fin_portable=:fin_portable WHERE fin_id = :fin_id;");
					$req->bindParam(':fin_id', $d_auto_ent[0]["fin_id"]);
					$req->bindParam(':fin_prenom', $_POST['con_prenom']);
					$req->bindParam(':fin_nom', $_POST['con_nom']);
					$req->bindParam(':fin_fax', $_POST['con_fax']);
					$req->bindParam(':fin_mail', $_POST['con_mail']);
					$req->bindParam(':fin_tel', $_POST['con_tel']);
					$req->bindParam(':fin_portable', $_POST['con_portable']);
					try{
						$req->execute();
					}Catch(Exception $e){
						$warning_txt=$e->getMessage();
					}
				
					if(empty($warning_txt)){
					
						if($d_auto_ent[0]["fin_nom"]!=$_POST['con_nom'] OR $d_auto_ent[0]["fin_prenom"]!=$_POST['con_prenom']){
							
							// CHANGEMENT DE NOM OU DE PRENOM -> ON ACTUALISE LE FICHE INTERVENANT PLANNING
							
							$sql="SELECT soc_id FROM Societes ORDER BY soc_id;";	
							$req = $Conn->query($sql);
							$d_societe=$req->fetchAll();
							if(!empty($d_societe)){
								
								$int_label_1=$fournisseur["fou_code"] . "-" . $_POST['con_nom'];
								$int_label_2=$_POST['con_prenom'];
								
								foreach($d_societe as $soc){
									
									$ConnFct=connexion_fct($soc["soc_id"]);
									
									$req = $ConnFct->prepare("UPDATE Intervenants SET int_label_1 = :int_label_1, int_label_2 = :int_label_2 
									WHERE int_type=3 AND int_ref_1=:int_ref_1 AND int_ref_2=:int_ref_2;");							
									$req->bindParam(':int_label_1', $int_label_1);
									$req->bindParam(':int_label_2', $int_label_2);
									$req->bindParam(':int_ref_1', $fournisseur_id);
									$req->bindParam(':int_ref_2', $d_auto_ent[0]["fin_id"]);
									try{
										$req->execute();
									}Catch(Exception $e){
										$warning_txt=$e->getMessage();
									}					
								}
							}	
						}
						
					}
				}
					
			}else{
				
				// création d'un intervenant
				$req = $Conn->prepare("INSERT INTO fournisseurs_intervenants (fin_nom, fin_prenom, fin_ad1, fin_ad2, fin_ad3, fin_cp, fin_ville, fin_tel, fin_fax, fin_mail, fin_fournisseur, fin_archive) VALUES (:fin_nom,:fin_prenom,:fin_ad1,:fin_ad2,:fin_ad3,:fin_cp,:fin_ville,:fin_tel,:fin_fax,:fin_mail, :fin_fournisseur,0) ");
				$req->bindParam(':fin_prenom', $_POST['fco_prenom']);
				$req->bindParam(':fin_nom', $_POST['fco_nom']);
				$req->bindParam(':fin_ad1', $_POST['fad_ad1']);
				$req->bindParam(':fin_ad2', $_POST['fad_ad2']);
				$req->bindParam(':fin_ad3', $_POST['fad_ad3']);
				$req->bindParam(':fin_cp', $_POST['fad_cp']);
				$req->bindParam(':fin_ville', $_POST['fad_ville']);
				$req->bindParam(':fin_fax', $_POST['fco_fax']);
				$req->bindParam(':fin_mail', $_POST['fco_mail']);
				$req->bindParam(':fin_tel', $_POST['fco_tel']);
				$req->bindParam(':fin_fournisseur', $fournisseur_id);
				try{
					$req->execute();
				}Catch(Exception $e){
					$warning_txt=$e->getMessage();
				}
			}
		}
		
		// ON RECUPERE LE LIBELLE DE LA FONCTION
		
		if(!empty($con_fonction)){
			
			$req = $Conn->prepare("SELECT cfo_libelle FROM Contacts_Fonctions WHERE cfo_id=:con_fonction;");
			$req->bindParam(':con_fonction',$con_fonction);
			$req->execute();
			$d_fonction=$req->fetch();
		}
		
	}

	if(empty($erreur_txt)){
		$contact=array(
			"fco_id" => $contact_id,
			"fco_nom" => $_POST['con_nom'],
			"fco_prenom" => $_POST['con_prenom'],
			"fco_tel" => $_POST['con_tel'],
			"fco_portable" => $_POST['con_portable'],
			"fco_fax" => $_POST['con_fax'],
			"fco_mail" => $_POST['con_mail'],
			"fco_fonction" => $con_fonction,
			"fco_fonction_nom" => $_POST['con_fonction_nom'],
			"fco_defaut" => $con_defaut,
			"cfo_libelle" => "",
			"warning_txt" => $warning_txt
		);
		
		if(isset($d_fonction)){
			$contact["cfo_libelle"]=$d_fonction["cfo_libelle"];
		}
		
	}else{
		$contact=array(
			"erreur_type" => 1,
			"erreur_txt" => $erreur_txt
		);
	}
	echo json_encode($contact);
?>