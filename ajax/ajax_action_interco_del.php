<?php

include "../includes/controle_acces.inc.php";

include '../includes/connexion_soc.php';


// SUPPRIME L'INSCRIPTION D'UN STAGIAIRE A UNE ACTION

$erreur_txt="";;

if(!empty($_POST)){

	$aci=0;
	if (isset($_POST['aci'])){
		$aci=intval($_POST['aci']);
	}

	if($aci>0){
        $sql="DELETE FROM actions_clients_intercos WHERE aci_id = :aci";
        $req=$ConnSoc->prepare($sql);
        $req->bindParam(":aci",$aci);
        $req->execute();
	}else{
		$erreur_txt="Formulaire incomplet!";
	}
    
}else{
	$erreur_txt="Formulaire incomplet!";
}


if(!empty($erreur_txt)){
	echo("Formulaire incomplet!");
}else{
	$retour=array(
		"aci" => $aci
	);
	echo json_encode($retour);
}

?>
