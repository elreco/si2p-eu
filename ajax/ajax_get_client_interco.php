<?php
include("../includes/connexion.php");
include("../includes/connexion_soc.php");
include("../includes/connexion_fct.php");
include("../modeles/mod_parametre.php");

if(isset($_GET['societe'])){

    global $Conn;

    $societe = $_GET['societe'];
    $action_client = $_GET['action_client'];
    $explode = explode("-",$action_client);
    $action_client = $explode[1];

    $ConnFct=connexion_fct($societe);
    // CHERCHER LES ACTIONS CORRESPONDANTES SUR LE PLANNING
    $sql="SELECT cli_id, cli_nom, cli_code FROM
    Clients LEFT JOIN Actions_Clients ON (Actions_Clients.acl_client = Clients.cli_id)
    WHERE acl_id = :action_client";
    $req = $ConnFct->prepare($sql);
    $req->bindParam(":action_client",$action_client);
    $req->execute();
    $client = $req->fetch();
    /////////////////// FIN INTERCO ///////////////////////

    if(!empty($client)){
        echo json_encode($client);
    }else{
        echo "Pas de client";
    }


}
?>
