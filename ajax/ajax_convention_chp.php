<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // SCRIPT GENERIQUE POUR METTRE A JOUR N CHAMP DE ACTIONS_CLIENTS

 // retour JSON compatible select2
 
 
$erreur_txt="";
if(isset($_POST)){
	 
	$con_id=0;
	if(!empty($_POST["con_id"])){
		$con_id=intval($_POST["con_id"]); 
	}
	
	if(empty($con_id)){
		
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();
		
	}
}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}

if(count($_POST)<=1){
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}
	
$sql="UPDATE Conventions SET ";
foreach($_POST as $k => $v){
	if($k!="con_id" AND $k!="societ"){
		$sql.=$k . "=:" . $k . ",";
	}
}
$sql=substr($sql,0,-1);
$sql.=" WHERE con_id=:con_id;";

$req=$ConnSoc->prepare($sql);
foreach($_POST as $k => $v){
	if($k!="societ"){
		$req->bindValue($k,$v);
	}
}
try{
	$req->execute();
}Catch(Exception $e){
	$erreur_txt=$e->getMessage();
	echo($erreur_txt);
	die();
}

echo(json_encode($_POST));
die();
?>