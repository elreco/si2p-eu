<?php 

include("../includes/connexion.php");
include_once('../connexion_soc.php');
include_once('../modeles/mod_parametre.php');

if(!empty($_POST['date_debut']) && !empty($_POST['date_fin'])){
	$date_debut = convert_date_sql($_POST['date_debut']);
	$date_fin = convert_date_sql($_POST['date_fin']);
}

$famille = $_POST['famille'];
$sous_famille = $_POST['sous_famille'];

if(!empty($date_debut) && !empty($date_fin) && !empty($famille)){
	$req = $ConnSoc->prepare("SELECT act_date_deb, act_intervenant, act_id FROM actions WHERE (act_date_deb BETWEEN '" . $date_debut . "' AND '" . $date_fin . "' ) AND act_pro_famille = " . $famille . " AND act_pro_sous_famille = " . $sous_famille);
	$req->execute();
	$actions = $req->fetchAll();
	
	foreach($actions as $a){
		$req = $ConnSoc->prepare("SELECT int_label_1, int_label_2 FROM intervenants WHERE int_id = " . $a['act_intervenant']);
		$req->execute();
		$intervenant = $req->fetch();
		$retour[] = array(
			"act_id" => $a['act_id'],
			"act_date_deb" => convert_date_txt($a['act_date_deb']),
			"act_intervenant" => $intervenant['int_label_2'] . " " . $intervenant['int_label_1']
		);
	}
	echo json_encode($retour);
}else{
	echo 0;
}



?>