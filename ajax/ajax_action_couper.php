<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // PERMET DE MEMORISER UNE ACTION A COUPER
 
if(isset($_POST)){

	if(!empty($_POST)){
		
		$action_id=0;
		if(!empty($_POST['action'])){
			$action_id=intval($_POST['action']);
		}
		
		if(!empty($action_id)){
		
			$sql="SELECT pda_texte,pda_survol FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=:action AND NOT pda_archive;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action",$action_id);
			try{
				$req->execute();
			}Catch(Exception $e){
				echo($e->getMessage());
				die();
			}
			$result=$req->fetchAll();
			if(!empty($result)){
				
				$action_coupe=array(
					"texte" => $result[0]["pda_texte"],
					"survol" => $result[0]["pda_survol"],
					"action" => $action_id,
					"nb_demi_j" => count($result)
				);
				$_SESSION["action_coupe"]=$action_coupe;
				
			}else{
				echo("Impossible de charger les dates de la formation.");
				die();	
			}
			
		}else{
			
			// action = 0 revient a annulé le coupé
			
			if(isset($_SESSION["action_coupe"])){
				unset($_SESSION["action_coupe"]);
				$action_coupe="ok";
			}else{
				echo("Aucune action dans le presse-papier.");
				die();	
			}
			
		}
		
	}else{
		echo("Formulaire incomplet!");
		die();
	}
}else{
	echo("Formulaire incomplet!");
	die();
}
echo(json_encode($action_coupe));			
?>