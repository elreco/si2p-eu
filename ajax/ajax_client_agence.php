<?php
include("../includes/connexion.php");
if(isset($_POST['field'])){

    $id= $_POST['field'];
    global $Conn;

    $req = $Conn->prepare("SELECT * FROM clients_societes WHERE cso_societe = " . $id . " AND cso_agence != 0 AND cso_archive = 0 AND cso_client =" . $_POST['client']);
    $req->execute();

    $clients_societes = $req->fetchAll();

    $agences = array();
    foreach($clients_societes as $c){
    	$req = $Conn->prepare("SELECT * FROM agences WHERE age_id =" . $c['cso_agence']);
	    $req->execute();

	    $agences[$c['cso_agence']] = $req->fetch();
    }

    echo json_encode($agences);

}
?>
