<?php
 // PERMET DE PRE-SELECTIONNER DES CLIENTS D'APRES UNE SAISIE UTILISATEUR
 
session_start();
include('../includes/connexion.php');
include('../includes/connexion_soc.php');
$erreur="";

/* UTILISATION 

- devis_voir

*/

// parametre

if(isset($_POST)){
		
	$stagiaire=0;
	if(!empty($_POST['stagiaire'])){
		$stagiaire=intval($_POST['stagiaire']);
	}
	
	$devis=0;
	if(!empty($_POST['devis'])){
		$devis=intval($_POST['devis']);
	}
	
	$devis_ligne=0;
	if(!empty($_POST['devis_ligne'])){
		$devis_ligne=intval($_POST['devis_ligne']);
	}
	
	if(empty($devis_ligne) OR empty($devis) OR empty($stagiaire)){
			$erreur="formulaire incomplet!";
	}
}

if(empty($erreur)){
	
	$sql="DELETE FROM Devis_Stagiaires WHERE dst_devis=" . $devis . " AND dst_devis_ligne=" . $devis_ligne . " AND dst_stagiaire=" . $stagiaire . ";";
	try{
		$ConnSoc->query($sql);
	}Catch(Exception $e){
		$erreur=$e->getMessage();
	}
	
}

if(!empty($erreur)){
	echo($erreur);
}else{
	echo(json_encode($stagiaire));
}
?>
