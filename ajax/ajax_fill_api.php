<?php

include "../includes/controle_acces.inc.php";
include('../includes/connexion.php');
include('../includes/connexion_soc.php');

$siret = $_GET['siren'] . $_GET['siret'];
// CODE APE
$sql="SELECT * FROM Ape ORDER BY ape_code,ape_libelle";
$req = $Conn->query($sql);
$d_apes = $req->fetchAll();

if(empty($siret)){

    return false;
}else{
    $cli_siren = $_GET['siren'];
    $adr_siret = $_GET['siret'];
    // REQUETE VERS l'API

    $ch = curl_init();
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    // UTILISER LA V3
    curl_setopt($ch,CURLOPT_URL, "https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/" . preg_replace('/\s+/', '', $siret));
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $resultat_json=curl_exec($ch);
    curl_close($ch);
    $result=json_decode($resultat_json);
    // SI JE NE TROUVE RIEN AVEC LE SIRET, JE CHERCHE PAR NOM
    if(empty($result->etablissement)){
        echo json_encode(false);
    }else{
        // CHERCHER LE NUMERO DE TVA
        // REQUETE VERS l'API
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        // UTILISER LA V3
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch,CURLOPT_URL, "https://entreprise.data.gouv.fr/api/sirene/v1/siren/" . preg_replace('/\s+/', '', $cli_siren));
        $resultat_json=curl_exec($ch);
        curl_close($ch);
        $result2=json_decode($resultat_json);
        $ape = "";
        $ape_code = "";
        $ape_nom = "";
        foreach($d_apes as $d_ape){
            if(!empty($result->etablissement->activite_principale) && str_replace(".", "", $result->etablissement->activite_principale) == $d_ape['ape_code']){
                $ape = $d_ape['ape_id'];
                $ape_code = $d_ape['ape_code'];
                $ape_nom = $d_ape['ape_libelle'];
            }
        }

        $cli_ad1 = $result->etablissement->numero_voie . " " . $result->etablissement->type_voie . " " . $result->etablissement->libelle_voie;
        $cli_ad2 = $result->etablissement->complement_adresse;
        $cli_cp = $result->etablissement->code_postal;
        $cli_ville = $result->etablissement->libelle_commune;
        $numero_tva_intra="";
        if(!empty($result2->numero_tva_intra)){
            $numero_tva_intra = $result2->numero_tva_intra;
        }
        $return = [
            "cli_nom" => $result->etablissement->unite_legale->denomination,
            "cli_ape" => $ape,
            "cli_ape_code" => $ape_code,
            "cli_ape_nom" => $ape_nom,
            "cli_adr_ad1" => $cli_ad1,
            'cli_adr_ad2' => $cli_ad2,
            "cli_adr_cp" => $cli_cp,
            "cli_adr_ville" => $cli_ville,
            'cli_ident_tva' => $numero_tva_intra
        ];

        echo json_encode($return);
    }

}

?>
