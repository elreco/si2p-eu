<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion_soc.php';

/************************
	ENREGISTREMENT D'UN REMBOURSEMENT SUR UN REGLEMENT
***************************
*/

$erreur_txt="";
if(isset($_POST)){

	if(!empty($_POST["chp_date"])){
		$DT_periode=date_create_from_format('d/m/Y',$_POST["chp_date"]);
		if(!is_bool($DT_periode)){
			$chp_date=$DT_periode->format("Y-m-d");
		}else{
			$erreur_txt="Formulaire incomplet!";
		}
	}else{
		$erreur_txt="Formulaire incomplet!";
	}

	if(!empty($_POST["fac_retro"])){
		if(is_array($_POST["fac_retro"])){
			$fac_retro=$_POST["fac_retro"]; 
		}else{
			$erreur_txt="Formulaire incomplet!";
		}
	}else{
		$erreur_txt="Formulaire incomplet!";
	}

	if(empty($erreur_txt)){
		
		$fac_retro_liste=implode(",",$fac_retro);
		
		$sql="UPDATE Factures SET fac_aff_retro=1,fac_aff_retro_date=:fac_aff_retro_date WHERE fac_id IN (" . $fac_retro_liste . ");"; 
		$req = $ConnSoc->prepare($sql);
		$req->bindParam(":fac_aff_retro_date",$chp_date);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur_txt="Impossible de mettre les factures à jour. (" . $e->getMessage() . ")";
		}
		
	}
	
	if(empty($erreur_txt)){
		
		$retour=array(
			"date" => $_POST["chp_date"],
			"factures" => $fac_retro
		);
	
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}
if(empty($erreur_txt)){
	echo json_encode($retour);
}else{
	echo($erreur_txt);
}
 
?>