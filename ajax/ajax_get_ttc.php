<?php
include "../includes/controle_acces.inc.php";
include("../includes/connexion.php");
include('../modeles/mod_tva.php');

$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = " . $_POST['commande']);
$req->execute();
$commande = $req->fetch();
$req = $Conn->prepare("SELECT * FROM commandes_lignes WHERE cli_commande = " . $_POST['commande']);
$req->execute();
$commandes_lignes = $req->fetchAll();
$total = 0;
$total_ligne = array();
foreach($commandes_lignes as $l){ // pour toutes les lignes calculer la tva

    $date = $commande['com_date'];
    $req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = " . $l['cli_tva'] . " AND tpe_date_deb <= '" . $date . "' AND (tpe_date_fin >= '"  . $date . "' OR tpe_date_fin IS NULL)");
    $req->execute();
    $tva = $req->fetch();

    $req = $Conn->prepare("UPDATE commandes_lignes SET cli_tva_taux = " . $tva['tpe_taux'] . " WHERE cli_commande = " . $l['cli_id']);
    $req->execute();


    $tva_nom = $base_tva[$l['cli_tva']];
    $total_ligne[$tva_nom]['nom'] = $tva['tpe_taux'];

    $valeur = (($l['cli_ht'])*$tva['tpe_taux'])/100;

    if(!empty($total_ligne[$tva_nom]['montant'])){
        $total_ligne[$tva_nom]['montant'] = floatval(str_replace(',', '.', $total_ligne[$tva_nom]['montant'])) + $valeur;
    }else{
        $total_ligne[$tva_nom]['montant'] = $valeur;
    }
    $total_ligne[$tva_nom]['montant'] = number_format($total_ligne[$tva_nom]['montant'], 2, ',', ' ');

    $valeur = (($l['cli_ht'])*$tva['tpe_taux'])/100;
    $valeur = floatval(str_replace(',', '.', $l['cli_ht'])) + floatval(str_replace(',', '.', $valeur));

    if(!empty($total)){
        $total = $total + $valeur;
    }else{
        $total = $valeur;
    }
}
$req = $Conn->prepare("UPDATE commandes SET com_ttc = " . $total . " WHERE com_id = " . $commande['com_id']);
$req->execute();
echo json_encode($total_ligne);
?>
