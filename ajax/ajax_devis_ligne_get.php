<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion_soc.php';

 // RETOURNE LES DONNEES D'UNE LIGNE DE DEVIS
 
 
$erreur="";
if(isset($_POST)){
	if(!empty($_POST)){
		
		$devis_ligne=0;
		if(!empty($_POST["devis_ligne"])){
			$devis_ligne=intval($_POST["devis_ligne"]); 
		}else{
			$erreur="Paramètres absent";
		}
		
		if(empty($erreur)){
			
			$sql="SELECT * FROM Devis_Lignes WHERE dli_id=" . $devis_ligne . ";";
			$req=$ConnSoc->query($sql);
			$d_devis_ligne=$req->fetch(PDO::FETCH_ASSOC);
			if(!empty($d_devis_ligne)){
				echo json_encode($d_devis_ligne);
			}else{
				$erreur="Impossible de charger les données du devis.";
			}
		}
	}else{
		$erreur="Paramètres absent";
	}
}else{
	$erreur="Paramètres absent";
}
if(!empty($erreur)){
	echo($erreur);
}
?>