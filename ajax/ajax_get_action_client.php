<?php
include "../includes/controle_acces.inc.php";

include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // RETOURNE LES INFOS D'UN ENREGISTREMENT ACTIONS CLIENTS
 
 // RETOUR FORMAT JSON
 
$erreur_text="";
if(isset($_POST)){
	 
	$action_client_id=0;
	if(!empty($_POST["action_client"])){
		$action_client_id=intval($_POST["action_client"]); 
	}
	
	if(empty($action_client_id)){
		$erreur_text="Impossible de charger les données";
	}
}else{
	$erreur_text="Impossible de charger les données";
}

if(empty($erreur_text)){
	
	$stagiaire_id=0;
	if(!empty($_POST["stagiaire"])){
		$stagiaire_id=intval($_POST["stagiaire"]); 
	}
	
	$sql="SELECT acl_attestation,acl_client FROM Actions_Clients WHERE acl_id=:action_client_id;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action_client_id",$action_client_id);
	try{
		$req->execute();
	}Catch(Exception $e){
		$erreur_text=$e->getMessage();
	}
	$client_action=$req->fetch();
	
	// ON RECUPERE AUSSI LES DATES DE FORMATIONS
	// -> si stagiaire>0 on ajoute la valeur d'inscription du stagiaire
	
	$sql="SELECT int_label_1,DATE_FORMAT(pda_date,'%d/%m/%Y') AS pda_date_fr,pda_demi,ase_id,ase_h_deb,ase_h_fin";
	if(!empty($stagiaire_id)){
		$sql.=",ass_session";
	}
	$sql.=" FROM Actions_Clients
	INNER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
	INNER JOIN Plannings_Dates ON (Plannings_Dates.pda_id=Actions_Clients_Dates.acd_date)
	INNER JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)
	INNER JOIN Actions_Sessions ON (Actions_Clients_Dates.acd_date=Actions_Sessions.ase_date)";
	if(!empty($stagiaire_id)){
		$sql.=" LEFT JOIN Actions_Stagiaires_Sessions ON (Actions_Sessions.ase_id=Actions_Stagiaires_Sessions.ass_session AND ass_stagiaire=:stagiaire_id)";
	}
	$sql.=" WHERE acl_id=:acd_action_client ORDER BY pda_date,pda_demi;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":acd_action_client",$action_client_id);
	if(!empty($stagiaire_id)){
		$req->bindParam(":stagiaire_id",$stagiaire_id);
	}
	$req->execute();
	$d_sessions=$req->fetchAll(PDO::FETCH_ASSOC);
	$client_action["sessions"]=$d_sessions;
}
if(empty($erreur_text)){
	echo json_encode($client_action);
}else{
	echo($erreur_text);
};
 
 
?>