<?php
include("../includes/connexion.php");
include('../includes/connexion_soc.php');

include('../modeles/mod_del_suspect_correspondance.php');


// PERMET DE SUPPRIMER UNE CORRESPONDANCE

$erreur="";
if(isset($_POST)){

	$correspondance=0;
	if(!empty($_POST["correspondance"])){
		$correspondance=intval($_POST["correspondance"]);
	}

	
	if($correspondance>0){
		
		$result_supp=del_suspect_correspondance($correspondance);
		
		if(!is_array($result_supp)){
			var_dump($result_supp);
			$erreur="1-ERREUR TRAITEMENT";
		}

	}else{
		$erreur="1-ERREUR PARAMETRE";
	}
}else{
	$erreur="2-ERREUR PARAMETRE";
}
if($erreur==""){
	
	// index result_supp
	// "correspondance" : id corresp supp
	// "non_traite" : id de la corresp qui n'est plus traité

	
	echo json_encode($result_supp);
}else{
	echo $erreur;
}
?>