<?php
include("../includes/connexion.php");
include('../connexion_soc.php');

if(isset($_GET['liste_case'])){
	
	$case=explode (",",$_GET['liste_case']);	
	
	/*echo "<pre>";
		print_r($case);
	echo "</pre>";*/

	$req_supp=$ConnSoc->prepare("DELETE FROM Plannings_Dates WHERE pda_intervenant=:intervenant AND pda_date=:date AND pda_demi=:demi;");
	
	foreach($case as $c){
		
		if(!empty($c)){
			$donnee_case=explode ("_",$c);
			$intervenant=$donnee_case[0];
			$date=$donnee_case[1];
			$demi=$donnee_case[2];
			
			$req_supp->bindParam(":intervenant",$intervenant);
			$req_supp->bindParam(":date",$date);
			$req_supp->bindParam(":demi",$demi);
			$req_supp->execute();

		}
	}
}else{
	echo "ERREUR PARAMETRE";
}
?>
