<?php 
include "../includes/controle_acces.inc.php";
include("../includes/connexion.php");
include("../includes/connexion_fct.php");
include('../modeles/mod_add_notification.php');

$erreur_txt="";

if(isset($_POST)){
	
	$commande_id=0;
	if(!empty($_POST['com_id'])){
		$commande_id=intval($_POST['com_id']);
	}
	
	$com_annule_txt = rawurldecode($_POST['com_annule_txt']);

	if(empty($commande_id) OR empty($com_annule_txt)){
		$erreur_txt="Formulaire incomplet";	
	}
}else{
	$erreur_txt="Formulaire incomplet";	
}
	
// ANNULATION D'UN BC

if(empty($erreur_txt)){
	
	$req = $Conn->prepare("UPDATE commandes SET com_annule = 1, com_annule_txt = :com_annule_txt  WHERE com_id = :com_id");
	$req->bindParam(':com_annule_txt', $com_annule_txt);
    $req->bindParam(':com_id', $commande_id);
	try{
		$req->execute();
	}Catch(Exception $e){
		$erreur_txt->$e->getMessage();
	}
	
}
if(empty($erreur_txt)){
	
	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}

	$notif_user=array();
	
	$req = $Conn->prepare("SELECT com_donneur_ordre,com_numero,com_action,com_societe FROM Commandes WHERE com_id=:com_id;");
	$req->bindParam(':com_id', $commande_id);
	$req->execute();
	$d_commande=$req->fetch(PDO::FETCH_ASSOC);
	if($d_commande["com_donneur_ordre"]!=$acc_utilisateur){
		$notif_user[]=$d_commande["com_donneur_ordre"];
	}
	
	$req = $Conn->prepare("SELECT cva_utilisateur FROM commandes_validations  WHERE cva_commande = :com_id AND cva_utilisateur>0");
    $req->bindParam(':com_id',$commande_id);
	$req->execute();
	$d_validants=$req->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($d_validants)){
		foreach($d_validants as $v){
			$notif_user[]=$v["cva_utilisateur"];
		}
	}
	
	if(!empty($notif_user)){
		
		$liste=implode($notif_user,",");
		$not_txt=$_SESSION['acces']['acc_prenom'] . " " . $_SESSION['acces']['acc_nom'] . " a annulé le bon de commande " . $c['com_numero'];
		add_notifications("<i class='fa fa-truck'></i>",$not_txt,"bg-danger","commande_voir.php?commande=" . $commande_id,"","",$liste,0,0,0);

	}

	// CHARGE SUR L'ACTION
	if(!empty($d_commande['com_action'])){
		
		$act_charge=0;
		
		$sql="SELECT SUM(com_ht) FROM Commandes,Fournisseurs WHERE com_fournisseur=fou_id AND fou_interco_soc=0 AND com_etat>0 AND NOT com_annule AND com_societe=:societe AND com_action=:action;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":societe",$d_commande['com_societe']);
		$req->bindParam(":action",$d_commande['com_action']);
		$req->execute();
		$d_action_commande=$req->fetch();
		if(!empty($d_action_commande)){
			$act_charge=$d_action_commande[0];	
		}
		
		$ConnFct=connexion_fct($d_commande['com_societe']);
		
		$sql="UPDATE Actions SET act_charge=:charge WHERE act_id=:action;";
		$req=$ConnFct->prepare($sql);
		$req->bindParam(":charge",$act_charge);
		$req->bindParam(":action",$d_commande['com_action']);
		$req->execute();

	}
}
if(empty($erreur_txt)){
	$retour=array(
		"com_annule_txt" => $com_annule_txt
	);
	echo json_encode($retour);	
}else{
	echo($erreur_txt);
}

?>