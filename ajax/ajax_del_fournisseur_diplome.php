<?php
	include "../includes/controle_acces.inc.php";
	include("../connexion.php");
	include("../modeles/mod_parametre.php");
	include("../modeles/mod_competence.php");
	include("../modeles/mod_diplome.php");
	$req = $Conn->prepare("SELECT * FROM diplomes WHERE dip_id = :dip_id");
	$req->bindParam("dip_id",$_POST['diplome']);
	$req->execute();
	// on va chercher le diplome
	$diplome = $req->fetch();

	$req = $Conn->prepare("SELECT * FROM intervenants_diplomes WHERE idi_ref_id = :idi_ref_id AND idi_diplome = :idi_diplome");
	$req->bindParam("idi_ref_id",$_POST['intervenant']);
	$req->bindParam("idi_diplome",$_POST['diplome']);
	$req->execute();
	// on va chercher le diplome
	$intervenant = $req->fetch();

	unlink("../documents/intervenants/" . $_POST['fournisseur'] . "/" . $_POST['intervenant'] . "_" . $_POST['diplome'] . "_" . clean($diplome['dip_libelle']) . "." . $intervenant['idi_ext']);

	$req = $Conn->prepare("DELETE FROM intervenants_diplomes WHERE idi_ref_id = :idi_ref_id AND idi_diplome = :idi_diplome AND idi_ref = 2");
	$req->bindParam("idi_ref_id",$_POST['intervenant']);
	$req->bindParam("idi_diplome",$_POST['diplome']);
	$req->execute();
	//
		$ref = 2;
		$ref_id = $_POST['intervenant'];
		$competences_act=get_competences_intervenant($ref,$ref_id,1);

		if(!empty($competences_act)){
			
			foreach($competences_act as $c){
				
				// pour chaque competence on controle la presence des diplomes 
				
				$ico_controle_dt=1;
				$ico_controle_dt_txt="";
				
				$diplomes_act=get_diplomes_competence_inter($ref,$ref_id,$c["com_id"]);		

				if(!empty($diplomes_act)){
					
					foreach($diplomes_act as $d){
						
						
						
						if(empty($d["idi_date_deb"])){
							
							// le diplome n'est pas renseigné
							
							if($ico_controle_dt_txt!=""){
								$ico_controle_dt_txt.="<br>";
							}
							if($d["dco_obligatoire"]){										
								if($ico_controle_dt<3){
									$ico_controle_dt=3;
								}
								$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est pas renseigné (obligatoire).";
							}else{
								if($ico_controle_dt<2){
									$ico_controle_dt=2;
								}
								$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est pas renseigné.";	
							}
							
						}elseif($d["dip_validite"]>0 AND date('Y-m-d', strtotime("+" . $d['dip_validite'] . " months", strtotime($d['idi_date_deb']))) < date("Y-m-d")){
							
							// le diplome est périodique et il est perimé
							
							if($d["dco_obligatoire"]){										
								if($ico_controle_dt<3){
									$ico_controle_dt=3;
								}
								$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est plus valide (obligatoire).";
							}else{
								if($ico_controle_dt<2){
									$ico_controle_dt=2;
								}
								$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est plus valide.";	
							}
						}						
					}
				}
				update_competence_intervenant($ref,$ref_id,$c["com_id"],$ico_controle_dt,$ico_controle_dt_txt);
				$array[] = array(
					"ico_controle_dt" => $ico_controle_dt, 
					"ico_controle_dt_txt" => $ico_controle_dt_txt, 
					"com_id" => $c['com_id']
				);
			}
		}
	////////////////////////////// FIN MISE A JOUR COMPETENCES ////////////////////////////////

	
	echo json_encode($array);


?>