<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // RETOURNE LES DONNEES ADRESSES ENREGISTRES DANS LA TABLE ADRESSES
 
// parametre POST

// liste_ref_id : string liste des ID adr_ref_id
// ref : INT 1 adresses clients

// 	type : type d'adresse a retourne (facultatif) 

// retour => tableau JSON

// Appel :
// planning.php

// auteur : FG 06/09/2016
 
 
$erreur=0;

if(isset($_POST)){
	 
	$client=0;
	if(!empty($_POST["client"])){
		$client=$_POST["client"]; 
	}	
	
	$ref=0;
	if(!empty($_POST["ref"])){
		$ref=$_POST["ref"]; 
	}	

	$action=0;
	if(!empty($_POST["action"])){
		$action=intval($_POST["action"]); 
	}

	
	if($ref!=0 AND ($client!="" OR $action!=0)){
		
		$type=0;
		if(!empty($_POST["type"])){
			$type=$_POST["type"]; 
		}
		
		$groupe=0;
		if(empty($action)){
			if(!empty($_POST["groupe"])){
				$groupe=intval($_POST["groupe"]); 
			}
		}
	
		
		if($action>0){
			
			// on cree liste_ref_id d'apres les clients qui participe à l'action
			
			$sql="SELECT DISTINCT acl_client FROM Actions_Clients WHERE acl_action=" . $action . ";";
			$req = $ConnSoc->query($sql);
			$results = $req->fetchAll();
			if(!empty($results)){
				foreach($results as $r){
					$liste_ref_id.=$r["acl_client"]. ",";
				}
			}		
		}elseif($groupe>0){
			
			$sql_cli="SELECT cli_id,cli_groupe,cli_filiale_de,cli_fil_de,cli_niveau FROM Clients WHERE cli_id=:client;";
			$req_cli = $Conn->prepare($sql_cli);
			$req_cli -> bindParam(":client",$client);
			$req_cli ->execute();
			$result = $req_cli->fetch();
			if(!empty($result)){
				
				if($result["cli_groupe"]==1 AND $result["cli_filiale_de"]>0){
					
					$liste_ref_id=$result["cli_id"] . "," . $result["cli_filiale_de"];
					
					if(!empty($result["cli_niveau"])){
						
						$entite=$result["cli_fil_de"];
						$securite=0;

						while ($entite!=$result["cli_filiale_de"] AND $securite<6){
							
							$securite++;
							
							$req_cli -> bindParam(":client",$entite);
							$req_cli ->execute();
							$r_boucle = $req_cli->fetch();
							if(!empty($r_boucle)){
								
								$liste_ref_id.="," . $r_boucle["cli_id"];
								$entite=$r_boucle["cli_fil_de"];
								
							}else{
								$securite=999;
								break;
							}
						}
						
						
					}
				}
			}	
			
		}
		
		if(isset($liste_ref_id)){
			if(substr($liste_ref_id,-1)==","){
				$liste_ref_id=substr($liste_ref_id, 0, -1);
			};
		}
   
		$adresses=array();

		// ADRESSE D'INTERVENTION
		$sql="SELECT adr_id,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_defaut,adr_ref_id";
		$sql.=" FROM Adresses WHERE adr_ref=" . $ref;
		if(isset($liste_ref_id)){
			$sql.=" AND adr_ref_id IN (" . $liste_ref_id . ")";
		}else{
			$sql.=" AND adr_ref_id=" . $client;
		}
		if($type>0){
			$sql.=" AND adr_type=" . $type;	
		}
		$sql.=" ORDER BY adr_nom";
		$req = $Conn->query($sql);
		$results = $req->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$adresses[]=array(
					"id" => $r["adr_id"],		// nécessaire pour injection plugin select2
					"text" => $r["adr_nom"] . " " . $r["adr_cp"] . " " . $r["adr_ville"],	// nécessaire pour injection plugin select2
					"ref_id" => $r["adr_ref_id"],
					"nom" => $r["adr_nom"],
					"service" => $r["adr_service"],
					"ad1" => $r["adr_ad1"],
					"ad2" => $r["adr_ad2"],
					"ad3" => $r["adr_ad3"],
					"cp" => $r["adr_cp"],
					"ville" => $r["adr_ville"],
					"defaut" => $r["adr_defaut"]
				);	
			}
		}	
	}else{
		$erreur=10;	
	}
}else{
	$erreur=10;
}
//echo("erreur : " . $erreur);
if($erreur==0){
	echo json_encode($adresses);
}else{
	header("HTTP/1.0 500 ");
};
 
 
?>