<?php
	include "../includes/controle_acces.inc.php";
	
	include_once("../includes/connexion_soc.php");
	include_once("../includes/connexion.php");
	

	// AJOUT OU MAJ D'UN CLIENT SUR UNE ACTION
	
	// utilise sur le planning
	
	$erreur_text="";
	
	// ELEMENT DU FORM OBLIGATOIRE
	
	$action_id=0;
	if(isset($_POST["action"])){
		if(!empty($_POST["action"])){
			$action_id=intval($_POST["action"]);
		}else{
			$erreur_text="Formulaire incomplet!";
		}	
	}else{
		$erreur_text="Formulaire incomplet!";
	}
	
	$client=0;
	if(isset($_POST["client"])){
		if(!empty($_POST["client"])){
			$client=intval($_POST["client"]);
		}else{
			$erreur_text="Formulaire incomplet!";
		}	
	}else{
		$erreur_text="Formulaire incomplet!";
	}
	
	$acl_produit=0;
	if(isset($_POST["acl_produit"])){
		if(!empty($_POST["acl_produit"])){
			$acl_produit=$_POST["acl_produit"];
		}else{
			$erreur_text="Formulaire incomplet!";
		}	
	}else{
		$erreur_text="Formulaire incomplet!";
	}
	
	$acl_commercial=0;
	if(isset($_POST["acl_commercial"])){
		if(!empty($_POST["acl_commercial"])){
			$acl_commercial=$_POST["acl_commercial"];
		}else{
			$erreur_text="Formulaire incomplet!";
		}	
	}else{
		$erreur_text="Formulaire incomplet!";
	}

	// AUTRE ELEMENT DU FORM
	
	if(empty($erreur_text)){

		$action_client_id=0;
		if(!empty($_POST["action_client"])){
			$action_client_id=intval($_POST["action_client"]);
		}
		
		// le client devient lieu d'intervention
		
		$act_adr_ref=0;
		if(!empty($_POST["act_adr_ref"])){
			$act_adr_ref=intval($_POST["act_adr_ref"]);
		}
		
		$act_adresse=0;
		$act_contact=0;
		
		if($act_adr_ref==1){
			
			$act_adresse=0;
			if(!empty($_POST["act_adresse"])){
				$act_adresse=intval($_POST["act_adresse"]);
			}
			$act_contact=0;
			if(!empty($_POST["act_contact"])){
				$act_contact=intval($_POST["act_contact"]);
			}
		}
		
		// type de produit
		$acl_pro_inter=0;
		$acl_ca=0;
		$acl_ca_unit=0;
		if(!empty($_POST["acl_pro_inter"])){
			$acl_pro_inter=1;
		}
		if($acl_pro_inter==1){
			if(!empty($_POST["ca_inter"])){
				$acl_ca=$_POST["ca_inter"];
			}
		}else{
			if(!empty($_POST["ca_intra"])){
				$acl_ca=$_POST["ca_intra"];
			}
		}
		
		$acl_confirme=0;
		$acl_confirme_date=null;
		if(isset($_POST["acl_confirme"])){
			if($_POST["acl_confirme"]=="on"){
				$acl_confirme=1;
				if(isset($_POST["acl_confirme_date"])){
					if(!empty($_POST["acl_confirme_date"])){
						$acl_confirme_date=date_create_from_format('j/m/Y',$_POST["acl_confirme_date"]);					
					}	
				}
			}	
		}
		
		$acl_archive=0;
		if(isset($_POST["acl_archive"])){
			if($_POST["acl_archive"]=="on"){
				$acl_archive=1;
			}	
		}
		
		$acl_opca_fac=0;
		if(isset($_POST["acl_opca_fac"])){
			if($_POST["acl_opca_fac"]=="on"){
				$acl_opca_fac=1;
			}	
		}
		
	}
	
	// DONNEE POUR TRAITEMENT
	
	if(empty($erreur_text)){
		
		// sur la personne connecte
		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];	
		}
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];	
		}
		
		$acc_utilisateur=0;
		if(!empty($_SESSION['acces']["acc_ref"])){
			if($_SESSION['acces']["acc_ref"]==1){
				$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
			}
		}

		
		
		// SUR LE CODE PRODUIT
		$sql="SELECT pro_code_produit FROM Produits WHERE pro_id=:produit;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":produit",$acl_produit);
		$req->execute();
		$d_produit=$req->fetch();
		
		// tab pour suivre modif sur action client
		$avant_modif=array(
			"acl_confirme" =>0,
			"acl_confirme_date" => null
		);

		if(!empty($action_client_id)){
			
			// EDITION		
			$sql="SELECT acl_produit,acl_pro_recurrent,acl_act_recurrent,acl_client,acl_confirme_date FROM Actions_Clients WHERE acl_id=:action_client;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client",$action_client_id);
			$req->execute();
			$avant_modif=$req->fetch();
			
			if($acl_pro_inter==1){
				// si pro inter il faut mettre a jour le ca en fonction du nombre d'inscrit
				$sql="SELECT COUNT(ast_stagiaire) FROM Actions_Stagiaires WHERE ast_action_client=:action_client;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":action_client",$action_client_id);
				$req->execute();
				$inscription=$req->fetch();
				if(!empty($inscription)){
					$acl_ca=$acl_ca_unit*$inscription[0];
				}
			}
		}
		
		// confirmation sans date
		if(!empty($acl_confirme) AND empty($acl_confirme_date)){
			if(!empty($avant_modif["acl_confirme_date"])){
				$acl_confirme_date=$avant_modif["acl_confirme_date"];
			}else{
				$acl_confirme_date=date("Y-m-d");;
			}
		}
	
		
		// SUR LES AUTRES CLIENTS
		
		if($acl_pro_inter==0){
			// c'est un intra
			$act_type=1;
		}else{
			$act_type=2;
		}
		
		$pda_confirme=$acl_confirme;
		
		$sql="SELECT acl_pro_inter,acl_confirme FROM Actions_Clients WHERE acl_action=:action AND NOT acl_id=:action_client AND NOT acl_archive;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_client",$action_client_id);
		$req->bindParam(":action",$action_id);
		$req->execute();
		$d_clients=$req->fetchAll();
		if(!empty($d_clients)){
			foreach($d_clients as $cli){
				
				if(empty($cli["acl_confirme"]) AND $pda_confirme==1){
					$pda_confirme=0;
				}
				if($act_type==1 AND empty($cli["acl_pro_inter"])){
					$erreur_text="Vous ne pouvez pas superposer 2 intra!";
				}elseif($act_type==2 AND empty($cli["acl_pro_inter"])){
					$act_type=1;
				}
			}
		}
	}
	/************************************
			ENREGISTREMENT 
	****************************************/
	
	if(empty($erreur_text)){
		
		if($action_client_id>0){
			
			//echo("EDITION<br/>");

			$sql="UPDATE Actions_Clients SET 
			acl_client=:client,
			acl_produit=:acl_produit,
			acl_commercial=:acl_commercial,
			acl_ca=:acl_ca,
			acl_ca_unit=:acl_ca_unit,
			acl_pro_inter=:acl_pro_inter,
			acl_pro_reference=:acl_pro_reference,
			acl_pro_libelle=:acl_pro_libelle,
			acl_confirme=:acl_confirme,
			acl_confirme_date=:acl_confirme_date,
			acl_archive=:acl_archive,
			acl_opca_fac=:acl_opca_fac,
			acl_opca_num=:acl_opca_num";
			$sql.=" WHERE acl_id=:action_client;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":client",$client);
			$req->bindParam(":acl_produit",$acl_produit);
			$req->bindParam(":acl_commercial",$acl_commercial);
			$req->bindParam(":acl_ca",$acl_ca);
			$req->bindParam(":acl_ca_unit",$acl_ca_unit);
			$req->bindParam(":acl_pro_inter",$acl_pro_inter);
			$req->bindParam(":acl_pro_reference",$d_produit["pro_code_produit"]);
			$req->bindParam(":acl_pro_libelle",$_POST["acl_pro_libelle"]);
			$req->bindParam(":acl_confirme",$acl_confirme);
			$req->bindParam(":acl_confirme_date",$acl_confirme_date);
			$req->bindParam(":acl_archive",$acl_archive);
			$req->bindParam(":acl_opca_fac",$acl_opca_fac);
			$req->bindParam(":acl_opca_num",$acl_opca_num);
			$req->bindParam(":action_client",$action_client_id);
			try{		
				$req->execute();
			}Catch(Exception $e){
				$erreur_text="A" . $e->getMessage();
			}
			
		}else{
		
			// CREATION
			
			$sql="INSERT INTO Actions_Clients (
			acl_action,
			acl_client,
			acl_produit,
			acl_commercial,
			acl_ca_unit,
			acl_ca,
			acl_pro_inter,
			acl_creation_date,
			acl_creation_uti,
			acl_pro_reference,
			acl_pro_libelle,
			acl_confirme,
			acl_confirme_date,
			acl_archive,
			acl_opca_fac,
			acl_opca_num
			) VALUES (
			:acl_action,
			:acl_client,
			:acl_produit,
			:acl_commercial,
			:acl_ca_unit,
			:acl_ca,
			:acl_pro_inter,
			NOW(),
			:acl_creation_uti,
			:acl_pro_reference,
			:acl_pro_libelle,
			:acl_confirme,
			:acl_confirme_date,
			:acl_archive,
			:acl_opca_fac,
			:acl_opca_num);";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":acl_action",$action_id);
			$req->bindParam(":acl_client",$client);
			$req->bindParam(":acl_produit",$acl_produit);
			$req->bindParam(":acl_commercial",$acl_commercial);
			$req->bindParam(":acl_ca_unit",$acl_ca_unit);
			$req->bindParam(":acl_ca",$acl_ca);		
			$req->bindParam(":acl_pro_inter",$acl_pro_inter);
			$req->bindParam(":acl_creation_uti",$acc_utilisateur);			
			$req->bindParam(":acl_pro_reference",$d_produit["pro_code_produit"]);
			$req->bindParam(":acl_pro_libelle",$_POST["acl_pro_libelle"]);
			$req->bindParam(":acl_confirme",$acl_confirme);
			$req->bindParam(":acl_confirme_date",$acl_confirme_date);
			$req->bindParam(":acl_archive",$acl_archive);
			$req->bindParam(":acl_opca_fac",$acl_opca_fac);
			$req->bindParam(":acl_opca_num",$acl_opca_num);
			try{		
				$req->execute();
				$action_client_id=$ConnSoc->lastInsertId();
			}Catch(Exception $e){
				$erreur_text="B" . $e->getMessage();
			}
		}
	}
		
	// TRAITEMENT ANNEXE
	
	if(empty($erreur_text)){
		
		// MAJ DE L'ACTION 
		
		$sql="UPDATE Actions SET 
		act_pro_type=:act_type";
		if($act_adr_ref==1){
			// le client est utilisé comme lieu d'intervention
			if(!empty($act_adresse)){
				$sql.=",act_adr_ref=1,
				act_adr_ref_id=:client,
				act_adresse=:act_adresse,
				act_adr_nom=:act_adr_nom,
				act_adr_service=:act_adr_service,
				act_adr1=:act_adr1,
				act_adr2=:act_adr2,
				act_adr3=:act_adr3,
				act_adr_cp=:act_adr_cp,
				act_adr_ville=:act_adr_ville";
				if(!empty($act_contact)){
					$sql.=",act_contact=:act_contact,
					act_con_ref=1,
					act_con_ref_id=:client,
					act_con_nom=:act_con_nom,
					act_con_prenom=:act_con_prenom,
					act_con_tel=:act_con_tel,
					act_con_portable=:act_con_portable";
				}
			}
			$sql.=" WHERE act_id=:action_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":act_type",$act_type);
			if(!empty($act_adresse)){
				$req->bindParam(":client",$client);
				$req->bindParam(":act_adresse",$act_adresse);
				$req->bindParam(":act_adr_nom",$_POST["act_adr_nom"]);
				$req->bindParam(":act_adr_service",$_POST["act_adr_service"]);
				$req->bindParam(":act_adr1",$_POST["act_adr1"]);
				$req->bindParam(":act_adr2",$_POST["act_adr2"]);
				$req->bindParam(":act_adr3",$_POST["act_adr3"]);
				$req->bindParam(":act_adr_cp",$_POST["act_adr_cp"]);
				$req->bindParam(":act_adr_ville",$_POST["act_adr_ville"]);
				if(!empty($act_contact)){
					$req->bindParam(":act_contact",$_POST["act_contact"]);
					$req->bindParam(":act_con_nom",$_POST["act_con_nom"]);
					$req->bindParam(":act_con_prenom",$_POST["act_con_prenom"]);
					$req->bindParam(":act_con_tel",$_POST["act_con_tel"]);
					$req->bindParam(":act_con_portable",$_POST["act_con_portable"]);
				}
			}
			$req->bindParam(":action_id",$action_id);
			try{		
				$req->execute();
			}Catch(Exception $e){
				$erreur_text="C" . $e->getMessage();
			}
		}
		
		// MISE A JOUR DES DATES DE FORMATION
		
		$sql="UPDATE Plannings_Dates SET pda_confirme=:pda_confirme WHERE pda_type=1 AND pda_ref_1=:action_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->bindParam(":pda_confirme",$pda_confirme);
		$req->execute();	
	}
	
	if(!empty($erreur_text)){
		echo($erreur_text);
	}
?>