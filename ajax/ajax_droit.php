<?php

	// permet d'affecter ou de retirer un droit à un profil ou à un utilisateur
	
	include("../includes/connexion.php");
	include("../modeles/mod_droit.php");
	include("../modeles/mod_utilisateur.php");
	
	$droit=0;
	if(!empty($_GET["droit"])){
		$droit=$_GET["droit"];
	}
	$profil=0;
	if(!empty($_GET["profil"])){
		$profil=$_GET["profil"];
	}
	$utilisateur=0;
	if(!empty($_GET["utilisateur"])){
		$utilisateur=$_GET["utilisateur"];
	}
	
	$action=0;
	if(!empty($_GET["action"])){
		$action=$_GET["action"];
	}
	
	if($droit>0 AND $action>0 AND ($profil>0 OR $utilisateur>0) ){
		
		if($profil>0){
			
			$utilisateurs=get_utilisateurs(0,0,$profil,"",false);
			
			// mise a jour d'un droit par defaut pour un profil
			
			if($action==1){
				
				// affectation du droit
							
				if(!empty(get_droit_profil($droit,$profil))){
					
					delete_droit_profil($droit,$profil);
					
					// on aplique la suppresion à tout les utilisateurs du profils
					
					foreach($utilisateurs as $u){
						delete_droit_utilisateur($droit,$u["uti_id"]);	
					}
					
				}else{
					
					insert_droit_profil($droit,$profil);
					
					// on aplique l'affectation à tout les utilisateurs du profils
					
					foreach($utilisateurs as $u){
					
						if(empty(get_droit_utilisateur($droit,$u["uti_id"]))){
							// s'il ne l'avait pas déjà par défaut on ne donne pas le droit de réaffection
							insert_droit_utilisateur($droit,$u["uti_id"],0);
							// s'il l'avait déja on ne fait rien
						}
					}
					
				};
			}else{
					
				// gestion de l'héritage
				
				$donnee=get_droit_profil($droit,$profil);
				
				if(!empty($donnee)){
					if($donnee["udr_reaffecte"]==1){
						
						// on supprime le droit de réaffection
						update_droit_profil($droit,$profil,0);	
						
						foreach($utilisateurs as $u){
							if(!empty(get_droit_utilisateur($droit,$u["uti_id"]))){
								// l'utilisateur a le droit -> on supprime la réaffection							
								update_droit_utilisateur($droit,$u["uti_id"],0);		
								// l'utilisateur n'avait pas le droit -> on fait rien
							}
						}
					}else{
						// on attribue le droit de réaffectation
						update_droit_profil($droit,$profil,1);
						
						foreach($utilisateurs as $u){
							if(!empty(get_droit_utilisateur($droit,$u["uti_id"]))){
								// l'utilisateur a le droit -> on lui donne la réaffection						
								update_droit_utilisateur($droit,$u["uti_id"],1);									
							}else{
								// l'utilisateur n'avait pas le droit -> on lui donne le droit s'il peut le reaffecte il doit pouvoir l'avoir	
								insert_droit_utilisateur($droit,$u["uti_id"],1);
							}
						}
						
					};
				};
			};
			
			echo(json_encode("ok"));
						
		}else if($utilisateur>0){
			
			// mise a jour d'un droit d'un utilisateur
			
			if($action==1){
				
				// affectation du droit
							
				if(!empty(get_droit_utilisateur($droit,$utilisateur))){
					
					delete_droit_utilisateur($droit,$utilisateur);

				}else{
					
					insert_droit_utilisateur($droit,$utilisateur,0);

				};
			}else{
					
				// gestion de l'héritage
				
				$donnee=get_droit_utilisateur($droit,$utilisateur);
				
				if(!empty($donnee)){
					if($donnee["udr_reaffecte"]==1){
						
						// on supprime le droit de réaffection
						update_droit_utilisateur($droit,$utilisateur,0);	

					}else{
						// on attribue le droit de réaffectation
						update_droit_utilisateur($droit,$utilisateur,1);
						
					};
				};
			};
			
			echo(json_encode("ok"));
			
		};
	
	};
?>	
