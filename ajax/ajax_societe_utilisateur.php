<?php

	// permet d'affecter ou de retirer un droit à un profil ou à un utilisateur
	
	include("../includes/connexion.php");
	include("../modeles/mod_utilisateur.php");
	
	$societe=0;
	if(!empty($_GET["societe"])){
		$societe=$_GET["societe"];
	}
	$agence=0;
	if(!empty($_GET["agence"])){
		$agence=$_GET["agence"];
	}
	$utilisateur=0;
	if(!empty($_GET["utilisateur"])){
		$utilisateur=$_GET["utilisateur"];
	}
	
	$coche=0;
	if($_GET["coche"] != "false"){
		$coche=1;
	}
	
	$retour=array();
	if(!empty($societe) && empty($agence)){
		
		if(!empty($coche)){
			// on coche la société
			$req=$Conn->prepare("SELECT * FROM Utilisateurs_Societes WHERE uso_utilisateur = :uso_utilisateur AND uso_societe = :uso_societe AND uso_agence=0");
			$req->bindParam(":uso_utilisateur",$utilisateur);
			$req->bindParam(":uso_societe",$societe);
			$req->execute();
			$utilisateurs_societes = $req->fetch();
			if(empty($utilisateurs_societes)){
				$req=$Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur, uso_societe, uso_agence) VALUES(:uso_utilisateur, :uso_societe,0)");
				$req->bindParam(":uso_utilisateur",$utilisateur);
				$req->bindParam(":uso_societe",$societe);
				$req->execute();
			}
			// PAR DEJAUT ON AFFECTE TOUTES LES AGENCES
			$req=$Conn->prepare("SELECT age_id,uso_agence FROM Agences LEFT OUTER JOIN Utilisateurs_Societes 
			ON (Agences.age_id=Utilisateurs_Societes.uso_agence AND Utilisateurs_Societes.uso_societe=:uso_societe 
			AND Utilisateurs_Societes.uso_utilisateur=:uso_utilisateur) 
			WHERE age_societe=:uso_societe;");
			$req->bindParam(":uso_utilisateur",$utilisateur);
			$req->bindParam(":uso_societe",$societe);
			$req->execute();
			$utilisateurs_agences = $req->fetchAll();
			if(!empty($utilisateurs_agences)){
				foreach($utilisateurs_agences as $tab_agence){
					if(empty($tab_agence["uso_agence"])){
						$req=$Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur, uso_societe, uso_agence) VALUES(:uso_utilisateur, :uso_societe,:uso_agence)");
						$req->bindParam(":uso_utilisateur",$utilisateur);
						$req->bindParam(":uso_societe",$societe);
						$req->bindParam(":uso_agence",$tab_agence["age_id"]);
						$req->execute();
						
						$retour[]=array(
							"uso_utilisateur" => $utilisateur,
							"uso_societe" => $societe,
							"uso_agence" => $tab_agence["age_id"]
							
						);
					}
					
				}
				
				if(!empty($retour)){
					echo json_encode($retour);
				}
			}
		}else{
			// ON DECOCHE LA SOCIETE
			/*$req=$Conn->prepare("SELECT * FROM Utilisateurs_Societes WHERE uso_utilisateur = :uso_utilisateur AND uso_societe = :uso_societe");
			$req->bindParam(":uso_utilisateur",$utilisateur);
			$req->bindParam(":uso_societe",$societe);
			$req->execute();
			$utilisateurs_societes = $req->fetchAll();

			if(!empty($utilisateurs_societes)){

				echo json_encode($utilisateurs_societes);
			}*/
			$req=$Conn->prepare("DELETE FROM Utilisateurs_Societes WHERE uso_utilisateur = :uso_utilisateur AND uso_societe = :uso_societe AND uso_agence=0");
			$req->bindParam(":uso_utilisateur",$utilisateur);
			$req->bindParam(":uso_societe",$societe);
			$req->execute();
		}
		
	};
	if(!empty($agence) && !empty($societe)){
		
		if(!empty($coche)){
			// ON COCHE UNE AGENCE
			$req=$Conn->prepare("SELECT * FROM Utilisateurs_Societes WHERE uso_utilisateur = :uso_utilisateur AND uso_agence = :uso_agence AND uso_societe = :uso_societe");
			$req->bindParam(":uso_utilisateur",$utilisateur);
			$req->bindParam(":uso_agence",$agence);
			$req->bindParam(":uso_societe",$societe);
			$req->execute();
			$utilisateurs_agences = $req->fetch();
			if(empty($utilisateurs_agences)){
				$req=$Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur, uso_agence, uso_societe) VALUES(:uso_utilisateur, :uso_agence, :uso_societe)");
				$req->bindParam(":uso_utilisateur",$utilisateur);
				$req->bindParam(":uso_agence",$agence);
				$req->bindParam(":uso_societe",$societe);
				$req->execute();
			}
			
		}else{

			// ON DECOCHE UNE AGENCE
			$req=$Conn->prepare("DELETE FROM Utilisateurs_Societes WHERE uso_utilisateur = :uso_utilisateur AND uso_societe = :uso_societe AND uso_agence = :uso_agence");
			$req->bindParam(":uso_utilisateur",$utilisateur);
			$req->bindParam(":uso_societe",$societe);
			$req->bindParam(":uso_agence",$agence);
			$req->execute();
			
			// SI UTI N'A PAS ACCES A UNE DES AGENCES IL N'A PAS ACCES A LA SOCIETE
			$req=$Conn->prepare("DELETE FROM Utilisateurs_Societes WHERE uso_utilisateur = :uso_utilisateur AND uso_societe = :uso_societe AND uso_agence=0");
			$req->bindParam(":uso_utilisateur",$utilisateur);
			$req->bindParam(":uso_societe",$societe);
			$req->execute();
			
			echo($societe);
			
			
		}
		
	};
?>	
