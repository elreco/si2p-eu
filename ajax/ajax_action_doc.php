<?php

include "../includes/controle_acces.inc.php";

include '../includes/connexion_soc.php';
include '../includes/connexion.php';
include '../modeles/mod_parametre.php';
include '../modeles/mod_convention_data.php';
include '../modeles/mod_convention_pdf.php';
include('../modeles/mod_convocation_data.php');
include('../modeles/mod_convocation_pdf.php');

include('../modeles/mod_presence_conso_data.php');
include('../modeles/mod_presence_conso_pdf.php');

include('../modeles/mod_stagiaire_attestation_data.php');
include('../modeles/mod_stagiaire_attestation_pdf.php');

include('../modeles/mod_action_attestation_data.php');
include('../modeles/mod_action_attestation_pdf.php');

include('../modeles/mod_get_juridique.php');
include('../modeles/mod_planning_periode_lib.php');
include('../modeles/mod_date_periode_lib.php');

include('../modeles/mod_upload.php');

// PERMET DE GENERER LES DOCS PDF ASSOCIE A UNE FORMATION

$erreur_txt="";

if(!empty($_POST)){

	$action_id=0;
	if (isset($_POST['action'])){
		$action_id=intval($_POST['action']);
	}
	if(empty($action_id)){
		$erreur_txt="Formulaire incomplet!";
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}

/***********************************
			CONTROLE
***********************************/
if(isset($_POST['monop'])){
//
	$erreur = upload('file','Societes/' . $_POST['acc_societe'] . '/Presences/','monop_presence_' . $_POST['acl_id'],0,['pdf'],1);
	if(empty($erreur)){
		$_SESSION['message'][] = array(
			"titre" => "Succès",
			"type" => "success",
			"message" => "Votre pdf a été enregistré"
		);
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => "Votre pdf n'a pas été enregistré"
		);
	}
	Header("Location: /action_voir.php?action=" . $action_id . "&onglet=3&societ=" . $_POST['acc_societe']);
	die();

}else{
	if(empty($erreur_txt)){

		// SUR L'ACTION

		$req=$ConnSoc->prepare("SELECT act_agence,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille FROM Actions WHERE act_id=:action_id;");
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$d_action=$req->fetch();

		// personne connecte
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];
		}
		if($conn_soc_id!=$acc_societe){
			$acc_societe=$conn_soc_id;
		}

		// CONVENTIONS

		$retour=array(
			"conventions" => array(),
			"convocations" => array(),
			"presences" => array(),
			"attestations_ind" => array(),
			"attestations_ses" => array(),
			"attestations_form" => array()
		);

		$sql="SELECT con_id,con_numero FROM Conventions WHERE con_action=:action_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$d_conventions=$req->fetchAll();
		if(!empty($d_conventions)){
			foreach($d_conventions as $con){

				if(!empty($_POST['convention_' . $con["con_id"]])){
					$data=convention_data($con["con_id"]);
					if(!empty($data)){
						$pdf=model_convention_pdf($data);
						if($pdf){
							$retour["conventions"][]=array(
								"con_id" => $con["con_id"],
								"con_numero" => $con["con_numero"]
							);
						}
					}
				}
			}
		}

		// SELECTION DES STAGIAIRES

		$req_sta=$ConnSoc->prepare("SELECT * FROM Actions_Stagiaires WHERE ast_action=:action_id AND ast_action_client=:action_client_id;");
		$req_sta->bindParam(":action_id",$action_id);



		$sql="SELECT acl_id FROM Actions_Clients WHERE acl_action=:action_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$d_action_clients=$req->fetchAll();
		if(!empty($d_action_clients)){
			foreach($d_action_clients as $action_cli){

				if(!empty($_POST['convocation_' . $action_cli["acl_id"]])){
					$data=convocation_data($action_cli["acl_id"],0);
					if(!empty($data)){
						$pdf=model_convocation_pdf($data);
						if($pdf){
							$retour["convocations"][]=$action_cli["acl_id"];
						}
					}
				}

				if(!empty($_POST['presence_conso_' . $action_cli["acl_id"]])){
					$data= presence_conso_data($action_id,$action_cli["acl_id"]);
					if(!empty($data)){
						$pdf=model_presence_conso_pdf($data);
						if($pdf){
							$retour["presences"][]=$action_cli["acl_id"];
						}
					}
				}

				if(!empty($_POST['attest_ind_' . $action_cli["acl_id"]])){

					// ON RECHERCHE LES STAGIARES QUI PARTICIPE

					$d_attestations=array();

					$req_sta->bindParam(":action_client_id",$action_cli["acl_id"]);
					$req_sta->execute();
					$d_stagiaires=$req_sta->fetchAll();
					if(!empty($d_stagiaires)){
						foreach($d_stagiaires as $sta){
							$r_attest=stagiaire_attestation_data($sta);
							if(!empty($r_attest)){
								$d_attestations[]=$r_attest;
							}
						}
					}
					if(!empty($d_attestations)){
						$d_attestations["pdf"]="attestation_ind_" . $action_id . "_" . $action_cli["acl_id"];
						$pdf=model_stagiaire_attestation_pdf($d_attestations);
						if($pdf){
							$retour["attestations_ind"][]=$action_cli["acl_id"];
						}
					}
				}

				if(!empty($_POST['attest_ses_' . $action_cli["acl_id"]])){
					$data=action_attestation_data($action_id,$action_cli["acl_id"],2);
					if(!empty($data)){
						$pdf=model_action_attestation_pdf($data);
						if($pdf){
							$retour["attestations_ses"][]=$action_cli["acl_id"];
						}
					}
				}

				if(!empty($_POST['attest_form_' . $action_cli["acl_id"]])){
					$data=action_attestation_data($action_id,$action_cli["acl_id"],1);
					if(!empty($data)){
						$pdf=model_action_attestation_pdf($data);
						if($pdf){
							$retour["attestations_form"][]=$action_cli["acl_id"];
						}
					}
				}
			}
		}
	}

	if(empty($erreur_txt)){
		echo json_encode($retour);
	}
}

?>
