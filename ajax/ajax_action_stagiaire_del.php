<?php

include "../includes/controle_acces.inc.php";

include '../includes/connexion_soc.php';

include "../modeles/mod_action_stagiaire_del.php";


// SUPPRIME L'INSCRIPTION D'UN STAGIAIRE A UNE ACTION

$erreur_txt="";;

if(!empty($_POST)){
	$action_id=0;
	if (isset($_POST['action'])){
		$action_id=intval($_POST['action']);
	}

	$stagiaire_id=0;
	if (isset($_POST['stagiaire'])){
		$stagiaire_id=intval($_POST['stagiaire']);
	}

	$session_id=0;


	if($stagiaire_id>0 AND $action_id>0){

		// personne connecte
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];
		}

		if(isset($_POST['session'])){
			$session_id=intval($_POST['session']);
		}

		$result=del_action_stagiaire($stagiaire_id,$action_id,$session_id);

		if($result && $result===2){
			$erreur_txt="Impossible de supprimer le stagiaire car une signature est enregistrée !";
		}

		if($result===false){
			$erreur_txt="Erreur modèle!";
		}

	}else{
		$erreur_txt="Formulaire incomplet!";
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}


if(!empty($erreur_txt)){
	echo($erreur_txt);
}else{
	$retour=array(
		"session" => $session_id,
		"stagiaire" => $stagiaire_id
	);
	echo json_encode($retour);
}

?>
