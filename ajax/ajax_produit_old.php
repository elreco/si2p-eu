<?php

	include("../includes/connexion.php");
	include('../connexion_soc.php');


	$client=0;
	if(!empty($_GET['client'])){
		$client=$_GET['client'];
	}
	$type=0;
	if(!empty($_GET['type'])){
		$type=$_GET['type'];
	}
	
	$action=0;
	if(!empty($_GET['action'])){
		$action=$_GET['action'];
	}
	//echo("action : " . $action);
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	
	if($client>0){
		$req = $ConnSoc->prepare("SELECT cli_categorie,cli_groupe,cli_filiale_de,cli_filiale_de_soc FROM Clients WHERE cli_id=:client;");
		$req->bindParam(":client",$client);
		$req->execute();
		$c=$req->fetch();
	};

	if(!empty($c)){
		
		if($action>0){
			$req = $ConnSoc->prepare("SELECT act_pro_categorie,act_pro_famille,act_pro_sous_famille FROM Actions WHERE act_id=:action;");
			$req->bindParam(":action",$action);
			$req->execute();
			$a=$req->fetch();
			if(empty($a)){
				unset($a);
			}			
		}
		
		
		if($c["cli_categorie"]!=2){
			
			// CLIENT CLASSIQUE
			
			// il a accès à tout les codes produits du catalogue

			if($type==1){
				// INTRA
				$sql="SELECT pro_id,pro_reference FROM Produits";
				$sql.=" WHERE NOT pro_archive AND pro_categorie IN (1,2) AND NOT ISNULL(pro_reference)";
				if(isset($a)){
					$sql.=" AND pro_categorie=:categorie AND pro_famille=:famille AND pro_sous_famille=:sous_famille";
				}
				$sql.=" AND NOT pro_reference='' ORDER BY pro_reference;";	
			}elseif($type==2){
				// iNTER
				$sql="SELECT pro_id,pro_reference_inter FROM Produits";
				$sql.=" WHERE NOT pro_archive AND pro_categorie IN (1,2) AND NOT ISNULL(pro_reference_inter)";
				if(isset($a)){
					$sql.=" AND pro_categorie=:categorie AND pro_famille=:famille AND pro_sous_famille=:sous_famille";
				}
				$sql.=" AND NOT pro_reference_inter='' ORDER BY pro_reference_inter;";				
			}
			$req = $Conn->prepare($sql);
			if(isset($a)){
				$req ->bindParam(":categorie",$a["act_pro_categorie"]);
				$req ->bindParam(":famille",$a["act_pro_famille"]);
				$req ->bindParam(":sous_famille",$a["act_pro_sous_famille"]);
			}
			$req ->execute();
			$results=$req->fetchAll();
			if(!empty($results)){
				foreach($results as $r){
					$produits[]=array(
						"id" => $r["pro_id"],
						"text" => $r[1]
					);
				}
			}

		}else{
			// GC
			
			if($c["cli_groupe"]&&$c["cli_filiale_de"]>0){
				// il s'agit d'une filiale et les produits sont renseignés sur la holding
				$conn_get_id=$c["cli_filiale_de_soc"]; // societe de la mm
				$produit_client=$c["cli_filiale_de"];
			}else{
				$conn_get_id=$acc_societe;
				$produit_client=$client;
			}
			include('../connexion_get.php');
			
			if($type==1){
				// INTRA
				$sql="SELECT cpr_produit,cpr_reference,cpr_categorie FROM Clients_Produits WHERE cpr_client=:client";
				$sql.=" AND NOT cpr_archive AND cpr_categorie IN (1,2)";
				if(isset($a)){
					$sql.=" AND cpr_categorie=:categorie AND cpr_famille=:famille AND cpr_sous_famille=:sous_famille";
				}
				$sql.=" ORDER BY cpr_reference;";		
			}elseif($type==2){
				// iNTER
				$sql="SELECT cpr_produit,cpr_reference_inter,cpr_categorie FROM Clients_Produits WHERE cpr_client=:client";
				$sql.=" AND NOT cpr_archive AND cpr_categorie IN (1,2)";
				if(isset($a)){
					$sql.=" AND cpr_categorie=:categorie AND cpr_famille=:famille AND cpr_sous_famille=:sous_famille";
				}
				$sql.=" ORDER BY cpr_reference_inter;";			
			}
			$req = $ConnGet->prepare($sql);
			$req->bindParam(":client",$produit_client);
			if(isset($a)){
				$req ->bindParam(":categorie",$a["act_pro_categorie"]);
				$req ->bindParam(":famille",$a["act_pro_famille"]);
				$req ->bindParam(":sous_famille",$a["act_pro_sous_famille"]);
			}
			$req->execute();
			$results=$req->fetchAll();
			if(!empty($results)){
				foreach($results as $r){
					$produits[]=array(
						"id" => $r["pro_id"],
						"text" => $r[1]
					);
				}
			}
			
		}
		if(!empty($produits)){
			echo json_encode($produits);
		}else{
			header("HTTP/1.0 500 ");
		}	
	}
?>
