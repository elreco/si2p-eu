<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion_soc.php';
include '../modeles/mod_del_session.php';


 // PERMET DE METTRE A JOUR UN CHAMP PRECIS DANS LA BASE
  
$erreur=0;
$erreur_txt="";
$valeur="";
if(isset($_POST)){
	
	if(!empty($_POST["table"])){
		$table=$_POST["table"]; 
	}else{
		$erreur_txt="Erreur paramètre!";	  
		$erreur=1;
	}
	if(!empty($_POST["champ"])){
		$champ=$_POST["champ"]; 
	}else{
		$erreur_txt="Erreur paramètre!";	  
		$erreur=1;
	}
	if(!empty($_POST["champ_id"])){
		$champ_id=$_POST["champ_id"]; 
	}else{
		$erreur_txt="Erreur paramètre!";	  
		$erreur=1;
	}
	if(!empty($_POST["valeur_id"])){
		$valeur_id=$_POST["valeur_id"]; 
	}else{
		$erreur_txt="Erreur paramètre!";	  
		$erreur=1;
	}
	
	
	
	if($erreur==0){
		
		
		$societe=0;
		if(!empty($_POST["societe"])){
			$societe=intval($_POST["societe"]); 
		}
		$valeur=$_POST["valeur"]; 

		include '../includes/connexion_fct.php';
	
		$ConnFct=connexion_fct($societe);
		
		$sql="UPDATE " . $table . " SET " . $champ . " = '" . $valeur . "' WHERE " . $champ_id . "=" . $valeur_id . ";";		
		try {
			$req=$ConnFct->query($sql);
		} catch (Exception $e) {
			$erreur_txt=$sql;	  
			$erreur=1;
		}
	}
	

}else{
	$erreur_txt=$e->getMessage();	  
	$erreur=1;
}
if($erreur==0){
	$retour=array(
		"erreur" => $erreur,
		"erreur_txt" => $erreur_txt
	);
}else{
	$retour=array(
		"erreur" => $erreur,
		"erreur_txt" => $erreur_txt,
		$champ => $valeur
	);
}

echo(json_encode($retour));
?>