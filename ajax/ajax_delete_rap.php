<?php
include("../includes/connexion.php");
include("../includes/connexion_soc.php");
include("../includes/connexion_fct.php");
include_once('../modeles/mod_parametre.php');
include_once('../modeles/mod_tva.php');
if(isset($_GET)){
    $for_id = $_GET['rap_id'];

    $sql="SELECT * FROM Formations WHERE for_id =" . $for_id;
    $req = $ConnSoc->query($sql);
    $d_formation=$req->fetch();
    $prec_rapport_km_deb = 0;
    // MAJ DES KILOMETRES
    if(!empty($d_formation['for_rap_precedent'])){

        $ConnFct = connexion_fct($d_formation['for_rap_precedent_soc']);
        $sql="SELECT * FROM Formations  WHERE for_id =" . $d_formation['for_rap_precedent'];
        $req = $ConnFct->prepare($sql);
        $req->execute();
        $d_formation_prec=$req->fetch();
        if(!empty($d_formation['for_rap_suivant']) && !empty($d_formation_prec)){
            $ConnFct = connexion_fct($d_formation['for_rap_suivant_soc']);
            $sql="SELECT * FROM Formations  WHERE for_id =" . $d_formation['for_rap_suivant'];
            $req = $ConnFct->prepare($sql);
            $req->execute();
            $d_formation_suivant=$req->fetch();



            $ConnFct = connexion_fct($d_formation['for_rap_suivant_soc']);
            $sql="UPDATE Formations  SET for_rap_precedent_soc=" . $d_formation['for_rap_precedent_soc'] . ", for_rap_precedent=" . $d_formation_prec['for_id'] . " WHERE for_id=" . $d_formation['for_rap_suivant'];
            $req = $ConnFct->prepare($sql);
            $req->execute();

            $for_new_km = abs($d_formation_suivant['for_km_deb'] - $d_formation_prec['for_km_deb']);
            $ConnFct = connexion_fct($d_formation['for_rap_precedent_soc']);
            $sql="UPDATE Formations  SET for_km_fin= ".$d_formation_suivant['for_km_deb'].",for_km= ". $for_new_km . ", for_rap_suivant_soc=" . $d_formation_suivant['for_id'] . ", for_rap_suivant=" . $d_formation['for_rap_suivant_soc'] ." WHERE for_id=" . $d_formation_prec['for_id'];
            $req = $ConnSoc->prepare($sql);
            $req->execute();

        }elseif(!empty($d_formation_prec)){
            $ConnFct = connexion_fct($d_formation['for_rap_precedent_soc']);
            $sql="UPDATE Formations  SET for_km_fin= 0,for_km= 0, for_rap_suivant_soc=0, for_rap_suivant=0 WHERE for_id=" . $d_formation_prec['for_id'];
            $req = $ConnSoc->prepare($sql);
            $req->execute();
        }


    }

    // DELETE
    $sql="DELETE FROM Formations WHERE for_id =" . $for_id;
    $req = $ConnSoc->query($sql);
    // DELETE
    $sql="DELETE FROM Formations_Actions WHERE fac_formation =" . $for_id;
    $req = $ConnSoc->query($sql);

    echo 1;

}
