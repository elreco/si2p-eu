<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion_soc.php';

include "../modeles/mod_del_planning_intervenant.php"; 


// SUPPRESSION D'UNE LIGNE DE PLANNING

$erreur=0;

if(!empty($_POST)){
	
	$intervenant=0;
	if (isset($_POST['intervenant'])){
		$intervenant=intval($_POST['intervenant']);	
	}
	
	if(!empty($_POST["date_deb"])){
		$date_deb = date_create_from_format('d/M/Y',$_POST["date_deb"]);
		if(is_bool($date_deb)){
			$date_deb = date_create_from_format('Y-m-d',$_POST["date_deb"]);
		}
		if(is_bool($date_deb)){
			unset($date_deb);
		}
	}
	
	$agence=0;
	if (isset($_POST['agence'])){
		$agence=intval($_POST['agence']);	
	}
	
	if($intervenant>0 AND isset($date_deb)){
		
		$date_fin = new DateTime($date_deb->format('Y-m-d'));
		$date_fin->add(new DateInterval('P5D'));
		
		$suppression=del_planning_intervenant($intervenant,$date_deb,$date_fin,$agence);

		if($suppression===FALSE){
			echo("Impossible de supprimer cette ligne de planning");
		}else{
			
			$semaine=$date_deb->format("W");
			$annee=$date_deb->format("Y");
		
			$retour=array(
				"intervenant" => $intervenant,
				"semaine" => $semaine,
				"annee" => $annee,
				"agence" => $agence
			);
			
			echo json_encode($retour);
		}
	}else{
		echo("ERREUR PARAM");
	}
}else{
	echo("ERREUR POST");
}

?>
