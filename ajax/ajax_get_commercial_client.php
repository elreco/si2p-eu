<?php
include_once('../includes/connexion.php');
include_once('../includes/connexion_soc.php');
include_once('../includes/connexion_fct.php');
if(isset($_POST['client'])){

	$id=0;
	if(!empty($_POST['client'])){
		$id=$_POST['client'];
	}
	$societe=0;
	if(!empty($_POST['societe'])){
		$societe=$_POST['societe'];
	}
	$agence=0;
	if(!empty($_POST['agence'])){
		$agence=$_POST['agence'];
	}

    global $Conn;
	global $ConnSoc;

    $req = $Conn->prepare("SELECT cso_commercial FROM Clients_Societes WHERE cso_client =" . $id . " AND cso_societe =" . $societe . " AND cso_agence = " . $agence);
    $req->execute();
    $cso_commercial = $req->fetch();

    if(!empty($cso_commercial)){
		$ConnFct = connexion_fct($societe);
    	$req = $ConnFct->prepare("SELECT com_label_1, com_label_2 FROM Commerciaux WHERE com_id =" . $cso_commercial['cso_commercial']);
	    $req->execute();
	    $cli_commercial = $req->fetch();

    }else{
    	$cli_commercial['com_label_1'] = "Pas de commercial";
    	$cli_commercial['com_label_2'] = "";
    }

    echo json_encode($cli_commercial);


}
?>
