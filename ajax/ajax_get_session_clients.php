<?php
include "../includes/controle_acces.inc.php";
include '../connexion_soc.php';
include '../modeles/mod_get_action_clients.php';

 // RETOURNE LES INFOS CONCERNANT LES CLIENTS QUI PARTICIPENT A UNE SESSION
 
 // RETOUR FORMAT JSON
 
$erreur=0;
if(isset($_POST)){
	 
	$session_id=0;
	if(!empty($_POST["session"])){
		$session_id=$_POST["session"]; 
	}else{
		$erreur=1;
	}	 
	
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=$_POST["action"]; 
	}else{
		$erreur=1;
	}	 
	
	
	if($erreur==0){
		
		// SUR L'ACTION DE REF
		
		$results=get_action_clients($action_id,0,$session_id);
		if(!empty($results)){
			foreach($results as $r){
				$clients[]=array(
					"id" => $r["acl_id"], 	//compatibilite select2
					"text" => $r["cli_code"] . " - " . $r["acl_pro_reference"],		//compatibilite select2
				);
			}
			echo json_encode($clients);
		}
			
			
	}
}
 
?>