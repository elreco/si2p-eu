<?php
 // PERMET DE RECUPERER LES INFOS D'UN FOURNISSEURS INTERVENANT INCLUS
 
include("../includes/connexion.php");

session_start();

$erreur_text="";

if(!empty($_POST)){
	$fournisseur_id="";
	if(!empty($_POST['fournisseur'])){
		$fournisseur_id=intval($_POST['fournisseur']);
	}
	if(empty($fournisseur_id)){
		$erreur_text="Formulaire incomplet!";
	}
}else{
	$erreur_text="Formulaire incomplet!";
}

if(empty($erreur_text)){
	
	$sql="SELECT fou_id, fou_nom, fou_code,fou_type,fou_montant_max,fou_depassement_autorise FROM fournisseurs WHERE fou_id=" . $fournisseur_id;
	$req = $Conn->query($sql);
	$d_fournisseur = $req->fetch(PDO::FETCH_ASSOC);
	
	if(!empty($d_fournisseur)){
		
		// ON VA RECUPERER LES INTERVENANTS
		$sql="SELECT fin_id,fin_nom,fin_prenom FROM Fournisseurs_Intervenants WHERE fin_fournisseur=" . $fournisseur_id . " AND NOT fin_archive ORDER BY fin_nom,fin_prenom;";
		$req = $Conn->query($sql);
		$d_fou_intervenants=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($d_fou_intervenants)){
			foreach($d_fou_intervenants as $int){
				$d_fournisseur["intervenants"][]=array(
					"id" => $int["fin_id"],
					"text" => $int["fin_nom"] . " " . $int["fin_prenom"]
				);
			}
		}
		
		// REGARDE SI LE FOUNISSEUR EST MULTI AGENCE
		$d_fournisseur["agence"]=-1;
	/*	Réunion 10/12/2018 JP MM -> fournisseur toujours multi 
		$d_fournisseur["agence"]=0;
		if(empty($acc_agence)){
			$sql="SELECT fso_agence FROM Fournisseurs_Societes WHERE fso_fournisseur=:fso_fournisseur AND fso_societe=:acc_societe;";
			$req = $Conn->prepare($sql);
			$req -> bindParam(":fso_fournisseur",$fournisseur_id);
			$req -> bindParam(":acc_societe",$acc_societe);
			$req ->execute();
			$d_fournisseur_soc=$req->fetchAll();
			if(!empty($d_fournisseur_soc)){
				if(count($d_fournisseur_soc)>1){
					$d_fournisseur["agence"]=-1;
				}elseif($d_fournisseur_soc[0]["fso_agence"]==0){
					$d_fournisseur["agence"]=-1;
				}else{
					$d_fournisseur["agence"]=$d_fournisseur_soc[0]["fso_agence"];
				}
			}
		}*/
		
		// ON RECUPERE LES CONTACTS		
		$sql="SELECT * FROM Fournisseurs_Contacts WHERE fco_fournisseur=:fco_fournisseur ORDER BY fco_nom,fco_prenom;";
		$req = $Conn->prepare($sql);
		$req -> bindParam(":fco_fournisseur",$fournisseur_id);
		$req ->execute();
		$d_results=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($d_results)){
			$d_fournisseur["contacts_select"]=array();
			$d_fournisseur["contacts"]=array();
			$d_fournisseur["contact_def"]=0;
			foreach($d_results as $r){
				$d_fournisseur["contacts_select"][]=array(
					"id" => $r["fco_id"],
					"text" => $r["fco_nom"] . " " . $r["fco_prenom"]
				);
				$d_fournisseur["contacts"][$r["fco_id"]]=array(
					"fco_titre" => $r["fco_titre"],
					"fco_nom" => $r["fco_nom"],
					"fco_prenom" => $r["fco_prenom"],
					"fco_tel" => $r["fco_tel"],
					"fco_portable" => $r["fco_portable"],
					"fco_fax" => $r["fco_fax"],
					"fco_mail" => $r["fco_mail"],
					"fco_fonction" => $r["fco_fonction"],
					"fco_fonction_nom" => $r["fco_fonction_nom"]
				);
				if($r["fco_defaut"]==1){
					$d_fournisseur["contact_def"]=$r["fco_id"];
				}
			}
		}
		
		// ON RECUPERE L'ADRESSE
		$sql="SELECT * FROM Fournisseurs_Adresses WHERE fad_fournisseur=:fad_fournisseur;";
		$req = $Conn->prepare($sql);
		$req -> bindParam(":fad_fournisseur",$fournisseur_id);
		$req ->execute();
		$d_results=$req->fetch(PDO::FETCH_ASSOC);
		if(!empty($d_results)){
			$d_fournisseur["adresses"]=$d_results;
		}
		
	}
	
}
if(empty($erreur_text)){
	echo json_encode($d_fournisseur);	
}else{
	echo($erreur_text);
}
?>
