<?php

include("../includes/connexion.php");
include('../includes/connexion_soc.php');

$erreur_txt="";

// retourne les ressources disponibles 

if(!empty($_POST)){
	
	$action=0;
	if(!empty($_POST["action"])){
		$action=intval($_POST["action"]);
	}
	
	$liste_dates=$_POST["dates"];
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	
	$agence=0;
	if(!empty($_POST["agence"])){
		$agence=intval($_POST["agence"]);
	}
	
	if(!empty($liste_dates) OR !empty($action)){
		
		// on convertie la liste de date en tableau
				
		$cases=explode(",", $liste_dates);
		
		$dates=array();
		$dates_req="";
		$int_req="";
		$cle=-1;
		if(!empty($cases)){
			foreach($cases as $c){
				if(!empty($c)){
					$cle++;
					$dates[$cle]=explode("_",$c);
					$dates_req.="(pda_date='" . $dates[$cle][1] . "' AND pda_demi=" . $dates[$cle][2] . ") OR ";
					$int_req.="(pda_intervenant=" . $dates[$cle][0] . " AND pda_date='" . $dates[$cle][1] . "' AND pda_demi=" . $dates[$cle][2] . ") OR ";
				}
			}
		}
		if(!empty($int_req)){
			$int_req=substr($int_req, 0, -4);
		}else{
			$erreur_txt="Impossible de récupérer les données (Err 002)";	
		}
		if(!empty($dates_req)){
			$dates_req=substr($dates_req, 0, -4);
		}else{
			$erreur_txt="Impossible de récupérer les données (Err 002)";	
		}
		/*echo("<pre>");
			print_r($dates);
		echo("</pre>");*/

		//echo($dates_req);
		
	}else{
		$erreur_txt="Impossible de récupérer les données (Err 001)";	
	}
	if(empty($erreur_txt)){
		
		// ON VERIFIE QUE LES DATES SONT TOUJOURS LIBRES
		
		$sql="SELECT pda_id FROM Plannings_Dates WHERE NOT pda_archive AND (" . $int_req . ")";
		$req=$ConnSoc->query($sql);
		$d_libre=$req->fetchAll();
		if(!empty($d_libre)){
			$retour=array(
				"erreur_num" => 1,
				"erreur_txt" => "Les dates selectionnées ne sont plus disponibles!" 
			);
			echo (json_encode($retour));
			die();
		}
		
		
		// DATES DISPO -> TRAITEMENT

		// format de retour
		$retour=array(
			"agence" => $agence,
			"salles" => array(),
			"vehicules" => array(),
			"commerciaux" => array(),
			"horaires" => array()
		);
		
		// premier formateur
		
		//var_dump($dates[0][0]);
		
		/*$sql="SELECT int_agence FROM Intervenants WHERE int_id=" . $dates[0][0] . ";";
		$req=$ConnSoc->query($sql);
		$d_intervenant=$req->fetch();
		if(!empty($d_intervenant)){
			
			$retour["agence"]=$d_intervenant["int_agence"];
		}else{
			$erreur_txt="Impossible de récupérer les données (Err 002)!";		
		}*/
		
		// les commerciaux
		
		$sql="SELECT com_id,com_label_1,com_label_2 FROM Commerciaux WHERE NOT com_archive";
		if(!empty($retour["agence"])){
			$sql.=" AND com_agence=" . $retour["agence"];
		}
		$sql.=" ORDER BY com_label_1,com_label_2;";
		$req=$ConnSoc->query($sql);
		$d_commerciaux=$req->fetchAll();
		if(!empty($d_commerciaux)){
			foreach($d_commerciaux as $d_com){
				$retour["commerciaux"][]=array(
					"id" => $d_com["com_id"],
					"text" => $d_com["com_label_1"] . " " . $d_com["com_label_2"]
				);
			}
		}
		
		// VEHICULES
		// attention vehicules et dates ne sont pas sur la même base
		$sql="SELECT DISTINCT pda_vehicule FROM Plannings_Dates WHERE (" . $dates_req . ");";
		$req=$ConnSoc->query($sql);
		$d_veh_non_dispo=$req->fetchAll();
		
		$veh_pas_dispo="";
		
		$sql="SELECT veh_id,veh_libelle FROM Vehicules WHERE NOT veh_archive AND veh_societe=". $acc_societe;
		if(!empty($retour["agence"])){
			$sql.=" AND (veh_agence=" . $retour["agence"] . " OR veh_agence=0 OR ISNULL(veh_agence))";
		}
		if(!empty($veh_pas_dispo)){		
			$sql.=" AND NOT veh_id IN (" . $veh_pas_dispo . ")";
		}
		$sql.=" ORDER BY veh_libelle;";
		$req=$Conn->query($sql);
		$d_vehicules=$req->fetchAll();
		if(!empty($d_vehicules)){
			foreach($d_vehicules as $d_veh){
				$retour["vehicules"][]=array(
					"id" => $d_veh["veh_id"],
					"text" => $d_veh["veh_libelle"]
				);
			}
		}
		
		// SALLES
		$sql="SELECT sal_id,sal_nom,pda_id FROM Salles LEFT OUTER JOIN Plannings_Dates ON (Salles.sal_id=Plannings_Dates.pda_salle AND (" . $dates_req . "))";
		$sql.=" WHERE ISNULL(pda_id)";
		if(!empty($retour["agence"])){
			$sql.=" AND (sal_agence=" .$retour["agence"] . " OR sal_agence=0)";
		}
		$sql.=" ORDER BY sal_nom;";
		$req=$ConnSoc->query($sql);
		$d_salles=$req->fetchAll();
		if(!empty($d_salles)){
			foreach($d_salles as $d_salle){
				$retour["salles"][]=array(
					"id" => $d_salle["sal_id"],
					"text" => $d_salle["sal_nom"]
				);
			}
		}
		
		// horraires par défauts
		
		if(!empty($retour["agence"])){
			$sql="SELECT age_h_deb_matin,age_h_fin_matin,age_h_deb_am,age_h_fin_am FROM Agences WHERE age_id=:acc_agence;";
			$req = $Conn->prepare($sql);
			$req->bindParam(":acc_agence",$retour["agence"]);
			$req->execute();
			$agence = $req->fetch();
			if(!empty($agence)){				
				$retour["horaires"]=array(
					0 => $agence["age_h_deb_matin"], 
					1 => $agence["age_h_fin_matin"], 
					2 => $agence["age_h_deb_am"], 
					3 => $agence["age_h_fin_am"]
				);
			}
		}else{
			$sql="SELECT soc_h_deb_matin,soc_h_fin_matin,soc_h_deb_am,soc_h_fin_am FROM Societes WHERE soc_id=:acc_societe;";
			$req = $Conn->prepare($sql);
			$req->bindParam(":acc_societe",$acc_societe);
			$req->execute();
			$agence = $req->fetch();
			if(!empty($agence)){				
				$retour["horaires"]=array(
					0 => $agence["soc_h_deb_matin"], 
					1 => $agence["soc_h_fin_matin"], 
					2 => $agence["soc_h_deb_am"], 
					3 => $agence["soc_h_fin_am"]
				);
			}
		}			
	}
}else{
	$erreur_txt="Impossible de récupérer les données";	
}

if(!empty($erreur_txt)){
	echo($erreur_txt);
}else{
	echo (json_encode($retour));
}
?>
