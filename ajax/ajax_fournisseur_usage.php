<?php 

include("../includes/connexion.php");
include_once('../modeles/mod_parametre.php');


	$erreur_txt="";
	
	// le fournisseur
	$fournisseur=0;
	if(!empty($_POST['fournisseur'])){
		$fournisseur=intval($_POST['fournisseur']);
	}

	$commande=0;
	if(!empty($_POST['commande'])){
		$commande=intval($_POST['commande']);
	}
	
	if(empty($fournisseur)){
		$erreur_txt="Impossible de récupérer le taux d'usage!";
	}
	
	
	if(empty($erreur_txt)){

		if(!empty($commande)){
			$req = $Conn->query("SELECT com_date FROM commandes WHERE com_id = " . $commande . ";");
			$d_commande = $req->fetch();
			if(empty($d_commande)){
				$erreur_txt="Impossible de récupérer le taux d'usage!";
			}else{
				$DT_periode=date_create_from_format('Y-m-d',$d_commande["com_date"]);
				if(!is_bool($DT_periode)){
					$mois_actuel=intval($DT_periode->format("m"));
					$year=intval($DT_periode->format("Y"));
					if($mois_actuel < 4){
						$year=$year-1;
					}
				}else{
					$erreur_txt="Impossible de récupérer le taux d'usage!";
				}
				
			}
		}else{
			$mois_actuel = date("n");
			if($mois_actuel < 4){
				$year = date("Y",strtotime("-1 year"));
			}else{
				$year = date("Y");
			}
		}
	}
	if(empty($erreur_txt)){
		
		$date_deb = "01/04/" . $year;
		$date_deb_sql = convert_date_sql($date_deb);

		$date_fin = "31/03/" . intval($year+1);
		$date_fin_sql = convert_date_sql($date_fin);
		
		$req = $Conn->prepare("SELECT fou_montant_max, fou_depassement_autorise FROM fournisseurs WHERE fou_id = " . $fournisseur);
		$req->execute();
		$f = $req->fetch();
		if(!empty($f )){
			
			if($f['fou_montant_max'] > 0){
				
				// IL Y A UN MONTANT MAX
				
				$sql="SELECT SUM(com_ht) FROM commandes WHERE com_fournisseur = " . $fournisseur . " AND com_date >= " . $date_deb_sql . " AND com_date<=" . $date_fin_sql;
				$sql.=";";
				$req = $Conn->prepare($sql);
				$req->execute();
				$somme_ht = $req->fetch();
				
				if($f['fou_montant_max'] >= $somme_ht["SUM(com_ht)"]){
					
					$pourcentage = pourcentage($somme_ht["SUM(com_ht)"], $f['fou_montant_max']);					
					$retour = array(
						"existe" => 1,
						"pourcentage" => $pourcentage,
						"depassement" => 0
					);
				}else{
					$pourcentage = $somme_ht["SUM(com_ht)"] - $f['fou_montant_max'];
					if($f["fou_depassement_autorise"] == 1){
						
						$retour = array(
							"existe" => 1,
							"pourcentage" => $pourcentage,
							"depassement" => 1,
							"depassement_autorise" => 1
						);
					}else{
						$retour = array(
							"existe" => 1,
							"pourcentage" => $pourcentage,
							"depassement" => 1,
							"depassement_autorise" => 0
						);
					}
				}
			}else{
				$retour = array(
					"existe" => 0,
				);
			}
		}else{
			$erreur_txt="Impossible de récupérer le taux d'usage!";
		}
	}

	if(!empty($erreur_txt)){
		echo($erreur_txt);
	}else{
		echo json_encode($retour);
	};

