<?php
include("../includes/connexion.php");
include('../includes/connexion_soc.php');

// AJOUTE LES DATES liste_case à la formation action

// format de retour
$case_maj=array(
	"cases" =>array(
		"type" =>"1",
		"ref_1" => 0,
		"texte" => "",
		"style" => "",
		"survol" => "",
		"date" => array(),
		"erreur" => 0
	),
	"erreur_type" => 0,
	"erreur_code" => 0,
	"erreur_text" => ""

);

$action=0;
$liste_case="";
$warning_text="";

if(isset($_POST['action'])&&isset($_POST['liste_case'])){

	if(!empty($_POST['action'])){
		$action=intval($_POST['action']);
	}

	if(!empty($_POST['liste_case'])){
		$liste_case=$_POST['liste_case'];
	}
}

if(empty($action) OR empty($liste_case)){
	$case_maj["erreur_type"]=1;
	$case_maj["erreur_text"]="Paramètres absents";
}

/******************************
	DONNEE AVANT ENREGISTREMENT
*******************************/

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}


// SUR L'ACTION DE REFERENCE

if(empty($case_maj["erreur_type"])){

	$sql="SELECT act_date_deb,act_date_h_def_1,act_date_h_def_2,act_vehicule,act_salle,act_verrou_admin FROM Actions WHERE act_id=:action;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action",$action);
	$req->execute();
	$d_action= $req->fetch();
	if(empty($d_action)){
		$case_maj["erreur_type"]=1;
		$case_maj["erreur_text"]="L'action de référence n'a pas été trouvée";
	}
}

	// INFO DATE
if(empty($case_maj["erreur_type"])){

	$sql="SELECT * FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=:action ORDER BY pda_date;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action",$action);
	$req->execute();
	$info_case= $req->fetchAll();
	if(!empty($info_case[0])){
		$req = $Conn->prepare("SELECT veh_couleur FROM vehicules WHERE veh_id=" . $info_case[0]["pda_vehicule"]);
		$req->execute();
		$veh_couleur = $req->fetch();
		$case_maj["cases"]["type"]=1;
		$case_maj["cases"]["ref_1"]=$action;
		$case_maj["cases"]["texte"]=$info_case[0]["pda_texte"];
		if($info_case[0]["pda_type"]<3){
			if(empty($info_case[0]["pda_confirme"])){
			$info_case[0]["pda_style_txt"]="#FFF";
			}else{
				$info_case[0]["pda_style_txt"]="#333";
			}
		}else{
			if(!empty($info_case[0]["pda_style_txt"])){
				$info_case[0]["pda_style_txt"] = $info_case[0]["pda_style_txt"];
			}else{
				$info_case[0]["pda_style_txt"]="#333";
			}
		}
		if(!empty($d_action['act_verrou_admin'])){
			$case_maj["cases"]["style"]="background-color:#fff;color:#F00;";
		}elseif(!empty($veh_couleur['veh_couleur'])){
			$case_maj["cases"]["style"]="background-color:" . $veh_couleur['veh_couleur'] . ";color:" . $info_case[0]["pda_style_txt"] . ";";
		}else{
			$case_maj["cases"]["style"]="background-color:" . $info_case[0]["pda_style_bg"] . ";color:" . $info_case[0]["pda_style_txt"] . ";";
		}

		$case_maj["cases"]["survol"]=$info_case[0]["pda_survol"];

	}else{
		$case_maj["erreur_type"]=1;
		$case_maj["erreur_text"]="L'action de référence n'a pas été trouvée";
	}
}

if(empty($case_maj["erreur_type"])){

	// HORAIRE PAR DEFAUT POUR LES NOUVELLES DATES

	$d_session=array(
		"1" => array(
			"0" => array(
				"ase_h_deb" => "",
				"ase_h_fin" => ""
			)
		),
		"2" => array(
			"0" => array(
				"ase_h_deb" => "",
				"ase_h_fin" => ""
			)
		)
	);

	$sql="SELECT ase_h_deb,ase_h_fin FROM Actions_Sessions WHERE ase_date=:date_def;";
	$req=$ConnSoc->prepare($sql);

	if(!empty($d_action["act_date_h_def_1"])){
		$req->bindParam(":date_def",$d_action["act_date_h_def_1"]);
		$req->execute();
		$d_session_matin=$req->fetchAll();
		$d_session["1"]=$d_session_matin;
	}
	if(!empty($d_action["act_date_h_def_2"])){
		$req->bindParam(":date_def",$d_action["act_date_h_def_2"]);
		$req->execute();
		$d_session_ap=$req->fetchAll();
		$d_session["2"]=$d_session_ap;
	}

	// LES CLIENTS QUI PARTICIPE A LA FORMATION

	$sql="SELECT acl_id,acl_pro_inter,acl_ca_unit,acl_ca,acl_facture_ht,acl_ca_gfc,acl_facture_gfc_ht,acl_facturation,acl_facture,acl_facture_gfc_ht,
	COUNT(acd_date) as nb_demi_j
	FROM Actions_Clients LEFT JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client) 
	WHERE acl_action=:action AND NOT acl_archive GROUP BY acl_id;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action",$_POST['action']);
	$req->execute();
	$action_client= $req->fetchAll();

}

/******************************
	PREPARTION DES REQUETES
*******************************/
if(empty($case_maj["erreur_type"])){

	// controle que la date ajouté est bien libre

	$req_select=$ConnSoc->prepare("SELECT pda_id,pda_type FROM Plannings_Dates WHERE pda_intervenant=:intervenant AND pda_date=:date AND pda_demi=:demi AND NOT pda_archive;");

	// ajout date

	$sql_date="INSERT INTO Plannings_Dates (pda_intervenant,pda_date,pda_demi,pda_type,pda_ref_1,pda_texte,pda_style_bg,pda_style_txt,pda_survol,pda_vehicule,pda_salle,pda_confirme)";
	$sql_date.=" VALUES (:intervenant, :date, :demi, 1, :action, :texte, :style_bg , :style_txt, :survol, :vehicule, :salle, :confirme);";
	$req_date=$ConnSoc->prepare($sql_date);
	$req_date->bindParam(":action",$action);
	$req_date->bindParam(":texte",$case_maj["cases"]["texte"]);
	$req_date->bindParam(":style_bg",$info_case[0]["pda_style_bg"]);
	$req_date->bindParam(":style_txt",$info_case[0]["pda_style_txt"]);
	$req_date->bindParam(":survol",$case_maj["cases"]["survol"]);
	$req_date->bindParam(":vehicule",$d_action["act_vehicule"]);
	$req_date->bindParam(":salle",$d_action["act_salle"]);
	$req_date->bindParam(":confirme",$info_case[0]["pda_confirme"]);

	// ajout session

	$sql_session="INSERT INTO Actions_Sessions (ase_date,ase_h_deb,ase_h_fin,ase_action)";
	$sql_session.=" VALUES (:date_id, :h_deb, :h_fin, :action);";
	$req_session=$ConnSoc->prepare($sql_session);
	$req_session->bindParam(":action",$action);

	// affectation des dates aux clients

	$sql_act_cli="INSERT INTO Actions_Clients_Dates (acd_action_client,acd_date)";
	$sql_act_cli.=" VALUES (:action_client, :date_id);";
	$req_act_cli=$ConnSoc->prepare($sql_act_cli);

	// MAJ des INFOS CLIENTS

	$sql_maj_act_cli="UPDATE Actions_Clients SET acl_ca=:ca,acl_qte_pm=:qte_pm,acl_facture=:facture,acl_ca_gfc=:ca_gfc
	,acl_facture_gfc=:facture_gfc
	WHERE acl_id=:action_client;";
	$req_maj_act_cli=$ConnSoc->prepare($sql_maj_act_cli);

}

/******************************
	ENREGISTREMENT
*******************************/
if(empty($case_maj["erreur_type"])){


	$case=explode (",",$liste_case);
	$nb_jour=0;
	foreach($case as $c){

		if(empty($case_maj["erreur_type"])){

			if(!empty($c)){

				$donnee_case=explode ("_",$c);
				$intervenant=$donnee_case[0];
				$date=$donnee_case[1];
				$demi=$donnee_case[2];

				// ON S'ASSURE QUE LA DATE EST LIBRE

				$req_select->bindParam(":intervenant",$intervenant);
				$req_select->bindParam(":date",$date);
				$req_select->bindParam(":demi",$demi);
				$req_select->execute();
				$case_select = $req_select->fetch();
				if(empty($case_select)){

					$date_id=0;

					// la case est libre -> on ajoute les données
					$req_date->bindParam(":intervenant",$intervenant);
					$req_date->bindParam(":date",$date);
					$req_date->bindParam(":demi",$demi);
					try{
						$req_date->execute();
						$date_id=$ConnSoc->lastInsertId();
					}Catch(Exception $e){
						$case_maj["erreur_type"]=1;
						$case_maj["erreur_code"]=2;
						$case_maj["erreur_text"]="Certaine dates selectionnées n'ont pas pu être enregistrées!";
					}

					if(empty($case_maj["erreur_type"])){

						$nb_jour++;

						// on ajoute la date au tableau de retour

						array_push($case_maj["cases"]["date"],$c);

						// on affecte les clients a la date

						foreach($action_client as $num_client => $act_cli){

							if(!isset($action_client[$num_client]["date_add"])){
								$action_client[$num_client]["date_add"]=0;
							}

							$req_act_cli->bindParam(":action_client",$act_cli["acl_id"]);
							$req_act_cli->bindParam(":date_id",$date_id);
							try{
								$req_act_cli->execute();
								$action_client[$num_client]["date_add"]++;
							}Catch(Exception $e){

							}

						}

						// premier jour ajoute
						if($date<$d_action["act_date_deb"]){
							$sql="UPDATE Actions SET act_date_deb = :date_deb, act_intervenant=:intervenant WHERE act_id=:action;";
							$req=$ConnSoc->prepare($sql);
							$req->bindParam(":date_deb",$date);
							$req->bindParam(":intervenant",$intervenant);
							$req->bindParam(":action",$action);
							try{
								$req->execute();
							}Catch(Exception $e){
								$warning_text="La date de début de formation n'a pas été mise à jour!";
							}
						}

						// Enregistrement des horraires

						if(empty($warning_text)){
							foreach($d_session[$demi] as $sm){
								$req_session->bindParam(":h_deb",$sm["ase_h_deb"]);
								$req_session->bindParam(":h_fin",$sm["ase_h_fin"]);
								$req_session->bindParam(":date_id",$date_id);
								try{
									$req_session->execute();
								}Catch(Exception $e){
									$warning_text="Certains horaires n'ont pas été enregistrés! " . $e->getMessage();
								}
							}

						}

						// BLOCAGE ADMINISTRATIF

						$sql="SELECT int_type,int_ref_1,int_ref_3 FROM Intervenants WHERE int_id=:intervenant AND int_type IN (2,3);";
						$req=$ConnSoc->prepare($sql);
						$req->bindParam(":intervenant",$intervenant);
						$req->execute();
						$d_intervenant=$req->fetch();
						if(!empty($d_intervenant)){
							

							// la date est lié a un ST 
							// l'ajout de date génère forcément un blocage administratif
							$act_verrou_admin=1;
						
							if($d_intervenant["int_type"]==3){
								$fournisseur=$d_intervenant["int_ref_1"];
							}else{
								$fournisseur=$d_intervenant["int_ref_3"];
							}

					
							if (!empty($fournisseur) ) {

								
								// on annule un potentiel BC clôturé										
								$sql_up_commande="UPDATE Commandes SET com_annule=1 
								WHERE com_fournisseur=:fournisseur AND com_action=:action AND com_societe=:societe
								AND com_etat>0 AND NOT com_annule;";
								$req_up_commande=$Conn->prepare($sql_up_commande);
								$req_up_commande->bindParam(":fournisseur",$fournisseur);
								$req_up_commande->bindParam(":societe",$acc_societe);
								$req_up_commande->bindParam(":action",$action);
								try{
									$req_up_commande->execute();
								}Catch(Exception $e){
									$warning_text.="Le BC du fournisseur n'a pas été annulé. " . $e->getMessage();
								}

								// Annulation d'un BC = recalcul de act_charge

								$sql_get_bc="SELECT SUM(com_ht) AS act_charge FROM Commandes WHERE com_action=:action AND com_societe=:societe AND com_etat>1 AND NOT com_annule;";
								$req_get_bc=$Conn->prepare($sql_get_bc);
								$req_get_bc->bindParam(":societe",$acc_societe);
								$req_get_bc->bindParam(":action",$action);
								$req_get_bc->execute();
								$d_bc=$req_get_bc->fetch();
								if(!empty($d_bc)){
									if(!empty($d_bc["act_charge"])){
										$act_charge=$d_bc["act_charge"];
									}
								}

							}else{
								$warning_text.="Certains intervenant ne sont pas lié à un fournisseur. L'action fait l'objet d'un blocage administratif";
							}
						}

						if(!empty($warning_text)){
							$case_maj["erreur_type"]=2;
							$case_maj["erreur_code"]=0;
							$case_maj["erreur_text"]=$warning_text;
						}
					}



				}else{
					$case_maj["erreur_type"]=1;
					$case_maj["erreur_code"]=2;
					$case_maj["erreur_text"]="Certaine dates selectionnées n'étaient plus disponible!";
				}
			}
		}else{
			break;
		}
	}

	// ON MAJ LE CA suite a l'ajoute de date

	foreach($action_client as $act_cli){

		if($act_cli["acl_pro_inter"]==0 AND isset($act_cli["date_add"])){

			$acl_facture_ht=$act_cli["acl_facture_ht"];

			$acl_facturation=$act_cli["acl_facturation"];


			$acl_facture_gfc_ht=$act_cli["acl_facture_gfc_ht"];



			$ca_demi_jour=0;
			if($act_cli["nb_demi_j"]!=0){
				$ca_demi_jour=$act_cli["acl_ca"]/$act_cli["nb_demi_j"];
			}
			$acl_qte_pm=$act_cli["nb_demi_j"]+$act_cli["date_add"];

			$acl_ca=$ca_demi_jour*$acl_qte_pm;

			if($acl_ca!=$acl_facture_ht){
				$acl_facture=0;
			}else{
				$acl_facture=1;
			}

			$acl_ca_gfc=0;
			$acl_facture_gfc=0;
			if($acc_societe!=4 AND $acl_facturation==1){
				$acl_ca_gfc=$acl_ca/0.88;
				if($acl_ca_gfc==$acl_facture_gfc_ht){
					$acl_facture_gfc=1;
				}
			}

			$req_maj_act_cli->bindParam(":ca",$acl_ca);
			$req_maj_act_cli->bindParam(":qte_pm",$acl_qte_pm);
			$req_maj_act_cli->bindParam(":facture",$acl_facture);
			$req_maj_act_cli->bindParam(":ca_gfc",$acl_ca_gfc);
			$req_maj_act_cli->bindParam(":facture_gfc",$acl_facture_gfc);
			$req_maj_act_cli->bindParam(":action_client",$act_cli["acl_id"]);
			$req_maj_act_cli->execute();

		}
	}

	// MAJ DE L'ACTION

	if(isset($act_verrou_admin) AND empty($d_action["act_verrou_admin"]) ){

		$case_maj["cases"]["style"]="background-color:#fff;color:#F00;";
		$case_maj["cases"]["up_style_act"]=1;

		$sql_up_action="UPDATE Actions SET act_verrou_admin=1, act_verrou_bc=1, act_verrou_marge=0";
		if(isset($act_charge)){
			$sql_up_action.=",act_charge=:act_charge";
		}
		$sql_up_action.=" WHERE act_id=:action;";
		$sql_up_action=$ConnSoc->prepare($sql_up_action);
		$sql_up_action->bindParam(":action",$action);
		if(isset($act_charge)){
			$sql_up_action->bindParam(":act_charge",$act_charge);
		}
		try{
			$sql_up_action->execute();
		}Catch(Exception $e){
			$case_maj["erreur_type"]=2;
			$case_maj["erreur_code"]=0;
			$case_maj["erreur_text"]=$case_maj["erreur_text"] . "Le blocage administratif n'a pas été actualisé sur l'action!<br/>";
		}

		$sql_up_action_date="UPDATE Plannings_Dates SET pda_alert=1 WHERE pda_type=1 AND pda_ref_1=:action;";
		$req_up_action_date=$ConnSoc->prepare($sql_up_action_date);
		$req_up_action_date->bindParam(":action",$action);
		try{
			$req_up_action_date->execute();
		}Catch(Exception $e){
			$case_maj["erreur_type"]=2;
			$case_maj["erreur_code"]=0;
			$case_maj["erreur_text"]=$case_maj["erreur_text"] . "Le blocage administratif n'a pas été actualisé sur le planning!<br/>" . $e->getMessage();
		}

	}

}
echo json_encode($case_maj);
?>
