<?php
 // PERMET DE PRE-SELECTIONNER DES CLIENTS D'APRES UNE SAISIE UTILISATEUR
 
session_start();
include('../includes/connexion.php');
include('../includes/connexion_soc.php');

$erreur="";

/* UTILISATION 

- devis_voir

*/

// parametre

$erreur_text="";

$param="";
if($_GET['q']){
	$param=$_GET['q'] . "%";
}
$client=0;
if(isset($_GET['client'])){
	if($_GET['client']){
		$client=intval($_GET['client']);
	}
}
$client_actif=0;
if(isset($_GET['client_actif'])){
	if($_GET['client_actif']){
		$client_actif=intval($_GET['client_actif']);
	}
}

if(empty($erreur_text)){

	// REQUETE
	$sql="SELECT sta_id,sta_nom,sta_prenom,DATE_FORMAT(sta_naissance,'%d/%m/%Y') AS sta_naissance FROM Stagiaires LEFT JOIN Stagiaires_Clients ON (Stagiaires.sta_id=Stagiaires_Clients.scl_stagiaire) WHERE sta_nom LIKE '" . $param . "'";
	if($client>0){
		if($client_actif>0){
			$sql.=" AND sta_client=" . $client;	
		}else{
			$sql.=" AND scl_client=" . $client;
		}
	}
	$sql.=" ORDER BY sta_nom,sta_prenom";
	$req = $Conn->query($sql);
	$stagiaires = $req->fetchAll();
	if(!empty($stagiaires)){
		foreach($stagiaires as $c){
			
			$retour[] = array("id"=>$c['sta_id'], "nom"=>$c['sta_nom'] . " " . $c['sta_prenom'] . " (" . $c['sta_naissance'] . ")");
		}
	}
}

// ENVOIE DU RESULTAT
if(!empty($retour)){
	echo json_encode($retour);	
}
?>
