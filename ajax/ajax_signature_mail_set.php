	
<?php
 // PERMET DE METTRE A JOUR LES DONNEES UTI AVANT DE GENERE LA SIGNATURE
 
session_start();
include('../includes/connexion.php');

// PARAMETRE
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
if(empty($acc_utilisateur)){
	$erreur_text="Impossible de générer la signature!";
}

if(!empty($_POST)){
	
	$uti_fonction="";
	if(!empty($_POST["uti_fonction"])){
		$uti_fonction=$_POST["uti_fonction"];
	}
	
	$uti_tel="";
	if(!empty($_POST["uti_tel"])){
		$uti_tel=$_POST["uti_tel"];
	}
	
	$uti_mobile="";
	if(!empty($_POST["uti_mobile"])){
		$uti_mobile=$_POST["uti_mobile"];
	}
	
	$uti_fax="";
	if(!empty($_POST["uti_fax"])){
		$uti_fax=$_POST["uti_fax"];
	}
	
	$uti_mail="";
	if(!empty($_POST["uti_mail"])){
		$uti_mail=$_POST["uti_mail"];
	}
	
	$logo_nf=0;
	if(!empty($_POST["logo_nf"])){
		$logo_nf=1;
	}
	
	$logo_datadock=0;
	if(!empty($_POST["logo_datadock"])){
		$logo_datadock=1;
	}
	
	// MAJ DE L'UTILISATEUR
	
	$sql="UPDATE Utilisateurs SET
	uti_fonction=:uti_fonction,
	uti_tel=:uti_tel,
	uti_mobile=:uti_mobile,
	uti_fax=:uti_fax,
	uti_mail=:uti_mail
	WHERE uti_id=:uti_id";
	$req=$Conn->prepare($sql);
	$req->bindParam(":uti_fonction",$uti_fonction);
	$req->bindParam(":uti_tel",$uti_tel);
	$req->bindParam(":uti_mobile",$uti_mobile);
	$req->bindParam(":uti_fax",$uti_fax);
	$req->bindParam(":uti_mail",$uti_mail);
	$req->bindParam(":uti_id",$acc_utilisateur);
	try{
		$req->execute();
	}Catch(Exception $e){
		$erreur_text=$e->getMessage();
	}

	// ENREGISTREMENT OK ON GENERE LA SIGNATURE
	if(empty($erreur_text)){
		
		$d_signature=array(
			"uti_fonction" => $uti_fonction,
			"uti_tel" => $uti_tel,
			"uti_mobile" => $uti_mobile,
			"uti_fax" => $uti_fax,
			"uti_mail" => $uti_mail,
			"uti_nom" => "",
			"uti_prenom" => "",
			"logo_1" => "",
			"logo_2" => "",
			"cnaps" => "",
			"site" => "",
			"soc_nom" => "",
			"logo_nf" => $logo_nf,
			"logo_datadock" => $logo_datadock
		);
		
		$sql="SELECT uti_nom,uti_prenom,uti_societe,uti_agence FROM Utilisateurs WHERE uti_id=:utilisateur;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":utilisateur",$acc_utilisateur);
		$req->execute();
		$d_utilisateur=$req->fetch();
		if(!empty($d_utilisateur)){			
			$d_signature["uti_nom"]=$d_utilisateur["uti_nom"];
			$d_signature["uti_prenom"]=$d_utilisateur["uti_prenom"];
		}

		// DONNE AGENCE
		if(!empty($d_utilisateur["uti_agence"])){
			
			$sql="SELECT age_logo_1,age_logo_2,age_site FROM Agences WHERE age_id=:age_id;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":age_id",$d_utilisateur["uti_agence"]);
			$req->execute();
			$d_agence=$req->fetch();
			if(!empty($d_agence)){	
				
				if(!empty($d_agence["age_logo_1"])){
					$d_signature["logo_1"]=$d_agence["age_logo_1"];
				}
				if(!empty($d_agence["age_logo_2"])){
					$d_signature["logo_2"]=$d_agence["age_logo_2"];
				}
				if(!empty($d_agence["age_site"])){
					$d_signature["site"]=$d_agence["age_site"];
				}
			}
			
		}
		
		// DONNEE SOCIETE
		$sql="SELECT soc_logo_1,soc_logo_2,soc_cnaps,soc_site,soc_nom FROM Societes WHERE soc_id=:soc_id;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":soc_id",$d_utilisateur["uti_societe"]);
		$req->execute();
		$d_societe=$req->fetch();
		if(!empty($d_societe)){	
	
			$d_signature["soc_nom"]=$d_societe["soc_nom"];

			if(!empty($d_societe["soc_logo_1"])){
				$d_signature["logo_1"]=$d_societe["soc_logo_1"];
			}
			if(!empty($d_societe["soc_logo_2"])){
				$d_signature["logo_2"]=$d_societe["soc_logo_2"];
			}
			if(!empty($d_societe["soc_cnaps"])){
				$d_signature["cnaps"]=$d_societe["soc_cnaps"];
			}
			if(!empty($d_societe["soc_site"])){
				$d_signature["site"]=$d_societe["soc_site"];
			}
		}
		
		if(!empty($d_signature["site"])){
			$d_signature["site"]=str_replace("http://","",$d_signature["site"]);
			$d_signature["site"]=str_replace("https://","",$d_signature["site"]);
			
		}
		
	}
			
}else{
	$erreur_text="Impossible de générer la signature!";
}
if(!empty($erreur_text)){
	echo($erreur_text);
}else{
	echo(json_encode($d_signature));
};
?>
