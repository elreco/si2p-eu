<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../connexion_soc.php';
include '../modeles/mod_get_action_sessions.php';

 /* RETOURNE LA LISTE DES DATES ET HORRAIRES D'UNE FORMATION

 planning
retour => tableau JSON

Appel :
planning.php

auteur : FG 06/02/2017

*/
 
 
$erreur=0;
if(isset($_POST)){
	 
	$action="";
	if(!empty($_POST["action"])){
		$action=intval($_POST["action"]); 
	}else{
		$erreur=1; 
	}
	
	if($erreur==0){
		
		
		
		// info sur l'action
		$sql="SELECT act_multi_session FROM Actions WHERE act_id=" . $action . ";";
		$req=$ConnSoc->query($sql);
		$result=$req->fetch();
		if(!empty($result)){
			$tab_retour=array(
				"multi_session" => $result["act_multi_session"]
			);
		}
		
		$tab_session=get_action_sessions($action,0);		
		if(!empty($tab_session)){
			$horaires=array();
			foreach($tab_session as $ts){
				
				$date = date_create_from_format('Y-m-d',$ts["pda_date"]);

				
				$horaires[]=array(				
					"int_label_1" => $ts["int_label_1"],
					"pda_id" => $ts["pda_id"],
					"pda_date" => $date->format("d/m/Y"),
					"pda_demi" => $ts["pda_demi"],
					"pda_test" => $ts["pda_test"],
					"ase_id" => $ts["ase_id"],
					"ase_h_deb" => $ts["ase_h_deb"],
					"ase_h_fin" => $ts["ase_h_fin"]
				);     
			}
			$tab_retour["horaires"]=$horaires;
		}
	}
}else{
	$erreur=10;
}
if(isset($tab_retour)){
	if(!empty($tab_retour)){
		echo json_encode($tab_retour);
	}
}
 
?>