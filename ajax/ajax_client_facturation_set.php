<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_fct.php';

include "../modeles/mod_get_groupe_filiales.php";

	// AJOUT OU SUPPRESION DE LA FACTURATION GROUPE SUR UN GROUPE 
	 
	$erreur_txt="";
	if(isset($_POST)){
		 
		$client_id=0;
		if(!empty($_POST["client"])){
			$client_id=intval($_POST["client"]); 
		}
		$ope=0;
		if(!empty($_POST["ope"])){
			$ope=intval($_POST["ope"]); 
		}

		if(empty($client_id)){
			$erreur_txt="Formulaire incomplet!";
			echo($erreur_txt);
			die();	
		}
	}else{
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();
	}

	if(!$_SESSION["acces"]["acc_droits"][8]){
		$erreur_txt="Accès refusé";
		echo($erreur_txt);
		die();
	}


	// TRAITEMENT DES REQUIRED FORM
	
	$sql="SELECT cli_groupe,cli_filiale_de,cli_categorie,cli_code FROM Clients WHERE cli_id=:client AND cli_groupe=1 AND cli_categorie=2 AND cli_sous_categorie IN (1,3);";
	$req=$Conn->prepare($sql);
	$req->bindParam(":client",$client_id);
	$req->execute();
	$d_client=$req->fetch();
	if(empty($d_client)){
		$erreur_txt="Impossible de charger les informations du client";
		echo($erreur_txt);
		die();
	}else{
		
		if($ope==1){
			$cli_fac_groupe=$client_id;
		}else{
			$cli_fac_groupe=0;
		}
		
		if($d_client["cli_filiale_de"]==0){
			// c'est une maison mère
			
			// on update les filiales
			
			$sql="UPDATE Clients SET cli_fac_groupe=:cli_fac_groupe WHERE (cli_id=:client OR cli_filiale_de=:client);";
			$req=$Conn->prepare($sql);
			$req->bindParam(":client",$client_id);
			$req->bindParam(":cli_fac_groupe",$cli_fac_groupe);
			try{
				$req->execute();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
				echo($erreur_txt);
				die();
			}
			
			// on met en jour en base N
			
			$sql="SELECT soc_id FROM Societes WHERE NOT soc_archive;";
			$req=$Conn->query($sql);
			$d_societes=$req->fetchAll();
			if(!empty($d_societes)){
				
				foreach($d_societes as $soc){
					
					$ConnFct=connexion_fct($soc["soc_id"]);
					
					$sql="UPDATE Clients SET cli_fac_groupe=:cli_fac_groupe WHERE (cli_id=:client OR cli_filiale_de=:client);";
					$req=$ConnFct->prepare($sql);
					$req->bindParam(":client",$client_id);
					$req->bindParam(":cli_fac_groupe",$cli_fac_groupe);
					try{
						$req->execute();
					}Catch(Exception $e){
						$erreur_txt=$e->getMessage();
						echo($erreur_txt);
						die();
					}
				}
			}
			
			$resultat=array(
				"resultat" => true,
				"code" => $d_client["cli_code"],
				"filiales" => null
			);
			
		}else{
			
			// on recherche les filiales pouvant etre facture
			$filiales=get_groupe_filiales($client_id);
			$tab_fil=array_column ($filiales,"cli_id");
			$liste_fil=implode($tab_fil,",");
			if(!empty($liste_fil)){
				
				$liste_fil.="," . $client_id;
				
				$sql="UPDATE Clients SET cli_fac_groupe=:cli_fac_groupe WHERE cli_id IN (" . $liste_fil . ");";
				$req=$Conn->prepare($sql);
				$req->bindParam(":cli_fac_groupe",$cli_fac_groupe);
				try{
					$req->execute();
				}Catch(Exception $e){
					$erreur_txt="ERREUR FIL " . $e->getMessage();
					echo($erreur_txt);
					die();
				}
				
				$sql="SELECT soc_id FROM Societes WHERE NOT soc_archive;";
				$req=$Conn->query($sql);
				$d_societes=$req->fetchAll();
				if(!empty($d_societes)){
					
					foreach($d_societes as $soc){
						
						$ConnFct=connexion_fct($soc["soc_id"]);
						
						$sql="UPDATE Clients SET cli_fac_groupe=:cli_fac_groupe WHERE cli_id IN (" . $liste_fil . ");";
						$req=$ConnFct->prepare($sql);
						$req->bindParam(":cli_fac_groupe",$cli_fac_groupe);
						try{
							$req->execute();
						}Catch(Exception $e){
							$erreur_txt=$e->getMessage();
							echo($erreur_txt);
							die();
						}
					}
				}
				$tab_fil[]=$client_id;
				$resultat=array(
					"resultat" => true,
					"code" => $d_client["cli_code"],
					"filiales" => $tab_fil
				);
					
				
				
			}else{
				
				
				
				// pas de filiale on ne groupe pas sur le client
				
				if($ope==1){
					echo("Vous ne pouvez pas grouper le facturation sur une fiche sans filiale");
					die();
				}else{
				
					$sql="UPDATE Clients SET cli_fac_groupe=0 WHERE cli_id=:client;";
					$req=$Conn->prepare($sql);
					$req->bindParam(":client",$client_id);
					try{
						$req->execute();
					}Catch(Exception $e){
						$erreur_txt=$e->getMessage();
						echo($erreur_txt);
						die();
					}
					
					$sql="SELECT soc_id FROM Societes WHERE NOT soc_archive;";
					$req=$Conn->query($sql);
					$d_societes=$req->fetchAll();
					if(!empty($d_societes)){
						
						foreach($d_societes as $soc){
							
							$ConnFct=connexion_fct($soc["soc_id"]);
							
							$sql="UPDATE Clients SET cli_fac_groupe=0 WHERE cli_id=:client;";
							$req=$ConnFct->prepare($sql);
							$req->bindParam(":client",$client_id);
							try{
								$req->execute();
							}Catch(Exception $e){
								$erreur_txt=$e->getMessage();
								echo($erreur_txt);
								die();
							}
						}
					}
					
					$resultat=array(
						"resultat" => true,
						"code" => $d_client["cli_code"],
						"filiales" => array(
							$client_id
						)
					);
				}
				
			}	
			
		}
	}
	echo(json_encode($resultat));
	die();
?>