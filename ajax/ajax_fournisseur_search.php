<?php

include("../includes/connexion.php");
if(isset($_POST)){
	// variable connexion
	global $Conn;

	// initialisation des variables
	$code=$_POST['code'];
	$nom=rawurldecode($_POST['nom']); // decode la chaine encodée en javascript
	$siren=substr($_POST['siret'], 0, 11);
	$siret=substr($_POST['siret'], 12, 6);

	// recherche sur code et nom
	$sql="SELECT * FROM fournisseurs";

	
	$mil="";
	if($siren!=""){
		$mil.= " AND (fou_siren = :siren";
		if($siret != ""){
			$mil.= " AND fou_siret = :siret"; 	
		}
	};
	$mil.= ")";

	

	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$req = $Conn->prepare($sql);

	/////////////// bind params

	if($siren!=""){
		$req->bindParam("siren",$siren);
	};

	if($siret!=""){
		$req->bindParam("siret",$siret);
	};
	///////////////
	$req->execute();
	$fournisseurs = $req->fetch();


	// recherche sur code et nom
	$sql="SELECT * FROM fournisseurs WHERE fou_code LIKE :code";

	
	/*$mil="";
	if($code!=""){
		$mil.="  OR "; 
	};

	

	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}*/
	$req = $Conn->prepare($sql);

	/////////////// bind params
	if($code!=""){
		$req->bindParam("code",$code);
	};
	/*if($nom!=""){
		$req->bindParam("nom",$nom);
	};*/
	///////////////
	$req->execute();
	$fournisseurs2 = $req->fetch();


	if(empty($fournisseurs) && empty($fournisseurs2)){
		echo "OK";
	}else if(!empty($fournisseurs) && empty($fournisseurs2)){
		echo $fournisseurs['fou_id'];
	}elseif(empty($fournisseurs) && !empty($fournisseurs2)){
		echo "CODE";
	}elseif(!empty($fournisseurs) && !empty($fournisseurs2)){
		echo $fournisseurs['fou_id'];
	}
	

}
?>