<?php
include("../includes/connexion.php");


// PERMET DE SUPPRIMER UNE ADRESSE

$resultat=array(
	"statut" => null,
	"warning" => ""
);
if(isset($_POST)){

	if(!empty($_POST["url_fichier"])){
		$url_fichier=$_POST["url_fichier"];
		
		if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/documents/" . $url_fichier)){ 
			unlink($_SERVER["DOCUMENT_ROOT"] . "/documents/" . $url_fichier);
			$resultat["statut"]="ok";
			
		}else{
			$resultat["statut"]="warning";
			$resultat["warning"]="Le fichier n'existe pas!";
		}
		echo(json_encode($resultat));
	}else{
		echo("Paramètre absent!");
		die();
	}
}else{
	echo("Paramètre absent!");
	die();
}
?>