<?php
include("../includes/connexion.php");


// PERMET DE SUPPRIMER UNE ADRESSE

$erreur="";
if(isset($_POST)){

	$adresse=0;
	if(!empty($_POST["adresse"])){
		$adresse=intval($_POST["adresse"]);
	}
	
	$client=0;
	if(!empty($_POST["client"])){
		$client=intval($_POST["client"]);
	}


	
	if($adresse>0 AND $client>0){
		
		// modele pas nécéssaire
		
		$sql="SELECT adr_type FROM Adresses WHERE adr_id=" . $adresse . " AND adr_ref_id=" . $client . ";";
		$req=$Conn->query($sql);
		$d_adresse=$req->fetch();
		if(empty($d_adresse)){
			$erreur="2-ERREUR DONNEE";
		}
		
		if(empty($erreur)){
			$sql="DELETE FROM Adresses WHERE adr_id=" . $adresse . " AND adr_ref_id=" . $client . ";";
			try{
				$req=$Conn->query($sql);
			}catch (Exception $e){
				$erreur=$e->getMessage();
			}
		}

	}else{
		$erreur="1-ERREUR PARAMETRE";
	}
}else{
	$erreur="2-ERREUR PARAMETRE";
}

if($erreur==""){
	
	$result_supp=array(
		"adresse" => $adresse,
		"type" => $d_adresse["adr_type"]
	);
	echo json_encode($result_supp);
	
}else{
	echo $erreur;
}
?>