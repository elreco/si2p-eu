<?php

include "../includes/controle_acces.inc.php";

include '../includes/connexion_soc.php';
include '../includes/connexion.php';

// RECUPERE LES DONNEES SUR L'INSCRIPTION D'UN STAGIAIRE A UNE ACTION

$erreur_txt="";

if(!empty($_POST)){
	
	$action_id=0;
	if (isset($_POST['action'])){
		$action_id=intval($_POST['action']);	
	}
	
	$stagiaire_id=0;
	if(isset($_POST['stagiaire'])){
		$stagiaire_id=intval($_POST['stagiaire']);	
	}
	
	if(empty($action_id) OR empty($stagiaire_id) ){
		$erreur_txt="Formulaire incomplet!";
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}

if(empty($erreur_txt)){
	
	$json=array();
	
	$req=$Conn->prepare("SELECT sta_nom,sta_prenom FROM Stagiaires WHERE sta_id=:stagiaire_id;");
	$req->bindParam(":stagiaire_id",$stagiaire_id);
	$req->execute();
	$d_stagiaire=$req->fetch();
	if(!empty($d_stagiaire)){
		$json["sta_id"]=$stagiaire_id;
		$json["sta_nom"]=$d_stagiaire['sta_nom'];
		$json["sta_prenom"]=$d_stagiaire['sta_prenom'];
	}else{
		$erreur_txt="Impossible de charger les données liées au stagiaire!";	
	}
	
}
if(empty($erreur_txt)){
	
	// DONNEE SUR L4INSCRIPTION DU STA
	$req=$ConnSoc->prepare("SELECT ast_attestation,ast_confirme,ast_action_client,act_gest_sta FROM Actions_Stagiaires,Actions WHERE ast_action=act_id AND ast_action=:action_id AND ast_stagiaire=:stagiaire_id;");
	$req->bindParam(":action_id",$action_id);
	$req->bindParam(":stagiaire_id",$stagiaire_id);
	$req->execute();
	$d_action_stagiaire=$req->fetch();
	if(!empty($d_action_stagiaire)){
		$json["ast_attestation"]=$d_action_stagiaire["ast_attestation"];
		$json["ast_confirme"]=$d_action_stagiaire["ast_confirme"];
		$json["ast_action_client"]=$d_action_stagiaire["ast_action_client"];	
	}else{
		$erreur_txt="Impossible de charger les données d'inscription!";	
	}
}
if(empty($erreur_txt)){
	
	if($d_action_stagiaire["act_gest_sta"]==2){
		
		// INSCRIPTION A LA SESSION
		
		$req=$ConnSoc->prepare("SELECT ass_session FROM Actions_Stagiaires_Sessions WHERE ass_action=:action_id AND ass_stagiaire=:stagiaire_id;");
		$req->bindParam(":action_id",$action_id);
		$req->bindParam(":stagiaire_id",$stagiaire_id);
		$req->execute();
		$d_sessions=$req->fetchAll();
		if(!empty($d_sessions)){
			if(count($d_sessions)>1){
				$erreur_txt="Erreur. Le stagiaire est inscrit sur plusieurs sessions!";	
			}else{
				$json["session_id"]=$d_sessions[0]["ass_session"];
			}
		}else{
			$json["session_id"]=0;
		}
		
	}else{
		
		// INSCRIPTION FORMATION 
		// -> cette info est récupérer par ajax_get_action_client
		/*$req=$ConnSoc->prepare("SELECT int_label_1,DATE_FORMAT(pda_date,'%d/%m/%Y') AS pda_date,pda_demi,ase_id,ase_h_deb,ase_h_fin,ass_session 
		FROM Plannings_Dates INNER JOIN Actions_Clients_Dates ON (Plannings_Dates.pda_id=Actions_Clients_Dates.acd_date)
		INNER JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)
		INNER JOIN Actions_Sessions ON (Actions_Clients_Dates.acd_date=Actions_Sessions.ase_date)
		LEFT JOIN Actions_Stagiaires_Sessions ON (Actions_Sessions.ase_id=Actions_Stagiaires_Sessions.ass_session AND Actions_Stagiaires_Sessions.ass_stagiaire=:stagiaire_id)
		WHERE pda_type=1 AND pda_ref_1=:action_id AND acd_action_client=:acd_action_client;");
		$req->bindParam(":action_id",$action_id);
		$req->bindParam(":acd_action_client",$d_action_stagiaire["ast_action_client"]);
		$req->bindParam(":stagiaire_id",$stagiaire_id);
		$req->execute();
		$d_sessions=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($d_sessions)){
			$json["sessions"]=$d_sessions;
		}*/
		
		$json["session_id"]=0;
	}
}
if(!empty($erreur_txt)){
	echo($erreur_txt);
}else{
	echo json_encode($json);
}
?>
