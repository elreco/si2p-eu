<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // ACTUALISE LE BLOC FACTURATION D'UNE CONVENTION

 // retour JSON compatible select2
 
 
$erreur_txt="";

if(isset($_POST)){
	 
	$convention_id=0;
	if(!empty($_POST["convention"])){
		$convention_id=intval($_POST["convention"]); 
	}	
	
	if(empty($convention_id)){
		
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();
		
	}else{
		
		$sql="SELECT con_numero,con_cli_geo,con_action,con_action_client,con_tva_id,con_societe FROM Conventions WHERE con_id=" . $convention_id . ";";
		$req=$ConnSoc->query($sql);
		$d_convention=$req->fetch();
		if(empty($d_convention)){
			$erreur_txt="Impossible de charger la convention!";	
			echo($erreur_txt);
			die();			
		}
	}
	
	// SUR L'ACTION
	$sql="SELECT act_date_deb,acl_produit,acl_facturation,acl_ca,acl_ca_gfc FROM Actions,Actions_Clients WHERE act_id=acl_action AND acl_id=" . $d_convention["con_action_client"] . ";";
	$req=$ConnSoc->query($sql);
	$d_action=$req->fetch();
	if(!empty($d_action)){
		
		$con_ht=0;
		if($d_action["acl_facturation"]==1){
			$con_ht=$d_action["acl_ca_gfc"];
		}else{
			$con_ht=$d_action["acl_ca"];
		}
	}else{
		$erreur_txt="Impossible de charger la formation!";	
		echo($erreur_txt);
		die();			
	}
	
	// SUR LA SOCIETE DE LA CONVENTION
	$sql="SELECT soc_tva_exo FROM Societes WHERE soc_id=" . $d_convention["con_societe"] . ";";
	$req=$Conn->query($sql);
	$d_societe=$req->fetch();
	if(empty($d_societe)){
		$erreur_txt="Impossible d'identifier la société!";	
		echo($erreur_txt);
		die();			
	}
	
	// SUR LE PRODUIT
	$sql="SELECT pro_tva,pro_deductible FROM Produits WHERE pro_id=" . $d_action["acl_produit"] . ";";
	$req=$Conn->query($sql);
	$d_produit=$req->fetch();
	if(empty($d_produit)){
		$erreur_txt="Impossible de charger la formation!";	
		echo($erreur_txt);
		die();			
	}
	
	// CALCUL DE LA TVA
	
	if($d_convention["con_cli_geo"]!=1 OR ($d_produit["pro_deductible"]==1 AND $d_societe["soc_tva_exo"]==1)){
		$con_tva_id=3;
		$con_tva_periode=3;
		$con_tva_taux=0;
	}else{
	
		$con_tva_id=$d_produit["pro_tva"];
		
		// CHERCHE DE LA TVA EN VIGUEUR
		$sql="SELECT tpe_id,tpe_taux FROM Tva_Periodes WHERE tpe_tva=" . $con_tva_id . " 
		AND tpe_date_deb<='" .$d_action["act_date_deb"] . "' AND (tpe_date_fin>='" .$d_action["act_date_deb"] . "' OR ISNULL(tpe_date_fin) );";
		$req=$Conn->query($sql);
		$d_tva=$req->fetch();
		if(empty($d_tva)){
			$erreur_txt="Impossible de calculer la TVA!";	
			echo($erreur_txt);
			die();			
		}else{
			$con_tva_periode=$d_tva["tpe_id"];
			$con_tva_taux=$d_tva["tpe_taux"];
		}
	}
	
	// CHAMP DU FORM
	
	$con_unite=0;
	if(!empty($_POST["con_unite"])){
		$con_unite=intval($_POST["con_unite"]); 
	}
	
	$con_qte=0;
	if(!empty($_POST["con_qte"])){
		$con_qte=floatval($_POST["con_qte"]); 
	}
	
	// CALCUL TTC
	
	$con_ttc=$con_ht + ($con_ht*($con_tva_taux/100));
	$con_ttc=round($con_ttc,2);
	
	$con_tva=$con_ttc-$con_ht;
	$con_tva=round($con_tva,2);
	
	
	// UPDATE DE LA CONVENTION
	
	$sql="UPDATE Conventions SET 
	con_unite=:con_unite,
	con_unite_txt=:con_unite_txt,
	con_qte=:con_qte,
	con_ht=:con_ht,
	con_tva_id=:con_tva_id,
	con_tva_periode=:con_tva_periode,
	con_tva_taux=:con_tva_taux,
	con_ttc=:con_ttc,
	con_comment=:con_comment
	WHERE con_id=:con_id;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam("con_unite",$con_unite);
	$req->bindParam("con_unite_txt",$_POST["con_unite_txt"]);
	$req->bindParam("con_qte",$con_qte);
	$req->bindParam("con_ht",$con_ht);
	$req->bindParam("con_tva_id",$con_tva_id);
	$req->bindParam("con_tva_periode",$con_tva_periode);
	$req->bindParam("con_tva_taux",$con_tva_taux);
	$req->bindParam("con_ttc",$con_ttc);
	$req->bindParam("con_comment",$_POST["con_comment"]);
	$req->bindParam("con_id",$convention_id);
	try{
		$req->execute();
	}Catch(Exception $e){
		$erreur_txt=$e->getMessage();
		echo($erreur_txt);
		die();
	}
	
	if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Conventions/" . $d_convention["con_numero"] . ".pdf")){
		unlink($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Conventions/" . $d_convention["con_numero"] . ".pdf");
	}
	
	// MAJ DU CA ACTION CLIENT
	$sql="UPDATE Actions_clients SET acl_ca=:acl_ca WHERE acl_id=:acl_id;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam("acl_ca",$con_ht);
	$req->bindParam("acl_id",$d_convention["con_action_client"]);
	try{
		$req->execute();
	}Catch(Exception $e){
	}

}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}
$retour=array(
	"con_qte" => $con_qte,
	"con_unite_txt" => $_POST["con_unite_txt"],
	"con_ht" => $con_ht,
	"con_tva_taux" => $con_tva_taux,
	"con_ttc" => $con_ttc,
	"con_tva" => $con_tva,
	"con_comment" => $_POST["con_comment"] ,
	"con_tva_id" => $con_tva_id
);
echo(json_encode($retour));

?>