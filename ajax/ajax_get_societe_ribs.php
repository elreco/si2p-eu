<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

// RETOURNE LES RIBS LIE à UNE FAMILLE

// retour => tableau JSON (compatible Select2)


$erreur=0;
$ribs=array();

if(isset($_POST)){
	 
	$societe=0;
	if(!empty($_POST["societe"])){
		$societe=intval($_POST["societe"]); 
	}else{
		$erreur=1;
	}	

	if($erreur==0){
		
		$sql="SELECT rib_id,rib_nom FROM Rib WHERE rib_societe=" . $societe . ";";
		$req=$Conn->query($sql);	
		$results=$req->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$ribs[]=array(
					"id" => $r["rib_id"],
					"text" => $r["rib_nom"]
				);
			}
		}
	}
}else{
	$erreur=10;
}
if($erreur==0){
	echo json_encode($ribs);
}else{
	//header("HTTP/1.0 500 " . $erreur);
};
?>
