<?php 

	include("../connexion.php");

	$retour=array();
	
	$sql="SELECT * FROM Cnrs_stagiaires WHERE NOT sta_archive";
	if(!empty($_POST["q"])){
		$sta_nom=$_POST["q"] . "%";
		$sql.=" AND sta_nom LIKE :sta_nom";
	}	
	$sql.=" ORDER BY sta_nom,sta_prenom";
	
	$req = $Conn->prepare($sql);
	if(!empty($_POST["q"])){
		$req->bindParam(":sta_nom",$sta_nom);
	}	
	$req->execute();
	$stagiaires = $req->fetchAll();
	if(!empty($stagiaires)){
		foreach($stagiaires as $s){
			$retour[] = array(
				"id" => $s['sta_id'],
				"nom" => $s['sta_nom'] . " " . $s['sta_prenom']
			);
		}
	}
	echo json_encode($retour);
	?>