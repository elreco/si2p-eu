<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // ACTUALISE LE STATUT DE L'ATTESTATION D'UN STAGIAIRE

 // retour JSON compatible select2
 
 
$erreur_txt="";
if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]); 
	}	
	$stagiaire_id=0;
	if(!empty($_POST["stagiaire"])){
		$stagiaire_id=intval($_POST["stagiaire"]); 
	}
	
	if(empty($action_id) OR empty($stagiaire_id)){
		
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();
		
	}else{
		
		// personne connecte
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];  
		}
		
		
		// ON RECUPERE l'ATTESTATION
		$sql="SELECT ast_attestation FROM Actions_Stagiaires WHERE ast_action=:action_id AND ast_stagiaire=:stagiaire_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("action_id",$action_id);
		$req->bindParam("stagiaire_id",$stagiaire_id);
		$req->execute();
		$d_sta_attestation=$req->fetch();
		if(empty($d_sta_attestation)){
			$erreur_txt="Impossible de récupérer l'attestation";
			echo($erreur_txt);
			die();
		}
		
		// ON RECUPERE LES COMPETENCES
		
		$reussite=1;
		
		$tab_comp=array();
		
		$sql="SELECT aco_id FROM Attestations_competences WHERE aco_attestation=" . $d_sta_attestation["ast_attestation"] . " ORDER BY aco_id";
		$req=$Conn->prepare($sql);
		$req->bindParam("action_id",$action_id);
		$req->bindParam("stagiaire_id",$stagiaire_id);
		$req->execute();
		$d_competences=$req->fetchAll();
		if(!empty($d_competences)){
			foreach($d_competences as $comp){
				
				if(!empty($_POST["competence_" . $comp["aco_id"]])){
					$acquise=1;
				}else{
					$acquise=0;
					$reussite=0;
				}
				$tab_comp[]=array(
					"aco_id" => $comp["aco_id"],
					"aco_acquise" => $acquise
				);
			}
		}
		
		// ON DELETE TOUTES LES COMPETENCES
		$sql="DELETE FROM Stagiaires_Attestations_Competences WHERE sac_action=:action_id AND sac_stagiaire=:stagiaire_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("action_id",$action_id);
		$req->bindParam("stagiaire_id",$stagiaire_id);
		try{
			$req->execute();
		}Catch(Exception $e){
			echo("A " . $e->getMessage());
			die();
		}
		
		// SI ECHEC ON ENREGISTRE LES DETAILS DES COMPETENCES
		if(empty($reussite)){
			
			$sql="INSERT INTO Stagiaires_Attestations_Competences (sac_stagiaire,sac_action,sac_attest_competence,sac_acquise) VALUES (:stagiaire_id,:action_id,:sac_attest_competence,:sac_acquise);";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam("action_id",$action_id);
			$req->bindParam("stagiaire_id",$stagiaire_id);
			
			foreach($tab_comp as $tc){
				$req->bindParam("sac_attest_competence",$tc["aco_id"]);
				$req->bindParam("sac_acquise",$tc["aco_acquise"]);
				try{
					$req->execute();
				}Catch(Exception $e){
					echo("B " . $e->getMessage());
					die();
				}
			}
		}
		
		// on supp le fichier PDF
		
		if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Attestations/attestation_". $action_id . "_" . $stagiaire_id . ".pdf")){ 
			unlink($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Attestations/attestation_". $action_id . "_" . $stagiaire_id . ".pdf");
		}
		
		// ON MAJ L'ETAT DE L'ATTESTATION
		// ON RECUPERE l'ATTESTATION
		$sql="UPDATE Actions_Stagiaires SET ast_attest_ok=:reussite WHERE ast_action=:action_id AND ast_stagiaire=:stagiaire_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("action_id",$action_id);
		$req->bindParam("stagiaire_id",$stagiaire_id);
		$req->bindParam("reussite",$reussite);
		try{
			$req->execute();
		}Catch(Exception $e){
			echo("C " . $e->getMessage());
			die();
		}
		
		$retour=array(
			"stagiaire" => $stagiaire_id,
			"reussite" => $reussite
		);
		echo json_encode($retour);
		
		
		
		
	}
}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}
?>