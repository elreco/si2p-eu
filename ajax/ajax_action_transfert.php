<?php
include "../includes/controle_acces.inc.php";

include '../includes/connexion.php';
include '../connexion_soc.php';

$erreur=0;
if(isset($_POST)){
	 
	$action=0;
	if(!empty($_POST["mdl_cli_action"])){
		$action=$_POST["mdl_cli_action"]; 
	}else{
		$erreur=1;
	}	 
	$liste_case="";
	if(!empty($_POST['mdl_cli_liste_case'])){
		$liste_case=$_POST['mdl_cli_liste_case']; 
	}else{
		$erreur=1;
	}
	
	$liste_action_client="";
	if(!empty($_POST['mdl_cli_action_client'])){
		$liste_action_client=$_POST['mdl_cli_action_client']; 
	}else{
		$erreur=1;
	}
	//echo("liste_action_client " . $liste_action_client);
	
	
	if($erreur==0){	
	
			// ACTION DE DEPART -> DONNEE REPRISE SUR LA NOUVELLE ACTION ACTION
			
		$sql_action="SELECT act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_nb_session";
		$sql_action.=",act_h_deb_1_1,act_h_fin_1_1,act_h_deb_1_2,act_h_fin_1_2,act_h_deb_1_3,act_h_fin_1_3,act_h_deb_1_4,act_h_fin_1_4";
		$sql_action.=",act_h_deb_2_1,act_h_fin_2_1,act_h_deb_2_2,act_h_fin_2_2,act_h_deb_2_3,act_h_fin_2_3,act_h_deb_2_4,act_h_fin_2_4";
		$sql_action.=",act_alerte_jour";
		$sql_action.=" FROM Actions WHERE act_id=:action";	
		$req_action=$ConnSoc->prepare($sql_action);
		$req_action->bindParam(":action",$action);
		$req_action->execute();
		$d_action=$req_action->fetch();
		if(!empty($d_action)){
			

			//DONNEE NECESSAIRE A LA CREATION DE LA NOUVELLE ACTION
			
			$act_produit_categorie=0;
			$sql_produit="SELECT pfa_couleur FROM Produits_Familles WHERE pfa_id=:famille";	
			$req_produit=$Conn->prepare($sql_produit);
			$req_produit->bindParam(":famille",$d_action["act_pro_famille"]);
			$req_produit->execute();
			$d_produit=$req_produit->fetch();
			
			
			// ON COMPLETE AVEC LES DONNE EN PROVENANCE DU FORM
			
			// les case select
			
			$case=explode (",",$liste_case);
			
			$nb_demi_j=count($case);
			
			// info sur la première date
			$donnee_case=explode ("_",$case[0]);
			$intervenant=$donnee_case[0];
			$date=$donnee_case[1];
			
			
			
			// le contact sur site
			$contact=0;
			$con_ref=0;
			$con_ref_id=0;
			if(!empty($_POST["mdl_cli_contact"])){
				if($_POST["mdl_cli_contact"]>0){
					$contact=$_POST["mdl_cli_contact"];
					$con_ref=1;
					if(!empty($_POST["mdl_cli_client"])){
						$con_ref_id=$_POST["mdl_cli_client"];
					}
				}	
			}
			
			// adresse d'intervention
			
			$adr_ref=0;
			$adr_ref_id=0;
			$adresse=0;
			if(!empty($_POST["mdl_cli_adr_ref"])){
				$adr_ref=$_POST["mdl_cli_adr_ref"];
				if($adr_ref==1){
					if($_POST["mdl_cli_adr_ref_id"]>0){
						if(!empty($_POST["mdl_cli_client"])){
							$adr_ref_id=$_POST["mdl_cli_client"];
						}
						$adresse=$_POST["mdl_cli_adr_ref_id"];
					}
				}
			}
			
			$act_alerte_date=null;
			if($d_action["act_alerte_jour"]>0){
				$act_alerte_date=new DateTime($date);
				$act_alerte_date.AddDays(-$act_alerte_date);
			}

			// LES ACTIONS CLIENTS CONCERNNE PAR DE TRANSFERT
		
			$tab_action_client=explode (",",$liste_action_client);
			
			$sql="SELECT acl_pro_inter,cli_code FROM Actions_Clients,Clients WHERE acl_client=cli_id AND acl_id=:action_client;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client",$tab_action_client[0]);		
			$req->execute();
			$result=$req->fetch();
			if(!empty($result)){
				if($result["acl_pro_inter"]){
					$texte=$_POST["mdl_cli_act_adr_ville"];
				}else{
					$texte=$result["cli_code"];
				}
			}

			//print_r($tab_action_client);
			
			
			/*-----------------------------
				TRAITEMENT
			-------------------------------*/
			
			// ENREGISTREMENT DE LA NOUVELLE Action
			
			$sql_action="INSERT INTO Actions (";
			$sql_action.="act_adr_ref,act_adr_ref_id,act_adresse,act_adr_nom,act_adr_service,act_adr1,act_adr2,act_adr3,act_adr_cp,act_adr_ville,act_pro_categorie,act_pro_famille";
			$sql_action.=",act_pro_sous_famille,act_intervenant,act_date_deb";
			$sql_action.=",act_contact,act_con_ref,act_con_ref_id,act_con_nom,act_con_prenom,act_con_tel,act_con_portable";
			$sql_action.=",act_nb_session,act_h_deb_1_1,act_h_fin_1_1,act_h_deb_1_2,act_h_fin_1_2,act_h_deb_1_3,act_h_fin_1_3,act_h_deb_1_4,act_h_fin_1_4";
			$sql_action.=",act_h_deb_2_1,act_h_fin_2_1,act_h_deb_2_2,act_h_fin_2_2,act_h_deb_2_3,act_h_fin_2_3,act_h_deb_2_4,act_h_fin_2_4";
			//$sql_action.=",act_alerte_date,act_alerte_jour";
			$sql_action.=")";
			$sql_action.=" VALUES (";
			$sql_action.=" :adr_ref, :adr_ref_id, :adresse, :adr_nom, :adr_service, :adr1, :adr2, :adr3, :adr_cp, :adr_ville, :pro_categorie, :pro_famille";
			$sql_action.=",:pro_sous_famille, :intervenant, :date_deb";
			$sql_action.=",:contact,:con_ref,:con_ref_id,:con_nom,:con_prenom,:con_tel,:con_portable";
			$sql_action.=",:nb_session,:h_deb_1_1,:h_fin_1_1,:h_deb_1_2,:h_fin_1_2,:h_deb_1_3,:h_fin_1_3,:h_deb_1_4,:h_fin_1_4";
			$sql_action.=",:h_deb_2_1,:h_fin_2_1,:h_deb_2_2,:h_fin_2_2,:h_deb_2_3,:h_fin_2_3,:h_deb_2_4,:h_fin_2_4";
			//$sql_action.=",:alerte_date,:act_alerte_jour";
			$sql_action.=");";		
			$req_action=$ConnSoc->prepare($sql_action);
			$req_action->bindParam(":adr_ref",$adr_ref);
			$req_action->bindParam(":adr_ref_id",$adr_ref_id);
			$req_action->bindParam(":adresse",$adresse);
			$req_action->bindParam(":adr_nom",$_POST["mdl_cli_act_adr_nom"]);
			$req_action->bindParam(":adr_service",$_POST["mdl_cli_act_adr_service"]);
			$req_action->bindParam(":adr1",$_POST["mdl_cli_act_adr1"]);
			$req_action->bindParam(":adr2",$_POST["mdl_cli_act_adr2"]);
			$req_action->bindParam(":adr3",$_POST["mdl_cli_act_adr3"]);
			$req_action->bindParam(":adr_cp",$_POST["mdl_cli_act_adr_cp"]);
			$req_action->bindParam(":adr_ville",$_POST["mdl_cli_act_adr_ville"]);
			$req_action->bindParam(":pro_categorie",$d_action["act_pro_categorie"]);
			$req_action->bindParam(":pro_famille",$d_action["act_pro_famille"]);
			$req_action->bindParam(":pro_sous_famille",$d_action["act_pro_sous_famille"]);
			$req_action->bindParam(":intervenant",$intervenant);
			$req_action->bindParam(":date_deb",$date);
			
			$req_action->bindParam(":contact",$contact);
			$req_action->bindParam(":con_ref",$con_ref);
			$req_action->bindParam(":con_ref_id",$con_ref_id);
			$req_action->bindParam(":con_nom",$_POST["mdl_cli_con_nom"]);
			$req_action->bindParam(":con_prenom",$_POST["mdl_cli_con_prenom"]);
			$req_action->bindParam(":con_tel",$_POST["mdl_cli_con_tel"]);
			$req_action->bindParam(":con_portable",$_POST["mdl_cli_con_portable"]);
			
			$req_action->bindParam(":nb_session",$d_action["act_nb_session"]);
			
			$req_action->bindParam(":h_deb_1_1",$d_action["act_h_deb_1_1"]);
			$req_action->bindParam(":h_fin_1_1",$d_action["act_h_fin_1_1"]);
			$req_action->bindParam(":h_deb_1_2",$d_action["act_h_deb_1_2"]);
			$req_action->bindParam(":h_fin_1_2",$d_action["act_h_fin_1_2"]);
			$req_action->bindParam(":h_deb_1_3",$d_action["act_h_deb_1_3"]);
			$req_action->bindParam(":h_fin_1_3",$d_action["act_h_fin_1_3"]);
			$req_action->bindParam(":h_deb_1_4",$d_action["act_h_deb_1_4"]);
			$req_action->bindParam(":h_fin_1_4",$d_action["act_h_fin_1_4"]);
			
			$req_action->bindParam(":h_deb_2_1",$d_action["act_h_deb_2_1"]);
			$req_action->bindParam(":h_fin_2_1",$d_action["act_h_fin_2_1"]);
			$req_action->bindParam(":h_deb_2_2",$d_action["act_h_deb_2_2"]);
			$req_action->bindParam(":h_fin_2_2",$d_action["act_h_fin_2_2"]);
			$req_action->bindParam(":h_deb_2_3",$d_action["act_h_deb_2_3"]);
			$req_action->bindParam(":h_fin_2_3",$d_action["act_h_fin_2_3"]);
			$req_action->bindParam(":h_deb_2_4",$d_action["act_h_deb_2_4"]);
			$req_action->bindParam(":h_fin_2_4",$d_action["act_h_fin_2_4"]);
			
			//$req_action->bindParam(":alerte_date",$act_alerte_date);
			//$req_action->bindParam(":alerte_jour",$act_alerte_jour);
			//$req_action->bindParam(":alerte_jour",$act_alerte_jour);
			
			$req_action->execute();
			$action_cible = $ConnSoc -> lastInsertId();
			
			// style des cases
			if(!empty($d_produit["pfa_couleur"])){
				$style="background-color:#" . $d_produit["pfa_couleur"];
			} 	
			
			// table de retour pour ecriture planning
			$case_maj=array(
				"type" =>"1",
				"ref_1" =>$action_cible,
				"texte" => $texte,
				"style" => $style,
				"date" => array(),
				"erreur" => 0
			);
			
			// ENREGISTREMENT DES DATES ASSOCIES
			
			// PREPARATION DES REQUETES

			// controle que la date ajouter soit bien libre
			$req_select=$ConnSoc->prepare("SELECT pda_id,pda_type FROM Plannings_Dates WHERE pda_intervenant=:intervenant AND pda_date=:date AND pda_demi=:demi;");
			
			// ajout de date
			$sql_insert="INSERT INTO Plannings_Dates (pda_intervenant,pda_date,pda_demi,pda_type,pda_ref_1,pda_texte,pda_style)";
			$sql_insert.=" VALUES (:intervenant, :date, :demi, 1, :action, :texte, :style);";		
			$req_insert=$ConnSoc->prepare($sql_insert);
			$req_insert->bindParam(":texte",$texte);
			$req_insert->bindParam(":style",$style);
			$req_insert->bindParam(":action",$action_cible);
			
			// ajout de session
			$sql_session="INSERT INTO Actions_Sessions (ase_action,ase_date,ase_h_deb,ase_h_fin)";
			$sql_session.=" VALUES (:action, :date, :h_deb, :h_fin);";	
			$req_session=$ConnSoc->prepare($sql_session);
			$req_session->bindParam(":action",$action_cible);
			
			// ajout du client sur une date
			$sql_cd="INSERT INTO Actions_Clients_Dates (acd_action_client,acd_date)";
			$sql_cd.=" VALUES (:action_client, :date);";	
			$req_cd=$ConnSoc->prepare($sql_cd);
			
			
			
			// FIN PREP REQUETE
	
			// enrehistrement des dates
			foreach($case as $c){			
				if(!empty($c) AND $erreur==0){
					$donnee_case=explode ("_",$c);
					$intervenant=$donnee_case[0];
					$date=$donnee_case[1];
					$demi=$donnee_case[2];
					
					// on regarde si il y a déja une date d'enregistrer
					$req_select->bindParam(":intervenant",$intervenant);
					$req_select->bindParam(":date",$date);
					$req_select->bindParam(":demi",$demi);
					$req_select->execute();
					$case_select = $req_select->fetch();	

					// LA DATE DOIT ETRE LIBRE POUR CONTINUER	
					
					if(empty($case_select)){
						// enregistrement de la date
						$req_insert->bindParam(":intervenant",$intervenant);			
						$req_insert->bindParam(":date",$date);
						$req_insert->bindParam(":demi",$demi);
						$req_insert->execute();
						$date_id = $ConnSoc -> lastInsertId();
						
						// ajout des sessions
						$req_session->bindParam(":date",$date_id);	
						
						for($i=1;$i<=$d_action["act_nb_session"];$i++){	
							if($i==1 OR !empty($d_action["act_h_deb_" . $demi . "_" . $i]) ){
								$req_session->bindParam(":h_deb",$d_action["act_h_deb_" . $demi . "_" . $i]);
								$req_session->bindParam(":h_fin",$d_action["act_h_fin_" . $demi . "_" . $i]);
								$req_session->execute();
							}
						}
						
						
						foreach($tab_action_client as $tac){
							
							//echo("tac : " . $tac);
							//die();
							$req_cd->bindParam(":date",$date_id);
							$req_cd->bindParam(":action_client",intval($tac));
							$req_cd->execute();
						}
						array_push($case_maj["date"],$c);				
					}else{
						$erreur=10;
					}
				}
			}
			
			// FIN ENREGISTREMENT DATE
			
			// MAJ DE ACTIONS_CLIENTS 
			
			$sql_up_acl="UPDATE Actions_Clients SET acl_action=:action_cible WHERE acl_id=:action_client;";
			$req_up_acl=$ConnSoc->prepare($sql_up_acl);
			
			$sql_select_acd="SELECT pda_id FROM Actions_Clients_Dates,Plannings_Dates WHERE acd_date=pda_id AND pda_type=1 AND pda_ref_1=:action AND acd_action_client=:action_client;";
			$req_select_acd=$ConnSoc->prepare($sql_select_acd);
			
			$sql_delete_acd="DELETE FROM Actions_Clients_Dates WHERE acd_date=:date_id AND acd_action_client=:action_client;";
			$req_delete_acd=$ConnSoc->prepare($sql_delete_acd);

			foreach($tab_action_client as $tac){
				
				$req_up_acl->bindParam(":action_cible",$action_cible);
				$req_up_acl->bindParam(":action_client",$tac);
				$req_up_acl->execute();
				
				$req_select_acd->bindParam(":action",$action);
				$req_select_acd->bindParam(":action_client",$tac);
				$req_select_acd->execute();
				$result=$req_select_acd->fetchAll();
				if(!empty($result)){
					foreach($result as $r){
						$req_delete_acd->bindParam(":date_id",$r["pda_id"]);
						$req_delete_acd->bindParam(":action_client",$tac);
						$req_delete_acd->execute();
					}
				}

			}
			
		}	
	}
}
if(!isset($case_maj)){
	$case_maj=array(
		"erreur" => 1
	);
}else{
	$case_maj["erreur"]=$erreur;
}
echo json_encode($case_maj);
 
?>