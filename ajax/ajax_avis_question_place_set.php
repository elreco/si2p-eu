<?php
 // DEPLACE UNE QUESTION AU SEIN D'UN QUESTIONNAIRE
 
include('../includes/connexion.php');


$erreur="";

// parametre

if(isset($_POST['question'])){

	$question=intval($_POST['question']);
	$deplacement=intval($_POST['deplacement']);

	if(empty($question) OR empty($deplacement)){
		$erreur="Impossible de deplacer la question!";	
	} 
	
}else{
	$erreur="Impossible de deplacer la question!";	
}


if(empty($erreur)){
	
	// SUR LA QUESTION DEPLACE
	
	$sql="SELECT aqu_rubrique FROM avis_questions WHERE aqu_id=" . $question . " ORDER BY aqu_place,aqu_id;";
	try{
		$req = $Conn->query($sql);			
	}Catch(Exception $e){
		$erreur=$e->getMessage();
	}
	if(empty($erreur)){
		$d_question=$req->fetch();
		$rubrique=intval($d_question["aqu_rubrique"]);
	}
	
	if($rubrique>0){
	
		// ON MAJ LES PLACES PAR SECURITE
		
		$sql="SELECT aqu_id FROM avis_questions WHERE aqu_rubrique=" . $rubrique . " ORDER BY aqu_place,aqu_id;";
		try{
			$req = $Conn->query($sql);	
		}Catch(Exception $e){
			$erreur=$e->getMessage();
		}
		if(empty($erreur)){
			
			$sql_secu="UPDATE avis_questions SET aqu_place=:place WHERE aqu_id=:question;";
			$req_secu = $Conn->prepare($sql_secu);	

			$d_questions=$req->fetchAll();
			$max_q=count($d_questions);

			foreach($d_questions as $key => $q){
				
				$place=$key+1;
				$req_secu->bindParam(":place",$place);
				$req_secu->bindParam(":question",$q["aqu_id"]);
				try{
					$req_secu->execute();
				}Catch(Exception $e){
					$erreur=$e->getMessage();
					break;
				}
			}
		}
		
		if(empty($erreur)){	
		
			

			$sql="SELECT aqu_place FROM avis_questions WHERE aqu_id=" . $question . ";";
			try{
				$req = $Conn->query($sql);
			}Catch(Exception $e){
				$erreur=$e->getMessage();
			}
			if(empty($erreur)){
				
				$retour=array();
				
				$d_question=$req->fetch();
				
				// MISE A JOUR DE LA PLACE DE LA QUESTION SELECTIONNE
				
				$new_place=$d_question["aqu_place"] + $deplacement;
				
				$sql="UPDATE avis_questions SET aqu_place=" . $new_place . " WHERE aqu_id=" . $question . ";";
				try{
					$req = $Conn->query($sql);
					$retour[]=array(
						"question" => $question,
						"place" => $new_place
					);
				}Catch(Exception $e){
					$erreur=$e->getMessage();
				}
				
				if(empty($erreur)){
					
					// MISE A JOUR DE LA PLACE DE L'AUTRE QUESTION (suiv ou prec)

					// suite à la maj $question à aussi la valeur $new_place
					$sql="SELECT aqu_id,aqu_place FROM avis_questions WHERE aqu_rubrique=" . $rubrique . " AND aqu_place=" . $new_place . " AND NOT aqu_id=" . $question . ";";
					try{
						$req = $Conn->query($sql);
					}Catch(Exception $e){
						$erreur=$e->getMessage();
					}
					if(empty($erreur)){
				
						$d_question_autre=$req->fetch();
						if(!empty($d_question_autre)){
							$sql="UPDATE avis_questions SET aqu_place=" . $d_question["aqu_place"] . " WHERE aqu_id=" . $d_question_autre["aqu_id"] . ";";
							try{
								$req = $Conn->query($sql);
								$retour[]=array(
									"question" => $d_question_autre["aqu_id"],
									"place" => $d_question["aqu_place"]
								);
							}Catch(Exception $e){
								$erreur=$e->getMessage();
							}
						}
					}
				}
				$retour[]=array(
					"max_q" => $max_q
				);
				
			}
		}
	}else{
		$erreur="Impossible de deplacer la question!";	
	}
}
if(!empty($erreur)){
	echo($erreur);
}else{
	echo json_encode($retour);	
}
?>