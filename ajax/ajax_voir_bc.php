<?php 
include "../includes/controle_acces.inc.php";
include("../includes/connexion.php");
include('../modeles/mod_tva.php');
include('../modeles/mod_parametre.php');
if(isset($_POST)){

	$req = $Conn->prepare("SELECT * FROM commandes_lignes WHERE cli_commande = " . $_POST['com_id']);
	$req->execute();
	$commandes = $req->fetchAll();

	foreach($commandes as $cli){


		$req = $Conn->prepare("SELECT ffa_libelle FROM fournisseurs_familles WHERE ffa_id = " . $cli['cli_famille']);
		$req->execute();
		$ffa = $req->fetch();

		if(!empty($cli['cli_produit_si2p'])){
			$req = $Conn->prepare("SELECT pro_reference FROM produits WHERE pro_id = " . $cli['cli_produit_si2p']);
			$req->execute();
			$pro = $req->fetch();
		}
		if(!empty($cli['cli_vehicule'])){
			$req = $Conn->prepare("SELECT veh_libelle FROM vehicules WHERE veh_id = " . $cli['cli_vehicule']);
			$req->execute();
			$veh = $req->fetch();
		}
		if(!empty($cli['cli_utilisateur'])){
			$req = $Conn->prepare("SELECT uti_nom, uti_prenom FROM utilisateurs WHERE uti_id = " . $cli['cli_utilisateur']);
			$req->execute();
			$uti = $req->fetch();
		}
		if(empty($cli['cli_vehicule'])){
			$veh['veh_libelle'] = "";
		}
		if(empty($cli['cli_utilisateur'])){
			$uti['uti_prenom'] = "";
			$uti['uti_nom'] = "";
		}
		foreach($base_tva as $b => $v){ 
	        
			if($b > 0){
	        	
	        	if($b == $cli['cli_tva']){
	        		$tva = $v;
	        	}
	        	
	        } 
	    } 
	    if(!empty($pro)){
	    	$retour[] = array(
	    		"cli_id" => $cli['cli_id'],
	    		"cli_reference" => $cli['cli_reference'],
	    		"cli_descriptif" => $cli['cli_descriptif'],
	    		"cli_libelle" => $cli['cli_libelle'],
				"famille" => $ffa['ffa_libelle'],
				"tva" => $tva, 
				"prix_ht" => $cli['cli_ht'],
				"cli_qte" => $cli['cli_qte'],
				"produit" => $pro['pro_reference'],
				"uti_libelle" => $uti['uti_prenom'] . " " . $uti['uti_nom'],
				"veh_libelle" => $veh['veh_libelle'],
				"date_livraison" => convert_date_txt($cli['cli_livraison']),
			);
	    }else{
	    	$retour[] = array(
	    		"cli_id" => $cli['cli_id'],
	    		"cli_reference" => $cli['cli_reference'],
	    		"cli_descriptif" => $cli['cli_descriptif'],
	    		"cli_libelle" => $cli['cli_libelle'],
				"famille" => $ffa['ffa_libelle'],
				"tva" => $tva,
				"produit" => "",
				"prix_ht" => $cli['cli_ht'],
				"cli_qte" => $cli['cli_qte'],
				"uti_libelle" => $uti['uti_prenom'] . " " . $uti['uti_nom'],
				"veh_libelle" => $veh['veh_libelle'],
				"date_livraison" => convert_date_txt($cli['cli_livraison']),
			);
	    }
	}


	echo json_encode($retour);
}

?>