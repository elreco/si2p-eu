<?php

include "../includes/controle_acces.inc.php";

include '../includes/connexion_soc.php';
include '../includes/connexion.php';

include "../modeles/mod_add_stagiaire.php";
include "../modeles/mod_action_stagiaire_add.php";



// CREATION ET INSCRIPTION D'UN STAGIAIRE A UNE ACTION DE FORMATION

// LA MAJ EST GERE VIA UN AUTRE SCRIPT AJAX

$erreur_txt="";

if(!empty($_POST)){
	
	$action_id=0;
	if (isset($_POST['action'])){
		$action_id=intval($_POST['action']);	
	}
	
	$action_client_id=0;
	if(isset($_POST['action_client'])){
		$action_client_id=intval($_POST['action_client']);	
	}
	
	if(empty($action_client_id)){
		$erreur_txt="Formulaire incomplet!";
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}
/***********************************
			CONTROLE
***********************************/

if(empty($erreur_txt)){

	$session_id=0;
	if(isset($_POST['session'])){
		$session_id=intval($_POST['session']);	
	}
	
	// INFO SUR L'ACTION
	$req=$ConnSoc->prepare("SELECT act_gest_sta,acl_client,acl_produit FROM Actions,Actions_Clients WHERE act_id=acl_action AND act_id=:action_id AND acl_id=:action_client_id;");
	$req->bindParam(":action_id",$action_id);
	$req->bindParam(":action_client_id",$action_client_id);
	$req->execute();
	$d_action=$req->fetch();
	if(!empty($d_action)){
		if($d_action["act_gest_sta"]==2 AND $session_id==0){
			$erreur_txt="Impossible d'identifier la session pour l'inscription";	
		}			
	}else{
		$erreur_txt="Impossible de charger les données liées à l'action!";
	}
}

if(empty($erreur_txt)){
	
	$stagiaire=0;
	if (isset($_POST['sta_id'])){
		$stagiaire=intval($_POST['sta_id']);	
	}
	$sta_nom=$_POST['sta_nom'];
	$sta_prenom=$_POST['sta_prenom'];
	$sta_client=0;
	
	if(empty($stagiaire)){
		
		
		if(empty($sta_nom) OR empty($sta_prenom)){
			$erreur_txt="Le nom et le prénom sont obligatoires pour enregistrer un nouveau stagiaire!";	
		}
		
	}else{
		
		// utilisation d'un stagiaire existant
		$req=$Conn->prepare("SELECT sta_nom,sta_prenom,sta_client FROM Stagiaires WHERE sta_id=:stagiaire;");
		$req->bindParam(":stagiaire",$stagiaire);
		$req->execute();
		$d_stagiaire=$req->fetch();
		if(!empty($d_stagiaire)){
			$sta_nom=$d_stagiaire['sta_nom'];
			$sta_prenom=$d_stagiaire['sta_prenom'];
			$sta_client=$d_stagiaire['sta_client'];
		}else{
			$erreur_txt="Impossible de charger les données liées au stagiaire!";	
		}
	}
}
		
// FIN CONTROLE	

if(empty($erreur_txt)){
	
	/***********************************
		TRAITEMENT AVANT ENR
	***********************************/
	
	$ast_attestation=0;
	if (isset($_POST['ast_attestation'])){
		$ast_attestation=intval($_POST['ast_attestation']);	
	}
	
	$ast_confirme=0;
	if(isset($_POST['ast_confirme'])){
		if(!empty($_POST['ast_confirme'])){
			$ast_confirme=1;
		}
	}
	
	// SUR LE PRODUIT
	
	$req=$Conn->prepare("SELECT pro_qualification FROM Produits WHERE pro_id=:pro_id;");
	$req->bindParam(":pro_id",$d_action["acl_produit"]);
	$req->execute();
	$d_produit=$req->fetch();
	
	
	/***********************************
		ENR
	***********************************/
	
	if(empty($stagiaire)){
		
		$add_result=add_stagiaire($sta_nom,$sta_prenom,$_POST['sta_naissance'],"","","","","","","","","","","",$d_action["acl_client"],1);
		if(!is_int($add_result)){
			$erreur_txt=$add_result;
		}elseif($add_result>0){
			// creation ok
			$stagiaire=$add_result;
		}else{
			$erreur_txt="Le stagiaire n'a pas été crée!";
		}
	}
}
if(empty($erreur_txt)){
	
	// le stagiaire existait ou a été crée

	if(!empty($session_id)){
		$param_session=$session_id;
	}elseif(isset($_POST["sessions"])){
		$param_session=$_POST["sessions"];
	}else{
		$param_session=0;
	}
	$erreur=add_action_stagiaire($stagiaire,$action_id,$action_client_id,$param_session,$ast_confirme,$ast_attestation,$d_produit["pro_qualification"]);	
	if(!empty($erreur)){
		if(is_string($erreur)){
			$erreur_txt=$erreur;
		}else{
			$erreur_txt="L'inscription du stagiaire n'a pas été prise en compte!";
		}
	}
}
// LE STAGIAIRE EST INSCRIT A LA SESSION -> ON CONSIDERE QUE LE CLIENT EST L'EMPLOYEUR ACTUEL
if(empty($erreur_txt)){
	
	if($d_action["acl_client"]!=$sta_client){
		
		$req=$Conn->prepare("SELECT cli_code,cli_nom FROM Clients WHERE cli_id=:client;");
		$req->bindParam(":client",$d_action["acl_client"]);
		$req->execute();
		$d_client=$req->fetch();
		
		$req=$Conn->prepare("UPDATE Stagiaires SET sta_client=:sta_client ,sta_client_code=:sta_client_code, sta_client_nom=:sta_client_nom WHERE sta_id=:stagiaire;");
		$req->bindParam(":sta_client",$d_action["acl_client"]);
		$req->bindParam(":sta_client_code",$d_client["cli_code"]);
		$req->bindParam(":sta_client_nom",$d_client["cli_nom"]);
		$req->bindParam(":stagiaire",$stagiaire);
		try{
			$req->execute();
		}Catch(Exception $e){
		
		}
		
		$req=$Conn->prepare("INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:scl_stagiaire,:scl_client);");
		$req->bindParam(":scl_stagiaire",$stagiaire);
		$req->bindParam(":scl_client",$d_action["acl_client"]);
		try{
			$req->execute();
		}Catch(Exception $e){
			
		}
	}
}
if(!empty($erreur_txt)){
	echo($erreur_txt);
}else{
	$retour=array(
		"action_client" => $action_client_id,
		"session" => $session_id,
		"stagiaire" => $stagiaire,
		"nom" => $sta_nom,
		"prenom" => $sta_prenom,
		"confirme" => $ast_confirme,
		"attestation" => $ast_attestation,
		"qualification" => $d_produit["pro_qualification"],
		"erreur" => $erreur
	);
	
	echo json_encode($retour);
}
?>
