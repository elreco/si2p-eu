<?php
include("../includes/connexion.php");
if(isset($_POST['field'])){

    $id= $_POST['field'];
    global $Conn;

    $req = $Conn->prepare("SELECT * FROM clients_societes WHERE cso_archive = 0  AND  cso_client =" . $id);
    $req->execute();

    $clients_societes = $req->fetchAll();

    foreach($clients_societes as $c){
    	$req = $Conn->prepare("SELECT * FROM societes WHERE soc_id =" . $c['cso_societe']);
	    $req->execute();

	    $societes[$c['cso_societe']] = $req->fetch();
    }
    if(!empty($societes)){
        echo json_encode($societes);
    }else{
        echo "Pas de société associée au client";
    }


}
?>
