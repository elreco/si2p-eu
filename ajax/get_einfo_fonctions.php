<?php

include "../includes/controle_acces.inc.php";

include '../includes/connexion.php';

 // RETOURNE LES FONCTIONS ASSOCIEES A UN UTILISATEURS
 // RETOUR FORMAT JSON
 
$erreur=0;

if(isset($_POST)){
	 
	$projet=0;
	if(!empty($_POST["projet"])){
		$projet=$_POST["projet"]; 
	}else{
		$erreur=1;
	}
	
	$utilisateur=0;
	if(!empty($_POST["utilisateur"])){
		$utilisateur=$_POST["utilisateur"]; 
	}else{
		$erreur=1;
	}	 
		
	
	if($erreur==0){
		
		$retour=array();
		$sql="SELECT sfo_id,sfo_nom,fon_nom,mod_nom 
		FROM Sous_Fonctions LEFT JOIN Fonctions ON (Sous_Fonctions.sfo_fonction=Fonctions.fon_id)
		LEFT JOIN Modules ON (Sous_Fonctions.sfo_module=Modules.mod_id)
		WHERE sfo_projet=" . $projet . " AND sfo_utilisateur=" . $utilisateur . " ORDER BY mod_nom,fon_nom,sfo_nom,sfo_id;"
		$req = $Conn->query($sql);
		$results = $req->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$adresses[]=array(
					"id" => $r["sfo_id"],		// nécessaire pour injection plugin select2
					"text" => $r["mod_nom"] . " " . $r["fon_nom"] . " " . $r["sfo_nom"]	// nécessaire pour injection plugin select2
				);	
			}
		}	
	}else{
		echo("ERREUR");
	}
	
}else{
	$erreur=1;
	echo("ERREUR");
}

if($erreur==0){
	echo json_encode($retour);
}
?>