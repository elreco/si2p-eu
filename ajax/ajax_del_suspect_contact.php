<?php
include('../includes/connexion_soc.php');


// PERMET DE SUPPRIMER UN CONTACT

$erreur="";
if(isset($_POST)){

	$contact=0;
	if(!empty($_POST["contact"])){
		$contact=intval($_POST["contact"]);
	}
	
	$suspect=0;
	if(!empty($_POST["suspect"])){
		$suspect=intval($_POST["suspect"]);
	}

	if($contact>0 AND $suspect>0){
		
		// modele pas nécéssaire
		
		$sql="DELETE FROM Suspects_Contacts WHERE sco_id=" . $contact . " AND sco_ref_id=" . $suspect . ";";
		try{
			$req=$ConnSoc->query($sql);
		}catch (Exception $e){
			$erreur=$e->getMessage();
		}
		
	}else{
		$erreur="1-ERREUR PARAMETRE";
	}
}else{
	$erreur="2-ERREUR PARAMETRE";
}

if($erreur==""){
	
	$result_supp=array(
		"contact" => $contact
	);
	echo json_encode($result_supp);
	
}else{
	echo $erreur;
}
?>