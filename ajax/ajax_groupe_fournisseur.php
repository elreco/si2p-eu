<?php
include("../includes/connexion.php");
$currentlevel = $_POST['levelId'];
$currentid = $_POST['id'];
$parentid = $_POST['parentId'];
$parentlevel = $_POST['parentLevel'];


if($parentlevel == "undefined"){ // maison mère

	$req = $Conn->prepare("SELECT fou_id, fou_filiale_de FROM fournisseurs WHERE fou_id = :id");
	$req->bindParam(':id', $currentid);
	$req->execute();
	$mm = $req->fetch();

	$req = $Conn->prepare("UPDATE fournisseurs SET fou_filiale_de = 0, fou_niveau = 0 WHERE fou_id = :fou_id;");
	$req->bindParam(':fou_id', $currentid);
	$req->execute();

	$req = $Conn->prepare("UPDATE fournisseurs SET fou_filiale_de = :fou_id, fou_niveau = 0 WHERE fou_id = :fou_id;");
	$req->bindParam(':fou_id', $mm['fou_filiale_de']);
	$req->execute();

	// actualisation des filiales de la nouvelle mm
	$req = $Conn->prepare("SELECT fou_id, fou_filiale_de, fou_fil_de, fou_niveau FROM fournisseurs WHERE fou_fil_de = :id");
	$req->bindParam(':id', $currentid);
	$req->execute();
	$fil_de = $req->fetchAll();

	foreach($fil_de as $f){
		$newniveau = $f['fou_niveau'] -1;
		$req = $Conn->prepare("UPDATE fournisseurs SET fou_filiale_de = :fou_id, fou_niveau = :fou_niveau WHERE fou_fil_de = :fou_id;");
		$req->bindParam(':fou_id', $currentid);
		$req->bindParam(':fou_niveau', $newniveau);
		$req->execute();
	}

	// actualisation des filiales de la nouvelle mm
	$req = $Conn->prepare("UPDATE fournisseurs SET fou_filiale_de = :fou_filiale_de WHERE fou_filiale_de = :id");
	$req->bindParam(':fou_filiale_de', $currentid);
	$req->bindParam(':id', $mm['fou_filiale_de']);
	$req->execute();


}else{
	$req = $Conn->prepare("SELECT fou_id, fou_filiale_de FROM fournisseurs WHERE fou_id = :id");
	$req->bindParam(':id', $parentid);
	$req->execute();
	$maisonmere = $req->fetch();
	if($maisonmere['fou_filiale_de'] == 0){
		$req = $Conn->prepare("UPDATE fournisseurs SET  fou_niveau = 0, fou_fil_de = 0 WHERE fou_id = :fou_id;");
		$req->bindParam(':fou_id', $currentid);
		$req->execute();
	}else{
		$newniveau = $parentlevel + 1;
		$req = $Conn->prepare("UPDATE fournisseurs SET  fou_niveau = :fou_niveau, fou_fil_de = :fou_fil_de WHERE fou_id = :fou_id;");
		$req->bindParam(':fou_id', $currentid);
		$req->bindParam(':fou_niveau', $newniveau);
		$req->bindParam(':fou_fil_de', $parentid);
		$req->execute();
	}
	

}