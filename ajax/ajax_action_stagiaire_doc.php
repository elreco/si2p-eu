<?php

include "../includes/controle_acces.inc.php";

include '../includes/connexion_soc.php';
include '../includes/connexion.php';
include('../modeles/mod_parametre.php');
include('../modeles/mod_stagiaire_attestation_data.php');
include('../modeles/mod_stagiaire_attestation_pdf.php');

include('../modeles/mod_convocation_data.php');
include('../modeles/mod_convocation_pdf.php');

include('../modeles/mod_get_juridique.php');
include('../modeles/mod_planning_periode_lib.php');

/*require_once("../vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;*/


// PERMET DE GENERER LES DOCS PDF ASSOCIE AUX STAGIAIRE D'UN FORMATION


// LA MAJ EST GERE VIA UN AUTRE SCRIPT AJAX

$erreur_txt="";

if(!empty($_POST)){
	
	$action_id=0;
	if (isset($_POST['action'])){
		$action_id=intval($_POST['action']);	
	}
	if(empty($action_id)){
		$erreur_txt="Formulaire incomplet!";
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}

/***********************************
			CONTROLE
***********************************/

if(empty($erreur_txt)){

	// personne connecte
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	if($acc_societe!=$conn_soc_id){
		$acc_societe=$conn_soc_id;
	}

	$retour=array(
		"attestations" => array(),
		"convocations" => array()
	);
	
	// ON PARCOURCES LES INSCRITS
	
	$req=$ConnSoc->prepare("SELECT * FROM Actions_Stagiaires WHERE ast_action=:action_id;");
	$req->bindParam(":action_id",$action_id);
	$req->execute();
	$d_stagiaires=$req->fetchAll();
	if(!empty($d_stagiaires)){
		foreach($d_stagiaires as $sta){
			
			if(!empty($_POST["attest_" . $sta["ast_stagiaire"]])){
				$data=array();
				$data[]=stagiaire_attestation_data($sta);
				if(!empty($data)){
					$pdf=model_stagiaire_attestation_pdf($data);
					if($pdf){						
						$retour["attestations"][]=$sta["ast_stagiaire"];	
					}
				}
			}

			if(!empty($_POST["convoc_" . $sta["ast_stagiaire"]])){						
				$data=convocation_data($sta["ast_action_client"],$sta["ast_stagiaire"]);
				if(!empty($data)){
					$pdf=model_convocation_pdf($data);
					if($pdf){						
						$retour["convocations"][]=$sta["ast_stagiaire"];	
					}
				}
			}
		}				
	}else{
		$erreur_txt="Impossible de charger la liste des stagiaires!";
	}
}

if(empty($erreur_txt)){
	echo json_encode($retour);
}
?>
