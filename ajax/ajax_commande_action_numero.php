<?php 

include("../includes/connexion.php");
include_once('../connexion_soc.php');
include_once('../modeles/mod_parametre.php');

if(!empty($_POST['action'])){
	$req = $ConnSoc->prepare("SELECT act_date_deb, act_intervenant, act_id FROM actions WHERE act_id=" . $_POST['action']);
	$req->execute();
	$actions = $req->fetch();
	
	if(!empty($actions)){


		$req = $ConnSoc->prepare("SELECT int_label_1, int_label_2 FROM intervenants WHERE int_id = " . $actions['act_intervenant']);
		$req->execute();
		$intervenant = $req->fetch();
		$retour = array(
			"act_id" => $actions['act_id'],
			"act_date_deb" => convert_date_txt($actions['act_date_deb']),
			"act_intervenant" => $intervenant['int_label_2'] . " " . $intervenant['int_label_1']
		);
	
		echo json_encode($retour);
	}else{
		echo 0;
	}
}else{
	echo 0;
}



?>