<?php

include("../includes/connexion.php");
include("../includes/connexion_soc.php");
include('../modeles/mod_check_siret.php');
include('../modeles/mod_check_suspect_siret.php');


/* LES echo() declenche une erreur (json attendu)
	=> declenche une modal_alert_user
*/

if(isset($_POST)){

	$siren="";
	if(!empty($_POST['siren'])){
		$siren=$_POST['siren'];
	}else{
		echo("Impossible de tester le siret.");
		die();
	}
	
	$siret="";
	if(!empty($_POST['siret'])){
		$siret=$_POST['siret'];
	}else{
		echo("Impossible de tester le siret.");
		die();
	}
	
	$adresse=0;
	if(!empty($_POST['adresse'])){
		$adresse=intval($_POST['adresse']);
	}
	
	// on commence par tester le siret dans la base client
	
	$client_existe=check_siret($siren,$siret,0);
	if(!empty($client_existe)){
		echo("Le siret est déjà utilisé dans la base client.");
		die();
	}else{
		$suspect_existe=check_suspect_siret($siren,$siret,$adresse);	
		if(!empty($suspect_existe)){
			if($suspect_existe["doublon"]){
				echo("Le siret est déjà utilisé dans la base suspect.");
				die();
			}
		}
	}
	echo json_encode("OK");
	die();
}else{
	echo("Impossible de tester le siret.");
	die();
}
?>