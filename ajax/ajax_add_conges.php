<?php
include("../includes/connexion.php");
include('../includes/connexion_soc.php');

include('../includes/connexion_soc.php');
include("../modeles/mod_envoi_mail.php");
include("../modeles/mod_parametre.php");

// ENREGISTREMENT D'UNE CASE PLANNING

// format de retour
$case_maj=array(
	"cases" =>array(
		"type" =>"2",
		"ref_1" => "0",
		"texte" => "",
		"style" => "",
		"survol" => "",
		"date" => array(),
		"erreur" => 0,
		"spe_ref_1" => array(),
		"spe_style" => array()
	),
	"erreur_type" => 0,
	"erreur_code" => 0,
	"erreur_text" => ""
);

// gestion des erreurs
$erreur_type=0;
$erreur_text="";
$erreur_code=0;

$type_conges=0;
if(isset($_GET['type_conges'])){
	if(!empty($_GET['type_conges'])){
		$type_conges=intval($_GET['type_conges']);
	}
}
if($type_conges>0&&isset($_GET['liste_case'])){

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}

	$req=$Conn->prepare("SELECT * FROM Conges_Categories WHERE cca_id=:type_conges;");
	$req->bindParam(":type_conges",$type_conges);
	$req->execute();
	$info_conges= $req->fetch();
	if(empty($info_conges)){
		$erreur_type=2;
		$erreur_text="Impossible de récupérer les informations sur le type de congés.";
	}else{
		// le libelle sera vrai pour toutes les cases
		$case_maj["cases"]["texte"]=$info_conges["cca_libelle"];
	}

	if(empty($erreur_text)){

		//*********************************************
		// 		PREP DES REQUETES
		//*********************************************

		$sql_uti_get="SELECT uti_mail, uti_matricule,uti_societe,uti_agence,uti_responsable,uti_nom,uti_prenom FROM Utilisateurs WHERE uti_id=:uti_id;";
		$req_uti_get=$Conn->prepare($sql_uti_get);

		$sql_uti_drt_get="SELECT udr_utilisateur FROM Utilisateurs_Droits WHERE udr_utilisateur=:udr_utilisateur AND udr_droit=:udr_droit;";
		$req_uti_drt_get=$Conn->prepare($sql_uti_drt_get);

		$sql_con_check="SELECT con_id FROM Conges WHERE con_annul = 0 AND con_uti_valide != 2 AND con_resp_valide != 2 AND con_utilisateur=:con_utilisateur
		AND (con_date_fin>:con_date_deb OR (con_date_fin=:con_date_deb AND con_demi_fin>=:con_demi_deb) )
		AND (con_date_deb<:con_date_fin OR (con_date_deb=:con_date_fin AND con_demi_deb<=:con_demi_fin) )";
		$req_con_check=$Conn->prepare($sql_con_check);


		$sql_case_check="SELECT pda_id FROM Plannings_Dates,Intervenants WHERE pda_intervenant=int_id AND int_type=1 AND int_ref_1=con_utilisateur
		AND (pda_date>:con_date_deb OR (pda_date=:con_date_deb AND pda_demi>=:con_demi_deb) )
		AND (pda_date<:con_date_fin OR (pda_date=:con_date_fin AND pda_demi<=:con_demi_fin) )";
		$req_case_check=$Conn->prepare($sql_con_check);


		$sql_conges_add="INSERT INTO Conges (
		con_societe,
		con_agence,
		con_utilisateur,
		con_uti_valide,
		con_uti_valide_date,
		con_responsable,
		con_resp_nom,
		con_resp_prenom,
		con_resp_valide,
		con_resp_valide_date,
		con_nom,
		con_prenom,
		con_matricule,
		con_categorie,
		con_date_deb,
		con_demi_deb,
		con_date_fin,
		con_demi_fin,
		con_nb_jour,
		con_creation_uti,
		con_creation_date) VALUES (
		:con_societe,
		:con_agence,
		:con_utilisateur,
		:con_uti_valide,
		:con_uti_valide_date,
		:con_responsable,
		:con_resp_nom,
		:con_resp_prenom,
		:con_resp_valide,
		:con_resp_valide_date,
		:con_nom,
		:con_prenom,
		:con_matricule,
		:con_categorie,
		:con_date_deb,
		:con_demi_deb,
		:con_date_fin,
		:con_demi_fin,
		:con_nb_jour,
		:con_creation_uti,
		NOW());";
		$req_conges_add=$Conn->prepare($sql_conges_add);

		$sql_case_add="INSERT INTO Plannings_Dates (
		pda_intervenant,
		pda_date,
		pda_demi,
		pda_type,
		pda_ref_1,
		pda_texte,
		pda_style_bg,
		pda_confirme) VALUES (
		:pda_intervenant,
		:pda_date,
		:pda_demi,
		2,
		:pda_ref_1,
		:pda_texte,
		:pda_style_bg,
		:pda_confirme);";
		$req_case_add=$ConnSoc->prepare($sql_case_add);

		//*********************************************
		// 		FONCTION CREATION DE CONGES
		//*********************************************

		function enregistrer_conges($data_conges,$conges_dates){

			// connexion et requete
			global $Conn;
			global $ConnSoc;
			global $req_con_check;
			global $req_case_check;
			global $req_conges_add;
			global $req_case_add;

			// parametre
			global $acc_utilisateur;
			global $info_conges;
			global $erreur_type;
			global $erreur_code;
			global $erreur_text;

			// donnee retour
			global $case_maj;

			$err=0;

			// ON VERIFIE QU4IL N'Y A PAS DE CONGES SUR LA PERIODE

			$req_con_check->bindParam(":con_utilisateur",$data_conges["con_utilisateur"]);
			$req_con_check->bindParam(":con_date_deb",$data_conges["con_date_deb"]);
			$req_con_check->bindParam(":con_demi_deb",$data_conges["con_demi_deb"]);
			$req_con_check->bindParam(":con_date_fin",$data_conges["con_date_fin"]);
			$req_con_check->bindParam(":con_demi_fin",$data_conges["con_demi_fin"]);
			$req_con_check->execute();
			$d_con_existe=$req_con_check->fetchAll();
			if(!empty($d_con_existe)){
				$erreur_type=2;
				$erreur_code=2;
				$erreur_text.="Il y a déjà des congés enregistrés pour " . $data_conges["con_prenom"] . " " . $data_conges["con_nom"] . "
				sur la période du " . $data_conges["con_date_deb"] . " au " . $data_conges["con_date_fin"] . "<br/>";

				$err=1;
			}

			// ON VERIFIE QUE LE PLANNING EST LIBRE
			if(empty($err)){

				$req_case_check->bindParam(":con_utilisateur",$data_conges["con_utilisateur"]);
				$req_case_check->bindParam(":con_date_deb",$data_conges["con_date_deb"]);
				$req_case_check->bindParam(":con_demi_deb",$data_conges["con_demi_deb"]);
				$req_case_check->bindParam(":con_date_fin",$data_conges["con_date_fin"]);
				$req_case_check->bindParam(":con_demi_fin",$data_conges["con_demi_fin"]);
				$req_case_check->execute();
				$d_case_existe=$req_case_check->fetchAll();
				if(!empty($d_case_existe)){

					$err=1;

					$erreur_type=2;
					$erreur_code=2;
					$erreur_text.="Le planning de " . $data_conges["con_prenom"] . " " . $data_conges["con_nom"] . "
					n'est pas libre sur la période du " . $data_conges["con_date_deb"] . " au " . $data_conges["con_date_fin"] . "<br/>";
				}
			}
			// ON VERIFIE QUE L'UTILISATEUR A LE DROIT
			$sql="SELECT uti_responsable, uti_agence, uti_societe FROM utilisateurs WHERE uti_id=" . $data_conges["con_utilisateur"];
			$req = $Conn->query($sql);
			$utilisateur_actif = $req->fetch();

			$sql = "SELECT uti_id FROM utilisateurs
			LEFT JOIN utilisateurs_societes ON (utilisateurs_societes.uso_utilisateur = utilisateurs.uti_id)
			WHERE uso_agence = " . $utilisateur_actif['uti_agence'] . " AND uso_societe = " . $utilisateur_actif['uti_societe'] . " AND uti_profil = 15 AND uti_archive = 0";
			$req = $Conn->query($sql);
			$chef_agence = $req->fetch();

			$continuer = false;

			if ($_SESSION['acces']['acc_ref_id'] == $data_conges["con_utilisateur"] && !empty($info_conges['cca_user'])) {
				$continuer = true;
			}

			if ($_SESSION['acces']['acc_ref_id'] == $utilisateur_actif['uti_responsable'] && !empty($info_conges['cca_resp'])) {
				$continuer = true;
			}

			if ($_SESSION['acces']['acc_ref_id'] == $chef_agence['uti_id'] && !empty($info_conges['cca_ra'])) {
				$continuer = true;
			}

			if (!$continuer) {
				$err=1;
				$erreur_type=2;
				$erreur_code=2;
				$erreur_text.="Vous n'avez pas les droits pour poser des congés de ce type.";
			}

			// TRAITEMENT
			if(empty($err)){

				// ENREGISTREMENT DE LA DEMANDE

				$nb_jour=count($conges_dates)/2;

				$req_conges_add->bindParam(":con_societe",$data_conges["con_societe"]);
				$req_conges_add->bindParam(":con_agence",$data_conges["con_agence"]);
				$req_conges_add->bindParam(":con_utilisateur",$data_conges["con_utilisateur"]);
				$req_conges_add->bindParam(":con_uti_valide",$data_conges["con_uti_valide"]);
				$req_conges_add->bindParam(":con_uti_valide_date",$data_conges["con_uti_valide_date"]);
				$req_conges_add->bindParam(":con_responsable",$data_conges["con_responsable"]);
				$req_conges_add->bindParam(":con_resp_nom",$data_conges["con_resp_nom"]);
				$req_conges_add->bindParam(":con_resp_prenom",$data_conges["con_resp_prenom"]);
				$req_conges_add->bindParam(":con_resp_valide",$data_conges["con_resp_valide"]);
				$req_conges_add->bindParam(":con_resp_valide_date",$data_conges["con_resp_valide_date"]);
				$req_conges_add->bindParam(":con_nom",$data_conges["con_nom"]);
				$req_conges_add->bindParam(":con_prenom",$data_conges["con_prenom"]);
				$req_conges_add->bindParam(":con_matricule",$data_conges["con_matricule"]);
				$req_conges_add->bindParam(":con_categorie",$info_conges["cca_id"]);
				$req_conges_add->bindParam(":con_date_deb",$data_conges["con_date_deb"]);
				$req_conges_add->bindParam(":con_demi_deb",$data_conges["con_demi_deb"]);
				$req_conges_add->bindParam(":con_date_fin",$data_conges["con_date_fin"]);
				$req_conges_add->bindParam(":con_demi_fin",$data_conges["con_demi_fin"]);
				$req_conges_add->bindParam(":con_nb_jour",$nb_jour);
				$req_conges_add->bindParam(":con_creation_uti",$acc_utilisateur);
				try{
					$req_conges_add->execute();
					$conges_id=$Conn->lastInsertId();
				}Catch(Exception $e){
					$err=2;
					$erreur_type=2;
					$erreur_text.="ADD conges : " . $e->getMessage() . "<br/>";
				}
			}


			if(empty($err)){

				$pda_confirme=0;
				if($data_conges["con_resp_valide"]!=0 AND $data_conges["con_uti_valide"]!=0){
					$pda_confirme=1;
				}
				$case_style="background-color:" . $info_conges["cca_couleur_bg"] . ";";
				if(empty($pda_confirme)){
					$case_style.="color:#FFF;";
				}else{
					$case_style.="color:#000;";
				}

				foreach($conges_dates as $cd){

					$Dt_date=DateTime::createFromFormat('Y-m-d',$cd[1]);

					if($Dt_date->format("N")!="6" AND $Dt_date->format("N")!="7"){

						$req_case_add->bindParam(":pda_intervenant",$cd[0]);
						$req_case_add->bindParam(":pda_date",$cd[1]);
						$req_case_add->bindParam(":pda_demi",$cd[2]);
						$req_case_add->bindParam(":pda_ref_1",$conges_id);
						$req_case_add->bindParam(":pda_texte",$info_conges["cca_libelle"]);
						$req_case_add->bindParam(":pda_style_bg",$info_conges["cca_couleur_bg"]);
						$req_case_add->bindParam(":pda_confirme",$pda_confirme);
						try{
							$req_case_add->execute();
						}Catch(Exception $e){

							$err=3;
							$erreur_type=2;
							$erreur_text.="ADD DATE : " . $e->getMessage() . "<br/>";


						}

						if(empty($err)){
							$case_maj["cases"]["date"][]=$cd[0] . "_" . $cd[1] . "_" . $cd[2];
							$case_maj["cases"]["spe_ref_1"][]=$conges_id;
							$case_maj["cases"]["spe_style"][]=$case_style;
						}else{
							break;
						}
					}
				}
				// ENVOI DU MAIL
				// LE CE
				$sql="SELECT * FROM utilisateurs
				LEFT JOIN utilisateurs_societes ON (utilisateurs_societes.uso_utilisateur = utilisateurs.uti_id)
				WHERE uso_agence = " . $data_conges["con_agence"] . " AND uso_societe = " . $data_conges["con_societe"] . " AND uti_profil = 15 AND uti_archive = 0";
				$req = $Conn->query($sql);
				$chef_agence = $req->fetch();

				// NOTIFICATIONS A AFFICHER
				$param_mail=array(
					"sujet" => "ORION : Demande de congés pour " . $data_conges["con_prenom"] . " " . $data_conges["con_nom"],
					"identite" => "ORION",
					"message" => "Une demande de congés a été faite depuis le planning pour la période du " . convert_date_txt($data_conges["con_date_deb"]) . " au " . convert_date_txt($data_conges["con_date_fin"])
				);

				$adr_mail=array(
					"0" => array(
						"adresse" => $data_conges["con_resp_mail"],
						"nom" => $data_conges["con_resp_prenom"] . " " . $data_conges["con_resp_nom"]
					),
				);
				$adr_mail=array(
					"1" => array(
						"adresse" => $data_conges["con_mail"],
						"nom" => $data_conges["con_prenom"] . " " . $data_conges["con_nom"]
					),
				);
				if(!empty($chef_agence)){
					$adr_mail["2"] = array(
						"adresse" => $chef_agence['uti_mail'],
						"nom" => $chef_agence['uti_prenom'] . " " . $chef_agence['uti_nom']
					);
				}

				envoi_mail("si2p",$param_mail,$adr_mail);
			}
		}



		// TRAITEMENT DES DATES SELECTIONNES

		$case=explode (",",$_GET['liste_case']);
		$intervenant=null;
		$date=null;
		$demi=null;
		$j_consecutif=true;

		// on parcours les dates selectionner pour identifier les différentes périodes et conc les différents congés à créer.
		foreach($case as $c){
			if(!empty($c)){

				$donnee_case=explode ("_",$c);

				$Dt_date=DateTime::createFromFormat('Y-m-d',$donnee_case[1]);

				if($Dt_date->format("N")!="6" AND $Dt_date->format("N")!="7"){

					if($intervenant!=$donnee_case[0]){

						if(!is_null($intervenant)){

							enregistrer_conges($data_conges,$conges_dates);

						}
						$data_conges=array();
						$data_conges["con_utilisateur"]=0;
						$data_conges["con_date_deb"]=$donnee_case[1];
						$data_conges["con_demi_deb"]=$donnee_case[2];
						$data_conges["con_date_fin"]=$donnee_case[1];
						$data_conges["con_demi_fin"]=$donnee_case[2];

						// memerise les case de plannings concerne par une demande de congés
						$conges_dates=array();
						$conges_dates[]=$donnee_case;

						$j_consecutif=true;

						$intervenant=$donnee_case[0];

						// on recupere les info de l'intervenant

						$sql_int_get="SELECT int_type,int_ref_1,int_label_1 FROM Intervenants WHERE int_id=:int_id;";
						$req_int_get=$ConnSoc->prepare($sql_int_get);
						$req_int_get->bindParam(":int_id",$intervenant);
						$req_int_get->execute();
						$d_intervenant=$req_int_get->fetch();

						if(!empty($d_intervenant)){

							if($d_intervenant["int_type"]==1){

								if(!empty($d_intervenant["int_ref_1"])){
									$data_conges["con_utilisateur"]=$d_intervenant["int_ref_1"];
								}else{
									$erreur_type=2;
									$erreur_text.="L'intervenant " . $d_intervenant["int_label_1"] . " n'est pas lié à un compte utilisateur.<br/>";
								}

							}else{
								$erreur_type=2;
								$erreur_text.="L'intervenant " . $d_intervenant["int_label_1"] . " n'est pas un formateur interne.<br/>";
							}

						}else{
							$erreur_text.="Impossible de charger les données de l'intervenant " . $intervenant . "<br/>";
							$erreur_type=2;
						}

						// ON RECUPERE LES INFOS SUR L'UTILISATEUR
						if(!empty($data_conges["con_utilisateur"])){

							$req_uti_get->bindParam(":uti_id",$data_conges["con_utilisateur"]);
							$req_uti_get->execute();
							$d_utilisateur=$req_uti_get->fetch();
							if(empty($d_utilisateur)){
								$erreur_type=2;
								$erreur_text.="Impossible de charger les données du compte utilisateur de l'intervenant " . $d_intervenant["int_label_1"] . ".<br/>";
								$data_conges["con_utilisateur"]=0;
							}else{
								$data_conges["con_societe"]=$d_utilisateur["uti_societe"];
								$data_conges["con_agence"]=$d_utilisateur["uti_agence"];
								$data_conges["con_matricule"]=$d_utilisateur["uti_matricule"];
								$data_conges["con_nom"]=$d_utilisateur["uti_nom"];
								$data_conges["con_prenom"]=$d_utilisateur["uti_prenom"];
								$data_conges["con_mail"]=$d_utilisateur["uti_mail"];
								$data_conges["con_responsable"]=$d_utilisateur["uti_responsable"];
							}
						}

						// on recupere les infos sur le responsable
						if(!empty($data_conges["con_utilisateur"])){

							$req_uti_get->bindParam(":uti_id",$d_utilisateur["uti_responsable"]);
							$req_uti_get->execute();
							$d_responsable=$req_uti_get->fetch();
							if(empty($d_responsable)){
								$erreur_type=2;
								$erreur_text.="Le responsable de " . $d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"] . " n'a pas pû êter identifié.<br/>";
								$data_conges["con_utilisateur"]=0;
							}else{
								$data_conges["con_resp_nom"]=$d_responsable["uti_nom"];
								$data_conges["con_resp_prenom"]=$d_responsable["uti_prenom"];
								$data_conges["con_resp_mail"]=$d_responsable["uti_mail"];
							}
						}
						if(!empty($data_conges["con_utilisateur"])){

							$data_conges["con_uti_valide"]=0;
							if(!empty($info_conges["cca_validation"])){
								$data_conges["con_uti_valide"]=3;
								$data_conges["con_uti_valide_date"]=date("Y-m-d");
							}elseif($acc_utilisateur==$data_conges["con_utilisateur"]){
								$data_conges["con_uti_valide"]=1;
								$data_conges["con_uti_valide_date"]=date("Y-m-d");
							}

							$data_conges["con_resp_valide"]=0;
							$data_conges["con_resp_valide_date"]=null;
							if($acc_utilisateur==$d_utilisateur["uti_responsable"]){
								$data_conges["con_resp_valide"]=1;
								$data_conges["con_resp_valide_date"]=date("Y-m-d");
							}
						}


					}elseif(!empty($data_conges["con_utilisateur"])){

						if($donnee_case[2]==2){
							if($data_conges["con_demi_fin"]==1 AND $data_conges["con_date_fin"]==$donnee_case[1]){
								$data_conges["con_demi_fin"]=2;
								$conges_dates[]=$donnee_case;
							}else{
								$j_consecutif=false;
							}
						}else{
							if($data_conges["con_demi_fin"]==2){

								$DT_periode=date_create_from_format('Y-m-d',$donnee_case[1]);
								$DT_periode_prec=date_create_from_format('Y-m-d',$data_conges["con_date_fin"]);
								$diff=$DT_periode->diff($DT_periode_prec);

								if($diff->format("%d")==1 OR ($diff->format("%d")==3 AND $DT_periode->format("w")==1)){
									$data_conges["con_date_fin"]=$donnee_case[1];
									$data_conges["con_demi_fin"]=$donnee_case[2];
									$conges_dates[]=$donnee_case;
								}else{
									$j_consecutif=false;
								}
							}else{
								$j_consecutif=false;
							}
						};

						if(!$j_consecutif){
							enregistrer_conges($data_conges,$conges_dates);

							$data_conges["con_date_deb"]=$donnee_case[1];
							$data_conges["con_demi_deb"]=$donnee_case[2];
							$data_conges["con_date_fin"]=$donnee_case[1];
							$data_conges["con_demi_fin"]=$donnee_case[2];

							// memerise les case de plannings concerne par une demande de congés
							$conges_dates=array();
							$conges_dates[]=$donnee_case;

							$j_consecutif=true;

						}
					}
				}
			}
		}
		enregistrer_conges($data_conges,$conges_dates);

	}

}else{
	$erreur_type=1;
	$erreur_text="Paramètres absents";
}
$case_maj["erreur_type"]=$erreur_type;
$case_maj["erreur_text"]=$erreur_text;
$case_maj["erreur_code"]=$erreur_code;
echo json_encode($case_maj);
?>
