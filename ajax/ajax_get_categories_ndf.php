<?php

include("../includes/connexion.php");

if(isset($_POST['type'])){

	$id=0;
	if(!empty($_POST['type'])){
		$id=$_POST['type'];
	}

    global $Conn;

    $req = $Conn->prepare("SELECT * FROM ndf_categories");
    $req->execute();
	$cats = $req->fetchAll();

    foreach($cats as $c){
    	 $req = $Conn->prepare("SELECT * FROM ndf_categories_profils WHERE ncp_type = $id AND ncp_profil = " . $_POST['profil'] . " AND ncp_categorie = " . $c['nca_id']);
		 $req->execute();
		 $cate = $req->fetch();

		if(!empty($cate['ncp_acces']) && $cate['ncp_acces'] == 1){
			$categories[] = array(
				"nca_id" => $c['nca_id'],
				"nca_libelle" => $c['nca_libelle'],
				"nca_vehicule" => $c['nca_vehicule'],
			);
		}
	}

    if(empty($categories)){
    	$categories=array();
    }
    echo json_encode($categories);

}