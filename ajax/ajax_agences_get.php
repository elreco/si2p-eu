<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

// RETOURNE LES SOUS-FAMILLES ASSOCIES à UNE FAMILLE

// retour => tableau JSON (compatible Select2)


$erreur=0;
$agences=array();

if(isset($_POST)){

	$societe=0;
	if(!empty($_POST["societe"])){
		$societe=intval($_POST["societe"]);
	}else{
		$erreur=1;
	}

	$acc_drt=0;
	if(!empty($_POST["acc_drt"])){
		$acc_drt=$_POST["acc_drt"];
	}

	$age_interco=0;
	if(!empty($_POST["age_interco"])){
		$age_interco=intval($_POST["age_interco"]);
	}

	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}


	if($erreur==0){
		if(!empty($acc_drt)){
			$sql="SELECT age_id,age_nom,age_code FROM Agences,Utilisateurs_Societes WHERE age_id=uso_agence AND age_societe=" . $societe . " AND uso_utilisateur=" . $acc_utilisateur;
		}else{
			$sql="SELECT age_id,age_nom,age_code FROM Agences WHERE age_societe=" . $societe;
		}
		if(!empty($age_interco)){
			$sql.=" AND NOT age_id=" . $age_interco;
		}
		$sql.=" ORDER BY age_nom;";
		$req=$Conn->query($sql);
		$results=$req->fetchAll();

		if(!empty($results)){
			foreach($results as $r){
				$agences[]=array(
					"id" => $r["age_id"],
					"text" => $r["age_nom"],
					"age_code" => $r["age_code"],
				);
			}
		}
	}
}else{
	$erreur=10;
}
if($erreur==0){
	echo json_encode($agences);
}else{
	//header("HTTP/1.0 500 " . $erreur);
};
?>
