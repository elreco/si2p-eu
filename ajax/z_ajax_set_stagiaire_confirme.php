<?php
include "../includes/controle_acces.inc.php";
include '../connexion_soc.php';

 // RETOURNE LES INFOS D'UNE ACTION ENREGISTRE DANS LA TABLE ACTIONS
 
 // RETOUR FORMAT JSON
 
$erreur=0;
if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]); 
	}	 
	
	$stagiaire_id=0;
	if(!empty($_POST["stagiaire"])){
		$stagiaire_id=intval($_POST["stagiaire"]); 
	}

		
	$checked=0;
	if(!empty($_POST["checked"])){
		if($_POST["checked"]=="true"){
			$checked=1; 
		}
	}
	
	if($action_id!=0 AND $stagiaire_id!=0){
	
		// SUR L'ACTION DE REF
		
		$sql="UPDATE Actions_Stagiaires SET ast_confirme=:checked WHERE ast_action=:action AND ast_stagiaire=:stagiaire;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":checked",$checked);
		$req->bindParam(":action",$action_id);
		$req->bindParam(":stagiaire",$stagiaire_id);
		$req->execute();
		$err_code=$ConnSoc->errorCode();
		if($err_code!=="00000"){
			$erreur=12;
		}
	}else{
		$erreur=11;
	}
}else{
	$erreur=10;
}
		
if($erreur!=0){
	header("HTTP/1.0 500 ");
}
 
 
?>