<?php
include "../includes/controle_acces.inc.php";
include '../connexion_soc.php';
include '../includes/connexion.php';
include '../connexion_fct.php';

 // CONFIRME OU ANNUL CONFIRMATION D'UNE ACTION
 
$erreur=0;
if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]); 
	}	 
	
	if($action_id!=0){
		
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];	
		}
		
		// INFO SUR L'ACTION
		
		$sql="SELECT act_agence FROM Actions WHERE act_id=" . $action_id . ";";
		$req=$ConnSoc->query($sql);
		$action=$req->fetch();
	
		
		// TRAITEMENT DU FORM
		
		$act_adr_ref=0;
		if(!empty($_POST["act_adr_ref"])){
			$act_adr_ref=intval($_POST["act_adr_ref"]);	
		}

		// GESTION DE L'ADRESSE
		
		$act_adr_societe=0;
		$act_adresse=0;
		$act_adr_ref_id=0;
		
		$act_contact=0;
		$act_con_ref=0;
		$act_con_ref_id=0;
		
		switch ($act_adr_ref) {
			case 1:
			
				// adresse client 
				
				$sql="SELECT adr_ref_id FROM Adresses WHERE adr_id=" . $act_adresse . ";";
				
				if(!empty($_POST["act_adresse"])){
					$act_adresse=intval($_POST["act_adresse"]);	
				}
				if(!empty($_POST["act_adr_societe"])){
					$act_adr_societe=intval($_POST["act_adr_societe"]); 
					$ConnFct=connexion_fct($societe);
					$req=$ConnFct->query($sql);
				}else{
					$req=$ConnSoc->query($sql);
				}
				$result=$req->fetch();
				if(!empty($result)){
					$act_adr_ref_id=$result["adr_ref_id"];
				}
				
				$act_adr_nom=$_POST["act_adr_nom"];	
				$act_adr_service=$_POST["act_adr_service"];	
				$act_adr1=$_POST["act_adr1"];	
				$act_adr2=$_POST["act_adr2"];	
				$act_adr3=$_POST["act_adr3"];	
				$act_adr_cp=$_POST["act_adr_cp"];	
				$act_adr_ville=$_POST["act_adr_ville"];	
				
				// gestion d'un contact lié au client
				if(!empty($_POST["act_contact"])){
					$act_contact=intval($_POST["act_contact"]);	
					if($act_contact>0){
						$act_con_ref=1;
						$act_con_ref_id=$result["adr_ref_id"];
					}
				}
				
				break;
				
			case 2:			
				// agence
				if($ation["act_agence"]>0){
					// adresse d'agence					
					$sql="SELECT age_nom,age_ad1,age_ad2,age_ad3,age_cp,age_ville FROM Societes WHERE age_id=" . $ation["act_agence"] . ";";
					$req=$ConnSoc->query($sql);
					$societe=$req->fetch();
					if(!empty($societe)){
						$act_adr_nom=$societe["soc_nom"];	
						$act_adr_service="";	
						$act_adr1=$societe["soc_ad1"];	
						$act_adr2=$societe["soc_ad2"];	
						$act_adr3=$societe["soc_ad3"];	
						$act_adr_cp=$societe["soc_cp"];	
						$act_adr_ville=$societe["soc_ville"];	
					}
				}else{
					// adresse de societe
					$sql="SELECT soc_nom,soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville FROM Societes WHERE soc_id=" . $acc_societe . ";";
					$req=$ConnSoc->query($sql);
					$societe=$req->fetch();
					if(!empty($societe)){
						$act_adr_nom=$societe["soc_nom"];	
						$act_adr_service="";	
						$act_adr1=$societe["soc_ad1"];	
						$act_adr2=$societe["soc_ad2"];	
						$act_adr3=$societe["soc_ad3"];	
						$act_adr_cp=$societe["soc_cp"];	
						$act_adr_ville=$societe["soc_ville"];	
					}
				}
				break;
				
			case 3:
				// autre
				$act_adr_nom=$_POST["act_adr_nom"];	
				$act_adr_service=$_POST["act_adr_service"];	
				$act_adr1=$_POST["act_adr1"];	
				$act_adr2=$_POST["act_adr2"];	
				$act_adr3=$_POST["act_adr3"];	
				$act_adr_cp=$_POST["act_adr_cp"];	
				$act_adr_ville=$_POST["act_adr_ville"];	
				break;
		}
		
		// GESTION DU CONTACT
		$act_con_nom=$_POST["act_con_nom"];	
		$act_con_prenom=$_POST["act_con_prenom"];	
		$act_con_tel=$_POST["act_con_tel"];	
		$act_con_portable=$_POST["act_con_portable"];	
		
		
	
		$sql="UPDATE Actions SET 
		act_adr_ref=:act_adr_ref,
		act_adr_ref_id=:act_adr_ref_id,
		act_adr_societe=:act_adr_societe,
		act_adresse=:act_adresse,
		act_adr_nom=:act_adr_nom,
		act_adr_service=:act_adr_service,
		act_adr1=:act_adr1,
		act_adr2=:act_adr2,
		act_adr3=:act_adr3,
		act_adr_cp=:act_adr_cp,
		act_adr_ville=:act_adr_ville,
		
		act_contact=:act_contact,
		act_con_ref=:act_con_ref,
		act_con_ref_id=:act_con_ref_id,
		act_con_nom=:act_con_nom,
		act_con_prenom=:act_con_prenom,
		act_con_tel=:act_con_tel,
		act_con_portable=:act_con_portable
		
		WHERE act_id=:action;";
		$req=$ConnSoc->prepare($sql);
		
		$req->bindParam(":act_adr_ref",$act_adr_ref);
		$req->bindParam(":act_adr_ref_id",$act_adr_ref_id);
		$req->bindParam(":act_adr_societe",$act_adr_societe);
		$req->bindParam(":act_adresse",$act_adresse);	
		$req->bindParam(":act_adr_nom",$act_adr_nom);
		$req->bindParam(":act_adr_service",$act_adr_service);
		$req->bindParam(":act_adr1",$act_adr1);
		$req->bindParam(":act_adr2",$act_adr2);
		$req->bindParam(":act_adr3",$act_adr3);
		$req->bindParam(":act_adr_cp",$act_adr_cp);
		$req->bindParam(":act_adr_ville",$act_adr_ville);
		$req->bindParam(":act_contact",$act_contact);
		$req->bindParam(":act_con_ref",$act_con_ref);
		$req->bindParam(":act_con_ref_id",$act_con_ref_id);
		$req->bindParam(":act_con_nom",$act_con_nom);
		$req->bindParam(":act_con_prenom",$act_con_prenom);
		$req->bindParam(":act_con_tel",$act_con_tel);
		$req->bindParam(":act_con_portable",$act_con_portable);
		$req->bindParam(":action",$action_id);
		try{
			$req->execute();
		}catch (Exception $e) {
			 $erreur=1;
		}
	}else{
		$erreur=1;
	}
}else{
	$erreur=1;
}
	
if($erreur!=0){
	echo($erreur);	
}
 
 
?>