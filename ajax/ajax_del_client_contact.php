<?php
include("../includes/connexion.php");


// PERMET DE SUPPRIMER UN CONTACT

$erreur="";
if(isset($_POST)){

	$contact=0;
	if(!empty($_POST["contact"])){
		$contact=intval($_POST["contact"]);
	}
	
	$client=0;
	if(!empty($_POST["client"])){
		$client=intval($_POST["client"]);
	}

	if($contact>0 AND $client>0){
		
		// modele pas nécéssaire
		
		$sql="DELETE FROM Contacts WHERE con_id=" . $contact . " AND con_ref_id=" . $client . ";";
		try{
			$req=$Conn->query($sql);
		}catch (Exception $e){
			$erreur=$e->getMessage();
		}
		
	}else{
		$erreur="1-ERREUR PARAMETRE";
	}
}else{
	$erreur="2-ERREUR PARAMETRE";
}

if($erreur==""){
	
	$result_supp=array(
		"contact" => $contact
	);
	echo json_encode($result_supp);
	
}else{
	echo $erreur;
}
?>