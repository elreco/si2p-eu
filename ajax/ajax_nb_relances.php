<?php
 // RECHERCHE DE SUSPECT VIA SELECT2

session_start();
include('../includes/connexion.php');
include('../includes/connexion_fct.php');
$erreur="";

// DONNEE POUR TRAITEMENT

// sur la personne connecte
$sql="SELECT * FROM Relances_Etats ORDER BY ret_j_deb";
$req = $Conn->query($sql);
$d_results=$req->fetchAll();

if(!empty($_GET['type'])){

	// Pour chaque état de relance

    foreach($d_results as $k=>$r){

            if($_GET['type'] == 1){

				// EN FONCTION DE L'ETAPE
				// dans cette config la facture doit-être a l'étape precedente pour pouvoir etre selectionnee (pas de saut d'étape)

                if($k>0){

                   $etape_prec = $d_results[$k-1]['ret_id'];
                }
                $delai = -$r['ret_j_deb'];
                $fac_date_reg_prev_fin= date('Y-m-d', strtotime(date('Y-m-d') . $delai . ' days'));
                $fac_date_reg_prev_deb = null;

            }elseif($_GET['type'] == 2){

				// EN FONCTION DU RETARD

				$delai_debut = -$r['ret_j_deb'];
				$delai_fin = -$r['ret_j_fin'];
				$fac_date_reg_prev_fin = date('Y-m-d', strtotime(date('Y-m-d') . $delai_debut . ' days'));
				$fac_date_reg_prev_deb = date('Y-m-d', strtotime(date('Y-m-d') . $delai_fin . ' days'));

				// en mode retard, la facture ne doit pas encore etre sur l'étape demandée ni sur les suivantes
				$etape_relance=$r['ret_id'];
				foreach($d_results as $r2){
					if($r2['ret_id'] != $r['ret_id'] && $r2['ret_id']>$r['ret_id']){
						if(!empty($etape_relance)){
							$etape_relance = $etape_relance . "," . $r2["ret_id"];

						}else{
							$etape_relance = $r2["ret_id"];
						}
					}
				}
            }

            $sql = "SELECT DISTINCT fac_id, cli_id,fac_etat_relance,fac_date_reg_rel FROM Factures,Clients
            WHERE cli_id=fac_client
			AND ((fac_total_ttc>fac_regle AND fac_nature=1) )
			AND fac_nature != 2
		    AND NOT fac_relance_stop AND  (cli_interco_soc = 0 OR cli_interco_soc IS NULL)";

			// FG s'il faut réactiver ce criètre prevoir l'équivalent pour les avoirs.
			//$sql.= " AND fac_total_ttc-fac_regle > 0.5";

		   if(!empty($fac_date_reg_prev_deb)){
                $sql.=" AND ((fac_date_reg_prev >= :fac_date_reg_prev_deb AND ISNULL(fac_date_reg_rel))
                OR fac_date_reg_rel >= :fac_date_reg_prev_deb)";
            }
            if(!empty($fac_date_reg_prev_fin)){
                $sql.=" AND ((fac_date_reg_prev <= :fac_date_reg_prev_fin AND ISNULL(fac_date_reg_rel))
                OR fac_date_reg_rel <= :fac_date_reg_prev_fin)";
            }


            //if(!empty($etape[$r['ret_id']])){
			if($_GET['type'] == 1){

				// EN FONCTION DE L'ETAPE

				//if($etape[$r['ret_id']] != 0 And $etape[$r['ret_id']] != 1){
				if($r['ret_id']==2){
					// Première relance j+8 -> l'étape précédente peut etre releve ou rien si le client ne veut pas de relevé
					$sql = $sql . " AND (fac_etat_relance =0 OR fac_etat_relance=1 OR ISNULL(fac_etat_relance) ) ";
				}elseif($r['ret_id']==1){
					// relevé -> étape précedente forcément rien
					$sql = $sql . " AND (fac_etat_relance=0 OR ISNULL(fac_etat_relance)) ";
				}else{
					$sql = $sql . " AND fac_etat_relance =" . $etape_prec;
				}

			}elseif($_GET['type']==2 && !empty($etape_relance)){
				// EN FONCTION DU RETARD
					$sql=$sql . " AND fac_etat_relance NOT IN(" . $etape_relance . ")";
			}
           // }

            if(empty($_GET['societe'])){

                $count = 0;
                $sql_soc="SELECT soc_id,soc_nom, soc_code FROM Societes WHERE soc_archive=0 ORDER BY soc_nom;";
                $req_soc = $Conn->query($sql_soc);
                $d_societes=$req_soc->fetchAll();
                foreach($d_societes as $s){

                    $ConnFct = connexion_fct($s['soc_id']);
                    $req = $ConnFct->prepare($sql);
                    if(!empty($fac_date_reg_prev_deb)){
                        $req->bindParam("fac_date_reg_prev_deb",$fac_date_reg_prev_deb);
                    }
                    if(!empty($fac_date_reg_prev_fin)){
                        $req->bindParam("fac_date_reg_prev_fin",$fac_date_reg_prev_fin);
                    }
                    /*if(!empty($etape[$r['ret_id']])){
                        $req->bindParam("etape",$etape[$r['ret_id']]);
					}*/
					$req->execute();

                    $resu = $req->fetchAll();
                    $count = $count + count($resu);


                    if($_GET['type'] == 1){

						// EN FONCTION DE L'ETAPE

						if($r['ret_id'] == 1 OR $r['ret_id'] == 2){

							foreach($resu as $res){
								$sql_cli="SELECT cli_releve, cli_id FROM Clients WHERE cli_id = " . $res['cli_id'];
								$req_cli = $Conn->query($sql_cli);
								$d_client=$req_cli->fetch();
								if($r['ret_id']==1){
									// releve mais le client n'en veut pas on retire la facture
									if(empty($d_client['cli_releve'])){
										if($count > 0){
											$count = $count - 1;
										}
									}
								}else{
									// première relance -> le client veut un releve et on l'a pas fait -> on retire la facture
									if(!empty($d_client['cli_releve']) AND $res["fac_etat_relance"]!=1){
										if($count > 0){
											$count = $count - 1;

										}

									}
								}
							}
						}
					}elseif($_GET['type'] == 2){

						if($r['ret_id'] == 1){

							foreach($resu as $res){
								$sql_cli="SELECT cli_releve, cli_id FROM Clients WHERE cli_id = " . $res['cli_id'];
								$req_cli = $Conn->query($sql_cli);
								$d_client=$req_cli->fetch();
								// le retard nécéssite un releve mais le client n'en veut pas on retire la facture
								if(empty($d_client['cli_releve'])){
									if($count > 0){
										$count = $count - 1;
									}
								}
							}
						}
					}

                }

            }else{

				// U a filtré sur une societe

                $count = 0;
                $ConnFct = connexion_fct($_GET['societe']);
                $req = $ConnFct->prepare($sql);
                if(!empty($fac_date_reg_prev_deb)){
                    $req->bindParam("fac_date_reg_prev_deb",$fac_date_reg_prev_deb);
                }
                if(!empty($fac_date_reg_prev_fin)){
                    $req->bindParam("fac_date_reg_prev_fin",$fac_date_reg_prev_fin);
                }


               /* if(!empty($etape[$r['ret_id']])){
                    $req->bindParam("etape",$etape[$r['ret_id']]);
                }*/
                $req->execute();
                $resu = $req->fetchAll();
				$count = count($resu);

                if($_GET['type'] == 1){

					// en fonction de l'étape

					if($r['ret_id'] == 1 OR $r['ret_id'] == 2){
						foreach($resu as $res){
							$sql_cli="SELECT cli_releve, cli_id FROM Clients WHERE cli_id = " . $res['cli_id'];
							$req_cli = $Conn->query($sql_cli);
							$d_client=$req_cli->fetch();

							if($r['ret_id']==1){

								// U demande en fonction de l'étape relevé

								if(empty($d_client['cli_releve'])){
									if($count > 0){
										$count = $count - 1;
									}
								}

							}else{

								// U demande en fonction de l'étape R1
								if(!empty($d_client['cli_releve']) AND $res["fac_etat_relance"]!=1){
									// Le client veut un releve et il n'a pas été fait pour la facture
									if($count > 0){
										$count = $count - 1;
									}
								}
							}

						}
					}

				}elseif($_GET['type'] == 2){

					if($r['ret_id'] == 1){

						foreach($resu as $res){
							$sql_cli="SELECT cli_releve, cli_id FROM Clients WHERE cli_id = " . $res['cli_id'];
							$req_cli = $Conn->query($sql_cli);
							$d_client=$req_cli->fetch();
							// le retard nécéssite un releve mais le client n'en veut pas on retire la facture
							if(empty($d_client['cli_releve'])){
								if($count > 0){
									$count = $count - 1;
								}
							}
						}
					}
				}

            }

            $factures[$r['ret_id']]["count"] = $count;
            $factures[$r['ret_id']]["libelle"] = $r['ret_libelle'];
    }
}
if(!empty($factures)){
	foreach($factures as $k=>$f){
		$retour[] = array("id"=>$k, "nom"=>$f['libelle'] . " (" . $f['count'] . ")");
	}
}else{
    foreach($d_results as $d){
		$retour[] = array("id"=>$d['ret_id'], "nom"=>$d['ret_libelle']);
	}
}
// ENVOIE DU RESULTAT
if(!empty($retour)){
	echo json_encode($retour);
}
?>
