<?php
 // PERMET DE RECUPERER LES INFOS D'UN CLIENT RECU EN PARAMETRE
 
	include("../includes/connexion.php");
	
	// SCRIPT EN ATTENTE
	// CONFIRMATION AVANT SUPP
	// FABIEN

	$erreur=0;
	
	// LA PERSONNE CONNECTE
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	
	// FORMULAIRE
	$client=0;
	if(!empty($_POST['client'])){
		$client=$_POST['client'];
	}
	$produit=0;
	if(!empty($_POST['produit'])){
		$produit=$_POST['produit'];
	}else{
		$erreur=0;
	}
	
	if($erreur==0){
		
		// INFO GENERIQUE SUR LE PRODUIT
		
		$sql="SELECT pro_nb_demi_jour,pro_ht_intra,pro_ht_inter FROM Produits WHERE pro_id=:produit";
		$req = $Conn->prepare($sql);
		$req->bindParam(":produit",$produit);
		$req->execute();
		$p = $req->fetch();
		if(!empty($p)){
			$produit=array(
				"nb_demi_jour" => $p["pro_nb_demi_jour"],
				"ht_intra" => $p["pro_ht_intra"],
				"ht_inter" => $p["pro_ht_inter"]
			);
		}else{
			$erreur=0;
		}
		
		if($erreur==0){
			
			// DONNEES QUI DEPENDENT DU CLIENT
			
			if($client>0){
				
				$req = $ConnSoc->prepare("SELECT cli_categorie,cli_groupe,cli_filiale_de,cli_filiale_de_soc FROM Clients WHERE cli_id=:client;");
				$req->bindParam(":client",$client);
				$req->execute();
				$c=$req->fetch();
				if($c["cli_groupe"]&&$c["cli_filiale_de"]>0){
					// il s'agit d'une filiale 
					// et les produits sont renseignés sur la holding
					$conn_get_id=$c["cli_filiale_de_soc"];
					$produit_client=$c["cli_filiale_de"];
				}else{
					$conn_get_id=$acc_societe;
					$produit_client=$client;
				}
				include('../connexion_get.php');
			
				$sql="SELECT cpr_ht_intra,cpr_ht_inter FROM Clients_Produits WHERE cpr_client=:client AND cpr_produit=:produit";
				$req = $ConnGet->prepare($sql);
				$req->bindParam(":client",$produit_client);
				$req->bindParam(":produit",$produit);
				$req->execute();
				$p=$req->fetch();
				if(!empty($p)){
					$produits["ht_intra"]=$p["cpr_ht_intra"];
					$produits["ht_inter"]=$p["cpr_ht_inter"];
				};
			}
			
		}
	}
	
	if($erreur==0){
		echo json_encode($produit);
	}else{
		echo("erreur : " . $erreur);
	}
?>
