<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

 // RETOURNE LA LISTE DES ATTESTATIONS DISPONIBLE POUR UNE CLASSIFICATION DE PRODUIT (famillle,s famille, s s famille)

 // retour JSON compatible select2
 
 
$erreur_txt="";
if(isset($_POST)){
	 
	$famille=0;
	if(!empty($_POST["famille"])){
		$famille=intval($_POST["famille"]); 
	}	
	$sous_famille=0;
	if(!empty($_POST["sous_famille"])){
		$sous_famille=intval($_POST["sous_famille"]); 
	}
	$sous_sous_famille=0;
	if(!empty($_POST["sous_sous_famille"])){
		$sous_sous_famille=intval($_POST["sous_sous_famille"]); 
	}

	if(empty($famille) OR empty($sous_famille)){
		
		$erreur_txt="Impossible de récupérer les attestations";
		echo($erreur_txt);
		
	}else{
		
		$d_attestations=array();
		
		$sql="SELECT att_id,att_titre FROM Attestations WHERE att_statut=1 
		AND att_famille=". $famille . " AND att_sous_famille=" . $sous_famille . " AND att_sous_sous_famille=" . $sous_sous_famille . " 
		ORDER BY att_titre";
		$req=$Conn->query($sql);
		$results=$req->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$d_attestations[]=array(
					"id" => $r["att_id"],
					"text" => $r["att_titre"]
				);		
			}
		}
		
		echo json_encode($d_attestations);
		
	}
}else{
	$erreur_txt="Impossible de récupérer les attestations";
	echo($erreur_txt);
}
?>