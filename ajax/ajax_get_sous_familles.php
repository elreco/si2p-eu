<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

// RETOURNE LES SOUS-FAMILLES ASSOCIES à UNE FAMILLE

// retour => tableau JSON (compatible Select2)


$erreur=0;
$sous_familles=array();

if(isset($_POST)){
	 
	$famille=0;
	if(!empty($_POST["famille"])){
		$famille=intval($_POST["famille"]); 
	}	

	if($famille>0){
		
		$sql="SELECT * FROM Produits_Sous_Familles WHERE psf_pfa_id=" . $famille . ";";
		$req=$Conn->query($sql);	
		$results=$req->fetchAll();
		if(!empty($results)){
			foreach($results as $r){
				$sous_familles[]=array(
					"id" => $r["psf_id"],
					"text" => $r["psf_libelle"]
				);
			}
		}
	}
}else{
	echo("Erreur paramètre!");
}
if($erreur==0){
	echo json_encode($sous_familles);
};
?>
