<?php

include("../includes/connexion.php");

// achat pour revente OU sous-traitance
if($_POST['type'] == 1 OR $_POST['type'] == 2){

	if($_POST['type'] == 1){ // achat pour revente
		$req = $Conn->prepare("SELECT * FROM produits_categories WHERE pca_revente = 0 AND pca_sous_traitance = 1");
	    $req->execute();
	    $categories_sous_traitance = $req->fetchAll();
	    echo json_encode($categories_sous_traitance);
	}elseif($_POST['type'] == 2){ // sous_traitance
		$req = $Conn->prepare("SELECT * FROM produits_categories WHERE pca_revente = 1 AND pca_sous_traitance = 0");
	    $req->execute();
	    $categories_sous_traitance = $req->fetchAll();
	    echo json_encode($categories_sous_traitance);
	}


}else{
	echo 1; // ne pas afficher les autres select
}
