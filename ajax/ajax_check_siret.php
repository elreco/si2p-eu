<?php

include("../includes/connexion.php");
if(file_exists("../includes/connexion_soc.php")){
	include("../includes/connexion_soc.php");
}elseif(file_exists("includes/connexion_soc.php")){
	include("includes/connexion_soc.php");
}

include('../modeles/mod_check_siret.php');

// retour texte => declenche modal erreur avec aff echo()
// retour JSON gestion affichage sur page d'appel

if(isset($_POST)){
	

	$siren="";
	if(!empty($_POST['siren'])){
		$siren=$_POST['siren'];
	}else{
		echo("Impossible de tester le siret.");
		die();
	}
	
	$siret="";
	if(!empty($_POST['siret'])){
		$siret=$_POST['siret'];
	}else{
		echo("Impossible de tester le siret.");
		die();
	}
	
	$adresse=0;
	if(!empty($_POST['adresse'])){
		$adresse=intval($_POST['adresse']);
	}
	
	
	$test=check_siret($siren,$siret,$adresse);
	// si utilise test contient array() avec donnee utilisation
	if(!empty($test)){
		echo json_encode($test);
	}else{
		echo json_encode(array());	
	}

}
?>