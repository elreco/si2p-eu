<?php
 // RECHERCHE DE SUSPECT VIA SELECT2

session_start();
include('../includes/connexion.php');
include('../includes/connexion_soc.php');
include('../modeles/mod_parametre.php');
$erreur="";

// DONNEE POUR TRAITEMENT
$date = $_GET['date'];
$demi = $_GET['demi'];
$intervenant = $_GET['intervenant'];
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}

$d_actions_clients=array();
// for_action
// for_produit
// for_client
// IL FAUT QUE J'AILLE CHERCHER TOUS LES CLIENTS ET LES CODES PRODUITS
// LIÉS A LA DATE QUE JE SELECTIONNE
//
// Produit 1 - Client 1 - NbStagiaires 1
// Produit 2 - Client 2 - NbStagiaires 2
// Produit 2 - Client 3 - NbStagiaires 3
//
// for_nb_stagiaires = ?? // Aller remplir ==> acl_nb_sta_rap
//
$continue = true;
if($demi == 1.5){
	$sql="SELECT pda_ref_1, pda_type FROM plannings_dates  WHERE pda_intervenant = " . $intervenant . " AND pda_date = '" . $_GET['date'] . "' AND pda_archive=0";
	$req = $ConnSoc->query($sql);
	$dates=$req->fetchAll();

	if(!empty($dates[0]) && !empty($dates[1])){
		if($dates[0]['pda_ref_1'] != $dates[1]['pda_ref_1'] OR $dates[0]['pda_type'] != $dates[1]['pda_type']){
			$continue = false;
		}
	}else{
		// BLOQUER
		$continue = false;
	}

}

if($continue == true){
	if(!empty($_GET['formation'])){
		$sql="SELECT DISTINCT acl_id, acd_action_client, cli_id, cli_code, acl_produit, acl_action,acl_pro_reference, fac_nb_stagiaires FROM plannings_dates
		 LEFT JOIN actions_clients_dates ON actions_clients_dates.acd_date = plannings_dates.pda_id
		 LEFT JOIN actions_clients ON actions_clients.acl_id = actions_clients_dates.acd_action_client
		 LEFT JOIN clients ON actions_clients.acl_client = clients.cli_id";
		$sql = $sql . " LEFT JOIN formations_actions ON formations_actions.fac_action_client = actions_clients.acl_id ";
	}else{
		$sql="SELECT DISTINCT acl_id, acd_action_client, cli_id, cli_code, acl_produit, acl_action,acl_pro_reference FROM plannings_dates
		 LEFT JOIN actions_clients_dates ON actions_clients_dates.acd_date = plannings_dates.pda_id
		 LEFT JOIN actions_clients ON actions_clients.acl_id = actions_clients_dates.acd_action_client
		 LEFT JOIN clients ON actions_clients.acl_client = clients.cli_id";
	}
	if($demi == 1.5){
		$sql = $sql . " WHERE pda_intervenant = " . $intervenant . " AND pda_date = '" . $_GET['date'] . "' AND pda_archive=0 AND acl_archive=0";

	}else{
		$sql = $sql . " WHERE pda_intervenant = " . $intervenant . " AND pda_demi = " . $demi . " AND pda_date = '" . $_GET['date'] . "' AND pda_archive=0 AND acl_archive=0";

	}
	if(!empty($_GET['formation'])){
		$sql = $sql . " AND fac_formation = " . $_GET['formation'];
	}
	$req = $ConnSoc->query($sql);
	$d_actions_clients=$req->fetchAll();
	if(empty($d_actions_clients)){
		$retour = json_encode(2);
	}
}else{
	$retour = json_encode(1);
}




// echo("<pre>");
// print_r($d_actions_clients);
// echo("</pre>");
foreach($d_actions_clients as $acl){
	if(!isset($retour[$acl['acl_produit']])){

		$retour[$acl['acl_produit']] = array(
			"pro_id" => $acl['acl_produit'],
			"pro_code_produit" =>$acl['acl_pro_reference'],
			"acl_action" => $acl['acl_action'],
			"clients" => array(

			)
	    );
	}
	if(!empty($acl['fac_nb_stagiaires'])){
		$retour[$acl['acl_produit']]['clients'][] = array(
			"cli_id" => $acl['cli_id'],
			"cli_code" => $acl['cli_code'],
			"acl_id" => $acl['acl_id'],
			"fac_nb_stagiaires" => $acl['fac_nb_stagiaires']
		);
	}else{
		$retour[$acl['acl_produit']]['clients'][] = array(
			"cli_id" => $acl['cli_id'],
			"cli_code" => $acl['cli_code'],
			"acl_id" => $acl['acl_id'],
			"fac_nb_stagiaires" => 0
		);
	}




}

// ENVOIE DU RESULTAT
echo json_encode($retour);

?>
