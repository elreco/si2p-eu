<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // ACTUALISE LE STATUT DE L'ATTESTATION D'UN STAGIAIRE

 // retour JSON compatible select2
 
 
$erreur_txt="";
if(isset($_POST)){
	 
	$convention_id=0;
	if(!empty($_POST["convention"])){
		$convention_id=intval($_POST["convention"]); 
	}	
	$con_cli_adresse=0;
	if(!empty($_POST["con_cli_adresse"])){
		$con_cli_adresse=intval($_POST["con_cli_adresse"]); 
	}
	
	if(empty($convention_id) OR empty($con_cli_adresse)){
		
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();
		
	}else{
		
		$sql="SELECT con_numero,con_cli_geo,con_action_client,con_ht,con_societe FROM Conventions WHERE con_id=" . $convention_id . ";";
		$req=$ConnSoc->query($sql);
		$d_convention=$req->fetch();
		if(empty($d_convention)){
			$erreur_txt="Impossible de charger la convention!";	
			echo($erreur_txt);
			die();			
		}
	}
	
	// PERSONNE CONNECTE
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}

	// ON RECUPERE LES DONNEES ADRESSES
	
	$sql="SELECT cli_nom,adr_id,adr_libelle,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_geo 
	FROM Clients,Adresses WHERE cli_id=adr_ref_id AND adr_ref=1 AND adr_id=" . $con_cli_adresse . " 
	AND ( (NOT cli_categorie=3 AND adr_type=1) OR (cli_categorie=3 AND adr_type=2) )
	ORDER BY adr_libelle,cli_nom,adr_ville;";
	$req=$Conn->query($sql);
	$d_adresse=$req->fetch();
	if(empty($d_adresse)){
		$erreur_txt="Impossible de charger l'adresse!";		
		echo($erreur_txt);
		die();
	}else{
		
		if(!empty($d_adresse["adr_nom"])){
			$con_cli_nom=$d_adresse["adr_nom"];
		}else{
			$con_cli_nom=$d_adresse["cli_nom"];
		}
	}

	// ADRESSE => CHANGEMENT DE TVA
	
	$actu_tva=false;
	
	if($d_convention["con_cli_geo"]!=$d_adresse["adr_geo"]){
		
		$actu_tva=true;
			
		// SUR LA SOCIETE POUR LA TVA
		$sql="SELECT soc_tva_exo FROM Societes WHERE soc_id=" . $d_convention["con_societe"] . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();
		if(empty($d_societe)){
			$erreur_txt="Impossible d'identifier la société!";	
			echo($erreur_txt);
			die();			
		}

		// SUR L'ACTION
		$sql="SELECT act_date_deb,acl_produit FROM Actions,Actions_Clients WHERE act_id=acl_action AND acl_id=" . $d_convention["con_action_client"] . ";";
		$req=$ConnSoc->query($sql);
		$d_action=$req->fetch();
		if(empty($d_action)){
			$erreur_txt="Impossible de charger la formation!";	
			echo($erreur_txt);
			die();			
		}
			
		// SUR LE PRODUIT
		$sql="SELECT pro_tva FROM Produits WHERE pro_id=" . $d_action["acl_produit"] . ";";
		$req=$Conn->query($sql);
		$d_produit=$req->fetch();
		if(empty($d_produit)){
			$erreur_txt="Impossible de charger la formation!";	
			echo($erreur_txt);
			die();			
		}
		
			// CALCUL DE LA TVA
		
		if($d_adresse["adr_geo"]!=1 OR ($d_produit["pro_deductible"]==1 AND $d_societe["soc_tva_exo"]==1) ){
			
			$con_tva_id=3;
			$con_tva_periode=3;
			$con_tva_taux=0;
			
		}else{
		
			$con_tva_id=$d_produit["pro_tva"];
			
			// CHERCHE DE LA TVA EN VIGUEUR
			$sql="SELECT tpe_id,tpe_taux FROM Tva_Periodes WHERE tpe_tva=" . $con_tva_id . " 
			AND tpe_date_deb<='" .$d_action["act_date_deb"] . "' AND (tpe_date_fin>='" .$d_action["act_date_deb"] . "' OR ISNULL(tpe_date_fin) );";
			$req=$Conn->query($sql);
			$d_tva=$req->fetch();
			if(empty($d_tva)){
				$erreur_txt="Impossible de calculer la TVA!";	
				echo($erreur_txt);
				die();			
			}else{
				$con_tva_periode=$d_tva["tpe_id"];
				$con_tva_taux=$d_tva["tpe_taux"];
			}
		}
		
		// CALCUL TTC
		$con_ht=floatval($d_convention["con_ht"]);
		
		$con_ttc=$con_ht + ($con_ht*($con_tva_taux/100));
		$con_ttc=round($con_ttc,2);
		
		$con_tva=$con_ttc-$con_ht;
		$con_tva=round($con_tva,2);
		
	}
	
	
		
	// UPDATE DE LA CONVENTION
	
	$sql="UPDATE Conventions SET con_cli_adresse=:con_cli_adresse,con_cli_nom=:con_cli_nom,con_cli_service=:con_cli_service,con_cli_ad1=:con_cli_ad1,con_cli_ad2=:con_cli_ad2,
	con_cli_ad3=:con_cli_ad3,con_cli_cp=:con_cli_cp,con_cli_ville=:con_cli_ville,con_cli_geo=:con_cli_geo";
	if($actu_tva){
		$sql.=",con_tva_id=:con_tva_id,con_tva_periode=:con_tva_periode,con_tva_taux=:con_tva_taux,con_ttc=:con_ttc";
	}
	$sql.=" WHERE con_id=:con_id;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam("con_cli_adresse",$con_cli_adresse);
	$req->bindParam("con_cli_nom",$con_cli_nom);
	$req->bindParam("con_cli_service",$d_adresse["adr_service"]);
	$req->bindParam("con_cli_ad1",$d_adresse["adr_ad1"]);
	$req->bindParam("con_cli_ad2",$d_adresse["adr_ad2"]);
	$req->bindParam("con_cli_ad3",$d_adresse["adr_ad3"]);
	$req->bindParam("con_cli_cp",$d_adresse["adr_cp"]);
	$req->bindParam("con_cli_ville",$d_adresse["adr_ville"]);
	$req->bindParam("con_cli_geo",$d_adresse["adr_geo"]);
	if($actu_tva){
		$req->bindParam("con_tva_id",$con_tva_id);
		$req->bindParam("con_tva_periode",$con_tva_periode);
		$req->bindParam("con_tva_taux",$con_tva_taux);
		$req->bindParam("con_ttc",$con_ttc);
	}
	$req->bindParam("con_id",$convention_id);
	try{
		$req->execute();
	}Catch(Exception $e){
		$erreur_txt=$e->getMessage();
		echo($erreur_txt);
		die();
	}
	
	if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Conventions/" . $d_convention["con_numero"] . ".pdf")){
		unlink($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Conventions/" . $d_convention["con_numero"] . ".pdf");
	}

}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}
if(!$actu_tva){	
	$retour=array(
		"erreur" => 0,
		"actu_tva" => 0
	);
}else{
	$retour=array(
		"erreur" => 0,
		"actu_tva" => 1,
		"con_tva_id" => $con_tva_id,
		"con_tva_periode" => $con_tva_periode,
		"con_tva_taux" => $con_tva_taux,
		"con_ttc" => $con_ttc,
		"con_tva" => $con_tva
	);
}
echo(json_encode($retour));
	

?>