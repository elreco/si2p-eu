<?php
include "../includes/controle_acces.inc.php";

include '../includes/connexion.php';
include '../connexion_soc.php';

 // RETOURNE LES INFOS CONCERNANT LES CLIENTS QUI PARTICICPENT A UNE ACTION
 
 // RETOUR FORMAT JSON
 
$erreur=0;
if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=$_POST["action"]; 
	}else{
		$erreur=1;
	}	 
	
	if($erreur==0){
		
		// SUR L'ACTION DE REF
		
		$sql="SELECT act_adr_ref_id";
		$sql.=" FROM Actions WHERE act_id=:action_id AND act_adr_ref=1";
		$sql.=";";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$action=$req->fetch();
		
	}
	if($erreur==0){	

		// DONNEES ACTIONS CLIENTS
		$client_action=array();
		$sql="SELECT cli_id,cli_code,cli_nom";
		$sql.=",acl_id,acl_pro_reference,acl_pro_libelle";
		$sql.=" FROM Clients,Actions_CLients WHERE cli_id=acl_client AND acl_action=:action_id AND NOT acl_archive";
		$sql.=" ORDER BY cli_code,cli_nom;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$result=$req->fetchAll();
		if(!empty($result)){
			foreach($result as $r){
				$intervention=0;
				if(!empty($action)){
					if($r["cli_id"]==$action["act_adr_ref_id"]){
						$intervention=1;
					}
				}
				$client_action[]=array(
					"action_client" => $r["acl_id"],
					"client" => $r["cli_id"],
					"code" => $r["cli_code"],
					"nom" => $r["cli_nom"],
					"pro_reference" => $r["acl_pro_reference"],
					"pro_libelle" => $r["acl_pro_libelle"],
					"intervention" => $intervention					
				);					
			}			
		}
		
	}
}else{
	$erreur=10;
}

if($erreur==0){
	echo json_encode($client_action);
}else{
	header("HTTP/1.0 500 ");
};
 
 
?>