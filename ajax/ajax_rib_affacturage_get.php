<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

// RETOURNE L'ID DU RIB AFFACTURAGE DE LA SOCIETE 0 si pas de RIB

if(isset($_POST)){
	 
	$societe=0;
	if(!empty($_POST["societe"])){
		$societe=intval($_POST["societe"]); 
	}else{
		echo("formulaire incomplet!");
		die();
	}	
		
	$sql="SELECT rib_id,rib_nom FROM Rib WHERE rib_societe=" . $societe . " AND rib_affacturage;";
	$req=$Conn->query($sql);	
	$results=$req->fetchAll();
	if(!empty($results)){
		$rib=$results["rib_id"];
	}else{
		$rib=0;
	}
	echo(json_encode($rib));
	
}else{
	echo("formulaire incomplet!");
	die();
}
?>
