<?php
	include "../includes/controle_acces.inc.php";

	include '../includes/connexion.php';
	include '../includes/connexion_soc.php';

	 // VALIDATION D'UNE DEROGATION MARGE
	 
	 
	$erreur="";
	if(isset($_POST)){
		if(!empty($_POST)){
			
			$action=0;
			if(!empty($_POST["action"])){
				$action=intval($_POST["action"]); 
			}
			if(empty($action)){
				$erreur="Paramètres absents";
			}
		}else{
			$erreur="Paramètres absents";
		}
	}else{
		$erreur="Paramètres absents";
	}

	if(empty($erreur)){

    	if(!$_SESSION['acces']["acc_droits"][9]) {
			$erreur="Accès refusé!";
		}

	}

	if(empty($erreur)){

		$sql="SELECT act_verrou_admin,act_verrou_bc FROM Actions WHERE act_id=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$action);
		$req->execute();
		$d_action=$req->fetch();
		if(empty($d_action)){
			$erreur="Impossible de charger l'action.";
		}
	}

	if(empty($erreur)){

		// on somme le nouveau CA.

		$sql="SELECT SUM(acl_ca) FROM Actions_Clients WHERE acl_confirme AND NOT acl_archive AND acl_action=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$action);
		$req->execute();
		$d_action_ca=$req->fetch();
		if(!empty($d_action_ca)){
			if($d_action_ca[0]<=0){
				$erreur="L'action ne dispose pas de CA validé!";
			}
		}else{
			$erreur="L'action ne dispose pas de CA validé!";
		}
	}

	if(empty($erreur)){

		// l'action dispose de CA validé -> on va lever le verrou marge

		if(empty($d_action["act_verrou_bc"])){

			//le verrou bc doit systématiquement être a false puisqu'il y eu le calcul du verrou marge mais sécurité quand même.

			$d_action["act_verrou_admin"]=0;
			
		}

		// MAJ DE L'ACTION

		$sql_up="UPDATE Actions SET act_verrou_marge=0, act_marge_ca=:act_marge_ca, act_verrou_admin=:act_verrou_admin
		WHERE act_id=:action;";
		$req_up=$ConnSoc->prepare($sql_up);
		$req_up->bindParam("act_marge_ca",$d_action_ca[0]);
		$req_up->bindParam("act_verrou_admin",$d_action["act_verrou_admin"]);
		$req_up->bindParam(":action",$action);
		try{
			$req_up->execute();
		}Catch (Exception $e){
			$erreur="L'action n'a pas été mise à jour!<br/>Merci de transmettre le numéro d'action et le message suivant au SI.<br/>" . $e->getMessage();
		}
	}

	if(empty($erreur)){

		$data=array(
			"action" => $action,
			"verrou_admin" => $d_action["act_verrou_admin"],
			"warning" => ""
		);

		// la déro lève le blocage administratif

		if($d_action["act_verrou_admin"]==0){

			// maj des cases du planning

			$sql_up_dates="UPDATE Plannings_Dates SET pda_alert=0 WHERE pda_type=1 AND pda_ref_1=:action;";
			$req_up_dates=$ConnSoc->prepare($sql_up_dates);
			$req_up_dates->bindParam(":action",$action);
			try{
				$req_up_dates->execute();
			}Catch (Exception $e){
				$data["warning"]="Le blocage administratif n'a pas été levé sur les dates de formation!<br/>Merci de transmettre le numéro d'action et le message suivant au SI.<br/>" . $e->getMessage();
			}
		}

		echo json_encode($data);
		die();
	}

	if(!empty($erreur)){
		echo($erreur);
		die();
	}
?>