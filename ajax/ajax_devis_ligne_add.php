<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // AJOUT D'UNE LIGNE DE DEVIS
 
 // RETOUR FORMAT JSON
 
$erreur_text="";

$retour=array(
	"action" => 0,
	"notif" => ""
);

if(isset($_POST)){
	 
	$devis_id=0;
	if(!empty($_POST["dli_devis"])){
		$devis_id=intval($_POST["dli_devis"]); 
	}
	
	$dli_produit=0;
	if(!empty($_POST["dli_produit"])){
		$dli_produit=intval($_POST["dli_produit"]); 
	}
	
	$dli_libelle=$_POST["dli_libelle"];
	
	$dli_type=0;
	if(!empty($_POST["dli_type"])){
		$dli_type=intval($_POST["dli_type"]); 
	}
	
	if(empty($devis_id) OR empty($dli_produit) OR empty($dli_libelle) OR empty($dli_type)){
		$erreur_text="Formulaire incomplet";
	}
	
}else{
	$erreur_text="Formulaire incomplet";
}

// CONTROLE


// SUR LE PRODUIT
if(empty($erreur_text)){

	$req=$ConnSoc->query("SELECT dev_date,dev_adr_geo,dev_total_ht,dev_total_ttc FROM Devis WHERE dev_id=" . $devis_id . ";");
	$d_devis=$req->fetch();
	if(empty($d_devis)){
		$erreur_text="Impossible de charger les données de devis.";
	}	
}
// SUR LE PRODUIT
if(empty($erreur_text)){
	$req=$Conn->query("SELECT pro_categorie,pro_famille,pro_sous_famille,pro_sous_sous_famille,pro_deductible,pro_tva FROM Produits INNER JOIN Produits_Categories
	ON (Produits.pro_categorie=Produits_Categories.pca_id) WHERE pro_id=" . $dli_produit);
	$d_produit=$req->fetch();
	if(empty($d_produit)){
		$erreur_text="Impossible de charger les données sur le produit.";
	}
}
// DONNE POUR CREATION
if(empty($erreur_text)){

	// form non required
	
	$dli_ht=0;
	if(!empty($_POST["dli_ht"])){
		$dli_ht=floatval($_POST["dli_ht"]); 
	}
	$dli_ht=number_format($dli_ht,2);
	
	$dli_qte=0;
	if(!empty($_POST["dli_ht"])){
		$dli_ht=floatval($_POST["dli_ht"]); 
	}
	$dli_montant_ht=$dli_ht*$dli_ht;
	$dli_montant_ht=number_format($dli_montant_ht,2);
	
	// personne conectée
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}
	// SUR LA SOCIETE
	
	$req=$Conn->query("SELECT soc_tva_exo FROM Societes WHERE soc_id=" . $acc_societe . ";");
	$d_societe=$req->fetch();
	
	// CALCUL DE LA TVA
	
	if(($d_societe["soc_tva_exo"]==1 AND $d_produit["pro_deductible"]==1) OR $d_devis["dev_adr_geo"]!=1){
		$dli_tva_id=3;
		$dli_tva_periode=3;
		$dli_tva_taux=floatval(0);
	}else{
		
		$req=$Conn->query("SELECT tpe_id,tpe_tva,tpe_taux FROM Tva_Periodes WHERE tpe_tva=" . $d_produit["pro_tva"] . "
		AND tpe_date_deb<='" . $d_devis["dev_date"] . "' AND tpe_date_fin>='" . $d_devis["dev_date"] . "';");
		$d_tva=$req->fetch();
		if(!empty($d_tva)){
			$dli_tva_id=$d_tva["tpe_tva"];
			$dli_tva_periode=$d_tva["tpe_id"];
			$dli_tva_taux=floatval($d_tva["tpe_taux"]);
		}
	}
	
	// PLACE DE LA LIGNE
	
	$dli_place=1;
	$req=$ConnSoc->query("SELECT MAX(dli_place) FROM Devis_Lignes WHERE dli_devis=" . $devis_id . ";");
	$result=$req->fetch();
	if(!empty($result)){
		$dli_place=$result[0]+1;
	}
	
	// val fixe
	$dli_action_client=0;
	$dli_action_cli_soc=0;	
}


// ENREGISTREMENT
if(empty($erreur_text)){	

	$sql="INSERT INTO Devis_Lignes (
	dli_devis,dli_produit,dli_categorie,dli_famille,dli_sous_famille,dli_sous_sous_famille,dli_type,dli_reference,dli_libelle,dli_texte
	,dli_ht,dli_qte,dli_montant_ht,dli_tva_id,dli_tva_periode,dli_tva_taux
	,dli_place,dli_soc_exo,dli_deductible,dli_revente
	,dli_action_client,dli_action_cli_soc,dli_dates,dli_stagiaires_txt
	) VALUES (
	:dli_devis,:dli_produit,:dli_categorie,:dli_famille,:dli_sous_famille,:dli_sous_sous_famille,:dli_type,:dli_reference,:dli_libelle,:dli_texte
	,:dli_ht,:dli_qte,:dli_montant_ht,:dli_tva_id,:dli_tva_periode,:dli_tva_taux
	,:dli_place,:dli_soc_exo,:dli_deductible,:dli_revente
	,:dli_action_client,:dli_action_cli_soc,:dli_dates,:dli_stagiaires_txt);";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam("dli_devis",$devis_id);
	$req->bindParam("dli_produit",$dli_produit);
	$req->bindParam("dli_categorie",$d_produit["pro_categorie"]);
	$req->bindParam("dli_famille",$d_produit["dli_famille"]);
	$req->bindParam("dli_sous_famille",$d_produit["dli_sous_famille"]);
	$req->bindParam("dli_sous_sous_famille",$d_produit["dli_sous_sous_famille"]);
	$req->bindParam("dli_type",$dli_type);
	$req->bindParam("dli_reference",$d_produit["pro_code_produit"]);
	$req->bindParam("dli_libelle",$dli_libelle);
	$req->bindParam("dli_texte",$_POST["dli_texte"]);
	$req->bindParam("dli_ht",$dli_ht);
	$req->bindParam("dli_qte",$dli_qte);
	$req->bindParam("dli_montant_ht",$dli_montant_ht);
	$req->bindParam("dli_tva_id",$dli_tva_id);
	$req->bindParam("dli_tva_periode",$dli_tva_periode);
	$req->bindParam("dli_tva_taux",$dli_tva_taux);
	$req->bindParam("dli_place",$dli_place);
	$req->bindParam("dli_soc_exo",$d_societe["soc_tva_exo"]);
	$req->bindParam("dli_deductible",$d_produit["pro_deductible"]);
	$req->bindParam("dli_revente",$d_produit["pca_revente"]);
	$req->bindParam("dli_action_client",$dli_action_client);
	$req->bindParam("dli_action_cli_soc",$dli_action_cli_soc);
	$req->bindParam("dli_dates",$_POST["dli_dates"]);
	$req->bindParam("dli_stagiaires_txt",$_POST["dli_stagiaires_txt"]);
	try{
		$req->execute();
		$dli_id=$ConnSoc->lastInsertId();
	}Catch(Exception $e){
		$erreur_text="Err add ligne : " . $e->getMessage();
	}
}

// TRAITEMENT ANNEXE	

if(empty($erreur_text)){
	
	// ON SELECT LA LIGNE QUE L'ON VIENT DE ADD -> facilite retout json
	
	$req=$ConnSoc->query("SELECT * FROM Devis_Lignes WHERE dli_id=" . $dli_id . ";");
	$retour=$req->fetch(PDO::FETCH_ASSOC);
	
	// DEVIS
	
	$dev_total_ht=$d_devis["dev_total_ht"] + $dli_montant_ht;
	
	$coeff_tva=(100+$dli_tva_taux)/100;
	$dli_montant_ttc=$dli_montant_ht*$coeff_tva;
	$dli_montant_ttc=number_format($dli_montant_ttc,2);
	$dev_total_ttc=$d_devis["dev_total_ttc"] + $dli_montant_ttc;
	
	$req=$ConnSoc->prepare("UPDATE Devis SET dev_total_ht=:dev_total_ht,dev_total_ttc=:dev_total_ttc WHERE dev_id=:devis_id;");
	$req->bindParam("dev_total_ht",$dev_total_ht);
	$req->bindParam("dev_total_ttc",$dev_total_ttc);
	$req->bindParam("devis_id",$devis_id);
	try{
		$req->execute();
		$retour["dev_total_ht"]=$dev_total_ht;
		$retour["dev_total_ttc"]=$dev_total_ttc;
	}Catch(Exception $e){
		$retour["warning"]=$e->getMessage();
	}
	
}
if(empty($erreur_text)){
	echo json_encode($retour);
}else{
	echo($erreur_text);
};
 
 
?>