<?php
include "../includes/controle_acces.inc.php";
include '../connexion_soc.php';

 // CONFIRME OU ANNUL CONFIRMATION D'UNE ACTION
 
$erreur=0;
if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]); 
	}	 

	if($action_id!=0){

		
		$sql="UPDATE Actions SET act_planning_txt=:act_planning_txt, act_commentaire=:act_commentaire WHERE act_id=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":act_planning_txt",$_POST["act_planning_txt"]);
		$req->bindParam(":act_commentaire",$_POST["act_commentaire"]);
		$req->bindParam(":action",$action_id);
		try{
			$req->execute();
		} catch (Exception $e) {
			$erreur=1;
			echo($e->getMessage());
		}
		
		if($erreur==0){
			
			// mise à jour du texte pour les case du planning
			$sql="UPDATE Plannings_Dates SET pda_survol=:act_planning_txt WHERE pda_type=1 AND pda_ref_1=:action;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":act_planning_txt",$_POST["act_planning_txt"]);
			$req->bindParam(":action",$action_id);
			try{
				$req->execute();
			} catch (Exception $e) {
				$erreur=1;
				echo($e->getMessage());
			}
		}
		
	}else{
		$erreur=1;
		echo("Paramètres absents");
	}
}else{
	$erreur=1;
	echo("Paramètres absents");
}
	
if($erreur==0){
	$retour=array(
		"action" => $action_id,
		"survol" => $_POST["act_planning_txt"]
	);
	echo(json_encode($retour));	
}
 
 
?>