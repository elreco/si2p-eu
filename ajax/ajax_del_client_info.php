<?php
include("../includes/connexion.php");
include('../includes/connexion_soc.php');


// PERMET DE SUPPRIMER UNE INFO DU CLIENT

$erreur="";
if(isset($_POST)){

	$info=0;
	if(!empty($_POST["info"])){
		$info=intval($_POST["info"]);
	}

	
	if($info>0){
		
		try {
			$req=$Conn->query("DELETE FROM Infos WHERE inf_id=" . $info . " ;");
		}
		catch( PDOException $Exception ) {
			$erreur=$Exception->getMessage();
		}
	}else{
		$erreur="1-ERREUR PARAMETRE";
	}
}else{
	$erreur="2-ERREUR PARAMETRE";
}
if($erreur==""){
	
	$retour=array(
		"info" => $info
	);
	
	echo json_encode($retour);
}else{
	echo $erreur;
}
?>