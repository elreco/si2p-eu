<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../modeles/mod_clients_get.php';

 // SCRIPT GENERIQUE POUR METTRE A JOUR N CHAMP DE ACTIONS_CLIENTS

 // retour JSON compatible select2
 
 
$erreur_txt="";

if(isset($_POST)){
	 
	$categorie=0;
	if(!empty($_POST["categorie"])){
		$categorie=intval($_POST["categorie"]); 
	}
	
	$sous_categorie=0;
	if(!empty($_POST["sous_categorie"])){
		$sous_categorie=intval($_POST["sous_categorie"]); 
	}
	
	$archive=null;
	if(!is_null($_POST["archive"])){
		$archive=intval($_POST["archive"]); 
	}
	
	$acces=null;
	if(!is_null($_POST["acces"])){
		// 1 uniquement clients accessible à U
		$acces=intval($_POST["acces"]); 
	}
	
}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}
	// personne connecté
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}
	if(empty($acc_societe)){
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();
	}


	$d_clients=get_clients($categorie,$sous_categorie,$archive,$acces);
	echo(json_encode($d_clients));
	die();
?>