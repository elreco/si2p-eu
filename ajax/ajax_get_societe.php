<?php

include("../includes/connexion.php");

if(isset($_POST['societe'])){

	$societe_id=0;
	if(!empty($_POST['societe'])){
		$societe_id=intval($_POST['societe']);
	}
 
	global $Conn;

    $req = $Conn->prepare("SELECT * FROM societes WHERE soc_id=" . $societe_id);
    $req->execute();
    $result = $req->fetch();
	if(!empty($result)){
		// on supprime le soc pour pouvoir traiter de la mm façon json societe et json agence
		$societe=array(
			"nom" => $result["soc_nom"],
			"ad1" => $result["soc_ad1"],
			"ad2" => $result["soc_ad2"],
			"ad3" => $result["soc_ad3"],
			"cp" => $result["soc_cp"],
			"ville" => $result["soc_ville"],
			"siren" => $result["soc_siren"],
			"siret" => $result["soc_siret"],
			"ape" => $result["soc_ape"],
			"tva" => $result["soc_tva"]
		);
	}
    echo json_encode($societe);

}
?>