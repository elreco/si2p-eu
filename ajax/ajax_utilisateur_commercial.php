	
<?php
 // PERMET DE PRE-SELECTIONNER DES CLIENTS D'APRES UNE SAISIE UTILISATEUR
 
session_start();
include('../includes/connexion.php');
include('../includes/connexion_soc.php');


$sql="SELECT DISTINCT uti_id,uti_nom,uti_prenom FROM Utilisateurs 
	LEFT JOIN utilisateurs_societes ON (utilisateurs_societes.uso_utilisateur = Utilisateurs.uti_id)
WHERE uso_societe=:acc_societe AND NOT uti_archive";
if($_GET['agence']>0){
	$sql.=" AND uso_agence=:acc_agence";
}
$sql.=" ORDER BY uti_nom,uti_prenom;";
$req=$Conn->prepare($sql);
$req->bindParam("acc_societe",$_SESSION['acces']['acc_societe']);
if($_GET['agence']>0){
	$req->bindParam("acc_agence",$_GET['agence']);
}
$req->execute();
$utilisateurs=$req->fetchAll();

if(!empty($utilisateurs)){
	$sql="SELECT com_ref_1 FROM Commerciaux WHERE com_type=1";
	if($_GET['agence']>0){
		$sql.=" AND com_agence=:acc_agence";
	}
	$req=$ConnSoc->prepare($sql);
	if($_GET['agence']>0){
		$req->bindParam("acc_agence",$_GET['agence']);
	}
	$req->execute();
	$uti_deja_inscrit=$req->fetchAll();
	if(!empty($uti_deja_inscrit)){
		foreach($uti_deja_inscrit as $u){
			if($u['com_ref_1'] != $_GET['defaut']){
				$cle=array_search($u["com_ref_1"],array_column($utilisateurs,"uti_id"),true);
				if(!is_bool($cle)){
					array_splice($utilisateurs, $cle, 1);
				};
			}
			
		}
	}
}
if(!empty($utilisateurs)){
	foreach($utilisateurs as $u){
		$retour[] = array("id"=>$u['uti_id'], "text"=>$u['uti_nom'] . " " .  $u['uti_prenom']);
	}
}

// ENVOIE DU RESULTAT
if(!empty($retour)){
	echo json_encode($retour);	
}
?>
