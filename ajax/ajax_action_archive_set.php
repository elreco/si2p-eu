<?php
include "../includes/controle_acces.inc.php";

include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // MODIFI L'ETAT ARCHIVE D'UNE ACTION OU D'UNE FORMATION COMPLETE

 // RETOUR FORMAT JSON

$erreur_text="";

if(isset($_POST)){

	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]);
	}
	$action_client_id=0;
	if(!empty($_POST["action_client"])){
		$action_client_id=intval($_POST["action_client"]);
	}

	$archive_type=0; // 1 archive 2 confirme archive 3 annul archive
	if(!empty($_POST["type"])){
		$archive_type=intval($_POST["type"]);
	}

	if(empty($archive_type) OR (empty($action_id) AND empty($action_client_id) ) ){
		$erreur_text="Formulaire incomplet";
	}

}else{
	$erreur_text="Formulaire incomplet";
}
if(empty($erreur_text)){
	if(!$_SESSION["acces"]["acc_droits"][10] AND !$_SESSION["acces"]["acc_droits"][11]){
		$erreur_text="Vous ne disposez pas des droits nécéssaire!";
	}
}

if(empty($erreur_text)){

	$retour=array(
		"action" => 0,
		"type" => $archive_type,
		"action_client" => array()
	);

	if($archive_type==1){

		// CONFIRMATION D'ARCHIVAGE

		$tab_archive=array();
		if(!empty($action_id)){

			// CONTROLE

			// on verifie que toutes les clients sont acrhivées

			$req=$ConnSoc->query("SELECT acl_id,acl_archive,acl_archive_ok FROM Actions_Clients WHERE acl_action=" . $action_id . ";");
			$d_actions_clients=$req->fetchAll();
			if(!empty($d_actions_clients)){
				foreach($d_actions_clients as $d_ac){
					if(empty($d_ac["acl_archive"])){
						$erreur_text="Une inscription non archivée est liée à cette formation. Impossible de confirmer l'archivage!";
						break;
					}else{
						$retour["action_client"][]=$d_ac["acl_id"];
					}
				}
			}

			// on confirme l'archivage des clients
			if(empty($erreur_text)){
				try{
					$req=$ConnSoc->query("UPDATE Actions_Clients SET acl_archive_ok=1 WHERE acl_action=" . $action_client_id . " AND NOT acl_archive_ok;");
				}Catch(Exception $e){
					$erreur_text=$e->getMessage();
				}
			}

			// on confirme l'archivage de l'action
			if(empty($erreur_text)){
				try{
					$req=$ConnSoc->query("UPDATE Actions SET act_archive_ok=1 WHERE act_id=" . $action_id . ";");
				}Catch(Exception $e){
					$erreur_text=$e->getMessage();
				}
			}

			if(empty($erreur_text)){
				$retour["action"]=$action_id;
			}


		}else{

			// ARCHIVAGE D'UNE ACTION_CLIENT

			try{
				$req=$ConnSoc->query("UPDATE Actions_Clients SET acl_archive_ok=1 WHERE acl_id=" . $action_client_id . ";");
			}Catch(Exception $e){
				$erreur_text=$e->getMessage();
			}
			if(empty($erreur_text)){
				$retour["action_client"][]=$action_client_id;
			}
		}

	}elseif($archive_type==2){

		// DESARCHIVAGE

		if(!empty($action_id)){

			// ON DESARCHIVE UNE FORMATION COMPLETE

			$req=$ConnSoc->query("SELECT act_agence FROM Actions WHERE act_id=" . $action_id . ";");
			$d_action=$req->fetch();
			if(empty($d_action)){
				$erreur_text="Impossible de charger les données sur l'action!";
			}

			if(empty($erreur_text)){

				// on controle que les ressources sont toujours dispo

				$req_int=$ConnSoc->prepare("SELECT pin_place FROM Plannings_Intervenants WHERE pin_intervenant=:intervenant AND pin_agence=:agence
				AND pin_annee=:annee AND pin_semaine=:semaine;");
				$req_int->bindParam(":agence",$d_action["act_agence"]);

				$req_dates=$ConnSoc->prepare("SELECT pda_id,pda_intervenant,pda_salle,pda_vehicule FROM Plannings_Dates
				WHERE NOT pda_archive AND pda_date=:date AND pda_demi=:demi AND (NOT pda_type=1 OR NOT pda_ref_1=" . $action_id . ")
				AND (pda_intervenant=:intervenant OR (pda_salle=:salle AND NOT pda_salle=0) OR (pda_vehicule=:vehicule AND NOT pda_vehicule=0) );");


				$req=$ConnSoc->query("SELECT pda_date,pda_demi,pda_intervenant,pda_salle,pda_vehicule FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=" . $action_id . " ORDER BY pda_date,pda_demi;");
				$_dates=$req->fetchAll();
				if(!empty($_dates)){
					foreach($_dates as $d_d){

						$DT_date=date_create_from_format("Y-m-d",$d_d["pda_date"]);

						$semaine=$DT_date->format("W");
						$annee=$DT_date->format("Y");

						// ON VERIF QUE L'INTERVENANT EST BIEN INSCRIT AU PLANNING
						$req_int->bindParam(":semaine",$semaine);
						$req_int->bindParam(":annee",$annee);
						$req_int->bindParam(":intervenant",$d_d["pda_intervenant"]);
						$req_int->execute();
						$d_int_plannning=$req_int->fetch();
						if(empty($d_int_plannning)){
							$erreur_text="Certains formateurs ne sont plus inscrits au planning!";
							break;
						}


						if(empty($erreur_text)){

							$err_salle="";
							$err_vehicule="";
							$err_int="";

							// ON VERIFIE QUE LES RESSOURCES SONT TOUJOURS DISPONIBLES
							$req_dates->bindParam(":date",$d_d["pda_date"]);
							$req_dates->bindParam(":demi",$d_d["pda_demi"]);
							$req_dates->bindParam(":salle",$d_d["pda_salle"]);
							$req_dates->bindParam(":vehicule",$d_d["pda_vehicule"]);
							$req_dates->bindParam(":intervenant",$d_d["pda_intervenant"]);
							$req_dates->execute();
							$d_pris=$req_dates->fetchAll();

							if(!empty($d_pris)){
								foreach($d_pris as $d_p){
									if($d_p["pda_intervenant"]==$d_d["pda_intervenant"]){
										$err_int="Certains formateurs ne sont plus disponibles.";
									}
									if($d_p["pda_vehicule"]==$d_d["pda_vehicule"] AND !empty($d_d["pda_vehicule"])){
										$err_vehicule="Certains vehicules ne sont plus disponibles.";
									}
									if($d_p["pda_salle"]==$d_d["pda_salle"] AND !empty($d_d["pda_salle"])){
										$err_salle="Certaines salles ne sont plus disponibles.";
									}
								}
							}

							if(!empty($err_int)){
								$erreur_text.=$err_int . "<br/>";
							}
							if(!empty($err_vehicule)){
								$erreur_text.=$err_vehicule . "<br/>";
							}
							if(!empty($err_salle)){
								$erreur_text.=$err_salle . "<br/>";
							}
						}

					}

				}
			}
			// ressource ok -> on desarchive les dates
			if(empty($erreur_text)){
				try{
					$req=$ConnSoc->query("UPDATE Plannings_Dates SET pda_archive=0 WHERE pda_type=1 AND pda_ref_1=" . $action_id . ";");
				}Catch(Exception $e){
					$erreur_text=$e->getMessage();
				}
			}

			// ressource ok -> on desarchive l'action
			if(empty($erreur_text)){

				try{
					$req=$ConnSoc->query("UPDATE Actions SET act_archive=0, act_archive_date=NULL, act_archive_uti=0, act_archive_ok=0
					WHERE act_id=" . $action_id . ";");
				}Catch(Exception $e){
					$erreur_text=$e->getMessage();
				}

				try{
					$req=$ConnSoc->query("UPDATE Actions_Clients SET acl_archive=0, acl_archive_date=NULL, acl_archive_uti=0, acl_archive_ok=0
					WHERE acl_action=" . $action_id . ";");
				}Catch(Exception $e){
					$erreur_text=$e->getMessage();
				}

			}

			if(empty($erreur_text)){
				$retour["action"]=$action_id;
			}
		}else{
			// DESARCHIVE UNE ACTION CLIENT

			if(empty($erreur_text)){

				try{
					$req=$ConnSoc->query("UPDATE Actions_CLients SET acl_archive=0, acl_archive_date=NULL, acl_archive_uti=0, acl_archive_ok=0
					WHERE acl_id=" . $action_client_id . ";");
				}Catch(Exception $e){
					$erreur_text=$e->getMessage();
				}

			}

			if(empty($erreur_text)){
				$retour["action_client"][]=$action_client_id;
			}

		}
	}

}
if(empty($erreur_text)){

	echo json_encode($retour);
}else{
	echo($erreur_text);
};


?>