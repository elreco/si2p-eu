<?php
include("../includes/connexion.php");
include('../modeles/mod_document.php');
session_start();
$erreur=0;

if(!empty($_POST)){
	
	// on recupere le document couper
	$doc = get_document($_SESSION['etech_couper_id']);
	
	// chercher la liste des utilisateurs ayant accès au document
	$req=$Conn->prepare("SELECT uti_id FROM Utilisateurs LEFT JOIN documents_utilisateurs ON (Utilisateurs.uti_id = documents_utilisateurs.dut_utilisateur) WHERE dut_document = " . $doc['doc_id']);
	$req->execute();
	$utilisateurs = $req->fetchAll();

	$url_dossier_source = $doc['doc_url'] . $doc['doc_nom_aff'] . "/";
	$url_dossier_cible = $_POST['doc_url'];

	$position = strpos($url_dossier_cible, $url_dossier_source);

	if($url_dossier_source != $url_dossier_cible){
		if((is_int($position) && $position > 0) OR is_bool($position)){

			// maj du document
			update_document_coller($doc['doc_id'],$_POST['doc_url']);
			
			/*on verifie que u doit encore avori accès au dossier source
			Attention toujour effectuer cette requete après maj sinon doc est toujours dans source 
			et delete_affichage_dossier ne marche pas */
			delete_affichage_dossier($utilisateurs, $url_dossier_source);
			
			// on s'assure que U a accès au doosier par de doc
			set_affichage_dossier($utilisateurs, $url_dossier_cible);

			// maj de l'URL des sous-dossier
			$sous_docs = get_documents_like($doc['doc_url'] . $doc['doc_nom_aff'] . "/", $doc['doc_id']);
			foreach($sous_docs as $s){
				$a1 = explode("/" , $s['doc_url']);
				$a2 = explode("/", $doc['doc_url']);
					// var_dump(xdiff_string_diff($s['doc_url'], $_POST['doc_url'], 1));
				update_document_coller($s['doc_id'],$_POST['doc_url'] . join('/', array_diff($a1, $a2)) . "/");
			}

		}else{
			$erreur=1;
		}
	}else{
		$erreur=1;
	}
}
echo $erreur;
unset($_SESSION['etech_couper_id']);


?>