<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

include "../modeles/mod_get_cli_s_classifications.php";

// RETOURNE LES SOUS CLASSIFICATION ASSOCIES à UNE CLASSIFICATION

// retour => tableau JSON (compatible Select2)

if(isset($_POST)){
	 
	$classification=0;
	if(!empty($_POST["classification"])){
		$classification=intval($_POST["classification"]); 
	}
	
	$retour=array();
	
	if($classification>0){
		
		
		$d_s_classification=get_cli_s_classifications($classification);
		if(!empty($d_s_classification)){
			
			foreach($d_s_classification as $dsc){
				$retour[]=array(
					"id" => $dsc["csc_id"],
					"text" => $dsc["csc_libelle"]
				);
			}
			
		}
	}
	echo json_encode($retour);
}else{
	echo("Erreur paramètre!");
}
?>
