<?php
include "../includes/controle_acces.inc.php";

include '../connexion_soc.php';
 // CONFIRME OU ANNUL CONFIRMATION D'UNE ACTION
 
$erreur=0;
if(isset($_POST)){
	 
	$client=0;
	if(!empty($_POST["client"])){
		$client=intval($_POST["client"]); 
	}	
	$produit=0;
	if(!empty($_POST["produit"])){
		$produit=intval($_POST["produit"]); 
	}
	
	$ht_inter=0;
	if(!empty($_POST["ht_inter"])){
		$ht_inter=floatval($_POST["ht_inter"]); 
	}
	
	$ht_intra=0;
	if(!empty($_POST["ht_intra"])){
		$ht_intra=floatval($_POST["ht_intra"]); 
	}
	
	$retour=array(
		"produit" => $produit,
		"ht_intra" => $ht_intra,
		"ht_inter" => $ht_inter,
		"erreur" => ""
	);
	
	if($client>0 AND $produit>0){

		$sql="UPDATE Clients_Produits SET cpr_ht_intra=" . $ht_intra . ", cpr_ht_inter=" . $ht_inter . " 
		WHERE cpr_client=" . $client . " AND cpr_produit=" . $produit . ";";
		try{    
			$req=$ConnSoc->query($sql);
		}catch(PDOException $e)
		{	$retour["erreur"]=$e->getMessage();
		}

	}else{
		$retour["erreur"]="Erreur paramètres";
	}
}

echo json_encode($retour);

 
 
?>