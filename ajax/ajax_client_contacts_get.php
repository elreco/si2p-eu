<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../modeles/mod_get_contacts.php';


 // RETOURNE LES DONNEES ENREGISTRES DANS LA TABLE CONTACTS
 
// parametre POST

// ref : INT 1 contact clients
	// ref_id : ID clients 

// retour => tableau JSON

// Appel :
// planning.php

// auteur : FG 06/09/2016
 
 
$erreur_txt="";
if(isset($_POST)){
	 
	$ref=0;
	if(!empty($_POST["ref"])){
		$ref=intval($_POST["ref"]); 
	}	
	$ref_id=0;
	if(!empty($_POST["ref_id"])){
		$ref_id=intval($_POST["ref_id"]); 
	}
	
	$adresse=0;
	if(!empty($_POST["adresse"])){
		$adresse=intval($_POST["adresse"]); 
	}

	if((empty($ref_id) AND empty($adresse)) OR empty($ref)){
		
		$erreur_txt="Impossible de récupérer les contacts";
		
	}else{

		
		$adr_lien=0;
		if(!empty($_POST["adr_lien"])){
			$adr_lien=intval($_POST["adr_lien"]); 
		}
		
		// ON CALCUL ref_id. Utilisation -> si U a acces à un carnet autre que client (ex facturation à la MM) -> il faut recuperer les contact de la MM
		if(empty($ref_id)){
			$sql="SELECT adr_ref_id FROM Adresses WHERE adr_id=" . $adresse . ";";
			$req=$Conn->query($sql);
			$d_adresse=$req->fetch();
			if(!empty($d_adresse)){
				$ref_id=$d_adresse["adr_ref_id"];
			}else{
				$erreur_txt="Impossible de récupérer les contacts";
			}
		}
	}
	
	if(empty($erreur_txt)){
	
		$results=get_contacts($ref,$ref_id,$adresse,$adr_lien);
		if(!empty($results)){
			if(is_array($results)){
				foreach($results as $r){
					
					// la jointure sur Adresses_Contacts n'existe que si adresse >0
					$aco_defaut=0;
					if(isset($r["aco_defaut"])){
						$aco_defaut=$r["aco_defaut"];
					}
					$contacts[]=array(
						"id" => $r["con_id"],
						"text" => $r["con_nom"] . " " . $r["con_prenom"],
						"titre" => $r["con_titre"],
						"nom" => $r["con_nom"],
						"prenom" => $r["con_prenom"],
						"tel" => $r["con_tel"],
						"portable" => $r["con_portable"],
						"mail" => $r["con_mail"],
						"defaut" => $aco_defaut
					);				
				}
			}else{
				$erreur_txt=$results;
			}
		}else{
			$contacts=array();
			//$erreur_txt="Impossible de récupérer les contacts";
		}
	}
}else{
	$erreur_txt="Impossible de récupérer les contacts";
}
//echo("erreur : " . $erreur);
if(empty($erreur_txt)){
		echo json_encode($contacts);
	
	
}else{
	echo($erreur_txt);
};
 
 
?>