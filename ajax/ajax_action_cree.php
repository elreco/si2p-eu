<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // ENREGISTREMENT D'UNE NOUVELLE ACTION

 // 1 erreur generique -> aff text
 // n nécéssite un traitement dans la fonction de retour
 // n=2 date non dispo reload
$erreur_type=0;
$erreur_code=0;
$erreur_text="";

if(isset($_POST)){

	if(!empty($_POST)){

		// la personne logué

		if($_SESSION['acces']['acc_ref']==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];
		}
		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];
		}

		// agence de l'action déterminé par l'intervenant via pin_agence
		$act_agence=0;
		if(!empty($_POST['agence'])){
			$act_agence=intval($_POST['agence']);
		}
		if(empty($act_agence)){
            $act_agence = $acc_agence;
        }
		$produit_id=0;
		if(!empty($_POST["produit"])){
			$produit_id=$_POST["produit"];
		}

		$act_pro_type=0;
		if(!empty($_POST["produit_type"])){
			$act_pro_type=intval($_POST["produit_type"]);
		}

		$client_id=0;
		if(!empty($_POST["client"])){
			$client_id=intval($_POST["client"]);
		}

		$id_heracles=0;
		if(!empty($_POST["id_heracles"])){
			$id_heracles=intval($_POST["id_heracles"]);
		}

	//******************************************
	//	CONTROLE - DONNEE TOUJOURS REQUIRED
	//*******************************************/

		// ON VERIFIE LA DISPO DES DATES

		$liste_case="";
		if(!empty($_POST['liste_case'])){
			$liste_case=$_POST['liste_case'];
		}else{
			$erreur_type=1;
			$erreur_text="Vous n'avez selectionné aucune date!";
		}

		if(empty($erreur_type)){
			if(empty($client_id) AND empty(!$id_heracles)){
				$erreur_type=1;
				$erreur_text="Erreur de synchronisation Héraclès!";
			}
		}

		if(empty($erreur_type)){

			// les case select
			$case=explode (",",$liste_case);

			// info sur la première date
			$donnee_case=explode ("_",$case[0]);
			$intervenant=$donnee_case[0];
			$act_date_deb=$donnee_case[1];
			$demi=$donnee_case[2];
			//$nb_demi_j=count($case);

			$planning_cases=array();

			// on verifie que les dates sont toujours disponible

			$sql="SELECT pda_id FROM Plannings_Dates WHERE NOT pda_archive AND ( ";
			foreach($case as $c){
				if(!empty($c)){
					$donnee_case=explode ("_",$c);
					$sql.="(pda_intervenant=" . $donnee_case[0] . " AND pda_date='" . $donnee_case[1] . "' AND pda_demi=" . $donnee_case[2] . ") OR ";

					$planning_cases[]=array(
						"intervenant" => $donnee_case[0],
						"date" => $donnee_case[1],
						"demi" => $donnee_case[2],
						"case" => $c,	// id de la case a passer au tableau de retour
						"id" => 0
					);
				}
			}
			$sql=substr($sql,0,-4);
			$sql.=" )";
			$req=$ConnSoc->query($sql);
			$libre=$req->fetchAll();
			if(!empty($libre)){
				$erreur_type=1;
				$erreur_code=2; // declenche un reload
				$erreur_text="A Les dates selectionnées ne sont plus disponible";
			}
		}

		// FAMILLE / SOUS FAMILLE
		if(empty($erreur_type)){
			$act_pro_categorie=1;

			$act_pro_famille=0;
			if(!empty($_POST["act_pro_famille"])){
				$act_pro_famille=intval($_POST["act_pro_famille"]);
			}
			$act_pro_sous_famille=0;
			if(!empty($_POST["act_pro_sous_famille"])){
				$act_pro_sous_famille=intval($_POST["act_pro_sous_famille"]);
			}
			$act_pro_sous_sous_famille=0;
			if(!empty($_POST["act_pro_sous_sous_famille"])){
				$act_pro_sous_sous_famille=intval($_POST["act_pro_sous_sous_famille"]);
			}

			if(empty($act_pro_famille)){
				$erreur_type=1;
				$erreur_text="A - Formulaire incomplet. Impossible d'enregistrer les informations!";

			}elseif(!empty($act_pro_sous_famille) AND empty($act_pro_sous_sous_famille) ){

				// il n'y a pas de sous sous famille -> on s'assure que c'est parce que la sous famille n'en a pas
				$sql="SELECT pss_id FROM Produits_sous_Sous_Familles WHERE pss_psf_id=" . $act_pro_sous_famille . ";";
				$req=$Conn->query($sql);
				$controle_fam=$req->fetchAll();
				if(!empty($controle_fam)){
					$erreur_type=1;
					$erreur_text="B - Formulaire incomplet. Impossible d'enregistrer les informations!";
				}

			}elseif(empty($act_pro_sous_famille) ){

				// il n'y a pas de sous famille -> on s'assure que c'est parce que la famille n'en a pas
				$sql="SELECT psf_id FROM Produits_Sous_Familles WHERE psf_pfa_id=" . $act_pro_famille . ";";
				$req=$Conn->query($sql);
				$controle_fam=$req->fetchAll();
				if(!empty($controle_fam)){
					$erreur_type=1;
					$erreur_text="C - Formulaire incomplet. Impossible d'enregistrer les informations!";
				}
			}
		}

	//******************************************
	//	CONTROLE - DONNEE REQUIRED EN FONCTION DE LA SITUATION
	//*******************************************/

		// adresse d'intervention si adresse client ou autre
		if(empty($erreur_type)){
			$act_adr_ref=0;
			if(!empty($_POST["act_adr_ref"])){
				$act_adr_ref=intval($_POST["act_adr_ref"]);
			}

			$act_adr_cp=$_POST["act_adr_cp"];
			$act_adr_ville=$_POST["act_adr_ville"];

			$adresse=0;
			if($_POST["adresse"]>0){
				$adresse=intval($_POST["adresse"]);
			}

			if( ($act_adr_ref==1 OR $act_adr_ref==3) AND (empty($act_adr_cp) OR empty($act_adr_ville) ) ){
				$erreur_type=1;
				$erreur_text="D - Formulaire incomplet. Impossible d'enregistrer les informations!";
			}
		}

		// ADD NECESSITE UN CLIENT
		if(empty($erreur_type)){

			// si on intervient chez un client ou si on crée une intra -> le client est obligatoire
			if( ($act_adr_ref==1 OR $act_pro_type==1) AND empty($client_id) ){
				$erreur_type=1;
				$erreur_text="E - Formulaire incomplet. Impossible d'enregistrer les informations!";
			}

		}

		// obligation d'avoir une agence
		if(empty($erreur_type)){

			$sql="SELECT soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville,soc_agence FROM Societes WHERE soc_id=:acc_societe;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":acc_societe",$acc_societe);
			$req->execute();
			$d_societe=$req->fetch();
			if(!empty($d_societe)){
				if(!empty($d_societe["soc_agence"]) AND empty($act_agence)){
					$erreur_type=1;
					$erreur_text="Impossible d'identifier l'agence.";
				}
			}
		}


		// DONNEE DU CLIENT
		// agence détermine par le formateur via l'organisation du planning
		// on controle que le client est bien accéssible à l'agence

		if(empty($erreur_type)){
			if(!empty($client_id)){
				$sql="SELECT cli_code,cli_filiale_de,cli_nom,cli_categorie,cli_stagiaire,cli_groupe,cli_filiale_de,cli_interco_soc,cli_facture_opca FROM Clients LEFT JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client)
				WHERE cso_client=:client AND cso_societe=:societe";
				if(!empty($act_agence)){
					$sql.=" AND cso_agence=:agence";
				}
				$sql.=";";
				$req=$Conn->prepare($sql);
				$req->bindParam(":client",$client_id);
				$req->bindParam(":societe",$acc_societe);
				if(!empty($act_agence)){
					$req->bindParam(":agence",$act_agence);
				}
				$req->execute();
				$d_client=$req->fetch();
				if(empty($d_client)){
					$erreur_type=1;
					$erreur_text="Impossible de récupérer les données du client!";
				}
			}else{
				$d_client=array(
					"cli_code" => "",
					"cli_nom" => "",
					"cli_filiale_de" => 0,
					"cli_categorie" => 0,
					"cli_stagiaire" => 0,
					"cli_groupe" => 0,
					"cli_filiale_de" => 0,
					"cli_facture_opca" => 0,
					"cli_interco_soc" => 0
				);
			}
		}

		// LE PRODUIT

		if(empty($erreur_type)){

			$act_gest_sta=1;
			$act_multi_session=0;

			$acl_facturation=0;
			$acl_derogation=0;
			$acl_rem_ca=1;
			$acl_rem_famille=1;

			$client_produit=$client_id;
			if(!empty($d_client["cli_filiale_de"])){
				$client_produit=$d_client["cli_filiale_de"];
			}

			$sql="SELECT pro_code_produit,pro_gestion_sta, pro_categorie,pro_famille,pro_sous_famille,pro_nb_demi_jour,pro_recurrent,pro_recur_mois,pro_min_intra,pro_min_inter,pro_qualification
			,pro_ht_intra,pro_ht_inter";
			$sql.=",cpr_cn,cpr_derogation,cpr_rem_famille,cpr_rem_ca,cpr_min_intra,cpr_min_inter";
			$sql.=" FROM Produits LEFT JOIN Produits_Familles ON (Produits.pro_famille=Produits_Familles.pfa_id)
			LEFT OUTER JOIN Clients_Produits ON (Produits.pro_id=Clients_Produits.cpr_produit AND cpr_client=:client_produit)
			WHERE pro_id=:produit;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":produit",$produit_id);
			$req->bindParam(":client_produit",$client_produit);
			$req->execute();
			$produit=$req->fetch();
			if(!empty($produit)){
				if(!empty($produit['pro_gestion_sta'])){
					$act_gest_sta = $produit['pro_gestion_sta'];
				}

				if($produit["pro_nb_demi_jour"]==1){
					$act_multi_session=1;
					//$act_gest_sta=2;
				}

				if(!empty($d_client["cli_interco_soc"])){

					// client interco

					// FG 17/12/2019. INTERCO Le tarif est 50% du TG peut importe la dero

					$produit["pro_min_intra"]=$produit["pro_ht_intra"]*0.5;
					$produit["cpr_min_intra"]=$produit["pro_ht_intra"]*0.5;

					$produit["pro_min_inter"]=$produit["pro_ht_inter"]*0.5;
					$produit["cpr_min_inter"]=$produit["pro_ht_inter"]*0.5;


				}elseif(!empty($produit["cpr_cn"]) AND $acc_societe!=4){

					if($d_client["cli_categorie"]==2){

						$acl_facturation=1;

						$produit["pro_min_intra"]=$produit["pro_min_intra"]*0.88;
						$produit["cpr_min_intra"]=$produit["cpr_min_intra"]*0.88;

						$produit["pro_min_inter"]=$produit["pro_min_inter"]*0.88;
						$produit["cpr_min_inter"]=$produit["cpr_min_inter"]*0.88;

					}else{
						$erreur_type=1;
						$erreur_text="Le client selectionné ne devrait pas disposer d'une facturation GC.";
					}
				}
				if(!empty($produit["cpr_derogation"])){
					$acl_derogation=$produit["cpr_derogation"];
					$acl_rem_ca=$produit["cpr_rem_ca"];
					$acl_rem_famille=$produit["cpr_rem_famille"];
				}

				$acl_pro_reference=$produit["pro_code_produit"];
			}
		}

		// devis associée (hors INETRCO ET GC)

		$acl_devis=0;
		$acl_devis_numero="";
		if(empty($erreur_type)){

			$devis_ligne=0;
			if(empty($d_client["cli_interco_soc"]) AND $d_client["cli_categorie"]!=2){
				if(!empty($_POST['devis_ligne'])){
					$devis_ligne=intval($_POST['devis_ligne']);
				}
			}
			if($devis_ligne>0){
				$sql="SELECT dev_id,dev_numero,dli_derogation,dli_rem_famille,dli_rem_ca,dli_ht,dli_qte FROM Devis,Devis_Lignes WHERE dev_id=dli_devis AND dli_id=:devis_ligne AND dev_client=:client AND dli_type=:type;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":devis_ligne",$devis_ligne);
				$req->bindParam(":client",$client_id);
				$req->bindParam(":type",$act_pro_type);
				$req->execute();
				$d_devis=$req->fetch();
				if(empty($d_devis)){
					$erreur_type=1;
					$erreur_text="Le devis selectionné ne correspond pas à la formation.";
				}else{

					// Les déro du devis surclasse celle de la base produit
					$acl_derogation=$d_devis["dli_derogation"];
					$acl_rem_ca=$d_devis["dli_rem_ca"];
					$acl_rem_famille=$d_devis["dli_rem_famille"];

					$acl_devis=$d_devis["dev_id"];
					$acl_devis_numero=$d_devis["dev_numero"];

				}
			}

		}

	//******************************************
	//	AUTRES CHAMPS DU FORM NON SOUMIS A CONTROLE
	//*******************************************/

		if(empty($erreur_type)){

			// le contact sur site
			$contact=0;
			$con_ref=0;
			$con_ref_id=0;
			if(!empty($_POST["act_contact"])){
				if($_POST["act_contact"]>0){
					$contact=$_POST["act_contact"];
					$con_ref=1;
					$con_ref_id=$client_id;
				}
			}

			$act_adr_ref_id=0;
			if($act_adr_ref==1){
				$act_adr_ref_id=$client_id;
			}


			$acl_confirme=0;
			$acl_confirme_date=null;
			if(!empty($_POST["acl_confirme"])){
				$acl_confirme=1;
				$acl_confirme_date=date("Y-m-d");
			}


			// les sessions
			$nb_session=1;
			if(!empty($_POST["nb_session"])){
				$nb_session=$_POST["nb_session"];
			}
			$session=array(
				1 => array(),
				2 => array()
			);

			for($i=1;$i<=$nb_session;$i++){
				for($j=1;$j<=2;$j++){
					$h_deb="";
					if(isset($_POST["h_deb_" . $j . "_" . $i])){
						$h_deb=$_POST["h_deb_" . $j . "_" . $i];
						if(strlen($h_deb)==2){
							$h_deb.="h00";
						}elseif(strlen($h_deb)==3){
							$h_deb.="00";
						}
					}
					$h_fin="";
					if(isset($_POST["h_fin_" . $j . "_" . $i])){
						$h_fin=$_POST["h_fin_" . $j . "_" . $i];
						if(strlen($h_fin)==2){
							$h_fin.="h00";
						}elseif(strlen($h_fin)==3){
							$h_fin.="00";
						}
					}
					if($nb_session==1 OR ( !empty($h_deb) AND !empty($h_fin) ) ){
						$session[$j][]=array(
							"deb" => $h_deb,
							"fin" => $h_fin
						);
					}
				}
			}


			// le vehicule
			$vehicule=0;
			if(!empty($_POST['vehicule'])){
				$vehicule=intval($_POST['vehicule']);
			}

			$salle=0;
			if(!empty($_POST['salle'])){
				$salle=intval($_POST['salle']);
			}

			$acl_attestation=0;
			if(!empty($_POST['acl_attestation'])){
				$acl_attestation=intval($_POST['acl_attestation']);
			}

			// tarif inconnu
			$acl_ca_nc=0;
			if(!isset($d_devis)){
				if(!empty($_POST["acl_ca_nc"])){
					$acl_ca_nc=1;
				}
			}
		}

	}else{
		$erreur_type=1;
		$erreur_text="E - Formulaire incomplet!";
	}
}else{
	$erreur_type=1;
	$erreur_text="F - Formulaire incomplet!";
}

	//******************************************
	//	DONNEE COMPLEMENTAIRE POUR ENREGISTREMENT
	//*******************************************/
	//donnee qui n'ont pas encore été récupéré pendant la phase de controle

if(empty($erreur_type)){

	// FAMILLE SOUS FAMILLE


	$act_alerte_jour=0;
	$act_alerte_date=Null;

	$sql="SELECT psf_libelle,psf_couleur_bg,psf_couleur_txt FROM Produits_Sous_Familles WHERE psf_id=" . $act_pro_sous_famille . ";";
	$req=$Conn->query($sql);
	$d_pro_s_fam=$req->fetch();

	$sql="SELECT pss_libelle,pss_couleur_bg,pss_couleur_txt FROM Produits_Sous_Sous_Familles WHERE pss_id=" . $act_pro_sous_sous_famille . ";";
	$req=$Conn->query($sql);
	$d_pro_s_s_fam=$req->fetch();

	if(!empty($d_pro_s_s_fam["pss_alerte_jour"])){
		$act_alerte_jour=$d_pro_s_s_fam["pss_alerte_jour"];
	}elseif(!empty($d_pro_s_fam["psf_alerte_jour"])){
		$act_alerte_jour=$d_pro_s_fam["pss_alerte_jour"];
	}
	if(!empty($act_alerte_jour)){
		$dt_alert = DateTime::createFromFormat('Y-m-d',$act_date_deb);
		$dt_alert->modify('-' . $act_alerte_jour . ' day');
		$act_alerte_date=$dt_alert->format('Y-m-d"');
	}

	// SUR LE VEHICULE

	if(!empty($vehicule)){
		$sql="SELECT veh_couleur FROM Vehicules WHERE veh_id=:vehicule;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":vehicule",$vehicule);
		$req->execute();
		$d_vehicule=$req->fetch();
		if(empty($d_vehicule)){
			unset($d_vehicule);
		}
	}

	// ADRESSE AGENCE
	if($act_adr_ref==2){

		if(!empty($acc_agence)){
			$sql="SELECT age_ad1,age_ad2,age_ad3,age_cp,age_ville FROM Agences WHERE age_id=:acc_agence;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":acc_agence",$acc_agence);
			$req->execute();
			$d_agence=$req->fetch();
			if(!empty($d_agence)){
				$act_adr1=$d_agence["age_ad1"];
				$act_adr2=$d_agence["age_ad2"];
				$act_adr3=$d_agence["age_ad3"];
				$act_adr_cp=$d_agence["age_cp"];
				$act_adr_ville=$d_agence["age_ville"];
			}
		}else{
			// info recup dans la partie controle
			if(!empty($d_societe)){
				$act_adr1=$d_societe["soc_ad1"];
				$act_adr2=$d_societe["soc_ad2"];
				$act_adr3=$d_societe["soc_ad3"];
				$act_adr_cp=$d_societe["soc_cp"];
				$act_adr_ville=$d_societe["soc_ville"];
			}
		}

	}

	// type d'inscription
	$acl_pro_inter=1;
	if($act_pro_type==1){
		$acl_pro_inter=0;
	}

	// TARIFS

	$ca=0;
	$ca_unit=0;
	$ca_gfc=0;

	if($act_pro_type==1){

		// INTRA
		$acl_qte_pm=count($case)-1;

		$coeff_ca=0;
		if(!empty($produit["pro_nb_demi_jour"])){
			$coeff_ca=$acl_qte_pm/$produit["pro_nb_demi_jour"];
		}

		if(isset($d_devis)){
			$ca=$d_devis["dli_ht"]*$coeff_ca;
		}else{
			if($acl_ca_nc==0){
				$ca=$_POST["ca_intra"];
				if(!empty($acl_derogation)){
					if($ca<($produit["cpr_min_intra"]*$coeff_ca)){
						$ca=$produit["cpr_min_intra"];
					}
				}elseif($ca<($produit["pro_min_intra"]*$coeff_ca)){
					$ca=$produit["pro_min_intra"];
				}
			}
		}
		$ca_unit=0;
		$for_nb_sta=0;


	}else{
		// INTER

		if(isset($d_devis)){
			$ca_unit=$d_devis["dli_ht"];
			$for_nb_sta=$d_devis["dli_qte"];
		}else{
			if($acl_ca_nc==0){
				$ca_unit=$_POST["ca_inter"];
				if(!empty($acl_derogation)){
					if($ca_unit<$produit["cpr_min_inter"]){
						$ca_unit=$produit["cpr_min_inter"];
					}
				}elseif($ca_unit<$produit["pro_min_inter"]){
					$ca_unit=$produit["pro_min_inter"];
				}
			}

			$for_nb_sta=$_POST["for_nb_sta"];
		}
		$ca=$ca_unit*$for_nb_sta;
		$acl_qte_pm=0;

	}
	// facturation region vers gfc : on calcul le ca de gfc vers client
	// controle pour que seul les GC aient un acl_facturation =1 et q'on ne puissent pas affecter de devis à une action GC
	if($acl_facturation==1){
		$ca_gfc=$ca/0.88;
	}

	// AFFICHAGE PLANNING

	if($act_pro_type==1){

		// INTRA
		$texte=$d_client["cli_code"];
		$texte_survol=$acl_pro_reference . " " . $act_adr_ville;


	}else{
		// INTER
		$texte=$act_adr_ville;
		$texte_survol=$act_adr_cp . " " . $act_adr_ville;
	}


	// style des cases
	$style="";
	$pda_style_bg="#FFF";
	$pda_style_txt="#333";
	if(isset($d_vehicule)){
		$pda_style_bg=$d_vehicule["veh_couleur"];
	}elseif(!empty($d_pro_s_s_fam["pss_couleur_bg"])){
		$pda_style_bg=$d_pro_s_s_fam["pss_couleur_bg"];
		$pda_style_txt=$d_pro_s_s_fam["pss_couleur_txt"];
	}elseif(!empty($d_pro_s_fam["psf_couleur_bg"])){
		$pda_style_bg=$d_pro_s_fam["psf_couleur_bg"];
		$pda_style_txt=$d_pro_s_fam["psf_couleur_txt"];
	}
	if($acl_confirme==0){
		$pda_style_txt="#FFF";
	}
	// le style "blocage admin est ajouté en automatique -> permet de conserver le style pour ne pas le recalculé à la levé du blocage.
	$style="background-color:" . $pda_style_bg . ";color:" . $pda_style_txt . ";";


	// SOUS-TRAITANCE
	// la presence de ST doit modifier le visuel des cases donc on doit avoir l'info avant l'enregistrement des dates.
	// si le visuel doit etre propre à chaque case nous pourrions le gérer dans la boucles d'enregistrement des cases.

	$act_verrou_admin=false;
	$tab_intervenants=array();
	foreach($planning_cases as $k => $c){
		if(!empty($c)){
			if(empty($tab_intervenants[$c["intervenant"]])){
				$tab_intervenants[$c["intervenant"]]=$c["intervenant"];
			}
		}
	}
	if(!empty($tab_intervenants)){

		$listeInt=implode(",",$tab_intervenants);

		$sql="SELECT * FROM Intervenants WHERE int_id IN (" . $listeInt . ") AND int_type IN (2,3);";
		$req=$ConnSoc->query($sql);
		$d_st=$req->fetchAll();
		if(!empty($d_st)){
			$act_verrou_admin=true;
		}
	}
	

}

	/*****************************
			ENREGISTREMENT
	*******************************/

// ENREGISTREMENT DE L'ACTION
if(empty($erreur_type)){

	$sql_action="INSERT INTO Actions (
	act_agence,
	act_adr_ref,
	act_adr_ref_id,
	act_adresse,
	act_adr_nom,
	act_adr_service,
	act_adr1,
	act_adr2,
	act_adr3,
	act_adr_cp,
	act_adr_ville,
	act_produit,
	act_pro_type,
	act_pro_categorie,
	act_pro_famille,
	act_pro_sous_famille,
	act_pro_sous_sous_famille,
	act_intervenant,
	act_date_deb,
	act_contact,
	act_con_ref,
	act_con_ref_id,
	act_con_nom,
	act_con_prenom,
	act_con_tel,
	act_con_portable,
	act_multi_session,
	act_gest_sta,
	act_commentaire,
	act_planning_txt,
	act_vehicule,
	act_salle,
	act_alerte_jour,
	act_alerte_date,
	act_verrou_admin,
	act_verrou_bc";
	if(!empty($id_heracles)){
		$sql_action.=",act_id";
	}
	$sql_action.=")
	VALUES (
	:act_agence,
	:act_adr_ref,
	:act_adr_ref_id,
	:act_adresse,
	:act_adr_nom,
	:act_adr_service,
	:act_adr1,
	:act_adr2,
	:act_adr3,
	:act_adr_cp,
	:act_adr_ville,
	:act_produit,
	:act_pro_type,
	:act_pro_categorie,
	:act_pro_famille,
	:act_pro_sous_famille,
	:act_pro_sous_sous_famille,
	:act_intervenant,
	:act_date_deb,
	:act_contact,
	:act_con_ref,
	:act_con_ref_id,
	:act_con_nom,
	:act_con_prenom,
	:act_con_tel,
	:act_con_portable,
	:act_multi_session,
	:act_gest_sta,
	:act_commentaire,
	:act_planning_txt,
	:act_vehicule,
	:act_salle,
	:act_alerte_jour,
	:act_alerte_date,
	:act_verrou_admin,
	:act_verrou_bc";
	if(!empty($id_heracles)){
		$sql_action.=",:act_id";
	}
	$sql_action.=");";

	$req_action=$ConnSoc->prepare($sql_action);
	if(!empty($id_heracles)){
		$req_action->bindParam(":act_id",$id_heracles);
	}
	$req_action->bindParam(":act_agence",$act_agence);
	$req_action->bindParam(":act_adr_ref",$act_adr_ref);
	$req_action->bindParam(":act_adr_ref_id",$act_adr_ref_id);
	$req_action->bindParam(":act_adresse",$adresse);
	$req_action->bindParam(":act_adr_nom",$_POST["act_adr_nom"]);
	$req_action->bindParam(":act_adr_service",$_POST["act_adr_service"]);
	$req_action->bindParam(":act_adr1",$_POST["act_adr1"]);
	$req_action->bindParam(":act_adr2",$_POST["act_adr2"]);
	$req_action->bindParam(":act_adr3",$_POST["act_adr3"]);
	$req_action->bindParam(":act_adr_cp",$act_adr_cp);
	$req_action->bindParam(":act_adr_ville",$act_adr_ville);
	$req_action->bindParam(":act_produit",$produit_id);
	$req_action->bindParam(":act_pro_type",$act_pro_type);
	$req_action->bindParam(":act_pro_categorie",$act_pro_categorie);
	$req_action->bindParam(":act_pro_famille",$act_pro_famille);
	$req_action->bindParam(":act_pro_sous_famille",$act_pro_sous_famille);
	$req_action->bindParam(":act_pro_sous_sous_famille",$act_pro_sous_sous_famille);
	$req_action->bindParam(":act_intervenant",$intervenant);
	$req_action->bindParam(":act_date_deb",$act_date_deb);
	$req_action->bindParam(":act_contact",$contact);
	$req_action->bindParam(":act_con_ref",$con_ref);
	$req_action->bindParam(":act_con_ref_id",$con_ref_id);
	$req_action->bindParam(":act_con_nom",$_POST["act_con_nom"]);
	$req_action->bindParam(":act_con_prenom",$_POST["act_con_prenom"]);
	$req_action->bindParam(":act_con_tel",$_POST["act_con_tel"]);
	$req_action->bindParam(":act_con_portable",$_POST["act_con_portable"]);
	$req_action->bindParam(":act_multi_session",$act_multi_session);
	$req_action->bindParam(":act_gest_sta",$act_gest_sta);
	$req_action->bindParam(":act_commentaire",$_POST["act_commentaire"]);
	$req_action->bindParam(":act_planning_txt",$texte_survol);
	$req_action->bindParam(":act_vehicule",$vehicule);
	$req_action->bindParam(":act_salle",$salle);
	$req_action->bindParam(":act_alerte_jour",$act_alerte_jour);
	$req_action->bindParam(":act_alerte_date",$act_alerte_date);
	$req_action->bindParam(":act_verrou_admin",$act_verrou_admin);
	$req_action->bindParam(":act_verrou_bc",$act_verrou_admin);
	try{
		$req_action->execute();
		$action=$ConnSoc -> lastInsertId();
	}Catch(Exception $e){
		$erreur_type=1;
		$erreur_text="B - " . $e->getMessage();
	}
}

// ENREGISTREMENT DES DATES
if(empty($erreur_type)){

	// ajout de date
	$sql_insert="INSERT INTO Plannings_Dates (pda_intervenant,pda_date,pda_demi,pda_type,pda_ref_1,pda_texte,pda_style_bg,pda_style_txt,pda_confirme,pda_survol,pda_vehicule,pda_salle,pda_alert)";
	$sql_insert.=" VALUES (:intervenant, :date, :demi, 1, :action, :texte, :style_bg, :style_txt, :confirme, :survol, :vehicule, :salle, :alert);";
	$req_insert=$ConnSoc->prepare($sql_insert);
	$req_insert->bindParam(":texte",$texte);
	$req_insert->bindParam(":style_bg",$pda_style_bg);
	$req_insert->bindParam(":style_txt",$pda_style_txt);
	$req_insert->bindParam(":confirme",$acl_confirme);
	$req_insert->bindParam(":action",$action);
	$req_insert->bindParam(":survol",$texte_survol);
	$req_insert->bindParam(":vehicule",$vehicule);
	$req_insert->bindParam(":salle",$salle);
	$req_insert->bindParam(":alert",$act_verrou_admin);

	// ajout de session
	$sql_session="INSERT INTO Actions_Sessions (ase_action,ase_date,ase_h_deb,ase_h_fin)";
	$sql_session.=" VALUES (:action, :date, :h_deb, :h_fin);";
	$req_session=$ConnSoc->prepare($sql_session);
	$req_session->bindParam(":action",$action);

	// format de retour
	$case_maj=array(
		"cases" =>array(
			"type" =>"1",
			"ref_1" =>$action,
			"texte" => $texte,
			"style" => $style,
			"survol" => $texte_survol,
			"date" => array(),
			"erreur" => 0
		),
		"erreur_type" => "",
		"erreur_text" => ""
	);
	if($act_verrou_admin){
		$case_maj["cases"]["style"]="background-color:#FFF;color:#F00;";
	}

	$act_date_h_def_1=0;
	$act_date_h_def_2=0;

	$session_id=array();

	// enregistrement des dates
	foreach($planning_cases as $k => $c){
		if(!empty($c)){

			$req_insert->bindParam(":intervenant",$c["intervenant"]);
			$req_insert->bindParam(":date",$c["date"]);
			$req_insert->bindParam(":demi",$c["demi"]);
			try{
				$req_insert->execute();
				$date =$ConnSoc -> lastInsertId();
			}Catch(Exception $e){
				$erreur_text="C - " . $e->getMessage();
				$erreur_type=1;
			}

			if(empty($erreur_text)){

				if($c["demi"]==1 AND $act_date_h_def_1==0){
					$act_date_h_def_1=$date;
				}elseif($c["demi"]==2 AND $act_date_h_def_2==0){
					$act_date_h_def_2=$date;
				}
				// ajout des sessions

				$req_session->bindParam(":date",$date);

				foreach($session[$c["demi"]] as $s){
					$req_session->bindParam(":h_deb",$s["deb"]);
					$req_session->bindParam(":h_fin",$s["fin"]);
					try{
						$req_session->execute();
						$session_id[]=array(
							"date"=> $date,
							"session" => $ConnSoc -> lastInsertId()
						);
					}Catch(Eception $e){
						$erreur_text="D - " . $e->getMessage();
						$erreur_type=1;
					}

				}
				$planning_cases[$k]["id"]=$date;

				// on ajoute la date dans le tableau de retour

				array_push($case_maj["cases"]["date"],$c["case"]);
			}

		}
	}
}

if(empty($erreur_type)){
	// mise a jour de l'action en fonction des dates

	$req=$ConnSoc->prepare("UPDATE Actions SET act_date_h_def_1=:act_date_h_def_1, act_date_h_def_2=:act_date_h_def_2 WHERE act_id=:action");
	$req->bindParam(":act_date_h_def_1",$act_date_h_def_1);
	$req->bindParam(":act_date_h_def_2",$act_date_h_def_2);
	$req->bindParam(":action",$action);
	$req->execute();

}

// SI ERREUR ACION MAL CREE -> on supprime tout
// dans le cas contraire les erreur suivante seront des warning
if(!empty($erreur_type)){

	if(!empty($action)){
		$sql_supp="DELETE FROM Actions WHERE act_id=:action;";
		$req_supp=$ConnSoc->prepare($sql_supp);
		$req_supp->bindParam(":action",$action);
		$req_supp->execute();

		$sql_supp="DELETE FROM Actions_Sessions WHERE ase_action=:action;";
		$req_supp=$ConnSoc->prepare($sql_supp);
		$req_supp->bindParam(":action",$action);
		$req_supp->execute();

		$sql_supp="DELETE FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=:action;";
		$req_supp=$ConnSoc->prepare($sql_supp);
		$req_supp->bindParam(":action",$action);
		$req_supp->execute();
	}

}

// ENREGISTREMENT DE L'INSCRIPTION DU CLIENT

$warning_text="";

if(empty($erreur_type)){

	// ON LIE LE CLIENT A L'Action
	if(!empty($client_id)){

		if(!empty($id_heracles)){
			$sql="SELECT acl_id FROM Actions_Clients WHERE acl_id=:acl_id;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":acl_id",$id_heracles);
			$req->execute();
			$acl_id_exists = $req->fetch();
		}

		if(empty($acl_id_exists)){
			$sql_client="INSERT INTO Actions_Clients (acl_action,acl_client,acl_client_societe,acl_produit,acl_commercial,acl_ca_unit,acl_ca,acl_ca_gfc
			,acl_pro_inter,acl_pro_reference,acl_creation_uti,acl_creation_date,acl_pro_libelle,acl_facturation,acl_pro_recurrent,acl_act_recurrent
			,acl_confirme,acl_confirme_date,acl_attestation,acl_for_nb_sta,acl_derogation,acl_rem_ca,acl_rem_famille,acl_qte_pm,acl_ca_nc,acl_devis,acl_devis_soc,acl_devis_numero,acl_devis_ligne
			,acl_opca_fac";
			if(!empty($id_heracles)){
				$sql_client.=",acl_id";
			}
			$sql_client.=")";
			$sql_client.=" VALUES (:action, :client, :client_societe, :produit, :commercial, :ca_unit, :ca, :ca_gfc
			,:pro_inter, :pro_reference, :creation_uti, NOW(), :pro_libelle, :acl_facturation,:pro_recurrent,:act_recurrent
			,:acl_confirme,:acl_confirme_date,:acl_attestation,:acl_for_nb_sta,:acl_derogation,:acl_rem_ca,:acl_rem_famille,:acl_qte_pm,:acl_ca_nc
			,:acl_devis,:acl_devis_soc,:acl_devis_numero,:acl_devis_ligne
			,:acl_opca_fac";
			if(!empty($id_heracles)){
				$sql_client.=",:acl_id";
			}
			$sql_client.=");";
			$req_client=$ConnSoc->prepare($sql_client);
			$req_client->bindParam(":action",$action);
			$req_client->bindParam(":client",$client_id);
			$req_client->bindParam(":client_societe",$client_societe);
			$req_client->bindParam(":produit",$produit_id);
			$req_client->bindParam(":commercial",$_POST["commercial"]);
			$req_client->bindParam(":ca_unit",$ca_unit);
			$req_client->bindParam(":ca",$ca);
			$req_client->bindParam(":ca_gfc",$ca_gfc);
			$req_client->bindParam(":pro_inter",$acl_pro_inter);
			$req_client->bindParam(":pro_reference",$acl_pro_reference);
			$req_client->bindParam(":creation_uti",$acc_utilisateur);
			$req_client->bindParam(":pro_libelle",$_POST["produit_libelle"]);
			$req_client->bindParam(":acl_facturation",$acl_facturation);
			$req_client->bindParam(":pro_recurrent",$produit["pro_recurrent"]);
			$req_client->bindParam(":act_recurrent",$act_recurrent_suiv);
			$req_client->bindParam(":acl_confirme",$acl_confirme);
			$req_client->bindParam(":acl_confirme_date",$acl_confirme_date);
			$req_client->bindParam(":acl_attestation",$acl_attestation);
			if(!empty($id_heracles)){
				$req_client->bindParam(":acl_id",$id_heracles);
			}
			$req_client->bindParam(":acl_for_nb_sta",$for_nb_sta);
			$req_client->bindParam(":acl_derogation",$acl_derogation);
			$req_client->bindParam(":acl_rem_ca",$acl_rem_ca);
			$req_client->bindParam(":acl_rem_famille",$acl_rem_famille);
			$req_client->bindParam(":acl_qte_pm",$acl_qte_pm);
			$req_client->bindParam(":acl_ca_nc",$acl_ca_nc);
			$req_client->bindParam(":acl_devis",$acl_devis);
			$req_client->bindParam(":acl_devis_soc",$acc_societe);
			$req_client->bindParam(":acl_devis_numero",$acl_devis_numero);
			$req_client->bindParam(":acl_devis_ligne",$devis_ligne);
			$req_client->bindParam(":acl_opca_fac",$d_client["cli_facture_opca"]);
			try{
				$req_client->execute();
				$action_client = $ConnSoc -> lastInsertId();
			}catch(Exception $e){
				$warning_text=$e->getMessage();
				$erreur_type=2;
			}
		}
		// par defaut on enregistre que le client participe à toute la formation
		if(empty($warning_text)){

			$sql_cd="INSERT INTO Actions_Clients_Dates (acd_action_client,acd_date)";
			$sql_cd.=" VALUES (:action_client, :date);";
			$req_cd=$ConnSoc->prepare($sql_cd);
			$req_cd->bindParam(":action_client",$action_client);

			foreach($planning_cases as $k => $c){
				$req_cd->bindParam(":date",$c["id"]);
				$req_cd->execute();
			}
		}

		// ON LIE LE DEVIS ET l'ACTION
		if(empty($warning_text)){
			if(!empty($devis_ligne)){

				$sql_dev="UPDATE Devis_Lignes SET dli_action=:action,dli_action_client=:action_client,dli_action_cli_soc=:acc_societe WHERE dli_id=:devis_ligne;";
				$req_dev=$ConnSoc->prepare($sql_dev);
				$req_dev->bindParam(":action",$action);
				$req_dev->bindParam(":action_client",$action_client);
				$req_dev->bindParam(":acc_societe",$acc_societe);
				$req_dev->bindParam(":devis_ligne",$devis_ligne);
				$req_dev->execute();


			}
		}

		// on inscvrit les stagiaires qui était enregistre sur le devis
		if(empty($warning_text)){

			$sql_get_dev_sta="SELECT dst_stagiaire FROM Devis_Stagiaires WHERE dst_devis_ligne=:devis_ligne;";
			$req_get_dev_sta=$ConnSoc->prepare($sql_get_dev_sta);
			$req_get_dev_sta->bindParam(":devis_ligne",$devis_ligne);
			$req_get_dev_sta->execute();
			$d_sta_dev=$req_get_dev_sta->fetchAll();
			if(!empty($d_sta_dev)){

				$sql_add_dev_sta="INSERT INTO Actions_stagiaires (ast_stagiaire,ast_action,ast_action_client,ast_confirme,ast_attestation,ast_attest_ok,ast_qualification)
				VALUES (:ast_stagiaire,:ast_action,:ast_action_client,:ast_confirme,:ast_attestation,:ast_attest_ok,:ast_qualification)";
				$req_add_dev_sta=$ConnSoc->prepare($sql_add_dev_sta);
				$req_add_dev_sta->bindParam("ast_action",$action);
				$req_add_dev_sta->bindParam("ast_action_client",$action_client);
				$req_add_dev_sta->bindValue("ast_confirme",1);
				$req_add_dev_sta->bindParam("ast_attestation",$acl_attestation);
				$req_add_dev_sta->bindValue("ast_attest_ok",1);
				$req_add_dev_sta->bindParam("ast_qualification",$produit["pro_qualification"]);

				$sql_add_sta_ses="INSERT INTO Actions_stagiaires_Sessions (ass_stagiaire,ass_session,ass_action)
				VALUES (:ass_stagiaire,:ass_session,:ass_action);";
				$req_add_sta_ses=$ConnSoc->prepare($sql_add_sta_ses);
				$req_add_sta_ses->bindParam("ass_action",$action);

				foreach($d_sta_dev as $s_d){

					$req_add_dev_sta->bindParam("ast_stagiaire",$s_d["dst_stagiaire"]);
					$req_add_dev_sta->execute();

					if(!empty($session_id)){
						foreach($session_id as $s_cle => $s_id){
							$req_add_sta_ses->bindParam(":ass_session",$s_id["session"]);
							$req_add_sta_ses->bindParam(":ass_stagiaire",$s_d["dst_stagiaire"]);
							$req_add_sta_ses->execute();
						}
					}

				}
			}

		}

		// INSCRIPTION DU STAGIAIRE SI CLIENT PARTICULIER

		if(empty($warning_text)){
			if($d_client["cli_categorie"]==3 AND !empty($d_client["cli_stagiaire"])){

				// on enregistre le fait que le stagiaire participe à l'action
				$sql_sta="INSERT INTO Actions_stagiaires (ast_stagiaire,ast_action,ast_action_client,ast_confirme,ast_valide)";
				$sql_sta.=" VALUES (" . $d_client["cli_stagiaire"] . "," . $action . "," . $action_client . ",0,0);";
				$req_sta=$ConnSoc->query($sql_sta);

				// on l'associe à toutes les sessions

				if(!empty($session_id)){
					$sql_sta="INSERT INTO Actions_stagiaires_Sessions (ass_stagiaire,ass_session,ass_action)";
					$sql_sta.=" VALUES (" . $d_client["cli_stagiaire"] . ",:session," . $action . ");";
					$req_sta=$ConnSoc->prepare($sql_sta);
					foreach($session_id as $s_cle => $s_id){
						$req_sta->bindParam(":session",$s_id["session"]);
						$req_sta->execute();
					}

				}
			}
		}
	}
}

if(!empty($erreur_type)){
	if($erreur_type==1){
		$case_maj=array(
			"erreur_type" => 1,
			"erreur_code" => $erreur_code,
			"erreur_text" => $erreur_text
		);
	}elseif(!empty($warning_text)){
		$case_maj["erreur_type"]=2;
		$case_maj["erreur_text"]=$warning_text;
	}
}
echo json_encode($case_maj);

?>
