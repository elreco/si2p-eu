<?php

include_once('../connexion_soc.php');
if(isset($_POST)){


	$code=$_POST['code'];
	$nom=rawurldecode($_POST['nom']);
	$siret=$_POST['siret'];
	$siren=$_POST['siren'];

	global $ConnSoc;
	
	$sql="SELECT * FROM clients";

	$mil="";
	if($code!=""){
		$mil.="  OR cli_code = :code"; 
	};
	if($siren!=""){
		$mil.="  OR cli_siren = :siren"; 
	};
	if($nom!=""){
		$mil.="  OR cli_nom = :nom"; 
	};

	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$req = $ConnSoc->prepare($sql);

	/////////////// bind params
	if($code!=""){
		$req->bindParam("code",$code);
	};
	if($siren!=""){
		$req->bindParam("siren",$siren);
	};
	if($nom!=""){
		$req->bindParam("nom",$nom);
	};
	///////////////
	$req->execute();
	$clients = $req->fetchAll();

	$sql2="SELECT * FROM suspects";

	$mil2="";
	if($code!=""){
		$mil2.="  OR sus_code = :code"; 
	};
	if($siren!=""){
		$mil2.="  OR sus_siren = :siren"; 
	};
	if($nom!=""){
		$mil2.="  OR sus_nom = :nom"; 
	};

	if($mil2!=""){
		$sql2.=" WHERE " . substr($mil2, 5, strlen($mil2)-5);
	}

	$req2 = $ConnSoc->prepare($sql2);

	if($code!=""){
		$req2->bindParam("code",$code);
	};
	if($siren!=""){
		$req2->bindParam("siren",$siren);
	};
	if($nom!=""){
		$req2->bindParam("nom",$nom);
	};

	$req2->execute();
	$suspects = $req2->fetchAll();
	if(!empty($siret) && $siret != " "){
		$sql3="SELECT * FROM suspects_adresses WHERE sad_siret = :siret";

		$req3 = $ConnSoc->prepare($sql3);
		$req3->bindParam("siret",$siret);
		$req3->execute();
		$sirets = $req3->fetchAll();

		$sql4="SELECT * FROM adresses WHERE adr_siret = :sireti";

		$req4 = $ConnSoc->prepare($sql4);
		$req4->bindParam("sireti",$siret);
		$req4->execute();
		$sirets2 = $req4->fetchAll();

	}
	


	if(empty($suspects) && empty($clients) && empty($sirets) && empty($sirets2)){
		echo "OK";
	}elseif(!empty($sirets) OR !empty($sirets2)){
		echo "SIRET";
	}else{
		echo "NO";
	}
	

}
?>