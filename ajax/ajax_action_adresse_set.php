<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // SCRIPT GENERIQUE POUR METTRE A JOUR N CHAMP DE ACTIONS_CLIENTS

 // retour JSON compatible select2
 
 
$erreur_txt="";
if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]); 
	}
	$adr_ref=0;
	if(!empty($_POST["adr_ref"])){
		$adr_ref=intval($_POST["adr_ref"]); 
	}

	if(empty($action_id) OR empty($adr_ref) OR empty($_POST["adr_cp"]) OR empty($_POST["adr_ville"]) ){
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();	
	}
}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}

// TRAITEMENT DES REQUIRED FORM 

// champ required 
$adr_ref_id=0;
$adresse=0;
if($adr_ref==1){
	if(!empty($_POST["client"])){
		$adr_ref_id=intval($_POST["client"]);
	}
	if(!empty($_POST["adresse"])){
		$adresse=intval($_POST["adresse"]);
	}
	if(empty($adr_ref_id) OR empty($adresse)){
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();
	}
}

$adr_cp="";
if(!empty($_POST["adr_cp"])){
	$adr_cp=$_POST["adr_cp"];
}
$adr_ville="";
if(!empty($_POST["adr_ville"])){
	$adr_ville=$_POST["adr_ville"];
}
if($adr_ref!=2 AND (empty($adr_cp) OR empty($adr_ville)) ){
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}

// DONNE COMP & CONTROLE

// personne connecte
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
}

// action
$sql="SELECT act_agence FROM Actions WHERE act_id=:action;";
$req=$ConnSoc->prepare($sql);
$req->bindParam(":action",$action_id);
$req->execute();
$d_action=$req->fetch();
if(empty($d_action)){
	$erreur_txt="Impossible d'identifier la formation!";
	echo($erreur_txt);
	die();
}

// SURCLASSEMENT DU FORM
if($adr_ref==2){
	
	$contact=0;
	$con_nom="";
	$con_prenom="";
	$con_tel="";
	$con_portable="";
	
	if(!empty($d_action["act_agence"])){
		
		$sql="SELECT age_nom,age_ad1,age_ad2,age_ad3,age_cp,age_ville FROM Agences WHERE age_id=:agence;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":agence",$d_action["act_agence"]);
		$req->execute();
		$d_agence=$req->fetch();
		if(!empty($d_agence)){
			$adr_nom=$d_agence["age_nom"];
			$adr_service="";		
			$adr1=$d_agence["age_ad1"];
			$adr2=$d_agence["age_ad2"];
			$adr3=$d_agence["age_ad3"];
			$adr_cp=$d_agence["age_cp"];
			$adr_ville=$d_agence["age_ville"];
		}else{
			$erreur_txt="Impossible d'identifier l'agence d'intervention!";
			echo($erreur_txt);
			die();
		}
	}else{
		
		$sql="SELECT soc_nom,soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville FROM Societes WHERE soc_id=:societe;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":societe",$acc_societe);
		$req->execute();
		$d_agence=$req->fetch();
		if(!empty($d_agence)){
			$adr_nom=$d_agence["soc_nom"];
			$adr_service="";		
			$adr1=$d_agence["soc_ad1"];
			$adr2=$d_agence["soc_ad2"];
			$adr3=$d_agence["soc_ad3"];
			$adr_cp=$d_agence["soc_cp"];
			$adr_ville=$d_agence["soc_ville"];
		}else{
			$erreur_txt="Impossible d'identifier l'agence d'intervention!";
			echo($erreur_txt);
			die();
		}
	}
	
}else{
	
	// TRAITEMENT DES AUTRES CHAMP DU FORM
	$adr_nom="";
	if(!empty($_POST["adr_nom"])){
		$adr_nom=$_POST["adr_nom"];
	}
	$adr_service="";
	if(!empty($_POST["adr_service"])){
		$adr_service=$_POST["adr_service"];
	}
	$adr1="";
	if(!empty($_POST["adr1"])){
		$adr1=$_POST["adr1"];
	}
	$adr2="";
	if(!empty($_POST["adr2"])){
		$adr2=$_POST["adr2"];
	}
	$adr3="";
	if(!empty($_POST["adr3"])){
		$adr3=$_POST["adr3"];
	}
	$contact=0;
	if(!empty($_POST["contact"])){
		$contact=intval($_POST["contact"]);
	}
	$con_nom="";
	if(!empty($_POST["con_nom"])){
		$con_nom=$_POST["con_nom"];
	}
	$con_prenom="";
	if(!empty($_POST["con_prenom"])){
		$con_prenom=$_POST["con_prenom"];
	}
	$con_tel="";
	if(!empty($_POST["con_tel"])){
		$con_tel=$_POST["con_tel"];
	}
	$con_portable="";
	if(!empty($_POST["con_portable"])){
		$con_portable=$_POST["con_portable"];
	}
}

// ENREGISTREMENT 
$sql="UPDATE Actions SET 
act_adr_ref=:adr_ref, 
act_adr_ref_id=:adr_ref_id,
act_adresse=:adresse,
act_adr_nom=:adr_nom,
act_adr_service=:adr_service,
act_adr1=:adr1,
act_adr2=:adr2,
act_adr3=:adr3,
act_adr_cp=:adr_cp,
act_adr_ville=:adr_ville,
act_contact=:contact,
act_con_nom=:con_nom,
act_con_prenom=:con_prenom,
act_con_tel=:con_tel,
act_con_portable=:con_portable
WHERE act_id=:action;";
$req=$ConnSoc->prepare($sql);
$req->bindParam(":adr_ref",$adr_ref);
$req->bindParam(":adr_ref_id",$adr_ref_id);
$req->bindParam(":adresse",$adresse);
$req->bindParam(":adr_nom",$adr_nom);
$req->bindParam(":adr_service",$adr_service);
$req->bindParam(":adr1",$adr1);
$req->bindParam(":adr2",$adr2);
$req->bindParam(":adr3",$adr3);
$req->bindParam(":adr_cp",$adr_cp);
$req->bindParam(":adr_ville",$adr_ville);
$req->bindParam(":contact",$contact);
$req->bindParam(":con_nom",$con_nom);
$req->bindParam(":con_prenom",$con_prenom);
$req->bindParam(":con_tel",$con_tel);
$req->bindParam(":con_portable",$con_portable);
$req->bindParam(":action",$action_id);
try{
	$req->execute();
}Catch(Exception $e){
	$erreur_text="UPDATE ACTION " . $e->getMessage();
	echo($erreur_txt);
	die();
}
$retour=array(
	"adr_nom" => $adr_nom,
	"adr_service" => $adr_service,
	"adr1" => $adr1,
	"adr2" => $adr2,
	"adr3" => $adr3,
	"adr_cp" => $adr_cp,
	"adr_ville" => $adr_ville
);
echo(json_encode($retour));
die();
?>