<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';
include '../modeles/mod_add_notification.php';

 // ARCHIVE UNE FORMATION COMPLETTE 
 // ACTION et ACTIONS CLIENTS
 
 // RETOUR FORMAT JSON
 
$erreur_text="";
$retour=array(
	"notif" => ""
);

if(isset($_POST)){
	 
	$action_id=0;
	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]); 
	}
	$action_client_id=0;
	if(!empty($_POST["action_client"])){
		$action_client_id=intval($_POST["action_client"]); 
	}
	if(empty($action_id) OR empty($action_client_id)){
		$erreur_text="Formulaire incomplet";
	}
	
}else{
	$erreur_text="Formulaire incomplet";
}

if(empty($erreur_text)){	
	if(!$_SESSION["acces"]["acc_droits"][10]){
		$erreur_text="Vous ne disposez pas des droits nécéssaire!";
	}
}

// CONTROLE
if(empty($erreur_text)){
	$sql="SELECT act_agence,cli_nom FROM Actions INNER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)
	INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)
	WHERE acl_action=:action_id AND acl_id=:action_client_id;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action_client_id",$action_client_id);
	$req->bindParam(":action_id",$action_id);
	$req->execute();
	$d_action_client=$req->fetch();
	if(empty($d_action_client)){
		$erreur_text="Impossible de charger l'inscription du client";
	}
}		

// MAJ 

if(empty($erreur_text)){
	
	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}

	$archive_ok=0;
	if($_SESSION["acces"]["acc_droits"][11]){
		$archive_ok=1;
	}
	
	// ON ARCHIVE DU CLIENT
	
	$sql="UPDATE Actions_Clients SET 
	acl_archive = 1,
	acl_archive_date=NOW(),
	acl_archive_uti=" . $acc_utilisateur . ",
	acl_archive_ok=" . $archive_ok . " 
	WHERE NOT acl_archive AND acl_action=" . $action_id . " AND acl_id=" . $action_client_id . ";";
	try {
		$req=$ConnSoc->query($sql);				
	}catch( PDOException $Exception ) {
		$erreur_text="Impossible d'archiver les inscriptions des clients " . $Exception->getMessage();	
	}
	
}
if(empty($erreur_text)){
	
	$retour["notif"]="L'inscription du client " . $d_action_client["cli_nom"] . " sur l'action N°" . $action_id . " à bien été archivée"; 
	$texte=$_SESSION["acces"]["acc_prenom"] . " " . $_SESSION["acces"]["acc_nom"] . " a archivé le client " . $d_action_client["cli_nom"] . " sur l'action N°" . $action_id; 
	$url="action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id;

	$sql="SELECT uti_responsable FROM utilisateurs WHERE uti_id = :uti_id";
					$req=$Conn->prepare($sql);
					$req->bindParam(":uti_id",$_SESSION["acces"]["acc_ref_id"]);
					$req->execute();
					$uti_co=$req->fetch();

					$sql="SELECT * FROM utilisateurs WHERE uti_id = :uti_id AND uti_id != 98";
					$req=$Conn->prepare($sql);
					$req->bindParam(":uti_id",$uti_co['uti_responsable']);
					$req->execute();
					$responsable=$req->fetch();
	if(!empty($responsable)){
		$sql="SELECT * FROM utilisateurs_droits WHERE udr_utilisateur = :uti_id AND udr_droit = 11";
		$req=$Conn->prepare($sql);
		$req->bindParam(":uti_id",$responsable['uti_id']);
		$req->execute();
		$responsable_droit=$req->fetch();

		if(!empty($responsable_droit)){
			add_notifications("<i class='fa fa-times' ></i>",$texte,"bg-danger",$url,"","",$responsable['uti_id'],$acc_societe,$d_action_client["act_agence"],1);
		}else{
			$sql="SELECT * FROM utilisateurs WHERE uti_responsable = :uti_id AND uti_id != 98";
			$req=$Conn->prepare($sql);
			$req->bindParam(":uti_id",$responsable['uti_id']);
			$req->execute();
			$responsable_bis=$req->fetch();
			add_notifications("<i class='fa fa-times' ></i>",$texte,"bg-danger",$url,"","",$responsable_bis['uti_id'],$acc_societe,$d_action_client["act_agence"],1);
		}
	}
	
	
	
}
if(empty($erreur_text)){
	echo json_encode($retour);
}else{
	echo($erreur_text);
};
 
 
?>