<?php

include("../includes/connexion.php");

if(isset($_POST['agence'])){

	$agence_id=0;
	if(!empty($_POST['agence'])){
		$agence_id=intval($_POST['agence']);
	}
 
     global $Conn;

    $req=$Conn->query("SELECT * FROM Agences WHERE age_id =" . $agence_id);
    $result = $req->fetch();
	if(!empty($result)){
		// on supprime le soc pour pouvoir traiter de la mm façon json societe et json agence
		$agence=array(
			"nom" => $result["age_nom"],
			"ad1" => $result["age_ad1"],
			"ad2" => $result["age_ad2"],
			"ad3" => $result["age_ad3"],
			"cp" => $result["age_cp"],
			"ville" => $result["age_ville"],
			"siren" => $result["age_siren"],
			"siret" => $result["age_siret"],
			"ape" => $result["age_ape"],
			"tva" => $result["age_tva"]
		);
	}
	
    echo json_encode($agence);

}
?>