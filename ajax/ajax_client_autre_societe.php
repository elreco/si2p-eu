<?php
 // PERMET DE PRE-SELECTIONNER DES CLIENTS D'APRES UNE SAISIE UTILISATEUR
 // SOURCE : client sur ORION_$acc_societe

session_start();
include('../includes/connexion_fct.php');

$erreur="";
$param="";
if($_GET['q']){
	$param=$_GET['q'] . "%";
}
if(isset($_GET['societe'])){
	$societe=$_GET['societe'];
}else{
	$societe=0;
}

if(isset($_GET['agence'])){
	$agence=$_GET['agence'];
}else{
	$agence=0;
}

/* $acces
	1 -> exclut les clients CN
*/
// DONNEE POUR TRAITEMENT

// sur la personne connecte
$acc_utilisateur=0;
if($_SESSION['acces']['acc_ref']==1){
	if(isset($_SESSION['acces']["acc_ref_id"])){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
}

$sql="SELECT DISTINCT cli_id,cli_nom,cli_code FROM clients WHERE (cli_nom LIKE '" . $param . "' OR cli_code LIKE '" . $param . "')";
// critère auto selon connection
if(!$_SESSION['acces']["acc_droits"][6]){
	$sql.=" AND cli_utilisateur=" . $acc_utilisateur;
}
if($agence>0){
	$sql.=" AND cli_agence=" . $agence;
}

$sql.=" ORDER BY cli_nom";
$ConnFct=connexion_fct($societe);
$req = $ConnFct->query($sql);

$clients = $req->fetchAll();
if(!empty($clients)){
	foreach($clients as $c){
		$retour[] = array("id"=>$c['cli_id'], "nom"=>$c['cli_nom'] . " (" . $c['cli_code'] . ")");
	}
}

// ENVOIE DU RESULTAT
if(!empty($retour)){
	echo json_encode($retour);
}
?>
