<?php

include("../includes/connexion.php");

if(isset($_POST['contact'])){

	$contact_id=0;
	if(!empty($_POST['contact'])){
		$contact_id=intval($_POST['contact']);
	}
 
     global $Conn;

    $req=$Conn->query("SELECT * FROM Fournisseurs_Contacts LEFT JOIN Contacts_fonctions ON(Fournisseurs_Contacts.fco_fonction=Contacts_fonctions.cfo_id) WHERE fco_id =" . $contact_id);
    $result = $req->fetch();
	if(!empty($result)){
		// on créer le tableau
		$contact=array(
			"fco_id" => $result["fco_id"],
			"fco_defaut" => $result["fco_defaut"],
			"fco_fonction" => $result["fco_fonction"],
			"fco_fonction_nom" => $result["fco_fonction_nom"],
			"cfo_libelle" => $result["cfo_libelle"],
			"fco_titre" => $result["fco_titre"],
			"fco_nom" => $result["fco_nom"],
			"fco_prenom" => $result["fco_prenom"],
			"fco_tel" => $result["fco_tel"],
			"fco_fax" => $result["fco_fax"],
			"fco_portable" => $result["fco_portable"],
			"fco_mail" => $result["fco_mail"]
		);
	}
	
    echo json_encode($contact);

}
?>