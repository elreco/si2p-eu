<?php

	// Mise à jour d'un compte utilisateur
	
	include("../includes/connexion.php");
	
	$ref=0;
	if(!empty($_GET["ref"])){
		$ref=$_GET["ref"];
	}
	
	$ref_id=0;
	if(!empty($_GET["ref_id"])){
		$ref_id=$_GET["ref_id"];
	}
	$competence=0;
	if(!empty($_GET["competence"])){
		$competence=$_GET["competence"];
	}
	if($ref>0 AND $ref_id>0 AND $competence>0){
		
		$coche=0;
		if($_GET["coche"]=="true"){			
			$coche=1;
		}
		
		global $Conn;
		
		$req = $Conn->prepare("UPDATE Intervenants_Competences SET ico_valide=:coche WHERE ico_ref=:ref AND ico_ref_id=:ref_id AND ico_competence=:competence");
		$req->bindParam(":ref",$ref);
		$req->bindParam(":ref_id",$ref_id);
		$req->bindParam(":competence",$competence);
		$req->bindParam(":coche",$coche);
		$req->execute();

	}else{
		header("HTTP/1.0 500 ");
	};
?>	
