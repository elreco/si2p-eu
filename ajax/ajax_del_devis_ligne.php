<?php
 // SUPPRIMER UNE LIGNE DE DEVIS
 
session_start();
include('../includes/connexion.php');
include('../includes/connexion_soc.php');

$erreur="";

/* UTILISATION 

- devis_voir

*/

// parametre

if(isset($_POST)){
		
	$devis=0;
	if(!empty($_POST['devis'])){
		$devis=intval($_POST['devis']);
	}
	
	$devis_ligne=0;
	if(!empty($_POST['devis_ligne'])){
		$devis_ligne=intval($_POST['devis_ligne']);
	}
	
	if(empty($devis_ligne) OR empty($devis)){
		$erreur="formulaire incomplet!";
	}
}

if(empty($erreur)){
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	
	
	// taux de tva
	// on est obligé de connaitre tous les taux de tva pour pouvoir les afficher / masquer suite a modif
	$tva=array();
	$req=$Conn->query("SELECT DISTINCT tpe_taux FROM Tva_periodes ORDER BY tpe_taux;");
	$d_tva_taux=$req->fetchAll();
	if(!empty($d_tva_taux)){
		foreach($d_tva_taux as $tt){
			$tva[$tt["tpe_taux"]]=0;
		}
		
	}
	// tab retour JSON
	
	$result=array(
		"ligne" => $devis_ligne,
		"tva" => $tva,
		"ht" => 0,
		"ttc" => 0,
		"revente" => 0,
		"warning" =>""
	);
	
	// RECUP DES DONNEES CONCERNANT LE DEVIS ET LA LIGNE
	$sql="SELECT dev_bc,dev_fournisseur,dli_action_cli_soc,dli_action_client FROM Devis_Lignes,Devis WHERE dli_devis=dev_id AND dev_id=" . $devis . " AND dli_id=" . $devis_ligne . ";";
	$req=$ConnSoc->query($sql);
	$d_ligne=$req->fetch();
	if(empty($d_ligne)){
		$erreur="Erreur! Impossible de supprimer la ligne. (Err 001)!";
	}
	
	// LIGNES DEVIS APRES SUPPRESSION
	if(empty($erreur)){
		$revente=0;
		$total_tva=0;
		$sql="SELECT SUM(dli_montant_ht) AS montant_ht,dli_tva_taux,dli_revente FROM Devis_Lignes 
		WHERE dli_devis=" . $devis . " AND NOT dli_id=" . $devis_ligne . " GROUP BY dli_tva_taux,dli_revente;";
		$req=$ConnSoc->query($sql);
		$d_lignes=$req->fetchAll();
		if(!empty($d_lignes)){
			foreach($d_lignes as $l){			
				if($l["dli_revente"]==1){
					$revente=1;	
				}
				$montant_tva=round($l["montant_ht"]*($l["dli_tva_taux"]/100),2);				
				$result["tva"][$l["dli_tva_taux"]]+=$montant_tva;
				$result["ht"]+=$l["montant_ht"];
				$result["ttc"]+=($l["montant_ht"]+$montant_tva);
			}	
		}
		$result["revente"]=$revente;
		
		/*echo("<pre>");
			print_r($result);
		echo("</pre>");*/
	}
	
	if(empty($erreur)){
		// LA LIGNE EST LIE A UNE ACTION
		// on bloque la supp si l'action est confirmée.
		if(!empty($d_ligne["dli_action_client"])){			

			include('../includes/connexion_fct.php');
			
			$ConnFct=connexion_fct($d_ligne["dli_action_cli_soc"]);
			
			$sql="SELECT acl_id FROM Actions_Clients WHERE acl_id=" . $d_ligne["dli_action_client"] . " AND acl_confirme=1 AND acl_archive=0;";
			$req=$ConnFct->query($sql);
			$d_action=$req->fetch();
			if(!empty($d_action)){
				$erreur="Cette ligne est liée à une action confirmée. Suppression impossible!";
			}
		}
	}
	
	// SUPPRESSION ET TRAITEMENT ASSOCIE
	if(empty($erreur)){

		// SUPRESSION DE LA LIGNE
		$sql="DELETE FROM Devis_Lignes WHERE dli_devis=" . $devis . " AND dli_id=" . $devis_ligne . ";";
		try{
			$ConnSoc->query($sql);
		}Catch(Exception $e){
			$erreur=$e->getMessage();
		}

		// SUPP DES STAGIAIRES
		$sql="DELETE FROM Devis_Stagiaires WHERE dst_devis=" . $devis . " AND dst_devis_ligne=" . $devis_ligne . ";";
		try{
			$ConnSoc->query($sql);
		}Catch(Exception $e){
			$result["warning"].=$e->getMessage();
		}
		
		// ON ACTUALISE LE DEVIS
		/* si le devis est lié à un un BC on conserve le lien même s'il n'y a plus de revente */
		$sql="UPDATE Devis SET dev_total_ht=" . $result["ht"] . ",dev_total_ttc=" . $result["ttc"];
		if(empty($revente) AND empty($d_ligne["dev_bc"]) ){
			$sql.=",dev_fournisseur=0";
		}
		$sql.=" WHERE dev_id=" . $devis . ";";
		try{
			$ConnSoc->query($sql);
		}Catch(Exception $e){
			$result["warning"].=$e->getMessage();
		}
		
		// LA LIGNE ETAIT LIE A UNE ACTION	
		if(!empty($d_ligne["dli_action_client"])){			
			/* -> si controle ok = action en projet -> on supp le lien action /devis 
			-> pas d'archivage automatique TROP RISQUE*/

			$sql="UPDATE Actions_Clients SET acl_devis=0,acl_devis_soc=0,acl_devis_numero='' 
			WHERE acl_id=" . $d_ligne["dli_action_client"] . " AND acl_devis=" . $devis . " AND acl_devis_soc=" . $acc_societe . ";";
			try{
				$ConnFct->query($sql);
			}Catch(Exception $e){
				$result["warning"].=$e->getMessage();
			}
		}	
	}
}

if(!empty($erreur)){
	echo($erreur);
}else{
	echo(json_encode($result));
}
?>
