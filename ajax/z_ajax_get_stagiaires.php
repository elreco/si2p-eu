<?php

include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../connexion_soc.php';

/* RETOURNE RETOURNES UNE LISTE DE STAGIAIRES

// retour => tableau JSON

// option action_client_id>0 // permet de retourver client_id pour restreindre le resultat aux stagiaires d'un client

// auteur : FG 27/10/2016
	MAJ 27/09/2016 utilisation de mod_get_produits_ref()
*/

$erreur=0;
$produits=array();

if(isset($_GET)){
	 
	$sta_nom=0;
	if(!empty($_GET["sta_nom"])){
		$sta_nom=$_GET["sta_nom"] . "%"; 
	}else{
		$erreur=1;
	}	

	if($erreur==0){
		
		$action_client_id=0;
		if(!empty($_GET['action_client'])){
			$action_client_id=intval($_GET['action_client']);
		}

		$client_id=0;
		if($action_client_id>0){
			$sql="SELECT acl_client FROM Actions_Clients WHERE acl_id=:action_client;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client",$action_client_id);
			$req->execute();
			$client=$req->fetch();
			if(!empty($client)){
				$client_id=$client["acl_client"];
			}
		}
		
		
		$sql="SELECT sta_id,sta_nom,sta_prenom,sta_naissance FROM Stagiaires";
		if($client_id>0){
			$sql.=",Stagiaires_Clients WHERE sta_id=scl_stagiaire AND scl_client=:client AND sta_nom LIKE :nom";
		}else{
			$sql.=" WHERE sta_nom LIKE :nom";	
		}
		$sql.=" ORDER BY sta_nom,sta_prenom,sta_naissance;";
		
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":nom",$sta_nom);
		if($client_id>0){
			$req->bindParam(":client",$client_id);
		}
		$req->execute();
		$stagiaires=$req->fetchAll();
		if(!empty($stagiaires)){
			foreach($stagiaires as $r){
				$retour[] = array("id"=>$r['sta_id'], "nom"=>$r['sta_nom'] . " " . $r['sta_prenom']);
			}
		}

	}
}else{
	$erreur=1;
}
if($erreur==0){
	
	if(isset($retour)){
		echo json_encode($retour);
	}
	
}else{
	header("HTTP/1.0 500 ");
};
?>
