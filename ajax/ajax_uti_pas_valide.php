<?php

	// permet d'affecter ou de retirer un droit à un profil ou à un utilisateur

	include("../includes/connexion.php");
	include("../modeles/mod_droit.php");
	include("../modeles/mod_utilisateur.php");

	$utilisateur=0;
	if(!empty($_GET["utilisateur"])){
		$utilisateur=$_GET["utilisateur"];
	}

    $sql="SELECT uti_eva_pas_valide_tech FROM Utilisateurs WHERE uti_id = " . $utilisateur;
    $req=$Conn->query($sql);
    $uti =$req->fetch();

    if(empty($uti['uti_eva_pas_valide_tech'])){
        $uti_eva_pas_valide_tech = 1;
    }else{
        $uti_eva_pas_valide_tech = 0;
    }
    $sql="UPDATE Utilisateurs SET uti_eva_pas_valide_tech = " . $uti_eva_pas_valide_tech . " WHERE uti_id = " . $utilisateur;
    $req=$Conn->query($sql);

    echo json_encode("ok");
?>
