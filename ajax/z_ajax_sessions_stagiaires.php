<?php 

include("../includes/connexion.php");
include_once('../modeles/mod_parametre.php');

$req = $Conn->prepare("SELECT * FROM sessions_stagiaires LEFT JOIN 
 sessions ON sessions.ses_id = sessions_stagiaires.sst_session WHERE sst_stagiaire = " . $_POST['sta_id']);
$req->execute();
$sessions_stagiaires = $req->fetchAll();

foreach($sessions_stagiaires as $k=>$s){
	$retour[$k] = array(
		"ses_date" => convert_date_fr($s['ses_date']),
		"ses_h_deb" => $s['ses_h_deb'],
		"ses_h_fin" => $s['ses_h_fin'],
		);
}

echo json_encode($retour);


?>