<?php
 // 
 
session_start();

include('../includes/connexion_soc.php');

include('../modeles/mod_devis_lignes_set.php');

$erreur="";

/* UTILISATION 

- devis_voir

*/

// parametre

if(isset($_POST)){
	
	$dli_id=0;
	if(!empty($_POST['dli_id'])){
		$dli_id=intval($_POST['dli_id']);
	}
	
	$dli_devis=0;
	if(!empty($_POST['dli_devis'])){
		$dli_devis=intval($_POST['dli_devis']);
	}
	
	if(empty($dli_id) OR empty($dli_devis)){
		$erreur="formulaire incomplet!";
	}
}

if(empty($erreur)){
	
	$donnees=array();
	$donnees[]=$_POST;
	
	$result=set_devis_lignes($donnees);
	if(is_bool($result)){
		if($result){
			$erreur=$result;
		}else{
			$erreur=$result;
		}
	}else{
		$erreur=$result;
	}
}

if(!empty($erreur)){
	echo($erreur);
}else{
	echo(json_encode($_POST));
}
?>
