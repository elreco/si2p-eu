<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion_soc.php';

/************************
	ENREGISTREMENT D'UN REMBOURSEMENT SUR UN REGLEMENT
***************************
*/

$erreur_txt="";
if(isset($_POST)){

	if(!empty($_POST["chp_date"])){
		$DT_periode=date_create_from_format('d/m/Y',$_POST["chp_date"]);
		if(!is_bool($DT_periode)){
			$chp_date=$DT_periode->format("Y-m-d");
		}else{
			$erreur_txt="Formulaire incomplet!";
		}
	}else{
		$erreur_txt="Formulaire incomplet!";
	}

	if(!empty($_POST["reg_rb"])){
		if(is_array($_POST["reg_rb"])){
			$reg_rb=$_POST["reg_rb"]; 
		}else{
			$erreur_txt="Formulaire incomplet!";
		}
	}else{
		$erreur_txt="Formulaire incomplet!";
	}

	if(empty($erreur_txt)){
		
		$reg_rb_liste=implode(",",$reg_rb);
		
		$sql="UPDATE Reglements SET reg_rb=1,reg_rb_date=:reg_rb_date WHERE reg_id IN (" . $reg_rb_liste . ");"; 
		$req = $ConnSoc->prepare($sql);
		$req->bindParam(":reg_rb_date",$chp_date);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur_txt="Impossible de mettre les règlements à jour. (" . $e->getMessage() . ")";
		}
		
	}
	
	if(empty($erreur_txt)){
		
		$retour=array(
			"date" => $_POST["chp_date"],
			"reglements" => $reg_rb
		);
	
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}
if(empty($erreur_txt)){
	echo json_encode($retour);
}else{
	echo($erreur_txt);
}
 
?>