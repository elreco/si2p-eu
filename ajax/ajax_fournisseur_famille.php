<?php 
include "../includes/controle_acces.inc.php";
include("../includes/connexion.php");

if($_POST['ajout'] == 1){
	$req = $Conn->prepare("INSERT INTO fournisseurs_familles_jointure (ffj_famille, ffj_fournisseur) VALUES (:ffj_famille, :ffj_fournisseur)");
	$req->bindParam(':ffj_famille', $_POST['famille']);
	$req->bindParam(':ffj_fournisseur', $_POST['fournisseur']);
	$req->execute();
}else{
	$req = $Conn->prepare("DELETE FROM fournisseurs_familles_jointure WHERE ffj_famille = :ffj_famille AND ffj_fournisseur = :ffj_fournisseur");
	$req->bindParam(':ffj_famille', $_POST['famille']);
	$req->bindParam(':ffj_fournisseur', $_POST['fournisseur']);
	$req->execute();
}


?>