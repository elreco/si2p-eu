<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';

 // SCRIPT GENERIQUE POUR METTRE A JOUR N CHAMP DE ACTIONS_CLIENTS

 // retour JSON compatible select2
 
 
$erreur_txt="";
if(isset($_POST)){
	 
	$acl_id=0;
	if(!empty($_POST["acl_id"])){
		$acl_id=intval($_POST["acl_id"]); 
	}
	
	if(empty($acl_id)){
		
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();
		
	}
}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}

if(count($_POST)<=1){
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}
	
$sql="UPDATE Actions_Clients SET ";
foreach($_POST as $k => $v){
	if($k!="acl_id" AND $k!="societ"){
		$sql.=$k . "=:" . $k . ",";
	}
}
$sql=substr($sql,0,-1);
$sql.=" WHERE acl_id=:acl_id;";

$req=$ConnSoc->prepare($sql);
foreach($_POST as $k => $v){
	if($k!="societ"){
		$req->bindValue($k,$v);
	}
}
try{
	$req->execute();
}Catch(Exception $e){
	$erreur_txt=$e->getMessage();
	echo($erreur_txt);
	die();
}

echo(json_encode($_POST));
die();
?>