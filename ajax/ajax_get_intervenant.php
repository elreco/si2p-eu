<?php

include "../includes/controle_acces.inc.php";
include '../connexion_soc.php';
include '../modeles/mod_get_intervenant.php';

/* RETOURNE LES DONNEES D'UN INTERVENANT

	retour => tableau JSON

	auteur : FG 14/11/2016

*/

$erreur=0;
if(isset($_POST)){
	 
	$intervenant=0;
	if(!empty($_POST["intervenant"])){
		$intervenant=$_POST["intervenant"]; 
	}else{
		$erreur=1;
	}	

	if($erreur==0){
		
		

		$results=get_intervenant($intervenant);
		echo json_encode($results);
	}
}else{
	$erreur=10;
}

?>
