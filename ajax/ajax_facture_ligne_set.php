<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';


include '../modeles/mod_get_client_produits.php';

 // MISE A JOUR ou CREATION d'une ligne de facture

$erreur_txt="";
if(isset($_POST)){
	 
	$fli_produit=0;
	if(!empty($_POST["fli_produit"])){
		$fli_produit=intval($_POST["fli_produit"]); 
	}
	$fli_facture=0;
	if(!empty($_POST["fli_facture"])){
		$fli_facture=intval($_POST["fli_facture"]); 
	}

	$fli_libelle="";
	if(!empty($_POST["fli_libelle"])){
		$fli_libelle=$_POST["fli_libelle"];
	}

	if(empty($fli_produit) OR empty($fli_facture) OR empty($fli_libelle) ){
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();	
	}
}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}

// DONNE COMP & CONTROLE

// personne connecte
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
}
// formulaire

$fli_id=0;
if(!empty($_POST["fli_id"])){
	$fli_id=intval($_POST["fli_id"]);
}
$fli_intra_inter=0;
$fli_action_client=0;

	// ca
	$fli_qte=0;
	if(!empty($_POST["fli_qte"])){
		$fli_qte=floatval($_POST["fli_qte"]);
	}
	$fli_montant_ht=0;
	if(!empty($_POST["fli_montant_ht"])){
		$fli_montant_ht=floatval($_POST["fli_montant_ht"]);
	}
	$pu=0;
	if($fli_qte!=0){
		$pu=round(($fli_montant_ht/$fli_qte),2);	
	}

// societe
$sql="SELECT soc_tva_exo FROM Societes WHERE soc_id=:acc_societe;";
$req=$Conn->prepare($sql);
$req->bindParam(":acc_societe",$acc_societe);
$req->execute();
$d_societe=$req->fetch();
if(empty($d_societe)){
	$erreur_txt="Impossible de charger les données de la société!";
	echo($erreur_txt);
	die();
}

// facture
$sql="SELECT fac_client,fac_nature,fac_cli_geo,fac_date FROM Factures WHERE fac_id=:fli_facture;";
$req=$ConnSoc->prepare($sql);
$req->bindParam(":fli_facture",$fli_facture);
$req->execute();
$d_facture=$req->fetch();
if(empty($d_facture)){
	$erreur_txt="Impossible de charger les données de la facture!";
	echo($erreur_txt);
	die();
}
// produit 
/* on utilise le modele get_client_produits() pour recuperer les spécificité du produit lié au client */

$d_produit=get_client_produits($d_facture["fac_client"],$fli_produit,1,0,1,null,null,null,null,null,null);
if(empty($d_produit)){
	$erreur_txt="Impossible de charger les données du produit!";
	echo($erreur_txt);
	die();
}else{
	
	if($fli_intra_inter==2){
		
		// produit inter
		
	}else{
		
		// produit intra		
		$fli_ht=round($d_produit["tg_intra"],2);
		
		if($d_produit["ca_verrou_intra"]){
			
			$pu=round($d_produit["ht_intra"],2);
			
			$fli_montant_ht=$pu*$fli_qte;
			$fli_montant_ht=round($fli_montant_ht,2);
			
		}elseif($d_produit["min_intra"]>0 AND $d_produit["min_intra"]>$pu){
			$pu=round($d_produit["min_intra"],2);
			
			$fli_montant_ht=$pu*$fli_qte;
			$fli_montant_ht=round($fli_montant_ht,2);
		}
	}
}

// calcul des remise
$fli_remise=round(($fli_ht-$pu),2);
$fli_taux_remise=0;
if(!empty($fli_ht)){
	$fli_taux_remise=($fli_remise*100)/$fli_ht;
	$fli_taux_remise=round($fli_taux_remise,2);
}

// calcul de la TVA

$fli_exo=0;
if($d_facture["fac_cli_geo"]!=1){
	$fli_tva_id=3;
	$fli_tva_per=3;
	$fli_tva_taux=0;
}elseif(!empty($d_societe["soc_tva_exo"]) AND !empty($d_produit["pro_deductible"])){
	$fli_exo=1;
	$fli_tva_id=3;
	$fli_tva_per=3;
	$fli_tva_taux=0;
}else{
	
	$fli_tva_id=$d_produit["tva"];
	
	$sql="SELECT tpe_id,tpe_taux FROM Tva_Periodes WHERE tpe_tva=:fli_tva_id AND tpe_date_deb<=:fac_date AND (tpe_date_fin>=:fac_date OR ISNULL(tpe_date_fin));";
	$req=$Conn->prepare($sql);
	$req->bindParam(":fli_tva_id",$fli_tva_id);
	$req->bindParam(":fac_date",$d_facture["fac_date"]);
	$req->execute();
	$d_tva=$req->fetch();
	if(!empty($d_tva)){
		$fli_tva_per=$d_tva["tpe_id"];
		$fli_tva_taux=$d_tva["tpe_taux"];
	}else{
		$erreur_txt="Impossible de calculer la TVA!";
		echo($erreur_txt);
		die();
	}
}
	/*echo("fli_ht : ");
	var_dump($fli_ht);
	echo("<br/>");
	
	echo("fli_qte : ");
	var_dump($fli_qte);
	echo("<br/>");
	
	echo("fli_remise : ");
	var_dump($fli_remise);
	echo("<br/>");
	
	echo("fli_montant_ht : ");
	var_dump($fli_montant_ht);
	echo("<br/>");
	
	echo("fli_tva_id : ");
	var_dump($fli_tva_id);
	echo("<br/>");
	
	echo("fli_tva_taux : ");
	var_dump($fli_tva_taux);
	echo("<br/>");
	
	echo("fli_tva_per : ");
	var_dump($fli_tva_per);
	echo("<br/>");
	
	die();*/


// ENREGISTREMENT 
if($fli_id>0){
	$sql="UPDATE Factures_Lignes SET 
	fli_produit=:fli_produit, 
	fli_code_produit=:fli_code_produit,
	fli_categorie=:fli_categorie,
	fli_famille=:fli_famille,
	fli_sous_famille=:fli_sous_famille,
	fli_sous_sous_famille=:fli_sous_sous_famille,
	fli_action_client=:fli_action_client,
	fli_action_cli_soc=:fli_action_cli_soc,
	fli_intra_inter=:fli_intra_inter,
	fli_libelle=:fli_libelle,
	fli_formation=:fli_formation,
	fli_complement=:fli_complement,
	fli_ht=:fli_ht,
	fli_qte=:fli_qte,
	fli_remise=:fli_remise,
	fli_taux_remise=:fli_taux_remise,
	fli_montant_ht=:fli_montant_ht,
	fli_tva_id=:fli_tva_id,
	fli_tva_taux=:fli_tva_taux,
	fli_tva_per=:fli_tva_per,
	fli_exo=:fli_exo
	WHERE fli_id=:fli_id AND fli_facture=:fli_facture;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":fli_produit",$fli_produit);
	$req->bindParam(":fli_code_produit",$d_produit["reference"]);
	$req->bindParam(":fli_categorie",$d_produit["categorie"]);
	$req->bindParam(":fli_famille",$d_produit["famille"]);
	$req->bindParam(":fli_sous_famille",$d_produit["sous_famille"]);
	$req->bindParam(":fli_sous_sous_famille",$d_produit["sous_sous_famille"]);
	$req->bindParam(":fli_action_client",$fli_action_client);
	$req->bindParam(":fli_action_cli_soc",$fli_action_cli_soc);
	$req->bindParam(":fli_intra_inter",$fli_intra_inter);
	$req->bindParam(":fli_libelle",$fli_libelle);
	$req->bindParam(":fli_formation",$_POST["fli_formation"]);
	$req->bindParam(":fli_complement",$_POST["fli_complement"]);
	$req->bindParam(":fli_ht",$fli_ht);
	$req->bindParam(":fli_qte",$fli_qte);
	$req->bindParam(":fli_remise",$fli_remise);
	$req->bindParam(":fli_taux_remise",$fli_taux_remise);
	$req->bindParam(":fli_montant_ht",$fli_montant_ht);
	$req->bindParam(":fli_tva_id",$fli_tva_id);
	$req->bindParam(":fli_tva_taux",$fli_tva_taux);
	$req->bindParam(":fli_tva_per",$fli_tva_per);
	$req->bindParam(":fli_exo",$fli_exo);
	$req->bindParam(":fli_id",$fli_id);
	$req->bindParam(":fli_facture",$fli_facture);
	try{
		$req->execute();
	}Catch(Exception $e){
		$erreur_text="UPDATE Facture " . $e->getMessage();
		echo($erreur_txt);
		die();
	}
}else{
	
	$sql="INSERT INTO Factures_Lignes (
	fli_facture,fli_produit,fli_code_produit,fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,fli_action_client,fli_action_cli_soc,fli_intra_inter,fli_libelle
	,fli_formation,fli_complement,fli_ht,fli_qte,fli_remise,fli_taux_remise,fli_montant_ht,fli_tva_id,fli_tva_taux,fli_tva_per,fli_exo) VALUES (
	:fli_facture,:fli_produit,:fli_code_produit,:fli_categorie,:fli_famille,:fli_sous_famille,:fli_sous_sous_famille,:fli_action_client,:fli_action_cli_soc,:fli_intra_inter,:fli_libelle
	,:fli_formation,:fli_complement,:fli_ht,:fli_qte,:fli_remise,:fli_taux_remise,:fli_montant_ht,:fli_tva_id,:fli_tva_taux,:fli_tva_per,:fli_exo
	);";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":fli_facture",$fli_facture);
	$req->bindParam(":fli_produit",$fli_produit);
	$req->bindParam(":fli_code_produit",$d_produit["reference"]);
	$req->bindParam(":fli_categorie",$d_produit["categorie"]);
	$req->bindParam(":fli_famille",$d_produit["famille"]);
	$req->bindParam(":fli_sous_famille",$d_produit["sous_famille"]);
	$req->bindParam(":fli_sous_sous_famille",$d_produit["sous_sous_famille"]);
	$req->bindParam(":fli_action_client",$fli_action_client);
	$req->bindParam(":fli_action_cli_soc",$fli_action_cli_soc);
	$req->bindParam(":fli_intra_inter",$fli_intra_inter);
	$req->bindParam(":fli_libelle",$fli_libelle);
	$req->bindParam(":fli_formation",$_POST["fli_formation"]);
	$req->bindParam(":fli_complement",$_POST["fli_complement"]);
	$req->bindParam(":fli_ht",$fli_ht);
	$req->bindParam(":fli_qte",$fli_qte);
	$req->bindParam(":fli_remise",$fli_remise);
	$req->bindParam(":fli_taux_remise",$fli_taux_remise);
	$req->bindParam(":fli_montant_ht",$fli_montant_ht);
	$req->bindParam(":fli_tva_id",$fli_tva_id);
	$req->bindParam(":fli_tva_taux",$fli_tva_taux);
	$req->bindParam(":fli_tva_per",$fli_tva_per);
	$req->bindParam(":fli_exo",$fli_exo);
	try{
		$req->execute();
		$fli_id=$ConnSoc->lastInsertId();
	}Catch(Exception $e){
		$erreur_text="INSERT Facture " . $e->getMessage();
		echo($erreur_txt);
		die();
	}
}

	
	// MAJ DE LA FACTURE
	$fac_total_ht=0;
	$tva=array();
	$fac_total_ttc=0;
	
	$sql="SELECT SUM(fli_montant_ht) as fli_montant_ht,fli_tva_taux FROM Factures_Lignes WHERE fli_facture=" . $fli_facture . " GROUP BY fli_tva_taux;";
	$req=$ConnSoc->query($sql);
	$d_lignes=$req->fetchAll();
	if(!empty($d_lignes)){
		foreach($d_lignes as $l){
			
			$fac_ht=floatval($l["fli_montant_ht"]);
			$fac_tva_taux=floatval($l["fli_tva_taux"]);
			
			$fac_tva=$fac_ht*($fac_tva_taux/100);
			$fac_tva=round($fac_tva,2);
			
			$fac_ttc=round(($fac_ht+$fac_tva),2);
			
			if(!isset($tva[$fac_tva_taux])){
				$tva[$fac_tva_taux]=0;
			}
			$tva[$fac_tva_taux]=$tva[$fac_tva_taux] + $fac_tva;
			
			
			$fac_total_ht=$fac_total_ht + $fac_ht;
			$fac_total_ttc=$fac_total_ttc + $fac_ttc;
		}
	}
	
	// UPDATE DE LA FACTURE
	$warning_text="";
	$sql="UPDATE Factures SET 
	fac_total_ht=:fac_total_ht, 
	fac_total_ttc=:fac_total_ttc
	WHERE fac_id=:fli_facture;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":fac_total_ht",$fac_total_ht);
	$req->bindParam(":fac_total_ttc",$fac_total_ttc);
	$req->bindParam(":fli_facture",$fli_facture);
	try{
		$req->execute();
	}Catch(Exception $e){
		$warning_text="UPDATE Facture " . $e->getMessage();
	}

$retour=array(
	"ligne" => $fli_id,
	"code" => $d_produit["reference"],
	"libelle" => $fli_libelle,
	"formation" => $_POST["fli_formation"],
	"complement" => $_POST["fli_complement"],
	"pu" => $pu,
	"qte" => $fli_qte,
	"montant_ht" => $fli_montant_ht,
	"total_ht" => $fac_total_ht,
	"tva" => $tva,
	"total_ttc" => $fac_total_ttc,
	"warning_text" => $warning_text
);
echo(json_encode($retour));
die();
?>