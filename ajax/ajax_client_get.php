<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';

/************************
	RETOURNE LES DONNEES ENREGISTRES DANS LA TABLE CLIENTS 0 POUR UN ID EN PARTICULIER
***************************
 
 parametre POST

// client : ID clients 

// facultatif
- categorie >0  categorie du client si elle est indiqué, elle permet de déterminer dans quelle table est stocké le client
- adresse_type >0 active le recherche du carnet d'acresse du client pour le type en question
- contact 0 ou 1 : active le recherche des contact du client

	RETOUR
	retourne toujours un tableau JSON succes ou erreur -> C'est au callback de gérer la reussite ou l'echec
	echec clé erreur_txt = motif de l'erreur
	
	retour => tableau JSON ATTENTION 2 dimension meme si il n'y a ni adresse ni contact

// Appel :
// planning.php
// devis.php

ile est possible de recuperer séparément les adresses et les contacts avec ajax_get_adresses, ajax_get_contacts ... 

// auteur : FG 06/09/2016
 
*/
$erreur_txt="";
if(isset($_POST)){
	 
	$client=0;
	if(!empty($_POST["client"])){
		$client=intval($_POST["client"]); 
	}else{
		$erreur_txt="Formulaire incomplet!";
	}

	$agence=0;
	if(!empty($_POST["agence"])){
		$agence=intval($_POST["agence"]); 
	}

	$societe=0;
	if(!empty($_POST["societe"])){
		$societe=intval($_POST["societe"]); 
	}
	
	
	$adr_type=0;
	if(!empty($_POST["adr_type"])){
		$adr_type=intval($_POST["adr_type"]); 
	}
	
	$adr_contact=0;
	if(!empty($_POST["adr_contact"])){
		$adr_contact=intval($_POST["adr_contact"]); 
	}		
	
	if(empty($erreur_txt)){
		
		// sur la personne connecte
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);	
		}
		
		// DONNEE DU CLIENT
		
		$sql="SELECT cso_commercial,cli_categorie,cli_id,cli_reg_formule,cli_reg_nb_jour,cli_reg_fdm,cli_facture_opca,cli_opca,cli_reg_type
		,cli_groupe,cli_filiale_de,cli_niveau,cli_fil_de,cli_interco_soc
		FROM clients LEFT JOIN clients_societes ON clients.cli_id=clients_societes.cso_client 
		WHERE cli_id=:client";
		if(!empty($agence)){
			$sql.=" AND cso_agence=:agence";
		}
		$req = $Conn->prepare($sql);
		$req->bindParam(":client",$client);
		if(!empty($agence)){
			$req->bindParam(":agence",$agence);
		}
		$req->execute();
		$result = $req->fetchAll();
		
		if(!empty($result)){
				$client_info=array(
					"commercial" => $result[0]["cso_commercial"],
					"categorie" => $result[0]["cli_categorie"],
					"interco_soc" => $result[0]["cli_interco_soc"],
					"reg_formule" => $result[0]["cli_reg_formule"],
					"reg_nb_jour" => $result[0]["cli_reg_nb_jour"],
					"reg_type" => $result[0]["cli_reg_type"],
					"reg_fdm" => $result[0]["cli_reg_fdm"],
					"adr_int" => array(),
					"int_defaut" => 0,
					"adr_fac" => array(),
					"fac_defaut" => 0,
					"con_int" => array(),
					"con_fac" => array(),
					"erreur_txt" => "",
					"facture_opca" => $result[0]["cli_facture_opca"],
					"opca" => $result[0]["cli_opca"],
				);
				
				//ON DEMANDE LES ADRESSES DES PARENTS
				if($adr_type==91 OR $adr_type==92){

					if($result[0]["cli_groupe"]==1 AND !empty($result[0]["cli_filiale_de"])){
					
						// PERMTINENT QUE QI LE CLIENT EST UNE FILIALE
						
						$clientsCarnet=$client . "," . $result[0]["cli_filiale_de"];
						
						if($result[0]["cli_niveau"]>0){
							
							// systeme de recherche des entites parents
													
							$entite_parent=array(
								"holding" => $result[0]["cli_filiale_de"],
								"entite" => $result[0]["cli_fil_de"],
								"secure" => 0
							);
							
							while ($entite_parent["entite"]!=$entite_parent["holding"] AND $entite_parent["secure"]<6){
								
								$sql="SELECT cli_id,cli_fil_de FROM Clients WHERE cli_id=" . $entite_parent["entite"] . ";";
								$req=$Conn->query($sql);
								$result_cli=$req->fetch();
								if(!empty($result_cli)){
									
									$clientsCarnet.="," . $result_cli["cli_id"];
									
									$entite_parent["entite"]=$result_cli["cli_fil_de"];
									$entite_parent["secure"]++;
									
								}else{
									$entite_parent["secure"]=999;
									break;
								}
							}
							
						}
					}
					// MAJ LA VAL $adr_type -> a partir de la $clientsCarnet indique qu'on doit recuperer les carnets parents
					$adr_type=$adr_type-90;
					
				}
		}else{
			$erreur_txt="Impossible de recuperer les données du client!";
		}
		
		
		if(empty($erreur_txt)){
			
			if($adr_type>0){
				
				$adr_fac=0;
				$adr_int=0;
				
				
				
				$sql_con="SELECT con_id,con_nom,con_prenom,con_tel,con_portable,aco_defaut,aco_adresse 
				FROM Contacts LEFT OUTER JOIN Adresses_Contacts ON (Contacts.con_id=Adresses_Contacts.aco_contact)
				WHERE (ISNULL(aco_adresse) OR aco_adresse=:adresse) AND con_ref_id=" . $client . " ORDER BY con_nom,con_prenom;";
				$req_con = $Conn->prepare($sql_con);
				
				$sql="SELECT adr_id,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_defaut,adr_ref_id,adr_type,adr_geo";
				$sql.=" FROM Adresses WHERE adr_ref=1";
				if(isset($clientsCarnet)){
					$sql.=" AND adr_ref_id IN (" . $clientsCarnet . ")";	
				}else{
					$sql.=" AND adr_ref_id=" . $client;
				}
				
				if($client_info["categorie"]==3){
					$sql.=" AND adr_type=2"; 
				}elseif($adr_type==999){
					$sql.=" AND adr_type IN (1,2)";
				}else{
					$sql.=" AND adr_type=" . $adr_type;
				}
				$sql.=" ORDER BY adr_nom;";				
				$req = $Conn->query($sql);
				$results = $req->fetchAll();
				if(!empty($results)){				
					foreach($results as $r){						
						if($r["adr_type"]==1 OR ($adr_type==1 AND $client_info["categorie"]==3)){
							// pour les particulier on utilise l'adresse de factuartion comme adresse d'intrevention
							// pas d'adresse d'intervention
							$client_info["adr_int"][]=array(
								"id" => $r["adr_id"],		// nécessaire pour injection plugin select2
								"text" => $r["adr_nom"] . " " . $r["adr_cp"] . " " . $r["adr_ville"],	// nécessaire pour injection plugin select2
								"ref_id" => $r["adr_ref_id"],
								"nom" => $r["adr_nom"],
								"service" => $r["adr_service"],
								"ad1" => $r["adr_ad1"],
								"ad2" => $r["adr_ad2"],
								"ad3" => $r["adr_ad3"],
								"cp" => $r["adr_cp"],
								"ville" => $r["adr_ville"],
								"defaut" => $r["adr_defaut"],
								"geo" => $r["adr_geo"]
							);
							
							if($r["adr_defaut"]){
								$client_info["int_defaut"]=$r["adr_id"];
							}						
							
						}elseif($r["adr_type"]==2){
							
							$adr_txt=$r["adr_nom"] . " " . $r["adr_cp"] . " " . $r["adr_ville"];
							
							$client_info["adr_fac"][]=array(
								"id" => $r["adr_id"],		// nécessaire pour injection plugin select2
								"text" => $adr_txt,			// nécessaire pour injection plugin select2
								"ref_id" => $r["adr_ref_id"],
								"nom" => $r["adr_nom"],
								"service" => $r["adr_service"],
								"ad1" => $r["adr_ad1"],
								"ad2" => $r["adr_ad2"],
								"ad3" => $r["adr_ad3"],
								"cp" => $r["adr_cp"],
								"ville" => $r["adr_ville"],
								"defaut" => $r["adr_defaut"],
								"geo" => $r["adr_geo"]
							);
							
							if($r["adr_defaut"] AND $r["adr_ref_id"]==$client){
								$client_info["fac_defaut"]=$r["adr_id"];
																
							};
						}
					}
				}
				
				// contact
				
				if(!empty($client_info["fac_defaut"]) AND ($adr_contact==2 OR $adr_contact==999)){					
					$req_con->bindParam(":adresse",$client_info["fac_defaut"]);
					$req_con->execute();
					$contacts = $req_con->fetchAll();
					if(!empty($contacts)){
						foreach($contacts as $r){							
							$client_info["con_fac"][]=array(
								"id" => $r["con_id"],
								"text" => $r["con_nom"],
								"nom" => $r["con_nom"],
								"prenom" => $r["con_prenom"],
								"tel" => $r["con_tel"],
								"portable" => $r["con_portable"],
								"defaut" => $r["aco_defaut"]
							);	
						}
					}
				}
				if(!empty($client_info["int_defaut"]) AND ($adr_contact==1 OR $adr_contact==999)){					
					$req_con->bindParam(":adresse",$client_info["int_defaut"]);
					$req_con->execute();
					$contacts = $req_con->fetchAll();
					if(!empty($contacts)){
						foreach($contacts as $r){							
							$client_info["con_int"][]=array(
								"id" => $r["con_id"],
								"text" => $r["con_nom"],
								"nom" => $r["con_nom"],
								"prenom" => $r["con_prenom"],
								"tel" => $r["con_tel"],
								"portable" => $r["con_portable"],
								"defaut" => $r["aco_defaut"]
							);	
						}
					}
				}
			}
		}
		
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}
if(empty($erreur_txt)){
	/*echo("<pre>");
		print_r($client_info);
	echo("</pre>");*/
	echo json_encode($client_info);
}else{
	$client_info=array(
		"erreur_txt" => $erreur_txt
	);
	echo json_encode($client_info);
};
 
?>