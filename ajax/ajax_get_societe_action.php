<?php
include("../includes/connexion.php");
include("../includes/connexion_soc.php");
include("../includes/connexion_fct.php");
include("../modeles/mod_parametre.php");

if(isset($_GET['client']) && isset($_GET['societe'])){

    global $Conn;

    $client = $_GET['client'];
    $societe = $_GET['societe'];
    $action = $_GET['action'];
    $action_client = $_GET['action_client'];
    $famille = $_GET['famille'];

    // ALLER CHERCHER L'ACTION
    $sql="SELECT * FROM actions WHERE act_id = :action";
    $req = $ConnSoc->prepare($sql);
    $req->bindParam(":action",$action);
    $req->execute();
    $d_action = $req->fetch();
    // FIN ALLER CHERCHER L'ACTION

    // ALLER CHERCHER LES DATES
    $sql="SELECT * FROM actions_clients_dates
     LEFT JOIN actions_clients ON (actions_clients.acl_id = actions_clients_dates.acd_action_client)
     LEFT JOIN plannings_dates ON (plannings_dates.pda_id = actions_clients_dates.acd_date)
    WHERE acd_action_client = :action_client
    ORDER BY pda_date ASC";
    $req = $ConnSoc->prepare($sql);
    $req->bindParam(":action_client",$action_client);
    $req->execute();
    $d_actions_dates = $req->fetchAll();
    // FIN ALLER CHERCHER LES DATES
    $date_deb = $d_actions_dates[0]['pda_date'];
    $date_fin = $d_actions_dates[0]['pda_date'];
    // ALLER CHERCHER LA DATE DE FIN
    foreach($d_actions_dates as $k=>$acd){
        if($acd['pda_date'] > $date_fin){
            $date_fin = $acd['pda_date'];
        }
    }
    ///////////////////// INTERCO //////////////////////////
    if(!empty($d_actions_dates)){
        $ConnFct=connexion_fct($societe);
        // CHERCHER LES ACTIONS CORRESPONDANTES SUR LE PLANNING
        $sql="SELECT DISTINCT act_id, acl_id, act_date_deb, acl_pro_libelle FROM plannings_dates
        LEFT JOIN Actions_Clients_Dates ON (Actions_Clients_Dates.acd_date = plannings_dates.pda_id)
        LEFT JOIN Actions_Clients ON (Actions_Clients.acl_id = Actions_Clients_Dates.acd_action_client)
        LEFT JOIN Actions ON (Actions.act_id = Actions_clients.acl_action)
        WHERE pda_date >= :date_deb AND pda_date <= :date_fin AND acl_client = :acl_client AND act_pro_famille = :act_pro_famille AND act_pro_sous_famille = :act_pro_sous_famille";
        $req = $ConnFct->prepare($sql);
        $req->bindParam(":date_deb",$date_deb);
        $req->bindParam(":date_fin",$date_fin);
        $req->bindParam(":acl_client",$client);
        $req->bindParam(":act_pro_famille",$famille);
        $req->bindParam(":act_pro_sous_famille",$sous_famille);
        $req->bindParam(":act_pro_sous_sous_famille",$sous_sous_famille);
        $req->execute();
        $d_actions_interco = $req->fetchAll();

        foreach($d_actions_interco as $k=>$dai){
            $d_actions_interco[$k]['act_date_deb'] = convert_date_txt($dai['act_date_deb']);
        }

    }
    /////////////////// FIN INTERCO ///////////////////////

    if(!empty($d_actions_interco)){
        echo json_encode($d_actions_interco);
    }else{
        echo "Pas d'action associée";
    }


}
?>
