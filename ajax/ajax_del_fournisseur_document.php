<?php
	include("../includes/connexion.php");
	include("../modeles/mod_parametre.php");
	// on va chercher le fichier
	$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fichier = :ffi_fichier AND ffi_fournisseur = :ffi_fournisseur AND ffi_societe = :ffi_societe");
	$req->bindParam("ffi_fichier",$_POST['document']);
	$req->bindParam("ffi_fournisseur",$_POST['fournisseur']);
	$req->bindParam("ffi_societe",$_POST['societe']);
	$req->execute();
	// on va chercher le document
	$fichier = $req->fetch();
	$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_id = :fdo_id");
	$req->bindParam("fdo_id",$fichier['ffi_fichier']);
	$req->execute();
	$document = $req->fetch();

	unlink("../documents/fournisseurs/" . $_POST['fournisseur'] . "/" . $_POST['societe'] . "_" . $fichier['ffi_fichier'] . "_" . $fichier['ffi_nom'] . "." . $fichier['ffi_ext']);

	$req = $Conn->prepare("DELETE FROM fournisseurs_fichiers WHERE ffi_fichier = :ffi_fichier AND ffi_fournisseur = :ffi_fournisseur AND ffi_societe = :ffi_societe");
	$req->bindParam("ffi_fichier",$_POST['document']);
	$req->bindParam("ffi_fournisseur",$_POST['fournisseur']);
	$req->bindParam("ffi_societe",$_POST['societe']);
	$req->execute();
	// 
	///////////////////// MISE A JOUR ETAT DOCUMENTS FOURNISSEUR ///////////////////
	// mise a jour de l'état
	$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id=" . $_POST['fournisseur']);
	$req->execute();
	$fournisseur = $req->fetch();

	$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_type = 1"); // documents du fournisseur
	$req->execute();
	$documents = $req->fetchAll();
	
	$txt = "";
	// verif etat du document 1 par 1
	foreach($documents as $d){
		$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur=" . $_POST['fournisseur'] . " AND ffi_fichier = " . $d['fdo_id']);
		$req->execute();
		$fichier = $req->fetch();

		if(($d['fdo_obligatoire'] == 1 && empty($fichier)) OR (!empty($fichier) && $d['fdo_periodicite_mois'] != 0 && date('Y-m-d') > date('Y-m-d', strtotime("+" . $d['fdo_periodicite_mois'] . " months", strtotime($fichier['ffi_date']))))){
			$statut = 1;
		

		}elseif(($d['fdo_optionnel'] == 1 && empty($fichier))){
			$statut = 2; // warning
		}else{
			$statut = 3; // succes
		}
		if($statut == 1 OR $statut == 2){
			if(empty($txt)){
				$txt = $d['fdo_libelle'] . "<br>";
			}else{
				$txt .= $d['fdo_libelle'] . "<br>";
			}
		}else{
			$txt = "";
		}
		

	}

	if(!empty($statut)){
		 $req = $Conn->prepare("SELECT * FROM fournisseurs_validations WHERE fva_fournisseur = :fva_fournisseur AND fva_societe = :fva_societe");
        $req->bindParam(':fva_fournisseur', $_POST['fournisseur']);
        $req->bindParam(':fva_societe', $_SESSION['acces']['acc_societe']);
        $req->execute();
        $fou_valide = $req->fetch();

        if(empty($fou_valide)){
             $req = $Conn->prepare("INSERT INTO fournisseurs_validations (fva_admin_etat, fva_admin_txt, fva_fournisseur, fva_societe) VALUES (:fva_admin_etat, :fva_admin_txt, :fva_fournisseur, :fva_societe)");
            $req->bindParam(':fva_fournisseur', $_POST['fournisseur']);
            $req->bindParam(':fva_societe', $_SESSION['acces']['acc_societe']);
            $req->bindParam(':fva_admin_etat', $statut);
            $req->bindParam(':fva_admin_txt', $txt);
            $req->execute();
        }else{
             $req = $Conn->prepare("UPDATE fournisseurs_validations SET fva_admin_etat = :fva_admin_etat, fva_admin_txt = :fva_admin_txt  WHERE fva_fournisseur = :fva_fournisseur AND fva_societe = :fva_societe");
            $req->bindParam(':fva_fournisseur', $_POST['fournisseur']);
            $req->bindParam(':fva_societe', $_SESSION['acces']['acc_societe']);
            $req->bindParam(':fva_admin_etat', $statut);
            $req->bindParam(':fva_admin_txt', $txt);
            $req->execute();
        }
	}
	///////////////////// FIN MISE A JOUR ETAT DOCUMENTS FOURNISSEUR ///////////////////

	
	if($document['fdo_obligatoire'] == 1){
		echo 1;
	}else{
		echo 2;
	}

?>