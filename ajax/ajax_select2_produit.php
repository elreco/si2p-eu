<?php
 // PERMET DE PRE-SELECTIONNER DES PRODUITS D'APRES UNE RECHERCHE UTILSATEUR
 // VIA LE PLUGIN SELECT2
 
 session_start();
include("../includes/connexion.php");

// parametre

$erreur=0;

$param="";
if(isset($_GET['q'])){
	if($_GET['q']){
		$param="%" . $_GET['q'] . "%";
	}
}
$categorie=0;
if(isset($_GET['categorie'])){
	if($_GET['categorie']){
		$categorie=intval($_GET['categorie']);
	}
}

$famille=0;
if(isset($_GET['famille'])){
	if($_GET['famille']){
		$famille=intval($_GET['famille']);
	}
}

$sous_famille=0;
if(isset($_GET['sous_famille'])){
	if($_GET['sous_famille']){
		$sous_famille=intval($_GET['sous_famille']);
	}
}

$sous_sous_famille=0;
if(isset($_GET['sous_sous_famille'])){
	if($_GET['sous_sous_famille']){
		$sous_sous_famille=intval($_GET['sous_sous_famille']);
	}
}

// demande un type particulier intra / inter
$type=0;
if(isset($_GET['type'])){
	if($_GET['type']){
		$type=intval($_GET['type']);
	}
}

if($type==0){	
	if(isset($_GET['intra'])){
		if($_GET['intra']=="true"){
			$type=1;
		}
	}
	if(isset($_GET['inter'])){
		if($_GET['inter']=="true"){
			$type=2;
		}
	}
}

// 2 methode pour récuperer client_id
// Note : 2 variables pour l'appel auto via select2-produit 
// pour la fonction get_produits() -> on utilise data_client
$client=0;
if(isset($_GET['data_client'])){
	if($_GET['data_client']){
		$client=intval($_GET['data_client']);
	}
}
if(!empty($client)){
	if(isset($_GET['select2_client'])){
		if($_GET['select2_client']){
			$client=intval($_GET['select2_client']);
		}
	}	
}

// $base_client = 2 U ne demande que les produits qui ne sont pas déjà dans la base
$base_client=0;
if(isset($_GET['base_client'])){
	if(!empty($_GET['base_client'])){
		$base_client=intval($_GET['base_client']);
	}
}

$produit=0;
if(isset($_GET['produit'])){
	if(!empty($_GET['produit'])){
		$produit=intval($_GET['produit']);
	}
}
// produit => mode édition produit avant modif

if($client>0){
	
	// Si client >0 on n'affiche que les produits accéssibles au clients
	
	$maison_mere=$client;
	
	$sql="SELECT cli_categorie,cli_groupe,cli_filiale_de,cca_gc FROM Clients,Clients_Categories WHERE cli_categorie=cca_id AND cli_id=" . $client . ";";
	$req=$Conn->query($sql);
	$d_client=$req->fetch();
	if(empty($d_client)){
		echo("Erreur paramètre!");
		die();
	}else{
		if($d_client["cli_groupe"] AND $d_client["cli_filiale_de"]>0){
			$maison_mere=$d_client["cli_filiale_de"];
		}
	}
	$sql ="SELECT pro_id,pro_code_produit,pro_libelle,cpr_libelle,cpr_produit
	FROM Produits LEFT JOIN Clients_Produits ON (Produits.pro_id=Clients_Produits.cpr_produit AND cpr_client=:maison_mere) 
	LEFT JOIN Produits_Categories ON (Produits.pro_categorie=Produits_Categories.pca_id) WHERE NOT pro_archive AND pro_categorie=1 
	AND pro_famille=1 AND pro_sous_famille=4 AND (pro_client=0 OR pro_client=40231) AND pro_intra AND (ISNULL(cpr_produit) OR cpr_intra) 
	AND cpr_valide=1 AND NOT pca_compta ORDER BY pro_code_produit";

	$sql="SELECT pro_id,pro_code_produit,pro_libelle,cpr_libelle,cpr_produit
	FROM Produits LEFT JOIN Clients_Produits ON (Produits.pro_id=Clients_Produits.cpr_produit AND cpr_client=:maison_mere)
	LEFT JOIN Produits_Categories ON (Produits.pro_categorie=Produits_Categories.pca_id)";
	$sql.=" WHERE NOT pro_archive";
	if($categorie>0){
		$sql.=" AND pro_categorie=" . $categorie;
	}
	if($famille>0){
		$sql.=" AND pro_famille=" . $famille;
	}
	if($sous_famille>0){
		$sql.=" AND pro_sous_famille=" . $sous_famille;
	}
	if($sous_sous_famille>0){
		$sql.=" AND pro_sous_sous_famille=" . $sous_sous_famille;
	}
	if($maison_mere>0){
		$sql.=" AND (pro_client=0 OR pro_client=" . $maison_mere . ")";
	}
	if(!empty($param)){
		$sql.=" AND (pro_code_produit LIKE :param OR pro_libelle LIKE :param OR cpr_libelle LIKE :param)";
	}
	if($type==1){
		$sql.=" AND pro_intra AND (ISNULL(cpr_produit) OR cpr_intra)";		
	}elseif($type==2){
		$sql.=" AND pro_inter AND (ISNULL(cpr_produit) OR cpr_inter)";
	}
	if($base_client==2){
		$sql.=" AND (ISNULL(cpr_produit)";
		if($produit>0){
			$sql.=" OR cpr_produit=". $produit;
		}
		$sql.=")";
	}

	// SI GC EN U n'a pas le DRT GC on utilise que les tarifs valide
	if($d_client["cli_categorie"]==2 AND !$_SESSION['acces']["acc_droits"][8]){
		$sql.=" AND cpr_valide=1";
	}
	if(!$_SESSION["acces"]["acc_droits"][9]){
		$sql.=" AND NOT pca_compta";	
	}
	$sql.=" ORDER BY pro_code_produit";
	//echo($sql);
	$req = $Conn->prepare($sql);
	if(!empty($param)){
		$req->bindParam(":param",$param);
	}
	$req->bindParam(":maison_mere",$maison_mere);
	$req->execute();
	$produits = $req->fetchAll();
	if(!empty($produits)){
		foreach($produits as $p){
			
			if(!empty($p['cpr_libelle'])){
				$retour[] = array(
					"id"=>$p['pro_id'],
					"text" => $p['pro_code_produit'] . " - " . $p['cpr_libelle']
				);
			}else{
				$retour[] = array(
					"id"=>$p['pro_id'], 
					"text"=>$p['pro_code_produit'] . " - " . $p['pro_libelle']
				);
			}
		}
	}
		
}else{
	
	$sql="SELECT pro_id,pro_code_produit,pro_libelle FROM Produits WHERE NOT pro_archive";
	if($categorie>0){
		$sql.=" AND pro_categorie=" . $categorie;
	}
	if($famille>0){
		$sql.=" AND pro_famille=" . $famille;
	}
	if($sous_famille>0){
		$sql.=" AND pro_sous_famille=" . $sous_famille;
	}
	if($sous_sous_famille>0){
		$sql.=" AND pro_sous_sous_famille=" . $sous_sous_famille;
	}
	if(!empty($param)){
		$sql.=" AND (pro_code_produit LIKE :param OR pro_libelle LIKE :param)";
	}
	if($type==1){
		$sql.=" AND pro_intra";				
	}elseif($type==2){
		$sql.=" AND pro_inter";			
	}
	$sql.=" ORDER BY pro_code_produit";
	$req = $Conn->prepare($sql);
	if(!empty($param)){
		$req->bindParam(":param",$param);
	}
	$req->execute();
	$produits = $req->fetchAll();
	if(!empty($produits)){
		foreach($produits as $p){
				$retour[] = array(
					"id"=>$p['pro_id'],"text"=>$p['pro_code_produit']);	
		}
	}
	
}

if(empty($erreur)){
	if(!isset($retour)){
		$retour=array();
	}
	echo json_encode($retour);	
}
?>
