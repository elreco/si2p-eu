<?php 

	include("../connexion.php");
	
	// SUPPRESION DE L'INSCRIPTION

	$req = $Conn->prepare("DELETE FROM sessions_stagiaires WHERE sst_stagiaire = :sst_stagiaire AND sst_session = :sst_session");
	$req->bindParam("sst_stagiaire",$_POST['sta_id']);
	$req->bindParam("sst_session",$_POST['session']);
	$req->execute();
	
	// SUPP ATTESTAION DU STAGIAIRE
	
	if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/documents/attestations/' . $_POST['session'] . '/' . $_POST['sta_id'] . '.pdf')){
		unlink($_SERVER['DOCUMENT_ROOT'] . '/documents/attestations/' . $_POST['session'] . '/' . $_POST['sta_id'] . ".pdf");	
	}
	
	// SUPP DE L'ATTESTATION GROUPE
	
	if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/documents/attestations/' . $_POST['session'] . '/conso_' . $_POST['session'] . '.pdf')){
		unlink($_SERVER['DOCUMENT_ROOT'] . '/documents/attestations/' . $_POST['session'] . '/conso_' . $_POST['session'] . ".pdf");	
	}
	
	// DONNEE POUR REGENERE L'ATTESTATION GROUPE
	$req = $Conn->prepare("SELECT * FROM sessions WHERE ses_id = " . $_POST['session'] . ";");
	$req->execute();
	$attestation_session = $req->fetch();
			
	$req = $Conn->query("SELECT sta_id, sta_nom, sta_prenom, sta_titre 
	FROM sessions_stagiaires LEFT JOIN CNRS_Stagiaires ON (sessions_stagiaires.sst_stagiaire = CNRS_Stagiaires.sta_id)
	WHERE sst_session = " . $_POST['session'] . " ORDER BY sta_nom,sta_prenom;");
	$req->execute();
	$d_stagiaires = $req->fetchAll();
	if(!empty($d_stagiaires)){
		
		setlocale(LC_TIME, "fr");
		date_default_timezone_set('Europe/Paris');

		// attestation conso
		if(count($d_stagiaires)>1){
			
			$attestation_stagiaires=$d_stagiaires;
			include("../modeles/mod_parametre.php");
			include("../modeles/mod_attestation_pdf.php");
		}
	
	}

	echo "OK";
	?>