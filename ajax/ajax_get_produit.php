<?php
 
session_start();
include("../includes/connexion.php");
include("../modeles/mod_get_client_produits.php");

/* RETOURNE LES DONNEES ASSOCIEE A UN PRODUIT ( TABLE PRODUITS ET Clients_Produits)
 
// parametre POST

// produit : ID produit 
// client : ID du client pour surclasser le TG 

// retour => tableau JSON

// Appel :
// client_produit.php
// auteur : FG 06/09/2016
*/
$erreur=0;
if(isset($_POST)){
	 
	$produit_id=0;
	if(!empty($_POST["produit"])){
		$produit_id=$_POST["produit"]; 
	}else{
		$erreur=1;
	}	
	
	$client=0;
	if(!empty($_POST['client'])){
		$client=intval($_POST['client']);
	}
	
	/*else{
		$erreur=1;
	}*/
	
	if($erreur==0){
		
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];  
		}
		
		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];  
		}
		
		
		$societ=0;
		if(!empty($_POST['societ'])){
			$societ=intval($_POST['societ']);
		}
		if($societ>0 AND $acc_societe!=$societ){
			$acc_societe=$societ;
			$acc_agence=0;
		}
		

		// FORMULAIRE
		
		//aff_ht => 0 ou "" retourne les prix enregistrés 
		//aff_ht => 1 retourne les prix calculé (50% interco ou 88% CN)
		$aff_ht=0;
		if(!empty($_POST['aff_ht'])){
			$aff_ht=intval($_POST['aff_ht']);
		}
		
		//base_client => 0 tous les produits 
		//base_client => 1 uniquement les produits dans la base client
		//base_client => 2 uniquement les produits absent la base client
		$base_client=0;
		if(!empty($_POST['base_client'])){
			$base_client=intval($_POST['base_client']);
		}
		
		//acces => 0 tous les produits 
		//acces => 1 uniquement que U peut utiliser
		$acces=0;
		if(!empty($_POST['acces'])){
			$acces=intval($_POST['acces']);
		}

		$pro_type=0;
		$pro_cat=0;
		$pro_fam=0;
		$pro_s_fam=0;
		$pro_s_s_fam=0;
		$pro_archive=0;

		$produit=get_client_produits($client,$produit_id,$aff_ht,$base_client,$acces,$pro_type,$pro_cat,$pro_fam,$pro_s_fam,$pro_s_s_fam,$pro_archive);
		if(is_array($produit)){
			echo json_encode($produit);	
		}else{
			echo("ERREUR 003");
		}
	}else{
		echo("ERREUR 002");
	}
}else{
	echo("ERREUR 001");
}
?>
