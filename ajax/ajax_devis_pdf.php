<?php

include "../includes/controle_acces.inc.php";

include '../includes/connexion_soc.php';
include '../includes/connexion.php';

include('../modeles/mod_devis_data.php');
include('../modeles/mod_devis_pdf.php');

include('../modeles/mod_get_juridique.php');
include('../modeles/mod_parametre.php');

/*require_once("../vendor/autoload.php");
use Spipu\Html2Pdf\Html2Pdf;*/


// PERMET DE GENERER UN DEVIS EN PDF

$erreur_txt="";

if(!empty($_POST)){
	
	$devis_id=0;
	if (isset($_POST['devis'])){
		$devis_id=intval($_POST['devis']);	
	}
	if(empty($devis_id)){
		$erreur_txt="Formulaire incomplet!";
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}

/***********************************
			CONTROLE
***********************************/

if(empty($erreur_txt)){

	// la personne logue
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}

	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}
	
	$data=devis_data($devis_id);
	if(!empty($data)){
		$pdf=model_devis_pdf($data);
		
		if(!$pdf){						
			$erreur_txt="Erreur lors de la génération du PDF!";
		}
	}else{
		$erreur_txt="impossible d'afficher la page";
	}
}

if(empty($erreur_txt)){
	echo json_encode("OK");
}
?>
