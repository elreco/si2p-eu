<?php
include "../includes/controle_acces.inc.php";
include '../includes/connexion.php';
include '../includes/connexion_soc.php';


include '../modeles/mod_get_client_produits.php';

 // SuppRESSION D'UNE LIGNE DE FACTURE

$erreur_txt="";
if(isset($_POST)){
	 
	$facture=0;
	if(!empty($_POST["facture"])){
		$facture=intval($_POST["facture"]); 
	}
	$ligne=0;
	if(!empty($_POST["ligne"])){
		$ligne=intval($_POST["ligne"]); 
	}
	if(empty($facture) OR empty($ligne) ){
		$erreur_txt="Formulaire incomplet!";
		echo($erreur_txt);
		die();	
	}
}else{
	$erreur_txt="Formulaire incomplet!";
	echo($erreur_txt);
	die();
}

$sql="SELECT * FROM Factures_Lignes WHERE fli_facture=:fli_facture AND fli_id=:fli_id;";
$req=$ConnSoc->prepare($sql);
$req->bindParam("fli_facture",$facture);
$req->bindParam("fli_id",$ligne);
try{
	$req->execute();
}Catch(Exception $e){
	$erreur_txt="DELETE " . $e->getMessage();
	echo($erreur_txt);
	die();
}
$retour=$req->fetch(PDO::FETCH_ASSOC);
if(!empty($retour)){
	echo(json_encode($retour));
	
}else{
	$erreur_txt="Impossible de charger la ligne de facture!";
	echo($erreur_txt);
}
die();
?>