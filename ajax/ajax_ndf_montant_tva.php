<?php
include("../includes/connexion.php");
include_once('../modeles/mod_parametre.php');
if(isset($_POST)){
    $req = $Conn->prepare("SELECT nli_ttc FROM ndf_lignes WHERE nli_id = :nli_id");
    $req->bindParam("nli_id", $_POST['nli_id']);
    $req->execute();
    $nli = $req->fetch();

	$req = $Conn->prepare("UPDATE ndf_lignes SET nli_montant_tva = :nli_montant_tva WHERE nli_id = :nli_id");
    $req->bindParam("nli_montant_tva",$_POST['val']);
    $req->bindParam("nli_id", $_POST['nli_id']);
    $req->execute();

    echo $nli['nli_ttc'] - $_POST['val'];
}
?>