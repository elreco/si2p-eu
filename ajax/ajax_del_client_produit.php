<?php
include("../includes/connexion.php");


// PERMET DE SUPPRIMER UN PRODUIT D'UNE BASE CLIENT

$erreur="";
if(isset($_POST)){
	
	if(!empty($_POST["client"])){
		$client=intval($_POST["client"]);
	}
	
	if(!empty($_POST["produit"])){
		$produit=intval($_POST["produit"]);
	}
	
	if(isset($client) AND isset($produit)){
		
		$cpr_client=$client;
		$req=$Conn->query("SELECT cli_filiale_de FROM Clients WHERE cli_id=" . $client . " AND cli_groupe=1 AND cli_filiale_de>0;");
		$d_client=$req->fetch();
		if(!empty($d_client)){
			$cpr_client=$d_client["cli_filiale_de"];
		}
		try {

			$req=$Conn->query("DELETE FROM Clients_Produits WHERE cpr_produit=" . $produit . " AND cpr_client=" . $cpr_client . ";");
		
		}
		catch( PDOException $Exception ) {
			$erreur=$Exception->getMessage();
		}
	}else{
		$erreur="ERREUR PARAMETRE";
	}
}else{
	$erreur="ERREUR PARAMETRE";
}
if($erreur==""){
	echo json_encode($produit);
}else{
	echo $erreur;
}

?>
