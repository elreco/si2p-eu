<?php
include("../includes/connexion.php");
include_once('../modeles/mod_parametre.php');
include_once('../modeles/mod_tva.php');
if(isset($_POST)){

 	$req = $Conn->prepare("SELECT * FROM commandes_lignes WHERE cli_commande = " . $_POST['commande']);
	$req->execute();
	$commandes_lignes = $req->fetchAll();
	$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = " . $_POST['commande']);
	$req->execute();
	$commande = $req->fetch();

	foreach($commandes_lignes as $c){ // pour toutes les lignes calculer la tva
		$date = $commande['com_date'];
		$req = $Conn->prepare("SELECT * FROM tva_periodes WHERE tpe_tva = " . $c['cli_tva'] . " AND tpe_date_deb <= '" . $date . "' AND (tpe_date_fin >= '"  . $date . "' OR tpe_date_fin IS NULL)");
		$req->execute();
		$tva = $req->fetch();

		$req = $Conn->prepare("UPDATE commandes_lignes SET cli_tva_taux = " . $tva['tpe_taux'] . " WHERE cli_commande = " . $c['cli_id']);
		$req->execute();

		 
		$valeur = (($c['cli_ht'])*$tva['tpe_taux'])/100;
		$valeur = $c['cli_ht'] + $valeur;

		if(!empty($total)){
			$total = $total + $valeur;
		}else{
			$total = $valeur;
		}
	} 


	$req = $Conn->prepare("UPDATE commandes SET com_ttc = " . $total . " WHERE com_id = " . $commande['com_id']);
	$req->execute();
	$total = number_format($total, 2, ',', ' ');
	echo($total);

}