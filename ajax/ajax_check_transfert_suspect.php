<?php

// verifie qu'un suspect rempli les condition pour être basculer en prospect

include("../includes/connexion.php");
include('../includes/connexion_soc.php');

include('../modeles/mod_check_client.php');
include('../modeles/mod_check_siret.php');

if(isset($_POST)){

	$suspect=0;
	if(!empty($_POST['suspect'])){
		$suspect=intval($_POST['suspect']);
	}
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	
	if($suspect>0){
		
		$erreur="";
		
		$sql="SELECT * FROM Suspects WHERE sus_id=" . $suspect . ";";
		$req=$ConnSoc->query($sql);
		$d_suspect=$req->fetch();
		
		if(!empty($d_suspect)){
			
			if(empty($d_suspect["sus_categorie"])){
				$erreur.="<li>la catégorie n'est pas renseignée</li>";
			}elseif(empty($d_suspect["sus_reg_formule"])){
				$erreur.="<li>le délai de règlement n'est pas renseigné</li>";
			}else{
				
				if($d_suspect["sus_categorie"]==3){
					if(empty($d_suspect["sus_stagiaire_naiss"])){
						$erreur.="<li>La date de naissance est obligatoire pour les particuliers</li>";
					}
				}else{
					// le siren est obligatoire
					if(empty($d_suspect["sus_siren"])){
						$erreur.="<li>le siren n'est pas renseigné</li>";
					}
				}
			}
			
			// controle sur le client
			
			$verif=check_client($acc_societe,$d_suspect["sus_code"],$d_suspect["sus_nom"],"");
			if(is_array($verif)){
				foreach($verif["resultat_text"] as $message){
					$erreur.="<li>" . $message . "</li>";
				}
			}
			
			// CONTROLE SUR LES ADRESSES
			
			$adr_int=false;
			$adr_fac=false;		
			$sql="SELECT sad_type,sad_siret,sad_defaut,sad_geo FROM Suspects_Adresses WHERE sad_ref_id=" . $suspect . ";";
			$req=$ConnSoc->query($sql);
			$d_adresses=$req->fetchAll();
			if(!empty($d_adresses)){
				
				foreach($d_adresses as $adresse){
					
					if($adresse["sad_type"]==1){
						// intervention
						if($adresse["sad_defaut"]){
							$adr_int=true;	
						}
					}elseif($adresse["sad_type"]==2){
						
						// facturation
						if($adresse["sad_defaut"]){
							$adr_fac=true;	
						}
						if($d_suspect["sus_categorie"]!=3){
							if(empty($adresse["sad_siret"])){
								$erreur.="<li>certaines adresses facturations n'ont pas de siret</li>";	
							}elseif(!empty($d_suspect["sus_siren"])){
								$result=check_siret($d_suspect["sus_siren"],$adresse["sad_siret"],0,0);
								if(!empty($result)){
									$erreur.="<li>Le siret " . $d_suspect["sus_siren"] . " " .$adresse["sad_siret"] . " est déjà enregistré</li>";		
								}
							}
						}
					}
					
					if($d_suspect["sus_categorie"]!=3){
						if($adresse["sad_geo"]!=1 AND empty($d_suspect["sus_ident_tva"])){
							$erreur.="<li>le numéro de TVA est obligatoire pour les adresses en dehors de France</li>";
						}
					}
				}
			}
			if(!$adr_int AND $d_suspect["sus_categorie"]!=3){
				$erreur.="<li>il n'y a pas d'adresse d'intervention par défaut</li>";
			}
			if(!$adr_fac){
				$erreur.="<li>il n'y a pas d'adresse de facturation par défaut</li>";
			}
			
			// CONTROLE SUR LES CONTACTS
			
			$con_defaut=false;
			$con_compta=false;
			$sql="SELECT sco_compta FROM Suspects_Contacts WHERE sco_ref_id=" . $suspect;
			$req=$ConnSoc->query($sql);
			$d_contacts=$req->fetchAll();
			if(!empty($d_contacts)){
				foreach($d_contacts as $contact){
					if($contact["sco_compta"]){
						$con_compta=true;
					}				
				}
			}else{
				$erreur.="<li>il n'y a pas de contact par défaut</li>";
			}
			
			if(!$con_compta){
				$erreur.="<li>Il n'y a pas de contact compta</li>";
			}
			
			if(!empty($erreur)){
				$erreur="<p>Impossible de passer ce suspect en prospect pour les raisons suivantes :</p><ul>" . $erreur . "</ul>";
				echo($erreur);
			}else{// A CE MOMENT LA IL FAUT REDIRIGER VERS LA FICHE CLIENT OU LA FICHE D'APRES ?
				echo json_encode("OK");
			}
			
		}
	
	}
}
?>