<?php
 // PERMET DE PRE-SELECTIONNER DES UTILISATEURS D'APRES UNE SAISIE UTILISATEUR


include("../includes/connexion.php");

session_start();


/* UTILISATION
- commande_tri.php
 */

$sql="SELECT udr_droit FROM utilisateurs_droits WHERE udr_utilisateur=" . $_POST['field'];
$req = $Conn->query($sql);
$droits = $req->fetchAll();

$sql="SELECT * FROM utilisateurs WHERE uti_id=" . $_POST['field'];
$req = $Conn->query($sql);
$utilisateur = $req->fetch();

$sql="SELECT * FROM utilisateurs WHERE uti_id=" . $_POST['utilisateur'];
$req = $Conn->query($sql);
$utilisateur_concerne = $req->fetch();

// LE CA
$sql = "SELECT uti_id FROM utilisateurs
LEFT JOIN utilisateurs_societes ON (utilisateurs_societes.uso_utilisateur = utilisateurs.uti_id)
 WHERE uso_agence = " . $utilisateur_concerne['uti_agence'] . " AND uso_societe = " . $utilisateur_concerne['uti_societe'] . " AND uti_profil = 15 AND uti_archive = 0";
$req = $Conn->query($sql);
$chef_agence = $req->fetch();


if(!empty($droits)){
    foreach($droits as $d){
        $list_droits[] = $d['udr_droit'];
    }
    $query_droits = implode(',', $list_droits);
}else{
    $query_droits = 0;
}

$sql="SELECT * FROM conges_categories WHERE cca_droit = 0 OR cca_droit IN(" . $query_droits . ")";
$req = $Conn->query($sql);
$categories = $req->fetchAll();
if(!empty($categories)){
	foreach($categories as $c){
		$continuer = false;

		if ($_SESSION['acces']['acc_ref_id'] == $_POST['utilisateur'] && !empty($c['cca_user'])) {
			$continuer = true;
		}

		if ($_SESSION['acces']['acc_ref_id'] == $utilisateur_concerne['uti_responsable'] && !empty($c['cca_resp'])) {
			$continuer = true;
		}

		if ($_SESSION['acces']['acc_ref_id'] == $chef_agence['uti_id'] && !empty($c['cca_ra'])) {
			$continuer = true;
		}

		if ($continuer) {
			$retour[] = array(
				"id"=>$c['cca_id'],
				"nom"=>$c['cca_libelle']
			);
		}

	}
}

// ENVOIE DU RESULTAT
if(!empty($retour)){
	echo json_encode($retour);
} else {
	echo json_encode([]);
}
?>
