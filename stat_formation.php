<?php

	// ECRAN D'ACCUEIL DES STATISTIQUES DE L'ONGLET FORMATION

	$menu_actif = "2-7";

	include "includes/controle_acces.inc.php";
	include "modeles/mod_parametre.php";
	include "includes/connexion.php";
	include "includes/connexion_soc.php";

	// CONTROLE D'ACCES
	// cf item menu
	// SERVICE COM (RE), DAF, DG, RA , Assist R (pour "stat" rapport hebdo)


	if($_SESSION['acces']['acc_service'][1]!=1 AND $_SESSION['acces']['acc_profil'] != 11 AND $_SESSION['acces']['acc_profil'] != 14 AND $_SESSION['acces']['acc_profil'] != 15 AND $_SESSION['acces']['acc_profil'] != 4 AND $_SESSION['acces']['acc_profil'] != 1 AND $_SESSION['acces']['acc_profil'] != 10 ){ 
		// service com
		// profil DAF,DG,RA,Assist,formateur,RE
		echo("Accès refusé!");
		die();
	}

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	
	$acces_stat=array(
		"stat_1" => false,
		"stat_2" => false,
		"stat_3" => true,	// indicateurs avis de stage
	);

	if($_SESSION['acces']['acc_service'][1]==1 OR in_array($_SESSION['acces']['acc_profil'], array('4', '15', '10', '11', '14'))){
		$acces_stat["stat_1"]=true;
	}



	if($_SESSION['acces']['acc_service'][1]==1 OR $_SESSION['acces']['acc_profil'] == 11 OR $_SESSION['acces']['acc_profil'] == 14 OR $_SESSION['acces']['acc_profil'] == 15){ 
		$acces_stat["stat_2"]=true;
	}

	// if($_SESSION["acces"]["acc_ref"]!=1 OR $_SESSION['acces']['acc_service'][3]!=1){
	// 	$erreur="Accès refusé!";
	// 	echo($erreur);
	// 	die();
	// }

	// LES TESTEURS = INTERVENANTS

	$sql="SELECT int_id,int_label_1,int_label_2 FROM Intervenants";
	if(!empty($acc_agence)){
		$sql.=" WHERE (int_agence=" . $acc_agence . " OR int_agence=0)";
	}

	$sql.=" ORDER BY int_label_1,int_label_2";
	$req=$ConnSoc->query($sql);
	$d_intervenants=$req->fetchAll();


	// LES TYPES DE RAPPORTS HEBDO

	$sql="SELECT fut_id,fut_libelle FROM Formations_Utilisations ORDER BY fut_libelle;";
	$req=$Conn->query($sql);
	$d_form_utilisations=$req->fetchAll();

	// LES VEHICULES

	$sql="SELECT veh_id,veh_libelle FROM Vehicules WHERE (veh_societe=" . $acc_societe . ")";
	if(!empty($acc_agence)){
		$sql.=" AND (veh_agence=" . $acc_agence . " OR veh_agence=0)";
	}
	$sql.=" ORDER BY veh_libelle;";
	$req=$Conn->query($sql);
	$d_vehicules=$req->fetchAll();


?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<div id="main" >
<?php
			if($_SESSION["acces"]["acc_ref"]==1){
				include "includes/header_def.inc.php";
			} ?>
			<section id="content_wrapper" >
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn" >
					<div class="row " >

						<h1 class="text-center" >Statistiques</h1>

						<div class="row" >
					<?php	if($acces_stat["stat_1"]){ ?>
								<div class="col-md-3" >
									<div class="panel mt10 panel-primary" >
										<div class="panel-heading panel-head-sm" >
											<span class="panel-title">Rapports hebdomadaires</span>
										</div>
										<div class="panel-intro" >
											Liste les informations contenues dans les rapports hebdomadaires selon les critères ci-dessous.
										</div>
										<div class="panel-body" >
											<form method="post" action="stat_form_rap_hebdo.php" >
												<div class="admin-form" >
													<div class="row mt15" >
														<div class="col-md-12" >
															<label for="rp_intervenant" >Intervenant :</label>
															<select class="select2" id="rp_intervenant" name="intervenant" >
																<option value='0' >Intervenant ...</option>
														<?php	foreach($d_intervenants as $int){
																	echo("<option value='" . $int["int_id"] . "' >" . $int["int_label_1"] . " " . $int["int_label_2"] . "</option>");
																} ?>

															</select>
														</div>
													</div>
													<div class="row mt15" >
														<div class="col-md-12" >
															<label for="rp_type" >Type :</label>
															<select class="select2" id="rp_type" name="utilisation" >
																<option value='0' >Type ...</option>
														<?php	foreach($d_form_utilisations as $q){
																	echo("<option value='" . $q["fut_id"] . "' >" . $q["fut_libelle"] . "</option>");
																} ?>
															</select>
														</div>
													</div>
													<div class="row mt15" >
														<div class="col-md-12" >
															<label for="rp_vehicule" >Véhicule :</label>
															<select class="select2" id="rp_type" name="vehicule" >
																<option value='0' >Véhicule ...</option>
														<?php	foreach($d_vehicules as $q){
																	echo("<option value='" . $q["veh_id"] . "' >" . $q["veh_libelle"] . "</option>");
																} ?>
															</select>
														</div>
													</div>
													<div class="row mt15" >
														<div class="col-md-6" >
															<label for="rp_periode_deb" >Période du</label>
															<span  class="field prepend-icon">
																<input type="text" id="rp_periode_deb" name="periode_deb" class="gui-input datepicker" placeholder="Entre le" required />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
														<div class="col-md-6" >
															<label for="rp_periode_fin" >au</label>
															<span  class="field prepend-icon">
																<input type="text" id="rp_periode_fin" name="periode_fin" class="gui-input datepicker" placeholder="et le" required />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>

													<div class="row mt15" >
														<div class="col-md-12 text-center" >
															<button type="submit" class="btn btn-md btn-primary" >
																Afficher les résultats
															</button>
														</div>
													</div>
												</div>

											</form>
										</div>
									</div>
								</div>

					<?php	} ?>
																
					<?php	if($acces_stat["stat_2"]){ ?>
								<div class="col-md-3" >
									<div class="panel mt10 panel-primary" >
										<div class="panel-heading panel-head-sm" >
											<span class="panel-title">Planifications et annulations</span>
										</div>
										<div class="panel-intro" >
											Affiche le nombre de demi-journées plannifiées, reportées et annulées pour la période selectionnée.
										</div>
										<div class="panel-body" >

											<form method="post" action="stat_form_annulation.php" >
												<div>
													<input type="hidden" id="source_2" name="source" value="" />
												</div>
												<div class="admin-form" >													
													<div class="row mt15" >
														<div class="col-md-6" >
															<label for="periode_deb_2" >Période du</label>
															<span  class="field prepend-icon">
																<input type="text" id="periode_deb_2" name="periode_deb" class="gui-input datepicker" placeholder="Entre le" required />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
															<small class="text-danger texte_required" id="periode_deb_2_required">
																Merci de selectionner une période
															</small>

														</div>
														<div class="col-md-6" >
															<label for="periode_fin_2" >au</label>
															<span  class="field prepend-icon">
																<input type="text" id="periode_fin_2" name="periode_fin" class="gui-input datepicker" placeholder="et le" required />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
															<small class="text-danger texte_required" id="periode_fin_2_required">
																Merci de selectionner une période
															</small>
														</div>
													</div>
													<div class="row mt15" >
														<div class="col-md-6 text-center" >		
															<div class="radio-custom mb5">
																<input id="source_2_0" name="source" type="radio" value="0" checked >
																<label for="source_2_0">Données détaillées <?=$societe_brand?></label>
															</div>
														</div>
														<div class="col-md-6 text-center" >		
															<div class="radio-custom mb5">
																<input id="source_2_1" name="source" type="radio" value="1" >
																<label for="source_2_1">Données consolidées</label>
															</div>
														</div>
													</div>
													<div class="row mt15 text-center" >
														<div class="col-md-12" >
															<button type="submit" class="btn btn-md btn-primary sub_form_2" >
																Afficher les résultats
															</button>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
					<?php	}
							
							if($acces_stat["stat_3"]){ 
								// AVIS DE STAGE ?>
								<div class="col-md-3" >
									<div class="panel mt10 panel-primary" >
										<div class="panel-heading panel-head-sm" >
											<span class="panel-title">Indicateurs 'Avis de stage'</span>
										</div>
										<div class="panel-intro" >
											Affiche le taux de statisfaction sur une période donnée.
										</div>
										<div class="panel-body" >
											<form method="post" action="stat_form_avis.php" >
												<div class="admin-form" >
													<div class="row mt15" >
														<div class="col-md-12" >
															<label for="indicateur_3" >Indicateurs :</label>
															<select class="select2" id="indicateur_3" name="indicateur" >
																<option value='1' >Par sous-famille de produit</option>
																<option value='2' >Par produit</option>
														<?php	if($_SESSION['acces']['acc_profil'] != 1){
																	echo("<option value='3' >Par formateur</option>");
																} ?>
															</select>
														</div>
													</div>												
													<div class="row mt15" >
														<div class="col-md-6" >
															<label for="periode_deb_3" >Période du</label>
															<span  class="field prepend-icon">
																<input type="text" id="periode_deb_3" name="periode_deb" class="gui-input datepicker" placeholder="Entre le" required />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
														<div class="col-md-6" >
															<label for="periode_fin_3" >au</label>
															<span  class="field prepend-icon">
																<input type="text" id="periode_fin_3" name="periode_fin" class="gui-input datepicker" placeholder="et le" required />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>

													<div class="row mt15" >
														<div class="col-md-12 text-center" >
															<button type="submit" class="btn btn-md btn-primary" >
																Afficher les résultats
															</button>
														</div>
													</div>
												</div>

											</form>
										</div>
									</div>
								</div>

					<?php	} ?>
								
						</div>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 text-center"></div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){

				$(".reseau").click(function(){
					stat=$(this).data("stat");
					if(stat>0){
						if($(this).is(":checked")){
							$(".no-reseau-" + stat).hide();
						}else{
							$(".no-reseau-" + stat).show();
						}
					}
				});

			});
		</script>
	</body>
</html>
