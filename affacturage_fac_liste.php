<?php 

// AFFICHE LA LISTE DES FACTURES

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');


// DONNEE UTILE AU PROGRAMME

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}

if($_SESSION['acces']['acc_service'][2]!=1){
	header("location : deconnect.php");
	die();
}

$_SESSION['retour'] = "affacturage_fac_liste.php";
$_SESSION["retourFacture"] = "affacturage_fac_liste.php";

	if (isset($_POST['search'])){
		
		// le formulaire a été soumit
		// on actualise les criteres de recherche
		
		$fac_date_deb="";
		if(!empty($_POST["fac_date_deb"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["fac_date_deb"]);
			if(!is_bool($DT_periode)){
				$fac_date_deb=$DT_periode->format("Y-m-d");
			}
		}
		$fac_date_fin="";
		if(!empty($_POST["fac_date_fin"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["fac_date_fin"]);
			if(!is_bool($DT_periode)){
				$fac_date_fin=$DT_periode->format("Y-m-d");
			}
		}

	
		$fac_agence=0;
		if(!empty($_POST['fac_agence'])){
			$fac_agence=intval($_POST['fac_agence']);
		}
		$fac_aff_remise=0;
		if(!empty($_POST['fac_aff_remise'])){
			$fac_aff_remise=intval($_POST['fac_aff_remise']);
		}
		$fac_chrono=0;
		if(!empty($_POST['fac_chrono'])){
			$fac_chrono=intval($_POST['fac_chrono']);
		}
		$fac_cli_categorie=0;
		if(!empty($_POST['fac_cli_categorie'])){
			$fac_cli_categorie=intval($_POST['fac_cli_categorie']);
		}
		$fac_cli_s_categorie=0;
		if(!empty($_POST['fac_cli_s_categorie'])){
			$fac_cli_s_categorie=intval($_POST['fac_cli_s_categorie']);
		}
		
		$fac_client=0;
		if(!empty($_POST['fac_client'])){
			$fac_client=intval($_POST['fac_client']);
		}
		
		$fac_filiale=false;
		if(!empty($_POST['fac_filiale'])){
			$fac_filiale=true;
		}
		
		
		
		$fac_reg_deb="";
		if(!empty($_POST["fac_reg_deb"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["fac_reg_deb"]);
			if(!is_bool($DT_periode)){
				$fac_reg_deb=$DT_periode->format("Y-m-d");
			}
		}
		$fac_reg_fin="";
		if(!empty($_POST["fac_reg_fin"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["fac_reg_fin"]);
			if(!is_bool($DT_periode)){
				$fac_reg_fin=$DT_periode->format("Y-m-d");
			}
		}
		
		$fac_banque=null;
		if(!empty($_POST['fac_banque'])){
			$fac_banque=intval($_POST['fac_banque']);
		}
		
		$fac_etat_date="";
		$fac_etat_date_fr="";
		if(!empty($_POST["fac_etat_date"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["fac_etat_date"]);
			if(!is_bool($DT_periode)){
				$fac_etat_date=$DT_periode->format("Y-m-d");
				$fac_etat_date_fr=$_POST["fac_etat_date"];
			}
		}
		
		$option_recherche=null;
		if(!empty($_POST['option_recherche'])){
			$option_recherche=intval($_POST['option_recherche']);
		}
		
		// mémorisation des critere

		$_SESSION['fac_tri'] = array(
			"fac_agence" => $fac_agence,
			"fac_aff_remise" => $fac_aff_remise,
			"fac_chrono" => $fac_chrono,
			"fac_cli_categorie" => $fac_cli_categorie,		// valeur actuelle du client pas celle enregistre sur la fac
			"fac_cli_s_categorie" => $fac_cli_s_categorie,
			"fac_client"        => $fac_client,			
			"fac_filiale" => $fac_filiale,
			"fac_date_deb" => $fac_date_deb,			
			"fac_date_fin" => $fac_date_fin,
			"fac_reg_deb" => $fac_reg_deb,
			"fac_reg_fin" => $fac_reg_fin,
			"fac_banque" => $fac_banque,
			"fac_etat_date" => $fac_etat_date,
			"fac_etat_date_fr" => $fac_etat_date_fr,
			"option_recherche" => $option_recherche,
		);
	}
	
	// DONNEES A RECUPERER EN FONCTION DES CRITERES
	$liste_banques="";
	$sql="SELECT ban_id FROM Banques WHERE ban_factor=1;";
	$req=$Conn->query($sql);
	$src_banques=$req->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($src_banques)){
		$tab_banques=array_column ($src_banques,"ban_id");
		$liste_banques=implode($tab_banques,",");
	}


	
	

	// ON CONSTRUIT LA REQUETE

	$critere=array();
	
	$mil="";
	$mil_s_req="";
	if(!empty($_SESSION['fac_tri']['fac_chrono'])){
		$mil.=" AND fac_chrono =:fac_chrono"; 
		$critere["fac_chrono"]=$_SESSION['fac_tri']['fac_chrono'];
		
	}else{
		
		if(!empty($_SESSION['fac_tri']['fac_date_deb'])){
			$mil.=" AND fac_date>=:fac_date_deb"; 
			$critere["fac_date_deb"]=$_SESSION['fac_tri']['fac_date_deb'];
		};
		if(!empty($_SESSION['fac_tri']['fac_date_fin'])){
			$mil.=" AND fac_date<=:fac_date_fin"; 
			$critere["fac_date_fin"]=$_SESSION['fac_tri']['fac_date_fin'];
		};
		
		
		
		if(!empty($_SESSION['fac_tri']['fac_agence'])){
			$mil.=" AND fac_agence=:fac_agence"; 
			$critere["fac_agence"]=$_SESSION['fac_tri']['fac_agence'];
		};
		
		if(!empty($_SESSION['fac_tri']['fac_aff_remise'])){
			$mil.=" AND fac_aff_remise=:fac_aff_remise"; 
			$critere["fac_aff_remise"]=$_SESSION['fac_tri']['fac_aff_remise'];
		};
		
		// maj suite echange Marion. Si le client change de categorie GC -> REG. Le CA doit rester GC
		if(!empty($_SESSION['fac_tri']['fac_cli_categorie'])){
			$mil.=" AND fac_cli_categorie =:fac_cli_categorie";
			$critere["fac_cli_categorie"]=$_SESSION['fac_tri']['fac_cli_categorie'];
		};
		if(!empty($_SESSION['fac_tri']['fac_cli_s_categorie'])){
			$mil.=" AND fac_cli_sous_categorie =:fac_cli_s_categorie";
			$critere["fac_cli_s_categorie"]=$_SESSION['fac_tri']['fac_cli_s_categorie'];
		};
		if(!empty($_SESSION['fac_tri']['fac_client'])){
			if($_SESSION['fac_tri']['fac_filiale']){
				$mil.=" AND (cli_id = :fac_client OR cli_filiale_de=:fac_client)"; 
				$critere["fac_client"]=$_SESSION['fac_tri']['fac_client'];
			}else{
				$mil.=" AND fac_client = :fac_client"; 
				$critere["fac_client"]=$_SESSION['fac_tri']['fac_client'];
			};
		}
		
		// critères sur règlements
		// En l'absence de critère reglements, le scripts va afficher tous les règlements de la factures.
		// si utilisatation des critères ci-dessous, le script ne va afficher que les reglements correspondants aux critères
		// Attention : sans mettre a jour TTC donc on obtient un delta entre TTC et somme des règlements
		// Il faut reporter les critères $mil sur $mil_s_req pour conserver le bon rowspan sur les lignes factures.
		
		if(!empty($_SESSION['fac_tri']['fac_reg_deb'])){
			$mil.=" AND reg_date>=:fac_reg_deb"; 
			$mil_s_req.=" AND reg_date>=:fac_reg_deb"; 
			$critere["fac_reg_deb"]=$_SESSION['fac_tri']['fac_reg_deb'];
		};
		
		if(!empty($_SESSION['fac_tri']['fac_reg_fin'])){
			$mil.=" AND reg_date<=:fac_reg_fin"; 
			$mil_s_req.=" AND reg_date<=:fac_reg_fin"; 
			$critere["fac_reg_fin"]=$_SESSION['fac_tri']['fac_reg_fin'];
		};
		
		if(!empty($liste_banques)){
			if(!empty($_SESSION['fac_tri']['fac_banque'])){
				if($_SESSION['fac_tri']['fac_banque']==1){
					// FACTOR
					$mil.=" AND reg_banque IN (" . $liste_banques . ")"; 
					$mil_s_req.=" AND reg_banque IN (" . $liste_banques . ")"; 
				}else{
					// AUTRES
					$mil.=" AND NOT reg_banque IN (" . $liste_banques . ")"; 
					$mil_s_req.=" AND NOT reg_banque IN (" . $liste_banques . ")"; 
				}
			}
		}
		if(!empty($_SESSION['fac_tri']['option_recherche'])){	
			if($_SESSION['fac_tri']['option_recherche']==1){
				// Remboursement à réaliser
				$mil.=" AND fac_aff_remise>0 AND NOT reg_banque IN (" . $liste_banques . ") AND reg_rb=0";
				$mil_s_req.=" AND NOT reg_banque IN (" . $liste_banques . ") AND reg_rb=0"; 
			}elseif($_SESSION['fac_tri']['option_recherche']==2){
				// Remboursement à percevoir
				$mil.=" AND (ISNULL(fac_aff_remise) OR fac_aff_remise=0) AND reg_banque IN (" . $liste_banques . ") AND reg_rb=0";
				$mil_s_req.=" AND reg_banque IN (" . $liste_banques . ") AND reg_rb=0"; 
			}
		}

		if(!empty($_SESSION['fac_tri']['fac_etat_date'])){			
			$mil.=" AND (aff_date<=:fac_etat_date OR ISNULL(aff_date) ) AND reg_date<=:fac_etat_date";
			$mil_s_req.=" AND reg_date<=:fac_etat_date"; 
			$critere["fac_etat_date"]=$_SESSION['fac_tri']['fac_etat_date'];
		};
		
		
		
	}

	// fin critère form
	
	// critere lie au droit d'accès
	
	if($acc_agence>0){
		$mil.=" AND fac_agence=" . $acc_agence;
	}
	

	// champ a selectionne
	$sql="SELECT fac_id,fac_numero,fac_total_ttc,fac_regle,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date_fr,fac_aff_retro,DATE_FORMAT(fac_aff_retro_date,'%d/%m/%Y') AS fac_aff_retro_date_fr
	,fac_aff_remise
	,cli_code,cli_nom
	,DATE_FORMAT(aff_date,'%d/%m/%Y') AS aff_date_fr
	,reg_banque,DATE_FORMAT(reg_date,'%d/%m/%Y') AS reg_date_fr,reg_montant,reg_rb,reg_rb_date,reg_id,DATE_FORMAT(reg_rb_date,'%d/%m/%Y') AS reg_rb_date_fr
	,nb_reg,total_reg
	FROM Factures 
	INNER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)
	LEFT JOIN Affacturages ON (Factures.fac_aff_remise=Affacturages.aff_id)	
	LEFT JOIN Reglements ON (Factures.fac_id=Reglements.reg_facture)";
	
	
	// requete pour cont les reglements => rowspan
	$sql.=" LEFT JOIN (SELECT COUNT(reg_id) AS nb_reg,reg_facture,SUM(reg_montant) AS total_reg FROM Reglements";
	if($mil_s_req!=""){
		$sql.=" WHERE " . substr($mil_s_req, 5, strlen($mil_s_req)-5);
	}
	$sql.=" GROUP BY reg_facture) AS Factures_Reglements ON (Factures.fac_id=Factures_Reglements.reg_facture)";
	
	$sql.=" WHERE ( (NOT ISNULL(fac_aff_remise) AND NOT fac_aff_remise=0) OR (reg_banque IN (" . $liste_banques . ")) )" ;
	
	if($mil!=""){
		//$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		$sql.=$mil;
	}
	$sql.=" ORDER BY fac_date,fac_chrono,fac_numero,fac_id;";
	$req = $ConnSoc->prepare($sql);	

	if(!empty($critere)){
		foreach($critere as $c => $u){
			$req->bindValue($c,$u);
		}
	};
	$req->execute();
	$factures = $req->fetchAll();
		
	
	// DONNEES PARAMETRES
	
	// les banques

	$d_banques=array();
	$sql="SELECT ban_id,ban_code FROM Banques ORDER BY ban_code;";
	$req=$Conn->query($sql);
	$d_result_banque=$req->fetchAll();
	if(!empty($d_result_banque)){
		foreach($d_result_banque as $r){
			$d_banques[$r["ban_id"]]=$r["ban_code"];
		}
	}
	
	
	
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
		<form method="post" action="" id="form_liste" >
			<input type="hidden" name="chp_date" id="chp_date" value="" />
			<input type="hidden" id="callback" value="" />
				<div id="main">
	<?php			include "includes/header_def.inc.php"; ?>
					<section id="content_wrapper">
						<section id="content" class="animated fadeIn">
					
							<h1>Liste des factures</h1>

			<?php 			if(!empty($factures)){ 
								$total_ht=0;
								$total_ttc=0;
								$total_regle=0;
								$total_reste_du=0; 
								
								$total_cols=5; 
								
								if(!empty($_SESSION['fac_tri']['fac_etat_date'])){ ?>
									<p class="alert alert-warning" >
										<b>Attetion :</b> le reste dû correspond au dû client <b>en date du <?=$_SESSION['fac_tri']['fac_etat_date_fr']?></b>
									</p>
									
						<?php	} ?>
							
								<div class="admin-form theme-primary ">

									<div class="table-responsive">
										<table class="table" >
											<thead>
												<tr class="dark2" >											
													<th rowspan="2" >Client</th>									
													<th rowspan="2" >Num fac</th>
													<th rowspan="2" >Date fac</th>											
													<th class="text-right" rowspan="2" >T.T.C.</th>
													<th rowspan="2" >Remise factor date</th>		
													<th class="text-right" rowspan="2" >Reste du</th>
													<th class="text-center" colspan="3" >Règlement</th>	
													<th>Remboursement date</th>	
													<th class="text-center" >Rétrocession</th>												
												</tr>
												<tr>
													<th>Montant</th>
													<th>Date</th>
													<th>Banque</th>
													<th class="text-center" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="check-all" id="rb_all" value="" >
																<span class="checkbox"></span>
															</label>
														</div>
													</th>
													<th class="text-center" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="check-all" id="retro_all" value="" >
																<span class="checkbox"></span>
															</label>
														</div>
													</th>
													
												</tr>
											</thead>
											<tbody>
									<?php		$fac_id=null;
												$nb_fac=0;
												foreach($factures as $f){
													
													$total_regle+=$f['reg_montant'];
													
													if($f["fac_id"]!=$fac_id){
														
														$fac_id=$f["fac_id"];
														
														$nb_fac++;
														$style="";
														if(empty($f["fac_aff_remise"])){
															$style="class='info'";
														}elseif($nb_fac % 2==0){
															$style="style='background-color:#f5f5f5;'";
														}
														
														$fac_date="";
														if(!empty($f['fac_date'])){
															$dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
															$fac_date=$dt_fac_date->format("d/m/Y");
														}
														$fac_date_reg_prev="";
														if(!empty($f['fac_date_reg_prev'])){
															$dt_fac_date_reg_prev=date_create_from_format("Y-m-d",$f['fac_date_reg_prev']);
															$fac_date_reg_prev=$dt_fac_date_reg_prev->format("d/m/Y");
														}
														if(!empty($_SESSION['fac_tri']['fac_etat_date'])){
															$reste_du=$f['fac_total_ttc']-$f['total_reg'];
														}else{
															$reste_du=$f['fac_total_ttc']-$f['fac_regle'];
														}
														
														$total_ttc+=$f['fac_total_ttc'];
														$total_reste_du+=$reste_du; 
														
														
														
														$nb_reg=1;
														if(!empty($f['nb_reg'])){
															$nb_reg=$f['nb_reg'];
														}?>
														<tr <?=$style?> >
															<td rowspan="<?=$nb_reg?>" ><?= $f['cli_code'] ?></td>
															<td rowspan="<?=$nb_reg?>" ><?= $f['fac_numero'] ?></td>
															<td rowspan="<?=$nb_reg?>" ><?= $f['fac_date_fr'] ?></td>
															<td rowspan="<?=$nb_reg?>" class="text-right" >
														<?php	if($_SESSION["acces"]["acc_droits"][30]){ ?>
																	<a href="reglement.php?facture=<?=$f['fac_id']?>" >
																		<?= number_format($f['fac_total_ttc'], 2, ',', ' ')?>
																	</a>
														<?php	}else{ 
																	echo(number_format($f['fac_total_ttc'], 2, ',', ' '));
																} ?>
															</td>
															<td rowspan="<?=$nb_reg?>" ><?= $f['aff_date_fr'] ?></td>
															<td rowspan="<?=$nb_reg?>" class="text-right" ><?= number_format($reste_du, 2, ',', ' ')?></td>
															<td class="text-right" id="reg_montant_<?=$f["reg_id"]?>" data-reg_montant="<?=$f["reg_montant"]?>" ><?= number_format($f['reg_montant'], 2, ',', ' ')?></td>
															<td><?= $f['reg_date_fr'] ?></td>
															<td>
													<?php		if(!empty($d_banques[$f["reg_banque"]])){
																	echo($d_banques[$f["reg_banque"]]);
																};?>
															</td>
															<td class="text-center" id="reg_rb_<?=$f["reg_id"]?>" >
													<?php		if(!empty($f["reg_id"])){
																	if(empty($f["reg_rb"])){	
																		// pas rembousser ?>
																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" class="rb" data-reglement="<?=$f["reg_id"]?>" name="reg_rb[]" value="<?=$f["reg_id"]?>" >
																				<span class="checkbox"></span>
																			</label>
																		</div>
														<?php		}else{
																		echo($f["reg_rb_date_fr"]);
																	} 
																} ?>
															</td>
															<td rowspan="<?=$nb_reg?>" id="fac_<?=$f["fac_id"]?>" class="text-center" >
											<?php				if(empty($f["fac_aff_retro"])){	
																	// pas rembousser ?>
																	<div class="option-group field">
																		<label class="option option-dark">
																			<input type="checkbox" class="retro" name="fac_retro[]" value="<?=$f["fac_id"]?>" >
																			<span class="checkbox"></span>
																		</label>
																	</div>
													<?php		}else{
																	echo($f["fac_aff_retro_date_fr"]);
																} ?>
															</td>
															
														</tr>										
						<?php						}else{ ?>
														<tr <?=$style?> >
															<td class="text-right" id="reg_montant_<?=$f["reg_id"]?>" data-reg_montant="<?=$f["reg_montant"]?>" >
																<?= number_format($f['reg_montant'], 2, ',', ' ')?>
															</td>
															<td><?= $f['reg_date_fr'] ?></td>
															<td>
													<?php		if(!empty($d_banques[$f["reg_banque"]])){
																	echo($d_banques[$f["reg_banque"]]);
																};?>
															</td>
															<td class="text-center" id="reg_rb_<?=$f["reg_id"]?>" >
													<?php		if(!empty($f["reg_id"])){
																	if(empty($f["reg_rb"])){	
																		// pas rembousser ?>
																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" class="rb" data-reglement="<?=$f["reg_id"]?>" name="reg_rb[]" value="<?=$f["reg_id"]?>" >
																				<span class="checkbox"></span>
																			</label>
																		</div>
														<?php		}else{
																		echo($f["reg_rb_date_fr"]);
																	}
																} ?>
															</td>
															
														</tr>
						<?php						}
												} ?>
											</tbody>
											<tfoot>
												<tr>
													<th colspan="3" class="text-right" >Total :</th>
													<td class="text-right" ><?=number_format($total_ttc, 2, ',', ' ');?></td>			
													<td>&nbsp;</td>											
													<td class="text-right" ><?=number_format($total_reste_du, 2, ',', ' ');?></td>
													<td class="text-right" ><?=number_format($total_regle, 2, ',', ' ');?></td>	
													<td colspan="4" >&nbsp;</td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							
			<?php 			}else{ ?>
								<div class="col-md-12 text-center" style="padding:0;" >
									<div class="alert alert-warning" style="border-radius:0px;">
										Aucune facture correspondant à votre recherche.
									</div>
								</div>
			<?php			} ?>
			
						</section>
					</section>
				</div>
				<footer id="content-footer" class="affix">
					<div class="row">
						<div class="col-xs-3 footer-left">
							<a href="affacturage_accueil.php" class="btn btn-default btn-sm" role="button">
								<span class="fa fa-search"></span>
								<span class="hidden-xs">Nouvelle recherche</span>
							</a>
							<button type="button" class="btn btn-system btn-sm" data-toggle="modal" data-target="#modal_aide" >
								<i class="fa fa-info" ></i>
							</button>
						</div>
						<div class="col-xs-6 footer-middle text-center" >
							
							<button type="button" class="btn btn-sm btn-success" id="add_rb" >
								<span class="fa fa-plus"></span>
								<span class="hidden-xs">Rembourssement</span>
							</button>
							<button type="button" class="btn btn-sm btn-success" id="add_retro" >
								<span class="fa fa-plus"></span>
								<span class="hidden-xs">Rétrocession</span>
							</button>
				
						</div>
						<div class="col-xs-3 footer-right"></div>
					</div>
				</footer>
			</form>
			
			<div id="modal_date" class="modal fade" role="dialog" >
				<div class="modal-dialog">			
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
							<h4 class="modal-title" id="modal_date_titre" >
								<i class="fa fa-info" ></i> - <span></span>
							</h4>
						</div>
						<div class="modal-body" >
							<div class="admin-form theme-primary ">		
								<div class="row mb15" id="mdl_bloc_montant" >
									<div class="col-md-12" ></div>
								</div>
								<div class="row" >
									<div class="col-md-12" >
										<label for="mdl_chp_date" >Date :</label>
										<span  class="field prepend-icon">
											<input type="text" id="mdl_chp_date" class="gui-input datepicker" placeholder="Date" required="required" />
											<span class="field-icon"><i
												class="fa fa-calendar-o"></i>
											</span>
										</span>
										<small class="text-danger texte_required" id="chp_date_required">
											Merci de selectionner une date
										</small>
									</div>
								</div>
							</div>
						</div>				
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Fermer
							</button>
							<button type="button" class="btn btn-success btn-sm" id="modal_date_sub" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
						</div>
					</div>
				</div>
			</div>
			
			
			
		</form>
		
		<div id="modal_aide" class="modal fade" role="dialog" >
			<div class="modal-dialog">			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="mdl_doc_titre" >
							<i class="fa fa-info" ></i> - <span>Aide</span>
						</h4>
					</div>
					<div class="modal-body" >
					
						<table class="table" >
							<caption>Code couleur</caption>
							<tr class="info" >
								<td>Facture non remise payée au FACTOR</td>
							</tr>
							
						</table>
						
					</div>				
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
							<i class="fa fa-close" ></i>
							Fermer
						</button>	
					</div>
				</div>
			</div>
		</div>
				
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
			
				
				// U veut enregistrer des remboursement (rb)
				$("#add_rb").click(function(){
					
					if($('.rb:checked').length>0){
						
						montant=0;
						$(".rb:checked" ).each(function( index ){
							reg_id=$(this).data("reglement");
							ht=$("#reg_montant_" + reg_id).data("reg_montant"); 
							montant=montant+ht;
						});
						montant=Math.round(montant*100)/100;
	
						$("#modal_date_titre").html("Nouveau remboursement");
						$("#form_liste").prop("action","ajax/ajax_reglement_rb.php");
						
						$("#mdl_bloc_montant .col-md-12").html("<b>Montant : </b>" + montant + " €");
						$("#mdl_bloc_montant").show();						
						$("#modal_date").modal("show");
						$("#callback").val(1);
					}else{
						modal_alerte_user("Vous n'avez selectionné aucun règlement.","");						
					}
				});
				
				$("#add_retro").click(function(){
					
					if($('.retro:checked').length>0){
						$("#modal_date_titre").html("Nouvelle rétrocession");
						$("#form_liste").prop("action","ajax/ajax_fac_retrocession.php");	
						
						$("#mdl_bloc_montant .col-md-12").html("");
						$("#mdl_bloc_montant").hide();	
						
						$("#modal_date").modal("show");
						$("#callback").val(2);
					}else{
						modal_alerte_user("Vous n'avez selectionné aucune facture.","");						
					}
				});
				
				$("#modal_date_sub").click(function(){
					if($("#mdl_chp_date").val()!=""){
						$("#chp_date").val($("#mdl_chp_date").val());	
						if($("#callback").val()==1){
							envoyer_form_ajax($("#form_liste"),afficher_rb,"","");
						}else if($("#callback").val()==2){
							envoyer_form_ajax($("#form_liste"),afficher_retro,"","");
						}
						$('.rb:checked').prop("checked",false);
						$('#rb_all').prop("checked",false);
						$("#modal_date").modal("hide");
					}else{
						$("#chp_date_required").show().delay(3000).fadeOut(500);
					}
				});
				
			
			});	

			function afficher_rb(json){
				
				$.each(json.reglements, function(i, obj) {
					$("#reg_rb_" + obj).html(json.date);
				});
				
			}
			
			function afficher_retro(json){
				
				$.each(json.factures, function(i, obj) {
					$("#fac_" + obj).html(json.date);
				});
				
			}
		</script>
	</body>
</html>
