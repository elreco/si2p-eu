<?php

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

$erreur="";

if(!empty($_POST)){

	$sous_famille_id=0;
	if(isset($_POST['psf_id'])){
		$sous_famille_id=intval($_POST['psf_id']);
	}
	
	$famille_id=0;
	if(isset($_POST['psf_pfa_id'])){
		$famille_id=intval($_POST['psf_pfa_id']);
	}
	
	if($famille_id>0){
		if($sous_famille_id>0){
			$req = $Conn->prepare("UPDATE produits_sous_familles SET psf_libelle=:psf_libelle,psf_pfa_id=:psf_pfa_id WHERE psf_id=:psf_id;");
			$req->bindParam("psf_libelle",$_POST['psf_libelle']);
			$req->bindParam("psf_pfa_id",$famille_id);
			$req->bindParam("psf_id",$sous_famille_id);
			$req->execute();

		}else{
			$req = $Conn->prepare("INSERT INTO produits_sous_familles (psf_libelle,psf_pfa_id) VALUES (:psf_libelle,:psf_pfa_id);");
			$req->bindParam("psf_libelle",$_POST['psf_libelle']);
			$req->bindParam("psf_pfa_id",$famille_id);
			$req->execute();

		}
	}else{
		$erreur="Formulaire incomplet!";
	}
		
}else{
	$erreur="Formulaire incomplet!";
}

if(!empty($erreur)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur
	);
}

header("location : sous_famille_liste.php");
	
 ?>
