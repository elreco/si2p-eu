 <?php
 
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");
	
	include 'modeles/mod_droit.php';
	include 'modeles/mod_get_client_produits.php';
	include 'modeles/mod_get_commerciaux.php';
	
	
	// AJOUT OU MAJ D4UN CLIENT SUR UNE ACTION
	
	$erreur_txt="";
	
	$action_id=0;
	if(isset($_GET["action"])){
		if(!empty($_GET["action"])){
			$action_id=intval($_GET["action"]);
		}	
	}
	
	if(empty($action_id)){
		echo("Formulaire incomplet!");
		die();
	}
	
	// CONTROLE
	
	if(empty($erreur_txt)){
		
		// info sur l'action
		
		$sql="SELECT act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,act_produit,act_agence,act_archive FROM Actions WHERE act_id=:action_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$d_action=$req->fetch();
		if(empty($d_action)){
			$erreur_txt="Impossible de charger les données";
		}else{
			if($d_action["act_archive"]==1){
				$erreur_txt="Impossible d'afficher la page!";
			}
			
		}
	}
	
	if(empty($erreur_txt)){
		
		
		$action_client_id=0;
		if(isset($_GET["action_client"])){
			if(!empty($_GET["action_client"])){
				$action_client_id=intval($_GET["action_client"]);
			}	
		}
	
		// sur la personne connecte
		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];	
		}
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];	
		}
		$consultation_gc=false;
		if($conn_soc_id!=$acc_societe){
			$acc_societe=$conn_soc_id;
			$acc_agence=0;	
			$consultation_gc=true;
		}
		
		
		// les commerciaux dispo
		$commerciaux=get_commerciaux($acc_societe,$d_action['act_agence'],"");
		
		// les dates de la formation
		$sql="SELECT pda_id,DATE_FORMAT(pda_date,'%d/%m/%Y') AS pda_date,pda_demi,pda_categorie,int_label_1,acd_date
		FROM Plannings_Dates INNER JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)
		LEFT JOIN Actions_Clients_Dates ON (Plannings_Dates.pda_id=Actions_Clients_Dates.acd_date AND acd_action_client=:action_client_id)
		WHERE pda_type=1 AND pda_ref_1=:action_id ORDER BY pda_date,pda_demi,int_label_1;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->bindParam(":action_client_id",$action_client_id);
		$req->execute();
		$d_dates=$req->fetchAll();
		
		// ATTESTATIONS
		
		$sql="SELECT att_id,att_titre FROM Attestations WHERE att_statut=1 AND att_famille=" . $d_action["act_pro_famille"] . " AND att_sous_famille=" . $d_action["act_pro_sous_famille"] . "
		AND att_sous_sous_famille=" . $d_action["act_pro_sous_sous_famille"] . " ORDER BY att_titre;";
		$req=$Conn->query($sql);
		$d_attestations=$req->fetchAll();

		// DONNEE ACTION CLIENT
		if($action_client_id>0){
		
			$sql="SELECT acl_id,acl_client,acl_produit,acl_commercial,acl_ca_unit,acl_ca,acl_pro_inter,acl_pro_reference,acl_pro_libelle,acl_confirme
			,DATE_FORMAT(acl_confirme_date,'%d/%m/%Y') AS confirme_date,acl_archive,acl_facturation,acl_opca_fac,acl_opca_num,acl_bc_num,acl_attestation
			,cli_code,cli_nom,acl_archive_ok,acl_commentaire,acl_for_nb_sta,acl_for_nb_sta_resa,acl_derogation,acl_rem_ca,acl_rem_famille,acl_non_fac,acl_ca_nc,acl_non_fac_gfc,acl_reporte
			FROM Actions_Clients,CLients WHERE acl_client=cli_id AND acl_id=:action_client;"; 
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_client",$action_client_id);
			$req->execute();
			$d_action_client=$req->fetch();
		
	
		}else{
			
			// NOUVELLE INSCRIPTION
			
			$d_action_client=array(
				"acl_client" => 0,
				"acl_produit" => 0,
				"acl_commercial" => 0,
				"acl_ca_unit" => 0,
				"acl_ca" => 0,
				"acl_pro_inter" => 0,
				"acl_pro_libelle" => "",
				"acl_confirme" => 0,
				"confirme_date" => "",
				"acl_archive" => 0,
				"acl_facturation" => "",
				"acl_opca_fac" => "",
				"acl_opca_num" => "",
				"cli_code" => "",
				"cli_nom" => "",
				"acl_bc_num" => "",
				"acl_attestation" => 0,
				"acl_commentaire" =>"",
				"acl_for_nb_sta" => 0,
				"acl_derogation" => 0,
				"acl_rem_ca" => 0,
				"acl_rem_famille" => 0,
				"acl_for_nb_sta_resa" => 0,
				"acl_ca_nc" => 0,
				"acl_reporte" => 0
			);
			
			// SUR LA SOCIETE
			
			$sql="SELECT soc_intra_inter FROM Societes WHERE soc_id=:soc_id;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":soc_id",$acc_societe);
			$req->execute();
			$d_societe=$req->fetch();
			
			
		}
		
		// ON REGARDE S'IL Y A D'AUTRE CLIENT
		
		$acl_pro_intra=0;
		
		$sql="SELECT acl_pro_inter FROM Actions_Clients WHERE NOT acl_id=:action_client AND acl_action=:action AND NOT acl_archive;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_client",$action_client_id);
		$req->bindParam(":action",$action_id);
		$req->execute();
		$d_autre_clients=$req->fetchAll();
		if(!empty($d_autre_clients)){
			foreach($d_autre_clients as $cli){
				if($cli["acl_pro_inter"]==0){
					$acl_pro_intra=1;
				}
			}
		}
		
		// ON INITIALISE LES PRODUITS DU CLIENT
		
		$pro_type=1;
		
		if(!empty($d_action_client["acl_client"])){

			if($d_action_client["acl_pro_inter"]==1){
				$pro_type=2;
			}
		
			$d_produits=get_client_produits($d_action_client["acl_client"],0,1,"",1,$pro_type,$d_action["act_pro_categorie"],$d_action["act_pro_famille"],$d_action["act_pro_sous_famille"],$d_action["act_pro_sous_sous_famille"],0);
		
		}elseif($acl_pro_intra==1){
			
			$pro_type=2;
		}

		// S'IL N'Y A PAS D'AUTRE CLIENT 
		// -> on peut changer le type de formation 
		
		if(empty($d_autre_clients)){
			
			$req=$Conn->query("SELECT pfa_id,pfa_libelle FROM Produits_Familles ORDER BY pfa_libelle;");
			$d_pro_fam=$req->fetchAll();
			
			if(!empty($d_action["act_pro_famille"])){
				$req=$Conn->query("SELECT psf_id,psf_libelle FROM Produits_Sous_Familles WHERE psf_pfa_id=" . $d_action["act_pro_famille"] . " ORDER BY psf_libelle;");
				$d_pro_s_fam=$req->fetchAll();
			}
			
			if(!empty($d_action["act_pro_sous_famille"])){
				$req=$Conn->query("SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles WHERE pss_psf_id=" . $d_action["act_pro_sous_famille"] . " ORDER BY pss_libelle;");
				$d_pro_s_s_fam=$req->fetchAll();
			}
			
			
		}
		
		
	}

	if(empty($erreur_txt)){
?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<title>SI2P - Orion</title>
				<meta name="keywords" content=""/>
				<meta name="description" content="">
				<meta name="author" content="">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<!-- CSS THEME -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
				<link href="assets/admin-tools/admin-forms/css/admin-forms.css" rel="stylesheet" type="text/css">

				<!-- CSS PLUGINS -->
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
				
				<!-- CSS Si2P -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
				
				<!-- Favicon -->
				<link rel="shortcut icon" href="assets/img/favicon.png">
			   
				<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
				<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
				
				<style type="text/css" >
					.bloc-particulier{
						display:none;
					}
				</style>
			</head>
			<body class="sb-top sb-top-sm ">
				<form method="post" action="action_client_enr.php" id="form" >
					<div>
						<input type="hidden" name="action" value="<?=$action_id?>" />
						<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
						<input type="hidden" id="action_client" name="action_client" value="<?=$action_client_id?>" />
						<input type="hidden" id="acl_facturation" name="acl_facturation" value="<?=$d_action_client["acl_facturation"]?>" />
						<input type="hidden" id="id_heracles" name="id_heracles" value="0" />
						<input type="hidden" id="nb_demi_jour" name="nb_demi_jour" value="0" />
						<input type="hidden" class="client-n-agence" id="act_agence" value="<?=$d_action["act_agence"]?>" />
					</div>
					<div id="main">
			<?php		include "includes/header_def.inc.php"; ?>
						<section id="content_wrapper">
							

							<section id="content" class="animated">
							
								<div class="row" >
							
									<div class="col-md-10 col-md-offset-1">
										
										<div class="admin-form theme-primary ">												
											
											<div class="panel heading-border panel-primary">
											
												<div class="panel-body bg-light">

													<div class="content-header mtn mbn">
												<?php
														if($action_client_id>0){
															echo("<h2>Action N°". $action_id . " - <b class='text-primary' >Modification client</b></h2>");
														}else{
															echo("<h2>Action N°". $action_id . " - <b class='text-primary' >Nouveau client</b></h2>");
														} ?>
														
													</div>							
													<div class="row" >
													
														<div class="col-md-6" >	
															<div class="section" >
																<label for="client" >Client :</label>
																<select name="client" id="client" class="select2 select2-client-n get-client" required >
													<?php			if($d_action_client["acl_client"]>0){ ?>
																		<option value='' selected='selected' >Client</option>
																		<option value="<?=$d_action_client["acl_client"]?>" selected="selected" ><?=$d_action_client["cli_code"] . " (" . $d_action_client["cli_nom"] . ")"?></option>				
													<?php			}else{
																		echo("<option value='' selected='selected' >Client</option>");
																	}?>
																		
																</select>
																<small class="text-danger texte_required" id="client_required">
																	Merci de selectionner un client
																</small>																
															</div>
														</div>
												<?php	if(isset($d_societe)){
															if($d_societe["soc_intra_inter"]==2){ ?>
																<div class="col-md-3 pt25 text-center" >	
																	<div class="section" >
																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" name="particulier" id="particulier"  value="on">
																				<span class="checkbox"></span>Créer un particulier
																			</label>
																		</div>															
																	</div>
																</div>	
																<div class="row" >
																	<div class="col-md-3 pt25 text-center" >	
																		<div class="section" >
																			<div class="option-group field">
																				<label class="option option-dark">
																					<input type="checkbox" name="devis" id="devis"  value="on">
																					<span class="checkbox"></span>Créer un devis
																				</label>
																			</div>															
																		</div>
																	</div>	
																</div>
												<?php		}
														} ?>
													</div>

													<div class="row bloc-particulier" >
														<div class="col-md-6">
														
															<div class="row" >
																<div class="col-md-12">
																	<label for="code" >Code :</label>
																	<div class="field prepend-icon">
																		<input type="text" name="par_code" id="par_code" class="gui-input nom chp-particulier" maxlength="20" placeholder="Code" />
																		<span class="field-icon">
																			<i class="fa fa-code"></i>
																		</span>
																	</div>
																	<small class="text-danger texte_required" id="par_code_required">
																		Merci de renseigner le code
																	</small>
																</div>
															</div>
															<div class="section-divider mb40" >
																<span>Adresse</span>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="par_adr1" id="par_adr1" class="gui-input chp-particulier" placeholder="Adresse" value="" />
																		<label for="ad1" class="field-icon">
																			<i class="fa fa-holding"></i>
																		</label>
																	</div>
																	<small class="text-danger texte_required" id="par_adr1_required">
																		Merci de renseigner l'adresse
																	</small>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="par_adr2" class="gui-input" id="par_adr2" placeholder="Adresse (Complément 1)" value="">
																		<label for="cli_ad2" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>

															<div class="row">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="par_adr3" id="par_adr3" class="gui-input" placeholder="Adresse (Complément 2)" value="" >
																		<label for="cli_ad3" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>

															<div class="row">
																<div class="col-md-3">
																	<div class="field prepend-icon">
																		<input type="text" id="par_cp" name="par_cp" class="gui-input code-postal chp-particulier" placeholder="Code postal"  value="" >
																		<label for="cli_cp" class="field-icon">
																			<i class="fa fa fa-certificate"></i>
																		</label>
																	</div>
																	<small class="text-danger texte_required" id="par_cp_required">
																		Merci de renseigner le CP
																	</small>
																</div>
																<div class="col-md-9">
																	<div class="field prepend-icon">
																		<input type="text" name="par_ville" id="par_ville" class="gui-input nom chp-particulier" placeholder="Ville"  value="" >
																		<label for="cli_ville" class="field-icon">
																			<i class="fa fa fa-building"></i>
																		</label>
																	</div>
																	<small class="text-danger texte_required" id="par_ville_required">
																		Merci de renseigner la ville
																	</small>
																</div>
															</div>											
														</div>
														<div class="col-md-6" >
															<div class="section-divider mb40" >
																<span>Contact</span>
															</div>
															
															<div class="row">
																<div class="col-md-6">																	
																	<div class="field select">
																		<select name="par_titre" id="par_titre" class="chp-particulier" >
																			<option value="" >Civilité...</option>
																			<option value="1">Monsieur</option>
																			<option value="2">Madame</option>
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																	<small class="text-danger texte_required" id="par_titre_required">
																		Merci de renseigner la civilité
																	</small>
																</div>
															</div>
															<div class="row pt5">
																<div class="col-md-6">																
																	<div class="field prepend-icon">
																		<input type="text" name="par_nom" id="par_nom" class="gui-input nom chp-particulier" placeholder="Nom"  value=""  >
																		<label for="par_nom" class="field-icon">
																			<i class="fa fa-user"></i>
																		</label>
																	</div>
																	<small class="text-danger texte_required" id="par_nom_required">
																		Merci de renseigner le nom du client
																	</small>
																</div>
																<div class="col-md-6">																	
																	<div class="field prepend-icon">
																		<input type="text" name="par_prenom" id="par_prenom" class="gui-input prenom chp-particulier" placeholder="Prénom"  value="" >
																		<label for="par_prenom" class="field-icon">
																			<i class="fa fa-tag"></i>
																		</label>
																	</div>
																	<small class="text-danger texte_required" id="par_prenom_required">
																		Merci de renseigner le prénom du client
																	</small>
																</div>
															</div>
															<div class="row pt5">
																<div class="col-md-6">
																	
																	<div class="field prepend-icon">
																		<input name="par_tel" id="par_tel" type="tel" class="gui-input telephone" placeholder="Numéro de téléphone"  value="">
																		<label for="par_tel" class="field-icon">
																			<i class="fa fa fa-phone"></i>
																		</label>
																	</div>
																	
																</div>
																<div class="col-md-6">
																
																	<div class="field prepend-icon">
																		<input name="par_fax" id="par_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax"  value="">
																		<label for="par_fax" class="field-icon">
																			<i class="fa fa-phone-square"></i>
																		</label>
																	</div>
																	
																</div>
															</div>
															<div class="row pt5">
																<div class="col-md-6">
																	
																		<div class="field prepend-icon">
																			<input name="par_portable" id="par_portable" type="tel" class="gui-input telephone" placeholder="Numéro de portable"  value="">
																			<label for="par_portable" class="field-icon">
																				<i class="fa fa fa-mobile"></i>
																			</label>
																		</div>
																	
																</div>
																<div class="col-md-6">
																	
																		<div class="field prepend-icon">
																			<input type="email" name="par_mail" id="par_mail" class="gui-input" placeholder="Adresse Email"  value="">
																			<label for="cli_mail" class="field-icon">
																				<i class="fa fa-envelope"></i>
																			</label>
																		</div>
																	
																</div>
															</div>
															<div class="row pt5">
																<div class="col-md-6" >
																	<label for="par_naissance" class="field prepend-icon">
																		<input type="text" id="par_naissance" name="par_naissance" class="gui-input datepicker date chp-particulier" placeholder="Date de naissance" />
																		<span class="field-icon">
																			<i class="fa fa-calendar-o"></i>
																		</span>
																	</label>
																	<small class="text-danger texte_required" id="par_naissance_required">
																		Merci de renseigner la date de naissance du client
																	</small>
																</div>
															</div>
														</div>
													</div>
													
													<div class="row" >
														<!-- colonne 1 -->
														<div class="col-md-6" >	
													
															<div class="section-divider mb40" >
																<span>Formation</span>
															</div>								
															<div class="row" >																
																<div class="col-md-6" >	
																	<div class="section" >
																		 <label class="option">
																<?php		if($acl_pro_intra==1){ ?>
																				<input type="radio" class="produit-type" name="produit_type" id="produit_intra" value="1" disabled />	
																<?php		}else{ ?>
																				<input type="radio" class="produit-type" name="produit_type" id="produit_intra" value="1"  <?php if($pro_type==1) echo("checked"); ?> />																			
																<?php		} ?>
																			<span class="radio"></span>Produit intra
																		</label>
																	</div>
																</div>
																
																<div class="col-md-6" >	
																	<div class="section" >
																		 <label class="option">
																			<input type="radio" class="produit-type" name="produit_type" id="produit_inter" value="2" <?php if($pro_type==2) echo("checked"); ?> />
																			<span class="radio"></span>Produit inter
																		</label>
																	</div>
																</div>																
															</div>
													<?php	if(empty($d_autre_clients)){ ?>
																<div class="row" >																
																	<div class="col-md-12" >
																		<div class="section" >
																			<label for="pro_fam" >Famille :</label>
																			<select name="act_pro_famille" id="pro_fam" class="select2" required >
																				<option value="" >Famille</option>
																		<?php	if(!empty($d_pro_fam)){
																					foreach($d_pro_fam as $p){
																						if($p["pfa_id"]==$d_action["act_pro_famille"]){
																							echo("<option value='" . $p["pfa_id"] . "' selected >" . $p["pfa_libelle"] . "</option>");
																						}else{
																							echo("<option value='" . $p["pfa_id"] . "' >" . $p["pfa_libelle"] . "</option>");
																						}
																					}
																				} ?>																								
																			</select>
																			<small class="text-danger texte_required" id="pro_fam_required" >
																				Merci de selectionner une famille
																			</small>
																		</div>
																	</div>
																</div>
																<div class="row bloc-s-fam" >																
																	<div class="col-md-12" >
																		<div class="section" >
																			<label for="pro_s_fam" >Sous-Famille :</label>
																			
																			<select name="act_pro_sous_famille" id="pro_s_fam" class="select2" required >
																				<option value="" >Sous-Famille</option>
																		<?php	if(!empty($d_pro_s_fam)){
																					foreach($d_pro_s_fam as $p){
																						if($p["psf_id"]==$d_action["act_pro_sous_famille"]){
																							echo("<option value='" . $p["psf_id"] . "' selected >" . $p["psf_libelle"] . "</option>");
																						}else{
																							echo("<option value='" . $p["psf_id"] . "' >" . $p["psf_libelle"] . "</option>");
																						}
																					}
																				} ?>																								
																			</select>
																			<small class="text-danger texte_required" id="pro_s_fam_required">
																				Merci de selectionner une sous-famille
																			</small>
																		</div>
																	</div>
																</div>
																<div class="row bloc-ss-fam" <?php if(empty($d_pro_s_s_fam)) echo("style='display:none;'"); ?> >																
																	<div class="col-md-12" >
																		<div class="section" >
																			<label for="pro_s_s_fam" >Sous Sous-Famille :</label>
																			<select name="act_pro_sous_sous_famille" id="pro_s_s_fam" class="select2" >
																				<option value="" >Sous Sous-Famille</option>
																		<?php	if(!empty($d_pro_s_s_fam)){
																					foreach($d_pro_s_s_fam as $p){
																						if($p["pss_id"]==$d_action["act_pro_sous_sous_famille"]){
																							echo("<option value='" . $p["pss_id"] . "' selected >" . $p["pss_libelle"] . "</option>");
																						}else{
																							echo("<option value='" . $p["pss_id"] . "' >" . $p["pss_libelle"] . "</option>");
																						}
																					}
																				} ?>																								
																			</select>
																			<small class="text-danger texte_required" id="pro_s_s_fam_required">
																				Merci de selectionner une sous sous-famille
																			</small>
																		</div>
																	</div>
																</div>
													<?php	} ?>
															<div class="row bloc-produit" >
																<div class="col-md-12" >	
																	<div class="section" >
																		<label for="produit" >Produit :</label>
																		<select name="produit" id="produit" class="select2 produit" required >
																			<option value="" >Produit</option>
																	<?php	$ca_verrou=false;
																			if(isset($d_produits)){
																				if(!empty($d_produits)){
																					foreach($d_produits as $p){
																						if($p["id"]==$d_action_client["acl_produit"]){
																							if(($pro_type==1 AND $p["ca_verrou_intra"]) OR ($pro_type==2 AND $p["ca_verrou_inter"]) ){
																								$ca_verrou=true;
																							}
																							echo("<option value='" . $p["id"] . "' selected >" . $p["reference"] . " - " . $p["libelle"] . "</option>");
																						}else{
																							echo("<option value='" . $p["id"] . "' >" . $p["reference"] . " - " . $p["libelle"] . "</option>");
																						}
																					}
																				}else{
																					echo("<option value='0' >Sélectionnez un produit</option>");
																				} 
																			} 
																			// ca verrou intervient dans le calcul des attributs des champs commerce
																			
																			$att_ca_nc="";
																			$att_ca="";
																			if($ca_verrou){
																				$att_ca_nc="disabled";																	
																			}elseif($d_action_client["acl_ca_nc"]){
																				$att_ca_nc="checked";	
																				
																			}
																			if($ca_verrou){
																				$att_ca="readonly";																	
																			}elseif($d_action_client["acl_ca_nc"]){
																				$att_ca="disabled";	
																				
																			}
																				
																			?>																								
																		</select>
																		<small class="text-danger texte_required" id="produit_required">
																			Merci de selectionner un produit
																		</small>
																	</div>
																	
																</div>
															</div>
															<div class="row bloc-produit" >
																<div class="col-md-12" >	
																	<div class="section" >
																		<label for="produit_libelle" >Libellé de la formation :</label>
																		<input type="text" class="gui-input" name="acl_pro_libelle" id="produit_libelle" value="<?=$d_action_client["acl_pro_libelle"]?>" />
																	</div>
																</div>
															</div>
															<div class="row bloc-produit" >
																<div class="col-md-12" >	
																	<div class="section" >
																		<label for="attestation" >Attestation :</label>
																		<select name="acl_attestation" id="attestation" class="select2" >
																			<option value="" >Attestation</option>
																	<?php	if(!empty($d_attestations)){
																				foreach($d_attestations as $att){
																					if($att["att_id"]==$d_action_client["acl_attestation"]){
																						echo("<option value='" . $att["att_id"] . "' selected >" . $att["att_titre"] . "</option>");
																					}else{
																						echo("<option value='" . $att["att_id"] . "' >" . $att["att_titre"] . "</option>");
																					}
																				}
																			}else{
																				echo("<option value='0' >Sélectionnez un produit</option>");
																			} ?>																								
																		</select>
																		
																	</div>																	
																</div>
															</div>
															<div class="row" >
																<div class="col-md-6 mt15" >
																	<label class="option">
																		<input type="checkbox" id="confirme" name="confirme" value="on" <?php if($d_action_client["acl_confirme"]==1) echo("checked"); ?> />
																		<span class="checkbox"></span>Confirmée
																	</label>
																</div>
																<div class="col-md-6" >
																	<label for="produit" >Date de confirmation :</label>
																	<span class="field prepend-icon">
																		<input type="text" id="confirme_date" name="confirme_date" class="gui-input datepicker" placeholder="Selectionner une date" value="<?=$d_action_client["confirme_date"]?>" />
																		<span class="field-icon"><i class="fa fa-calendar-o"></i></span>
																	</span>
																</div>
															</div>
															<div class="row mt15" >
														<?php	if($action_client_id>0 AND( ($_SESSION["acces"]["acc_droits"][10] AND !$d_action_client["acl_archive_ok"]) OR $_SESSION["acces"]["acc_droits"][11])){ ?>												
																	<div class="col-md-6" >	
																		<label class="option">
																			<input type="checkbox" class="archive" name="archive" value="on" <?php if($d_action_client["acl_archive"]==1) echo("checked"); ?> />
																			<span class="checkbox"></span>Archivée
																		</label>
																	</div>
																
														<?php	} ?>
																<div class="col-md-6" >	
																	<label class="option">
																		<input type="checkbox" class="reporte" name="reporte" value="on" <?php if($d_action_client["acl_reporte"]==1) echo("checked"); ?> />
																		<span class="checkbox"></span>Action reportée
																	</label>
																</div>
															</div>
															
															<!-- COMMERCE -->
															<div class="section-divider mb40" >
																<span>Commerce</span>
															</div>
															<div class="row" >
																<div class="col-md-12" >	
																	<div class="section" >
																		<label for="commercial" >Commercial :</label>
																		<select name="commercial" id="commercial" class="select2" required >
																			<option value="" >Selectionnez un commercial ...</option>	
															<?php			if(!empty($commerciaux)){
																				foreach($commerciaux as $c){
																					if($c["com_id"]==$d_action_client["acl_commercial"]){
																						echo("<option value='" . $c["com_id"] . "' selected >" . $c["com_label_1"] . " " . $c["com_label_2"] . "</option>");
																					}else{
																						echo("<option value='" . $c["com_id"] . "' >" . $c["com_label_1"] . " " . $c["com_label_2"] . "</option>");
																					}
																				}
																			} ?>
																		</select>
																		<small class="text-danger texte_required" id="commercial_required">
																			Merci de selectionner un commercial
																		</small>
																	</div>
																</div>
															</div>
															<div class="row" >
																<div class="col-md-12" >	
																	<div class="section" >
																		<div class="option-group field">
																	
																			<label class="option option-dark">
																				<input type="checkbox" id="ca_nc" name="acl_ca_nc" value="on" <?=$att_ca_nc?> >
																				<span class="checkbox"></span>Tarif inconnu 
																			</label>
																		</div>
																	</div>
																</div>
															</div>
															
															<!-- TARIF -->
															<div class="row" id="cont_ca_intra" <?php if($pro_type==2) echo("style='display:none;'"); ?> >															
																<div class="col-md-12" >
																	<div class="section"  >																	
																		<label>CA Formation</label>
																		<div class="field prepend-icon">
																			<input id="ca_intra_unit" name="ca_intra_unit" type="hidden" value="<?=$d_action_client["acl_ca_unit"]?>" />
																			<input id="ca_intra" name="ca_intra" class="gui-input input-float" type="text" value="<?=$d_action_client["acl_ca"]?>"  min="0.00" step="0.01" <?=$att_ca?> >
																			<label class="field-icon" for="ca_intra">
																				 <i class="fa fa-euro" ></i>
																			</label>
																		</div>
																		<div id="alert_min_intra" ></div>
																	</div>
																</div>
															</div>
															<div class="row" id="cont_ca_inter" <?php if($pro_type==1) echo("style='display:none;'"); ?> >															
																<div class="col-md-4" >	
																	<div class="section" >
																		<label>CA / stagiaire</label>
																		<div class="field prepend-icon">
																			<input id="ca_inter_unit" name="ca_inter_unit" class="gui-input input-float" type="text" value="<?=$d_action_client["acl_ca_unit"]?>" min="0.00" step="0.01" <?=$att_ca?> >
																			<label class="field-icon" for="ca_inter_unit">
																				 <i class="fa fa-euro" ></i>
																			</label>
																		</div>
																		<div id="alert_min_inter" ></div>
																	</div>
																</div>
																<div class="col-md-4" >	
																	<div class="section" >
																		<label>Nb. Stagiaire(s) confirmé(s)</label>
																		<div class="field prepend-icon">
																			<input id="for_nb_sta" name="for_nb_sta" class="gui-input input-int nb-sta" type="text" value="<?=$d_action_client["acl_for_nb_sta"]?>" min="0" step="1" >
																			<label class="field-icon" for="for_nb_sta">
																				 <i class="fa fa-user" ></i>
																			</label>
																		</div>
																	</div>
																</div>
																<div class="col-md-4" >	
																	<div class="section" >
																		<label>Nb. Stagiaire(s) non confirmé(s)</label>
																		<div class="field prepend-icon">
																			<input id="for_nb_sta_resa" name="for_nb_sta_resa" class="gui-input input-int nb-sta" type="text" value="<?=$d_action_client["acl_for_nb_sta_resa"]?>" min="0" step="1" >
																			<label class="field-icon" for="for_nb_sta_resa">
																				 <i class="fa fa-user" ></i>
																			</label>
																		</div>
																	</div>
																</div>																
															</div>
												<?php 		if($_SESSION["acces"]["acc_droits"][27] OR $_SESSION["acces"]["acc_droits"][31]){ ?>
																<div class="row mt15 bloc-derog" <?php if($_SESSION["acces"]["acc_droits"][27] AND $d_action_client["acl_derogation"]==2) echo("style='display:none;'"); ?> >
																	<div class="col-md-12">
																		<div class="section-divider">
																			<span>Dérogation tarifaire</span>
																		</div>
																	</div>
																</div>
																<div class="row mt15 bloc-derog" <?php if($_SESSION["acces"]["acc_droits"][27] AND $d_action_client["acl_derogation"]==2) echo("style='display:none;'"); ?> >
																	<div class="col-md-12" >															
																		<div class="row mt15" >
																			<div class="col-md-4 text-center" >		
																				<div class="radio-custom mb5">
																					<input id="derog_niv_0" name="acl_derogation" type="radio" value="0"<?php if(empty($d_action_client["acl_derogation"])) echo(" checked"); ?> >
																					<label for="derog_niv_0">Pas de dérogation</label>
																				</div>
																			</div>
																			<div class="col-md-4 text-center" >		
																				<div class="radio-custom mb5">
																					<input id="derog_niv_1" name="acl_derogation" type="radio" value="1"<?php if($d_action_client["acl_derogation"]==1) echo(" checked"); ?> >
																					<label for="derog_niv_1">Dérogation niv.1</label>
																				</div>
																			</div>
																	<?php	if($_SESSION["acces"]["acc_droits"][31]){ ?>
																				<div class="col-md-4 text-center" >		
																					<div class="radio-custom mb5">
																						<input id="derog_niv_2" name="acl_derogation" type="radio" value="2"<?php if($d_action_client["acl_derogation"]==2) echo(" checked"); if(!$_SESSION["acces"]["acc_droits"][31]) echo(" disabled"); ?> />
																						<label for="derog_niv_2">Dérogation niv.2</label>
																					</div>
																				</div>
																	<?php	} ?>
																		</div>
																	</div>
																</div>
																<div class="row mt25 bloc-derog" >
																	<div class="col-md-3" >
																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" id="rem_famille" name="acl_rem_famille" value="on" <?php if(!empty($d_action_client["acl_rem_famille"]) OR empty($d_action_client["acl_derogation"]) ) echo(" checked"); if(empty($d_action_client["acl_derogation"])) echo(" disabled"); ?> >
																				<span class="checkbox"></span><b>Prime "Famille"</b>
																			</label>
																		</div>
																	</div>
																	<div class="col-md-9" >
																		Cocher cette case pour que le CA généré par ce code produit soit pris en compte dans le calcul des primes "Familles".
																	</div>
																</div>
																<div class="row mt25 bloc-derog" >
																	<div class="col-md-3" >
																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" id="rem_ca" name="acl_rem_ca" value="on" <?php if(!empty($d_action_client["acl_rem_ca"]) OR empty($d_action_client["acl_derogation"]) ) echo(" checked"); if(empty($d_action_client["acl_derogation"])) echo(" disabled"); ?> >
																				<span class="checkbox"></span><b>Prime "CA"</b>
																			</label>
																		</div>
																	</div>
																	<div class="col-md-9" >
																		Cocher cette case pour que le CA généré par ce code produit soit pris en compte dans le calcul des primes "CA".
																	</div>
																</div>
												<?php		} ?>
											
															<div class="row" >
																<div class="col-md-6 mt15" >	
																	<label class="option">
																		<input type="checkbox" name="opca_fac" value="on" <?php if($d_action_client["acl_opca_fac"]==1) echo("checked"); ?> />
																		<span class="checkbox"></span> Facturation financeur
																	</label>
																</div>
																<div class="col-md-6" >	
																	<label for="opca_num" >Numéro financeur :</label>
																	<input type="text" id="opca_num" name="opca_num" class="gui-input" value="<?=$d_action_client["acl_opca_num"]?>" placeholder="Numéro de prise en charge" />						
																</div>
															</div>
															<div class="row" >
																<div class="col-md-6" >	
																	<label for="opca_num" >Numéro de BC :</label>
																	<input type="text" id="bc_num" name="acl_bc_num" class="gui-input" value="<?=$d_action_client["acl_bc_num"]?>" placeholder="Numéro de bon de commande" />						
																</div>
														<?php	if($action_client_id>0 AND($_SESSION["acces"]["acc_droits"][10] OR $_SESSION["acces"]["acc_droits"][11])){ ?>			
																	<div class="col-md-6 mt25 text-center" >
														<?php			if($d_action_client["acl_facturation"]==1 AND $consultation_gc){ ?>
																			<label class="option">
																				<input type="checkbox" name="acl_non_fac" value="on" <?php if($d_action_client["acl_non_fac_gfc"]==1) echo("checked"); ?> />
																				<span class="checkbox"></span>Non facturable
																			</label>
														<?php			}else{ ?>																	
																			<label class="option">
																				<input type="checkbox" name="acl_non_fac" value="on" <?php if($d_action_client["acl_non_fac"]==1) echo("checked"); ?> />
																				<span class="checkbox"></span>Non facturable
																			</label>
														<?php			} ?>																				
																	</div>
														<?php	} ?>
															</div>
														
															<!-- COMMENTAIRE -->
															<div class="section-divider mb40" >
																<span>Commentaire</span>
															</div>
															<div class="row" >
																<div class="col-md-12" >
																	<textarea class="gui-textarea" name="acl_commentaire" id="commentaire" ><?=$d_action_client["acl_commentaire"]?></textarea>
																</div>																	
															</div>
														</div>
														<!-- fin colonne 1 -->	
														
														<!-- colonne 2 -->	
														<div class="col-md-6" >	
															<div class="section-divider mb40" >
																<span>Présence du client</span>
															</div>
															<table class="table table-condensed" >
																<thead>
																	<tr>
																		<th>
																			<label class="option">
																				<input type="checkbox" class="check-all" id="action_date_all" />
																				<span class="checkbox"></span>
																			</label>
																		</th>
																		<td colspan="3" >&nbsp;</td>
																		<td>&nbsp;</td>																		
																	</tr>
																</thead>
																<tbody>
														<?php		$date_id=0;
																	$date_test=false;
																	$nb_demi_j=0;
																	foreach($d_dates as $d){
																		if(!empty($d["ase_h_deb"])){
																			$horraire=$d["ase_h_deb"] . " - " . $d["ase_h_fin"];
																		}elseif($d["pda_demi"]==1){
																			$horraire="Matin";
																		}else{
																			$horraire="Après-midi";
																		} 
																		if(!empty($d["acd_date"])){
																			$nb_demi_j++;
																		}
																		?>
																		<tr>
																			<td>
																		<?php	if($date_id!=$d["pda_id"]){
																					$pda_cat="Th";
																					if($d["pda_categorie"]==1){
																						$pda_cat="Pr";
																					}elseif($d["pda_categorie"]==2){
																						$pda_cat="Ev";
																					}
																					?>
																					<label class="option">
																						<input type="checkbox" class="action-date" name="date_<?=$d["pda_id"]?>" value="on" <?php if(!empty($d["acd_date"])) echo("checked"); ?> />
																						<span class="checkbox"></span>
																					</label>
																		<?php 	}else{
																					echo("&nbsp;");
																				} ?>
																			</td>
																			<td><?=$d["pda_date"]?></td>
																			<td><?=$horraire?></td>
																			<td><?=$d["int_label_1"]?></td>
																			<td><?=$pda_cat?></td>
																		</tr>
														<?php			$date_id=$d["pda_id"]; 
																	} ?>
																</tbody>
															</table>
															<small class="text-danger texte_required" id="date_required">
																Merci de selectionner au moins une date 
															</small>
														</div>
														<!-- fin colonne 2 -->	
														
													</div>
												</div>
												<!-- fin panel body -->
											</div>
											<!-- fin panel -->
										</div>
										<!-- fin admin form -->	
									</div>
									
								</div>
									
							</section>
							
						</section>
					</div>
					<footer id="content-footer" class="affix">
						<div class="row">
							<div class="col-xs-3 footer-left">
								<a href="action_voir.php?action=<?=$action_id?>&action_client=<?=$action_client_id?>&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
									Retour
								</a>							
							</div>
							<div class="col-xs-6 footer-middle"></div>
							<div class="col-xs-3 footer-right">
								<button type="button" class="btn btn-success btn-sm" id="sub_form" >
									<i class="fa fa-save" ></i> Enregistrer
								</a>
							</div>
						</div>
					</footer>
				</form>	
			
		<?php	
				include "includes/footer_script.inc.php"; ?>
				<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>	
				<script src="vendor/plugins/mask/jquery.mask.js"></script>
				<script src="assets/js/custom.js"></script>

				<script type="text/javascript" src="/assets/js/sync_heracles.js"></script>
				<script type="text/javascript" >
					var societ=parseInt(<?=$acc_societe?>);
				
					// val memo au chargement
			<?php	if($_SESSION["acces"]["acc_droits"][31]){ ?>
						var u_derog=2;
			<?php	}elseif($_SESSION["acces"]["acc_droits"][27]){ ?>
						var u_derog=1;
			<?php	}else{ ?>
						var u_derog=0;
			<?php	} ?>					
					var categorie="<?=$d_action["act_pro_categorie"]?>";
					
			<?php	if($d_action_client["acl_pro_inter"]==1){ ?>		
						var type_produit=2;
			<?php	}else{ ?>
						var type_produit=1;
			<?php	} ?>			
					var famille="<?=$d_action["act_pro_famille"]?>";
					var sous_famille="<?=$d_action["act_pro_sous_famille"]?>";
					var sous_sous_famille="<?=$d_action["act_pro_sous_sous_famille"]?>";

					// ini des val pour calcul du ca en cas de non changement de produit
			<?php	if(isset($d_produits)){
						if(!empty($d_produits[$d_action_client["acl_produit"]])){ ?>
							var ht_intra=parseFloat("<?=$d_produits[$d_action_client["acl_produit"]]["ht_intra"]?>");
							var min_intra=parseFloat("<?=$d_produits[$d_action_client["acl_produit"]]["min_intra"]?>");
							var min_dr_intra=parseFloat("<?=$d_produits[$d_action_client["acl_produit"]]["min_dr_intra"]?>");
							
							var ht_inter=parseFloat("<?=$d_produits[$d_action_client["acl_produit"]]["ht_inter"]?>");
							var min_inter=parseFloat("<?=$d_produits[$d_action_client["acl_produit"]]["min_inter"]?>");
							var min_dr_inter=parseFloat("<?=$d_produits[$d_action_client["acl_produit"]]["min_dr_inter"]?>");
							
							var derogation=parseFloat("<?=$d_action_client["acl_derogation"]?>");
							<?php if(!empty($d_produits[$d_action_client["acl_produit"]]["nb_demi_jour"])){ ?>
							var produit_demi_j=parseInt("<?=$d_produits[$d_action_client["acl_produit"]]["nb_demi_jour"]?>");
							<?php }?>

							var cout_demi=0;						
			<?php			if($d_action_client["acl_pro_inter"]==0){
								if(!empty($nb_demi_j)){ ?>
									cout_demi=parseFloat("<?=$d_action_client["acl_ca"]/$nb_demi_j?>");
			<?php				}
							}
						}else{ ?>
							
							var ht_intra=0;
							var min_intra=0;
							var min_dr_intra=0;
							
							var ht_inter=0;
							var min_inter=0;
							var min_dr_inter=0;
							var derogation=0;
							
							var cout_demi=0;
							var produit_demi_j=0;
			<?php		}
					}else{ ?>
						var ht_intra=0;
						var min_intra=0;
						var min_dr_intra=0;
						
						var ht_inter=0;
						var min_inter=0;
						var min_dr_inter=0;
						var derogation=0;
						
						var cout_demi=0;
						var produit_demi_j=0;
						
			<?php	} ?>
			
		
					jQuery(document).ready(function() {
						
						
						$("#particulier").click(function(){
							if($(this).is(":checked")){
								$("#client").select2("val",0);
								$("#client").val(0);
								recuperer_produits();
								$(".bloc-particulier").show();							
							}else{
								$(".bloc-particulier").hide();
							}
						});
						
						$("#client").change(function(){
							if($(this).val()>0){
								$("#particulier").prop("checked",false);
								$(".bloc-particulier").hide();
							}
						});
						
						$(".produit-type").change(function(){
							
							type_produit=$('input[name=produit_type]:checked').val()					
							afficher_produit_type(type_produit);	
							
							if($("#produit").is(":visible")){
								if($("#client").val()>0){
									recuperer_produits();
								}
							}
							
							
							//get_produits(client,type_produit,categorie,famille);							
						});
						
						$("#pro_fam").change(function(){
							get_sous_familles($(this).val(),actualiser_sous_famille,"",0);
						});						
						
						$("#pro_s_fam").change(function(){
							get_sous_sous_familles($(this).val(),actualiser_ss_famille,"",0);
						});
						
						$("#pro_s_s_fam").change(function(){
							if($(this).val()>0){
								afficher_bloc_produit();
							}else{
								$(".bloc-produit").hide();	
							}
							
						});

						$("#produit").change(function(){
							console.log("#produit a");
							client=$("#client").val();
							produit=$(this).val();
							if((client>0||$("#particulier").is(":checked"))&&produit>0){
								get_produit(client,produit,1,"",afficher_produit,"","");	
							}else{
								afficher_produit("");
							}	
						});
						
						$("#ca_nc").click(function(){
							if($(this).is(":checked")){
								$("#ca_intra").val(0);
								$("#ca_intra").prop("disabled",true);
								$("#ca_inter_unit").val(0);
								$("#ca_inter_unit").prop("disabled",true);
								if($("#devis").length>0){
									$("#devis").prop("checked",false);
									$("#devis").prop("disabled",true);
								}
							}else{
								
								$("#ca_intra").prop("disabled",false);
								$("#ca_inter_unit").prop("disabled",false);
								
								if($("#devis").length>0){
									$("#devis").prop("disabled",false);
								}
								
								if(type_produit==1){
									$("#ca_intra").val(ht_intra);
								}else{
									$("#ca_inter_unit").val(ht_inter);
								}								
							}
						});
						
						$("#ca_intra").blur(function(){
							
							ca=$(this).val();
							ca=parseFloat(Math.round(ca * 100) / 100).toFixed(2);
							$("#ca_intra").val(ca);
							
							nb_demi_j=$(".action-date:checked").length;
							
							coeff_ca=0;
							if(produit_demi_j!=0){
								coeff_ca=nb_demi_j/produit_demi_j;
							}
							
							if(derogation<2){							
								if(derogation==1){
									if(min_dr_intra>0&&$(this).val()<min_dr_intra*coeff_ca){
										ca=min_dr_intra*coeff_ca;
										$(this).val(ca);
										afficher_txt_user($("#alert_min_intra"),"Tarif minimum à " + ca + " €","danger",5000);					
									}
								}else{
									if(min_intra>0&&$(this).val()<min_intra*coeff_ca){
										ca=min_intra*coeff_ca;
										$(this).val(ca);
										afficher_txt_user($("#alert_min_intra"),"Tarif minimum à " + ca + " €","danger",5000);					
									}
								}
							}
							// on calcul le cout jour							
							if(nb_demi_j!=0){
								cout_demi=ca/nb_demi_j;
							}
						});
						
						$("#ca_inter_unit").blur(function(){
							if(derogation<2){
								if(derogation==1){
									if(min_dr_inter>0&&$(this).val()<min_dr_inter){
										$(this).val(min_dr_inter);
										afficher_txt_user($("#alert_min_inter"),"Tarif minimum à " + min_dr_inter + " €","danger",5000);					
									}
								}else{
									if(min_inter>0&&$(this).val()<min_inter){
										$(this).val(min_inter);
										afficher_txt_user($("#alert_min_inter"),"Tarif minimum à " + min_inter + " €","danger",5000);					
									}
								}
							}
						});
						
						$(".nb-sta").blur(function(){							
							if($("#for_nb_sta").val()>0 && $("#for_nb_sta_resa").val()==0 && !$("#confirme").is(":checked")){							
								
								$("#confirme").prop("checked",true);
								$("#confirme_date").prop("disabled",false);
								$("#confirme_date").datepicker('setDate', '00/00/0000');
								
							}else if($("#for_nb_sta_resa").val()>0){
								
								$("#confirme").prop("checked",false);
								$("#confirme_date").prop("disabled",true);
								$("#confirme_date").val("");
							}
						});
						
						$("#confirme").click(function(){
							if($(this).is(":checked")){
								$("#confirme_date").prop("disabled",false);
								$("#confirme_date").datepicker('setDate', '00/00/0000');
							}else{
								$("#confirme_date").prop("disabled",true);
								$("#confirme_date").val("");
							}
						});
						
						// changement de dérogation
						$('input[name=acl_derogation]').change(function(){
							derogation=$('input[name=acl_derogation]:checked').val();
							if(derogation==0){
								$("#rem_famille").prop("checked",true);
								$("#rem_famille").prop("disabled",true);
								$("#rem_ca").prop("checked",true);
								$("#rem_ca").prop("disabled",true);
							}else{
								$("#rem_famille").prop("disabled",false);
								$("#rem_ca").prop("disabled",false);
							}
							pro_type=$('input[name=produit_type]:checked').val();
							if(pro_type==1){
								$("#ca_intra").trigger("blur");
							}else{
								$("#ca_inter_unit").trigger("blur");
							}
							
						});
						
						// selection des dates
						$(".action-date").click(function(){						
							afficher_ca_jour();
						});
						
						$(".check-all").click(function(){
							afficher_ca_jour();
						});
						
						
						$("#sub_form").click(function(){
							
							if($("#particulier").is(":checked")){
								$("#client").prop("required",false);
								$(".chp-particulier").prop("required",true);	
							}else{
								$("#client").prop("required",true);	
								$(".chp-particulier").prop("required",false);	
							}
							
							if($("#devis").length>0){
								if($("#devis").is(":checked")){
									
								}else{
									
								}
							}
								
							valide=valider_form("#form");
							console.log(valide);
							
							valid_comp=true;						
							if($('.action-date:checked').length==0){
								valid_comp=false;
								$("#date_required").show();
							}else{
								$("#date_required").hide();
							}						
							if(valide && valid_comp){
								nb_demi_j=$(".action-date:checked").length;
								$("#nb_demi_jour").val(nb_demi_j);
								if($("#action_client").val()==0){
									
									action_sync("<?=$acc_societe?>","#form","#id_heracles","","","");
								}else{
									$("#form").submit();
		
								}
							}
						});


						// ----- FIN DOC READY -----
					});
					(jQuery);
					
					function actualiser_get_client(json){
						console.log("actualiser_get_client()");
						if(json){
							
							$("#commercial").val(json.commercial).trigger("change");
							
							recuperer_produits();

						}else{
							$("#commercial").val(0).trigger("change");
							actualiser_select2("","#produit","");
						}											
					}
					
					function actualiser_sous_famille(json){
						
						$("#pro_s_fam option[value!='0'][value!='']").remove();
						$("#pro_s_fam").select2({
							data: json,
							closeOnSelect: true			
						});
						actualiser_ss_famille("");	
						if(json==""){
							$(".bloc-s-fam").hide();							
						}else{
							$(".bloc-s-fam").show();
						}
						
						
						
					}
					
					function actualiser_ss_famille(json){
						console.log("actualiser_ss_famille()");
						$("#pro_s_s_fam option[value!='0'][value!='']").remove();
						$("#pro_s_s_fam").select2({
							data: json,
							closeOnSelect: true			
						});
						if(json==""){
							$(".bloc-ss-fam").hide();				
							if($("#pro_s_fam").select2("val")>0){
								afficher_bloc_produit();
							}else{
								$(".bloc-produit").hide();
								$("#produit").val("").trigger("change");
							}
						}else{
							$(".bloc-ss-fam").show();
							$("#pro_s_s_fam").prop("required",true);
							$(".bloc-produit").hide();
							$("#produit").val("").trigger("change");
						}
						
						
					}
					
					function recuperer_produits(){
						console.log("recuperer_produits()");
						intra=true;
						inter=false;
						if($("#produit_inter").is(":checked")){
							intra=false;
							inter=true;							
						}
						client=$("#client").val();
						
						if($('#pro_fam').length>0){							
							select_fam=$("#pro_fam").select2("val");
							select_s_fam=$("#pro_s_fam").select2("val");
							select_ss_fam=$("#pro_s_s_fam").select2("val");	
								
							get_produits(client,categorie,select_fam,select_s_fam,select_ss_fam,intra,inter,actualiser_produits,"","");
						}else{
							get_produits(client,categorie,famille,sous_famille,sous_sous_famille,intra,inter,actualiser_produits,"","");
						}	
					}
					
					function actualiser_produits(json){
						$("#produit option[value!='0'][value!='']").remove();
						$("#produit").select2({
							data: json,
							closeOnSelect: true			
						});
						$("#produit").val("").trigger("change");
						
						if($('#pro_fam').length>0){							
							select_fam=$("#pro_fam").select2("val");
							select_s_fam=$("#pro_s_fam").select2("val");
							select_ss_fam=$("#pro_s_s_fam").select2("val");							
							get_attestations(select_fam,select_s_fam,select_ss_fam,actualiser_select2,"#attestation","");
						}
					}
									
					function afficher_bloc_produit(){
						recuperer_produits();
						$(".bloc-produit").show();
					}
					
					// utilisation des donnees renvoyer suite au choix du produit
					function afficher_produit(json){
						console.log("afficher_produit()");
						if(json){
							
							$("#produit_libelle").val(json.libelle);
							
							ht_intra=parseFloat(json.ht_intra);
							min_intra=parseFloat(json.min_intra);
							min_dr_intra=parseFloat(json.min_dr_intra);
							
							ht_inter=parseFloat(json.ht_inter);
							min_inter=parseFloat(json.min_inter);
							min_dr_inter=parseFloat(json.min_dr_inter);	
							
							derogation=parseInt(json.derogation);
																				
							produit_demi_j=parseInt(json.nb_demi_jour);
							
							// si u à accès aux derog on affiche s'il dispose du droit nécéssaire
							
							if($(".bloc-derog").length>0){
								$("#derog_niv_" + derogation).prop("checked",true);
								$("#derog_niv_" + derogation).trigger("change");
								if(derogation>0){
									if(json.rem_famille==1){
										$("#rem_famille").prop("checked",true);
									}else{
										$("#rem_famille").prop("checked",false);
									}
									if(json.rem_ca==1){
										$("#rem_ca").prop("checked",true);
									}else{
										$("#rem_ca").prop("checked",false);
									}
								}
								
								if(derogation>u_derog){
									$(".bloc-derog").hide();
								}else{
									$(".bloc-derog").show();
								}
							}
							
							if($("#produit_inter").is(":checked")){
								$("#ca_inter_unit").val(ht_inter);
								if(json.ca_verrou_inter){
									$("#ca_nc").prop("checked",false);
									$("#ca_nc").prop("disabled",true);
									$("#ca_inter_unit").prop("disabled",false);
									$("#ca_inter_unit").prop("readonly",true);
								}else{
									$("#ca_nc").prop("disabled",false);
									$("#ca_inter_unit").prop("readonly",false);
								}
							}else{
								cout_demi=0;
								if(produit_demi_j!=0){
									cout_demi=ht_intra/produit_demi_j;
								}
								nb_demi_j=$(".action-date:checked").length;
								
								console.log("ht_intra : " + ht_intra);
								console.log("produit_demi_j : " + produit_demi_j);
								console.log("cout_demi : " + cout_demi);
								console.log("nb_demi_j : " + nb_demi_j);
								
								$("#ca_intra").val(cout_demi*nb_demi_j);
								if(json.ca_verrou_intra){
									$("#ca_nc").prop("checked",false);
									$("#ca_nc").prop("disabled",true);
									
									$("#ca_intra").prop("disabled",false);
									$("#ca_intra").prop("readonly",true);
									
								}else{
									$("#ca_intra").prop("readonly",false);
									$("#ca_nc").prop("disabled",false);
								}
							}
							
							
							
						}else{
							$("#produit_libelle").val("");
							$("#ca_intra").val("");
							$("#ca_inter_unit").val("");
							derogation=0;
							min_intra=0;
							min_dr_intra=0;
							min_inter=0;
							min_dr_inter=0;
						}
						
					}
					
					// affiche les elements du form en fonction du type de produit
					function afficher_produit_type(type){
						console.log("afficher_produit_type(" + type + ")");
						if(type==1){
							$("#cont_ca_inter").hide();
							$("#cont_ca_intra").show();
						}else{
							$("#cont_ca_inter").show();
							$("#cont_ca_intra").hide();
						}					
					}
					
					function afficher_ca_jour(){
					
						nb_demi_j=$(".action-date:checked").length;
					
						if($("#produit_intra").is(":checked")){
							
							ca=cout_demi*nb_demi_j;
							ca=parseFloat(Math.round(ca * 100) / 100).toFixed(2);
							
							$("#ca_intra").val(ca);
						}
					}
				</script>
			</body>
		</html>
<?php
	}else{
		echo($erreur_txt);
	}	?>