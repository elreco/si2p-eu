<?php 
/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');
///////////////////// Contrôles des parametres ////////////////////
// session retour

// fin session retour
$param_fournisseur = 0;

if(!empty($_GET['id'])){

    // si les get sont pas remplis
    $param_fournisseur = intval($_GET['id']);


}
if($param_fournisseur == 0){
 
    echo("Impossible d'afficher la page");
    die();
}
///////////////////// Fin Contrôles des parametres ////////////////////
/////////////// TRAITEMENTS BDD ///////////////////////

$req = $Conn->prepare("SELECT * FROM sessions WHERE ses_id = " . $_GET['id']);
$req->execute();
$session = $req->fetch();

// LES HORRAIRES

$req = $Conn->prepare("SELECT * FROM sessions_horaires WHERE sho_session = " . $_GET['id'] . " ORDER BY sho_h_deb;");
$req->execute();
$d_horaires = $req->fetchAll();


$sql="SELECT * FROM CNRS_stagiaires 
LEFT JOIN sessions_stagiaires ON (CNRS_stagiaires.sta_id = sessions_stagiaires.sst_stagiaire)
LEFT JOIN sessions_horaires ON (sessions_stagiaires.sst_horaire = sessions_horaires.sho_id)
WHERE sst_session = " . $_GET['id'] . " ORDER BY sta_nom ,sta_prenom;";
$req = $Conn->prepare($sql);
$req->execute();
$sessions_stagiaires = $req->fetchAll();


?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Inscrire un stagiaire</title> 
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">

	<form method="post" action="stagiaire_enr.php" enctype="multipart/form-data">
		<div id="main">
<?php 		include "includes/header_cli.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
			<?php 	if(isset($_GET['succes']) && $_GET['succes'] == 1){ ?>
						<div class="col-md-12">
							<div class="alert alert-success">
								Le stagiaire a bien été inscrit.
							</div>
						</div>
			<?php 	}
					if(isset($_GET['succes']) && $_GET['succes'] == 2){ ?>
						<div class="col-md-12">
							<div class="alert alert-success">
								L'inscription a bien été modifiée.
							</div>
						</div>
			<?php 	}
					if(isset($_GET['warning']) && $_GET['warning'] == 1){ ?>
						<div class="col-md-12">
							<div class="alert alert-warning">
								Le stagiaire existait déjà dans la base.
							</div>
						</div>
			<?php 	}  ?>
					<div class="row"> 		
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary ">
								
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
									
										<div class="content-header">
											<h2>
												Formation <b class="text-primary"><?= $session['ses_formation'] ?></b> 
												du <b class="text-primary"><?= convert_date_txt($session['ses_date']) ?></b>
									<?php		if(!empty($session['ses_lieu'])){ ?>
													à <b class="text-primary"><?=$session['ses_lieu']?></b>
									<?php		} ?>
											</h2>       
										</div>
										<div class="row" >
											<div class="col-md-6" >
												Feuille de présence :
									<?php		if(file_exists("documents/sessions_stagiaires/" . $session['ses_id'] . "." . $session['ses_ext'])){
													$download_nom="Présence " . $session['ses_formation'] . " " . convert_date_txt($session['ses_date']); ?>
													<a href="documents/sessions_stagiaires/<?= $session['ses_id'] ?>.<?= $session['ses_ext'] ?>" download="<?=$download_nom?>" data-toggle="tooltip" title="Télécharger la feuille de présence" class="btn btn-success btn-sm">
														<i class="fa fa-file-pdf-o"></i>
													</a>
									<?php		}else{
													echo("<span class='text-danger' >Non disponible</span>");
												} ?>
											</div>
											<div class="col-md-6" >
												Attestation groupée :
									<?php		if(file_exists("documents/attestations/". $session['ses_id'] . "/conso_" . $session['ses_id'] . ".pdf")){
													$download_nom="Attestation groupée " . $session['ses_formation'] . " " . convert_date_txt($session['ses_date']); ?>
													<a href="documents/attestations/<?= $session['ses_id'] ?>/conso_<?= $session['ses_id'] ?>.pdf" download="<?=$download_nom?>" data-toggle="tooltip" title="Télécharger l'attestation groupée" class="btn btn-success btn-sm">
														<i class="fa fa-file-pdf-o"></i>
													</a>
									<?php		}else{
													echo("<span class='text-danger' >Non disponible</span>");
												} ?>
											</div>
										</div>
									
											
										<div class="row" >
											<div class="col-md-6" >
												<h3>Liste des sessions</h3>
												 <table class="table">
													<thead>
														<tr class="dark">
															<th class="text-center">Heure de début</th>
															<th class="text-center">Heure de fin</th>
															<th class="text-center">Inscrire</th>
														</tr>
													</thead>
													<tbody>
												<?php	if(!empty($d_horaires)){
															foreach($d_horaires as $d){ ?>
																<tr>
																	<td class="text-center" ><?=$d["sho_h_deb"]?></td>
																	<td class="text-center" ><?=$d["sho_h_fin"]?></td>
																	<td class="text-center" >
																		<a href="session_stagiaire_mod.php?formation=<?=$param_fournisseur?>&session=<?=$d["sho_id"]?>" data-toggle="tooltip" class="btn btn-success btn-sm" title="Inscrire un nouveau stagiaire" >
																			<i class="fa fa-plus"></i>
																		</a> 
																	</td>
																</tr>
												<?php		}								
														} ?>
													</tbody>
												</table>
											</div>				
											<div class="col-md-6" >
												
												<h3>Liste des stagiaires</h3>

												<table class="table">
													<thead>
														<tr class="dark">
															<th>Nom et prénom</th>
															<th class="text-center">Session</th>
															<th class="text-center">Attestation</th>
															<th class="text-center">Modifier</th>
															<th class="text-center">Supprimer</th>
														</tr>
													</thead>
													<tbody>
											<?php 		foreach($sessions_stagiaires as $s){ 
															
															?>
															<tr class="tr_stagiaire" data-id="<?= $s['sta_id'] ?>">
																<td><?= $s['sta_nom'] ?> <?=$s['sta_prenom'] ?></td>
																<td class="text-center" ><?=$s['sho_h_deb'] . " / " . $s['sho_h_fin']?></td>
																<td class="text-center" >
												<?php				if(file_exists("documents/attestations/" . $s['sst_session'] . "/" . $s['sta_id'] . ".pdf")){
																		$down_name="Attestation " . $session['ses_formation'] . " " . $session['ses_date'] . " " . $s['sta_nom'] . ".pdf" ?>
																		<a download="<?=$down_name?>" href="documents/attestations/<?=$s['sst_session']?>/<?=$s['sta_id']?>.pdf" class="btn btn-success btn-sm" data-toggle="tooltip" data-title="Télécharger l'attestation" >
																			<i class="fa fa-download"></i>
																		</a>
												<?php				}else{
																		echo("<span class='text-danger' >Non disponible</span>");
																	} ?>
																</td>
																<td class="text-center" >	
																	<a href="session_stagiaire_mod.php?formation=<?=$_GET['id']?>&stagiaire=<?=$s['sta_id']?>" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Modifier l'inscription d'un stagiaire" >
																		<i class="fa fa-pencil"></i>
																	</a>
																</td>
																<td class="text-center">	
																	<a href="#" data-id="<?= $s['sta_id'] ?>" data-toggle="modal" data-target="#modalsuppr" class="btn btn-danger btn-sm btn-suppr">
																		<i class="fa fa-times"></i>
																	</a>
																</td>
															</tr>
											<?php 		} ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>

		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
		<?php		if(!empty($_SESSION["retour_formation"])){
						$url_retour=$_SESSION["retour_formation"];
					}else{
						$url_retour="session_liste.php";	
					} ?>
					<a href="<?=$url_retour?>" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle">

				</div>
				<div class="col-xs-3 footer-right">
					
					<a href="session_stagiaire_mod.php?formation=<?= $param_fournisseur?>" data-toggle="tooltip" class="btn btn-success btn-sm" title="Inscrire un nouveau stagiaire" >
						<i class='fa fa-plus'></i> Inscrire un nouveau stagiaire
					</a>
				</div>
			</div>
		</footer>
	</form>


<!-- MODAL SUPPRESSION PRODUIT -->
<div id="modalsuppr" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Supprimer un stagiaire</h4>
            </div>

            <div class="modal-body">
                Êtes-vous sûr de vouloir supprimer ce stagiaire de la session ?

            </div>

            <div class="modal-footer">
                <button type="button" id="delete_stagiaire" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</a>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
            </div>
        </div>

    </div>
</div>
<!-- FIN MODAL SUPPRESSION PRODUIT -->

<?php include "includes/footer_script.inc.php"; ?>  

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<!-- SCRIPT SELECT2 -->
<script src="vendor/plugins/select2/js/select2.min.js"></script>
	
	<script type="text/javascript">
	
		jQuery(document).ready(function (){
			
			// ouverture du modal de confirmation
			$(".btn-suppr").click(function(){
				sta_id = $(this).data("id");
				$("#delete_stagiaire").data("id", sta_id);
			});
			
			$(".btn-suppr").click(function(){
				 sta_id = $(this).data("id");
				del_stagiaire_session(sta_id); 
			}); 
		}); 
 
	   ////////////// FONCTIONS ///////////////

		function del_stagiaire_session(sta_id){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_del_stagiaire_session.php',                  
				data : 'sta_id=' + sta_id + "&session=<?= $session['ses_id'] ?>",          
				dataType: 'html',              
				success: function(data)       
				{
					console.log(data);
					$(".tr_stagiaire").each(function(key, value) {
						if($(this).data('id') == sta_id){
							$(this).remove();
						}
					});

					$("#modalsuppr").modal("hide");

				}
			});
		}
	</script>

</body>
</html>