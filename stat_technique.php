<?php
	
	// ECRAN D'ACCUEIL DES STATISTIQUES DE L'ONGLET TECHNIQUE

	$menu_actif = "4-4";
	include "includes/controle_acces.inc.php";
	include "modeles/mod_parametre.php";
	include "includes/connexion.php";
	include "includes/connexion_soc.php";

	
	// LE PERSONNE CONNECTE
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]); 
	}
	
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]); 
	}
	
	if($_SESSION["acces"]["acc_ref"]!=1 OR ($_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']["acc_profil"]!=11 AND $_SESSION['acces']["acc_profil"]!=14 AND $_SESSION['acces']["acc_profil"]!=15 AND $_SESSION['acces']["acc_profil"]!=10) ) {
		// accès au service tech, DAF, DG, RA et RE
		$erreur="Accès refusé!";
		echo($erreur);
		die();
	}
	
	// LES TESTEURS = INTERVENANTS

	$sql="SELECT int_id,int_label_1,int_label_2 FROM Intervenants";
	if(!empty($acc_agence)){
		$sql.=" WHERE (int_agence=" . $acc_agence . " OR int_agence=0)";
	}
	
	$sql.=" ORDER BY int_label_1,int_label_2";
	$req=$ConnSoc->query($sql);
	$d_intervenants=$req->fetchAll();

		

	
	// LES QUALIFICATIONS CACES
	
	$sql="SELECT qua_id,qua_libelle FROM Qualifications WHERE qua_caces=1 ORDER BY qua_libelle;";
	$req=$Conn->query($sql);
	$d_qualifications=$req->fetchAll();
	
		

	
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

	
	<!-- PERSO -->	
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	
	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
		<div id="main" >
<?php		
			if($_SESSION["acces"]["acc_ref"]==1){
				include "includes/header_def.inc.php"; 
			} ?>			
			<section id="content_wrapper" >					
			
				<section id="content" class="animated fadeIn" >
					<div class="row " >
					
						<h1 class="text-center" >Statistiques</h1>
					
						<div class="row" >

							<!-- STAT 1 : Test CACES -->

							<div class="col-md-3" >							
								<div class="panel mt10 panel-primary" > 
									<div class="panel-heading panel-head-sm" >
										<span class="panel-title">Tests CACES</span>										
									</div>
									<div class="panel-intro" >
										Affiche le nombre de tests CACES demandés,obtenus et ajournés.<br/>
										Si vous ne selectionnez pas de testeur, la stat prendra tous en compte (interne,interco,sous-traitant).
									</div>
									<div class="panel-body" >
										<form method="post" action="stat_tech_caces_test.php" >
											<div class="admin-form" >
												<div class="row mt15" >
													<div class="col-md-12" >
														<label for="caces_testeur" >Testeur</label>
														<select class="select2" id="caces_testeur_1" name="testeur" >
															<option value='0' >Testeur ...</option>
													<?php	foreach($d_intervenants as $int){
																echo("<option value='" . $int["int_id"] . "' >" . $int["int_label_1"] . " " . $int["int_label_2"] . "</option>");
															} ?>
													
														</select>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-12" >
														<label for="caces_qualification_1" >Qualification</label>
														<select class="select2" id="caces_qualification_1" name="qualification" >
															<option value='0' >Qualification ...</option>
													<?php	foreach($d_qualifications as $q){
																echo("<option value='" . $q["qua_id"] . "' >" . $q["qua_libelle"] . "</option>");
															} ?>
														</select>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-6" >
														<label for="caces_periode_deb" >Période du</label>
														<span  class="field prepend-icon">
															<input type="text" id="caces_periode_deb_1" name="periode_deb" class="gui-input datepicker" placeholder="Testé entre le" required />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
													</div>
													<div class="col-md-6" >
														<label for="caces_periode_fin" >au</label>
														<span  class="field prepend-icon">
															<input type="text" id="caces_periode_fin_1" name="periode_fin" class="gui-input datepicker" placeholder="et le" required />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
													</div>
												</div>

												<div class="row mt15" >
													<div class="col-md-12 text-center" >
														<button type="submit" class="btn btn-md btn-primary" >
															Lancer les calculs
														</button>
													</div>
												</div>
											</div>
											
										</form>
									</div>
								</div>
							</div>

							<!-- STAT 2 : NB JOUR FORM ET TEST -->

							<div class="col-md-3" >							
								<div class="panel mt10 panel-primary" > 
									<div class="panel-heading panel-head-sm" >
										<span class="panel-title">Bilan d'activité testeur</span>										
									</div>
									<div class="panel-intro" >
										Affiche le nombre de jours de formation et de test par formateur. La stat indique également quelles catégories ont été testées au moins un fois sur la période.
									</div>
									<div class="panel-body" >
										<form method="post" action="stat_tech_caces_jour.php" id="form_2" >
											<div class="admin-form" >												
												<div class="row mt15" >
													<div class="col-md-12" >
														<label for="caces_qualification_2" >Qualification</label>
														<select class="select2" id="caces_qualification_2" name="qualification" required >
															<option value='0' >Qualification ...</option>
													<?php	foreach($d_qualifications as $q){
																echo("<option value='" . $q["qua_id"] . "' >" . $q["qua_libelle"] . "</option>");
															} ?>
														</select>
														<small class="text-danger texte_required" id="caces_qualification_2_required" >Merci de selectionner une qualification</small>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-6" >
														<label for="caces_periode_deb" >Période du</label>
														<span  class="field prepend-icon">
															<input type="text" id="caces_periode_deb_2" name="periode_deb" class="gui-input datepicker" placeholder="Période du" required />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
														<small class="text-danger texte_required" id="caces_periode_deb_2_required" >Merci de selectionner une période</small>
													</div>
													<div class="col-md-6" >
														<label for="caces_periode_fin" >au</label>
														<span  class="field prepend-icon">
															<input type="text" id="caces_periode_fin_2" name="periode_fin" class="gui-input datepicker" placeholder="au" required />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
														<small class="text-danger texte_required" id="caces_periode_fin_2_required" >Merci de selectionner une période</small>
													</div>
												</div>

												<div class="row mt15" >
													<div class="col-md-12 text-center" >
														<button type="button" class="btn btn-md btn-primary" id="sub_form_2" >
															Lancer les calculs
														</button>
													</div>
												</div>
											</div>
											
										</form>
									</div>
								</div>
							</div>

							<!-- STAT 3 : NB JOUR FORM ET TEST -->
							<div class="col-md-3" >							
								<div class="panel mt10 panel-primary" > 
									<div class="panel-heading panel-head-sm" >
										<span class="panel-title">Chiffres d'affaires CACES</span>										
									</div>
									<div class="panel-intro" >
										Affiche la répartition du CA par qualification (formation/test et INTRA/INTER)
									</div>
									<div class="panel-body" >
										<form method="post" action="stat_tech_caces_ca.php" >
											<div class="admin-form" >																										
												<div class="row mt15" >
													<div class="col-md-6" >
														<label for="caces_periode_deb" >Période du</label>
														<span  class="field prepend-icon">
															<input type="text" id="caces_periode_deb_3" name="periode_deb" class="gui-input datepicker" placeholder="Période du" required />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
													</div>
													<div class="col-md-6" >
														<label for="caces_periode_fin" >au</label>
														<span  class="field prepend-icon">
															<input type="text" id="caces_periode_fin_3" name="periode_fin" class="gui-input datepicker" placeholder="au" required />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
													</div>
												</div>

												<div class="row mt15" >
													<div class="col-md-12 text-center" >
														<button type="submit" class="btn btn-md btn-primary" >
															Lancer les calculs
														</button>
													</div>
												</div>
											</div>
											
										</form>
									</div>
								</div>
							</div>	


							<!-- STAT 4 : Taux de test en CDT -->

							<div class="col-md-3" >							
								<div class="panel mt10 panel-primary" > 
									<div class="panel-heading panel-head-sm" >
										<span class="panel-title">Taux de test en CDT</span>										
									</div>
									<div class="panel-intro" >
										Affiche le CA et le nombre de tests réalisés en INTRA et en INTER pour chaque catégorie.
									</div>
									<div class="panel-body" >
										<form method="post" action="stat_tech_caces_cdt.php" id="form_4" >
											<div class="admin-form" >												
												<div class="row mt15" >
													<div class="col-md-12" >
														<label for="caces_qualification_4" >Qualification</label>
														<select class="select2" id="caces_qualification_4" name="qualification" required >
															<option value='0' >Qualification ...</option>
													<?php	foreach($d_qualifications as $q){
																echo("<option value='" . $q["qua_id"] . "' >" . $q["qua_libelle"] . "</option>");
															} ?>
														</select>
														<small class="text-danger texte_required" id="caces_qualification_4_required" >Merci de selectionner une qualification</small>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-6" >
														<label for="caces_periode_deb" >Période du</label>
														<span  class="field prepend-icon">
															<input type="text" id="caces_periode_deb_4" name="periode_deb" class="gui-input datepicker" placeholder="Période du" required />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
														<small class="text-danger texte_required" id="caces_periode_deb_4_required" >Merci de selectionner une période</small>
													</div>
													<div class="col-md-6" >
														<label for="caces_periode_fin" >au</label>
														<span  class="field prepend-icon">
															<input type="text" id="caces_periode_fin_4" name="periode_fin" class="gui-input datepicker" placeholder="au" required />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
														<small class="text-danger texte_required" id="caces_periode_fin_4_required" >Merci de selectionner une période</small>
													</div>
												</div>

												<div class="row mt15" >
													<div class="col-md-12 text-center" >
														<button type="button" class="btn btn-md btn-primary" id="sub_form_4" >
															Lancer les calculs
														</button>
													</div>
												</div>
											</div>
											
										</form>
									</div>
								</div>
							</div>			
						</div>

					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 text-center"></div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){	
				
				$("#sub_form_2").click(function(){
					if(valider_form("#form_2")){
						$("#form_2").submit();					
					}
				});
				$("#sub_form_4").click(function(){
					if(valider_form("#form_4")){
						$("#form_4").submit();					
					}
				});
				
		
			});
		</script>
	</body>
</html>
