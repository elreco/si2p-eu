<?php 
// ENREGISTREMENT D'UN DEVIS
	
include "includes/controle_acces.inc.php";
include_once("includes/connexion_soc.php");
include_once("includes/connexion.php");
include('modeles/mod_parametre.php');

// FIN DES CONTROLES

// ENREGISTREMENT
if(!empty($_POST['eva_id'])){
		$req=$Conn->query("SELECT * FROM Evacuations_Chronologies_Param ORDER BY ecp_position");
		$remarques=$req->fetchAll();

	foreach($remarques as $r){
		$req=$Conn->query("SELECT * FROM Evacuations_Chronologies WHERE ech_evacuation=" . $_POST['eva_id'] . " AND ech_chronologie = " . $r['ecp_id']);
		$chrono=$req->fetch();
		if(empty($chrono)){
			// INSERT
			$sql_edit="INSERT INTO Evacuations_Chronologies (ech_valeur, ech_evacuation, ech_chronologie) VALUES (:ech_valeur, :ech_evacuation, :ech_chronologie)";
		 	$req_edit=$Conn->prepare($sql_edit);
			$req_edit->bindValue(":ech_valeur", $_POST['ech_valeur'][$r['ecp_id']]);
			$req_edit->bindValue(":ech_evacuation", $_POST['eva_id']);
			$req_edit->bindValue(":ech_chronologie", $r['ecp_id']);
		}else{
			// UPDATE
			$sql_edit="UPDATE Evacuations_Chronologies SET 
			ech_valeur = :ech_valeur WHERE ech_evacuation=" . $_POST['eva_id'] . " AND ech_chronologie = " . $r['ecp_id'];
			 $req_edit=$Conn->prepare($sql_edit);
			$req_edit->bindValue(":ech_valeur", $_POST['ech_valeur'][$r['ecp_id']]);
		}
		

		$req_edit->execute();
	}

}
$_SESSION['message'][] = array(
	"titre" => "Succès",
	"type" => "success",
	"message" => "Votre chronologie a été enregistrée"
);
Header("Location: evac_voir.php?id=" . $_POST['eva_id']);
die();



