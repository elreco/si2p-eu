<?php

	// STAT VENTILATION CA CLIENT PAR FAMILLE


	include "includes/controle_acces.inc.php";
	include "modeles/mod_parametre.php";
	include "includes/connexion.php";
	include "includes/connexion_soc.php";


	// DONNEE FORM
	$_SESSION['retour'] = "stat_com_cli_pfam.php";
	$_SESSION["retourFacture"]="stat_com_cli_pfam.php";
	$_SESSION["retourClient"]="stat_com_cli_pfam.php";
	$_SESSION["retour_action"]="stat_com_cli_pfam.php";
	$erreur_txt="";
	if(!empty($_POST)){

		//print_r($_POST);

		$commercial=0;
		if(!empty($_POST["commercial"])){
			$commercial=intval($_POST["commercial"]);
		}

		$periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode)){
				$periode_deb=$DT_periode->format("Y-m-d");
			}
		}

		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode)){
				$periode_fin=$DT_periode->format("Y-m-d");
			}
		}

		$com_src=0;
		if(!empty($_POST["com_src"])){
			$com_src=intval($_POST["com_src"]);
		}

		if(empty($periode_deb) OR empty($periode_fin) OR empty($com_src)){
			$erreur_txt="Formulaire incomplet!";
		}
	}else{
		$erreur_txt="Formulaire incomplet!";

	}

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : stat_commercial.php");
		die();
	}


	// LE PERSONNE CONNECTE

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}


	// PARAMETRE VARIABLE STAT
	$param_stat=array(
		"titre" => "",
		"agence" => 0
	);


	// LE COMMERCIAL

	if($commercial>0){
		$sql="SELECT com_label_1,com_label_2,com_agence FROM Commerciaux WHERE com_id=" . $commercial . ";";
		$req=$ConnSoc->query($sql);
		$d_commercial=$req->fetch();
		if(empty($d_commercial)){

			$_SESSION['message'][] = array(
				"titre" => "Erreur",
				"type" => "danger",
				"message" => "Formulaire incomplet!"
			);
			header("location : stat_commercial.php");
			die();

		}else{
			$param_stat["titre"]=$d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"];
			$param_stat["agence"]=$d_commercial["com_agence"];
		}
	}elseif($acc_agence>0){

		$sql="SELECT age_nom FROM Agences WHERE age_id=" . $acc_agence . ";";
		$req=$Conn->query($sql);
		$d_agence=$req->fetch();
		if(empty($d_agence)){

			$_SESSION['message'][] = array(
				"titre" => "Erreur",
				"type" => "danger",
				"message" => "Formulaire incomplet!"
			);
			header("location : stat_commercial.php");
			die();
		}else{
			$param_stat["titre"]=$d_agence["age_nom"];
			$param_stat["agence"]=$acc_agence;
		}

	}else{

		$sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();
		if(empty($d_societe)){

			$_SESSION['message'][] = array(
				"titre" => "Erreur",
				"type" => "danger",
				"message" => "Formulaire incomplet!"
			);
			header("location : stat_commercial.php");
			die();
		}else{
			$param_stat["titre"]=$d_societe["soc_nom"];
			$param_stat["agence"]=0;
		}
	}


	// CATEGORIE DE CLIENTS

	$cli_categories=array();
	$sql="SELECT cca_id,cca_libelle FROM Clients_Categories ORDER BY cca_libelle;";
	$req=$Conn->query($sql);
	$d_cli_categories=$req->fetchAll();
	if(!empty($d_cli_categories)){
		foreach($d_cli_categories as $categorie){
			$cli_categories[$categorie["cca_id"]]=$categorie["cca_libelle"];
		}
	}



	// SOUS-CATEGORIE DE CLIENTS

	$cli_s_categories=array();
	$sql="SELECT csc_id,csc_libelle FROM Clients_Sous_Categories ORDER BY csc_libelle;";
	$req=$Conn->query($sql);
	$d_cli_s_categories=$req->fetchAll();
	if(!empty($d_cli_s_categories)){
		foreach($d_cli_s_categories as $s_categorie){
			$cli_s_categories[$s_categorie["csc_id"]]=$s_categorie["csc_libelle"];
		}
	}

	// CONSTRUCTION DES DONNEES AXE X

	$num_x=0;
	$cle_x=array();
	$lib_x=array();

	// LES CATEGORIES DE PRODUITS

	$sql="SELECT pca_id,pca_libelle FROM Produits_Categories WHERE (pca_revente OR pca_sous_traitance);";
	$req=$Conn->query($sql);
	$d_categorie=$req->fetchAll();
	if(!empty($d_categorie)){
		foreach($d_categorie as $cat){
			$num_x++;
			$lib_x[$num_x]=array(
				"libelle" => $cat["pca_libelle"],
				"ca" => 0,
				"total" => 0
			);

			$cle_x[$cat["pca_id"]][0][0][0]=$num_x;

		}
	}

	// LES FAMILLES

	$sql="SELECT pfa_id,pfa_libelle,psf_id,psf_libelle,pss_id,pss_libelle
	FROM Produits_Familles LEFT JOIN Produits_Sous_Familles ON (Produits_Familles.pfa_id=Produits_Sous_Familles.psf_pfa_id)
	LEFT JOIN Produits_Sous_Sous_Familles ON (Produits_Sous_Familles.psf_id=Produits_Sous_Sous_Familles.pss_psf_id)
	ORDER BY pfa_libelle,psf_libelle,pss_libelle;";
	$req=$Conn->query($sql);
	$d_familles=$req->fetchAll();
	/*echo("<pre>");
		print_r($d_familles);
	echo("</pre>");*/
	if(!empty($d_familles)){
		foreach($d_familles as $fam){

			$fam_id=$fam["pfa_id"];

			$s_fam_id=0;
			if(!empty($fam["psf_id"])){
				$s_fam_id=$fam["psf_id"];
			}
			$s_s_fam_id=0;
			if(!empty($fam["pss_id"])){
				$s_s_fam_id=$fam["pss_id"];
			}


			$lib="";
			if(!empty($s_s_fam_id)){
				$lib=$fam["pss_libelle"];
			}elseif(!empty($s_fam_id)){
				$lib=$fam["psf_libelle"];
			}else{
				$lib=$fam["pfa_libelle"];
			};

			/*echo($fam_id . "<br/>");
			echo($s_fam_id . "<br/>");
			echo($s_s_fam_id . "<br/>");
			echo($lib . "<br/>");
			echo("<br/>");*/

			$num_x++;
			$lib_x[$num_x]=array(
				"libelle" => $lib,
				"ca" => 0,
				"total" => 0
			);

			/*if(!isset($cle_x[1][$fam_id])){
				$cle_x[1][$fam_id]=array();
			}
			if(!isset($cle_x[1][$fam_id][$s_fam_id])){
				$cle_x[1][$fam_id][$s_fam_id]=array();
			}*/

			$cle_x[1][$fam_id][$s_fam_id][$s_s_fam_id]=$num_x;

		}
	}

	/*echo("<pre>");
		print_r($cle_x);
	echo("</pre>");
	die();*/

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css">

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#zone_print{
				display:none;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 landscape;
				margin:5mm;
			}
			html{
				background-color:#fff!important
			}
			body{
				background-color:#fff!important
			}
			#zone_print{
				background-color:#fff!important;
				font-size:8pt;
			}
		</style>
	</head>

	<body class="sb-top sb-top-sm no-scroll" >

		<div id="zone_print" ></div>

		<div id="main" >
<?php
			if($_SESSION["acces"]["acc_ref"]==3){
				include "includes/header_sta.inc.php";
			}elseif($_SESSION["acces"]["acc_ref"]==2){
				include "includes/header_cli.inc.php";
			}else{
				include "includes/header_def.inc.php";
			} ?>
			<section id="content_wrapper" >
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn" style="overflow:scroll;" >

					<div id="page_print" >
						<h1 class="text-center" >
					<?php	if($com_src==1){
								echo("CA des clients de " . $param_stat["titre"]);
							}else{
								echo("CA facturé par " . $param_stat["titre"]);
							}
							echo("<br/>Du " . $_POST["periode_deb"] . " au " . $_POST["periode_fin"]);
							?>
						</h1>

							<div class="table-responsive">
								<table class="table table-striped table-hover" >
									<thead>
										<tr class="dark">
											<th>ID client</th>
											<th>Code client</th>
											<th>Nom client</th>
											<th>Département</th>
											<th colspan="2" >Catégorie</th>
								<?php		$nb_cols=6;
											if(empty($commercial)){
												$nb_cols++; ?>
												<th>Commercial</th>
								<?php		}
											foreach($lib_x as $lib){ ?>
												<th><?=$lib["libelle"]?></th>
								<?php		} ?>
											<th>Total</th>
										</tr>
									</thead>
					<?php
									$sql="SELECT SUM(fli_montant_ht) AS montant_ht,fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille
									,cli_id,cli_code,cli_nom,cli_cp,cli_categorie,cli_sous_categorie,fac_client
									,com_label_1,com_label_2
									FROM Factures_Lignes INNER JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)";

									$mil="";
									if($com_src==1){
										// commercial du client
										// on ne doit compabiliser que les factures émise par l'agence
										$sql.=" INNER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)";
										if($commercial>0){
											$mil.=" AND cli_commercial=" . $commercial;
										}elseif($acc_agence>0){
											$mil.=" AND cli_agence=" . $acc_agence;
										}

									}else{
										// commercial de la facture
										// jointure avec agence pour éviter que le CA soit comptabilisé autant de fois qu'il y a d'agence
										$sql.=" LEFT OUTER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)";
										if($commercial>0){
											$mil.=" AND fac_commercial=" . $commercial;
										}elseif($acc_agence>0){
											$mil.=" AND fac_agence=" . $acc_agence;
										}
									}

									$sql.=" LEFT JOIN Commerciaux ON (Clients.cli_commercial=Commerciaux.com_id)";

									$mil.=" AND fac_date>='". $periode_deb . "' AND fac_date<='" . $periode_fin . "'";
									if($mil!=""){
										$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
									}



									$sql.=" GROUP BY fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,cli_id,cli_code,cli_nom,cli_cp,cli_categorie,cli_sous_categorie,fac_client
									,com_label_1,com_label_2";
									$sql.=" ORDER BY cli_code,cli_nom;";
									//echo($sql);
									$req=$ConnSoc->query($sql);
									$d_stats=$req->fetchAll();
									/*echo("<pre>");
										print_r($d_stats);
									echo("</pre>");*/

									/*echo("<pre>");
										print_r($cle_x);
									echo("</pre>");*/

									if(!empty($d_stats)){ ?>
										<tbody>
							<?php			$cli_id=0;
											$total_cli=0;
											foreach($d_stats as $stat){
												if(empty($stat["cli_id"])){
													echo($stat["fac_client"] . ",");
												}

												if($cli_id!==$stat["cli_id"]){
													if($cli_id!==0){ ?>
														<tr>
															<td><?=$cli_id?></td>
															<td>
															<a href="client_voir.php?client=<?= $cli_id ?>">
																<?=$cli_code?>
															</a>
															</td>
															<td><?=$cli_nom?></td>
															<td><?=$cli_cp?></td>
															<td><?=$cat_lib?></td>
															<td><?=$s_cat_lib?></td>
												<?php		if(empty($commercial)){ ?>
																<td><?=$com_identite?></td>
												<?php		}
															foreach($lib_x as $cle_lib => $lib){ ?>
																<td class="text-right" ><?=number_format($lib["ca"],0,",","")?></td>
												<?php			$lib_x[$cle_lib]["ca"]=0;
															} ?>
															<td class="text-right" ><?=number_format($total_cli,0,",","")?></td>
														</tr>
							<?php					}
													$cli_id=$stat["cli_id"];
													$cli_code=$stat["cli_code"];
													$cli_nom=$stat["cli_nom"];
													$com_identite=$stat["com_label_2"] . " " . $stat["com_label_1"];
													$cli_cp="";
													if(!empty($stat["cli_cp"])){
														$cli_cp=substr($stat["cli_cp"],0,2);
													}

													$cat_lib="";
													if(!empty($stat["cli_categorie"])){
														$cat_lib=$cli_categories[$stat["cli_categorie"]];
													};
													$s_cat_lib="";
													if(!empty($stat["cli_sous_categorie"])){
														$s_cat_lib=$cli_s_categories[$stat["cli_sous_categorie"]];
													};
													$total_cli=0;
												}


												$num_x=0;

												$p_cat=0;
												if(!empty($stat["fli_categorie"])){
													$p_cat=$stat["fli_categorie"];
												};

												$p_fam=0;
												if(!empty($stat["fli_famille"])){
													$p_fam=$stat["fli_famille"];
												};

												$p_s_fam=0;
												if(!empty($stat["fli_sous_famille"])){
													$p_s_fam=$stat["fli_sous_famille"];
												};

												$p_s_s_fam=0;
												if(!empty($stat["fli_sous_sous_famille"])){
													$p_s_s_fam=$stat["fli_sous_sous_famille"];
												};

												if($stat["fac_client"]==24229){
													/*var_dump($stat[0]);
													var_dump($cle_x[$p_cat][$p_fam][$p_s_fam][$p_s_s_fam]);*/

												}



												if(!empty($cle_x[$p_cat][$p_fam][$p_s_fam][$p_s_s_fam])){

													$num_x=$cle_x[$p_cat][$p_fam][$p_s_fam][$p_s_s_fam];



													$lib_x[$num_x]["ca"]=$stat["montant_ht"];
													$lib_x[$num_x]["total"]=$lib_x[$num_x]["total"]+$stat["montant_ht"];

													$total_cli=$total_cli+$stat["montant_ht"];
												}


											} ?>
											<tr>
												<td><?=$cli_id?></td>
												<td><?=$cli_code?></td>
												<td><?=$cli_nom?></td>
												<td><?=$cli_cp?></td>
												<td><?=$cat_lib?></td>
												<td><?=$s_cat_lib?></td>
									<?php		if(empty($commercial)){ ?>
													<td><?=$com_identite?></td>
									<?php		}
												foreach($lib_x as $lib){ ?>
													<td class="text-right" ><?=number_format($lib["ca"],0,",","")?></td>
									<?php		} ?>
												<td class="text-right" ><?=number_format($total_cli,0,",","")?></td>
											</tr>
											<tr>
												<th colspan="<?=$nb_cols?>" >Total :</th>
									<?php		$total_cli=0;
												foreach($lib_x as $lib){
													$total_cli+=$lib["total"]; ?>
													<th class="text-right" ><?=number_format($lib["total"],0,",","")?></th>
									<?php		} ?>
												<th class="text-right" ><?=number_format($total_cli,0,",","")?></th>
											</tr>
										</tbody>
							<?php	} ?>
								</table>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="stat_commercial.php" class="btn btn-sm btn-default"  >
							<i class="fa fa-left-arrow" ></i>Retour
						<a>
						<!--<button type="button" class="btn btn-sm btn-info ml15 btn-print" >
							<i class="fa fa-print"></i> Imprimer
						</button>-->
					</div>
					<div class="col-xs-6 footer-middle text-center" style=""></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>

<?php	include "includes/footer_script.inc.php"; ?>

		<script src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script src="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function () {

				/*

				var calcDataTableHeight = function (elt_content, elt_head) {
					return $(elt_content).height() - $(elt_head).height() - 6;
				};
				var tableDefFix = $('#tableFix').dataTable({
					"language": {
						"url": "/vendor/plugins/DataTables/media/js/French.json"
					},
					paging: false,
					searching: false,

					info: false,
					scrollY: calcDataTableHeight("#tableCont", "#tableHead"),
					scrollCollapse: true,
					order: [[1, "asc"], [2, "asc"]],
					columnDefs: [
						{ targets: 'no-sort', orderable: false }
					]
				});
				$(window).resize(function () {
					var tableDefFixParam = tableDefFix.fnSettings();
					tableDefFixParam.oScroll.sY = calcDataTableHeight("#tableCont", "#tableHead");
					tableDefFix.fnDraw();
				});
				$(window).load(function () {
					setTimeout(function () {
						$(".dataTables_scrollBody").mCustomScrollbar({
							theme: "dark"
						});
					}, 100);
				});
		*/

			});
			(jQuery);
		</script>
	</body>
</html>
