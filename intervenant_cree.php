<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");

include 'modeles/mod_parametre.php';

// AJOUT D'UN INTERVENANT (ligne de planning) et données associés

	// PERSONNE CONNECTE
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];	
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref_id"])){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];	
	}
	
	// SUR LA Societes
	
	if($acc_agence==0){
		$sql="SELECT soc_agence FROM Societes WHERE soc_id=:acc_societe;";
		$req=$Conn->prepare($sql);
		$req->bindParam("acc_societe",$acc_societe);
		$req->execute();
		$societe=$req->fetch();
		if(!empty($societe)){
			if($societe["soc_agence"]){ 
				// gestion d'agence on liste celles auquelle à accès l'uti
				
				$sql="SELECT age_id,age_nom FROM Agences,Utilisateurs_Societes WHERE age_id=uso_agence AND uso_utilisateur=:acc_utilisateur AND uso_societe=:acc_societe";
				$sql.=" ORDER BY age_nom";
				$req=$Conn->prepare($sql);
				$req->bindParam("acc_societe",$acc_societe);
				$req->bindParam("acc_utilisateur",$acc_utilisateur);
				$req->execute();
				$acc_agence_liste=$req->fetchAll();
			}
		}
	}
			
	

	// LES FORMATEUR INTERNE DISPO

	//echo("uti_societe : " . $acc_societe . "<br/>");
	//echo("acc_agence : " . $acc_agence . "<br/>");
	
	$sql="SELECT uti_id,uti_nom,uti_prenom FROM Utilisateurs WHERE uti_societe=:acc_societe AND NOT uti_archive";
	if($acc_agence>0){
		$sql.=" AND uti_agence=:acc_agence";
	}
	$sql.=" ORDER BY uti_nom,uti_prenom;";
	$req=$Conn->prepare($sql);
	$req->bindParam("acc_societe",$acc_societe);
	if($acc_agence>0){
		$req->bindParam("acc_agence",$acc_agence);
	}
	$req->execute();
	$utilisateurs=$req->fetchAll();
	
	//echo("<pre>");
	//	var_dump($utilisateurs);
	//echo("</pre>");
	//die();
	// on supprime ceux deja inscrit

	if(!empty($utilisateurs)){
		$sql="SELECT int_ref_1 FROM Intervenants WHERE int_type=1";
		$req=$ConnSoc->query($sql);
		$req->execute();
		$uti_deja_inscrit=$req->fetchAll();
		if(!empty($uti_deja_inscrit)){
			foreach($uti_deja_inscrit as $u){
				$cle=array_search($u["int_ref_1"],array_column($utilisateurs,"uti_id"),true);
				if(!is_bool($cle)){
					array_splice($utilisateurs, $cle, 1);
				};
			}
		}
	}
	//die();

	// LES INTERCO

	$sql="SELECT soc_id,soc_code FROM Societes WHERE (NOT soc_id=:acc_societe OR soc_agence) AND NOT soc_archive";
	$req = $Conn->prepare($sql);
	$req->bindParam(":acc_societe",$acc_societe);
	$req->execute();
	$intercos=$req->fetchAll();
	
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<form method="post" action="intervenant_cree_enr.php" id="form_int" >
			<!-- Start: Main -->
			<div id="main">
				<?php
					include "includes/header_def.inc.php";
				?>
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper">


					<!-- Begin: Content -->
					<section id="content" class="animated fadeIn">		

						<div class="panel" > 
							<div class="panel-heading panel-head-sm" >
								<span class="panel-title">Type d'intervenant</span>
							</div>
							<div class="panel-body">
								<div class="admin-form" >
									<div class="row" >
										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="int_type" value="1" checked >
												<span class="radio"></span>Formateur interne
											</label>
										</div>
										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="int_type" value="2" >
												<span class="radio"></span>Interco
											</label>
										</div>
										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="int_type" value="3" >
												<span class="radio"></span>Sous-Traitant
											</label>
										</div>
										<div class="col-md-3" >
											<label class="option">
												<input type="radio" name="int_type" value="0" >
												<span class="radio"></span>Autre
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--FORMATEUR INTERNE -->	
						<div class="panel" id="form_interne" > 
							<div class="panel-heading" >
								<span class="panel-title">Formateur interne</span>
							</div>
							<div class="panel-body">
							
								<div class="admin-form" >
							
									<div class="row" >
										<div class="col-md-5" >
											<select id="utilisateur" name="utilisateur" placeholder="Compte utilisateur existants" >
												<option value="0" >&nbsp;</option>
									<?php		if(!empty($utilisateurs)){
													foreach($utilisateurs as $u){
														echo("<option value='" . $u["uti_id"] . "' >" . $u["uti_nom"] ." " . $u["uti_prenom"] . "</option>");											
													}
												} ?>
											</select>
										</div>
										<div class="col-md-2 text-center" >
											OU
										</div>
										<div class="col-md-5" >
											<div class="option-group field">
												<label class="option">
													<input type="checkbox" id="utilisateur_add" name="utilisateur_add" value="on" />	
													Créer un nouveau compte utilisateur : 													
													<span class="checkbox"></span>													
												</label>
											</div>									
										</div>
										
									</div>
									<!--utilisateur_add_form -->
									<div id="utilisateur_add_form" style="display:none" >
									
									
										<div class="row" >
											<div class="col-md-12" >
												<div class="section-divider mb40" >
													<span>Identité</span>
												</div>
											</div>
										</div>
											
							<?php		if(isset($acc_agence_liste)){
											if(!empty($acc_agence_liste)){ ?>
												<div class="row" >
													<div class="col-md-4" >
														<div class="section">
															<label class="field select">
																<select name="uti_agence" id="uti_agence" >
																	<option value="">Agence...</option>
														<?php		foreach($acc_agence_liste as $a){
																		echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																	};?>																																	
																</select>
																<i class="arrow simple"></i>
															</label>
														</div>
													</div>
												</div>
							<?php			}
										} ?>
										<div class="row" >			
											<div class="col-md-4" >
												<div class="section">
													<label class="field select">
														<select name="uti_titre" id="uti_titre" >
															<option value="">Civilité...</option>
												<?php		foreach($base_civilite as $cle => $valeur){
																if($cle>0){
																	echo("<option value='" . $cle . "' >" . $valeur . "</option>");
																};																
															}; ?>																																	
														</select>
														<i class="arrow simple"></i>
													</label>
												</div>
											</div>
											<div class="col-md-4">
												<div class="section">
													<div class="field prepend-icon">
														<input type="text" name="uti_nom" id="uti_nom" class="gui-input nom" placeholder="Nom" >
														<label for="uti_nom" class="field-icon">
															<i class="fa fa-user"></i>
														</label>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="section">
													<div class="field prepend-icon">
														<input type="text" name="uti_prenom" id="uti_prenom" class="gui-input prenom" placeholder="Prénom" >
														<label for="uti_prenom" class="field-icon">
															<i class="fa fa-user"></i>
														</label>
													</div>
												</div>
											</div>
											
										</div>
										
										<div class="row">
										
											<div class="col-md-8" >
												<div class="section-divider mb40" >
													<span>Adresse</span>
												</div>
												<div class="col-md-12">
													<div class="section">
														<div class="field prepend-icon">
															<input type="text" name="uti_ad1" id="uti_ad1" class="gui-input" placeholder="Adresse" >
															<label for="uti_ad1" class="field-icon">
																<i class="fa fa-map-marker"></i>
															</label>
														</div>
													</div>
													<div class="section">
														<div class="field prepend-icon">
															<input type="text" name="uti_ad2" class="gui-input" id="uti_ad2" placeholder="Adresse (Complément 1)">
															<label for="uti_ad2" class="field-icon">
																<i class="fa fa-map-marker"></i>
															</label>
														</div>
													</div>
													<div class="section">
														<div class="field prepend-icon">
															<input type="text" name="uti_ad3" id="uti_ad3" class="gui-input" placeholder="Adresse (Complément 2)">
															<label for="uti_ad3" class="field-icon">
																<i class="fa fa-map-marker"></i>
															</label>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="section">
														<div class="field prepend-icon">
															<input type="text" id="uti_cp" name="uti_cp" class="gui-input code-postal" placeholder="Code postal" >
															<label for="uti_cp" class="field-icon">
																<i class="fa fa fa-certificate"></i>
															</label>
														</div>
													</div>
												</div>
												<div class="col-md-9">
													<div class="section">
														<div class="field prepend-icon">
															<input type="text" name="uti_ville" id="uti_ville" class="gui-input nom" placeholder="Ville" >
															<label for="uti_ville" class="field-icon">
																<i class="fa fa fa-building"></i>
															</label>
														</div>
													</div>
												</div>
											</div>
											<!--fin bloc adresse -->
											<!--bloc contact -->
											<div class="col-md-4" >
												<div class="section-divider mb40" >
													<span>Contact Pro.</span>
												</div>
												<div class="section">
													<div class="field prepend-icon">
														<input type="tel" name="uti_tel" id="uti_tel" class="gui-input telephone" placeholder="Numéro de téléphone" >
														<label for="uti_tel" class="field-icon">
															<i class="fa fa fa-phone"></i>
														</label>
													</div>
												</div>
												<div class="section">
													<div class="field prepend-icon">
														<input name="uti_fax" id="uti_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax" >
														<label for="uti_fax" class="field-icon">
															<i class="fa fa-fax"></i>
														</label>
													</div>
												</div>
												<div class="section">
													<div class="field prepend-icon">
														<input type="tel" name="uti_mobile" id="uti_mobile" class="gui-input telephone" placeholder="Numéro de mobile" >
														<label for="uti_mobile" class="field-icon">
															<i class="fa fa-mobile"></i>
														</label>
													</div>
												</div>
												<div class="section">
													<div class="field prepend-icon">
														<input type="email" name="uti_mail" id="uti_mail" class="gui-input" placeholder="Adresse Email" >
														<label for="uti_mail" class="field-icon">
															<i class="fa fa-envelope"></i>
														</label>
													</div>
												</div>
											</div>
											<!--fin bloc contact -->
										</div>								
									</div>
									<!--FIN DE utilisateur_add_form -->
								</div>
								
							</div>
						</div>
						<!--FIN DE FORMATEUR INTERNE -->
						
						
						<!--INTERCO -->	
						<div class="panel" id="form_interco" style="display:none;" > 
							<div class="panel-heading" >
								<span class="panel-title">Interco</span>
							</div>
							<div class="panel-body">
							
								<div class="admin-form" >
							
									<div class="row" >
										<div class="col-md-4">
											<div class="field select">
												<select id="societe" name="societe" >
													<option value="">Sélectionner la société...</option>
										<?php		if(!empty($intercos)){
														foreach($intercos as $i){
															echo("<option value='" . $i["soc_id"] . "' >" . $i["soc_code"] . "</option>");
														}										
													} ?>
												</select>
												<i class="arrow simple"></i>
											</div>											
										</div>                                           
										<div class="col-md-4">										
											<div class="field select" id="cont_agence" >
												<select id="agence" name="agence" >
													<option value="">Sélectionner une agence...</option>
												</select>
												<i class="arrow simple"></i>
											</div>										
										</div>								
										<div class="col-md-4">
											<input type="text" name="interco_label" class="gui-input" id="interco_label" placeholder="Libellé">											
										</div>																	
									</div>									
								</div>
								
							</div>
						</div>
						<!--FIN DE INTERCO -->
						
						<!--AUTRE -->	
						<div class="panel" id="form_autre" style="display:none;" > 
							<div class="panel-heading" >
								<span class="panel-title">Autre</span>
							</div>
							<div class="panel-body">
							
								<div class="admin-form" >
							
									<div class="row" >
										<div class="col-md-6">
											<input type="text" name="autre_label" class="gui-input" id="autre_label" placeholder="Libellé">														
										</div>                                           
															
									</div>									
								</div>
								
							</div>
						</div>
						<!--FIN DE AUTRE -->
						
						
						<!--BLOC INSCRIPTION -->	
						<div class="panel" id="form_interne" > 
							<div class="panel-heading" >
								<span class="panel-title">Inscription</span>
							</div>
							<div class="panel-body">
								<div class="admin-form" >
									<div class="row" >
										<div class="col-md-6">
											Du							
											<div class="field prepend-icon">
												<input type="text" id="periode_deb" name="periode_deb" class="gui-input" placeholder="Selectionner une date" value="<?php if(isset($_SESSION["pla_periode_deb"])) echo($_SESSION["pla_periode_deb"]);?>" required />						
												<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
											</div>									
										</div>
										<div class="col-md-6">
											Au
											<div class="field prepend-icon">
												<input type="text" id="periode_fin" name="periode_fin" class="gui-input" placeholder="Selectionner une date" value="<?php if(isset($_SESSION["pla_periode_fin"])) echo($_SESSION["pla_periode_fin"]);?>" required />
												<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
											</div>
										</div>						
									</div>
								</div>
							</div>
						</div>					
					
					</section>
					<!-- End: Content -->
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" ></div>
					<div class="col-xs-6 footer-middle" >&nbsp;</div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm" >
							<span class="fa fa-save"></span>
							Enregistrer
						</a>
					</div>
				</div>
			</footer>
		</form>
		<?php
		include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>
		<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script type="text/javascript" >
			jQuery(document).ready(function(){	
			
				// CHOIX DU BLOC A AFFICHER
				$('input[name=int_type]').click(function(){	
					switch($('input[name=int_type]:checked').val()) {
						case "1":
							$('#form_interne').show();
							$('#form_interco').hide();
							$('#form_autre').hide();
							break;
						case "2":
							$('#form_interne').hide();   
							$('#form_interco').show();
							$('#form_autre').hide();
							break;
						case "3":
							$('#form_interne').hide(); 
							$('#form_interco').hide();
							$('#form_autre').hide();
							break;
						default:
							$('#form_interne').hide();    
							$('#form_interco').hide();
							$('#form_autre').show();							
					}
				});

				// BLOC FORMATEUR INTERNE
				$("#utilisateur").select2();
				$("#utilisateur").change(function(){
					if($(this).val()>0){
						$("#utilisateur_add").prop("checked",false);
						$("#utilisateur_add_form").hide();									
					}
				});
				$("#utilisateur_add").click(function(){				
					if($(this).is(":checked")){
						$("#utilisateur").select2("val", "");
						$("#utilisateur_add_form").show();							
					}else{
						$("#utilisateur_add_form").hide();			
					}
				});
			
				//BLOC INTERCO
				$("#cont_agence").hide();
				$("#societe").change(function(){
					if($(this).val()!=""){
						$("#interco_label").val($("#societe option:selected").text());
					}else{
						$("#interco_label").val("");
					}
					actu_select_agence(this,"agence");
				});
				$("#agence").change(function(){	
					if($(this).val()!=""){
						$("#interco_label").val($("#agence option:selected").text());
					}else{
						$("#interco_label").val("");
					}							
				});
				
				function actu_select_agence(liste_societe,cible){
					$.ajax({
						type:'POST',
						url: '/ajax/ajax_agence.php',
						data : 'field=' + liste_societe.value + "&interco=1",
						dataType: 'json',                
						success: function(data)
						{	
							$('#' + cible).find("option:gt(0)").remove();
							if (data['0'] == undefined){
								$("#cont_agence").hide();
							}else{
								$("#cont_agence").show();
								$.each(data, function(key, value){
									$('#' + cible)
									.append($("<option></option>")
									.attr("value",key)
									.text(value["age_code"]));
								});
							}
						}
					});
				 }
				
				//PERIODE d'INSCRIPTION
				$("#periode_deb").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=1) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},
					onSelect: function (input, inst){
						$("#periode_mois").val("");
						var date_fin=$('#periode_deb').datepicker( "getDate" );
						date_fin.setDate(date_fin.getDate() + 5);
						$("#periode_fin").datepicker( "setDate",date_fin);
						$("#periode_fin").datepicker( "option", "minDate", date_fin);
						
					}
				});
				$("#periode_fin").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}		
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=6) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},
					
				});

				// VALIDATION DU FORMULAIRE
				$("#form_int").submit(function(){
					switch($('input[name=int_type]:checked').val()) {
						case "1":	
							
							if($("#utilisateur_add").is(":checked")){
								$("#uti_nom").rules("add", {
									required: true
								});	
								$("#uti_prenom").rules("add", {
									required: true
								});
								if($("#uti_agence").size()==1){
									$("#uti_agence").rules("add", {
										required: true
									});
								};
								
							}else{
								if($("#utilisateur").select2("val")==0){
									return false;
								};							
							}
						break;
						
						case "2":	
							$("#societe").rules("add", {
								required: true
							});
							$("#interco_label").rules("add", {
								required: true
							});
							if($("#cont_agence").is(":visible")){									
								$("#agence").rules("add", {
									required: true
								});
							}
						break;
						
						default:			
							$("#autre_label").rules("add", {
								required: true
							});	
								
						break;
					}
					
				});
				$("#form_int").validate();
			});
		</script>
	</body>
</html>