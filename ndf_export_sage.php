<?php
////////////////// MENU ACTIF HEADER ///////////////////

////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
if (isset($_GET['menu_retour'])) {
	unset($_SESSION['retour']);
}
///////////////////// VARIABLES ////////////////////////////
$mois = array(
	1 => "Janvier",
	2 => "Février",
	3 => "Mars",
	4 => "Avril",
	5 => "Mai",
	6 => "Juin",
	7 => "Juillet",
	8 => "Août",
	9 => "Septembre",
	10 => "Octobre",
	11 => "Novembre",
	12 => "Décembre"
);

$annees = array(
	1 => date('Y', strtotime(' -5 years')),
	2 => date('Y', strtotime(' -4 years')),
	3 => date('Y', strtotime(' -3 years')),
	4 => date('Y', strtotime(' -2 years')),
	5 => date('Y', strtotime(' -1 years')),
	6 => date('Y'),
);

$req = $Conn->prepare("SELECT * FROM comptes");
$req->execute();
$_comptes = $req->fetchAll();

function initModele()
{
	return [
		'codeJournal' => 'ACH',
		'dateEcriture' => '',
		'typePiece' => mb_str_pad(' ', 2),
		'numCompteGeneral' => '',
		'typeCompte' => '',
		'compteAuxilaire' => '',
		'referenceEcriture' => '',
		'libelleEcriture' => '',
		'typePaiement' => 'V',
		'dateEcheance' => mb_str_pad(' ', 6),
		'debitCredit' => '',
		'montant' => '',
		'typeEcriture' => 'N',
		'numeroPiece' => mb_str_pad(' ', 7),
		'zoneReserve' => mb_str_pad(' ', 26),
		'devise' => 'EUR',
		'planAnalytique' => '',
		'sectionAnalytique' => mb_str_pad(' ', 25)
	];
}

function writeLine($modele)
{
	$line = '';
	$line .= $modele['codeJournal'];
	$line .= $modele['dateEcriture'];
	$line .= $modele['typePiece'];
	$line .= $modele['numCompteGeneral'];
	$line .= $modele['typeCompte'];
	$line .= $modele['compteAuxilaire'];
	$line .= $modele['referenceEcriture'];
	$line .= $modele['libelleEcriture'];
	$line .= $modele['typePaiement'];
	$line .= $modele['dateEcheance'];
	$line .= $modele['debitCredit'];
	$line .= $modele['montant'];
	$line .= $modele['typeEcriture'];
	$line .= $modele['numeroPiece'];
	$line .= $modele['zoneReserve'];
	$line .= $modele['devise'];
	$line .= $modele['montant'];
	$line .= $modele['devise'];
	$line .= $modele['planAnalytique'];
	$line .= $modele['sectionAnalytique'];
	$line .= '<br>';
	return $line;
}
/////////////////// FIN VARIABLES //////////////////////////
///////////////////// TRAITEMENTS PHP ///////////////////////
if (!empty($_POST)) {


	$ndf_mois = $_POST['ndf_mois'];
	$ndf_annee = $_POST['ndf_annee'];
	$ndf_periode = $_POST['ndf_periode'];

	$sql = "SELECT * FROM ndf
	LEFT JOIN utilisateurs ON utilisateurs.uti_id = ndf.ndf_utilisateur
	LEFT JOIN societes ON utilisateurs.uti_societe = societes.soc_id
	LEFT JOIN agences ON agences.age_id = utilisateurs.uti_agence
	WHERE ndf_mois = " . $_POST['ndf_mois'] . " AND ndf_annee = " . $_POST['ndf_annee'] . "
	AND ndf_statut = 6 AND ndf_societe = " . $_SESSION['acces']['acc_societe'] . " AND ndf_agence = " . $_SESSION['acces']['acc_agence'];

	if (empty($ndf_periode)) {
		$sql .= " AND DAY(ndf_quinzaine_fin) = 15";
	} else {
		$sql .= " AND (DAY(ndf_quinzaine_deb) = 16 OR DAY(ndf_quinzaine_fin) != 15)";
	}

	$req = $Conn->prepare($sql);
	$req->execute();
	$ndfs = $req->fetchAll();
	/* var_dump($sql);
	die(); */
	// DEROULAGE DES NDF
	$comptes = [];
	$comptes_total = 0;
	$result = '';
	$erreur = '';

	foreach ($ndfs as $ndf) {
		$modele = initModele();
		$modele['dateEcriture'] = convert_date_ndf($ndf['ndf_quinzaine_fin']);
		$modele['referenceEcriture'] = mb_str_pad("NDF-" . mb_str_pad($ndf['ndf_id'], 6, 0, STR_PAD_LEFT), 13);
		$modele['libelleEcriture'] = mb_str_pad(mb_substr("NDF " . $ndf['uti_nom'] . " " . $ndf['uti_prenom'] . " " . $mois[$ndf['ndf_mois']] . " " . $ndf['ndf_annee'], 0, 25), 25);

		$req = $Conn->prepare("SELECT SUM(nli_ttc-nli_depassement_valide-nli_montant_tva) as nli_ht FROM ndf_lignes WHERE nli_refus = 0 AND nli_ndf = " . $ndf['ndf_id']);
		$req->execute();
		$ndf_lignes_ht = $req->fetch();

		$total_ht = $ndf_lignes_ht['nli_ht'];

		$req = $Conn->prepare("SELECT * FROM ndf_lignes WHERE nli_refus = 0 AND nli_ndf = " . $ndf['ndf_id']);
		$req->execute();
		$ndf_lignes = $req->fetchAll();

		$total_tva = 0;
		foreach ($ndf_lignes as $ligne) {
			$montant_ttc = $ligne['nli_ttc'] - $ligne['nli_depassement_valide'];
			$montant_ht = $ligne['nli_ttc'] - $ligne['nli_montant_tva'];
			$total_tva = $total_tva + ($montant_ttc - $montant_ht);

			$req = $Conn->prepare("SELECT nca_compte FROM ndf_categories WHERE nca_id = " . $ligne['nli_categorie']);
			$req->execute();
			$nca_compte = $req->fetch();

			foreach ($_comptes as $_compte) {
				if ($_compte['cpt_id'] == $ligne['nli_compte']) {
					$compte = $_compte;
				}
			}
			if (!empty($compte['cpt_id'])) {
				if (empty($comptes[$compte['cpt_id']])) {
					$comptes[$compte['cpt_id']] = [
						'montant_ht' =>  0,
						'numero' => ''
					];
				}

				$comptes[$compte['cpt_id']]['montant_ht'] =  $comptes[$compte['cpt_id']]['montant_ht'] + $montant_ht;
				$comptes[$compte['cpt_id']]['numero'] = $compte['cpt_numero'];
				$comptes_total = $comptes_total + $montant_ht;
			}
		}

		// BOUCLE SUR LES COMPTES
		if (!empty($total_ht)) {
			$total_ht = round($total_ht, 2);
		}
		if (abs(($total_ht-$comptes_total)/$comptes_total) < 0.00001) {
			foreach ($comptes as $cpt_id => $compte) {
				$modele['typeCompte'] = 'G';
				$modele['numCompteGeneral'] = mb_str_pad($compte['numero'], 13, 0);
				$modele['compteAuxilaire'] = mb_str_pad(' ', 13);
				$modele['debitCredit'] = 'D';
				$modele['montant'] = mb_str_pad(number_format($compte['montant_ht'], 2, ',', ' '), 20, ' ', STR_PAD_LEFT);
				$modele['planAnalytique'] = mb_str_pad(' ', 10);
			}

			$result = writeLine($modele);


			// LIGNE ANALYTIQUE
			$req = $Conn->prepare("SELECT * FROM agences WHERE age_id = " . $ndf['ndf_agence']);
			$req->execute();
			$agence = $req->fetch();

			if ($agence['age_analytique_agence']) {
				$modele['typeCompte'] = 'A';
				$modele['planAnalytique'] = mb_str_pad("1", 10, ' ');
				$modele['compteAuxilaire'] = mb_str_pad($agence['age_analytique_agence'], 13, ' ');
				$result .= writeLine($modele);
			}
			// FIN LIGNE ANALYTIQUE

			// LIGNE TVA
			$modele['numCompteGeneral'] = mb_str_pad('445660', 13, 0);
			$modele['typeCompte'] = 'G';
			$modele['compteAuxiliaire'] = mb_str_pad(' ', 13, 0);
			$modele['debitCredit'] = 'D';
			$modele['planAnalytique'] = mb_str_pad(' ', 10);
			$modele['montant'] = mb_str_pad(number_format($total_tva, 2, ',', ' '), 20, ' ', STR_PAD_LEFT);
			$result .= writeLine($modele);
			// FIN LIGNE TVA

			// LIGNE TTC SALARIE
			$modele['numCompteGeneral'] = mb_str_pad('423', 13, 0);
			$modele['typeCompte'] = 'X';
			if ($ndf['ndf_societe'] == 13) {
				$modele['compteAuxiliaire'] = mb_str_pad("42300" . substr($ndf['uti_matricule'], -3), 13);
			} else {
				$modele['compteAuxiliaire'] = mb_str_pad("423" . substr($ndf['uti_matricule'], -3), 13);
			}
			$modele['debitCredit'] = 'C';
			$modele['planAnalytique'] = mb_str_pad(' ', 10);
			$modele['montant'] = mb_str_pad(number_format($montant_ttc, 2, ',', ' '), 20, ' ', STR_PAD_LEFT);
			$result .= writeLine($modele);
			// FIN LIGNE TTC SALARIE
		} else {
			$erreur .= "Il y a une erreur avec la note " .  $ndf['ndf_id'] . " de " . $ndf['uti_prenom'] . " " . $ndf['uti_nom'] . ". Intégration impossible.";
		}
	}

	if (empty($erreur)) {
		foreach ($ndfs as $ndf) {
			// UPDATE LES NOTES
			$req = $Conn->prepare("UPDATE ndf SET ndf_statut = 7 WHERE ndf_id = " . $ndf['ndf_id']);
			$req->execute();
		}
	}
}
////////////////////FIN TRAITEMENTS PHP//////////////////////
?>
<!DOCTYPE html>
<html>

<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Notes de frais</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
	<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="sb-top sb-top-sm">
	<!-- Start: Main -->
	<div id="main">

		<?php include "includes/header_def.inc.php"; ?>
		<!-- Start: Content-Wrapper -->
		<section id="content_wrapper">

			<section id="content" class="animated fadeIn pr20 pl20 admin-forms">
				<h1>Résultat</h1>
				<div class="row">

					<div class="col-md-8">

						<?php if (empty($erreur)) { ?>

							<?php if (!empty($result)) { ?>
								<div class="row">
									<div class="col-md-12">
										<button type="button" class="btn btn-sm btn-primary mb10" onclick="copyToClipboard('#copyToClipboard')">Copier dans le press-papier</button>
									</div>
									<div class="col-md-12">
										<div class="panel-body p10" style="overflow-x: auto;">
											<span id="copyToClipboard" style="white-space:pre;font-family:Courier;"><?= $result ?></span>
										</div>
									</div>
								</div>
							<?php } else { ?>
								<?php if (empty($_POST)) { ?>
									<div class="col-md-12">
										<div class="alert alert-info">
											Veuillez lancer l'export
										</div>
									</div>
								<?php } else { ?>
									<div class="col-md-12">
										<div class="alert alert-info">
											Aucune note de frais disponible
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						<?php } else { ?>
							<div class="col-md-12">
								<div class="alert alert-danger">
									<?= $erreur ?>
								</div>
							</div>
						<?php } ?>
					</div>
					<form action="ndf_export_sage.php" class="admin-form" method="POST">
						<div class="col-md-4">
							<div class="panel mb10">
								<div class="panel-heading panel-head-sm">
									<span class="panel-icon">
										<i class="fa fa-plus" aria-hidden="true"></i>
									</span>
									<span class="panel-title">Export vers SAGE</span>
								</div>
								<div class="panel-body p10">
									<div class="row">
										<div class="col-md-6">
											<p class="text-center mb10 mt20" style="font-weight:bold;">Mois :</p>
											<select id="statut" name="ndf_mois" class="select2">
												<option value="0">Mois...</option>
												<?php foreach ($mois as $k => $m) { ?>
													<option value="<?= $k  ?>" <?php
																				if (!empty($_POST)) {
																					if ($_POST['ndf_mois'] == $k) { ?> selected <?php }
																														} else {
																															if ($k == date("n")) { ?> selected <?php
																																						}
																																					} ?>><?= $m ?></option>
												<?php } ?>
											</select>

										</div>
										<div class="col-md-6">
											<p class="text-center mb10 mt20" style="font-weight:bold;">Année :</p>
											<select id="statut" name="ndf_annee" class="select2">
												<option value="0">Année...</option>

												<?php foreach ($annees as $a) {

												?>
													<option value="<?= $a ?>" <?php if (!empty($_POST)) {
																					if ($_POST['ndf_annee'] == $a) { ?> selected <?php
																																}
																															} else {
																																if ($a == date("Y")) {
																																	?> selected <?php }
																																	} ?>><?= $a ?>
													</option>
												<?php

												} ?>
											</select>

										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<p class="text-center mb10 mt20" style="font-weight:bold;">Période :</p>
											<select name="ndf_periode" class="select2">
												<option value="0">Première quinzaine</option>
												<option value="1" <?php if (!empty($_POST) && $_POST['ndf_periode'] == 1) { ?> selected <?php } ?>>Deuxième quinzaine et carte affaire</option>
											</select>
										</div>
									</div>
								</div>
								<div class="panel-footer text-right">
									<button type="submit" class="btn btn-sm btn-primary">Lancer l'export</button>
								</div>

							</div>
						</div>
					</form>

				</div>
	</div>
	</section>
	</section>
	</div>
	<!-- End: Main -->
	<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
		<div class="row">
			<div class="col-xs-4 footer-left pt7">
				<a href="parametre.php" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a>
			</div>
			<div class="col-xs-4 footer-middle pt7"></div>
			<div class="col-xs-4 footer-right pt7">
			</div>
		</div>
	</footer>
	<?php
	include "includes/footer_script.inc.php"; ?>

	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
	<!-- plugin pour les masques formulaires -->
	<script src="assets/js/custom.js"></script>

	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script src="vendor/plugins/summernote/summernote.min.js"></script>
	<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

	<script src="vendor/plugins/jquery.numberformat.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->
	<script>
		// DOCUMENT READY //a
		jQuery(document).ready(function() {

			// initilisation plugin datatables
			////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
		});

		function copyToClipboard(element) {
			var $temp = $("<textarea>");
			$("body").append($temp);
			var text = $(element).clone().find('br').prepend('\r\n').end().text();
			$temp.val(text).select();
			document.execCommand("copy");
			$temp.remove();
			new PNotify({
				title: "Copie effectuée",
				text: "L'export est dans votre presse-papiers, vous pouvez le coller dans Sage",
				type: "success", // all contextuals available(info,system,warning,etc)
				hide: true,
				mouse_reset: false,
				delay: 4000
			});
		}
		///////////// FONCTIONS //////////////
		//////////// FIN FONCTIONS //////////
	</script>
</body>

</html>