<?php 

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";

include('includes/connexion.php');

include('modeles/mod_get_juridique.php');

include('modeles/mod_parametre.php');

include('modeles/mod_ssiap_data.php');

// VISU D'UN OU PLUSIEUR CACES

$erreur=0;

$diplome=0;
$print=0;



if(!empty($_GET["diplome"])){
	$diplome=intval($_GET["diplome"]);
	$dossiers=array(
		"0" => $diplome
	);
}elseif(!empty($_POST["dossiers"])){
	$dossiers=$_POST["dossiers"];
}



if(empty($dossiers)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();
	
}else{

	// la personne logue
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	// pas d'include de connexion_soc
	$conn_soc_id=$acc_societe;
	if($_SESSION['acces']["acc_ref"]!=2){
		if($_SESSION['acces']["acc_droits"][8]){
			if(isset($_GET["societ"])){
				if(!empty($_GET["societ"])){
					$conn_soc_id=intval($_GET["societ"]);
				}
			}
		}
	}


	// ON RECUPERE LES DONNES DU DEVIS A AFFICHER
	
	$multi_data=array();
	foreach($dossiers as $dos){
		$multi_data[]=ssiap_data($dos);
	}
	
		
	if(empty($multi_data)){
		$erreur_txt="impossible d'afficher la page";
		echo($erreur_txt);
		die();
	}
	
} ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#container_print{
				width:28cm;
			}
			
			.page{
                height:20cm;
                padding:30px;
			}
			
			.diplome{
				border:2px solid #e31936;
                padding:10px;
                height:100%;
                font-size:14px!important;
			}
			
			h2{
                font-size:14pt;
                color:#7e8082;
                text-align:center; 
            }
			
			.logo{
				max-height:150px;
				max-width:200px;
				padding-top:10px!important;
            }
            .formDonnee{
                padding:10px;
            }
			.signature{
				max-width:150px;
			}
			.cachet{
				font-size:10px;
				border:1px solid black;
				padding:3px;
            }
            .blocInline{
                float:left;
            }
            .stopBlocInline{
                clear:left;
            }
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 landscape;
				margin:5mm;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm document-affiche">
		<div id="zone_print" ></div>
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">	
		<?php 		if($erreur==0){ ?>
		
							<div class="row" >
							
								<!-- zone d'affichage du devis -->
								
								<div class="col-md-12"  >	
								
									<div id="container_print" >
								
										<div id="page_print" >
									
								<?php		foreach($multi_data as $data){ ?>
												<div class="page" >
											
													<div class="diplome" >
														<table class="formDonnee">
															<tr>
																<td class="w-25 text-left">
																	
																<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
																			<img src="../<?=$data["juridique"]["logo_1"]?>" class="logo"/>
															<?php		}else{
																			echo("&nbsp;");
																		} ?>
																	
																</td>
																<td class="text-center w-50">
																	<h2><?=$data['dss_form_titre']?></h2>
																	<h1><?=$data['dss_form_libelle']?></h1>
																</td>
																<td class="text-right w-25">
																	<img src="assets/img/photo_id.png" alt="photo_id" title="Photo d'identité" />
																</td>
															</tr>																											
														</table>
														<p class="mt5" ><?=$data['dss_form_texte_1']?></p>	
														<table class="formDonnee" >
															<tr>
																<th class="text-right" >Nom :</th>
																<td><?=$data['dss_sta_nom']?></td> 
																<th class="text-right" >Prénom :</th>
																<td><?=$data['dss_sta_prenom']?></td>
															</tr>
															<tr>
																<th class="text-right" >Né(e) le :</th>
																<td><?=convert_date_txt($data['dss_sta_naissance'])?></td> 
																<th class="text-right" >à :</th>
																<td>
															<?php	echo($data['dss_sta_ville'] . " (");
																	if(strlen($data['dss_sta_cp'])==2){
																		echo("0" . $data['dss_sta_cp']);																
																	}else{
																		echo($data['dss_sta_cp']);						
																	}
																	echo(")"); ?>
																</td> 
															</tr>
														</table>
														<p class="text-center" ><?=str_replace(chr(10),"<br/>",$data['dss_form_texte_2'])?></p>
														<p>
															telles que définies dans l'arrêté du 02 mai 2005, modifié.
														</p>
														<p class="text-center" >
															<b>Diplôme n° <?=$data['dss_numero']?></b>
													</p>
														<p>Fait à <?=$data['dss_ville']?>, le <?=convert_date_txt($data['dss_date'])?></p>
														
														<div class="blocInline" style="width:50%;" >
															<p>
																<?=$data['juridique']['resp_fonction']?><br/>
																Nom : <?=$data['juridique']['resp_ident']?><br>
																<?php if(!empty($data["juridique"]["resp_signature"])){ ?>
																		<img src="<?=$data["juridique"]["resp_signature"]?>" class="img-responsive" />
																<?php	} ?>
															</p>
														</div>
														<div class="blocInline" style="width:50%;" >
															<p>
																Le représentant du service d'incendie et de secours compétent,<br/><br/>
																Nom :<br/><br/>
																GRADE :<br/><br/>
																Signature
															</p>
														</div>
														<div class="stopBlocInline" ></div>
													</div>
												</div>	
								<?php		} ?>		
										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->

							</div>
						

		<?php		}else{ ?>
						
						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>
					
		<?php		} ?>
									
				</section>
				
				<!-- End: Content -->
			</section>
			
			
		</div>
		<!-- End: Main -->
		
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >	
		<?php		if(isset($_SESSION["retourSsiap"])){
						if(!empty($_SESSION["retourSsiap"])){ ?>
							<a href="<?=$_SESSION["retourSsiap"]?>" class="btn btn-sm btn-default" >
								<i class="fa fa-left-arrow"></i> Retour
							</a>
		<?php			}
					} ?>
					
				</div>
				<div class="col-xs-6 footer-middle text-center" >		
					<button type="button" class="btn btn-sm btn-info ml15" id="print_local" >
						<i class="fa fa-print"></i> Imprimer
					</button>							
				</div>
				<div class="col-xs-3 footer-right" >
			<?php	if(!empty($diplome)){ ?>
						<a href="dip_ssiap.php?diplome=<?=$diplome?>&action=<?=$data["dss_action"]?>&stagiaire=<?=$data["dss_stagiaire"]?>&societ=<?=$conn_soc_id?>" class="btn btn-sm btn-warning" >
							<i class="fa fa-pencil"></i> Modifier
						</a>
			<?php	} ?>
				</div>
			</div>
		</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
		
			var l_page=28;
			var h_page=21;

			jQuery(document).ready(function (){								
				var ppc=(parseInt($("#page_print").width())/l_page);
				var hauteur=parseInt((h_page*ppc));
				/*$(".page").each(function( index ){
					$(this).css("height", hauteur + "px");
				});*/
			
				$("#print_local").click(function(){
		
					$("#zone_print").html($("#container_print").html());
					
					$("#main").hide();
					$("#content-footer").hide();					
					$(".btn").hide();
					
					$("#zone_print").show();
					
					window.print();
					
					$("#zone_print").hide();
					
					$("#main").show();
					$("#content-footer").show();
					$(".btn").show();
					
					
				});
				
			});
			
			
		</script>
	</body>
</html>
