<?php 

// AFFICHE LA LISTE DES FACTURES

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');


// DONNEE UTILE AU PROGRAMME

if($_SESSION['acces']['acc_service'][2]!=1){
	header("location : deconnect.php");
	die();
}

$_SESSION['retour'] = "reglement_liste.php";
$_SESSION["retourFacture"] = "reglement_liste.php";

	if (isset($_POST['search'])){
		
		// le formulaire a été soumit
		// on actualise les criteres de recherche
		
		$reg_date_deb="";
		if(!empty($_POST["reg_date_deb"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["reg_date_deb"]);
			if(!is_bool($DT_periode)){
				$reg_date_deb=$DT_periode->format("Y-m-d");
			}
		}
		$reg_date_fin="";
		if(!empty($_POST["reg_date_fin"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["reg_date_fin"]);
			if(!is_bool($DT_periode)){
				$reg_date_fin=$DT_periode->format("Y-m-d");
			}
		}

		$reg_banque=null;
		if(!empty($_POST['reg_banque'])){
			$reg_banque=intval($_POST['reg_banque']);
		}
		
		$reg_type=null;
		if(!empty($_POST['reg_type'])){
			$reg_type=intval($_POST['reg_type']);
		}
		
		// mémorisation des critere

		$_SESSION['fac_tri'] = array(
			"reg_date_deb" => $reg_date_deb,
			"reg_date_fin" => $reg_date_fin,
			"reg_banque" => $reg_banque,
			"reg_type" => $reg_type
		);
	}
	

	// ON CONSTRUIT LA REQUETE

	$critere=array();
	
	$mil="";
	if(!empty($_SESSION['fac_tri']['reg_date_deb'])){
		$mil.=" AND reg_date>=:reg_date_deb"; 
		$critere["reg_date_deb"]=$_SESSION['fac_tri']['reg_date_deb'];
	};
	
	if(!empty($_SESSION['fac_tri']['reg_date_fin'])){
		$mil.=" AND reg_date<=:reg_date_fin"; 
		$critere["reg_date_fin"]=$_SESSION['fac_tri']['reg_date_fin'];
	};
	
	if(!empty($_SESSION['fac_tri']['reg_banque'])){
		$mil.=" AND reg_banque=:reg_banque"; 
		$critere["reg_banque"]=$_SESSION['fac_tri']['reg_banque'];
	};
	
	if(!empty($_SESSION['fac_tri']['reg_type'])){
		$mil.=" AND reg_type=:reg_type"; 
		$critere["reg_type"]=$_SESSION['fac_tri']['reg_type'];
	};
	
	// fin critère form
	
	// champ a selectionne
	$sql="SELECT fac_id,fac_numero,fac_total_ttc,fac_regle,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date_fr
	,cli_code,cli_nom
	,reg_banque,DATE_FORMAT(reg_date,'%d/%m/%Y') AS reg_date_fr,reg_montant,reg_rb,reg_rb_date,reg_id,DATE_FORMAT(reg_rb_date,'%d/%m/%Y') AS reg_rb_date_fr
	,reg_type,reg_reference
	FROM Factures 
	INNER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)	
	INNER JOIN Reglements ON (Factures.fac_id=Reglements.reg_facture)";
	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY fac_date,fac_chrono,fac_numero,fac_id;";
	$req = $ConnSoc->prepare($sql);	

	if(!empty($critere)){
		foreach($critere as $c => $u){
			$req->bindValue($c,$u);
		}
	};
	$req->execute();
	$factures = $req->fetchAll();
		
	
	// DONNEES PARAMETRES
	
	// les banques

	$d_banques=array();
	$sql="SELECT ban_id,ban_code FROM Banques ORDER BY ban_code;";
	$req=$Conn->query($sql);
	$d_result_banque=$req->fetchAll();
	if(!empty($d_result_banque)){
		foreach($d_result_banque as $r){
			$d_banques[$r["ban_id"]]=$r["ban_code"];
		}
	}
	
	// les types de reglemnts

	$d_types=array();
	$sql="SELECT rty_id,rty_libelle FROM Reglements_Types ORDER BY rty_libelle;";
	$req=$Conn->query($sql);
	$d_result_type=$req->fetchAll();
	if(!empty($d_result_type)){
		foreach($d_result_type as $r){
			$d_types[$r["rty_id"]]=$r["rty_libelle"];
		}
	}
	
	
	
	
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
			
					<h1>Liste des règlements</h1>

	<?php 			if(!empty($factures)){ 
	
						$total_regle=0; ?>
					
						<div class="admin-form theme-primary ">

							<div class="table-responsive">
								<table class="table table-striped table-hover" >
									<thead>
										<tr class="dark2" >											
											<th>Client</th>									
											<th>Num fac</th>
											<th>Date fac</th>											
											<th>T.T.C.</th>
											<th>Réglé</th>
											<th>Reste du</th>
											<th>Montant du règlement</th>
											<th>Date</th>										
											<th>Banque</th>
											<th>Type</th>
											<th>Commentaire</th>
										</tr>
									</thead>
									<tbody>
							<?php		foreach($factures as $f){
											
											$reste_du=$f['fac_total_ttc']-$f['fac_regle'];
											
											$total_regle+=$f['reg_montant']; ?>
											<tr>
												<td><?= $f['cli_code'] ?></td>
												<td><?= $f['fac_numero'] ?></td>
												<td><?= $f['fac_date_fr'] ?></td>
												<td class="text-right" ><?=number_format($f['fac_total_ttc'], 2, ',', ' ')?></td>
												<td class="text-right" >
											<?php	if($_SESSION["acces"]["acc_droits"][30]){ ?>
														<a href="reglement.php?facture=<?=$f['fac_id']?>" >
															<?=number_format($f['fac_regle'], 2, ',', ' ')?>
														</a>
											<?php	}else{ 
														echo(number_format($f['fac_regle'], 2, ',', ' '));
													} ?>
												</td>
												<td class="text-right" ><?= number_format($reste_du, 2, ',', ' ')?></td>
												<td class="text-right" ><?= number_format($f['reg_montant'], 2, ',', ' ')?></td>
												<td><?= $f['reg_date_fr'] ?></td>
												<td>
										<?php		if(!empty($d_banques[$f["reg_banque"]])){
														echo($d_banques[$f["reg_banque"]]);
													};?>
												</td>	
												<td>
										<?php		if(!empty($d_types[$f["reg_type"]])){
														echo($d_types[$f["reg_type"]]);
													};?>
												</td>
												<td><?= $f['reg_reference'] ?></td>
											</tr>										
				<?php					} ?>
									</tbody>
									<tfoot>
										<tr>
											<th colspan="6" class="text-right" >Total :</th>
											<td class="text-right" ><?=number_format($total_regle, 2, ',', ' ');?></td>		
											<td colspan="2" >&nbsp;</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					
	<?php 			}else{ ?>
						<div class="col-md-12 text-center" style="padding:0;" >
							<div class="alert alert-warning" style="border-radius:0px;">
								Aucune règlement correspondant à votre recherche.
							</div>
						</div>
	<?php			} ?>
	
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="reglement_tri.php" class="btn btn-default btn-sm" role="button">
						<span class="fa fa-search"></span>
						<span class="hidden-xs">Nouvelle recherche</span>
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
				
				</div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>
				
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
			
			});	
		</script>
	</body>
</html>
