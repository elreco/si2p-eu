<?php  
// LISTE DES STAGIAIRES

include "includes/controle_acces.inc.php";
include('includes/connexion.php');

// personne connecté
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}

if(!empty($_POST)){
	
	// ENVOIE DU FORM -> on contruit la requete
	
	$sta_client=0;
	if(!empty($_POST["sta_client"])){
		$sta_client=intval($_POST["sta_client"]);
	}

	$mil="";
	$sql="SELECT DISTINCT sta_id,sta_nom,sta_prenom,DATE_FORMAT(sta_naissance,'%d/%m/%Y') AS date_naiss,sta_client,sta_client_nom,sta_client_code 
	FROM Stagiaires INNER JOIN Stagiaires_Clients ON (Stagiaires.sta_id=Stagiaires_Clients.scl_stagiaire)
	INNER JOIN Clients_Societes ON (Stagiaires_Clients.scl_client=Clients_Societes.cso_client)
	WHERE cso_societe=" . $acc_societe;
	if(!empty($acc_agence)){
		$sql.=" AND cso_agence=" . $acc_agence;
	}
	if(!empty($sta_client)){
		if(!empty($_POST["employeur"])){
			// employeur actuel
			$sql.=" AND sta_client=" . $sta_client;
		}else{
			$sql.=" AND scl_client=" . $sta_client;
		}
	}
	if(!empty($_POST['sta_nom'])){
		$sql.=" AND sta_nom LIKE '" . $_POST['sta_nom'] . "%'"; 
	};

	if(!empty($_POST['sta_prenom'])){
		$sql.=" AND sta_prenom LIKE '" . $_POST['sta_prenom'] . "%'"; 
	};
	$sql.=" ORDER BY sta_nom,sta_prenom;";
	//echo($sql);
	$_SESSION["sql_sta"]=$sql;

}elseif(isset($_SESSION["sql_sta"])){
	$sql=$_SESSION["sql_sta"];
}else{
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => "Impossible d'effectuer la recherche demandé" 
	);
	header("location : stagiaire_tri.php");
	die();
}
/*
echo($sql);
die();*/
$req = $Conn->query($sql);
$d_stagiaires = $req->fetchAll();
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
		<div id="main">
<?php		include "includes/header_def.inc.php";  ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
					<h1>Liste des stagiaires</h1>
		<?php 		if(!empty($d_stagiaires)){ ?>
						<div class="table-responsive">
							<table class="table table-striped table-hover" id="table_id">
								<thead>
									<tr class="dark">
										<th>&nbsp;</th>
										<th>Nom</th> 
										<th>Prénom</th>
										<th>Date de naissance</th>
										<th colspan="2" >Client</th>		
										<th>Modifier</th>
									</tr>
								</thead>
								<tbody>
						<?php 		foreach($d_stagiaires as $s){ ?>                      
										<tr>
											<td>
												<a href="stagiaire_voir.php?stagiaire=<?= $s['sta_id'] ?>" class="btn btn-info btn-sm" data-toggle="tooltip" title="Voir la fiche stagiaire" >
													<i class="fa fa-eye"></i>
												</a>
											</td>
											<td><?= $s['sta_nom']?></td>
											<td><?= $s['sta_prenom']?></td>
											<td><?=$s['date_naiss']?></td>
											<td><?= $s['sta_client_code']?></td>
											<td><?= $s['sta_client_nom']?></td>
											<td>
												<a href="stagiaire.php?stagiaire=<?= $s['sta_id'] ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Modifier la fiche stagiaire" >
													<i class="fa fa-pencil"></i>
												</a>
											</td>
										</tr>
						<?php		} ?>
								</tbody>
							</table>
						</div>
	<?php 			}else{ ?>
						<div class="col-md-12 text-center" style="padding:0;" >
							<div class="alert alert-warning" style="border-radius:0px;">
							  Aucun stagiaire.
							</div>
						</div>
	<?php 			} ?>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="stagiaire_tri.php" class="btn btn-default btn-sm" role="button">
						<span class="fa fa-search"></span>
						<span class="hidden-xs">Nouvelle recherche</span>
					</a>        
				</div>	
				<div class="col-xs-6 "></div>	
				<div class="col-xs-3 footer-right">
					<!--<a href="stagiaire.php" class="btn btn-success btn-sm" role="button">
						<span class="fa fa-plus"></span>
						<span class="hidden-xs">Nouveau stagiaire</span>
					</a>-->
				</div>
			</div>
		</footer>
<?php	include "includes/footer_script.inc.php"; ?>  
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
			  
			});
		</script>
	</body>
</html>
