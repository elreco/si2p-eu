<?php 
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include("includes/connexion_fct.php");

$erreur_txt="";

$client=0;
if(!empty($_POST)){
	
	if(!empty($_POST['client'])){
		$client=intval($_POST['client']);
	}
}

if($client>0){
	
	// PERSONNE CONNECTE
	
	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}
	
	// DONNEE DU CLIENT 
	$req=$Conn->query("SELECT * FROM Clients WHERE cli_id=" . $client . ";");
	$d_client=$req->fetch();
	if(empty($d_client)){
		echo("Impossible d'afficher la page !");
		die();
	}else{
		
		if($d_client["cli_categorie"]==2){
			if(empty($_SESSION["acces"]["acc_droits"][8])){
				echo("Impossible d'afficher la page !");
				die();
			}
		}elseif($d_client["cli_categorie"]==5){
			
			if(empty($_SESSION['acces']['acc_service'][1]) OR empty($_SESSION["acces"]["acc_droits"][15])){
				// c'est un client NATIONNAL et U ne fait pas partie du service com ou n'a pas le droit client réseau
				echo("Impossible d'afficher la page !");
				die();
			}
		}elseif(empty($_SESSION["acces"]["acc_droits"][15])){
			echo("Impossible d'afficher la page !");
			die();	
		}
		$cli_facture=0;
		if(!empty($d_client["cli_first_facture"])){
			$cli_facture=1;
		}
		
	}
	
	// RIB PAR DEFAUT DE CHAQUE SOCIETE
	
	$rib=array();
	$req=$Conn->query("SELECT rib_id,rib_change,rib_societe FROM Rib WHERE rib_defaut ORDER BY rib_societe;");
	$result=$req->fetchAll(); 
	foreach($result as $r){
		$rib[$r["rib_societe"]]=array(
			"id" => $r["rib_id"],
			"change" => $r["rib_change"]
		);
		
	}
	
	// requete prep
	$req_add_soc=$Conn->prepare("INSERT INTO Clients_Societes (cso_client,cso_societe,cso_agence,cso_commercial,cso_archive,cso_utilisateur,cso_rib,cso_rib_change)
	VALUES (" . $client . ", :cso_societe, :cso_agence, :cso_commercial,0, :cso_utilisateur,:cso_rib,:cso_rib_change);");
	
	$req_up_soc=$Conn->prepare("UPDATE Clients_Societes SET cso_commercial=:cso_commercial,cso_archive=:cso_archive, cso_utilisateur=:cso_utilisateur,cso_rib=:cso_rib, cso_rib_change=:cso_rib_change 
	WHERE cso_client=" . $client . " AND cso_societe=:cso_societe AND cso_agence=:cso_agence;");
	
	$req_get_soc=$Conn->prepare("SELECT * FROM Clients_Societes WHERE cso_client=" . $client . " AND cso_societe=:cso_societe AND NOT cso_agence=:cso_agence ORDER BY cso_rib DESC;");
	
	$soc_id=0;
	
	$sql="SELECT soc_id,soc_nom,soc_agence,age_id,age_nom,cso_societe,cso_agence,cso_commercial,cso_utilisateur,cso_rib,cso_rib_change 
	FROM Societes 
	LEFT OUTER JOIN Agences ON (Societes.soc_id=Agences.age_societe)";
	if($d_client["cli_categorie"]!=2 AND $_SESSION['acces']["acc_profil"]!=10){
		$sql.=" LEFT JOIN Utilisateurs_Societes ON (Societes.soc_id=Utilisateurs_Societes.uso_societe AND (Agences.age_id=Utilisateurs_Societes.uso_agence OR ISNULL(age_id) ) )";
	}
	$sql.=" LEFT OUTER JOIN Clients_Societes ON (Societes.soc_id=Clients_Societes.cso_societe AND (Agences.age_id=Clients_Societes.cso_agence OR ISNULL(age_id)) AND Clients_Societes.cso_client=" . $client .")
	WHERE NOT soc_archive";
	if($d_client["cli_categorie"]!=2 AND $_SESSION['acces']["acc_profil"]!=10){
		$sql.=" AND uso_utilisateur=" . $acc_utilisateur;
	}else{
		$sql.=" AND NOT soc_id IN (1,6,10)";
	}
	$sql.=" ORDER BY soc_id,age_id;";
	$req=$Conn->query($sql);
	$societes=$req->fetchAll(); 
	foreach($societes as $s){
		
		if($s["soc_id"]!=$soc_id){
			$ConnFct = connexion_fct($s['soc_id']);
			$soc_id=$s["soc_id"];
		}
		
		$age_id=0;
		if(!empty($s['age_id'])){
			$age_id=$s['age_id'];
		}
		
		// val avant modif (empty en cas d'ajout)
		$cso_rib=$s["cso_rib"];
		$cso_rib_change=$s["cso_rib_change"];
	
		$maj_n=false;
		
		if(!empty($_POST['cso_societe_' . $soc_id . "_" . $age_id])){
			
			// L'ENTITE EST COCHE
			
			$maj_n=true;
			$cso_archive=0;
			
			// la societe / agence est coché
			
			$cso_commercial=0;
			if(!empty($_POST['cso_commercial_' . $soc_id . "_" . $age_id])){
				$cso_commercial=intval($_POST['cso_commercial_' . $soc_id . "_" . $age_id]);
			}
			
			// donnee du com
			
			$req=$ConnFct->query("SELECT * FROM Commerciaux WHERE com_id=" . $cso_commercial . ";"); 
			$d_commercial=$req->fetch(); 
			
			
			if(!empty($d_commercial)){
				$cso_utilisateur=$d_commercial["com_ref_1"];
			}
			

			if(empty($s['cso_societe'])){
				
				// client_societe n'existe pas -> on add
				
				// RIB identique sur toutes les agences => on regarde si rib existe deja
				$req_get_soc->bindParam("cso_societe",$soc_id);
				$req_get_soc->bindParam("cso_agence",$age_id);
				$req_get_soc->execute();
				$client_autres=$req_get_soc->fetchAll();
				if(!empty($client_autres)){
					foreach($client_autres as $ca){
						if(!empty($ca["cso_rib"])){
							$cso_rib=$ca["cso_rib"];
							$cso_rib_change=$ca["cso_rib_change"];							
							break;
						}
					}
					if(empty($cso_rib)){				
						$cso_rib=$rib[$soc_id]["id"];
						$cso_rib_change=1;
					}
				}else{
					// on attribue le rib par defaut
					$cso_rib=$rib[$soc_id]["id"];
					$cso_rib_change=1;
				}
				
				// ADD
				$req_add_soc->bindParam(":cso_societe",$soc_id);
				$req_add_soc->bindParam(":cso_agence",$age_id);
				$req_add_soc->bindParam(":cso_commercial",$cso_commercial);
				$req_add_soc->bindParam(":cso_utilisateur",$cso_utilisateur);
				$req_add_soc->bindParam(":cso_rib",$cso_rib);
				$req_add_soc->bindParam(":cso_rib_change",$cso_rib_change);
				$req_add_soc->execute();
				
				
			}else{
				
				// client etait déjà affecté ou lié mais archivé et le reste -> on up

				$req_up_soc->bindParam(":cso_societe",$soc_id);
				$req_up_soc->bindParam(":cso_agence",$age_id);
				$req_up_soc->bindParam(":cso_commercial",$cso_commercial);
				$req_up_soc->bindParam(":cso_utilisateur",$cso_utilisateur);
				$req_up_soc->bindParam(":cso_archive",$cso_archive);
				$req_up_soc->bindParam(":cso_rib",$cso_rib);
				$req_up_soc->bindParam(":cso_rib_change",$cso_rib_change);
				$req_up_soc->execute();
				
			}
		}elseif(!empty($s['cso_societe'])){
			
			// etait affecté mais ne l'est plus -> on archive
			
			
			$maj_n=true;
			$cso_archive=1;
			$cso_commercial=$s["cso_commercial"];
			$cso_utilisateur=$s["cso_utilisateur"];
			
			$req_up_soc->bindParam(":cso_societe",$soc_id);
			$req_up_soc->bindParam(":cso_agence",$age_id);
			$req_up_soc->bindParam(":cso_commercial",$cso_commercial);
			$req_up_soc->bindParam(":cso_utilisateur",$cso_utilisateur);
			$req_up_soc->bindParam(":cso_archive",$cso_archive);
			$req_up_soc->bindParam(":cso_rib",$cso_rib);
			$req_up_soc->bindParam(":cso_rib_change",$cso_rib_change);
			$req_up_soc->execute();
			
		}
		
		// MAJ DE LA BASE N
		
		if($maj_n){
			
			// ON DUPLIQUE LES DONNEES DU CLIENT EN BASE N
			$req=$ConnFct->query("SELECT cli_id FROM Clients WHERE cli_id = " . $client . " AND cli_agence = " . $age_id);
			$d_client_soc=$req->fetch();
			if(!empty($d_client_soc)){
				// update
				
				$req=$ConnFct->prepare("UPDATE Clients SET cli_code=:cli_code,cli_nom=:cli_nom,cli_commercial=:cli_commercial,cli_utilisateur=:cli_utilisateur,
				cli_categorie=:cli_categorie,cli_groupe=:cli_groupe,cli_filiale_de=:cli_filiale_de,cli_archive=:cli_archive,cli_sous_categorie=:cli_sous_categorie,
				cli_rib=:cli_rib,cli_rib_change=:cli_rib_change,cli_cp=:cli_cp,cli_interco_soc=:cli_interco_soc,cli_interco_age=:cli_interco_age
				,cli_affacturable=:cli_affacturable,cli_affacturage=:cli_affacturage,cli_fac_groupe=:cli_fac_groupe,cli_facture=:cli_facture
				WHERE cli_id=:cli_id AND cli_agence=:cli_agence;");
				$req->bindValue("cli_id",$client);
				$req->bindValue("cli_agence",$age_id);
				$req->bindValue("cli_code",$d_client['cli_code']);
				$req->bindValue("cli_nom",$d_client['cli_nom']);
				$req->bindValue("cli_commercial",$cso_commercial);
				$req->bindValue("cli_utilisateur",$cso_utilisateur);
				$req->bindValue("cli_categorie",$d_client['cli_categorie']);
				$req->bindValue("cli_sous_categorie",$d_client['cli_sous_categorie']);
				$req->bindValue("cli_groupe",$d_client['cli_groupe']);
				$req->bindValue("cli_filiale_de",$d_client['cli_filiale_de']);
				$req->bindValue("cli_archive",$cso_archive);			
				$req->bindValue("cli_rib",$cso_rib);
				$req->bindValue("cli_rib_change",$cso_rib_change);
				$req->bindValue("cli_cp",$d_client['cli_adr_cp']);
				$req->bindValue("cli_interco_soc",$d_client['cli_interco_soc']);
				$req->bindValue("cli_interco_age",$d_client['cli_interco_age']);
				$req->bindValue("cli_affacturage",$d_client['cli_affacturage']);
				$req->bindValue("cli_affacturable",$d_client['cli_affacturable']);
				$req->bindValue("cli_facture",$cli_facture);
				$req->bindValue("cli_fac_groupe",$d_client['cli_fac_groupe']);
				$req->execute();
				
			}else{
				//add
				
				$req=$ConnFct->prepare("INSERT INTO Clients (cli_id,cli_code,cli_nom,cli_agence,cli_commercial,cli_utilisateur,cli_categorie,cli_groupe,cli_filiale_de,cli_archive,cli_sous_categorie,cli_rib,cli_rib_change
				,cli_cp,cli_interco_soc,cli_interco_age,cli_affacturage,cli_affacturable,cli_facture,cli_fac_groupe) 
				VALUES (:cli_id,:cli_code,:cli_nom,:cli_agence,:cli_commercial, :cli_utilisateur,:cli_categorie, :cli_groupe, :cli_filiale_de,:cli_archive,:cli_sous_categorie,:cli_rib,:cli_rib_change
				,:cli_cp,:cli_interco_soc,:cli_interco_age,:cli_affacturage,:cli_affacturable,:cli_facture,:cli_fac_groupe)");
				$req->bindValue("cli_id",$client);
				$req->bindValue("cli_code",$d_client['cli_code']);
				$req->bindValue("cli_nom",$d_client['cli_nom']);
				$req->bindValue("cli_agence",$age_id);
				$req->bindValue("cli_commercial",$cso_commercial);
				$req->bindValue("cli_utilisateur",$cso_utilisateur);
				$req->bindValue("cli_categorie",$d_client['cli_categorie']);
				$req->bindValue("cli_sous_categorie",$d_client['cli_sous_categorie']);
				$req->bindValue("cli_groupe",$d_client['cli_groupe']);
				$req->bindValue("cli_filiale_de",$d_client['cli_filiale_de']);
				$req->bindValue("cli_archive",$cso_archive);
				$req->bindValue("cli_rib",$cso_rib);
				$req->bindValue("cli_rib_change",$cso_rib_change);		
				$req->bindValue("cli_cp",$d_client['cli_adr_cp']);
				$req->bindValue("cli_interco_soc",$d_client['cli_interco_soc']);
				$req->bindValue("cli_interco_age",$d_client['cli_interco_age']);
				$req->bindValue("cli_affacturage",$d_client['cli_affacturage']);
				$req->bindValue("cli_affacturable",$d_client['cli_affacturable']);
				$req->bindValue("cli_facture",$cli_facture);
				$req->bindValue("cli_fac_groupe",$d_client['cli_fac_groupe']);
				$req->execute();
				
			}
			
			// ON MET A JOUR LES RIB SUR TOUTES LES AGENCES DE LA SOCIETES
			
			if(isset($client_autres)){
				foreach($client_autres as $ca){
					
					if($ca["cso_rib"]!=$cso_rib){
						
						$req_up_soc->bindParam(":cso_societe",$ca["cso_societe"]);
						$req_up_soc->bindParam(":cso_agence",$ca["cso_agence"]);
						$req_up_soc->bindParam(":cso_commercial",$ca["cso_commercial"]);
						$req_up_soc->bindParam(":cso_utilisateur",$ca["cso_utilisateur"]);
						$req_up_soc->bindParam(":cso_archive",$ca["cso_archive"]);
						$req_up_soc->bindParam(":cso_rib",$cso_rib);
						$req_up_soc->bindParam(":cso_rib_change",$cso_rib_change);
						$req_up_soc->execute();
						
						// MAJ CLIENTS N POUR LES AUTRES AGENCES
						
						$req->bindValue("cli_id",$client);
						$req->bindValue("cli_agence",$ca["cso_agence"]);
						$req->bindValue("cli_code",$d_client['cli_code']);
						$req->bindValue("cli_nom",$d_client['cli_nom']);
						$req->bindValue("cli_commercial",$ca["cso_commercial"]);
						$req->bindValue("cli_utilisateur",$ca['cso_utilisateur']);
						$req->bindValue("cli_categorie",$d_client['cli_categorie']);
						$req->bindValue("cli_groupe",$d_client['cli_groupe']);
						$req->bindValue("cli_filiale_de",$d_client['cli_filiale_de']);
						$req->bindValue("cli_archive",$ca['cso_archive']);
						$req->bindValue("cli_sous_categorie",$d_client['cli_sous_categorie']);
						$req->bindValue("cli_rib",$cso_rib);
						$req->bindValue("cli_rib_change",$cso_rib_change);
						
					}
					
				}
				
			}
		}
	}
	// FIN SOCIETES
	
}else{
	$erreur_txt="Impossible de mettre à jour la fiche client.";
}

if(!empty($erreur_txt)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur!",
		"type" => "danger",
		"message" => $erreur_txt
	);
}else{
	$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Vous avez bien actualisé le réseau du client"
	);
}
Header("Location: client_reseau.php?client=" . $client);
die();

?>