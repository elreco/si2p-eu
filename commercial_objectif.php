<?php
$menu_actif = 1;
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");;
include_once("includes/connexion_soc.php");

// PARAMETRE
if(isset($_GET["commercial"])){
	if(!empty($_GET["commercial"])){
		$commercial=intval($_GET["commercial"]);
	}
}
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}

$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
}

// CONTROLE D'ACCES
if ($_SESSION['acces']["acc_profil"]!=10 AND $acc_utilisateur!=98 AND $acc_utilisateur!=403) {
	header("location : deconnect.php");
	die();
}elseif($commercial==0 OR $exercice==0){
	echo("Impossible d'afficher la page!");
	die();
}

$drt_validation=false;
if($acc_utilisateur==98 OR $acc_utilisateur==403){
	$drt_validation=true;	
}


$objectifs=array(
	"0" =>array(
		"libelle" => "Divers",
		"famille" => array(),
		"mois_1" =>0,
		"mois_2" =>0,
		"mois_3" =>0,
		"mois_4" =>0,
		"mois_5" =>0,
		"mois_6" =>0,
		"mois_7" =>0,
		"mois_8" =>0,
		"mois_9" =>0,
		"mois_10" =>0,
		"mois_11" =>0,
		"mois_12" =>0,
		"total" =>0,
		"autre" =>0
	)
);

// CATEGORIE
$sql="SELECT * FROM Produits_Categories ORDER BY pca_id;";
$req=$Conn->query($sql);
$d_categories=$req->fetchAll();
if(!empty($d_categories)){
	foreach($d_categories as $categorie){
		$objectifs[$categorie["pca_id"]]=array(
			"libelle" => $categorie["pca_libelle"],
			"famille" => array(),
			"mois_1" =>0,
			"mois_2" =>0,
			"mois_3" =>0,
			"mois_4" =>0,
			"mois_5" =>0,
			"mois_6" =>0,
			"mois_7" =>0,
			"mois_8" =>0,
			"mois_9" =>0,
			"mois_10" =>0,
			"mois_11" =>0,
			"mois_12" =>0,
			"total" =>0,
			"autre" =>0
		);
	}
}

// FAMILLE DE FORMATION
$sql="SELECT * FROM Produits_Familles 
LEFT OUTER JOIN Produits_Sous_Familles ON (Produits_Familles.pfa_id=Produits_Sous_Familles.psf_pfa_id)
LEFT OUTER JOIN Produits_Sous_Sous_Familles ON (Produits_Sous_Familles.psf_id=Produits_Sous_Sous_Familles.pss_psf_id)
ORDER BY pfa_id,psf_id,pss_id;";
$req=$Conn->query($sql);
$d_familles=$req->fetchAll();
if(!empty($d_familles)){
	$fam_id=0;
	$s_fam_id=0;
	$s_s_fam_id=0;
	foreach($d_familles as $famille){
		
		if($famille["pfa_id"]!=$fam_id){
			$fam_id=$famille["pfa_id"];
			$objectifs[1]["famille"][$fam_id]=array(
				"libelle" => $famille["pfa_libelle"],
				"sous_famille" => array(),
				"mois_1" =>0,
				"mois_2" =>0,
				"mois_3" =>0,
				"mois_4" =>0,
				"mois_5" =>0,
				"mois_6" =>0,
				"mois_7" =>0,
				"mois_8" =>0,
				"mois_9" =>0,
				"mois_10" =>0,
				"mois_11" =>0,
				"mois_12" =>0,
				"total" =>0,
				"autre" =>0
			);
		}
		if(!empty($famille["psf_id"])){
			if($famille["psf_id"]!=$s_fam_id){
				$s_fam_id=$famille["psf_id"];
				$objectifs[1]["famille"][$fam_id]["sous_famille"][$s_fam_id]=array(
					"libelle" => $famille["psf_libelle"],
					"sous_sous_famille" => array(),
					"mois_1" =>0,
					"mois_2" =>0,
					"mois_3" =>0,
					"mois_4" =>0,
					"mois_5" =>0,
					"mois_6" =>0,
					"mois_7" =>0,
					"mois_8" =>0,
					"mois_9" =>0,
					"mois_10" =>0,
					"mois_11" =>0,
					"mois_12" =>0,
					"total" =>0,
					"autre" =>0
				);
			}
		}
		if(!empty($famille["pss_id"])){
			if($famille["pss_id"]!=$s_s_fam_id){
				$s_s_fam_id=$famille["pss_id"];
				$objectifs[1]["famille"][$fam_id]["sous_famille"][$s_fam_id]["sous_sous_famille"][$s_s_fam_id]=array(
					"libelle" => $famille["pss_libelle"],
					"mois_1" =>0,
					"mois_2" =>0,
					"mois_3" =>0,
					"mois_4" =>0,
					"mois_5" =>0,
					"mois_6" =>0,
					"mois_7" =>0,
					"mois_8" =>0,
					"mois_9" =>0,
					"mois_10" =>0,
					"mois_11" =>0,
					"mois_12" =>0,
					"total" =>0,
					"autre" =>0
				);
			}
		}
		
		
	}
}


// LES OBJECTIFS RENSEIGNE

$objectif_valide=false;

$sql="SELECT * FROM Commerciaux_Objectifs WHERE cob_commercial=" . $commercial . " AND cob_exercice=" . $exercice . ";";
$req=$ConnSoc->query($sql);
$d_donnees=$req->fetchAll();
if(!empty($d_donnees)){
	foreach($d_donnees as $d){
		
		if(!empty($d["cob_valide"]) AND !$objectif_valide){
			$objectif_valide=true;	
		}
		$cob_autre=false;
		if(!empty($d["cob_autre"])){
			$cob_autre=true;	
		}
		
		if($d["cob_categorie"]==1){
			$i=0;
			for($i=1;$i<=12;$i++){
				if(!empty($d["cob_sous_sous_famille"])){
					$objectifs[$d["cob_categorie"]]["famille"][$d["cob_famille"]]["sous_famille"][$d["cob_sous_famille"]]["sous_sous_famille"][$d["cob_sous_sous_famille"]]["mois_" . $i]=$d["cob_objectif_" . $i];
				}elseif(!empty($d["cob_sous_famille"])){
					$objectifs[$d["cob_categorie"]]["famille"][$d["cob_famille"]]["sous_famille"][$d["cob_sous_famille"]]["mois_" . $i]=$d["cob_objectif_" . $i];
					
				}elseif(!empty($d["cob_famille"])){
					$objectifs[$d["cob_categorie"]]["famille"][$d["cob_famille"]]["mois_" . $i]=$d["cob_objectif_" . $i];
				
				}elseif(!empty($d["cob_categorie"])){
					$objectifs[$d["cob_categorie"]]["mois_" . $i]=$d["cob_objectif_" . $i];
					
				}
				
			}
			if(!empty($d["cob_sous_sous_famille"])){
				$objectifs[$d["cob_categorie"]]["famille"][$d["cob_famille"]]["sous_famille"][$d["cob_sous_famille"]]["sous_sous_famille"][$d["cob_sous_sous_famille"]]["total"]=$d["cob_objectif_total"];
				$objectifs[$d["cob_categorie"]]["famille"][$d["cob_famille"]]["sous_famille"][$d["cob_sous_famille"]]["sous_sous_famille"][$d["cob_sous_sous_famille"]]["autre"]=$cob_autre;
			}elseif(!empty($d["cob_sous_famille"])){
				$objectifs[$d["cob_categorie"]]["famille"][$d["cob_famille"]]["sous_famille"][$d["cob_sous_famille"]]["total"]=$d["cob_objectif_total"];
				$objectifs[$d["cob_categorie"]]["famille"][$d["cob_famille"]]["sous_famille"][$d["cob_sous_famille"]]["autre"]=$cob_autre;
			}elseif(!empty($d["cob_famille"])){
				$objectifs[$d["cob_categorie"]]["famille"][$d["cob_famille"]]["total"]=$d["cob_objectif_total"];
				$objectifs[$d["cob_categorie"]]["famille"][$d["cob_famille"]]["autre"]=$cob_autre;
			}elseif(!empty($d["cob_categorie"])){
				$objectifs[$d["cob_categorie"]]["total"]=$d["cob_objectif_total"];
				$objectifs[$d["cob_categorie"]]["autre"]=$cob_autre;
			}		
		}else{
			
			// DIVERS
			$i=0;
			for($i=1;$i<=12;$i++){
				$objectifs[$d["cob_categorie"]]["mois_" . $i]=$d["cob_objectif_" . $i];
			}
			$objectifs[$d["cob_categorie"]]["total"]=$d["cob_objectif_total"];
			$objectifs[$d["cob_categorie"]]["autre"]=$cob_autre;
		}
	}	
}
// securité : les RE (ou toutes autres personnes) ayant accès perdent ce dernier dès que les objectifs sont validés.
if (!$drt_validation AND $objectif_valide) {
	header("location : deconnect.php");
	die();
}
/*
echo("<pre>");
print_r($objectifs);
echo("</pre>");
die();*/

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<form method="post" action="commercial_objectif_enr.php" >
			<div>
				<input type="hidden" name="commercial" value="<?=$commercial?>" />
				<input type="hidden" name="exercice" value="<?=$exercice?>" />
			</div>
			<div id="main">
		<?php	include "includes/header_def.inc.php"; ?>
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">
					
						<div class="table-responsive">
							<table class="table table-striped" >
								<thead>
									<tr class="dark2" >
										<th colspan="2" >&nbsp;</th>
										<th>Autres</th>
										<th>Avril</th>
										<th>Mai</th>
										<th>Juin</th>
										<th>Juillet</th>
										<th>Août</th>
										<th>Septembre</th>
										<th>Octobre</th>
										<th>Novembre</th>
										<th>Décembre</th>
										<th>Janvier</th>
										<th>Fevrier</th>							
										<th>Mars</th>
										<th>Total</th>
									</tr>
								</thead>					
					<?php		if(!empty($objectifs)){
									$num_ligne=0; ?>
									<tbody>
					<?php				foreach($objectifs as $cat_id => $categorie){ 
											if($cat_id>0){
												$num_ligne++; ?>						
												<tr>
													<td><?=$categorie["libelle"]?></td>
						<?php						if(!empty($categorie["famille"])){ ?>
														<td>
															<button type="button" class="btn btn-default btn-sm categorie" data-categorie="<?=$cat_id?>" >
																<i id="minus_cat_<?=$cat_id?>" class="fa fa-minus" ></i>
															</button>
														</td>
						<?php						}else{ ?>
														<td>&nbsp;</td>
						<?php						}?>
													<td class="text-center" >
														<input type="checkbox" class="autre" data-ligne="<?=$num_ligne?>" name="cat_autre_<?=$cat_id?>" <?php if($categorie["autre"]) echo("checked='checked'"); ?> />															
													</td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_4" value="<?=$categorie["mois_4"]?>" /></td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_5" value="<?=$categorie["mois_5"]?>" /></td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_6" value="<?=$categorie["mois_6"]?>" /></td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_7" value="<?=$categorie["mois_7"]?>" /></td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_8" value="<?=$categorie["mois_8"]?>" /></td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_9" value="<?=$categorie["mois_9"]?>" /></td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_10" value="<?=$categorie["mois_10"]?>" /></td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_11" value="<?=$categorie["mois_11"]?>" /></td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_12" value="<?=$categorie["mois_12"]?>" /></td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_1" value="<?=$categorie["mois_1"]?>" /></td>
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_2" value="<?=$categorie["mois_2"]?>" /></td>					
													<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_<?=$cat_id?>_3" value="<?=$categorie["mois_3"]?>" /></td>
													<td><input type="text" size="8" class="total" data-ligne="<?=$num_ligne?>"  id="total_<?=$num_ligne?>" name="cat_<?=$cat_id?>_total" value="<?=$categorie["total"]?>" <?php if(!$categorie["autre"]) echo("readonly"); ?> /></td>
												</tr>
						<?php					if(!empty($categorie["famille"])){
													foreach($categorie["famille"] as $fam_id => $famille){
														$num_ligne++; ?>						
														<tr class="categorie_<?=$cat_id?>" >
															<td style="padding-left:20px;" ><?=$famille["libelle"]?></td>
															<td>
						<?php									if(!empty($famille["sous_famille"])){ ?>
																	<button type="button" class="btn btn-default btn-sm famille" data-famille="<?=$fam_id?>" >
																		<i id="minus_fam_<?=$fam_id?>" class="fa fa-minus" ></i>
																	</button>
						<?php									}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td class="text-center" >
																<input type="checkbox" class="autre" data-ligne="<?=$num_ligne?>" name="fam_autre_<?=$fam_id?>" <?php if($famille["autre"]) echo("checked='checked'"); ?> />															
															</td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_4" value="<?=$famille["mois_4"]?>" /></td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_5" value="<?=$famille["mois_5"]?>" /></td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_6" value="<?=$famille["mois_6"]?>" /></td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_7" value="<?=$famille["mois_7"]?>" /></td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_8" value="<?=$famille["mois_8"]?>" /></td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_9" value="<?=$famille["mois_9"]?>" /></td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_10" value="<?=$famille["mois_10"]?>" /></td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_11" value="<?=$famille["mois_11"]?>" /></td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_12" value="<?=$famille["mois_12"]?>" /></td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_1" value="<?=$famille["mois_1"]?>" /></td>
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_2" value="<?=$famille["mois_2"]?>" /></td>					
															<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="fam_<?=$fam_id?>_3" value="<?=$famille["mois_3"]?>" /></td>
															<td><input type="text" size="8" class="total" data-ligne="<?=$num_ligne?>" id="total_<?=$num_ligne?>" name="fam_<?=$fam_id?>_total" value="<?=$famille["total"]?>" <?php if(!$categorie["autre"]) echo("readonly"); ?> /></td>
														</tr>
							<?php						if(!empty($famille["sous_famille"])){
															foreach($famille["sous_famille"] as $s_fam_id => $sous_famille){ 
																$num_ligne++; ?>						
																<tr class="categorie_<?=$cat_id?> famille_<?=$fam_id?>" >
																	<td style="padding-left:40px;" ><?=$sous_famille["libelle"]?></td>
														<?php		if(!empty($sous_famille["sous_sous_famille"])){ ?>
																		<td>
																			<button type="button" class="btn btn-default btn-sm sous_famille" data-sous_famille="<?=$s_fam_id?>" >
																				<i id="minus_s_fam_<?=$s_fam_id?>" class="fa fa-minus" ></i>
																			</button>
																		</td>
														<?php		}else{ ?>
																		<td>&nbsp;</td>
														<?php		} ?>
																	<td class="text-center" >
																		<input type="checkbox" class="autre" data-ligne="<?=$num_ligne?>" name="s_fam_autre_<?=$s_fam_id?>" <?php if($sous_famille["autre"]) echo("checked='checked'"); ?> />															
																	</td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_4" value="<?=$sous_famille["mois_4"]?>" /></td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_5" value="<?=$sous_famille["mois_5"]?>" /></td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_6" value="<?=$sous_famille["mois_6"]?>" /></td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_7" value="<?=$sous_famille["mois_7"]?>" /></td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_8" value="<?=$sous_famille["mois_8"]?>" /></td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_9" value="<?=$sous_famille["mois_9"]?>" /></td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_10" value="<?=$sous_famille["mois_10"]?>" /></td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_11" value="<?=$sous_famille["mois_11"]?>" /></td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_12" value="<?=$sous_famille["mois_12"]?>" /></td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_1" value="<?=$sous_famille["mois_1"]?>" /></td>
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_2" value="<?=$sous_famille["mois_2"]?>" /></td>					
																	<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_3" value="<?=$sous_famille["mois_3"]?>" /></td>
																	<td><input type="text" size="8" class="total" data-ligne="<?=$num_ligne?>"  id="total_<?=$num_ligne?>" name="s_fam_<?=$s_fam_id?>_total" value="<?=$sous_famille["total"]?>" <?php if(!$categorie["autre"]) echo("readonly"); ?> /></td>
																</tr>
								<?php							if(!empty($sous_famille["sous_sous_famille"])){
																	foreach($sous_famille["sous_sous_famille"] as $s_s_fam_id => $sous_sous_famille){ 
																		$num_ligne++; ?>						
																		<tr class="categorie_<?=$cat_id?> famille_<?=$fam_id?> sous_famille_<?=$s_fam_id?>" >
																			<td style="padding-left:60px;" ><?=$sous_sous_famille["libelle"]?></td>
																			<td>&nbsp;</td>
																			<td class="text-center" >
																				<input type="checkbox" class="autre" data-ligne="<?=$num_ligne?>" name="s_s_fam_autre_<?=$s_s_fam_id?>" <?php if($sous_sous_famille["autre"]) echo("checked='checked'"); ?> />															
																			</td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_4" value="<?=$sous_sous_famille["mois_4"]?>" /></td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_5" value="<?=$sous_sous_famille["mois_5"]?>" /></td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_6" value="<?=$sous_sous_famille["mois_6"]?>" /></td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_7" value="<?=$sous_sous_famille["mois_7"]?>" /></td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_8" value="<?=$sous_sous_famille["mois_8"]?>" /></td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_9" value="<?=$sous_sous_famille["mois_9"]?>" /></td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_10" value="<?=$sous_sous_famille["mois_10"]?>" /></td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_11" value="<?=$sous_sous_famille["mois_11"]?>" /></td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_12" value="<?=$sous_sous_famille["mois_12"]?>" /></td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_1" value="<?=$sous_sous_famille["mois_1"]?>" /></td>
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_2" value="<?=$sous_sous_famille["mois_2"]?>" /></td>					
																			<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_3" value="<?=$sous_sous_famille["mois_3"]?>" /></td>
																			<td><input type="text" size="8" class="total" data-ligne="<?=$num_ligne?>" id="total_<?=$num_ligne?>" name="s_s_fam_<?=$s_s_fam_id?>_total" value="<?=$sous_sous_famille["total"]?>" <?php if(!$categorie["autre"]) echo("readonly"); ?> /></td>
																		</tr>
									<?php							} 
																}
															}
														}
													} 
												}
											}
										}	
										
										$num_ligne++; ?>						
										<tr>
											<td>Divers</td>
											<td>&nbsp;</td>
											<td class="text-center" >
												<input type="checkbox" class="autre" data-ligne="<?=$num_ligne?>" name="cat_autre_0" <?php if($objectifs[0]["autre"]) echo("checked='checked'"); ?> />															
											</td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_4" value="<?=$objectifs[0]["mois_4"]?>" /></td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_5" value="<?=$objectifs[0]["mois_5"]?>" /></td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_6" value="<?=$objectifs[0]["mois_6"]?>" /></td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_7" value="<?=$objectifs[0]["mois_7"]?>" /></td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_8" value="<?=$objectifs[0]["mois_8"]?>" /></td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_9" value="<?=$objectifs[0]["mois_9"]?>" /></td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_10" value="<?=$objectifs[0]["mois_10"]?>" /></td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_11" value="<?=$objectifs[0]["mois_11"]?>" /></td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_12" value="<?=$objectifs[0]["mois_12"]?>" /></td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_1" value="<?=$objectifs[0]["mois_1"]?>" /></td>
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_2" value="<?=$objectifs[0]["mois_2"]?>" /></td>					
											<td><input type="text" size="6" class="case case_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" name="cat_0_3" value="<?=$objectifs[0]["mois_3"]?>" /></td>
											<td><input type="text" size="8" class="total" data-ligne="<?=$num_ligne?>" id="total_<?=$num_ligne?>" name="cat_0_total" value="<?=$objectifs[0]["total"]?>" <?php if(!$categorie["autre"]) echo("readonly"); ?> /></td>
										</tr>
									</tbody>	
					<?php		} ?>					
							</table>					
						</div>		
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="commercial_result.php?commercial=<?=$commercial?>&exercice=<?=$exercice?>" class="btn btn-default btn-sm" >
							<i class="fa fa-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle text-center" >
			<?php		if($drt_validation){ ?>
							<div class="admin-form theme-primary ">
								<div class="row" >
									<div class="col-offset-md-2 col-md-8" >
										<div class="option-group field">
											<label class="option option-dark">
												<input type="checkbox" name="cob_valide" value="cob_valide" <?php if($objectif_valide) echo("checked"); ?> >
												<span class="checkbox"></span>Objectif validé
											</label>
										</div>
									</div>
								</div>
							</div>
			<?php		} ?>
					</div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm" role="button" >
							<i class="fa fa-save"></i>
							Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php		
		include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
				$(".categorie").click(function(){
					$categorie=$(this).data("categorie");
					if($("#minus_cat_" + $categorie).prop("class")=="fa fa-minus"){
						// on masque
						$(".categorie_" + $categorie).hide();
						$("#minus_cat_" + $categorie).prop("class","fa fa-plus")
					}else{
						$(".categorie_" + $categorie).show();
						$("#minus_cat_" + $categorie).prop("class","fa fa-minus")
					}
				});
				$(".famille").click(function(){
					$famille=$(this).data("famille");
					if($("#minus_fam_" + $famille).prop("class")=="fa fa-minus"){
						// on masque
						$(".famille_" + $famille).hide();
						$("#minus_fam_" + $famille).prop("class","fa fa-plus")
					}else{
						$(".famille_" + $famille).show();
						$("#minus_fam_" + $famille).prop("class","fa fa-minus")
					}
				});
				$(".sous_famille").click(function(){
					$sous_famille=$(this).data("sous_famille");
					if($("#minus_s_fam_" + $sous_famille).prop("class")=="fa fa-minus"){
						// on masque
						$(".sous_famille_" + $sous_famille).hide();
						$("#minus_s_fam_" + $sous_famille).prop("class","fa fa-plus")
					}else{
						$(".sous_famille_" + $sous_famille).show();
						$("#minus_s_fam_" + $sous_famille).prop("class","fa fa-minus")
					}
				});
				$(".case").blur(function(){
					console.log("case blur");
					$ligne=$(this).data("ligne");
					console.log("ligne " + $ligne);
					$total=0;
					$(".case_"+ $ligne).each(function(){
						$total=1*$total+1*$(this).val();
					});
					$("#total_" + $ligne).val($total);
				});
					
				$(".autre").click(function(){
					$ligne=$(this).data("ligne");
					if($(this).is(":checked")){
						$("#total_" + $ligne).prop("readonly",false);
					}else{
						$("#total_" + $ligne).val(0);
						$("#total_" + $ligne).prop("readonly",true);
					}
				});	
				
				$(".total").blur(function(){
					$ligne=$(this).data("ligne");
					$(".case_" + $ligne).val(0);
				});	
			
			});						
		</script>
	
	</body>
</html>
