<?php

// ECRAN DE MODIFICATION DES SUSPECTS

include "includes/controle_acces.inc.php";
error_reporting( error_reporting() & ~E_NOTICE );

// CONTROLE DROITS ACCESS
if(!$_SESSION["acces"]["acc_droits"][23]){
	header("location: deconnect.php");
	die();
}

include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");
require "modeles/mod_get_commerciaux.php";
require "modeles/mod_parametre.php";
require "modeles/mod_get_cli_classifications.php";
require "modeles/mod_get_cli_s_classifications.php";
require "modeles/mod_get_activites_secteurs.php";
require "modeles/mod_get_activites_sous_secteurs.php";
require "modeles/mod_clients_get.php";


	// MODIFICATION D'UNE FICHE SUSPECT

$suspect=0;
if(!empty($_GET['suspect'])){
	$suspect=intval($_GET['suspect']);
}
if($suspect==0){
	echo("Erreur : suspect inconnu !");
	die();	
}

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=intval($_SESSION['acces']["acc_agence"]);	
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);	
}


// Le suspect
$sql="SELECT * FROM Suspects WHERE sus_id=" . $suspect . ";";
$req = $ConnSoc->query($sql);
$d_suspect = $req->fetch();
if(empty($d_suspect)){
	echo("Erreur : suspect inconnu !");
	die();
}else{
	$sus_stagiaire_naiss="";
	if(!empty($d_suspect["sus_stagiaire_naiss"])){
		$stagiaire_naiss=date_create_from_format('Y-m-d',$d_suspect["sus_stagiaire_naiss"]);
		if(!is_bool($stagiaire_naiss)){
			$sus_stagiaire_naiss=$stagiaire_naiss->format("d/m/Y");
		}
	}
}

// categorie accessible pour le suspect
$sql="SELECT cca_id,cca_libelle FROM Clients_Categories WHERE NOT cca_id=5";
if($acc_agence!=4 OR !$_SESSION['acces']["acc_droits"][8]){
	$sql.=" AND NOT cca_id=2";
}
$sql.=" ORDER BY cca_libelle;";
$req = $Conn->query($sql);
$d_categories = $req->fetchAll();

// sous categorie

if($d_suspect["sus_categorie"]>0){
	
	$sql="SELECT csc_id,csc_libelle FROM Clients_Sous_Categories WHERE csc_categorie=" . $d_suspect["sus_categorie"] . ";";
	$req = $Conn->query($sql);
	$d_sous_categories=$req->fetchAll();
}

// catégorie de financeur => sous_categorie de DO

$sql="SELECT csc_id,csc_libelle FROM Clients_Sous_Categories WHERE csc_categorie=4;";
$req = $Conn->query($sql);
$d_opca_type=$req->fetchAll();

// liste des financeurs
if($d_suspect["sus_categorie"]!=4){
	if(!empty($d_suspect["sus_opca_type"])){
		$d_opca=get_clients(4,$d_suspect["sus_opca_type"],0,null);
		
	}
}


// LA SOCIETE

$sql="SELECT soc_nom,soc_agence FROM Societes WHERE soc_id=" . $acc_societe . ";";
$req = $Conn->query($sql);
$d_societe=$req->fetch();

//agence si societe gere
if($d_societe["soc_agence"]==1 AND $acc_agence==0){
	$sql="SELECT age_id,age_nom FROM Agences WHERE age_societe=" . $acc_societe . ";";
	$req = $Conn->query($sql);
	$d_agences=$req->fetchAll();
}elseif($d_suspect["sus_agence"]>0){
	$sql="SELECT * FROM Agences WHERE age_id=" . $d_suspect["sus_agence"] . ";";
	$req = $Conn->query($sql);
	$d_agence=$req->fetch();	
}
// LES COMMERCIAUX POUVANT ETRE UTILISE
if(!isset($d_agences) OR $d_suspect["sus_agence"]>0){
	$sql="SELECT com_id,com_label_1,com_label_2 FROM Commerciaux";
	$mil="";
	if($acc_agence>0){
		$mil=" AND com_agence=" . $acc_agence;
	}elseif($d_suspect["sus_agence"]>0){
		$mil=" AND com_agence=" . $d_suspect["sus_agence"];
	}
	if(!$_SESSION["acces"]["acc_droits"][6]){
		$mil.=" AND com_ref_1=" . $_SESSION['acces']["acc_ref_id"];
	}
	if(!empty($mil)){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY com_label_1,com_label_2";
	$req = $ConnSoc->query($sql);
	$d_commerciaux=$req->fetchAll();
}else{
	// l'utilisateur doit selectionner l'agence ce qui permettra d'obtenir la liste des commerciaux
	$d_commerciaux=array();
}

// Adresse d'intervention par defaut

$sql="SELECT * FROM Suspects_Adresses WHERE sad_ref_id=" . $suspect . " AND sad_defaut AND sad_type=1;";
$req = $ConnSoc->query($sql);
$d_ad_intervention=$req->fetch();

// Adresse de facturation par defaut

$sql="SELECT * FROM Suspects_Adresses WHERE sad_ref_id=" . $suspect . " AND sad_defaut AND sad_type=2;";
$req = $ConnSoc->query($sql);
$d_ad_facturation=$req->fetch();
if(!empty($d_ad_facturation)){
	if(!empty($d_suspect["sus_siren"]) AND !empty($d_ad_facturation["sad_siret"])){
		$d_suspect["sus_siret"]=$d_suspect["sus_siren"] . " " . $d_ad_facturation["sad_siret"];
	}else{
		$d_suspect["sus_siret"]="";
	}
}

// FONCTIONS DES CONTACTS

$sql="SELECT cfo_id,cfo_libelle FROM Contacts_Fonctions ORDER BY cfo_libelle;";
$req = $Conn->query($sql);
$d_contact_fonctions=$req->fetchAll();

// CONTACT PAR DEFAUT

	$sql = "";
			// particulier
	$sql="SELECT * FROM Suspects_Contacts WHERE sco_id=" . $d_suspect["sus_contact"];


	if(!empty($sql)){
		$req = $ConnSoc->query($sql);
		$d_contact=$req->fetch();
	}else{
		$d_contact = array();
	}


// Classification
$d_classifications=get_cli_classifications();
if(!empty($d_suspect["sus_classification"])){
	/*$d_sous_classifications=get_cli_s_classifications($d_suspect["sus_classification"]);*/
	$sql="SELECT * FROM Clients_classifications_categories WHERE ccc_classification=" . $d_suspect["sus_classification"];
	$req = $Conn->query($sql);
	$d_classifications_categories = $req->fetchAll();
}else{
	$d_classifications_categories = array();
}

if(!empty($d_suspect["sus_classification"])){
	/*$d_sous_classifications=get_cli_s_classifications($d_suspect["sus_classification"]);*/
	$sql="SELECT * FROM Clients_classifications_types WHERE cct_classification=" . $d_suspect["sus_classification"] . " ORDER BY cct_code";
	$req = $Conn->query($sql);
	$d_classifications_types = $req->fetchAll();
}else{
	$d_classifications_types = array();
}

	// LES PRESCRIPTEURS
	
	$prescripteur_verrou=false;
	
	if(!empty($d_suspect["sus_prescripteur"])){
		
		$sql="SELECT cpr_import,cpr_libelle,cpr_ouvert FROM Clients_Prescripteurs WHERE cpr_id=" . $d_suspect["sus_prescripteur"] . ";";
		$req = $Conn->query($sql);
		$d_prescripteur=$req->fetch();
		if(!empty($d_prescripteur)){
			if($d_prescripteur["cpr_import"]==1){
				$prescripteur_verrou=true;
			}
		}
	}
	if(!$prescripteur_verrou){
		$sql="SELECT cpr_id,cpr_libelle,cpr_ouvert FROM Clients_Prescripteurs WHERE NOT cpr_archive AND NOT cpr_import ORDER BY cpr_libelle;";
		$req = $Conn->query($sql);
		$d_prescripteurs=$req->fetchAll();
	}
	

// CODE APE
$sql="SELECT * FROM Ape ORDER BY ape_code,ape_libelle";
$req = $Conn->query($sql);
$d_apes = $req->fetchAll();



// on écrase la recherche
/*
unset($_SESSION['client_tableau']);
if(isset($_GET["menu_actif"])){
	unset($_SESSION['retour']);
}*/
?>
<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8"> 
		<title>SI2P - Orion - Modification d'un suspect</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<!-- Plugin -->
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<!-- Si2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

	</head>
	<body class="sb-top sb-top-sm ">
		
		<form method="post" action="suspect_mod_enr.php" >
			<div>
				<input type="hidden" name="suspect" value="<?=$suspect?>" />
				<input type="hidden" name="adresse_int" value="<?=$d_ad_intervention["sad_id"]?>" />
				<input type="hidden" name="adresse_fac" value="<?=$d_ad_facturation["sad_id"]?>" />
				<input type="hidden" name="contact" value="<?=$d_contact["sco_id"]?>" />
			</div>
			<div id="main">
  <?php			include "includes/header_def.inc.php";   ?>
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper" >				
					<section id="content" class="animated fadeIn">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<!-- deb PANEL FORM -->
								<div class="admin-form theme-dark ">
									<div class="panel heading-border panel-dark">
										
										<div class="panel-body bg-light">										
											
												<div class="content-header">
													<h2>Editer un <b class="text-dark">suspect</b></h2>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Informations générales</span> 
														</div>
													</div>
												</div>
												
												<div class="row" id="suspect_ok" style="display:none;" >
													<div class="col-md-12 alert alert-success" ></div>
												</div>

												<div class="row" >
													<div class="col-md-4">
														<label for="code" >Code :</label>
														<div class="field prepend-icon">
															<input type="text" name="sus_code" id="code" class="gui-input nom" placeholder="Code" required value="<?=$d_suspect["sus_code"]?>" />
															<span class="field-icon">
																<i class="fa fa-code"></i>
															</span>
														</div>
													</div>
													<div class="col-md-8">
														<label for="nom" >Nom :</label>
														<div class="field prepend-icon">
															<input type="text" name="sus_nom" id="nom" class="gui-input" placeholder="Nom" required value="<?=$d_suspect["sus_nom"]?>" />
															<span class="field-icon">
																<i class="fa fa-building-o"></i>
															</span>
														</div>
													</div>
												</div>
												<div class="row mt15">
													<div class="col-md-4 pt15">
														Société : <b><?=$d_societe["soc_nom"]?></b>
													</div>
											<?php 	if(isset($d_agences)){ ?>
														<div class="col-md-4" >
															<div class="section">
																<select id="agence" name="sus_agence" class="select2" >
																	<option value="0">Agence...</option>
											<?php 					foreach($d_agences as $a){ 
																		if($d_suspect["sus_agence"]==$a["age_id"]){
																			echo("<option value='" . $a["age_id"] . "' selected >" . $a["age_nom"] . "</option>");	
																		}else{
																			echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");	
																		}
																	} ?>
																</select>
															</div>
														</div>
											<?php	}elseif(isset($d_agence)){ ?>
														<div class="col-md-4 pt15" >
															Agence : <b><?=$d_agence["age_nom"]?></b>
															<input type="hidden" name="sus_agence" value="<?=$d_agence["age_id"]?>" />
														</div>												
											<?php	}
													if(count($d_commerciaux)>1 OR isset($d_agences)){ ?>				
														<div class="col-md-4">													
															<div class="section">
																<select name="sus_commercial" id="commercial" class="select2" >
																	<option value="0">Commercial...</option>
										<?php						if(!empty($d_commerciaux)){
																		foreach($d_commerciaux as $com){
																			if($d_suspect["sus_commercial"]==$com["com_id"]){
																				echo("<option value='" . $com["com_id"] . "' selected >" . $com["com_label_1"] . " "  . $com["com_label_2"] . "</option>");	
																			}else{
																				echo("<option value='" . $com["com_id"] . "' >" . $com["com_label_1"] . " "  . $com["com_label_2"] . "</option>");
																			}
											 
																		}
																	} ?>
																</select>
															</div>
														</div>
										<?php		}elseif(!empty($d_commerciaux)){ ?>
														<div class="col-md-4">													
															<div class="section">
																Commercial : <b><?=$d_commerciaux[0]["com_label_2"] . " " . $d_commerciaux[0]["com_label_1"]?></b>
																<input type="hidden" name="sus_commercial" id="commercial" value="<?= $d_commerciaux[0]['com_id']?>" />
															</div>
														</div>
										<?php		} ?>												
												</div>
												
												<div class="row mt15">
													<div class="col-md-6">
														<div class="section">
															<label for="categorie">Catégorie</label>
															<select id="categorie" name="sus_categorie" class="select2" required >
																<!-- <option value="0" >Sélectionner une catégorie...</option> -->
													<?php		if(!empty($d_categories)){
																	foreach($d_categories as $dc){
																		if($d_suspect["sus_categorie"]==$dc["cca_id"]){
																			echo("<option value='" . $dc["cca_id"] . "' selected >" . $dc["cca_libelle"] . "</option>");
																		}else{
																			echo("<option value='" . $dc["cca_id"] . "' >" . $dc["cca_libelle"] . "</option>");
																		}
																	}
																} ?>
															</select>
														</div>
													</div>
													<div class="col-md-6" id="sous_categorie_bloc" <?php if(empty($d_sous_categories)) echo("style='display:none;'"); ?> >
														<label for="sous_categorie">Sous-catégorie</label>
														<select id="sous_categorie" name="sus_sous_categorie" class="select2" >
															<option value="0">Sélectionner une sous catégorie...</option>  
												<?php		if(!empty($d_sous_categories)){
																foreach($d_sous_categories as $dsc){
																	if($d_suspect["sus_sous_categorie"]==$dsc["csc_id"]){
																		echo("<option value='" . $dsc["csc_id"] . "' selected >" . $dsc["csc_libelle"] . "</option>");
																	}else{
																		echo("<option value='" . $dsc["csc_id"] . "' >" . $dsc["csc_libelle"] . "</option>");
																	}
																}
															} ?>																
														</select>                                 
													</div>
												</div>

												<div class="row">
												
													<!-- COLONNE INTERVENTION -->
													<div class="col-md-6">
														
														<div class="row bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Adresse d'intervention</span>
																</div>
															</div>
														</div>
														
														<div class="row bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?>>
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_libelle1" id="adr_libelle1" class="gui-input" placeholder="Libellé"  value="<?=$d_ad_intervention['sad_libelle']?>">
																	<label for="cli_ad1" class="field-icon">
																		<i class="fa fa-tag"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="row bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?>>
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_nom1" id="adr_nom1" class="gui-input" placeholder="Nom"  value="<?=$d_ad_intervention['sad_nom']?>">
																	<label for="cli_ad1" class="field-icon">
																			<i class="fa fa-tag"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="row bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_service1" id="adr_service1" class="gui-input" placeholder="Service"  value="<?=$d_ad_intervention['sad_service']?>">
																	<label for="cli_ad1" class="field-icon">
																		<i class="fa fa-tag"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="row bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_ad11" id="adr_ad11" class="gui-input" placeholder="Adresse"  value="<?=$d_ad_intervention['sad_ad1']?>">
																	<label for="cli_ad1" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="row bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_ad21" class="gui-input" id="adr_ad21" placeholder="Adresse (Complément 1)" value="<?=$d_ad_intervention['sad_ad2']?>">
																	<label for="cli_ad2" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</label>
																</div>
															</div>
														</div>
														
														<div class="row bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_ad31" id="adr_ad31" class="gui-input" placeholder="Adresse (Complément 2)" value="<?=$d_ad_intervention['sad_ad3']?>">
																	<label for="cli_ad3" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</label>
																</div>
															</div>
														</div>
														
														<div class="row bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-3">
																<div class="field prepend-icon">
																	<input type="text" id="adr_cp1" name="sad_cp1" class="gui-input code-postal" placeholder="Code postal"  value="<?=$d_ad_intervention['sad_cp']?>">
																	<label for="cli_cp" class="field-icon">
																		<i class="fa fa fa-certificate"></i>
																	</label>
																</div>
															</div>
															<div class="col-md-9">
																<div class="field prepend-icon">
																	<input type="text" name="sad_ville1" id="adr_ville1" class="gui-input nom" placeholder="Ville"  value="<?=$d_ad_intervention['sad_ville']?>">
																	<label for="cli_ville" class="field-icon">
																		<i class="fa fa fa-building"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="row mt15 bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-12 text-center">Zone géographique</div>
														</div>
														<div class="row bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-12 text-center">
																<label for="geo11" class=" mt15 option option-dark">
																	<input type="radio" id="geo11" class="geo1" name="sad_geo1" value="1" <?php if($d_ad_intervention['sad_geo']==1) echo("checked"); ?>>
																	<span class="radio"></span> France
																</label>
																<label for="geo21" class=" mt15 option option-dark">
																	<input type="radio" id="geo21" class="geo1" name="sad_geo1" value="2" <?php if($d_ad_intervention['sad_geo']==2) echo("checked"); ?> >
																	<span class="radio"></span> UE
																</label>
																<label for="geo31" class=" mt15 option option-dark">
																	<input type="radio" id="geo31" class="geo1" name="sad_geo1" value="3" <?php if($d_ad_intervention['sad_geo']==3) echo("checked"); ?> >
																	<span class="radio"></span> Autre
																</label>
															</div>
														</div>	
														
														<div class="row mt15 bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-12">
																<label class="option option-dark">
																	<input type="checkbox" id="similaire" value="oui">
																	<span class="checkbox"></span>
																	<label for="similaire">Utiliser comme adresse de facturation</label>
																</label>
															</div>
														</div>
														
														<!-- CONTACT PAR DEFAUT -->
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span class="contact_libelle">Contact par défaut du lieu d'intervention</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<div class="field select">
																		<select name="sco_fonction" id="fonction" >
																			<option value="0">Sélectionner une fonction...</option>
																	<?php	if(!empty($d_contact_fonctions)){
																				foreach($d_contact_fonctions as $dcf){
																					if($d_contact["sco_fonction"]==$dcf["cfo_id"]){
																						echo("<option value='" . $dcf["cfo_id"] . "' selected >" . $dcf["cfo_libelle"] . "</option>");
																					}else{
																						echo("<option value='" . $dcf["cfo_id"] . "'>" . $dcf["cfo_libelle"] . "</option>");
																					}
																					
																				}
																			} ?>
																			<option value="autre" <?php if(!empty($d_contact["sco_fonction_nom"])) echo("selected"); ?> >Autre fonction</option>	
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
															<div class="col-md-6" id="fonction_nom_bloc" <?php if(empty($d_contact["sco_fonction_nom"])) echo("style='display:none;'"); ?> >
																<div class="section">
																	<div class="field prepend-icon">
																	  <input type="text" name="sco_fonction_nom" id="fonction_nom" class="gui-input" placeholder="Autre fonction" value="<?=$d_contact["sco_fonction_nom"]?>" />
																	  <label for="cli_nom" class="field-icon">
																		<i class="fa fa-tag"></i>
																	  </label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<div class="field select">
																		<select name="sco_titre" id="con_titre" >
																			<option value="0" <?php if($d_contact["sco_titre"]==0) echo("selected"); ?> >Civilité...</option>
																			<option value="1" <?php if($d_contact["sco_titre"]==1) echo("selected"); ?> >Monsieur</option>
																			<option value="2" <?php if($d_contact["sco_titre"]==2) echo("selected"); ?> >Madame</option>
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="text" name="sco_nom" id="con_nom" class="gui-input nom" placeholder="Nom"  value="<?=$d_contact['sco_nom']?>">
																		<label for="con_nom" class="field-icon">
																			<i class="fa fa-user"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																  <div class="section">
																	<div class="field prepend-icon">
																	  <input type="text" name="sco_prenom" id="con_prenom" class="gui-input prenom" placeholder="Prénom"  value="<?=$d_contact['sco_prenom']?>">
																	  <label for="con_prenom" class="field-icon">
																		<i class="fa fa-tag"></i>
																	  </label>
																	</div>
																  </div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																<div class="field prepend-icon">
																  <input name="sco_tel" id="con_tel" type="tel" class="gui-input telephone" placeholder="Numéro de téléphone"  value="<?=$d_contact['sco_tel']?>">
																  <label for="cli_tel" class="field-icon">
																	<i class="fa fa fa-phone"></i>
																  </label>
																</div>
																</div>
															</div>
															<div class="col-md-6">
																  <div class="section">
																	<div class="field prepend-icon">
																	  <input name="sco_fax" id="con_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax"  value="<?=$d_contact['sco_fax']?>">
																	  <label for="cli_fax" class="field-icon">
																		<i class="fa fa-phone-square"></i>
																	  </label>
																	</div>
																  </div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																  <div class="section">
																	<div class="field prepend-icon">
																	  <input name="sco_portable" id="con_portable" type="tel" class="gui-input telephone" placeholder="Numéro de portable"  value="<?=$d_contact['sco_portable']?>">
																	  <label for="cli_tel" class="field-icon">
																		<i class="fa fa fa-mobile"></i>
																	  </label>
																	</div>
																  </div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<div class="field prepend-icon">
																	  <input type="email" name="sco_mail" id="con_mail" class="gui-input" placeholder="Adresse Email"  value="<?=$d_contact['sco_mail']?>">
																	  <label for="cli_mail" class="field-icon">
																		<i class="fa fa-envelope"></i>
																	  </label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label class="option block mn">
																	  <input type="checkbox" name="sco_compta" value="on" <?php if($d_contact['sco_compta']==1) echo("checked"); ?> />
																	  <span class="checkbox mn"></span> Contact "impayé"
																</label>
															</div>
															<div class="col-md-6 bloc-particulier" <?php if($d_suspect["sus_categorie"]!=3) echo("style='display:none;'"); ?> >
																<label for="sus_stagiaire_naiss" class="field prepend-icon">
																	<input type="text" id="sus_stagiaire_naiss" name="sus_stagiaire_naiss" class="gui-input date datepicker" placeholder="Date de naissance" value="<?=$sus_stagiaire_naiss?>" >
																	<span class="field-icon">
																		<i class="fa fa-calendar-o"></i>
																	</span>
																</label>																	
															</div>
														</div>
														
														<div class="row bloc-particulier mt15" <?php if($d_suspect["sus_categorie"]!=3) echo("style='display:none;'"); ?> >
															<div class="col-md-6">
																<label for="sus_sta_naiss_cp" class="field prepend-icon">
																	<input type="text" id="sus_sta_naiss_cp" name="sus_sta_naiss_cp" class="gui-input" placeholder="Département de naissance" value="<?=$d_suspect["sus_sta_naiss_cp"]?>" >
																	<span class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</span>
																</label>	
															</div>
															<div class="col-md-6" >
																<label for="sus_sta_naiss_ville" class="field prepend-icon">
																	<input type="text" id="sus_sta_naiss_ville" name="sus_sta_naiss_ville" class="gui-input nom" placeholder="Ville de naissance" value="<?=$d_suspect["sus_sta_naiss_ville"]?>" >
																	<span class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</span>
																</label>																	
															</div>
														</div>
														
														<!-- FIN CONTACT PAR DEFAUT -->
														
													</div>
													<!-- FIN DE INTERVENTION -->
													
													<!-- FACTURATION -->
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																  <span>Adresse de facturation</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_libelle2" id="adr_libelle2" class="gui-input" placeholder="Libellé"  value="<?=$d_ad_facturation['sad_libelle']?>">
																	<label for="cli_ad1" class="field-icon">
																		<i class="fa fa-tag"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_nom2" id="adr_nom2" class="gui-input" placeholder="Nom"  value="<?=$d_ad_facturation['sad_nom']?>">
																	<label for="adr_nom2" class="field-icon">
																		<i class="fa fa-tag"></i>
																	</label>																
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_service2" id="adr_service2" class="gui-input" placeholder="Service"  value="<?=$d_ad_facturation['sad_service']?>">
																	<label for="cli_ad1" class="field-icon">
																		<i class="fa fa-tag"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_ad12" id="adr_ad12" class="gui-input" placeholder="Adresse"  value="<?=$d_ad_facturation['sad_ad1']?>">
																	<label for="cli_ad1" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_ad22" class="gui-input" id="adr_ad22" placeholder="Adresse (Complément 1)" value="<?=$d_ad_facturation['sad_ad2']?>">
																	<label for="cli_ad2" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="field prepend-icon">
																	<input type="text" name="sad_ad32" id="adr_ad32" class="gui-input" placeholder="Adresse (Complément 2)" value="<?=$d_ad_facturation['sad_ad3']?>">
																	<label for="cli_ad3" class="field-icon">
																	  <i class="fa fa-map-marker"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-3">
																<div class="field prepend-icon">
																	<input type="text" id="adr_cp2" name="sad_cp2" class="gui-input code-postal" placeholder="Code postal" value="<?=$d_ad_facturation['sad_cp']?>"  >
																	<span for="cli_cp" class="field-icon">
																		<i class="fa fa fa-certificate"></i>
																	</span>
																</div>
															</div>
															<div class="col-md-9">
																<div class="field prepend-icon">
																	<input type="text" name="sad_ville2" id="adr_ville2" class="gui-input nom" placeholder="Ville"  value="<?=$d_ad_facturation['sad_ville']?>">
																	<label for="cli_ville" class="field-icon">
																		<i class="fa fa fa-building"></i>
																	</label>
																</div>
															</div>
														</div>
						
														<div class="row mt15 text-center">
															<div class="col-md-12">
																Zone géographique
															</div>
														</div>
														<div class="row text-center">
															<div class="col-md-12">
																<label for="geo12" class=" mt15 option option-dark">
																	<input type="radio" id="geo12" name="sus_geo2" value="1" <?php if($d_ad_facturation['sad_geo']==1) echo("checked"); ?> >
																	<span class="radio"></span> France
																</label>
																<label for="geo22" class=" mt15 option option-dark">
																	<input type="radio" id="geo22" name="sus_geo2" value="2" <?php if($d_ad_facturation['sad_geo']==2) echo("checked"); ?> >
																	<span class="radio"></span> UE
																</label>
																<label for="geo32" class=" mt15 option option-dark">
																	<input type="radio" id="geo32" name="sus_geo2" value="3" <?php if($d_ad_facturation['sad_geo']==3) echo("checked"); ?> >
																	<span class="radio"></span> Autre
																</label>
															</div>
														</div>
														
														<!-- SIREN / SIRET -->
														<div class="row mt15 bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-6">
																<label for="siren" >Siren :</label>
																<div class="field prepend-icon">
																	<input type="text" name="sus_siren" id="siren" class="gui-input siren" placeholder="Siren" value="<?=$d_suspect["sus_siren"]?>" data-prefixe="" />
																	<span class="field-icon">
																		<i class="fa fa-barcode"></i>
																	</span>
																</div>
															</div>
															<div class="col-md-6">
																<label for="nic" >Nic :</label>
																<div class="field prepend-icon">
																	<input type="text" name="sad_siret" id="nic" class="gui-input siret" placeholder="Nic" value="<?=$d_ad_facturation['sad_siret']?>"  <?php if(empty($d_suspect["sus_siren"])) echo("disbaled");?> data-prefixe="" />
																	<span class="field-icon">
																		<i class="fa fa-barcode"></i>
																	</span>
																</div>
															</div> 
														</div>
														<div class="row mt15 bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
															<div class="col-md-12">
																<label for="siret" >Siret :</label>
																<div class="field prepend-icon">
																	<input type="text" id="siret" class="gui-input siretlong" placeholder="Siret" value="<?=$d_suspect['sus_siret']?>" data-prefixe="" />
																	<span class="field-icon">
																		<i class="fa fa-barcode"></i>
																	</span>
																</div>
															</div> 
														</div>
													
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Facturation</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<span class="select">
																		<select id="reg_type" name="sus_reg_type">
																			<option value="0">Mode de règlement...</option>
												<?php 						foreach($base_reglement as $k => $n){
																				if($k > 0){ 
																					if($k==$d_suspect["sus_reg_type"]){
																						echo("<option value='" . $k . "' selected >" . $n . "</option>");
																					}else{
																						echo("<option value='" . $k . "' >" . $n . "</option>");
																					}
																					
																				} 
																			} ?>
																		</select>
																		<i class="arrow"></i>
																	</span>
																</div>
															</div>
														</div>
														
														<div class="row mt5" >
															<div class="col-md-12">
																<label class="option">
																	<input type="radio" id="reg_formule_1" class="reg-formule" name="reg_formule" value="1" <?php if($d_suspect["sus_reg_formule"]==1) echo("checked") ?> />
																	<span class="radio"></span>
																</label>
														<?php 	if($d_suspect["sus_reg_formule"]==1){ ?>
																	Date + <input type="number" max="60" size="2" name="reg_nb_jour_1" value="<?=$d_suspect["sus_reg_nb_jour"]?>" class="champ-reg-formule reg-formule-1" /> jours
														<?php	}else{ ?>
																	Date + <input type="number" max="60" size="2" name="reg_nb_jour_1" value="0" class="champ-reg-formule reg-formule-1" disabled /> jours
														<?php	} ?>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12">
																<label class="option">
																	<input type="radio" id="reg_formule_2" class="reg-formule" name="reg_formule" value="2" <?php if($d_suspect["sus_reg_formule"]==2) echo("checked") ?> >
																	<span class="radio"></span>
																</label>
																Date + 45j + fin de mois
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12">
																<label class="option">
																	<input type="radio" id="reg_formule_3" class="reg-formule" name="reg_formule" value="3" <?php if($d_suspect["sus_reg_formule"]==3) echo("checked") ?> >
																	<span class="radio"></span>
																</label>
																Date + Fin de mois + 45j
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12">
																<label class="option">
																	<input type="radio" id="reg_formule_4" class="reg-formule" name="reg_formule" value="4" <?php if($d_suspect["sus_reg_formule"]==4) echo("checked") ?> />
																	<span class="radio"></span>
																</label>
														<?php 	if($d_suspect["sus_reg_formule"]==4){ ?>
																	Date + <input type="number" max="30" size="2" maxlength="2" name="reg_nb_jour_4" value="<?=$d_suspect["sus_reg_nb_jour"]?>" class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
																	+ Fin de mois +  <input type="number" size="2" maxlength="2" max="20" name="reg_fdm_4" value="<?=$d_suspect["sus_reg_fdm"]?>" class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
														<?php	}else{ ?>
																	Date + <input type="number" max="30" size="2" maxlength="2" name="reg_nb_jour_4" value="0" disabled class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
																	+ Fin de mois +  <input type="number" size="2" maxlength="2" max="20" name="reg_fdm_4" value="0" disabled class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
														<?php	} ?>
															</div>
														</div>
														
														<div class="row mt15">															
															<div class="col-md-6 pt10">
																<div class="option-group field">
																	<label class="option option-dark">
																		<input type="checkbox" name="sus_releve" value="on" <?php if($d_suspect["sus_releve"]==1) echo("checked"); ?> />
																		<span class="checkbox"></span>Relevés de factures
																	</label>
																</div>
															</div>
															<div class="col-md-6 bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
																<div class="field prepend-icon">
																	<input type="text" name="sus_ident_tva" id="ident_tva" class="gui-input" placeholder="Identification TVA" value="<?=$d_suspect["sus_ident_tva"]?>" />
																	<label for="cli_ident_tva" class="field-icon">
																	  <i class="fa fa-barcode"></i>
																	</label>
																</div>
															</div>
														</div>
														
														
														<div class="row bloc-no-do" <?php if($d_suspect["sus_categorie"]==4) echo("style='display:none;'"); ?> >
															<div class="col-md-12">
																<div class="section-divider">
																	<span>Financeur</span>
																</div>
															</div>
														</div>											
														<div class="row mt15 bloc-no-do" <?php if($d_suspect["sus_categorie"]==4) echo("style='display:none;'"); ?> >
															<div class="col-md-12">
																<label class="option option-dark">
																	<input type="checkbox" name="sus_facture_opca" id="facture_opca" value="oui" <?php if($d_suspect["sus_facture_opca"]==1) echo("checked"); ?> />
																	<span class="checkbox"></span>Facturer un financeur
																</label>
															</div>
														</div>
														
														<div class="row mt15 bloc-no-do" id="bloc_financeur" <?php if($d_suspect["sus_facture_opca"]!=1 OR $d_suspect["sus_categorie"]==4) echo("style='display:none;'"); ?> >
															<div class="col-md-6">
																<label for="opca_type" >Type de financeur : </label>
																<select id="opca_type"  name="sus_opca_type" class="select2" >
																	<option value="0">Type de financeur ...</option>
													<?php 			if(!empty($d_opca_type)){
																		foreach($d_opca_type as $opca){
																			if($opca["csc_id"]==$d_suspect["sus_opca_type"]){
																				echo("<option value='" . $opca["csc_id"] . "' selected >" . $opca["csc_libelle"] . "</option>");
																			}else{
																				echo("<option value='" . $opca["csc_id"] . "' >" . $opca["csc_libelle"] . "</option>");
																			}
																		}
																	} ?>
																</select>	
															</div>
															<div class="col-md-6">
																<label for="opca" >Financeur : </label>
																<select id="opca" name="sus_opca" class="select2" >
																	<option value="0">Sélectionner un financeur...</option>
													<?php 			if(!empty($d_opca)){
																		foreach($d_opca as $opca){
																			if($d_suspect["sus_opca"]==$opca["id"]){
																				echo("<option value='" . $opca["id"] . "' selected >" . $opca["text"] . "</option>");
																			}else{
																				echo("<option value='" . $opca["id"] . "' >" . $opca["text"] . "</option>");
																			}
																			
																		}
																	} ?>
																</select>
															</div>
														</div>
														
														
														
													</div>
													<!-- fin de facturation -->
												</div>
												
												<!-- AUTRE INFO -->
												<div class="section-divider mb40">
													<span>Autres infos</span> 
												</div>
											
												
												<div class="row mt15 bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
													<div class="col-md-4">
														<label for="classification">Classification :</label>
														<select id="classification" name="sus_classification" class="select2 select2-classification" >
															<option value="0">Sélectionner une classification...</option>
												<?php		if(!empty($d_classifications)){
																foreach($d_classifications as $classifications){
																	if($d_suspect["sus_classification"]==$classifications["ccl_id"]){
																		echo("<option value='" . $classifications["ccl_id"] . "' selected >" . $classifications["ccl_libelle"] . "</option>");	
																	}else{
																		echo("<option value='" . $classifications["ccl_id"] . "' >" . $classifications["ccl_libelle"] . "</option>");
																	}
																}
															}  ?>
														</select>															 
													</div>
													<div class="col-md-4">
														<label for="sous_classification">Type de classification :</label>
														<select id="sous_classification" name="sus_classification_type" class="select2 select2-classification-type" >
															<option value="0">Type de classification...</option>
													<?php 	foreach($d_classifications_types as $t){ ?>
																<option value="<?=$t['cct_id']?>" <?php if($t['cct_id'] == $d_suspect["sus_classification_type"]) echo("selected"); ?> ><?= $t['cct_code'] . "-" . $t['cct_libelle'] ?></option>
													<?php 	} ?>
														</select>
													</div>
													<div class="col-md-4">
														<label for="classification-categorie">Catégorie de classification :</label>
														<select id="classification-categorie" name="sus_classification_categorie" class="select2 select2-classification-categorie" >
															<option value="0">Catégorie de classification...</option>
													<?php 	foreach($d_classifications_categories as $c){ ?>
																<option value="<?=$c['ccc_id']?>" <?php if($c['ccc_id'] == $d_suspect["sus_classification_categorie"]) echo("selected"); ?> ><?=$c['ccc_libelle']?></option>
													<?php 	} ?>
														</select>
													</div>
												</div>
												<div class="row mt15">
													<div class="col-md-4">
														<label for="stagiaire_type">Type de stagiaire :</label>
														<select id="stagiaire_type" name="sus_stagiaire_type" class="select2" >
															<option value="0">Type de stagiaire...</option>
												<?php		if(!empty($base_type_stagiaire)){
																foreach($base_type_stagiaire as $k => $n){
																	if($d_suspect["sus_stagiaire_type"]==$k){
																		echo("<option value='" . $k . "' selected >" . $n . "</option>");	
																	}else{
																		echo("<option value='" . $k . "' >" . $n . "</option>");
																	}
																}
															}  ?>
														</select>														
													</div>
													<div class="col-md-4 bloc-no-particulier" >
														<div class="section" >
															<label for="sus_tel" >Téléphone (standard) :</label>
															<input type="text" id="sus_tel" name="sus_tel" class="gui-input telephone" placeholder="Téléphone" value="<?=$d_suspect["sus_tel"]?>" />
														</div>
													</div>
												</div>
												<div class="row mt15">
													<div class="col-md-4 bloc-no-particulier">
														<label for="ape" >APE :</label>
														<select id="ape"  name="sus_ape" class="select2"  >
															<option value="0">Sélectionner un code APE...</option>
													<?php	if(!empty($d_apes)){
																foreach($d_apes as $d_ape){																	
																	if($d_suspect["sus_ape"]==$d_ape["ape_id"]){
																		echo("<option value='" . $d_ape["ape_id"] . "' selected >" . $d_ape["ape_code"] . "-" . $d_ape["ape_libelle"] . "</option>");
																	}else{
																		echo("<option value='" . $d_ape["ape_id"] . "' >" . $d_ape["ape_code"] . "-" . $d_ape["ape_libelle"] . "</option>");	
																	}
																} 
															} ?>
														</select>															
													</div>
											<?php	if(!$prescripteur_verrou){ ?>
														<div class="col-md-4">
															<label for="prescripteur" >Source :</label>
															<select id="prescripteur"  name="sus_prescripteur" class="select2"  >
																<option value="0">Sélectionner une source...</option>
														<?php	if(!empty($d_prescripteurs)){
																	foreach($d_prescripteurs as $dp){																	
																		if($d_suspect["sus_prescripteur"]==$dp["cpr_id"]){
																			echo("<option value='" . $dp["cpr_id"] . "' selected >" . $dp["cpr_libelle"] . "</option>");
																		}else{
																			echo("<option value='" . $dp["cpr_id"] . "' >" . $dp["cpr_libelle"] . "</option>");	
																		}
																	} 
																} ?>
																<option value="-1" <?php if($d_suspect["sus_prescripteur"]==-1) echo("selected"); ?> >Autre</option>
															</select>															
														</div>
														<div class="col-md-4" id="bloc_source_detail" <?php if($d_prescripteur["cpr_ouvert"]!=1 AND $d_suspect["sus_prescripteur"]!=-1) echo("style='display:none;'"); ?> >
															<label for="prescripteur_info" ><?=$d_prescripteur["cpr_libelle"]?></label>
															<input type="text" name="sus_prescripteur_info" id="prescripteur_info" class="gui-input" placeholder="Merci de préciser votre source" value="<?=$d_suspect["sus_prescripteur_info"]?>" />
														</div>
														
											<?php	}else{ ?>
														<div class="col-md-4 pt25 text-center">
															Source : <b><?=$d_prescripteur["cpr_libelle"]?></b>
														</div>
														<div class="col-md-4 pt25">
											<?php			if(!empty($d_suspect["sus_prescripteur_info"])){
																echo($d_prescripteur["cpr_libelle"] . " :<b>" . $d_suspect["sus_prescripteur_info"] . "</b>");
															} ?>
														</div>
											<?php	} ?>
												</div>
												<div class="row mt15 bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
													<div class="col-md-6">
														<div class="section" >
															<label for="sus_capital" >Capital</label>
															<div class="input-group">																
																<input type="text" id="sus_capital" name="sus_capital" class="gui-input" placeholder="Capital" value="<?=$d_suspect["sus_capital"]?>" />
																<div class="input-group-addon">€</div>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section" >
															<label for="sus_soc_type" >Type de société</label>
															<input type="text" name="sus_soc_type" id="sus_soc_type" class="gui-input" placeholder="Type de société" value="<?=$d_suspect["sus_soc_type"]?>" />
														</div>
													</div>		
												</div>
												<div class="row mt15 bloc-no-particulier" <?php if($d_suspect["sus_categorie"]==3) echo("style='display:none;'"); ?> >
													<div class="col-md-6" >
														<div class="section" >
															<label for="sus_immat_lieu" >Lieu d'immatriculation</label>
															<input type="text" id="sus_immat_lieu" name="sus_immat_lieu" class="gui-input" placeholder="Lieu d'immatriculation" value="<?=$d_suspect["sus_immat_lieu"]?>" />
														</div>
													</div>
												</div>
												
											
										</div>
									</div>
								</div>
								<!-- FIN PANEL FORM -->
                            </div>
						</div>
					</section>
				</section>
			</div>
			
			<footer id="content-footer" class="affix">
				<div class="row"> 
                    <div class="col-xs-3 footer-left" >
						<a href="suspect_voir.php?suspect=<?=$suspect?>" class="btn btn-default btn-sm" >
							<i class="fa fa-long-arrow-left"></i> Retour
						</a>
					</div>
                    <div class="col-xs-6 footer-middle"></div>
                    <div class="col-xs-3 footer-right" >
						<button type="submit" name="submit" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
                    </div>
				</div>
			</footer>
		</form>
 <?php	include "includes/footer_script.inc.php"; ?>	


		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>

		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<script src="vendor/plugins/holder/holder.min.js"></script>
		<script src="assets/js/custom.js"></script>

		<script type="text/javascript">
			
			var societe="<?=$acc_societe?>";
			var agence="<?=$acc_agence?>";
			
			var source=new Array();
			source[0]=new Array();
			source[0]["libelle"]="";
			source[0]["ouvert"]=0;
			source[-1]=new Array();
			source[-1]["libelle"]="Autre";
			source[-1]["ouvert"]=1;
	<?php	if(!empty($d_prescripteurs)){
				foreach($d_prescripteurs as $prescripteur){ ?>
					source[<?=$prescripteur["cpr_id"]?>]=new Array();
					source[<?=$prescripteur["cpr_id"]?>]["libelle"]="<?=$prescripteur["cpr_libelle"]?>";
					source[<?=$prescripteur["cpr_id"]?>]["ouvert"]="<?=$prescripteur["cpr_ouvert"]?>";
	<?php		} 
			} ?>
			
			
			jQuery(document).ready(function(){
				
				//**********************
				// INTERACTION FORM
				//**********************
				
				// Nom
				$("#nom").keyup(function(){
					$("#adr_nom1").val($(this).val());
					if($("#similaire").is(":checked")){
						$("#adr_nom2").val($(this).val());
					}
				})
				
				
				// categorie du client
				$("#categorie").change(function(){
					get_sous_categories($(this).val(),actualiser_sous_categorie,"","");		
					if($(this).val()==4){
						$(".bloc-no-do").hide();
					}else{
						$(".bloc-no-do").show();
					}
					
					if($(this).val()==3){
						console.log("bloc-no-particulier hide");
						$("#con_compta").prop("checked", true);
						$(".bloc-no-particulier").hide();
						$(".contact_libelle").text("Coordonnées du particulier");
						$(".bloc-particulier").show();	
					}else{
						$("#con_compta").prop("checked", false);
						$(".bloc-no-particulier").show();	
						$(".contact_libelle").text("Contact par défaut du lieu d'intervention");	
						$(".bloc-particulier").hide();
					}
				});
				/*get_sous_categories($("#categorie").val(),actualiser_sous_categorie,"","");	*/			
				if($("#categorie").val()==3){
					console.log("bloc-no-particulier hide");
					$(".bloc-no-particulier").hide();
					$(".contact_libelle").text("Coordonnées du particulier");
					$(".bloc-particulier").show();	
				}else{

					$(".bloc-no-particulier").show();	
					$(".contact_libelle").text("Contact par défaut du lieu d'intervention");	
					$(".bloc-particulier").hide();
				}
				
				// choix du commercial
				$("#agence").change(function(){
					get_commerciaux(societe,$(this).val(),0,actualiser_select2,"#commercial",$("#commercial").val());
				});
				
				// Adresse
				
				$("#similaire").click(function(){
					if($(this).is(":checked")){
						$("#adr_nom2").val($("#adr_nom1").val());
						$("#adr_service2").val($("#adr_service1").val());
						$("#adr_ad12").val($("#adr_ad11").val());
						$("#adr_ad22").val($("#adr_ad21").val());
						$("#adr_ad32").val($("#adr_ad31").val());
						$("#adr_cp2").val($("#adr_cp1").val());
						$("#adr_ville2").val($("#adr_ville1").val());
						$("#geo12").prop("checked",$("#geo11").prop("checked"));
						$("#geo22").prop("checked",$("#geo21").prop("checked"));
						$("#geo32").prop("checked",$("#geo31").prop("checked"));
					}
				});
				
				$("#adr_nom1").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_nom2").val($(this).val());
					}
				})
				$("#adr_service1").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_service2").val($(this).val());
					}
				})
				$("#adr_ad11").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_ad12").val($(this).val());
					}
				})
				$("#adr_ad21").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_ad22").val($(this).val());
					}
				})
				$("#adr_ad31").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_ad32").val($(this).val());
					}
				})
				$("#adr_cp1").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_cp2").val($(this).val());
					}
				})
				$("#adr_ville1").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_ville2").val($(this).val());
					}
				})
				
				$(".geo1").click(function(){
					if($("#similaire").is(":checked")){
						$("#geo12").prop("checked",$("#geo11").prop("checked"));
						$("#geo22").prop("checked",$("#geo21").prop("checked"));
						$("#geo32").prop("checked",$("#geo31").prop("checked"));
					}
				})
			
				// contact
				$("#fonction").change(function(){
					if($(this).val()=="autre"){
						$("#fonction_nom_bloc").show();
					}else{
						$("#fonction_nom_bloc").hide();
						$("#fonction_nom").val("");
					}
						
				});
				
				// GESTION SIREN / SIRET
				$(".siren").focusout(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length!=11){
						$("#" + prefixe + "nic").val("");
						$("#" + prefixe + "nic").prop("disabled",true);
						
						$("#" + prefixe + "nic").val("");
					}else{
						check_siren_val(prefixe);
					}
				});
				$(".siren").keyup(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length==11){
						$("#" + prefixe + "nic").prop("disabled",false);							
						if($("#" + prefixe + "nic").val()!=""){
							$("#" + prefixe + "siret").val($(this).val() + " " + $("#" + prefixe + "nic").val());
						}
					}
				});
				$(".siret").focusout(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length!=6){
						$("#" + prefixe + "siret").val("");
					}
				});
				$(".siret").keyup(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length==6){
						if($("#" + prefixe + "siren").val()!=""){
							$("#" + prefixe + "siret").val($("#" + prefixe + "siren").val() + " " + $(this).val());
						}
					}
				});
				
				$(".siretlong").focusout(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length!=18){
						$("#" + prefixe + "nic").val("");
					}else{
						check_siren_val(prefixe);
					}
				});
				$(".siretlong").keyup(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length==18){					
						$("#" + prefixe + "siren").val($(this).val().substring(0,11));
						$("#" + prefixe + "nic").prop("disabled",false);
						$("#" + prefixe + "nic").val($(this).val().substring(12));						
					}
				});
				
				// GESTION DES MODALITES DE REGLEMENTS						
				$("input[name=reg_formule]").click(function (){
					reg_formule=$("input[name=reg_formule]:checked").val();
					$('.champ-reg-formule').each(function(){
						if($(this).hasClass("reg-formule-" + reg_formule)){
							$(this).prop("disabled",false);	
						}else{
							$(this).val(0);	
							$(this).prop("disabled",true);	
						}
					});							
				});
				$(".champ-reg-formule").blur(function(){
					//console.log($(this).val());
					//console.log($(this).prop("max"));
					if( ($(this).val()*1)>($(this).prop("max")*1) ){
						$(this).val($(this).prop("max"));
					}
				});
				
				$("#prescripteur").change(function(){
					$("#prescripteur_info").val("");	
					if(source[$(this).val()]["ouvert"]==1){
						
						$("#bloc_source_detail label").html(source[$(this).val()]["libelle"]);
						$("#bloc_source_detail").show();
					}else{
						$("#bloc_source_detail").hide();
					}
				});
				
				
				// FINANCEUR 
				$("#facture_opca").click(function(){
					if($(this).is(":checked")){
						$("#bloc_financeur").show();	
					}else{						
						$("#bloc_financeur").hide();	
						
					}
				});
				
				$("#opca_type").change(function(){
					if($(this).val()>0){
						get_clients(4,$(this).val(),0,null,actualiser_select2,"#opca",0);
					}else{
						$("#opca option[value!='0'][value!='']").remove();
					}
				});
			});
			
			//**********************
			// FONCTION
			//**********************
			
			// GESTION DES CATEGORIE
			
			function actualiser_sous_categorie(json){
				$("#sous_categorie option[value!='0']").remove();
				if(json!=""){
					$("#sous_categorie").select2({
						data: json				
					});
					$("#sous_categorie_bloc").show();
				}else{
					$("#sous_categorie_bloc").hide();
				}
			}
			
			// CONTROLE DES VALEURS DE SIREN
			function check_siren_val(prefixe){
				if($("#" + prefixe + "siren").val()=="123 456 789"||$("#" + prefixe + "siren").val()=="000 000 000"||$("#" + prefixe + "siren").val()=="111 111 111"||$("#" + prefixe + "siren").val()=="222 222 222"||$("#" + prefixe + "siren").val()=="333 333 333"||$("#" + prefixe + "siren").val()=="444 4444 444"||$("#" + prefixe + "siren").val()=="555 555 555"||$("#" + prefixe + "siren").val()=="666 666 666"||$("#" + prefixe + "siren").val()=="777 777 777"||$("#" + prefixe + "siren").val()=="888 888 888"||$("#" + prefixe + "siren").val()=="999 999 999"||$("#" + prefixe + "siren").val()=="987 654 321"){
					afficher_txt_user($("#" + prefixe + "siren_err"),"Saisie incorrecte!","danger",5000);
					$("#" + prefixe + "siren").val("");
					$("#" + prefixe + "nic").val("");
					$("#" + prefixe + "nic").prop("disabled",true);
					$("#" + prefixe + "siret").val("");
				}else{
					$("#" + prefixe + "nic").prop("disabled",false);
				}
			}
			
			
		</script>
	</body>
</html>
