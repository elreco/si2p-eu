<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	
	// SUPPRESSION D'UN Notifications
	

	function delete_notification($id){
		
		global $Conn;

		$req = $Conn->prepare("DELETE FROM Notifications WHERE not_id = :not_id");
		$req->bindParam("not_id",$id);
		$req->execute();

	}

	if(isset($_GET["notification"])){
		
		// suppresion simple
		if(!empty($_GET["notification"])){
			delete_notification($_GET["notification"]);
		}
		
	}else{
		
		// suppresion multiple
		if(isset($_POST["nb"])){
			if(!empty($_POST["nb"])){
				
				for($i=1;$i<=$_POST["nb"];$i++){
					if(isset($_POST["notif_".$i])){
						if(!empty($_POST["notif_".$i])){
							delete_notification($_POST["notif_".$i]);
						}	
					}
				}
				
			}
		}
		
	}
	
	header("Location: notification_liste.php");

