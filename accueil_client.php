<?php
//////////////// MENU ACTIF ////////////////////
$menu_actif = 0;
//////////////// INCLUDES /////////////////////
include "includes/controle_acces_cli.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');
include('modeles/mod_parametre.php');
require "modeles/mod_erreur.php";
//////////////// REQUETES VERS LE SERVEUR /// ///////////////

$_SESSION["retour"]="accueil_client.php";



$sql="SELECT soc_id FROM Societes ORDER BY soc_id;";
$req = $Conn->query($sql);
$d_societes=$req->fetchAll();
$sessions = array();
if(!empty($d_societes)){
	foreach($d_societes as $soc){

		$ConnFct=connexion_fct($soc["soc_id"]);
        $sql="SELECT DISTINCT cli_id,cli_nom, cli_code,act_demi_deb, act_date_deb, act_adr_ville, acl_id, act_id, act_produit, acl_produit
         FROM  actions
        LEFT JOIN actions_clients ON (actions_clients.acl_action = actions.act_id)
		LEFT JOIN Clients ON (actions_clients.acl_client = clients.cli_id)
		WHERE acl_archive = 0 AND acl_client IN (" . $_SESSION['acces']['acc_groupe_liste'] . ") AND act_date_deb >= '" . date("Y-m-d") . "'";
        $sql.=" ORDER BY act_date_deb DESC;";
		$req = $ConnFct->query($sql);
        $sessions_build = $req->fetchAll();

        if(!empty($sessions)){
            $last_key = array_key_last($sessions);
        }else{
            $last_key = 0;
        }


        foreach($sessions_build as $k=>$sb){
            $ikey = $last_key + $soc['soc_id'] + $k;

            $continue = true;
            if(!empty($sessions)){
                foreach($sessions as $ses){
                    if($ses['societe'] == $soc['soc_id'] && $ses['act_id'] == $sb['act_id']){
                        $continue = false;



                    }
                }
            }
            if($continue == true){
                $sessions[$ikey]['act_adr_ville'] = $sb['act_adr_ville'];
                $sessions[$ikey]['acl_id'] = $sb['acl_id'];
                $sessions[$ikey]['act_id'] = $sb['act_id'];
                $sessions[$ikey]['acl_produit'] = $sb['acl_produit'];
                $sessions[$ikey]['act_date_deb'] = $sb['act_date_deb'];
                $sessions[$ikey]['act_produit'] = $sb['act_produit'];
				$sessions[$ikey]['cli_nom'] = $sb['cli_nom'];
				$sessions[$ikey]['cli_code'] = $sb['cli_code'];
                $sessions[$ikey]['societe'] = $soc['soc_id'];
				$sessions[$ikey]['cli_id'] = $sb['cli_id'];
            }

        }
    }


function sortFunction( $a, $b ) {

    return strtotime($a["act_date_deb"]) - strtotime($b["act_date_deb"]);
}

usort($sessions, "sortFunction");
}

?>

<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>Si2P - ORION</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="Si2P">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->

<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

<link rel="shortcut icon" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="sb-top sb-top-sm">
    <div id="main">
<?php	include "includes/header_cli.inc.php"; ?>
        <section id="content_wrapper">



			<section id="content" class="animated fadeIn">

				<h1>Les 10 formations à venir</h1>
				<!-- FIN FORMULAIRE RECHERCHE -->

		<?php 	if(!empty($sessions)){ ?>

					<div class="table-responsive">
						<table class="table table-striped table-hover dataTable" id="table_id">
							<thead>
								<tr class="dark">
									<th>Date</th>
									<th>Client</th>
									<th>Lieu</th>
									<th>Formation</th>
                                    <th>Centre de formation</th>
									<th class="text-center" >Attestations</th>
									<?php if($_SESSION['acces']['acc_client'] == 16670 OR $_SESSION['acces']['acc_holding'] == 16670){ ?>
										<th class="text-center" >Feuille de présence</th>
									<?php }?>
									<th class="no-sort">Actions</th>
								</tr>
							</thead>
							<tbody>
                            <?php
                            $i=0;
					/* foreach($d_societes as $soc){
						if(!empty($sessions[$soc['soc_id']])){ */

                             	foreach($sessions as $s){
                                    $i = $i+1;
                                    if($i < 11){


                                    $sql="SELECT pro_code_produit, pro_libelle FROM Produits WHERE pro_id = " . $s['acl_produit'];
                                    $req = $Conn->query($sql);
                                    $d_produit=$req->fetch();

                                    $sql="SELECT soc_nom FROM Societes WHERE soc_id = " . $s['societe'];
                                    $req = $Conn->query($sql);
                                    $d_societe=$req->fetch();
                        ?>
								<tr>

									<td data-sort="<?= $s['act_date_deb'] ?>">
									   <?= convert_date_fr($s['act_date_deb']); ?>

									</td>
									<td><?= $s['cli_nom'] ?> (<?= $s['cli_code'] ?>)</td>
									<td>
										<?= $s['act_adr_ville']; ?>
									</td>
									 <td>
										<?= $d_produit["pro_code_produit"] ?> (<?= $d_produit["pro_libelle"] ?>)
									</td>
                                    <td>
                                        <?= $d_societe['soc_nom'] ?>
                                    </td>
									<td class="text-center" >
							<?php		if(file_exists("documents/Societes/".$s['societe'] ."/Attestations/attestation_ses_".$s['act_id']."_".$s["acl_id"].".pdf")){
											$download_nom="Attestation groupée " . $d_produit["pro_code_produit"] . " " . convert_date_txt($s['act_date_deb']); ?>
											<a href="documents/Societes/<?= $s['soc_id'] ?>/Attestations/attestation_ses_<?= $s['act_id'] ?>_<?= $s["acl_id"] ?>.pdf" download="<?=$download_nom?>" data-toggle="tooltip" title="Télécharger l'attestation groupée" class="btn btn-success btn-sm">
												<i class="fa fa-file-pdf-o"></i>
											</a>
							<?php		}else{
											echo("<span class='text-danger' >Non disponible</span>");
										} ?>
									</td>
									<?php if($_SESSION['acces']['acc_client'] == 16670 OR $_SESSION['acces']['acc_holding'] == 16670){ ?>
										<td class="text-center">
											<?php
											if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $s['societe'] . "/Presences/monop_presence_". $s["acl_id"] . ".pdf")){
												$download_nom="Feuille de présence " . $d_produit["pro_code_produit"] . " " . convert_date_txt($s['act_date_deb']); ?>
												<a href="documents/Societes/<?= $s['societe'] ?>/Presences/monop_presence_<?=$s["acl_id"]?>.pdf" download="<?=$download_nom?>" data-toggle="tooltip" title="Télécharger la feuille de présence" class="btn btn-success btn-sm">
													<i class="fa fa-file-pdf-o"></i>
												</a>
											<?php }else{
												echo("<span class='text-danger' >Non disponible</span>");
											}?>
										</td>
									<?php }?>
									<td>
										<a href="session_client_stagiaire.php?id=<?= $s['act_id'] ?>&acl_id=<?= $s['acl_id'] ?>&soc=<?= $s['societe'] ?>" class="btn btn-success btn-sm" data-toggle="tooltip" title="Voir la liste des inscrits">
											<i class="fa fa-eye"></i>
										</a>
									</td>
								</tr>
                    <?php 	 }
                        }
                   ?>
						</tbody>
					</table>
				</div>
	<?php 	}else{ ?>
				<div class="col-md-12 text-center" style="padding:0;" >
					<div class="alert alert-warning" style="border-radius:0px;">
					  Aucune session.
					</div>
				</div>
	<?php 	} ?>
            </section>
        </section>
    </div>
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">


            </div>
            <div class="col-xs-6 footer-middle">&nbsp;</div>
            <div class="col-xs-3 footer-right">




            </div>
        </div>
    </footer>

<!-- FIN MODAL SUPPRESSION PRODUIT -->
    <style type="text/css">
        #table_id_filter label .form-control{
            width:300px;
        }

    </style>
<?php
include "includes/footer_script.inc.php"; ?>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
/////////////////////// INTERACTIONS UTILISATEUR /////////////////////////
jQuery(document).ready(function () {
    $(document).on('click', '.last_session', function() {
        sta_id = $(this).data("id");
        get_sessions(sta_id);
    });
    // fin suppression onclick suppr
    $(document).on('click', '.delete', function() {
        $('#delete-btn').attr("href","session_delete.php?id=" + $(this).data('id'));
    });
    /*$('#table_id').DataTable({
        "language": {
        "url": "/vendor/plugins/DataTables/media/js/French.json",
        searchPlaceholder: "Rechercher une formation (exemple : 04 Avril 2017)"
        },
        "aaSorting": []

    });*/
    $('#table_id').DataTable({
        "language": {
        "url": "vendor/plugins/DataTables/media/js/French.json"
        },
        "paging": false,
        "searching": false,
        "info": false,
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false,
        }]
    });
    $(".datepicker").datepicker({
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        showButtonPanel: false,
        beforeShow: function(input, inst) {
            var newclass = 'admin-form';
            var themeClass = $(this).parents('.admin-form').attr('class');
            var smartpikr = inst.dpDiv.parent();
            if (!smartpikr.hasClass(themeClass)) {
                inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
            }
        }
    });
});


///////////////////////////// FONCTIONS ////////////////////////////////

function get_sessions(sta_id){
    $.ajax({
        type:'POST',
        url: 'ajax/ajax_sessions_stagiaires.php',
        data : 'sta_id=' + sta_id,
        dataType: 'json',
        success: function(data)
        {
            console.log(data);
            $.each(data, function(key, value) {
                $('.body-liste').append($("<li></li>").text(value['ses_date'] + " de " + value['ses_h_deb'] + " à " + value['ses_h_fin']));
            });

        }
    });
}


</script>
</body>
</html>
