<?php

	// STATISTIQUE CA PAR PRODUIT

	include "includes/controle_acces.inc.php";

	include "includes/connexion.php";
	include "includes/connexion_fct.php";


	// CONTROLE ACCES

	if(empty($_SESSION['acces']['acc_droits'][35]) && $_SESSION['acces']['acc_service'][1]!=1 AND $_SESSION['acces']['acc_service'][2]!=1 AND $_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']['acc_service'][5]!=1 AND $_SESSION['acces']["acc_profil"]!=5 AND $_SESSION['acces']["acc_profil"]!=15){
		echo("Impossible d'afficher la page!");
		die();
	}

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}


	// TRAITEMENT DU FORM

	$crit_stat=array();
	if(!empty($_POST)){

		$reseau=false;
		if($_SESSION["acces"]["acc_droits"][28]){
			if(!empty($_POST["reseau"])){
				$reseau=true;
			}
		}
		$crit_stat["reseau"]=$reseau;

		if(empty($acc_agence)){
			$agence=0;
			if(!empty($_POST["agence"])){
				$agence=intval($_POST["agence"]);
			}
		}else{
			$agence=$acc_agence;
		}
		$crit_stat["agence"]=$agence;

		$periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$periode_deb_fr=$_POST["periode_deb"];
			$DT_periode=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode)){
				$periode_deb=$DT_periode->format("Y-m-d");

			}
		}
		$crit_stat["periode_deb"]=$periode_deb;

		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$periode_fin_fr=$_POST["periode_fin"];
			$DT_periode=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode)){
				$periode_fin=$DT_periode->format("Y-m-d");
			}
		}
		$crit_stat["periode_fin"]=$periode_fin;

		$pro_categorie=0;
		if(!empty($_POST["pro_categorie"])){
			$pro_categorie=intval($_POST["pro_categorie"]);
		}
		$crit_stat["pro_categorie"]=$pro_categorie;

		$pro_famille=0;
		if(!empty($_POST["pro_famille"])){
			$pro_famille=intval($_POST["pro_famille"]);
		}
		$crit_stat["pro_famille"]=$pro_famille;

		$pro_sous_famille=0;
		if(!empty($_POST["pro_sous_famille"])){
			$pro_sous_famille=intval($_POST["pro_sous_famille"]);
		}
		$crit_stat["pro_sous_famille"]=$pro_sous_famille;

		$pro_sous_sous_famille=0;
		if(!empty($_POST["pro_sous_sous_famille"])){
			$pro_sous_sous_famille=intval($_POST["pro_sous_sous_famille"]);
		}
		$crit_stat["pro_sous_sous_famille"]=$pro_sous_sous_famille;

		$pro_type=0;
		if(!empty($_POST["pro_type"])){
			$pro_type=intval($_POST["pro_type"]);
		}
		$crit_stat["pro_type"]=$pro_type;

		$client=0;
		if(!empty($_POST["client"])){
			$client=intval($_POST["client"]);
		}
		$crit_stat["client"]=$client;

		$filiale=0;
		if(!empty($_POST["filiale"])){
			$filiale=1;
		}
		$crit_stat["filiale"]=$filiale;

		$ca_gc=0;
		if(!$reseau AND $acc_societe==4 AND ($agence==0 OR $agence==4) AND $_SESSION["acces"]["acc_droits"][8]){
			// si reseau le ca GC est aotomatiquement inclut dans la stat
			if(!empty($_POST["ca_gc"])){
				$ca_gc=1;
			}
		}
		$crit_stat["ca_gc"]=$ca_gc;


		$produit_list="";
		if(!empty($_POST["produit_list"])){
			$produit_list=implode(",",$_POST["produit_list"]);
		}
		$crit_stat["produit_list"]=$produit_list;

		$_SESSION["crit_stat"]=$crit_stat;
	}elseif(isset($_SESSION["crit_stat"])){

		$crit_stat=$_SESSION["crit_stat"];

		$DT_periode=date_create_from_format('Y-m-d',$crit_stat["periode_deb"]);
		if(!is_bool($DT_periode)){
			$periode_deb_fr=$DT_periode->format("d/m/y");
		}

		$DT_periode=date_create_from_format('Y-m-d',$crit_stat["periode_fin"]);
		if(!is_bool($DT_periode)){
			$periode_fin_fr=$DT_periode->format("d/m/y");
		}

	}

	if(empty($crit_stat)){
		echo("Formulaire incomplet");
		die();
	}

	// TRAITEMENT

	// ON LISTE LES PRODUITS CONCERNE PAR LA Statistiques

	$d_produits=array();
	$mil="";
	$sql="SELECT pro_id,pro_code_produit,pro_libelle,pro_archive,pca_libelle,pfa_libelle,psf_libelle,pss_libelle
	FROM Produits LEFT JOIN Produits_Categories ON (Produits.pro_categorie=Produits_Categories.pca_id)
	LEFT JOIN Produits_Familles ON (Produits.pro_famille=Produits_Familles.pfa_id)
	LEFT JOIN Produits_Sous_Familles ON (Produits.pro_sous_famille=Produits_Sous_Familles.psf_id)
	LEFT JOIN Produits_Sous_Sous_Familles ON (Produits.pro_sous_sous_famille=Produits_Sous_Sous_Familles.pss_id)";
	/*if(!empty($crit_stat["pro_type"])){
		if(!empty($crit_stat["pro_type"]==1)){
			$mil.=" AND pro_intra";
		}elseif(!empty($crit_stat["pro_type"]==2)){
			$mil.=" AND pro_inter";
		}
	}*/
	if(!empty($crit_stat["produit_list"])){
		$mil.=" AND pro_id IN (" . $crit_stat["produit_list"] . ")";
	}else{
		if(!empty($crit_stat["pro_categorie"])){
			$mil.=" AND pro_categorie=" . $crit_stat["pro_categorie"];
		}
		if(!empty($crit_stat["pro_famille"])){
			$mil.=" AND pro_famille=" . $crit_stat["pro_famille"];
		}
		if(!empty($crit_stat["pro_sous_famille"])){
			$mil.=" AND pro_sous_famille=" . $crit_stat["pro_sous_famille"];
		}
		if(!empty($crit_stat["pro_sous_sous_famille"])){
			$mil.=" AND pro_sous_sous_famille=" . $crit_stat["pro_sous_sous_famille"];
		}

	}
	if(!empty($mil)){

		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY pro_code_produit;";

	$req=$Conn->query($sql);

	$d_results=$req->fetchAll();
	if(!empty($d_results)){
		foreach($d_results as $r){
			$d_produits[$r["pro_id"]]=array(
				"pro_code_produit" => $r["pro_code_produit"],
				"pro_libelle" => $r["pro_libelle"],
				"pro_archive" => $r["pro_archive"],
				"pca_libelle" => $r["pca_libelle"],
				"pfa_libelle" => $r["pfa_libelle"],
				"psf_libelle" => $r["psf_libelle"],
				"pss_libelle" => $r["pss_libelle"],
				"ca" => 0,
				"qte" => 0
			);
		}
	}

	$d_soc_src=array();

	if($crit_stat["reseau"]){

		$sql="SELECT uso_societe,uso_agence FROM Utilisateurs_Societes WHERE uso_utilisateur=" . $acc_utilisateur . " ORDER BY uso_societe,uso_agence DESC;";
		$req=$Conn->query($sql);
		$d_societes=$req->fetchAll();
		if(!empty($d_societes)){
			foreach($d_societes as $s){
				if(!isset($d_soc_src[$s["uso_societe"]])){
					if(!empty($s["uso_agence"])){
						$d_soc_src[$s["uso_societe"]]=$s["uso_agence"];
					}else{
						$d_soc_src[$s["uso_societe"]]="";
					}
				}else{
					if(!empty($s["uso_agence"])){
						$d_soc_src[$s["uso_societe"]].="," . $s["uso_agence"];
					}else{
						$d_soc_src[$s["uso_societe"]]="";
					}
				}
			}
		}

	}elseif($crit_stat["ca_gc"]){

		// recherche sur GFC
		$d_soc_src[$acc_societe]="";
		if(!empty($crit_stat["agence"])){
			$d_soc_src[$acc_societe]=$crit_stat["agence"];
		}

		// on ajoutes toutes les autres societes pour la ca gc
		$sql="SELECT soc_id FROM Societes WHERE NOT soc_id=4 AND NOT soc_archive ORDER BY soc_id;";
		$req=$Conn->query($sql);
		$d_societes=$req->fetchAll();
		if(!empty($d_societes)){
			foreach($d_societes as $s){
				$d_soc_src[$s["soc_id"]]="";
			}
		}

	}else{

		$d_soc_src[$acc_societe]="";
		if(!empty($crit_stat["agence"])){
			$d_soc_src[$acc_societe]=$crit_stat["agence"];
		}

	}

	// LE CLIENT CONCERNE

	if(!empty($crit_stat["client"])){

		$sql="SELECT cli_code FROM Clients WHERE cli_id=" . $crit_stat["client"] . ";";
		$req=$Conn->query($sql);
		$d_client=$req->fetch();
	}

	/*echo("<pre>");
		print_r($d_produits);
	echo("</pre>");
	die();*/

	$total_ca=0;
	$total_qte=0;
	foreach($d_soc_src as $s_id => $s_age_list){

		$ConnFct=connexion_fct($s_id);

		$sql="SELECT SUM(fli_montant_ht) AS ht,SUM(fli_qte) as qte,fli_produit
		FROM Factures_Lignes INNER JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
		LEFT JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)";

		// FG plus necessaire avec nouvelle condition cf l335
		//if($crit_stat["ca_gc"]==1 AND $s_id!=4){
			// demande du ca GC depuis GFC
			// on n'ajoute une jointure pour ne recupérer que les CA facture en direct
			// le CA facture via GFC sera comptabilisé sur la boucle $s_id==4
			// on ne peut pas se contenter de la table facture car sur la facture vers GFC cli_categorie=2
		//	$sql.=" LEFT JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id)";
		//}
		$sql.=" WHERE fac_date>='". $crit_stat["periode_deb"] . "' AND fac_date<='" . $crit_stat["periode_fin"] . "'";
		if(!empty($s_age_list)){
			$sql.=" AND fac_agence IN (" . $s_age_list. ")";
		}
		if(!empty($crit_stat["pro_type"])){
			$sql.=" AND fli_intra_inter=" . $crit_stat["pro_type"];
		}
		if(!empty($crit_stat["produit_list"])){
			$sql.=" AND fli_produit IN (" . $crit_stat["produit_list"] . ")";
		}else{
			if(!empty($crit_stat["pro_categorie"])){
				$sql.=" AND fli_categorie=" . $crit_stat["pro_categorie"];
			}
			if(!empty($crit_stat["pro_famille"])){
				$sql.=" AND fli_famille=" . $crit_stat["pro_famille"];
			}
			if(!empty($crit_stat["pro_sous_famille"])){
				$sql.=" AND fli_sous_famille=" . $crit_stat["pro_sous_famille"];
			}
			if(!empty($crit_stat["pro_sous_sous_famille"])){
				$sql.=" AND fli_sous_sous_famille=" . $crit_stat["pro_sous_sous_famille"];
			}
		}
		if(!empty($crit_stat["client"])){
			if(empty($crit_stat["filiale"])){
				// client seul
				$sql.=" AND fac_client=" . $crit_stat["client"];
			}else{
				// MM et filiale
				$sql.=" AND (cli_id=" . $crit_stat["client"] . " OR cli_filiale_de=" . $crit_stat["client"] . ")";
			}
		}
		if($crit_stat["ca_gc"]==1 AND $s_id!=4){
			// les fac à gfc sont fac_cli_categorie=2 donc obligé d'utilisé acl_facturation pour ne pas comptabiliser 2 fois le CA
			//
			//$sql.=" AND fac_cli_categorie=2 AND (NOT fli_categorie=1 OR acl_facturation=0)";

			// FG cette nouvelle condition permet d'exclure les fac a gfc sans utiliser actions clients donc compatible 2018 qui n'a pas d'action
			$sql.=" AND fac_cli_categorie=2 AND NOT fac_client=8891";
		}

		$sql.=" GROUP BY fli_produit;";
		$req=$ConnFct->query($sql);
		$d_results_fac=$req->fetchAll();
		if(!empty($d_results_fac)){
			foreach($d_results_fac as $fac){
				$cle_pro=intval($fac["fli_produit"]);
				if(!empty($cle_pro)){



						$d_produits[$cle_pro]["ca"]=$d_produits[$cle_pro]["ca"] + $fac["ht"];
						$d_produits[$cle_pro]["qte"]=$d_produits[$cle_pro]["qte"] + $fac["qte"];

						$total_ca=$total_ca+$fac["ht"];
						$total_qte=$total_qte+$fac["qte"];

				}
			}
		}
	}

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">

<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" >
					<div class="row ">

						<h1 class="text-center" >
					<?php	if(isset($d_client)){
								echo($d_client["cli_code"]);
								if(!empty($crit_stat["filiale"])){
									echo(" et filiale");
								}
								echo("<br/>");
							}
							echo("CA");
							if($crit_stat["pro_type"]==1){
								echo(" INTRA");
							}elseif($crit_stat["pro_type"]==2){
								echo(" INTER");
							}
							echo(" par produit du " . $periode_deb_fr . " au " . $periode_fin_fr);

							?>
						</h1>

						<div class="table-responsive">
							<table class="table table-striped table-hover" >
								<thead>
									<tr class="dark">
										<th>Code</th>
										<th>libellé</th>
										<th>Catégorie</th>
										<th>Famille</th>
										<th>Sous-famille</th>
										<th>Sous sous-famille</th>
										<th>Archivé</th>
										<th>Qte</th>
										<th>CA</th>
									</tr>
								</thead>
								<tbody>
						<?php		if(!empty($d_produits)){
										foreach($d_produits as $p_id => $p){
											if(!empty($p["ca"])){?>
												<tr>
													<td><?=$p["pro_code_produit"]?></td>
													<td><?=$p["pro_libelle"]?></td>
													<td><?=$p["pca_libelle"]?></td>
													<td><?=$p["pfa_libelle"]?></td>
													<td><?=$p["psf_libelle"]?></td>
													<td><?=$p["pss_libelle"]?></td>
													<td>
												<?php	if(!empty($p["pro_archive"])){
															echo("OUI");
														}else{
															echo("&nbsp;");
														} ?>
													</td>
													<td class="text-right" ><?=$p["qte"]?></td>
													<td class="text-right" >
														<a href="stat_com_produit_fac.php?produit=<?=$p_id?>" >
															<?=number_format($p["ca"],2,","," ")?>
														</a>
													</td>
												</tr>
						<?php				}
										} ?>
										<tr>
											<th colspan="7" >Total :</th>
											<td class="text-right" ><?=$total_qte?></td>
											<td class="text-right" >
												<a href="stat_com_produit_fac.php" >
													<?=number_format($total_ca,2,","," ")?>
												</a>
											</td>
										</tr>
						<?php		} ?>
								</tbody>
							</table>
						</div>

					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="stat_commercial.php" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){



			});
		</script>
	</body>
</html>
