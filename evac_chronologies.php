<?php
////////////////// MENU ACTIF HEADER ///////////////////
$menu_actif = 4;
////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
////////////////// TRAITEMENTS SERVEUR ///////////////////
/////////////////PERSONNE CONNECTEE ///////////////////


// C'EST UNE EDITION
if(!empty($_GET['id'])){
	$req=$Conn->query("SELECT * FROM Evacuations_Chronologies_Param ORDER BY ecp_position");
	$Evacuations_Chronologies_Param=$req->fetchAll();
}
?>
<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>SI2P - Orion - Rapports d'évacuation</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
	<form method="post" action="evac_chronologies_enr.php" enctype="multipart/form-data">
		<!-- Start: Main -->
		<div id="main">
			<?php include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper" >


				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="text-left">
											<div class="content-header">
												<h2>
													<?php if(isset($_GET['id'])){?>
														Saisir la chronologie du rapport <b class='text-primary'><?= $_GET['id'] ?></b>
													<?php }?>
												</h2>
											</div>
											<div class="col-md-10 col-md-offset-1">
												<div class="row">

												    <?php if(isset($_GET['id'])){ ?>
												    	<input type="hidden" name="eva_id" value="<?= $_GET['id'] ?>">
												    <?php }?>
												    <!-- COLONNE DE GAUCHE -->
												    <h2>Chronologie</h2>
													<?php foreach($Evacuations_Chronologies_Param as $r){
														$req=$Conn->query("SELECT * FROM Evacuations_Chronologies WHERE ech_chronologie = " . $r['ecp_id'] . " AND ech_evacuation =" . $_GET['id']);
														$Evacuations_Chronologies=$req->fetch();

														?>
												    	<div class="row mt20">
												    		<div class="col-md-4">
																<p><?= $r['ecp_libelle'] ?></p>
															</div>
												    		<div class="col-md-8">
																<textarea class="form-control" rows="5" id="ech_valeur[<?= $r['ecp_id'] ?>]" name="ech_valeur[<?= $r['ecp_id'] ?>]" required ><?= $Evacuations_Chronologies['ech_valeur'] ?></textarea>
															</div>
												    	</div>
												    	<hr>

													<?php } ?>

												</div>


											</div>

										</div>


									</div>
								</div>
							</div>
						</div>
					</div>


				</section>
			</section>

		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<?php if(!isset($_SESSION['retour'])){ ?>
				        <a href="<?= $_SESSION['retour'] ?>" class="btn btn-default btn-sm">
				          <i class="fa fa-long-arrow-left"></i>
				          Retour
				        </a>
				    <?php }else{ ?>
				    	<a href="<?= $_SESSION['retour'] ?>" class="btn btn-default btn-sm">
				          <i class="fa fa-long-arrow-left"></i>
				          Retour
				        </a>
				    <?php } ?>
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
					<button type="submit" id="submit" class="btn btn-success btn-sm">
						<i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php
include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->
<script src="assets/js/custom.js"></script>
<script src="vendor/plugins/holder/holder.min.js"></script>
<script src="vendor/plugins/summernote/summernote.min.js"></script>
<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->
<style type="text/css">
	.note-codable{
		display:none;
	}
</style>

<script>

jQuery(document).ready(function($) {
	$('.summernote_special').summernote({
					lang: 'fr-FR', // default: 'en-US'
					height: 255, //set editable area's height
					focus: false, //set focus editable area after Initialize summernote
					oninit: function() {},
					onChange: function(contents, $editable) {},
					  toolbar: [
					    // [groupName, [list of button]]
					    ['style', ['bold', 'italic', 'underline', 'clear']],
					    ['font', ['strikethrough', 'superscript', 'subscript']],
					    ['fontsize', ['fontsize']],
					    ['color', ['color']],
					    ['para', ['ul', 'ol', 'paragraph']],
					  ]
				});
});

//////////////// FONCTIONS /////////////////


</script>

</body>
</html>
