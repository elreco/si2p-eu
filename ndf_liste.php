<?php
////////////////// MENU ACTIF HEADER ///////////////////

////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
if (isset($_GET['menu_retour'])) {
	unset($_SESSION['retour']);
}

$_SESSION['retour_ndf'] = "ndf_liste.php";
///////////////////// Contrôles des parametres ////////////////////


///////////////////// FIN Contrôles des parametres ////////////////////

////////////////// TRAITEMENTS SERVEUR ///////////////////
// récupérer toutes les ndf
if (isset($_GET['raz'])) {
	unset($_SESSION['ndf_tri']);
}
if (!empty($_POST)) {

	$ndf_annule = 0;
	$ndf_statut = null;
	if ($_POST['ndf_statut']!= 999) {
		$ndf_statut = $_POST['ndf_statut'];
	} else if (!empty($_POST['ndf_annule'])) {
		$ndf_annule = 1;
	}

	if (!empty($_POST['ndf_utilisateur'])) {
		$ndf_utilisateur = $_POST['ndf_utilisateur'];
	} else {
		$ndf_utilisateur = 0;
	}
	$_SESSION['ndf_tri'] = array(
		"ndf_societe"         => $_POST['ndf_societe'],
		"ndf_agence"          => $_POST['ndf_agence'],
		"ndf_utilisateur"   => $ndf_utilisateur,
		"ndf_statut"   => $ndf_statut,
		"ndf_mois"        => $_POST['ndf_mois'],
		"ndf_annee"        => $_POST['ndf_annee'],
		"ndf_annule" 		=> $ndf_annule
	);
}

if (!empty($_SESSION['ndf_tri'])) {

	$critere = array();
	$mil = "";

	if (!empty($_SESSION['ndf_tri']['ndf_societe'])) {
		$mil .= " AND ndf_societe = :ndf_societe";
		$critere["ndf_societe"] = $_SESSION['ndf_tri']['ndf_societe'];
	};
	if (!empty($_SESSION['ndf_tri']['ndf_agence'])) {
		$mil .= " AND ndf_agence = :ndf_agence";
		$critere["ndf_agence"] = $_SESSION['ndf_tri']['ndf_agence'];
	};
	if (!empty($_SESSION['ndf_tri']['ndf_utilisateur'])) {
		$mil .= " AND ndf_utilisateur = :ndf_utilisateur";
		$critere["ndf_utilisateur"] = $_SESSION['ndf_tri']['ndf_utilisateur'];
	};
	if (!is_null($_SESSION['ndf_tri']['ndf_statut'])) {
		$mil .= " AND ndf_statut = :ndf_statut";
		$critere["ndf_statut"] = $_SESSION['ndf_tri']['ndf_statut'];
	};
	if (!empty($_SESSION['ndf_tri']['ndf_mois'])) {
		$mil .= " AND ndf_mois = :ndf_mois";
		$critere["ndf_mois"] = $_SESSION['ndf_tri']['ndf_mois'];
	};
	if (!empty($_SESSION['ndf_tri']['ndf_annee'])) {
		$mil .= " AND ndf_annee = :ndf_annee";
		$critere["ndf_annee"] = $_SESSION['ndf_tri']['ndf_annee'];
	};

	if (!empty($_SESSION['ndf_tri']['ndf_annule'])) {
		$mil .= " AND ndf_statut = 8";
	} else {
		$mil .= " AND ndf_statut != 8";
	}

	$sql = "SELECT * FROM ndf
  LEFT JOIN utilisateurs ON utilisateurs.uti_id = ndf.ndf_utilisateur
  LEFT JOIN ndf_avances ON ndf_avances.nav_ndf = ndf.ndf_id
  LEFT JOIN societes ON utilisateurs.uti_societe = societes.soc_id
  LEFT JOIN agences ON agences.age_id = utilisateurs.uti_agence
  INNER JOIN utilisateurs_societes ON (utilisateurs_societes.uso_societe = ndf.ndf_societe AND utilisateurs_societes.uso_agence = ndf.ndf_agence)";
	/*if(!$rechercheParCorrespondance){
      $sql.=",MAX(Suspects_Correspondances.fco_date)";
	}*/
	$mil .= " AND uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'];
	if ($mil != "") {
		$sql .= " WHERE " . substr($mil, 5, strlen($mil) - 5);
	}
	$sql .= " ORDER BY ndf_quinzaine_deb LIMIT 8000";
	$req = $Conn->prepare($sql);

	if (!empty($critere)) {
		foreach ($critere as $c => $v) {
			$req->bindValue($c, $v);
		}
	};


	$req->execute();
	$notes = $req->fetchAll();
} else {
	$req = $Conn->prepare("SELECT * FROM ndf
		LEFT JOIN utilisateurs ON utilisateurs.uti_id = ndf.ndf_utilisateur
		LEFT JOIN ndf_avances ON ndf_avances.nav_ndf = ndf.ndf_id
		LEFT JOIN societes ON utilisateurs.uti_societe = societes.soc_id
		LEFT JOIN agences ON agences.age_id = utilisateurs.uti_agence
		INNER JOIN utilisateurs_societes ON (utilisateurs_societes.uso_societe = ndf.ndf_societe AND utilisateurs_societes.uso_agence = ndf.ndf_agence)
		WHERE ndf_societe = " . $_SESSION['acces']['acc_societe'] . " AND uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . "
		ORDER BY ndf_quinzaine_deb");
	$req->execute();
	$notes = $req->fetchAll();
	/* var_dump($req);
	var_dump($notes);
	die(); */
}
// fin récupérer toutes les ndf

// chercher les societes et agences
$req=$Conn->query("SELECT DISTINCT soc_id,soc_code,soc_nom FROM Societes LEFT JOIN Utilisateurs_Societes ON uso_societe = soc_id WHERE uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " AND soc_archive=0 ORDER BY soc_nom;");
$req->execute();
$societes=$req->fetchAll();

if (!empty($_SESSION['ndf_tri'])) {
	$req = $Conn->prepare("SELECT age_id, age_nom FROM agences LEFT JOIN Utilisateurs_Societes ON uso_agence = age_id WHERE uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " AND age_archive != 1 AND age_societe = " . $_SESSION['ndf_tri']['ndf_societe']);
	$req->execute();
	$agences = $req->fetchAll();
} else {
	$req = $Conn->prepare("SELECT age_id, age_nom FROM agences LEFT JOIN Utilisateurs_Societes ON uso_agence = age_id WHERE uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " AND age_archive != 1 AND age_societe = " . $_SESSION['acces']['acc_societe']);
	$req->execute();
	$agences = $req->fetchAll();
}
// fin chercher les societes et agences
///////////////////////// FIN TRAITEMENTS SERVEUR /////////////////////////////
///////////////////// TRAITEMENTS PHP ///////////////////////

// récupérer tous les mois dont j'ai besoin
$mois = array(

	1 => "Janvier",
	2 => "Février",
	3 => "Mars",
	4 => "Avril",
	5 => "Mai",
	6 => "Juin",
	7 => "Juillet",
	8 => "Août",
	9 => "Septembre",
	10 => "Octobre",
	11 => "Novembre",
	12 => "Décembre"


);

$statuts = array(
	0 => "En cours de saisie",
	1 => "En cours de validation",
	2 => "Validée RA",
	3 => "Validée RE",
	4 => "Validée Compta",
	5 => "Validée RH",
	6 => "En cours de paiement",
	7 => "Payée"
);

$annees = array(
	1 => date('Y', strtotime(' -5 years')),
	2 => date('Y', strtotime(' -4 years')),
	3 => date('Y', strtotime(' -3 years')),
	4 => date('Y', strtotime(' -2 years')),
	5 => date('Y', strtotime(' -1 years')),
	6 => date('Y'),
	7 => date('Y', strtotime(' +1 year')),
);

$sum = 0;

////////////////////FIN TRAITEMENTS PHP//////////////////////
?>
<!DOCTYPE html>
<html>

<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Notes de frais</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
	<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Favicon -->
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<style type="text/css">
	.panel-tabs>li>a {
		color: #AAA;
		font-size: 14px;
		letter-spacing: 0.2px;
		line-height: 30px;
		/*padding: 9px 20px 11px;*/
		border-radius: 0;
		border-left: 1px solid transparent;
		border-right: 1px solid transparent;
	}

	.nav2>li>a {
		position: relative;
		display: block;
		/* padding: 10px 15px; */
	}
</style>

<body class="sb-top sb-top-sm">


	<!-- Start: Main -->
	<div id="main">

		<?php include "includes/header_def.inc.php"; ?>
		<!-- Start: Content-Wrapper -->
		<section id="content_wrapper">

			<!-- MESSAGES SUCCES -->

			<section id="content" class="animated fadeIn pr20 pl20">
				<h1>Liste des notes de frais</h1>
				<div class="row">
					<div class="col-md-12">
						<div class="panel mb10">
							<div class="panel-heading panel-head-sm">
								<span class="panel-icon">
									<i class="fa fa-cogs" aria-hidden="true"></i>
								</span>
								<span class="panel-title"> Filtrer</span>
							</div>
							<div class="panel-body p10">
								<div class="row admin-forms">
									<form action="ndf_liste.php" method="POST">
										<div class="col-md-3">
											<p class="text-center mb20" style="font-weight:bold;">Société :</p>
											<select id="societe" name="ndf_societe" class="select2 select2-societe" data-acc_drt="1">
												<option value="0">Sélectionner une société...</option>
												<?php foreach ($societes as $s) { ?>
													<option value="<?= $s['soc_id'] ?>" <?php
																						if (!empty($_SESSION['ndf_tri'])) {
																							if ($_SESSION['ndf_tri']['ndf_societe'] == $s['soc_id']) {
																						?> selected <?php
																							}
																						} else {
																							if ($_SESSION['acces']['acc_societe'] == $s['soc_id']) { ?> selected <?php }
																													} ?>><?= $s['soc_nom'] ?></option>
												<?php } ?>
											</select>

										</div>
										<div class="col-md-3">
											<p class="text-center mb20" style="font-weight:bold;">Agence :</p>
											<select id="agence" name="ndf_agence" class="select2 select2-societe-agence">
												<option value="0">Sélectionner une agence...</option>
												<?php foreach ($agences as $a) { ?>
													<option value="<?= $a['age_id'] ?>" <?php

																						if (!empty($_SESSION['ndf_tri'])) {
																							if ($_SESSION['ndf_tri']['ndf_agence'] == $a['age_id']) {
																						?> selected <?php
																							}
																						} else {
																							if (!empty($_SESSION['acces']['acc_agence'])) {

																								if ($_SESSION['acces']['acc_agence'] == $a['age_id']) {
																			?> selected <?php  }
																							}
																						} ?>><?= $a['age_nom'] ?></option>
												<?php } ?>
											</select>

										</div>
										<div class="col-md-3">
											<p class="text-center mb20" style="font-weight:bold;">Utilisateur :</p>
											<select id="utilisateur" name="ndf_utilisateur" class="select2 select2-utilisateur-societe-agence">
												<?php if (!empty($_SESSION['ndf_tri']['ndf_utilisateur'])) {
													$req = $Conn->prepare("SELECT uti_prenom, uti_nom FROM utilisateurs WHERE uti_id = " . $_SESSION['ndf_tri']['ndf_utilisateur']);
													$req->execute();
													$uti = $req->fetch();

												?>
													<option value="<?= $_SESSION['ndf_tri']['ndf_utilisateur'] ?>"><?= $uti['uti_prenom'] ?> <?= $uti['uti_nom'] ?></option>
												<?php } ?>
											</select>

										</div>

										<div class="col-md-3">
											<p class="text-center mb20" style="font-weight:bold;">Statut :</p>
											<select id="statut" name="ndf_statut" class="select2">
												<option value="999">Sélectionnez un statut...</option>
												<?php foreach ($statuts as $k => $s) { ?>
													<option value="<?= $k ?>" <?php
																				if (!empty($_SESSION['ndf_tri']['ndf_statut'])) {
																					if ($k == $_SESSION['ndf_tri']['ndf_statut']) { ?> selected <?php }
																												} ?>><?= $s ?></option>
												<?php } ?>
											</select>

										</div>
										<div class="col-md-3">
											<p class="text-center mb20 mt20" style="font-weight:bold;">Mois :</p>
											<select id="statut" name="ndf_mois" class="select2">
												<option value="0" selected>Mois...</option>
												<?php foreach ($mois as $k => $m) { ?>
													<option value="<?= $k  ?>" <?php
																				if (!empty($_SESSION['ndf_tri'])) {
																					if ($_SESSION['ndf_tri']['ndf_mois'] == $k) { ?> selected <?php }
																												} ?>><?= $m ?></option>
												<?php } ?>
											</select>

										</div>
										<div class="col-md-3">
											<p class="text-center mb20 mt20" style="font-weight:bold;">Année :</p>
											<select id="statut" name="ndf_annee" class="select2">
												<option value="0" selected>Année...</option>

												<?php foreach ($annees as $a) {

												?>
													<option value="<?= $a ?>" <?php if (!empty($_SESSION['ndf_tri'])) {
																					if ($_SESSION['ndf_tri']['ndf_annee'] == $a) { ?> selected <?php
																													}
																												} ?>><?= $a ?></option>
												<?php

												} ?>
											</select>

										</div>
										<div class="col-md-3">
											<div class="checkbox-custom mt35 text-center">
												<input type="checkbox" id="ndf_annule" name="ndf_annule" value="oui" <?php if (!empty($_SESSION['ndf_tri']['ndf_annule'])) { ?> checked <?php } ?>>
												<label for="ndf_annule">Note annulée</label>
											</div>
										</div>
										<div class="col-md-3 text-center mt35">
											<button type="submit" class="btn btn-sm btn-primary">Filtrer</button> <a href="ndf_liste.php?raz" class="btn btn-sm btn-info">RAZ formulaire</a>
										</div>
									</form>



								</div>

							</div>

						</div>
					</div>
					<div class="col-md-12">

						<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
							<thead>
								<tr class="dark">
									<th>Nom</th>
									<th class="no-sort">N°</th>
									<th class="no-sort">Statut</th>
									<th class="no-sort">Mois/Année</th>
									<th class="no-sort">Du</th>
									<th class="no-sort">Au</th>
									<th class="no-sort">Société</th>
									<th class="no-sort">Agence</th>
									<th class="no-sort">TTC</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach ($notes as $n) {
									$req = $Conn->prepare("SELECT SUM(nli_pris_en_charge) as nli_pris_en_charge FROM ndf_lignes WHERE nli_ndf =" . $n['ndf_id']);
									$req->execute();
									$nli = $req->fetch();

									$sum += $nli['nli_pris_en_charge'];
									?>
									<tr>
										<td><?= $n['uti_prenom'] ?> <?= $n['uti_nom'] ?></td>
										<td><?= $n['ndf_id'] ?></td>
										<td>
											<?php
											switch ($n['ndf_statut']) {
												case 0:
													echo "<i class='fa fa-spinner fa-spin'></i> En cours de saisie";
													break;
												case 1:
													echo "<i class='fa fa-spinner fa-spin fa-fw'></i> En cours de validation";
													break;
												case 2:
														echo "<i class='fa fa-check'></i> Validée par le Resp. d'agence le " . convert_date_txt($n['ndf_ra_valide']);
														break;
												case 3:
													echo "<i class='fa fa-check'></i> Validée par le Resp. d'exploitation le " . convert_date_txt($n['ndf_re_valide']);
													break;
												case 4:
													echo "<i class='fa fa-check'></i> Validée par la compta le " . convert_date_txt($n['ndf_compta_valide']);
													break;
												case 5:
													echo "<i class='fa fa-check'></i> Validée par RH le " . convert_date_txt($n['ndf_rh_valide']);
													break;
												case 6:
													echo "<i class='fa fa-spinner fa-spin fa-fw'></i> En cours de paiement";
													break;
												case 7:
													echo "<i class='fa fa-check'></i> Payée";
													break;
												case 8:
													echo "<i class='fa fa-times'></i> Annulée";
													break;
											}
											?>
										</td>
										<td>
											<?php
											if (!empty($mois[$n['ndf_mois']])) {
												echo $mois[$n['ndf_mois']];
											}
											?> <?php
												echo date("Y", strtotime($n['ndf_date']));
												?>
										</td>
										<td>
											<?= convert_date_txt($n['ndf_quinzaine_deb']) ?>
										</td>
										<td>
											<?= convert_date_txt($n['ndf_quinzaine_fin']) ?>
										</td>
										<td><?= $n['soc_nom'] ?></td>
										<td><?= $n['age_nom'] ?></td>
										<td><?= number_format($nli['nli_pris_en_charge'], 2, ',', ' ') ?> €</td>
										<td>
											<a href="ndf.php?id=<?= $n['ndf_id'] ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
											<?php if (in_array($_SESSION['acces']['acc_profil'], [8, 11, 13, 9])) { ?>
												<?php if ($n['ndf_statut'] == 5) { ?>
													<a href="ndf_valide_date.php?id=<?= $n['ndf_id'] ?>&paiement" class="btn btn-sm btn-success" data-toggle="tooltip" title="Basculer en cours de paiement"><i class="fa fa-check-square" aria-hidden="true"></i></a>
												<?php } elseif ($n['ndf_statut'] == 6) { ?>
													<!-- <a href="ndf_valide_date.php?id=<?= $n['ndf_id'] ?>&payee" class="btn btn-sm btn-success" data-toggle="tooltip" title="Payée"><i class="fa fa-check" aria-hidden="true"></i></a> -->
												<?php } ?>
											<?php } ?>
											<?php if ($n['ndf_statut'] < 6 && in_array($_SESSION['acces']['acc_profil'], [8, 11, 13, 9])) { ?>
												<?php if (!empty($n['nav_id'])) { ?>
													<a href="ndf_avance.php?id=<?= $n['ndf_id'] ?>&utilisateur=<?= $n['ndf_utilisateur'] ?>&nav=<?= $n['nav_id'] ?>" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Modifier l'avance"><i class="fa fa-plus" aria-hidden="true"></i></a>
												<?php } else { ?>
													<a href="ndf_avance.php?id=<?= $n['ndf_id'] ?>&utilisateur=<?= $n['ndf_utilisateur'] ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Enregistrer une avance"><i class="fa fa-plus" aria-hidden="true"></i></a>
												<?php } ?>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<div class="col-md-2 col-md-offset-10 mt20">
						<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
							<thead>
								<tr class="dark">
									<th class="  text-center">Total</th>

								</tr>
							</thead>
							<tbody>
								<td class=" text-center">
									<?= number_format($sum, 2, ',', ' ') ?> €
								</td>
							</tbody>
						</table>
					</div>

				</div>





			</section>
		</section>

	</div>
	<!-- End: Main -->
	<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
		<div class="row">
			<div class="col-xs-4 footer-left pt7">
			<?php if(isset($_GET['retour_param'])) { ?>
				<a href="parametre.php" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a>
				<?php } ?>
			</div>
			<div class="col-xs-4 footer-middle pt7"></div>
			<div class="col-xs-4 footer-right pt7">

				<a href="ndf_ouvrir.php" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Ouvrir une note</a>
			</div>
		</div>
	</footer>






	<?php
	include "includes/footer_script.inc.php"; ?>

	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
	<!-- plugin pour les masques formulaires -->
	<script src="assets/js/custom.js"></script>

	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script src="vendor/plugins/summernote/summernote.min.js"></script>
	<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

	<script src="vendor/plugins/jquery.numberformat.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->


	<script>
		// DOCUMENT READY //
		jQuery(document).ready(function() {

			// initilisation plugin datatables
			$('#table_id').DataTable({
				"language": {
					"url": "vendor/plugins/DataTables/media/js/French.json"
				},
				"paging": false,
				"searching": false,
				"info": false,
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});
			// quand on veut imprimer


			////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
		});

		///////////// FONCTIONS //////////////

		//////////// FIN FONCTIONS //////////
	</script>

</body>

</html>