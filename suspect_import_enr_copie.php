<?php
include "includes/controle_acces.inc.php";
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');

include('modeles/mod_client.php');
$_SESSION['import_sus'] = array();
$inputFile = $_FILES['file']['tmp_name'];
$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
unset($_SESSION['import_sus_existe']);

if($extension != "csv"){
	echo 2; 
	exit();
}

$row = 0;

if (($handle = fopen($inputFile, "r")) !== FALSE) {

	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

		if(utf8_encode($data[0]) != "Code;Référence interne;Nom;Siren (000 000 000);Siret (000 00);Ape;Identification tva;Adresse 1;Adresse 2;Adresse 3;Code Postal (00 000);Ville;Nom du contact;Prénom;Email;Téléphone (00 00 00 00 00);Portable (00 00 00 00 00)" && $row == 0){
			echo 3;
			exit();
		}

		$num = count($data);
		
		$row++;
		
		if($row>1){
							
			$data[0]=utf8_encode($data[0]);
			$array = explode(";", $data[0]);
			
			if(count($array)>2){

				$req = $ConnSoc->prepare("SELECT * FROM suspects_adresses WHERE sad_siret = :siret");
				$req->bindParam(':siret', $array[3]);
				$req->execute();

				$sus_adr = $req->fetch();

				$req = $ConnSoc->prepare("SELECT * FROM adresses WHERE adr_siret = :siret");
				$req->bindParam(':siret', $array[3]);
				$req->execute();

				$cli_adr = $req->fetch();
				
				if(empty($cli_adr) && empty($sus_adr)){

					$id = insert_suspect_csv($_POST['cli_commercial'], $_POST['cli_categorie'], $_POST['cli_sous_categorie'], $array[0], $array[1], $array[2],$array[3],$array[4],$array[5],$array[6],$array[7],$array[8],$array[9],$array[10],$array[11],$array[12],$array[13], $array[14], $array[15], $array[16]);
					$_SESSION['import_sus'][$row] = $id;
					$_SESSION['import_sus_time'] = time();

					
				}else{

					$_SESSION['import_sus_existe'][1] = $sus_adr['sad_siret'];
					$_SESSION['import_sus_existe'][2] = $cli_adr['adr_siret'];
					$erreur = 1;

				}

			}
		}
	}
	fclose($handle);
}

if($erreur == 1){
	echo 4;
}else{
	echo 1;
}

