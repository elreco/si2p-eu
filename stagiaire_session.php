<?php 
/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
// session retour

// fin session retour

/////////////// TRAITEMENTS BDD ///////////////////////

?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Sessions stagiaires</title> 
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
<form method="post" action="stagiaire_session_enr.php" enctype="multipart/form-data">
<!-- Start: Main -->
<div id="main">
<?php include "includes/header_def.inc.php"; ?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper" >
    <!--DEBUT CONTENT -->
    <section id="content" class="animated fadeIn">
        <!--DEBUT ROW -->
        <div class="row"> 
        <!--DEBUT COL-MD-10 -->
            <div class="col-md-10 col-md-offset-1">
              <!--DEBUT ADMIN FORM -->
                <div class="admin-form theme-primary ">
                    <!--DEBUT PANEL HEADING -->
                    <div class="panel heading-border panel-primary">
                      <!--DEBUT PANEL BODY -->
                        <div class="panel-body bg-light text-center">
                            <div class="content-header">
                                <h2>Enregistrer <b class="text-primary">un stagiaire</b></h2>            
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row">
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-6">
                                        <!-- DATE -->
                                        <div class="row mt5" >
                                            <div class="col-md-4 mt10 text-right" >Date de session :</div>
                                            <div class="col-md-8" >
                                                <span class="field prepend-icon prepend-icon-sm">
                                                    <input type="text" name="ses_date" id="date_session" class="gui-input datepicker date" placeholder="Selectionner une date" value="" />
                                                    <label class="field-icon" for="date_session" ><i class="fa fa-calendar-o"></i></label>
                                                </span>
                                            </div>
                                        </div>
                                        <!-- FIN DATE -->
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                    <!-- COLONNE DE DROITE -->
                                    
                                    <!-- FIN COLONNE DE DROITE -->
                                </div>
                                <div class="row">
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-6">
                                        <div class="row mt5" >
                                            <div class="col-md-6 mt10 text-right" >Heure du début de la session :</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="ses_h_deb" class="gui-input gui-input-sm heure" placeholder="" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-6">
                                        <div class="row mt5" >
                                            <div class="col-md-6 mt10 text-right" >Heure de fin de la session :</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="ses_h_fin" class="gui-input gui-input-sm heure" placeholder="" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                </div>
                               
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="row mt25" >
                                            <div class="col-md-6 mt10 text-right" >Titre :</div>
                                            <div class="col-md-6" >
                                                <div class="select2-sm">
                                                    <select class="select2" id="" name="sta_titre" required>
                                                        <?php foreach($base_civilite as $k => $b){ 
                                                            if($k > 0){
                                                            ?>
                                                        <option value="<?= $k ?>"><?= $b ?></option>
                                                        <?php 
                                                            }
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-4">
                                        <div class="row mt25" >
                                            <div class="col-md-6 mt10 text-right" >Nom :</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="sta_nom" class="gui-input gui-input-sm nom" placeholder="" value="" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-4">
                                        <div class="row mt25" >
                                            <div class="col-md-6 mt10 text-right" >Prenom :</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="sta_prenom" class="gui-input gui-input-sm prenom" placeholder="" value="" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                </div>
                                <div class="row">
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-4">
                                        <div class="row mt5" >
                                            <div class="col-md-6 mt10 text-right" >Date de naissance :</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="sta_naissance" class="gui-input gui-input-sm datepicker date" placeholder="" value="" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                    <!-- COLONNE DE MILIEU -->
                                    <div class="col-md-4">
                                        <div class="row mt5" >
                                            <div class="col-md-6 mt10 text-right" >CP naissance :</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="sta_cp_naiss" class="gui-input gui-input-sm code-postal" placeholder="" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE MILIEU -->
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-4">
                                        <div class="row mt5" >
                                            <div class="col-md-6 mt10 text-right" >Ville de naissance :</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="sta_ville_naiss" class="gui-input gui-input-sm nom" placeholder="" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                </div>
                                <div class="row">
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-4">
                                        <div class="row mt25" >
                                            <div class="col-md-6 mt10 text-right" >Adresse :</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="sta_ad1" class="gui-input gui-input-sm" placeholder="" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                    <!-- COLONNE DE MILIEU -->
                                    <div class="col-md-4">
                                        <div class="row mt25" >
                                            <div class="col-md-6 mt10 text-right" >Adresse (complément 1):</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="sta_ad2" class="gui-input gui-input-sm" placeholder="" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE MILIEU -->
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-4">
                                        <div class="row mt25" >
                                            <div class="col-md-6 mt10 text-right" >Adresse (complément 2):</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="sta_ad3" class="gui-input gui-input-sm" placeholder="" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                </div>
                                <div class="row">
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-6">
                                        <div class="row mt5" >
                                            <div class="col-md-6 mt10 text-right" >Code postal :</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="sta_cp" class="gui-input gui-input-sm code-postal" placeholder="" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                    <!-- COLONNE DE MILIEU -->
                                    <div class="col-md-6">
                                        <div class="row mt5" >
                                            <div class="col-md-6 mt10 text-right" >Ville :</div>
                                            <div class="col-md-6" >
                                                <input type="text" name="sta_ville" class="gui-input gui-input-sm nom" placeholder="" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE MILIEU -->
                                    
                                </div>
                                <div class="row">
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-3">
                                        <div class="row mt25" >
                                            <div class="col-md-4 mt10 text-right" >Téléphone :</div>
                                            <div class="col-md-8" >
                                                <input type="text" name="sta_tel" class="gui-input gui-input-sm telephone" placeholder="" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row mt25" >
                                            <div class="col-md-4 mt10 text-right" >Portable :</div>
                                            <div class="col-md-8" >
                                                <input type="text" name="sta_portable" class="gui-input gui-input-sm telephone" placeholder="" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                    <!-- COLONNE DE MILIEU -->
                                    <div class="col-md-3">
                                        <div class="row mt25" >
                                            <div class="col-md-4 mt10 text-right" >Mail :</div>
                                            <div class="col-md-8" >
                                                <input type="mail" name="sta_mail" class="gui-input gui-input-sm" placeholder="" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE MILIEU -->
                                    <!-- COLONNE DE MILIEU -->
                                    <div class="col-md-3">
                                        <div class="row mt25" >
                                            <div class="col-md-4 mt10 text-right" >Mail perso :</div>
                                            <div class="col-md-8" >
                                                <input type="mail" name="sta_mail_perso" class="gui-input gui-input-sm" placeholder="" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE MILIEU -->
                                    
                                </div>
                                <div class="row">
                                    <!-- COLONNE DE GAUCHE -->
                                    <div class="col-md-12">
                                        <div class="row mt25" >
                                            <div class="col-md-3 mt10 text-right" >Fichier :</div>
                                            <div class="col-md-6" >
                                                <input type="file" name="doc" class="gui-input" placeholder="" value="" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIN COLONNE DE GAUCHE -->
                                    
                                </div>
                               

                                
                            </div>
                        <!-- FIN COL-MD-10 -->
                        </div>
                    <!-- FIN PANEL BODY -->
                    </div>
                <!-- FIN PANEL HEADING -->
                </div>
            <!-- FIN ADMIN FORM -->
            </div>
        </div>

        </section>

    </section>

</div>
    <!-- End: Main -->
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">
                <a href="stagiaire_session_liste.php" class="btn btn-default btn-sm">
                    <i class="fa fa-long-arrow-left"></i>
                            Retour
                </a>
            </div>
            <div class="col-xs-6 footer-middle">

            </div>
            <div class="col-xs-3 footer-right">
                
                <button type="submit" name="search" class="btn btn-success btn-sm">
                    <i class='fa fa-floppy-o'></i> Enregistrer
                </button>
            </div>
        </div>
    </footer>
</form>
<?php include "includes/footer_script.inc.php"; ?>  

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<!-- SCRIPT SELECT2 -->
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
    ////////////// FONCTIONS ///////////////


    ////////////// FIN FONCTIONS ///////////////


    <!-- -->


    //////// ÉVENEMENTS UTILISATEURS //////////


    //////// FIN ÉVENEMENTS UTILISATEURS //////
</script>

</body>
</html>