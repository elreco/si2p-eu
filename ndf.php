<?php
////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
if (isset($_GET['ndf_menu'])) {
	unset($_SESSION['retour_ndf']);
}
if (isset($_GET['menu_retour'])) {
	unset($_SESSION['retour']);
}
///////////////////// Contrôles des parametres ////////////////////


if (isset($_GET['id'])) {
	$_GET['id'] = intval($_GET['id']);
}

///////////////////// FIN Contrôles des parametres ////////////////////

////////////////// TRAITEMENTS SERVEUR ///////////////////

// catégories de frais
$req = $Conn->prepare("SELECT * FROM ndf_categories");
$req->execute();
$categories = $req->fetchAll();
// fin catégories de frais
$req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id = " . $_SESSION['acces']['acc_ref_id']);
$req->execute();
$utilisateur = $req->fetch();

if (isset($_GET['id'])) {
	$req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id = " . $_GET['id']);
	$req->execute();
	$ndf_actuelle = $req->fetch();
} else {
	if($utilisateur['uti_carte_affaire'] == 1) {
		$dateDeb = new DateTime(date('Y') . "-" . date('m') . "-01");
		$dateDeb2 = new DateTime(date('Y') . "-" . date('m') . "-01");
		$dateFin = $dateDeb2->modify('last day of this month');
	} else {
		if (date('d') > 15) {
			$dateDeb = new DateTime(date('Y') . "-" . date('m') . "-16");
			$dateDeb2 = new DateTime(date('Y') . "-" . date('m') . "-16");
			$dateFin = $dateDeb2->modify('last day of this month');
		} else {
			$dateDeb = new DateTime(date('Y') . "-" . date('m') . "-01");
			$dateFin = new DateTime(date('Y') . "-" . date('m') . "-15");
		}
	}

	$dateDeb = $dateDeb->format('Y-m-d');
	$dateFin = $dateFin->format('Y-m-d');

	// aller chercher la note
	$req = $Conn->prepare("SELECT * FROM ndf WHERE
	ndf_societe = " . $_SESSION['acces']['acc_societe'] . "
	AND ndf_agence = " . $_SESSION['acces']['acc_agence'] . "
	AND ndf_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . "
	AND ndf_quinzaine_deb >= '" . $dateDeb . "' AND ndf_quinzaine_fin <=  '" . $dateFin . "'
	ORDER BY ndf_quinzaine_fin DESC
	LIMIT 1");
	$req->execute();
	$ndf_actuelle = $req->fetch();
}

if (!empty($ndf_actuelle)) {
	// trouver l'utilisateur
	$req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id = " . $ndf_actuelle['ndf_utilisateur']);
	$req->execute();
	$utilisateur = $req->fetch();
	// fin trouver l'utilisateur

	// société de l'utilisateur
	$req = $Conn->prepare("SELECT soc_id, soc_nom, soc_code, soc_logo_1 FROM societes WHERE soc_id = " . $ndf_actuelle['ndf_societe']);
	$req->execute();
	$societe = $req->fetch();
	// fin société de l'utilisateur

	// agence de l'utilisateur
	$req = $Conn->prepare("SELECT age_id, age_nom, age_code, age_logo_1 FROM agences WHERE age_id = " . $ndf_actuelle['ndf_agence']);
	$req->execute();
	$agence = $req->fetch();
	// fin agence de l'utilisateur

	// les lignes de la note de frais
	$req = $Conn->prepare("SELECT * FROM ndf_lignes WHERE nli_ndf = " . $ndf_actuelle['ndf_id']);
	$req->execute();
	$ndf_lignes = $req->fetchAll();
	// fin les lignes de la note de frais

	//debug($ndf_lignes);
	//die();
	// calcul du total ttc
	$req = $Conn->prepare("SELECT SUM(nli_ttc-nli_depassement_valide) as nli_ttc_format FROM ndf_lignes WHERE nli_refus = 0 AND nli_ndf = " . $ndf_actuelle['ndf_id']);
	$req->execute();
	$ndf_ttc = $req->fetch();
	// fin calcul du total ttc

	// calcul des avances
	$req = $Conn->prepare("SELECT nav_ttc FROM ndf_avances WHERE nav_ndf = " . $ndf_actuelle['ndf_id']);
	$req->execute();
	$ndf_avances = $req->fetch();
	// fin calcul des avances

	// calcul du solde
	if (!empty($ndf_avances['nav_ttc'])) {
		$ndf_solde = $ndf_ttc['nli_ttc_format'] - $ndf_avances['nav_ttc'];
	} else {
		$ndf_solde = $ndf_ttc['nli_ttc_format'];
	}

	// fin calcul du solde

	// récupérer les avances des notes de frais
	$req = $Conn->prepare("SELECT * FROM ndf_avances WHERE nav_ndf = " . $ndf_actuelle['ndf_id']);
	$req->execute();
	$avances = $req->fetch();
	// fin récupérer les avances des notes de frais
} else {
	// société de l'utilisateur
	$req = $Conn->prepare("SELECT soc_id, soc_nom, soc_code, soc_logo_1 FROM societes WHERE soc_id = " . $_SESSION['acces']['acc_societe']);
	$req->execute();
	$societe = $req->fetch();
	// fin société de l'utilisateur

	// agence de l'utilisateur
	$req = $Conn->prepare("SELECT age_id, age_nom, age_code, age_logo_1 FROM agences WHERE age_id = " . $_SESSION['acces']['acc_agence']);
	$req->execute();
	$agence = $req->fetch();
	// fin agence de l'utilisateur

	/////////////////// DATE A DATE /////////////////////////
	$format = "d/m/Y";

	// le premier jour du mois
	$premier_jour = new DateTime("first day of this month");
	$premier_jour = $premier_jour->format('Y-m-d');
	// le milieu 15
	$milieu_1 = DateTime::createFromFormat($format, "15/" . date("m") . "/" . date("Y"));
	$milieu_1_str = $milieu_1->format('Y-m-d');
	// le milieu 16
	$milieu_2 = "16/" . date("m") . "/" . date("Y");
	$dernier_jour = new DateTime('last day of this month');
	$dernier_jour = $dernier_jour->format('Y-m-d');
	// le dernier jour du mois
	$now = DateTime::createFromFormat($format, date("Y-m-d"));

	// déterminer la quinzaine
	/*si l'utilisateur a une carte affaire :
	il faut que ce soit découpé sur le mois et pas en quinzaine */
	if ($utilisateur['uti_carte_affaire'] == 1) {
		$ndf_actuelle['ndf_quinzaine_deb'] = $premier_jour;
		$ndf_actuelle['ndf_quinzaine_fin'] = $dernier_jour;
	} else {
		if ($now > $milieu_1) {
			/////////////// DATE 1 ==> DEBUT /////////////////
			/////////////// DATE 2 ==> FIN ///////////////////
			$ndf_actuelle['ndf_quinzaine_deb'] = $milieu_2;
			$ndf_actuelle['ndf_quinzaine_fin'] = $dernier_jour;
		} else {
			/////////////// DATE 1 ==> DEBUT /////////////////
			/////////////// DATE 2 ==> FIN ///////////////////
			$ndf_actuelle['ndf_quinzaine_deb'] = $premier_jour;
			$ndf_actuelle['ndf_quinzaine_fin'] = $milieu_1_str;
		}
	}

	///////////////// FIN DATE A DATE ///////////////////////////////
	// pas de ndf en paramètre et pas de ndf trouvée
	$ndf_actuelle['ndf_societe'] = $utilisateur['uti_societe'];
	$ndf_actuelle['ndf_agence'] = $utilisateur['uti_agence'];
	$ndf_actuelle['ndf_utilisateur'] = $utilisateur['uti_id'];
	$ndf_actuelle['ndf_statut'] = 0;
	$ndf_actuelle['ndf_id'] = 0;
	$ndf_actuelle['ndf_ttc'] = 0;
}


// validation re
if (!empty($ndf_actuelle['ndf_re'])) {
	$req = $Conn->prepare("SELECT uti_id,uti_prenom, uti_nom FROM utilisateurs WHERE uti_id =" . $ndf_actuelle['ndf_re']);
	$req->execute();
	$re = $req->fetch();
}

// validation ca
if (!empty($ndf_actuelle['ndf_ra'])) {
	$req = $Conn->prepare("SELECT uti_id,uti_prenom, uti_nom FROM utilisateurs WHERE uti_id =" . $ndf_actuelle['ndf_ra']);
	$req->execute();
	$ra = $req->fetch();
}
// validation

// validation compta
if (!empty($ndf_actuelle['ndf_compta'])) {
	$req = $Conn->prepare("SELECT uti_id, uti_prenom, uti_nom FROM utilisateurs WHERE uti_id =" . $ndf_actuelle['ndf_compta']);
	$req->execute();
	$compta = $req->fetch();
}
// validation

// validation rh
if (!empty($ndf_actuelle['ndf_rh'])) {
	$req = $Conn->prepare("SELECT uti_id, uti_prenom, uti_nom FROM utilisateurs WHERE uti_id =" . $ndf_actuelle['ndf_rh']);
	$req->execute();
	$rh = $req->fetch();
}
// validation

///////////////////////// FIN TRAITEMENTS SERVEUR /////////////////////////////
///////////////////// TRAITEMENTS PHP ///////////////////////

// aller chercher la prochaine ndf

$req = $Conn->prepare("SELECT * FROM ndf
WHERE ndf_societe = " . $_SESSION['acces']['acc_societe'] . "
AND ndf_agence = " . $_SESSION['acces']['acc_agence'] . "
AND ndf_utilisateur = " . $utilisateur['uti_id'] . "
AND ndf_id = (SELECT ndf_id FROM ndf WHERE ndf_societe = " . $_SESSION['acces']['acc_societe'] . "
AND ndf_agence = " . $_SESSION['acces']['acc_agence'] . "
AND ndf_utilisateur = " . $utilisateur['uti_id'] . "
AND ndf_quinzaine_deb > '" . $ndf_actuelle['ndf_quinzaine_fin'] . "' ORDER BY ndf_quinzaine_deb ASC LIMIT 1)
AND ndf_id != " . $ndf_actuelle['ndf_id']);
$req->execute();
$ndf_prochaine = $req->fetch();

// aller chercher la prochain ndf fin

// aller chercher la précedente ndf
$req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_societe = " . $_SESSION['acces']['acc_societe'] . "
	AND ndf_agence = " . $_SESSION['acces']['acc_agence'] . "
	AND ndf_utilisateur = " . $utilisateur['uti_id'] . "
	AND ndf_id = (SELECT ndf_id FROM ndf  WHERE ndf_societe = " . $_SESSION['acces']['acc_societe'] . "
	AND ndf_agence = " . $_SESSION['acces']['acc_agence'] . "
	AND ndf_utilisateur = " . $utilisateur['uti_id'] . "
	AND ndf_quinzaine_fin < '" . $ndf_actuelle['ndf_quinzaine_deb'] . "'
	ORDER BY ndf_quinzaine_fin DESC LIMIT 1)
	AND ndf_id != " . $ndf_actuelle['ndf_id']);
$req->execute();
$ndf_precedente = $req->fetch();

// aller chercher la précedente ndf

////////////////////FIN TRAITEMENTS PHP//////////////////////

///////////////////// TESTS DES DROITS //////////////////////

// contrôle utilisateur

if ($_SESSION['acces']['acc_ref_id'] != $ndf_actuelle['ndf_utilisateur'] AND !in_array($_SESSION['acces']['acc_profil'], [7, 13, 12, 8, 11, 15, 10, 9])) {
	die("Vous n'êtes pas autorisé à être ici");
}

$req=$Conn->query("SELECT uso_societe FROM Utilisateurs_Societes WHERE uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " AND uso_societe = " . $ndf_actuelle['ndf_societe'] . " AND uso_agence = " . $ndf_actuelle['ndf_agence']);
$req->execute();
$societes=$req->fetch();

if (empty($societes)) {
	die("Vous n'êtes pas autorisé à être ici");
}
/////////////////// FIN TESTS DES DROITS ////////////////////
?>
<!DOCTYPE html>
<html>

<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Notes de frais</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
	<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


	<!-- Favicon -->
	<link rel="icon" type="image/png" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<style type="text/css">
	.panel-tabs>li>a {
		color: #AAA;
		font-size: 14px;
		letter-spacing: 0.2px;
		line-height: 30px;
		/*padding: 9px 20px 11px;*/
		border-radius: 0;
		border-left: 1px solid transparent;
		border-right: 1px solid transparent;
	}

	.nav2>li>a {
		position: relative;
		display: block;
		/* padding: 10px 15px; */
	}
</style>
<style type="text/css" media="print">
	@page {
		size: landscape;
	}
</style>

<body class="sb-top sb-top-sm">

	<form method="post" action="commande_enr.php" enctype="multipart/form-data">
		<!-- Start: Main -->
		<div id="main">

			<?php include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<section id="content" class="animated fadeIn pr20 pl20">

					<h1>Note de frais <small>- du <b><?= convert_date_txt($ndf_actuelle['ndf_quinzaine_deb']); ?></b> au <b><?= convert_date_txt($ndf_actuelle['ndf_quinzaine_fin']); ?></b></small></h1>

					<div class="row">

						<div class="col-md-12">
							<div class="panel mb10">
								<div class="panel-heading panel-head-sm">
									<span class="panel-icon">
										<i class="fa fa-file-text-o" aria-hidden="true"></i>
									</span>
									<span class="panel-title"> Détails</span>
								</div>
								<div class="panel-body p10">
									<div class="row">
										<div class="col-md-3 col-xs-3 text-center align-self-center">
											<?php if (!empty($agence)) { // aller chercher le logo de l'agence si elle existe
											?>
												<img src="documents/societes/logos/<?= $agence['age_logo_1'] ?>" class="p30">
											<?php } else { ?>
												<img src="documents/societes/logos/<?= $societe['soc_logo_1'] ?>" class="p30">
											<?php }  ?>
										</div>
										<div>
											<div class="col-md-2 col-xs-3 pt35 pl35 pb20">
												<p><strong>Nom et prénom : </strong> <?= $utilisateur['uti_prenom'] ?> <?= $utilisateur['uti_nom'] ?></p>
												<p><strong>Statut : </strong>
													<?php if (empty($ndf_actuelle) or $ndf_actuelle['ndf_statut'] == 0) { // si la note de frais n'existe pas ou l'état est a 0
													?>
														<i class="fa fa-spinner fa-spin"></i> En cours de saisie
													<?php } else { // sinon traduire les statuts de la note de frais
														switch ($ndf_actuelle['ndf_statut']) {
															case 1:
																echo "<i class='fa fa-spinner fa-spin fa-fw'></i> En cours de validation";
																break;
															case 2:
																	echo "<i class='fa fa-check'></i> Validée par le Resp. d'agence";
																	break;
															case 3:
																echo "<i class='fa fa-check'></i> Validée par le Resp. d'exploitation";
																break;
															case 4:
																echo "<i class='fa fa-check'></i> Validée par la compta";
																break;
															case 5:
																echo "<i class='fa fa-check'></i> Validée par RH";
																break;
															case 6:
																echo "<i class='fa fa-spinner fa-spin fa-fw'></i> En cours de paiement";
																break;
															case 7:
																echo "<i class='fa fa-check'></i> Payée";
																break;
															case 8:
																echo "<i class='fa fa-times'></i> Annulée";
																break;
														}
													} ?>
												</p>
												<?php if (!empty($ndf_actuelle['ndf_id'])) { // l'id de la note de frais
												?>
													<p><strong>Numéro : </strong> <?= $ndf_actuelle['ndf_id'] ?></p>
												<?php } ?>
											</div>
											<div class="col-md-2 col-xs-3 pt35 pl35 pr35 pb20">
												<p><strong>Société : </strong> <?= $societe['soc_nom'] ?></p>
												<p><strong>Agence : </strong> <?= !empty($agence) ? $agence['age_nom'] : '' ?></p>
											</div>
											<div class="col-md-2 col-xs-3 pt35 pl35 pr35 pb20">
												<p><strong>Matricule : </strong> <?= $utilisateur['uti_matricule'] ?></p>
											</div>
											<div class="col-md-2 col-xs-3 pt35 pl35 pr35 pb20">
												<p><strong>Du : </strong> <?= convert_date_txt($ndf_actuelle['ndf_quinzaine_deb']); ?></p>
												<p><strong>Au : </strong> <?= convert_date_txt($ndf_actuelle['ndf_quinzaine_fin']); ?></p>
											</div>
										</div>
									</div>

								</div>

							</div>
						</div>


					</div>
					<div class="row ">
						<div class="col-md-12 mb10">
							<table class="table table-striped table-hover text-center" id="table_id" style="border:1px solid #e2e2e2;">
								<thead>
									<tr class="dark ">
										<th class="text-center">Date</th>
										<th class="no-sort text-center ">Type de dépense</th>
										<th class="no-sort text-center">Frais</th>
										<th class="no-sort text-center">Détail</th>
										<th class="no-sort text-center">Quantité</th>
										<th class="no-sort text-center">Total T.T.C (€)</th>
										<?php if (in_array($_SESSION['acces']['acc_profil'], [7, 13, 9, 12, 8, 11, 15, 10])) { ?>
											<th class="no-sort text-center">HT (€)</th>
										<?php } ?>
										<?php if (!empty($ndf_actuelle) && $ndf_actuelle['ndf_statut'] > 0 && in_array($_SESSION['acces']['acc_profil'], [7, 13, 9, 12, 8, 11, 15, 10])) { ?>
											<th class="no-sort text-center">TVA (€)</th>
										<?php } ?>
										<th class="no-sort text-center">Pris en charge (€)</th>
										<th class="no-sort text-center">Dépassement à valider (€)</th>
										<?php if (!empty($ndf_actuelle) && $ndf_actuelle['ndf_statut'] > 0) { ?>
											<th class="no-sort text-center">Dépassement accordé (€)</th>
										<?php } ?>
										<th class="no-sort text-center">Véhicule</th>
										<th class="text-center">Numéro de justificatif</th>

										<th class="text-center no-sort actions_th">Actions</th>
									</tr>
								</thead>
								<tbody>


									<?php
									$total_ht = 0;
									$total_tva = 0;
									if (!empty($ndf_lignes)) {

										foreach ($ndf_lignes as $n) {
											// récupérer les categories de profils
											$req = $Conn->prepare("SELECT * FROM ndf_categories_profils WHERE ncp_profil = :ncp_profil AND ncp_categorie = :ncp_categorie AND ncp_type = :ncp_type");
											$req->bindParam("ncp_categorie", $n['nli_categorie']);
											$req->bindParam("ncp_profil", $utilisateur['uti_profil']);
											$req->bindParam("ncp_type", $n['nli_type']);
											$req->execute();
											$cp = $req->fetch();

											// récupérer le vehicule
											$sql = "SELECT veh_id, veh_libelle, veh_immat FROM vehicules WHERE veh_id = " . $n['nli_vehicule'];
											$req = $Conn->query($sql);
											$d_vehicule = $req->fetch();

											// calcul TVA

											// catégorie ligne

											$ttc = $n['nli_ttc'] - $n['nli_depassement_valide'];
											// montant ht = ttc
											$ht = $ttc - $n['nli_montant_tva'];
											if (empty($n['nli_refus'])) {
												$total_ht = $total_ht + $ht;
												$total_tva = $total_tva + ($ttc - $ht);
											}
									?>
											<tr
											<?php if(!empty($n['nli_refus'])) { ?>
												class="danger"
											<?php } elseif ($n['nli_type'] == 2) { ?> class="success" <?php } ?>

											>
												<td><?= convert_date_txt($n['nli_date']) ?></td>
												<td>
													<?php
													switch ($n['nli_type']) {
														case 1:
															echo "Classique";
															break;
														case 2:
															echo "Formation";
															break;
													}
													?>
												</td>

												<td>
													<?php foreach ($categories as $c) {
														if ($c['nca_id'] == $n['nli_categorie']) {
													?>
															<?= $c['nca_libelle'] ?>
													<?php
														}
													} ?>
												</td>
												<td style="max-width: 150px;">
													<?= $n["nli_commentaire"] ?>
													<?php if(in_array($n['nli_categorie'], [31,37,30,36,16])) {
														$sql="SELECT * FROM ndf_invitations
														LEFT JOIN utilisateurs ON (utilisateurs.uti_id = ndf_invitations.nin_utilisateur_invite)
														WHERE nin_ndf = " . $ndf_actuelle['ndf_id'] . " AND nin_ligne = " . $n['nli_id'] . " ORDER BY uti_nom, uti_prenom DESC";
														$req = $Conn->query($sql);
														$ndf_invitations=$req->fetchAll();

														if (in_array($n['nli_categorie'], [31,37,16])) {
														foreach ($ndf_invitations as $i => $invitation) {
														?>
														<strong>Utilisateur invité <?= $i + 1 ?></strong><br> <?= $invitation['uti_prenom'] ?> <?= $invitation['uti_nom'] ?><br><br>
													<?php
															}
														} else { ?>
															<strong>Client/Fournisseur invité</strong><br> <?php if(!empty($ndf_invitations)) { ?><?= $ndf_invitations[0]['nin_autre_client'] ?><?php } else { ?><i>Non renseigné</i><?php }?>
														<?php
														}
													}?>
												</td>
												<td>
													<?= $n["nli_qte"] ?>
												</td>
												<td>
													<?= number_format($n['nli_ttc'], 2, ',', ' ')  ?>
												</td>
												<?php if (in_array($_SESSION['acces']['acc_profil'], [7, 13, 12, 8, 11, 15,10, 9])) { ?>
													<td class="nli_ht_<?= $n['nli_id'] ?>">
														<?= number_format($n['nli_ttc'] - $n['nli_montant_tva'], 2, ',', ' ') ?>
													</td>
												<?php } ?>
												<?php if (!empty($ndf_actuelle) && $ndf_actuelle['ndf_statut'] > 0 && in_array($_SESSION['acces']['acc_profil'], [7, 9, 13, 12, 8, 11, 15,10])) { ?>
													<td>
														<?php if (!empty($n['nli_montant_tva'])) { ?>
															<?= number_format($n['nli_montant_tva'], 2, ',', ' ') ?>
														<?php } ?>

													</td>
												<?php }	?>
												<td class="nli_pris_en_charge_<?= $n['nli_id'] ?>">
													<?= number_format($n['nli_pris_en_charge'], 2, ',', ' ') ?>
												</td>
												<td>
													<?php if (!empty($n['nli_depassement_valide'])) { ?>
														<?= number_format($n['nli_depassement_valide'], 2, ',', ' ') ?>
													<?php } ?>
												</td>
												<?php if (!empty($ndf_actuelle) && $ndf_actuelle['ndf_statut'] > 0) { ?>
													<td>
														<?php if (!empty($n['nli_depassement_accorde'])) { ?>
															<?= number_format($n['nli_depassement_accorde'], 2, ',', ' ') ?>
														<?php } ?>
													</td>
												<?php }	?>
												<td>
													<?php if (!empty($d_vehicule)) { ?>
														<?= $d_vehicule['veh_libelle'] ?>
													<?php } ?>
												</td>
												<td>
													<?= $n['nli_numero'] ?>
												</td>
												<td class="actions_th" style="white-space: nowrap">
													<?php if (!empty($ndf_actuelle) && $ndf_actuelle['ndf_statut'] < 5) { // si le statut est différent de en cours de saisie
													?>
														<?php if (in_array($_SESSION['acces']['acc_profil'], [7, 13, 9, 12, 8, 11, 15,10]) or ($_SESSION['acces']['acc_ref_id'] == $ndf_actuelle['ndf_utilisateur'] && $ndf_actuelle['ndf_statut'] == 0)) { ?>
															<?php if (
																$ndf_actuelle['ndf_statut'] == 0
																or
																($ndf_actuelle['ndf_statut'] == 1 && $_SESSION['acces']['acc_ref_id'] == $ndf_actuelle['ndf_ra'])
																or
																($ndf_actuelle['ndf_statut'] == 2 && $_SESSION['acces']['acc_ref_id'] == $ndf_actuelle['ndf_re'])
																or
																($ndf_actuelle['ndf_statut'] == 3 && in_array($_SESSION['acces']['acc_profil'], [8, 11, 13, 9]))
																or
																($ndf_actuelle['ndf_statut'] == 4 && in_array($_SESSION['acces']['acc_profil'], [7, 12, 13, 9]))
															) { ?>
																<?php if(in_array($_SESSION['acces']['acc_profil'], [7,13,12, 10, 11, 12, 9, 14]) && !empty($n['nli_depassement_valide']) && $ndf_actuelle['ndf_statut'] > 0 && empty($n['nli_refus'])) { ?>
																	<a href="ndf_paiement.php?id=<?= $ndf_actuelle['ndf_id'] ?>&nli=<?= $n['nli_id'] ?>" data-toggle="tooltip" title="Valider le dépassement" class="btn btn-info btn-sm"><i class="fa fa-check"></i></a>
																<?php } ?>
																<?php if((empty($n['nli_depassement_accorde']) OR $n['nli_depassement_accorde'] < $n['nli_depassement_valide']) && in_array($_SESSION['acces']['acc_profil'], [7,13,12, 10, 15, 12, 9, 14]) && $ndf_actuelle['ndf_statut'] > 0) { ?>
																	<?php if(empty($n['nli_refus'])) { ?>
																		<a href="ndf_nli_refus.php?id=<?= $ndf_actuelle['ndf_id'] ?>&nli=<?= $n['nli_id'] ?>&refus=1" data-toggle="tooltip" title="Refuser le frais" class="btn btn-danger btn-sm"><i class="fa fa-user-times"></i></a>
																	<?php } else { ?>
																		<a href="ndf_nli_refus.php?id=<?= $ndf_actuelle['ndf_id'] ?>&nli=<?= $n['nli_id'] ?>&refus=0" data-toggle="tooltip" title="Annuler le refus" class="btn btn-info btn-sm"><i class="fa fa-check-square"></i></a>
																	<?php } ?>
																<?php } ?>
																<a href="ndf_ligne.php?id=<?= $ndf_actuelle['ndf_id'] ?>&utilisateur=<?= $ndf_actuelle['ndf_utilisateur'] ?>&ligne=<?= $n['nli_id'] ?>" data-toggle="tooltip" title="Modifier la ligne" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
																<button type="button" onclick="deleteLigne(<?= $n['nli_id'] ?>, <?= $ndf_actuelle['ndf_id'] ?>)" data-toggle="tooltip" title="Supprimer la ligne" class="btn btn-danger btn-sm mt-10"><i class="fa fa-times"></i></button>
															<?php } ?>
														<?php } ?>
													<?php } ?>
												</td>
											</tr>
									<?php
										}
									} ?>


								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 mt10">
							<div class="panel">
								<div class="panel-heading panel-head-sm">
									<span class="panel-icon">
										<i class="fa fa-check"></i>
									</span>
									<span class="panel-title"> Suivi de la note de frais</span>
								</div>
								<div class="panel-body p10">
									<?php if (!empty($ndf_actuelle['ndf_valide'])) { // si la validation a été apportée par l'utilisateur
									?>
										<div class="col-md-2 col-xs-2 pn">
											<div class="panel panel-tile text-center br-a br-light">
												<div class="panel-body bg-success pn">


													<i class="fa fa-check fa-check text-muted fs20 br64 bg-success dark p15 ph20 mt5"></i>

													<h2 class="fs20" style="font-size:14px!important">DEMANDEUR</h2>
													<h4 class="text-white" style="font-size:12px"><?= $utilisateur['uti_prenom'] ?> <?= $utilisateur['uti_nom'] ?></h4>
													<h6 class="text-white">VALIDÉE LE : <?= convert_date_txt($ndf_actuelle['ndf_valide']) ?></h6>
												</div>
											</div>
										</div>
									<?php } else { // si pas validée par l'utilisateur
									?>
										<div class="col-md-2 col-xs-2 pn">
											<div class="panel panel-tile text-center br-a br-light">
												<div class="panel-body bg-danger pn">


													<i class="fa fa-check fa-times text-muted fs20 br64 bg-danger dark p15 ph20 mt5"></i>

													<h2 class="fs20" style="font-size:14px!important">DEMANDEUR</h2>
													<h4 class="text-white" style="font-size:12px"><?= $utilisateur['uti_prenom'] ?> <?= $utilisateur['uti_nom'] ?></h4>
													<h6 class="text-white">En cours de saisie</h6>
													<div class="text-center mt15 mb15">
														<?php if (!empty($ndf_lignes) && !empty($ndf_actuelle) && ($_SESSION['acces']['acc_ref_id'] == $ndf_actuelle['ndf_utilisateur'] or $_SESSION['acces']['acc_ref_id'] == $utilisateur['uti_responsable'])) { ?>
															<a href="ndf_valide_date.php?id=<?= $ndf_actuelle['ndf_id'] ?>&demandeur=<?= $_SESSION['acces']['acc_ref_id'] ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Valider la NDF"><i class="fa fa-check"></i>
															</a>
														<?php } ?>

													</div>
												</div>
											</div>
										</div>
									<?php } ?>

									<?php if (!empty($ndf_actuelle) && $ndf_actuelle['ndf_statut'] > 0) {
										// si le statut est > 0 : En cours de validation ++
									?>
										<?php
										if (!empty($ndf_actuelle['ndf_ra'])) {
											if (!empty($ndf_actuelle['ndf_valide']) && !empty($ndf_actuelle['ndf_ra_valide'])) { // si la validation a été apportée par le demandeur
										?>
												<div class="col-md-2 col-xs-2 pn">
													<div class="panel panel-tile text-center br-a br-light">

														<div class="panel-body bg-success pn">


															<i class="fa fa-check fa-check text-muted fs20 br64 bg-success dark p15 ph20 mt5"></i>

															<h2 class="fs20" style="font-size:14px!important">Resp. Agence</h2>
															<h4 class="text-white" style="font-size:12px" style="font-size:12px"><?= $ra['uti_prenom'] ?> <?= $ra['uti_nom'] ?></h4>
															<h6 class="text-white">VALIDÉE LE : <?= convert_date_txt($ndf_actuelle['ndf_ra_valide']) ?></h6>
														</div>
													</div>
												</div>
											<?php } elseif (!empty($ndf_actuelle['ndf_valide']) && empty($ndf_actuelle['ndf_ra_valide'])) { ?>
												<div class="col-md-2 col-xs-2 pn">
													<div class="panel panel-tile text-center br-a br-light">
														<div class="panel-body bg-danger pn">


															<i class="fa fa-check fa-times text-muted fs20 br64 bg-danger dark p15 ph20 mt5"></i>

															<h2 class="fs20" style="font-size:14px!important">Resp. Agence</h2>
															<h4 class="text-white" style="font-size:12px" style="font-size:12px"><?= $ra['uti_prenom'] ?> <?= $ra['uti_nom'] ?></h4>
															<h6 class="text-white">En cours de validation</h6>
															<?php if ($ra['uti_id'] == $_SESSION['acces']['acc_ref_id']) { // si c'est bien le DR de connecté
															?>
																<div class="text-center mt15 mb15">

																	<a href="ndf_valide_date.php?id=<?= $ndf_actuelle['ndf_id'] ?>&ra=<?= $ra['uti_id'] ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Valider la note de frais"><i class="fa fa-check"></i></a>


																</div>
															<?php } ?>
														</div>
													</div>
												</div>
										<?php }
										} ?>
										<?php if (!empty($ndf_actuelle['ndf_valide']) && !empty($ndf_actuelle['ndf_re_valide'])) { // si la validation a été apportée par le demandeur

										?>
											<div class="col-md-2 col-xs-2 pn">
												<div class="panel panel-tile text-center br-a br-light">

													<div class="panel-body bg-success pn">


														<i class="fa fa-check fa-check text-muted fs20 br64 bg-success dark p15 ph20 mt5"></i>

														<h2 class="fs20" style="font-size:14px!important">Resp. Exploitation</h2>
														<h4 class="text-white" style="font-size:12px" style="font-size:12px"><?= $re['uti_prenom'] ?> <?= $re['uti_nom'] ?></h4>
														<h6 class="text-white">VALIDÉE LE : <?= convert_date_txt($ndf_actuelle['ndf_re_valide']) ?></h6>
													</div>
												</div>
											</div>
										<?php } elseif (!empty($ndf_actuelle['ndf_valide']) && empty($ndf_actuelle['ndf_re_valide'])) { ?>
											<div class="col-md-2 col-xs-2 pn">
												<div class="panel panel-tile text-center br-a br-light">
													<div class="panel-body bg-danger pn">


														<i class="fa fa-check fa-times text-muted fs20 br64 bg-danger dark p15 ph20 mt5"></i>

														<h2 class="fs20" style="font-size:14px!important">Resp. Exploitation</h2>
														<h4 class="text-white" style="font-size:12px" style="font-size:12px"><?= $re['uti_prenom'] ?> <?= $re['uti_nom'] ?></h4>
														<h6 class="text-white">En cours de validation</h6>
														<?php if ($re['uti_id'] == $_SESSION['acces']['acc_ref_id']) { // si c'est bien le DR de connecté
														?>
															<?php if (!empty($ndf_actuelle['ndf_ra']) && !empty($ndf_actuelle['ndf_ra_valide']) or empty($ndf_actuelle['ndf_ra'])) { ?>
																<div class="text-center mt15 mb15">

																	<a href="ndf_valide_date.php?id=<?= $ndf_actuelle['ndf_id'] ?>&re=<?= $re['uti_id'] ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Valider la note de frais"><i class="fa fa-check"></i></a>


																</div>
														<?php }
														} ?>
													</div>
												</div>
											</div>
										<?php } ?>

										<?php if (!empty($ndf_actuelle['ndf_valide']) && !empty($ndf_actuelle['ndf_compta_valide'])) { // si la validation a été apportée par le demandeur

										?>
											<div class="col-md-2 col-xs-2  pn">
												<div class="panel panel-tile text-center br-a br-light">
													<div class="panel-body bg-success pn">


														<i class="fa fa-check fa-check text-muted fs20 br64 bg-success dark p15 ph20 mt5"></i>

														<h2 class="fs20" style="font-size:14px!important">Compta</h2>
														<h4 class="text-white" style="font-size:12px"><?= $compta['uti_prenom'] ?> <?= $compta['uti_nom'] ?></h4>
														<h6 class="text-white">VALIDÉE LE : <?= convert_date_txt($ndf_actuelle['ndf_compta_valide']) ?></h6>
													</div>
												</div>
											</div>
										<?php } elseif (!empty($ndf_actuelle['ndf_valide']) && empty($ndf_actuelle['ndf_compta_valide'])) { ?>
											<div class="col-md-2 col-xs-2 pn">
												<div class="panel panel-tile text-center br-a br-light">
													<div class="panel-body bg-danger pn">


														<i class="fa fa-check fa-times text-muted fs20 br64 bg-danger dark p15 ph20 mt5"></i>

														<h2 class="fs20" style="font-size:14px!important">Compta</h2>
														<h4 class="text-white" style="font-size:12px"></h4>
														<h6 class="text-white">En cours de validation</h6>
														<?php if ($_SESSION['acces']['acc_profil'] == 8 or $_SESSION['acces']['acc_profil'] == 11) { // si DAF ou COmpta
														?>
															<?php if (!empty($ndf_actuelle['ndf_re']) && !empty($ndf_actuelle['ndf_re_valide'])) { ?>
																<div class="text-center mt15 mb15">

																	<a href="ndf_valide_date.php?id=<?= $ndf_actuelle['ndf_id'] ?>&compta=<?= $_SESSION['acces']['acc_ref_id'] ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Valider la NDF"><i class="fa fa-check"></i></a>


																</div>
															<?php }	?>
														<?php } ?>

													</div>
												</div>
											</div>
										<?php } ?>
										<?php if (!empty($ndf_actuelle['ndf_valide']) && !empty($ndf_actuelle['ndf_rh_valide'])) { // si la validation a été apportée par le demandeur

										?>
											<div class="col-md-2 col-xs-2 pn">
												<div class="panel panel-tile text-center br-a br-light">
													<div class="panel-body bg-success pn">


														<i class="fa fa-check fa-check text-muted fs20 br64 bg-success dark p15 ph20 mt5"></i>

														<h2 class="fs20" style="font-size:14px!important">RH</h2>
														<h4 class="text-white" style="font-size:12px"><?= $rh['uti_prenom'] ?> <?= $rh['uti_nom'] ?></h4>
														<h6 class="text-white">VALIDÉE LE : <?= convert_date_txt($ndf_actuelle['ndf_rh_valide']) ?></h6>

													</div>
												</div>
											</div>
										<?php } elseif (!empty($ndf_actuelle['ndf_valide']) && empty($ndf_actuelle['ndf_rh_valide'])) { ?>
											<div class="col-md-2 col-xs-2 pn">
												<div class="panel panel-tile text-center br-a br-light">
													<div class="panel-body bg-danger pn">


														<i class="fa fa-check fa-times text-muted fs20 br64 bg-danger dark p15 ph20 mt5"></i>

														<h2 class="fs20" style="font-size:14px!important">RH</h2>
														<h4 class="text-white" style="font-size:12px"></h4>
														<h6 class="text-white">En cours de validation</h6>
														<?php if ($_SESSION['acces']['acc_profil'] == 7 or $_SESSION['acces']['acc_profil'] == 12) { // si RH ou RRH
														?>
															<?php if (!empty($ndf_actuelle['ndf_compta']) && !empty($ndf_actuelle['ndf_compta_valide'])) { ?>
																<div class="text-center mt15 mb15">

																	<a href="ndf_valide_date.php?id=<?= $ndf_actuelle['ndf_id'] ?>&rh=<?= $_SESSION['acces']['acc_ref_id'] ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Valider la NDF"><i class="fa fa-check"></i></a>


																</div>
															<?php } ?>
														<?php } ?>

													</div>
												</div>
											</div>
										<?php } ?>

									<?php } ?>

									<?php if (!empty($ndf_actuelle) && $ndf_actuelle['ndf_statut'] > 1) {
										// si le statut est > 1 : Validée par dr,rh,compta ++
									?>
										<?php if (!empty($ndf_actuelle['ndf_paiement'])) { ?>
											<div class="col-md-2 col-xs-2 pn">
												<div class="panel panel-tile text-center br-a br-light">
													<div class="panel-body bg-success pn">

														<i class="fa fa-check fa-check text-muted fs20 br64 bg-success dark p15 ph20 mt5"></i>

														<h2 class="fs20">PAIEMENT</h2>
														<h6 class="text-white">LE : <?= convert_date_txt($ndf_actuelle['ndf_paiement']) ?></h6>
													</div>
												</div>
											</div>
										<?php } ?>
									<?php } ?>

								</div>

							</div>
						</div>
						<div class="col-md-3 mt10">
							<div class="panel">
								<div class="panel-heading panel-head-sm">
									<span class="panel-icon">
										<i class="fa fa-check"></i>
									</span>
									<span class="panel-title"> Détail de l'avance pour cette note</span>
								</div>
								<div class="panel-body pn">

									<?php if (empty($avances)) { ?>
										<div class="alert alert-warning" style="margin:0;border-radius:0;">
											Il n'y a pas d'avance enregistrée pour cette note
										</div>
									<?php } else { ?>
										<table class="table mbn tc-icon-2 tc-med-2 tc-bold-last">
											<thead>
												<tr class="hidden">
													<th></th>

													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody id="tva_table">

												<tr>
													<td>
														<strong>Date</strong>
													</td>
													<td>
														<?= convert_date_txt($avances['nav_date']); ?>
													</td>

												</tr>
												<tr>
													<td><strong>TTC</strong></td>
													<td>

														<?= number_format($avances['nav_ttc'], 2, ',', ' ')  ?> €

													</td>
												</tr>


												<tr>
													<td>
														<strong>Commentaire</strong>
													</td>
													<td class="text-center">
														<?= $avances['nav_commentaire']  ?>
													</td>

												</tr>
											</tbody>
										</table>
									<?php } ?>


								</div>


							</div>
						</div>
						<div class="col-md-3">

							<div class="panel-body pn mt10">

								<table class="table mbn tc-icon-2 tc-med-2 tc-bold-last">

									<tbody id="tva_table">
										<tr>
											<td>
												<strong>Total HT</strong>
											</td>
											<td class="total_ht_txt">
												<?php if (!empty($ndf_actuelle)) { ?>

													<?= number_format($total_ht, 2, ',', ' ')  ?> €
												<?php } else { ?>
													0,00 €
												<?php } ?>
											</td>

										</tr>
										<tr>
											<td>
												<strong>Total TVA</strong>
											</td>
											<td class="total_tva_txt">
												<?php if (!empty($total_tva)) { ?>

													<?= number_format($total_tva, 2, ',', ' ')  ?> €
												<?php } else { ?>
													0,00 €
												<?php } ?>
											</td>

										</tr>
										<tr>
											<td>
												<strong>Total TTC</strong>
											</td>
											<td class="total_ttc_txt">
												<?php if (!empty($ndf_ttc)) { ?>

													<?= number_format($ndf_ttc['nli_ttc_format'], 2, ',', ' ')  ?> €
												<?php } else { ?>
													0,00 €
												<?php } ?>
											</td>

										</tr>
										<tr class="tva_table">
											<td><strong>Total avance</strong></td>
											<td>
												<?php if (!empty($ndf_avances['nav_ttc'])) { ?>
													<?= number_format($ndf_avances['nav_ttc'], 2, ',', ' ')  ?> €
												<?php } else { ?>
													0,00 €
												<?php } ?>
											</td>
										</tr>


										<tr>
											<td>
												<strong>Solde</strong>
											</td>
											<td class="table_ttc">
												<?php if (!empty($ndf_solde)) { ?>
													<?= number_format($ndf_solde, 2, ',', ' ')  ?> €</td>
										<?php } ?>

										</tr>
									</tbody>
								</table>
							</div>

						</div>
					</div>

				</section>
			</section>

		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
			<div class="row">
				<div class="col-xs-4 footer-left pt7">
					<?php if(!empty($_SESSION['retour_ndf'])) { ?>
						<a href="<?= $_SESSION['retour_ndf'] ?>" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a>
					<?php } ?>
					<?php if ((!empty($ndf_actuelle) && $_SESSION['acces']['acc_ref_id'] == $ndf_actuelle['ndf_utilisateur']) || empty($ndf_actuelle)) { ?>
						<?php if (!empty($ndf_precedente)) { ?>
							<a href="ndf.php?id=<?= $ndf_precedente['ndf_id'] ?>" class="btn btn-success btn-sm"><i class="fa fa-long-arrow-left"></i> Note précédente</a>
						<?php } ?>
					<?php } ?>
				</div>
				<div class="col-xs-4 footer-middle pt7"></div>
				<div class="col-xs-4 footer-right pt7">
					<?php if (!empty($ndf_actuelle) && in_array($_SESSION['acces']['acc_profil'], [8, 11, 13, 9])) { ?>
						<?php if ($ndf_actuelle['ndf_statut'] == 5) { ?>
							<a href="ndf_valide_date.php?id=<?= $ndf_actuelle['ndf_id'] ?>&paiement&retour_ndf" class="btn btn-sm btn-success" data-toggle="tooltip" title="Basculer en cours de paiement"><i class="fa fa-check-square" aria-hidden="true"></i></a>
						<?php } elseif ($ndf_actuelle['ndf_statut'] == 6) { ?>
							<!-- <a href="ndf_valide_date.php?id=<?= $ndf_actuelle['ndf_id'] ?>&payee&retour_ndf" class="btn btn-sm btn-success" data-toggle="tooltip" title="Payée"><i class="fa fa-check" aria-hidden="true"></i></a> -->
						<?php } ?>
					<?php } ?>
					<?php if (!empty($ndf_actuelle) && $ndf_actuelle['ndf_statut'] > 4) { // si le statut est différent de en cours de saisie
					?>
						<!-- <a href="#" class="btn btn-success btn-sm printMe"><i class="fa fa-print"></i> Imprimer</a> -->
					<?php } ?>

					<?php if (!empty($ndf_actuelle) && $ndf_actuelle['ndf_statut'] == 0) { // si le statut est différent de en cours de saisie
					?>

						<a href="ndf_ligne.php?id=<?= $ndf_actuelle['ndf_id'] ?>&utilisateur=<?= $ndf_actuelle['ndf_utilisateur'] ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Dépense</a>
						<a href="#" data-toggle="modal" data-target="#modal_suppr" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Annuler NDF</a>
					<?php } elseif (empty($ndf_actuelle)) { ?>
						<a href="ndf_ligne.php?utilisateur=<?= $_SESSION['acces']['acc_ref_id'] ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Dépense</a>

					<?php } ?>
					<?php if (!empty($ndf_actuelle) && $_SESSION['acces']['acc_ref_id'] == $ndf_actuelle['ndf_utilisateur']){ ?>
						<?php if ((!empty($ndf_prochaine) && !empty($ndf_actuelle) && $ndf_prochaine['ndf_id'] != $ndf_actuelle['ndf_id']) OR (!empty($ndf_actuelle) && $ndf_actuelle['ndf_quinzaine_fin'] < date('Y-m-d'))) { ?>
							<?php if(!empty($ndf_prochaine)){ ?>
							<a href="ndf.php?id=<?= $ndf_prochaine['ndf_id'] ?>" class="btn btn-success btn-sm">Note suivante <i class="fa fa-long-arrow-right"></i></a>
							<?php }else if(!empty($_GET['id'])){ ?>
								<a href="ndf.php" class="btn btn-success btn-sm">Note suivante <i class="fa fa-long-arrow-right"></i></a>
							<?php } ?>
						<?php } ?>
					<?php } ?>

				</div>
			</div>
		</footer>
	</form>
	<?php if (!empty($ndf_actuelle)) { ?>
		<!-- MODAL ANNULE NDF -->
		<div id="modal_suppr" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Annulation NDF</h4>
					</div>

					<div class="modal-body">
						Êtes-vous sûr de vouloir annuler cette note de frais ?

					</div>

					<div class="modal-footer">
						<a href="ndf_annule.php?id=<?= $ndf_actuelle['ndf_id'] ?>&utilisateur=<?= $ndf_actuelle['ndf_utilisateur'] ?>" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</a>
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
					</div>
				</div>

			</div>
		</div>
		<!-- MODAL ANNULE NDF -->
	<?php } ?>
	<!-- MODAL ANNULE NDF -->
	<div id="modal_suppr_ligne" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Supprimer la ligne</h4>
				</div>

				<div class="modal-body">
					Êtes-vous sûr de vouloir supprimer cette ligne ?

				</div>

				<div class="modal-footer">
					<a href="#" id="suppr_ligne" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
				</div>
			</div>

		</div>
	</div>
	<!-- MODAL ANNULE NDF -->
	<?php
	include "includes/footer_script.inc.php"; ?>

	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
	<!-- plugin pour les masques formulaires -->
	<script src="assets/js/custom.js"></script>

	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script src="vendor/plugins/summernote/summernote.min.js"></script>
	<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

	<script src="vendor/plugins/jquery.numberformat.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->


	<script>
		// DOCUMENT READY //
		jQuery(document).ready(function() {
			initDatatable()
			// initilisation plugin datatables

			// quand on veut imprimer
			$('.printMe').click(function() {
				printDiv();

			});

			$(".nli_depassement_accorde").change(function() {
				var val = 0;
				if ($(this).val() != "" && $(this).val() > $(this).data('max')) {
					val = $(this).data('max');
					$(this).val(val)
				} else if ($(this).val() != "" && $(this).val() <= $(this).data('max')) {
					val = $(this).val();
				}
				actualise_accorde($(this).data("id"), val);
			});

			////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
		});

		///////////// FONCTIONS //////////////
		function actualise_accorde(nli_id, val) {

			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_ndf_depassement_accorde.php',
				data: {
					"nli_id": nli_id,
					"val": val,
				},
				dataType: 'html',
				success: function(data) {
					$(".nli_pris_en_charge_" + nli_id).html(Number(data).toLocaleString("fr-FR", {
						minimumFractionDigits: 2
					}))
				}
			});

		}

		function actualise_montant_tva(nli_id, val) {

			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_ndf_montant_tva.php',
				data: {
					"nli_id": nli_id,
					"val": val,
				},
				dataType: 'html',
				success: function(data) {
					$(".nli_ht_" + nli_id).html(Number(data).toLocaleString("fr-FR", {
						minimumFractionDigits: 2
					}))
				}
			});

		}

		function deleteLigne(id, ndf) {
			$("#suppr_ligne").attr('href', 'ndf_suppr_ligne.php?id=' + id + '&ndf=' + ndf)
			$('#modal_suppr_ligne').modal('show')
		}

		function initDatatable() {
			$('#table_id').DataTable({
				"language": {
					"url": "vendor/plugins/DataTables/media/js/French.json"
				},
				"paging": false,
				"searching": false,
				"info": false,
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});
		}

		function removeDatatable() {
			$('#table_id').DataTable().destroy();
		}

		function printDiv() {
			window.print();
		}
		var beforePrint = function() {
			$("#content-footer").hide();
			$("#content_wrapper").addClass('backgroundWhite')
			$("#content").addClass('backgroundWhite')
			$("#main").addClass('backgroundWhite')
			$("body").addClass('backgroundWhite')
			$(".actions_th").hide()
			removeDatatable()
		};
		var afterPrint = function() {
			$("#content-footer").show();
			$("#content_wrapper").removeClass('backgroundWhite')
			$("#content").removeClass('backgroundWhite')
			$("#main").removeClass('backgroundWhite')
			$("body").removeClass('backgroundWhite')
			$(".actions_th").show()
			initDatatable()
		};

		window.onbeforeprint = beforePrint;
		window.onafterprint = afterPrint;
		//////////// FIN FONCTIONS //////////
	</script>
	<style>
		.backgroundWhite,
		.backgroundWhite:before {
			background: white !important;
		}
	</style>
</body>

</html>