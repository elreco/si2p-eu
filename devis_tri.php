 <?php

$menu_actif = "1-6";
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'includes/connexion_fct.php';
include 'modeles/mod_get_commerciaux.php';
unset($_SESSION["critere_sql"]);
unset($_SESSION["critere_affichage"]);

$_SESSION["retourDevis"]="devis_tri.php";
 
// sur la personne connecte

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];    
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}

$param_dev_fiabilite=array(
	"0" => "Sans suite",
	"30" => "fiable à 30%",
	"50" => "fiable à 50%",
	"90" => "fiable à 90%",
	"100" => "Commandé"
);

// CATEGORIE DE CLIENTS
$sql="SELECT cca_id,cca_libelle,cca_gc FROM Clients_Categories";
$sql.=" ORDER BY cca_libelle";
$req = $Conn->prepare($sql);
$req->execute();
$client_categories = $req->fetchAll();

// LES COMMERCIAUX
$commerciaux=get_commerciaux($acc_societe,$acc_agence,"");

// CATEGORIE DE PRODUITS
$sql="SELECT pca_id,pca_libelle FROM Produits_Categories ORDER BY pca_libelle";
$req = $Conn->prepare($sql);
$req->execute();
$produit_categories = $req->fetchAll();

// FAMILLES DE PRODUITS
$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles";
$sql.=" ORDER BY pfa_libelle";
$req = $Conn->prepare($sql);
$req->execute();
$familles = $req->fetchAll();

// LES AGENCES
if($acc_agence==0){
	$sql="SELECT age_id,age_nom FROM Agences,Societes WHERE soc_id=age_societe AND age_societe=" . $acc_societe . " AND soc_agence ORDER BY age_nom;";
	$req = $Conn->query($sql);
	$agences = $req->fetchAll();
}

// PERIODE PAR DEFAUT
$date = new DateTime();
$dateDeb = $date -> format('01/m/Y');
$dateFin = $date -> format('t/m/Y');													


?>
<!DOCTYPE html>
<html>
<!-- Mirrored from admindesigns.com/demos/absolute/1.1/layout_horizontal-sm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 09:52:52 GMT -->
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Recherche devis</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
	
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

   <!-- Favicon -->
   <link rel="shortcut icon" href="assets/img/favicon.png">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <form method="post" action="devis_liste.php">
		<div>
			<input type="hidden" name="table" id="table" value="sus" />
		</div>
		<!-- Start: Main -->
		<div id="main">
<?php 		include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">
				

				<section id="content" class="animated">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary ">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
									
										<div class="content-header mtn mbn">
									<?php	echo("<h2>Recherche de <b class='text-primary' >devis</b></h2>"); ?>
										</div>	
													
										<div class="col-md-10 col-md-offset-1">
										   
											<div class="row">
												<div class="col-md-6">
													<label for="chrono" >Devis :</label>
													<div class="smart-widget sm-left sml-80">
														<label class="field">
															<input type="text" name="chrono" id="chrono" class="gui-input" placeholder="Numéro">
														</label>
														<label for="sm3" class="button">DVXX-</label>
													</div>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-6">
													<label for="client_categorie" >Catégorie :</label>
													<select name="client_categorie" name="client_categorie" class="select2 select2-categorie" >
														<option value="0" >Catégorie</option>
												<?php	if(!empty($client_categories)){
															foreach($client_categories as $c){
																echo("<option value='" . $c["cca_id"] . "' >" . $c["cca_libelle"] . "</option>");
															}
														} ?>
													</select>
												</div>
												<div class="col-md-6">
													<label for="client_sous_categorie" >Catégorie :</label>
													<select name="client_sous_categorie" id="client_sous_categorie" class="select2 select2-categorie-sous-categorie" >
														<option value="0" >Sous-catégorie</option>
													</select>
												</div>
											</div>
											
											<div class="row mt15">
												<div class="col-md-12">
													<label for="client" >Client :</label>
													<select name="client" id="client" class="select2-client-n" >
														<option value="0" >Client</option>
													</select>
												</div>
											</div>
											<div class="row mt15">
									<?php		if(isset($agences)){
													if(!empty($agences)){ ?>												
														<div class="col-md-6">
															<label for="commercial" >Agence :</label>
															<select name="commercial" id="commercial" class="select2"  >
																<option value="0" >Sélectionnez une agence</option>
														<?php	foreach($agences as $a){
																	echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																}?>
															</select>
														</div>	
									<?php			}
												} 
												if(!empty($commerciaux)){?>
													<div class="col-md-6">
														<label for="commercial" >Commercial :</label>
														<select name="commercial" id="commercial" class="select2"  >
															<option value="0" >Sélectionnez un commercial</option>
													<?php	if(!empty($commerciaux)){
																foreach($commerciaux as $c){
																	echo("<option value='" . $c["com_id"] . "' >" . $c["com_label_1"] . " " . $c["com_label_2"] . "</option>");
																}
															} ?>
														</select>
													</div>
									<?php		} ?>
											</div>
											<div class="row mt15">
												<div class="col-md-6">
													<label for="date_deb" >Devis entre le :</label>
													<span class="field prepend-icon">
														<input type="text" class="gui-input datepicker" id="date_deb" name="date_deb" placeholder="Selectionner une date" value="<?=$dateDeb?>" >
														<label class="field-icon">
															<i class="fa fa-calendar-o"></i>
														</label>
													</span>
												</div>
												<div class="col-md-6">
													<label for="date_fin" >et le :</label>
													<span class="field prepend-icon">
														<input type="text" class="gui-input datepicker" id="date_fin" name="date_fin" placeholder="Selectionner une date" value="<?=$dateFin?>" >
														<label class="field-icon">
															<i class="fa fa-calendar-o"></i>
														</label>
													</span>
												</div>											
											</div>
											<div class="row mt15">
												<div class="col-md-4">
													<label for="esperance" >Fiabilité :</label>
													<select name="esperance" class="select2" >
														<option value="-1" >Fiabilité du devis</option>
												<?php	foreach($param_dev_fiabilite as $c => $v){
															echo("<option value='" . $c . "' >" . $v . "</option>");
														} ?>
													</select>
												</div>
												<div class="col-md-4 mt20 pt10" >
													<label class="option">
													  <input type="checkbox" name="sans_suite" value="1" >
													  <span class="checkbox"></span>Afficher les "sans suite"
													</label>
												</div>
												<div class="col-md-4 mt20 pt10" >
													<label class="option">
													  <input type="checkbox" name="commande" value="1" checked >
													  <span class="checkbox"></span>Afficher les "commandé"
													</label>
												</div>																						
											</div>
											<div class="section-divider" >
												<span>Produits</span>
											</div>
											<div class="row mt15">											
												<div class="col-md-4">
													<label for="pro_categorie" >Catégorie :</label>
													<select name="pro_categorie" id="pro_categorie" class="select2 select2-produit-cat" >
														<option value="0" >Catégorie ...</option>
												<?php	if(!empty($produit_categories)){
															foreach($produit_categories as $c){
																echo("<option value='" . $c["pca_id"] . "' >" . $c["pca_libelle"] . "</option>");
															}
														} ?>
													</select>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-4">
													<label for="pro_famille" >Famille :</label>
													<select name="pro_famille" class="select2 select2-famille" >
														<option value="0" >Famille ...</option>
											<?php		if(!empty($familles)){
															foreach($familles as $f){
																echo("<option value='" . $f["pfa_id"] . "' >" . $f["pfa_libelle"] . "</option>");
															}
														} ?>
													</select>
												</div>
												<div class="col-md-4">
													<label for="pro_sous_famille" >Sous-famille :</label>
													<select name="pro_sous_famille" id="pro_sous_famille" class="select2 select2-famille-sous-famille" >
														<option value="0" >Sous-famille ...</option>
													</select>
												</div>
												<div class="col-md-4">
													<label for="pro_sous_sous_famille" >Sous-Sous-Famille :</label>
													<select name="pro_sous_sous_famille" id="pro_sous_sous_famille" class="select2 select2-famille-sous-sous-famille" >
														<option value="0" >Sous-sous-famille</option>
													</select>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-6">
													<label for="pro_type" >Type :</label>
													<select name="pro_type" id="pro_type" class="select2" >
														<option value="0" >Type ...</option>
														<option value="1" >Intra</option>
														<option value="2" >Inter</option>
													</select>
												</div>
											</div>	
											<div class="row mt15">
												<div class="col-md-10">
													<label for="produit" >Produit :</label>
													<select name="produit" id="produit" class="select2-produit" data-obselete="2" data-archive="2" >
														<option value="0" >Produit ...</option>
													</select>
												</div>
											</div>	
											<div class="section-divider" >
												<span>Affichage</span>
											</div>
											<div class="row mt15" >
												<div class="col-md-2" ></div>
												<div class="col-md-10" >
													<label class="option">
													  <input type="checkbox" name="affiche_ligne" value="1" />
													  <span class="checkbox"></span>Afficher les lignes de devis
													</label>
												</div>		
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</section>
				<!-- End: Content -->
			</section>
			<!-- End: Content WRAPPER -->
		</div>
		<!-- End: Main -->
	
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">

				</div>
				<div class="col-xs-6 footer-middle"></div>
				<div class="col-xs-3 footer-right">
					<a href="devis.php" class="btn btn-success btn-sm" role="button">
						<span class="fa fa-plus"></span>
						<span class="hidden-xs">Nouveau devis</span>
					</a>

					<button type="submit" name="search" class="btn btn-primary btn-sm">
						<i class='fa fa-search'></i> Rechercher
					</button>

				</div>
			</div>
		</footer>
	</form>
<?php
	include "includes/footer_script.inc.php"; ?>	

	<script src="/vendor/plugins/select2/js/select2.min.js"></script>
	
	<script type="text/javascript" src="assets/admin-tools/admin-forms/js/jquery-ui-datepicker.min.js"></script>

	<script type="text/javascript" src="assets/js/custom.js"></script>
	
</body>
</html>
