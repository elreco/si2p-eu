<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');


$req = $Conn->prepare("SELECT * FROM ndf_lignes WHERE nli_id =" . $_GET['nli']);
$req->execute();
$nli = $req->fetch();

$req = $Conn->prepare("UPDATE ndf_lignes SET nli_refus = :nli_refus WHERE nli_id =" . $_GET['nli']);
$req->bindValue(':nli_refus', $_GET['refus']);
$req->execute();

$req = $Conn->prepare("SELECT SUM(nli_ttc) FROM ndf_lignes WHERE nli_refus = 0 AND nli_ndf = " . $_GET['id']);
$req->execute();
$ndf_ttc = $req->fetch();

$req = $Conn->prepare("UPDATE ndf SET ndf_ttc = :ndf_ttc WHERE ndf_id = :ndf_id");
$req->bindValue(':ndf_ttc', $ndf_ttc['SUM(nli_ttc)']);
$req->bindValue(':ndf_id', $_GET['id']);
$req->execute();

$_SESSION['message'][] = array(
    "titre" => "Note de frais",
    "type" => "success",
    "message" => "Le frais a été actualisé."
);

Header("Location: ndf.php?id=" . $_GET['id']);
die();