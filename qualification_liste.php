<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");;
include_once "modeles/mod_qualification.php";
$qualifications = get_qualifications();
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper">

				<h1>Liste des qualifications</h1>

				<section id="content" class="animated fadeIn">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="dark" >
									<th>ID</th>
									<th>Nom de la qualification</th>
									<th>Code</th>
									<th>Durée (en mois)</th>
									<th colspan="3" >Options</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($qualifications as $q): ?>
								<tr>
									<td><?= $q['qua_id'] ?></td>
									<td><?= $q['qua_libelle'] ?></td>
									<td><?= $q['qua_code'] ?></td>
									<td><?= $q['qua_duree'] ?></td>
									<td><?= $q['qua_opt_1'] ?></td>
									<td><?= $q['qua_opt_2'] ?></td>
									<td><?= $q['qua_opt_3'] ?></td>
									<td class="text-center" >
										<a href="qualification.php?id=<?= $q['qua_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier cette qualificiation">
											<span class="fa fa-pencil"></span>
										</a>
									</td>
								</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-long-arrow-left"></span>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="qualification.php" class="btn btn-success btn-sm" role="button" >
						<span class="fa fa-plus"></span>
						Nouvelle qualification
					</a>
				</div>
			</div>
		</footer>

		<?php
		include "includes/footer_script.inc.php"; ?>	
	</body>
</html>
