<?php

	// STAT VENTILATION CA CLIENT PAR FAMILLE


	include "includes/controle_acces.inc.php";
	include "modeles/mod_parametre.php";
	include "includes/connexion.php";
	include "includes/connexion_soc.php";


	// DONNEE FORM
	if($_SESSION['acces']['acc_service'][1]!=1 AND $_SESSION['acces']['acc_service'][3]!=1){
		echo("Accès refusé!");
		die();
	}
	$erreur_txt="";
	if(!empty($_POST)){

		$commercial=0;
		if(!empty($_POST["commercial"])){
			$commercial=intval($_POST["commercial"]);
		}

		$periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode)){
				$periode_deb=$DT_periode->format("Y-m-d");
			}
		}

		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$DT_periode=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode)){
				$periode_fin=$DT_periode->format("Y-m-d");
			}
		}

		$com_src=0;
		if(!empty($_POST["com_src"])){
			$com_src=intval($_POST["com_src"]);
		}
		
		$affichage=1;
		if(!empty($_POST["affichage"])){
			$affichage=intval($_POST["affichage"]);
		}

		if(empty($periode_deb) OR empty($periode_fin) OR empty($com_src)){
			$erreur_txt="Formulaire incomplet!";
		}
	}

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : stat_commercial.php");
		die();
	}

	// LE PERSONNE CONNECTE
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	// LE COMMERCIAL
	if(!empty($_POST)){
		if($commercial>0){
			$sql="SELECT com_label_1,com_label_2,com_agence FROM Commerciaux WHERE com_id=" . $commercial . ";";
			$req=$ConnSoc->query($sql);
			$d_commercial=$req->fetch();
			if(empty($d_commercial)){

				$_SESSION['message'][] = array(
					"titre" => "Erreur",
					"type" => "danger",
					"message" => "Formulaire incomplet!"
				);
				header("location : stat_commercial.php");
				die();

			}else{
				$param_stat["titre"]=$d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"];
				$param_stat["agence"]=$d_commercial["com_agence"];
			}
		}elseif($acc_agence>0){

			$sql="SELECT age_nom FROM Agences WHERE age_id=" . $acc_agence . ";";
			$req=$Conn->query($sql);
			$d_agence=$req->fetch();
			if(empty($d_agence)){

				$_SESSION['message'][] = array(
					"titre" => "Erreur",
					"type" => "danger",
					"message" => "Formulaire incomplet!"
				);
				header("location : stat_commercial.php");
				die();
			}else{
				$param_stat["titre"]=$d_agence["age_nom"];
				$param_stat["agence"]=$acc_agence;
			}

		}else{

			$sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
			$req=$Conn->query($sql);
			$d_societe=$req->fetch();
			if(empty($d_societe)){

				$_SESSION['message'][] = array(
					"titre" => "Erreur",
					"type" => "danger",
					"message" => "Formulaire incomplet!"
				);
				header("location : stat_commercial.php");
				die();
			}else{
				$param_stat["titre"]=$d_societe["soc_nom"];
				$param_stat["agence"]=0;
			}
		}
	}



    //echo($sql);
	$_SESSION['factures_stats_ok'] = [
		'sql' => '',
		'com_src' => '',
		'param_stat' => '',
		'periode_deb' => '',
		'periode_fin' => ''
	];
	if(!empty($_POST)){
		// CONSTRUCTION DU TABLEAU
	    // GET CA
	    $sql="SELECT SUM(fac_total_ht) AS montant_ht,fac_client FROM Factures ";
	    $mil="";
	    if($com_src==1){
	        // commercial du client
	        // on ne doit compabiliser que les factures émise par l'agence
	        $sql.=" INNER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)";
	        if($commercial>0){
	            $mil.=" AND cli_commercial=" . $commercial;
	        }elseif($acc_agence>0){
	            $mil.=" AND cli_agence=" . $acc_agence;
	        }
	    }else{
	        // commercial de la facture
	        // jointure avec agence pour éviter que le CA soit comptabilisé autant de fois qu'il y a d'agence
			$sql.=" LEFT OUTER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)";
	        if($commercial>0){
	            $mil.=" AND fac_commercial=" . $commercial;
	        }elseif($acc_agence>0){
	            $mil.=" AND fac_agence=" . $acc_agence;
	        }
	    }
		$sql.=" LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";
	    $mil.=" AND fac_date>='". $periode_deb . "' AND fac_date<='" . $periode_fin . "'";

	    if($mil!=""){
	        $sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	    }
		$sql.=" GROUP BY fac_client";
		$_SESSION['factures_stats_ok_sql'] = $sql;
		$req=$ConnSoc->query($sql);
		$_SESSION['factures_stats_ok_com_src'] = $com_src;
		$_SESSION['factures_stats_ok_param_stat'] = $param_stat;
		$_SESSION['factures_stats_ok_periode_deb'] = $periode_deb;
		$_SESSION['factures_stats_ok_periode_fin'] = $periode_fin;
		$_SESSION['factures_stats_ok_affichage'] = $affichage;
		//////////////////////
		// SQL POUR PAGE SUIVANTE//
		$sql_fac="SELECT fac_date,fac_client,fac_opca, cli_code, cli_affacturage,fac_cli_nom, fac_id,fac_aff_remise, fac_total_ttc, fac_total_ht, com_label_1,com_label_2,fac_chrono,fac_regle,fac_numero FROM Factures ";
	    $mil="";
	    if($com_src==1){
	        // commercial du client
	        // on ne doit compabiliser que les factures émise par l'agence
	        $sql_fac.=" INNER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)";
	        if($commercial>0){
	            $mil.=" AND cli_commercial=" . $commercial;
	        }elseif($acc_agence>0){
	            $mil.=" AND cli_agence=" . $acc_agence;
	        }
	    }else{
	        // commercial de la facture
	        // jointure avec agence pour éviter que le CA soit comptabilisé autant de fois qu'il y a d'agence
			$sql_fac.=" LEFT OUTER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)";
	        if($commercial>0){
	            $mil.=" AND fac_commercial=" . $commercial;
	        }elseif($acc_agence>0){
	            $mil.=" AND fac_agence=" . $acc_agence;
	        }
	    }
		$sql_fac.=" LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";
	    $mil.=" AND fac_date>='". $periode_deb . "' AND fac_date<='" . $periode_fin . "'";

	    if($mil!=""){
	        $sql_fac.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	    }
		//$sql_fac.=" GROUP BY fac_client";
	    //echo($sql);
		$_SESSION['factures_stats'] = $sql_fac;
		//////////////////////


	}else{
			$req=$ConnSoc->query($_SESSION['factures_stats_ok_sql']);
			$com_src = $_SESSION['factures_stats_ok_com_src'];
			$param_stat  = $_SESSION['factures_stats_ok_param_stat'];
			$periode_deb  = $_SESSION['factures_stats_ok_periode_deb'];
			$periode_fin  = $_SESSION['factures_stats_ok_periode_fin'];
			$affichage=$_SESSION['factures_stats_ok_affichage'];


	}
    $d_stats=$req->fetchAll();


	$donnees=array(
		"autre" => array(
			"ape_code" => "NC",
			"ape_libelle" => "Clients sans code APE",
			"ca" => 0
		),
		"total" => array(
			"ape_code" => "",
			"ape_libelle" => "Total",
			"ca" => 0
		),
	);
	
	// SI AFFICHAGE PAR DIVISION
	
	if($affichage==2){
		
		$sql="SELECT adi_code, adi_libelle FROM ape_divisions ORDER BY adi_code";
		$req=$Conn->query($sql);
		$d_div_ape=$req->fetchAll();
		if(!empty($d_div_ape)){
			foreach($d_div_ape as $da){
				$donnees[$da["adi_code"]]=array(
					"ape_code" => $da["adi_code"],
					"ape_libelle" => $da["adi_libelle"], 
					"ca" => 0
				);
				
			}
	  	}
	}
	
	foreach($d_stats as $stat){
		$sql="SELECT ape_code, ape_libelle, ape_id FROM Clients LEFT JOIN ape ON (Clients.cli_ape = ape.ape_id) WHERE cli_id = :fac_client ORDER BY ape_code";
		$req=$Conn->prepare($sql);
		$req->bindValue("fac_client", $stat['fac_client']);
		$req->execute();
		$d_ape=$req->fetch();
		if(!empty($d_ape)){
			
			$ca = $stat['montant_ht'];
			$cle = "autre";
			
			if(!empty($d_ape["ape_id"])){
				if($affichage==2){
					$cle = substr($d_ape["ape_code"],0,2);
					if(empty($donnees[$cle])){
						$cle="autre";
						
					}
					
				}else{
					$cle = intval($d_ape["ape_id"]);
					if(empty($donnees[$cle])){
						$donnees[$cle]=array(
							"ape_code" => $d_ape["ape_code"],
							"ape_libelle" => $d_ape["ape_libelle"], 
							"ca" => 0
						);
				   }
				}
			}
			
			
		   
		   $donnees[$cle]["ca"] =  $donnees[$cle]["ca"] + $ca;
		   $donnees["total"]["ca"] =  $donnees["total"]["ca"] + $ca;

	  	}
	}

	asort($donnees);

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css">

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#zone_print{
				display:none;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 landscape;
				margin:5mm;
			}
			html{
				background-color:#fff!important
			}
			body{
				background-color:#fff!important
			}
			#zone_print{
				background-color:#fff!important;
				font-size:8pt;
			}
		</style>
	</head>

	<body class="sb-top sb-top-sm no-scroll" >

		<div id="zone_print" ></div>

		<div id="main" >
<?php
			if($_SESSION["acces"]["acc_ref"]==3){
				include "includes/header_sta.inc.php";
			}elseif($_SESSION["acces"]["acc_ref"]==2){
				include "includes/header_cli.inc.php";
			}else{
				include "includes/header_def.inc.php";
			} ?>
			<section id="content_wrapper" >
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn" style="overflow:scroll;" >

					<div id="page_print" >
						<h1 class="text-center" >
                      <?php	echo("CA par");
							if($affichage==1){
                                echo(" code APE");
                            }else{
								 echo(" division APE");
                            }
							if($com_src==1){
                                 echo(" de " . $param_stat["titre"]);
                            }else{
                                echo(" facturé par " . $param_stat["titre"]);
                            }
                            echo("<br/>Du " . convert_date_txt($periode_deb) . " au " . convert_date_txt($periode_fin));
                            ?>
						</h1>

							<div class="table-responsive">
								<table class="table table-striped table-hover" >
									<thead>
										<tr class="dark">
								<?php		if($affichage==1){ ?>
												<th>Code APE</th>
												<th>Libellé APE</th>
								<?php		}else{ ?>
												<th>Code division</th>
												<th>Libellé division</th>
								<?php		} ?>
											<th>CA</th>
										</tr>
									</thead>
							<?php 	if(!empty($donnees)){ ?>
										<tbody>
                                <?php		foreach($donnees as $apeid=>$l){
												if($l['ape_libelle'] != "Clients sans code APE" && $l['ape_libelle'] != "Total" && !empty($l['ca']) ){ ?>
                                                	<tr>
														<td><?=$l['ape_code']?></td>
														<td><?=$l['ape_libelle']?></td>
														<td class="text-right">
													<?php	if($affichage==1){ ?>
																<a href="stat_code_ape_fac.php?ape=<?= $apeid ?>">
																	<?=number_format($l['ca'], 2, ',', ' ');?>
																</a>
													<?php	}else{ ?>
																<a href="stat_code_ape_fac.php?div=<?= $apeid ?>">
																	<?=number_format($l['ca'], 2, ',', ' ');?>
																</a>
													<?php	} ?>
														</td>
													</tr>
										<?php 	}
											} ?>
											<tr class="warning">
												<td><?=$donnees['autre']['ape_code']?></td>
												<td><?=$donnees['autre']['ape_libelle']?></td>
												<td class="text-right">
													<a href="stat_code_ape_fac.php?ape=0">
														<?=number_format($donnees['autre']['ca'], 2, ',', ' ');?>
													</a>
												</td>
											</tr>
											<tr>
												<td><?=$donnees['total']['ape_code']?></td>
												<td><?=$donnees['total']['ape_libelle']?></td>
												<td class="text-right">
													<a href="stat_code_ape_fac.php">
														<?=number_format($donnees['total']['ca'], 2, ',', ' ');?>
													</a>
												</td>
											</tr>

										</tbody>
                                    <?php }?>
								</table>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="stat_commercial.php" class="btn btn-sm btn-default"  >
							<i class="fa fa-left-arrow" ></i>Retour
						<a>
						<!--<button type="button" class="btn btn-sm btn-info ml15 btn-print" >
							<i class="fa fa-print"></i> Imprimer
						</button>-->
					</div>
					<div class="col-xs-6 footer-middle text-center" style=""></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>

<?php	include "includes/footer_script.inc.php"; ?>

		<script src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script src="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function () {

			});
			(jQuery);
		</script>
	</body>
</html>
