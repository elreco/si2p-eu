<?php 
/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces_cli.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');
include('modeles/mod_parametre.php');
///////////////////// Contrôles des parametres ////////////////////
// session retour

// fin session retour
$param_fournisseur = 0;

if(!empty($_GET['id'])){
    // si les get sont pas remplis
    $param_fournisseur = intval($_GET['id']);
}
if($param_fournisseur == 0){
 
    echo("Impossible d'afficher la page");
    die();
}

$acl_id = 0;
if(!empty($_GET['acl_id'])){
    // si les get sont pas remplis
    $acl_id = intval($_GET['acl_id']);
}
if($acl_id == 0){
 
    echo("Impossible d'afficher la page");
    die();
}
///////////////////// Fin Contrôles des parametres ////////////////////
/////////////// TRAITEMENTS BDD ///////////////////////
$ConnFct=connexion_fct($_GET['soc']);

$req = $ConnFct->prepare("SELECT * FROM actions WHERE act_id = " . $_GET['id']);
$req->execute();
$action = $req->fetch();

$req = $ConnFct->prepare("SELECT * FROM actions_sessions WHERE ase_action = " . $action['act_id']);
$req->execute();
$session = $req->fetch();

$req = $ConnFct->prepare("SELECT * FROM actions_sessions 
LEFT JOIN actions_clients ON (actions_clients.acl_action = actions_sessions.ase_action) 
LEFT JOIN plannings_dates ON (actions_sessions.ase_date = plannings_dates.pda_id) 
WHERE acl_id =" . $acl_id . " AND ase_action = " . $action['act_id'] . " AND acl_client IN (" . $_SESSION['acces']['acc_groupe_liste'] . ") ORDER BY pda_date DESC");
$req->execute();
$sessions = $req->fetchAll();

$req = $ConnFct->prepare("SELECT * FROM actions_clients WHERE acl_action = " . $action['act_id']);
$req->execute();
$action_client = $req->fetch();

$req = $Conn->prepare("SELECT pro_code_produit FROM produits WHERE pro_id = " . $action['act_produit']);
$req->execute();
$produit = $req->fetch();

$req = $ConnFct->prepare("SELECT pda_date FROM plannings_dates WHERE pda_id = " . $session['ase_date']);
$req->execute();
$pda_date = $req->fetch();

// LES HORRAIRES

$sql="SELECT * FROM  actions_stagiaires
LEFT JOIN actions_clients ON (actions_clients.acl_id =  actions_stagiaires.ast_action_client) 
WHERE acl_action = " . $_GET['id'] . " AND acl_client IN (" . $_SESSION['acces']['acc_groupe_liste'] . ")";
$req = $ConnFct->prepare($sql);
$req->execute();
$sessions_stagiaires_build = $req->fetchAll();

/* echo $_SESSION['acces']['acc_groupe_liste'];
echo("<pre>");
var_dump($sessions_stagiaires_build);
echo("</pre>");
die(); */
$sessions_stagiaires = array();
foreach($sessions_stagiaires_build as $k=>$ssb){
	$sessions_stagiaires[$ssb['ast_stagiaire']] = $sessions_stagiaires_build[$k];
}


?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Inscrire un stagiaire</title> 
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">

	<form method="post" action="stagiaire_client_enr.php" enctype="multipart/form-data">
		<div id="main">
<?php 		include "includes/header_cli.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
			
					<div class="row"> 		
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary ">
								
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
									
										<div class="content-header">
											<h2>
												Formation <b class="text-primary"><?= $produit['pro_code_produit'] ?></b> 
												du <b class="text-primary"><?= convert_date_txt($pda_date['pda_date']) ?></b>
									<?php		if(!empty($action['act_adr_ville'])){ ?>
													à <b class="text-primary"><?=$action['act_adr_ville']?></b>
									<?php		} ?>
											</h2>       
										</div>
										<div class="row" >
											<div class="col-md-6" >
												Attestation groupée :
									<?php		if(file_exists("documents/Societes/".$_GET['soc'] ."/Attestations/attestation_form_".$action['act_id']."_".$acl_id .".pdf")){
													$download_nom="Attestation groupée " . $produit["pro_code_produit"]; ?>
													<a href="documents/Societes/<?= $_GET['soc'] ?>/Attestations/attestation_form_<?= $action['act_id'] ?>_<?= $acl_id  ?>.pdf" download="<?=$download_nom?>" data-toggle="tooltip" title="Télécharger l'attestation groupée" class="btn btn-success btn-sm">
														<i class="fa fa-file-pdf-o"></i>
													</a>
									<?php		}else{
													echo("<span class='text-danger' >Non disponible</span>");
												} ?>
											</div>
										</div>
									
											
										<div class="row" >	
                                            <div class="col-md-6" >
												<h3>Liste des sessions</h3>
												 <table class="table">
													<thead>
														<tr class="dark">
                                                            <th class="text-center">Date</th>
															<th class="text-center">Heure de début</th>
															<th class="text-center">Heure de fin</th>
														</tr>
													</thead>
													<tbody>
												<?php	if(!empty($sessions)){
															foreach($sessions as $d){ ?>
																<tr>
                                                                    <td class="text-center" ><?=convert_date_txt($d["pda_date"])?></td>
																	<td class="text-center" ><?=$d["ase_h_deb"]?></td>
																	<td class="text-center" ><?=$d["ase_h_fin"]?></td>
																</tr>
												<?php		}								
														} ?>
													</tbody>
												</table>
											</div>		
											<div class="col-md-6" >
												
												<h3>Liste des stagiaires</h3>

												<table class="table">
													<thead>
														<tr class="dark">
															<th>Nom et prénom</th>
															<th class="text-center">Attestation</th>
														</tr>
													</thead>
													<tbody>
											<?php 		foreach($sessions_stagiaires as $s){ 
															$req = $Conn->prepare("SELECT * FROM stagiaires WHERE sta_id = " . $s['ast_stagiaire']);
                                                            $req->execute();
                                                            $stagiaire = $req->fetch();
															?>
															<tr class="tr_stagiaire" data-id="<?= $stagiaire['sta_id'] ?>">
																<td><?= $stagiaire['sta_nom'] ?> <?=$stagiaire['sta_prenom'] ?></td>
																<td class="text-center" >
                                                                <?php		if(file_exists("documents/Societes/" . $_GET['soc'] . "/Attestations/attestation_". $action['act_id'] . "_" . $stagiaire["sta_id"] . ".pdf")){ ?>
																					<a href="documents/Societes/<?=$_GET['soc']?>/Attestations/attestation_<?=$action['act_id']?>_<?=$stagiaire["sta_id"]?>.pdf" download="Attestation <?=$stagiaire["sta_nom"] ?>.pdf" >
																						<i class='fa fa-file-pdf-o' ></i>
																					</a>
																	<?php }else{
																		echo("<span class='text-danger' >Non disponible</span>");
                                                                    }?>
																</td>
																
															</tr>
											<?php 		} ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>

		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
		<?php		if(!empty($_SESSION["retour"])){
						$url_retour=$_SESSION["retour"];
					}else{
						$url_retour="session_client_liste.php";	
					} ?>
					<a href="<?=$url_retour?>" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center">
					<!--<a href="session_attestation.php?formation=<?= $param_fournisseur?>" data-toggle="tooltip" class="btn btn-primary btn-sm" title="Générer toutes les attestations" >
						<i class='fa fa-refresh'></i> Générer les attestations
					</a>-->
				</div>
				<div class="col-xs-3 footer-right">
					
					
				</div>
			</div>
		</footer>
	</form>


<!-- MODAL SUPPRESSION PRODUIT -->
<div id="modalsuppr" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Supprimer un stagiaire</h4>
            </div>

            <div class="modal-body">
                Êtes-vous sûr de vouloir supprimer ce stagiaire de la session ?

            </div>

            <div class="modal-footer">
                <button type="button" id="delete_stagiaire" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</a>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
            </div>
        </div>

    </div>
</div>
<!-- FIN MODAL SUPPRESSION PRODUIT -->

<?php include "includes/footer_script.inc.php"; ?>  

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<script src="assets/js/orion.js"></script>
<!-- SCRIPT SELECT2 -->
<script src="vendor/plugins/select2/js/select2.min.js"></script>
	
	<script type="text/javascript">
	
		jQuery(document).ready(function (){
			
			// ouverture du modal de confirmation
			$(".btn-suppr").click(function(){
				sta_id = $(this).data("id");
				$("#delete_stagiaire").data("id", sta_id);
			});
			
			$(".btn-suppr").click(function(){
				 sta_id = $(this).data("id");
				del_stagiaire_session(sta_id); 
			}); 
		}); 
 
	   ////////////// FONCTIONS ///////////////

		function del_stagiaire_session(sta_id){
			$.ajax({
				type:'POST',
				url: 'ajax/ajax_del_stagiaire_session.php',                  
				data : 'sta_id=' + sta_id + "&session=<?= $session['ase_id'] ?>",          
				dataType: 'html',              
				success: function(data)       
				{
					console.log(data);
					$(".tr_stagiaire").each(function(key, value) {
						if($(this).data('id') == sta_id){
							$(this).remove();
						}
					});

					$("#modalsuppr").modal("hide");

				}
			});
		}
	</script>

</body>
</html>