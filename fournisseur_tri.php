<?php 
/////////////////// MENU ACTIF /////////////////////// 
$menu_actif = "5-1";
/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php'); 
include("modeles/mod_fournisseur.php");
include("modeles/mod_contact.php");
include("modeles/mod_competence.php");
// session retour
$_SESSION['retour'] = "fournisseur_tri.php";
// fin session retour

/////////////// TRAITEMENTS BDD ///////////////////////

// rechercher les familles de produit
$req = $Conn->prepare("SELECT pfa_id, pfa_libelle FROM produits_familles");
$req->execute();
$produits_familles = $req->fetchAll();

?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Fournisseurs</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
<form method="post" action="fournisseur_liste.php" enctype="multipart/form-data">
<!-- Start: Main -->
<div id="main">
<?php include "includes/header_def.inc.php"; ?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper" >
    <!--DEBUT CONTENT -->
    <section id="content" class="animated fadeIn">
        <!--DEBUT ROW -->
        <div class="row">
        <!--DEBUT COL-MD-10 -->
            <div class="col-md-10 col-md-offset-1">
              <!--DEBUT ADMIN FORM -->
                <div class="admin-form theme-primary ">
                    <!--DEBUT PANEL HEADING -->
                    <div class="panel heading-border panel-primary">
                      <!--DEBUT PANEL BODY -->
                        <div class="panel-body bg-light text-center">
                            <div class="content-header">
                                <h2>Recherche de <b class="text-primary">fournisseurs</b></h2>            
                            </div>
                            <div class="col-md-10 col-md-offset-1">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section-divider mb40">
                                            <span>Informations générales</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="section">
                                        <div class="field prepend-icon">
                                            <input type="text" name="fou_code" id="fou_code" class="gui-input nom" placeholder="Code">
                                            <label for="fou_code" class="field-icon">
                                                <i class="fa fa-barcode"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="section">
                                        <div class="field prepend-icon">
                                            <input type="text" name="fou_nom" id="fou_nom" class="gui-input" placeholder="Nom">
                                            <label for="fou_nom" class="field-icon">
                                                <i class="fa fa-bookmark" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="section">
                                        <div class="field prepend-icon">
                                            <input type="text" name="fad_cp" id="fad_cp" class="gui-input code-postal" placeholder="Code postal">
                                            <label for="fad_cp" class="field-icon">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="section">
                                        <div class="field prepend-icon">
                                            <input type="text" name="fad_ville" id="fad_ville" class="gui-input nom" placeholder="Ville">
                                            <label for="fad_ville" class="field-icon">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="section">
                                        <span class="select">
                                            <select id="fou_famille" name="fou_famille">
                                                <option value="0">Famille...</option>
                                                <?= get_fournisseur_famille_select(0); ?>

                                            </select>
                                            <i class="arrow"></i>
                                        </span>
                                    </div>
                                </div>

                                <!-- Actualisation de ces deux listes en fonction de la famille choisie -->
                                <div id="select-famille-si2p" style="display:none;">  

                                    <div class="col-md-4">
                                        <div class="section">

                                            <select id="pro_famille" name="pro_famille" class="select2 select2-famille">
                                                <option value="0">Famille Si2P...</option>
                                                <?php foreach($produits_familles as $pfa): ?>
                                                    <option value="<?= $pfa['pfa_id'] ?>"><?= $pfa['pfa_libelle']; ?></option>
                                                <?php endforeach; ?>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="section">

                                            <select id="pro_sous_famille" name="pro_sous_famille" class="select2 select2-famille-sous-famille">
                                                <option value="0">Sous-famille Si2P...</option>
                                            </select>

                                        </div>
                                    </div>
                                </div> 
                                <!-- Actualisation de ces deux listes en fonction de la famille choisie -->

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section-divider mb40">
                                            <span>Contact</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="section">
                                        <div class="field prepend-icon">
                                            <input type="text" name="fco_nom" id="fco_nom" class="gui-input nom" placeholder="Nom">
                                            <label for="fco_nom" class="field-icon">
                                                <i class="fa fa-user"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="section">
                                        <div class="field prepend-icon">
                                            <input type="text" name="fco_prenom" id="fco_prenom" class="gui-input prenom" placeholder="Prénom">
                                            <label for="fco_prenom" class="field-icon">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="section">
                                        <div class="field select">
                                            <select name="fco_mail" id="con_mail">
                                                <option value="0">Mail...</option>
                                                <option value="1">Renseigné</option>
                                                <option value="2">Non renseigné</option>
                                            </select>
                                        <i class="arrow simple"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="section">
                                        <div class="field select">
                                            <select name="fco_fonction" id="con_fonction" onchange="nomchange(this.value)">
                                                <option value="0">Sélectionner une fonction...</option>

                                                <?= get_contact_fonction_select(0); ?>
                                                <option value="autre">Autre...</option>
                                            </select>
                                            <i class="arrow simple"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 con_fonction_nom_style" style="display:none;">
                                    <div class="field prepend-icon">
                                        <input type="text" name="fco_fonction_nom" id="fco_fonction_nom" class="gui-input" placeholder="Nouvelle fonction">
                                        <label for="fco_fonction_nom" class="field-icon">
                                            <i class="fa fa-tag"></i>
                                        </label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section-divider mb40">
                                            <span>Compta</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="section">
                                        <div class="field prepend-icon">
                                            <input type="text" name="fou_siren" id="fou_siren" class="gui-input siren" placeholder="Siren">
                                            <label for="fou_siren" class="field-icon">
                                                <i class="fa fa-barcode"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="section">
                                        <div class="field prepend-icon">
                                            <input type="text" name="fou_siret" id="fou_siret" class="gui-input siret" placeholder="Siret">
                                            <label for="fou_siret" class="field-icon">
                                                <i class="fa fa-barcode" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section-divider mb40">
                                            <span>Intervenant extérieur</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="section">
                                        <div class="field prepend-icon">
                                            <input type="text" name="fin_nom" id="fin_nom" class="gui-input nom" placeholder="Nom">
                                            <label for="fco_nom" class="field-icon">
                                                <i class="fa fa-user"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="section">
                                        <div class="field prepend-icon">
                                            <input type="text" name="fin_prenom" id="fin_prenom" class="gui-input prenom" placeholder="Prénom">
                                            <label for="fco_prenom" class="field-icon">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="section">
                                        <div class="field select">
                                            <select name="fic_competence" id="fic_competence" >
                                                <option value="0">Sélectionner une compétence...</option>

                                                <?=get_competences_select(0);?>

                                            </select>
                                            <i class="arrow simple"></i>
                                        </div>
                                    </div>
                                </div>
                                <!-- ETAT ADMINISTRATIF -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section-divider mb40">
                                            <span>Etat administratif du fournisseur</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">      
                                        <div class="checkbox-custom checkbox-success mb5">
                                            <input type="checkbox" name="fou_admin_etat[]" id="checkboxAdmin1" value="3">
                                            <label for="checkboxAdmin1">Valide</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">      
                                        <div class="checkbox-custom checkbox-warning mb5">
                                            <input type="checkbox" name="fou_admin_etat[]" id="checkboxAdmin2" value="2">
                                            <label for="checkboxAdmin2">Certains documents manquants</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">      
                                        <div class="checkbox-custom checkbox-danger mb5">
                                            <input type="checkbox" name="fou_admin_etat[]" id="checkboxAdmin3" value="1">
                                            <label for="checkboxAdmin3">Non valide</label>
                                        </div>
                                    </div>
                                </div>    
                                <!-- FIN ETAT ADMINISTRATIF -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section-divider mb40">
                                            <span>Autres infos</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="option option-dark">
                                            <input type="checkbox" name="fou_blacklist" id="fou_blacklist" value="oui">
                                            <span class="checkbox"></span>
                                            <label for="fou_blacklist">Blacklisté</label>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="option option-dark">
                                            <input type="checkbox" name="fou_archive" id="fou_archive" value="oui">
                                            <span class="checkbox"></span>
                                            <label for="fou_archive">Archivé</label>
                                        </label>
                                    </div>
                                </div>
                                <!-- AUTRES INFOS -->
                                <div class="col-md-4">
                                    <div class="section">
                                        <label class="option option-dark">
                                            <input type="checkbox" name="fou_famille_non" id="fou_famille_non" value="oui">
                                            <span class="checkbox"></span>
                                            <label for="fou_famille_non">Famille non renseignée</label>
                                        </label>
                                    </div>
                                </div>
                            <!-- FIN AUTRES INFOS -->
                            </div>
                        <!-- FIN COL-MD-10 -->
                        </div>
                    <!-- FIN PANEL BODY -->
                    </div>
                <!-- FIN PANEL HEADING -->
                </div>
            <!-- FIN ADMIN FORM -->
            </div>
        </div>

        </section>

    </section>

</div>
    <!-- End: Main -->
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">
            </div>
            <div class="col-xs-6 footer-middle">

            </div>
            <div class="col-xs-3 footer-right">
                <a href="fournisseur.php" class="btn btn-success btn-sm">
                    <i class='fa fa-plus'></i> Nouveau fournisseur
                </a>
                <button type="submit" name="search" class="btn btn-primary btn-sm">
                    <i class='fa fa-search'></i> Rechercher
                </button>
            </div>
        </div>
    </footer>
</form>
<?php include "includes/footer_script.inc.php"; ?>  

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<!-- SCRIPT SELECT2 -->
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
    ////////////// FONCTIONS ///////////////
    // affichage de l'input autre fonction si nécessaire
    function nomchange(selected){

        if(selected == "autre"){
            $(".con_fonction_nom_style").show(400);
        }else{
            $(".con_fonction_nom_style").hide(400);
            $("#con_fonction_nom").removeAttr("value");
        }

    }

    ////////////// FIN FONCTIONS ///////////////


    <!-- -->


    //////// ÉVENEMENTS UTILISATEURS //////////
    jQuery(document).ready(function () {
        $("#fou_famille").change(function(){

            if($(this).val() == 1 || $(this).val() == 7){

              $("#select-famille-si2p").show();

          }else{

              $("#select-famille-si2p").hide();

          }

        });
    });

    //////// FIN ÉVENEMENTS UTILISATEURS //////
</script>

</body>
</html>