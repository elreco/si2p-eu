<?php

	// LISTE LA PLANNIFICATION ET LES ANNULATIONS ENTRE J et J +30
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
    include "includes/connexion_soc.php";
    include "includes/connexion_fct.php";

	// CONTROLE D'ACCES
	// cf item stat_formation.php stat_2

	if($_SESSION['acces']['acc_service'][1]!=1 AND $_SESSION['acces']['acc_service'][2]!=1 AND $_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']['acc_profil'] != 15 AND $_SESSION['acces']['acc_profil'] != 14){ 
		echo("Accès refusé!");
		die();
    }
    
    $erreur_txt="";
    if(!empty($_POST)){

        $periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}
		
		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
        }

        $source=0;
        if(!empty($_POST["source"])){
            $source=intval($_POST["source"]);
        }
        
		if(empty($periode_deb) OR empty($periode_fin)){
			$erreur_txt="Formulaire incomplet!";
        }
        

    }else{
        $erreur_txt="Formulaire incomplet!";
    }



    if (!empty($erreur_txt)) {

        $_SESSION['message'][] = array(
            "aff" => "",
            "titre" => "Erreur!",
            "type" => "danger",
            "message" => $erreur_txt
        );
        header("location : stat_formation.php");
        die();
    

    }

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
    }

    $acc_utilisateur=0;
    if(!empty($_SESSION['acces']["acc_ref"])){
        if($_SESSION['acces']["acc_ref"]==1){
            $acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
        }
    }


   

    // CALCUL DE LA PERIODE

    $jour_n=date_create_from_format('Y-m-d',$periode_deb);;
   
    $dates=array();
    $dates[$periode_deb]=0;

    while ( $jour_n<$DT_periode_fin) {
        $jour_n->add(new DateInterval('P1D'));
        if ($jour_n->format("N")!=7) {
            $dates[$jour_n->format("Y-m-d")]=0;
        }
    }
    

    // LES ENTITES CONCERNE

    $data=array();

    $sql="SELECT soc_id,soc_nom,age_id,age_nom,soc_agence,soc_intra_inter,age_intra_inter FROM Utilisateurs_Societes 
    INNER JOIN Societes ON (Utilisateurs_Societes.uso_societe=Societes.soc_id)
    LEFT JOIN Agences ON (Utilisateurs_Societes.uso_agence=Agences.age_id)
    WHERE uso_utilisateur=" . $acc_utilisateur . " AND NOT soc_archive AND NOT soc_id IN (10,14)
    AND ( 
            (
                soc_agence=0 AND uso_agence=0";             
            $sql.=" ) OR (
                soc_agence=1 AND uso_agence>0";              
            $sql.=")
    )";
    if (empty($source)) {
        $sql.=" AND soc_id=" . $acc_societe;
        if (!empty($acc_agence)) {
            $sql.=" AND age_id=" . $acc_agence;
        }
    }
    $sql.=" ORDER BY soc_nom,age_nom;";
    $req=$Conn->query($sql);
    $d_societes=$req->fetchAll();
    if (!empty($d_societes)) {
       foreach($d_societes as $soc){

            if (!empty($soc["age_id"])) {
                $cle=$soc["soc_id"] . "-" . $soc["age_id"];
                $titre=$soc["soc_nom"] . "-" . $soc["age_nom"];
            }else {
                $cle=$soc["soc_id"] . "-0";
                $titre=$soc["soc_nom"];
            }

            if (empty($source)) {
                $data[$cle]=array(
                    "nom" => $titre,
                    "plannifiees" => $dates,
                    "total_plannifiees" => 0,
                    "annulees" => $dates,
                    "total_annulees" => 0,
                    "reportees" => $dates,
                    "total_reportees" => 0
                );
            }else {
                $data[$cle]=array(
                    "nom" => $titre,
                    "plannifiees" => 0,
                    "annulees" => 0,
                    "reportees" => 0
                );
            }

            $connFct=connexion_fct($soc["soc_id"]);

            $sql="SELECT COUNT(pda_id) as nb_demi,int_option,act_agence";
            if (empty($source)){
                $sql.=",pda_date";
            }
            $sql.=" FROM Plannings_Dates,Intervenants,Actions
            WHERE pda_intervenant=int_id AND pda_ref_1=act_id AND pda_type=1";
            $sql.=" AND pda_date>='" . $periode_deb . "' AND pda_date<='" . $periode_fin . "' AND NOT pda_archive AND NOT act_archive";
            if (!empty($soc["age_id"])) {
                $sql.=" AND act_agence=" . $soc["age_id"];
            }
            $sql.=" GROUP BY int_option,act_agence";
            if (empty($source)){
                $sql.=",pda_date";
            }
            $req=$connFct->query($sql);
            $d_sources=$req->fetchAll();
            if (!empty($d_sources)) {
                /*echo("<pre>");
                    print_r($d_sources);
                echo("</pre>");*/
                foreach($d_sources as $s) {

                    if ($s["int_option"]==1) {
                        $type="annulees";
                    } elseif ($s["int_option"]==2) {
                        $type="reportees";
                    } else {
                        $type="plannifiees";
                    }

                    if (empty($source)) {                   
                        $data[$cle][$type][$s["pda_date"]]=$data[$cle][$type][$s["pda_date"]] + $s["nb_demi"];
                        $data[$cle]["total_" . $type]=$data[$cle]["total_" . $type] + $s["nb_demi"];
                    }else{
                       
                        $data[$cle][$type]=$s["nb_demi"];
                    }

                }
            }
       }
       /*if($soc_agence){
           // creation d'un tableau anomalie pour les actions sans agence
           $data[0]=array(
                "anomalie" => false,
                "nom" => "Agence non affecté",
                "plannifiees" => $dates,
                "annulees" => $dates,
                "reportees" => $dates
            );
       }*/

    }

    /*echo("<pre>");
        print_r($data);
    echo("</pre>");
    die();*/

    // SOURCE

    /*$sql="SELECT COUNT(pda_id) as nb_demi,pda_date,int_option,act_agence FROM Plannings_Dates,Intervenants,Actions
    WHERE pda_intervenant=int_id AND pda_ref_1=act_id AND pda_type=1";
    $sql.=" AND pda_date>='" . $jour_j->format("Y-m-d") . "' AND pda_date<='" . $jour_n->format("Y-m-d") . "' AND NOT pda_archive";
    if (!empty($acc_agence)) {
        $sql.=" AND act_agence=" . $acc_agence;
    }
    $sql.=" GROUP BY pda_date,int_option,act_agence;";
    $req=$ConnSoc->query($sql);
    $d_sources=$req->fetchAll();
    if (!empty($d_sources)) {
        foreach($d_sources as $s) {

            if($soc_agence){
                $entite=0;
                if (!empty($s["act_agence"])) {
                    $entite=$s["act_agence"];              
                }else{

                    $data[0]["anomalie"]=true;
                }

            }else{
                $entite=$acc_societe;
            }

            if ($s["int_option"]==1) {
                $type="annulees";
            } elseif ($s["int_option"]==2) {
                $type="reportees";
            } else {
                $type="plannifiees";
            }
            
            $data[$entite][$type][$s["pda_date"]]=$data[$entite][$type][$s["pda_date"]] + $s["nb_demi"];
        }
    }*/
    

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<div id="main" >
<?php
			if($_SESSION["acces"]["acc_ref"]==1){
				include "includes/header_def.inc.php";
			} ?>
			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" style="overflow:scroll;" >

                    <h1 class="text-center">Planifications entre <?=$DT_periode_deb->format("d/m/Y")?> et le <?=$DT_periode_fin->format("d/m/Y")?></h1>

       <?php       if (!empty($data)) { 
           
                        if(empty($source)){ 
                            
                                // Données journalières pour l'agence / société connecté
                            
                            ?>
                            <table class="table table-striped table-hover" >
                                <thead>
                                    <tr class="dark">
                                        <th rowspan="2" >Date</th>
                                <?php   foreach ($data as $k => $d) { 
                                            if($k!=0 OR ($d["anomalie"]) ) { ?>     
                                                <th colspan="3" class="text-center" ><?=$d["nom"]?></th>   
                                <?php       }
                                        } ?>
                                    </tr>
                                    <tr>
                                <?php  foreach ($data as $k => $d) { 
                                            if($k!=0 OR ($d["anomalie"]) ) { ?>                                          
                                                <th class="text-center" >Plannifiés (1/2 jours)</th>
                                                <th class="text-center" >Annulés (1/2 jours)</th>
                                                <th class="text-center" >Reportés (1/2 jours)</th>                                      
                                <?php       }
                                        } ?>   
                                    </tr>                      
                                </thead>
                                <tbody>
                    <?php           $total_plannifiees=0; 
                                    $total_annulees=0;
                                    $total_reportees=0;
                                    foreach ($dates as $date => $v) { 
                                        $d_aff = new DateTime($date); ?>
                                        <tr>
                                            <td><?=$d_aff->format("d/m/Y")?></td>
                                    <?php  foreach ($data as $k => $d) { 
                                                if($k!=0 OR ($d["anomalie"]) ) { 
                                                    
                                                    $total_plannifiees=$total_plannifiees + $d["plannifiees"][$date]; 
                                                    $total_annulees=$total_annulees + $d["annulees"][$date];
                                                    $total_reportees=$total_reportees + $d["reportees"][$date]; 
                                                    
                                                    ?>
                                                    <td class="text-center" ><?=$d["plannifiees"][$date]?></td>
                                                    <td class="text-center" ><?=$d["annulees"][$date]?></td>
                                                    <td class="text-center" ><?=$d["reportees"][$date]?></td>
                                    <?php      }
                                            } ?>     
                                        </tr>
                    <?php           } ?>
                                    <tr>
                                        <td>Total : </td>
                    <?php               foreach ($data as $k => $d) { 
                                            if($k!=0 OR ($d["anomalie"]) ) {  ?>
                                                <td class="text-center" ><?=$d["total_plannifiees"]?></td>
                                                <td class="text-center" ><?=$d["total_annulees"]?></td>
                                                <td class="text-center" ><?=$d["total_reportees"]?></td>
                        <?php               }
                                        } ?>
                                    </tr>
                                </tbody>
                            </table>
                    
                            <!--<table class="table table-striped table-hover" >
                                <thead>
                                    <tr class="dark">
                                        <th>Entité</th>
                                        <th>Actions</th>
                            <?php        foreach ($dates as $d => $v) {
                                            $d_aff = new DateTime($d);
                                            echo("<th>" .  $d_aff->format("d/m/Y") . "</th>");
                                            }; ?>
                                    </tr>                           
                                </thead>
                                <tbody>
                    <?php       if (!empty($data)) {
                                    foreach ($data as $k => $d) { 
                                        if($k!=0 OR ($d["anomalie"]) ) { ?>
                                            <tr>
                                                <td rowspan="3" ><?=$d["nom"]?></td>
                                                <td>Plannifiés (1/2 jours)</td>    
                                    <?php         foreach ($d["plannifiees"] as $v) {
                                                    echo("<td>" . $v . "</td>");
                                                } ?>
                                            </tr>
                                            <tr>
                                                <td>Annulés (1/2 jours)</td>   
                                    <?php         foreach ($d["annulees"] as $v) {
                                                    echo("<td>" . $v . "</td>");
                                                } ?>                                 
                                            </tr>
                                            <tr>
                                                <td>Reportés (1/2 jours)</td>    
                                    <?php         foreach ($d["reportees"] as $v) {
                                                    echo("<td>" . $v . "</td>");
                                                } ?>                                    
                                            </tr>
                        <?php		    }
                                    }
                                }  ?>
                            </table>-->
             <?php      }else{ 
                            // Données consolidés sur les sociétés / agences de user connect
                            ?>

                            <table class="table table-striped table-hover" >
                                <thead>
                                    <tr class="dark">
                                        <th>Société / agence</th>
                                        <th>Date de relevé</th>
                                        <th class="text-center" >Plannifiés (1/2 jours)</th>
                                        <th class="text-center" >Annulés (1/2 jours)</th>
                                        <th class="text-center" >Reportés (1/2 jours)</th>
                                    </tr>      
                                </thead>
                                <tbody>                                       
                            <?php   $total_plannifiees=0; 
                                    $total_annulees=0;
                                    $total_reportees=0;
                                    foreach ($data as $k => $d) { 
                                        if($k!=0 OR ($d["anomalie"]) ) { 
                                            $total_plannifiees=$total_plannifiees + $d["plannifiees"]; 
                                            $total_annulees=$total_annulees + $d["annulees"]; 
                                            $total_reportees=$total_reportees + $d["reportees"];  ?>   
                                            <tr>  
                                                <td><?=$d["nom"]?></th>
                                                <td><?=date("d/m/Y")?></td>     
                                                <td class="text-center" ><?=$d["plannifiees"]?></td>
                                                <td class="text-center" ><?=$d["annulees"]?></td>
                                                <td class="text-center" ><?=$d["reportees"]?></td>
                                            </tr>
                            <?php       }
                                    } ?>
                                    <tr>  
                                        <td colspan="2" class="text-right">Total :</th>
                                        <td class="text-center" ><?=$total_plannifiees?></td>
                                        <td class="text-center" ><?=$total_annulees?></td>
                                        <td class="text-center" ><?=$total_reportees?></td>
                                    </tr>
                                </tbody>
                            </table>
                <?php   } 
                    } ?>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 text-center">
                    <a href="stat_formation.php" class="btn btn-md btn-default" >
                        Retour
                    </a>
                </div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){

				

			});
		</script>
	</body>
</html>
