<?php

	include "includes/controle_acces.inc.php";
	include_once 'includes/connexion.php';
	include_once 'modeles/mod_add_client_produit.php';
	include_once 'modeles/mod_set_client_produit.php';
	
	include_once 'modeles/mod_add_notification.php';
	
	// ENREGISTREMENT DES DONNEES D'UN PRODUIT
	
	$erreur_txt="";
	
	if($_POST){
		
		$cpr_client=0; 
		if(!empty($_POST['cpr_client'])){
			$cpr_client=intval($_POST['cpr_client']);		
		}
		$cpr_produit=0;
		if(!empty($_POST['cpr_produit'])){
			$cpr_produit=intval($_POST['cpr_produit']);		
		}		
		// val produit avant modif
		$produit=0;
		if(!empty($_POST['produit'])){
			$produit=intval($_POST['produit']);		
		}
		if($cpr_produit==0){
			// PRODUIT SPE cpr_produit n'existe pas
			$cpr_produit=$produit;
		}
		
		if($cpr_client==0 OR $cpr_produit==0){
			echo("B Erreur paramètre!");
			die();
		}
		
		
		// DONNEE POUR TRAITEMENT
		
		// info sur le client
		$req=$Conn->query("SELECT cli_code,cli_categorie,cli_sous_categorie,cca_gc FROM Clients,Clients_Categories WHERE cli_categorie=cca_id AND cli_id=" . $cpr_client . ";");
		$d_client=$req->fetch();
		if(empty($d_client)){
			echo("Erreur : client inconnu !");
			die();
		}
		
		// sur le produit
		$req=$Conn->query("SELECT pro_code_produit,pro_libelle,pro_intra,pro_min_intra,pro_min_dr_intra,pro_ht_intra,pro_inter,pro_min_inter,pro_min_dr_inter,pro_ht_inter,pro_client FROM Produits WHERE pro_id=" . $cpr_produit . ";");
		$d_produit=$req->fetch();
		if(empty($d_produit)){
			echo("Erreur : produit inconnu !");
			die();
		}elseif(!empty($d_produit["pro_client"]) AND $d_produit["pro_client"]!=$cpr_client ){
			echo("Erreur : produit spécifique à un autre client !");
			die();
		}
		
		// Clients Produits avant modif
		if(!empty($produit)){
			$req=$Conn->query("SELECT cpr_inter,cpr_ht_inter,cpr_min_inter,cpr_intra,cpr_ht_intra,cpr_min_intra,cpr_derogation,cpr_rem_famille,cpr_rem_ca
			,cpr_derogation,cpr_nego FROM Clients_Produits WHERE cpr_client=" . $cpr_client . " AND cpr_produit=" . $produit . ";");
			$d_client_old=$req->fetch();
		}

		
		// personne connecté
		$acc_utilisateur=0;
		if(isset($_SESSION['acces']["acc_ref"])){
			if($_SESSION['acces']["acc_ref"]==1){
				$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
			}
		}

		// TRAITEMENT DU FORM
		
		// donnée pour modele add_client_produit et set_client_produit
		$donnee=array();
		
		// fiche client à l'origine de la modif => pour retour
		$client=0;
		if(!empty($_POST['client'])){
			$client=intval($_POST['client']);		
		}
		if($client==0){
			$client=$cpr_client;
		}
		
		if(empty($d_produit["pro_client"])){
			
			// champ non modifiable pour les produit spé => maj via la fiche produit
		
			if(!empty($_POST["cpr_libelle"])){
				$donnee["cpr_libelle"]=$_POST["cpr_libelle"];
			}else{
				$donnee["cpr_libelle"]=$d_produit["pro_libelle"];	
			}
			
			$verrou_tarif=false;
			
			if(isset($d_client_old)){
				if( (!empty($d_client_old["cpr_derogation"]) AND !$_SESSION["acces"]["acc_droits"][27] AND !$_SESSION["acces"]["acc_droits"][31]) OR ($d_client_old["cpr_derogation"]>1 AND !$_SESSION["acces"]["acc_droits"][31]) ){
					
					// une derogation supperieur au niveau de U a été accordé => on bloque toutes modif de tarif
					$verrou_tarif=true;
					
					$cpr_produit=$produit;
					
					$donnee["cpr_intra"]=$d_client_old["cpr_intra"];
					$donnee["cpr_ht_intra"]=$d_client_old["cpr_ht_intra"];
					$donnee["cpr_min_intra"]=$d_client_old["cpr_min_intra"];
					
					$donnee["cpr_inter"]=$d_client_old["cpr_inter"];
					$donnee["cpr_ht_inter"]=$d_client_old["cpr_ht_inter"];
					$donnee["cpr_min_inter"]=$d_client_old["cpr_min_inter"];
					
					$donnee["cpr_derogation"]=$d_client_old["cpr_derogation"];
					$donnee["cpr_rem_famille"]=$d_client_old["cpr_rem_famille"];
					$donnee["cpr_rem_ca"]=$d_client_old["cpr_rem_ca"];
					
					$donnee["cpr_nego"]=$d_client_old["cpr_nego"];
					
				}
			}
			
			if(!$verrou_tarif){

				$donnee["cpr_derogation"]=0;
				if(!empty($_POST['cpr_derogation'])){
					$donnee["cpr_derogation"]=intval($_POST['cpr_derogation']);		
				}
				
				$donnee["cpr_rem_famille"]=0;
				if(!empty($_POST['cpr_rem_famille'])){
					$donnee["cpr_rem_famille"]=1;		
				}
				
				$donnee["cpr_rem_ca"]=0;
				if(!empty($_POST['cpr_rem_ca'])){
					$donnee["cpr_rem_ca"]=1;		
				}
				$donnee["cpr_nego"]=0;
				if(!empty($_POST['cpr_nego'])){
					$donnee["cpr_nego"]=1;		
				}
				
				// securite sur le niveau de dérogation
				if($donnee["cpr_derogation"]>1 AND !$_SESSION["acces"]["acc_droits"][31]){
					$donnee["cpr_derogation"]=0;
					$donnee["cpr_rem_famille"]=0;	
					$donnee["cpr_rem_ca"]=0;	
					$donnee["cpr_nego"]=0;		
				}elseif($donnee["cpr_derogation"]>0 AND !$_SESSION["acces"]["acc_droits"][27] AND !$_SESSION["acces"]["acc_droits"][31]){
					$donnee["cpr_derogation"]=0;
					$donnee["cpr_rem_famille"]=0;	
					$donnee["cpr_rem_ca"]=0;
					$donnee["cpr_nego"]=0;							
				}

				$donnee["cpr_intra"]=0;
				$donnee["cpr_ht_intra"]=0;
				$donnee["cpr_min_intra"]=0;
				
				if($d_produit["pro_intra"]){
					
					if(!empty($_POST["cpr_intra"])){
						
						$donnee["cpr_intra"]=1;
						
						if(!empty($_POST["cpr_ht_intra"])){
							$donnee["cpr_ht_intra"]=floatval($_POST["cpr_ht_intra"]);	
						}else{
							$donnee["cpr_ht_intra"]=$d_produit["pro_ht_intra"];	
						}
						
						if($donnee["cpr_derogation"]==2){
							$donnee["cpr_min_intra"]=0;	
						}elseif($donnee["cpr_derogation"]==1){
							$donnee["cpr_min_intra"]=$d_produit["pro_min_dr_intra"];	
						}else{
							$donnee["cpr_min_intra"]=$d_produit["pro_min_intra"];	
						}
						
					}
				}
				
				$donnee["cpr_inter"]=0;
				$donnee["cpr_ht_inter"]=0;
				$donnee["cpr_min_inter"]=0;
					
				if($d_produit["pro_inter"]){
					if(!empty($_POST["cpr_inter"])){
						
						$donnee["cpr_inter"]=1;
						
						if(!empty($_POST["cpr_ht_inter"])){
							$donnee["cpr_ht_inter"]=floatval($_POST["cpr_ht_inter"]);	
						}else{
							$donnee["cpr_ht_inter"]=$d_produit["pro_ht_inter"];	
						}
						
						if($donnee["cpr_derogation"]==2){
							$donnee["cpr_min_inter"]=0;	
						}elseif($donnee["cpr_derogation"]==1){
							$donnee["cpr_min_inter"]=$d_produit["pro_min_dr_inter"];	
						}else{
							$donnee["cpr_min_inter"]=$d_produit["pro_min_inter"];	
						}
					}
				}
			}
		
		}
		
		if(!empty($_POST["cpr_devis_txt"])){
			if($_POST["cpr_devis_txt"]!="<br>"){
				$donnee["cpr_devis_txt"]=$_POST["cpr_devis_txt"];
			}else{
				$donnee["cpr_devis_txt"]="";
			}
		}

		if(!empty($_POST["cpr_comment_txt"])){
			$donnee["cpr_comment_txt"]=$_POST["cpr_comment_txt"];	
		}
		
		// ACCES VIA DRT GC
		if($_SESSION["acces"]["acc_droits"][8]){
			
			if($d_client["cli_categorie"]==2){
				
				// GC
			
				$donnee["cpr_valide"]=0;
				if(!empty($_POST["cpr_valide"]) OR !empty($d_produit["pro_client"])){
					$donnee["cpr_valide"]=1;
				}
				
				$donnee["cpr_alerte_jour"]=0;
				if(!empty($_POST["cpr_alerte_jour"])){
					$donnee["cpr_alerte_jour"]=intval($_POST["cpr_alerte_jour"]);	
				}
				
				$donnee["cpr_confirmation_gc"]=0;
				if(!empty($_POST["cpr_confirmation_gc"])){
					$donnee["cpr_confirmation_gc"]=1;	
				}
				
				if($d_client["cli_sous_categorie"]==1){
					$donnee["cpr_cn"]=1;
				}elseif($d_client["cli_sous_categorie"]==2){
					$donnee["cpr_cn"]=0;
				}else{
					$donnee["cpr_cn"]=0;
					if(!empty($_POST["cpr_cn"])){
						$donnee["cpr_cn"]=1;
					}
				}
				
				
			}elseif($d_client["cli_categorie"]==5){
				
				// CLIENT NATIONNAL
				
				$donnee["cpr_valide"]=0;
				if(!empty($_POST["cpr_valide"])){
					$donnee["cpr_valide"]=1;
				}
				$donnee["cpr_alerte_jour"]=0;
				$donnee["cpr_confirmation_gc"]=0;
				$donnee["cpr_cn"]=0;

			}
		}
		
		//$donnee["cpr_archive"]=0;
		if(empty($d_produit["pro_client"])){
			if(empty($donnee["cpr_inter"]) AND empty($donnee["cpr_intra"]) AND $donnee["cpr_valide"]==1){
				echo("C Erreur paramètre!");
				die();
			}
		}
		
		/*echo("<pre>");
		print_r($donnee);
		echo("</pre>");
		die();*/
	
		// *** FIN TRAITEMENT FORM ***


		// ENREGISTREMENT
		// L'archivage se gere au niveau de la base produit.
		// Si un produit est archivé le commercial doit enregistrer une correspondance dans c'est base produit
		
		/*if(isset($d_client_old)){
			if(!empty($d_client_old)){
				if($produit!=$cpr_produit AND $d_client_old["cpr_archive"]==0){
					// changement de produit 
					// produit est archivé
					
					$donnee_old=array(
						"cpr_archive" => 1
					);
					set_client_produit($produit,$cpr_client,$donnee_old,$d_client_old);
				}
			}
		}*/
		
		// tab de donnée à mettre a jour
		
		$sql="SELECT cpr_produit,cpr_intra,cpr_intra,cpr_ht_intra,cpr_inter,cpr_ht_inter,cpr_derogation,cpr_nego FROM Clients_Produits WHERE cpr_client=" . $cpr_client . " AND cpr_produit=" . $cpr_produit . ";";
		$req=$Conn->query($sql);
		$existe=$req->fetch();
		if(!empty($existe)){
			set_client_produit($cpr_produit,$cpr_client,$donnee,$existe);
		}else{
			add_client_produit($cpr_produit,$cpr_client,$donnee);
		}
		
		// LE NIVEAU DE DEROGATION AUGMENTE => NOTIF DC ET DAF
		
		/*var_dump($existe);
		echo("<br/>");
		var_dump($donnee["cpr_derogation"]);
		echo("<br/>");
		var_dump($d_client_old["cpr_derogation"]);
		die();*/
		
		if(!empty($existe) AND $donnee["cpr_nego"]==1 AND $d_client_old["cpr_nego"]==0){
			
			$texte=$_SESSION["acces"]["acc_prenom"] . " " . $_SESSION["acces"]["acc_nom"] . " vient de faire une demande de renégociation pour le code " . $d_produit["pro_code_produit"] . " 
			chez le client " . $d_client["cli_code"] . ".";

			$liste_uti="";
			$sql="SELECT cso_utilisateur FROM Clients_Societes WHERE cso_client=" .  $cpr_client . " AND NOT cso_archive;"; 
			$req=$Conn->query($sql);
			$d_uti_not=$req->fetchAll();
			if(!empty($d_uti_not)){
				$tab_uti=array_column ($d_uti_not,"cso_utilisateur");
				$liste_uti=implode($tab_uti,",");
			}
			if(!empty($liste_uti)){
				add_notifications("<i class='fa fa-euro' ></i>",$texte,"bg-info","client_voir.php?client=" . $cpr_client . "&onglet=13","","",$liste_uti,0,0,1);
			}
		}
		
		
		if( (!empty($existe) AND $donnee["cpr_derogation"]>$d_client_old["cpr_derogation"]) OR (empty($existe) AND $donnee["cpr_derogation"]>0) ){
			$texte=$_SESSION["acces"]["acc_prenom"] . " " . $_SESSION["acces"]["acc_nom"] . " vient d'enregistrer une dérogation de niveau " . $donnee["cpr_derogation"] . " 
			sur le code produit " . $d_produit["pro_code_produit"] . " pour le client " . $d_client["cli_code"]; 

			add_notifications("<i class='fa fa-euro' ></i>",$texte,"bg-info","client_voir.php?client=" . $cpr_client . "&onglet=13","10,11","","",0,0,null);
			
		}
		
	}else{
		echo("A Erreur paramètre!");
		die("1");
	}
	
	$url="client_voir.php?client=" . $client . "&onglet=13";
	Header("Location: ". $url);
?>