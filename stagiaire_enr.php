<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');

	// ENREGISTREMENT D'UNE FICHE STAGIAIRE
	
	$err_danger="";
	$err_warning="";
	
	if(isset($_POST)){
		
		if(empty($_POST['sta_nom']) OR empty($_POST['sta_prenom'])){
			
			$err_danger="Formulaire incomplet! La fiche n'a pas été enregistrée.";
		
		}else{
			
			$stagiaire=0;
			if(!empty($_POST['sta_id'])){
				$stagiaire=intval($_POST['sta_id']);
			}
			
			$sta_nom="";
			if(!empty($_POST['sta_nom'])){
				$sta_nom=trim($_POST['sta_nom']);
			}
			
			$sta_prenom="";
			if(!empty($_POST['sta_prenom'])){
				$sta_prenom=trim($_POST['sta_prenom']);
			}
			
			$sta_naissance=null;
			if(!empty($_POST['sta_naissance'])){
				$Dt_naiss=DateTime::createFromFormat('d/m/Y',$_POST['sta_naissance']);
				if(!is_bool($Dt_naiss)){
					$sta_naissance=$Dt_naiss->format("Y-m-d");
				}
			}
			
			$sta_client=0;				
			if(!empty($_POST["employeur"])){
				if(!empty($_POST['sta_client'])){
					$sta_client=intval($_POST['sta_client']);
				}
			}
			
			$sta_titre=0;				
			if(!empty($_POST["sta_titre"])){
				$sta_titre=intval($_POST['sta_titre']);
			}
			
			/**************************************
				CONTROLE
			***************************************/
			
			if(!empty($stagiaire)){
				
				// donnee avant enr
					
				$req=$Conn->query("SELECT sta_client FROM Stagiaires WHERE sta_id=" . $stagiaire . ";");
				$d_stagiaire=$req->fetch();
				
				// si form ne contient oas sta_client on garde la valeur enregistre
				if(empty($sta_client)){
					$sta_client=$d_stagiaire["sta_client"];
				}
			}
			if(empty($sta_client)){
				$err_danger="Le stagiaire doit-être associé à un client";
			}
			
			
			if(empty($err_danger)){

				// on regarde si l'employeur actuel fait partie d'un groupe
				
				$mm=$sta_client;
				$req=$Conn->query("SELECT cli_filiale_de,cli_code,cli_nom FROM CLients WHERE cli_id=" . $sta_client . ";");
				$d_client=$req->fetch();
				if(!empty($d_client)){
					if(!empty($d_client["cli_filiale_de"])){
						$mm=$d_client["cli_filiale_de"];
					}
				}else{
					$err_danger="Le client associé au stagiaire n'a pas été trouvé";	
				}
			}
			
			if(empty($err_danger)){
				
				// controle de doublon
				
				$mil="";
				$sql="SELECT sta_id FROM Stagiaires";
				if(!empty($sta_naissance)){
					$mil.=" AND sta_naissance=:sta_naissance";
				}else{
					$sql.=" INNER JOIN Stagiaires_Clients ON (Stagiaires.sta_id=Stagiaires_Clients.scl_stagiaire)
					INNER JOIN Clients ON (Stagiaires_Clients.scl_client=Clients.cli_id)";
					$mil.=" AND (cli_id=" . $mm . " OR cli_filiale_de=" . $mm . ")";
				}
				$sql.=" WHERE NOT sta_id=:stagiaire AND sta_nom=:sta_nom AND sta_prenom=:sta_prenom" . $mil . ";";
				$req=$Conn->prepare($sql);
				$req->bindParam("stagiaire",$stagiaire);
				$req->bindParam("sta_nom",$sta_nom);
				$req->bindParam("sta_prenom",$sta_prenom);
				if(!empty($sta_naissance)){
					$req->bindParam("sta_naissance",$sta_naissance);
				}
				$d_doublon=$req->fetch();
				if(!empty($d_doublon)){
					$err_danger="Le stagiaire existe déjà.";
				}
				
			}
		}
		
		/****************************
			ENREGISTREMENT
		****************************/
		
		if(empty($err_danger)){
			
			if($stagiaire==0){
				
				$req = $Conn->prepare("INSERT INTO Stagiaires 
					(sta_nom, 
					sta_prenom, 
					sta_naissance, 
					sta_ad1,
					sta_ad2,					
					sta_ad3,
					sta_cp,
					sta_ville,
					sta_ville_naiss,
					sta_cp_naiss,
					sta_portable,
					sta_tel,
					sta_mail_perso,
					sta_titre,
					sta_client,
					sta_client_code,
					sta_client_nom
					) 
					VALUES 
					(:sta_nom, 
					:sta_prenom, 
					:sta_naissance, 
					:sta_ad1,
					:sta_ad2, 
					:sta_ad3,
					:sta_cp,
					:sta_ville,
					:sta_ville_naiss,
					:sta_cp_naiss,
					:sta_portable,
					:sta_tel,
					:sta_mail_perso,
					:sta_titre,
					:sta_client,
					:sta_client_code,
					:sta_client_nom);"
				);
				$req->bindParam(':sta_nom', $sta_nom);
				$req->bindParam(':sta_prenom', $sta_prenom);
				$req->bindParam(':sta_naissance', $sta_naissance);
				$req->bindParam(':sta_ad1', $_POST['sta_ad1']);
				$req->bindParam(':sta_ad2', $_POST['sta_ad2']);
				$req->bindParam(':sta_ad3', $_POST['sta_ad3']);
				$req->bindParam(':sta_cp', $_POST['sta_cp']);
				$req->bindParam(':sta_ville', $_POST['sta_ville']);
				$req->bindParam(':sta_ville_naiss', $_POST['sta_ville_naiss']);
				$req->bindParam(':sta_cp_naiss', $_POST['sta_cp_naiss']);
				$req->bindParam(':sta_portable', $_POST['sta_portable']);
				$req->bindParam(':sta_tel', $_POST['sta_tel']);
				$req->bindParam(':sta_mail_perso', $_POST['sta_mail_perso']);
				$req->bindParam(':sta_titre', $sta_titre);
				$req->bindParam(':sta_client', $sta_client);
				$req->bindParam(':sta_client_code', $d_client['cli_code']);
				$req->bindParam(':sta_client_nom', $d_client['cli_nom']);
				try{					
					$req->execute();					
					$stagiaire = $Conn->lastInsertId();
				}Catch(Exception $e){
					$err_danger="ADD " . $e->getMessage();
				}
				
				// on associe le client et le stagiaire
				if(empty($err_danger)){
					$req = $Conn->prepare("INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:scl_stagiaire,:scl_client);");
					$req->bindParam(':scl_stagiaire', $stagiaire);
					$req->bindParam(':scl_client', $sta_client);
					try{					
						$req->execute();					
					}Catch(Exception $e){
						$err_warning=$e->getMessage();
					}
				}
				
			}else{
				
				$req = $Conn->prepare("UPDATE Stagiaires SET
				sta_nom=:sta_nom, 
				sta_prenom=:sta_prenom, 
				sta_naissance=:sta_naissance, 
				sta_ad1=:sta_ad1,
				sta_ad2=:sta_ad2,					
				sta_ad3=:sta_ad3,
				sta_cp=:sta_cp,
				sta_ville=:sta_ville,
				sta_ville_naiss=:sta_ville_naiss,
				sta_cp_naiss=:sta_cp_naiss,
				sta_portable=:sta_portable,
				sta_tel=:sta_tel,
				sta_mail_perso=:sta_mail_perso,
				sta_titre=:sta_titre,
				sta_client=:sta_client,
				sta_client_code=:sta_client_code,
				sta_client_nom=:sta_client_nom
				WHERE sta_id=:stagiaire;");
				$req->bindParam(':sta_nom', $sta_nom);
				$req->bindParam(':sta_prenom', $sta_prenom);
				$req->bindParam(':sta_naissance', $sta_naissance);
				$req->bindParam(':sta_ad1', $_POST['sta_ad1']);
				$req->bindParam(':sta_ad2', $_POST['sta_ad2']);
				$req->bindParam(':sta_ad3', $_POST['sta_ad3']);
				$req->bindParam(':sta_cp', $_POST['sta_cp']);
				$req->bindParam(':sta_ville', $_POST['sta_ville']);
				$req->bindParam(':sta_ville_naiss', $_POST['sta_ville_naiss']);
				$req->bindParam(':sta_cp_naiss', $_POST['sta_cp_naiss']);
				$req->bindParam(':sta_portable', $_POST['sta_portable']);
				$req->bindParam(':sta_tel', $_POST['sta_tel']);
				$req->bindParam(':sta_mail_perso', $_POST['sta_mail_perso']);
				$req->bindParam(':sta_titre', $sta_titre);
				$req->bindParam(':sta_client', $sta_client);
				$req->bindParam(':sta_client_code', $d_client['cli_code']);
				$req->bindParam(':sta_client_nom', $d_client['cli_nom']);
				$req->bindParam(':stagiaire', $stagiaire);
				try{					
					$req->execute();					
				}Catch(Exception $e){
					$err_danger="UPDATE ". $e->getMessage();
				}
				
				// on s'assure que le stagiaire est bien lié au client
				if(empty($err_danger)){
					$sql="SELECT scl_client FROM Stagiaires_Clients WHERE scl_stagiaire=:stagiaire AND scl_client=:client;";
					$req = $Conn->prepare($sql);
					$req->bindParam(':stagiaire', $stagiaire);
					$req->bindParam(':client', $sta_client);
					$req->execute();	
					$lien=$req->fetch();
					if(empty($lien)){
						$req = $Conn->prepare("INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:scl_stagiaire,:scl_client);");
						$req->bindParam(':scl_stagiaire', $stagiaire);
						$req->bindParam(':scl_client', $sta_client);
						try{					
							$req->execute();					
						}Catch(Exception $e){
							$err_warning=$e->getMessage();
						}
					}
				}
			}
		}
				
		/****************************
			TRAITEMENT ANNEXE
		****************************/	
		
		if(empty($err_danger)){
			
			// MAJ DE L'ADRESSE CLIENTS PARTICULIER
			
			$client_adresse=0;
			$sql="SELECT adr_id,adr_ref_id FROM Adresses,Clients WHERE adr_ref_id=cli_id AND cli_stagiaire=:stagiaire;";
			$req = $Conn->prepare($sql);
			$req->bindParam(':stagiaire', $stagiaire);
			$req->execute();
			$adresses=$req->fetchAll();
			/*echo("<pre>");
				print_r($adresses);
			echo("</pre>");*/
			print_r($adresses);
			if(!empty($adresses)){
				if(count($adresses)==1){
					
					$req = $Conn->prepare("UPDATE Adresses SET
					adr_ad1=:adr_ad1,
					adr_ad2=:adr_ad2,					
					adr_ad3=:adr_ad3,
					adr_cp=:adr_cp,
					adr_ville=:adr_ville
					WHERE adr_id=:adresse;");
					$req->bindParam(':adr_ad1', $_POST['sta_ad1']);
					$req->bindParam(':adr_ad2', $_POST['sta_ad2']);
					$req->bindParam(':adr_ad3', $_POST['sta_ad3']);
					$req->bindParam(':adr_cp', $_POST['sta_cp']);
					$req->bindParam(':adr_ville', $_POST['sta_ville']);
					$req->bindParam(':adresse',$adresses[0]["adr_id"]);
					try{					
						$req->execute();	
						$client_adresse=$adresses[0]["adr_ref_id"];						
					}Catch(Exception $e){
						$err_warning.="Echec de la mise à jour de l'adresse client.<br/>" . $e->getMessage();
					}
				}else{
					$err_warning.="Le stagiaire est associé à plusieurs fiches clients. Ces dernières n'ont pas été mises à jour!";
				}
			}
			
			// MAJ DU CONTACT CLIENTS PARTICULIER
			
			$client_contact=0;
			$sql="SELECT con_id,con_ref_id FROM Contacts,Clients WHERE con_ref_id=cli_id AND cli_stagiaire=:stagiaire;";
			$req = $Conn->prepare($sql);
			$req->bindParam(':stagiaire', $stagiaire);
			$req->execute();
			$contacts=$req->fetchAll();
			/*echo("<pre>");
				print_r($contacts);
			echo("</pre>");*/
			if(!empty($contacts)){
				if(count($contacts)==1){
					
					
					
					$req = $Conn->prepare("UPDATE Contacts SET
					con_nom=:con_nom,
					con_prenom=:con_prenom,		
					con_tel=:con_tel,
					con_portable=:con_portable,
					con_mail=:con_mail,
					con_titre=:con_titre
					WHERE con_id=:con_id;");
					$req->bindParam(':con_nom', $sta_nom);
					$req->bindParam(':con_prenom', $sta_prenom);
					$req->bindParam(':con_tel', $_POST['sta_tel']);
					$req->bindParam(':con_portable', $_POST['sta_portable']);
					$req->bindParam(':con_mail', $_POST['sta_mail_perso']);
					$req->bindParam(':con_titre', $sta_titre);
					$req->bindParam(':con_id',$contacts[0]["con_id"]);
					try{					
						$req->execute();	
						$client_contact=$contacts[0]["con_ref_id"];
					}Catch(Exception $e){
						$err_warning.="Echec de la mise à jour de du contact client.<br/>" . $e->getMessage();
					}
				}else{
					$err_warning.="Le stagiaire est associé à plusieurs fiches clients. Ces dernières n'ont pas été mises à jour!";
				}
			}
			
			// ON MAJ LES VALEURS D'ADRESSE ET DE CONTACT PAR DEFAUT SUR LA FICHE CLIENT
			
			if(!empty($client_adresse) OR !empty($client_contact)){
				
				if(!empty($client_adresse) AND !empty($client_contact) AND $client_adresse!=$client_contact){
					// on s'assure que le contact et l'adresse sont bien associé au meme client 
					$err_warning.="Le stagiaire est associé à plusieurs fiches clients. Ces dernières n'ont pas été mises à jour!";
				}else{
					
					if(!empty($client_adresse)){
						$client_id=$client_adresse;
					}else{
						$client_id=$client_contact;
					}
					
					$sql="UPDATE Clients SET ";
					if(!empty($client_adresse)){
						$sql.="cli_adresse=:cli_adresse,
						cli_adr_ad1=:sta_ad1,
						cli_adr_ad2=:sta_ad2,					
						cli_adr_ad3=:sta_ad3,
						cli_adr_cp=:sta_cp,
						cli_adr_ville=:sta_ville";
						if(!empty($client_contact)){
							$sql.=",";
						}
					}
					if(!empty($client_contact)){
						$sql.="cli_contact=:cli_contact,
						cli_con_nom=:sta_nom, 
						cli_con_prenom=:sta_prenom, 
						cli_con_tel=:sta_tel,
						cli_con_mail=:sta_mail_perso,
						cli_con_titre =:sta_titre";
					}
					$sql.=" WHERE cli_id=:client;";
					$req = $Conn->prepare($sql);
					if(!empty($client_adresse)){
						$req->bindParam(':cli_adresse', $adresses[0]["adr_id"]);
						$req->bindParam(':sta_ad1', $_POST['sta_ad1']);
						$req->bindParam(':sta_ad2', $_POST['sta_ad2']);
						$req->bindParam(':sta_ad3', $_POST['sta_ad3']);
						$req->bindParam(':sta_cp', $_POST['sta_cp']);
						$req->bindParam(':sta_ville', $_POST['sta_ville']);
					}
					if(!empty($client_contact)){	
						$req->bindParam(':cli_contact',$contacts[0]["con_id"]);
						$req->bindParam(':sta_nom', $sta_nom);
						$req->bindParam(':sta_prenom', $sta_prenom);					
						$req->bindParam(':sta_tel', $_POST['sta_tel']);
						$req->bindParam(':sta_mail_perso', $_POST['sta_mail_perso']);
						$req->bindParam(':sta_titre', $sta_titre);
					}
					$req->bindParam(':client',$client_id);
					
					try{					
						$req->execute();	
					}Catch(Exception $e){
						$err_warning.="Echec de la mise à jour des coordonnées par défaut du client.<br/>" . $e->getMessage();
					}
				}
			}
			
		}
		// FIN TRAITEMENT ANNEXE
		
	}else{
		$err_danger="Formulaire incomplet! La fiche n'a pas été enregistrée.";
	}
	
	if(!empty($err_danger)){
		
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $err_danger
		);
		
		if(!empty($stagiaire)){
			header("location : stagiaire_voir.php?stagiaire=" . $stagiaire);
			die();
		}else{
			header("location : stagiaire_liste.php");
			die();
		}
	}else{
		
		if(!empty($err_warning)){
			echo("WARNING");
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "warning",
				"message" => $err_warning
			);
		}else{
			$_SESSION['message'][] = array(
				"titre" => "Enregistrement terminé",
				"type" => "success",
				"message" => "Vos modifications ont bien été prise en compte."
			);
		}
		Header("Location: stagiaire_voir.php?stagiaire=" . $stagiaire);
		
		
	}
?>