<?php
include "includes/controle_acces.inc.php";

// TRAITEMENT DU FORM suspect_correspondance

include("includes/connexion_soc.php");
include("modeles/mod_set_suspect_contact.php");
include("modeles/mod_set_suspect_correspondance.php");

if(isset($_POST)){
	
	$suspect=0;
	if(!empty($_POST['suspect'])){
		$suspect=intval($_POST['suspect']);
	}
	
	$sco_date_sql="";
	if(!empty($_POST['sco_date'])){
		$date = date_create_from_format('d/m/Y',$_POST['sco_date']);
		if(!is_bool($date)){
			$sco_date_sql=$date->format("Y-m-d");
		}
	}

	if($suspect>0 AND !empty($sco_date_sql)){
		
		$categorie=0;
		if(!empty($_POST['categorie'])){
			$categorie=intval($_POST['categorie']);
		}
		
		$edit_correspondance=array();
		
		if(!empty($_POST['correspondance'])){
			$edit_correspondance["sco_id"]=intval($_POST['correspondance']);
		}
		if($_SESSION['acces']["acc_ref"]==1){
			$edit_correspondance["sco_utilisateur"]=$_SESSION['acces']["acc_ref_id"];
		}
		$edit_correspondance["sco_suspect"]=$suspect;
		$edit_correspondance["sco_date"]=$sco_date_sql;
		
		$edit_correspondance["sco_type"]=1;
		if(!empty($_POST['sco_type'])){
			$edit_correspondance["sco_type"]=intval($_POST['sco_type']);
		}
		$edit_correspondance["sco_raison"]=0;
		if(!empty($_POST['sco_raison'])){
			$edit_correspondance["sco_raison"]=intval($_POST['sco_raison']);
			if($edit_correspondance["sco_raison"]==-1){
				$edit_correspondance["sco_raison_info"]=$_POST['sco_raison_info'];
			}else{
				$edit_correspondance["sco_raison_info"]="";
			}
		}
		

		// contact
	
		$nv_contact=false;	
		$edit_correspondance["sco_contact"]=0;
		$edit_correspondance["sco_contact_nom"]="";
		$edit_correspondance["sco_contact_prenom"]="";
		$edit_correspondance["sco_contact_tel"]="";
		
		
			if($_POST['sco_contact']=="nouveau"){
				
				if($categorie!=3){
					$nv_contact=true;
				}
				
			}else{
				$edit_correspondance["sco_contact"]=intval($_POST['sco_contact']);
				$edit_correspondance["sco_contact_nom"]=$_POST['sco_contact_nom'];
				$edit_correspondance["sco_contact_prenom"]=$_POST['sco_contact_prenom'];
				$edit_correspondance["sco_contact_tel"]=$_POST['sco_contact_tel'];
				
			}
		

		$edit_correspondance["sco_commentaire"]=$_POST['sco_commentaire'];
		
		$sco_rappel_sql="";
		if(!empty($_POST['sco_rappeler_le'])){
			$rappel = date_create_from_format('d/m/Y',$_POST['sco_rappeler_le']);
			if(!is_bool($rappel)){
				$edit_correspondance["sco_rappeler_le"]=$rappel->format("Y-m-d");
			}
		}
		
		if(!empty($_POST['sco_rappel'])){
			$edit_correspondance["sco_rappel"]=1;
		}else{
			$edit_correspondance["sco_rappel"]=0;
		}

		// GESTION DE L'ENREGISTREMENT D'UN NOUVEAU CONTACT
		
		if($nv_contact){
			
			if(!empty($_POST['sco_nom'])){
				$sco_nom=$_POST['sco_nom'];
			}else{
				$_SESSION['message'][] = array(
					"titre" => "Attention",
					"type" => "danger",
					"message" => "Veuillez renseigner au moins le nom, le prénom et un numéro de téléphone svp."
				);
				header("location : suspect_correspondance.php?suspect=" . $suspect . "&tab5");
				die();	
				$nv_contact=false;
			}
			if(!empty($_POST['sco_tel'])){
				
				$sco_tel=$_POST['sco_tel'];
			}else{
				$_SESSION['message'][] = array(
					"titre" => "Attention",
					"type" => "danger",
					"message" => "Veuillez renseigner au moins le nom, le prénom et un numéro de téléphone svp."
				);
				header("location : suspect_correspondance.php?suspect=" . $suspect . "&tab5");
				die();	
				$nv_contact=false;
			}
		}
		if($nv_contact){
			
			$sco_fonction=0;
			$sco_fonction_nom="";
			if(!empty($_POST["sco_fonction"])){
				if($_POST["sco_fonction"]=="autre"){					
					$sco_fonction_nom=$_POST['sco_fonction_nom'];
				}else{
					$sco_fonction=intval($_POST["sco_fonction"]);
				}
			}
			
			$sco_titre=0;
			if(!empty($_POST["sco_titre"])){
				$sco_titre=intval($_POST["sco_titre"]);
			}
			
			$edit_contact=array(
				"sco_ref_id" => $suspect,
				"sco_fonction" => $sco_fonction,
				"sco_fonction_nom" => $sco_fonction_nom,
				"sco_titre" => $sco_titre,
				"sco_nom" => $sco_nom,
				"sco_prenom" => $_POST["sco_prenom"],
				"sco_tel" => $sco_tel,
				"sco_portable" => $_POST["sco_portable"],
				"sco_fax" => $_POST["sco_fax"],
				"sco_mail" => $_POST["sco_mail"]
			);
		}

		// ENREGISTREMENT
		
		
		
		// nouveau contact
		
		if($nv_contact){
			$contact=set_suspect_contact($edit_contact);
			if(is_int($contact)){
				$edit_correspondance["sco_contact"]=$contact;
				$edit_correspondance["sco_contact_nom"]=$edit_contact['sco_nom'];
				$edit_correspondance["sco_contact_prenom"]=$edit_contact['sco_prenom'];
				$edit_correspondance["sco_contact_tel"]=$edit_contact['sco_tel'];
			}
		}
		
		
		// SUR LE SUSPECT
		
		$req=$ConnSoc->query("SELECT sco_id FROM Suspects_Correspondances WHERE sco_suspect=" . $suspect . ";");
		$d_cor_avant_maj=$req->fetch();
		
		$correspondance=set_suspect_correspondance($edit_correspondance);
		
		if(is_bool($correspondance)){
			echo("Erreur : impossible d'enregsitrer la correspondance!");
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "danger",
				"message" => "Veuillez renseigner au moins la date et un commentaire svp."
			);
			header("location : suspect_correspondance.php?suspect=" . $suspect . "&tab5");
			die();	
		}else{
			if(!empty($d_cor_avant_maj)){
				$_SESSION['message'][] = array(
					"titre" => "Succès",
					"type" => "success",
					"message" => "La correspondance a bien été ajoutée"
				);
			}else{
				$_SESSION['message'][] = array(
					"titre" => "Succès",
					"type" => "success",
					"message" => "La première correspondance a bien été ajoutée. Le statut de la fiche est passé à <b>prospect non validé</b>"
				);
			}
			header("location : " . $_SESSION['retourCorresp']);
			die();
		}
		

	}else{
		echo("Erreur paramètre!");
		die();
	}
	
}else{
	echo("Erreur form!");
	die();
}
