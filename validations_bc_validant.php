<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	

	// LES UTILISATEURS
	$req = $Conn->prepare("SELECT uti_id,uti_prenom,uti_nom FROM utilisateurs WHERE NOT uti_archive ORDER BY uti_nom,uti_prenom");
	$req->execute();
	$utilisateurs = $req->fetchAll();
	
	// LES PROFILS
	$req = $Conn->prepare("SELECT pro_id,pro_libelle FROM profils ORDER BY pro_libelle");
	$req->execute();
	$profils = $req->fetchAll();

	// LES DROITS
	$req = $Conn->prepare("SELECT drt_id,drt_nom FROM Droits ORDER BY drt_nom");
	$req->execute();
	$droits = $req->fetchAll();
	
	// EDITION DU GRILLE
	$grille=0;
	if(!empty($_GET['id'])){
		$grille=intval($_GET['id']);
	}
	
	if(!empty($grille)){
		
		$req = $Conn->prepare("SELECT * FROM commandes_grilles WHERE cgr_id=" . $grille . ";");
		$req->execute();
		$d_grille = $req->fetch();
		
		// toutes les familels de fournisseurs pour le select
		$req = $Conn->prepare("SELECT ffa_libelle FROM fournisseurs_familles WHERE ffa_id=" . $d_grille["cgr_famille"] . ";");
		$req->execute();
		$famille = $req->fetch();
		
	}else{
		
		$d_grille=array(
			"cgr_montant_min" => 0,
			"cgr_responsable_1" => 0,
			"cgr_utilisateur_1" => null,
			"cgr_profil_1" => 0,
			"cgr_droit_1" => 0,
			"cgr_utilisateur_2" => 0,
			"cgr_profil_2" => 0,
			"cgr_droit_2" => 0,
			"cgr_utilisateur_3" => 0,
			"cgr_profil_3" => 0,
			"cgr_droit_3" => 0,
			"cgr_utilisateur_4" => 0,
			"cgr_profil_4" => 0,
			"cgr_droit_4" => 0
		);
		
		// toutes les familels de fournisseurs pour le select
		$req = $Conn->prepare("SELECT * FROM fournisseurs_familles ORDER BY ffa_id");
		$req->execute();
		$fournisseurs_familles = $req->fetchAll();
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    	<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
		<form method="post" action="validations_bc_enr.php" >
<?php	 	if(isset($_GET['id'])){ ?>
				<input type="hidden" name="grille" value="<?=$grille?>" />
	<?php 	}?>
			<div id="main">
		<?php	include "includes/header_def.inc.php"; ?>	
					
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper">		 
					<!-- Begin: Content -->
								
					<section id="content" class="animated fadeIn">
					
					
						<div class="admin-form theme-primary ">
						
							<div class="panel heading-border panel-primary">
								<div class="panel-body bg-light">
								
									<div class="row" >
									
										<div class="col-md-10 col-md-offset-1" >
								
											<div class="row" >
												<div class="col-md-12" >							
													<div class="text-center">								
														<div class="content-header">
												<?php	if(!empty($grille)){ ?>
															<h2>Mise à jour d'une <b class="text-primary">grille de validation</b></h2>
												<?php	}else{ ?>
															<h2>Nouvelle <b class="text-primary">grille de validation</b></h2>	
												<?php	} ?>
															
														</div>
													</div>
												</div>
											</div>									
											<div class="row">
												<div class="col-md-4">
											<?php	if(empty($grille)){ ?>
														<div class="section">
															<select id="cgr_famille" name="cgr_famille" required class="select2">
													<?php 		foreach($fournisseurs_familles as $ffa){ ?>
																	<option value="<?= $ffa['ffa_id'] ?>"><?= $ffa['ffa_libelle'] ?></option>
													<?php 		}?>
															</select>
														</div>
											<?php	}else{ ?>
														<div class="section pt15 text-center">
															Famille : <strong><?=$famille["ffa_libelle"]?></strong>
														</div>
											<?php	} ?>
			                                    </div>
			                                    <div class="col-md-2">
			                                    	<h3 style="margin-top:10px;" class="text-right">Montant supérieur à</h3>
			                                    </div>
			                                    <div class="col-md-4">
													<div class="section">
			                                            <input type="number" required step="0.01" id="cgr_montant_min" name="cgr_montant_min" class="gui-input" placeholder="Montant (€)" value="<?=$d_grille["cgr_montant_min"]?>" >
			                                        </div>
			                                    </div>			                                    
											</div>
											
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span>Validants</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3">
													<div class="field select mb10">
			                                            <select name="type_1" id="type_1">
			                                                <option value="1" <?php if(!empty($d_grille["cgr_profil_1"])) echo("selected"); ?> >Profil</option>
			                                                <option value="2" <?php if(!empty($d_grille["cgr_utilisateur_1"]) OR is_null($d_grille["cgr_utilisateur_1"])) echo("selected"); ?> >Utilisateur</option>
															<option value="3" <?php if(!empty($d_grille["cgr_responsable_1"])) echo("selected"); ?> >Responsable du DO</option>
															<option value="4" <?php if(!empty($d_grille["cgr_droit_1"])) echo("selected"); ?> >Droit</option>
			                                            </select>
			                                        	<i class="arrow simple"></i>
			                                        </div>													
			                                        <div class="section" id="cont_utilisateur_1" <?php if(empty($d_grille["cgr_utilisateur_1"]) AND !is_null($d_grille["cgr_utilisateur_1"]) ) echo("style='display:none;'"); ?> >
			                                            <select name="utilisateur_1" id="utilisateur_1" class="select2">
			                                                <option value="0" selected="selected">Validant 1...</option>
												<?php		foreach($utilisateurs as $u){
																if($d_grille["cgr_utilisateur_1"]==$u["uti_id"]){
																	echo("<option value='" . $u["uti_id"] . "' selected >" . $u["uti_nom"] . " " . $u["uti_prenom"] . "</option>");
																}else{
																	echo("<option value='" . $u["uti_id"] . "' >" . $u["uti_nom"] . " " . $u["uti_prenom"] . "</option>");
																}
															} ?>
			                                            </select>														
			                                        </div>												
													<div class="section" id="cont_profil_1" <?php if(empty($d_grille["cgr_profil_1"])) echo("style='display:none;'"); ?>  >
														<select name="profil_1" id="profil_1" class="select2">
			                                                <option value="0" selected="selected">Validant 1...</option>
												<?php		foreach($profils as $p){
																if($d_grille["cgr_profil_1"]==$p["pro_id"]){
																	echo("<option value='" . $p["pro_id"] . "' selected >" . $p["pro_libelle"] . "</option>");
																}else{
																	echo("<option value='" . $p["pro_id"] . "' >" . $p["pro_libelle"] . "</option>");
																}
															} ?>
			                                            </select>	
													</div>
													<div class="section" id="cont_droit_1" <?php if(empty($d_grille["cgr_droit_1"])) echo("style='display:none;'"); ?>  >
														<select name="droit_1" id="droit_1" class="select2">
			                                                <option value="0" selected="selected">Droit ...</option>
												<?php		foreach($droits as $d){
																if($d_grille["cgr_droit_1"]==$d["drt_id"]){
																	echo("<option value='" . $d["drt_id"] . "' selected >" . $d["drt_nom"] . "</option>");
																}else{
																	echo("<option value='" . $d["drt_id"] . "' >" . $d["drt_nom"] . "</option>");
																}
															} ?>
			                                            </select>	
													</div>
			                                    </div>
												
			                                    <div class="col-md-3">
			                                    	<div class="field select mb10">
			                                            <select name="type_2" id="type_2">
			                                                <option value="1" <?php if(!empty($d_grille["cgr_profil_2"])) echo("selected"); ?> >Profil</option>
			                                                <option value="2" <?php if(empty($d_grille["cgr_profil_2"]) AND empty($d_grille["cgr_droit_2"]) ) echo("selected"); ?> >Utilisateur</option>
															<option value="4" <?php if(!empty($d_grille["cgr_droit_2"])) echo("selected"); ?> >Droit</option>
			                                            </select>
			                                        	<i class="arrow simple"></i>
			                                        </div>
			                                        <div class="section" id="cont_utilisateur_2" <?php if(!empty($d_grille["cgr_profil_2"]) OR !empty($d_grille["cgr_droit_2"])) echo("style='display:none;'"); ?> >
			                                            <select name="utilisateur_2" id="utilisateur_2" class="select2">
			                                                <option value="0" selected="selected">Validant 2...</option>
												<?php		foreach($utilisateurs as $u){
																if($d_grille["cgr_utilisateur_2"]==$u["uti_id"]){
																	echo("<option value='" . $u["uti_id"] . "' selected >" . $u["uti_nom"] . " " . $u["uti_prenom"] . "</option>");
																}else{
																	echo("<option value='" . $u["uti_id"] . "' >" . $u["uti_nom"] . " " . $u["uti_prenom"] . "</option>");
																}
															} ?>
			                                            </select>														
			                                        </div>												
													<div class="section" id="cont_profil_2" <?php if(empty($d_grille["cgr_profil_2"])) echo("style='display:none;'"); ?> >
														<select name="profil_2" id="profil_2" class="select2">
			                                                <option value="0" selected="selected">Validant 2...</option>
												<?php		foreach($profils as $p){
																if($d_grille["cgr_profil_2"]==$p["pro_id"]){
																	echo("<option value='" . $p["pro_id"] . "' selected >" . $p["pro_libelle"] . "</option>");
																}else{
																	echo("<option value='" . $p["pro_id"] . "' >" . $p["pro_libelle"] . "</option>");
																}
															} ?>
			                                            </select>	
													</div>
													<div class="section" id="cont_droit_2" <?php if(empty($d_grille["cgr_droit_2"])) echo("style='display:none;'"); ?>  >
														<select name="droit_2" id="droit_2" class="select2">
			                                                <option value="0" selected="selected">Droit ...</option>
												<?php		foreach($droits as $d){
																if($d_grille["cgr_droit_2"]==$d["drt_id"]){
																	echo("<option value='" . $d["drt_id"] . "' selected >" . $d["drt_nom"] . "</option>");
																}else{
																	echo("<option value='" . $d["drt_id"] . "' >" . $d["drt_nom"] . "</option>");
																}
															} ?>
			                                            </select>	
													</div>

			                                    </div>
			                                    <div class="col-md-3">
			                                    	<div class="field select mb10">
			                                            <select name="type_3" id="type_3">
			                                                <option value="1" <?php if(!empty($d_grille["cgr_profil_3"])) echo("selected"); ?> >Profil</option>
			                                                <option value="2" <?php if(empty($d_grille["cgr_profil_3"]) AND empty($d_grille["cgr_droit_3"]) ) echo("selected"); ?>>Utilisateur</option>
															<option value="4" <?php if(!empty($d_grille["cgr_droit_3"])) echo("selected"); ?> >Droit</option>
			                                            </select>
			                                        	<i class="arrow simple"></i>
			                                        </div>	
			                                        <div class="section" id="cont_utilisateur_3" <?php if(!empty($d_grille["cgr_profil_3"]) OR !empty($d_grille["cgr_droit_3"]) ) echo("style='display:none;'"); ?> >
			                                            <select name="utilisateur_3" id="utilisateur_3" class="select2">
			                                                <option value="0" selected="selected">Validant 3...</option>
												<?php		foreach($utilisateurs as $u){
																if($d_grille["cgr_utilisateur_3"]==$u["uti_id"]){
																	echo("<option value='" . $u["uti_id"] . "' selected >" . $u["uti_nom"] . " " . $u["uti_prenom"] . "</option>");
																}else{
																	echo("<option value='" . $u["uti_id"] . "' >" . $u["uti_nom"] . " " . $u["uti_prenom"] . "</option>");
																}
															} ?>
			                                            </select>														
			                                        </div>												
													<div class="section" id="cont_profil_3" <?php if(empty($d_grille["cgr_profil_3"])) echo("style='display:none;'"); ?> >
														<select name="profil_3" id="profil_3" class="select2">
			                                                <option value="0" selected="selected">Validant 3...</option>
												<?php		foreach($profils as $p){
																if($d_grille["cgr_profil_3"]==$p["pro_id"]){
																	echo("<option value='" . $p["pro_id"] . "' selected >" . $p["pro_libelle"] . "</option>");
																}else{
																	echo("<option value='" . $p["pro_id"] . "' >" . $p["pro_libelle"] . "</option>");
																}
															} ?>
			                                            </select>	
													</div>
													<div class="section" id="cont_droit_3" <?php if(empty($d_grille["cgr_droit_3"])) echo("style='display:none;'"); ?>  >
														<select name="droit_3" id="droit_3" class="select2">
			                                                <option value="0" selected="selected">Droit ...</option>
												<?php		foreach($droits as $d){
																if($d_grille["cgr_droit_3"]==$d["drt_id"]){
																	echo("<option value='" . $d["drt_id"] . "' selected >" . $d["drt_nom"] . "</option>");
																}else{
																	echo("<option value='" . $d["drt_id"] . "' >" . $d["drt_nom"] . "</option>");
																}
															} ?>
			                                            </select>	
													</div>
			                                    </div>
										<?php	if (empty($grille) OR $d_grille["cgr_famille"]!=2) { ?>
													<div class="col-md-3" id="validation_4" >
														<div class="field select mb10">
															<select name="type_4" id="type_4">
																<option value="1" <?php if(!empty($d_grille["cgr_profil_4"])) echo("selected"); ?> >Profil</option>
																<option value="2" <?php if(empty($d_grille["cgr_profil_4"])) echo("selected"); ?>>Utilisateur</option>
															</select>
															<i class="arrow simple"></i>
														</div>	
														<div class="section" id="cont_utilisateur_4" <?php if(!empty($d_grille["cgr_profil_4"]) OR !empty($d_grille["cgr_droit_4"])) echo("style='display:none;'"); ?> >
															<select name="utilisateur_4" id="utilisateur_4" class="select2">
																<option value="0" selected="selected">Validant 4...</option>
													<?php		foreach($utilisateurs as $u){
																	if($d_grille["cgr_utilisateur_4"]==$u["uti_id"]){
																		echo("<option value='" . $u["uti_id"] . "' selected >" . $u["uti_nom"] . " " . $u["uti_prenom"] . "</option>");
																	}else{
																		echo("<option value='" . $u["uti_id"] . "' >" . $u["uti_nom"] . " " . $u["uti_prenom"] . "</option>");
																	}
																} ?>
															</select>														
														</div>												
														<div class="section" id="cont_profil_4" <?php if(empty($d_grille["cgr_profil_4"])) echo("style='display:none;'"); ?>  >
															<select name="profil_4" id="profil_4" class="select2">
																<option value="0" selected="selected">Validant 4...</option>
													<?php		foreach($profils as $p){
																	if($d_grille["cgr_profil_4"]==$p["pro_id"]){
																		echo("<option value='" . $p["pro_id"] . "' selected >" . $p["pro_libelle"] . "</option>");
																	}else{
																		echo("<option value='" . $p["pro_id"] . "' >" . $p["pro_libelle"] . "</option>");
																	}
																} ?>
															</select>	
														</div>
													</div>
										<?php	} ?>
											</div>

										</div>
										
									</div>
										
								</div>
							</div>
							
						</div>

					</section>
					<!-- End: Content -->
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix"  >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<?php if(!empty($d_grille['cgr_famille'])){ ?>
							<a href="validations_bc.php?famille=<?=$d_grille["cgr_famille"]?>" class="btn btn-default btn-sm" >
								<i class="fa fa-long-arrow-left"></i>
								Retour
							</a>
						<?php }else{ ?>
							<a href="validations_bc.php" class="btn btn-default btn-sm" >
								<i class="fa fa-long-arrow-left"></i>
								Retour
							</a>
						<?php } ?>
					</div>
					<div class="col-xs-6 footer-middle" >
					</div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm" >
							<i class="fa fa-add" ></i>
							Enregistrer
						</button>
					</div>
				</div>
			</footer>
	</form>
									
		
	<?php
		include "includes/footer_script.inc.php"; ?>	                         

		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$("#type_1").change(function() {
					actualiser_liste($("#type_1").val(),1);
				});
				$("#type_2").change(function() {
					actualiser_liste($("#type_2").val(), 2);
				});
				$("#type_3").change(function() {
					actualiser_liste($("#type_3").val(), 3);
				});
				$("#type_4").change(function() {
					actualiser_liste($("#type_4").val(), 4);
				});

				$("#cgr_famille").change(function() {
					if($(this).val()!=2){
						$("#validation_4").show();
					}else{
						$("#validation_4").hide();
					}
				});
			});	

			// fonctions
			function actualiser_liste(type, numero){
				
				// type == 1 => profil
				// type == 2 => utilisateur
				// type == 3 => responsable du DO
				
				switch(type) {
					case "2" :
						$("#cont_utilisateur_" + numero).show();
						$("#profil_" + numero).val(0);
						$("#cont_profil_" + numero).hide();
						$("#droit_" + numero).val(0);
						$("#cont_droit_" + numero).hide();
						break;
					case "1":
						$("#cont_profil_" + numero).show();
						$("#utilisateur_" + numero).val(0);
						$("#cont_utilisateur_" + numero).hide();
						$("#droit_" + numero).val(0);
						$("#cont_droit_" + numero).hide();
						break;
					case "4" :
						$("#utilisateur_" + numero).val(0);
						$("#cont_utilisateur_" + numero).hide();
						$("#profil_" + numero).val(0);
						$("#cont_profil_" + numero).hide();
						$("#cont_droit_" + numero).show();
						break;
					default:
						$("#utilisateur_" + numero).val(0);
						$("#cont_utilisateur_" + numero).hide();
						$("#profil_" + numero).val(0);
						$("#cont_profil_" + numero).hide();
						$("#droit_" + numero).val(0);
						$("#cont_droit_" + numero).hide();
				} 
			}
		</script>
	</body>
</html>
