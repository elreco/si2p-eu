<?php


include "includes/controle_acces.inc.php";
include('includes/connexion.php');

include('modeles/mod_parametre.php');
include('modeles/mod_get_correspondance.php');
include('modeles/mod_get_contact.php');
include('modeles/mod_get_contacts.php');

$client=0;
if(!empty($_GET['client'])){
	$client=intval($_GET['client']);
}

$correspondance=0;
if(!empty($_GET['id'])){
	$correspondance=intval($_GET['id']);
}
if($client==0){
	echo("Erreur paramètre!");
	die();
}

$sql="SELECT cli_nom,cli_categorie,cli_code FROM Clients WHERE cli_id=" . $client . ";";
$req=$Conn->query($sql);
$d_client =$req->fetch();

// contact connu
$d_contacts = get_contacts(1,$client,0,0);

// fonction
$sql="SELECT cfo_id,cfo_libelle FROM Contacts_Fonctions ORDER BY cfo_libelle;";
$req=$Conn->query($sql);
$d_contact_fonctions =$req->fetchAll();

if($correspondance>0){
	
	$d_correspondance = get_correspondance($correspondance);
	
	$date=date_create_from_format('Y-m-d',$d_correspondance['cor_date']);
	if(!is_bool($date)){
		$d_correspondance["cor_date_aff"]=$date->format("d/m/Y");
	}
	$rappeler_le=date_create_from_format('Y-m-d',$d_correspondance['cor_rappeler_le']);
	if(!is_bool($rappeler_le)){
		$d_correspondance["cor_rappel_aff"]=$rappeler_le->format("d/m/Y");
	}else{
		$d_correspondance["cor_rappel_aff"]="";
	}
	if(!empty($d_correspondance['cor_contact'])){
		$d_contact = get_contact($d_correspondance['cor_contact']);
		if(!empty($d_contact)){
			$d_correspondance["cor_contact_nom"]=$d_contact["con_nom"];
			$d_correspondance["cor_contact_prenom"]=$d_contact["con_prenom"];	
			$d_correspondance["cor_contact_tel"]=$d_contact["con_tel"];
			$d_correspondance["cor_contact_fonction"]=$d_contact["con_fonction"];	
			
		}
	}
	if(empty($d_correspondance["cor_type"])){
		$d_correspondance["cor_type"]=1;
	}
}else{
	$d_correspondance=array(
		"cor_date_aff" => date("d/m/Y"),
		"cor_contact" =>0,
		"cor_contact_fonction" =>0,
		"cor_contact_nom" =>"",
		"cor_contact_prenom" =>"",
		"cor_contact_tel" =>"",
		"cor_commentaire" => "",
		"cor_rappel_aff" => "",
		"cor_type" => 1,
		"cor_raison" => 0,
		"cor_raison_info" => ""
	);
}

// gestion du retour
if(isset($_GET["tab5"])){
	$_SESSION['retourCorresp'].="&tab5";
}

// TYPES DE CORRESPONDANCES

$sql="SELECT cty_id,cty_libelle FROM Correspondances_Types ORDER BY cty_libelle;";
$req=$Conn->query($sql);
$d_cor_types=$req->fetchAll();

$sql="SELECT cra_id,cra_libelle FROM Correspondances_Raisons ORDER BY cra_libelle;";
$req=$Conn->query($sql);
$d_cor_raisons=$req->fetchAll();

/*
echo("<pre>");
	print_r($d_correspondance);
echo("</pre>");
die();*/
?>
<!DOCTYPE html>
<html>  
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion - <?= $d_client['cli_nom'] ?></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
		<link href="vendor/plugins/summernote/summernote-bs3.css" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />


		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		
		<form id="form_corresp" action="client_correspondance_enr.php" method="post" class="admin-form form-inline form-inline-grid" >
			<div>
				<input type="hidden" name="client" value="<?=$client?>" />
				<input type="hidden" name="categorie" value="<?=$d_client['cli_categorie']?>" />
				<input type="hidden" name="correspondance" value="<?=$correspondance?>" />
			</div>
			
			<div id="main">
		<?php 	include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" class="">
					<section id="content" class="">
						
						<div class="row">
							<div class="col-md-10 col-md-offset-1">							
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											
											<div class="content-header">
										<?php	if($correspondance==0){ ?>
													<h2><b class="text-primary"><?=$d_client["cli_code"]?></b> - Nouvelle <b class="text-primary">correspondance</b></h2>
										<?php	}else{ ?>
													<h2><b class="text-primary"><?=$d_client["cli_code"]?></b> - Editer une <b class="text-primary">correspondance</b></h2>
										<?php	} ?>
												
											</div>
										
											<div class="row">
												<div class="col-md-4">														
													<label for="cor_date"> Date de la correspondance :</label>
													<input type="text" name="cor_date" id="cor_date" class="gui-input datepicker" placeholder="Date de la correspondance" required value="<?=$d_correspondance["cor_date_aff"]?>" />  																												
													<small class="text-danger texte_required" id="cor_date_required">												
														Merci de compléter ce champ.
													</small>
												</div>
												<div class="col-md-4">														
													<label class="field">Contact :</label>
													<select name="cor_contact" id="cor_contact" class="form-control" >
														<option value="0" selected>Contact...</option>
												<?php 	if($d_client['cli_categorie'] != 3){
															echo("<option value='nouveau' >Ajouter un contact...</option>");
														}
														if(!empty($d_contacts)){
															foreach($d_contacts as $dc){
																if($dc["con_id"]==$d_correspondance["cor_contact"]){
																	echo("<option value='" . $dc["con_id"] . "' selected >" . $dc["con_nom"] . " " . $dc["con_prenom"] . "</option>");
																}else{
																	echo("<option value='" . $dc["con_id"] . "' >" . $dc["con_nom"] . " " . $dc["con_prenom"] . "</option>");
																}
															}	
														} ?>
													</select>												
												</div>	
											</div> 
										
											
											<div class="row mt15" id="contact" >
												<div class="col-md-4">													
													<label class="field">Nom :</label>
													<input type="text" name="cor_contact_nom" id="cor_contact_nom" class="gui-input nom" placeholder="Nom" value="<?= $d_correspondance['cor_contact_nom'] ?>" <?php if($d_correspondance['cor_contact']>0) echo("readonly") ?> />
												</div>
												<div class="col-md-4">													
													<label class="field">Prénom :</label>
													<input type="text" name="cor_contact_prenom" id="cor_contact_prenom" class="gui-input prenom" placeholder="Prénom" value="<?= $d_correspondance['cor_contact_prenom'] ?>" <?php if($d_correspondance['cor_contact']>0) echo("readonly") ?> />
												</div>
												<div class="col-md-4">												
													<label class="field">Téléphone :</label>
													<input type="tel" name="cor_contact_tel" id="cor_contact_tel" class="gui-input telephone" placeholder="Téléphone" value="<?= $d_correspondance['cor_contact_tel'] ?>" />
												</div>												
											</div>
											
											<!-- NOUVEAU CONTACT -->											
											<div id="nouv-contact" style="display:none;" >
											
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Nouveau contact</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<label class="field">Civilité :</label>
														<select name="con_titre" id="con_titre" class="form-control">
											<?php 			foreach($base_civilite as $b => $c){
																if($b>0){
																	echo("<option value='" . $b . "' >" . $c . "</option>");
																}
															} ?>
														</select>
													</div>												
													<div class="col-md-4">														
														<label class="field">Nom :</label>
														<input type="text" name="con_nom" id="con_nom" class="gui-input nom" placeholder="Nom" value="" />																											
													</div>
													<div class="col-md-4">															
														<label class="field">Prénom :</label>
														<input type="text" name="con_prenom" id="con_prenom" class="gui-input prenom" placeholder="Prénom" value="" />
													</div>
												</div>															
												<div class="row mt15">
													<div class="col-md-4">	
														<label class="field">Fonction :</label>
														<select name="con_fonction" class="form-control" id="con_fonction" >
															<option value="0">Sélectionner une fonction...</option>
												<?php 		if(!empty($d_contact_fonctions)){
																foreach($d_contact_fonctions as $dcf){
																	echo("<option value='" . $dcf["cfo_id"] . "' >" . $dcf["cfo_libelle"] . "</option>");
																}
															} ?>
															<option value="autre">Autre...</option>
														</select>														
													</div>
													<div class="col-md-4" id="fonction_nom" style="display:none;" >																											
														<label for="con_fonction_nom" >Autre fonction :</label>
														<input type="text" name="con_fonction_nom" id="con_fonction_nom" class="gui-input" placeholder="Autre fonction">
													</div>
												</div>
												<div class="row mt15">
													<div class="col-md-4">												
														<label class="field">Téléphone :</label>
														<input type="text" name="con_tel" class="gui-input telephone" id="con_tel" placeholder="Tél" />												
													</div>												
													<div class="col-md-4">
														<label class="field">Portable :</label>
														<input type="text" name="con_portable" class="gui-input telephone" id="con_portable" placeholder="Portable" />
													</div>
													<div class="col-md-4">
														<label class="field">Fax :</label>
														<input type="text" name="con_fax" class="gui-input telephone" id="con_fax" placeholder="Fax" />								
													</div>
												</div>
												<div class="row mt15">
													<div class="col-md-12">
														<label class="field">Mail :</label>
														<input type="email" name="con_mail" class="gui-input" id="con_mail" placeholder="Email" />
													</div>
												</div>
											</div>
											<!-- FIN BLOC CONTACT -->
											
											<div class="row mt15" >
												<div class="col-md-4">														
													<label class="field">Méthode de contact :</label>
													<select name="cor_type" id="cor_type" class="select2" >							
												<?php 	if(!empty($d_cor_types)){
															foreach($d_cor_types as $cor_type){
																if($cor_type["cty_id"]==$d_correspondance["cor_type"]){
																	echo("<option value='" . $cor_type["cty_id"] . "' selected >" . $cor_type["cty_libelle"] . "</option>");
																}else{
																	echo("<option value='" . $cor_type["cty_id"] . "' >" . $cor_type["cty_libelle"] . "</option>");
																}
															}	
														} ?>
													</select>
													<small class="text-danger texte_required" id="cor_type_required">												
														Merci de compléter ce champ.
													</small>
												</div>
												
												<div class="col-md-4">														
													<label class="field">Raison de la correspondance</label>
													<select name="cor_raison" id="cor_raison" class="form-control" >
														<option value="0" >Raison ...</option>
												<?php 	if(!empty($d_cor_raisons)){
															foreach($d_cor_raisons as $cor_raison){
																if($cor_raison["cra_id"]==$d_correspondance["cor_raison"]){
																	echo("<option value='" . $cor_raison["cra_id"] . "' selected >" . $cor_raison["cra_libelle"] . "</option>");
																}else{
																	echo("<option value='" . $cor_raison["cra_id"] . "' >" . $cor_raison["cra_libelle"] . "</option>");
																}
															}	
														} ?>
														<option value="-1" <?php if($d_correspondance["cor_raison"]==-1) echo("selected") ?> >Autre</option>
													</select>
												</div>
												<div class="col-md-4" id="bloc_raison_info" <?php if($d_correspondance["cor_raison"]!=-1) echo("style='display:none;'"); ?> >
													<label for="cor_raison_info" > Autre (précisez votre choix) :</label>
													<input type="text" name="cor_raison_info" id="cor_raison_info" class="gui-input" placeholder="Précisez votre choix" value="<?=$d_correspondance['cor_raison_info']?>" />
												</div>
												
											</div>

											<div class="row mt15" >
												<div class="col-md-offset-3 col-md-6">
													<label for="cor_commentaire">Commentaire <strong>(obligatoire)</strong></label>
													<textarea class="summernote" id="cor_commentaire" placeholder="Commentaire..." name="cor_commentaire" required ><?=$d_correspondance['cor_commentaire']?></textarea>
												
													<small class="text-danger texte_required" id="cor_commentaire_required">												
														Merci de compléter ce champ.
													</small>
												</div>
											</div>
											<div class="row mt15" >
												<div class="col-md-4">
													<label class="field"> Rappeler le :</label>
													<input type="text" name="cor_rappeler_le" class="gui-input datepicker" placeholder="Rappeler le" value="<?=$d_correspondance['cor_rappel_aff']?>" />
												</div>
									<?php		if(!empty($correspondance)){ ?>											
													<div class="col-md-4 text-center pt25">													
														<label class="option option-dark">
															<input type="checkbox" name="cor_rappel" value="cor_rappel" <?php if($d_correspondance['cor_rappel'] == 1) echo("checked");?> >
															<span class="checkbox"></span>Rappelé
														</label>
													</div>											
										<?php 	}else{ ?>
													<div class="col-md-4">&nbsp;</div>			
										<?php	} ?>
												<div class="col-md-4 pt25 text-center">													
													<label class="option option-dark">
														<input type="checkbox" name="cor_devis" value="cor_devis" />
														<span class="checkbox"></span>Créer un devis
													</label>
												</div>	
											</div>
															
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="<?=$_SESSION['retourCorresp']?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right">
						<button type="button" id="btn_submit" class="btn btn-success btn-sm">
							<i class='fa fa-floppy-o'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php
		include "includes/footer_script.inc.php"; ?>   


		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery-ui-datepicker.min.js"></script>
		<script src="vendor/plugins/summernote/summernote.min.js"></script>
		<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

		<script src="assets/js/custom.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<script src="assets/js/responsive-tabs.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function(){	
			
				$("#cor_contact").change(function(){
					if($(this).val()=="nouveau"){
						$("#contact").hide();
						$("#nouv-contact").show();
					}else{
						afficher_contact($(this).val());
					}
				});
				
				$("#con_fonction").change(function(){
					if($(this).val()=="autre"){
						$("#fonction_nom").show();
					}else{
						$("#fonction_nom").hide();
					}
				});
				
				$("#btn_submit").click(function(){
					if(valider_form("#form_corresp")){
						
						$("#form_corresp").submit();					
					}
					
				});
				
				$("#cor_raison").change(function(){
					if($(this).val()==-1){
						$("#bloc_raison_info").show();
					}else{
						$("#sco_raison_info").val("");
						$("#bloc_raison_info").hide();
					}
				});
			
			
			});
			
			var tab_contacts=new Array();
			tab_contacts[0]=new Array(2);
			tab_contacts[0][0]="";
			tab_contacts[0][1]="";
			tab_contacts[0][2]="";
	<?php	if(!empty($d_contacts)){
				foreach($d_contacts as $cj){ ?>
					tab_contacts[<?=$cj["con_id"]?>]=new Array(2);
					tab_contacts[<?=$cj["con_id"]?>][0]="<?=$cj["con_nom"]?>";
					tab_contacts[<?=$cj["con_id"]?>][1]="<?=$cj["con_prenom"]?>";
					tab_contacts[<?=$cj["con_id"]?>][2]="<?=$cj["con_tel"]?>";
	<?php		}
			} ?>	
			function afficher_contact(contact){
				if(contact>0){
					$("#cor_contact_nom").val(tab_contacts[contact][0]);
					$("#cor_contact_prenom").val(tab_contacts[contact][1]);
					$("#cor_contact_tel").val(tab_contacts[contact][2]);
					$("#cor_contact_nom").prop("readonly",true);
					$("#cor_contact_prenom").prop("readonly",true);
					
				}else{
					$("#cor_contact_nom").val("");
					$("#cor_contact_prenom").val("");
					$("#cor_contact_tel").val("");
					$("#cor_contact_nom").prop("readonly",false);
					$("#cor_contact_prenom").prop("readonly",false);
				}
				$("#contact").show();
				$("#nouv-contact").hide();
			}

		</script>
	</body>
</html>
