<?php
include "includes/controle_acces.inc.php";

include('includes/connexion.php');

include("modeles/mod_upload.php");


$erreur="";
$warning="";

if(!empty($_POST)){
	
	$soc_id=0;
	if(isset($_POST['soc_id'])){
		if(intval($_POST['soc_id'])){
			$soc_id=intval($_POST['soc_id']);
		}
	}
	
	$soc_responsable=0;
	if(isset($_POST['soc_responsable'])){
		if(!empty($_POST['soc_responsable'])){
			$soc_responsable=$_POST['soc_responsable'];
		};
	}
	$soc_tva_exo=0;
	if(isset($_POST['soc_tva_exo'])){
		if(!empty($_POST['soc_tva_exo'])){
			$soc_tva_exo=1;
		};
	}
	$soc_compta=0;
	if(isset($_POST['soc_compta'])){
		if(!empty($_POST['soc_compta'])){
			$soc_compta=$_POST['soc_compta'];
		};
	}
	
	
	$soc_compta_age=$_POST['soc_compta_age'];
	$soc_interco_intra=0;
	if(isset($_POST['soc_interco_intra'])){
		if(!empty($_POST['soc_interco_intra'])){
			$soc_interco_intra=$_POST['soc_interco_intra'];
		};
	}
	$soc_interco_inter=0;
	if(isset($_POST['soc_interco_inter'])){
		if(!empty($_POST['soc_interco_inter'])){
			$soc_interco_inter=$_POST['soc_interco_inter'];
		};
	}
	$soc_agence=0;
	if(isset($_POST['soc_agence'])){
		if(!empty($_POST['soc_agence'])){
			$soc_agence=$_POST['soc_agence'];
		};
	}
	
	$soc_archive=0;
	if(isset($_POST['soc_archive'])){
		if(!empty($_POST['soc_archive'])){
			$soc_archive=1;
		};
	}
	$soc_region_nom="";
	if(isset($_POST['soc_region_nom'])){
		if(!empty($_POST['soc_region_nom'])){
			$soc_region_nom=$_POST['soc_region_nom'];
		};
	}

    if($soc_id>0) {
		
		$sql="UPDATE Societes SET soc_h_deb_matin=:soc_h_deb_matin, soc_h_fin_matin=:soc_h_fin_matin, soc_h_deb_am=:soc_h_deb_am, soc_h_fin_am=:soc_h_fin_am";
		$sql=$sql . ",soc_tva_type=:soc_tva_type, soc_nom=:soc_nom, soc_code=:soc_code, soc_ad1=:soc_ad1, soc_ad2=:soc_ad2, soc_ad3=:soc_ad3, soc_cp=:soc_cp, soc_ville=:soc_ville";
      	$sql=$sql . ",soc_tel=:soc_tel, soc_fax=:soc_fax, soc_mail=:soc_mail, soc_site=:soc_site, soc_responsable=:soc_responsable, soc_siret=:soc_siret, soc_siren=:soc_siren";
		$sql=$sql . ",soc_tva=:soc_tva, soc_num_existence=:soc_num_existence, soc_ape=:soc_ape, soc_rcs=:soc_rcs, soc_capital=:soc_capital, soc_type=:soc_type, soc_compta=:soc_compta";
		$sql=$sql . ",soc_compta_age=:soc_compta_age, soc_compta_tel=:soc_compta_tel, soc_compta_fax=:soc_compta_fax, soc_compta_mail=:soc_compta_mail, soc_interco_intra=:soc_interco_intra, soc_tva_exo=:soc_tva_exo ";
		$sql=$sql . ",soc_interco_inter=:soc_interco_inter";
		$sql=$sql . ",soc_agence=:soc_agence, soc_archive=:soc_archive, soc_region_nom=:soc_region_nom";
		$sql=$sql . " WHERE soc_id=:soc_id";   
		 
        $req = $Conn->prepare($sql);	
		$req->bindParam("soc_id",$soc_id);
		$req->bindParam("soc_h_deb_matin",$_POST['soc_h_deb_matin']);
		$req->bindParam("soc_h_fin_matin",$_POST['soc_h_fin_matin']);
		$req->bindParam("soc_h_deb_am",$_POST['soc_h_deb_am']);
		$req->bindParam("soc_h_fin_am",$_POST['soc_h_fin_am']);
		$req->bindParam("soc_tva_type",$_POST['soc_tva_type']);
		$req->bindParam("soc_nom",$_POST['soc_nom']);
		$req->bindParam("soc_code",$_POST['soc_code']);
		$req->bindParam("soc_ad1",$_POST['soc_ad1']);
		$req->bindParam("soc_ad2",$_POST['soc_ad2']);
		$req->bindParam("soc_ad3",$_POST['soc_ad3']);
		$req->bindParam("soc_cp",$_POST['soc_cp']);
		$req->bindParam("soc_region_nom",$_POST['soc_region_nom']);
		$req->bindParam("soc_ville",$_POST['soc_ville']);
		$req->bindParam("soc_tel",$_POST['soc_tel']);
		$req->bindParam("soc_fax",$_POST['soc_fax']);
		$req->bindParam("soc_mail",$_POST['soc_mail']);
		$req->bindParam("soc_site",$_POST['soc_site']);
		$req->bindParam("soc_responsable",$soc_responsable);
		$req->bindParam("soc_siret",$_POST['soc_siret']);
		$req->bindParam("soc_siren",$_POST['soc_siren']);
		$req->bindParam("soc_tva",$_POST['soc_tva']);
		$req->bindParam("soc_num_existence",$_POST['soc_num_existence']);
		$req->bindParam("soc_ape",$_POST['soc_ape']);
		$req->bindParam("soc_rcs",$_POST['soc_rcs']);
		$req->bindParam("soc_capital",$_POST['soc_capital']);
		$req->bindParam("soc_type",$_POST['soc_type']);
		$req->bindParam("soc_compta",$soc_compta);
		$req->bindParam("soc_compta_age",$soc_compta_age);
		$req->bindParam("soc_compta_tel",$_POST['soc_compta_tel']);
		$req->bindParam("soc_compta_fax",$_POST['soc_compta_fax']);
		$req->bindParam("soc_compta_mail",$_POST['soc_compta_mail']);
		$req->bindParam("soc_interco_intra",$soc_interco_intra);
		$req->bindParam("soc_interco_inter",$soc_interco_inter);
		$req->bindParam("soc_agence",$soc_agence);
		$req->bindParam("soc_archive",$soc_archive);
		$req->bindParam("soc_tva_exo",$soc_tva_exo);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur=$e->getMessage();
		}
	
		
    } else {
		
        $req = $Conn->prepare("INSERT INTO societes (soc_region_nom,soc_h_deb_matin,soc_h_fin_matin,soc_h_deb_am,soc_h_fin_am,soc_tva_type,soc_nom, soc_code, soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville,soc_tel,soc_fax,
      soc_mail,soc_site,soc_responsable,soc_siret,soc_siren,soc_tva,soc_num_existence,soc_ape,soc_rcs,soc_capital,soc_type,soc_compta,soc_compta_age,soc_compta_tel,
	  soc_compta_fax,soc_compta_mail,soc_interco_intra,soc_interco_inter,soc_agence,soc_archive, soc_tva_exo)

      VALUES ('" . $_POST['soc_region_nom'] . "', '" . $_POST['soc_h_deb_matin'] . "','" . $_POST['soc_h_fin_matin'] . "','" . $_POST['soc_h_deb_am'] . "','" . $_POST['soc_h_fin_am'] . "','" . $_POST['soc_tva_type'] . "',
        '" . $_POST['soc_nom'] . "',
        '" . $_POST['soc_code'] . "', '" . $_POST['soc_ad1'] . "', '" . $_POST['soc_ad2'] . "', '" . $_POST['soc_ad3'] . "', '" . $_POST['soc_cp'] . "', '" . $_POST['soc_ville'] . "',
        '" . $_POST['soc_tel'] . "', '" . $_POST['soc_fax'] . "',
        '" . $_POST['soc_mail'] . "', '" . $_POST['soc_site'] . "',
        '" . $soc_responsable . "', '" . $_POST['soc_siret'] . "','" . $_POST['soc_siren'] . "',
        '" . $_POST['soc_tva'] . "', '" . $_POST['soc_num_existence'] . "', '" . $_POST['soc_ape'] . "',
        '" . $_POST['soc_rcs'] . "', '" . $_POST['soc_capital'] . "', '" . $_POST['soc_type'] . "','" . $soc_compta . "'," . $soc_compta_age . ",'" . $_POST['soc_compta_tel'] . "',
        '" . $_POST['soc_compta_fax'] . "', '" . $_POST['soc_compta_mail'] . "', " . $soc_interco_intra . "," . $soc_interco_inter . "," . $soc_agence . "
		," . $soc_archive . ", " . $soc_tva_exo . ")");
		try{
			$req->execute();
			$soc_id = $Conn->lastInsertId();
		}Catch(Exception $e){
			$erreur=$e->getMessage();
			
		}
   }
   
   // UPLOAD DES LOGOS
   
   if(empty($erreur)){

		$logo1 = null;
		$logo2 = null;
		$qualite1 = null;
		$qualite2 = null;
	
		if (!empty($_FILES['soc_logo_1']['name'])) {

			$er_logo_1 = upload('soc_logo_1', "societes/logos/","soc_logo_1_" . $soc_id . "_" . date('m-d-Y'), 0, array(), 1);
			if($er_logo_1==0){
				$logo1 = "soc_logo_1_" . $soc_id . "_" . date('m-d-Y') . "." . pathinfo($_FILES['soc_logo_1']['name'], PATHINFO_EXTENSION);
			}else{
				$warning="Certains logos n'ont pas été chargés.";
			}
		}

		if (!empty($_FILES['soc_logo_2']['name'])) {
			$er_logo_2 = upload('soc_logo_2', "societes/logos/","soc_logo_2_" . $soc_id . "_" . date('m-d-Y'), 0, array(), 1);
			if($er_logo_2==0){
				$logo2 = "soc_logo_2_" . $soc_id . "_" . date('m-d-Y') . "." . pathinfo($_FILES['soc_logo_2']['name'], PATHINFO_EXTENSION);
			}else{
				$warning="Certains logos n'ont pas été chargés.";
			}
		}
		if (!empty($_FILES['soc_qualite_1']['name'])) {
			$er_qualite_1 = upload('soc_qualite_1', "societes/logos/","soc_qualite_1_" . $soc_id . "_" . date('m-d-Y'), 0, array(), 1);
			if($er_qualite_1==0){
				$qualite1 = "soc_qualite_1_" . $soc_id . "_" . date('m-d-Y') . "." . pathinfo($_FILES['soc_qualite_1']['name'], PATHINFO_EXTENSION);
			}else{
				$warning="Certains logos n'ont pas été chargés.";
			}
		}
		if (!empty($_FILES['soc_qualite_2']['name'])) {
			$er_qualite_2 = upload('soc_qualite_2', "societes/logos/","soc_qualite_2_" . $soc_id . "_" . date('m-d-Y'), 0, array(), 1);
			if($er_qualite_2==0){
				$qualite2 = "soc_qualite_2_" . $soc_id . "_" . date('m-d-Y') . "." . pathinfo($_FILES['soc_qualite_2']['name'], PATHINFO_EXTENSION);
			}else{
				$warning="Certains logos n'ont pas été chargés.";
			}
		}
		
		if(!empty($logo1) OR !empty($logo2) OR !empty($qualite1) OR !empty($qualite2)){
			
			$sql="UPDATE societes SET";
			if(!empty($logo1)){
				$sql.=" soc_logo_1 ='" . $logo1 . "',";
			}
			if(!empty($logo2)){
				$sql.=" soc_logo_2 ='" . $logo2 . "',";
			}
			if(!empty($qualite1)){
				$sql.=" soc_qualite_1 ='" . $qualite1 . "',";
			}
			if(!empty($qualite2)){
				$sql.=" soc_qualite_2 ='" . $qualite2 . "',";
			}
			$sql=substr($sql,0,-1);
			$sql.=" WHERE soc_id=" . $soc_id . ";";
			try{
				$req = $Conn->query($sql);
			}Catch(Exception $e){
				$erreur=$sql;
				$warning.="Certains logos n'ont pas été chargés.";
			}
		}
   }
   
}else{
	$erreur="Formulaire incomplet";
}

if(!empty($erreur)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur 
	);
	header('Location: societe.php?id=' . $soc_id);
}else{
	if(!empty($warning)){
		$_SESSION['message'][] = array(
			"titre" => "Attention",
			"type" => "warning",
			"message" => $warning
		);
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Enregistrement terminé",
			"type" => "success",
			"message" => "Le formulaire a bien été enregistré." 
		);
	}
	header('Location: societe_liste.php');
}
 ?>
