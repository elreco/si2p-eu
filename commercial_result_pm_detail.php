<?php 

// AFFICHE LA LISTE DES FACTURES DEPUIS LE TABLEAU RESULTAT

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_parametre.php');

// DONNEE UTILE AU PROGRAMME

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	} 
}

// PARAMETRE GET

//conso
$agence=0;
if(isset($_GET["agence"])){
	if(!empty($_GET["agence"])){
		$agence=intval($_GET["agence"]);
	}
}
$com_type=0;
if(isset($_GET["com_type"])){
	if(!empty($_GET["com_type"])){
		$com_type=intval($_GET["com_type"]);
	}
}
$cli_type=0;
if(isset($_GET["cli_type"])){
	if(!empty($_GET["cli_type"])){
		$cli_type=intval($_GET["cli_type"]);
	}
}
$commercial=0;
if(isset($_GET["commercial"])){
	if(!empty($_GET["commercial"])){
		$commercial=intval($_GET["commercial"]);
	}
}
$pm_type=0;
if(isset($_GET["pm_type"])){
	if(!empty($_GET["pm_type"])){
		$pm_type=intval($_GET["pm_type"]);
	}
}

// periode
$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}
$mois=0;
if(isset($_GET["mois"])){
	if(!empty($_GET["mois"])){
		$mois=intval($_GET["mois"]);
	}
}

// classification
$categorie="";
if(isset($_GET["categorie"])){
	if(!empty($_GET["categorie"])){
		$categorie=$_GET["categorie"];
	}
}
$famille=0;
if(isset($_GET["famille"])){
	if(!empty($_GET["famille"])){
		$famille=intval($_GET["famille"]);
	}
}
$s_famille=0;
if(isset($_GET["s_famille"])){
	if(!empty($_GET["s_famille"])){
		$s_famille=intval($_GET["s_famille"]);
	}
}
$s_s_famille=0;
if(isset($_GET["s_s_famille"])){
	if(!empty($_GET["s_s_famille"])){
		$s_s_famille=intval($_GET["s_s_famille"]);
	}
}

// CONTROLE D'ACCESS

if($_SESSION["acces"]["acc_profil"]==3 AND $commercial==0){
	header("location:deconnect.php");
	die();
}elseif($agence>0){
	if($acc_agence>0 AND $agence!=$acc_agence){
		header("location:deconnect.php");
		die();
	}
}elseif(empty($agence) AND empty($commercial)){
	if(!isset($_SESSION["acces"]["acc_liste_societe"][$acc_societe . "-0"])){
		header("location:deconnect.php");
		die();
	}
}

// DONNEE PARAMETRE
// CATEGORIE
$d_categories=array();
$sql="SELECT pca_id,pca_libelle FROM Produits_Categories ORDER BY pca_id;";
$req=$Conn->query($sql);
$d_result_cat=$req->fetchAll();
if(!empty($d_result_cat)){
	foreach($d_result_cat as $cat){
		$d_categories[$cat["pca_id"]]=$cat["pca_libelle"];
	}	
}
// Famille
$d_familles=array();
$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles  ORDER BY pfa_id;";
$req=$Conn->query($sql);
$d_result_fam=$req->fetchAll();
if(!empty($d_result_fam)){
	foreach($d_result_fam as $fam){
		$d_familles[$fam["pfa_id"]]=$fam["pfa_libelle"];
	}	
}
// Sous-Famille
$d_s_familles=array();
$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles ORDER BY psf_id;";
$req=$Conn->query($sql);
$d_result_s_fam=$req->fetchAll();
if(!empty($d_result_s_fam)){
	foreach($d_result_s_fam as $s_fam){
		$d_s_familles[$s_fam["psf_id"]]=$s_fam["psf_libelle"];
	}	
}
// Sous-Sous-Famille
$d_s_s_familles=array();
$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles ORDER BY pss_id;";
$req=$Conn->query($sql);
$d_result_s_s_fam=$req->fetchAll();
if(!empty($d_result_s_s_fam)){
	foreach($d_result_s_s_fam as $s_s_fam){
		$d_s_s_familles[$s_s_fam["pss_id"]]=$s_s_fam["pss_libelle"];
	}	
}

// Famille a exclure
// exemple si on affiche famille formation, il ne faut pas juste filtrer fli_famille=fomration
// mais il faut exclure les sous-éléments qui disposent de leur propre objectifs
// on part du niv le plus bas et on remonte l'arbo
$mil="";
if($categorie!="total"){
	//categorie=999 on affiche tout
	//categorie=0 est reserve pour autre
	if($s_s_famille>0){
		
		$mil.=" AND act_pro_sous_sous_famille=" . $s_s_famille;
		
	}else{
		
		$sql="SELECT cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille 
		FROM Commerciaux 
		LEFT JOIN Commerciaux_Objectifs ON (Commerciaux.com_id=Commerciaux_Objectifs.cob_commercial)
		WHERE cob_exercice=" . $exercice;
		// critere d'accès
		if($_SESSION['acces']["acc_profil"]==3){
			$sql.=" AND com_ref_1=" . $acc_utilisateur;
		}elseif($acc_agence>0){
			$sql.=" AND com_agence=" . $acc_agence;
		}
		// critère de tri
		if($commercial>0){
			$sql.=" AND com_id=" . $commercial;
		}elseif($com_type>0){
			$sql.=" AND com_type=" . $com_type;
		}elseif($agence>0){
			$sql.=" AND com_agence=" . $agence;
		}
		if($categorie=="conso"){
			
			$sql.=" AND NOT cob_autre";
			
		}else{
			if($s_famille>0){
				$mil.=" AND act_pro_sous_famille=" . $s_famille;
				$sql.=" AND cob_sous_famille=" . $s_famille . " AND cob_sous_sous_famille>0";
			}elseif($famille>0){
				$mil.=" AND act_pro_famille=" . $famille;
				$sql.=" AND cob_famille=" . $famille . " AND cob_sous_famille>0";
			}elseif($categorie>0){
				$mil.=" AND act_pro_categorie=" . $categorie;
				$sql.=" AND cob_categorie=" . $categorie . " AND cob_famille>0";
			}
		}
		$sql.=";";
		$req=$ConnSoc->query($sql);
		$d_exclut=$req->fetchAll();
		if(!empty($d_exclut)){
			foreach($d_exclut as $exclut){
				if($exclut["cob_sous_sous_famille"]>0){
					$mil.=" AND NOT act_pro_sous_sous_famille=" . $exclut["cob_sous_sous_famille"];	
				}elseif($exclut["cob_sous_famille"]>0){
					$mil.=" AND NOT act_pro_sous_famille=" . $exclut["cob_sous_famille"];	
				}elseif($exclut["cob_famille"]>0){
					$mil.=" AND NOT act_pro_famille=" . $exclut["cob_famille"];	
				}elseif($exclut["cob_categorie"]>0){
					$mil.=" AND NOT act_pro_categorie=" . $exclut["cob_categorie"];	
				}
			}
		}
	}
}

	// champ a selectionne
	
	/* 	- la req doit intégrer les actions non facturable donc LEFT JOIN sur Factures est OBLIGATOIRE
		- du coup fac_client peut etre null et comme la stat doit exclure les interco, nous sommes obligés de faire la jointure sur acl_Client
		
		- sauf si Actions.act_date_fin est fiable (pas le cas 20/11/2019), nous sommes obligé de garder la jointure 
			-> donc impossible de calculer nb sta en reel
			-> donc on utilise acl_qte_pm
	*/
	$sql="SELECT act_id,act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,act_date_deb";
	$sql.=",SUM(fli_montant_ht) AS ca";
	$sql.=",int_label_1,int_label_2";
	$sql.=",Plannings.nb_demi_j,Plannings.act_date_fin";
	if($pm_type==2){
		$sql.=",acl_id,acl_for_nb_sta";
	}
	
	$sql.=" FROM Actions";
	$sql.=" INNER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)";
	$sql.=" INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND Actions.act_agence=Clients.cli_agence)";
	$sql.=" INNER JOIN (
		SELECT COUNT(pda_id) AS nb_demi_j, MAX(pda_date) AS act_date_fin,pda_ref_1 FROM Plannings_Dates WHERE pda_type=1 GROUP BY pda_ref_1 
	) AS Plannings ON (Actions.act_id = Plannings.pda_ref_1)";
	
	
	$sql.=" LEFT OUTER JOIN Factures_Lignes ON (Actions_Clients.acl_id=Factures_Lignes.fli_action_client AND fli_action_cli_soc=" . $acc_societe . ")";
	$sql.=" LEFT OUTER JOIN Factures ON (Factures.fac_id=Factures_Lignes.fli_facture)";
	$sql.=" LEFT OUTER JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)";
	$sql.=" LEFT OUTER JOIN Intervenants ON (Intervenants.int_id=Actions.act_intervenant)";
	
	
	$sql.=" WHERE (acl_facture OR acl_non_fac) AND NOT acl_facturation=1 AND NOT acl_ca_nc";
	$sql.=" AND (cli_interco_soc=0 OR ISNULL(cli_interco_soc) )";
	$sql.=" AND NOT act_archive";
	
	
	$sql.=" AND Plannings.act_date_fin>='" . $exercice . "-04-01' AND Plannings.act_date_fin<='" . intval($exercice+1) . "-03-31'";
	$sql.=" AND act_pro_type=" . $pm_type;
	
	if($_SESSION['acces']["acc_profil"]==3){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif($acc_agence>0){
		$sql.=" AND act_agence=" . $acc_agence;
	}
	
	
	if($commercial>0){
		$sql.=" AND acl_commercial=" . $commercial;
	}elseif($com_type>0){
		$sql.=" AND com_type=" . $com_type;
	}elseif($agence>0){
		$sql.=" AND act_agence=" . $agence;
	}
	if($cli_type>0){
		if($cli_type==2){
			$sql.=" AND cli_categorie=2";
		}else{
			$sql.=" AND NOT cli_categorie=2";
		}
	}
	
	if($mois>0){
		$sql.=" AND MONTH(Plannings.act_date_fin)=" . $mois;
	}
	if($mil!=""){
		$sql.=$mil;
	}
	$sql.=" GROUP BY act_id,act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,act_date_deb";
	if($pm_type==2){
		$sql.=",acl_id,acl_qte_pm";
	}
	$sql.=" ORDER BY act_id;";
	$req = $ConnSoc->prepare($sql);	
	$req->execute();
	$factures = $req->fetchAll();
	
	/*echo("<pre>");
		print_r($factures);
	echo("</pre>");
	die();*/
	
	// ON AJOUTE LA COMMUNICATION
	
	/*if($pm_type==1 AND ($categorie=="total" OR $categorie=="conso" OR $categorie==3) ){
		
		$sql="SELECT fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date_fr
		,fli_famille,fli_sous_famille,fli_sous_sous_famille,fli_qte,fli_montant_ht,fli_code_produit
		,fac_cli_categorie
		,MONTH(fac_date) AS mois";
		$sql.=" FROM Factures_Lignes INNER JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)";
		$sql.=" LEFT OUTER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";
		$sql.=" WHERE fli_categorie=3";
		$sql.=" AND fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'";
		
		if($_SESSION['acces']["acc_profil"]==3){
			$sql.=" AND com_ref_1=" . $acc_utilisateur;
		}elseif($acc_agence>0){
			$sql.=" AND fac_agence=" . $acc_agence;
		}
		
		if($commercial>0){
			$sql.=" AND fac_commercial=" . $commercial;
		}elseif($com_type>0){
			$sql.=" AND com_type=" . $com_type;
		}elseif($agence>0){
			$sql.=" AND fac_agence=" . $agence;
		}
		if($cli_type>0){
			if($cli_type==2){
				$sql.=" AND fac_cli_categorie=2";
			}else{
				$sql.=" AND NOT fac_cli_categorie=2";
			}
		}
		if($mois>0){
			$sql.=" AND MONTH(fac_date)=" . $mois;
		}	
		
		$sql.=" ORDER BY fac_numero";
		$sql.=";";
		$req=$ConnSoc->query($sql);
		$factures_comm=$req->fetchAll(PDO::FETCH_ASSOC);
		
	}*/
	
	
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">

		<?php 			if(empty($factures)){ ?>
							<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-warning" style="border-radius:0px;">
									Aucune facture correspondant à votre recherche.
								</div>
							</div>
		<?php			}else{
			
							$total_ca=0; 
							$total_nb_j=0; ?>
			
							<div class="table-responsive">
								<table class="table table-striped" >
									<caption>Formation</caption>
									<thead>
										<tr class="dark2" >											
											<th>Action</th> 																																
											<th>Catégorie</th>
											<th>Famille</th>
											<th>Sous-Famille</th>
											<th>Sous-Sous-Famille</th>
											<th>Intervenant</th>	
											<th>Date de début</th>		
											<th>Date de fin</th>	
											<th>H.T.</th>
								<?php		if($pm_type==1){ ?>
												<th>Nb. 1/2 jour</th>
								<?php		}else{ ?>
												<th>Nb. stagiaire</th>
								<?php		} ?>
											<th>PM</th>
										</tr>
									</thead>
									<tbody>
							<?php		foreach($factures as $f){
											
											// style de la ligne										
											$style="";
											
											$act_date_deb_fr="";
											if(!empty($f['act_date_deb'])){
												$dt_date_deb=date_create_from_format("Y-m-d",$f['act_date_deb']);
												$act_date_deb_fr=$dt_date_deb->format("d/m/Y");
											}
											
											$act_date_fin_fr="";
											if(!empty($f['act_date_fin'])){
												$dt_date_fin=date_create_from_format("Y-m-d",$f['act_date_fin']);
												$act_date_fin_fr=$dt_date_fin->format("d/m/Y");
											}
											
											$unit=0;
											if($pm_type==1){
												$unit=$f['nb_demi_j'];
											}else{ 
												$unit=$f['acl_for_nb_sta'];
											} 

											$total_ca+=$f['ca'];
											$total_nb_j+=$unit;
											
											$pm=0;
											if(!empty($unit)){
												$pm=$f['ca']/$unit;
												$pm=round($pm,2);
											}else{
												echo("&nbsp;");
											} 
											?>
											<tr>
												<td>
											<?php	echo($f['act_id']);
													if($pm_type==2){
														echo("-" . $f['acl_id']);
													} ?>
												</td>																							
												<td>
										<?php		echo($d_categories[1]); ?>
												</td>
												<td>
										<?php		if(!empty($f['act_pro_famille'])){
														echo($d_familles[$f['act_pro_famille']]);
													}else{
														echo("&nbsp;");
													} ?>
												</td>
												<td>
										<?php		if(!empty($f['act_pro_sous_famille'])){
														echo($d_s_familles[$f['act_pro_sous_famille']]);
													}else{
														echo("&nbsp;");
													} ?>
												</td>
												<td>
										<?php		if(!empty($f['act_pro_sous_sous_famille'])){
														echo($d_s_s_familles[$f['act_pro_sous_sous_famille']]);
													}else{
														echo("&nbsp;");
													} ?>
												</td>
												<td><?= $f['int_label_1'] . " " . $f['int_label_2'] ?></td>																															
												<td><?=	$act_date_deb_fr?></td>		
												<td><?=	$act_date_fin_fr?></td>		
												<td class="text-right" ><?= $f['ca'] ?></td>	
												<td class="text-right" ><?= $unit?></td>
												<td class="text-right" ><?=$pm?></td>			
											</tr>
				<?php					}  ?>
									</tbody>
									<tfoot>
										<tr>
											<th colspan="8" class="text-right" >Total :</th>
											<td class="text-right" ><?=$total_ca?></td>	
											<td class="text-right" ><?=$total_nb_j?></td>	
											<td class="text-right" >
									<?php		if(!empty($total_nb_j)){
													echo(round(($total_ca/$total_nb_j),2));
												}else{
													echo("&nbsp;");
												} ?>
											</td>	
										</tr>
									</tfoot>
								</table>
							</div>						 
			<?php		} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="commercial_result_pm.php?agence=<?=$agence?>&com_type=<?=$com_type?>&commercial=<?=$commercial?>&exercice=<?=$exercice?>&pm_type=<?=$pm_type?>" class="btn btn-default btn-sm" role="button">
							<i class="fa fa-left"></i> Retour
						</a>	</div>
					<div class="col-xs-6 footer-middle">&nbsp;</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
			
			});						
		</script>
	</body>
</html>
