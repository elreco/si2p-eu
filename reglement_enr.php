﻿<?php 

session_start(); 
include "includes/connexion.php";
include "includes/connexion_soc.php";

$erreur_txt="";
$warning_txt="";

if(!empty($_POST)){
	
	$facture_id=0;
	if(!empty($_POST["facture"])){
		$facture_id=intval($_POST["facture"]);
	}
	if(empty($facture_id)){
		$erreur_txt="Formulaire incomplet!";
	}else{
		if(!$_SESSION["acces"]["acc_droits"][30]){
			$erreur_txt="impossible d'enregistrer les réglements";
		}
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}

if(empty($erreur_txt)){
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	
	// SUR LA FACTURE 
	
	$sql="SELECT fac_total_ttc,fac_nature,fac_rib,fac_affacturage,fac_client FROM Factures WHERE fac_id=" . $facture_id . ";";
	$req=$ConnSoc->query($sql);
	$d_facture=$req->fetch();
	if(empty($d_facture)){
		$erreur_txt="Impossible de charger la facture.";
	}
	
}
if(empty($erreur_txt)){
	
	// SUR LE RIB
	if(!empty($d_facture)){
		$sql="SELECT rib_banque FROM Rib WHERE rib_id=" . $d_facture["fac_rib"] . " AND rib_banque>0;";
		$req=$Conn->query($sql);
		$d_facture_rib=$req->fetch();
		if(empty($d_facture_rib)){
			$erreur_txt="Le RIB de la facture n'est lié à aucune banque";
		}
	}
	
}

// TRAITEMENT 
if(empty($erreur_txt)){
	
	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}

	
	$sql_del="DELETE FROM Reglements WHERE reg_id=:reglement;";
	$req_del=$ConnSoc->prepare($sql_del);
	
	$sql_add="INSERT INTO Reglements (reg_facture,reg_date,reg_montant,reg_type,reg_reference,reg_banque,reg_date_enr,reg_uti_enr,reg_rb,reg_rb_date) 
	VALUES (:reg_facture,:reg_date,:reg_montant,:reg_type,:reg_reference,:reg_banque,:reg_date_enr,:reg_uti_enr,:reg_rb,:reg_rb_date);";
	$req_add=$ConnSoc->prepare($sql_add);
	$req_add->bindParam(":reg_facture",$facture_id);
	
	$sql_up="UPDATE Reglements SET reg_date=:reg_date, reg_montant=:reg_montant, reg_type=:reg_type, reg_reference=:reg_reference, reg_banque=:reg_banque,
	reg_date_enr=:reg_date_enr,reg_rb=:reg_rb,reg_rb_date=:reg_rb_date
	WHERE reg_id=:reglement AND reg_facture=:reg_facture;";
	$req_up=$ConnSoc->prepare($sql_up);
	$req_up->bindParam(":reg_facture",$facture_id);
	
	$fac_regle=0;
	$fac_date_reg=null;
	$warning=false;
	
	$erreur_banque=false;
	
	foreach($_POST["reg_montant"] as $ligne => $montant){
		
		$reg_date=null;
		if(!empty($_POST["reg_date"][$ligne])){
			$DT_reg_date=date_create_from_format('d/m/Y',$_POST["reg_date"][$ligne]);
			if(!is_bool($DT_reg_date)){
				$reg_date=$DT_reg_date->format("Y-m-d");
			}
		}
	
		$reg_id=0;
		if(!empty($_POST["reg_id"][$ligne])){
			$reg_id=intval($_POST["reg_id"][$ligne]);
		}
		
		$reg_montant=0;
		if(!empty($montant)){
			$reg_montant=floatval($montant);
		}
		
		$reg_type=0;
		if(!empty($_POST["reg_type"][$ligne])){
			$reg_type=intval($_POST["reg_type"][$ligne]);
		}
		
		$reg_reference=$_POST["reg_reference"][$ligne];
		
		$reg_banque=0;
		if(!empty($_POST["reg_banque"][$ligne])){
			$reg_banque=intval($_POST["reg_banque"][$ligne]);
		}
		
		$reg_date_enr=null;
		if(!empty($_POST["reg_date_enr"][$ligne])){
			$DT_reg_date_enr=date_create_from_format('d/m/Y',$_POST["reg_date_enr"][$ligne]);
			if(!is_bool($DT_reg_date_enr)){
				$reg_date_enr=$DT_reg_date_enr->format("Y-m-d");
			}
		}
		
		$reg_rb_date=null;
		$reg_rb=0;
		if(!empty($_POST["reg_rb_date"][$ligne])){
			$DT_reg_rb_date=date_create_from_format('d/m/Y',$_POST["reg_rb_date"][$ligne]);
			if(!is_bool($DT_reg_rb_date)){
				$reg_rb_date=$DT_reg_rb_date->format("Y-m-d");
				$reg_rb=1;
			}
		}
		
		$erreur=false;
		
		if($reg_montant!=0){

			if(!empty($reg_date) AND !empty($reg_type) AND !empty($reg_reference)){
				
				if($reg_id>0){
					
					// update
					$req_up->bindParam(":reg_date",$reg_date);
					$req_up->bindParam(":reg_montant",$reg_montant);
					$req_up->bindParam(":reg_type",$reg_type);
					$req_up->bindParam(":reg_reference",$reg_reference);
					$req_up->bindParam(":reg_banque",$reg_banque);
					$req_up->bindParam(":reg_date_enr",$reg_date_enr);
					$req_up->bindParam(":reg_rb_date",$reg_rb_date);		
					$req_up->bindParam(":reg_rb",$reg_rb);		
					$req_up->bindParam(":reglement",$reg_id);		
					
					try{
						$req_up->execute();
					}Catch(Exception $e){
						$erreur=true;
						echo($e->getMessage());
						die();
					}
					
				}else{
					
					// insertion
					$req_add->bindParam(":reg_date",$reg_date);
					$req_add->bindParam(":reg_montant",$reg_montant);
					$req_add->bindParam(":reg_type",$reg_type);
					$req_add->bindParam(":reg_reference",$reg_reference);
					$req_add->bindParam(":reg_banque",$reg_banque);
					$req_add->bindParam(":reg_date_enr",$reg_date_enr);	
					$req_add->bindParam(":reg_uti_enr",$acc_utilisateur);	
					$req_add->bindParam(":reg_rb_date",$reg_rb_date);		
					$req_add->bindParam(":reg_rb",$reg_rb);		
					try{
						$req_add->execute();
					}Catch(Exception $e){
						$erreur=true;
						echo($e->getMessage());
						die();
					}

					// on controle que le paiement a été fait sur la bonne banque
					// uniquement sur la création pour eviter que premier reg erroné fasse sauter le courrier lors des reglements suivants qui peuvent etre correct
					if(!$erreur_banque){
						if($d_facture["fac_affacturage"] AND $acc_societe!=17){
							// l'affacturage est géré différemment sur adequation (un iban par client avec fac_rib = rib client et non rib affacturage)
							
							if($reg_banque!=$d_facture_rib["rib_banque"]){
								$erreur_banque=true;
							}
							
						}
					}

				}
				
				// on totalise
				$fac_regle=$fac_regle + $reg_montant;				
				if($reg_date>$fac_date_reg){
					$fac_date_reg=$reg_date;
				}
				
			}else{
				$erreur=true;
				echo("A Formulaire incomplet");
				die();
			}
			
		}elseif($reg_id){
			
			$req_del->bindParam(":reglement",$reg_id);
			try{
				$req_del->execute();
			}Catch(Exception $e){
				$erreur=true;
			}
			
		}
		if($erreur){
			$warning=true;	
		}
	}

	
	// le client n'as pas payé sur la banque factor -> on annule le courier
	
	if($warning){
		$warning_txt="Tous les réglements n'ont pas été correctement enregistrés!";	
	}else{
	
		// MAJ DE LA FACTURE
		if(($d_facture["fac_nature"]==1 AND $fac_regle<$d_facture["fac_total_ttc"]) OR ($d_facture["fac_nature"]==2 AND $fac_regle>$d_facture["fac_total_ttc"])){
			$fac_date_reg=null;
		}
		$sql="UPDATE Factures SET fac_regle=:fac_regle, fac_date_reg=:fac_date_reg WHERE fac_id=:facture_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":facture_id",$facture_id);
		$req->bindParam(":fac_regle",$fac_regle);
		$req->bindParam(":fac_date_reg",$fac_date_reg);
		try{
			$req->execute();
		}Catch(Exception $e){
			$warning_txt="La facture n'a pas été actualisée!" . $e->getMessage();	
		}	
	}
	
	if($erreur_banque){
		
		
		
		$sql="UPDATE Clients_Societes SET cso_aff_courrier=0 WHERE cso_client=:client AND cso_societe=:societe";
		$req=$Conn->prepare($sql);
		$req->bindParam(":client",$d_facture["fac_client"]);
		$req->bindParam(":societe",$acc_societe);
		try{
			$req->execute();
		}Catch(Exception $e){
			$warning_txt.="Le courrier d'affacturage n'a pas été réactivé!" . $e->getMessage();	
		}	
	}
}

if(!empty($erreur_txt)){
	
	$_SESSION['message'][] = array(
		"titre" => "Echec",
		"type" => "danger",
		"message" => $erreur_txt
	);
	
}elseif(!empty($warning_txt)){
	$_SESSION['message'][] = array(
		"titre" => "Attention!",
		"type" => "warning",
		"message" => $warning_txt
	);
}
header("location : " . $_SESSION["retourFacture"]);

?>