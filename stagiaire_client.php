<?php 

// CREATION OU MAJ D'UNE FICHE STAGIAIRE
/* NOTE :
seul la maj fonctionne. La création des stagiaires n'existe pas en direct. Elle est déclencher la la céation d'une fiche stagiaire ou par des inscription. */
	
include "includes/controle_acces_cli.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');

$stagiaire=0;
if(isset($_GET['stagiaire'])){
	if(!empty($_GET['stagiaire'])){
		$stagiaire=intval($_GET['stagiaire']);
	}
}
/* if($stagiaire==0){
	echo("impossible d'afficher la page");
	die();
} */

// DONNEES POUR AFFICHAGE

// LE STAGIAIRE

$req = $Conn->prepare("SELECT * FROM Stagiaires WHERE sta_id = :sta_id");
$req->bindParam(':sta_id',$stagiaire);
$req->execute();
$d_stagiaire = $req->fetch();
$sta_naissance="";
if(!empty($d_stagiaire)){
	
	
	if(!empty($d_stagiaire["sta_naissance"])){
		$Dt_naiss=DateTime::createFromFormat('Y-m-d',$d_stagiaire["sta_naissance"]);
		$sta_naissance=$Dt_naiss->format("d/m/Y");
    }
    
	
}


?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Stagiaire</title> 
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<!-- Admin forms -->
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<!-- SELECT2 -->
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
		<!-- ORION.CSS a mettre toujours à la fin -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="icon" type="image/png" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="stagiaire_client_enr.php" >
			<input type="hidden" name="sta_id" value="<?=$stagiaire?>">
			<div id="main">
<?php 			include "includes/header_cli.inc.php"; ?>
				<section id="content_wrapper" >
					<section id="content" class="animated fadeIn">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary ">									
									<div class="panel heading-border panel-primary">
									
										<div class="panel-body bg-light">
											<div class="content-header">
									<?php 		if(!empty($stagiaire)){ ?>
													<h2>Modifier la fiche stagiaire de <b class="text-primary"><?= $d_stagiaire['sta_prenom'] ?> <?= $d_stagiaire['sta_nom'] ?></b></h2>  
									<?php 		}else{ ?>
													<h2>Nouveau <b class="text-primary"> stagiaire</b></h2>
									<?php 		} ?>   
												<span style="color:red">*</span> Les champs marqués d'un astérisque sont obligatoires       
											</div>									
											<div class="col-md-10 col-md-offset-1">		
																						
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Identité</span>
														</div>
													</div>
												</div>
												<div class="row mt5" >
													<div class="col-md-3" >
														<label for="sta_titre" >Civilité :</label>
														<select class="select2" id="sta_titre" name="sta_titre" required>										
											<?php 			foreach($base_civilite as $k => $b){ 
																if($k > 0){
																	if($k==$d_stagiaire['sta_titre']){
																		echo("<option value='" . $k . "' selected >" . $b . "</option>");
																	}else{
																		echo("<option value='" . $k . "' >" . $b . "</option>");
																	}
																}
															} ?>
														</select>
													</div>
													<div class="col-md-3" >
														<label for="sta_nom" >Nom :</label>	
														<input type="text" name="sta_nom" id="sta_nom" class="gui-input nom" placeholder="Nom" value="<?= $d_stagiaire['sta_nom'] ?>" required />									
													</div>
													<div class="col-md-3" >
														<label for="sta_nom" >Prénom :</label>	
														<input type="text" name="sta_prenom" class="gui-input prenom" placeholder="Prénom" value="<?= $d_stagiaire['sta_prenom'] ?>" required />							
													</div>
													<div class="col-md-3" >
														<label for="sta_naissance" >Date de naissance :</label>
														<span  class="field prepend-icon">
															<input type="text" id="sta_naissance" name="sta_naissance" class="gui-input datepicker" required placeholder="Date de naissance" value="<?=$sta_naissance?>" />
															<span class="field-icon"><i
																class="fa fa-calendar-o"></i>
															</span>
														</span>
													</div>
												</div>
												
												<div class="row" >
													
													
													<div class="col-md-6">
													
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Coordonnées</span>
																</div>
															</div>
														</div>
														
														<div class="row">												
															<div class="col-md-12">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="email" name="sta_mail_perso" id="sta_mail_perso" class="gui-input" placeholder="Adresse Email" value="<?= $d_stagiaire['sta_mail_perso'] ?>" >
																		<label for="sta_mail_perso" class="field-icon">
																			<i class="fa fa-envelope"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														
														
													</div>																						
												</div>
																				
											</div>
										</div>				
									</div>                             
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a 
                        <?php if(!empty($_SESSION['retour'])){ ?>
                            href="<?=$_SESSION['retour']?>" 
                        <?php }else{?>
                            href="stagiaire_client_voir.php?stagiaire=<?=$stagiaire?>" 
                        <?php }?>
                        
                        class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
									Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle">

					</div>
					<div class="col-xs-3 footer-right">
						
						<button type="submit" name="search" class="btn btn-success btn-sm">
							<i class='fa fa-floppy-o'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php 
		include "includes/footer_script.inc.php"; ?>  
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			
		</script>
	</body>
</html>