<?php

// MISE à JOUR PREFERENCE MARKET

include('includes/connexion.php');

$erreur="";
if(!empty($_POST["public"])){
	$public=$_POST["public"];
}else{
	$erreur="Erreur!";
}
if(!empty($_POST["con_mail"])){
	$con_mail=$_POST["con_mail"];
}else{
	$erreur="Erreur!";
}
if(!empty($_POST["con_nom"])){
	$con_nom=$_POST["con_nom"];
}else{
	$erreur="Erreur!";
}

if(empty($erreur)){

	
	$sql="SELECT con_id FROM Contacts WHERE con_hash_public=:con_hash_public;";
	$req=$Conn->prepare($sql);
	$req->bindParam("con_hash_public",$public);
	$req->execute();
	$d_contact=$req->fetch();
	if(empty($d_contact)){
		$erreur="Erreur!";
	}
}

if(empty($erreur)){

	$market_partiel=0;
	
	$market_all=0;
	if(!empty($_POST["market_all"])){
		$market_all=1;
	}

	$market_no=0;
	if(!empty($_POST["market_no"])){
		$market_no=1;
	}

	$sql_add="INSERT INTO Contacts_Marketing (cma_market,cma_contact) VALUES (:cma_market,:cma_contact);";
	$req_add=$Conn->prepare($sql_add);

	$sql_del="DELETE FROM Contacts_Marketing WHERE cma_market=:cma_market AND cma_contact=:cma_contact;";
	$req_del=$Conn->prepare($sql_del);

	$sql="SELECT mac_id,cma_market,cma_contact FROM Marketing_Actions LEFT OUTER JOIN Contacts_Marketing ON (Marketing_Actions.mac_id=Contacts_Marketing.cma_market AND Contacts_Marketing.cma_contact=:contact)
	ORDER BY mac_libelle;";
	$req=$Conn->prepare($sql);
	$req->bindParam("contact",$d_contact["con_id"]);
	$req->execute();
	$d_market=$req->fetchAll();
	if(!empty($d_market)){
		foreach($d_market as $mar){

			
			if((isset($_POST["market_" . $mar["mac_id"]]) OR $market_all) AND empty($mar["cma_contact"]) ){

				$req_add->bindParam("cma_market",$mar["mac_id"]);
				$req_add->bindParam("cma_contact",$d_contact["con_id"]);
				$req_add->execute();
			
				$market_partiel=1;
				
			}elseif((!isset($_POST["market_" . $mar["mac_id"]]) OR $market_no) AND !empty($mar["cma_contact"])){
				
				$req_del->bindParam("cma_market",$mar["mac_id"]);
				$req_del->bindParam("cma_contact",$d_contact["con_id"]);
				$req_del->execute();

			}elseif(!empty($mar["cma_contact"])){
				// deja select et reste select 
				$market_partiel=1;
				
			}
		
		}
	}

}

if(empty($erreur)){
	
	if($market_partiel==1 AND $market_all==1){
		$market_partiel=0;
	}

	$sql="UPDATE Contacts SET con_nom=:con_nom,
	con_prenom=:con_prenom,
	con_tel=:con_tel,
	con_portable=:con_portable,
	con_mail=:con_mail,
	con_market_all=:con_market_all,
	con_market_no=:con_market_no,
	con_market_partiel=:con_market_partiel
	WHERE con_id=:con_id;";
	$req=$Conn->prepare($sql);
	$req->bindParam("con_id",$d_contact["con_id"]);
	$req->bindParam("con_nom",$_POST["con_nom"]);
	$req->bindParam("con_prenom",$_POST["con_prenom"]);
	$req->bindParam("con_tel",$_POST["con_tel"]);
	$req->bindParam("con_portable",$_POST["con_portable"]);
	$req->bindParam("con_mail",$_POST["con_mail"]);
	$req->bindParam("con_market_all",$market_all);
	$req->bindParam("con_market_no",$market_no);
	$req->bindParam("con_market_partiel",$market_partiel);
	try{
		$req->execute();
	}Catch(Exeception $e){
		$erreur="Erreur";
	}
}

if(!empty($erreur)){
	header("location : preference_mail_valide.php?err");
}else{
	header("location : preference_mail_valide.php");
}
?>