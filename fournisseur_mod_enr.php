<?php
include "includes/controle_acces.inc.php";
include("includes/connexion.php");
include("includes/connexion_fct.php");

global $Conn;

$erreur_txt="";
$warning_txt="";

if(isset($_POST)){
	
	if(!empty($_POST)){
		
		$fournisseur_id=0;
		if(!empty($_POST['fou_id'])){
			$fournisseur_id=intval($_POST['fou_id']);
		}
		
		$nom=$_POST['fou_nom'];
		$siret=$_POST['fou_siret'];
		$siren=$_POST['fou_siren'];
		
		if(empty($nom)){
			$erreur_txt="Le nom du fournisseur doit-être renseigné!";
		}elseif(empty($siret) OR empty($siren)){
			$erreur_txt="Le siret doit-être renseigné!";
		}
		
	}else{
		$erreur_txt="Formulaire incomplet";
	}
}else{
	$erreur_txt="Formulaire incomplet";
}


// CONTROLE
if(empty($erreur_txt)){

	// SELECT LE FOURNISSEUR
	$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id = " . $_POST['fou_id']);
	$req->execute();
	$fournisseur_actuel = $req->fetch();

	//if(empty($fournisseur_actuel["fou_interco_soc"])){ // stand by pour l'instant

		$sql="SELECT * FROM fournisseurs WHERE fou_id != :fou_id AND fou_siren = :siren AND fou_siret = :siret"; 
		$req = $Conn->prepare($sql);
		$req->bindParam("fou_id",$fournisseur_id);
		$req->bindParam("siren",$siren);
		$req->bindParam("siret",$siret);
		$req->execute();
		$fournisseur_existe = $req->fetch();
		if(!empty($fournisseur_existe)){
			$erreur_txt="Le siret saisie est déjà utilisé.";
		}
	//}
}

if(empty($erreur_txt)){

	
	// LE TYPE DE FOURNISSEUR A CHANGE
	if($_POST['fou_type'] != $fournisseur_actuel['fou_type']){
		// Société
		if($_POST['fou_type']==3){
			// SELECT LE CONTACT
			$req = $Conn->prepare("SELECT * FROM fournisseurs_contacts WHERE fco_fournisseur = " . $_POST['fou_id']);
			$req->execute();
			$fournisseur_contact_actuel = $req->fetchAll();
			// SELECT L'INTERVENANT
			$req = $Conn->prepare("SELECT * FROM fournisseurs_intervenants WHERE fin_fournisseur = " . $_POST['fou_id'] . " AND fin_archive = 0");
			$req->execute();
			$fournisseur_intervenant_actuel = $req->fetchAll();
			// SI INTERVENANT > 1, ON NE PEUT PAS CHANGER 
			if(count($fournisseur_intervenant_actuel) > 1){
				$_SESSION['message'][] = array(
					"titre" => "Erreur !",
					"type" => "danger",
					"message" => "Vous ne pouvez pas changer le type de ce fournisseur"
				);
				
				Header("Location: fournisseur_mod.php?id=" . $_POST['fou_id']);
				die();
			}
			// SI CONTACT > 1, ON NE PEUT PAS CHANGER 
			if(count($fournisseur_contact_actuel) > 1){
				$_SESSION['message'][] = array(
					"titre" => "Erreur !",
					"type" => "danger",
					"message" => "Vous ne pouvez pas changer le type de ce fournisseur"
				);
				Header("Location: fournisseur_mod.php?id=" . $_POST['fou_id']);
				die();
			}
			// SI NOM ET PRENOM DIFFERENTS
			if(empty($fournisseur_intervenant_actuel)){
				$_SESSION['message'][] = array(
					"titre" => "Erreur !",
					"type" => "danger",
					"message" => "Vous ne pouvez pas changer le type de ce fournisseur"
				);
				Header("Location: fournisseur_mod.php?id=" . $_POST['fou_id']);
				die();
			}
			if(!empty($fournisseur_intervenant_actuel) && $fournisseur_contact_actuel[0]['fco_nom'] != $fournisseur_intervenant_actuel[0]['fin_nom'] OR $fournisseur_contact_actuel[0]['fco_prenom'] != $fournisseur_intervenant_actuel[0]['fin_prenom']){
				$_SESSION['message'][] = array(
					"titre" => "Erreur !",
					"type" => "danger",
					"message" => "Vous ne pouvez pas changer le type de ce fournisseur"
				);
				Header("Location: fournisseur_mod.php?id=" . $_POST['fou_id']);
				die();
			}
		}
	}
	
	
	// TRAITEMENT DU FORM
	
	// groupe du fournisseur
	if(isset($_POST['fou_groupe'])){
		$fou_groupe = 1;

	}else{
		$fou_groupe = 0;
	}
	if(isset($_POST['fou_archive'])){
		$fou_archive = 1;
	}else{
		$fou_archive = 0;
	}
	if(isset($_POST['fou_blacklist'])){
		$fou_blacklist = 1;
		$fou_blacklist_uti = $_SESSION['acces']['acc_ref_id'];
	}else{
		$fou_blacklist = 0;
		$fou_blacklist_uti = 0;
		$_POST['fou_blacklist_txt'] = "";
	}

	if(isset($_POST['fou_groupe']) && isset($_POST['fou_maison_mere']) && $_POST['fou_maison_mere'] == "oui"){

		$fou_filiale_de = 0;

	}

	if(isset($_POST['fou_groupe']) && isset($_POST['fou_maison_mere']) && $_POST['fou_maison_mere'] == "non"){

		$fou_filiale_de = $_POST['fou_filiale_de'];
	}
	if(isset($_POST['fou_depassement_autorise']) && $_POST['fou_depassement_autorise'] == "oui"){
		$fou_depassement = 1;
	}else{
		$fou_depassement = 0;
	}
	
	$fco_fonction=0;
	if(!empty($_POST['fco_fonction'])){
		$fco_fonction=intval($_POST['fco_fonction']);
	}
	if($_POST['fco_fonction'] == "autre"){ 
		$fco_fonction= 0;
	}
	
	// ENREGISTREMENT

	$req = $Conn->prepare("UPDATE fournisseurs SET fou_nom = :fou_nom, fou_type = :fou_type, fou_site = :fou_site, fou_sage = :fou_sage, fou_filiale_de = :fou_filiale_de, fou_groupe = :fou_groupe, fou_siren = :fou_siren, fou_siret =:fou_siret, fou_tva =:fou_tva, fou_reg_type = :fou_reg_type, fou_reg_nb_jour = :fou_reg_nb_jour, fou_reg_formule = :fou_reg_formule, fou_reg_fdm =:fou_reg_fdm, fou_bic =:fou_bic, fou_iban = :fou_iban, fou_montant_max = :fou_montant_max, fou_depassement_autorise = :fou_depassement, fou_blacklist = :fou_blacklist, fou_archive = :fou_archive, fou_blacklist_txt = :fou_blacklist_txt, fou_blacklist_uti = :fou_blacklist_uti WHERE fou_id = :fou_id;");
	$req->bindParam(':fou_id', $_POST['fou_id']);
	$req->bindParam(':fou_nom', $_POST['fou_nom']);
	$req->bindParam(':fou_type', $_POST['fou_type']);
	$req->bindParam(':fou_site', $_POST['fou_site']);
	$req->bindParam(':fou_sage', $_POST['fou_sage']);
	$req->bindParam(':fou_filiale_de', $fou_filiale_de);
	$req->bindParam(':fou_groupe', $fou_groupe);
	$req->bindParam(':fou_archive', $fou_archive);
	$req->bindParam(':fou_blacklist', $fou_blacklist);
	$req->bindParam(':fou_blacklist_txt', $_POST['fou_blacklist_txt']);
	$req->bindParam(':fou_blacklist_uti', $fou_blacklist_uti);
	$req->bindParam(':fou_siren', $_POST['fou_siren']);
	$req->bindParam(':fou_siret', $_POST['fou_siret']);
	$req->bindParam(':fou_tva', $_POST['fou_tva']);
	$req->bindParam(':fou_reg_type', $_POST['fou_reg_type']);
	$req->bindParam(':fou_reg_nb_jour', $_POST['fou_reg_nb_jour']);
	$req->bindParam(':fou_reg_formule', $_POST['fou_reg_formule']);
	$req->bindParam(':fou_reg_fdm', $_POST['fou_reg_fdm']);
	$req->bindParam(':fou_bic', $_POST['fou_bic']);
	$req->bindParam(':fou_iban', $_POST['fou_iban']);
	$req->bindParam(':fou_montant_max', $_POST['fou_montant_max']);
	$req->bindParam(':fou_depassement', $fou_depassement);
	$req->execute();

	$req = $Conn->prepare("SELECT fad_id, fad_fournisseur FROM fournisseurs_adresses WHERE fad_fournisseur = :fad_fournisseur");
	$req->bindParam(':fad_fournisseur', $_POST['fou_id']);
	$req->execute();
	$adresse = $req->fetch();
	if(!empty($adresse)){
		$req = $Conn->prepare("UPDATE fournisseurs_adresses SET fad_fournisseur = :fad_fournisseur, fad_nom = :fad_nom, fad_ad1 = :fad_ad1, fad_ad2 = :fad_ad2, fad_ad3 = :fad_ad3, fad_cp = :fad_cp, fad_ville = :fad_ville WHERE fad_id = :fad_id");
		$req->bindParam(':fad_id', $adresse['fad_id']);
		$req->bindParam(':fad_fournisseur', $_POST['fou_id']);
		$req->bindParam(':fad_nom', $_POST['fad_nom']);
		$req->bindParam(':fad_ad1', $_POST['fad_ad1']);
		$req->bindParam(':fad_ad2', $_POST['fad_ad2']);
		$req->bindParam(':fad_ad3', $_POST['fad_ad3']);
		$req->bindParam(':fad_cp', $_POST['fad_cp']);
		$req->bindParam(':fad_ville', $_POST['fad_ville']);
		$req->execute();

	}else{
		$req = $Conn->prepare("INSERT INTO fournisseurs_adresses (fad_fournisseur, fad_nom, fad_ad1, fad_ad2, fad_ad3, fad_cp, fad_ville) VALUES (:fad_fournisseur, :fad_nom, :fad_ad1, :fad_ad2, :fad_ad3, :fad_cp, :fad_ville)");

		$req->bindParam(':fad_fournisseur', $_POST['fou_id']);
		$req->bindParam(':fad_nom', $_POST['fad_nom']);
		$req->bindParam(':fad_ad1', $_POST['fad_ad1']);
		$req->bindParam(':fad_ad2', $_POST['fad_ad2']);
		$req->bindParam(':fad_ad3', $_POST['fad_ad3']);
		$req->bindParam(':fad_cp', $_POST['fad_cp']);
		$req->bindParam(':fad_ville', $_POST['fad_ville']);


		$req->execute();
	}

	// CONTACT
	
	$warning_con="";
	
	$req = $Conn->prepare("SELECT fco_id, fco_fournisseur FROM fournisseurs_contacts WHERE fco_fournisseur = :fco_fournisseur AND fco_defaut = 1");
	$req->bindParam(':fco_fournisseur', $fournisseur_id);
	$req->execute();
	$contact = $req->fetch();
	if(!empty($contact)){
		
		// UPDATE DU CONTACT
		
		$req = $Conn->prepare("UPDATE fournisseurs_contacts SET fco_fournisseur = :fco_fournisseur, fco_fonction = :fco_fonction, fco_fonction_nom = :fco_fonction_nom, fco_titre = :fco_titre, fco_nom = :fco_nom, fco_prenom = :fco_prenom, fco_tel = :fco_tel, fco_fax = :fco_fax, fco_mail = :fco_mail, fco_portable = :fco_portable WHERE fco_id = :fco_id");
		$req->bindParam(':fco_id', $contact['fco_id']);
		$req->bindParam(':fco_fournisseur', $_POST['fou_id']);
		$req->bindParam(':fco_fonction', $_POST['fco_fonction']);
		$req->bindParam(':fco_fonction_nom', $_POST['fco_fonction_nom']);
		$req->bindParam(':fco_titre', $_POST['fco_titre']);
		$req->bindParam(':fco_nom', $_POST['fco_nom']);
		$req->bindParam(':fco_prenom', $_POST['fco_prenom']);
		$req->bindParam(':fco_tel', $_POST['fco_tel']);
		$req->bindParam(':fco_fax', $_POST['fco_fax']);
		$req->bindParam(':fco_mail', $_POST['fco_mail']);
		$req->bindParam(':fco_portable', $_POST['fco_portable']);
		try{
			$req->execute();
		}Catch(Exception $e){
			$warning_con="UPDATE CONTACT :" . $e->getMessage();
		}
		
	}else{
		
		$req = $Conn->prepare("INSERT INTO fournisseurs_contacts (fco_fournisseur, fco_fonction, fco_fonction_nom, fco_titre, fco_nom, fco_prenom, fco_tel, fco_fax, fco_mail,fco_portable, fco_defaut) VALUES (:fco_fournisseur, :fco_fonction, :fco_fonction_nom, :fco_titre, :fco_nom, :fco_prenom, :fco_tel, :fco_fax, :fco_mail,:fco_portable, 1)");
		$req->bindParam(':fco_fournisseur', $_POST['fou_id']);
		$req->bindParam(':fco_fonction', $_POST['fco_fonction']);
		$req->bindParam(':fco_fonction_nom', $_POST['fco_fonction_nom']);
		$req->bindParam(':fco_titre', $_POST['fco_titre']);
		$req->bindParam(':fco_nom', $_POST['fco_nom']);
		$req->bindParam(':fco_prenom', $_POST['fco_prenom']);
		$req->bindParam(':fco_tel', $_POST['fco_tel']);
		$req->bindParam(':fco_fax', $_POST['fco_fax']);
		$req->bindParam(':fco_mail', $_POST['fco_mail']);
		$req->bindParam(':fco_portable', $_POST['fco_portable']);
		try{
			$req->execute();
		}Catch(Exception $e){
			$warning_con="ADD CONTACT :" . $e->getMessage();
		}
		
	}

	// C'EST UN AUTO ENTREPRENEUR
	
	if($_POST['fou_type'] == 3 AND empty($warning_con)){
		
		$sql="SELECT fin_id,fin_nom,fin_prenom FROM Fournisseurs_Intervenants WHERE fin_fournisseur=" . $fournisseur_id . ";";
		$req = $Conn->query($sql);
		$d_auto_ent=$req->fetchAll();
		if(!empty($d_auto_ent)){
			if(count($d_auto_ent)>1){
				
				$warning_con="Cet auto-entrepreneur dispose de plusieur intervenants!";	
				
			}else{
				
				// update de l'intervenant
				$req = $Conn->prepare("UPDATE fournisseurs_intervenants SET fin_nom = :fin_nom, fin_prenom = :fin_prenom, fin_tel = :fin_tel, fin_fax = :fin_fax, fin_mail = :fin_mail, fin_portable=:fin_portable WHERE fin_id = :fin_id;");
				$req->bindParam(':fin_id', $d_auto_ent[0]["fin_id"]);
				$req->bindParam(':fin_prenom', $_POST['fco_prenom']);
				$req->bindParam(':fin_nom', $_POST['fco_nom']);
				$req->bindParam(':fin_fax', $_POST['fco_fax']);
				$req->bindParam(':fin_mail', $_POST['fco_mail']);
				$req->bindParam(':fin_tel', $_POST['fco_tel']);
				$req->bindParam(':fin_portable', $_POST['fco_portable']);
				try{
					$req->execute();
				}Catch(Exception $e){
					$warning_con="UP FOU INT : " . $e->getMessage();
				}
				
				if(empty($warning_con)){
					
					if($d_auto_ent[0]["fin_nom"]!=$_POST['fco_nom'] OR $d_auto_ent[0]["fin_prenom"]!=$_POST['fco_prenom']){
						
						// CHANGEMENT DE NOM OU DE PRENOM -> ON ACTUALISE LE FICHE INTERVENANT PLANNING
						
						$sql="SELECT soc_id FROM Societes ORDER BY soc_id;";	
						$req = $Conn->query($sql);
						$d_societe=$req->fetchAll();
						if(!empty($d_societe)){
							
							$int_label_1=$fournisseur_actuel["fou_code"] . "-" . $_POST['fco_nom'];
							$int_label_2=$_POST['fco_prenom'];
							
							foreach($d_societe as $soc){
								
								$ConnFct=connexion_fct($soc["soc_id"]);
								
								$req = $ConnFct->prepare("UPDATE Intervenants SET int_label_1 = :int_label_1, int_label_2 = :int_label_2 
								WHERE int_type=3 AND int_ref_1=:int_ref_1 AND int_ref_2=:int_ref_2;");							
								$req->bindParam(':int_label_1', $int_label_1);
								$req->bindParam(':int_label_2', $int_label_2);
								$req->bindParam(':int_ref_1', $fournisseur_id);
								$req->bindParam(':int_ref_2', $d_auto_ent[0]["fin_id"]);
								try{
									$req->execute();
								}Catch(Exception $e){
									$warning_con="UP INT : " . $e->getMessage();
								}					
							}
						}	
					}
					
				}
			}
			
		}else{
			
			// création d'un intervenant
			$req = $Conn->prepare("INSERT INTO fournisseurs_intervenants (fin_nom, fin_prenom, fin_ad1, fin_ad2, fin_ad3, fin_cp, fin_ville, fin_tel, fin_fax, fin_mail, fin_fournisseur, fin_archive) 
			VALUES (:fin_nom,:fin_prenom,:fin_ad1,:fin_ad2,:fin_ad3,:fin_cp,:fin_ville,:fin_tel,:fin_fax,:fin_mail, :fin_fournisseur,0) ");
			$req->bindParam(':fin_nom', $_POST['fco_nom']);
			$req->bindParam(':fin_prenom', $_POST['fco_prenom']);
			$req->bindParam(':fin_ad1', $_POST['fad_ad1']);
			$req->bindParam(':fin_ad2', $_POST['fad_ad2']);
			$req->bindParam(':fin_ad3', $_POST['fad_ad3']);
			$req->bindParam(':fin_cp', $_POST['fad_cp']);
			$req->bindParam(':fin_ville', $_POST['fad_ville']);
			$req->bindParam(':fin_tel', $_POST['fco_tel']);
			$req->bindParam(':fin_fax', $_POST['fco_fax']);
			$req->bindParam(':fin_mail', $_POST['fco_mail']);			
			$req->bindParam(':fin_fournisseur', $fournisseur_id);
			try{
				$req->execute();
			}Catch(Exception $e){
				$warning_con="ADD FOU INT " . $e->getMessage();
			}
		}
	}
	
	if(!empty($warning_con)){
		$warning_txt.=$warning_con;
	}
	
	// FIN CONTACT
	

	if(!empty($_POST['ffj_famille'])){
			
		// familles des fournisseurs
		$req = $Conn->prepare("SELECT * FROM fournisseurs_familles_jointure WHERE ffj_fournisseur =" . $_POST['fou_id']);
		$req->execute();
		$fournisseurs_familles_jointure = $req->fetchAll();		
		foreach($fournisseurs_familles_jointure as $ffa){
			$req = $Conn->prepare("SELECT * FROM fournisseurs_produits WHERE fpr_fournisseur =" . $_POST['fou_id'] . " AND fpr_type = " . $ffa['ffj_famille']);
			$req->execute();
			$prod_existe = $req->fetch();
			if(!empty($prod_existe)){
				$req = $Conn->prepare("SELECT * FROM fournisseurs_familles_jointure WHERE ffj_famille = :ffj_famille AND ffj_fournisseur = :ffj_fournisseur");
				$req->bindParam(':ffj_famille', $ffa['ffj_famille']);
				$req->bindParam(':ffj_fournisseur', $_POST['fou_id']);
				$req->execute();
				$jointure_check = $req->fetch();
				
				if(empty(in_array($ffa['ffj_famille'],$_POST['ffj_famille'])) && !empty($jointure_check)){
					
					$_SESSION['message'][] = array(
						"titre" => "Attention",
						"type" => "warning",
						"message" => "Vous ne pouvez pas décocher cette famille car un produit existe déjà"
					);
	
					Header("Location: fournisseur_mod.php?id=" . $fournisseur_id);
					die();
				}
				
			}else{
				// aller chercher la famille
				$req = $Conn->prepare("SELECT * FROM fournisseurs_familles WHERE ffa_id =" . $ffa['ffj_famille']);
				$req->execute();
				$famille_droit = $req->fetch();
				$peut_delete_famille = 0;
				if(!empty($famille_droit['ffa_droit'])){
					
					if($_SESSION["acces"]["acc_droits"][$famille_droit['ffa_droit']]){
						$peut_delete_famille = 1;
					}
				}else{
					$peut_delete_famille = 1;
				}
				
				if($peut_delete_famille){
					
					$req = $Conn->prepare("DELETE FROM fournisseurs_familles_jointure WHERE ffj_famille = :ffj_famille AND ffj_fournisseur = :ffj_fournisseur");
					$req->bindParam(':ffj_famille', $ffa['ffj_famille']);
					$req->bindParam(':ffj_fournisseur', $_POST['fou_id']);
					$req->execute();
				}else{
					$req = $Conn->prepare("SELECT * FROM fournisseurs_familles_jointure WHERE ffj_famille = :ffj_famille AND ffj_fournisseur = :ffj_fournisseur");
					$req->bindParam(':ffj_famille', $ffa['ffj_famille']);
					$req->bindParam(':ffj_fournisseur', $_POST['fou_id']);
					$req->execute();
					$jointure_check = $req->fetch();
					if(!empty($jointure_check) && empty(in_array($ffa['ffj_famille'],$_POST['ffj_famille']))){
						$_SESSION['message'][] = array(
							"titre" => "Attention",
							"type" => "warning",
							"message" => "Vous ne pouvez pas décocher la famille " . $famille_droit['ffa_libelle'] . " car vous n'avez pas le droit"
						);
	
						Header("Location: fournisseur_mod.php?id=" . $fournisseur_id);
						die();
					}
					
				}
				
			}
		}
		foreach($_POST['ffj_famille'] as $ffj){
			$req = $Conn->prepare("SELECT * FROM fournisseurs_familles_jointure WHERE ffj_famille = :ffj_famille AND ffj_fournisseur = :ffj_fournisseur");
			$req->bindParam(':ffj_famille', $ffj);
			$req->bindParam(':ffj_fournisseur', $_POST['fou_id']);
			$req->execute();
			$ffj_existe = $req->fetch();
			
			if(empty($ffj_existe)){
				
				// aller chercher la famille
				$req = $Conn->prepare("SELECT * FROM fournisseurs_familles WHERE ffa_id =" . $ffj);
				$req->execute();
				$famille_droit = $req->fetch();
				$peut_ajouter_famille = 0;
				if(!empty($famille_droit['ffa_droit'])){
					
					if($_SESSION["acces"]["acc_droits"][$famille_droit['ffa_droit']]){
						$peut_ajouter_famille = 1;
					}
				}else{
					$peut_ajouter_famille = 1;
				}
				if($peut_ajouter_famille){
					$req = $Conn->prepare("INSERT INTO fournisseurs_familles_jointure (ffj_famille, ffj_fournisseur) VALUES (:ffj_famille, :ffj_fournisseur)");
					$req->bindParam(':ffj_famille', $ffj);
					$req->bindParam(':ffj_fournisseur', $_POST['fou_id']);
					$req->execute();
				}else{
					$_SESSION['message'][] = array(
						"titre" => "Attention",
						"type" => "warning",
						"message" => "Vous ne pouvez pas cocher la famille " . $famille_droit['ffa_libelle'] . " car vous n'avez pas le droit"
					);

					Header("Location: fournisseur_mod.php?id=" . $fournisseur_id);
					die();
				}
			}
		}
	}
}

if(!empty($erreur_txt)){
	
	$_SESSION['message'] = array(
		"aff" => "modal",
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur_txt
	);

	Header("Location: fournisseur_mod.php?id=" . $fournisseur_id);
	
}else{
	
	if(!empty($warning_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Attention",
			"type" => "warning",
			"message" => $warning_txt
		);
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Succès",
			"type" => "success",
			"message" => "Le fournisseur a bien été actualisé"
		);
	}
	Header("Location: fournisseur_voir.php?fournisseur=" . $fournisseur_id);
}
?>