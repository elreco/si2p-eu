<?php
	// a mettre sur toute les pages de l'application
	// contient le session_start();
//include "includes/controle_acces.inc.php";
include "modeles/mod_parametre.php";
include "includes/connexion.php";
include "includes/connexion_fct.php";
include('modeles/mod_planning_periode_lib.php');
include('modeles/mod_planning_periode.php');

if(!empty($_GET['token']) && !empty($_GET['societ'])){
    $hash = urldecode($_GET['token']);
    $action_client_id = my_decrypt($hash, 'orion');
    $societe = $_GET['societ'];
}else{
    die("probleme");
}

$ConnFct = connexion_fct($societe);
$sql="SELECT acl_avis_emis, acl_pro_libelle FROM actions_clients WHERE acl_id = " . $action_client_id;
$req = $ConnFct->prepare($sql);
$req->execute();
$d_action_client = $req->fetch();

if(empty($d_action_client)){
    die("probleme");
}else{
    $ConnFct = connexion_fct($societe);

    if(empty($d_action_client['acl_avis_recu'])){
        $sql="UPDATE actions_clients SET acl_avis_recu = 1, acl_avis_recu_date = NOW() WHERE acl_id = " . $action_client_id;
        $req = $ConnFct->prepare($sql);
        $req->execute();
    }

    $sql="SELECT pda_date,pda_demi FROM Plannings_Dates,Actions_Clients_Dates WHERE pda_id=acd_date AND acd_action_client=:action_client_id ORDER BY pda_date,pda_demi;";
    $req=$ConnFct->prepare($sql);
    $req->bindParam(":action_client_id",$action_client_id);
    $req->execute();
    $d_dates=$req->fetchAll();
    $planning_txt = "";
    if(!empty($d_dates)){
        $planning_txt=mb_strtolower(planning_periode($d_dates));
    }

}

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

	<!-- CSS Si2P -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm no-scroll" >
		<div id="main" style="height:100%!important;">

			<section id="content_wrapper" style="height:100%!important;">
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn" style="height:100%!important;">
                    <!-- EXEMPLE DE FONCTION POUR LES MODELES -->
                    <div class="row">
                        <div class="col-md-6 col-md-offset-2" style="margin-bottom:25px">
                            <img src="/documents/societes/logos/soc_logo_2_16_03-06-2019.png" alt="" >
                        </div>
                        <div class="col-md-6 col-md-offset-2">

                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-icon"><i class="fa fa-file"></i></span>
                                    <span class="panel-title">Accusé de réception</span>
                                </div>
                                <div class="panel-body" style="height:100%;">
                                    <p>Nous avons bien reçu la confirmation pour la <?= $d_action_client["acl_pro_libelle"] ?> qui aura lieu <?= $planning_txt ?></p>
                                    <p> <strong>Vous pouvez fermer cette fenêtre</strong> </p>
                                </div>
                            </div>
                        </div>
                    </div>

				</section>
			</section>
		</div>




		<script type="text/javascript" src="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css"></script>


		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<!-- SCRIPT SPE -->

	</body>
</html>
