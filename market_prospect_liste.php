<?php
	include "includes/controle_acces.inc.php";

/* 	LISTE DES CONTACT CLIENTS POUVANT ÊTRE UTILISE POUR LE MARKETING*/

	include('includes/connexion.php');
	include('includes/connexion_soc.php');

	// DONNEE UTILE AU PROGRAMME

	// personne connecté

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}
	$_SESSION['retour'] = "market_client_liste.php";


	if (isset($_POST['search'])){



		$market_action=0;
		if(!empty($_POST['market_action'])){
			$market_action=intval($_POST['market_action']);
		}

		$cli_categorie=0;
		if(!empty($_POST['cli_categorie'])){
			$cli_categorie=intval($_POST['cli_categorie']);
		}
		$cli_sous_categorie=0;
		if(!empty($_POST['cli_sous_categorie'])){
			$cli_sous_categorie=intval($_POST['cli_sous_categorie']);
		}
		$cli_groupe=0;
		if(!empty($_POST['cli_groupe'])){
			$cli_groupe=intval($_POST['cli_groupe']); // 1 mm 2 groupe 3 entreprise
		}
		$cli_prescripteur=0;
		if(!empty($_POST['cli_prescripteur'])){
			$cli_prescripteur=intval($_POST['cli_prescripteur']);
		}

		$cli_ape="";
		if(!empty($_POST['cli_ape'])){
			if(is_array($_POST['cli_ape'])){
				$cli_ape=implode(",",$_POST['cli_ape']);
			}

		}

		$cli_classification=0;
		if(!empty($_POST['cli_classification'])){
			$cli_classification=intval($_POST['cli_classification']);
		}
		$cli_classification_categorie=0;
		if(!empty($_POST['cli_classification_categorie'])){
			$cli_classification_categorie=intval($_POST['cli_classification_categorie']);
		}
		$cli_classification_type=0;
		if(!empty($_POST['cli_classification_type'])){
			$cli_classification_type=intval($_POST['cli_classification_type']);
		}

		$cso_commercial=0;
		if(!empty($_POST['cli_commercial'])){
			$cso_commercial=intval($_POST['cli_commercial']);
		}

		$con_fonction=0;
		if(!empty($_POST['con_fonction'])){
			$con_fonction=intval($_POST['con_fonction']);
		}
		$con_mail=0;
		if(!empty($_POST['con_mail'])){
			$con_mail=1;
		}

		$cli_dep="";
		if(!empty($_POST['cli_dep'])){
			if(is_array($_POST['cli_dep'])){
				$cli_dep=$_POST['cli_dep'];
			}

		}


		// mémorisation des critere

		$_SESSION['cli_tri'] = array(
			"market_action" => $market_action,
			"cli_categorie" => $cli_categorie,
			"cli_sous_categorie" => $cli_sous_categorie,
			"cli_groupe" => $cli_groupe,
			"cli_prescripteur" => $cli_prescripteur,
			"cli_ape" => $cli_ape,
			"cli_classification" => $cli_classification,
			"cli_classification_categorie" => $cli_classification_categorie,
			"cli_classification_type" => $cli_classification_type,
			"cso_commercial" => $cso_commercial,
			"cli_dep" => $cli_dep,
			"con_fonction" => $con_fonction,
			"con_mail" => $con_mail
		);
	}


	$critere=array();
	$critere_cli=array();

	$mil_cli="";
	$mil_sus="";

	if(!empty($_SESSION['cli_tri']['market_action'])){
		$critere["market_action"]=$_SESSION['cli_tri']['market_action'];
	}
	if(!empty($_SESSION['cli_tri']['cli_categorie'])){
		$mil_cli.=" AND cli_categorie =:cli_categorie";
		$mil_sus.=" AND sus_categorie =:cli_categorie";
		$critere["cli_categorie"]=$_SESSION['cli_tri']['cli_categorie'];
	}
	if(!empty($_SESSION['cli_tri']['cli_sous_categorie'])){
		$mil_cli.=" AND cli_sous_categorie =:cli_sous_categorie";
		$mil_sus.=" AND sus_sous_categorie =:cli_sous_categorie";
		$critere["cli_sous_categorie"]=$_SESSION['cli_tri']['cli_sous_categorie'];
	}
	if(!empty($_SESSION['cli_tri']['cli_groupe'])){
		if($_SESSION['cli_tri']['cli_groupe']==1){
			$mil_cli.=" AND cli_groupe = 1 AND cli_filiale_de = 0";
		}else if($_SESSION['cli_tri']['cli_groupe']==2){
			$mil_cli.=" AND cli_groupe != 0";
		}else if($_SESSION['cli_tri']['cli_groupe']==3){
			$mil_cli.=" AND cli_groupe = 0";
		}
	};
	if(!empty($_SESSION['cli_tri']['cli_prescripteur'])){
		$mil_cli.=" AND cli_prescripteur =:cli_prescripteur";
		$mil_sus.=" AND sus_prescripteur =:cli_prescripteur";
		$critere["cli_prescripteur"]=$_SESSION['cli_tri']['cli_prescripteur'];
	}
	if(!empty($_SESSION['cli_tri']['cli_ape'])){
		$mil_cli.=" AND cli_ape IN (" . $_SESSION['cli_tri']['cli_ape'] . ")";
		$mil_sus.=" AND sus_ape IN (" . $_SESSION['cli_tri']['cli_ape'] . ")";
	}
	if(!empty($_SESSION['cli_tri']['cli_classification'])){
		$mil_cli.=" AND cli_classification =:cli_classification";
		$mil_sus.=" AND sus_classification =:cli_classification";
		$critere["cli_classification"]=$_SESSION['cli_tri']['cli_classification'];
	}
	if(!empty($_SESSION['cli_tri']['cli_classification_categorie'])){
		$mil_cli.=" AND cli_classification_categorie =:cli_classification_categorie";
		$mil_sus.=" AND sus_classification_categorie =:cli_classification_categorie";
		$critere["cli_classification_categorie"]=$_SESSION['cli_tri']['cli_classification_categorie'];
	}
	if(!empty($_SESSION['cli_tri']['cli_classification_type'])){
		$mil_cli.=" AND cli_classification_type =:cli_classification_type";
		$mil_sus.=" AND sus_classification_type =:cli_classification_type";
		$critere["cli_classification_type"]=$_SESSION['cli_tri']['cli_classification_type'];
	}

	// client societe
	if(!empty($_SESSION['cli_tri']['cso_commercial'])){
		$mil_cli.=" AND cso_commercial=:cso_commercial";
		$mil_sus.=" AND sus_commercial=:cso_commercial";
		$critere["cso_commercial"]=$_SESSION['cli_tri']['cso_commercial'];
	};

	// adresse d'inter par defaut (lu sur tab client)

	// adresse d'inter par defaut (lu sur tab client)
	if(!empty($_SESSION['cli_tri']['cli_dep'])){
		$mil_cli.=" AND (";
		foreach($_SESSION['cli_tri']['cli_dep'] as $k => $cp){
			if($k==0){
				$mil_cli.="cli_adr_cp LIKE '" . $cp . "%'";
			}else{
				$mil_cli.=" OR cli_adr_cp LIKE '" . $cp . "%'";
			}
		}
		$mil_cli.=" )";

		$mil_sus.=" AND (";
		foreach($_SESSION['cli_tri']['cli_dep'] as $k => $cp){
			if($k==0){
				$mil_sus.="sus_adr_cp LIKE '" . $cp . "%'";
			}else{
				$mil_sus.=" OR sus_adr_cp LIKE '" . $cp . "%'";
			}
		}
		$mil_sus.=" )";
	};

	// contact
	if(!empty($_SESSION['cli_tri']['con_fonction'])){
		$mil_cli.=" AND con_fonction=:con_fonction";
		$mil_sus.=" AND sco_fonction=:con_fonction";
		$critere["con_fonction"]=$_SESSION['cli_tri']['con_fonction'];
	};
	if(!empty($_SESSION['cli_tri']['con_mail'])){
		$mil_cli.=" AND NOT ISNULL(con_mail) AND NOT con_mail=''";
		$mil_sus.=" AND NOT ISNULL(sco_mail) AND NOT sco_mail=''";
	};

	// DONNEE EN BASE CLIENT

	// champ a selectionne
	$sql_cli="SELECT DISTINCT con_nom,con_prenom,cli_code,cli_nom,cli_id
	,con_id,con_tel,con_portable,con_mail, cli_classification_categorie, cli_classification_type, cli_adr_cp
	,con_market_all,cma_market,con_market_partiel,con_market_no
	FROM Clients INNER JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client)
	INNER JOIN Contacts ON (Clients.cli_id=Contacts.con_ref_id AND Contacts.con_ref=1)
	LEFT OUTER JOIN Contacts_Marketing ON (Contacts.con_id=Contacts_Marketing.cma_contact AND cma_market=:market_action)";

	// critere auto
	$sql_cli.=" WHERE cso_societe=" . $acc_societe . " AND NOT cso_archive";
	$sql_cli.=" AND (ISNULL(cli_first_facture) OR cli_first_facture =0)";
	if(!empty($acc_agence)){
		$sql_cli.=" AND cso_agence=" . $acc_agence;
	}
	if(!$_SESSION['acces']["acc_droits"][6]){
		$sql_cli.=" AND cso_utilisateur=" . $acc_utilisateur;
	}
	if($mil_cli!=""){
		$sql_cli.=$mil_cli;
	}
	$sql_cli.=" ORDER BY con_nom,con_prenom,cli_code,cli_nom,cli_id;";
	$req = $Conn->prepare($sql_cli);
	if(!empty($critere)){
		foreach($critere as $c => $u){
			$req->bindValue($c,$u);
		}
	};
	$req->execute();
	$clients = $req->fetchAll(PDO::FETCH_ASSOC);

	// DONNEE EN BASE SUSPECT

	if(empty($_SESSION['cli_tri']['cli_groupe'])){

		$sql_sus="SELECT DISTINCT sco_nom as con_nom,sco_prenom as con_prenom,sus_code as cli_code,sus_nom as cli_nom,sus_id
		,sco_tel AS con_tel,sco_portable as con_portable,sco_mail as con_mail,Suspects_Contacts.sco_id AS con_id
		,sco_market_all as con_market_all,scm_market AS con_market,sco_market_partiel AS con_market_partiel, sco_market_no AS con_market_no
		FROM Suspects
		INNER JOIN Suspects_Contacts ON (Suspects.sus_id=Suspects_Contacts.sco_ref_id AND Suspects_Contacts.sco_ref=1)
		LEFT OUTER JOIN Suspects_Contacts_Marketing ON (Suspects_Contacts.sco_id=Suspects_Contacts_Marketing.scm_contact AND scm_market=:market_action)
		LEFT OUTER JOIN Suspects_Correspondances ON (Suspects.sus_id=Suspects_Correspondances.sco_suspect)";

		// critere auto
		$sql_sus.=" WHERE NOT ISNULL(Suspects_Correspondances.sco_suspect)";
		if(!empty($acc_agence)){
			$sql_sus.=" AND sus_agence=" . $acc_agence;
		}
		if(!$_SESSION['acces']["acc_droits"][6]){
			$sql_sus.=" AND sus_utilisateur=" . $acc_utilisateur;
		}
		if($mil_sus!=""){
			$sql_sus.=$mil_sus;
		}
		$sql_sus.=" ORDER BY sco_nom,sco_prenom,sus_code,sus_nom,sus_id;";

		$req = $ConnSoc->prepare($sql_sus);
		if(!empty($critere)){
			foreach($critere as $c => $u){
				$req->bindValue($c,$u);
			}
		};
		$req->execute();
		$suspects = $req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($suspects)){
			$clients =array_merge($clients, $suspects);
			asort($clients);
		}

	}

	/*echo("<pre>");
		print_r($suspects);
	echo("</pre>");
	die();*/


?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">

		<!-- Start: Main -->
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php		if(!empty($clients)){ ?>
						<h1>Liste des contacts</h1>
						<div class="table-responsive">
							<table class="table" id="table_id">
								<thead>
									<tr class="success" >
										<th>ID</th>
										<th>Nom</th>
										<th>Prenom</th>
										<th>Téléphone</th>
										<th>Portable</th>
										<th>Mail</th>
										<th>Code client</th>
										<th>Nom client</th>
										<th>Consentement</th>
										<th>Catégorie de classification</th>
										<th>Type de classification</th>
										<th>Dpt</th>
									</tr>
								</thead>
								<tbody>
						<?php		foreach($clients as $c){ ?>
										<tr>
											<td><?=$c['con_id'] ?></td>
											<td><?=$c['con_nom'] ?></td>
											<td><?=$c['con_prenom'] ?></td>
											<td><?=$c['con_tel']?></td>
											<td><?=$c['con_portable']?></td>
											<td><?=$c['con_mail']?></td>
											<td><?=$c['cli_code']?></td>
											<td><?=$c['cli_nom']?></td>
											<td>
									<?php		if($c['con_market_all'] OR ($c['con_market_partiel'] AND !empty($c['cma_market'])) ){
													echo("OK");
												}elseif($c['con_market_no'] OR ($c['con_market_partiel'] AND empty($c['cma_market'])) ){
													echo("REFUS");
												}else{
													echo("PAS DE REPONSE");
												} ?>
											</td>
											<?php
												if(!empty($c["cli_classification_categorie"])){
													$sql="SELECT ccc_libelle FROM Clients_classifications_categories WHERE ccc_id=" . $c["cli_classification_categorie"] . ";";
													$req=$Conn->query($sql);
													$d_classification_categorie=$req->fetch();
												}

												if(!empty($c["cli_classification_type"])){
													$sql="SELECT cct_libelle FROM Clients_classifications_types WHERE cct_id=" . $c["cli_classification_type"] . ";";
													$req=$Conn->query($sql);
													$d_classification_type=$req->fetch();
												}
											?>
											<td>
												<?php if(!empty($d_classification_categorie)){ ?>
													<?=$d_classification_categorie['ccc_libelle']?>
												<?php }?>
											</td>
											<td>
												<?php if(!empty($d_classification_type)){ ?>
													<?=$d_classification_type['cct_libelle']?>
												<?php }?>
											</td>
											<td><?=substr($c['cli_adr_cp'], 0, 2)?></td>
										</tr>
							<?php	} ?>
								</tbody>
							</table>
						</div>
		<?php 		}else{ ?>
						<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-warning" style="border-radius:0px;">
								Aucun contact correspondant à votre recherche.
							</div>
						</div>
		<?php 		} ?>
				</section>
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="market_prospect_tri.php" class="btn btn-primary btn-sm">
						<span class="fa fa-search"></span>
						<span class="hidden-xs">Nouvelle recherche</span>
					</a>
				</div>
				<div class="col-xs-6 footer-middle">&nbsp;</div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>
<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				$('#table_id').DataTable( {
					"language": {
						"url": "vendor/plugins/DataTables/media/js/French.json"
					},
					"paging": false,
					"searching": false,
					"info": false,
					"order": [[ 1, "asc" ]]
				} );
				 //Disable full page
			    /*$('body').bind('cut copy paste', function (e) {
			        e.preventDefault();
			    });*/
			    //Disable mouse right click
			   /* $("body").on("contextmenu",function(e){
			        return false;
			    });*/
			});
		</script>
	</body>
</html>
