﻿<?php 

session_start(); 

include "includes/connexion.php";
require "modeles/mod_get_user_acces.php";

$erreur=1;

$utilisateur=0;
if(!empty($_POST["utilisateur"])){
	$utilisateur=intval($_POST["utilisateur"]);
}
if(isset($_SESSION["acces"])){
	if(!empty($_SESSION["acces"]["acc_ref"]) AND !empty($_SESSION["acces"]["acc_ref_id"]) AND ($_SESSION["acces"]["acc_profil"]==13 OR $_SESSION["acces"]["acc_ref_id"]==1 OR $_SESSION["acces"]["acc_ref_id"]==191) AND $utilisateur>0){
		$erreur=0;	
	}
}

if($erreur==0){
	
	$d_access=get_user_acces(1,$utilisateur);
	
	if(is_array($d_access)){
		
		$_SESSION["acces"]=$d_access;
		header("location:accueil.php");	
		
	}else{
		$erreur=1;
	}
}

if($erreur==1){
	$_SESSION['message'][] = array(
		"titre" => "Echec",
		"type" => "danger",
		"message" => "Connexion impossible." 
	);
	header("location:index.php");
}
?>