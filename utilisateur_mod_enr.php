<?php
	// MISE A JOUR D'UNE FICHE UTILSATEUR
	
	include "includes/controle_acces.inc.php";
	include_once 'includes/connexion.php';
	include 'modeles/mod_utilisateur.php';
	include 'modeles/mod_acces.php';
	include 'modeles/mod_droit.php';
	include 'modeles/mod_upload.php';
	include 'modeles/mod_societe.php';
	include 'modeles/mod_agence.php';
	include "modeles/mod_competence.php";
	include "modeles/mod_diplome.php";
	
	$erreur=0;
	if(!empty($_POST["uti_id"])){
		$uti_id=$_POST["uti_id"];
	}else{
		$erreur=1;
	}
	
	// droit de la personne connecté
	$acc_utilisateur=0;
	$acc_service=0;
	$acc_profil=0;
	$acc_drt_competence=false;
	if($_SESSION['acces']['acc_ref']==1){
		$acc_utilisateur=$_SESSION['acces']['acc_ref_id'];
		$acc_service=$_SESSION['acces']['acc_service'];
		$acc_profil=$_SESSION['acces']['acc_profil'];
		if(!empty(get_droit_utilisateur(3,$_SESSION['acces']['acc_ref_id']))){
			$acc_drt_competence=true;
		}
	}
	// droit sur le programme
	$ma_fiche=false;	
	$drt_maj_rh=false;
	$drt_maj_compta=false;
	
	if($acc_utilisateur==$uti_id){
		$ma_fiche=true;
	}
	if($acc_profil==13 OR $acc_profil==14){
		$drt_maj_rh=true;
		$drt_maj_compta=true;
	}elseif($acc_service==2){
		$drt_maj_compta=true;
	}elseif($acc_service==4){
		$drt_maj_rh=true;
	}
	
	// variable pour traitement particulier
	$change_profil=false;
	
	if($erreur==0){
		
		// MISE A JOUR DE LA TABLE UTILISATEUR
		
		if($ma_fiche){
			// l'utilisateur modif sa propre fiche
			
			$uti_ad1=$_POST["uti_ad1"];
			$uti_ad2=$_POST["uti_ad2"];
			$uti_ad3=$_POST["uti_ad3"];
			$uti_cp=$_POST["uti_cp"];
			$uti_ville=$_POST["uti_ville"];
			
			$uti_tel=$_POST["uti_tel"];
			$uti_fax=$_POST["uti_fax"];
			$uti_mobile=$_POST["uti_mobile"];
			$uti_mail=$_POST["uti_mail"];
			
			if(isset($_POST["uti_tel_perso"])){
				$uti_tel_perso=$_POST["uti_tel_perso"];
			}
			if(isset($_POST["uti_mobile_perso"])){
				$uti_mobile_perso=$_POST["uti_mobile_perso"];
			}
			if(isset($_POST["uti_mail_perso"])){
				$uti_mail_perso=$_POST["uti_mail_perso"];
			}
			
			$erreur=update_utilisateur_ma_fiche($uti_id,$uti_ad1,$uti_ad2,$uti_ad3,$uti_cp,$uti_ville,$uti_tel,$uti_fax,$uti_mobile,$uti_mail,$uti_tel_perso,$uti_mobile_perso,$uti_mail_perso);
		
		}else if($drt_maj_rh OR $drt_maj_compta){
			
			// modification de la fiche d'un autre autilisateur
			
			$u=get_utilisateur($uti_id);
			$acces=get_acces(1,$uti_id);
			
			// ONGLET GENERAL
			
			if(!empty($_POST["uti_nom"])){
				$uti_nom=$_POST["uti_nom"];
			}else{
				$erreur=1;
			}			
			if(!empty($_POST["uti_prenom"])){
				$uti_prenom=$_POST["uti_prenom"];
			}else{
				$erreur=1;
			}
			if(!$drt_maj_rh)
			{	// le service compta n'a pas la main	
				$uti_profil=$u["uti_profil"];
				$uti_societe=$u["uti_societe"];
			}else{
				if(!empty($_POST["uti_profil"])){
					$uti_profil=$_POST["uti_profil"];
				}else{
					$erreur=1;
				}
				if(!empty($_POST["uti_societe"])){
					$uti_societe=$_POST["uti_societe"];
				}else{
					$erreur=1;
				}
			}
			/*echo("drt_maj_rh : " . $drt_maj_rh . "<br/>");
			echo("drt_maj_compta : " . $drt_maj_compta . "<br/>");
			echo("erreur : " . $erreur . "<br/>");
			die();*/
			if($erreur==0){
				
				$uti_titre=0;
				if(!empty($_POST["uti_titre"])){
					$uti_titre=$_POST["uti_titre"];
				}
				$uti_archive=0;
				if(isset($_POST["uti_archive"])){
					if($_POST["uti_archive"]=="on"){
						$uti_archive=1;	
					}
				}	
				$uti_ad1=$_POST["uti_ad1"];
				$uti_ad2=$_POST["uti_ad2"];
				$uti_ad3=$_POST["uti_ad3"];
				$uti_cp=$_POST["uti_cp"];
				$uti_ville=$_POST["uti_ville"];
		
				$uti_tel=$_POST["uti_tel"];
				$uti_fax=$_POST["uti_fax"];
				$uti_mobile=$_POST["uti_mobile"];
				$uti_mail=$_POST["uti_mail"];
		
				if(!$drt_maj_rh)
				{	// element interdit à la compta
					$uti_tel_perso=$u["uti_tel_perso"];
					$uti_mobile_perso=$u["uti_mobile_perso"];
					$uti_mail_perso=$u["uti_mail_perso"];
				}else{
					$uti_tel_perso=$_POST["uti_tel_perso"];
					$uti_mobile_perso=$_POST["uti_mobile_perso"];
					$uti_mail_perso=$_POST["uti_mail_perso"];	
				}
		
				// donnée de l'onglet droit et acces
			
				if($uti_profil!=$u["uti_profil"]){
					$change_profil=true;
				}
			
			
				// donnée de l'onglet RH
				if(!$drt_maj_rh){
					// donnee interdit au service compta
					
					$acc_uti_ident=$acces["acc_uti_ident"];
					$acc_uti_passe=$acces["acc_uti_passe"]; 
					
					$uti_agence=$u["uti_agence"];
					$uti_matricule=$u["uti_matricule"];
					$uti_contrat=$u["uti_contrat"];
					$uti_fonction=$u["uti_fonction"];
					$uti_responsable=$u["uti_responsable"];
					$uti_service=$u["uti_service"];
					$uti_veh_cv=$u["uti_veh_cv"];
					$uti_tranche_km=$u["uti_tranche_km"];
					$uti_dist_dom=$u["uti_dist_dom"];
					$uti_charge=$u["uti_charge"];
					$uti_charge_dom=$u["uti_charge_dom"];

				}else{				
					$acc_uti_ident=$_POST["acc_uti_ident"];
					$acc_uti_passe=$_POST["acc_uti_passe"]; 
					
					$uti_agence=0;
					if(!empty($_POST["uti_agence"])){
						$uti_agence=$_POST["uti_agence"];
					}
					
					$uti_matricule=$_POST["uti_matricule"]; 
					
					$uti_contrat=0;
					if(!empty($_POST["uti_contrat"])){
						$uti_contrat=$_POST["uti_contrat"];
					}
					
					$uti_fonction=$_POST["uti_fonction"];
					
					$uti_responsable=0;
					if(!empty($_POST["uti_responsable"])){
						$uti_responsable=$_POST["uti_responsable"];
					}
					
					$uti_service=0;
					if(!empty($_POST["uti_service"])){
						$uti_service=$_POST["uti_service"];
					}
					
					$uti_veh_cv=0;
					if(!empty($_POST["uti_veh_cv"])){
						$uti_veh_cv=$_POST["uti_veh_cv"];
					}
					
					$uti_tranche_km=1;
					if(!empty($_POST["uti_tranche_km"])){
						$uti_tranche_km=$_POST["uti_tranche_km"];
					}
					
					$uti_dist_dom=0;
					if(!empty($_POST["uti_dist_dom"])){
						$uti_dist_dom=$_POST["uti_dist_dom"];
					}
					$uti_charge=0;
					if(!empty($_POST["uti_charge"])){
						$uti_charge=$_POST["uti_charge"];
					}
					$uti_charge_dom=0;
					if(!empty($_POST["uti_charge_dom"])){
						$uti_charge_dom=$_POST["uti_charge_dom"];
					}
				}
				// DONNEE DE L'ONGLET COMPTA
			
				if(!$drt_maj_compta){
					// donnée interdit service RH
					$uti_carte_affaire=$u["uti_carte_affaire"];
					$uti_population=$u["uti_population"];
					
				}else{	
					$uti_carte_affaire=0;
					if(isset($_POST["uti_carte_affaire"])){
						if($_POST["uti_carte_affaire"]=="on"){
							$uti_carte_affaire=1;	
						}
					}			
					$uti_population=0;
					if(!empty($_POST["uti_population"])){
						$uti_population=$_POST["uti_population"];
					}
				}
		
				// ON DECLENCHE LA MAJ
				
				if($ma_fiche){
					$erreur=update_utilisateur_ma_fiche($uti_id,$uti_ad1,$uti_ad2,$uti_ad3,$uti_cp,$uti_ville,$uti_tel,$uti_fax,$uti_mobile,$uti_mail,$uti_tel_perso,$uti_mobile_perso,$uti_mail_perso);	
				}else{
					$erreur=update_utilisateur($uti_id,$uti_societe,$uti_agence,$uti_responsable,$uti_profil,$uti_titre,$uti_nom,$uti_prenom,$uti_ad1,$uti_ad2,$uti_ad3,$uti_cp,$uti_ville,$uti_tel,$uti_fax,$uti_mobile,$uti_mail,$uti_tel_perso,$uti_mobile_perso,$uti_mail_perso,$uti_matricule,$uti_archive,$uti_carte_affaire,$uti_tranche_km,$uti_veh_cv,$uti_population,$uti_contrat,$uti_charge,$uti_charge_dom,$uti_dist_dom,$uti_service,$uti_fonction);	
				}
			}
		}
	}
	if($erreur==0 AND $drt_maj_rh){
		
		// MISE A JOUR DE L'ACCES AU SITE
		die();
		if(!empty($acces)){

			update_acces(1,$uti_id,$uti_profil,$acc_uti_ident,$acc_uti_passe,$acces["acc_passe_modif"],$uti_mail);
				
		}elseif(!empty($acc_uti_ident) AND !empty($acc_uti_passe)){
				
			// on cree un accès orion
			
			$jour=mktime(0,0,0,date("m")+1,1,date("Y"));
			
			if(date("N",$jour)!=1){
				$acc_date_modif=date("Y-m-d",mktime(0,0,0,date("m",$jour),date("d",$jour)+(8-date("N",$jour)),date("Y",$jour)));
			}else{
				$acc_date_modif=date("Y-m-d",$jour);
			}
			insert_acces(1,$uti_id,$uti_profil,$acc_uti_ident,$acc_uti_passe,$acc_date_modif,$uti_mail);
		}
		
		// GESTION UPLOAD DE LA CARTE GRISE
		
		if(!empty($_FILES['uti_carte_grise']['name'])){			
			$extension=array("pdf");
			$erreur = upload("uti_carte_grise","utilisateurs/","carte_g_".$uti_id,0,$extension,1);
		}	
		
		// MISE A JOUR DES DROITS
		
		if($change_profil){
			
			// on commence par lui supprimer tout ces droits (car lié à son anacien profil)
			delete_droits_utilisateur($uti_id);
			$droit_par_defaut=get_liste_droit_profil($uti_profil);
		
			if(!empty($droit_par_defaut)){
				
				foreach($droit_par_defaut as $d){
					
					if(!empty($d["udr_droit"])){
						insert_droit_utilisateur($d["udr_droit"],$uti_id,$d["udr_reaffecte"]);
					}
				}
			}
		}
		
		// MISE A JOUR DES SOCIETE ACCESSIBLE
		
		// on commence par supprimer les accès 
		
		delete_utilisateur_societes($uti_id);
		
		$societes=get_societes();
		
		if(!empty($societes)){			
			foreach($societes as $s){			
				if(isset($_POST["societe_" . $s["soc_id"]])){
					insert_acces_societe($uti_id,$s["soc_id"],0);
				}
				// Si la société gere des agences -> on verif les acces			
				if($s["soc_agence"]){
					$agences=get_agence_societe($s["soc_id"]);
					if(!empty($agences)){
						foreach($agences as $a){			
							if(isset($_POST["agence_" . $a["age_id"]]) OR isset($_POST["societe_" . $s["soc_id"]]) ){
								insert_acces_societe($uti_id,$s["soc_id"],$a["age_id"]);								
							}
						}	
					}			
				}
			}			
		}
	}	
	// GESTION DES COMPETENCES	
	if($erreur==0 AND $acc_drt_competence){
		
		$competences=get_competences();
		if(!empty($competences)){
			foreach($competences as $c){
				
				if(isset($_POST["competence_" . $c["com_id"]])){
					
					if(!bool_competence_intervenant(1,$uti_id,$c["com_id"])){
						
						// si l'utilisateur dispose deja de la compétence, il n'y a rien a faire
						
						$ico_controle_dt=1;
						$ico_controle_dt_txt="";
						
						$diplomes=get_diplomes_competence_inter(1,$uti_id,$c["com_id"]);												
						if(!empty($diplomes)){
							foreach($diplomes as $d){
								
								if(empty($d["idi_date_deb"])){
									
									// le diplome n'est pas renseigné
									
									if($ico_controle_dt_txt!=""){
										$ico_controle_dt_txt.="<br/>";
									}
									if($d["dco_obligatoire"]){										
										if($ico_controle_dt<3){
											$ico_controle_dt=3;
										}
										$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est pas renseigné (obligatoire)";
									}else{
										if($ico_controle_dt<2){
											$ico_controle_dt=2;
										}
										$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est pas renseigné";	
									}
									
								}elseif($d["dip_validite"] AND $d["idi_date_fin"]<date){
									
									// le diplome est périodique et il est perimé
									
									if($d["dco_obligatoire"]){										
										if($ico_controle_dt<3){
											$ico_controle_dt=3;
										}
										$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est plus valide (obligatoire)";
									}else{
										if($ico_controle_dt<2){
											$ico_controle_dt=2;
										}
										$ico_controle_dt_txt.="Le diplôme " . $d["dip_libelle"] . " n'est plus valide";	
									}
								}
							}
						}
						insert_competence_intervenant(1,$uti_id,$c["com_id"],$ico_controle_dt,$ico_controle_dt_txt);							
					}								
				}else{
					delete_competence_intervenant(1,$uti_id,$c["com_id"]);
				}
			}
		}
		
		
	}
	// FIN GESTION COMPETENCES
	
	if($ma_fiche){
		$url_retour="utilisateur_voir.php?utilisateur=" . $uti_id;	
	}else{
		$url_retour="utilisateur_liste.php?erreur=" . $erreur;		
	}
	if($erreur>0){
		if(strpos($url_retour,"?")===FALSE){
			$url_retour.="?erreur=" . $erreur; 
		}else{
			$url_retour.="&erreur=" . $erreur; 
		}
	}
	
	header("location: " .$url_retour);
?>

