<?php

session_start();

include_once 'includes/connexion.php';
include_once 'modeles/mod_set_new_passe.php';

$erreur=1;

if(isset($_POST)){
	
	if(!empty($_POST["mail"])){
	
		$req = $Conn->prepare("SELECT acc_mail,acc_ref,acc_ref_id FROM Acces WHERE acc_mail=:acc_uti_email AND NOT acc_archive");
		$req->bindParam(':acc_uti_email', $_POST["mail"]);
		$req->execute();
		$d_acces = $req->fetch();
		if(!empty($d_acces)){
			
			if($d_acces["acc_ref"]==1){
				$modele_mail="mdp_recup";
			}else{
				$modele_mail="mdp_new";
			}
			$password=set_new_passe($d_acces["acc_ref"],$d_acces["acc_ref_id"],$d_acces["acc_mail"],$modele_mail);
			var_dump($password);
			if(is_string($password)){
				$erreur=0;	
			}
		}
	}
}
if($erreur==0){
	
	$_SESSION['message'][] = array(
		"titre" => "Mot de passe",
		"type" => "success",
		"message" => "Un mot de passe vient de vous être envoyé." 
	);
	
}else{
	$_SESSION['message'][] = array(
		"titre" => "Mot de passe",
		"type" => "danger",
		"message" => "Votre compte n'a pas pu être récupéré." 
	);
}
header('Location: index.php');


