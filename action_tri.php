 <?php
	$menu_actif = "2-4";
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");
	include 'modeles/mod_get_pro_categories.php';
	include 'modeles/mod_get_pro_familles.php';

	// ECRAN DE TRI DES ACTIONS
	
	// annul requete précédent
	if(isset($_SESSION["sql"])){
		unset($_SESSION["sql"]);
	}
	if(isset($_SESSION["sql_gc"])){
		unset($_SESSION["sql_gc"]);
	}
	if(isset($_SESSION["sql_param"])){
		unset($_SESSION["sql_param"]);
	}
	
	// personne connecté
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];	
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	
	$_SESSION["retour_action"]="action_tri.php";
	
	// LES COMMERCIAUX
	
	$sql="SELECT com_id,com_label_1,com_label_2 FROM commerciaux";
	$mil="";
	if($_SESSION['acces']['acc_profil'] == 3 && !$_SESSION['acces']["acc_droits"][6]){
		$mil=" AND com_ref_1 = " . $_SESSION['acces']['acc_ref_id'];
	}
	if($acc_agence>0){
		$mil.=" AND com_agence= " . $acc_agence;
	}
	$mil.=" AND com_archive= 0";
	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY com_label_1,com_label_2";
	$req = $ConnSoc->query($sql);
	$d_commercial=$req->fetchAll();
	
	// LES INTERVENANTS
	
	$sql="SELECT int_id,int_label_1,int_label_2 FROM Intervenants";
	if($acc_agence>0){
		$sql.=" WHERE (int_agence= " . $acc_agence . " OR int_agence=0)";
	}
	$sql.=" ORDER BY int_label_1,int_label_2";
	$req = $ConnSoc->query($sql);
	$d_intervenant=$req->fetchAll();
	
	// PERIODE PAR DEFAUT
	$date = new DateTime();
    $dateDeb = $date -> format('01/m/Y');
    $dateFin = $date -> format('t/m/Y');
	
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion - Recherche client</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<!-- PLUGIN -->
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css">
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css">
		
		<!-- PERSO Si2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
	   
	   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="action_liste.php">
			<div id="main">
    <?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					

					<section id="content" class="animated">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">						
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											
											<div class="content-header">
												<h2>Recherche d'<b class="text-primary title-suscli">actions</b></h2>
											</div>
											<div class="col-md-10 col-md-offset-1">	
												<div class="row" >
													<div class="col-md-2">	
														<div class="section" >
															<label for="act_id" >ID Formation :</label>
															<input type="text" id="act_id" name="act_id" class="gui-input" placeholder="" value="" />																											
														</div>	
													</div>
													<div class="col-md-1 text-center pt25"> - </div>
													<div class="col-md-2">	
														<div class="section" >														
															<label for="acl_id" >ID Inscription :</label>
															<input type="text" id="acl_id" name="acl_id" class="gui-input" placeholder="" value="" />																												
														</div>	
													</div>													
												</div>
												<div class="row" >
													<div class="col-md-6">	
														<div class="section" >
															<label for="commercial" >Action entre le :</label>
															<div class="field prepend-icon">
																<input type="text" id="act_date_deb" name="act_date_deb" class="gui-input datepicker" placeholder="" value="<?=$dateDeb?>" />												
																<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
															</div>
														</div>	
													</div>
													<div class="col-md-6">	
														<div class="section" >
															<label for="commercial" >et le :</label>
															<div class="field prepend-icon">
																<input type="text" id="act_date_fin" name="act_date_fin" class="gui-input datepicker" placeholder="" value="<?=$dateFin?>" />												
																<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
															</div>
														</div>	
													</div>													
												</div>
												<div class="row" >
													<div class="col-md-12" >
														<div class="section text-left" >															
															<label for="client" >Client :</label>
															<select name="client" id="client" class="select2-client-n" >
																<option value="0" >Client</option>					
															</select>																	
														</div>
													</div>
												</div>	
												<div class="row" >
													<div class="col-md-6" >		
														<div class="section" >
															<label for="commercial" >Commercial :</label>
															<select name="commercial" id="commercial" class="select2" >
																<option value="0" selected="selected" >commercial</option>	
													<?php		if(!empty($d_commercial)){
																	foreach($d_commercial as $c){
																		echo("<option value='" . $c["com_id"] . "' >" . $c["com_label_1"] . " " . $c["com_label_2"] . "</option>");
																	}
																}?>																			
															</select>
														</div>
													</div>
													<div class="col-md-6" >		
														<div class="section" >
															<label for="intervenant" >Intervenant :</label>
															<select name="intervenant" id="intervenant" class="select2" >
																<option value="0" selected="selected" >intervenant</option>	
													<?php		if(!empty($d_intervenant)){
																	foreach($d_intervenant as $c){
																		echo("<option value='" . $c["int_id"] . "' >" . $c["int_label_1"] . " " . $c["int_label_2"] . "</option>");
																	}
																}?>																			
															</select>
														</div>
													</div>
												</div>
												<div class="row">
												<div class="col-md-3" >
														<label for="cli_code" >Code :</label>
														<input type="text" name="cli_code" class="gui-input" id="cli_code" placeholder="Code" />	
													</div>
												</div>
												<div class="section-divider mb40" >
													<span>Formation</span>
												</div>
												<div class="row" >
													<div class="col-md-4" >		
														<div class="section" >
															<label for="type" >Type :</label>
															<select name="type" id="type" class="select2" >
																<option value="0" selected="selected" >Type</option>	
																<option value="1" >INTRA</option>
																<option value="2" >INTER</option>
															</select>
														</div>
													</div>
													
													<!--<div class="col-md-6" >		
														<div class="section" >														
															<select name="categorie" id="categorie" class="select2" >
																<option value="0" selected="selected" >catégorie :</option>	
													<?php		$categories=get_pro_categories(1);
																if(!empty($categories)){
																	foreach($categories as $c){
																		echo("<option value='" . $c["pca_id"] . "' >" . $c["pca_libelle"] . "</option>");
																	}
																}?>																			
															</select>
														</div>
													</div>-->													
												</div>
												<div class="row" >
													<div class="col-md-4" >		
														<div class="section" >
															<label for="famille" >Famille :</label>
															<select name="famille" id="famille" class="select2 select2-famille" >
																<option value="0" selected="selected" >famille</option>	
													<?php		$familles=get_pro_familles();
																if(!empty($familles)){
																	foreach($familles as $f){
																		echo("<option value='" . $f["pfa_id"] . "' >" . $f["pfa_libelle"] . "</option>");
																	}
																}?>																			
															</select>
														</div>
													</div>	
													<div class="col-md-4" >		
														<div class="section" >
															<label for="sous_famille" >Sous-Famille :</label>
															<select name="sous_famille" id="sous_famille" class="select2 select2-famille-sous-famille" >
																<option value="0" selected="selected" >sous-famille</option>				
															</select>
														</div>
													</div>	
													<div class="col-md-4" >		
														<div class="section" >	
															<label for="sous_sous_famille" >Sous Sous-Famille :</label>
															<select name="sous_sous_famille" id="sous_sous_famille" class="select2 select2-famille-sous-sous-famille" >
																<option value="0" selected="selected" >sous-famille</option>				
															</select>
														</div>
													</div>													
												</div>
												<div class="section-divider mb40" >
													<span>Lieu d'intervention</span>
												</div>
												<div class="row" >
													<div class="col-md-4" >		
														<div class="section" >														
															<input type="text" name="cp" class="gui-input code-postal" placeholder="CP" />
														</div>
													</div>	
													<div class="col-md-8" >		
														<div class="section" >														
															<input type="text" name="ville" class="gui-input nom" placeholder="Ville" />
														</div>
													</div>														
												</div>

												<div class="section-divider mb40" >
													<span>Blocage administratif</span>
												</div>
												<div class="row" >
													<div class="col-md-3" >													
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_blocage opt1" name="option_blocage" value="1">
																<span class="checkbox"></span>Toutes les actions bloquées
															</label>
														</div>														
													</div>
													<div class="col-md-3" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_blocage opt3" name="option_blocage" value="3">
																<span class="checkbox"></span>Blocage BC
															</label>
														</div>
													</div>	
													<div class="col-md-3" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_blocage opt2" name="option_blocage" value="2">
																<span class="checkbox"></span>Blocage marge
															</label>
														</div>
													</div>	
												</div>	

												<div class="section-divider mb40" >
													<span>Recherche rapide</span>
												</div>
												<div class="row" >
													<div class="col-md-3" >													
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_recherche opt1" name="option_recherche" value="1">
																<span class="checkbox"></span>Actions archivées
															</label>
														</div>
														
													</div>
													<div class="col-md-3" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_recherche opt2" name="option_recherche" value="2">
																<span class="checkbox"></span>Actions non-confirmées
															</label>
														</div>
													</div>												
													<div class="col-md-3" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_recherche opt3" name="option_recherche" value="3">
																<span class="checkbox"></span>Actions sans client
															</label>
														</div>
													</div>
													<div class="col-md-3" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_recherche opt4" name="option_recherche" value="4">
																<span class="checkbox"></span>Alertes actives
															</label>
														</div>
													</div>
													<div class="col-md-3 mt15" >
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_recherche opt5" name="option_recherche" value="5">
																<span class="checkbox"></span>Actions sans tarif
															</label>
														</div>
													</div>														
													<div class="col-md-3 mt15" >															
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" class="option_recherche opt6" name="option_recherche" value="6">
																<span class="checkbox"></span>Actions non facturable
															</label>
														</div>
														
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">

					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right">
						<button type="submit" name="search" class="btn btn-primary btn-sm">
							<i class='fa fa-search'></i> Rechercher
						</button>

					</div>
				</div>
			</footer>
		</form>

<?php	
		include "includes/footer_script.inc.php"; ?>	
		<script src="assets/admin-tools/admin-forms/js/jquery-ui-monthpicker.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
		<script type="text/javascript" src="assets/js/custom.js"></script>
		<script type="text/javascript">

			jQuery(document).ready(function (){
				$(".option_recherche").click(function(){
					if($(this).prop("checked")){
						val=$(this).val();
						$(".option_recherche").each(function( index ){
							if($(this).val()!=val){
								$(this).prop("checked",false);
							}
						});
					}
				});

				$(".option_blocage").click(function(){
					if($(this).prop("checked")){
						val=$(this).val();
						$(".option_blocage").each(function( index ){
							if($(this).val()!=val){
								$(this).prop("checked",false);
							}
						});
					}
				});
				
				$("#act_date_deb").change(function(){
					/*part_date=$(this).val().split("/");
					part_j=parseInt(part_date[0]);
					part_m=parseInt(part_date[1])+1;
					part_y=parseInt(part_date[2]);
					
					var dAujourdhui = new Date("2019-04-01");
					
					alert(dAujourdhui.getDate()-1 + "/" + dAujourdhui.getMonth() + "/" + dAujourdhui.getYear());
					//
					//dAujourdhui.setMonth(1);			
					//dAujourdhui.setDate(-1);
					
					//alert(dAujourdhui.getDate() + "/" + dAujourdhui.getMonth() + "/" + dAujourdhui.getYear());
					//alert(dAujourdhui);*/
					
				});
			});
			
					
		</script>
	</body>
</html>
