<?php
	include "includes/controle_acces.inc.php";
	include("includes/connexion.php");
	include("modeles/mod_profil.php");
	include("modeles/mod_droit.php");
	include("modeles/mod_service.php");
	
	$filt_profil=0;
	if(!empty($_POST["filt_profil"])){
		$filt_profil=$_POST["filt_profil"];
	};
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Profils et droits</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link href="assets/admin-tools/admin-forms/css/admin-forms.css" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
		
		<?php	
				include "includes/header_def.inc.php";
			?>	
				
			
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn">
				
					<div class="row mb25" >
						<div class="col-md-4 col-md-offset-4" >						
							<form method="post" action="droit_profil_voir.php" id="form_filtre" >	
							
								<label for="filt_profil">Profil :</label>
								<select name="filt_profil" id="filt_profil" class="form-control" onchange="choixProfil();" >								
									<option value="0" >Sélectionnez un profil pour afficher les droits par défaut</option>
									<?=get_profil_select($filt_profil)?>																		
								</select>	
								
							</form>							
						</div>					
					</div>
				<?php
					if($filt_profil>0){ ?>
					
						<h4>Droits par défaut attribués au profil "<?=get_profil_nom($filt_profil)?>"</h4>
						
				<?php	
						$service=get_services();
						
						foreach($service as $value){
							
							$droit_profil=get_liste_droit_profil($filt_profil,$value["ser_id"]);					
							if(!empty($droit_profil)){ ?>
							
								<div class="panel" >
									<div class="panel-heading" >
										<span class="panel-icon" ><i class="fa fa-users" ></i></span>
										<span class="panel-tittle" >Droits <?=$value["ser_libelle"]?></span>
										<span class="panel-controls">
											<a class="panel-control-collapse" href="#"></a>
										</span>
									</div>
									<div class="panel-body" >
										<div class="admin-form" >
											<table class="table" >
												<thead>
													<tr>
														<th>Accès</th>
														<th>Réaffectation</th>
														<th colspan="2" >&nbsp;</th>
													</tr>
												</thead>
												<tbody>
								<?php				foreach($droit_profil as $droit){ 
														
														$droit_affecte=0;
														$droit_reaffecte=0;
														if(!empty($droit["udr_droit"])){
															$droit_affecte=1;															
														}
														if(!empty($droit["udr_reaffecte"])){
															$droit_reaffecte=1;															
														}?>
														<tr>
															<td>															
																<span class="switch switch-success">
																	<input class="droit_affecte" id="drt_<?=$droit["drt_id"]?>" type="checkbox" <?php if($droit_affecte==1) echo("checked") ?> data-droit="<?=$droit["drt_id"]?>" >
																	<label data-off="NON" data-on="OUI" for="drt_<?=$droit["drt_id"]?>"></label>												
																</span>																							
															</td>
															<td>
																
																<span class="switch switch-success">																
																	<input class="droit_reaffecte" id="affect_<?=$droit["drt_id"]?>" type="checkbox" <?php if($droit_reaffecte==1) echo("checked");if($droit_affecte==0) echo("disabled"); ?> data-droit="<?=$droit["drt_id"]?>" >
																	<label data-off="NON" data-on="OUI" for="affect_<?=$droit["drt_id"]?>"></label>												
																</span>															
															</td>
															<td><b><?=$droit["drt_nom"]?></b></td>
															<td><?=$droit["drt_descriptif"]?></td>															
														</tr>
								<?php 				} ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>								
				<?php		};
						};
					}; ?>		
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left" ></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >
				</div>
				<div class="col-xs-3 footer-right" >
				</div>
			</div>
		</footer>
		
		<div id="ele"></div>
		
		<!-- BEGIN: PAGE SCRIPTS -->
<?php
		include "includes/footer_script.inc.php"; ?>			
		<script type="text/javascript">
			jQuery(document).ready(function() {
				$('.admin-panels').adminpanel();
				
				$('.droit_affecte').click(function()
				{	var droit = $(this).data('droit');	
					var coche = $(this).is(':checked');
					$.ajax({
						url : 'ajax/ajax_droit.php?droit=' + droit + '&profil=<?=$filt_profil?>&action=1',
						type : 'GET',
						//data : 'droit=' + droit + '&profil=<?=$filt_profil?>&action=1', 
						dataType : 'JSON',
						
						success : function(data){
							if(!coche){							
								$("#affect_" + droit).prop('checked', false);
								$("#affect_" + droit).prop('disabled', true);
							}else{
								$("#affect_" + droit).prop('disabled', false);
							};	 
						},
						error : function(data){	
							afficher_message("Erreur","danger",data.responseText)
							if(coche){
								$("#drt_" + droit).prop('checked',false);
							}else{
								$("#drt_" + droit).prop('checked',true);		
							};							
						}
					});									
				});
				
				$('.droit_reaffecte').click(function()
				{	var droit = $(this).data('droit');	
					var coche = $(this).is(':checked');	
					if(droit>0){
						$.ajax({
							url : 'ajax/ajax_droit.php?droit=' + droit + '&profil=<?=$filt_profil?>&action=2',
							type : 'GET',				
							dataType : 'html',
							success : function(code_html, statut){
	
							},
							error : function(resultat, statut, erreur){	
								if(coche){
									$("#affect_" + droit).prop('checked',false);
								}else{
									$("#affect_" + droit).prop('checked',true);		
								};						
							}						
						});	
					};
				});
							
			});
			(jQuery);
		
			
		</script>
		<script type="text/javascript">
			function choixProfil(){
				document.getElementById("form_filtre").submit();
			}
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
