<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "modeles/mod_parametre.php";
	
	// LISTE DES ATTESTATIONS

	if(isset($_POST["filt_valide"])){
		if($_POST["filt_valide"]!=""){
			$filt_valide=intval($_POST["filt_valide"]);
		}else{
			$filt_valide="";
		}
		$_SESSION["filt_valide"]=$filt_valide;
		
		if($_POST["filt_famille"]!=""){
			$filt_famille=intval($_POST["filt_famille"]);
		}else{
			$filt_famille=0;
		}
		$_SESSION["filt_famille"]=$filt_famille;
		
		if($_POST["filt_sous_famille"]!=""){
			$filt_sous_famille=intval($_POST["filt_sous_famille"]);
		}else{
			$filt_sous_famille=0;
		}
		$_SESSION["filt_sous_famille"]=$filt_sous_famille;
		
		if($_POST["filt_sous_sous_famille"]!=""){
			$filt_sous_sous_famille=intval($_POST["filt_sous_sous_famille"]);
		}else{
			$filt_sous_sous_famille=0;
		}
		$_SESSION["filt_sous_sous_famille"]=$filt_sous_sous_famille;
		
	}elseif(isset($_SESSION["filt_valide"])){
		
		$filt_valide=$_SESSION["filt_valide"];
		$filt_famille=$_SESSION["filt_famille"];
		$filt_sous_famille=$_SESSION["filt_sous_famille"];
		$filt_sous_sous_famille=$_SESSION["filt_sous_sous_famille"];
		
	}else{
		$filt_valide="";
		$filt_famille=0;
		$filt_sous_famille=0;
		$filt_sous_sous_famille=0;
		$_SESSION["filt_valide"]=$filt_valide;
		$_SESSION["filt_famille"]=$filt_famille;
		$_SESSION["filt_sous_famille"]=$filt_sous_famille;
		$_SESSION["filt_sous_sous_famille"]=$filt_sous_sous_famille;
	}
	
	$sql="SELECT att_id,att_titre,att_statut,att_date_creation,att_date_prod,att_date_archive,att_contenu 
	,pfa_libelle,psf_libelle,pss_libelle
	FROM attestations LEFT OUTER JOIN Produits_Familles ON (attestations.att_famille=Produits_Familles.pfa_id)
	LEFT OUTER JOIN Produits_Sous_Familles ON (attestations.att_sous_famille=Produits_Sous_Familles.psf_id)
	LEFT OUTER JOIN Produits_Sous_Sous_Familles ON (attestations.att_sous_sous_famille=Produits_Sous_Sous_Familles.pss_id)";
	$mil="";
	if(is_int($filt_valide)){
		$mil.=" AND att_statut=" . $filt_valide;
	}
	if(is_int($filt_famille)){
		if($filt_famille>0){
			$mil.=" AND att_famille=" . $filt_famille;
		}
	}
	if(is_int($filt_sous_famille)){
		if($filt_sous_famille>0){
			$mil.=" AND att_sous_famille=" . $filt_sous_famille;
		}
	}
	if(is_int($filt_sous_sous_famille)){
		if($filt_sous_sous_famille>0){
			$mil.=" AND att_sous_sous_famille=" . $filt_sous_sous_famille;
		}
	}
	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY att_id;";
	$req=$Conn->query($sql);
	$attestations=$req->fetchAll();
	
	$_SESSION["retour"]="attestation_liste.php";
	
	// LES FAMILLES DE PRODUITS
	
	$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles ORDER BY pfa_libelle;";
	$req=$Conn->query($sql);
	$d_familles=$req->fetchAll();
	
	// SOUS - FAMILLE
	if(!empty($filt_famille)){
		$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles WHERE psf_pfa_id=" . $filt_famille . " ORDER BY psf_libelle;";
		$req=$Conn->query($sql);
		$d_sous_familles=$req->fetchAll();
	}
	
	// SOUS SOUS FAMILLE
	if(!empty($filt_sous_famille)){
		$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles WHERE pss_psf_id=" . $filt_sous_famille . "  ORDER BY pss_libelle;";
		$req=$Conn->query($sql);
		$d_sous_sous_familles=$req->fetchAll();
	}
	
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Si2P</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Theme CSS -->
		<!-- Theme CSS PAR DEFAUT -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<!-- PERSONALISATION DU THEME -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">


		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm onload-check" style="min-height:0px;">
	
		<!-- Start: Main -->
		<div id="main">
			
	<?php	require "includes/header_def.inc.php"; ?>
			
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">
				<!-- DEBUT DU CONTENU -->
				<section id="content" class="" >
					<div class="admin-form theme-primary">
						<div class="panel heading-border panel-info">
							<div class="panel-heading">
								<span class="panel-title">Filtrer</span>
							</div>
							<div class="panel-body bg-light">
								<form action="attestation_liste.php" method="post" >
									<div class="row" >
										<div class="col-md-3" >									
											<label for="filt_famille">Famille :</label>
											<select id="filt_famille" name="filt_famille" class="select2 select2-famille" >
												<option value="" <?php if($filt_famille==0) echo("selected"); ?> >Famille</option>
									<?php		foreach($d_familles as $fam){ ?>
													<option value="<?=$fam["pfa_id"]?>" <?php if($filt_famille==$fam["pfa_id"]) echo("selected"); ?> ><?=$fam["pfa_libelle"]?></option>
									<?php		} ?>
											</select>	
										</div>
										<div class="col-md-3" >										
											<label for="filt_sous_famille">Sous-Famille :</label>
											<select id="filt_sous_famille" name="filt_sous_famille" class="select2 select2-famille-sous-famille" >
												<option value="" <?php if($filt_sous_famille==0) echo("selected"); ?> >Sous-Famille</option>
									<?php		if(isset($d_sous_familles)){
													foreach($d_sous_familles as $s_fam){ ?>
														<option value="<?=$s_fam["psf_id"]?>" <?php if($filt_sous_famille==$s_fam["psf_id"]) echo("selected"); ?> ><?=$s_fam["psf_libelle"]?></option>
									<?php			}
												} ?>
											</select>	
										</div>
										<div class="col-md-3" >											
											<label for="filt_sous_sous_famille">Sous Sous-Famille :</label>
											<select id="filt_sous_sous_famille" name="filt_sous_sous_famille" class="select2 select2-famille-sous-sous-famille" >
												<option value="" <?php if($filt_sous_sous_famille==0) echo("selected"); ?> >Sous Sous-Famille</option>
									<?php		if(isset($d_sous_sous_familles)){
													foreach($d_sous_sous_familles as $s_s_fam){ ?>
														<option value="<?=$s_s_fam["pss_id"]?>" <?php if($filt_sous_sous_famille==$s_s_fam["pss_id"]) echo("selected"); ?> ><?=$s_s_fam["pss_libelle"]?></option>
									<?php			}
												} ?>
											</select>	
										</div>
										<div class="col-md-2" >											
											<label for="filt_valide">Statut des attestations :</label>
											<select id="filt_valide" name="filt_valide" class="select2" >
												<option value="" <?php if($filt_valide==="") echo("selected"); ?> >Afficher toutes les attestations</option>
												<option value="0" <?php if($filt_valide===0) echo("selected"); ?> >Attestations en pré-productions</option>
												<option value="1" <?php if($filt_valide===1) echo("selected"); ?> >Attestations en productions</option>
												<option value="2" <?php if($filt_valide===2) echo("selected"); ?> >Attestations archivées</option>
											</select>	
										</div>
										<div class="col-md-1" >	
											<button type="submit" class="btn btn-info mt20" >Filtrer</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<h3>Liste des attestations</h3>
								
					<div class="table-responsive">
						<table class="table table-striped" >
							<thead>
								<tr class="dark" >
									<th class="text-center">Titre</th>
									<th class="text-center">Famille</th>
									<th class="text-center">Sous-Famille</th>
									<th class="text-center">Sous Sous-Famille</th>
									<th class="text-center">Date de création</th>
									<th class="text-center">Mise en production</th>
									<th class="text-center">Date d'archivage</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
					<?php 	foreach($attestations as $a){ 
								$classe_attest="";
								switch ($a['att_statut']) {
									case 0:
										$classe_attest="class='primary text-center'";
										break;
									
									case 1:
										$classe_attest="class='success text-center'";
										break;
									case 2:
										$classe_attest="class='danger text-center'";
										break;
								} ?>
								<tr <?=$classe_attest?> >
									<td><?= $a['att_titre'] ?></td>
									<td><?= $a['pfa_libelle'] ?></td>
									<td><?= $a['psf_libelle'] ?></td>
									<td><?= $a['pss_libelle'] ?></td>
									<td><?= convert_date_txt($a['att_date_creation']) ?></td>
									<td><?= convert_date_txt($a['att_date_prod']) ?></td>
									<td><?= convert_date_txt($a['att_date_archive']) ?></td>
									<td>
									<?php if($a['att_statut'] == 0){ ?>
									<a href="attestation.php?id=<?= $a['att_id'] ?>" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Modifier cette attestation"><i class="fa fa-pencil"></i></a>
									<?php } ?>

									<a href="attestation_voir.php?id=<?= $a['att_id'] ?>" class="btn btn-sm btn-info" data-toggle="tooltip" title="Voir cette attestation">
										<i class="fa fa-eye"></i>
									</a>
									 <a href="attestation_dupliquer_enr.php?id=<?= $a['att_id'] ?>" class="btn btn-sm btn-system" data-toggle="tooltip" title="Dupliquer cette attestation"><i class="fa fa-clipboard" aria-hidden="true"></i></a></td>
								</tr>
								<?php } ?>
					
							<tbody>				
						</table>
					</div>
				</section>
				<!-- FIN DU CONTENU -->
			</section>

		</div>
		<!-- End: Main -->
		<!-- DEBUT DU FOOTER -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6" ></div>
				<div class="col-xs-3 footer-right" >
					<a href="attestation.php" class="btn btn-success btn-sm" >
						<i class="fa fa-plus" ></i>
						Nouvelle attestation
					</a>
				</div>
			</div>
		</footer>
		<!-- FIN DE FOOTER  -->

		<!-- BEGIN: PAGE SCRIPTS -->

		<!-- jQuery -->
<?php	include "includes/footer_script.inc.php"; ?>	
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				
			});
		</script>
	</body>
</html>