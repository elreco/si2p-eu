<?php

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_document.php');
include('modeles/mod_parametre.php');
include('modeles/mod_utilisateur.php');

$d = get_document($_GET['id']);
$d_histos = get_document_histo($_GET['id']);

// UTI LIE AU DOC
$req = $Conn->prepare("SELECT uti_id,uti_nom,uti_prenom,dut_obligatoire,dut_vu,dut_vu_le FROM documents_utilisateurs LEFT JOIN Utilisateurs ON (documents_utilisateurs.dut_utilisateur=Utilisateurs.uti_id)
WHERE dut_document = :doc_id ORDER BY uti_nom,uti_prenom");
$req->bindParam(':doc_id',$_GET['id']);
$req->execute();
$d_utis = $req->fetchAll();


?>
<!DOCTYPE html>
<html>
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>SI2P - Orion - Paramètres</title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
  <!--
  <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/chosen/chosen.css">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm">
  <!-- Start: Main -->
  <div id="main">
    <?php
    include "includes/header_def.inc.php";
    ?>
    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper" >

      <section class="animated fadeIn" >
        <div>
          <div class="row">
            <?php if($d['doc_dossier'] == 0): ?>
              <div class="col-md-6"  style="border-right:5px solid #4C4A49;">
                <div class="panel-menu p12 admin-form theme-primary">
                  <div class="row">

                    <div class="col-md-12 text-center">
                     <h2>Historique du fichier <b class="text-primary"><?= $d['doc_nom_aff'] ?></b></h2>
                     <span class="fa fa-circle" style="color:#3f9532;"></span> Document actuel 
                   </div>

                 </div>
               </div>

               <table class="table admin-form table-striped table-condensed text-center">
                <thead>
                  <tr class="dark">

                    <th class="text-center">Nom</th>

                    <th class="text-center">Date</th>
                    <th class="text-center">Déposé par</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $doc_actuel=get_document($_GET['id']); ?>
                  <tr class="success">  
                      <td><?= $doc_actuel['doc_nom'] ?></td>
                      <td><?= convert_date_txt($doc_actuel['doc_date_creation']) ?></td>
                      <?php 	
                            // chercher la dernière personne qui a fait la
                            $uti = get_utilisateur($doc_actuel['doc_utilisateur']); ?>
							<td><?= $uti['uti_prenom'] ?> <?= $uti['uti_nom'] ?></td>
                  </tr>
                  <?php foreach($d_histos as $dh): ?>
                    <tr>

                      <td><?= $dh['dhi_nom']; ?></td>
                      <td><?= convert_date_txt($dh['dhi_date_archive']) ?></td>
                      <?php 	
                            // chercher la dernière personne qui a fait la
                            $uti = get_utilisateur($dh['dhi_utilisateur']); ?>
							<td><?= $uti['uti_prenom'] ?> <?= $uti['uti_nom'] ?></td>
                    </tr>
                  <?php endforeach; ?>




                </tbody>
              </table>
              
            </div>
          <?php endif; ?>
          <div <?php if($d['doc_dossier'] == 0): ?>class="col-md-6"<?php else: ?>class="col-md-12"<?php endif; ?>>
            <div class="panel-menu p12 admin-form theme-primary">
              <div class="row">

                <div class="col-md-12 text-center">
                 <h2>Liste des <b class="text-primary">utilisateurs</b></h2>
               </div>

             </div>
           </div>
           <form <?php if(!empty($_GET['dos'])): ?> action="etech_voir_enr.php?id=<?= $_GET['id'] ?>&dos=<?= $_GET['dos'] ?>" <?php else: ?> action="etech_voir_enr.php?id=<?= $_GET['id'] ?>"<?php endif; ?> method="POST"> 
             <table class="table admin-form table-striped table-condensed text-center">
              <thead>
                <tr class="dark">

                  <th class="text-center">Nom et prénom</th>

                  <th class="text-center">Consulté le</th>
                  <th class="text-center"><label class="option option-success" data-toggle="tooltip" title="Accès">
                    <input type="checkbox" id="selectall" <?php if(!empty($d_utis)): ?>checked<?php endif; ?> >
                    <span class="checkbox mn"></span>
                  </label></th>
                  
                  <th class="text-center"><label class="option option-primary" data-toggle="tooltip" title="Consultation obligatoire">
                    <input type="checkbox" id="selectall2"<?php if(!empty($d_utis) && array_check($d_utis, "dut_obligatoire", 1) == true):  ?> checked <?php endif; ?>>
                    <span class="checkbox mn"></span>
                  </label></th>
                </tr>
              </thead>
				<tbody>
		<?php		foreach($d_utis as $du){ ?>
						<tr>          
							<td><?=$du['uti_nom'] . " " . $du['uti_prenom']?></td>
                    <?php 	if($du['dut_vu'] == 0){ ?>
								<td class="danger">Pas encore consulté</td>
                    <?php 	}else{ ?>
								<td class="success"><?= convert_date_txt($du['dut_vu_le']) ?></td>
                    <?php 	} ?>
							<td class="text-center">
								<label class="option option-success" data-toggle="tooltip" title="Accès">
									<input type="checkbox" class="checkbox1" checked name="check_acc<?= $du['uti_id'] ?>" value="1">
									<span class="checkbox mn"></span>
								</label>
							</td>
							<td class="text-center">
								<label class="option option-primary" data-toggle="tooltip" title="Consultation obligatoire">
									<input type="checkbox" class="checkbox2" <?php if(!empty($du['dut_obligatoire'])) echo("checked") ?> name="check_obl<?= $du['uti_id'] ?>" value="1" />
									<span class="checkbox mn"></span>
								</label>
							</td>
						</tr>
		<?php 		} ?>







              </tbody>
            </table>
            <?php if(empty($d_utis)): ?>

              <div class="col-md-12 text-center" style="padding:0;" >
                <div class="alert alert-warning" style="border-radius:0px;">
                  Pas d'utilisateurs disponibles.
                </div>
              </div>

            <?php endif; ?>
          </div>

        </div>
      </div>


    </section>
    <!-- End: Content WRAPPER -->
  </div>
  <!-- End: Main -->
  <footer id="content-footer" class="affix">
    <div class="row">
      <div class="col-xs-3 footer-left" >
        <a 

        <?php if(isset($_GET['dos'])): ?>
          href="etech.php?dos=<?= $_GET['dos'] ?> " 
        <?php else: ?>
          href="etech.php" 
        <?php endif; ?>

        class="btn btn-default btn-sm">
        <i class="fa fa-long-arrow-left"></i>
        Retour
      </a>
    </div>
    <div class="col-xs-6 footer-middle" ></div>
    <div class="col-xs-3 footer-right" >
      <button type="submit" class="btn btn-success btn-sm">
        <i class='fa fa-save'></i> Enregistrer
      </button>
    </div>
  </div>
</footer>
</form>
<?php
include "includes/footer_script.inc.php"; ?>	
<!------------------------------------- -------------------------------------->

<!-- BEGIN: PAGE SCRIPTS -->


<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<script src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>

<script src="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="vendor/plugins/holder/holder.min.js"></script>
<script src="vendor/plugins/chosen/chosen.jquery.js"></script>
<script src="vendor/plugins/chosen/chosen.proto.js"></script>

<script>
  jQuery(document).ready(function () {

    $(".chosen-select").chosen({width: "100%",  search_contains: true});


    var calcDataTableHeight = function (elt_content, elt_head) {

      return $(elt_content).height() - $(elt_head).height() - 6;
    };
    var tableDefFix = $('#tableDefFix').dataTable({
      "language": {
        "url": "vendor/plugins/DataTables/media/js/French.json"
      },
      paging: false,
      searching: false,
      info: false,
      scrollY: calcDataTableHeight("#tableDefCont", "#tableDefHead"),
      scrollCollapse: true
    });
    $(window).resize(function () {

      var tableDefFixParam = tableDefFix.fnSettings();
      tableDefFixParam.oScroll.sY = calcDataTableHeight("#tableDefCont", "#tableDefHead");
      tableDefFix.fnDraw();

    });
    $(".dataTables_scrollBody").mCustomScrollbar({
      theme: "dark"
    });

    $("#selectall").change(function(){
      $(".checkbox1").prop('checked', $(this).prop("checked"));
    });

    $("#selectall2").change(function(){
      $(".checkbox2").prop('checked', $(this).prop("checked"));
    });

    /* SCRIPT VALIDATION DU FORM */

  });

</script>
</body>
</html>
