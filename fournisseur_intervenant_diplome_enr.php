<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "modeles/mod_competence.php";
	include "modeles/mod_diplome.php";
	include "modeles/mod_upload.php";
	include "modeles/mod_parametre.php";

	$erreur="";
	
	$ref=0;
	if(isset($_POST["ref"])){
		$ref=$_POST["ref"];
	}else{
		$erreur="Formulaire incomplet !";
	}

	$fournisseur=0;
	if(isset($_POST["fournisseur"])){
		$fournisseur=$_POST["fournisseur"];
	}else{
		$erreur="Formulaire incomplet !";
	}
	
	$ref_id=0;
	if(isset($_POST["intervenant"])){
		$ref_id=$_POST["intervenant"];
	}else{
		$erreur="Formulaire incomplet!";
	}
	
	$diplome=0;
	if(!empty($_POST["uti_diplome"])){
		$diplome=$_POST["uti_diplome"];
	}else{
		$diplome=$_POST["diplome"];
	}
	
	$idi_date_fin=null;
	if(!empty($_POST["idi_date_fin"]) && convert_date_sql($_POST["idi_date_fin"])>date("Y-m-d")){
		$idi_date_fin=$_POST["idi_date_fin"];

	}elseif(empty($_POST["idi_date_fin"])){
		$idi_date_fin="";

	}else{
		$erreur="Vous ne pouvez pas charger un diplôme expiré";
	}
	
	/*echo("<pre>");	
	print_r($_POST);
	echo("</pre>");*/
	
	if(empty($erreur)){

		// UPLOAD DU DIPLOME
		if(!empty($_FILES['idi_fichier'])){
			$idi_fichier="diplome_" .$ref_id . "_" . $diplome;
			
			if($ref==2){
				$url_dip="intervenants/diplomes/";
			}
			if(isset($_FILES['idi_fichier']['name'])){
				if(!empty($_FILES['idi_fichier']['name'])){	
					
					$extension=array("pdf");
					$erreur = upload("idi_fichier",$url_dip,$idi_fichier,0,$extension,1);
					if(!empty($erreur)){
						Header("Location:" . $_SESSION['retour'] . "?erreur=" . $erreur);
						die();
					}
				}		
			}
		}
		
	

		// ENREGISTREMENT DU DIPLOME DANS LA BASE
		
		$idi_fichier=$idi_fichier . ".pdf";
		$idi_date_fin=convert_date_sql($idi_date_fin);

		// calcul de la date de validite du diplome
		/*$dip_info=get_diplome($diplome);			
		if(!empty($dip_info)){
			if($dip_info["dip_validite"]>0){
				$idi_date_fin=ajout_date($idi_date_deb,"m",$dip_info["dip_validite"]);
			}
		}*/
		
		if(!empty(get_diplome_intervenant($ref,$ref_id,$diplome))){
			$erreur=update_diplome_intervenant($ref,$ref_id,$diplome,$idi_date_fin,$idi_fichier);
		}else{
			$erreur=insert_diplome_intervenant($ref,$ref_id,$diplome,$idi_date_fin,$idi_fichier);
		}
	
		
		// LE DIPLOME EST ENREGSITRE -> ON MET A JOUR LE STATUT DES COMPETENCES
		if(!empty($_POST["uti_diplome"])){
			// on recupere la liste de toutes les compétences affectées au diplome
			$sql="SELECT * FROM diplomes_competences WHERE dco_externe = 1 AND dco_obligatoire = 1";
			$req=$Conn->query($sql);
			$diplomes_competences = $req->fetchAll();
			
			$add_competence = 1;
			foreach($diplomes_competences as $d){
				$sql="SELECT * FROM intervenants_diplomes WHERE (idi_date_fin > NOW() OR idi_date_fin IS NULL) AND idi_ref=" . $ref . " AND idi_ref_id =" . $ref_id . " AND idi_diplome = " . $d['dco_diplome'];
				$req=$Conn->query($sql);
				$intervenants_diplomes = $req->fetch();
				// on ajoute pas la compétence
				if(!empty($intervenants_diplomes)){
					$no_add_competence[$d['dco_competence']]=1;
				}else{
					$no_add_competence[$d['dco_competence']]=0;
				}
			}

			foreach($no_add_competence as $k=>$n){
				if($n == 1){
					$req = $Conn->prepare("SELECT * FROM  Intervenants_Competences WHERE ico_ref = :ico_ref AND ico_ref_id=:ico_ref_id AND ico_competence=:ico_competence");
					$req -> bindParam(":ico_ref",$ref);
					$req -> bindParam(":ico_ref_id",$ref_id);
					$req -> bindParam(":ico_competence",$k);
					$req->execute();
					$comp = $req->fetch();
					if(empty($comp)){
						$req = $Conn->prepare("INSERT INTO Intervenants_Competences (ico_ref, ico_ref_id, ico_competence)
						VALUES (:ico_ref, :ico_ref_id, :ico_competence)");
						$req -> bindParam(":ico_ref",$ref);
						$req -> bindParam(":ico_ref_id",$ref_id);
						$req -> bindParam(":ico_competence",$k);
						$req->execute();
					}
					
				}
			}

		}else{
			// on recupere la liste de toutes le competense affecte a l'intervenant
			$competences=get_competences_intervenant($ref,$ref_id,1);
			if(!empty($competences)){
				
				foreach($competences as $c){
					
					// pour chaque competence on controle la presence des diplomes 

					$diplomes=get_diplomes_competence_inter($ref,$ref_id,$c["com_id"]);		
					$delete_competence=0;
					if(!empty($diplomes)){
						
						foreach($diplomes as $d){
							
							
							
							if(empty($d["idi_diplome"])){
								
								// le diplome n'est pas renseigné
								
								
								if($d["dco_obligatoire"]){										
									$delete_competence = 1;
								}
								
							}elseif($d["idi_date_fin"]<date("Y-m-d")){
								
								// le diplome est périodique et il est perimé
								
								if($d["dco_obligatoire"]){										
									$delete_competence = 1;
								}
							}						
						}
					}
					if($delete_competence==1){
						delete_competence_intervenant($ref,$ref_id,$c["com_id"]);
					}
					
					
				}
			}
		}
		
	}

	$retour="fournisseur_voir.php?fournisseur=" . $fournisseur . "&intervenant=" . $ref_id . "&tab=5";
	
	/*echo($erreur);
	die();*/
	
	if(!empty($erreur)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur !",
			"type" => "danger",
			"message" => $erreur
		);
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Ajout d'un diplôme",
			"type" => "success",
			"message" => "Le diplôme de l'intervenant a bien été enregistré"
		);
	}
	header("location : " . $retour);

?>
