<?php 

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');

// SUPPRESSION COMPLETE D'UNE ACTION

// NE JAMAIS TRANSFERER SUR LE SERVEUR!!!

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}

$erreur=0;

$action_id=0;
if(!empty($_GET["action_id"])){
	$action_id=intval($_GET["action_id"]);
}

if(empty($action_id) OR empty($acc_societe)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();
	
}else{
	
	var_dump($conn_soc_id);
	
	$sql_del_action="DELETE FROM Actions WHERE act_id=:action;";
	$req_del_action=$ConnSoc->prepare($sql_del_action);
	$req_del_action->bindParam(":action",$action_id);
	try{
		$req_del_action->execute();
	}catch(Exception $e){
		echo($e->getMessage());
		die();
	}
	
	
	// REQ PREP
	
	$sql_del_act_cli_date="DELETE FROM Actions_Clients_Dates WHERE acd_action_client=:action_client;";
	$req_del_act_cli_date=$ConnSoc->prepare($sql_del_act_cli_date);
	
	$sql_del_action_cli="DELETE FROM Actions_Clients WHERE acl_action=:action AND acl_id=:action_client;";
	$req_del_action_cli=$ConnSoc->prepare($sql_del_action_cli);
	
	
	$sql_get_acl="SELECT acl_id FROM Actions_Clients WHERE acl_action=:action;";
	$req_get_acl=$ConnSoc->prepare($sql_get_acl);
	$req_get_acl->bindParam(":action",$action_id);
	$req_get_acl->execute();
	$d_actions_clients=$req_get_acl->fetchAll();
	if(!empty($d_actions_clients)){
		
		foreach($d_actions_clients as $action_cli){
			
			$req_del_act_cli_date->bindParam(":action_client",$action_cli["acl_id"]);
			$req_del_act_cli_date->execute();
			
		}
		
		$req_del_action_cli->bindParam(":action",$action_id);
		$req_del_action_cli->bindParam(":action_client",$action_cli["acl_id"]);
		$req_del_action_cli->execute();
		
		
	}
	
	$sql_del_act_ses="DELETE FROM Actions_Sessions WHERE ase_action=:action;";
	$req_del_act_ses=$ConnSoc->prepare($sql_del_act_ses);
	$req_del_act_ses->bindParam(":action",$action_id);
	$req_del_act_ses->execute();
	
	$sql_del_act_sta="DELETE FROM Actions_Stagiaires WHERE ast_action=:action;";
	$req_del_act_sta=$ConnSoc->prepare($sql_del_act_sta);
	$req_del_act_sta->bindParam(":action",$action_id);
	$req_del_act_sta->execute();
	
	$sql_del_act_sta_ses="DELETE FROM Actions_Stagiaires_Sessions WHERE ass_action=:action;";
	$req_del_act_sta_ses=$ConnSoc->prepare($sql_del_act_sta_ses);
	$req_del_act_sta_ses->bindParam(":action",$action_id);
	$req_del_act_sta_ses->execute();
	
	// SUPP DES DATES
	
	$sql_del_act_date="DELETE FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=:action;";
	$req_del_act_date=$ConnSoc->prepare($sql_del_act_date);
	$req_del_act_date->bindParam(":action",$action_id);
	$req_del_act_date->execute();
	
	
	// MAJ DES DEVIS LIEE
	
	$sql_up_devis="UPDATE Devis_lignes SET dli_action=0,dli_action_client=0,dli_action_cli_soc=0 WHERE dli_action=:action;";
	$req_up_devis=$ConnSoc->prepare($sql_up_devis);
	$req_up_devis->bindParam(":action",$action_id);
	$req_up_devis->execute();

	echo("TERMINE");
	die();
}

?>