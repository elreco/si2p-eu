<?php 

// AFFICHE LA LISTE DES FACTURES DEPUIS LE TABLEAU DE REMUNERATION

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


// DONNEE UTILE AU PROGRAMME

// personne connecté
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	} 
}

// param get
$utilisateur=0;
if(isset($_GET["utilisateur"])){
	if(!empty($_GET["utilisateur"])){
		$utilisateur=intval($_GET["utilisateur"]);
	}
}

$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}
if($exercice==0){
	if(date("n")<4){
		$exercice=date("Y")-1;
	}else{
		$exercice=date("Y");
	}
}
$exercice_fin=$exercice+1;

$mois=0;
if(isset($_GET["mois"])){
	if(!empty($_GET["mois"])){
		$mois=intval($_GET["mois"]);
	}
}
$trimestre=0;
if(isset($_GET["trimestre"])){
	if(!empty($_GET["trimestre"])){
		$trimestre=intval($_GET["trimestre"]);
	}
}

$categorie="";
if(isset($_GET["categorie"])){
	if(!empty($_GET["categorie"])){
		$categorie=$_GET["categorie"];
	}
}
$famille=0;
if(isset($_GET["famille"])){
	if(!empty($_GET["famille"])){
		$famille=intval($_GET["famille"]);
	}
}
$s_famille=0;
if(isset($_GET["s_famille"])){
	if(!empty($_GET["s_famille"])){
		$s_famille=intval($_GET["s_famille"]);
	}
}
$s_s_famille=0;
if(isset($_GET["s_s_famille"])){
	if(!empty($_GET["s_s_famille"])){
		$s_s_famille=intval($_GET["s_s_famille"]);
	}
}

$ca=0; // 1 réalise 2 Contentieux
if(isset($_GET["ca"])){
	if(!empty($_GET["ca"])){
		$ca=intval($_GET["ca"]);
	}
}


// init var

$erreur_txt="";

// CONTROLE ACCES

if($_SESSION["acces"]["acc_droits"][26]){
	if($utilisateur>0 AND $exercice>0){
		$sql="SELECT uti_nom,uti_prenom,uti_profil,uti_agence,uti_societe FROM Utilisateurs LEFT JOIN Utilisateurs_Societes 
		ON (Utilisateurs.uti_societe=Utilisateurs_Societes.uso_societe AND Utilisateurs.uti_agence=Utilisateurs_Societes.uso_agence)
		WHERE uti_id=" . $utilisateur . " AND uso_utilisateur=" . $acc_utilisateur;
		if($_SESSION['acces']["acc_profil"]==3){
			// le com de voit que sa rem
			$sql.=" AND uti_id=" . $acc_utilisateur;
		}elseif($_SESSION['acces']["acc_profil"]==15){
			// le RA voit sa rem et celle des com
				$sql.=" AND (uti_id=" . $acc_utilisateur . " OR uti_profil=3)";
		}elseif($_SESSION['acces']["acc_profil"]==10){
			// responsable d'exploitation
			$sql.=" AND (uti_id=" . $acc_utilisateur . " OR uti_profil IN (3,15) )";
			if($acc_utilisateur!=3){
				$sql.=" AND uti_societe!=4";
			}
		}
		

		$sql.=";";
		$req=$Conn->query($sql);
		$d_utilisateur=$req->fetch();
		if(empty($d_utilisateur)){
			$erreur_txt="Impossible d'afficher la page (err:001)!";
		}
	}else{
		$erreur_txt="Impossible d'afficher la page (err:002)!";
	}
}else{
	$erreur_txt="Impossible d'afficher la page (err:003)!";
}

// PARAMETRE DE LA REM
IF(empty($erreur_txt)){
	$sql="SELECT upr_rem_type FROM Utilisateurs_Rem_Param WHERE urp_utilisateur=" . $utilisateur . " AND urp_exercice=" . $exercice . ";";
	$req=$Conn->query($sql);
	$d_rem=$req->fetch();
	if(empty($d_rem)){
		$erreur_txt="Paramètres de rémunération non renseignés!";
	}elseif(empty($d_rem["upr_rem_type"])){
		$erreur_txt="Le type de rémunération n'est pas renseigné!";		
	}
}

if(empty($erreur_txt)){
	
	// DONNEE PARAMETRE
	
	// CONNEXION SUR LA SOCIETE ADMINISTRATIVE DE L'UTILISATEUR
	$ConnFct=connexion_fct($d_utilisateur["uti_societe"]);
	
	// categorie
	$d_categories=array();
	$sql="SELECT pca_id,pca_libelle FROM Produits_Categories ORDER BY pca_id;";
	$req=$Conn->query($sql);
	$d_result_cat=$req->fetchAll();
	if(!empty($d_result_cat)){
		foreach($d_result_cat as $cat){
			$d_categories[$cat["pca_id"]]=$cat["pca_libelle"];
		}	
	}
	// Famille
	$d_familles=array();
	$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles  ORDER BY pfa_id;";
	$req=$Conn->query($sql);
	$d_result_fam=$req->fetchAll();
	if(!empty($d_result_fam)){
		foreach($d_result_fam as $fam){
			$d_familles[$fam["pfa_id"]]=$fam["pfa_libelle"];
		}	
	}
	// Sous-Famille
	$d_s_familles=array();
	$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles ORDER BY psf_id;";
	$req=$Conn->query($sql);
	$d_result_s_fam=$req->fetchAll();
	if(!empty($d_result_s_fam)){
		foreach($d_result_s_fam as $s_fam){
			$d_s_familles[$s_fam["psf_id"]]=$s_fam["psf_libelle"];
		}	
	}
	// Sous-Sous-Famille
	$d_s_s_familles=array();
	$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles ORDER BY pss_id;";
	$req=$Conn->query($sql);
	$d_result_s_s_fam=$req->fetchAll();
	if(!empty($d_result_s_s_fam)){
		foreach($d_result_s_s_fam as $s_s_fam){
			$d_s_s_familles[$s_s_fam["pss_id"]]=$s_s_fam["pss_libelle"];
		}	
	}
	
	// SUR LA SOCIETE
	
	$soc_intra_inter=1;
	$sql="SELECT soc_intra_inter FROM Societes WHERE soc_id=" . $d_utilisateur["uti_societe"] . ";";
	$req=$Conn->query($sql);
	$d_societe=$req->fetch();
	if(!empty($d_societe)){
		if(!empty($d_societe["soc_intra_inter"])){
			$soc_intra_inter=$d_societe["soc_intra_inter"];
		}
	}
	
	// TITRE DE LA PAGE
	$titre="Rémunération " . $d_utilisateur["uti_nom"] . " " . $d_utilisateur["uti_prenom"]; 
	$sous_titre="";
	
	if($ca==1){
		$sous_titre="CA";
	}else{
		$sous_titre="Contentieux";
	}
	if($s_s_famille>0){
		$sous_titre.=" " . $d_s_s_familles[$s_s_famille];
	}elseif($s_famille>0){
		$sous_titre.=" " . $d_s_familles[$s_famille];
	}elseif($famille>0){
		$sous_titre.=" " . $d_familles[$famille];
	}elseif(!empty($categorie) AND $categorie!="total"){
		if($categorie=="autre"){
			$sous_titre.=" 'Autre'";
		}elseif($categorie=="autre_1"){
			$sous_titre.=" Prime de lancement - Famille INCENDIE";
		}elseif($categorie=="autre_3"){
			$sous_titre.=" Prime de lancement - Famille SANTE";
		}elseif($categorie=="ca_no_obj"){
			$sous_titre.=" Hors Objectif";
		}elseif($categorie=="interco"){
			$sous_titre.=" INTERCO";
		}else{
			$sous_titre.=" " . $d_categories[$categorie];
		}
	}
	if($mois>0){
		if($mois>3){
			$sous_titre.=" en  " . utf8_encode(strftime("%B %Y",mktime(0, 0, 0, $mois, 1, $exercice)));
		}else{
			$sous_titre.=" en  " . utf8_encode(strftime("%B %Y",mktime(0, 0, 0, $mois, 1, $exercice_fin)));
		}
	}elseif($trimestre>0){
		if($exercice>=2019){
			
			$sous_titre.= " pour le période " . $trimestre . " " . $exercice . "/" . $exercice_fin;
					
		}else{
			
			$sous_titre.= " pour le T" . $trimestre . " " . $exercice . "/" . $exercice_fin;

		}
	
	}else{
		$sous_titre.= " pour l'exercice " . $exercice . "/" . $exercice_fin; 
	}
	
	// MOIS PERIODES 
	
	$mois_periode="";
	$cont_per="";
	if($trimestre>0){
		if($exercice>=2019){

			$sql="SELECT urp_mois FROM utilisateurs_rem_periode WHERE urp_exercice=" . $exercice . " AND urp_type=" . $soc_intra_inter . " AND urp_periode=" . $trimestre . ";";
			$req=$Conn->query($sql);
			$d_periode_mois=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($d_periode_mois)){
				$tab_per=array_column ($d_periode_mois,"urp_mois");
				$mois_periode=implode(",",$tab_per);
				
				foreach($tab_per as $k_per => $per){
					if($per==1){
						$tab_per[$k_per]=12;
					}else{
						$tab_per[$k_per]=$per-1;
					}
				}
			
				
				$cont_per=implode(",",$tab_per);

			}
					
		}else{
			
			if($trimestre==1){
				$mois_periode="4,5,6";
				$cont_per="4,5";
			}elseif($trimestre==2){
				$mois_periode.="7,8,9";
				$cont_per="6,7,8";
			}elseif($trimestre==3){
				$mois_periode="10,11,12";
				$cont_per="9,10,11";
			}elseif($trimestre==4){
				$mois_periode.="1,2,3";
				$cont_per="12,1,2";
			}
		}
	
	}

	// Famille a exclure
	// exemple si on affiche famille formation, il ne faut pas juste filtrer fli_famille=fomration
	// mais il faut exclure les sous-éléments qui disposent de leur propre objectifs
	// on part du niv le plus bas et on remonte l'arbo
	$mil=""; // critere ajouté a la selection des facture
	if(($categorie!="total" OR $exercice>=2019) AND $categorie!="interco"){
		//categorie=999 on affiche tout
		//categorie=0 est reserve pour autre
		if($s_s_famille>0){
			
			$mil.=" AND fli_sous_sous_famille=" . $s_s_famille;
			
		}else{
			
			$sql="SELECT cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille 
			FROM Commerciaux 
			LEFT JOIN Commerciaux_Objectifs ON (Commerciaux.com_id=Commerciaux_Objectifs.cob_commercial)
			WHERE cob_exercice=" . $exercice;
			// critere d'accès
			if($d_rem["upr_rem_type"]==1){
				$sql.=" AND com_ref_1=" . $utilisateur;
			}elseif($d_utilisateur["uti_agence"]>0){
				$sql.=" AND com_agence=" . $d_utilisateur["uti_agence"];
			}
			if($categorie!="total"){
				if($categorie=="autre"){
					
					if($exercice>=2019){
						// element a selectionne et non à exclure
						// autre avec obj sinon doit tomber dans CA Hors objectifs
						$sql.=" AND cob_autre AND NOT cob_famille IN (1,3)";
					}else{
						$sql.=" AND NOT cob_autre";
					}
					
					
				}elseif($categorie=="autre_1"){
					
					$mil.=" AND fli_famille=1";
					$sql.=" AND NOT cob_autre AND cob_famille=1";
					
				}elseif($categorie=="autre_3"){
					
					$mil.=" AND fli_famille=3";
					$sql.=" AND NOT cob_autre AND cob_famille=3";
					
				}elseif($categorie=="ca_no_obj"){
					
					
				}else{
					if($s_famille>0){
						$mil.=" AND fli_sous_famille=" . $s_famille;
						$sql.=" AND cob_sous_famille=" . $s_famille . " AND cob_sous_sous_famille>0";
					}elseif($famille>0){
						$mil.=" AND fli_famille=" . $famille;
						$sql.=" AND cob_famille=" . $famille . " AND cob_sous_famille>0";
					}elseif($categorie>0){
						$mil.=" AND fli_categorie=" . $categorie;
						$sql.=" AND cob_categorie=" . $categorie . " AND cob_famille>0";
					}
				}
			}
			$sql.=";";
			$req=$ConnFct->query($sql);
			$d_exclut=$req->fetchAll();
			if(!empty($d_exclut)){
				
				foreach($d_exclut as $exclut){
					if($exercice>=2019 AND ($categorie=="autre" OR $categorie=="total")){					
						// la req correspond à element à afficher et non à aexclure 						
						if($exclut["cob_famille"]>0){
							$mil.="fli_famille=" . $exclut["cob_famille"] . " OR ";
						}elseif($exclut["cob_categorie"]>0){
							$mil.="fli_categorie=" . $exclut["cob_categorie"] . " OR ";
						}
					}elseif($exclut["cob_sous_sous_famille"]>0){
						$mil.=" AND NOT fli_sous_sous_famille=" . $exclut["cob_sous_sous_famille"];	
					}elseif($exclut["cob_sous_famille"]>0){
						$mil.=" AND NOT fli_sous_famille=" . $exclut["cob_sous_famille"];	
					}elseif($exclut["cob_famille"]>0){
						$mil.=" AND NOT fli_famille=" . $exclut["cob_famille"];	
					}elseif($exclut["cob_categorie"]>0){
						$mil.=" AND NOT fli_categorie=" . $exclut["cob_categorie"];	
					}
				}
				// on construit mil avec les element requis et non les excluts
				if($exercice>=2019 AND ($categorie=="autre" OR $categorie=="total")){
					$mil=" AND ( " . substr($mil, 0, -4) . ")";
				}
			}
		}
	}

	//factures
	
	if($categorie!="interco"){
		
		
		$sql="SELECT Factures.fac_id,Factures.fac_chrono,Factures.fac_numero,Factures.fac_date,Factures.fac_cli_code,Factures.fac_date_relance,Factures.fac_date_reg,Factures.fac_regle
		,Factures.fac_total_ttc,Factures.fac_etat_relance,Factures.fac_nature
		,fli_code_produit,fli_montant_ht,fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille
		,cli_code,cli_nom
		,com_label_1,com_label_2";
		if($exercice>=2019){
			$sql.=",acl_id,acl_derogation,acl_rem_ca,acl_rem_famille
			,act_id,act_ca,act_charge";
			$sql.=",Avoirs.fac_date AS av_date";
		}
		$sql.=" FROM Factures 
		LEFT JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
		LEFT OUTER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Clients.cli_agence=Factures.fac_agence)
		LEFT OUTER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";
		if($exercice>=2019){
			$sql.=" LEFT OUTER JOIN Factures AS Avoirs ON (Factures.fac_avoir=Avoirs.fac_id)";
			$sql.=" LEFT JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id AND (fli_action_cli_soc=" . $d_utilisateur["uti_societe"] . " OR fli_action_cli_soc=0))
			LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)";
		}
		if($ca==1){
			//ca facturé
			
			// FG Il ne faut plus impacté l'avoir sur le mois de facturation mais sur le mois de l'avoir
			/*if($exercice>=2019){
				$sql.=" WHERE (
					(Factures.fac_nature=1 AND Factures.fac_date>='" . $exercice . "-04-01' AND .Factures.fac_date<='" . intval($exercice+1) . "-03-31')
					OR 
					(Factures.fac_nature=2 AND Avoirs.fac_date>='" . $exercice . "-04-01' AND Avoirs.fac_date<='" . intval($exercice+1) . "-03-31')
				)";
			}else{*/
				$sql.=" WHERE Factures.fac_date>='" . $exercice . "-04-01' AND Factures.fac_date<='" . intval($exercice+1) . "-03-31'";
			//}
		}else{
			// ca contentieux
			$sql.=" WHERE Factures.fac_nature=1 AND Factures.fac_etat_relance=6 AND Factures.fac_date_relance>='" . $exercice . "-03-01' AND Factures.fac_date_relance<'" . intval($exercice+1) . "-03-01'";
			if($exercice==2018){
				$sql.=" AND ( MONTH(Factures.fac_date_relance)=4 OR ISNULL(Factures.fac_date_reg) OR DATEDIFF(Factures.fac_date_reg,Factures.fac_date_relance)>30 )";
			}else{
				$sql.=" AND ( ISNULL(Factures.fac_date_reg) OR DATEDIFF(Factures.fac_date_reg,Factures.fac_date_relance)>30 )";
			}
		}
		$sql.=" AND fli_categorie<4";
		// critere d'accès
		if($d_rem["upr_rem_type"]==1){
			$sql.=" AND com_ref_1=" . $utilisateur;
		}elseif($d_utilisateur["uti_agence"]>0){
			$sql.=" AND Factures.fac_agence=" . $d_utilisateur["uti_agence"];
		}
		
		if($ca==1){
			if($mois>0){
				
				$sql.=" AND MONTH(Factures.fac_date)=" . $mois;
				
				/*if($exercice>=2019){
					if($exercice==2019 AND ($mois==4 OR $mois==5)){
						$sql.=" AND MONTH(Factures.fac_date)=" . $mois;	
					}else{
						$sql.=" AND ( (Factures.fac_nature=1 AND MONTH(Factures.fac_date)=" . $mois . ") OR (Factures.fac_nature=2 AND MONTH(Avoirs.fac_date)=" . $mois . "))";
					}
				}else{
					$sql.=" AND MONTH(Factures.fac_date)=" . $mois;
				}*/
			}elseif($trimestre>0){	
				/*if($exercice>=2019){
					$sql.=" AND ( (Factures.fac_nature=1 AND MONTH(Factures.fac_date) IN (" . $mois_periode . ")) OR (Factures.fac_nature=2 AND MONTH(Avoirs.fac_date) IN (" . $mois_periode . ")))";
				}else{*/
					$sql.=" AND MONTH(Factures.fac_date) IN (" . $mois_periode . ")";
				//}
			}
		}elseif($ca==2){
			
			if($mois>0){
				if($mois==4 AND $exercice==2018){
					$sql.=" AND MONTH(Factures.fac_date_relance)=" . $mois;
				}elseif($mois==5 AND $exercice==2018){
					// mois blanc pas de contentieux en prevision change regle
					$sql.=" AND MONTH(Factures.fac_date_relance)=999";
				}elseif($mois==1){
					// mois blanc pas de contentieux en prevision change regle
					$sql.=" AND MONTH(Factures.fac_date_relance)=12";
				}else{
					$sql.=" AND MONTH(Factures.fac_date_relance)=" . intval($mois-1);
				}
			}elseif($trimestre>0 AND !empty($cont_per)){
				
				$sql.=" AND MONTH(Factures.fac_date_relance) IN (" . $cont_per . ")";
				
				/*if($trimestre==1 AND $exercice==2018){
					$sql.=" AND MONTH(Factures.fac_date_relance) IN (4,5)";
				}elseif($trimestre==1){
					$sql.=" AND MONTH(Factures.fac_date_relance) IN (3,4,5)";
				}elseif($trimestre==2){
					$sql.=" AND MONTH(Factures.fac_date) IN (6,7,8)";
				}elseif($trimestre==3){
					$sql.=" AND MONTH(Factures.fac_date) IN (9,10,11)";
				}elseif($trimestre==4){
					$sql.=" AND MONTH(Factures.fac_date) IN (12,1,2)";
				}*/
			}
		}
		if($mil!=""){
			$sql.=$mil;
		}
		$sql.=" ORDER BY Factures.fac_date, Factures.fac_chrono, Factures.fac_numero;";
		/*echo($sql);
		die();*/
		$req = $ConnFct->prepare($sql);	
		$req->execute();
		$factures = $req->fetchAll(PDO::FETCH_ASSOC);
		
		/*echo("<pre>");
			print_r($factures);
		echo("</pre>");
		die();*/
		
		if($exercice==2019 AND $ca==1){
			
			// sur 2018/2019 avoir impact le mois de l'avoir -> avoir 04/19 ne va pas impacter la rem de 03/19
			// pour assurer une cohérence entre les 2 système, les avoirs liés à une facture de l'ancien exercice impact me mois du nouvel exercice
			
			// on ne prend que des avoir donc pas de gestion des contentieux ($ca=1)
			
		/*	$sql="SELECT Factures.fac_id,Factures.fac_chrono,Factures.fac_numero,Factures.fac_date,Factures.fac_cli_code,Factures.fac_date_relance,Factures.fac_date_reg,Factures.fac_regle
			,Factures.fac_total_ttc,Factures.fac_etat_relance
			,fli_code_produit,fli_montant_ht,fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille
			,cli_code,cli_nom
			,com_label_1,com_label_2
			,act_id,act_ca,act_charge
			,acl_id,acl_derogation,acl_rem_famille,acl_rem_ca
			FROM Factures 
			LEFT JOIN Factures AS Avoirs ON (Factures.fac_avoir=Avoirs.fac_id)
			LEFT JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
			LEFT OUTER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Clients.cli_agence=Factures.fac_agence)
			LEFT OUTER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
			LEFT JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id AND (fli_action_cli_soc=" . $d_utilisateur["uti_societe"] . " OR fli_action_cli_soc=0))
			LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
			WHERE Factures.fac_date>='" . $exercice . "-04-01' AND Factures.fac_date<='" . intval($exercice+1) . "-03-31'
			AND Factures.fac_nature=2 AND Avoirs.fac_date<='" . $exercice . "-03-31'
			AND fli_categorie<4";
			// critere d'accès
			if($d_rem["upr_rem_type"]==1){
				$sql.=" AND com_ref_1=" . $utilisateur;
			}elseif($d_utilisateur["uti_agence"]>0){
				$sql.=" AND Factures.fac_agence=" . $d_utilisateur["uti_agence"];
			}
			if($mois>0){
				$sql.=" AND MONTH(Factures.fac_date)=" . $mois;
			}elseif($trimestre>0){
				$sql.=" AND MONTH(Factures.fac_date) IN (" . $mois_periode . ")";					
			}
			if($mil!=""){
				$sql.=$mil;
			}
			$sql.=" ORDER BY fac_date,fac_chrono,fac_numero;";
			$req = $ConnFct->prepare($sql);	
			$req->execute();
			$avoirs=$req->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($avoirs)){
				foreach($avoirs as $avoir){
					array_push ($factures,$avoir);
				}
			}*/
		}
		
	}else{
		
		// INTERCO
		
		$factures=array();
		
		$sql="SELECT soc_id,soc_code FROM Societes LEFT JOIN Utilisateurs_Societes On (Societes.soc_id=Utilisateurs_Societes.uso_societe)
		WHERE uso_utilisateur=" . $utilisateur;
		if($d_utilisateur["uti_agence"]==0){
			$sql.=" AND NOT soc_id=" . $d_utilisateur["uti_societe"];
		}
		$sql.=";";
		$req=$Conn->query($sql);
		$d_scoietes=$req->fetchAll();
		if(!empty($d_scoietes)){
			foreach($d_scoietes as $soc){
				
				$ConnFct=connexion_fct($soc["soc_id"]);
				
				$sql="SELECT fac_id,fac_chrono,fac_numero,fac_date,fac_cli_code,fac_date_relance,fac_date_reg,fac_regle,fac_total_ttc,fac_etat_relance
				,fli_code_produit,fli_montant_ht,fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille
				,cli_code,cli_nom
				,com_label_1,com_label_2
				FROM Factures 
				LEFT JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
				LEFT OUTER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Clients.cli_agence=Factures.fac_agence)
				LEFT OUTER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";
				
				$sql.=" WHERE com_ref_1=" . $utilisateur . " AND fli_categorie<4";
				
				if($d_utilisateur["uti_agence"]>0 AND $d_utilisateur["uti_societe"]==$soc["soc_id"]){
					// com sur 2 agence du meme soc le CA uti_agence est deja compté
					$sql.=" AND NOT com_agence=" . $d_utilisateur["uti_agence"];
				}
	
				if($ca==1){
					//ca facturé
					$sql.=" AND fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'";
				}else{
					// ca contentieux
					$sql.=" AND fac_etat_relance=6 AND fac_date_relance>='" . $exercice . "-03-01' AND fac_date_relance<'" . intval($exercice+1) . "-03-01'";
					if($exercice==2018){
						$sql.=" AND ( MONTH(fac_date_relance)=4 OR ISNULL(fac_date_reg) OR DATEDIFF(fac_date_reg,fac_date_relance)>30 )";
					}else{
						$sql.=" AND ( ISNULL(fac_date_reg) OR DATEDIFF(fac_date_reg,fac_date_relance)>30 )";
					}
				}
				if($ca==1){
					if($mois>0){
						$sql.=" AND MONTH(fac_date)=" . $mois;
					}
				}elseif($ca==2){
					if($mois==4 AND $exercice==2018){
						$sql.=" AND MONTH(fac_date_relance)=" . $mois;
					}elseif($mois==5 AND $exercice==2018){
						// mois blanc pas de contentieux en prevision change regle
						$sql.=" AND MONTH(fac_date_relance)=999";
					}elseif($mois==1){
						// mois blanc pas de contentieux en prevision change regle
						$sql.=" AND MONTH(fac_date_relance)=12";
					}else{
						$sql.=" AND MONTH(fac_date_relance)=" . intval($mois-1);
					}
				}
				if($mil!=""){
					$sql.=$mil;
				}
				$sql.=" ORDER BY fac_date,fac_chrono,fac_numero;";
				$req = $ConnFct->prepare($sql);	
				$req->execute();
				$results = $req->fetchAll();
				if(!empty($results)){
					foreach($results as $k=>$r){
						
						$results[$k]["soc_code"]=$soc["soc_code"];
						$factures[]=$results[$k];
					}
				}
			}
		}
	}
	
		
	
	/*echo("sql : " . $sql);
	echo("mil : " . $mil);
	echo("<pre>");
		print_r($factures);
	echo("</pre>");

	die();*/
}
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">

		<?php 			if(!empty($erreur_txt)){ ?>
							<div class="col-md-12 text-center" style="padding:0;" >
								<div class="alert alert-danger" style="border-radius:0px;"><?=$erreur_txt?></div>
							</div>
		<?php			}else{
			
							echo("<h1>" . $titre . "</h1>");
							echo("<h2>" . $sous_titre. "</h2>");
							if(!empty($factures)){ 
								$total_ht=0; ?>
				
								<div class="table-responsive">
									<table class="table table-striped" >
										<thead>
											<tr class="dark2" >	
									<?php		$nbCols=7;
												if($categorie=="interco"){
													$nbCols=8; ?>
													<th>Société</th> 
									<?php		} ?>
												<th>Client</th> 
												<th>Commercial</th>										
												<th>Code produit</th>
												<th>Catégorie</th>
												<th>Famille</th>
												<th>Sous-Famille</th>
												<th>Sous-Sous-Famille</th>
												<th>H.T.</th>
												<th>Facture</th>
												<th>Date</th>
												<th>Date contentieux</th>												
												<th>Total TTC</th>
												<th>Réglé</th>
												<th>Date de réglement</th>
										<?php	if($exercice>=2019){ ?>
													<th>Action</th>
													<th>Info</th>
										<?php	} ?>	
											</tr>
										</thead>
										<tbody>
								<?php		foreach($factures as $f){
												
												$class="";
												
												$fac_date="";
												if(!empty($f['fac_date'])){
													$dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
													$fac_date=$dt_fac_date->format("d/m/Y");
												}

												$fac_date_relance="";
												if($f['fac_etat_relance']==6){
													if(!empty($f['fac_date_relance'])){
														$dt_fac_date_relance=date_create_from_format("Y-m-d",$f['fac_date_relance']);
														$fac_date_relance=$dt_fac_date_relance->format("d/m/Y");
													}
												}
												
												$fac_date_reg="";
												if(!empty($f['fac_date_reg'])){
													$dt_fac_date_reg=date_create_from_format("Y-m-d",$f['fac_date_reg']);
													$fac_date_reg=$dt_fac_date_reg->format("d/m/Y");
												}
												
												$montant_ht=0;
												if(!empty($f['fli_montant_ht'])){
													$montant_ht=$f['fli_montant_ht'];
												}
												
												$info="";
												if($exercice>=2019){
													if(!empty($f['act_id']) AND !empty($f['act_charge'])){
														if($f['act_ca']/$f['act_charge']<1.2){
															$montant_ht=0;
															$class="class='text-danger'";
															$info="Coeff ST < 1.2";
														}
													}
													if(!empty($f['acl_derogation'])){
														/*var_dump($f['acl_id']);
														die();*/
														if($categorie==1 OR $categorie=="autre_1" OR $categorie=="autre_2"){
															if(empty($f['acl_rem_famille'])){
																$montant_ht=0;
																$class="class='text-danger'";
																$info="Dérogation : CA non retenu en rem famille";
															}
														}elseif(empty($f['acl_rem_ca'])){
																$montant_ht=0;
																$class="class='text-danger'";
																$info="Dérogation : CA non retenu en rem mensuelle";
														}												
													}
												}
												

												$total_ht+=$montant_ht;
												
												?>
												<tr <?=$class?> >
										<?php		if($categorie=="interco"){ ?>
														<td><?= $f['soc_code']?></td>
										<?php		} ?>
													<td><?= $f['fac_cli_code']?></td>
													<td><?= $f['com_label_1'] ?></td>											
													<td><?=	$f['fli_code_produit'] ?></td>
													<td>
											<?php		if(!empty($f['fli_categorie'])){
															echo($d_categories[$f['fli_categorie']]);
														}else{
															echo("&nbsp;");
														} ?>
													</td>
													<td>
											<?php		if(!empty($f['fli_famille'])){
															echo($d_familles[$f['fli_famille']]);
														}else{
															echo("&nbsp;");
														} ?>
													</td>
													<td>
											<?php		if(!empty($f['fli_sous_famille'])){
															echo($d_s_familles[$f['fli_sous_famille']]);
														}else{
															echo("&nbsp;");
														} ?>
													</td>
													<td>
											<?php		if(!empty($f['fli_sous_sous_famille'])){
															echo($d_s_s_familles[$f['fli_sous_sous_famille']]);
														}else{
															echo("&nbsp;");
														} ?>
													</td>
													<td class="text-right" ><?= $montant_ht ?></td>
													<td><?= $f['fac_numero'] ?></td>																					
													<td><?=	$fac_date?></td>
													<td><?= $fac_date_relance?></td>	
													<td><?= $f['fac_total_ttc'] ?></td>
													<td><?= $f['fac_regle'] ?></td>	
													<td><?= $fac_date_reg?></td>	
											<?php	if($exercice>=2019){ ?>
														<td><?= $f['act_id'] . "-" . $f['acl_id']?></td>
														<td><?= $info?></td>	
											<?php	} ?>		
												</tr>
					<?php					}  ?>
										</tbody>
										<tfoot>
											<tr>
												<th colspan="<?=$nbCols?>" class="text-right" >Total :</th>
												<td class="text-right" ><?=$total_ht?></td>	
												<th colspan="6" class="text-right" >&nbsp;</th>
											</tr>
										</tfoot>
									</table>
								</div>
			<?php 			}else{ ?>
								<div class="col-md-12 text-center" style="padding:0;" >
									<div class="alert alert-warning" style="border-radius:0px;">
										Aucune facture correspondant à votre recherche.
									</div>
								</div>
			<?php			} 
						}?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="utilisateur_rem.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>" class="btn btn-default btn-sm" role="button">
							<i class="fa fa-left"></i> Retour
						</a>	</div>
					<div class="col-xs-6 footer-middle">&nbsp;</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
			
			});						
		</script>
	</body>
</html>
