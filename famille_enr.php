<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

if(!empty($_POST)){
	
	$famille_id=0;
	if(isset($_POST['pfa_id'])){
		$famille_id=intval($_POST['pfa_id']);
	}
	if($famille_id>0){
		$req = $Conn->prepare("UPDATE produits_familles SET pfa_libelle=:pfa_libelle WHERE pfa_id=:pfa_id;");
		$req->bindParam("pfa_libelle",$_POST['pfa_libelle']);
		$req->bindParam("pfa_id",$famille_id);
		$req->execute();

	}else{
		$req = $Conn->prepare("INSERT INTO produits_familles (pfa_libelle) VALUES (:pfa_libelle);");
		$req->bindParam("pfa_libelle",$_POST['pfa_libelle']);
		$req->execute();

	}
}
	
header("location: famille_liste.php");
 ?>
