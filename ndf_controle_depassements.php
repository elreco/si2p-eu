<?php
////////////////// MENU ACTIF HEADER ///////////////////

////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
if (isset($_GET['menu_retour'])) {
	unset($_SESSION['retour']);
}

$_SESSION['retour'] = "ndf_controle_depassements.php";
///////////////////// Contrôles des parametres ////////////////////


///////////////////// FIN Contrôles des parametres ////////////////////

////////////////// TRAITEMENTS SERVEUR ///////////////////
// récupérer toutes les ndf
if (isset($_GET['raz'])) {
	unset($_SESSION['ndf_dep_tri']);
}
if (!empty($_POST)) {

	if (!empty($_POST['ndf_utilisateur'])) {
		$ndf_utilisateur = $_POST['ndf_utilisateur'];
	} else {
		$ndf_utilisateur = 0;
    }

	$_SESSION['ndf_dep_tri'] = array(
		"ndf_societe"         => $_POST['ndf_societe'],
		"ndf_agence"          => $_POST['ndf_agence'],
		"ndf_utilisateur"   => $ndf_utilisateur,
		"ndf_mois"        => $_POST['ndf_mois'],
		"ndf_annee"        => $_POST['ndf_annee'],
		"ndf_groupe_par" => $_POST['ndf_groupe_par']
	);
} else {
    $_SESSION['ndf_dep_tri'] = array(
		"ndf_societe"         => $_SESSION['acces']['acc_societe'],
		"ndf_agence"          => $_SESSION['acces']['acc_agence'],
		"ndf_mois"        => date('m'),
		"ndf_annee"        => date('Y'),
		"ndf_groupe_par" => 0
	);
}
if (!empty($_SESSION['ndf_dep_tri'])) {
	$critere = array();
	$mil = "";
	if (!empty($_SESSION['ndf_dep_tri']['ndf_societe'])) {
		$mil .= " AND ndf_societe = :ndf_societe";
		$critere["ndf_societe"] = $_SESSION['ndf_dep_tri']['ndf_societe'];
	};
	if (!empty($_SESSION['ndf_dep_tri']['ndf_agence'])) {
		$mil .= " AND ndf_agence = :ndf_agence";
		$critere["ndf_agence"] = $_SESSION['ndf_dep_tri']['ndf_agence'];
	};
	if (!empty($_SESSION['ndf_dep_tri']['ndf_statut'])) {
		$mil .= " AND ndf_statut = :ndf_statut";
		$critere["ndf_statut"] = $_SESSION['ndf_dep_tri']['ndf_statut'];
	};
	if (!empty($_SESSION['ndf_dep_tri']['ndf_mois'])) {
		$mil .= " AND ndf_mois = :ndf_mois";
		$critere["ndf_mois"] = $_SESSION['ndf_dep_tri']['ndf_mois'];
	};
	if (!empty($_SESSION['ndf_dep_tri']['ndf_annee'])) {
		$mil .= " AND ndf_annee = :ndf_annee";
		$critere["ndf_annee"] = $_SESSION['ndf_dep_tri']['ndf_annee'];
	};
}

$mil .= " AND ndf_statut > 0";

$sql = "SELECT ROUND(SUM(nli_depassement_accorde), 2) as sum_nli_depassement_accorde, ndf_mois, ndf_annee, ndf_id, ndf_utilisateur, ndf_id, uti_prenom, uti_nom
		FROM ndf
        LEFT JOIN ndf_lignes ON ndf_lignes.nli_ndf = ndf.ndf_id
        LEFT JOIN utilisateurs ON utilisateurs.uti_id = ndf.ndf_utilisateur
        LEFT JOIN ndf_avances ON ndf_avances.nav_ndf = ndf.ndf_id
        LEFT JOIN societes ON utilisateurs.uti_societe = societes.soc_id
        LEFT JOIN agences ON agences.age_id = utilisateurs.uti_agence";
if ($mil != "") {
    $sql .= " WHERE " . substr($mil, 5, strlen($mil) - 5);
}
$sql .= " ORDER BY ndf_date LIMIT 8000";
$req = $Conn->prepare($sql);

if (!empty($critere)) {
    foreach ($critere as $c => $v) {
        $req->bindValue($c, $v);
    }
};
$req->execute();
$notes = $req->fetchAll();
// fin récupérer toutes les ndf

// chercher les societes et agences
$req=$Conn->query("SELECT DISTINCT soc_id,soc_code,soc_nom FROM Societes LEFT JOIN Utilisateurs_Societes ON uso_societe = soc_id WHERE uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " AND soc_archive=0 ORDER BY soc_nom;");
$req->execute();
$societes=$req->fetchAll();

if (!empty($_SESSION['ndf_dep_tri'])) {
	$req = $Conn->prepare("SELECT age_id, age_nom FROM agences LEFT JOIN Utilisateurs_Societes ON uso_agence = age_id WHERE uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " AND age_archive != 1 AND age_societe = " . $_SESSION['ndf_dep_tri']['ndf_societe']);
$req->execute();
$agences = $req->fetchAll();
} else {
	$req = $Conn->prepare("SELECT age_id, age_nom FROM agences LEFT JOIN Utilisateurs_Societes ON uso_agence = age_id WHERE uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " AND age_archive != 1 AND age_societe = " . $_SESSION['acces']['acc_societe']);
$req->execute();
$agences = $req->fetchAll();
}

// fin chercher les societes et agences

//////////////// FILTRE ///////////
$req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_archive = 0");
$req->execute();
$utilisateurs = $req->fetchAll();

///////////////////////// FIN TRAITEMENTS SERVEUR /////////////////////////////
///////////////////// TRAITEMENTS PHP ///////////////////////


// récupérer tous les mois dont j'ai besoin
$mois = array(

	1 => "Janvier",
	2 => "Février",
	3 => "Mars",
	4 => "Avril",
	5 => "Mai",
	6 => "Juin",
	7 => "Juillet",
	8 => "Août",
	9 => "Septembre",
	10 => "Octobre",
	11 => "Novembre",
	12 => "Décembre"


);

$statuts = array(
	0 => "En cours de saisie",
	1 => "En cours de validation",
	2 => "Validée RA",
	3 => "Validée RE",
	4 => "Validée Compta",
	5 => "Validée RH",
	6 => "En cours de paiement",
	7 => "Payée",
	8 => "Annulée"

);


$annees = array(
	1 => date('Y', strtotime(' -5 years')),
	2 => date('Y', strtotime(' -4 years')),
	3 => date('Y', strtotime(' -3 years')),
	4 => date('Y', strtotime(' -2 years')),
	5 => date('Y', strtotime(' -1 years')),
	6 => date('Y'),
	7 => date('Y', strtotime(' +1 year')),

);


///// BUILD RESULT
$result = [];
if (!empty($_SESSION['ndf_dep_tri']['ndf_groupe_par'])) {
	if ($_SESSION['ndf_dep_tri']['ndf_groupe_par'] == 1) {
		// GROUPEMENT PAR MOIS
		foreach($annees as $a) {
			foreach($mois as $m => $moi) {
				foreach ($notes as $n) {
					if ($n['ndf_mois'] == $m && $n['ndf_annee'] == $a){
						$result[$n['ndf_mois'] . "-" . $n['ndf_annee']] = $n;
					}

				}
			}
		}

	} else {
		// GROUPEMENT PAR NOTES
		foreach ($notes as $n) {
			$result[$n['ndf_id']] = $n;
		}
	}
} else {
	foreach ($utilisateurs as $u) {
		foreach ($notes as $n) {
			if($n['ndf_utilisateur'] == $u['uti_id'] && !empty($n['sum_nli_depassement_accorde'])) {

				$result[$n['ndf_utilisateur']] = $n;
			}
		}
	}
}
////////////////////FIN TRAITEMENTS PHP//////////////////////
?>
<!DOCTYPE html>
<html>

<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Notes de frais</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
	<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


	<!-- Favicon -->
	<link rel="icon" type="image/png" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<style type="text/css">
	.panel-tabs>li>a {
		color: #AAA;
		font-size: 14px;
		letter-spacing: 0.2px;
		line-height: 30px;
		/*padding: 9px 20px 11px;*/
		border-radius: 0;
		border-left: 1px solid transparent;
		border-right: 1px solid transparent;
	}

	.nav2>li>a {
		position: relative;
		display: block;
		/* padding: 10px 15px; */
	}
</style>

<body class="sb-top sb-top-sm">


	<!-- Start: Main -->
	<div id="main">

		<?php include "includes/header_def.inc.php"; ?>
		<!-- Start: Content-Wrapper -->
		<section id="content_wrapper">

			<!-- MESSAGES SUCCES -->

			<section id="content" class="animated fadeIn pr20 pl20">
				<h1>Contrôle des dépassements</h1>
				<div class="row">
					<div class="col-md-12">
						<div class="panel mb10">
							<div class="panel-heading panel-head-sm">
								<span class="panel-icon">
									<i class="fa fa-cogs" aria-hidden="true"></i>
								</span>
								<span class="panel-title"> Filtrer</span>
							</div>
							<div class="panel-body p10">
								<div class="row admin-forms">
									<form action="ndf_controle_depassements.php" method="POST">
										<div class="col-md-3">
											<p class="text-center mb20" style="font-weight:bold;">Société :</p>
											<select id="societe" name="ndf_societe" class="select2 select2-societe">
												<option value="0">Sélectionner une société...</option>
												<?php foreach ($societes as $s) { ?>
													<option value="<?= $s['soc_id'] ?>" <?php
																						if (!empty($_SESSION['ndf_dep_tri'])) {
																							if ($_SESSION['ndf_dep_tri']['ndf_societe'] == $s['soc_id']) {
																						?> selected <?php
																							}
																						} else {
																							if ($_SESSION['acces']['acc_societe'] == $s['soc_id']) { ?> selected <?php }
																													} ?>><?= $s['soc_nom'] ?></option>
												<?php } ?>
											</select>

										</div>
										<div class="col-md-3">
											<p class="text-center mb20" style="font-weight:bold;">Agence :</p>
											<select id="agence" name="ndf_agence" class="select2 select2-societe-agence">
												<option value="0">Sélectionner une agence...</option>
												<?php foreach ($agences as $a) { ?>
													<option value="<?= $a['age_id'] ?>" <?php

															if (!empty($_SESSION['ndf_dep_tri'])) {
																if ($_SESSION['ndf_dep_tri']['ndf_agence'] == $a['age_id']) {
															?> selected <?php
																}
															} else {
																if (!empty($_SESSION['acces']['acc_agence'])) {

																	if ($_SESSION['acces']['acc_agence'] == $a['age_id']) {
												?> selected <?php  }
																}
															} ?>><?= $a['age_nom'] ?></option>
												<?php } ?>
											</select>

										</div>
										<div class="col-md-3">
											<p class="text-center mb20" style="font-weight:bold;">Utilisateur :</p>
											<select id="utilisateur" name="ndf_utilisateur" class="select2 select2-utilisateur-societe-agence">
												<?php if (!empty($_SESSION['ndf_dep_tri']['ndf_utilisateur'])) {
													$req = $Conn->prepare("SELECT uti_prenom, uti_nom FROM utilisateurs WHERE uti_id = " . $_SESSION['ndf_dep_tri']['ndf_utilisateur']);
													$req->execute();
													$uti = $req->fetch();

												?>
													<option value="<?= $_SESSION['ndf_dep_tri']['ndf_utilisateur'] ?>"><?= $uti['uti_prenom'] ?> <?= $uti['uti_nom'] ?></option>
												<?php } ?>
											</select>

										</div>
										<div class="col-md-3">
											<p class="text-center mb20" style="font-weight:bold;">Groupé par :</p>
											<select id="statut" name="ndf_groupe_par" class="select2">
											<option value="0" <?php if (empty($_SESSION['ndf_dep_tri']['ndf_groupe_par'])) { ?>selected<?php } ?>>Utilisateur</option>
												<option value="1" <?php if (!empty($_SESSION['ndf_dep_tri']['ndf_groupe_par']) && $_SESSION['ndf_dep_tri']['ndf_groupe_par'] == 1) { ?>selected<?php } ?>>Mois</option>
												<option value="2" <?php if (!empty($_SESSION['ndf_dep_tri']['ndf_groupe_par']) && $_SESSION['ndf_dep_tri']['ndf_groupe_par'] == 2) { ?>selected<?php } ?>>Note</option>
											</select>
										</div>
										<div class="col-md-3">
											<p class="text-center mb20 mt20" style="font-weight:bold;">Mois :</p>
											<select id="statut" name="ndf_mois" class="select2">
												<option value="0">Mois...</option>
												<?php foreach ($mois as $k => $m) { ?>
													<option value="<?= $k  ?>" <?php
																				if (!empty($_SESSION['ndf_dep_tri'])) {
																					if ($_SESSION['ndf_dep_tri']['ndf_mois'] == $k) { ?> selected <?php }
																												} else {
																													if ($k == date("n")) { ?> selected <?php
																													}
																												} ?>><?= $m ?></option>
												<?php } ?>
											</select>

										</div>
										<div class="col-md-3">
											<p class="text-center mb20 mt20" style="font-weight:bold;">Année :</p>
											<select id="statut" name="ndf_annee" class="select2">
												<option value="0">Année...</option>

												<?php foreach ($annees as $a) {

												?>
													<option value="<?= $a ?>" <?php if (!empty($_SESSION['ndf_dep_tri'])) {
																					if ($_SESSION['ndf_dep_tri']['ndf_annee'] == $a) { ?> selected <?php
																													}
																												} else {
																													if ($a == date("Y")) {
																														?> selected <?php }
																												} ?>><?= $a ?></option>
												<?php

												} ?>
											</select>

										</div>

										<div class="col-md-3 text-center mt35">
											<button type="submit" class="btn btn-sm btn-primary">Filtrer</button>
										</div>
									</form>
								</div>

							</div>

						</div>
					</div>
					<div class="col-md-12">
						<?php if (empty($_SESSION['ndf_dep_tri']['ndf_groupe_par'])) {  ?>
							<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
								<thead>
									<tr class="dark">
										<th>Utilisateur</th>
										<th class="no-sort">Dépassement accordé</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($result as $r) { ?>
										<tr>
											<td><?= $r['uti_prenom'] ?> <?= $r['uti_nom'] ?></td>
											<td><?= $r['sum_nli_depassement_accorde'] ?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						<?php } ?>
						<?php if (!empty($_SESSION['ndf_dep_tri']['ndf_groupe_par']) && $_SESSION['ndf_dep_tri']['ndf_groupe_par'] == 1) {  ?>
							<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
							<thead>
								<tr class="dark">
									<th>Mois</th>
									<th>Année</th>
									<th class="no-sort">Dépassement accordé</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($result as $r) { ?>
									<tr>
										<td><?= $mois[$r['ndf_mois']] ?></td>
										<td><?= $annees[array_search($r['ndf_annee'], $annees)] ?></td>
										<td><?= $r['sum_nli_depassement_accorde'] ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php }?>
						<?php if (!empty($_SESSION['ndf_dep_tri']['ndf_groupe_par']) && $_SESSION['ndf_dep_tri']['ndf_groupe_par'] == 2) {  ?>
							<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
							<thead>
								<tr class="dark">
									<th>Mois</th>
									<th>Année</th>
									<th>Utilisateur</th>
									<th>Note</th>
									<th class="no-sort">Dépassement accordé</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($result as $r) { ?>
									<tr>
										<td><?= $mois[$r['ndf_mois']] ?></td>
										<td><?= $annees[array_search($r['ndf_annee'], $annees)] ?></td>
										<td><?= $r['uti_prenom'] ?> <?= $r['uti_nom'] ?></td>
										<td><a href="ndf.php?id=<?= $r['ndf_id'] ?>"><?= $r['ndf_id'] ?></a></td>
										<td><?= $r['sum_nli_depassement_accorde'] ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php }?>
					</div>
					<!-- <div class="col-md-2 col-md-offset-10 mt20">
						<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
							<thead>
								<tr class="dark">
									<th class="  text-center">Total</th>

								</tr>
							</thead>
							<tbody>
								<td class=" text-center">
									 €
								</td>
							</tbody>
						</table>
					</div> -->
				</div>
			</section>
		</section>

	</div>
	<!-- End: Main -->
	<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
		<div class="row">
			<div class="col-xs-4 footer-left pt7">
				<a href="parametre.php" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a>
			</div>
			<div class="col-xs-4 footer-middle pt7"></div>
			<div class="col-xs-4 footer-right pt7">
			</div>
		</div>
	</footer>

	<?php
	include "includes/footer_script.inc.php"; ?>

	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
	<!-- plugin pour les masques formulaires -->
	<script src="assets/js/custom.js"></script>

	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script src="vendor/plugins/summernote/summernote.min.js"></script>
	<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

	<script src="vendor/plugins/jquery.numberformat.js"></script>
	<!-- pour mettre des images de tests -->
	<!-- Theme Javascript -->

	<script>
		// DOCUMENT READY //
		jQuery(document).ready(function() {

			// initilisation plugin datatables
			$('#table_id').DataTable({
				"language": {
					"url": "vendor/plugins/DataTables/media/js/French.json"
				},
				"paging": false,
				"searching": false,
				"info": false,
				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});
			// quand on veut imprimer


			////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
		});

		///////////// FONCTIONS //////////////

		//////////// FIN FONCTIONS //////////
	</script>

</body>

</html>