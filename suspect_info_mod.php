<?php
$commerce_li = true;
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_client.php');
include('modeles/mod_parametre.php');
include('modeles/mod_famille.php');
include('modeles/mod_sous_famille.php');
$client = get_client($_GET['client'], "sus");
if(isset($_GET['id'])){
  $info = get_suspect_info($_GET['id']);
}

?>
<!DOCTYPE html>
<html>
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>SI2P - Orion - <?= $client['sus_nom'] ?></title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
  <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
  <link rel="stylesheet" type="text/css" href="vendor/plugins/footable/css/footable.core.min.css">
  <link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
<link href="vendor/plugins/summernote/summernote-bs3.css" rel="stylesheet" type="text/css">
  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
  <!-- Start: Main -->
  <div id="main">
    <?php
    include "includes/header_def.inc.php"; 
    ?>

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper" class="">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li>
              <span class="glyphicon glyphicon-shopping-cart"></span> <a href="#"> Commerce</a>
            </li>
            <li>
              <a href="client_tri.php">Recherche de suspects</a>
            </li>
            <li>
              <a href="client_liste.php">Liste des suspects</a>
            </li>
            <li>
              <a href="suspect_voir.php?client=<?= $_GET['client'] ?>"><?= $client['sus_nom'] ?></a>
            </li>
            <li class="crumb-trail">Nouvelle info</li>
          </ol>
        </div>

      </header>
      <section id="content" class="">

        <!-- Begin .page-heading -->


        <div class="row">

          <div class="col-md-12">
            <div class="panel">
              <div class="panel-heading  panel-head-sm">
                <i class="fa fa-pencil"></i> Ajouter l'info pour le client <strong><?= $client['sus_nom'] ?></strong>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                    <form id="filtre" action="suspect_voir_enr.php?client=<?= $_GET['client'] ?>&categorie=<?= $client['sus_categorie'] ?>" method="post"
                      class="admin-form form-inline form-inline-grid">
                      <?php if(isset($_GET['id'])): ?>
                        <input type="hidden" name="sin_id" value="<?= $_GET['id'] ?>">
                      <?php endif; ?>


                      <div class="row">
                        <div class="col-md-6">

  
                          <div class="section">
                          <label for="sin_type">Type d'info...</label>
                            <select name="sin_type" id="sin_type" class="form-control" required>
                              <?php if(isset($_GET['id'])): ?>
                                <?= get_client_info_select($info['sin_type'] , $client['sus_id']) ?>
                              <?php else: ?>
                                <?= get_client_info_select(0, $client['sus_id']); ?>
                              <?php endif; ?>
                            </select>

                          </div>

                        </div> 
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="section">
                            <select name="sin_famille" id="filtre_agence" class="form-control" onchange="sous_famille(this.value)">
                              <option value="0">Famille...</option>
                              <?php if (isset($_GET['id'])): ?>
                                <?= get_famille_select($info['sin_famille']); ?>
                              <?php else: ?>
                                <?= get_famille_select(0); ?>
                              <?php endif; ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="section">
                            <select name="sin_sous_famille" class="form-control" id="pro_sous_famille">
                              <option value="0">Sous-famille ...</option>
                              <?php if (isset($_GET['id']) && !empty($info['sin_famille'])):
                              echo get_sous_famille_select($info['sin_famille'], $info['sin_sous_famille']);
                              endif; ?>


                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                       <div class="col-md-6">

                        <div class="section">
                         
                          Description...
                            <textarea class="gui-textarea summernote" id="summernote" required name="sin_description"><?php if(isset($_GET['id'])): ?><?= $info['sin_description'] ?><?php endif; ?></textarea>
                            


                          
                        </div>
                      </div>
                    </div>
                    










                  </div>

                </div>


              </div>


            </div>
          </div>

        </div>
      </div>


    </section>
    <!-- End: Content -->
  </section>
</div>

<footer id="content-footer" class="affix">
  <div class="row">
    <div class="col-xs-3 footer-left">
      <a href="suspect_voir.php?client=<?= $_GET['client'] ?>&tab6" class="btn btn-default btn-sm">
        <i class="fa fa-long-arrow-left"></i>
        Retour
      </a>
    </div>
    <div class="col-xs-6 footer-middle"></div>
    <div class="col-xs-3 footer-right">

      <button type="submit" name="submit10" class="btn btn-success btn-sm">
        <i class='fa fa-floppy-o'></i> Enregistrer
      </button>
    </div>
  </div>
</footer>
</form>

<?php
include "includes/footer_script.inc.php"; ?>       


<script src="assets/js/custom.js"></script>
<script src="assets/js/responsive-tabs.js"></script>
<script src="vendor/plugins/summernote/summernote.min.js"></script>
<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
<script type="text/javascript">
$('.summernote').summernote({
        lang: 'fr-FR', // default: 'en-US'
        height: 255, //set editable area's height
        focus: false, //set focus editable area after Initialize summernote
        oninit: function() {},
        onChange: function(contents, $editable) {},
          toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
          ]
    });
  function sous_famille(selected_id){
    //-----------------------------------------------------------------------
    // 2) Send a http request with AJAX http://api.jquery.com/jQuery.ajax/
    //-----------------------------------------------------------------------

    $.ajax({
      type:'POST',
      url: 'ajax/ajax_sous_famille.php',
      //the script to call to get data
      data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
      //for example "id=5&parent=6"

      dataType: 'json',                //data format
      success: function(data)          //on recieve of reply
      {

        if (data['0'] == undefined){

          $("#pro_sous_famille").find("option:gt(0)").remove();

        }else{
          $("#pro_sous_famille").find("option:gt(0)").remove();
          $.each(data, function(key, value) {
            $('#pro_sous_famille')
            .append($("<option></option>")
              .attr("value",value["psf_id"])
              .text(value["psf_libelle"]));
          });
        }
      }
    });
  }

</script>

</body>
</html>
