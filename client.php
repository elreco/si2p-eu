<?php


$menu_actif = 1;
include "includes/controle_acces.inc.php";
error_reporting( error_reporting() & ~E_NOTICE );
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");
require "modeles/mod_client.php";
require "modeles/mod_societe.php";
require "modeles/mod_agence.php";
require "modeles/mod_utilisateur.php";
require "modeles/mod_parametre.php";
require "modeles/mod_prescripteur.php";
require "modeles/mod_contact.php";

require "modeles/mod_droit.php";
require "modeles/mod_get_commerciaux.php";

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];	
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];	
}

if(!empty(get_droit_utilisateur(8,$_SESSION['acces']['acc_ref_id']))){
    $acc_drt_grd_cpt=true;
}

$_SESSION['retour'] = "suspect_liste.php";

$c = array();
if (isset($_GET['id'])) {
  $c = get_client($_GET['id']);
}else{
  $c = array();
}

// categorie accessible pour le suspect
$d_categories=array();

//agence si societe gere
$d_agences=array();

$commerciaux=get_commerciaux($acc_agence,$acc_utilisateur,$_SESSION['acces']['acc_droits'][6],0);

// secteur d'activite

$d_secteurs=array();

// on écrase la recherche
unset($_SESSION['client_tableau']);
?>
<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8"> 
		<title>SI2P - Orion - Nouveau suspect</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<!-- Plugin -->
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<!-- Si2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

	</head>
	<body class="sb-top sb-top-sm ">
	
		<form id="form_int2" method="post" action="client_enr.php" enctype="multipart/form-data">
			<!-- Start: Main -->
			<div id="main">
  <?php			include "includes/header_def.inc.php";   ?>
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper" >				
					<section id="content" class="animated fadeIn">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<!-- deb PANEL FORM -->
								<div class="admin-form theme-dark ">
									<div class="panel heading-border panel-dark">
										
										<div class="panel-body bg-light">										
											<div class="text-center">
											
												<div class="content-header">
													<h2>Ajouter un <b class="text-dark">suspect</b></h2>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Informations générales</span> 
														</div>
													</div>
												</div>

												<div class="row" id="source1">
													<div class="col-md-4">
														<div class="section">
														  <div class="field prepend-icon">
															<input type="text" name="cli_code" id="cli_nom" class="gui-input cli_code" placeholder="Code">
															<label for="cli_code" class="field-icon">
															  <i class="fa fa-code"></i>
															</label>
														  </div>
														</div>
													</div>
													<div class="col-md-8">
														<div class="section">
														  <div class="field prepend-icon">
															<input type="text" name="cli_nom" id="cli_code" class="gui-input" placeholder="Nom">
															<label for="cli_nom" class="field-icon">
															  <i class="fa fa-building-o"></i>
															</label>
														  </div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6" id="source2">
														<div class="section">
														  <div class="field prepend-icon">
															<input type="text" name="cli_siren" id="cli_siren" class="gui-input siren" placeholder="Siren" >
															<label for="cli_siren" class="field-icon">
															  <i class="fa fa-barcode"></i>
															</label>
														  </div>
														</div>
													</div>
													<div class="col-md-6" id="source3">
														<div class="section">
														  <div class="field prepend-icon">
															<input type="text" name="cli_siret" id="cli_siret" class="gui-input siret" placeholder="Siret">
															<label for="cli_siret" class="field-icon">
															  <i class="fa fa-barcode"></i>
															</label>
														  </div>
														</div>
													</div>                       
												</div>
												<div class="row verification">
													<div class="col-md-12 text-center">
														<div class="section" id="loader">
														  <i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="font-size:12px;display:none;"></i>
														</div>
														<div class="section">

														  <button type="button" id="verif" class="btn btn-success btn-sm">Vérifier</button> <button type="button" id="ignorer" class="btn btn-warning btn-sm">Ignorer</button>
														</div>
													</div>
												</div>
												<div class="row verification">
													  <div class="col-md-12" id="msg1" style="display:none;">
														<div class="section">
														  <div class="alert alert-success">Il n'y a pas de suspect ni de client associé à ces informations.</div>
														</div>
													  </div>
													  <div class="col-md-12" id="msg2" style="display:none;">
														<div class="section">
														  <div class="alert alert-danger">Il y a un suspect ou un client associé à ces informations.</div>
														</div>
													  </div>
													  <div class="col-md-12" id="msg3" style="display:none;">
														<div class="section">
														  <div class="alert alert-danger">Veuillez renseigner le champ code ou nom.</div>
														</div>
													  </div>

													  <div class="col-md-12" id="msg4" style="display:none;">
														<div class="section">
														  <div class="alert alert-danger">Un client ou suspect existe déjà avec ce siret.</div>
														</div>
													  </div>													  
												</div>
												
												<!-- form apres verif -->
												<div class="reste-form" style="display:none;">   
												
													<div class="row">
														<div class="col-md-4">
															<div class="section">
															  <div class="field prepend-icon">
																<input type="text" name="cli_reference" id="cli_reference" class="gui-input" placeholder="Référence interne">
																<label for="cli_reference" class="field-icon">
																  <i class="fa fa-building-o"></i>
																</label>
															  </div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="section">
																<label for="cli_categorie">Catégorie</label>
																<select id="cli_categorie" name="cli_categorie" class="select2" required >
														<?php		if(!empty($d_categories)){
																		foreach($d_categories as $dc){
																			echo("<option value='" . $dc["cca_id"] . "' >" . $dc["cca_libelle"] . "</option>");
																		}
																	} ?>
																</select>
															</div>
														</div>
														<div class="col-md-6 cli_sous_categorie_bloc" style="display:none;">
															<select id="cli_sous_categorie" name="cli_sous_categorie" class="select2" >
																<option value="0">Sélectionner une sous catégorie...</option>                                   
															</select>                                 
														</div>
													</div>
													
													<div class="row">
														<div class="col-md-4">
															Sociéte
														</div>
												<?php 	if(!empty($d_agences)){ ?>
															<div class="col-md-4" >
																<div class="section">
																	<select id="cli_agence" name="cli_agence" class="select2" >
																		<option value="0">Agence...</option>
												<?php 					foreach($d_agences as $a){ 
																			echo("<option value='" . $dc["age_id"] . "' >" . $dc["age_libelle"] . "</option>");
																		} ?>
																	</select>
																</div>
															</div>
												<?php	} ?>
														<div class="col-md-4">
															<div class="section">
																<select name="cli_commercial" id="cli_commercial" class="select2" >
																	<option value="0">Commercial...</option>
										<?php						if(!empty($commerciaux)){
																		foreach($commerciaux as $com){
																			echo("<option value='" . $com["com_ref_1"] . "' >" . $com["com_label_1"] . " "  . $com["com_label_2"] . "</option>");
											 
																		}
																	} ?>
																</select>
															</div>
														</div>
													
													</div>
													
													<div class="row">
													
														<!-- COLONNE INTERVENTION -->
														<div class="col-md-6">
															
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Adresse d'intervention</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																  <div class="section">
																	<div class="field prepend-icon">
																	  <input type="text" name="adr_libelle1" id="adr_libelle1" class="gui-input" placeholder="Libellé"  value="<?=$s['adr_libelle']?>">
																	  <label for="cli_ad1" class="field-icon">
																		<i class="fa fa-tag"></i>
																	  </label>
																	</div>
																  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	  <div class="section">
																		<div class="field prepend-icon">
																		  <input type="text" name="adr_nom1" id="adr_nom1" class="gui-input" placeholder="Nom"  value="<?=$s['adr_nom']?>">
																		  <label for="cli_ad1" class="field-icon">
																			<i class="fa fa-tag"></i>
																		  </label>
																		</div>
																	  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																  <div class="section">
																	<div class="field prepend-icon">
																	  <input type="text" name="adr_service1" id="adr_service1" class="gui-input" placeholder="Service"  value="<?=$s['adr_service']?>">
																	  <label for="cli_ad1" class="field-icon">
																		<i class="fa fa-tag"></i>
																	  </label>
																	</div>
																  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																  <div class="section">
																	<div class="field prepend-icon">
																	  <input type="text" name="adr_ad11" id="adr_ad11" class="gui-input" placeholder="Adresse"  value="<?=$s['adr_ad1']?>">
																	  <label for="cli_ad1" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	  </label>
																	</div>
																  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																  <div class="section">
																	<div class="field prepend-icon">
																	  <input type="text" name="adr_ad21" class="gui-input" id="adr_ad21" placeholder="Adresse (Complément 1)" value="<?=$s['adr_ad2']?>">
																	  <label for="cli_ad2" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	  </label>
																	</div>
																  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																  <div class="section">
																	<div class="field prepend-icon">
																	  <input type="text" name="adr_ad31" id="adr_ad31" class="gui-input" placeholder="Adresse (Complément 2)" value="<?=$s['adr_ad3']?>">
																	  <label for="cli_ad3" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	  </label>
																	</div>
																  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-3">
																  <div class="section">
																	<div class="field prepend-icon">
																	  <input type="text" id="adr_cp1" name="adr_cp1" class="gui-input code-postal" placeholder="Code postal"  value="<?=$s['adr_cp']?>">
																	  <label for="cli_cp" class="field-icon">
																		<i class="fa fa fa-certificate"></i>
																	  </label>
																	</div>
																  </div>
																</div>
																<div class="col-md-9">
																	  <div class="section">
																		<div class="field prepend-icon">
																		  <input type="text" name="adr_ville1" id="adr_ville1" class="gui-input nom" placeholder="Ville"  value="<?=$s['adr_ville']?>">
																		  <label for="cli_ville" class="field-icon">
																			<i class="fa fa fa-building"></i>
																		  </label>
																		</div>
																	  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		Zone géographique
																	</div>
																	<div class="section">
																	  <label for="cli_geo11" class=" mt15 option option-dark">
																		<input type="radio" id="cli_geo11" name="cli_geo" value="1" checked>
																		<span class="radio"></span> France</label>

																		<label for="cli_geo21" class=" mt15 option option-dark">
																		  <input type="radio" id="cli_geo21" name="cli_geo" value="2">
																		  <span class="radio"></span> UE</label>

																		  <label for="cli_geo31" class=" mt15 option option-dark">
																			<input type="radio" id="cli_geo31" name="cli_geo" value="3" >
																			<span class="radio"></span> Autre</label>
																	</div>
																</div>
															</div>															
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																	  <label class="option option-dark">
																		<input type="checkbox" id="similaire" value="oui">
																		<span class="checkbox"></span>
																		<label for="similaire">Utiliser comme adresse de facturation</label>
																	  </label>
																	</div>
																</div>
															</div>
															
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Contact d'intervention par defaut</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																		<div class="field select">
																			<select name="con_fonction" id="con_fonction" onchange="nomchange(this.value)">
																				<option value="0">Sélectionner une fonction...</option>
                                                    <?php 						echo(get_contact_fonction_select(0); ?>
																				<option value="autre">Autre fonction...</option>
																			</select>
																			<i class="arrow simple"></i>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 con_fonction_nom_style" style="display:none;">
																	<div class="section">
																		<div class="field prepend-icon">
																		  <input type="text" name="con_fonction_nom" id="con_fonction_nom" class="gui-input" placeholder="Autre fonction"  >
																		  <label for="cli_nom" class="field-icon">
																			<i class="fa fa-tag"></i>
																		  </label>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<div class="field select">
																			<select name="con_titre" id="con_titre" >
																				<option value="0">Civilité...</option>
																				<option value="1">Monsieur</option>
																				<option value="2">Madame</option>
																			</select>
																			<i class="arrow simple"></i>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="con_nom" id="con_nom" class="gui-input nom" placeholder="Nom"  value="<?=$s['con_nom']?>">
																			<label for="con_nom" class="field-icon">
																				<i class="fa fa-user"></i>
																			</label>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	  <div class="section">
																		<div class="field prepend-icon">
																		  <input type="text" name="con_prenom" id="con_prenom" class="gui-input prenom" placeholder="Prénom"  value="<?=$s['cli_nom']?>">
																		  <label for="con_prenom" class="field-icon">
																			<i class="fa fa-tag"></i>
																		  </label>
																		</div>
																	  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																	<div class="field prepend-icon">
																	  <input name="con_tel" id="con_tel" type="tel" class="gui-input" placeholder="Numéro de téléphone"  value="<?=$s['cli_tel']?>">
																	  <label for="cli_tel" class="field-icon">
																		<i class="fa fa fa-phone"></i>
																	  </label>
																	</div>
																	</div>
																</div>
																<div class="col-md-6">
																	  <div class="section">
																		<div class="field prepend-icon">
																		  <input name="con_fax" id="con_fax" type="tel" class="gui-input" placeholder="Numéro de fax"  value="<?=$s['cli_fax']?>">
																		  <label for="cli_fax" class="field-icon">
																			<i class="fa fa-phone-square"></i>
																		  </label>
																		</div>
																	  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	  <div class="section">
																		<div class="field prepend-icon">
																		  <input name="con_portable" id="con_portable" type="tel" class="gui-input" placeholder="Numéro de portable"  value="<?=$s['con_portable']?>">
																		  <label for="cli_tel" class="field-icon">
																			<i class="fa fa fa-mobile"></i>
																		  </label>
																		</div>
																	  </div>
																</div>
																<div class="col-md-6">
																	  <div class="section">
																		<div class="field prepend-icon">
																		  <input type="email" name="con_mail" id="con_mail" class="gui-input" placeholder="Adresse Email"  value="<?=$s['cli_mail']?>">
																		  <label for="cli_mail" class="field-icon">
																			<i class="fa fa-envelope"></i>
																		  </label>
																		</div>
																	  </div>
																</div>
															</div>
														</div>
														<!-- FIN DE INTERVENTION -->
														
														<!-- FACTURATION -->
														<div class="col-md-6">
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																	  <span>Adresse de facturation</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																	  <div class="field prepend-icon">
																		<input type="text" name="adr_libelle2" id="adr_libelle2" class="gui-input" placeholder="Libellé"  value="<?=$s['adr_libelle']?>">
																		<label for="cli_ad1" class="field-icon">
																		  <i class="fa fa-tag"></i>
																		</label>
																	  </div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																	  <div class="field prepend-icon">
																		<input type="text" name="adr_nom2" id="adr_nom2" class="gui-input" placeholder="Nom"  value="<?=$s['adr_nom']?>">
																		<label for="adr_nom2" class="field-icon">
																		  <i class="fa fa-tag"></i>
																		</label>
																	  </div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																	  <div class="field prepend-icon">
																		<input type="text" name="adr_service2" id="adr_service2" class="gui-input" placeholder="Service"  value="<?=$s['adr_service']?>">
																		<label for="cli_ad1" class="field-icon">
																		  <i class="fa fa-tag"></i>
																		</label>
																	  </div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																	  <div class="field prepend-icon">
																		<input type="text" name="adr_ad12" id="adr_ad12" class="gui-input" placeholder="Adresse"  value="<?=$s['adr_ad1']?>">
																		<label for="cli_ad1" class="field-icon">
																		  <i class="fa fa-map-marker"></i>
																		</label>
																	  </div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																	  <div class="field prepend-icon">
																		<input type="text" name="adr_ad22" class="gui-input" id="adr_ad22" placeholder="Adresse (Complément 1)" value="<?=$s['adr_ad2']?>">
																		<label for="cli_ad2" class="field-icon">
																		  <i class="fa fa-map-marker"></i>
																		</label>
																	  </div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																	  <div class="field prepend-icon">
																		<input type="text" name="adr_ad32" id="adr_ad32" class="gui-input" placeholder="Adresse (Complément 2)" value="<?=$s['adr_ad3']?>">
																		<label for="cli_ad3" class="field-icon">
																		  <i class="fa fa-map-marker"></i>
																		</label>
																	  </div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-3">
																	<div class="section">
																	  <div class="field prepend-icon">
																		<input type="text" id="adr_cp2" name="adr_cp2" class="gui-input code-postal" placeholder="Code postal"  >
																		<label for="cli_cp" class="field-icon">
																		  <i class="fa fa fa-certificate"></i>
																		</label>
																	  </div>
																	</div>
																</div>
																<div class="col-md-9">
																	<div class="section">
																	  <div class="field prepend-icon">
																		<input type="text" name="adr_ville2" id="adr_ville2" class="gui-input nom" placeholder="Ville"  value="<?=$s['adr_ville']?>">
																		<label for="cli_ville" class="field-icon">
																		  <i class="fa fa fa-building"></i>
																		</label>
																	  </div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6 source2">
																	<div class="section">
																	  <div class="field prepend-icon">
																		<input type="text" name="cli_siren2" id="cli_siren2" class="gui-input siren" placeholder="Siren" >
																		<label for="cli_siren" class="field-icon">
																		  <i class="fa fa-barcode"></i>
																		</label>
																	  </div>
																	</div>
																</div>
																<div class="col-md-6 source3">
																	<div class="section">
																		  <div class="field prepend-icon">
																			<input type="text" name="cli_siret2" id="cli_siret2" class="gui-input siret" placeholder="Siret">
																			<label for="cli_siret" class="field-icon">
																			  <i class="fa fa-barcode"></i>
																			</label>
																		  </div>
																	</div>
																</div> 
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		Zone géographique
																	</div>
																	<div class="section">
																	  <label for="cli_geo12" class=" mt15 option option-dark">
																		<input type="radio" id="cli_geo12" name="cli_geo2" value="1" checked>
																		<span class="radio"></span> France</label>

																		<label for="cli_geo22" class=" mt15 option option-dark">
																		  <input type="radio" id="cli_geo22" name="cli_geo2" value="2">
																		  <span class="radio"></span> UE</label>

																		  <label for="cli_geo32" class=" mt15 option option-dark">
																			<input type="radio" id="cli_geo32" name="cli_geo2" value="3">
																			<span class="radio"></span> Autre</label>
																	</div>
																</div>
															</div>
														
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Facturation</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<span class="select">
																			<select id="cli_reg_type" name="cli_reg_type">
																				<option value="0">Mode de règlement...</option>
                                                    <?php 						foreach($base_reglement as $k => $n){
																					if($k > 0){ ?>
																						<option value="<?= $k ?>"><?= $n ?></option>
                                                      <?php 						} 
																				} ?>
																			</select>
																			<i class="arrow"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
															<?php	if($d_client["cli_reg_formule"]==1){ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_1" class="reg-formule" name="reg_formule" value="1" checked >
																			<span class="radio"></span>
																			
																		</label>
																		Date + <input type="number" max="60" size="2" name="reg_nb_jour_1" value="<?=$d_client["cli_reg_nb_jour"]?>" class="champ-reg-formule reg-formule-1" /> jours
															<?php	}else{ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_1" class="reg-formule" name="reg_formule" value="1" />
																			<span class="radio"></span>
																		</label>
																		Date + <input type="number" max="60" size="2" name="reg_nb_jour_1" value="0" class="champ-reg-formule reg-formule-1" disabled /> jours
															<?php	} ?>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
																	<label class="option">
																		<input type="radio" id="reg_formule_2" class="reg-formule" name="reg_formule" value="2" <?php if($d_client["cli_reg_formule"]==2) echo("checked"); ?> >
																		<span class="radio"></span>
																	</label>
																	Date + 45j + fin de mois
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
																	<label class="option">
																		<input type="radio" id="reg_formule_3" class="reg-formule" name="reg_formule" value="3" <?php if($d_client["cli_reg_formule"]==3) echo("checked"); ?> >
																		<span class="radio"></span>
																	</label>
																	Date + Fin de mois + 45j
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
															<?php	if($d_client["cli_reg_formule"]==4){ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_4" class="reg-formule" name="reg_formule" value="4" checked />
																			<span class="radio"></span>
																		</label>
																		Date + <input type="number" max="30" size="2" maxlength="2" name="reg_nb_jour_4" value="<?=$d_client["cli_reg_nb_jour"]?>" class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
																		+ Fin de mois +  <input type="number" size="2" maxlength="2" max="20" name="reg_fdm_4" value="<?=$d_client["cli_reg_fdm"]?>" class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
															<?php	}else{ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_4" class="reg-formule" name="reg_formule" value="4" />
																			<span class="radio"></span>
																		</label>
																		Date + <input type="number" max="30" size="2" maxlength="2"name="reg_nb_jour_4" value="0" disabled class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
																		+ Fin de mois +  <input type="number" size="2" maxlength="2" max="20" name="reg_fdm_4" value="0" disabled class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
															<?php	} ?>	
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																		<label class="option option-dark">
																		<input type="checkbox" name="cli_facture_opca" id="cli_facture_opca" value="oui">
																			<span class="checkbox"></span>
																			<label for="cli_facture_opca">Facturer à l'OPCA</label>
																		  </label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="section">
																	  <span class="select">
																		<select id="cli_opca"  name="cli_opca" >
																		  <option value="0">Sélectionner un OPCA...</option>
																		  <?php if (isset($_GET['id'])): ?>
																			<?=get_client_opca_select($s['cli_opca']);?>
																		  <?php else: ?>
																			<?=get_client_opca_select(0);?>
																		  <?php endif;?>
																		</select>
																		<i class="arrow"></i>
																	  </span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="cli_ident_tva" id="cli_ident_tva" class="gui-input" placeholder="Identification TVA" >
																			<label for="cli_ident_tva" class="field-icon">
																			  <i class="fa fa-barcode"></i>
																			</label>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="section">
																	  <div class="option-group field">
																		<label class="option option-dark">
																		  <input type="checkbox" name="cli_releve"
																		  value="cli_releve">
																		  <span class="checkbox"></span>Relevés de factures</label>

																		</div>
																	  </div>
																</div>
															</div>
														</div>
														<!-- fin de facturation -->
													</div>
													
													<div class="section-divider mb40">
														<span>Autres infos</span> 
													</div>
													
													<div class="row">
														<div class="col-md-6">
															<div class="section">
																<select id="cli_classification" name="cli_classification" class="select2 select2-classification" >
																	<option value="0">Sélectionner une classification...</option>
																	<?=get_client_classification_select(0);?>
																</select>															 
															</div>
														</div>
														<div class="col-md-6">
															<select id="cli_sous_classification" name="cli_sous_classification" class="select2 select2-sous-classification" >
																<option value="0">Sous-classification...</option>
															</select>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<select id="cli_stagiaire_type" name="cli_stagiaire_type" class="select2" >
																<option value="0">Type de stagiaire...</option>
															</select>															
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<label for="cli_secteur_activite" >Secteur d'activité</label>
															<select id="cli_secteur_activite" name="cli_secteur" class="select2" >																	
																<option value="0">Secteur...</option>
													<?php		if(!empty($d_secteurs)){
																	foreach($d_secteurs as $ds){
																		if($ds["ase_id"]==$d_client["cli_secteur"]){
																			echo("<option value='" . $ds["ase_id"] . "' selected >" . $ds["ase_libelle"] . "</option>");
																		}
																	} 
																} ?>
															</select>
														</div>
														<div class="col-md-6">
															<select id="cli_sous_sec_activite" name="cli_sous_secteur" class="select2" >
																<option value="0">Sous-secteur...</option>
															</select>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="section">
																<input type="text" name="cli_ape" id="cli_ape" class="gui-input" placeholder="APE"  value="<?=$s['cli_ape']?>">
															</div>
														</div>
														<div class="col-md-6">
															<div class="section">
																<select id="cli_prescripteur"  name="cli_prescripteur" class="select2"  >
																	<option value="0">Sélectionner un prescripteur...</option>
																	<?=get_prescripteur_select(0);?>
																</select>
															</div>
														</div>
													</div>
                          
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- FIN PANEL FORM -->
                            </div>
						</div>
					</section>
					<!-- End: Content -->
				</section>
				<!-- End: Content WRAPPER -->
			</div>
			<!-- End: Main -->
			
			<footer id="content-footer" class="affix">
				<div class="row"> 
                    <div class="col-xs-3 footer-left" >
				<?php 	if(isset($_SESSION['retour'])){ ?>
							<a href="<?=$_SESSION['retour']?>" class="btn btn-default btn-sm" >
								<i class="fa fa-long-arrow-left"></i> Retour
							</a>
				<?php 	} ?>
					</div>
                    <div class="col-xs-6 footer-middle"></div>
                    <div class="col-xs-3 footer-right" >
						<button type="submit" name="submit" id="sus_submit" style="display:none;" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
                    </div>
				</div>
			</footer>
		</form>
 <?php	include "includes/footer_script.inc.php"; ?>	


	  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
	  <script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>

	  <script src="vendor/plugins/mask/jquery.mask.js"></script>
	  <script src="vendor/plugins/select2/js/select2.min.js"></script>

	  <script src="vendor/plugins/holder/holder.min.js"></script>
	  <script src="assets/js/custom.js"></script>

	  <script type="text/javascript">

                function sous_categorie(selected_id){
                  //-----------------------------------------------------------------------
                  // 2) Send a http request with AJAX http://api.jquery.com/jQuery.ajax/
                  //-----------------------------------------------------------------------
                  if(selected_id == 3){
                    $("#destination2").hide();
                    $(".source2").hide();
                    $(".source3").hide();
                  }else{
                    $("#destination2").show();
                    $(".source2").show();
                    $(".source3").show();
                  }
                  if(selected_id == 2){

                    <?php if($acc_drt_grd_cpt == true){ ?>

                      if($("#cli_groupe").is(':checked') && $("#cli_maison_mere_non").is(':checked')){
                      $('#cli_societe_2').show();
                      $(".maisonm").show();
                    }
                    <?php }else{ ?>
                      $('#cli_societe_2').hide();
                      $('#cli_societe option:selected').removeAttr('selected');
                      $('#cli_societe option[value=<?= $_SESSION['acces']['acc_societe'] ?>]').attr('selected','selected');
                    <?php } ?>
                    


                  }else{
                    $('#cli_societe_2').hide();
                    //$("#cli_societe_2").find("option:gt(0)").remove();
                  }
                  $.ajax({
                    type:'POST',
                    url: 'ajax/ajax_sous_categorie.php',
                    //the script to call to get data
                    data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
                    //for example "id=5&parent=6"

                    dataType: 'json',                //data format
                    success: function(data)          //on recieve of reply
                    {

                      if (data['0'] == undefined){

                        $("#cli_sous_categorie").find("option:gt(0)").remove();
                        $(".cli_sous_categorie_bloc").hide();
                      }else{
                        $(".cli_sous_categorie_bloc").show();
                        $("#cli_sous_categorie").find("option:gt(0)").remove();
                        $.each(data, function(key, value) {
                          $('#cli_sous_categorie')
                          .append($("<option></option>")
                            .attr("value",value["csc_id"])
                            .text(value["csc_libelle"]));
                        });
                      }
                    }
                  });
                }
  function filiale(societe){
          $("#cli_filiale_de").find("option:gt(0)").remove();
  }
  function classification(selected_id){

    //-----------------------------------------------------------------------
    // 2) Send a http request with AJAX http://api.jquery.com/jQuery.ajax/
    //-----------------------------------------------------------------------
    $.ajax({
      type:'POST',
      url: 'ajax/ajax_classification.php',
      //the script to call to get data
      data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
      //for example "id=5&parent=6"

      dataType: 'json',                //data format
      success: function(data)          //on recieve of reply
      {

        if (data['0'] == undefined){

          $("#cli_sous_classification").find("option:gt(0)").remove();

        }else{
          $.each(data, function(key, value) {

            $('#cli_sous_classification')
            .append($("<option></option>")
              .attr("value",value["csc_id"])
              .text(value["csc_libelle"]));
          });
        }


        //get name
        //--------------------------------------------------------------------
        // 3) Update html content
        //--------------------------------------------------------------------
        //Set output element html
        //recommend reading up on jquery selectors they are awesome
        // http://api.jquery.com/category/selectors/
      }
    });

    
  }

// debut client select
var type_soc = $('input[name=sus_groupe_client]:checked', '#myForm').val(); 
  $('#myForm input').on('change', function() {
    type_soc = $('input[name=sus_groupe_client]:checked', '#myForm').val(); 
    $("#cli_filiale_de").find("option:gt(0)").remove();
  });
var societe_ajax = $('#cli_societe').val();
$('#cli_societe').on('change', function() {
  societe_ajax = $('#cli_societe').val();
});
// catégorie client
var categorie_ajax = $('#cli_categorie').val();
$('#cli_categorie').on('change', function() {
  categorie_ajax = $('#cli_categorie').val();
});
</script>

<script>
  var tab_sous_secteur=new Array();
  
  tab_sous_secteur[0]=new Array(2);
  tab_sous_secteur[0][0]="&nbsp;";
  tab_sous_secteur[0][1]="0";
  
  tab_sous_secteur[1]=new Array(2);
  tab_sous_secteur[1][0]="Agroalimentaire ";
  tab_sous_secteur[1][1]="1";
  
  tab_sous_secteur[2]=new Array(2);
  tab_sous_secteur[2][0]="Grande Distribution - Commerce ";
  tab_sous_secteur[2][1]="1";
  
  tab_sous_secteur[3]=new Array(2);
  tab_sous_secteur[3][0]="Hôtellerie - Restauration";
  tab_sous_secteur[3][1]="1";
  
  tab_sous_secteur[4]=new Array(2);
  tab_sous_secteur[4][0]="Banques - Etablissements financiers ";
  tab_sous_secteur[4][1]="2";
  
  tab_sous_secteur[5]=new Array(2);
  tab_sous_secteur[5][0]="Assurances ";
  tab_sous_secteur[5][1]="2";
  
  tab_sous_secteur[6]=new Array(2);
  tab_sous_secteur[6][0]="Conseil - Audit - Expertise";
  tab_sous_secteur[6][1]="2";
  
  tab_sous_secteur[7]=new Array(2);
  tab_sous_secteur[7][0]="BTP, Matériaux de construction";
  tab_sous_secteur[7][1]="3";
  
  tab_sous_secteur[8]=new Array(2);
  tab_sous_secteur[8][0]="Ingénierie ";
  tab_sous_secteur[8][1]="3";
  
  tab_sous_secteur[9]=new Array(2);
  tab_sous_secteur[9][0]="Immobilier ";
  tab_sous_secteur[9][1]="3";
  
  tab_sous_secteur[10]=new Array(2);
  tab_sous_secteur[10][0]="Audiovisuel, Presse, Edition";
  tab_sous_secteur[10][1]="4";
  
  tab_sous_secteur[11]=new Array(2);
  tab_sous_secteur[11][0]="Communication - Publicité";
  tab_sous_secteur[11][1]="4";
  
  tab_sous_secteur[12]=new Array(2);
  tab_sous_secteur[12][0]="Cosmétiques - Chimie - Pharmacie";
  tab_sous_secteur[12][1]="5";
  
  tab_sous_secteur[13]=new Array(2);
  tab_sous_secteur[13][0]="Distribution - Commerce";
  tab_sous_secteur[13][1]="6";
  
  tab_sous_secteur[14]=new Array(2);
  tab_sous_secteur[14][0]="Luxe ";
  tab_sous_secteur[14][1]="6";
  
  tab_sous_secteur[15]=new Array(2);
  tab_sous_secteur[15][0]="Habillement - Chaussures";
  tab_sous_secteur[15][1]="6";
  
  tab_sous_secteur[16]=new Array(2);
  tab_sous_secteur[16][0]="Informatique - Bureautique";
  tab_sous_secteur[16][1]="7";
  
  tab_sous_secteur[17]=new Array(2);
  tab_sous_secteur[17][0]="Telecom - Réseaux";
  tab_sous_secteur[17][1]="7";
  
  tab_sous_secteur[18]=new Array(2);
  tab_sous_secteur[18][0]="Electrique - Electronique - Electroménager";
  tab_sous_secteur[18][1]="7";
  
  tab_sous_secteur[19]=new Array(2);
  tab_sous_secteur[19][0]="SSII ";
  tab_sous_secteur[19][1]="7";
  
  tab_sous_secteur[20]=new Array(2);
  tab_sous_secteur[20][0]="Aéronautique";
  tab_sous_secteur[20][1]="8";
  
  tab_sous_secteur[21]=new Array(2);
  tab_sous_secteur[21][0]="Défense - Armement ";
  tab_sous_secteur[21][1]="8";
  
  tab_sous_secteur[22]=new Array(2);
  tab_sous_secteur[22][0]="Energie ";
  tab_sous_secteur[22][1]="9";
  
  tab_sous_secteur[23]=new Array(2);
  tab_sous_secteur[23][0]="Ingénierie";
  tab_sous_secteur[23][1]="9";
  
  tab_sous_secteur[24]=new Array(2);
  tab_sous_secteur[24][0]="Industries diverses";
  tab_sous_secteur[24][1]="9";
  
  tab_sous_secteur[25]=new Array(2);
  tab_sous_secteur[25][0]="Métallurgie - Sidérurgie";
  tab_sous_secteur[25][1]="10";
  
  tab_sous_secteur[26]=new Array(2);
  tab_sous_secteur[26][0]="Mécanique - Optique";
  tab_sous_secteur[26][1]="10";
  
  tab_sous_secteur[27]=new Array(2);
  tab_sous_secteur[27][0]="Plasturgie";
  tab_sous_secteur[27][1]="10";
  
  tab_sous_secteur[28]=new Array(2);
  tab_sous_secteur[28][0]="Papier - Carton ";
  tab_sous_secteur[28][1]="10";
  
  tab_sous_secteur[29]=new Array(2);
  tab_sous_secteur[29][0]="Verre - Céramique ";
  tab_sous_secteur[29][1]="10";
  
  tab_sous_secteur[30]=new Array(2);
  tab_sous_secteur[30][0]="Industries diverses ";
  tab_sous_secteur[30][1]="10";
  
  tab_sous_secteur[31]=new Array(2);
  tab_sous_secteur[31][0]="Automobile - Equipementiers";
  tab_sous_secteur[31][1]="11";
  
  tab_sous_secteur[32]=new Array(2);
  tab_sous_secteur[32][0]="Logistique - Transports ";
  tab_sous_secteur[32][1]="11";
  
  tab_sous_secteur[33]=new Array(2);
  tab_sous_secteur[33][0]=" Services Publics ";
  tab_sous_secteur[33][1]="12";
  
  tab_sous_secteur[34]=new Array(2);
  tab_sous_secteur[34][0]="ONG ";
  tab_sous_secteur[34][1]="12";
  
  tab_sous_secteur[35]=new Array(2);
  tab_sous_secteur[35][0]="Hôtellerie - Restauration ";
  tab_sous_secteur[35][1]="13";
  
  tab_sous_secteur[36]=new Array(2);
  tab_sous_secteur[36][0]="Tourisme - Sports - Loisirs ";
  tab_sous_secteur[36][1]="13";
  
  tab_sous_secteur[37]=new Array(2);
  tab_sous_secteur[37][0]="entreprises de service aux entreprises";
  tab_sous_secteur[37][1]="14";
  
  tab_sous_secteur[38]=new Array(2);
  tab_sous_secteur[38][0]="Personne individuelle";
  tab_sous_secteur[38][1]="15";
  
  tab_sous_secteur[39]=new Array(2);
  tab_sous_secteur[39][0]="Revendeurs de prestations de formation.";
  tab_sous_secteur[39][1]="16";
  
  tab_sous_secteur[40]=new Array(2);
  tab_sous_secteur[40][0]="bâtiments et matériaux de construction";
  tab_sous_secteur[40][1]="17";
  
  tab_sous_secteur[41]=new Array(2);
  tab_sous_secteur[41][0]="travaux publics";
  tab_sous_secteur[41][1]="17";
  
  tab_sous_secteur[42]=new Array(2);
  tab_sous_secteur[42][0]="hygiène, santé cosmétiques";
  tab_sous_secteur[42][1]="18";
  
  tab_sous_secteur[43]=new Array(2);
  tab_sous_secteur[43][0]="laboratoire médical et fabrication de médicaments";
  tab_sous_secteur[43][1]="18";
  
  tab_sous_secteur[44]=new Array(2);
  tab_sous_secteur[44][0]="équipements ";
  tab_sous_secteur[44][1]="18";
  
  tab_sous_secteur[45]=new Array(2);
  tab_sous_secteur[45][0]="Distribution spécialisée";
  tab_sous_secteur[45][1]="19";
  
  tab_sous_secteur[46]=new Array(2);
  tab_sous_secteur[46][0]="distributeurs généralistes";
  tab_sous_secteur[46][1]="19";
  
  tab_sous_secteur[47]=new Array(2);
  tab_sous_secteur[47][0]="médias et publicité";
  tab_sous_secteur[47][1]="19";
  
  tab_sous_secteur[48]=new Array(2);
  tab_sous_secteur[48][0]="voyages et loirsirs";
  tab_sous_secteur[48][1]="19";
  
  tab_sous_secteur[49]=new Array(2);
  tab_sous_secteur[49][0]="gaz eau et services multiples aux collectivités";
  tab_sous_secteur[49][1]="20";
  
  tab_sous_secteur[50]=new Array(2);
  tab_sous_secteur[50][0]="clinique privée";
  tab_sous_secteur[50][1]="18";
  
  tab_sous_secteur[51]=new Array(2);
  tab_sous_secteur[51][0]="maison de retraite";
  tab_sous_secteur[51][1]="18";
  
  tab_sous_secteur[52]=new Array(2);
  tab_sous_secteur[52][0]="hopitaux publics";
  tab_sous_secteur[52][1]="21";
  
  tab_sous_secteur[53]=new Array(2);
  tab_sous_secteur[53][0]="maison de retraite public";
  tab_sous_secteur[53][1]="21";
  
  tab_sous_secteur[54]=new Array(2);
  tab_sous_secteur[54][0]="établissements de soins public";
  tab_sous_secteur[54][1]="21";
  
  tab_sous_secteur[55]=new Array(2);
  tab_sous_secteur[55][0]="IME ET IMP ";
  tab_sous_secteur[55][1]="21";
  
  tab_sous_secteur[56]=new Array(2);
  tab_sous_secteur[56][0]="Intérim ";
  tab_sous_secteur[56][1]="14";
  
  tab_sous_secteur[57]=new Array(2);
  tab_sous_secteur[57][0]="Agencement et Aménagement de Bureaux";
  tab_sous_secteur[57][1]="22";
  var cli_sous_sec_activite=0;
  function changeSecteurAct()
  {   cli_secteur_activite=document.getElementById("cli_secteur_activite").value;
  liste=document.getElementById("cli_sous_sec_activite");
  for(i=liste.length;i>0;i--)
    liste.options[i]=null;
  nbOption=0;
  for(i=1;i<=tab_sous_secteur.length;i++)
    {   if(tab_sous_secteur[i][1]==cli_secteur_activite)
      {   nbOption=nbOption+1;
        liste.options[nbOption] = new Option(tab_sous_secteur[i][0],i);
      };
    };
  }

  function nomchange(selected){

    if(selected == "autre"){
      $(".con_fonction_nom_style").show(400);
    }else{
      $(".con_fonction_nom_style").hide(400);
    }

  }
  $('input[type=radio][name=cli_geo]').change(function() {
    if($("#similaire").is(":checked")){
     for (var i = 1; i<4; i++){
      if($("#cli_geo"+ i + "1").is(':checked')){
       $("#cli_geo"+ i + "2").prop("checked", true);	
     }else{
       $("#cli_geo"+ i + "2").prop("checked", false);	
     }
   };
 }
});

  jQuery(document).ready(function () {

   /*$(".chosenselect").select2();*/
    // validation formulaire
    $("#form_int2").submit(function(){

      if($("#cli_nom").val() == ""){ 
        
        $("#adr_cp1").rules("add", {
          required: true
        });
        $("#adr_ville1").rules("add", {
          required: true
        });      
      }else{

        $("#adr_cp1").rules("add", {
          required: false
        });
        $("#adr_ville1").rules("add", {
          required: false
        });

      }
    });
    $("#form_int2").validate();
    // fin
    $("#cli_rib1").attr("disabled", false);
    $("#cli_rib2").attr("disabled", true);
    $("#cli_rib3").attr("disabled", true);
    $("#cli_rib2").attr("value", 0);
    $("#cli_rib3").attr("value", 0);
    $( "#reg1" ).click(function() {

      $("#cli_rib1").attr("disabled", false);
      $("#cli_rib2").attr("disabled", true);
      $("#cli_rib3").attr("disabled", true);
      $("#cli_rib2").attr("value", 0);
      $("#cli_rib3").attr("value", 0);

    });

    $( "#reg2" ).click(function() {

      $("#cli_rib1").attr("disabled", true);
      $("#cli_rib2").attr("disabled", true);
      $("#cli_rib3").attr("disabled", true);
      $("#cli_rib1").attr("value", 0);
      $("#cli_rib2").attr("value", 0);
      $("#cli_rib3").attr("value", 0);

    });
    $( "#reg3" ).click(function() {

      $("#cli_rib1").attr("disabled", true);
      $("#cli_rib2").attr("disabled", true);
      $("#cli_rib3").attr("disabled", true);
      $("#cli_rib1").attr("value", 0);
      $("#cli_rib2").attr("value", 0);
      $("#cli_rib3").attr("value", 0);

    });
    $( "#reg4" ).click(function() {

      $("#cli_rib1").attr("disabled", true);
      $("#cli_rib2").attr("disabled", false);
      $("#cli_rib3").attr("disabled", false);
      $("#cli_rib1").attr("value", 0);

    });

    $( "#cli_rib1" ).focusout(function() {

      if($("#cli_rib1").val() > 60 ){
        $("#cli_rib1").val(60);
      }
      if($("#cli_rib1").val() < 0 ){
        $("#cli_rib1").val(0);
      }

    });
    $( "#cli_rib2" ).focusout(function() {

      if($("#cli_rib2").val() > 30 ){
        $("#cli_rib2").val(30);
      }
      if($("#cli_rib2").val() < 0 ){
        $("#cli_rib2").val(0);
      }

    });
    $( "#cli_rib3" ).focusout(function() {

      if($("#cli_rib3").val() > 20 ){
        $("#cli_rib3").val(20);
      }
      if($("#cli_rib3").val() < 0 ){
        $("#cli_rib3").val(0);
      }

    });
    $( "#cli_siren2" ).focusout(function() {
      if ( $("#cli_siren2").val().length != 11){

        $("#cli_siren2").val('');
        $("#cli_siren").val("");
      }else{

        $("#cli_siren").val($("#cli_siren2").val());
      }
    });
    $( "#cli_siren" ).focusout(function() {

      if ( $(this).val().length != 11){
        $(this).val("");
        $("#cli_siren2").val('');
      }else{

        $("#cli_siren2").val( $("#cli_siren").val());
      }   
    });
    $( "#cli_siret" ).focusout(function() {
      if ( $(this).val().length != 6){
        $(this).val('');
        $("#cli_siret2").val("");
      }else{

        $("#cli_siret2").attr("value", $("#cli_siret").val());

        
      }
      
    });
    $( "#cli_siret2" ).focusout(function() {
      if ( $(this).val().length != 6){
        $(this).val('');
      }
      
    });
    

    // SIMILAIRE A L'ADRESSE D'INTERVENTION FONCTION
    $("#similaire").change(function(event) {

      if($("#similaire").is(':checked')){

        $("#adr_libelle1").keyup(function(event) {
          $("#adr_libelle2").attr("value", $("#adr_libelle1").val());
        });
        $("#adr_nom1").keyup(function(event) {
          $("#adr_nom2").attr("value", $("#adr_nom1").val());
        });
        $("#adr_service1").keyup(function(event) {
          $("#adr_service2").attr("value", $("#adr_service1").val());
        });
        $("#adr_libelle1").keyup(function(event) {
          $("#adr_libelle2").attr("value", $("#adr_libelle1").val());
        });
        $("#adr_ad11").keyup(function(event) {
         $("#adr_ad12").attr("value", $("#adr_ad11").val());
       });
        $("#adr_ad21").keyup(function(event) {
         $("#adr_ad22").attr("value", $("#adr_ad21").val());
       });
        $("#adr_ad31").keyup(function(event) {
         $("#adr_ad32").attr("value", $("#adr_ad31").val());
       });
        $("#adr_cp1").keyup(function(event) {
         $("#adr_cp2").attr("value", $("#adr_cp1").val());
       });
        $("#adr_ville1").keyup(function(event) {
         $("#adr_ville2").attr("value", $("#adr_ville1").val());
       });

        

        $("#adr_libelle2").attr("value", $("#adr_libelle1").val());
        $("#adr_nom2").attr("value", $("#adr_nom1").val());
        $("#adr_service2").attr("value", $("#adr_service1").val());
        $("#adr_libelle2").attr("value", $("#adr_libelle1").val());
        $("#adr_ad12").attr("value", $("#adr_ad11").val());
        $("#adr_ad22").attr("value", $("#adr_ad21").val());
        $("#adr_ad32").attr("value", $("#adr_ad31").val());
        $("#adr_cp2").attr("value", $("#adr_cp1").val());
        $("#adr_ville2").attr("value", $("#adr_ville1").val());
        
        
        for (var i = 1; i<4; i++){
         if($("#cli_geo"+ i + "1").is(':checked')){
          $("#cli_geo"+ i + "2").prop("checked", true);	
        }else{
          $("#cli_geo"+ i + "2").prop("checked", false);	
        }
      };
    }else{

      $("#adr_libelle1").off();
      $("#adr_nom1").off();
      $("#adr_service1").off();
      $("#adr_libelle1").off();
      $("#adr_ad11").off();
      $("#adr_ad21").off();
      $("#adr_ad31").off();
      $("#adr_cp1").off();
      $("#adr_ville1").off();

      $("#adr_libelle2").attr("value", "");
      $("#adr_nom2").attr("value", "");
      $("#adr_service2").attr("value", "");
      $("#adr_libelle2").attr("value", "");
      $("#adr_ad12").attr("value", "");
      $("#adr_ad22").attr("value", "");
      $("#adr_ad32").attr("value", "");
      $("#adr_cp2").attr("value", "");
      $("#adr_ville2").attr("value", "");

      $("#cli_geo12").prop("checked", true);	
      $("#cli_geo22").prop("checked", false);	
      $("#cli_geo32").prop("checked", false);	
    }

  });
    // FIN DE SIMILAIRE A L'ADRESSE D'INTERVENTION FONCTION


    $("#cli_code").keyup(function(event) {
      var stt = $(this).val();
      $("#adr_nom2").attr("value", stt);
      $("#adr_nom1").attr("value", stt);
    });


    if($("#cli_maison_mere").is(':checked')){

      $("#cli_filiale_de").find("option:gt(0)").remove();
      $(".filiale").hide();  // checked
      $(".filiale2").hide(400);
      $("#cli_societe_2").hide(400);

    }else if($("#cli_maison_mere_non").is(':checked')){

        $(".filiale").show(400);  // checked
        $(".filiale2").show(400);
if(selected_id == 2){

      <?php if($acc_drt_grd_cpt == true): ?> 

        $('#cli_societe_2').show();

      <?php else: ?>

        $('#cli_societe_2').hide();
        $('#cli_societe option:selected').removeAttr('selected');
        $('#cli_societe option[value=<?= $_SESSION['acces']['acc_societe'] ?>]').attr('selected','selected');

      <?php endif; ?>
}else{
                    $('#cli_societe_2').hide();
                    //$("#cli_societe_2").find("option:gt(0)").remove();
                  }
    }

    $(".filiale").hide(); 
    $(".filiale2").hide();
    $('#cli_groupe').click(function() {
      if($("#cli_groupe").is(':checked')){
        $(".maisonm").show(400);
        if($("#cli_maison_mere").is(':checked')){
          $('#cli_societe_2').hide();
          $(".filiale").hide();
          $(".filiale2").hide();

          
        }else if($("#cli_maison_mere_non").is(':checked')){

          $(".filiale").show();
          $(".filiale2").show();
          if($("#cli_categorie").val() == 2){
          
          
          <?php if($acc_drt_grd_cpt == true): ?>
            
          $('#cli_societe_2').show();
          <?php else: ?>

            $('#cli_societe_2').hide();
            $('#cli_societe option:selected').removeAttr('selected');
            $('#cli_societe option[value=<?= $_SESSION['acces']['acc_societe'] ?>]').attr('selected','selected');
            
          <?php endif; ?>

          }else{
            $('#cli_societe_2').hide();
          }
        }

          <?php if($acc_drt_grd_cpt == true){ ?>
          var societe = $( "#cli_societe_2" ).val();
          <?php }else{ ?>
          var societe = <?= $_SESSION['acces']['acc_societe'] ?>;
          <?php } ?>

          $(".chosenselect").select2({
          minimumInputLength: 1,
          allowClear: true,
          ajax: {
            url: "ajax/ajax_client_groupe.php",
            dataType: 'json',

            delay: 250,
            method: 'GET',
            
            data: function (params) {
                
                var query = {
                  s: params.term,
                  t: type_soc,
                  societe: societe_ajax,
                  categorie: categorie_ajax,
                  client : 0,
                }

            // Query paramters will be ?search=[term]&page=[page]
              return query;
                
            },
            processResults: function (data) {
              // data correspond au tableau json des résultat
              // data.nom permet d'accéder à l'information $retour["nom"]
              
              return {
                results: data,
              };
            },
          },
          templateResult: function(data) {
            // permet de chosir quelles informations seront affichées dans la liste de résultat
            // parmit les éléments de data
            //if (data.loading) return data.text;
            return data.nom;
          },
          templateSelection: function(data) {
            // permet de chosir quelles informations seront affichées dans le select
            // parmit les éléments de data
            // une fois que l'utilisateur aura fait un choix.
            
            //if (data.loading) return data.text;
            // Sinon affichage du placeholder (data.text).
            //if (data.text) return data.text;

            return data.nom;
          },
        });


        
        // fin de client select
        }else{
          $(".filiale").hide(400);  // checked
          $(".filiale2").hide(400);
          $(".maisonm").hide(400);
          $('#cli_societe_2').hide();
          $('#cli_societe option:selected').removeAttr('selected');
        }
      });
     $(".maisonm").hide();
        if($("#cli_maison_mere").is(':checked')){
          $("#cli_filiale_de").find("option:gt(0)").remove();
          $(".filiale").hide();  // checked
          $(".filiale2").hide(400);
          $("#cli_societe_2").hide(400);
        }else{
          $(".filiale").show(400);  // checked
          $(".filiale2").show(400);
          if($("#cli_categorie").val() == 2){
            <?php if($acc_drt_grd_cpt == true): ?>
              
            $('#cli_societe_2').show();
            <?php else: ?>

              $('#cli_societe_2').hide();
              $('#cli_societe option:selected').removeAttr('selected');
              $('#cli_societe option[value=<?= $_SESSION['acces']['acc_societe'] ?>]').attr('selected','selected');
            <?php endif; ?>
          }else{
            $('#cli_societe_2').hide();
          }

        }

     $('input[type=radio][name=cli_maison_mere]').change(function() {

        if($("#cli_maison_mere").is(':checked')){

          $("#cli_filiale_de").find("option:gt(0)").remove();

          $(".filiale").hide();  // checked
          $(".filiale2").hide(400);
          $("#cli_societe_2").hide(400);

        }else if($("#cli_maison_mere_non").is(':checked')){

          $(".filiale").show(400);  // checked
          $(".filiale2").show(400);
          if($("#cli_categorie").val() == 2){
          <?php if($acc_drt_grd_cpt == true): ?>

            $('#cli_societe_2').show();

          <?php else: ?>

            $('#cli_societe_2').hide();
            $('#cli_societe option:selected').removeAttr('selected');
            $('#cli_societe option[value=<?= $_SESSION['acces']['acc_societe'] ?>]').attr('selected','selected');

          <?php endif; ?>
          }else{
            $('#cli_societe_2').hide();
          }
        }
     });

    $('.reste-form').hide();

    $( "#ignorer" ).click(function() {
     $("#sus_submit").show(400);
     $(this).hide(400);
     $('.reste-form').show(400);
     $("#source1").prependTo("#destination1");
     $("#source2").prependTo("#destination2");
     $("#source2").toggleClass("col-md-6", "col-md-12");
     $("#source3").hide();
     $( "#verif" ).hide();
   });
    $('#msg1').hide();
    $('#msg2').hide();
    $('#msg3').hide();
    $('#msgsiret1').hide();
    $('#msgsiret2').hide();
    $('#msgsiret3').hide();

    $( "#verif" ).click(function() {
      $("#sus_submit").show(400);
      $('#loader').show();
      var code= $('#cli_code').val();
      var nom= $('#cli_nom').val();
      var siret= $('#cli_siret').val();
      var siren= $('#cli_siren').val();
      $('#msg3').hide();
      code = encodeURIComponent(code);
      $.ajax({
        type:'POST',
        url: 'ajax/ajax_client_search.php',
      //the script to call to get data
      data :"code=" + nom + "&nom=" + code + "&siret=" + siret + "&siren=" + siren,
      dataType: 'html',                  
      success: function(data)          
      {
        console.log(data);
        $('#ignorer').show(400);
        if (data == "OK"){
          $("#source1").prependTo("#destination1");
          $("#source2").prependTo("#destination2");
          $("#source3").hide();
          $( "#verif" ).hide();
          $('#msg1').show(400);
          $('#msg2').hide(400);
          $('#msg4').hide();
          $('#ignorer').hide(400);
          $('.reste-form').show(400);
          $('#sus_submit').attr('disabled', false);
        }else if(data == "SIRET"){
          $('#msg1').hide(400);
          $('#msg2').hide(400);
          $('#msg3').hide(400);
          $('#msg4').show(400);
          $('#ignorer').hide();
          $('#sus_submit').attr('disabled', true);

        }else{
          $('#msg1').hide(400);
          $('#msg2').show(400);
          $('#msg4').hide();
          $('#sus_submit').attr('disabled', false);
          

          $( "#ignorer" ).click(function() {
            $("#source1").prependTo("#destination1");
            $("#source2").prependTo("#destination2");
            $("#source3").hide();
            $( "#verif" ).hide();
            $('#msg1').hide(400);
            $('#msg2').hide(400);
            $('#msg3').hide(400);
            $(this).hide(400);
            $('.reste-form').show(400);
          });
        }
      },
      complete: function(){
        $('#loader').hide();
      }
    });


    });


    /* @custom validation method (smartCaptcha)
    ------------------------------------------------------------------ */

    $('#adr_cp1').mask('00 000');
    $('#adr_cp2').mask('00 000');
    $('#con_tel').mask('00 00 00 00 00');
    $('#con_fax').mask('00 00 00 00 00');
    $('#con_portable').mask('00 00 00 00 00');


    $('#cli_interco').mask('00000000000', {
      'translation': {
        0: {pattern: /[0-9]/}
      },

    });

    $('#cli_nom').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
      'translation': {
        A: {pattern: /[A-Z" "a-z0-9]/}
      },
      onKeyPress: function (value, event) {
        event.currentTarget.value = value.toUpperCase();
      }
    });
    $('#adr_ville1').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
      'translation': {
        A: {pattern: /[A-Z" "a-z0-9 -]/}
      },
      onKeyPress: function (value, event) {
        event.currentTarget.value = value.toUpperCase();
      }
    });
    $('#adr_ville2').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
      'translation': {
        A: {pattern: /[A-Z" "a-z0-9 -]/}
      },
      onKeyPress: function (value, event) {
        event.currentTarget.value = value.toUpperCase();
      }
    });
    $('.cli_code').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
      'translation': {
        A: {pattern: /[A-Za-z0-9]/}
      }
    });
    $('#cli_ville').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', {
      'translation': {
        A: {pattern: /[A-Z" "a-z-]/}
      },
      onKeyPress: function (value, event) {
        event.currentTarget.value = value.toUpperCase();
      }
    });

  });



</script>
</body>
</html>
