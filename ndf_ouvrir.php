<?php
////////////////// MENU ACTIF HEADER ///////////////////

////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
if (isset($_GET['menu_retour'])) {
    unset($_SESSION['retour']);
}

$_SESSION['retour'] = "ndf_ouvrir.php";
///////////////////// Contrôles des parametres ////////////////////
// fin récupérer toutes les ndf

// chercher les societes et agences
$req=$Conn->query("SELECT DISTINCT soc_id,soc_code,soc_nom FROM Societes LEFT JOIN Utilisateurs_Societes ON uso_societe = soc_id WHERE uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " AND soc_archive=0 ORDER BY soc_nom;");
$req->execute();
$societes=$req->fetchAll();

$req = $Conn->prepare("SELECT age_id, age_nom FROM agences LEFT JOIN Utilisateurs_Societes ON uso_agence = age_id WHERE uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " AND age_archive != 1 AND age_societe = " . $_SESSION['acces']['acc_societe']);
$req->execute();
$agences = $req->fetchAll();
// fin chercher les societes et agences


///////////////////////// FIN TRAITEMENTS SERVEUR /////////////////////////////
///////////////////// TRAITEMENTS PHP ///////////////////////


// récupérer tous les mois dont j'ai besoin
$mois = array(

    1 => "Janvier",
    2 => "Février",
    3 => "Mars",
    4 => "Avril",
    5 => "Mai",
    6 => "Juin",
    7 => "Juillet",
    8 => "Août",
    9 => "Septembre",
    10 => "Octobre",
    11 => "Novembre",
    12 => "Décembre"


);


$annees = array(
    1 => date('Y', strtotime(' -5 years')),
    2 => date('Y', strtotime(' -4 years')),
    3 => date('Y', strtotime(' -3 years')),
    4 => date('Y', strtotime(' -2 years')),
    5 => date('Y', strtotime(' -1 years')),
    6 => date('Y'),
    7 => date('Y', strtotime(' +1 year')),
);




////////////////////FIN TRAITEMENTS PHP//////////////////////
?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Notes de frais</title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

    <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
    <link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
    <!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<style type="text/css">
    .panel-tabs>li>a {
        color: #AAA;
        font-size: 14px;
        letter-spacing: 0.2px;
        line-height: 30px;
        /*padding: 9px 20px 11px;*/
        border-radius: 0;
        border-left: 1px solid transparent;
        border-right: 1px solid transparent;
    }

    .nav2>li>a {
        position: relative;
        display: block;
        /* padding: 10px 15px; */
    }
</style>

<body class="sb-top sb-top-sm">


    <form action="ndf_ouvrir_enr.php" method="POST">
        <!-- Start: Main -->
        <div id="main">

            <?php include "includes/header_def.inc.php"; ?>
            <!-- Start: Content-Wrapper -->
            <section id="content_wrapper">

                <!-- MESSAGES SUCCES -->

                <section id="content" class="animated fadeIn pr20 pl20">
                    <h1>Ouvrir une note</h1>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel mb10">
                                <div class="panel-body p10 admin-forms">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p class="text-center mb20" style="font-weight:bold;">Société :</p>
                                            <select id="societe" name="ndf_societe" class="select2 select2-societe">
                                                <option value="0">Sélectionner une société...</option>
                                                <?php foreach ($societes as $s) { ?>
                                                    <option value="<?= $s['soc_id'] ?>" <?php
                                                                                        if (!empty($_SESSION['ndf_tri'])) {
                                                                                            if ($_SESSION['ndf_tri']['ndf_societe'] == $s['soc_id']) {
                                                                                        ?> selected <?php
                                                                                                }
                                                                                            } ?>><?= $s['soc_nom'] ?></option>
                                                <?php } ?>
                                            </select>

                                        </div>
                                        <div class="col-md-4">
                                            <p class="text-center mb20" style="font-weight:bold;">Agence :</p>
                                            <select id="agence" name="ndf_agence" class="select2 select2-societe-agence">
                                                <option value="0">Sélectionner une agence...</option>
                                                <?php if (!empty($_SESSION['ndf_tri'])) {
                                                foreach ($agences as $a) { ?>
                                                    <option value="<?= $a['age_id'] ?>" <?php


                                                                                            if ($_SESSION['ndf_tri']['ndf_agence'] == $a['age_id']) {
                                                                                        ?> selected <?php
                                                                                                }
                                                                                            }  ?>><?= $a['age_nom'] ?></option>
                                                <?php } ?>
                                            </select>

                                        </div>
                                        <div class="col-md-4">
                                            <p class="text-center mb20" style="font-weight:bold;">Utilisateur :</p>
                                            <select id="utilisateur" name="ndf_utilisateur" class="select2 select2-utilisateur-societe-agence" required>

                                            </select>

                                        </div>
                                    </div>
                                    <div class="row quinzaine_radio mt20" style="display:none">
                                        <div class="col-md-6 text-center">
                                            <div class="mb5">
                                                <input id="ndf_quinzaine_1" class="option option-info" name="ndf_quinzaine" type="radio" value="1" checked>
                                                <label for="ndf_quinzaine_1">Première quinzaine</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 text-center">
                                            <div class="mb5">
                                                <input id="ndf_quinzaine_2" name="ndf_quinzaine" type="radio" value="2">
                                                <label for="ndf_quinzaine_2">Deuxième quinzaine</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="text-center mb20 mt20" style="font-weight:bold;">Mois :</p>
                                            <select id="statut" name="ndf_mois" class="select2" required>
                                                <option value="">Mois...</option>
                                                <?php foreach ($mois as $k => $m) { ?>
                                                    <option value="<?= $k  ?>" <?php
                                                                                if (!empty($_SESSION['ndf_tri'])) {
                                                                                    if ($_SESSION['ndf_tri']['ndf_mois'] == $k) { ?> selected <?php }
                                                                                                                                        } else {
                                                                                                                                            if ($k == date("n")) { ?> selected <?php
                                                                                                                                                                            }
                                                                                                                                                                        } ?>><?= $m ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <p class="text-center mb20 mt20" style="font-weight:bold;">Année :</p>
                                            <select id="statut" name="ndf_annee" class="select2" required>
                                                <option value="">Année...</option>
                                                <?php foreach ($annees as $a) { ?>
                                                    <option value="<?= $a ?>" <?php if (!empty($_SESSION['ndf_tri'])) {
                                                                                    if ($_SESSION['ndf_tri']['ndf_annee'] == $a) { ?> selected <?php
                                                                                                                                            }
                                                                                                                                        } else {
                                                                                                                                            if ($a == date("Y")) {
                                                                                                                                                ?> selected <?php }
                                                                                                                                                    } ?>><?= $a ?></option>
                                                <?php
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>

        </div>
        <!-- End: Main -->
        <footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
            <div class="row">
                <div class="col-xs-4 footer-left pt7">
                    <a href="ndf_liste.php" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a>
                </div>
                <div class="col-xs-4 footer-middle pt7"></div>
                <div class="col-xs-4 footer-right pt7">
                    <button href="ndf_liste.php" class="btn btn-success btn-sm"><i class="fa fa-floppy-o"></i> Enregistrer</button>
                </div>
            </div>
        </footer>



    </form>



    <?php
    include "includes/footer_script.inc.php"; ?>

    <script src="vendor/plugins/mask/jquery.mask.js"></script>
    <script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
    <!-- plugin pour les masques formulaires -->
    <script src="assets/js/custom.js"></script>

    <script src="vendor/plugins/select2/js/select2.min.js"></script>
    <script src="vendor/plugins/summernote/summernote.min.js"></script>
    <script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

    <script src="vendor/plugins/jquery.numberformat.js"></script>
    <!-- pour mettre des images de tests -->
    <!-- Theme Javascript -->
    <script>
        // DOCUMENT READY //
        jQuery(document).ready(function() {
            // initilisation plugin datatables
            // quand on veut imprimer
            quinzaine($("#utilisateur").val());
            ////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
            $("#utilisateur").change(function() {
                var uti = $(this).val();
                quinzaine(uti);
            });
        });

        ///////////// FONCTIONS //////////////
        function quinzaine(val) {
            if(val) {
                $.ajax({
                    type: 'POST',
                    url: 'ajax/ajax_ndf_uti_carte_affaire.php',
                    data: {
                        "uti": val,
                    },
                    dataType: 'html',
                    success: function(data) {
                        if (data == 1) {
                            $(".quinzaine_radio").hide()
                        } else {
                            $(".quinzaine_radio").show()
                        }
                    }
                });
            }

        }
        //////////// FIN FONCTIONS //////////
    </script>

</body>

</html>