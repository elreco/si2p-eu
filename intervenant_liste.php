<?php
$menu_actif = "2-1";
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");
include 'modeles/mod_get_intervenants.php';

	// LA PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}

	$acc_agence=0;
	if($_SESSION['acces']['acc_ref']==1){
		if(!empty($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];
		}
		
	}
	
	$onglet=1;
	
	$_SESSION["retour"]="intervenant_liste.php";
	$_SESSION["retourIntervListe"]="intervenant_liste.php";
	// TYPE D'Intervenants

	$int_type=array(
		0 => "",
		1 => "Salariés Si2P",
		2 => "Interco",
		3 => "Sous-traitant"
	);
	
	// SUR LES AGENCES

	$sql="SELECT age_id,age_nom FROM Agences WHERE age_societe=:societe;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":societe",$acc_societe);
	$req->execute();
	$d_result=$req->fetchAll();
	if(!empty($d_result)){
		
		$d_agences=array(
			"0" => "&nbsp;"
		);
		foreach($d_result as $r){
			$d_agences[$r["age_id"]]=$r["age_nom"];
		}
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<!-- Start: Main -->
		<div id="main">
			<?php
				include "includes/header_def.inc.php";
			?>
			<section id="content_wrapper">
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn">	
					
					<div class="tab-block">							
						<ul class="nav nav-tabs responsive">
					<?php	for($bcl=1;$bcl<4;$bcl++){ ?>
								<li <?php if($bcl==$onglet) echo("class='active'") ?> >
									<a href="#tab_<?=$bcl?>" data-toggle="tab" aria-expanded="false"><?=$int_type[$bcl]?></a>
								</li>
					<?php	} ?>
							<li <?php if($onglet==4) echo("class='active'") ?> >
								<a href="#tab_autre" data-toggle="tab" aria-expanded="false">Autres</a>
							</li>
							<li <?php if($onglet==5) echo("class='active'") ?> >
								<a href="#tab_archive" data-toggle="tab" aria-expanded="false">Archivés</a>
							</li>
						</ul>
						<div class="tab-content responsive p30" style="min-height:500px;">
					<?php	for($bcl=1;$bcl<4;$bcl++){ ?>
								<div id="tab_<?=$bcl?>" class="tab-pane <?php if($bcl==$onglet) echo("active"); ?>" >
					<?php			$intervenant=get_intervenants($acc_agence,0,$bcl,0);
									if(!empty($intervenant))
									{ ?>
										<table class="table table-striped table-hover" >
											<thead>
												<tr class="dark" >
													<th>&nbsp;</th>
									<?php			if(isset($d_agences)){ ?>
														<th>Agence</th>
									<?php			} ?>
													<th>Identité</th>
													<th>Rapports hebdos</th>								
												</tr>
											</thead>
											<tbody>
									<?php		foreach($intervenant as $int){
													$url="";
													if($bcl==1 && $_SESSION["acces"]['acc_droits'][22] == true){
														$url="utilisateur_voir.php?utilisateur=" . $int["int_ref_1"] . "&retour_interv";
													}elseif($bcl==3){
														$url="fournisseur_voir.php?fournisseur=" . $int["int_ref_1"];
														if(!empty($int["int_ref_2"])){
																$url.="&intervenant=" . $int["int_ref_2"] . "&tab=5";
														}													
													}
													?>
													<tr>
														<td>
													<?php	if(empty($acc_agence) OR $acc_agence==$int["int_agence"]){ ?>
																<a href="intervenant.php?intervenant=<?=$int["int_id"]?>" class="btn btn-sm btn-warning" >
																	<i class="fa fa-pencil" ></i>
																</a>
													<?php	} ?>
														</td>
												<?php	if(isset($d_agences)){ 
															echo("<td>");
																echo($d_agences[$int["int_agence"]]);
															echo("</td>");
														} ?>		
														<td>
													<?php	if(!empty($url)){ ?>
																<a href='<?=$url?>' >
																	<?=$int["int_label_1"] . " " . $int["int_label_2"]?>
																</a>
													<?php	}else{
																echo($int["int_label_1"] . " " . $int["int_label_2"]);
															} ?>
														</td>
														<td>
															<a href='rap_for_liste.php?utilisateur=<?= $int["int_ref_1"]  ?>' class="btn btn-sm btn-info">
																<i class="fa fa-eye"></i>
															</a>
													
														</td>																								
													</tr>
									<?php		} ?>
											</tbody>
										</table>
		<?php						} ?>
								</div>
					<?php	} ?>
							
							<!-- AUTRE -->
							<div id="tab_autre" class="tab-pane <?php if($onglet==4) echo("active"); ?>" >
				<?php			$intervenant=get_intervenants($acc_agence,0,0,0);
								if(!empty($intervenant)){ ?>
									<table class="table table-striped table-hover" >
										<thead>
											<tr class="dark" >
												<th>&nbsp;</th>
								<?php			if(isset($d_agences)){ ?>
													<th>Agence</th>
								<?php			} ?>
												<th>Identité</th>							
											</tr>
										</thead>
										<tbody>
								<?php		foreach($intervenant as $int){ 
												$class="";
												if (!empty($int["int_option"])) {
													$class="class='text-warning'";
												}?>

												<tr>
													<td>
												<?php	if(empty($acc_agence) OR $acc_agence==$int["int_agence"]){ ?>
															<a href="intervenant.php?intervenant=<?=$int["int_id"]?>" class="btn btn-sm btn-warning" >
																<i class="fa fa-pencil" ></i>
															</a>	
												<?php	} ?>			
													</td>
											<?php	if(isset($d_agences)){ 
														echo("<td " . $class .">");
															echo($d_agences[$int["int_agence"]]);
														echo("</td>");
													} ?>	
													<td <?=$class?> >
												<?php	echo($int["int_label_1"] . " " . $int["int_label_2"]); ?>
													</td>																								
												</tr>
								<?php		} ?>
										</tbody>
									</table>
	<?php						} ?>
							</div>
				
						
							<!-- ARCHIVE -->
							<div id="tab_archive" class="tab-pane <?php if($bcl==$onglet) echo("active"); ?>" >
					<?php		for($bcl=1;$bcl<4;$bcl++){
									$intervenant=get_intervenants($acc_agence,0,$bcl,1);
									if(!empty($intervenant)){ ?>									
										<div class="panel">
											<div class="panel-heading">
												<span class="panel-title"><?=$int_type[$bcl]?></span>
											</div>
											<div class="panel-body">
												<table class="table table-striped table-hover" >
													<thead>
														<tr class="dark" >
															<th>&nbsp;</th>
											<?php			if(isset($d_agences)){ ?>
																<th>Agence</th>
											<?php			} ?>
															<th>Identité</th>							
														</tr>
													</thead>
													<tbody>
											<?php		foreach($intervenant as $int){ 
															$url="";
															if($bcl==1){
																$url="utilisateur_voir.php?utilisateur=" . $int["int_ref_1"];
															}elseif($bcl==3){
																$url="fournisseur_voir.php?fournisseur=" . $int["int_ref_1"];
																if(!empty($int["int_ref_2"])){
																		$url.="&intervenant=" . $int["int_ref_2"] . "&tab=5";
																}													
															} ?>
															<tr>
																<td>
															<?php	if(empty($acc_agence) OR ( $int["int_agence"]=0 OR $acc_agence=$int["int_agence"]) ){ ?>																
																		<a href="intervenant.php?intervenant=<?=$int["int_id"]?>" class="btn btn-sm btn-warning" >
																			<i class="fa fa-pencil" ></i>
																		</a>
															<?php	} ?>				
																</td>
														<?php	if(isset($d_agences)){ 
																	echo("<td>");
																		echo($d_agences[$int["int_agence"]]);
																	echo("</td>");
																} ?>	
																<td>
															<?php	if(!empty($url)){ ?>
																		<a href='<?=$url?>' >
																			<?=$int["int_label_1"] . " " . $int["int_label_2"]?>
																		</a>
															<?php	}else{
																		echo($int["int_label_1"] . " " . $int["int_label_2"]);
																	} ?>
																</td>									
															</tr>
											<?php		} ?>
													</tbody>
												</table>
											</div>
										</div>										
		<?php						}
								} ?>
								<!-- AUTRE ARCHIVE -->
								
						<?php	$intervenant=get_intervenants($acc_agence,0,0,1);
								if(!empty($intervenant)){ ?>									
									<div class="panel">
										<div class="panel-heading">
											<span class="panel-title">Autres</span>
										</div>
										<div class="panel-body">
											<table class="table table-striped table-hover" >
												<thead>
													<tr class="dark" >
														<th>&nbsp;</th>
										<?php			if(isset($d_agences)){ ?>
															<th>Agence</th>
										<?php			} ?>
														<th>Identité</th>							
													</tr>
												</thead>
												<tbody>
										<?php		foreach($intervenant as $int){ ?>
														<tr>
															<td>
														<?php	if(empty($acc_agence) OR ( $int["int_agence"]=0 OR $acc_agence=$int["int_agence"]) ){ ?>
																	<a href="intervenant.php?intervenant=<?=$int["int_id"]?>" class="btn btn-sm btn-warning" >
																		<i class="fa fa-pencil" ></i>
																	</a>
														<?php	} ?>
															</td>
													<?php	if(isset($d_agences)){ 
																echo("<td>");
																	echo($d_agences[$int["int_agence"]]);
																echo("</td>");
															} ?>	
															<td>
														<?php	echo($int["int_label_1"] . " " . $int["int_label_2"]); ?>
															</td>									
														</tr>
										<?php		} ?>
												</tbody>
											</table>
										</div>
									</div>	
						<?php	} ?>									
							</div>							
						</div>
					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" ></div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="intervenant.php" class="btn btn-success btn-sm" >
						<span class="fa fa-plus"></span>
						Ajouter un intervenant
					</a>
				</div>
			</div>
		</footer>

		<?php
		include "includes/footer_script.inc.php"; ?>	
	</body>
</html>