<?php
//////////////////// MENU ACTIF HEADER ///////////////
$menu_actif = 5; 
//////////////////// INCLUDES ///////////////////////
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include('modeles/mod_parametre.php');
//////////////////// Requêtes à la base de données /////////////////////
// rechercher le fournisseur
$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id = :fou_id");
$req->bindParam(':fou_id', $_GET['fournisseur']);
$req->execute();
$fournisseur = $req->fetch();


// rechercher le document modèle
$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_id = :fdo_id");
$req->bindParam(':fdo_id', $_GET['id']);
$req->execute();
$doc = $req->fetch();


// rechercher le fichier du fournisseur
    if($doc['fdo_une_seule_fois'] == 1){
        // si le document est demandé une seule fois
        $req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_fichier = :ffi_fichier AND ffi_societe = 0");
        $req->bindParam(':ffi_fournisseur', $_GET['fournisseur']);
        $req->bindParam(':ffi_fichier', $_GET['id']);
        $req->execute();
        $fichier = $req->fetch();

    }else{
        // sinon aller chercher le document sur la société
        $req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_fichier = :ffi_fichier AND ffi_societe = :ffi_societe");
        $req->bindParam(':ffi_fournisseur', $_GET['fournisseur']);
        $req->bindParam(':ffi_fichier', $_GET['id']);
        $req->bindParam(':ffi_societe', $_SESSION['acces']['acc_societe']);
        $req->execute();
        $fichier = $req->fetch();
    }

    // mettre en forme le nom pour télécharger le fichier

    if(!empty($fichier)){
        $filepath = $fichier['ffi_fournisseur'] . "/" . $fichier['ffi_societe'] . "_" . $doc['fdo_id'] . "_" . clean($fichier['ffi_nom']) . "." . $fichier['ffi_ext'];
    }

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Ajouter un document fournisseur</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
<!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- Favicon -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
<form action="fournisseur_doc_enr.php" method="POST" id="admin-form" enctype="multipart/form-data">
    <!-- Start: Main -->
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="" style="margin-bottom:40px;">

            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-center">

                                        <div class="content-header">

                                            <h2>
                                            <?php if(!empty($fichier)){ ?>
                                                Editer le document <b class="text-primary"><?= $doc['fdo_libelle'] ?></b>
                                            <?php }else{ ?>
                                                Ajouter le document <b class="text-primary"><?= $doc['fdo_libelle'] ?></b>
                                            <?php }; ?>
                                            </h2>
                                            

                                        </div>

                                        <?php if(!empty($fichier)){ ?>
                                            <div class="col-md-4 col-md-offset-4">
                                                <div class="panel panel-tile text-center br-a br-grey">
                                                    <div class="panel-body">
                                                        <h1 class="fs30 mt5 mbn">Document actuel</h1>
                                                        <h6 class="text-system" style="font-size:50px;"><a href="documents/fournisseurs/<?= $filepath; ?>" download><i class="fa fa-download" aria-hidden="true"  data-toggle="tooltip" data-placement="bottom" title="Voir le document"></i></a></h6>
                                                    </div> 
                                                    <div class="panel-footer br-t p12">
                                                        <span class="fs11">
                                                            <?= $fichier['ffi_societe'] . "_" . $doc['fdo_id'] . "_" . clean($fichier['ffi_nom']) . "." . $fichier['ffi_ext'] ?>
                                                            </b>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <div class="col-md-10 col-md-offset-1">

                                                <input type="hidden" name="fournisseur" value="<?= $_GET['fournisseur'] ?>">
                                                <input type="hidden" name="document" value="<?= $doc['fdo_id'] ?>">
                                                <?php 
                                                    // si le fichier existe, alors c'est une modification
                                                    if(!empty($fichier)){
                                                ?>
                                                    <input type="hidden" name="remplacer" value="1">
                                                <?php } ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="section">

                                                            <label class="field prepend-icon file">

                                                            <span class="button btn-primary">Choisir</span>
                                                            <input id="fileupload" type="file" name="doc" class="gui-file">
                                                            <input type="text" class="gui-input" id="uploader2"
                                                            <?php if(!empty($fichier)){ ?>
                                                                placeholder="Remplacer le fichier actuel"
                                                            <?php }else{ ?> 
                                                                placeholder="Merci de sélectionner un fichier" required=""
                                                            <?php } ?>>
                                                            <label class="field-icon">
                                                                <i class="fa fa-upload"></i>
                                                            </label>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div id="status" class="text-primary"></div>

                                                    <div class="col-md-6 text-left">
                                                        <div class="section">
                                                            <label class="field"> Date du document :
                                                                <input type="text" name="doc_date" id="datepicker-from" class="gui-input date" required placeholder="Date du document"
                                                                <?php if(!empty($fichier)){ ?>
                                                                    value="<?= convert_date_txt($fichier['ffi_date']); ?>"
                                                                <?php }else{ ?>
                                                                    value="<?= date("d/m/Y") ?>"
                                                                <?php }?>
                                                                >  
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 text-left">
                                                        <div class="section pt10">
                                                            <h3 id="ffi_date_vers" style="display:none;">Fin de validité le : <span class="text-primary"></span></h3>
                                                        </div> 
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>


    <!-- End: Main -->
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left" >
                <a href="fournisseur_voir.php?fournisseur=<?= $_GET['fournisseur'] ?>&tab=4" class="btn btn-default btn-sm">
                    <i class="fa fa-long-arrow-left"></i>
                    Retour
                </a>
            </div>
            <div class="col-xs-6 footer-middle" >
                <div class="progress" style="margin-bottom:0; margin-top:7px;">

                    <div class="progress-bar progress-bar-success progress-bar-striped bar" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                        <span class="percent text-center"></span>
                    </div>
                </div>


            </div>
            <div class="col-xs-3 footer-right" >
                <button type="submit" name="submit" id="upload-btn" class="btn btn-success btn-sm">
                    <i class='fa fa-save'></i> Enregistrer
                </button>
                <button type="button" id="cancel-btn" class="btn btn-danger btn-sm" style="display:none;">
                    <i class='fa fa-times'></i> Annuler
                </button>
            </div>
        </div>
    </footer>
</form>
                            
<?php include "includes/footer_script.inc.php"; ?>	

<!-- validation inputs -->

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->
<script src="assets/js/custom.js"></script>
<script src="vendor/plugins/upload/jquery.form.js"></script>
<script type="text/javascript">



//////////////// FONCTIONS ///////////////////
// récupère automatiquement la date de validite d'un fournisseur
function get_fournisseur_date_validite(id_input,id_output){
    date = $("#" + id_input).val();
    $.ajax({
        type:'POST',
        url: 'ajax/ajax_fournisseur_date_validite.php',
        data : 'date=' + date + "&doc=<?= $doc['fdo_id']; ?>",
        dataType: 'text',                
        success: function(data)
        { 
           if(data == 0){
                $("#" + id_output).hide();
                
            }else{
                $("#" + id_output).show();
                $("#" + id_output + " > span").text(data);

            }
        }
    });

}

//////////////// EVENEMENTS UTILISATEURS////////////
$(document).ready(function() { 
    ///////////////////EVENEMENTS UTILISATEURS///////////////////
    // lorsque un fichier est chargé

    $("#fileupload").change(function() {
        $("#uploader2").val($(this).val());
    });
    // lorsque la date change actualiser la date validité
    ////////////// au chargement
        if($("#datepicker-from").val()){
            get_fournisseur_date_validite("datepicker-from","ffi_date_vers");
        }else{
            $("#ffi_date_vers").hide();
        }
    $("#datepicker-from").change(function() {

        if($(this).val()){
            get_fournisseur_date_validite("datepicker-from","ffi_date_vers");
        }else{
            $("#ffi_date_vers").hide();
        }
        
    });
    // fin lorsque la date change actualiser la date validité
    //////////////// FIN EVENEMENTS UTILISATEURS/////////////////

    ////////////////////// INITIALISATIONS///////////////////////
    // masque du champ date du document
    $('.date').mask('00/00/0000');
    // fin initialisation plugin datepicker
    $("#datepicker-from").datepicker({
        defaultDate: "+1w",
        dateFormat: "dd/mm/yy",
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sem.',
        numberOfMonths: 3,
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        beforeShow: function (input, inst) {
            var themeClass = $(this).parents('.admin-form').attr('class');
            var smartpikr = inst.dpDiv.parent();
            if (!smartpikr.hasClass(themeClass)) {
                inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
            }
        },
    });
    // upload du fichier
    $(function() {

        var bar = $('.bar');
        var percent = $('.percent');
        var status = $('#status');
        $(".progress").hide();


        $('form').ajaxForm({

            timeout: 10000000000000,

            beforeSend: function(xhr) {

                status.empty();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.text(percentVal);
                $( "#cancel-btn" ).show();
                $( "#upload-btn" ).hide();
                $( "#cancel-btn" ).click(function() {
                    xhr.abort();
                    $( "#cancel-btn" ).hide();
                    $( "#upload-btn" ).show();
                });

            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.text(percentVal);
                $(".progress").show();
                $( "#cancel-btn" ).show();
                $( "#upload-btn" ).hide();

            },
            complete: function(xhr) {
                $( "#cancel-btn" ).hide();
                $( "#upload-btn" ).show();

                status.html(xhr.responseText);
                console.log(status.text());
                if (status.text() == 0) {
                    $(".progress").show();
                    var delay = 1000; //Your delay in milliseconds

                setTimeout(function(){ 
               
                    window.location = "fournisseur_voir.php?fournisseur=<?= $_GET['fournisseur'] ?>&tab=4";
                }, delay);
                status.hide();

                }else{
                    $(".progress").hide();
                }
            }

        });


    }); 
});

</script>

</body>
</html>
