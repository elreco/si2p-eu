<?php 

// AFFICHE LA LISTE DES FACTURES

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');
include('modeles/mod_get_correspondances.php');
include('modeles/mod_orion_utilisateur.php');


// DONNEE UTILE AU PROGRAMME
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
$etape=0;
if(!empty($_SESSION['etape'])){
    $etape = $_SESSION['etape'];
}
$etape_chrono=0;
if(!empty($_SESSION['etape_chrono'])){
    $etape_chrono = $_SESSION['etape_chrono'];
}

if($etape > 0){
    $etape_ref = $etape;
}else{
    $etape_ref = $etape_chrono;
}

$client=0;
if(!empty($_GET['client'])){
	$client=intval($_GET['client']);
}else{
    echo "probleme";
    die();
}

// RETOUR
$origine="poste";
$origine_fac="actvoir";
$_SESSION['retourContact'] = "facture_relance_histo.php?client=" . $client;
$_SESSION['retourClient'] = "facture_relance_histo.php?client=" . $client;
$_SESSION['retourAdresse'] = "facture_relance_histo.php?client=" . $client;
$_SESSION['retourFacture'] = "facture_relance_histo.php?client=" . $client;
$_SESSION['retourliste'] = "facture_relance_histo.php?client=" . $client;

$sql="SELECT * FROM Clients WHERE cli_id = " . $client;
$req = $Conn->query($sql);
$d_client=$req->fetch();

/* if(!empty($d_clients[0]['cli_filiale_de'])){
    $sql="SELECT * FROM Clients WHERE cli_filiale_de = " . $d_clients[0]['cli_filiale_de'] . " OR cli_id = " . $d_clients[0]['cli_filiale_de'];
    $req = $Conn->query($sql);
    $d_clients=$req->fetchAll();
}else{
    $sql="SELECT * FROM Clients WHERE cli_id = " . $client;
    $req = $Conn->query($sql);
    $d_clients=$req->fetchAll();
} */


$req="SELECT * FROM societes WHERE soc_archive = 0";
$req=$Conn->query($req);	
$d_societes=$req->fetchAll();

$listeFac="";
$nbFac = 0;

$sql="SELECT * FROM Relances_Etats ORDER BY ret_j_deb";
$req = $Conn->query($sql);
$relances=$req->fetchAll();
$d_statut_libelle=array();

$nbEtat = 0;

$rel_titre="";
$rel_date_rappel="";
$ret_traitement=0;
$relance_nom[0] = array(
    "ret_id" => 0,
    "ret_libelle"=>"",
    "ret_code" => "",
    "ret_j_deb" => 0,
    "ret_j_fin" => 0,
    "ret_traitement" => 0
);
$d_statut_libelle=array();
foreach($relances as $re){
    $d_statut_libelle[$re['ret_id']]= $re['ret_libelle'];
    $nbEtat = $nbEtat +1;
    
    $relance_nom[$nbEtat]["ret_id"] = $re['ret_id'];
    $relance_nom[$nbEtat]["ret_code"] = $re['ret_code'];
    $relance_nom[$nbEtat]["ret_libelle"] = $re['ret_libelle'];
    $relance_nom[$nbEtat]["ret_j_deb"] = $re['ret_j_deb'];
    $relance_nom[$nbEtat]["ret_j_fin"] = $re['ret_j_fin'];
    $relance_nom[$nbEtat]["ret_traitement"] = $re['ret_traitement'];

    if($re['ret_id'] == $etape){
        $rel_titre=$re['ret_libelle'];
        $ret_traitement=$re['ret_traitement'];
        if($re['ret_traitement'] == 1){
            $demain = new DateTime();
            $demain->modify('+1 day');
            
			$rel_date_rappel = $demain->format('Y-m-d');
        }
    }  

}

?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
		<!-- CSS PLUGINS -->
        <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">	
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">
                        <h1>Historique des relances <?= $d_client['cli_nom'] ?></h1>
                        
                        <div class="row mt15">
                            <div class="col-md-12">
                                    <div class="table-responsive mt15">
                                   
                                        <?php 
                                            // HISTORIQUES
                                            $sql="SELECT DISTINCT rel_id,rel_comment,rel_date,rel_utilisateur,rel_con_nom,rel_con_prenom,rel_con_tel,rel_etat_relance,rel_date_rappel,rel_opca
                                            ,rel_envoie,rel_envoie_date,rel_envoie_uti,rel_courrier,rel_courrier_date,rel_courrier_uti 
                                            FROM Relances LEFT OUTER JOIN Relances_Factures ON (Relances.rel_id=Relances_Factures.rfa_relance)  
                                           

                                            WHERE  rel_client = " . $client;
                                            
                                            $req = $ConnSoc->query($sql);
                                            $historiques=$req->fetchAll();

                                            if(!empty($historiques)){
                                        ?>
                                        <table class="table table-striped table-hover" >
                                            <thead>
                                                <tr class="dark2" >
                                                    <th class="no-sort">&nbsp;</th>
                                                    <th>Utilisateur</th>
                                                    <th>Contact</th>
                                                    <th>Commentaire</th>
                                                    <th>Rappel</th>
                                                    <th>Factures</th>
                                                    <th colspan="3" >Mail</th>
                                                    <th colspan="2" >LR/AR</th>
                                                    										
                                                </tr>
                                            </thead>
                                            <tbody>
                                                   <?php foreach($historiques as $h){ 
                                                       $sql="SELECT uti_nom,uti_prenom FROM utilisateurs WHERE uti_id=" . $h['rel_utilisateur'];
                                                       $req=$Conn->query($sql);
                                                       $utilisateur=$req->fetch();
                                                       $sql="SELECT uti_nom,uti_prenom FROM utilisateurs WHERE uti_id=" . $h['rel_envoie_uti'];
                                                       $req=$Conn->query($sql);
                                                       $envoi_utilisateur=$req->fetch();
                                                       $sql="SELECT uti_nom,uti_prenom FROM utilisateurs WHERE uti_id=" . $h['rel_courrier_uti'];
                                                       $req=$Conn->query($sql);
                                                       $courrier_utilisateur=$req->fetch();
                                                       $sql="SELECT cli_nom FROM clients WHERE cli_id=" . $h['rel_opca'];
                                                       $req=$Conn->query($sql);
                                                       $cli_opca=$req->fetch();
                                                       $sql="SELECT rfa_facture,rfa_facture_num FROM Relances_Factures WHERE rfa_relance=" . $h['rel_id'] . "  ORDER BY rfa_facture_num;";
                                                       $req=$Conn->query($sql);
                                                       $rfa=$req->fetchAll();
                                                       ?>
                                                        <tr>
                                                            <td>
                                                                <?php if($h['rel_utilisateur'] == $acc_utilisateur){ ?>
                                                                    <a href="facture_relance_mod.php?relance=<?=$h['rel_id']?>" class="btn btn-sm btn-warning">
                                                                        <i class="fa fa-pencil"></i>
                                                                    </a>
                                                                <?php }?>
                                                            </td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <?= convert_date_txt($h['rel_date']) ?>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <?= $utilisateur['uti_prenom'] ?> <?= $utilisateur['uti_nom'] ?>
                                                                    </div>
                                                                </div>
                                                                   
                                                            </td>
                                                            <td>
                                                                <?= $h['rel_con_prenom'] ?> <?= $h['rel_con_nom'] ?><br>
                                                                Tel : <?= $h['rel_con_tel'] ?>
                                                            </td>
                                                            <td>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                <?= $cli_opca['cli_nom'] ?>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <?= $h['rel_comment'] ?>
                                                                </div>
                                                            </div>
                                                                
                                                            </td>
                                                            <td>
                                                                <?php if(!empty($h['rel_date_rappel'])){ 
                                                                    $date = new DateTime($h['rel_date_rappel']);
                                                                    echo $date->format('d/m/Y H:i');
                                                                }?>
                                                            </td>
                                                            <td>
                                                                <?php if(!empty($rfa)){ ?>
                                                                <table style="border:none;">
                                                                    <?php foreach($rfa as $r){ ?>

                                                                        <tr>
                                                                            <td style="border:none;">
                                                                                <?= $r['rfa_facture_num'] ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php }?>
                                                                </table>
                                                                <?php }?>
                                                            </td>
                                                            <td class="text-center">
                                                                <a href="mail_prep.php?facture_relance=<?=$h['rel_id']?>">
                                                                    Copie interne
                                                                </a>     
                                                            </td>

                                                            <?php if(empty($relance_nom[$h['rel_etat_relance']])){ 
                                                                if($h['rel_etat_relance'] == 1){
                                                                ?>
                                                                <td class="text-center">
                                                                    <a href="mail_prep.php?facture_relance=<?=$h['rel_id']?>&releve=1">
                                                                        Mail Client
                                                                    </a>
                                                                </td>
                                                                <?php }else{ ?>
                                                                <td class="text-center">
                                                                    <a href="mail_prep.php?facture_relance=<?=$h['rel_id']?>&mail=1">
                                                                        Mail Client
                                                                    </a>
                                                                </td>
                                                        <?php }
                                                        if($h['rel_envoie']){ ?>
                                                            <td class="success">
                                                                Mail envoyé le <?= convert_date_txt($h['rel_envoie_date']) ?><br>
                                                                par <?= $envoi_utilisateur['uti_prenom'] ?> <?= $envoi_utilisateur['uti_nom'] ?>
                                                            </td>
                                                        <?php }else{?>
                                                            <td class="danger">
                                                                Mail non envoyé
                                                            </td>
                                                        <?php }?>
                                                        </tr>
                                                   <?php }else{?>
                                                    <td colspan="2" >&nbsp;</td>
                                                   <?php }?>
                                                        <?php if($h['rel_etat_relance'] == 5){ ?>
                                                            <td class="text-center" >
                                                                <a href="facture_relance_lr_voir.php?relance=<?=$h['rel_id']?>" >
                                                                   LR/AR
                                                                    
                                                                </a>
                                                            </td>
                                                            <?php if($h['rel_courrier']){ ?>
                                                                <td class="messageCorrect" >
                                                                    Courrier imprimé le <?=convert_date_txt($h['rel_courrier_date'])?><br/>
                                                                    par <?= $courrier_utilisateur['uti_prenom'] ?> <?= $courrier_utilisateur['uti_nom'] ?>
                                                                </td>
                                                            <?php }else{ ?>
                                                                <td class="danger">
                                                                    Courrier non envoyé
                                                                </td>
                                                            <?php }?>
                                                        <?php }else{?>
                                                            <td colspan="2" >&nbsp;</td>
                                                        <?php }?>
                                                    
                                                   <?php }?>
                                            </tbody>
                                            
                                        </table>
                                <?php 			}else{ ?>
                                        <div class="col-md-12 text-center mt15" style="padding:0;" >
                                            <div class="alert alert-warning" style="border-radius:0px;">
                                                Aucun historique.
                                            </div>
                                        </div>
                    <?php			} ?>
                                    </div>
                                
                            </div>
                            
                        </div>
                      
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="<?= $_SESSION['retourFactureRelance'] ?>" class="btn btn-default btn-sm" role="button">
							<span class="fa fa-long-arrow-left"></span>
							<span class="hidden-xs">Retour</span>
						</a>
					</div>
					<!-- <div class="col-xs-6 footer-middle">&nbsp;</div> -->
					<div class="col-xs-9 footer-right">
                        

                    </div>
				</div>
			</footer>
        <!-- modal de confirmation -->	
			<div id="modal_confirmation_user" class="modal fade" role="dialog" >
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title">Confirmer ?</h4>
						</div>
						<div class="modal-body">
							<p>Message</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								Annuler
							</button>
							<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
								Confirmer
							</button>
						</div>
					</div>
				</div>
			</div>

			<!-- FIN MODAL -->
        <style>
            .note-editable{ 
                background-color: white !important;
            }
        </style>
<?php	include "includes/footer_script.inc.php"; ?>	
        <script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script> 
        <script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
        <script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>
        <script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="vendor/plugins/moment/moment.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/fr.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript">
            var client="<?=$d_client["cli_id"]?>";
            var tab_contact=new Array(3);
            tab_contact[0]=new Array(3);
			<?php	if(!empty($contacts)){
						foreach($contacts as $con){ ?>
							tab_contact[<?=$con["con_id"]?>]=new Array(3);
							tab_contact[<?=$con["con_id"]?>]["nom"]="<?=$con["con_nom"]?>";
							tab_contact[<?=$con["con_id"]?>]["prenom"]="<?=$con["con_prenom"]?>";
							tab_contact[<?=$con["con_id"]?>]["tel"]="<?=$con["con_tel"]?>";
							tab_contact[<?=$con["con_id"]?>]["portable"]="<?=$con["con_portable"]?>";					
			<?php		} 
					} ?>
            $(document).ready(function () {
                $('.dataTable').DataTable({
                    "language": {
                    "url": "vendor/plugins/DataTables/media/js/French.json"
                    },
                    "paging": false,
                    "searching": false,
                    "info": false,
                    "columnDefs": [ {
                        "targets": 'no-sort',
                        "orderable": false,
                    }]
                });
                // INTERACTION CONTACT 
				$(".datetimepicker").datetimepicker({
                    locale: 'fr'
                });
				$(".delcontact").click(function(){
					contact=$(this).data("contact");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer ce contact?",confirmer_contact_supp,contact);
                }); 
                $("#contact").change(function(){					
                    afficher_contact($(this).val());					
                });
            });
            function afficher_contact(contact){
                if(contact>0){
                    $("#con_nom").val(tab_contact[contact]["nom"]);
                    $("#con_prenom").val(tab_contact[contact]["prenom"]);
                    $("#con_tel").val(tab_contact[contact]["tel"]);
                    $("#con_portable").val(tab_contact[contact]["portable"]);						
                }else{
                    $(".champ-contact").val("");							
                }
            }

            function confirmer_contact_supp(contact){
				del_client_contact(contact,client,supprimer_contact_client,contact);
            }
            // modif de l'affichage suite à supp
			function supprimer_contact_client(json){
				if(json.contact){
					$("#contact_" + json.contact).remove();
				}
				if($(".contact").length==0){
					$("#tab_contact").remove();
					$("#no_contact").show();
				}
            }
            var nbFac=0;
			var tab_lien=new Array();
			var rel_etat_relance=0;
			var min_relance=0;
			var etat_relance=0;
			var num_relance=0;
			var valide_select=false;
			var fac_numero="";
			
			var tab_relance=new Array();
			
			var tab_relance_nom=new Array();
		<?php for ($i=0; $i < $nbEtat; $i++) { ?>
            
				tab_relance[<?=$relance_nom[$i]['ret_id']?>]=new Array(2);
				tab_relance[<?=$relance_nom[$i]['ret_id']?>][0]=parseInt("<?=$i?>");
				tab_relance[<?=$relance_nom[$i]['ret_id']?>][1]="<?=$relance_nom[$i]['ret_libelle']?>";
				tab_relance[<?=$relance_nom[$i]['ret_id']?>][2]=parseInt("<?=$relance_nom[$i]['ret_traitement']?>");
				
				tab_relance_nom[<?=$i?>]="<?=$relance_nom[$i]['ret_libelle']?>";
        <?php }?>
			
			
			function controleFac(num_fac){
				valide_select=true;
				rel_etat_relance=document.getElementById("rel_etat_relance").value;
				if(rel_etat_relance>0){
					num_relance=tab_relance[rel_etat_relance][0];
					min_relance=document.getElementById("min_relance_" + num_fac).value;
					etat_relance=document.getElementById("etat_relance_" + num_fac).value;
                    fac_numero=document.getElementById("fac_numero_" + num_fac).value;
                   
					if(num_relance<min_relance){
						alert("L'opération '" + tab_relance[rel_etat_relance][1] + "' n'est plus disponible pour la facture " + fac_numero); 	
						valide_select=false;
					}else if(num_relance>etat_relance){
						alert("La facture " + fac_numero + " n'en n'est pas encore au stade '" + tab_relance[rel_etat_relance][1] + "'"); 	
						valide_select=false;
					};
				}
				if(valide_select){
					selectFac(num_fac);	
				}else{
					deselectFac(num_fac);	
				};
		
			}

			function selectFac(num_fac){
				document.getElementById("ligne_fac_" + num_fac).checked=true;
				document.getElementById("tab_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				document.getElementById("numero_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				if(document.getElementById("date_ligne_" + num_fac)){
					document.getElementById("date_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
				if(document.getElementById("regle_ligne_" + num_fac)){
					document.getElementById("regle_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
				if(document.getElementById("du_ligne_" + num_fac)){
					document.getElementById("du_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
				if(document.getElementById("ech_ligne_" + num_fac)){
					document.getElementById("ech_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
				document.getElementById("etat_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				if(document.getElementById("opca_ligne_" + num_fac)){
					document.getElementById("opca_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};	
			}
			
			function deselectFac(num_fac){
				document.getElementById("ligne_fac_" + num_fac).checked=false;
				document.getElementById("tab_ligne_" + num_fac).removeAttribute("class");
				document.getElementById("numero_ligne_" + num_fac).setAttribute("class","ligne_fac");
				if(document.getElementById("date_ligne_" + num_fac)){
					document.getElementById("date_ligne_" + num_fac).setAttribute("class","ligne_fac");
				};
				if(document.getElementById("regle_ligne_" + num_fac)){
					document.getElementById("regle_ligne_" + num_fac).setAttribute("class","ligne_fac");
				};
				if(document.getElementById("du_ligne_" + num_fac)){
					document.getElementById("du_ligne_" + num_fac).setAttribute("class","ligne_fac");
				}
				if(document.getElementById("ech_ligne_" + num_fac)){
					document.getElementById("ech_ligne_" + num_fac).setAttribute("class","ligne_fac");
				};
				document.getElementById("etat_ligne_" + num_fac).setAttribute("class","ligne_fac");
				if(document.getElementById("opca_ligne_" + num_fac)){
					document.getElementById("opca_ligne_" + num_fac).setAttribute("class","ligne_fac");
				}
			}
			
			
			/* function selectMultiFac(elt,soc){
				nbFac=document.getElementById("nbFac").value;
				for(i=1;i<=nbFac;i++){
					if(document.getElementById("ligne_fac_" + i)){
						if(elt.checked){
							if(document.getElementById("fac_societe_" + i).value==soc){
								controleFac(i);
							};
						}else{
							if(document.getElementById("fac_societe_" + i).value==soc){
								deselectFac(i);
							}
						};	
					};
				};	
			} */
			function choixFac(num_fac){
				if(document.getElementById("ligne_fac_" + num_fac).checked){	
					controleFac(num_fac);
				}else{
					deselectFac(num_fac);	
				};
			}
			function actuFacture(){
				rel_etat_relance=document.getElementById("rel_etat_relance").value;
				num_relance=tab_relance[rel_etat_relance][0];
                nbFac=document.getElementById("nbFac").value;
				for(i=1;i<=nbFac;i++){
                    alert(document.getElementById("etat_relance_" + i).value);
					if((document.getElementById("etat_relance_" + i).value==num_relance)&&(num_relance>0)){
                        selectFac(i);
                        
					}else{
						deselectFac(i);
					};
				};		
			}
            var date_jour="<?=date("Y-m-d")?>"
            <?php
                $demain = new DateTime();
                $demain->modify('+1 day');
            ?>
			var date_demain="<?= $demain->format('Y-m-d') ?>"
			function choixEtape(){
				rel_etat_relance=document.getElementById("rel_etat_relance").value;
				if(tab_relance[rel_etat_relance][2]==1){
					document.getElementById("rel_date").value=date_jour;
					document.getElementById("rel_date_rappel").value=date_demain;
					document.getElementById("rel_date").setAttribute("readonly","readonly");
					document.getElementById("rel_date_rappel").setAttribute("readonly","readonly");
				}else{
					document.getElementById("rel_date_rappel").value="";
					document.getElementById("rel_date").removeAttribute("readonly");
					document.getElementById("rel_date_rappel").removeAttribute("readonly");
				};
				
				nbFac=document.getElementById("nbFac").value;
				if(rel_etat_relance>0){
					document.getElementById("rel_titre").value=tab_relance[rel_etat_relance][1];
					for(i=1;i<=nbFac;i++){
						if(document.getElementById("ligne_fac_" + i).checked){
							controleFac(i);
						};
					};			
				}else{
					num_relance=0;	
					for(i=1;i<=nbFac;i++){
						if((document.getElementById("num_relance_" + i).value<num_relance)||(num_relance==0)){
							num_relance=document.getElementById("num_relance_" + i).value;
						};
					};	
					document.getElementById("rel_titre").value=tab_relance_nom[num_relance];
				}
				
				
			}
		</script>
	</body>
</html>
