<?php

	include "includes/controle_acces.inc.php";
	include_once 'includes/connexion.php';
	include_once 'includes/connexion_soc.php';
	include_once 'includes/connexion_fct.php';


	// GENERATION D'UN POINT PNM pour integration SAGE

	// seul le service compta à accès a ce programme
	if($_SESSION['acces']['acc_service'][2]!=1){
		echo("Impossible d'afficher cette page!");
		die();
	}
	
	
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}

	

	// TRAITEMENT DU FORM

	if(isset($_POST['search'])){

		$agence=0;
		if(!empty($_POST['agence'])){
			$agence=intval($_POST['agence']);
		}
		if(!empty($acc_agence)){
			$agence=$acc_agence;
		}
		
		$mois=0;
		if(!empty($_POST['mois'])){
			$mois=intval($_POST['mois']);
		}

		$annee=0;
		if(!empty($_POST['annee'])){
			$annee=intval($_POST['annee']);
		}
		
		$num_piece=0;
		if(!empty($_POST['num_piece'])){
			$num_piece=$_POST['num_piece'];
		}
		
		$tri= array(
			"agence" => $agence,
			"mois" => $mois,
			"annee" => $annee,
			"num_piece" => $num_piece
		);

		$_SESSION['tri'] =$tri;

	}else{
		if(!empty($_SESSION['tri'])){
			$tri=$_SESSION['tri'];
		}
	}
	
	
	// MOYEN DE PAIEMENT
	
	$reglements_types=array();
	$req=$Conn->query("SELECT rty_id,rty_code FROM Reglements_Types;");
	$d_resultats=$req->fetchAll();
	foreach($d_resultats as $r){
		$reglements_types[$r["rty_id"]]=$r["rty_code"];
	}
	
	// BASE PRODUIT
	$d_produits=array();
	$req=$Conn->query("SELECT pro_id,pro_ven_cpt,pro_ven_cpt_si2p,pro_ven_cpt_exo,pro_ven_cpt_si2p_exo,pro_code_produit FROM Produits;");
	$d_resultats=$req->fetchAll();
	foreach($d_resultats as $r){
		$d_produits[$r["pro_id"]]=array(
			"pro_ven_cpt" => $r["pro_ven_cpt"],
			"pro_ven_cpt_si2p" => $r["pro_ven_cpt_si2p"],
			"pro_ven_cpt_exo" => $r["pro_ven_cpt_exo"],
			"pro_ven_cpt_si2p_exo" => $r["pro_ven_cpt_si2p_exo"],
			"pro_code_produit" => $r["pro_code_produit"]
		);
	}

	// SUR LES AGENCES

	$d_agences=array();
	$req=$Conn->query("SELECT age_id,age_nom,age_analytique_agence FROM Agences WHERE age_societe=" . $acc_societe . ";");
	$d_resultats=$req->fetchAll();
	if(!empty($d_resultats)){
		foreach($d_resultats as $r){
			$d_agences[$r["age_id"]]=array(
				"age_nom" => $r["age_nom"],
				"age_analytique_agence" => $r["age_analytique_agence"]
			);
		}
	}
	
	// LES SOCIETES
	
	$d_societes=array();
	$plan_analytique_interco=0;
	$raison_social="";
	
	$req=$Conn->query("SELECT soc_id,soc_analytique_interco,soc_analytique_interco_plan,soc_nom FROM Societes;");
	$d_resultats=$req->fetchAll();
	if(!empty($d_resultats)){
		foreach($d_resultats as $r){
			$d_societes[$r["soc_id"]]=array(
				"soc_analytique_interco" => $r["soc_analytique_interco"]
			);
			if($acc_societe==$r["soc_id"]){
				$plan_analytique_interco=$r["soc_analytique_interco_plan"];
			}
			if($r["soc_id"]==$acc_societe){
				$raison_social=" " . $r["soc_nom"];
			}
		}
	}
	
	// BASE VEHICULE
	$d_vehicules=array();
	$req=$Conn->query("SELECT veh_id,veh_analytique FROM Vehicules;");
	$d_resultats=$req->fetchAll();
	foreach($d_resultats as $r){
		$d_vehicules[$r["veh_id"]]=array(
			"veh_analytique" => $r["veh_analytique"]
		);
	}
	
	// LES PERIODE TVA ET COMPTE ASSOCIES
	
	$d_tva=array();
	$req=$Conn->query("SELECT tpe_id,tpe_cpt_ven,tpe_taux FROM tva_periodes;");
	$d_resultats=$req->fetchAll();
	foreach($d_resultats as $r){
		$d_tva[$r["tpe_id"]]=array(
			"tpe_cpt_ven" => $r["tpe_cpt_ven"],
			"tpe_taux" => $r["tpe_taux"],
			"montant" => 0,
			"fac" => false
		);
	}
	
	// LES MOIS 
	
	$nom_mois=array(
		"1" => "Janvier",
		"2" => "Février",
		"3" => "Mars",
		"4" => "Avril",
		"5" => "Mai",
		"6" => "Juin",
		"7" => "Juillet",
		"8" => "Aout",
		"9" => "Septembre",
		"10" => "Octobre",
		"11" => "Novombre",
		"12" => "Décembre"
	);
	
	// COMPTES
	$d_comptes=array();
	$req=$Conn->query("SELECT cpt_id,cpt_numero,cpt_libelle FROM Comptes;");
	$d_resultats=$req->fetchAll();
	foreach($d_resultats as $r){
		$d_comptes[$r["cpt_id"]]=array(
			"cpt_numero" => $r["cpt_numero"],
			"cpt_libelle" => $r["cpt_libelle"]
		);
	}
	
	function myutf8_encode($str){
		$cp1252_map = array(
		"\xc2\x80" => "\xe2\x82\xac", /* EURO SIGN */
		"\xc2\x82" => "\xe2\x80\x9a", /* SINGLE LOW-9 QUOTATION MARK */
		"\xc2\x83" => "\xc6\x92", /* LATIN SMALL LETTER F WITH HOOK */
		"\xc2\x84" => "\xe2\x80\x9e", /* DOUBLE LOW-9 QUOTATION MARK */
		"\xc2\x85" => "\xe2\x80\xa6", /* HORIZONTAL ELLIPSIS */
		"\xc2\x86" => "\xe2\x80\xa0", /* DAGGER */
		"\xc2\x87" => "\xe2\x80\xa1", /* DOUBLE DAGGER */
		"\xc2\x88" => "\xcb\x86", /* MODIFIER LETTER CIRCUMFLEX ACCENT */
		"\xc2\x89" => "\xe2\x80\xb0", /* PER MILLE SIGN */
		"\xc2\x8a" => "\xc5\xa0", /* LATIN CAPITAL LETTER S WITH CARON */
		"\xc2\x8b" => "\xe2\x80\xb9", /* SINGLE LEFT-POINTING ANGLE QUOTATION */
		"\xc2\x8c" => "\xc5\x92", /* LATIN CAPITAL LIGATURE OE */
		"\xc2\x8e" => "\xc5\xbd", /* LATIN CAPITAL LETTER Z WITH CARON */
		"\xc2\x91" => "\xe2\x80\x98", /* LEFT SINGLE QUOTATION MARK */
		"\xc2\x92" => "\xe2\x80\x99", /* RIGHT SINGLE QUOTATION MARK */
		"\xc2\x93" => "\xe2\x80\x9c", /* LEFT DOUBLE QUOTATION MARK */
		"\xc2\x94" => "\xe2\x80\x9d", /* RIGHT DOUBLE QUOTATION MARK */
		"\xc2\x95" => "\xe2\x80\xa2", /* BULLET */
		"\xc2\x96" => "\xe2\x80\x93", /* EN DASH */
		"\xc2\x97" => "\xe2\x80\x94", /* EM DASH */
		"\xc2\x98" => "\xcb\x9c", /* SMALL TILDE */
		"\xc2\x99" => "\xe2\x84\xa2", /* TRADE MARK SIGN */
		"\xc2\x9a" => "\xc5\xa1", /* LATIN SMALL LETTER S WITH CARON */
		"\xc2\x9b" => "\xe2\x80\xba", /* SINGLE RIGHT-POINTING ANGLE QUOTATION*/
		"\xc2\x9c" => "\xc5\x93", /* LATIN SMALL LIGATURE OE */
		"\xc2\x9e" => "\xc5\xbe", /* LATIN SMALL LETTER Z WITH CARON */
		"\xc2\x9f" => "\xc5\xb8" /* LATIN CAPITAL LETTER Y WITH DIAERESIS*/
		);
		return strtr(utf8_encode($str), $cp1252_map);

	}
	
	$erreur=array();
	
	if(isset($tri)){
		
		if(!empty($tri["annee"]) AND !empty($tri["mois"])){
			
			
			// PREP REQUETE CLI ORION_0
			
			$sql_cli_get="SELECT cli_code,cli_filiale_de,cli_interco_soc,cli_interco_age,cli_nom FROM Clients WHERE cli_id=:client;";
			$req_cli_get=$Conn->prepare($sql_cli_get);
			
			$sql_act_date_get="SELECT MIN(pda_date) as date_deb FROM Plannings_Dates,Actions_Clients_Dates WHERE acd_date=pda_id AND acd_action_client=:action_client;";
			$req_act_date_get=$ConnSoc->prepare($sql_act_date_get);
			
			
			$facture_id=null;
			$erreur_txt="";
			
			$nb_fac=null;
			
			$norme_compte=12;
			if($acc_societe==13){
				$norme_compte=8;
			}
			
			$sql="SELECT fac_id,DATE_FORMAT(fac_date,'%d%m%y') as fac_date,DATE_FORMAT(fac_date_reg_prev,'%d%m%y') as fac_date_reg_prev,fac_numero,fac_nature,fac_total_ttc,fac_blocage
			,fac_reg_type,fac_commercial,fac_cli_nom,fac_client,fac_agence
			,fli_id,fli_action_client,fli_action_cli_soc,fli_vehicule,fli_produit,fli_montant_ht,fli_tva_per,fli_tva_taux
			FROM Factures LEFT OUTER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture) 
			WHERE year(fac_date)=" . $annee . " AND month(fac_date)=" . $mois;
			if(!empty($agence)){
				$sql.=" AND fac_agence=" . $agence;
			}
			$sql.=" ORDER BY fac_date,fac_chrono,fac_id,fli_id;";
			$req=$ConnSoc->query($sql);
			$d_factures=$req->fetchAll();
			if(!empty($d_factures)){
				
				$ligne[]=array(
					0 => $raison_social
				);
				
				foreach($d_factures as $fac){

					if($fac["fac_id"]!=$facture_id){
						
						if($facture_id>0){
							
							//echo("NOUVELLE FACTURE<br/>");
							
							// TRAITEMENT SUR LA FACTURE PRECEDENTE
							if(!empty($erreur_txt)){
								$erreur[]=$erreur_txt;
							}else{
								
								//echo("TVA FAC PREC<br/>");
								// ON ECRITE LA LIGNE DE TVA DE L'ANCIENNE FACTURE
								
								
								if($fac_nature==1){
									$debit_credit = "C";
								}else{
									$debit_credit = "D";
								}
								
							
								/*echo("<pre>");
									print_r($d_tva);
								echo("</pre>");*/

							
								foreach($d_tva as $t){
							
									
									if($t["fac"]==1 AND !empty($t["montant"])){
										
										$montant_tva=round($t["montant"],2);
									
										$num_compte_general=str_pad($d_comptes[$t["tpe_cpt_ven"]]["cpt_numero"],$norme_compte,"0");
					
										$ligne[]=array(
											0 => "VEN",
											1 => $fac_date,
											2 => $num_compte_general,
											3 => "",
											4 => $fac_numero,
											5 => $client_nom,
											6 => $type_paiement,
											7 => $fac_date_reg_prev,
											8 => $debit_credit,
											9 => str_replace(".",",",$montant_tva),
											10 => "G",
											11 => $numeroPiece,
											12 => "",
											13 => $t["tpe_taux"],
											14 => "",
											15 => ""
										);	
									}
								}
								
								// on actualise la ligne facture
								$ligne[$l_fac][13]=$taux_ref;
								
								/*echo("<pre>");
									print_r($ligne);
								echo("</pre>");
								die();*/
								
							}
							
						}else{
							//echo("PREMIERE FAC<br/>");
							
						}
						
						// NOUVELLE FACTURE
						
						// LIGNE D'INTEGRATION CLIENT / FACTURE
					
						$erreur_txt="";
						
						$facture_id=$fac["fac_id"];
						$fac_date=$fac["fac_date"];
						$fac_numero=$fac["fac_numero"];
						$fac_date_reg_prev=$fac["fac_date_reg_prev"];						
						$fac_nature=$fac["fac_nature"];
							
						$taux_ref=0;
						foreach($d_tva as $kt => $t){
							$d_tva[$kt]["fac"]=false;	
							$d_tva[$kt]["montant"]=0;
						}

						$interco=false;
					
						if(empty($fac["fac_blocage"])){
							$erreur_txt="La facture " . $fac_numero . " n'est pas bloquée.";
						}else{
							
							$num_compte_general= str_pad ("411",$norme_compte,"0");
							
							$req_cli_get->bindParam(":client",$fac["fac_client"]);
							$req_cli_get->execute();
							$d_client=$req_cli_get->fetch();					
							if(!empty($d_client)){
								
								if(!empty($d_client["cli_interco_soc"])){
									// on ne peut pas exclure tous les 999 car l'affectation n'a pas été faite pour l'ensemble des fiche
									if($fac["fac_client"]!=6699 AND $fac["fac_client"]!=30357 AND $fac["fac_client"]!=34486 AND $fac["fac_client"]!=34487 AND $fac["fac_client"]!=34688 AND $fac["fac_client"]!=44477 AND $fac["fac_client"]!=17979 AND $fac["fac_client"]!=44435 AND $fac["fac_client"]!=6836){
										$interco=true;
									}
								}
								$client_nom=utf8_decode($d_client["cli_nom"]);
								$num_compte="411" . $d_client["cli_code"];						
							}else{
								$erreur_txt=$fac["fac_numero"] . " : les données du client " . $fac["fac_cli_nom"] . " (ID : " . $fac["fac_client"] . ") n'ont pas pu être chargées!";
							}
						}
						if(empty($erreur_txt)){
							
							// Maison mère						
							if(!empty($d_client["cli_filiale_de"])){							
								$req_cli_get->bindParam(":client",$d_client["cli_filiale_de"]);
								$req_cli_get->execute();
								$d_cli_holding=$req_cli_get->fetch();					
								if(!empty($d_cli_holding)){														
									$num_compte="411" . $d_cli_holding["cli_code"];						
								}
							}		
					
							
							$type_paiement=$reglements_types[$fac["fac_reg_type"]];

							if($fac_nature==1){
								$debit_credit = "D";
								$montant_ttc = $fac["fac_total_ttc"];		
							}else{
								$debit_credit = "C";
								$montant_ttc =-$fac["fac_total_ttc"];
							}

							$num_piece++;						
							$numeroPiece=$num_piece;
							$numeroPiece=str_pad ($num_piece,8,"0",STR_PAD_LEFT);
							$numeroPiece="V" . $numeroPiece;
							
							
							

							// AJOUT DE LA LIGNE FACTURE
							$ligne[]=array(
								0 => "VEN",
								1 => $fac["fac_date"],
								2 => $num_compte_general,
								3 => $num_compte,
								4 => $fac["fac_numero"],
								5 => $client_nom,
								6 => $type_paiement,
								7 => $fac["fac_date_reg_prev"],
								8 => $debit_credit,
								9 => str_replace(".",",",$montant_ttc),
								10 => "G",
								11 => $numeroPiece,
								12 => "",
								13 => "",
								14 => "",
								15 => ""
							);
							
							/*if(count($ligne)==39){
								echo("<pre>");
									print_r($fac["fac_total_ttc"]);
								echo("</pre>");
								die();
							}*/
							
							// on memorise la ligne fac pour pouvoir mettre a jour en fonction des lignes porduits
							$l_fac=count($ligne)-1;
						}
					}
					
					// NOUVELLE LIGNE DE FACTURE
					if(!empty($fac["fli_id"])){
						
					
						if(empty($erreur_txt)){
							
							if(empty($fac["fli_produit"])){
								$erreur_txt="La facture " . $fac["fac_numero"] . " contient une ligne sans code produit";
							}
						}
						if(empty($erreur_txt)){

							$action_client=0;
							if(!empty($fac["fli_action_client"])){
								$action_client=$fac["fli_action_client"];
							}
							$action_client_soc=0;
							if(!empty($fac["fli_action_cli_soc"])){
								$action_client_soc=$fac["fli_action_cli_soc"];
							}
							
							$date_deb=null;
							
							if($action_client>0){
								
								if($action_client_soc==$acc_societe){									
									$req_act_date_get->bindParam(":action_client",$action_client);
									$req_act_date_get->execute();
									$d_action_date=$req_act_date_get->fetch();									
								}else{
									
									$ConnFct=connexion_fct($action_client_soc);
									$req=$ConnFct->prepare($sql_act_date_get);
									$req->bindParam(":action_client",$action_client);
									$req->execute();								
									$d_action_date=$req->fetch();								
								}
								
								if(!empty($d_action_date)){
									if(!empty($d_action_date["date_deb"])){								
										$DT_periode=date_create_from_format('Y-m-d',$d_action_date["date_deb"]);
										if(!is_bool($DT_periode)){
											$date_deb=$DT_periode->format("dmy");
										}
									}
								}							
							}

							
							if(empty($fac["fli_exo"])){
								
								// CLASSIQUE
								
								$compte_id=$d_produits[$fac["fli_produit"]]["pro_ven_cpt"];	

								// CLASSIQUE INTERCO
								if($interco){
									if(!empty($d_produits[$fac["fli_produit"]]["pro_ven_cpt_si2p"])){
										$compte_id=$d_produits[$fac["fli_produit"]]["pro_ven_cpt_si2p"];										
									}									
								}
								/*if(empty($compte_id)){
									$compte_id=116;
								}
								*/
							}else{
								
								// EXO
								$compte_id=$d_produits[$fac["fli_produit"]]["pro_ven_cpt_exo"];	
								// EXO INTERCO
								if($interco){
									if(!empty($d_produits[$fac["fli_produit"]]["pro_ven_cpt_si2p_exo"])){
										$compte_id=$d_produits[$fac["fli_produit"]]["pro_ven_cpt_si2p_exo"];										
									}									
								}
							}
							if(empty($compte_id)){
								$erreur_txt="Le produit " . $d_produits[$fac["fli_produit"]]["pro_code_produit"] . " ne dispose pas de compte.";
							}
						}
						
						
						if(empty($erreur_txt)){
						
							$num_compte_general= str_pad ($d_comptes[$compte_id]["cpt_numero"],$norme_compte,"0");
							
							$montant_ht=0;
							if($fac_nature==1){
								$debit_credit = "C";
								$montant_ht = $fac["fli_montant_ht"];		
							}else{
								$debit_credit = "D";
								$montant_ht =-$fac["fli_montant_ht"];
							}
							
							
								
							
							if(isset($d_tva[$fac["fli_tva_per"]])){

								if(!$d_tva[$fac["fli_tva_per"]]["fac"]){							
									$d_tva[$fac["fli_tva_per"]]["fac"]=true;
								}
								$d_tva[$fac["fli_tva_per"]]["montant"]=$d_tva[$fac["fli_tva_per"]]["montant"] + ($montant_ht*($d_tva[$fac["fli_tva_per"]]["tpe_taux"]/100));

								if($fac["fli_tva_taux"]>$taux_ref){
									$taux_ref=$fac["fli_tva_taux"];
								}
								
								// AJOUT DE LA LIGNE PRODUIT
								$ligne[]=array(
									0 => "VEN",
									1 => $fac["fac_date"],
									2 => $num_compte_general,
									3 => "",
									4 => $fac["fac_numero"],
									5 => $client_nom,
									6 => $type_paiement,
									7 => $fac["fac_date_reg_prev"],
									8 => $debit_credit,
									9 => str_replace(".",",",$montant_ht),
									10 => "G",
									11 => $numeroPiece,
									12 => $date_deb,
									13 => $fac["fli_tva_taux"],
									14 => "",
									15 => ""
								);
							}else{
								$erreur_txt=$fac["fac_numero"] . " : impossible de calculer la TVA";
							}
							
						}
						
					}
					// ANALYTIQUE
					
					if(empty($erreur_txt)){
					
						// ANALYTIQUE AGENCE
						
						if(!empty($fac["fac_agence"]) AND !empty($d_agences[$fac["fac_agence"]]["age_analytique_agence"])){
						
							$ligne[]=array(
								0 => "VEN",
								1 => $fac["fac_date"],
								2 => $num_compte_general,
								3 => "",
								4 => $fac["fac_numero"],
								5 => $client_nom,
								6 => $type_paiement,
								7 => $fac["fac_date_reg_prev"],
								8 => $debit_credit,
								9 => str_replace(".",",",$montant_ht),
								10 => "A",
								11 => $numeroPiece,
								12 => "",
								13 => "",
								14 => 1,
								15 => $d_agences[$fac["fac_agence"]]["age_analytique_agence"]
							);
						}
						
						if(!empty($fac["fli_vehicule"]) AND !empty($d_vehicules[$fac["fli_vehicule"]]["veh_analytique"])){
						
							$ligne[]=array(
								0 => "VEN",
								1 => $fac["fac_date"],
								2 => $num_compte_general,
								3 => "",
								4 => $fac["fac_numero"],
								5 => $client_nom,
								6 => $type_paiement,
								7 => $fac["fac_date_reg_prev"],
								8 => $debit_credit,
								9 => str_replace(".",",",$montant_ht),
								10 => "A",
								11 => $numeroPiece,
								12 => "",
								13 => "",
								14 => 2,
								15 => $d_vehicules[$fac["fli_vehicule"]]["veh_analytique"]
							);
						}
						
						// ANALYTIQUE INTERCO
						
						if($interco AND !empty($plan_analytique_interco)){

							$sectionAnalytique="";
							
							if(!empty($d_societes[$d_client["cli_interco_soc"]]["soc_analytique_interco"])){
								$sectionAnalytique=$d_societes[$d_client["cli_interco_soc"]]["soc_analytique_interco"];
							}else{
								$erreur_txt="La facture " . $fac["fac_numero"] . " est liée à une interco sans analytique!";
							}
							if(!empty($sectionAnalytique)){
								$ligne[]=array(
									0 => "VEN",
									1 => $fac["fac_date"],
									2 => $num_compte_general,
									3 => "",
									4 => $fac["fac_numero"],
									5 => $client_nom,
									6 => $type_paiement,
									7 => $fac["fac_date_reg_prev"],
									8 => $debit_credit,
									9 => str_replace(".",",",$montant_ht),
									10 => "A",
									11 => $numeroPiece,
									12 => "",
									13 => "",
									14 => $plan_analytique_interco,
									15 => $sectionAnalytique
								);
							}
						}
					}
				}
				// FIN DE BOUCLE SUR FACTURE
				
				if(!empty($erreur_txt)){
					$erreur[]=$erreur_txt;
				}else{
				
					// ON ECRIT LES LIGNES TVA DE LA DERNIERE FACTURE
					if(empty($erreur_txt)){
						if($fac_nature==1){
							$debit_credit = "C";
						}else{
							$debit_credit = "D";
						}
						
						foreach($d_tva as $t){
							
							if($t["fac"]==1 AND !empty($t["montant"])){					
								$montant_tva=round($t["montant"],2);
								
								$num_compte_general=str_pad($d_comptes[$t["tpe_cpt_ven"]]["cpt_numero"],$norme_compte,"0");
			
								$ligne[]=array(
									0 => "VEN",
									1 => $fac_date,
									2 => $num_compte_general,
									3 => "",
									4 => $fac_numero,
									5 => $client_nom,
									6 => $type_paiement,
									7 => $fac_date_reg_prev,
									8 => $debit_credit,
									9 => str_replace(".",",",$montant_tva),
									10 => "G",
									11 => $numeroPiece,
									12 => "",
									13 => $t["tpe_taux"],
									14 => "",
									15 => ""
								);	
							}
						}
						$ligne[$l_fac][13]=$taux_ref;
					}
				}
			}
	
		}else{
			$factures=array();
		}
	}else{
		
		$tri= array(
			"agence" => $acc_agence,
			"mois" => 0,
			"annee" => 0,
			"num_piece" => ""
		);
	}
	//die();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="iso-8859-1">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />


		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php
			include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<section id="content" class="animated fadeIn">
				
					<div class="admin-form theme-primary ">
						<div class="panel heading-border panel-primary">
							<div class="panel-body bg-light">
							
								<div class="content-header">
									<h2>Facturation : Intégration <b class="text-primary">SAGE</b></h2>
								</div>
							
								<form method="post" action="#" id="form" >
								
									<div class="row">
								<?php	if(empty($acc_agence) AND !empty($d_agences)){ ?>
							
											<div class="col-md-3">
												<label for="agence" >Agence :</label>
												<div class="field select">											
													<select name="agence" id="agence" >
														<option value="">Agence ...</option>
												<?php 	foreach($d_agences as $age_id => $age){
															if($age_id==$tri["agence"]){
																echo("<option value='" . $age_id . "' selected >" . $age["age_nom"] . "</option>");
															}else{
																echo("<option value='" . $age_id . "' >" . $age["age_nom"] . "</option>");
															} 
														} ?>
													</select>
													<i class="arrow simple"></i>
												</div>
											</div>
								<?php	}	?>	
										<div class="col-md-3">
											<label for="annee" >Année :</label>
											<div class="field select">											
												<select name="annee" id="annee" >
													<option value="">Année ...</option>
											<?php 	for($bcl_a=2018;$bcl_a<=date("Y");$bcl_a++){													
														if($bcl_a==$tri["annee"]){
															echo("<option value='" . $bcl_a . "' selected >" . $bcl_a . "</option>");
														}else{
															echo("<option value='" . $bcl_a . "' >" . $bcl_a . "</option>");
														} 
													} ?>
												</select>
												<i class="arrow simple"></i>
											</div>
										</div>
										<div class="col-md-3">
											<label for="mois" >Mois :</label>
											<div class="field select">											
												<select name="mois" id="mois" >
													<option value="">Mois ...</option>
											<?php 	for($bcl_m=1;$bcl_m<=12;$bcl_m++){													
														if($bcl_m==$tri["mois"]){
															echo("<option value='" . $bcl_m . "' selected >" . $nom_mois[$bcl_m] . "</option>");
														}else{
															echo("<option value='" . $bcl_m . "' >" . $nom_mois[$bcl_m] . "</option>");
														} 
													} ?>
												</select>
												<i class="arrow simple"></i>
											</div>
										</div>
										
										<div class="col-md-2">
											<label for="num_piece" >Numéro de pièce :</label>
											<input type="text" name="num_piece" class="gui-input" id="num_piece" placeholder="Numéro de pièce" value="<?=$tri["num_piece"]?>" />
										</div>
										<div class="col-md-1 text-center pt20">
											<button type="submit" class="btn btn-sm btn-info" name="search" >
												<i class="fa fa-refresh" ></i> Genérer fichier
											</button>
										</div>
									</div>
									
							<?php	if(!empty($d_factures)){ 
										
										if(!empty($erreur)){ ?>
										
											<p class="alert alert-danger mt25" >Les factures demandées ne peuvent pas être intégrées pour les raisons suivantes :</p>
											
											<ul>
									<?php		foreach($erreur as $err){
													echo("<li>" . $err . "</li>");
												} ?>
											</ul>
								<?php	}else{
									
											$fichier="documents/temp/sage_" . $acc_societe . "_" . $agence . "_" . $annee . "_" . $mois . "_" . date('Ymdhis') . ".txt";
											$handle = fopen($fichier, "x+");
											foreach($ligne as $l){
												$ligne_txt=implode(";",$l);
												//echo($ligne_txt . "<br/>");
												fwrite($handle,$ligne_txt . chr(10));											
											}								
											fclose($handle); ?>
											
											<p class="alert alert-success mt25" >
												<strong>Le fichier d'intégration a bien été généré.</strong><br/>											
											</p>
											<div class="row" >
												<div class="col-md-6 text-right mt15" ><b>Télécharger le fichier :</b></div>
												<div class="col-md-6" >
													<a href="<?=$fichier?>" class="btn btn-xl btn-success" >
														<i class="fa fa-download" ></i>
													</a>
												</div>
											</div>
											
								<?php	}
									}else{ ?>
										<p class="alert alert-warning mt25" >
											Aucune facture ne correspond à votre recherche.
										</p>
								
							<?php	} ?>
								</form>
							</div>
						</div>
					</div>					
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
			<?php	if(!empty($_SESSION["retour"])){ ?>
						<a href="<?=$_SESSION["retour"]?>" class="btn btn-default btn-sm" >
							<span class="fa fa-long-arrow-left"></span>
							Retour
						</a>
			<?php	} ?>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >&nbsp;</div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>


		<script type="text/javascript" >
		
			jQuery(document).ready(function (){
				
				$("#pro_categorie").change(function(){
					if($(this).val()==1){
						$("#bloc_famille").show();
					}else{
						$("#pro_famille").val(0);
						$("#pro_sous_famille").val("");
						$("#pro_sous_sous_famille").val("");
						$("#bloc_famille").hide();
					}
				});
				
				$("#sub_produit").click(function(){
					$("#form").prop("action","param_produit_enr.php");
					$("#form").submit();
				});
				
				
			});
		</script>
		
	</body>
</html>
