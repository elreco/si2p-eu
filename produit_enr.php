<?php
include "includes/controle_acces.inc.php";

	include_once 'includes/connexion.php';
	include_once 'includes/connexion_soc.php';
	include_once 'includes/connexion_fct.php';

	if($_SESSION['acces']['acc_profil'] == 2){
		Header('Location: index.php');
	}


	if(session_id() == '' || !isset($_SESSION)) {
		session_start();
	}

	include_once 'modeles/mod_produit.php';

	include_once "modeles/mod_add_client_produit.php";
	include_once "modeles/mod_set_client_produit.php";

	$succes_txt="";
	$danger_txt="";
	$warning_txt="";

	if(!empty($_POST) && !isset($_POST['search'])){

		$pro_id=0;
		if(isset($_POST['pro_id'])){
			$pro_id=intval($_POST['pro_id']);
		}
		// TRAITEMENT DU FORM

		// attribution des variables post
		if(!empty($_POST['pro_libelle'])){
			$pro_libelle=$_POST['pro_libelle'];
		}else{
			$danger_txt="Veuillez renseigner le libellé du produit";
		}

		$pro_tva = 0;
		if(!empty($_POST['pro_tva'])){
			$pro_tva=intval($_POST['pro_tva']);
		}
		if(empty($pro_tva)){
			$danger_txt="Veuillez renseigner la TVA à utiliser pour ce code produit";
		}

		$pro_code="";
		if(!empty($_POST['pro_code'])){
			$pro_code=$_POST['pro_code'];
		}else{
			//$erreur="Veuillez renseigner le code";
		}

		$pro_code_duree=0;
		$pro_code_unit ="";
		$pro_code_type="";
		if(!empty($pro_code)){

			if(!empty($_POST['pro_code_duree'])){
				$pro_code_duree=floatval($_POST['pro_code_duree']);
			}
			if(!empty($_POST['pro_code_unit'])){
				$pro_code_unit = $_POST['pro_code_unit'];
			}

			if(!empty($_POST['pro_code_type'])){
				$pro_code_type = $_POST['pro_code_type'];
			}
		}

		if(!empty($_POST['pro_code_produit'])){
			$pro_code_produit = $_POST['pro_code_produit'];
		}else{
			$danger_txt="Veuillez renseigner le code produit";
		}


		if(!empty($danger_txt)){
			$_SESSION['message'][] = array(
				"titre" => "Formulaire incomplet!",
				"type" => "danger",
				"message" => $danger_txt
			);
			Header("Location:produit.php");
			die();
		}

		// INTRA
		$pro_intra=0;
		$pro_ht_intra=0;
		$pro_min_intra=0;
		$pro_min_dr_intra=0;
		if(!empty($_POST['pro_ht_intra'])){
			$pro_intra=1;
			$pro_ht_intra=$_POST['pro_ht_intra'];
			$pro_min_intra=floatval($_POST['pro_min_intra']);
			$pro_min_dr_intra=floatval($_POST['pro_min_dr_intra']);
		}

		// INTER
		$pro_inter=0;
		$pro_ht_inter=0;
		$pro_min_inter=0;
		$pro_min_dr_inter=0;
		if(!empty($_POST['pro_ht_inter'])){
			$pro_inter=1;
			$pro_ht_inter=$_POST['pro_ht_inter'];
			$pro_min_inter=floatval($_POST['pro_min_inter']);
			$pro_min_dr_inter=floatval($_POST['pro_min_dr_inter']);
		}

		$pro_gest_sta = 1;
		if(!empty($_POST['pro_gest_sta'])){
			$pro_gest_sta=intval($_POST['pro_gest_sta']);
		}
		$pro_categorie=0;
		if(!empty($_POST['pro_categorie'])){
			$pro_categorie=intval($_POST['pro_categorie']);
		}
		$pro_famille=0;
		if(!empty($_POST['pro_famille'])){
			$pro_famille=intval($_POST['pro_famille']);
		}
		$pro_sous_famille=0;
		if(!empty($_POST['pro_sous_famille'])){
			$pro_sous_famille=intval($_POST['pro_sous_famille']);
		}
		$pro_sous_sous_famille=0;
		if(!empty($_POST['pro_sous_sous_famille'])){

			$pro_sous_sous_famille=intval($_POST['pro_sous_sous_famille']);
		}

		$pro_qualification = 0;
		$pro_qualif_mois = 0;
		$pro_recurrent=0;
		$pro_qualifiant=1;
		$pro_recur_mois=0;
		if($_POST['pro_recurrent'] == 1){
			$pro_recurrent=1;
			if(!empty($_POST['pro_recur_mois'])){
				$pro_recur_mois=intval($_POST['pro_recur_mois']);
			}
		}elseif($_POST['pro_recurrent'] == 2){
			$pro_qualifiant=1;
			$pro_qualification=intval($_POST['pro_qualification']);
			if(!empty($_POST['pro_qualif_mois'])){
				$pro_qualif_mois=intval($_POST['pro_qualif_mois']);
			}
		}

		if(!empty($_POST['pro_niveau'])){
			$pro_niveau=$_POST['pro_niveau'];
		}else{
			$pro_niveau = 0;
		}
		if(!empty($_POST['pro_nb_demi_jour'])){
			$pro_nb_demi_jour=$_POST['pro_nb_demi_jour'];
		}else{
			$pro_nb_demi_jour = 0;
		}
		if(!empty($_POST['pro_duree_prep'])){
			$pro_duree_prep=$_POST['pro_duree_prep'];
		}else{
			$pro_duree_prep = 0;
		}
		if(!empty($_POST['pro_duree_demi_jour'])){
			$pro_duree_demi_jour=$_POST['pro_duree_demi_jour'];
		}else{
			$pro_duree_demi_jour = 0;
		}

		$pro_client=0;
		if(!empty($_POST['pro_client'])){
			 $pro_client=intval($_POST['pro_client']);
		}

		$pro_devis_txt=$_POST['pro_devis_txt'];
		$pro_reference_sta=$_POST['pro_reference_sta'];

		$pro_deductible=0;
		if(!empty($_POST['pro_deductible'])){
			$pro_deductible=1;
		}

		// challenge

		$pro_pt_0=0;
		$pro_pt_1=0;
		$pro_pt_2=0;
		if(!empty($_POST['pro_pt_0'])){
			 $pro_pt_0=floatval($_POST['pro_pt_0']);
		}
		if($pro_pt_0>0){
			if(!empty($_POST['pro_pt_1'])){
				 $pro_pt_1=floatval($_POST['pro_pt_1']);
			}
			if(!empty($_POST['pro_pt_2'])){
				 $pro_pt_2=floatval($_POST['pro_pt_2']);
			}
		}

		if(isset($_POST['pro_rapport_evac']) && $_POST['pro_rapport_evac'] == "pro_rapport_evac"){
			$pro_rapport_evac = 1;
		}else{
			$pro_rapport_evac = 0;
		}

		$pro_archive=0;
		if(!empty($_POST['pro_archive'])){
			$pro_archive=1;
		}

		$pro_vehicule=0;
		if(!empty($_POST['pro_vehicule'])){
			$pro_vehicule=1;
		}

		// FIN TRAITEMENT DU FORM
//************************************************

		// personne connecté
		$acc_utilisateur=0;
		if(!empty($_SESSION['acces']["acc_ref"])){
			if($_SESSION['acces']["acc_ref"]==1){
				$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
			}
		}


		// CONSTRUCTION DU CODE PRODUIT

		/*$code_unit_txt="";
		if($pro_code_unit==1){
			// H
			$code_unit_txt="H";
			$tps = intval($pro_code_duree) . "H";
			if($pro_code_duree-intval($pro_code_duree)!=0){
				$tps.=number_format(($pro_code_duree-intval($pro_code_duree))*60);
			}
			$pro_code_produit=$pro_code . "-" . $tps . "-" . $pro_code_type;

		}else{
			$code_unit_txt="J";
			$pro_code_produit=$pro_code . "-" . $pro_code_duree . "J" . "-" . $pro_code_type;
		}*/

		// donnee pour ajout du produit dans clients_produits
		$add_produit=array(
			"cpr_libelle" => $pro_libelle,
			"cpr_intra" => $pro_intra,
			"cpr_ht_intra" => $pro_ht_intra,
			"cpr_min_intra" => $pro_min_intra,
			"cpr_inter" => $pro_inter,
			"cpr_ht_inter" => $pro_ht_inter,
			"cpr_min_inter" => $pro_min_inter
		);
		// donnee pour reactive un produit dans une base
		$set_produit=array(
			"cpr_libelle" => $pro_libelle,
			"cpr_intra" => $pro_intra,
			"cpr_ht_intra" => $pro_ht_intra,
			"cpr_min_intra" => $pro_min_intra,
			"cpr_inter" => $pro_inter,
			"cpr_ht_inter" => $pro_ht_inter,
			"cpr_min_inter" => $pro_min_inter,
			"cpr_archive" => $pro_archive
		);

		if($pro_id>0){

			// MODIFICATION D'UN PRODUIT


			//$code_controle=$pro_code_produit;
			//$pro_code_produit.="-". $pro_id;

			// info pro avant modification

			$req = $Conn->prepare("SELECT pro_client,pro_code_heracles FROM produits WHERE pro_id = :doc_id");
			$req->bindParam(':doc_id', $pro_id);
			$req->execute();
			$d_produit = $req->fetch();
			if(empty($pro_code_produit)){
				$pro_code_produit=$d_produit["pro_code_heracles"];
			}

			// mise a jour

			// ON REGARDE SI LE CODE PRODUIT n'EXISTE PAS
			/*if(!empty($pro_code)){
				$req = $Conn->prepare("SELECT pro_code_produit FROM produits WHERE pro_code = :pro_code AND pro_code_duree = :pro_code_duree AND pro_code_type = :pro_code_type AND pro_code_unit = :pro_code_unit  AND pro_id != :pro_id");
				$req->bindParam(':pro_code', $pro_code);
				$req->bindParam(':pro_code_duree', $pro_code_duree);
				$req->bindParam(':pro_code_type', $pro_code_type);
				$req->bindParam(':pro_code_unit', $pro_code_unit);
				$req->bindParam(':pro_id', $pro_id);
				$req->execute();
				$produit_existe = $req->fetch();
			}*/
			if(!empty($pro_code_produit)){
				$req = $Conn->prepare("SELECT pro_code_produit FROM produits WHERE pro_code_produit = :pro_code_produit AND pro_id != :pro_id");
				$req->bindParam(':pro_code_produit', $pro_code_produit);
				$req->bindParam(':pro_id', $pro_id);
				$req->execute();
				$produit_existe = $req->fetch();
			}

			// LE PRODUIT NEXISTE PAS
			if(empty($produit_existe)){
				$req = $Conn->prepare("UPDATE produits SET pro_gestion_sta = :pro_gest_sta, pro_code_produit=:pro_code_produit,pro_libelle=:pro_libelle, pro_rapport_evac = :pro_rapport_evac, pro_categorie=:pro_categorie,pro_famille=:pro_famille,pro_sous_famille=:pro_sous_famille,pro_sous_sous_famille=:pro_sous_sous_famille,pro_qualifiant = :pro_qualifiant, pro_ht_intra=:pro_ht_intra, pro_ht_inter=:pro_ht_inter, pro_tva=:pro_tva,pro_qualification=:pro_qualification,pro_recurrent=:pro_recurrent,pro_recur_mois=:pro_recur_mois,pro_niveau=:pro_niveau,pro_deductible=:pro_deductible,pro_nb_demi_jour=:pro_nb_demi_jour,
				pro_duree_prep=:pro_duree_prep,pro_duree_demi_jour=:pro_duree_demi_jour,pro_client=:pro_client,pro_devis_txt=:pro_devis_txt,pro_archive=:pro_archive, pro_qualif_mois=:pro_qualif_mois,pro_reference_sta=:pro_reference_sta,pro_inter=:pro_inter,pro_intra=:pro_intra,
				pro_vehicule=:pro_vehicule,pro_code=:pro_code,pro_code_unit=:pro_code_unit,pro_code_type=:pro_code_type,pro_code_duree=:pro_code_duree,pro_min_intra=:pro_min_intra,pro_min_inter=:pro_min_inter
				,pro_min_dr_inter=:pro_min_dr_inter,pro_min_dr_intra=:pro_min_dr_intra,pro_pt_0=:pro_pt_0,pro_pt_1=:pro_pt_1,pro_pt_2=:pro_pt_2
				WHERE pro_id=:pro_id");

				$req->bindParam(':pro_id', $pro_id);
				$req->bindParam(':pro_code_produit', $pro_code_produit);
				$req->bindParam(':pro_libelle', $pro_libelle);
				$req->bindParam(':pro_categorie', $pro_categorie);
				$req->bindParam(':pro_famille', $pro_famille);
				$req->bindParam(':pro_sous_famille', $pro_sous_famille);
				$req->bindParam(':pro_sous_sous_famille', $pro_sous_sous_famille);
				$req->bindParam(':pro_ht_intra', $pro_ht_intra);
				$req->bindParam(':pro_ht_inter', $pro_ht_inter);
				$req->bindParam(':pro_tva', $pro_tva);
				$req->bindParam(':pro_qualification', $pro_qualification);
				$req->bindParam(':pro_recurrent', $pro_recurrent);
				$req->bindParam(':pro_recur_mois', $pro_recur_mois);
				$req->bindParam(':pro_niveau', $pro_niveau);
				$req->bindParam(':pro_deductible', $pro_deductible);
				$req->bindParam(':pro_nb_demi_jour', $pro_nb_demi_jour);
				$req->bindParam(':pro_duree_prep', $pro_duree_prep);
				$req->bindParam(':pro_duree_demi_jour', $pro_duree_demi_jour);
				$req->bindParam(':pro_client', $pro_client);
				$req->bindParam(':pro_devis_txt', $pro_devis_txt);
				$req->bindParam(':pro_archive', $pro_archive);
				$req->bindParam(':pro_qualif_mois', $pro_qualif_mois);
				$req->bindParam(':pro_reference_sta', $pro_reference_sta);
				$req->bindParam(':pro_inter', $pro_inter);
				$req->bindParam(':pro_intra', $pro_intra);
				$req->bindParam(':pro_rapport_evac', $pro_rapport_evac);
				$req->bindParam(':pro_vehicule', $pro_vehicule);
				$req->bindParam(':pro_code', $pro_code);
				$req->bindParam(':pro_code_unit', $pro_code_unit);
				$req->bindParam(':pro_code_type', $pro_code_type);
				$req->bindParam(':pro_code_duree', $pro_code_duree);
				$req->bindParam(':pro_min_inter', $pro_min_inter);
				$req->bindParam(':pro_min_intra', $pro_min_intra);
				$req->bindParam(':pro_min_dr_inter', $pro_min_dr_inter);
				$req->bindParam(':pro_min_dr_intra', $pro_min_dr_intra);
				$req->bindParam(':pro_qualifiant', $pro_qualifiant);
				$req->bindParam(':pro_pt_0', $pro_pt_0);
				$req->bindParam(':pro_pt_1', $pro_pt_1);
				$req->bindParam(':pro_pt_2', $pro_pt_2);
				$req->bindParam(':pro_gest_sta', $pro_gest_sta);
				$req->execute();
			}else{
				// LE PRODUIT EXISTE DEJA
				$_SESSION['message'][] = array(
					"titre" => "Attention",
					"type" => "danger",
					"message" => "Un code produit ". $pro_code_produit . " existe déjà"
				);
				Header("Location:produit.php?id=" . $pro_id);
				die();
			}

			if($pro_client>0){

				// PRODUIT SPECIFIQUE A UN CLIENT

				if($d_produit['pro_client']==0){

					// le produit devient specifique
					// on archive le produit dans toutes les bases clients autre que le client spé

					$sql="SELECT cpr_archive FROM Clients_Produits WHERE cpr_produit=" . $pro_id . " AND NOT cpr_client=" . $pro_client . " AND cpr_archive=0;";
					$req=$Conn->query($sql);
					$result=$req->fetchAll();
					if(!empty($result)){
						$archive_pro=array(
							"cpr_archive" => 1
						);
						foreach($result as $r){
							set_client_produit($pro_id,$r["cpr_client"],$archive_pro,$r);
						}

					}

				}elseif($d_produit['pro_client'] != $pro_client){

					// produit change de client spécifique
					// ON ARCHIVE LE produit dans la bse de l'ancien client spé
					$sql="SELECT * FROM Clients_Produits WHERE cpr_produit=" . $pro_id . " AND cpr_client=" . $d_produit['pro_client'] . ";";
					$req=$Conn->query($sql);
					$result=$req->fetchAll();
					if(!empty($result)){
						$archive_pro=array(
							"cpr_archive" => 1
						);
						foreach($result as $r){

							set_client_produit($pro_id,$r["cpr_client"],$archive_pro,$r);
						}

					}
				}

				// on regarde s'il est présent dans la base du client spé
				$slq_select="SELECT * FROM Clients_Produits WHERE cpr_produit=" . $pro_id . " AND cpr_client=" . $pro_client . ";";
				$req=$Conn->query($slq_select);
				$d_client_produit=$req->fetch();
				if(empty($d_client_produit)){
					//ajout
					$result=add_client_produit($pro_id,$pro_client,$add_produit);
					if(!$result){
						$warning_txt="Le code produi n'a pas été associé au client!";
					}
				}else{
					// produit existe déja dans la base du client
					// on update
					$result=set_client_produit($pro_id,$pro_client,$set_produit,$d_client_produit);
					if(!$result){
						$warning_txt="Le code produi n'a pas été associé au client!";
					}
				}
			}
			if(empty($warning_txt)){
				$succes_txt="Le produit " . $pro_code_produit . " a bien été modifié!";
			}

			// fin traitement update

		}else{

			// ON REGARDE SI LE CODE PRODUIT n'EXISTE PAS
			/*$req = $Conn->prepare("SELECT pro_code_produit FROM produits WHERE pro_code = :pro_code AND pro_code_duree = :pro_code_duree AND pro_code_type = :pro_code_type AND pro_code_unit = :pro_code_unit");
			$req->bindParam(':pro_code', $pro_code);
			$req->bindParam(':pro_code_duree', $pro_code_duree);
			$req->bindParam(':pro_code_type', $pro_code_type);
			$req->bindParam(':pro_code_unit', $pro_code_unit);*/

			$req = $Conn->prepare("SELECT pro_code_produit FROM produits WHERE pro_code_produit = :pro_code_produit");
			$req->bindParam(':pro_code_produit', $pro_code_produit);
			$req->execute();
			$produit_existe = $req->fetch();
			//$produit_existe=array();

			// NOUVEAU PRODUIT IL NEXISTE PAS ENCORE
			if(empty($produit_existe)){
				$req = $Conn->prepare("INSERT INTO produits (pro_code_produit,pro_libelle,pro_code_duree,pro_code_type, pro_code_unit,pro_code,pro_rapport_evac,pro_categorie,pro_famille,pro_sous_famille,pro_sous_sous_famille,pro_ht_intra,
				pro_ht_inter, pro_tva,pro_qualification,pro_qualifiant, pro_recurrent,pro_recur_mois,pro_niveau,pro_deductible,pro_nb_demi_jour,
				pro_duree_prep,pro_duree_demi_jour,pro_client,pro_devis_txt,pro_archive, pro_qualif_mois,pro_reference_sta,pro_inter,pro_intra,pro_vehicule,pro_min_intra,pro_min_inter,pro_min_dr_intra,pro_min_dr_inter
				,pro_pt_0,pro_pt_1,pro_pt_2, pro_gestion_sta)
				VALUES ( :pro_code_produit,:pro_libelle,:pro_code_duree,:pro_code_type, :pro_code_unit,:pro_code,:pro_rapport_evac, :pro_categorie, :pro_famille, :pro_sous_famille, :pro_sous_sous_famille, :pro_ht_intra, :pro_ht_inter, :pro_tva,
				:pro_qualification,:pro_qualifiant, :pro_recurrent, :pro_recur_mois, :pro_niveau, :pro_deductible, :pro_nb_demi_jour, :pro_duree_prep, :pro_duree_demi_jour,
				:pro_client, :pro_devis_txt, :pro_archive, :pro_qualif_mois, :pro_reference_sta, :pro_inter, :pro_intra, :pro_vehicule, :pro_min_intra, :pro_min_inter, :pro_min_dr_intra, :pro_min_dr_inter
				,:pro_pt_0,:pro_pt_1,:pro_pt_2, :pro_gest_sta)");
				$req->bindParam(':pro_code_produit', $pro_code_produit);
				$req->bindParam(':pro_libelle', $pro_libelle);
				$req->bindParam(':pro_code', $pro_code);
				$req->bindParam(':pro_code_duree', $pro_code_duree);
				$req->bindParam(':pro_code_type', $pro_code_type);
				$req->bindParam(':pro_code_unit', $pro_code_unit);
				$req->bindParam(':pro_categorie', $pro_categorie);
				$req->bindParam(':pro_famille', $pro_famille);
				$req->bindParam(':pro_sous_famille', $pro_sous_famille);
				$req->bindParam(':pro_sous_sous_famille', $pro_sous_sous_famille);
				$req->bindParam(':pro_ht_intra', $pro_ht_intra);
				$req->bindParam(':pro_ht_inter', $pro_ht_inter);
				$req->bindParam(':pro_tva', $pro_tva);
				$req->bindParam(':pro_qualification', $pro_qualification);
				$req->bindParam(':pro_recurrent', $pro_recurrent);
				$req->bindParam(':pro_recur_mois', $pro_recur_mois);
				$req->bindParam(':pro_niveau', $pro_niveau);
				$req->bindParam(':pro_deductible', $pro_deductible);
				$req->bindParam(':pro_nb_demi_jour', $pro_nb_demi_jour);
				$req->bindParam(':pro_duree_prep', $pro_duree_prep);
				$req->bindParam(':pro_duree_demi_jour', $pro_duree_demi_jour);
				$req->bindParam(':pro_client', $pro_client);
				$req->bindParam(':pro_devis_txt', $pro_devis_txt);
				$req->bindParam(':pro_archive', $pro_archive);
				$req->bindParam(':pro_qualif_mois', $pro_qualif_mois);
				$req->bindParam(':pro_reference_sta', $pro_reference_sta);
				$req->bindParam(':pro_inter', $pro_inter);
				$req->bindParam(':pro_intra', $pro_intra);
				$req->bindParam(':pro_rapport_evac', $pro_rapport_evac);
				$req->bindParam(':pro_vehicule', $pro_vehicule);
				$req->bindParam(':pro_min_inter', $pro_min_inter);
				$req->bindParam(':pro_min_intra', $pro_min_intra);
				$req->bindParam(':pro_min_dr_inter', $pro_min_dr_inter);
				$req->bindParam(':pro_min_dr_intra', $pro_min_dr_intra);
				$req->bindParam(':pro_qualifiant', $pro_qualifiant);
				$req->bindParam(':pro_pt_0', $pro_pt_0);
				$req->bindParam(':pro_pt_1', $pro_pt_1);
				$req->bindParam(':pro_pt_2', $pro_pt_2);
				$req->bindParam(':pro_gest_sta', $pro_gest_sta);
				$req->execute();
				$pro_id = $Conn->lastInsertId();

				/*$pro_code_produit.="-". $pro_id;

				$req = $Conn->prepare("UPDATE produits SET pro_code_produit = '" . $pro_code_produit . "' WHERE pro_id = " . $pro_id);
				$req->execute();*/

			}else{
				// LE PRODUIT EXISTE DEJA
				$_SESSION['message'][] = array(
					"titre" => "Attention",
					"type" => "danger",
					"message" => "Un code produit " . $pro_code_produit . " existe déjà"
				);
				Header("Location:produit.php");
				die();
			}


			if($pro_client>0){

				// PRODUIT SPE

				//ajout

				$add_produit=array(
					"cpr_libelle" => $pro_libelle,
					"cpr_intra" => $pro_intra,
					"cpr_ht_intra" => $pro_ht_intra,
					"cpr_min_intra" => $pro_min_intra,
					"cpr_inter" => $pro_inter,
					"cpr_ht_inter" => $pro_ht_inter,
					"cpr_min_inter" => $pro_min_inter
				);

				add_client_produit($pro_id,$pro_client,$add_produit);

			}

			// CREATION D'UNE NOTIFICATION

			$not_icone="<i class='fa fa-label' ></i>";
			$not_texte="Le produit ". $pro_libelle  . " a été crée";
			$not_classe="bg-success";
			$not_url="produit_voir.php?produit=" . $pro_libelle;

			include "modeles/mod_add_notification.php" ;
			add_notifications($not_icone,$not_texte,$not_classe,$not_url,"7,11,8,12","","",0,0,0);


			$succes_txt="Le produit " . $pro_code_produit . " a bien été enregistré!";
		}

		$_SESSION['message'][] = array(
			"titre" => "Enregistrement terminé",
			"type" => "success",
			"message" => $succes_txt
		);

		if(!empty($_SESSION['retour'])){
			header("Location: " . $_SESSION['retour']);
		}else{
			header("Location: produit_tri.php");
		}
	}


?>
