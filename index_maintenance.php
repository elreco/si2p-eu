﻿<?php 

session_start(); 

?>
<html>
<head>
	<meta charset="utf-8" >
	<title>Si2P - Maintenance</title>
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.min.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/login.css">
	<link href='https://fonts.googleapis.com/css?family=Audiowide' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="main">
		<div class="logo" style="margin-bottom:20px;">
			<img src="assets/img/login/logo.png" />
		</div>
		
		<div class="formulaire">
		
			<h1>Maintenance</h1>
			
			<div class="formulaire-body" style="min-height:300px;padding-top:90px;"  >
			
				<p>
					Une opération de maintenance est actuellement en cours.
				</p>
				<p>
					Nous nous efforçons de rétablir le service au plus vite.
				</p>

			</div>
		</div>
	</div>
	
	<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
	<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
	<script src="vendor/plugins/pnotify/pnotify.js"></script>
	<script>
		jQuery(document).ready(function(){	
		
	<?php 	// ALERT A DESTINATION DES UTRILISATEURS
			if(!empty($_SESSION['message'])){
				foreach($_SESSION['message'] as $m){ ?>
					new PNotify({
						title: "<?= $m['titre'] ?>",
						text: "<?= $m['message'] ?>",
						type: "<?= $m['type'] ?>", // all contextuals available(info,system,warning,etc)
						hide: true,
						delay: 4000
					});
	<?php 		}
				unset($_SESSION['message']);
			} ?>
		});
	</script>	
</body>
</html>