<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';
include_once 'includes/connexion_fct.php';
include_once 'modeles/mod_parametre.php';

$client = 0;
if (isset($_GET['client'])){
   $client = $_GET['client'];
}
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$origine="";
if(!empty($_GET['origine'])){
	$origine=$_GET['origine'];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
$etape=0;
if(!empty($_SESSION['etape'])){
    $etape = $_SESSION['etape'];
}
$etape_chrono=0;
if(!empty($_SESSION['etape_chrono'])){
    $etape_chrono = $_SESSION['etape_chrono'];
}
$relance = 0;
if (isset($_GET['relance'])){
   $relance = $_GET['relance'];
}



$sql="SELECT * FROM Relances WHERE rel_id=" . $relance;
$req = $Conn->query($sql);
$d_relance=$req->fetch();
$blocage = false;
if(!empty($d_relance)){
    $client = $d_relance['rel_client'];
    if($d_relance['rel_courrier'] OR $d_relance['rel_envoie']){
        $blocage = true;
    }
}
// CLIENT
$req=$Conn->query("SELECT * FROM clients WHERE cli_id =" . $client);
$d_client=$req->fetch();
$maison_mere=false;
if(!empty($d_client['cli_filiale_de']) && $d_client['cli_categorie'] == 1){
    $maison_mere = true;
}

// OPCA

if(!empty($d_relance['rel_opca'])){
    $req=$Conn->query("SELECT * FROM clients WHERE cli_id =" . $d_relance['rel_opca']);
    $d_opca=$req->fetch();
}

$sql="SELECT * FROM Relances_Etats ORDER BY ret_j_deb;";
$req = $Conn->query($sql);
$d_relances_etats=$req->fetchAll();
$ret_libelle = "";
$ret_code = "";
$rel_j_deb=0;
$rel_j_fin=0;
$tab_relance = array();
$actu_fac =true;
$tab_relance[0] = $actu_fac;
$listeActuEtat = "0";
foreach($d_relances_etats as $d){
    if($d['ret_id'] != $d_relance['rel_etat_relance']){
        $listeActuEtat = $listeActuEtat . "," . $d['ret_id'];
    }elseif($d_relance['rel_etat_relance']==$d['ret_id']){
        $actu_fac=false;
        if($d['ret_id'] == 1){
            $listeActuEtat = $listeActuEtat . "," . $d['ret_id'];
            $ret_libelle = $d['ret_libelle'];
            $ret_code = $d['ret_code'];
            $rel_j_deb = $d['ret_j_fin'];
            $rel_j_fin = $d['ret_j_deb'];
        }
    }
    $tab_relance[$d['ret_id']] = $d['ret_code'];
}

$rel_date_deb = null;
if($rel_j_deb>0){
    $rel_date_deb =date('Y-m-d', strtotime($rel_date. ' - ' . $rel_j_deb . ' days'));
}
$rel_date_fin = null;
if($rel_j_fin>0){
    $rel_date_fin =date('Y-m-d', strtotime($rel_date. ' - ' . $rel_j_fin . ' days'));
}

if($origine == "histo"){
    $_SESSION['retour'] = "facture_relance_histo.php?client=" . $client;
}else{
    $_SESSION['retour'] = "facture_relance_voir.php?client=" . $client;
}
// FACTURES

$sql="SELECT soc_code FROM Societes WHERE soc_id =" . $acc_societe;
$req = $Conn->query($sql);
$d_societe=$req->fetch();


// FACTURES CLIENT
$listeCom ="";
if($_SESSION['acces']['acc_profil'] == 3){
    $sql="SELECT com_id FROM Commerciaux WHERE com_utilisateur =" . $acc_utilisateur;
    $req = $Conn->query($sql);
    $d_commerciaux=$req->fetchAll();

    foreach($d_commerciaux as $c){
        $listCom = $listeCom . "," . $c['com_id'];
    }
}
$total_ht=0;
$total_ttc=0;
$total_regle=0;
$total_reste_du=0;

$req=$Conn->query("SELECT * FROM Relances_Factures WHERE rfa_relance =" . $relance);
$Relances_Factures=$req->fetchAll();
$o=0;
foreach($Relances_Factures as $rfa){

   $ConnFct = connexion_fct($rfa['rfa_facture_soc']);
   $req="SELECT fac_id,fac_numero,fac_date,fac_total_ht,fac_date_reg_prev,fac_total_ttc,fac_regle,fac_nature,fac_etat_relance,fac_date_reg_rel,fac_opca";
    $req=$req . " ,fac_date_relance,fac_relance_stop";
    $req=$req . " FROM Factures";
    $req=$req . " WHERE fac_id=" . $rfa['rfa_facture'];
    $req=$req . "  ORDER BY fac_date_reg_prev,fac_numero;";

    if($_SESSION['acces']['acc_profil'] == 3){

    $req=$req . " AND fac_commercial IN (" . $listeCom . ")";
    }
    $req=$req . " ORDER BY fac_date_reg_prev,fac_numero;";
    $req=$ConnFct->query($req);
    $factures[$o]=$req->fetch();
    $factures[$o]['fac_societe'] = $rfa['rfa_facture_soc'];
    $o = $o+1;
}


$listeFac="";
$nbFac = 0;


$d_statut_libelle=array();

$nbEtat = 0;

$rel_titre="";
$rel_date_rappel="";
$ret_traitement=0;
$relance_nom[0] = array(
    "ret_id" => 0,
    "ret_libelle"=>"",
    "ret_code" => "",
    "ret_j_deb" => 0,
    "ret_j_fin" => 0,
    "ret_traitement" => 0
);
$d_statut_libelle=array();
foreach($d_relances_etats as $re){
    $d_statut_libelle[$re['ret_id']]= $re['ret_libelle'];
    $nbEtat = $nbEtat +1;

    $relance_nom[$nbEtat]["ret_id"] = $re['ret_id'];
    $relance_nom[$nbEtat]["ret_code"] = $re['ret_code'];
    $relance_nom[$nbEtat]["ret_libelle"] = $re['ret_libelle'];
    $relance_nom[$nbEtat]["ret_j_deb"] = $re['ret_j_deb'];
    $relance_nom[$nbEtat]["ret_j_fin"] = $re['ret_j_fin'];
    $relance_nom[$nbEtat]["ret_traitement"] = $re['ret_traitement'];

    if($re['ret_id'] == $etape){
        $rel_titre=$re['ret_libelle'];
        $ret_traitement=$re['ret_traitement'];
        if($re['ret_traitement'] == 1){
            $demain = new DateTime();
            $demain->modify('+1 day');

			$rel_date_rappel = $demain->format('Y-m-d');
        }
    }

}
// CONTACTS IMPAYES
$sql="SELECT DISTINCT con_id,con_fonction,con_fonction_nom,con_titre,con_nom,con_prenom,con_tel,con_portable,con_fax,con_mail,con_compta,con_comment,aco_contact
FROM Contacts LEFT OUTER JOIN Adresses_Contacts ON (Contacts.con_id=Adresses_Contacts.aco_contact AND aco_defaut)
WHERE con_ref_id=" . $client . " AND con_ref=1 AND con_compta = 1;";
$req=$Conn->query($sql);
$contacts=$req->fetchAll();

$sql="SELECT DISTINCT con_id,con_fonction,con_fonction_nom,con_titre,con_nom,con_prenom,con_tel,con_portable,con_fax,con_mail,con_compta,con_comment,aco_contact
FROM Contacts LEFT OUTER JOIN Adresses_Contacts ON (Contacts.con_id=Adresses_Contacts.aco_contact AND aco_defaut)
WHERE con_ref_id=" . $client . " AND con_ref=1 AND con_compta = 0;";
$req=$Conn->query($sql);
$contacts_autres=$req->fetchAll();

// CONTACTS CLIENT

//contact par defaut
$d_con_fonction_nom=array(
    "0" => ""
);
$sql="SELECT * FROM contacts_fonctions ORDER BY cfo_libelle";
$req=$Conn->query($sql);
$d_contact_fonctions=$req->fetchAll();
foreach($d_contact_fonctions as $d){
    $d_con_fonction_nom[$d["cfo_id"]]=$d["cfo_libelle"];
}

// die();
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Relances</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
<form action="facture_relance_mod_enr.php" method="POST" id="admin-form">
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">

            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-left">

                                        <div class="content-header">
                                                <h2>Relances du client  <b class="text-primary"><?= $d_client['cli_nom'] ?></b></h2>
                                                <h4>Modification de la relance du <?= convert_date_txt($d_relance['rel_date']) ?></h4>
                                        </div>
                                        <input type="hidden" name="relance" value="<?=$relance?>" />
                                        <input type="hidden" name="origine" value="<?=$origine?>" />
                                        <div class="col-md-12">

                                            <div class="row">
                                                <div class="col-md-12">
                                                <h2>Factures concernées par la relance</h2>
                                                <?php 			if(!empty($factures)){
                                         ?>

                                                <div class="table-responsive mt15">
                                                    <table class="table dataTables table-striped table-hover" >
                                                        <thead>
                                                            <tr class="dark2" >
                                                            <?php  if(empty($blocage)){?>
                                                                <th class="no-sort"> </th>
                                                            <?php }?>
                                                                <th>Facture</th>
                                                                <?php
                                                                if($d_client['cli_groupe'] == 1 && !empty($d_client['cli_filiale_de'])){ ?>
                                                                        <th>Client</th>
                                                                <?php } ?>
                                                                <th>Date</th>
                                                                <th>Date de réglement</th>
                                                                <th>Date réf. relance</th>
                                                                <th class="text-right">Retard</th>
                                                                <th class="text-right">TTC</th>
                                                                <th class="text-right">Réglé</th>
                                                                <th class="text-right">Reste dû</th>
                                                                <th class="text-center">&Eacute;tat relance</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                <?php

                                                            foreach($factures as $f){


                                                                $facture = $f['fac_id'];
                                                                $fac_date="";
                                                                if(!empty($f['fac_date'])){
                                                                    $dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
                                                                    $fac_date=$dt_fac_date->format("d/m/Y");
                                                                }
                                                                $fac_date_reg_prev="";
                                                                if(!empty($f['fac_date_reg_prev'])){
                                                                    $dt_fac_date_reg_prev=date_create_from_format("Y-m-d",$f['fac_date_reg_prev']);
                                                                    $fac_date_reg_prev=$dt_fac_date_reg_prev->format("d/m/Y");
                                                                }
                                                                $fac_date_reg_rel="";
                                                                if(!empty($f['fac_date_reg_rel'])){
                                                                    $dt_fac_date_reg_rel=date_create_from_format("Y-m-d",$f['fac_date_reg_rel']);
                                                                    $fac_date_reg_rel=$dt_fac_date_reg_rel->format("d/m/Y");
                                                                }


                                                                // style de la ligne
                                                                $facture_opca=false;
                                                                $fac_opca=0;
                                                                if(!empty($f['fac_opca'])){
                                                                    $fac_opca = $f['fac_opca'];
                                                                    $facture_opca=true;
                                                                }



                                                                // RETARD
                                                                $retard=0;

                                                                if($f['fac_nature'] == 1){
                                                                    if(!empty($f['fac_date_reg_rel'])){
                                                                        $datetime1 = new DateTime($f['fac_date_reg_rel']);
                                                                        $datetime2 = new DateTime();
                                                                        $interval = $datetime1->diff($datetime2);
                                                                        $retard = $interval->format('%a');
                                                                    }else{
                                                                        $datetime1 = new DateTime($f['fac_date_reg_prev']);
                                                                        $datetime2 = new DateTime();
                                                                        $interval = $datetime1->diff($datetime2);
                                                                        $retard = $interval->format('%a');
                                                                    }
                                                                }else{
                                                                    $fac_date_reg = " ";
                                                                }
                                                                if(empty($f['fac_relance_stop'])){
                                                                    if(empty($listeFac)){
                                                                        $listeFac=$facture;
                                                                    }else{
                                                                        $listeFac=$listeFac . "," . $facture;
                                                                    }

                                                                    $nbFac=$nbFac+1;
                                                                }
                                                                // OPCA
                                                                if(!empty($f['fac_opca'])){
                                                                    $sql="SELECT cli_nom FROM Clients WHERE cli_id = " . $f['fac_opca'];
                                                                    $req = $Conn->query($sql);
                                                                    $d_opca=$req->fetch();
                                                                }



                                                                foreach($relance_nom as $k=>$ren){
                                                                    if($f['fac_etat_relance'] == $ren['ret_id']){
                                                                        $code_relance = $ren['ret_code'];
                                                                    }
                                                                }


                                                                $reste_du=$f['fac_total_ttc']-$f['fac_regle'];

                                                                $total_ht+=$f['fac_total_ht'];
                                                                $total_ttc+=$f['fac_total_ttc'];
                                                                $total_regle+=$f['fac_regle'];
                                                                $total_reste_du+=$reste_du;




                                                                $style_tr="";
                                                                $style_a="";
                                                                $id_tr="";
                                                                if($f['fac_nature'] == 1){
                                                                    $id_tr="id='tab_ligne_" . $nbFac . "'";

                                                                    if($f['fac_relance_stop']){
                                                                        $style_tr="style='color:orange;'";
                                                                    }elseif($facture_opca){
                                                                        $style_tr="style='color:purple;'";
                                                                    }
                                                                    if($etape > 0 AND $etat_relance == $etape){
                                                                        $class_tr="class='ligne_fac_select'";
                                                                    }else{
                                                                        $class_tr="class='ligne_fac'";
                                                                    }
                                                                }

                                                                ?>
                                                                <tr  <?=$id_tr?> <?=$style_tr?> >
                                                                    <?php  if(empty($blocage)){?>
                                                                    <td>
                                                                    <?php if($f['fac_nature'] == 1){ ?>

                                                                        <input type="checkbox"  name="ligne_fac_<?=$nbFac?>"  disabled >
                                                                        <input type="hidden" id="fac_societe_<?=$nbFac?>" name="fac_societe_<?=$nbFac?>" value="<?=$f['fac_societe']?>" />
                                                                        <input type="hidden" name="fac_id_<?=$nbFac?>" value="<?=$facture?>" />
                                                                        <input type="hidden" id="fac_numero_<?=$nbFac?>" name="fac_numero_<?=$nbFac?>" value="<?=$f['fac_numero']?>" />
                                                                    <?php }?>
                                                                    </td>
                                                                    <?php }?>
                                                                    <td>

                                                                            <?=$f['fac_numero']?>

                                                                    </td>

                                                                    <?php if($d_client['cli_groupe'] == 1 && !empty($d_client['cli_filiale_de'])){ ?>

                                                                        <td> <?= $f['cli_code']?></td>

                                                                    <?php } ?>


                                                                    <td><?=convert_date_txt($f['fac_date'])?></td>
                                                                    <td>

                                                                        <?= $fac_date_reg_prev ?>
                                                                    </td>
                                                                    <td><?=$fac_date_reg_rel?></td>
                                                                    <td class="text-right"><?= $retard ?> jours</td>
                                                                    <td class="text-right"><?= number_format($f['fac_total_ttc'], 2, ',', ' ') ?> €</td>
                                                                    <td class="text-right">

                                                        <?php
                                                                            echo(number_format($f['fac_regle'], 2, ',', ' '));
                                                                         ?>
                                                                    </td>
                                                                    <td class="text-right">

                                                    <?php
                                                                        echo(number_format($reste_du, 2, ',', ' '));
                                                                    ?>
                                                                    </td>

                                                                    <td class="text-center" >
                                                                        <?php if(!empty($f['fac_etat_relance'])){ ?>

                                                                                <?= $code_relance ?>

                                                                        <?php }else{?>
                                                                        <a href="#">Pas d'état de relance</a>
                                                                        <?php }?>
                                                                    </td>

                                                                </tr>
                                    <?php					}
                        ?>
                                                        </tbody>
                                                        <tfoot>

                                                            <tr>
                                                                <?php
                                                                if($d_client['cli_groupe'] == 1 && !empty($d_client['cli_filiale_de'])){ ?>
                                                                    <th colspan="6" class="text-right" >Total :</th>
                                                            <?php }else{ ?>
                                                                    <th colspan="5" class="text-right" >Total :</th>
                                                            <?php }?>
                                                                <td class="text-right" ><?=number_format($total_ttc, 2, ',', ' ');?> €</td>
                                                                <td class="text-right" ><?=number_format($total_regle, 2, ',', ' ');?> €</td>
                                                                <td class="text-right" ><?=number_format($total_reste_du, 2, ',', ' ');?> €</td>
                                                                <td colspan="3" class="text-center" >&nbsp;</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                                <?php } ?>
                                                <h2>Factures pouvant être ajoutées à la relance</h2>
                                                <?php           if(empty($blocage)){
                                         ?>

                                                <div class="table-responsive mt15">
                                                    <table class="table dataTables table-striped table-hover" >
                                                        <thead>
                                                            <tr class="dark2" >
                                                                <th> </th>
                                                                <th>Facture</th>
                                                                <?php
                                                                if($d_client['cli_groupe'] == 1 && !empty($d_client['cli_filiale_de'])){ ?>
                                                                        <th>Client</th>
                                                                <?php } ?>
                                                                <th>Date</th>
                                                                <th>Date de réglement</th>
                                                                <th>Date réf. relance</th>
                                                                <th class="text-right">Retard</th>
                                                                <th class="text-right">TTC</th>
                                                                <th class="text-right">Réglé</th>
                                                                <th class="text-right">Reste dû</th>
                                                                <th class="text-center">&Eacute;tat relance</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                <?php

                                                            $total_ttc=0;
                                                            $total_regle=0;
                                                            $total_reste_du=0;
                                                            $req=$Conn->query("SELECT * FROM Relances_Factures WHERE NOT rfa_relance = ".$relance);
                                                            $Relances_Factures_2=$req->fetchAll();
                                                            $o2=0;
                                                            foreach($Relances_Factures_2 as $re2){
                                                                if(!empty($re2['rfa_facture_soc'])){
                                                                    $ConnFct = connexion_fct($re2['rfa_facture_soc']);
                                                                    $req="SELECT fac_id,fac_numero,fac_date,fac_total_ht,fac_date_reg_prev,fac_total_ttc,fac_regle,fac_nature,fac_etat_relance,fac_date_reg_rel,fac_opca";
                                                                    $req=$req . " ,fac_date_relance,fac_relance_stop";
                                                                    $req=$req . ",cli_id,cli_code";
                                                                    $req=$req . " FROM Factures ";
                                                                    $req=$req . " LEFT JOIN Clients ON (Factures.fac_client=Clients.cli_id)";
                                                                    $req=$req . " WHERE NOT fac_total_ttc = fac_regle AND fac_nature=1";
                                                                    $req=$req . " AND fac_etat_relance IN (".$listeActuEtat.")";
                                                                    if($maison_mere){
                                                                        $req=$req . " AND (cli_id=". $client." OR cli_filiale_de=" . $client . ")";
                                                                    }else{
                                                                        $req=$req . " AND cli_id=" . $client;
                                                                    }
                                                                    if($d_relance['rel_opca']>0){
                                                                        $req=$req . " AND fac_opca=" . $opca;
                                                                    }else{
                                                                        $req=$req . " AND fac_opca IN (0,999,1000)";
                                                                    }
                                                                    if($_SESSION['acces']['acc_profil'] == 3){

                                                                        $req=$req . " AND fac_commercial IN (" . $listeCom . ")";
                                                                    }
                                                                    $req=$req . " AND NOT fac_relance_perdu AND NOT fac_relance_stop AND fac_total_ttc-fac_regle > 0.5";
                                                                    $req=$req . " ORDER BY fac_date_reg_prev,fac_numero;";
                                                                    $req=$ConnFct->query($req);
                                                                    $factures_relance[$o2]=$req->fetch();
                                                                    if(!empty($factures_relance[$o2])){
                                                                        $factures_relance[$o2]["fac_societe"] = $re2['rfa_facture_soc'];
                                                                    }

                                                                    $o2=$o2+1;
                                                                }
                                                            }


                                                            foreach($factures_relance as $f){
                                                                if(!empty($f['fac_id'])){

                                                                $facture = $f['fac_id'];
                                                                $fac_date="";
                                                                if(!empty($f['fac_date'])){
                                                                    $dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
                                                                    $fac_date=$dt_fac_date->format("d/m/Y");
                                                                }
                                                                $fac_date_reg_prev="";
                                                                if(!empty($f['fac_date_reg_prev'])){
                                                                    $dt_fac_date_reg_prev=date_create_from_format("Y-m-d",$f['fac_date_reg_prev']);
                                                                    $fac_date_reg_prev=$dt_fac_date_reg_prev->format("d/m/Y");
                                                                }
                                                                $fac_date_reg_rel="";
                                                                if(!empty($f['fac_date_reg_rel'])){
                                                                    $dt_fac_date_reg_rel=date_create_from_format("Y-m-d",$f['fac_date_reg_rel']);
                                                                    $fac_date_reg_rel=$dt_fac_date_reg_rel->format("d/m/Y");
                                                                }


                                                                // style de la ligne
                                                                $facture_opca=false;
                                                                $fac_opca=0;
                                                                if(!empty($f['fac_opca'])){
                                                                    $fac_opca = $f['fac_opca'];
                                                                    $facture_opca=true;
                                                                }



                                                                // RETARD
                                                                $retard=0;

                                                                if($f['fac_nature'] == 1){
                                                                    if(!empty($f['fac_date_reg_rel'])){
                                                                        $datetime1 = new DateTime($f['fac_date_reg_rel']);
                                                                        $datetime2 = new DateTime();
                                                                        $interval = $datetime1->diff($datetime2);
                                                                        $retard = $interval->format('%a');
                                                                    }else{
                                                                        $datetime1 = new DateTime($f['fac_date_reg_prev']);
                                                                        $datetime2 = new DateTime();
                                                                        $interval = $datetime1->diff($datetime2);
                                                                        $retard = $interval->format('%a');
                                                                    }
                                                                }else{
                                                                    $fac_date_reg = " ";
                                                                }
                                                                if(empty($f['fac_relance_stop'])){
                                                                    if(empty($listeFac)){
                                                                        $listeFac=$facture;
                                                                    }else{
                                                                        $listeFac=$listeFac . "," . $facture;
                                                                    }

                                                                    $nbFac=$nbFac+1;
                                                                }
                                                                // OPCA
                                                                if(!empty($f['fac_opca'])){
                                                                    $sql="SELECT cli_nom FROM Clients WHERE cli_id = " . $f['fac_opca'];
                                                                    $req = $Conn->query($sql);
                                                                    $d_opca=$req->fetch();
                                                                }


                                                                foreach($relance_nom as $k=>$ren){
                                                                    if($f['fac_etat_relance'] == $ren['ret_id']){
                                                                        $code_relance = $ren['ret_code'];
                                                                    }
                                                                }
                                                                $reste_du=$f['fac_total_ttc']-$f['fac_regle'];

                                                                $total_ht+=$f['fac_total_ht'];
                                                                $total_ttc+=$f['fac_total_ttc'];
                                                                $total_regle+=$f['fac_regle'];
                                                                $total_reste_du+=$reste_du;




                                                                $style_tr="";
                                                                $style_a="";
                                                                $id_tr="";
                                                                if($f['fac_nature'] == 1){
                                                                    $id_tr="id='tab_ligne_" . $nbFac . "'";

                                                                    if($f['fac_relance_stop']){
                                                                        $style_tr="style='color:orange;'";
                                                                    }elseif($facture_opca){
                                                                        $style_tr="style='color:purple;'";
                                                                    }
                                                                    if($etape > 0 AND $etat_relance == $etape){
                                                                        $class_tr="class='ligne_fac_select'";
                                                                    }else{
                                                                        $class_tr="class='ligne_fac'";
                                                                    }
                                                                }

                                                                ?>
                                                                <tr  <?=$id_tr?> <?=$style_tr?> >
                                                                    <td>
                                                                    <?php if($f['fac_nature'] == 1){ ?>

                                                                        <input type="checkbox"  name="ligne_fac_<?=$nbFac?>"  disabled >
                                                                        <input type="hidden" id="fac_societe_<?=$nbFac?>" name="fac_societe_<?=$nbFac?>" value="<?=$f['fac_numero']?>" />
                                                                        <input type="hidden" name="fac_id_<?=$nbFac?>" value="<?=$facture?>" />
                                                                        <input type="hidden" id="fac_numero_<?=$nbFac?>" name="fac_numero_<?=$nbFac?>" value="<?=$f['fac_numero']?>" />
                                                                    <?php }?>
                                                                    </td>
                                                                    <td>

                                                                            <?=$f['fac_numero']?>

                                                                    </td>

                                                                    <?php if($d_client['cli_groupe'] == 1 && !empty($d_client['cli_filiale_de'])){ ?>

                                                                        <td><?= $f['cli_code']?></td>

                                                                    <?php } ?>


                                                                    <td><?=convert_date_txt($f['fac_date'])?></td>
                                                                    <td>

                                                                        <?= $fac_date_reg_prev ?>
                                                                    </td>
                                                                    <td><?=$fac_date_reg_rel?></td>
                                                                    <td class="text-right"><?= $retard ?> jours</td>
                                                                    <td class="text-right"><?= number_format($f['fac_total_ttc'], 2, ',', ' ') ?> €</td>
                                                                    <td class="text-right">

                                                        <?php
                                                                            echo(number_format($f['fac_regle'], 2, ',', ' '));
                                                                         ?>
                                                                    </td>
                                                                    <td class="text-right">

                                                    <?php
                                                                        echo(number_format($reste_du, 2, ',', ' '));
                                                                    ?>
                                                                    </td>

                                                                    <td class="text-center" >
                                                                        <?php if(!empty($f['fac_etat_relance'])){ ?>

                                                                                <?= $code_relance ?>

                                                                        <?php }else{?>
                                                                        <a href="#">Pas d'état de relance</a>
                                                                        <?php }?>
                                                                    </td>

                                                                </tr>
                                        <?php                   }
                                                            }
                        ?>
                                                        </tbody>
                                                        <tfoot>

                                                            <tr>
                                                                <?php
                                                                if($d_client['cli_groupe'] == 1 && !empty($d_client['cli_filiale_de'])){ ?>
                                                                    <th colspan="7" class="text-right" >Total :</th>
                                                            <?php }else{ ?>
                                                                    <th colspan="6" class="text-right" >Total :</th>
                                                            <?php }?>
                                                                <td class="text-right" ><?=number_format($total_ttc, 2, ',', ' ');?> €</td>
                                                                <td class="text-right" ><?=number_format($total_regle, 2, ',', ' ');?> €</td>
                                                                <td class="text-right" ><?=number_format($total_reste_du, 2, ',', ' ');?> €</td>
                                                                <td colspan="3" class="text-center" >&nbsp;</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                                <?php }else{ ?>
                                                    <?php if($d_relance['rel_envoie']){ ?>
                                                        <div class="alert alert-danger">
                                                            Un mail a été envoyé au client
                                                        </div>
                                                    <?php } ?>
                                                    <?php if($d_relance['rel_courrier']){ ?>
                                                        <div class="alert alert-danger">
                                                            Le recommandé a été imprimé.
                                                        </div>
                                                    <?php }?>
                                                    <p style="font-weight: bold" class="mt15">
                                                        Les factures associées à cette relance ne peuvent plus être modifiées
                                                    </p>
                                                <?php }?>
                                                <input type="hidden" value="<?=$nbFac?>" name="nbFac" id="nbFac" />
                                                    <div class="table-responsive mt15">
                                                        <table class="table table-striped table-hover" >
                                                            <thead>
                                                                <tr class="dark2" >
                                                                    <th>Utilisateur</th>
                                                                    <th>Contact</th>
                                                                    <th>Commentaires</th>
                                                                    <th>Rappeler</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                    <td>
                                                                        <div class="row">
                                                                            <div class="col-md-12"><?= $_SESSION['acces']['acc_prenom'] ?> <?= $_SESSION['acces']['acc_nom'] ?></div>
                                                                            <div class="col-md-12"><input type="text" name="rel_date" id="rel_date" class="form-control datepicker date" placeholder="Date" value="<?= convert_date_txt($d_relance['rel_date']) ?>"></div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                    <div class="cont-contact">
                                                                            <div class="row cont-contact-liste" >
                                                                                <div class="col-md-12" >
                                                                                    <div class="section" >
                                                                                        <select name="rel_contact_lib" id="contact" class="select2 contact" >
                                                                                            <option value="0" >Selectionnez un contact client</option>
                                                                                <?php		if(!empty($contacts)){
                                                                                                foreach($contacts as $c){
                                                                                                    if(!empty($d_relance) && $c['con_id'] == $d_relance['rel_contact']){
                                                                                                        echo("<option selected value='" . $c["con_id"] . "' >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
                                                                                                    }else{
                                                                                                        echo("<option value='" . $c["con_id"] . "' >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
                                                                                                    }

                                                                                                }
                                                                                            }?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="hidden" value="0" name="rel_contact" id="rel_contact"
                                                                                <?php if(!empty($d_relance)){ ?>
                                                                                value="<?= $d_relance['rel_contact'] ?>"
                                                                                <?php } ?>
                                                                                />
                                                                            </div>
                                                                            <div class="row mt5">
                                                                                <div class="col-md-6" >
                                                                                    <input type="text" name="rel_con_nom" id="con_nom" class="form-control nom champ-contact" placeholder="Nom"
                                                                                    <?php if(!empty($d_relance)){ ?>
                                                                                    value="<?= $d_relance['rel_con_nom'] ?>"
                                                                                    <?php } ?>
                                                                                    />
                                                                                </div>
                                                                                <div class="col-md-6" >
                                                                                    <input type="text" name="rel_con_prenom" id="con_prenom" class="form-control prenom champ-contact" placeholder="Prénom"
                                                                                    <?php if(!empty($d_relance)){ ?>
                                                                                    value="<?= $d_relance['rel_con_prenom'] ?>"
                                                                                    <?php } ?>
                                                                                    />
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6 mt5" >
                                                                                    <input type="text" name="rel_con_tel" id="con_tel" class="form-control telephone champ-contact" placeholder="Téléphone"
                                                                                    <?php if(!empty($d_relance)){ ?>
                                                                                    value="<?= $d_relance['rel_con_tel'] ?>"
                                                                                    <?php }?>
                                                                                    />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="row">

                                                                                <div class="col-md-12">
                                                                                Etat relance : <?=$ret_libelle?>
                                                                                </div>
                                                                            <div class="col-md-12">
                                                                                <input type="text" class="form-control form-control-sm" name="rel_titre" id="rel_titre" size="40" maxlength="50"
                                                                                <?php if(!empty($d_relance)){ ?>
                                                                                value="<?=$d_relance['rel_titre']?>"
                                                                                <?php }?>

                                                                                >
                                                                            </div>
                                                                            <div class="col-md-12 mt5">
                                                                                <textarea class="form-control" name="rel_comment" cols="60" rows="4" ><?php if(!empty($d_relance)){ ?><?=$d_relance['rel_comment']?><?php }?></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <td>
                                                                            <input id="rel_date_rappel" type="text" name="rel_date_rappel" class="form-control datetimepicker" placeholder="Date"
                                                                            <?php if(!empty($d_relance)){
                                                                                $date = new Datetime($d_relance['rel_date_rappel']);
                                                                                ?>
                                                                                value="<?=$date->format("d/m/Y H:i") ?>"
                                                                            <?php }?>
                                                                            >
                                                                        </td>
                                                                    </td>
                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                      </div>

                                  </div>
                              </div>


                          </div>
                      </div>

                  </div>
              </div>

          </div>
      </section>
      <!-- End: Content -->
  </section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="<?= $_SESSION['retourFacture'] ?>" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="button" class="btn btn-danger btn-sm" onClick="supprimer('facture_relance_supp.php?relance=<?=$relance?>&origine=<?=$origine?>');">
                <i class='fa fa-times'></i> Supprimer
            </button>
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-save'></i> Enregistrer
            </button>
        </div>
    </div>
</footer>
</form>
<?php
	include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="vendor/plugins/moment/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/fr.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        var client="<?=$d_client["cli_id"]?>";
            var tab_contact=new Array(3);
            tab_contact[0]=new Array(3);
			<?php	if(!empty($contacts)){
						foreach($contacts as $con){ ?>
							tab_contact[<?=$con["con_id"]?>]=new Array(3);
							tab_contact[<?=$con["con_id"]?>]["nom"]="<?=$con["con_nom"]?>";
							tab_contact[<?=$con["con_id"]?>]["prenom"]="<?=$con["con_prenom"]?>";
							tab_contact[<?=$con["con_id"]?>]["tel"]="<?=$con["con_tel"]?>";
							tab_contact[<?=$con["con_id"]?>]["portable"]="<?=$con["con_portable"]?>";
			<?php		}
					} ?>
            $(document).ready(function () {
                $('.dataTable').DataTable({
                    "language": {
                    "url": "vendor/plugins/DataTables/media/js/French.json"
                    },
                    "paging": false,
                    "searching": false,
                    "info": false,
                    "columnDefs": [ {
                        "targets": 'no-sort',
                        "orderable": false,
                    }]
                });
                // INTERACTION CONTACT
				$(".datetimepicker").datetimepicker({
                    locale: 'fr'
                });
				$(".delcontact").click(function(){
					contact=$(this).data("contact");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer ce contact?",confirmer_contact_supp,contact);
                });
                $("#contact").change(function(){
                    afficher_contact($(this).val());
                });
            });
            function supprimer(adresse)
            {   if(confirm(" La suppression sera définitive \n \n pour confirmer cliquez sur OK \n sinon \n cliquez sur Annuler"))
                    document.location=adresse;
            }
            function afficher_contact(contact){
                if(contact>0){
                    $("#con_nom").val(tab_contact[contact]["nom"]);
                    $("#con_prenom").val(tab_contact[contact]["prenom"]);
                    $("#con_tel").val(tab_contact[contact]["tel"]);
                    $("#con_portable").val(tab_contact[contact]["portable"]);
                }else{
                    $(".champ-contact").val("");
                }
            }

            function confirmer_contact_supp(contact){
				del_client_contact(contact,client,supprimer_contact_client,contact);
            }
            // modif de l'affichage suite à supp
			function supprimer_contact_client(json){
				if(json.contact){
					$("#contact_" + json.contact).remove();
				}
				if($(".contact").length==0){
					$("#tab_contact").remove();
					$("#no_contact").show();
				}
            }
            var nbFac=0;
			var tab_lien=new Array();
			var rel_etat_relance=0;
			var min_relance=0;
			var etat_relance=0;
			var num_relance=0;
			var valide_select=false;
			var fac_numero="";

			var tab_relance=new Array();

			var tab_relance_nom=new Array();
		<?php for ($i=0; $i < $nbEtat; $i++) { ?>

				tab_relance[<?=$relance_nom[$i]['ret_id']?>]=new Array(2);
				tab_relance[<?=$relance_nom[$i]['ret_id']?>][0]=parseInt("<?=$i?>");
				tab_relance[<?=$relance_nom[$i]['ret_id']?>][1]="<?=$relance_nom[$i]['ret_libelle']?>";
				tab_relance[<?=$relance_nom[$i]['ret_id']?>][2]=parseInt("<?=$relance_nom[$i]['ret_traitement']?>");

				tab_relance_nom[<?=$i?>]="<?=$relance_nom[$i]['ret_libelle']?>";
        <?php }?>


			function controleFac(num_fac){
				valide_select=true;
				rel_etat_relance=document.getElementById("rel_etat_relance").value;
				if(rel_etat_relance>0){
					num_relance=tab_relance[rel_etat_relance][0];
					min_relance=document.getElementById("min_relance_" + num_fac).value;
					etat_relance=document.getElementById("etat_relance_" + num_fac).value;
                    fac_numero=document.getElementById("fac_numero_" + num_fac).value;

					if(num_relance<min_relance){
						alert("L'opération '" + tab_relance[rel_etat_relance][1] + "' n'est plus disponible pour la facture " + fac_numero);
						valide_select=false;
					}else if(num_relance>etat_relance){
						alert("La facture " + fac_numero + " n'en n'est pas encore au stade '" + tab_relance[rel_etat_relance][1] + "'");
						valide_select=false;
					};
				}
				if(valide_select){
					selectFac(num_fac);
				}else{
					deselectFac(num_fac);
				};

			}

			function selectFac(num_fac){
				document.getElementById("ligne_fac_" + num_fac).checked=true;
				document.getElementById("tab_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				document.getElementById("numero_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				if(document.getElementById("date_ligne_" + num_fac)){
					document.getElementById("date_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
				if(document.getElementById("regle_ligne_" + num_fac)){
					document.getElementById("regle_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
				if(document.getElementById("du_ligne_" + num_fac)){
					document.getElementById("du_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
				document.getElementById("etat_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				if(document.getElementById("opca_ligne_" + num_fac)){
					document.getElementById("opca_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
			}

			function deselectFac(num_fac){
				document.getElementById("ligne_fac_" + num_fac).checked=false;
				document.getElementById("tab_ligne_" + num_fac).removeAttribute("class");
				document.getElementById("numero_ligne_" + num_fac).setAttribute("class","ligne_fac");
				if(document.getElementById("date_ligne_" + num_fac)){
					document.getElementById("date_ligne_" + num_fac).setAttribute("class","ligne_fac");
				};
				if(document.getElementById("regle_ligne_" + num_fac)){
					document.getElementById("regle_ligne_" + num_fac).setAttribute("class","ligne_fac");
				};
				if(document.getElementById("du_ligne_" + num_fac)){
					document.getElementById("du_ligne_" + num_fac).setAttribute("class","ligne_fac");
				}
				document.getElementById("etat_ligne_" + num_fac).setAttribute("class","ligne_fac");
				if(document.getElementById("opca_ligne_" + num_fac)){
					document.getElementById("opca_ligne_" + num_fac).setAttribute("class","ligne_fac");
				}
			}


			/* function selectMultiFac(elt,soc){
				nbFac=document.getElementById("nbFac").value;
				for(i=1;i<=nbFac;i++){
					if(document.getElementById("ligne_fac_" + i)){
						if(elt.checked){
							if(document.getElementById("fac_societe_" + i).value==soc){
								controleFac(i);
							};
						}else{
							if(document.getElementById("fac_societe_" + i).value==soc){
								deselectFac(i);
							}
						};
					};
				};
			} */
			function choixFac(num_fac){
				if(document.getElementById("ligne_fac_" + num_fac).checked){
					controleFac(num_fac);
				}else{
					deselectFac(num_fac);
				};
			}
			function actuFacture(){
				rel_etat_relance=document.getElementById("rel_etat_relance").value;
				num_relance=tab_relance[rel_etat_relance][0];
                nbFac=document.getElementById("nbFac").value;
				for(i=1;i<=nbFac;i++){
                    alert(document.getElementById("etat_relance_" + i).value);
					if((document.getElementById("etat_relance_" + i).value==num_relance)&&(num_relance>0)){
                        selectFac(i);

					}else{
						deselectFac(i);
					};
				};
			}
            var date_jour="<?=date("Y-m-d")?>"
            <?php
                $demain = new DateTime();
                $demain->modify('+1 day');
            ?>
			var date_demain="<?= $demain->format('Y-m-d') ?>"
			function choixEtape(){
				rel_etat_relance=document.getElementById("rel_etat_relance").value;
				if(tab_relance[rel_etat_relance][2]==1){
					document.getElementById("rel_date").value=date_jour;
					document.getElementById("rel_date_rappel").value=date_demain;
					document.getElementById("rel_date").setAttribute("readonly","readonly");
					document.getElementById("rel_date_rappel").setAttribute("readonly","readonly");
				}else{
					document.getElementById("rel_date_rappel").value="";
					document.getElementById("rel_date").removeAttribute("readonly");
					document.getElementById("rel_date_rappel").removeAttribute("readonly");
				};

				nbFac=document.getElementById("nbFac").value;
				if(rel_etat_relance>0){
					document.getElementById("rel_titre").value=tab_relance[rel_etat_relance][1];
					for(i=1;i<=nbFac;i++){
						if(document.getElementById("ligne_fac_" + i).checked){
							controleFac(i);
						};
					};
				}else{
					num_relance=0;
					for(i=1;i<=nbFac;i++){
						if((document.getElementById("num_relance_" + i).value<num_relance)||(num_relance==0)){
							num_relance=document.getElementById("num_relance_" + i).value;
						};
					};
					document.getElementById("rel_titre").value=tab_relance_nom[num_relance];
				}


			}
    </script>
</body>
</html>
