<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';


$req=$Conn->query("SELECT * FROM evacuations_parametres WHERE epa_id = 1");
$param=$req->fetch();

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Chrono</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
<form action="evac_param_alerte_enr.php" method="POST" id="admin-form">
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">

            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-left">

                                        <div class="content-header">
                                            <h2>Modifier les paramètres <b class="text-primary">des rapports</b></h2>

                                        </div>

                                        <div class="col-md-10 col-md-offset-1">


                                                <?php if (isset($_GET['id'])): ?>
                                                    <input type="hidden" name="ecp_id" value="<?=$chrono['ecp_id']?>"></input>
                                                <?php endif;?>
                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <div class="section">
                                                      <label>Longueur minimum de la description du thème (caractères) : </label>
                                                      <div class="field prepend-icon">
                                                        <input type="number" name="epa_theme_alert" class="gui-input" id="epa_theme_alert" placeholder="Longueur minimum de la description du thème"
                                                            value="<?=$param['epa_theme_alert']?>">
                                                        <label for="epa_theme_alert" class="field-icon">
                                                          <i class="fa fa-cog"></i>
                                                      </label>
                                                  </div>
                                              </div>
                                            </div>
                                              <div class="col-md-6">
                                                    <div class="section">
                                                      <label>Utilisation minimum de la chrono (en %) : </label>
                                                      <div class="field prepend-icon">
                                                        <input type="number" name="epa_chrono_alert" class="gui-input" id="epa_chrono_alert" placeholder="Utilisation minimun de la chrono (en %)"
                                                            value="<?=$param['epa_chrono_alert']?>">
                                                        <label for="epa_chrono_alert" class="field-icon">
                                                          <i class="fa fa-cog"></i>
                                                      </label>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                                    <div class="section">
                                                      <label>Nombre de remarques minimum : </label>
                                                      <div class="field prepend-icon">
                                                        <input type="number" name="epa_remarque_alert" class="gui-input" id="epa_remarque_alert" placeholder="Nombre de remarques minimum"
                                                            value="<?=$param['epa_remarque_alert']?>">
                                                        <label for="epa_remarque_alert" class="field-icon">
                                                          <i class="fa fa-cog"></i>
                                                      </label>
                                                  </div>
                                              </div>
                                              </div>
                                            <div class="col-md-6">
                                                    <div class="section">
                                                      <label>Longueur minimum de la conclusion (caractères) : </label>
                                                      <div class="field prepend-icon">
                                                        <input type="number" name="epa_conclusion_alert" class="gui-input" id="epa_conclusion_alert" placeholder="Longueur minimum de la conclusion"
                                                            value="<?=$param['epa_conclusion_alert']?>">
                                                        <label for="epa_conclusion_alert" class="field-icon">
                                                          <i class="fa fa-cog"></i>
                                                      </label>
                                                  </div>
                                              </div>
                                      </div>


                              </div>
                      </div>

                  </div>
              </div>

          </div>
      </section>
      <!-- End: Content -->
  </section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="parametre.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-save'></i> Enregistrer
            </button>
        </div>
    </div>
</footer>
</form>
<?php
	include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>

</body>
</html>
