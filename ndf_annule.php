 <?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');

//////////////////////////////////////////////////////////////
///////////// SI C'EST LE DEMANDEUR QUI VALIDE ///////////////
//////////////////////////////////////////////////////////////

if(isset($_GET['id'])){


	// changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("UPDATE ndf SET ndf_statut = 8 WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();

        $_SESSION['message'][] = array(
		"titre" => "Note de frais",
		"type" => "success",
		"message" => "La note de frais a été annulée."
	);
	// fin changer le statut de la ndf et mettre à jour la date de validation

        Header("Location: ndf.php?id=" . $_GET['id']);
        die();
}
