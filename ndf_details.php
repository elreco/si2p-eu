<?php
////////////////// MENU ACTIF HEADER ///////////////////

////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_tva.php');
// session retour
if(isset($_GET['menu_retour'])){
	unset($_SESSION['retour']);
}





///////////////////// TRAITEMENTS SERVEUR ////////////////////
// chercher les societes et agences
	$req = $Conn->prepare("SELECT soc_id, soc_nom FROM societes WHERE soc_archive != 1");
	$req->execute();
	$societes = $req->fetchAll();
	if(!empty($_POST['ndf_societe'])){
		$req = $Conn->prepare("SELECT age_id, age_nom FROM agences WHERE age_archive != 1 AND age_societe = " . $_POST['ndf_societe']);
		$req->execute();
		$agences = $req->fetchAll();
	}

// fin chercher les societes et agences

	/////////////////// RECHERCHER /////////////////////////

	if(!empty($_POST)){

		// si c'est par ligne
		if($_POST['consolidation'] == 1){

			$critere=array();
		    $mil="";

		    if(!empty($_POST['ndf_societe'])){
		        $mil.=" AND ndf_societe = :ndf_societe";
		        $critere["ndf_societe"]= $_POST['ndf_societe'];
		    };
		    if(!empty($_POST['ndf_agence'])){
		        $mil.=" AND ndf_agence = :ndf_agence";
		        $critere["ndf_agence"]= $_POST['ndf_agence'];
		    };
		    if(!empty($_POST['ndf_utilisateur'])){
        		$mil.=" AND ndf_utilisateur = :ndf_utilisateur";
        		$critere["ndf_utilisateur"]= $_POST['ndf_utilisateur'];
		    };
		    if(!empty($_POST['ndf_statut'])){
		        $mil.=" AND ndf_statut = :ndf_statut";
		        $critere["ndf_statut"]= $_POST['ndf_statut'];
		    };
		    if(!empty($_POST['ndf_mois_de'])){
		        $mil.=" AND ndf_mois >= :ndf_mois_de";
		        $critere["ndf_mois_de"]= $_POST['ndf_mois_de'];
		    };
		    if(!empty($_POST['ndf_annee_de'])){
		        $mil.=" AND ndf_annee >= :ndf_annee_de";
		        $critere["ndf_annee_de"]= $_POST['ndf_annee_de'];
		    };
		    if(!empty($_POST['ndf_mois_a'])){
		        $mil.=" AND ndf_mois <= :ndf_mois_a";
		        $critere["ndf_mois_a"]= $_POST['ndf_mois_a'];
		    };
		    if(!empty($_POST['ndf_annee_a'])){
		        $mil.=" AND ndf_annee <= :ndf_annee_a";
		        $critere["ndf_annee_a"]= $_POST['ndf_annee_a'];
		    };


		  	$sql="SELECT * FROM ndf_lignes
		  	LEFT JOIN ndf ON ndf_lignes.nli_ndf = ndf.ndf_id
		  	LEFT JOIN utilisateurs ON utilisateurs.uti_id = ndf.ndf_utilisateur
		  	LEFT JOIN ndf_categories ON ndf_categories.nca_id = ndf_lignes.nli_categorie
		  	LEFT JOIN societes ON utilisateurs.uti_societe = societes.soc_id
		  	LEFT JOIN agences ON agences.age_id = utilisateurs.uti_agence";

		    if($mil!=""){
		      $sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		    }
			$sql.=" ORDER BY  uti_nom, uti_prenom LIMIT 8000";
			$req = $Conn->prepare($sql);

			if(!empty($critere)){
				foreach($critere as $c => $v){
					$req->bindValue($c,$v);
				}
			};


			$req->execute();
			$notes = $req->fetchAll();

		}

		// si c'est par ligne
		if($_POST['consolidation'] == 2){
			// récupérer les utilisateurs
			$req = $Conn->prepare("SELECT * FROM utilisateurs");

			$req->execute();
			$utilisateurs = $req->fetchAll();



		  	foreach($utilisateurs as $u){
				$critere=array();
				$mil="";

				if(!empty($_POST['ndf_societe'])){
					$mil.=" AND ndf_societe = :ndf_societe";
					$critere["ndf_societe"]= $_POST['ndf_societe'];
				};
				if(!empty($_POST['ndf_agence'])){
					$mil.=" AND ndf_agence = :ndf_agence";
					$critere["ndf_agence"]= $_POST['ndf_agence'];
				};

				if(!empty($_POST['ndf_statut'])){
					$mil.=" AND ndf_statut = :ndf_statut";
					$critere["ndf_statut"]= $_POST['ndf_statut'];
				};
				if(!empty($_POST['ndf_mois_de'])){
					$mil.=" AND ndf_mois >= :ndf_mois_de";
					$critere["ndf_mois_de"]= $_POST['ndf_mois_de'];
				};
				if(!empty($_POST['ndf_annee_de'])){
					$mil.=" AND ndf_annee >= :ndf_annee_de";
					$critere["ndf_annee_de"]= $_POST['ndf_annee_de'];
				};
				if(!empty($_POST['ndf_mois_a'])){
					$mil.=" AND ndf_mois <= :ndf_mois_a";
					$critere["ndf_mois_a"]= $_POST['ndf_mois_a'];
				};
				if(!empty($_POST['ndf_annee_a'])){
					$mil.=" AND ndf_annee <= :ndf_annee_a";
					$critere["ndf_annee_a"]= $_POST['ndf_annee_a'];
				};

				if(!empty($_POST['ndf_utilisateur'])){
					$mil.=" AND ndf_utilisateur = :ndf_utilisateur";
					$critere["ndf_utilisateur"]= $_POST['ndf_utilisateur'];

					$sql="SELECT SUM(ndf_ttc), SUM(nli_qte),SUM(nli_pris_en_charge), SUM(nli_depassement_accorde), SUM(nli_depassement_valide), ndf_annee, ndf_mois,ndf_statut, ndf_utilisateur, ndf_agence, ndf_societe FROM ndf
					LEFT JOIN ndf_lignes ON ndf_lignes.nli_ndf = ndf.ndf_id";

					if($mil!=""){
					$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
					}
					$sql.=" GROUP BY nli_ttc, nli_qte, nli_pris_en_charge, nli_depassement_accorde, nli_depassement_valide, ndf_annee, ndf_mois,ndf_statut, ndf_utilisateur, ndf_agence, ndf_societe LIMIT 8000;";
					$req = $Conn->prepare($sql);

					if(!empty($critere)){
						foreach($critere as $c => $v){
							$req->bindValue($c,$v);
						}
					};
					$req->execute();
					$notes[$_POST['ndf_utilisateur']] = $req->fetch();
				} else {
					$mil.=" AND ndf_utilisateur = " . $u['uti_id'];
					$sql="SELECT SUM(ndf_ttc), SUM(nli_qte),SUM(nli_pris_en_charge), SUM(nli_depassement_accorde), SUM(nli_depassement_valide), ndf_annee, ndf_mois,ndf_statut, ndf_utilisateur, ndf_agence, ndf_societe FROM ndf
					LEFT JOIN ndf_lignes ON ndf_lignes.nli_ndf = ndf.ndf_id";

					if($mil!=""){
					$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
					}
					$sql.=" GROUP BY nli_ttc, nli_qte, nli_pris_en_charge, nli_depassement_accorde, nli_depassement_valide, ndf_annee, ndf_mois,ndf_statut, ndf_utilisateur, ndf_agence, ndf_societe LIMIT 8000;";
					$req = $Conn->prepare($sql);

					if(!empty($critere)){
						foreach($critere as $c => $v){
							$req->bindValue($c,$v);
						}
					};
					$req->execute();
					$notes[$u['uti_id']] = $req->fetch();
				}

        	}




		}

		// si c'est par ligne
		if($_POST['consolidation'] == 3){
			// récupérer les categories
			$req = $Conn->prepare("SELECT * FROM ndf_categories");
			$req->execute();
			$categories = $req->fetchAll();

		  	foreach($categories as $cat){
				$critere=array();
				$mil="";

				if(!empty($_POST['ndf_societe'])){
					$mil.=" AND ndf_societe = :ndf_societe";
					$critere["ndf_societe"]= $_POST['ndf_societe'];
				};
				if(!empty($_POST['ndf_agence'])){
					$mil.=" AND ndf_agence = :ndf_agence";
					$critere["ndf_agence"]= $_POST['ndf_agence'];
				};
				if(!empty($_POST['ndf_utilisateur'])){
					$mil.=" AND ndf_utilisateur = :ndf_utilisateur";
					$critere["ndf_utilisateur"]= $_POST['ndf_utilisateur'];
				};
				if(!empty($_POST['ndf_statut'])){
					$mil.=" AND ndf_statut = :ndf_statut";
					$critere["ndf_statut"]= $_POST['ndf_statut'];
				};
				if(!empty($_POST['ndf_mois_de'])){
					$mil.=" AND ndf_mois >= :ndf_mois_de";
					$critere["ndf_mois_de"]= $_POST['ndf_mois_de'];
				};
				if(!empty($_POST['ndf_annee_de'])){
					$mil.=" AND ndf_annee >= :ndf_annee_de";
					$critere["ndf_annee_de"]= $_POST['ndf_annee_de'];
				};
				if(!empty($_POST['ndf_mois_a'])){
					$mil.=" AND ndf_mois <= :ndf_mois_a";
					$critere["ndf_mois_a"]= $_POST['ndf_mois_a'];
				};
				if(!empty($_POST['ndf_annee_a'])){
					$mil.=" AND ndf_annee <= :ndf_annee_a";
					$critere["ndf_annee_a"]= $_POST['ndf_annee_a'];
				};
		  		$mil.=" AND nli_categorie = " . $cat['nca_id'];

			  	$sql="SELECT SUM(ndf_ttc), SUM(nli_qte),SUM(nli_pris_en_charge), SUM(nli_depassement_accorde), SUM(nli_depassement_valide), ndf_annee, ndf_mois,ndf_statut, ndf_utilisateur, ndf_agence, ndf_societe FROM ndf
			  	LEFT JOIN ndf_lignes ON ndf_lignes.nli_ndf = ndf.ndf_id";

			    if($mil!=""){
			      $sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
			    }
				$sql.=" GROUP BY nli_ttc, nli_qte, nli_pris_en_charge, nli_depassement_accorde, nli_depassement_valide, ndf_annee, ndf_mois,ndf_statut, ndf_utilisateur, ndf_agence, ndf_societe LIMIT 8000;";
				$req = $Conn->prepare($sql);

				if(!empty($critere)){
					foreach($critere as $c => $v){
						$req->bindValue($c,$v);
					}
				};

				$req->execute();

				$notes[$cat['nca_id']] = $req->fetch();

        	}

		}

	}
////////////////////FIN TRAITEMENTS SERVEUR /////////////////

///////////////////// TRAITEMENTS PHP ///////////////////////

// récupérer tous les mois dont j'ai besoin
$mois = array(

	1 => "Janvier",
	2 => "Février",
	3 => "Mars",
	4 => "Avril",
	5 => "Mai",
	6 => "Juin",
	7 => "Juillet",
	8 => "Août",
	9 => "Septembre",
	10 => "Octobre",
	11 => "Novembre",
	12 => "Décembre"


);

$statuts = array(
	0 => "En cours de saisie",
	1 => "En cours de validation",
	2 => "Validée RA",
	3 => "Validée RE",
	4 => "Validée Compta",
	5 => "Validée RH",
	6 => "En cours de paiement",
	7 => "Payée",
	8 => "Annulée"

);


$annees = array(
	1 => date('Y', strtotime(' -5 years')),
	2 => date('Y', strtotime(' -4 years')),
	3 => date('Y', strtotime(' -3 years')),
	4 => date('Y', strtotime(' -2 years')),
	5 => date('Y', strtotime(' -1 years')),
	6 => date('Y'),

);

////////////////////FIN TRAITEMENTS PHP//////////////////////
?>
<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>SI2P - Orion - Notes de frais</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<style type="text/css">
.panel-tabs > li > a {
    color: #AAA;
    font-size: 14px;
    letter-spacing: 0.2px;
    line-height: 30px;
    /*padding: 9px 20px 11px;*/
    border-radius: 0;
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;
}
.nav2 > li > a {
    position: relative;
    display: block;
    /* padding: 10px 15px; */
}
</style>
<body class="sb-top sb-top-sm">


		<!-- Start: Main -->
		<div id="main">

			<?php include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<section id="content" class="animated fadeIn pr20 pl20">
					<h1>Détails des notes de frais</h1>
					<div class="row">
						<div class="col-md-12">
							<div class="panel mb10">
	                            <div class="panel-heading panel-head-sm">
	                                <span class="panel-icon">
	                                    <i class="fa fa-cogs" aria-hidden="true"></i>
	                                </span>
	                                <span class="panel-title"> Filtrer</span>
	                            </div>
	                            <div class="panel-body p20">
	                                <div class="row admin-forms">
	                                	<form action="ndf_details.php" method="POST">
	                                		<div class="row">
												<div class="col-md-3">
			                                		<p class="text-center mb20" style="font-weight:bold;">Société :</p>
													<select id="societe" name="ndf_societe" class="select2 select2-societe" >
														<option value="0">Sélectionner une société...</option>
														<?php foreach($societes as $s){ ?>
														<option value="<?= $s['soc_id'] ?>"
														<?php if(!empty($_POST['ndf_societe'])){
															if($_POST['ndf_societe'] == $s['soc_id']){

															?>
																selected
														<?php }
														}?>

														><?= $s['soc_nom'] ?></option>
														<?php } ?>
													</select>

												</div>
												<div class="col-md-3">
													<p class="text-center mb20" style="font-weight:bold;">Agence :</p>
													<select id="agence" name="ndf_agence" class="select2 select2-societe-agence">
														<option value="0">Sélectionner une agence...</option>
														<?php
														if(!empty($agences)){
														foreach($agences as $a){ ?>
															<option value="<?= $a['age_id'] ?>"

															<?php if(!empty($_POST['ndf_agence']) && $_POST['ndf_agence'] == $a['age_id']){ ?>
																selected
															<?php } ?>
															><?= $a['age_nom'] ?></option>
														<?php }
														} ?>
													</select>

												</div>
												<div class="col-md-3">
													<p class="text-center mb20" style="font-weight:bold;">Utilisateur :</p>
													<select id="utilisateur" name="ndf_utilisateur" class="select2 select2-utilisateur-societe-agence">
														<?php if(!empty($_POST['ndf_utilisateur'])){
															$req = $Conn->prepare("SELECT uti_prenom, uti_nom FROM utilisateurs WHERE uti_id = " . $_POST['ndf_utilisateur']);
															$req->execute();
															$uti = $req->fetch();

														?>
														<option value="<?= $_POST['ndf_utilisateur'] ?>"><?= $uti['uti_prenom'] ?> <?= $uti['uti_nom'] ?></option>
													<?php }?>
													</select>

												</div>

												<div class="col-md-3">
													<p class="text-center mb20" style="font-weight:bold;">Statut :</p>
													<select id="statut" name="ndf_statut" class="select2">
													<option value="0"></option>
														<?php foreach($statuts as $k => $s){ ?>
															<option value="<?= $k ?>"
														<?php
														if(!empty($_POST['ndf_statut'])){
															if($s == $_POST['ndf_statut']){ ?>
														}
														selected
														<?php }

														}?>
															><?= $s ?></option>
														<?php }?>
													</select>

												</div>
											</div>
											<div class="row">
												<div class="col-md-3">
													<p class="text-center mb20 mt20" style="font-weight:bold;">De :</p>
													<select id="statut" name="ndf_mois_de" class="select2">

														<?php foreach($mois as $k => $m){ ?>
															<option value="<?= $k  ?>"
															<?php
															if(!empty($_POST['ndf_mois_de'])){
																if($_POST['ndf_mois_de'] == $k){ ?>
																	selected
															<?php }
															}?>
															><?= $m ?></option>
														<?php } ?>
													</select>

												</div>
												<div class="col-md-3">
													<p class="text-center mb20 mt20" style="font-weight:bold;">&nbsp;</p>
													<select id="statut" name="ndf_annee_de" class="select2">


															<?php foreach($annees as $a){

																?>
																<option value="<?= $a ?>"
																<?php
																	if(!empty($_POST['ndf_annee_de'])){
																		if($_POST['ndf_annee_de'] == $a){ ?>
																			selected
																	<?php }
																}?>
																><?= $a ?></option>
															<?php

															} ?>
													</select>

												</div>
												<div class="col-md-3">
													<p class="text-center mb20 mt20" style="font-weight:bold;">A :</p>
													<select id="statut" name="ndf_mois_a" class="select2">

														<?php foreach($mois as $k => $m){ ?>
															<option value="<?= $k  ?>"
																<?php
																	if(!empty($_POST['ndf_mois_a'])){
																		if($_POST['ndf_mois_a'] == $k){ ?>
																			selected
																	<?php }
																}?>
															><?= $m ?></option>
														<?php } ?>
													</select>

												</div>


												<div class="col-md-3">
													<p class="text-center mb20 mt20" style="font-weight:bold;">&nbsp;</p>
													<select id="statut" name="ndf_annee_a" class="select2">


															<?php foreach($annees as $a){

																?>
																<option value="<?= $a ?>"
																<?php
																	if(!empty($_POST['ndf_annee_a'])){
																		if($_POST['ndf_annee_a'] == $a){ ?>
																			selected
																	<?php }
																}?>
																><?= $a ?></option>
															<?php

															} ?>
													</select>

												</div>
											</div>
											<div class="row mt20">
												<div class="col-md-3">
													<p class="text-center mb20 mt20" style="font-weight:bold;">Consolidation : </p>
												</div>
												<div class="col-md-2 mt20">
													<div class="radio-custom">
													    <input type="radio" id="consolidation1" name="consolidation"
													    <?php if(!empty($_POST['consolidation'])){
													    	if($_POST['consolidation'] == 1){
													    	?>
													    	checked
													    <?php
													    	}
													    }else{ ?>
													    	checked
													    <?php } ?>
													    value="1">
													    <label for="consolidation1">Par ligne</label>
													</div>
												</div>
												<div class="col-md-2 mt20">
													<div class="radio-custom">
													    <input type="radio" id="consolidation2" name="consolidation"
													    <?php if(!empty($_POST['consolidation'])){
													    	if($_POST['consolidation'] == 2){
													    	?>
													    	checked
													    <?php
													    	}
													    } ?>

													     value="2">
													    <label for="consolidation2">Par utilisateur</label>
													</div>
												</div>
												<div class="col-md-2 mt20">
													<div class="radio-custom">
													    <input type="radio" id="consolidation3" name="consolidation" value="3"
													    <?php if(!empty($_POST['consolidation'])){
													    	if($_POST['consolidation'] == 3){
													    	?>
													    	checked
													    <?php
													    	}
													    } ?>

													    >
													    <label for="consolidation3">Par frais</label>
													</div>
												</div>
												<div class="col-md-3 text-center mt20">
													<button type="submit" class="btn btn-sm btn-primary">Filtrer</button> <a href="ndf_details.php" class="btn btn-sm btn-info">RAZ formulaire</a>
												</div>
											</div>

										</form>
</div>


	                                </div>

	                            </div>

	                        </div>
						</div>
						<?php if(!empty($_POST)){ ?>
							<div class="col-md-12">
							<?php if($_POST['consolidation'] == 1){ ?>
								<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
			                        <thead>
			                            <tr class="dark">
			                            	<th class="text-center">Note</th>
			                                <th class="text-center">Frais</th>
			                                <th class="text-center">Utilisateur</th>
			                                <th class="text-center">Date</th>
			                                <th class="text-center">Qte</th>
			                                <th class="text-center">TTC</th>
			                                <th class="text-center">Prise en charge TTC</th>
			                                <th class="text-center">HT</th>
			                                <th class="text-center">Dépassement à valider</th>
			                                <th class="text-center">Dépassement accordé</th>
			                                <th class="text-center">Numéro de justificatif</th>
			                            </tr>
			                        </thead>
			                        <tbody class="text-center">
				                        <?php
				                        	foreach($notes as $n){

				                        	?>
				                        	<tr>
				                        		<td><?= $n['ndf_id'] ?></td>
				                        		<td><?= $n['nca_libelle'] ?></td>
				                        		<td><?= $n['uti_nom'] ?> <?= $n['uti_prenom'] ?></td>
				                        		<td><?= convert_date_txt($n['ndf_date']) ?></td>
				                        		<td><?= $n['nli_qte'] ?></td>
				                        		<td><?= number_format($n['nli_ttc'], 2, ',', ' ')  ?></td>

				                        		<td>

				                        				<?= number_format($n['nli_pris_en_charge'], 2, ',', ' ')  ?>
				                        		</td>
				                        		<td>
				                        			<?php
				                        			$ht = $n['nli_ttc'] - $n['nli_montant_tva'];
				                        			?>
				                        			<?= number_format($ht, 2, ',', ' ') ?>
				                        		</td>

				                        		<td><?= number_format($n['nli_depassement_valide'], 2, ',', ' ')  ?></td>
				                        		<td>
				                        			<?= number_format($n['nli_depassement_accorde'], 2, ',', ' ')  ?>
				                        		</td>



				                        		<td><?= $n['nli_numero'] ?></td>
				                        	</tr>
				                        <?php


				                        }?>
			                        </tbody>
	                    		</table>
	                    		<?php }elseif($_POST['consolidation'] == 2){ ?>
		                    		<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
				                        <thead>
				                            <tr class="dark">

				                                <th class="text-center">Utilisateur</th>
				                                <th class="text-center">Qte</th>
				                                <th class="text-center">TTC</th>
				                                <th class="text-center">Prise en charge</th>
				                                <th class="text-center">Dépassement à valider</th>
				                                <th class="text-center">Dépassement accordé</th>
				                            </tr>
				                        </thead>
				                        <tbody class="text-center">
					                        <?php foreach($utilisateurs as $u){
					                        	if(!empty($notes[$u['uti_id']])){
					                        	?>
					                        	<tr>
					                        		<td><?= $u['uti_nom'] ?> <?= $u['uti_prenom'] ?></td>
					                        		<td><?= $notes[$u['uti_id']]['SUM(nli_qte)'] ?></td>
					                        		<td><?= number_format($notes[$u['uti_id']]['SUM(ndf_ttc)'] , 2, ',', ' ')  ?></td>
													<td><?= number_format($notes[$u['uti_id']]['SUM(nli_pris_en_charge)'] , 2, ',', ' ')  ?></td>
					                        		<td><?= number_format($notes[$u['uti_id']]['SUM(nli_depassement_valide)'] , 2, ',', ' ')  ?></td>
					                        		<td><?= number_format($notes[$u['uti_id']]['SUM(nli_depassement_accorde)'] , 2, ',', ' ')  ?></td>
					                        	</tr>
					                        <?php
					                        	}
					                        }?>
				                        </tbody>
		                    		</table>
	                    		<?php }elseif($_POST['consolidation'] == 3){ ?>
	                    			<table class="table table-striped table-hover outprint" id="table_id" style="border:1px solid #e2e2e2;">
				                        <thead>
				                            <tr class="dark">

				                                <th class="text-center">Catégorie</th>
				                                <th class="text-center">Qte</th>
				                                <th class="text-center">TTC</th>
				                                <th class="text-center">Prise en charge</th>
				                                <th class="text-center">Dépassement à valider</th>
				                                <th class="text-center">Dépassement accordé</th>
				                            </tr>
				                        </thead>
				                        <tbody class="text-center">
					                        <?php foreach($categories as $c){
					                        	if(!empty($notes[$c['nca_id']])){
					                        	?>
					                        	<tr>
					                        		<td><?= $c['nca_libelle'] ?></td>
					                        		<td><?= $notes[$c['nca_id']]['SUM(nli_qte)'] ?></td>
					                        		<td><?= number_format($notes[$c['nca_id']]['SUM(ndf_ttc)'] , 2, ',', ' ')  ?></td>
													<td><?= number_format($notes[$c['nca_id']]['SUM(nli_pris_en_charge)'] , 2, ',', ' ')  ?></td>
					                        		<td><?= number_format($notes[$c['nca_id']]['SUM(nli_depassement_valide)'] , 2, ',', ' ')  ?></td>
					                        		<td><?= number_format($notes[$c['nca_id']]['SUM(nli_depassement_accorde)'] , 2, ',', ' ')  ?></td>
					                        	</tr>
					                        <?php
					                        	}
					                        }?>
				                        </tbody>
		                    		</table>
	                    		<?php } ?>
							</div>
						<?php } ?>

					</div>





				</section>
			</section>

		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
			<div class="row">
				<div class="col-xs-4 footer-left pt7" >
					<a href="parametre.php" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a>
				</div>
				<div class="col-xs-4 footer-middle pt7"></div>
				<div class="col-xs-4 footer-right pt7">


				</div>
			</div>
		</footer>

</form>



<?php
include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
<!-- plugin pour les masques formulaires -->
<script src="assets/js/custom.js"></script>

<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script src="vendor/plugins/summernote/summernote.min.js"></script>
<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

<script src="vendor/plugins/jquery.numberformat.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->


<script>
// DOCUMENT READY //a
jQuery(document).ready(function () {

// initilisation plugin datatables
$('#table_id').DataTable({
    "language": {
      "url": "vendor/plugins/DataTables/media/js/French.json"
    },
    "paging": false,
    "searching": false,
    "info": false,
    "columnDefs": [ {
          "targets": 'no-sort',
          "orderable": false,
  	}]
});
// quand on veut imprimer
// edition d'une ligne

////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
});

///////////// FONCTIONS //////////////

//////////// FIN FONCTIONS //////////


</script>

</body>
</html>