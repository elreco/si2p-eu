<?php

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

$erreur="";

if(!empty($_POST)){


	$ebp_id=0;
	if(isset($_POST['ebp_id'])){
		$ebp_id=intval($_POST['ebp_id']);
	}

	if($ebp_id>0){

		// EDITION
		$req = $Conn->prepare("UPDATE evacuations_bilans_param SET ebp_remarque=:ebp_remarque,ebp_preconisation=:ebp_preconisation WHERE ebp_id=:ebp_id;");
		$req->bindParam("ebp_remarque",$_POST['ebp_remarque']);
		$req->bindParam("ebp_preconisation",$_POST['ebp_preconisation']);
		$req->bindParam("ebp_id",$ebp_id);
		$req->execute();

	}else{
		$req = $Conn->prepare("INSERT INTO evacuations_bilans_param (ebp_remarque,ebp_preconisation) VALUES (:ebp_remarque,:ebp_preconisation);");
		$req->bindParam("ebp_remarque",$_POST['ebp_remarque']);
		$req->bindParam("ebp_preconisation",$_POST['ebp_preconisation']);
		$req->execute();

	}

}else{
	$erreur="Formulaire incomplet!";
}

if(!empty($erreur)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur
	);
}else{
	$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Remarque ajoutée"
	);
}

header("location : evac_remarque_liste.php");

 ?>
