<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_parametre.php');
	include('modeles/mod_tva.php');
	include('modeles/mod_compte.php');
	include('modeles/mod_erreur.php');
	
	$tva_periode=0;
	if(!empty($_GET["tva_periode"])){
		$tva_periode=$_GET["tva_periode"];
	}
	

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">	
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<form method="post" action="param_tva_enr.php" >
			<div>
				<input type="hidden" name="tpe_id" id="tpe_id" value="<?=$tva_periode?>" >
			</div>
			<!-- Start: Main -->
			<div id="main">
			
				<?php	
					include "includes/header_def.inc.php";
				?>	
				
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper">		 
					<!-- Begin: Content -->
					
					
					<section id="content" class="animated fadeIn">
					
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
									
									<?php	
											$comptes=get_comptes();
											$tpe_tva=0;
											$tpe_taux=0;
											$tpe_date_deb="";
											$tpe_cpt_ach=0;
											$tpe_cpt_ven=0;
											
											if($tva_periode>0){
												$donnees=get_tva_periode($tva_periode);
												$tpe_tva=$donnees["tpe_tva"];
												$tpe_taux=$donnees["tpe_taux"];
												$tpe_date_deb=convert_date_txt($donnees["tpe_date_deb"]);												
												$tpe_cpt_ach=$donnees["tpe_cpt_ach"];
												$tpe_cpt_ven=$donnees["tpe_cpt_ven"];
												
											}; ?>																				
											<div class="text-center">
												<div class="content-header">
													<h2>
									<?php				if($tva_periode==0){
															echo("Nouvelle");
														}else{
															echo("Edition d'une");
														};	?>
														<b class="text-primary">TVA</b>
													</h2>
												</div>
											</div>
											<div class="row" >
												<div class="col-md-6" >
												
													<div class="section">
														<label class="field select" >
															<select name="tpe_tva" id="tpe_tva" required >	
																<option value="" >Type de TVA</option>
														<?php	foreach($base_tva as $cle => $value){
																	if($cle>0){
																		if($cle==$tpe_tva){
																			echo("<option value='" . $cle . "' selected >" . $value . "</option>");
																		}else{
																			echo("<option value='" . $cle . "' >" . $value . "</option>");
																		};
																	}
																}  ?>											
															</select>
															<i class="arrow simple"></i>
														</label>
													</div>
												</div>								
												<div class="col-md-6" >	
													<div class="section">
														<div class="field append-icon">			
															<input type="text"  name="tpe_taux" id="tpe_taux" class="gui-input" placeholder="Taux de TVA" value="<?=$tpe_taux?>" />
															<label for="tpe_taux" class="field-icon">%</label>										
														</div>
													</div>
												</div>
											</div>
											
											<div class="row" >
											
												<div class="col-md-6" >		
												
													<div class="section">
														<label for="tpe_date_deb" class="field prepend-icon">
															<input type="text" id="tpe_date_deb" name="tpe_date_deb" class="gui-input" placeholder="Date d'effet" value="<?=$tpe_date_deb?>" required />
															<label class="field-icon">
																<i class="fa fa-calendar-o"></i>
															</label>
														</label>
													</div>	
													
												</div>
											</div>
								
											<div class="row" >
											
												<div class="col-md-6" >	
													<div class="section">
													
														<label class="field select">
															<select name="tpe_cpt_ven" id="tpe_cpt_ven" >
																<option value="0" >Compte de vente</option>
														<?php	foreach($comptes as $value){
																	if($value["cpt_id"]==$tpe_cpt_ven){
																		echo("<option value='" . $value["cpt_id"] . "' selected >" . $value["cpt_numero"] . "</option>");	
																	}else{
																		echo("<option value='" . $value["cpt_id"] . "' >" . $value["cpt_numero"] . "</option>");	
																	}
																												
																}?>											
															</select>	
															<i class="arrow"></i>										
														</label>
													</div>
												</div>								
												<div class="col-md-6" >			
													<div class="section">
														<label class="field select">
															<select name="tpe_cpt_ach" id="tpe_cpt_ach" >	
																<option value="0" >Compte d'achat</option>
														<?php	foreach($comptes as $value){
																	if($value["cpt_id"]==$tpe_cpt_ach){
																		echo("<option value='" . $value["cpt_id"] . "' selected >" . $value["cpt_numero"] . "</option>");
																	}else{
																		echo("<option value='" . $value["cpt_id"] . "' >" . $value["cpt_numero"] . "</option>");
																	}																		
																}?>											
															</select>
															<i class="arrow"></i>										
														</label>
													</div>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>									
					<!-- End: Content -->
				</section>
				
			</div>
			<!-- End: Main -->
			
			<footer id="content-footer" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="param_tva_liste.php" class="btn btn-default btn-sm" >
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" >
					</div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm" >
							<i class="fa fa-plus" ></i>
							Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
	<?php
		include "includes/footer_script.inc.php"; ?>		
		
		<!-- BEGIN: PAGE SCRIPTS -->

		<!-- THEME -->		
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<!--PLUGIN-->
		<script src="vendor/plugins/mask/jquery.mask.js" ></script>
		
		<script type="text/javascript">
			jQuery(document).ready(function() {
				
								
				$('#tpe_taux').mask('00.00');
				
				$("#tpe_date_deb").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					showButtonPanel: false,
					beforeShow: function(input, inst) {
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					}
				});				
			});
			(jQuery);
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
