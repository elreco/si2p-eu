<?php

    include "includes/controle_acces.inc.php";
    include_once("includes/connexion_soc.php");
    include_once("includes/connexion_fct.php");
    include_once("includes/connexion.php");

    include 'modeles/mod_orion_utilisateur.php';
    include 'modeles/mod_orion_pro_categorie.php';
    include 'modeles/mod_orion_pro_famille.php';
    include 'modeles/mod_orion_pro_s_famille.php';
    include 'modeles/mod_orion_vehicule.php';

    include 'modeles/mod_parametre.php';
    include 'modeles/mod_get_commerciaux.php';
    include('modeles/mod_get_client_infos.php');

    // ECRAN DE VISU D'UNE ACTION

    $erreur = 0;
    $erreur_txt = "";

    $action_id = 0;
    if (isset($_GET["action"])) {
        if (!empty($_GET["action"])) {
            $action_id = $_GET["action"];
        }
    }

    // id du client consulter
    $action_client_id = 0;
    if (isset($_GET["action_client"])) {
        if (!empty($_GET["action_client"])) {
            $action_client_id = $_GET["action_client"];
        }
    }

    $onglet = 1;
    if (isset($_GET["onglet"])) {
        $onglet = intval($_GET["onglet"]);
    }

    // personne connecté

    $acc_agence = 0;
    if (isset($_SESSION['acces']["acc_agence"])) {
        $acc_agence = $_SESSION['acces']["acc_agence"];
    }
    $acc_societe = 0;
    if (isset($_SESSION['acces']["acc_societe"])) {
        $acc_societe = $_SESSION['acces']["acc_societe"];
    }
    $acc_profil = 0;
    if (isset($_SESSION['acces']["acc_profil"])) {
        $acc_profil = intval($_SESSION['acces']["acc_profil"]);
    }
    $acc_utilisateur = 0;
    if (!empty($_SESSION['acces']["acc_ref"])) {
        if ($_SESSION['acces']["acc_ref"] == 1) {
            $acc_utilisateur = intval($_SESSION['acces']["acc_ref_id"]);
        }
    }


    $consultation_gc = false;
    if ($conn_soc_id != $acc_societe) {
        $acc_societe = $conn_soc_id;
        $acc_agence = 0;
        $consultation_gc = true;
    }

    if ($action_id > 0) {

        // ON RECUPERE LES DONNES NECESSAIRE A L'AFFICHAGE

        // l'action

        $sql = "SELECT * FROM Actions WHERE act_id=:action";
        if (!empty($acc_agence) and $acc_profil != 1) {
            $sql .= " AND act_agence=:acc_agence";
        }
        $sql .= ";";
        $req = $ConnSoc->prepare($sql);
        $req->bindParam(":action", $action_id);
        if (!empty($acc_agence) and $acc_profil != 1) {
            // le formateur n'ont acces qu'aux actions qui leurs sont affectés (cf securite au niveau des dates)
            $req->bindParam(":acc_agence", $acc_agence);
        }
        $req->execute();
        $action = $req->fetch();

        if (!empty($action)) {

            $adresse = "";
            if (!empty($action["act_adr_nom"])) {
                $adresse .= $action["act_adr_nom"];
            };
            if (!empty($action["act_adr_service"])) {
                if (!empty($adresse)) {
                    $adresse .= "<br/>";
                };
                $adresse .= $action["act_adr_service"];
            };
            if (!empty($action["act_adr1"])) {
                if (!empty($adresse)) {
                    $adresse .= "<br/>";
                };
                $adresse .= $action["act_adr1"];
            };
            if (!empty($action["act_adr2"])) {
                if (!empty($adresse)) {
                    $adresse .= "<br/>";
                };
                $adresse .= $action["act_adr2"];
            };
            if (!empty($action["act_adr3"])) {
                if (!empty($adresse)) {
                    $adresse .= "<br/>";
                };
                $adresse .= $action["act_adr3"];
            };
            if (!empty($action["act_adr_ville"])) {
                if (!empty($adresse)) {
                    $adresse .= "<br/>";
                };
                $adresse .= $action["act_adr_cp"] . " " . $action["act_adr_ville"];
            };

            /*if(!empty($action["act_vehicule"])){
				$vehicule=orion_vehicule($action["act_vehicule"]);
			}*/
        } else {
            $erreur_txt = "Vous ne disposez pas des droits nécessaires pour afficher la page!";
        }
    } else {
        $erreur_txt = "Impossible d'afficher cette page!";
    }

    /***************************************
			DONNE COMPLEMENTAIRE
     ***************************************/

    if (empty($erreur_txt)) {

        // RETOUR

        $_SESSION["retourClient"] = "action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id . "&societ=" . $conn_soc_id;
        $_SESSION["retourFacture"] = "action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id . "&societ=" . $conn_soc_id;
        $_SESSION["retourDevis"] = "action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id . "&societ=" . $conn_soc_id;
        $_SESSION["retourAttest"] = "action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id . "&societ=" . $conn_soc_id . "&onglet=3";
        if($_SESSION["menu"]["rubrique"]!=5){
            $_SESSION["retourCommande"] = "action_voir.php?action=" . $action_id . "&action_client=" . $action_client_id . "&societ=" . $conn_soc_id;
        }

        // FAMILLES

        $action_head = "";
        $action_bg = "";
        $action_txt = "";

        $sql = "SELECT pfa_libelle FROM Produits_Familles WHERE pfa_id=" . $action["act_pro_famille"] . ";";
        $req = $Conn->query($sql);
        $d_pro_fam = $req->fetch();
        if (!empty($d_pro_fam)) {
            $action_head .= " " . $d_pro_fam["pfa_libelle"];
        }

        if (!empty($action["act_pro_sous_famille"])) {
            $sql = "SELECT psf_libelle,psf_couleur_bg,psf_couleur_txt FROM Produits_Sous_Familles WHERE psf_id=" . $action["act_pro_sous_famille"] . ";";
            $req = $Conn->query($sql);
            $d_pro_s_fam = $req->fetch();
            if (!empty($d_pro_s_fam)) {
                $action_head .= " / " . $d_pro_s_fam["psf_libelle"];
                if (!empty($d_pro_s_fam["psf_couleur_bg"])) {
                    $action_bg = $d_pro_s_fam["psf_couleur_bg"];
                    $action_txt = $d_pro_s_fam["psf_couleur_txt"];
                }
            }
        }
        if (!empty($action["act_pro_sous_sous_famille"])) {
            $sql = "SELECT pss_libelle,pss_couleur_bg,pss_couleur_txt FROM Produits_Sous_Sous_Familles WHERE pss_id=" . $action["act_pro_sous_sous_famille"] . ";";
            $req = $Conn->query($sql);
            $d_pro_s_s_fam = $req->fetch();
            if (!empty($d_pro_s_s_fam)) {
                $action_head .= " / " . $d_pro_s_s_fam["pss_libelle"];
                if (!empty($d_pro_s_s_fam["pss_couleur_bg"])) {
                    $action_bg = $d_pro_s_s_fam["pss_couleur_bg"];
                    $action_txt = $d_pro_s_s_fam["pss_couleur_txt"];
                }
            }
        }
        if (!empty($action_bg)) {
            $action_bg = "style='background-color:#" . $action_bg . ";color:" . $action_txt . "'";
        }

        // LES ATTESTATIONS

        $d_attestations = array();
        $sql = "SELECT att_id,att_titre FROM Attestations WHERE att_famille=" . $action["act_pro_famille"] . " AND att_sous_famille=" . $action["act_pro_sous_famille"] . "
		AND att_sous_sous_famille=" . $action["act_pro_sous_sous_famille"] . " ORDER BY att_titre;";
        $req = $Conn->query($sql);
        $results = $req->fetchAll();
        //print_r($results);
        if (!empty($results)) {
            foreach ($results as $r) {
                $d_attestations[$r["att_id"]] = $r["att_titre"];
            }
        }


        // le vehicule
        if (!empty($action["act_vehicule"])) {

            $sql = "SELECT veh_libelle FROM Vehicules WHERE veh_id=" . $action["act_vehicule"] . ";";
            $req = $Conn->query($sql);
            $d_vehicule = $req->fetch();
        }

        // la salle
        if (!empty($action["act_salle"])) {

            $sql = "SELECT sal_nom FROM Salles WHERE sal_id=" . $action["act_salle"] . ";";
            $req = $ConnSoc->query($sql);
            $d_salle = $req->fetch();
        }


        // LES CLIENTS INSCRITS

        $sql = "SELECT DISTINCT cli_interco_soc,cli_interco_age,cli_filiale_de, acl_id,acl_pro_reference,acl_pro_libelle,acl_commercial,acl_ca_unit,acl_pro_inter,DATE_FORMAT(acl_creation_date,'%d/%m/%Y') as acl_creation_date,acl_creation_uti
		,acl_confirme,DATE_FORMAT(acl_confirme_date,'%d/%m/%Y') as acl_confirme_date,acl_archive,DATE_FORMAT(acl_archive_date,'%d/%m/%Y') AS acl_archive_date,acl_archive_uti,acl_archive_ident,acl_archive_ok,acl_opca_fac,acl_opca_num,acl_bc_num
		,acl_attestation,acl_creation_ident,acl_commentaire,acl_client,acl_for_nb_sta_resa,acl_for_nb_sta,acl_devis,acl_devis_numero
		,acl_facturation,acl_ca_nc,acl_facture,acl_non_fac,acl_ca,acl_ca_gfc,acl_non_fac_gfc,acl_facture_gfc
		,acl_avis_emis,DATE_FORMAT(acl_avis_emis_date,'%d/%m/%Y') AS acl_avis_emis_date_fr,acl_avis_recu,DATE_FORMAT(acl_avis_recu_date,'%d/%m/%Y') AS acl_avis_recu_date_fr,acl_reporte
		,cli_groupe,cli_filiale_de,cli_id,cli_code,cli_nom,cli_categorie
		,con_id,con_numero";
        $sql .= " FROM Actions_Clients INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id";
        if ($acc_agence > 0) {
            $sql .= " AND cli_agence=:agence";
        }
        $sql .= ")
		LEFT JOIN Conventions ON ( (Actions_Clients.acl_id=Conventions.con_action_client OR (Actions_Clients.acl_client=Conventions.con_client AND Conventions.con_action_client=0)) AND Conventions.con_action=:action)
		WHERE acl_client=cli_id AND acl_action=:action ORDER BY acl_archive,acl_confirme DESC,cli_code;";
        $req = $ConnSoc->prepare($sql);
        $req->bindParam(":action", $action_id);
        if ($acc_agence > 0) {
            $req->bindParam(":agence", $action["act_agence"]);
        }
        $req->execute();
        $action_clients = $req->fetchAll();
    }

    // SI CLIENTS

    // var devant etre ini au cas ou il n'y aurait pas de client
    $total_resa = 0;
    $total_conf = 0;
    $orion_utilisateurs = orion_utilisateurs();
    if (empty($erreur_txt) and !empty($action_clients)) {



        // COMMERCIAUX
        $tab_com = array(
            0 => ""
        );
        $sql = "SELECT com_id,com_label_1,com_label_2 FROM Commerciaux ORDER BY com_id;";
        $req = $ConnSoc->prepare($sql);
        $req->execute();
        $com_source = $req->fetchAll();
        if (!empty($com_source)) {
            foreach ($com_source as $com) {
                $tab_com[$com["com_id"]] = $com["com_label_1"] . " " . $com["com_label_2"];
            };
        };

        // TYPE D'INFO

        $infos_types = array();
        $sql = "SELECT * FROM Clients_Infos ORDER BY cin_id";
        $req = $Conn->prepare($sql);
        $req->execute();
        $results = $req->fetchAll();
        if (!empty($results)) {
            foreach ($results as $r) {
                $infos_types[$r["cin_id"]] = $r["cin_libelle"];
            }
        }

        // NB ISNCRIT


        $sql = "SELECT COUNT(ast_stagiaire),ast_action_client,ast_confirme FROM Actions_Stagiaires WHERE ast_action=:action GROUP BY ast_action_client,ast_confirme;";
        $req = $ConnSoc->prepare($sql);
        $req->bindParam(":action", $action_id);
        $req->execute();
        $d_stagiaires = $req->fetchAll();

        if (!empty($d_stagiaires)) {
            foreach ($d_stagiaires as $sta) {
                $cle_cli = array_search($sta["ast_action_client"], array_column($action_clients, "acl_id"));
                if (!is_bool($cle_cli)) {
                    if (!empty($sta["ast_confirme"])) {
                        $action_clients[$cle_cli]["nb_confirme"] = $sta[0];
                        if (empty($action_clients[$cle_cli]["acl_archive"])) {
                            $total_conf = $total_conf + $sta[0];
                        }
                    } else {
                        if (empty($action_clients[$cle_cli]["acl_archive"])) {
                            $action_clients[$cle_cli]["nb_resa"] = $sta[0];
                            $total_resa = $total_resa + $sta[0];
                        }
                    }
                }
            }
        }


        // LE CLIENTS VISUALISE
        $cle_visu = 0;
        if ($action_client_id > 0) {
            $cle_visu = array_search($action_client_id, array_column($action_clients, "acl_id"));
            if (is_bool($cle_visu)) {
                $cle_visu = 0;
            }
        }

        $action_client = $action_clients[$cle_visu];
        $action_client_id = $action_client["acl_id"];

        /*unset($action_clients[$cle_visu]);
		$action_clients = array_values($action_clients);*/

        // INFO DU CLIENT
        $client_info = get_client_infos($action_client["cli_id"], 1, $action["act_pro_famille"], $action["act_pro_sous_famille"], $action["act_pro_sous_sous_famille"]);

        // FIN SI CLIENT
    }


    if (empty($erreur_txt)) {

        // LES DATES DE PRESENCE

        $d_intervenants = array();
        $d_sous_traitants = array();
        $d_interco = array();

        if ($acc_profil == 1) {
            $uti_formateur = false;    // indique si l'utilisateur fait partie des formateurs inscrits sur l'action
        } else {
            $uti_formateur = true;
        }

        $sql = "SELECT int_label_1,int_label_2,int_id,int_type,int_ref_1,int_ref_3";
        $sql .= ",pda_id,pda_date,pda_demi,pda_categorie";
        $sql .= ",ase_id,ase_h_deb,ase_h_fin,pda_sta_resa,pda_sta_conf";
        if (!empty($action_client_id)) {
            $sql .= ",acd_action_client";
        }
        $sql .= " FROM ";
        $sql .= " Intervenants LEFT JOIN Plannings_Dates ON (Intervenants.int_id=Plannings_Dates.pda_intervenant)";
        $sql .= " LEFT JOIN Actions_Sessions ON (Plannings_Dates.pda_id=Actions_Sessions.ase_date)";
        if (!empty($action_client_id)) {
            $sql .= " LEFT OUTER JOIN Actions_Clients_Dates ON (Plannings_Dates.pda_id=Actions_Clients_Dates.acd_date AND acd_action_client=:action_client)";
        }
        $sql .= " WHERE pda_type=1 AND pda_ref_1=:action AND NOT pda_archive";
        $sql .= " ORDER BY pda_date,pda_demi,ase_h_deb,int_label_1,int_id;";
        $req = $ConnSoc->prepare($sql);
        $req->bindParam(":action", $action_id);
        if (!empty($action_client_id)) {
            $req->bindParam(":action_client", $action_client_id);
        }
        $req->execute();
        $action_dates = $req->fetchAll();

        if (!empty($action_dates)) {
            foreach ($action_dates as $date) {

                if ($acc_profil == 1) {
                    if ($date["int_type"] == 1 and $date["int_ref_1"] == $acc_utilisateur) {
                        $uti_formateur = true;
                    }
                }

                if (!isset($d_intervenants[$date["int_id"]])) {
                    $d_intervenants[$date["int_id"]] = array(
                        "int_identite" => $date["int_label_2"] . " " . $date["int_label_1"]
                    );
                }
                // INTERVENANT ST
                if ($date["int_type"] == 3 or $date["int_type"] == 2) {

                    if ($date["int_type"] == 2) {
                        $fou_id = $date["int_ref_3"];
                    } else {
                        $fou_id = $date["int_ref_1"];
                    }
                    if (!isset($d_sous_traitants[$date["int_id"]])) {
                        $d_sous_traitants[$date["int_id"]] = array(
                            "fou_id" => $fou_id,
                            "fou_code" => "",
                            "com_id" => 0,
                            "com_numero" => "",
                            "com_ht" => 0
                        );
                    }
                }
            }
        }

        if ($acc_profil == 1 and !$uti_formateur) {
            $erreur_txt = "Vous ne disposez pas des droits nécessaires pour afficher la page!";
        }
    }
    if (empty($erreur_txt)) {

        // RECHERCHE LES BC DES ST
        if (!empty($d_sous_traitants)) {

            $sql_st = "SELECT fou_code,com_id,com_numero,com_ht FROM Fournisseurs LEFT JOIN Commandes ON (fou_id=com_fournisseur AND com_societe=:acc_societe AND com_agence=:act_agence AND com_action=:action AND NOT com_annule)
			WHERE fou_id=:fournisseur;";
            $req_st = $Conn->prepare($sql_st);

            foreach ($d_sous_traitants as $k => $st) {

                $req_st->bindParam(":fournisseur", $st["fou_id"]);
                $req_st->bindParam(":acc_societe", $acc_societe);
                $req_st->bindParam(":act_agence", $action["act_agence"]);
                $req_st->bindParam(":action", $action_id);
                $req_st->execute();
                $d_fournisseur = $req_st->fetch();
                if (!empty($d_fournisseur)) {

                    $com_id = 0;
                    if (!empty($d_fournisseur["com_id"])) {
                        $com_id = $d_fournisseur["com_id"];
                    }

                    $d_sous_traitants[$k]["fou_code"] = $d_fournisseur["fou_code"];
                    $d_sous_traitants[$k]["com_id"] = $com_id;
                    $d_sous_traitants[$k]["com_numero"] = $d_fournisseur["com_numero"];
                    $d_sous_traitants[$k]["com_ht"] = $d_fournisseur["com_ht"];
                }
            }
        }

        // DROIT D'ACCES DE L'UTILISATEURS SUR LE PROGRAMME

        $drt_edit_action = false;
        if ($acc_profil != 1) {
            $drt_edit_action = true;
        }
        $aff_ca = false;
        if ($acc_profil != 1) {
            $aff_ca = true;
        }


        // LES DOCUMENTS

        // FIN DOCUMENTS

        // SELECTIONNER LES INTERCOS
        // INTERCO
        $sql = "SELECT * FROM Actions_Clients_Intercos WHERE aci_action_client=:action_client";
        $req = $ConnSoc->prepare($sql);
        $req->bindParam(":action_client", $action_client['acl_id']);
        $req->execute();
        $d_actions_clients_intercos = $req->fetchAll();
        // FIN INTERCO
    }

    if (empty($erreur_txt)) { ?>
     <!DOCTYPE html>
     <html>

     <head>
         <meta charset="utf-8">
         <title>SI2P - Orion - Actions</title>
         <meta name="keywords" content="" />
         <meta name="description" content="">
         <meta name="author" content="">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <!-- CSS THEME -->
         <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
         <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />

         <!-- CSS PLUGINS -->
         <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
         <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

         <!-- CSS Si2P -->
         <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

         <!-- Favicon -->
         <link rel="shortcut icon" href="assets/img/favicon.png">

         <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
         <!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
     </head>

     <body class="sb-top sb-top-sm ">

         <div id="main">
             <?php include "includes/header_def.inc.php"; ?>
             <section id="content_wrapper">

                 <section id="content" class="animated">

                        <div class="tab-block">
                            <ul class="nav nav-tabs responsive">
                                <li <?php if ($onglet == 1) echo ("class='active'"); ?>>
                                    <a href="#action_general" id="ong_general" data-toggle="tab" aria-expanded="false" class="onglet" data-onglet="1">Général</a>
                                </li>
                        <?php   if (empty($d_actions_clients_intercos)) { ?>
                                    <li>
                                        <a href="action_voir_sta.php?action=<?= $action_id ?>&action_client=<?= $action_client_id ?>&societ=<?= $conn_soc_id ?>">Stagiaires</a>
                                    </li>
                            <?php   if ($drt_edit_action) { ?>
                                        <li id="ong_edition" <?php if ($onglet == 3) echo ("class='active'"); if ($action["act_verrou_admin"]) echo ("style='display:none;'"); ?> >
                                            <a href="#action_edition" data-toggle="tab" aria-expanded="false" class="onglet" data-onglet="3">Editions</a>
                                        </li>
                        <?php       } 
                                } ?>
                            </ul>
                         
                        <div class="tab-content responsive">
                                <!-- CONTENU GENERAL -->
                                <div id="action_general" class="tab-pane <?php if ($onglet == 1) echo ("active"); ?>">

                                    <div class="row">

                                        <!-- COL 1 -->
                                        <div class="col-md-6 col-xl-4 ">
                                            <?php if (!empty($action_client)) {


                                                    if (!isset($action_client["nb_resa"])) {
                                                        $action_client["nb_resa"] = 0;
                                                    }
                                                    if (!isset($action_client["nb_confirme"])) {
                                                        $action_client["nb_confirme"] = 0;
                                                    }

                                                    $sql_fac = "SELECT fac_id,fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date,fac_blocage,fli_montant_ht
                                                        FROM Factures,Factures_Lignes WHERE fac_id=fli_facture
                                                        AND fli_action_client=:fli_action_client AND fli_action_cli_soc=:fli_action_cli_soc ORDER BY fac_date,fac_chrono;";
                                                    $req_fac = $ConnSoc->prepare($sql_fac);
                                                    $req_fac->bindParam(":fli_action_client", $action_client["acl_id"]);
                                                    $req_fac->bindParam(":fli_action_cli_soc", $acc_societe);
                                                    $req_fac->execute();
                                                    $d_factures = $req_fac->fetchAll();


                                                    if ($action_client["acl_facturation"] == 1) {

                                                        $ConnGFC = connexion_fct(4);

                                                        $req_fac_gfc = $ConnGFC->prepare($sql_fac);
                                                        $req_fac_gfc->bindParam(":fli_action_client", $action_client["acl_id"]);
                                                        $req_fac_gfc->bindParam(":fli_action_cli_soc", $acc_societe);
                                                        $req_fac_gfc->execute();
                                                        $d_factures_gfc = $req_fac_gfc->fetchAll();
                                                        if (empty($d_factures_gfc)) {
                                                            unset($d_factures_gfc);
                                                        }
                                                    }

                                                    // on dertermine si l'action est modifiable
                                                    $drt_modif = false;
                                                    if (!$action["act_archive"]) {
                                                        if (!$action_client["acl_archive"] or ($_SESSION["acces"]["acc_droits"][10] and !$action_client["acl_archive_ok"]) or $_SESSION["acces"]["acc_droits"][11]) {
                                                            $drt_modif = true;
                                                        }
                                                    }

                                                    $class_panel = "";
                                                    if ($action_client["acl_archive"]) {
                                                        $class_panel = "panel-dark";
                                                    } elseif ($action_client["acl_confirme"]) {
                                                        $class_panel = "panel-success";
                                                    } else {
                                                        $class_panel = "panel-warning";
                                                    }
                                                ?>
                                                <div class="panel <?= $class_panel ?>">
                                                    <div class="panel-heading panel-head-sm">

                                                        <span class="panel-title">
                                                            <?php echo ($action_client["acl_id"]);
                                                                echo (" - " . $action_client["cli_code"]);
                                                                echo (" - " . $action_client["acl_pro_reference"]);    ?>
                                                        </span>
                                                        <?php if ($drt_edit_action) { ?>
                                                            <div class="pull-left">
                                                                <a href="client_voir.php?client=<?= $action_client["acl_client"] ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Consulter la fiche client">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                            </div>
                                                            <div class="pull-right">
                                                                <a href="action_client.php?action=<?= $action_id ?>&action_client=<?= $action_client_id ?>&societ=<?= $conn_soc_id ?>" class="btn btn-warning btn-sm<?php if (!$drt_modif) echo (" disabled-archive"); ?>" data-toggle="tooltip" data-placement="top" title="Modifier les infos du client" <?php if (!$drt_modif) echo ("disabled='disabled'"); ?>>
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                            </div>
                                                        <?php        } ?>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="admin-form">
                                                            <div class="row">
                                                                <div class="col-lg-4 text-right"><b>Formation :</b></div>
                                                                <div class="col-lg-8"><b><?= $action_client["acl_pro_libelle"] ?></b></div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-4 text-right"><b>Attestation :</b></div>
                                                                <div class="col-lg-8">
                                                                    <?php if (!empty($action_client["acl_attestation"])) { ?>
                                                                        <b><?= $d_attestations[$action_client["acl_attestation"]] ?></b>
                                                                    <?php    } ?>
                                                                </div>
                                                            </div>
                                                            <div class="row mt10">
                                                                <div class="col-md-4 text-right"><b>Commercial :</b></div>
                                                                <div class="col-md-8"><?= $tab_com[$action_client["acl_commercial"]] ?></div>
                                                            </div>
                                                            <?php if ($aff_ca) {
                                                                    if (!empty($action_client["acl_pro_inter"])) { ?>
                                                                    <div class="row mt10">
                                                                        <div class="col-md-4 text-right"><b>Nb. Sta. confirmé(s) :</b></div>
                                                                        <div class="col-md-2 text-success"><?= $action_client["acl_for_nb_sta"] ?></div>
                                                                        <div class="col-md-4 text-right"><b>Nb. Sta. résa. :</b></div>
                                                                        <div class="col-md-2 text-warning"><?= $action_client["acl_for_nb_sta_resa"] ?></div>
                                                                    </div>
                                                                    <div class="row mt10">
                                                                        <div class="col-md-4 text-right"><b>CA confirmé :</b></div>
                                                                        <div class="col-md-2 text-success"><?= $action_client["acl_ca"] ?> €</div>
                                                                        <div class="col-md-4 text-right"><b>CA projet :</b></div>
                                                                        <div class="col-md-2 text-warning"><?= $action_client["acl_ca_unit"] * $action_client["acl_for_nb_sta_resa"] ?> €</div>
                                                                    </div>
                                                                <?php    } else { ?>
                                                                    <div class="row mt10">
                                                                        <div class="col-md-4 text-right"><b>CA :</b></div>
                                                                        <div class="col-md-2"><?= $action_client["acl_ca"] ?> €</div>
                                                                    </div>
                                                            <?php    }
                                                                } ?>
                                                            <div class="row mt5">
                                                                <div class="col-md-4 text-right mt5"><b>Confirmée :</b></div>
                                                                <div class="col-md-2">
                                                                    <?php if ($action_client["acl_confirme"] == 1) {
                                                                            echo ("<i class='fa fa-check-square-o' ></i>");
                                                                        } else {
                                                                            echo ("<i class='fa fa-square-o' ></i>");
                                                                        } ?>
                                                                </div>
                                                                <?php if ($action_client["acl_confirme"] == 1) { ?>
                                                                    <div class="col-md-6">
                                                                        Le <?= $action_client["acl_confirme_date"] ?>
                                                                    </div>
                                                                <?php        } ?>
                                                            </div>
                                                            <div class="row mt5">
                                                                <div class="col-md-4 text-right mt5"><b>Facturation financeur :</b></div>
                                                                <div class="col-md-8">
                                                                    <?php if ($action_client["acl_opca_fac"] == 1) {
                                                                            echo ("<i class='fa fa-check-square-o' ></i>");
                                                                        } else {
                                                                            echo ("<i class='fa fa-square-o' ></i>");
                                                                        } ?>
                                                                </div>
                                                            </div>
                                                            <div class="row mt5">
                                                                <div class="col-md-4 text-right mt5"><b>Numéro financeur :</b></div>
                                                                <div class="col-md-8"><?= $action_client["acl_opca_num"] ?></div>
                                                            </div>
                                                            <div class="row mt5">
                                                                <div class="col-md-4 text-right mt5"><b>Numéro BC :</b></div>
                                                                <div class="col-md-8"><?= $action_client["acl_bc_num"] ?></div>
                                                            </div>
                                                            <div class="row mt5">
                                                                <div class="col-md-4 text-right mt5"><b>Nb. Inscrit :</b></div>
                                                                <div class="col-md-2 text-success"><?= $action_client["nb_confirme"] ?></div>
                                                                <div class="col-md-4 text-right mt5"><b>Nb. Résa. :</b></div>
                                                                <div class="col-md-2 text-warning"><?= $action_client["nb_resa"] ?></div>
                                                            </div>
                                                            <?php if (!empty($action_client["acl_reporte"])) { ?>
                                                                <div class="row mt5 mb15">
                                                                    <div class="col-md-12 text-center text-warning"><b>Action reportée</b></div>
                                                                </div>
                                                            <?php    }
                                                                if (!empty($action_client["acl_commentaire"])) { ?>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="section-divider mb10">
                                                                            <span>Commentaire</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt10">
                                                                    <div class="col-md-12"><?= nl2br($action_client["acl_commentaire"]) ?></div>
                                                                </div>
                                                            <?php    }  ?>

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="section-divider mb10">
                                                                        <span>Devis</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php if (!empty($action_client["acl_devis"])) { ?>
                                                                <p>
                                                                    <a href="devis_voir.php?devis=<?= $action_client["acl_devis"] ?>">
                                                                        <?= $action_client["acl_devis_numero"] ?>
                                                                    </a>
                                                                </p>
                                                            <?php    } else { ?>
                                                                <p class="alert alert-info">Pas de devis associée</p>
                                                            <?php    } ?>

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="section-divider mb10">
                                                                        <span>Avis de passage</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php if (empty($action_client["acl_avis_emis"])) {
                                                                    // avis non émit
                                                                ?>

                                                                <div class="row mt10">

                                                                    <div class="col-md-10">
                                                                        <p class="alert alert-warning">
                                                                            Avis de passage non émis.
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-2 pt10">
                                                                        <a href="mail_prep.php?action=<?= $action_id ?>&action_client=<?= $action_client_id ?>&societ=<?= $acc_societe ?>&avis" class="btn btn-sm btn-info" data-toggle="tooltip" title="Envoyer l'avis de passage">
                                                                            <i class="fa fa-envelope-o"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php        } else { ?>
                                                                <div class="row mt10">

                                                                    <div class="col-md-5">
                                                                        <p class="alert alert-success">
                                                                            Avis émis le <?= $action_client["acl_avis_emis_date_fr"] ?>.
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-1 pt10">
                                                                        <a href="mail_prep.php?action=<?= $action_id ?>&action_client=<?= $action_client_id ?>&societ=<?= $acc_societe ?>&avis" class="btn btn-sm btn-info" data-toggle="tooltip" title="Renvoyer l'avis de passage">
                                                                            <i class="fa fa-envelope-o"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <?php if (empty($action_client["acl_avis_recu"])) {         ?>
                                                                            <p class="alert alert-warning">
                                                                                Réception non confirmée.
                                                                            </p>
                                                                        <?php    } else { ?>
                                                                            <p class="alert alert-success">
                                                                                Réception confirmée le <?= $action_client["acl_avis_recu_date_fr"] ?>.
                                                                            </p>
                                                                        <?php    } ?>
                                                                    </div>
                                                                </div>
                                                            <?php        } ?>
                                                            <?php if ($aff_ca) {

                                                                    // FACTURE DE LA REGION
                                                                ?>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="section-divider mb10">
                                                                            <span>
                                                                                <?php if ($action_client["acl_facturation"] == 1) {
                                                                                        echo ("Factures à GFC");
                                                                                    } else {
                                                                                        echo ("Factures");
                                                                                    } ?>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php if ($action_client["acl_non_fac"] == 1) {
                                                                        echo ("<p class='alert alert-danger text-center' ><b>Action NON FACTURABLE</b></p>");
                                                                    }
                                                                    if (!empty($d_factures)) { ?>
                                                                    <table class="table">
                                                                        <?php foreach ($d_factures as $facture) { ?>
                                                                            <tr>
                                                                                <td><a href="facture_voir.php?facture=<?= $facture["fac_id"] ?>&societ=<?= $acc_societe ?>"><?= $facture["fac_numero"] ?></a></td>
                                                                                <td><?= $facture["fac_date"] ?></td>
                                                                                <td><?= number_format($facture["fli_montant_ht"], 2, ",", " ") ?> €</td>
                                                                            </tr>
                                                                        <?php            } ?>
                                                                    </table>
                                                                <?php    } elseif (empty($action_client["acl_non_fac"])) { ?>
                                                                    <p class="alert alert-info">Pas de facture associée</p>
                                                                <?php    }
                                                                    if (!$consultation_gc and $_SESSION["acces"]["acc_droits"][29] and !$action_client["acl_facture"] and !$action_client["acl_non_fac"] and !$action_client["acl_ca_nc"]) { ?>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="pull-right">
                                                                                <a href="facture.php?action_client=<?= $action_client_id ?>" class="btn btn-sm btn-success">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php    }

                                                                    // REFAC GFC

                                                                    if ($action_client["acl_facturation"] == 1) { ?>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="section-divider mb10">
                                                                                <span>Factures au client</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <?php if ($action_client["acl_non_fac_gfc"] == 1) {
                                                                            echo ("<p class='alert alert-danger text-center' ><b>Action NON FACTURABLE</b></p>");
                                                                        }
                                                                        if (isset($d_factures_gfc)) { ?>
                                                                        <table class="table">
                                                                            <?php foreach ($d_factures_gfc as $facture) { ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php if ($consultation_gc) { ?>
                                                                                            <a href="facture_voir.php?facture=<?= $facture["fac_id"] ?>"><?= $facture["fac_numero"] ?></a>
                                                                                        <?php    } else {
                                                                                                echo ($facture["fac_numero"]);
                                                                                            } ?>
                                                                                    </td>
                                                                                    <td><?= $facture["fac_date"] ?></td>
                                                                                    <td><?= number_format($facture["fli_montant_ht"], 2, ",", " ") ?> €</td>
                                                                                </tr>
                                                                            <?php            } ?>
                                                                        </table>
                                                                    <?php    } elseif (empty($action_client["acl_non_fac_gfc"])) { ?>
                                                                        <p class="alert alert-info">Pas de facture associée</p>
                                                                    <?php    }
                                                                        if ($consultation_gc and $_SESSION["acces"]["acc_droits"][29] and !$action_client["acl_facture_gfc"] and !$action_client["acl_non_fac_gfc"] and !$action_client["acl_ca_nc"]) { ?>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="pull-right">
                                                                                    <a href="facture.php?action_client=<?= $action_client_id ?>&action_soc=<?= $conn_soc_id ?>" class="btn btn-sm btn-success">
                                                                                        <i class="fa fa-plus"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                <?php    }
                                                                    }
                                                                }



                                                                if ($action_client["acl_archive"]) { ?>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="section-divider mb40">
                                                                            <span>Archivage</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt10 alert alert-danger" id="info_acl_archive_<?= $action_client["acl_id"] ?>">
                                                                    <div class="col-md-5">
                                                                        Action archivée le <?= $action_client["acl_archive_date"] ?>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        Par <?= $orion_utilisateurs[$action_client["acl_archive_uti"]]["identite"] ?>
                                                                    </div>
                                                                    <div class="col-md-3" id="acl_archive_ok_<?= $action_client["acl_id"] ?>">
                                                                        <?php if ($action_client["acl_archive_ok"]) {
                                                                                echo ("Confirmé");
                                                                            } else {
                                                                                echo ("Non confirmé");
                                                                            } ?>
                                                                    </div>
                                                                </div>
                                                                <?php if (!$action_client["acl_archive_ok"] and $_SESSION["acces"]["acc_droits"][11]) { ?>
                                                                    <div class="row mt10" id="btn_acl_archive_<?= $action_client["acl_id"] ?>">
                                                                        <div class="col-md-6 text-center">
                                                                            <button type="button" class="btn btn-sm btn-danger cli-archive<?php if ($action["act_archive"]) echo (" disabled-archive"); ?>" data-type="2" data-action_client="<?= $action_client["acl_id"] ?>" <?php if ($action["act_archive"]) echo ("disabled"); ?>>
                                                                                <i class="fa fa-times"></i> Annuler l'archivage
                                                                            </button>
                                                                        </div>
                                                                        <div class="col-md-6 text-center">
                                                                            <button type="button" class="btn btn-sm btn-success cli-archive" data-type="1" data-action_client="<?= $action_client["acl_id"] ?>">
                                                                                <i class="fa fa-times"></i> Confirmer l'archivage
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                            <?php        }
                                                                } ?>
                                                            <!-- [INTERCO] -->
                                                            <?php
                                                                if ($drt_edit_action && !empty($action_client['cli_interco_soc'])) {

                                                                ?>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="section-divider mb40">
                                                                            <span style="background:white">Interco</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <?php if (!empty($d_actions_clients_intercos)) { ?>
                                                                            <table class="table table-striped">
                                                                                <tbody>
                                                                                    <?php foreach ($d_actions_clients_intercos as $daci) {
                                                                                        $sql = "SELECT soc_nom FROM Societes WHERE soc_id=:aci_action_client_interco_soc";
                                                                                        $req = $Conn->prepare($sql);
                                                                                        $req->bindParam(":aci_action_client_interco_soc", $daci['aci_action_client_interco_soc']);
                                                                                        $req->execute();
                                                                                        $societe_interco = $req->fetch();
                                                                                        ?>
                                                                                        <tr id="aci_<?= $daci['aci_id'] ?>">
                                                                                            <td><?= $daci['aci_action_interco'] ?>-<?= $daci['aci_action_client_interco'] ?></td>
                                                                                            <td> <?= $daci['aci_nom'] ?> <?= $daci['aci_code'] ?></td>
                                                                                            <td> <?= $societe_interco['soc_nom'] ?></td>
                                                                                            <td><button type="button" id="suppr_interco" data-aci="<?= $daci['aci_id'] ?>" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                </tbody>
                                                                            </table>
                                                                        <?php } else { ?>
                                                                            <p class="alert alert-info">Pas d'action associée</p>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt10">
                                                                    <div class="col-md-12 text-right">


                                                                        <a href="action_client_interco.php?action=<?= $action_id ?>&action_client=<?= $action_client['acl_id'] ?>&societ=<?= $conn_soc_id ?>" class="btn btn-success btn-sm">
                                                                            <i class="fa fa-plus"></i> Sélectionner une action
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="row mt10">
                                                            <div class="col-md-6">
                                                                Crée le <?= $action_client["acl_creation_date"] ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Par
                                                                <?php if (!empty($action_client["acl_creation_uti"])) {
                                                                        echo ($orion_utilisateurs[$action_client["acl_creation_uti"]]["identite"]);
                                                                    } elseif (!empty($action_client["acl_creation_ident"])) {
                                                                        echo ($action_client["acl_creation_ident"]);
                                                                    } ?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <?php            }
                                                // INFOS DES CLIENTS
                                                if (isset($client_info)) {
                                                    if (!empty($client_info)) { ?>
                                                    <div class="panel">
                                                        <div class="panel-heading panel-head-sm">
                                                            <span class="panel-title">Infos</span>
                                                        </div>
                                                        <div class="panel-body p5">
                                                            <?php foreach ($client_info as $i) {
                                                                    $date_info = "";
                                                                    if (!empty($i["inf_date"])) {
                                                                        $DT_periode = date_create_from_format('Y-m-d', $i["inf_date"]);
                                                                        if (!is_bool($DT_periode)) {
                                                                            $date_info = $DT_periode->format("d/m/Y");
                                                                        }
                                                                    }


                                                                    echo ("<div class='mb10' >");
                                                                    echo ("<p>");
                                                                    echo ("<b>" . $infos_types[$i["inf_type"]] . "</b><br/>");
                                                                    echo ($i["inf_description"] . "<br/>");
                                                                    echo ($orion_utilisateurs[$i["inf_auteur"]]["identite"] . " le " . $date_info);
                                                                    echo ("</p>");
                                                                    echo ("</div>");
                                                                } ?>
                                                        </div>
                                                    </div>
                                            <?php        }
                                                } ?>
                                        </div>
                                        <!-- FIN COL 1 -->

                                        <!-- COL 2 -->
                                        <div class="col-xl-5 col-md-6">

                        <?php               if (!empty($action["act_verrou_admin"])) { ?>
                                                <p class="alert alert-danger" id="verrou_admin">
                                                    Cette action fait l'objet d'un blocage administratif!
                                                    <button class="btn btn-system btn-sm pull-right" data-toggle="modal" data-target="#modal_aide" >
                                                        <i class="fa fa-info" ></i>
                                                    </button>
                                                </p>
                        <?php               }
                                            if (!empty($action["act_verrou_marge"])) {  ?>
                                                <div id="verrou_marge">
                                                    <p class="alert alert-danger" >
                                                        <b>CA de référence pour la marge : <?=$action["act_marge_ca"]?> €.</b><br/>
                                                        Le CA validé est inférieur au CA de référence. Dérogation "compta" nécéssaire!
                                                        
                                                    </p>
                                                </div>
                            <?php           } ?>

                                            <div class="panel">
                                                <div class="panel-heading panel-head-sm" <?= $action_bg ?>>                                                   
                                                    <span class="panel-title text-dark ellipsis ">
                                                <?php   echo ("Action N° " . $action["act_id"]);
                                                        echo (" " . $action_head); ?>
                                                    </span>
                                            <?php   if ($drt_edit_action) { ?>
                                                        <div class="pull-right">
                                                            <a href="action_mod.php?action=<?= $action_id ?>&societ=<?= $conn_soc_id ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Editer l'action">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </div>
                                            <?php   } ?>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="admin-form">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <b>Adresse d'intervention :</b><br />
                                                                <?= $adresse ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <b>Contact sur site :</b><br />
                                                                <?php
                                                                    if (!empty($action["act_con_nom"]) or !empty($action["act_con_prenom"])) {
                                                                        echo ($action["act_con_prenom"] . " " . $action["act_con_nom"] . "<br/>");
                                                                    }
                                                                    if (!empty($action["act_con_tel"])) {
                                                                        echo ($action["act_con_tel"] . "<br/>");
                                                                    }
                                                                    if (!empty($action["act_con_portable"])) {
                                                                        echo ($action["act_con_portable"] . "<br/>");
                                                                    } ?>
                                                            </div>
                                                        </div>
                                                        <div class="row mt10">
                                                            <?php if (isset($d_vehicule)) { ?>
                                                                <div class="col-md-3 text-right">Véhicule :</div>
                                                                <div class="col-md-3"><?= $d_vehicule["veh_libelle"] ?></div>
                                                            <?php    }
                                                                if (isset($d_salle)) { ?>
                                                                <div class="col-md-3 text-right">Salle :</div>
                                                                <div class="col-md-3"><?= $d_salle["sal_nom"] ?></div>
                                                            <?php    } ?>
                                                        </div>
                                                        <?php   if ($action["act_alerte_jour"] > 0) { ?>
                                                            <div class="row mt10">
                                                                <div class="col-md-3 text-right">Alerte :</div>
                                                                <div class="col-md-3"><?= $action["act_alerte_jour"] ?> jour(s)</div>
                                                                <div class="col-md-4 text-right">Alerte traitée :</div>
                                                                <div class="col-md-2">
                                                                    <?php if ($action["act_alerte_ok"] == 1) {
                                                                            echo ("<i class='fa fa-check-square-o' ></i>");
                                                                        } else {
                                                                            echo ("<i class='fa fa-square-o' ></i>");
                                                                        } ?>
                                                                </div>
                                                            </div>
                                            <?php       } ?>
                                                        <div class="row mt10">
                                                            <div class="col-md-3 text-right">Dossier formateur :</div>
                                                            <div class="col-md-3">
                                                                <?php if ($action["act_dossier_ok"] == 1) {
                                                                        echo ("<i class='fa fa-check-square-o' ></i>");
                                                                    } else {
                                                                        echo ("<i class='fa fa-square-o' ></i>");
                                                                    } ?>
                                                            </div>
                                                            <div class="col-md-4 text-right">Dossier traité le :</div>
                                                            <div class="col-md-2">
                                                                <span id="action_dossier_date">
                                                                    <?= convert_date_txt($action["act_dossier_date"]) ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                            <!--
                                                                    <div class="row mt10" >
                                                                        <div class="col-md-3 text-right" >Registre présent :</div>
                                                                        <div class="col-md-3" >
                                                                            <label class="option">
                                                                                <input type="checkbox" id="action_registre" <?php if ($action["act_registre"]) echo ("checked"); ?> />
                                                                                <span class="checkbox"></span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-md-3 text-right" >Registre signé :</div>
                                                                        <div class="col-md-3" >
                                                                            <label class="option">
                                                                                <input type="checkbox" id="action_registre_signe" <?php if ($action["act_registre_signe"]) echo ("checked"); ?> />
                                                                                <span class="checkbox"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>	 -->
                                                        <div class="row mt10">
                                                            <div class="col-md-3 text-right">Texte planning :</div>
                                                            <div class="col-md-3"><?= $action["act_planning_txt"] ?></div>
                                                        </div>

                                                        <div class="row mt10">
                                                            <div class="col-md-3 text-right">Inscription des stagiaires :</div>
                                                            <div class="col-md-3">
                                                                <?php if ($action["act_gest_sta"] == 1) {
                                                                        echo ("Formation complète");
                                                                    } else {
                                                                        echo ("Session");
                                                                    } ?>
                                                            </div>
                                                            <div class="col-md-4 text-right">Nombre d'inscrits :</div>

                                                            <div class="col-md-1 text-success">
                                                                <span><?= $total_conf ?></span>
                                                            </div>
                                                            <div class="col-md-1 text-warning">
                                                                <span><?= $total_resa ?></span>
                                                            </div>
                                                        </div>

                                                <?php   if ($action["act_pro_sous_famille"] == 14) { ?>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="section-divider mb40">
                                                                        <span>Spécifique SST</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row mt10">
                                                                <div class="col-md-2 text-right">Dossier traité :</div>
                                                                <div class="col-md-2">
                                                                    <?php if (!empty($action["act_forprev_ok"])) {
                                                                            echo ("<i class='fa fa-check-square-o' ></i>");
                                                                        } else {
                                                                            echo ("<i class='fa fa-square-o' ></i>");
                                                                        } ?>
                                                                </div>
                                                                <div class="col-md-2 text-right">Le :</div>
                                                                <div class="col-md-2"><?= $action["act_forprev_date"] ?></div>
                                                                <div class="col-md-2 text-right">N° dossier :</div>
                                                                <div class="col-md-2"><?= $action["act_forprev_num"] ?></div>
                                                            </div>
                                            <?php       } elseif ($action["act_pro_sous_famille"] == 1) { ?>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="section-divider mb40">
                                                                        <span>Spécifique SSIAP</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row mt10">
                                                                <div class="col-md-3 text-right">Dossier SSIAP :</div>
                                                                <div class="col-md-9"><?= $action["act_forprev_num"] ?></div>
                                                            </div>
                                                <?php   } ?>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="section-divider mb40">
                                                                    <span>Commentaire</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row mt10">
                                                            <div class="col-md-12">
                                                                <?= nl2br($action["act_commentaire"]) ?>
                                                            </div>
                                                        </div>

                                            <?php       if ($action["act_archive"]) { ?>
                                                            <div class="row mt10 alert alert-danger" id="info_act_archive_<?= $action["act_id"] ?>">
                                                                <div class="col-md-5">
                                                                    Action archivée le <?= $action["act_archive_date"] ?>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    Par
                                                                    <?php if (!empty($action["act_archive_uti"])) {
                                                                            echo ($orion_utilisateurs[$action["act_archive_uti"]]["identite"]);
                                                                        } elseif (!empty($action["act_archive_ident"])) {
                                                                            echo ($action["act_archive_ident"]);
                                                                        } ?>
                                                                </div>
                                                                <div class="col-md-3" id="act_archive_ok_<?= $action["act_id"] ?>">
                                                                    <?php if ($action["act_archive_ok"]) {
                                                                            echo ("Confirmé");
                                                                        } else {
                                                                            echo ("Non confirmé");
                                                                        } ?>
                                                                </div>
                                                            </div>
                                                <?php       if (!$action["act_archive_ok"] and $_SESSION["acces"]["acc_droits"][11]) { ?>
                                                                <div class="row mt10" id="btn_act_archive_<?= $action["act_id"] ?>">
                                                                    <div class="col-md-6 text-center">
                                                                        <button type="button" class="btn btn-sm btn-danger cli-archive" data-type="2" data-action="<?= $action["act_id"] ?>">
                                                                            <i class="fa fa-times"></i> Annuler l'archivage
                                                                        </button>
                                                                    </div>
                                                                    <div class="col-md-6 text-center">
                                                                        <button type="button" class="btn btn-sm btn-success cli-archive" data-type="1" data-action="<?= $action["act_id"] ?>">
                                                                            <i class="fa fa-times"></i> Confirmer l'archivage
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                     <?php  }
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>

                                         <?php foreach ($action_clients as $ac) {
                                                if ($ac["acl_id"] != $action_client_id) {

                                                    $drt_modif = false;
                                                    if (!$action["act_archive"]) {
                                                        if (!$ac["acl_archive"] or ($_SESSION["acces"]["acc_droits"][10] and !$ac["acl_archive_ok"]) or $_SESSION["acces"]["acc_droits"][11]) {
                                                            $drt_modif = true;
                                                        }
                                                    }

                                                    if (!isset($ac["nb_resa"])) {
                                                        $ac["nb_resa"] = 0;
                                                    }
                                                    if (!isset($ac["nb_confirme"])) {
                                                        $ac["nb_confirme"] = 0;
                                                    }

                                                    $class_panel = "";
                                                    if ($ac["acl_archive"]) {
                                                        $class_panel = "panel-dark";
                                                    } elseif ($ac["acl_confirme"]) {
                                                        $class_panel = "panel-success";
                                                    } else {
                                                        $class_panel = "panel-warning";
                                                    } ?>

                                                 <div class="panel mn mt25 <?= $class_panel ?>">
                                                     <div class="panel-heading panel-head-sm">
                                                         <span class="panel-title"><b><?= $ac["cli_code"] ?> - <?= $ac["acl_pro_reference"] ?></b></span>
                                                         <div class="pull-right">
                                                             <a href="action_voir.php?action=<?= $action_id ?>&action_client=<?= $ac["acl_id"] ?>&societ=<?= $conn_soc_id ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Afficher les infos du client">
                                                                 <i class="fa fa-search"></i>
                                                             </a>
                                                             <?php if ($drt_edit_action) { ?>
                                                                 <a href="action_client.php?action=<?= $action_id ?>&action_client=<?= $ac["acl_id"] ?>&societ=<?= $conn_soc_id ?>" class="btn btn-warning btn-sm<?php if (!$drt_modif) echo (" disabled-archive"); ?>" data-toggle="tooltip" data-placement="top" title="Editer les infos du client" <?php if (!$drt_modif) echo ("disabled") ?>>
                                                                     <i class="fa fa-pencil"></i>
                                                                 </a>
                                                             <?php        } ?>
                                                         </div>
                                                     </div>
                                                     <div class="panel-body">
                                                         <div class="admin-form">
                                                             <div class="row">
                                                                 <div class="col-lg-3 text-right"><b>Nom :</b></div>
                                                                 <div class="col-lg-9"><b><?= $ac["acl_pro_libelle"] ?></b></div>
                                                             </div>

                                                             <div class="row mt10">
                                                                 <div class="col-md-3 text-right"><b>Commercial :</b></div>
                                                                 <div class="col-md-3"><?= $tab_com[$ac["acl_commercial"]] ?></div>
                                                             </div>
                                                             <?php if ($aff_ca) {
                                                                    if (!empty($action_client["acl_pro_inter"])) { ?>
                                                                     <div class="row mt10">
                                                                         <div class="col-md-4 text-right"><b>Nb. Sta. confirmé(s) :</b></div>
                                                                         <div class="col-md-2 text-success"><?= $ac["acl_for_nb_sta"] ?></div>
                                                                         <div class="col-md-4 text-right"><b>Nb. Sta. résa. :</b></div>
                                                                         <div class="col-md-2 text-warning"><?= $ac["acl_for_nb_sta_resa"] ?></div>
                                                                     </div>
                                                                     <div class="row mt10">
                                                                         <div class="col-md-4 text-right"><b>CA confirmé :</b></div>
                                                                         <div class="col-md-2 text-success"><?= $ac["acl_ca"] ?> €</div>
                                                                         <div class="col-md-4 text-right"><b>CA projet :</b></div>
                                                                         <div class="col-md-2 text-warning"><?= $ac["acl_ca_unit"] * $ac["acl_for_nb_sta_resa"] ?> €</div>
                                                                     </div>
                                                                 <?php    } else { ?>
                                                                     <div class="row mt10">
                                                                         <div class="col-md-4 text-right"><b>CA :</b></div>
                                                                         <div class="col-md-2"><?= $ac["acl_ca"] ?> €</div>
                                                                     </div>
                                                             <?php    }
                                                                } ?>


                                                             <div class="row mt5">
                                                                 <div class="col-md-4 text-right mt5"><b>Confirmée :</b></div>
                                                                 <div class="col-md-2">
                                                                     <?php if (!empty($ac["acl_confirme"])) {
                                                                            echo ("<i class='fa fa-check-square-o' ></i>");
                                                                        } else {
                                                                            echo ("<i class='fa fa-square-o' ></i>");
                                                                        } ?>
                                                                 </div>
                                                                 <?php if (!empty($ac["acl_confirme"])) { ?>
                                                                     <div class="col-md-6">Le <?= $ac["acl_confirme_date"] ?></div>
                                                                 <?php    } ?>
                                                             </div>
                                                             <div class="row mt5">
                                                                 <div class="col-md-4 text-right mt5"><b>Nb. Inscrit :</b></div>
                                                                 <div class="col-md-2 text-success"><?= $ac["nb_confirme"] ?></div>
                                                                 <div class="col-md-4 text-right mt5"><b>Nb. Résa. :</b></div>
                                                                 <div class="col-md-2 text-warning"><?= $ac["nb_resa"] ?></div>

                                                             </div>
                                                             <?php if (!empty($ac["acl_non_fac"])) {
                                                                    echo ("<p class='alert alert-danger text-center' ><b>Action NON FACTURABLE</b></p>");
                                                                } ?>
                                                             <div class="row">
                                                                 <div class="col-md-12">
                                                                     <div class="section-divider mb10">
                                                                         <span style="background:white;">Avis de passage</span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                             <?php if (empty($ac["acl_avis_emis"])) {
                                                                    // avis non émit
                                                                ?>

                                                                 <div class="row mt10">

                                                                     <div class="col-md-10">
                                                                         <p class="alert alert-warning">
                                                                             Avis de passage non émis.
                                                                         </p>
                                                                     </div>
                                                                     <div class="col-md-2 pt10">
                                                                         <a href="mail_prep.php?action=<?= $action_id ?>&action_client=<?= $ac['acl_id'] ?>&societ=<?= $acc_societe ?>&avis&action_retour=<?= $action_client_id ?>" class="btn btn-sm btn-info" data-toggle="tooltip" title="Envoyer l'avis de passage">
                                                                             <i class="fa fa-envelope-o"></i>
                                                                         </a>
                                                                     </div>
                                                                 </div>
                                                             <?php        } else { ?>
                                                                 <div class="row mt10">

                                                                     <div class="col-md-5">
                                                                         <p class="alert alert-success">
                                                                             Avis émis le <?= $ac["acl_avis_emis_date_fr"] ?>.
                                                                         </p>
                                                                     </div>
                                                                     <div class="col-md-1 pt10">
                                                                         <a href="mail_prep.php?action=<?= $action_id ?>&action_client=<?= $ac['acl_id'] ?>&societ=<?= $acc_societe ?>&avis&action_retour=<?= $action_client_id ?>" class="btn btn-sm btn-info" data-toggle="tooltip" title="Renvoyer l'avis de passage">
                                                                             <i class="fa fa-envelope-o"></i>
                                                                         </a>
                                                                     </div>
                                                                     <div class="col-md-6">
                                                                         <?php if (empty($ac["acl_avis_recu"])) {         ?>
                                                                             <p class="alert alert-warning">
                                                                                 Réception non confirmée.
                                                                             </p>
                                                                         <?php    } else { ?>
                                                                             <p class="alert alert-success">
                                                                                 Réception confirmée le <?= $ac["acl_avis_recu_date_fr"] ?>.
                                                                             </p>
                                                                         <?php    } ?>
                                                                     </div>
                                                                 </div>
                                                             <?php        } ?>
                                                             <div class="row mt10">
                                                                 <div class="col-md-6">
                                                                     Crée le <?= $ac["acl_creation_date"] ?>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     Par
                                                                     <?php if (!empty($ac["acl_creation_uti"])) {
                                                                            echo ($orion_utilisateurs[$ac["acl_creation_uti"]]["identite"]);
                                                                        } elseif (!empty($ac["acl_creation_ident"])) {
                                                                            echo ($ac["acl_creation_ident"]);
                                                                        } ?>
                                                                 </div>
                                                             </div>

                                                             <?php if ($ac["acl_archive"]) { ?>
                                                                 <div class="row mt10 alert alert-danger" id="info_acl_archive_<?= $ac["acl_id"] ?>">
                                                                     <div class="col-md-5">
                                                                         Action archivée le <?= $ac["acl_archive_date"] ?>
                                                                     </div>
                                                                     <div class="col-md-4">
                                                                         Par
                                                                         <?php if (!empty($ac["acl_archive_uti"])) {
                                                                                echo ($orion_utilisateurs[$ac["acl_archive_uti"]]["identite"]);
                                                                            } elseif (!empty($ac["acl_archive_ident"])) {
                                                                                echo ($ac["acl_archive_ident"]);
                                                                            } ?>
                                                                     </div>
                                                                     <div class="col-md-3" id="acl_archive_ok_<?= $ac["acl_id"] ?>">
                                                                         <?php if ($ac["acl_archive_ok"]) {
                                                                                echo ("Confirmé");
                                                                            } else {
                                                                                echo ("Non confirmé");
                                                                            } ?>
                                                                     </div>
                                                                 </div>
                                                                 <?php if (!$ac["acl_archive_ok"] and $_SESSION["acces"]["acc_droits"][11]) { ?>
                                                                     <div class="row mt10" id="btn_acl_archive_<?= $ac["acl_id"] ?>">
                                                                         <div class="col-md-6 text-center">
                                                                             <button type="button" class="btn btn-sm btn-danger cli-archive<?php if ($action["act_archive"]) echo (" disabled-archive"); ?>" data-type="2" data-action_client="<?= $ac["acl_id"] ?>" <?php if ($action["act_archive"]) echo ("disabled"); ?>>
                                                                                 <i class="fa fa-times"></i> Annuler l'archivage
                                                                             </button>
                                                                         </div>
                                                                         <div class="col-md-6 text-center">
                                                                             <button type="button" class="btn btn-sm btn-success cli-archive" data-type="1" data-action_client="<?= $ac["acl_id"] ?>">
                                                                                 <i class="fa fa-times"></i> Confirmer l'archivage
                                                                             </button>
                                                                         </div>
                                                                     </div>
                                                             <?php        }
                                                                } ?>
                                                         </div>
                                                     </div>
                                                 </div>


                                                 <?php    // INFOS DES CLIENTS
                                                    $client_info = get_client_infos($ac["cli_id"], 1, $action["act_pro_famille"], $action["act_pro_sous_famille"], $action["act_pro_sous_sous_famille"]);

                                                    if (!empty($client_info)) { ?>
                                                     <div class="panel mn">
                                                         <div class="panel-heading panel-head-sm">
                                                             <span class="panel-title">
                                                                 <?php echo ($ac["cli_code"] . " - Infos"); ?>

                                                             </span>
                                                             <span class="pull-right">
                                                                 <button class="btn btn-default btn-sm btn-aff-mas" data-cible="bloc_infos_<?= $ac["cli_id"] ?>">
                                                                     <i class="fa fa-plus"></i>
                                                                 </button>
                                                             </span>

                                                         </div>

                                                         <div class="panel-body" id="bloc_infos_<?= $ac["cli_id"] ?>" style="display:none;">
                                                             <?php foreach ($client_info as $i) {
                                                                    $date_info = "";
                                                                    if (!empty($i["inf_date"])) {
                                                                        $DT_periode = date_create_from_format('Y-m-d', $i["inf_date"]);
                                                                        if (!is_bool($DT_periode)) {
                                                                            $date_info = $DT_periode->format("d/m/Y");
                                                                        }
                                                                    }


                                                                    echo ("<div class='mb25' >");
                                                                    echo ("<p>");
                                                                    echo ("<b>" . $infos_types[$i["inf_type"]] . "</b><br/>");
                                                                    echo ($i["inf_description"] . "<br/>");
                                                                    echo ($orion_utilisateurs[$i["inf_auteur"]]["identite"] . " le " . $date_info);
                                                                    echo ("</p>");
                                                                    echo ("</div>");
                                                                } ?>
                                                         </div>
                                                     </div>
                                         <?php        }
                                                }
                                            } ?>
                                     </div>
                                     <!-- FIN COL 2 -->

                                     <!-- COL 3 -->
                                     <div class="col-xl-3 col-md-12">
                                         <!-- DATE ET HORRAIRE -->

                                         <?php    //echo("<pre>");
                                            //	print_r($action_client);
                                            //echo("</pre>");
                                            //die();
                                            //die();

                                            if (!empty($action_dates)) { ?>
                                             <div class="panel">
                                                 <div class="panel-heading panel-head-sm">
                                                     <span class="panel-title">Dates / Horaires</span>
                                                     <?php if ($drt_edit_action) { ?>
                                                         <div class="pull-right">
                                                             <a href="action_date.php?action=<?= $action_id ?>&action_client=<?= $action_client_id ?>&societ=<?= $conn_soc_id ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Editer l'action">
                                                                 <i class="fa fa-pencil"></i>
                                                             </a>
                                                         </div>
                                                     <?php        } ?>
                                                 </div>
                                                 <div class="panel-body">
                                                     <table class="table">
                                                         <thead>

                                                         </thead>
                                                         <tbody>
                                                             <?php $date_id = 0;
                                                                foreach ($action_dates as $ad) {
                                                                    $date = new DateTime($ad["pda_date"]);

                                                                    $h = "";
                                                                    if (!empty($ad["ase_h_deb"]) && !empty($ad["ase_h_fin"])) {
                                                                        $h = time_txt($ad["ase_h_deb"]) . " - " . time_txt($ad["ase_h_fin"]);
                                                                    } else {
                                                                        if ($ad["pda_demi"] == 1) {
                                                                            $h = "Matin";
                                                                        } else {
                                                                            $h = "Après-midi";
                                                                        }
                                                                    }

                                                                    $classe = "";
                                                                    $classe_tr = "";
                                                                    $inscrit = true;
                                                                    if (empty($ad["acd_action_client"])) {
                                                                        $classe_tr = "class='text-muted'";
                                                                        $inscrit = false;
                                                                    } ?>
                                                                 <tr <?= $classe_tr ?>>
                                                                     <?php if ($date_id != $ad["pda_id"]) { ?>
                                                                         <td>&nbsp;</td>
                                                                     <?php        } else {
                                                                            echo ("<td>&nbsp;</td>");
                                                                        } ?>
                                                                     <td><?= $ad["int_label_1"] ?></td>
                                                                     <td><?= $date->format("d/m/Y") ?></td>
                                                                     <td><?= $h ?></td>
                                                                     <td class="text-warning"><?= $ad["pda_sta_resa"] ?></td>
                                                                     <td class="text-success"><?= $ad["pda_sta_conf"] ?></td>
                                                                 </tr>
                                                             <?php $date_id = $ad["pda_id"];
                                                                } ?>
                                                         </tbody>
                                                     </table>

                                                 </div>
                                             </div>
                                         <?php    }

                                            if (!empty($d_sous_traitants)) { ?>
                                             <div class="panel">
                                                 <div class="panel-heading panel-head-sm">
                                                     <span class="panel-title">Sous-Traitants</span>
                                                 </div>
                                                 <div class="panel-body">
                                                     <table class="table">
                                                         <tbody>
                                                    <?php   foreach ($d_sous_traitants as $st) { ?>
                                                                <tr>
                                                                    <td><?= $st["fou_code"] ?></td>
                                                             <?php  if (empty($st["com_id"])) { ?>
                                                                         <td colspan="2" >
                                                                             <a href="commande_enr.php?action=<?= $action_id ?>&fournisseur=<?= $st["fou_id"] ?>" class="btn btn-sm btn-success">
                                                                                 <i class="fa fa-plus"></i>
                                                                             </a>
                                                                         </td>
                                                             <?php  } else { ?>
                                                                         <td>
                                                                             <a href="commande_voir.php?commande=<?= $st["com_id"] ?>">
                                                                                 <?= $st["com_numero"] ?>
                                                                             </a>
                                                                         </td>
                                                                         <td>
                                                                            <?= $st["com_ht"] ?>
                                                                         </td>
                                                         <?php      } ?>
                                                                </tr>
                                        <?php               } ?>
                                                         </tbody>
                                                     </table>

                                                 </div>
                                             </div>
                                         <?php    } ?>

                                         <!-- DOCUMENTS -->

                                         <!--<div class="panel">
													<div class="panel-heading panel-head-sm">
														<span class="panel-title">Documents</span>
													</div>
													<div class="panel-body">
														<table class="table" >
															<tbody>
																<tr>
																	<td class="text-right" >
																		<a href="#" class="document" data-nom="Dcument 1" data-descriptif="DESCRIPTIF DU DOCUMENT 1" data-url="documents/etch/" >
																			Dcument 1 :
																		</a>
																	</td>
																	<td>
																		<a href="#" class="document" data-nom="Dcument 1" data-descriptif="DESCRIPTIF DU DOCUMENT 1" data-url="documents/etch/" >
																			<img src="assets/img/icons/pdf_min.png" />
																		</a>
																	</td>
																	<td>01/08/2016</td>
																</tr>
																<tr>
																	<td class="text-right" >
																		<a href="#" class="document" data-nom="Dcument 2" data-descriptif="DESCRIPTIF DU DOCUMENT 2" data-url="documents/etch/" >
																			doc 2 :
																		</a>
																	</td>
																	<td>
																		<a href="#" class="document" data-nom="Dcument 2" data-descriptif="DESCRIPTIF DU DOCUMENT 2" data-url="documents/etch/" >
																			<img src="assets/img/icons/doc_min.png" />
																		</a>
																	</td>
																	<td>02/08/2016</td>
																</tr>
																<tr>
																	<td class="text-right" >
																		<a href="#" class="document" data-nom="Dcument 3" data-descriptif="DESCRIPTIF DU DOCUMENT 3" data-url="documents/etch/" >
																			Modèle document 3 :
																		</a>
																	</td>
																	<td>
																		<a href="#" class="document" data-nom="Dcument 3" data-descriptif="DESCRIPTIF DU DOCUMENT 3" data-url="documents/etch/" >
																			<img src="assets/img/icons/xls_min.png" />
																		</a>
																	</td>
																	<td>03/08/2016</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>-->
											</div>
											<!-- FIN COL 3 -->
										</div>
									</div>
									<!-- FIN CONTENU GENERAL -->

						<?php		if($drt_edit_action){ ?>
										<div id="action_edition" class="tab-pane <?php if($onglet==3) echo("active");?>" >

                                            <div class="admin-form theme-primary ">
                                                <form method="post" action="ajax/ajax_action_doc.php" id="form_doc" enctype="multipart/form-data">
                                                    <div>
                                                        <input type="hidden" name="action" value="<?=$action_id?>">
                                                        <input type="hidden" name="societ" value="<?=$conn_soc_id?>">
                                                    </div>
                                                    <table class="table" >
                                                        <caption class="text-danger" >Documents interne</caption>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center" >Dossier</th>
                                                                <th class="text-center" >Feuilles de présence</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="text-center">
                                                                    <a href="action_dossier_voir.php?action=<?=$action_id?>&societ=<?=$conn_soc_id?>" class="btn btn-info btn-sm" >
                                                                        <i class="fa fa-eye" ></i>
                                                                    </a>
                                                                </td>
                                                                <td class="text-center">
                                                        <?php		if($action["act_gest_sta"]==1){ ?>
                                                                        <a href="presence_conso.php?action=<?=$action_id?>&societ=<?=$conn_soc_id?>" class="btn btn-info btn-sm" >
                                                                            <i class="fa fa-eye" ></i>
                                                                        </a>
                                                        <?php		}else{ ?>
                                                                        <a href="presence.php?action=<?=$action_id?>&societ=<?=$conn_soc_id?>" class="btn btn-info btn-sm" >
                                                                            <i class="fa fa-eye" ></i>
                                                                        </a>
                                                        <?php 		} ?>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <table class="table" >
                                                        <caption class="text-danger" >Documents client</caption>
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="2" >Client</th>
                                                                <th colspan="3" class="text-center" >Conventions</th>
                                                                <th colspan="3" class="text-center" >Convocations</th>
                                                                <th class="text-center" rowspan="2" >Feuilles de présence</th>
                                                                <th class="text-center" rowspan="2" >Avis de stage</th>
                                                                <th colspan="3" class="text-center" >Att. individuel</th>
                                                                <th colspan="3" class="text-center" >Att. groupée / session</th>
                                                                <th colspan="3" class="text-center" >Att. groupée</th>
                                                                <th class="text-center" rowspan="2" >Mail</th>
                                                            </tr>
                                                            <tr>
                                                                <!--Convention -->
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <div class="option-group field">
                                                                        <label class="option option-dark">
                                                                            <input type="checkbox" class="check-all" id="convention_all" value="" />
                                                                            <span class="checkbox"></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>PDF</td>

                                                                <!--Convoc-->
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <div class="option-group field">
                                                                        <label class="option option-dark">
                                                                            <input type="checkbox" class="check-all" id="convocation_all" value="" />
                                                                            <span class="checkbox"></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>PDF</td>

                                                                <!--Att. ind-->
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <div class="option-group field">
                                                                        <label class="option option-dark">
                                                                            <input type="checkbox" class="check-all" id="attest_ind_all" value="" />
                                                                            <span class="checkbox"></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>PDF</td>

                                                                <!--Att. session-->
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <div class="option-group field">
                                                                        <label class="option option-dark">
                                                                            <input type="checkbox" class="check-all" id="attest_ses_all" value="" />
                                                                            <span class="checkbox"></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>PDF</td>

                                                                <!--Att. groupée-->
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <div class="option-group field">
                                                                        <label class="option option-dark">
                                                                            <input type="checkbox" class="check-all" id="attest_form_all" value="" />
                                                                            <span class="checkbox"></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>PDF</td>
                                                            </tr>
                                                        </thead>
                                        <?php			if(!empty($action_clients)){ ?>
                                                            <tbody>
                                        <?php					foreach($action_clients as $ac){
                                                                    if(empty($ac["acl_archive"])){?>
                                                                        <tr>
                                                                            <td>
                                                                                N° <?=$ac["acl_id"]?> - <?=$ac["cli_code"]?>
                                                                                <span id="cli_nom_<?=$ac["acl_id"]?>" ><?=$ac["cli_nom"]?></span>
                                                                            </td>

                                                                    <?php	// CONVENTION
                                                                            if(empty($ac["con_id"])){ ?>
                                                                                <td colspan="3" >
                                                                                    <a href="convention_cree.php?action_client=<?=$ac["acl_id"]?>&societ=<?=$conn_soc_id?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Enregistrer la convention" >
                                                                                        <i class="fa fa-plus" ></i>
                                                                                    </a>
                                                                                </td>
                                                                    <?php	}else{ ?>
                                                                                <td>
                                                                                    <a href="convention_voir.php?convention=<?=$ac["con_id"]?>&societ=<?=$conn_soc_id?>" class="btn btn-sm btn-info" >
                                                                                        <i class="fa fa-eye" ></i>
                                                                                    </a>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="option-group field">
                                                                                        <label class="option option-dark">
                                                                                            <input type="checkbox" class="convention" name="convention_<?=$ac["con_id"]?>" value="<?=$ac["con_id"]?>">
                                                                                            <span class="checkbox"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </td>
                                                                                <td id="pdf_conv_<?=$ac["con_id"]?>" >
                                                                    <?php			if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $conn_soc_id . "/Conventions/" . $ac["con_numero"] . ".pdf")){ ?>
                                                                                        <a href="documents/Societes/<?=$acc_societe?>/Conventions/<?=$ac["con_numero"]?>.pdf?<?= time();?>" >
                                                                                            <i class='fa fa-file-pdf-o' ></i>
                                                                                        </a>
                                                                <?php				} ?>
                                                                                </td>
                                                                    <?php	}

                                                                            // CONVOC ?>
                                                                            <td>
                                                                                <a href="convocation_voir.php?action=<?=$action_id?>&action_client=<?=$ac["acl_id"]?>&societ=<?=$acc_societe?>" class="btn btn-sm btn-info" >
                                                                                    <i class="fa fa-eye" ></i>
                                                                                </a>
                                                                            </td>
                                                                            <td>
                                                                                <div class="option-group field">
                                                                                    <label class="option option-dark">
                                                                                        <input type="checkbox" class="convocation" name="convocation_<?=$ac["acl_id"]?>" value="<?=$ac["acl_id"]?>">
                                                                                        <span class="checkbox"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td id="pdf_convoc_<?=$ac["acl_id"]?>" >
                                                            <?php				if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Convocations/convoc_". $ac["acl_id"] . ".pdf")){ ?>
                                                                                    <a href="documents/Societes/<?=$acc_societe?>/Convocations/convoc_<?=$ac["acl_id"]?>.pdf?<?= time();?>" download="Convocation <?=$ac["cli_nom"]?>.pdf" >
                                                                                        <i class='fa fa-file-pdf-o' ></i>
                                                                                    </a>
                                                            <?php				} ?>
                                                                            </td>

                                                                        <!-- FEUILLE DE PRESENCE -->
                                                                            <td class="text-center" >
                                                                            <?php if($ac["cli_id"] == 16670 OR $ac['cli_filiale_de'] == 16670){
                                                                                // MONOPRIX
                                                                                ?>
                                                            <?php				if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Presences/monop_presence_". $ac["acl_id"] . ".pdf")){ ?>
                                                                                    <a href="documents/Societes/<?=$acc_societe?>/Presences/monop_presence_<?=$ac["acl_id"]?>.pdf?<?= time();?>" download="Feuille de présence <?=$ac["cli_nom"]?>.pdf" >
                                                                                        <i class='fa fa-file-pdf-o' ></i>
                                                                                    </a>
                                                            <?php				}else{ ?>
                                                                                        <input type="hidden" name="acl_id" value="<?= $ac["acl_id"] ?>">
                                                                                        <input type="hidden" name="acc_societe" value="<?= $acc_societe ?>">
                                                                                        <!-- COMPONENT START -->
                                                                                        <div class="form-group">
                                                                                            <div class="input-group input-file" name="file">
                                                                                                <span class="input-group-btn">
                                                                                                    <button class="btn btn-default btn-choose" type="button">Choisir</button>
                                                                                                </span>
                                                                                                <input type="text" class="form-control" required placeholder='Choisir un pdf...' accept="application/pdf"/>
                                                                                                <span class="input-group-btn">
                                                                                                    <button class="btn btn-warning btn-reset" type="button">RAZ</button>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- COMPONENT END -->
                                                                                        <div class="form-group">
                                                                                            <button type="submit" name="monop" class="btn btn-primary pull-right">Charger</button>
                                                                                        </div>
                                                                        <?php   }?>
                                                                    <?php   }else{ ?>
                                                            <?php				if($action["act_gest_sta"]==1){ ?>
                                                                                    <a href="presence_conso.php?action=<?=$action_id?>&action_client=<?=$ac["acl_id"]?>&societ=<?=$acc_societe?>" class="btn btn-sm btn-info" >
                                                                                        <i class="fa fa-eye" ></i>
                                                                                    </a>
                                                            <?php				}else{ ?>
                                                                                    <a href="presence.php?action=<?=$action_id?>&action_client=<?=$ac["acl_id"]?>&societ=<?=$acc_societe?>" class="btn btn-sm btn-info" >
                                                                                        <i class="fa fa-eye" ></i>
                                                                                    </a>
                                                            <?php				} ?>
                                                            <?php           } ?>
                                                                            </td>
                                                                            <td class="text-center" >
                                                                                <a href="avis_voir.php?action=<?=$action_id?>&action_client=<?=$ac["acl_id"]?>&societ=<?=$acc_societe?>" class="btn btn-sm btn-info" >
                                                                                    <i class="fa fa-eye" ></i>
                                                                                </a>
                                                                            </td>
                                                                        <!--	<td>
                                                                                <div class="option-group field">
                                                                                    <label class="option option-dark">
                                                                                        <input type="checkbox" name="presence_conso_<?=$ac["acl_id"]?>" value="<?=$ac["acl_id"]?>">
                                                                                        <span class="checkbox"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td id="pdf_presence_<?=$ac["acl_id"]?>" >
                                                            <?php				if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Presences/presence_" . $action_id . "_". $ac["acl_id"] . ".pdf")){ ?>
                                                                                    <a href="documents/Societes/<?=$acc_societe?>/Presences/presence_<?=$action_id?>_<?=$ac["acl_id"]?>.pdf" download="Feuille de présence <?=$ac["cli_nom"]?>.pdf" >
                                                                                        <i class='fa fa-file-pdf-o' ></i>
                                                                                    </a>
                                                            <?php				} ?>
                                                                            </td>-->

                                                                            <!-- ATTEST IND -->
                                                                            <td class="text-center" >
                                                                                <a href="attestation_ind_voir.php?action=<?=$action_id?>&action_client=<?=$ac["acl_id"]?>&societ=<?=$acc_societe?>" class="btn btn-sm btn-info" >
                                                                                    <i class="fa fa-user" ></i>
                                                                                </a>
                                                                            </td>
                                                                            <td>
                                                                                <div class="option-group field">
                                                                                    <label class="option option-dark">
                                                                                        <input type="checkbox" name="attest_ind_<?=$ac["acl_id"]?>" class="attest-ind" value="<?=$ac["acl_id"]?>">
                                                                                        <span class="checkbox"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td id="pdf_attest_ind_<?=$ac["acl_id"]?>" >
                                                            <?php				if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Attestations/attestation_ind_" . $action_id . "_". $ac["acl_id"] . ".pdf")){ ?>
                                                                                    <a href="documents/Societes/<?=$acc_societe?>/Attestations/attestation_ind_<?=$action_id?>_<?=$ac["acl_id"]?>.pdf?<?= time();?>" download="Attestations individuelles <?=$ac["cli_nom"]?>.pdf" >
                                                                                        <i class='fa fa-file-pdf-o' ></i>
                                                                                    </a>
                                                            <?php				} ?>
                                                                            </td>


                                                                            <td class="text-center" >
                                                                                <a href="attestation_grp_voir.php?action=<?=$action_id?>&action_client=<?=$ac["acl_id"]?>&modele=2&societ=<?=$acc_societe?>" class="btn btn-sm btn-info" >
                                                                                    <i class="fa fa-users" ></i>
                                                                                </a>
                                                                            </td>
                                                                            <td>
                                                                                <div class="option-group field">
                                                                                    <label class="option option-dark">
                                                                                        <input type="checkbox" name="attest_ses_<?=$ac["acl_id"]?>" class="attest-ses" value="<?=$ac["acl_id"]?>">
                                                                                        <span class="checkbox"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td id="pdf_attest_ses_<?=$ac["acl_id"]?>" >
                                                            <?php				if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Attestations/attestation_ses_" . $action_id . "_". $ac["acl_id"] . ".pdf")){ ?>
                                                                                    <a href="documents/Societes/<?=$acc_societe?>/Attestations/attestation_ses_<?=$action_id?>_<?=$ac["acl_id"]?>.pdf?<?= time();?>" download="Attestation groupée <?=$ac["cli_nom"]?>.pdf" >
                                                                                        <i class='fa fa-file-pdf-o' ></i>
                                                                                    </a>
                                                            <?php				} ?>
                                                                            </td>

                                                                            <td class="text-center" >
                                                                                <a href="attestation_grp_voir.php?action=<?=$action_id?>&action_client=<?=$ac["acl_id"]?>&modele=1&societ=<?=$acc_societe?>" class="btn btn-sm btn-info" >
                                                                                    <i class="fa fa-users" ></i>
                                                                                </a>
                                                                            </td>
                                                                            <td>
                                                                                <div class="option-group field">
                                                                                    <label class="option option-dark">
                                                                                        <input type="checkbox" name="attest_form_<?=$ac["acl_id"]?>" class="attest-form" value="<?=$ac["acl_id"]?>">
                                                                                        <span class="checkbox"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td id="pdf_attest_form_<?=$ac["acl_id"]?>" >
                                                            <?php				if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $acc_societe . "/Attestations/attestation_form_" . $action_id . "_". $ac["acl_id"] . ".pdf")){ ?>
                                                                                    <a href="documents/Societes/<?=$acc_societe?>/Attestations/attestation_form_<?=$action_id?>_<?=$ac["acl_id"]?>.pdf" download="Attestations formation <?=$ac["cli_nom"]?>.pdf" >
                                                                                        <i class='fa fa-file-pdf-o' ></i>
                                                                                    </a>
                                                            <?php				} ?>
                                                                            </td>
                                                                            <td>
                                                                                <a href="mail_prep.php?action=<?=$action_id?>&action_client=<?=$ac["acl_id"]?>&societ=<?=$acc_societe?>" class="btn btn-sm btn-info" >
                                                                                    <i class="fa fa-envelope-o" ></i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                        <?php						}
                                                                } ?>
                                                            </tbody>
                                        <?php			} ?>
                                                    </table>
                                                </form>
                                            </div>
										</div>

						<?php		} ?>
								</div>
							</div>
						</section>
					</section>
				</div>

				<footer id="content-footer" class="affix">
					<div class="row">
						<div class="col-xs-3 footer-left">
					<?php	if(isset($_SESSION["retour_action"])){
								if(!empty($_SESSION["retour_action"])){ ?>
									<a href="<?=$_SESSION["retour_action"]?>" class="btn btn-default btn-sm" >
										Retour
									</a>
					<?php		}
							}
							if(!$consultation_gc){?>
								<a href="planning.php" class="btn btn-default btn-sm" >
									<i class="fa fa-calendar" ></i>
								</a>
								<!--<button class="btn btn-system btn-sm pull-right" data-toggle="modal" data-target="#modal_aide" >
									<i class="fa fa-info" ></i>
								</button>-->
				<?php		} ?>
						</div>

                        
						<div class="col-xs-6 footer-middle text-center">
			<?php			if(!$action["act_verrou_admin"]){ ?>
								<button type="button" class="btn btn-info btn-sm btn-ong btn-ong-3" id="btn_pdf" <?php if($onglet!=3) echo("style='display:none;'"); ?> >
									<i class="fa fa-refresh" ></i>
									Générer les PDF
								</button>
                <?php		} 
                            if (!empty($action["act_verrou_marge"]) AND $_SESSION["acces"]["acc_droits"][9]){ ?>
                                <button class="btn btn-sm btn-success" id="marge_valide">
                                    Valider la nouvelle marge
                                </button>
                <?php       } ?>
						</div>

						<div class="col-xs-3 footer-right">
				<?php		if($drt_edit_action){
								if(empty($action["act_archive"])){ ?>
									<a href="action_client.php?action=<?=$action_id?>&societ=<?=$acc_societe?>" class="btn btn-success btn-sm btn-ong btn-ong-1" <?php if($onglet!=1) echo("style='display:none;'"); ?> >
										<i class="fa fa-building-o" ></i>
									</a>
						<?php	}
							} ?>
						</div>
					</div>
				</footer>


	<!--		MODAL DOCUMENT -->
				<div id="modal_document" class="modal fade" role="dialog" >
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" id="mdl_doc_titre" ></h4>
							</div>
							<div class="modal-body" id="mdl_doc_texte" >

							</div>
							<div class="modal-footer">
								<a href="#" class="btn btn-success btn-sm" id="mdl_doc_lien" target="_blank" >
									<i class="fa fa-long-arrow-right" ></i>
									Télécharger
								</a>
								<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
									<i class="fa fa-close" ></i>
									Annuler
								</button>
							</div>
						</div>
					</div>
				</div>

	<!--		MODAL AIDE -->
				<div id="modal_aide" class="modal fade" role="dialog" >
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" id="mdl_doc_titre" >
									<i class="fa fa-info" ></i> - <span>Blocage administratif</span>                                   
								</h4>
							</div>
							<div class="modal-body" >
                                <p>Les motifs de blocage administratif sont les suivants :</p>
                                <ul>
                                    <li>Tous les sous-traitants ne disposent pas d'un BC validé.</li>
                                    <li>La marge générée par cette action est insuffisante et nécéssite une dérogation.</li>
                                </li>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
									<i class="fa fa-close" ></i>
									Fermer
								</button>
							</div>
						</div>
					</div>
				</div>

	<!--		MODAL CONFIRMATION -->
				<div id="modal_confirmation_user" class="modal fade" role="dialog" >
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" >
									<i class="fa fa-info" ></i> - <span>Confirmation</span>
								</h4>
							</div>
							<div class="modal-body" >

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
									<i class="fa fa-close" ></i>
									Annuler
								</button>
								<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
									<i class="fa fa-check" ></i>
									Confirmer
								</button>
							</div>
						</div>
					</div>
				</div>

		<?php
				include "includes/footer_script.inc.php"; ?>

				<script src="/vendor/plugins/select2/js/select2.min.js"></script>
				<script src="/vendor/plugins/mask/jquery.mask.js"></script>
				<script src="/assets/js/custom.js"></script>
				<script type="text/javascript" >
					var action="<?=$action_id?>";
                    function bs_input_file() {
                        $(".input-file").before(
                            function() {
                                if ( ! $(this).prev().hasClass('input-ghost') ) {
                                    var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                                    element.attr("name",$(this).attr("name"));
                                    element.change(function(){
                                        element.next(element).find('input').val((element.val()).split('\\').pop());
                                    });
                                    $(this).find("button.btn-choose").click(function(){
                                        element.click();
                                    });
                                    $(this).find("button.btn-reset").click(function(){
                                        element.val(null);
                                        $(this).parents(".input-file").find('input').val('');
                                    });
                                    $(this).find('input').css("cursor","pointer");
                                    $(this).find('input').mousedown(function() {
                                        $(this).parents('.input-file').prev().click();
                                        return false;
                                    });
                                    return element;
                                }
                            }
                        );
                    }
                    $(function() {
                        bs_input_file();
                    });
					jQuery(document).ready(function() {

						$(".onglet").click(function(){
							onglet=$(this).data("onglet");
							$(".btn-ong" ).each(function( index ){
								if($(this).hasClass("btn-ong-" + onglet)){
									$(this).show();
								}else{
									$(this).hide();
								}
							});

						});


						$(".cli-archive").click(function(){
							type=$(this).data("type");
							action_client=$(this).data("action_client");
							action=$(this).data("action");
							set_action_archive(action,action_client,type,actualiser_archive_client,"","");
						});

						$(".select2").select2();

						$(".document").click(function(){
							$("#mdl_doc_titre").html($(this).data('nom'));
							$("#mdl_doc_texte").html($(this).data('descriptif'));
							$("#mdl_doc_lien").prop("href",$(this).data('url'));
							$('#modal_document').modal('show');
						});

						$("#mdl_doc_lien").click(function(){
							$('#modal_document').modal('hide');
						});

						// ONGLET GENERALE

						$('.action-confirme').click(function(){
							var checked=$(this).prop("checked");
							var action_client=$(this).data("action_client");
							$.ajax({
								url : 'ajax/ajax_set_action_confirme.php',
								type : 'POST',
								data : 'action=' + action + '&action_client=' + action_client + '&checked=' + checked,
								dataType : 'html',
								success : function(code_html, statut){
									if(checked){
										var now = new Date();
										$("#confirm_" + action_client).html(now.getDate() + "/" + (1*now.getMonth()+1) + "/" + now.getFullYear());
									}else{
										$("#confirm_" + action_client).html("");
									}
								},
								error : function(resultat, statut, erreur){
									$(this).prop("checked",!checked);
								}
							});
						});

						//
						$("#convention_all").click(function(){
							if($("#convention_all").is(":checked")){
								if($(".convention").length==0){
									modal_alerte_user("Vous devez d'abord enregistrer des conventions avant de pouvoir les générer en PDF.","");
									$("#convention_all").prop("checked",false);
								}
							}
						});

						$("#btn_pdf").click(function(){
							envoyer_form_ajax($("#form_doc"),afficher_doc);
							$(this).html("<i class='fa fa-spinner fa-pulse fa-1x fa-fw'></i> Génération des PDF");
						});

                        $("#marge_valide").click(function(){
							valider_marge_dero(<?=$action_id?>,afficher_marge_dero,"","");
						});

						// ----- FIN DOC READY -----
					});
					(jQuery);

                    // supprimer stagiaire
                    function confirme_supp(aci) {
                        $.ajax({
                            type: 'POST',
                            url: 'ajax/ajax_action_interco_del.php',
                            data: 'aci=' + aci,
                            dataType: 'JSON',
                            success: function(data) {
                                supprimer_interco(aci);
                            },
                            error: function(data) {
                                modal_alerte_user(data.responseText, "");
                            }
                        });
                    }

                    function supprimer_interco(aci) {
                        $("#aci_" + aci).hide();
                        location.reload();
                    }


					// archive ou desarchive une action client ou une formation complete
					function set_action_archive(action,action_client,type,callback,param1,param2){
						$.ajax({
							type:'POST',
							url: 'ajax/ajax_action_archive_set.php',
							data : "action=" + action + "&action_client=" + action_client + "&type=" + type,
							dataType: 'json',
							success: function(data){
								callback(data,param1,param2);
							},
							error: function(data) {
								modal_alerte_user(data.responseText);
							}
						});
					}

					function actualiser_archive_client(json){
						if(json){
							if(json.type==1){

								// confirmation
								$.each(json.action_client, function(i, obj){
									$("#acl_archive_ok_" + obj).html("Confirmé");
									$("#bloc_acl_archive_" + obj).remove();
								});
								if(json.action){
									$("#act_archive_ok_" + json.action).html("Confirmé");
									$("#btn_act_archive_" + json.action).remove();
								}
							}else{

								if(json.action){
									$("#info_act_archive_" + json.action).remove();
									$("#btn_act_archive_" + json.action).remove();
									console.log($(".disabled-archive"));
									$(".disabled-archive").attr("disabled",false);
								}else{

									$.each(json.action_client, function(i, obj){
										$("#btn_acl_archive_" + obj).remove();
										$("#info_acl_archive_" + obj).remove();
									});
								}


							}
						}



					}

					// GERE LE RETOUR SUITE GENERATION DOC PDF

					function afficher_doc(json){
						if(json){
							if(json.conventions){
								$.each(json.conventions, function(i,con){
									html="<a href='documents/Societes/<?=$acc_societe?>/Conventions/" + con["con_numero"] + ".pdf' >";
										html=html + "<i class='fa fa-file-pdf-o' ></i>";
									html=html + "</a>";
									$("#pdf_conv_" + con["con_id"]).html(html);
								});
							}
							if(json.convocations){
								$.each(json.convocations, function(i,convoc){
									client_nom=$("#cli_nom_" + convoc).html();
									html="";
									html="<a href='documents/Societes/<?=$acc_societe?>/Convocations/convoc_" + convoc + ".pdf' download='Convocations " + client_nom + ".pdf' >";
										html=html + "<i class='fa fa-file-pdf-o' ></i>";
									html=html+ "</a>";
									$("#pdf_convoc_" + convoc).html(html);
								});
							}
							if(json.presences){
								$.each(json.presences, function(i,presence){
									client_nom=$("#cli_nom_" + presence).html();
									html="";
									html="<a href='documents/Societes/<?=$acc_societe?>/Presences/presence_<?=$action_id?>_" + presence + ".pdf' download='Feuille de présence " + client_nom + ".pdf' >";
										html=html + "<i class='fa fa-file-pdf-o' ></i>";
									html=html+ "</a>";
									$("#pdf_presence_" + presence).html(html);
								});
							}
							if(json.attestations_ind){
								$.each(json.attestations_ind, function(i,attest_ind){
									client_nom=$("#cli_nom_" + attest_ind).html();
									html="";
									html="<a href='documents/Societes/<?=$acc_societe?>/Attestations/attestation_ind_<?=$action_id?>_" + attest_ind + ".pdf' download='Attestations individuelles " + client_nom + ".pdf' >";
										html=html + "<i class='fa fa-file-pdf-o' ></i>";
									html=html+ "</a>";
									$("#pdf_attest_ind_" + attest_ind).html(html);
								});
							}
							if(json.attestations_ses){
								$.each(json.attestations_ses, function(i,attest_ses){
									client_nom=$("#cli_nom_" + attest_ses).html();
									html="";
									html="<a href='documents/Societes/<?=$acc_societe?>/Attestations/attestation_ses_<?=$action_id?>_" + attest_ses + ".pdf' download='Attestation groupée " + client_nom + ".pdf' >";
										html=html + "<i class='fa fa-file-pdf-o' ></i>";
									html=html+ "</a>";
									$("#pdf_attest_ses_" + attest_ses).html(html);
								});
							}
							if(json.attestations_form){
								$.each(json.attestations_form, function(i,attest_form){
									client_nom=$("#cli_nom_" + attest_form).html();
									html="";
									html="<a href='documents/Societes/<?=$acc_societe?>/Attestations/attestation_form_<?=$action_id?>_" + attest_form + ".pdf' download='Attestation formation " + client_nom + ".pdf' >";
										html=html + "<i class='fa fa-file-pdf-o' ></i>";
									html=html+ "</a>";
									$("#pdf_attest_form_" + attest_form).html(html);
								});
							}
							$("input[type=checkbox]").prop("checked",false);
							$("#btn_pdf").html("<i class='fa fa-refresh' ></i> Générer les PDF");
						}
                    }
                    
                  
                    function afficher_marge_dero(json){
                        if(json){
                            $("#verrou_marge").remove();
                            $("#marge_valide").remove();                          
                            if(json.verrou_admin==0){
                                $("#verrou_admin").remove();
                                $("#ong_edition").show();
                            }
                        }
                    }
				</script>
			</body>
		</html>
<?php
	}else{
		echo($erreur_txt);
		die();
	} ?>
