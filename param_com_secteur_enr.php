<?php

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');

if($_SESSION['acces']["acc_service"][1]!=1 AND $_SESSION['acces']["acc_service"][5]!=1){
	$erreur_txt="Accès refusé!";
}
if(!isset($erreur_txt)){

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}
	
	$sql="SELECT soc_agence,soc_nom FROM Societes WHERE soc_id=:societe;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":societe",$acc_societe);
	$req->execute();
	$d_societe=$req->fetch();
	if(!empty($d_societe)){
		if($d_societe["soc_agence"] AND empty($acc_agence)){
			$erreur_txt="Vous devez être connecté sur une agence pour pouvoir gérer la répartition géographique des vendeurs.";
		}
	}else{
		$erreur_txt="Paramètre absent!";
	}
}
	
if(!isset($erreur_txt)){
	
	if(!empty($acc_agence)){
		$sql="SELECT age_nom FROM Agences WHERE age_id=:agence;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":agence",$acc_agence);
		$req->execute();
		$d_agence=$req->fetch();
		if(empty($d_agence)){
			$erreur_txt="Paramètre absent!";
		}
	}
}

if(!isset($erreur_txt)){
	
	// PARAMETRE OK
	
	
	// DONNEES COMPLEMENTAIRES
	
	$d_com_uti=array();
	
	$sql_com="SELECT com_id,com_ref_1,com_label_1,com_label_2 FROM Commerciaux WHERE com_type=1 AND NOT com_archive";
	if(!empty($acc_agence)){
		$sql_com.=" AND com_agence=:agence";
	}
	$sql_com.=" ORDER BY com_id;";;
	$req_com=$ConnSoc->prepare($sql_com);
	if(!empty($acc_agence)){
		$req_com->bindParam(":agence",$acc_agence);
	}
	$req_com->execute();
	$d_r_com=$req_com->fetchAll();
	if(!empty($d_r_com)){
		foreach($d_r_com as $com){
			$d_com_uti[$com["com_id"]]=array(
				"identite" => $com["com_label_1"] . " " . $com["com_label_2"],
				"utilisateur" => $com["com_ref_1"]
			);
		}
	}
	
	// REQUETE POUR UPDATE
	
	// maj du com par défaut
	
	$sql_up_com_dep="UPDATE Societes_Departements SET sde_commercial=:commercial 
	WHERE sde_societe=:societe AND sde_agence=:agence AND sde_departement=:departement AND sde_arrondissement=:arrondissement;";
	$req_up_com_dep=$Conn->prepare($sql_up_com_dep);
						
	// maj du commercial sur orion_0 clients_Societes
	$sql_up_cli_soc="UPDATE Clients_Societes SET cso_commercial=:cso_commercial,cso_utilisateur=:cso_utilisateur 
	WHERE cso_client=:cso_client AND cso_societe=:cso_societe";
	if(!empty($acc_agence)){
		$sql_up_cli_soc.=" AND cso_agence=:cso_agence";
	}
	$sql_up_cli_soc.=";";
	$req_up_cli_soc=$Conn->prepare($sql_up_cli_soc);
	
	// maj du commercial sur orion_N
	$sql_up_cli_n="UPDATE Clients SET cli_commercial=:cli_commercial,cli_utilisateur=:cli_utilisateur WHERE cli_id=:cli_id";
	if(!empty($acc_agence)){
		$sql_up_cli_n.=" AND cli_agence=:cli_agence";
	}
	$sql_up_cli_n.=";";
	$req_up_cli_n=$ConnSoc->prepare($sql_up_cli_n);
	
	// maj du commercial sur suspect en base orion_N
	$sql_up_sus="UPDATE Suspects SET sus_commercial=:sus_commercial,sus_utilisateur=:sus_utilisateur WHERE sus_id=:sus_id";
	$sql_up_sus.=";";
	$req_up_sus=$ConnSoc->prepare($sql_up_sus);
	
	$sql_up_sus_cor="UPDATE Suspects_Correspondances SET sco_utilisateur=:sco_utilisateur 
	WHERE NOT ISNULL(sco_rappeler_le) AND NOT sco_rappel AND sco_suspect=:sco_suspect;";
	$req_up_sus_cor=$ConnSoc->prepare($sql_up_sus_cor);	
	
	

	// ON MAJ
	
	$sql="SELECT * FROM Societes_Departements WHERE sde_societe=:societe";
	if(!empty($acc_agence)){
		$sql.=" AND sde_agence=:agence";
	}
	$sql.=" ORDER BY sde_departement;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":societe",$acc_societe);
	if(!empty($acc_agence)){
		$req->bindParam(":agence",$acc_agence);
	}
	$req->execute();
	$d_departements=$req->fetchAll();
	if(!empty($d_departements)){
		foreach($d_departements as $dep){
			
			if(!empty($dep["sde_arrondissement"])){
				$cle="_" . $dep["sde_departement"] . "_" . $dep["sde_arrondissement"];
			}else{
				$cle="_" . $dep["sde_departement"] . "_00";
			}
			
			$commercial=0;
			if(!empty($_POST["commercial" . $cle])){
				$commercial=intval($_POST["commercial" . $cle]);
			}
			
			$reaffect=false;
			if(!empty($_POST["reaffect" . $cle])){
				$reaffect=true;
			}
			
			// maj du commercial par défaut
			
			$req_up_com_dep->bindParam(":societe",$dep["sde_societe"]);
			$req_up_com_dep->bindParam(":agence",$dep["sde_agence"]);
			$req_up_com_dep->bindParam(":departement",$dep["sde_departement"]);
			$req_up_com_dep->bindParam(":arrondissement",$dep["sde_arrondissement"]);
			$req_up_com_dep->bindParam(":commercial",$commercial);
			try{
				$req_up_com_dep->execute();
			}Catch(Exception $e){
				echo("maj commercial : " . $e->getMessage());
				die();
			}
			
			if($commercial>0 AND $reaffect){
				
				$warning="";
							
				// reaffectation des clients du départements
				
				$utilisateur=0;
				if(!empty($commercial)){
					if(!empty($d_com_uti[$commercial]["utilisateur"])){
						$utilisateur=$d_com_uti[$commercial]["utilisateur"];
					}	
				}
				
				if(empty($utilisateur)){
					$warning="Le commercial " . $d_com_uti[$commercial]["identite"] . " n'est pas lié à un utilisateur. Réaffectation impossible!";
				}
				
				if(empty($warning)){
				
					$suspect=false;
					if(!empty($_POST["suspect" . $cle])){
						$suspect=true;
					}
					$prospect=false;
					if(!empty($_POST["prospect" . $cle])){
						$prospect=true;
					}
					$client=false;
					if(!empty($_POST["client" . $cle])){
						$client=true;
					}
					
					
					$gc_mixte=false;
					if(!empty($_POST["gc_mixte" . $cle])){
						$gc_mixte=true;
					}
					$gc_cn=false;
					if(!empty($_POST["gc_cn" . $cle])){
						$gc_cn=true;
					}
					
					$rappel=false;
					if(!empty($_POST["rappel" . $cle])){
						$rappel=true;
					}
					
					if($client OR $prospect){
					
						$sql="SELECT cli_id,cli_adr_cp,cli_code,cli_nom FROM Clients,Clients_Societes 
						WHERE cli_id=cso_client AND cso_societe=" . $acc_societe . " AND NOT cso_archive AND cli_interco_soc=0
						AND cli_adr_cp LIKE '" . $dep["sde_departement"] . "%'";
						if(!empty($dep["sde_arrondissement"])){
							$sql.=" AND cli_adr_cp LIKE '%" . $dep["sde_arrondissement"] . "'";	
						}
						if(!empty($acc_agence)){
							$sql.=" AND cso_agence=" . $acc_agence;	
						}
						
						if(!$client OR !$prospect){
							if($client){
								// utilisateur ne veut que les client
								$sql.=" AND NOT cli_first_facture=0 AND NOT ISNULL(cli_first_facture)";
							}else{
								// l'utilisateur ne veut que les prospect
								$sql.=" AND (cli_first_facture=0 OR ISNULL(cli_first_facture) )";
							}
							
						}
						
						if(!$gc_mixte AND !$gc_cn){
							// classique tous sauf CN et mixte
							$sql.=" AND (cli_categorie IN (1,3,5) OR cli_sous_categorie=2)";
						}elseif($gc_mixte AND $gc_cn){
							// uti veut tous
							$sql.=" AND cli_categorie IN (1,3,5,2)";
						}elseif($gc_mixte AND !$gc_cn){
							// uti veut tous les CN
							$sql.=" AND (cli_categorie IN (1,3,5) OR (cli_sous_categorie IN (2,3) )";
						}elseif(!$gc_mixte AND $gc_cn){
							// uti veut tous les MIXTE
							$sql.=" AND (cli_categorie IN (1,3,5) OR (cli_sous_categorie IN (1,2) )";
						}
						$sql.=";";
						$req_cli=$Conn->query($sql);
						$d_clients=$req_cli->fetchAll();
						if(!empty($d_clients)){
							
							foreach($d_clients as $cli){
								
								// maj du commercial en base ORION
								
								$req_up_cli_soc->bindParam(":cso_commercial",$commercial);
								$req_up_cli_soc->bindParam(":cso_utilisateur",$utilisateur);
								$req_up_cli_soc->bindParam(":cso_client",$cli["cli_id"]);
								$req_up_cli_soc->bindParam(":cso_societe",$acc_societe);
								if(!empty($acc_agence)){
									$req_up_cli_soc->bindParam(":cso_agence",$acc_agence);
								}
								$req_up_cli_soc->execute();
								
								// maj du commercial en base N
								$req_up_cli_n->bindParam(":cli_commercial",$commercial);
								$req_up_cli_n->bindParam(":cli_utilisateur",$utilisateur);
								$req_up_cli_n->bindParam(":cli_id",$cli["cli_id"]);
								if(!empty($acc_agence)){
									$req_up_cli_n->bindParam(":cli_agence",$acc_agence);
								}
								$req_up_cli_n->execute();
														
								if($rappel){
									
									// utilisateur à demander de transferer les dates de rappels
									
									
									
									$sql_get_corresp="SELECT cor_id FROM Correspondances,Utilisateurs WHERE cor_utilisateur=uti_id AND uti_societe=:societe
									AND NOT ISNULL(cor_rappeler_le) AND NOT cor_rappel AND cor_client=:client AND cor_rappeler_le>='2020-01-01'";
									if(!empty($acc_agence)){
										$sql_get_corresp.=" AND uti_agence=:agence";
									}
									$sql_get_corresp.=";";
									$req_get_corresp=$Conn->prepare($sql_get_corresp);
									$req_get_corresp->bindParam(":client",$cli["cli_id"]);
									$req_get_corresp->bindParam(":societe",$acc_societe);
									if(!empty($acc_agence)){
										$req_get_corresp->bindParam(":agence",$acc_agence);
									}
									$req_get_corresp->execute();
									$src_corresp=$req_get_corresp->fetchAll();
									if(!empty($src_corresp)){
										$tab_corresp=array_column ($src_corresp,"cor_id");
										$liste_corresp=implode($tab_corresp,",");

										$Conn->query="UPDATE Correspondances SET cor_utilisateur=" . $utilisateur . " WHERE cor_id IN (" . $liste_corresp . ");";

									}
									
								}

							}
						}
						
					}
					// FIN TRAITEMENT CLIENT PROSPECT sur BASE 0
					
					// TRAITEMENT EN BASE N
					// pas de grand compte en base N
					// pas de correspondance pour les suspects donc pas de rappels

					if($suspect OR $prospect){
					
						$sql="SELECT DISTINCT sus_id,sus_adr_cp,sus_code,sus_nom,sco_suspect FROM Suspects";
						$sql.=" LEFT JOIN Suspects_Correspondances ON (Suspects.sus_id=Suspects_Correspondances.sco_suspect)";
						$sql.=" WHERE sus_adr_cp LIKE '" . $dep["sde_departement"] . "%'";
						if(!empty($dep["sde_arrondissement"])){
							$sql.=" AND sus_adr_cp LIKE '%" . $dep["sde_arrondissement"] . "'";	
						}
						if(!empty($acc_agence)){
							$sql.=" AND sus_agence=" . $acc_agence;	
						}
						
						if(!$suspect OR !$prospect){
							if($suspect){
								// utilisateur ne veut que les suspects
								$sql.=" AND ISNULL(.Suspects_Correspondances.sco_suspect)";
							}else{
								// l'utilisateur ne veut que les prospect = suspect avec correspondances
								$sql.=" AND NOT ISNULL(Suspects_Correspondances.sco_suspect)";
							}
							
						}
						$sql.=";";
						$req_sus=$ConnSoc->query($sql);
						$d_suspects=$req_sus->fetchAll();
						if(!empty($d_suspects)){
							
							foreach($d_suspects as $sus){
								

								$req_up_sus->bindParam(":sus_commercial",$commercial);
								$req_up_sus->bindParam(":sus_utilisateur",$utilisateur);
								$req_up_sus->bindParam(":sus_id",$sus["sus_id"]);							
								$req_up_sus->execute();
								
								if($rappel AND !is_null($sus["sco_suspect"])){
									
									// utilisateur à demander de transferer les dates de rappels
						
									$req_up_sus_cor->bindParam(":sco_utilisateur",$utilisateur);
									$req_up_sus_cor->bindParam(":sco_suspect",$sus["sus_id"]);									
									$req_up_sus_cor->execute();
								}

							}
						}
						
					}
	
			
				}else{
					
					if(isset($warning_txt)){
						$warning_txt.="<br/>" . $warning;
					}else{
						$warning_txt=$warning;
					}
				}
				
			}
			
		}
	}
}

if(isset($erreur_txt)){
	$_SESSION['message'][] = array(
		"aff" => "",
		"titre" => "Echec de l'enregistrement",
		"type" => "danger",
		"message" => $erreur_txt
	);
}elseif(isset($warning_txt)){
	
	$_SESSION['message'] = array(
		"aff" => "modal",
		"titre" => "Enregistrement incomplet!",
		"type" => "warning",
		"message" => $warning_txt 
	);
	
}else{
	$_SESSION['message'][] = array(
		"aff" => "",
		"titre" => "Enregistrement terminé",
		"type" => "success",
		"message" => "Vos modifications ont bien été enregistrées." 
	);
}
header("location : param_com_secteur.php");

?>
		