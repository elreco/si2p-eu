<?php
	
	// DETAIL DES VALEURS DE LA STAT "Tests CACES"


	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "includes/connexion_soc.php";

	if($_SESSION["acces"]["acc_ref"]!=1 OR ($_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']["acc_profil"]!=11 AND $_SESSION['acces']["acc_profil"]!=14 AND $_SESSION['acces']["acc_profil"]!=15 AND $_SESSION['acces']["acc_profil"]!=10) ){
        // SERVICE TECH, DAF, DG, RA et RE
        $erreur="Accès refusé!";
		echo($erreur);
		die();
	}

    $erreur_txt="";

    // CRITERE (POST) -> choix de l'utilisateur avant de lancer sa stat

    if (isset($_SESSION["stat_tech"])) {

		$testeur=0;
		if(!empty($_SESSION["stat_tech"]["testeur"])){
			$testeur=intval($_SESSION["stat_tech"]["testeur"]);
		}
		
		$qualification=0;
		if(!empty($_SESSION["stat_tech"]["qualification"])){
			$qualification=intval($_SESSION["stat_tech"]["qualification"]);
		}
		
		$periode_deb="";
		if(!empty($_SESSION["stat_tech"]["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_SESSION["stat_tech"]["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}
		
		$periode_fin="";
		if(!empty($_SESSION["stat_tech"]["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_SESSION["stat_tech"]["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
		}

	}else{
		$erreur_txt="Paramètres absents!";
    }
    
	if(empty($periode_deb) OR empty($periode_fin)){
		$erreur_txt="Formulaire incomplet!";
	}

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : stat_technique.php");
		die();
    }
    
    // CRITERE GET -> choix des valeurs à détailler

    $categorie=0;
    if(!empty($_GET["categorie"])){
        $categorie=intval($_GET["categorie"]);
    }

    $statut=0;
    if(!empty($_GET["statut"])){
        $statut=intval($_GET["statut"]);
    }

	
	
	// LE PERSONNE CONNECTE
	
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]); 
	}
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]); 
	}
	
	// TITRE 

    if(!empty($categorie)){
		$sql="SELECT qua_libelle,qca_libelle FROM Qualifications,Qualifications_Categories WHERE qua_id=qca_qualification AND qca_id=" . $categorie . ";";
		$req=$Conn->query($sql);
		$d_qualification=$req->fetch();
		if(!empty($d_qualification)){
			$titre_qualif=" '" . $d_qualification["qua_libelle"] . "' catégorie " . $d_qualification["qca_libelle"] ;
		}
	} elseif(!empty($qualification)) {
		$sql="SELECT qua_libelle FROM Qualifications WHERE qua_id=" . $qualification . ";";
		$req=$Conn->query($sql);
		$d_qualification=$req->fetch();
		if(!empty($d_qualification)){
			$titre_qualif=" '" . $d_qualification["qua_libelle"] . "'";
		}
	}
	
	
	
	
	if(!empty($testeur)){
		
		$sql="SELECT int_label_1,int_label_2 FROM Intervenants WHERE int_id=" . $testeur . ";";
		$req=$ConnSoc->query($sql);
		$d_intervenant=$req->fetch();
		if(!empty($d_intervenant)){
			$titre_testeur=" par " . $d_intervenant["int_label_1"] . " " . $d_intervenant["int_label_2"];
		}
	}
	
	//PAS de notion de societe / agence sur les diplomes UN CACES peut-etre multi soc
	// On liste les testeur de la societe pour identifier les tests a comptabilisé
	
	$sql="SELECT int_id,int_label_1,int_label_2 FROM Intervenants WHERE NOT int_type=0";
	if(!empty($acc_agence)){
		$sql.=" AND int_agence=" . $acc_agence;
	}
	$sql.=" ORDER BY int_label_1,int_label_2";
	$req=$ConnSoc->query($sql);
	$d_intervenants=$req->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($d_intervenants)){
		
		$tab_int=array_column ($d_intervenants,"int_id");
		$liste_int=implode($tab_int,",");
		
	}else{
		
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => "Impossible de générer la statistique!"
		);
		header("location : stat_technique.php");
		die();
	}
    
    
    $titre="Détail des tests";
    if(isset($titre_qualif)){
        $titre.= $titre_qualif;
        if(isset($titre_testeur)){
            $titre.="<br/>";
        }
    }
    if($statut==1){
        $titre.=" validés";
    }elseif($statut==2){
        $titre.=" ajournés";
    }else{
        $titre.=" réalisés";
    }
    if(isset($titre_testeur)){
        $titre.=$titre_testeur;
    }
	$titre.="<br/>entre le " . $DT_periode_deb->format("d/m/Y") . " et le " . $DT_periode_fin->format("d/m/Y") ;
	
	
	
	// LES TEST CACES
	
	$stat=array();
	$stat[0]=array(
		"valide" => 0,
		"echec" => 0,
		"total" => 0
	);
	
	$sql="SELECT qua_libelle,qca_id,qca_libelle,dcc_valide,dcc_diplome 
    ,DATE_FORMAT(dcc_date,'%d/%m/%Y') AS dcc_date,dcc_testeur_identite,dca_id,dca_sta_nom,dca_sta_prenom,dcc_passage
    ,dca_action_1,dca_action_2,dca_action_3,dca_action_4,dca_action_5,dca_action_6
    ,dca_action_client_1,dca_action_client_2,dca_action_client_3,dca_action_client_4,dca_action_client_5,dca_action_client_6
	FROM qualifications,qualifications_categories,diplomes_caces_cat,diplomes_caces
	WHERE qca_qualification=qua_id AND qca_id=dcc_categorie AND dcc_diplome=dca_id
	AND dca_diplome=2 AND dcc_date>='" . $periode_deb . "' AND dcc_date<='" . $periode_fin . "'";
	$sql.=" AND (
		(dca_action_soc_1=" . $acc_societe . " AND dcc_passage=1)
		OR (dca_action_soc_2=" . $acc_societe . " AND dcc_passage=2)
		OR (dca_action_soc_3=" . $acc_societe . " AND dcc_passage=3)
		OR (dca_action_soc_4=" . $acc_societe . " AND dcc_passage=4)
		OR (dca_action_soc_5=" . $acc_societe . " AND dcc_passage=5)
		OR (dca_action_soc_6=" . $acc_societe . " AND dcc_passage=6)
	)";
	if($testeur>0){
		$sql.=" AND dcc_testeur=" . $testeur;
	}else{
		
		$sql.=" AND dcc_testeur IN (" . $liste_int . ")";
	}
	if($qualification>0){
		$sql.=" AND qua_id=" . $qualification;
    }
    if ($categorie>0) {
		$sql.=" AND dcc_categorie=" . $categorie;
	}
	if ($statut==1) {
		$sql.=" AND dcc_valide=1";
	} elseif ($statut==2) {
		$sql.=" AND dcc_valide=0";
	}
	$sql.=" ORDER BY qua_libelle,qca_libelle,dcc_date";
	$req=$Conn->query($sql);
	$d_tests=$req->fetchAll();
	
	/*echo("<pre>");
		print_r($d_tests);
	echo("</pre>");
	die();*/
	
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<!-- PERSO -->	
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
		
		<div id="main" >
<?php	    include "includes/header_def.inc.php"; ?>			
			<section id="content_wrapper" >	

				<section id="content" class="animated fadeIn" >

                    <div id="page_print" >
                        <h1 class="text-center" >
                            <?=$titre?>
                        </h1>
							
				
			    <?php	if(!empty($d_tests)){ ?>
			
						
							<div class="row" >
								<div class="col-md-12" >
								
									<div class="table-responsive">
										<table class="table table-striped table-hover" >
											<thead>
												<tr class="dark">
													<th>Qualification</th>
													<th>Catégorie</th>
                                                    <th>Date</th>
													<th>Testeur</th>
                                                    <th>Validé</th>
                                                    <th>Ajournée</th>
                                                    <th>Dossier</th>
                                                    <th>Stagiaire</th>
													<th>Action</th>
                                                    <th>Inscription</th>							
												</tr>
											</thead>
											<tbody>
                                    <?php	    $total_valide=0;
                                                $total_echec=0; 
                                                foreach($d_tests as $k => $test){ ?>
														<tr>
															<td><?=$test["qua_libelle"]?></td>
															<td><?=$test["qca_libelle"]?></td>
                                                            <td><?=$test["dcc_date"]?></td>
                                                            <td><?=$test["dcc_testeur_identite"]?></td>
                                                            <td>
                                                        <?php   if($test["dcc_valide"]) {
                                                                    echo("1");
                                                                    $total_valide++;
                                                                } ?>
                                                            </td>
                                                            <td>
                                                        <?php   if(!$test["dcc_valide"]) {
                                                                    echo("1");
                                                                    $total_echec++;
                                                                } ?>
                                                            </td>
															<td><?=$test["dca_id"]?></td>
                                                            <td><?=$test["dca_sta_nom"] . " " . $test["dca_sta_prenom"]?></td>
                                                            <td><?=$test["dca_action_" .  $test["dcc_passage"]]?></td>
                                                            <td><?=$test["dca_action_client_" .  $test["dcc_passage"]]?></td>
														</tr>
									<?php		} ?>
											</tbody>
											<tfoot>
												<tr>
													<th colspan="4" class="text-right" >Total :</th>
													<td><?=$total_valide?></td>
													<td><?=$total_echec?></td>
													<td colspan="4" >&nbsp;</td>
												</tr>
											</tfoot>
										</table>							
									</div>
									
								</div>
                            </div>
							
            <?php		}else{ ?>
            
                            <p class="alert alert-warning" >
                                Pas de résultat
                            </p>
                            
            <?php		} ?>
                       
				    </div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="stat_tech_caces_test.php" class="btn btn-sm btn-default"  >
							<i class="fa fa-left-arrow" ></i>Retour
						<a>					
					</div>
					<div class="col-xs-6 footer-middle text-center" style=""></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function () {
				
			
				
			});
			(jQuery);
		</script>
	</body>
</html>
