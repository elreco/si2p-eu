<?php


include "includes/controle_acces.inc.php";
include "includes/connexion.php";


include 'modeles/mod_acc_client_edit.php';
include 'modeles/mod_get_pro_familles.php';
include 'modeles/mod_get_pro_s_familles.php';

	$client=0;
	if(!empty($_GET['client'])){
		$client=intval($_GET['client']);
	}
	if($client==0){
		echo("Erreur : client inconnu !");
		die();	
	}
	$document=0;
	if(!empty($_GET['document'])){
		$document=intval($_GET['document']);
	}
	
	// la personne connecte
	
	$acc_utilisateur=0;
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
	
	/*$acces=acc_client_edit($client);
	if(!$acces){
		echo("Erreur : impossible d'afficher !");
		die();	
	}*/
	
	
	// controle d'accès par rapport au client
	$d_doc=array(
		"nom" => "",
		"type" => 0,
		"famille" => 0,
		"sous_famille" => 0
	);
	
	if($document>0){
		$sql="SELECT * FROM Clients_Documents WHERE cdo_id=" . $document . ";";
		$req=$Conn->query($sql);
		$result=$req->fetch();
		if(!empty($result)){
			$d_doc["nom"]=htmlentities($result["cdo_nom"], ENT_QUOTES);
			$d_doc["type"]=$result["cdo_type"];
			$d_doc["famille"]=$result["cdo_pro_famille"];
			$d_doc["sous_famille"]=$result["cdo_pro_sous_famille"];
			$d_doc["sous_sous_famille"]=$result["cdo_pro_sous_sous_famille"];
		}
	}
	
	// type de doc client
	$sql="SELECT cdt_id,cdt_libelle FROM Clients_Documents_Types ORDER BY cdt_libelle;";
	$req = $Conn->query($sql);
	$d_doc_type = $req->fetchAll(); 
	
	// famille de produits
	$famille_produit=get_pro_familles();
	
	if($d_doc["famille"]>0){
		$sous_famille_produit=get_pro_s_familles($d_doc["famille"]);
	}
	if($d_doc["famille"]>0){
		$sous_sous_famille_produit=get_pro_s_s_familles($d_doc["sous_famille"]);
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	   
	   <!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form id="form_modal_doc" action="client_doc_enr.php" method="POST" class="admin-form" enctype='multipart/form-data'>
			<div>
				<input type="hidden" name="document" id="document" value="<?=$document?>" />
				<input type="hidden" name="client" value="<?=$client?>" />
			</div>
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" class="" >
					<section id="content" class="animated fadeIn">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
							
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											
											<div class="content-header">
										<?php	if($document==0){ ?>
													<h2>Nouveau <b class="text-primary">document</b></h2>
										<?php	}else{ ?>
													<h2>Editer un <b class="text-primary">document</b></h2>
										<?php	} ?>
												
											</div>
											<div class="col-md-10 col-md-offset-1">	
												<div class="row" >
													<div class="col-md-12">
														<label class="field">
															<input type="text" name="cdo_nom" id="cdo_nom" class="gui-input" placeholder="Nom du document" value="<?=$d_doc["nom"]?>" required />
														</label>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-12">
														<label class="field prepend-icon file">
															<span class="button btn-primary">Choisir</span>
															<input type="file" class="gui-file" name="cdo_doc" id="doc" />
															<input type="text" class="gui-input" id="doc_champ" placeholder="Importez votre document" />
															<label class="field-icon">
																<i class="fa fa-upload"></i>
															</label>
														</label>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-12">
														<select name="cdo_type" class="select2" id="cdo_type">
															<option value="0">Type...</option>
												<?php		if(!empty($d_doc_type)){ 
																foreach($d_doc_type as $ddt){
																	if($ddt["cdt_id"]==$d_doc["type"]){
																		echo("<option value='" . $ddt["cdt_id"] . "' selected >" . $ddt["cdt_libelle"] . "</option>");
																	}else{
																		echo("<option value='" . $ddt["cdt_id"] . "' >" . $ddt["cdt_libelle"] . "</option>");
																	}
																	
																}
															} ?>
														</select>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-12">
														<select name="cdo_pro_famille" id="cdo_famille" class="select2 select2-famille" >
															<option value="0">Famille...</option>
												<?php		if(!empty($famille_produit)){
																foreach($famille_produit as $fp){	
																	if($fp["pfa_id"]==$d_doc["famille"]){
																		echo("<option value='" . $fp["pfa_id"] . "' selected >" . $fp["pfa_libelle"] . "</option>");	
																	}else{
																		echo("<option value='" . $fp["pfa_id"] . "' >" . $fp["pfa_libelle"] . "</option>");	
																	}																
																}
															} ?>
														</select>
													</div>
												</div>
												<div class="row mt15" >
												<?php ?>
													<div class="col-md-12">
														<select name="cdo_pro_sous_famille" class="select2 select2-famille-sous-famille" id="cdo_sous_famille">
															<option value="0">Sous-famille ...</option>
												<?php		if(!empty($sous_famille_produit)){
																foreach($sous_famille_produit as $sfp){	
																	if($sfp["psf_id"]==$d_doc["sous_famille"]){
																		echo("<option value='" . $sfp["psf_id"] . "' selected >" . $sfp["psf_libelle"] . "</option>");	
																	}else{
																		echo("<option value='" . $sfp["psf_id"] . "' >" . $sfp["psf_libelle"] . "</option>");	
																	}																
																}
															} ?>
														</select>
													</div>
													
												</div>		
												<div class="row mt15">																		<div class="col-md-12">
														<select name="cdo_pro_sous_sous_famille" class="select2 select2-famille-sous-sous-famille" id="cdo_sous_sous_famille">
															<option value="0">Sous-Sous-famille ...</option>
												<?php		if(!empty($sous_sous_famille_produit)){
																foreach($sous_sous_famille_produit as $sfp){	
																	if($sfp["pss_id"]==$d_doc["sous_sous_famille"]){
																		echo("<option value='" . $sfp["pss_id"] . "' selected >" . $sfp["pss_libelle"] . "</option>");	
																	}else{
																		echo("<option value='" . $sfp["pss_id"] . "' >" . $sfp["pss_libelle"] . "</option>");	
																	}																
																}
															} ?>
														</select>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</section>				
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="client_voir.php?client=<?=$client?>&tab7" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" ></div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
		
<?php	include "includes/footer_script.inc.php"; ?>
		<script src="/vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" >
			jQuery(document).ready(function (){
				
			});
		</script>			
	</body>
</html>
