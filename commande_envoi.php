<?php 

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_parametre.php');
include('modeles/mod_commande_data.php');
include('modeles/mod_commande_pdf.php');
include('modeles/mod_envoi_mail.php');
// VISUALISATION D'UNE CONVOCATIONS

$erreur_txt="";



// DONNEE POUR AFFICHAGE
$data=array(
    "pages" => array()
);
// aller chercher la commande
$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = " . $_GET['commande']);
$req->execute();
$commande= $req->fetch();
$data["pages"][0]=commande_data($_GET['commande']);
if(empty($data["pages"][0])){
    $erreur_txt="impossible d'afficher la page";
}else{
	if(!file_exists("Documents/Commandes/" . $commande['com_numero'] . ".pdf")){
		$pdf=model_commande_pdf($data);
	}else{
		$pdf = 1;
	}
    
    if(!$pdf){
        $erreur_txt="impossible d'afficher la page";
    }
}
if(!empty($commande['com_contact_mail'])){
    $mail = envoi_mail("si2p","Documents/Commandes/" . $commande['com_numero'] . ".pdf",$commande['com_contact_mail']);
}else{
    $erreur_txt = "Impossible d'envoyer l'email car il n'y a pas d'adresse mail pour ce fournisseur.";
}

if(!empty($erreur_txt)){
	$_SESSION['message'][] = array(
		"aff" => "",
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur_txt
	);
}
if(!empty($erreur_txt)){
	header("location : commande_voir.php?commande=" . $_GET['commande']);
}else{
	header("location : Documents/Commandes/" . $commande['com_numero'] . ".pdf");
}

die();
?>