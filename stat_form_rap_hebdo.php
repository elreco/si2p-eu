<?php

	// EXTRACTION DES DONNEES RAPPORTS HEBDO


	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "includes/connexion_soc.php";

	// if($_SESSION["acces"]["acc_ref"]!=1 OR $_SESSION['acces']['acc_service'][3]!=1){
	// 	$erreur="Accès refusé!";
	// 	echo($erreur);
	// 	die();
	// }

	// DONNEE FORM

	$erreur_txt="";
	if(!empty($_POST)){

		//print_r($_POST);

		$intervenant=0;
		if(!empty($_POST["intervenant"])){
			$intervenant=intval($_POST["intervenant"]);
		}

		$utilisation=0;
		if(!empty($_POST["utilisation"])){
			$utilisation=intval($_POST["utilisation"]);
		}

		$vehicule=0;
		if(!empty($_POST["vehicule"])){
			$vehicule=intval($_POST["vehicule"]);
		}

		$periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}

		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
		}

		if(empty($periode_deb) OR empty($periode_fin)){
			$erreur_txt="Formulaire incomplet!";
		}
	}else{
		$erreur_txt="Formulaire incomplet!";

	}

	if(empty($erreur_txt)){

		// LE PERSONNE CONNECTE

		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=intval($_SESSION['acces']["acc_societe"]);
		}

		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=intval($_SESSION['acces']["acc_agence"]);
		}

		// DONNEES ORION_0

		// vehicules

		$d_vehicules=array(
			0 => ""
		);

		// on liste tous les vehicules -> evite message d'erreur en cas de changement d'affectation de vehicule
		$sql="SELECT veh_id,veh_libelle FROM Vehicules ORDER BY veh_id;";
		$req=$Conn->query($sql);
		$d_r_v=$req->fetchAll();
		if(!empty($d_r_v)){
			foreach($d_r_v as $veh){
				$d_vehicules[$veh["veh_id"]]=$veh["veh_libelle"];
			}
		}


		// type de rapport

		$d_types=array(
			0 => ""
		);
		$sql="SELECT fut_id,fut_libelle FROM Formations_Utilisations ORDER BY fut_id;";
		$req=$Conn->query($sql);
		$d_r_u=$req->fetchAll();
		if(!empty($d_r_u)){
			foreach($d_r_u as $typ){
				$d_types[$typ["fut_id"]]=$typ["fut_libelle"];
			}
		}


		// LISTE DES RAPPORTS HEBDO
		$sql="SELECT for_id,for_nb_demi,for_vehicule_id,DATE_FORMAT(for_date,'%d/%m/%Y') AS for_date_fr,for_demi,for_km_deb,for_km_fin,for_demi,for_utilisation
		,int_label_1,int_label_2
		,acl_produit,acl_pro_reference
		,cli_code,cli_nom
		,nb_row
		,fac_nb_stagiaires
		FROM
		Formations LEFT JOIN Intervenants ON (Formations.for_intervenant=Intervenants.int_id)";

		$sql.="LEFT JOIN (
		SELECT fac_formation, COUNT(fac_action_client) AS nb_row FROM Formations_Actions GROUP BY fac_formation ) AS groupe
		ON Formations.for_id = groupe.fac_formation";

		$sql.=" LEFT JOIN Formations_Actions ON (Formations.for_id=Formations_Actions.fac_formation)
		LEFT JOIN Actions_Clients ON (Formations_Actions.fac_action_client=Actions_Clients.acl_id)
		LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
		LEFT JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id AND Actions.act_agence=Clients.cli_agence)

		WHERE for_date>='" . $periode_deb . "' AND for_date<='" . $periode_fin . "'";
		if(!empty($intervenant)){
			$sql.=" AND for_intervenant=" . $intervenant;
		}
		if(!empty($utilisation)){
			$sql.=" AND for_utilisation=" . $utilisation;
		}
		if(!empty($vehicule)){
			$sql.=" AND for_vehicule_id=" . $vehicule;
		}
		$sql.=" ORDER BY for_date,for_demi,for_id;";
		$req=$ConnSoc->query($sql);
		$d_rapports=$req->fetchAll();

	}


?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<div id="main" >
<?php
			if($_SESSION["acces"]["acc_ref"]==1){
				include "includes/header_def.inc.php";
			} ?>
			<section id="content_wrapper" >
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn" >

			<?php	if(!empty($erreur_txt)){ ?>
						<p class="alert alert-danger" ><?=$erreur_txt?></p>
			<?php	}else{ ?>
						<div class="row " >

							<div class="col-md-12" >

								<h1 class="text-center" >Liste des rapports hebdomadaires</h1>

					<?php		if(!empty($d_rapports)){

									$total_km=0;
									$total_nb_demi=0;
									$total_stagiaire=0;
									$nb_form=0; ?>

									<div class="panel" >
										<div class="panel-body">

											<table class="table" >
												<thead>
													<tr class="dark">
														<th colspan="2" >Date</th>
														<th>Nb demi-journée</th>
														<th>Type</th>
														<th>Intervenant</th>
														<th>Clients</th>
														<th>Produits</th>
														<th>Nb stagiares</th>
														<th>Vehicule</th>
														<th>Km de départ</th>
														<th>Km de d'arrivé</th>
														<th>Km parcourus</th>
													</tr>
												</thead>
												<tbody>
								<?php				$bcl_formation=0;

													foreach($d_rapports as $k => $d_r){

														$total_stagiaire=$total_stagiaire+$d_r["fac_nb_stagiaires"];

														if($bcl_formation!=$d_r["for_id"]){

															$bcl_formation=$d_r["for_id"];

															$date_periode="";
															switch ($d_r["for_demi"]){
																case 1:
																	$date_periode="matin";
																	break;
																case 1.5:
																	$date_periode="toute la journée";
																	break;
																case 2:
																	$date_periode="après-midi";
																	break;
															}

															$km=$d_r["for_km_fin"]-$d_r["for_km_deb"];
															$total_km=$total_km+$km;

															$total_nb_demi=$total_nb_demi+$d_r["for_nb_demi"];

															$class="";
															if($km<0){
																$class="text-danger";
															}

															$nb_form++;
															$style="background-color:#FFF;";
															if($nb_form % 2 ==0){
																$style="background-color:#f5f5f5;";
															}

															$nb_row=1;
															if(!empty($d_r["nb_row"])){
																$nb_row=$d_r["nb_row"];
															}

															?>
															<tr class="<?=$class?>" style="<?=$style?>" >
																<td rowspan="<?=$nb_row?>" ><?=$d_r["for_date_fr"]?></td>
																<td rowspan="<?=$nb_row?>" ><?=$date_periode?></td>
																<td rowspan="<?=$nb_row?>" class="text-right" ><?=$d_r["for_nb_demi"]?></td>
																<td rowspan="<?=$nb_row?>" ><?=$d_types[$d_r["for_utilisation"]]?></td>
																<td rowspan="<?=$nb_row?>"  ><?=$d_r["int_label_1"] . " " . $d_r["int_label_2"]?></td>
																<td><?=$d_r["cli_nom"]?></td>
																<td><?=$d_r["acl_pro_reference"]?></td>
																<td class="text-right" ><?=$d_r["fac_nb_stagiaires"]?></td>
																<td rowspan="<?=$nb_row?>" ><?=$d_vehicules[$d_r["for_vehicule_id"]]?></td>
																<td rowspan="<?=$nb_row?>" class="text-right" ><?=$d_r["for_km_deb"]?></td>
																<td rowspan="<?=$nb_row?>" class="text-right" ><?=$d_r["for_km_fin"]?></td>
																<td rowspan="<?=$nb_row?>" class="text-right" ><?=$km?></td>
															</tr>
											<?php		}else{ ?>
															<tr class="<?=$class?>" style="<?=$style?>" >
																<td><?=$d_r["cli_nom"]?></td>
																<td><?=$d_r["acl_pro_reference"]?></td>
																<td class="text-right" ><?=$d_r["fac_nb_stagiaires"]?></td>
															</tr>

									<?php				}
													} ?>
												</tbody>
												<tfoot>
													<tr>
														<th colspan="2" class="text-right" >Total :</td>
														<td class="text-right" ><?=$total_nb_demi?></td>
														<td colspan="4" ></td>
														<td class="text-right" ><?=$total_stagiaire?></td>
														<td colspan="3" ></td>
														<td class="text-right" ><?=$total_km?></td>
													</tr>
												</tfoot>
											</table>

										</div>
									</div>
					<?php		}else{ ?>
									<p class="alert alert-warning" >Aucun rapport hebdomadaire ne correspond à votre recherche</p>
					<?php		} ?>
							</div>

						</div>

			<?php	} ?>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="stat_formation.php" class="btn btn-sm btn-default" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){

				$(".reseau").click(function(){
					stat=$(this).data("stat");
					if(stat>0){
						if($(this).is(":checked")){
							$(".no-reseau-" + stat).hide();
						}else{
							$(".no-reseau-" + stat).show();
						}
					}
				});

			});
		</script>
	</body>
</html>
