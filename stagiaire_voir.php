<?php  

 // VISU D'UNE FICHE STAGIAIRE

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');

$stagiaire=0;
if(isset($_GET['stagiaire'])){
	if(!empty($_GET['stagiaire'])){
		$stagiaire=intval($_GET['stagiaire']);
	}
}
if($stagiaire==0){
	echo("impossible d'afficher la page");
	die();
}
// DONNEES POUR AFFICHAGE

// retour
$_SESSION["retour_action"]="stagiaire_voir.php?stagiaire=" . $stagiaire;

// personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];  
}


// LE STAGIAIRE

$req = $Conn->prepare("SELECT * FROM Stagiaires WHERE sta_id = :sta_id");
$req->bindParam(':sta_id',$stagiaire);
$req->execute();
$d_stagiaire = $req->fetch();
if(!empty($d_stagiaire)){
	
	$sta_naissance="";
	if(!empty($d_stagiaire["sta_naissance"])){
		$Dt_naiss=DateTime::createFromFormat('Y-m-d',$d_stagiaire["sta_naissance"]);
		$sta_naissance=$Dt_naiss->format("d/m/Y");
	}
	
	$sta_civilite=$d_stagiaire["sta_prenom"] . " " . $d_stagiaire["sta_nom"];
	if(!empty($d_stagiaire["sta_prenom"])){
		if($d_stagiaire["sta_titre"]==1){
			$sta_civilite="Monsieur " . $sta_civilite;
		}elseif($d_stagiaire["sta_titre"]==2){
			$sta_civilite="Madame " . $sta_civilite;
		}
	}
	
}else{
	echo("Impossible d'afficher la page!");
	die();
}

// HISTORIQUE DES EMPLOYEURS

$sql="SELECT cli_code,cli_nom,cli_stagiaire,cli_categorie FROM Stagiaires_Clients INNER JOIN Clients ON (Stagiaires_Clients.scl_client=Clients.cli_id) 
INNER JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client) WHERE scl_stagiaire=:stagiaire AND cso_societe=:societe";
if(!empty($acc_agence)){
	$sql.=" AND cso_agence=:agence";
}
$sql.=" ORDER BY cli_code,cli_nom;";
$req = $Conn->prepare($sql);
$req ->bindParam(":stagiaire",$stagiaire);
$req ->bindParam(":societe",$acc_societe);
if(!empty($acc_agence)){
	$req ->bindParam(":agence",$acc_agence);
}
$req->execute();
$d_clients=$req->fetchAll();
if(empty($d_clients)){
	echo("Impossible d'afficher la page!");
	die();
}

// HISTO DE FORMATION

$sta_formations=array();

$sql="SELECT soc_id FROM Societes ORDER BY soc_id;";
$req = $Conn->query($sql);
$d_societes=$req->fetchAll();
if(!empty($d_societes)){
	
	foreach($d_societes as $soc){
	
		$ConnFct=connexion_fct($soc["soc_id"]);
	
		$sql_form="SELECT act_id,DATE_FORMAT(act_date_deb,'%d/%m/%Y') AS act_date_deb,act_agence,cli_code,cli_nom,acl_pro_reference,acl_pro_libelle,acl_id
		FROM Actions_Stagiaires LEFT JOIN Actions ON (Actions_Stagiaires.ast_action=Actions.act_id)
		LEFT JOIN Actions_Clients ON (Actions_Stagiaires.ast_action_client=Actions_Clients.acl_id)
		LEFT JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)
		WHERE ast_stagiaire=:stagiaire;";
		$req_form=$ConnFct->prepare($sql_form);
		$req_form->bindParam(":stagiaire",$stagiaire);
		$req_form->execute();
		$d_formations=$req_form->fetchAll();
		if(!empty($d_formations)){
			foreach($d_formations as $df){
				$sta_formations[]=array(
					"date" => $df["act_date_deb"],
					"societe" => $soc["soc_id"],
					"agence" => $df["act_agence"],
					"cli_code" => $df["cli_code"],
					"cli_nom" => $df["cli_nom"],
					"produit_code" => $df["acl_pro_reference"],
					"produit_nom" => $df["acl_pro_libelle"],
					"action" => $df["act_id"],
					"action_client" => $df["acl_id"],
				);
			}
		}	
	}	
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
		<div id="main">
<?php		include "includes/header_def.inc.php";  ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
				
					<h1><?=$sta_civilite?></h1>
					
					<div class="row">
						<div class="col-md-9" >					
							<div class="row">
								<div class="col-md-6" >
									<div class="panel" >
										<div class="panel-heading panel-head-sm">Adresse</div>
										<div class="panel-body">
									<?php	if(!empty($d_stagiaire["sta_ad1"])){
												echo($d_stagiaire["sta_ad1"] . "<br/>");
											} 
											if(!empty($d_stagiaire["sta_ad2"])){
												echo($d_stagiaire["sta_ad2"] . "<br/>");
											}
											if(!empty($d_stagiaire["sta_ad3"])){
												echo($d_stagiaire["sta_ad3"] . "<br/>");
											}
											echo($d_stagiaire["sta_cp"] . " " . $d_stagiaire["sta_ville"]); ?>
										</div>
									</div>
								</div>
								<div class="col-md-6" >
									<div class="panel" >
										<div class="panel-heading panel-head-sm">Coordonnées</div>
										<div class="panel-body">
											<div class="row" >	
												<div class="col-md-4 text-right">Téléphone :</div>
												<div class="col-md-8" >
													<strong><?=$d_stagiaire["sta_tel"]?></strong>
												</div>
											</div>
											<div class="row" >	
												<div class="col-md-4 text-right">Portable :</div>
												<div class="col-md-8" >
													<strong><?=$d_stagiaire["sta_portable"]?></strong>
												</div>
											</div>
											<div class="row" >	
												<div class="col-md-4 text-right">Mail :</div>
												<div class="col-md-8" >
													<strong><?=$d_stagiaire["sta_mail_perso"]?></strong>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row" >	
								<div class="col-md-12" >
									<div class="panel" >
										<div class="panel-heading panel-head-sm">Autres informations</div>
										<div class="panel-body">
											<div class="row" >	
												<div class="col-md-3 text-right">Date de naissance :</div>
												<div class="col-md-3" >
													<strong><?=$sta_naissance?></strong>
												</div>	
												<div class="col-md-3 text-right">Code postal de naissance :</div>
												<div class="col-md-3" >
													<strong><?=$d_stagiaire["sta_cp_naiss"]?></strong>
												</div>
											</div>
											<div class="row" >	
												<div class="col-md-3 text-right">Ville de naissance :</div>
												<div class="col-md-3" >
													<strong><?=$d_stagiaire["sta_ville_naiss"]?></strong>
												</div>	
												<div class="col-md-3 text-right">Employeur actuel :</div>
												<div class="col-md-3" >
													<strong><?=$d_stagiaire["sta_client_nom"]?></strong>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					<?php
							if(!empty($sta_formations)){ ?>
								<div class="panel" >
									<div class="panel-heading panel-head-sm">Formations suivis par <?=$d_stagiaire["sta_prenom"] . " " . $d_stagiaire["sta_nom"]?></div>
									<div class="panel-body">
										<table class="table" >								
											<thead>
												<tr>
													<th colspan="2" class="text-center" >Client</th>
													<th class="text-center" >Date</th>
													<th colspan="2" class="text-center" >Formation</th>
													<th class="text-center" >Attestations</th>
												</tr>
											</thead>
											<tbody>
									<?php	foreach($sta_formations as $sta_form){ ?>
												<tr>
													<td><?=$sta_form["cli_code"]?></td>
													<td><?=$sta_form["cli_nom"]?></td>
													<td><?=$sta_form["date"]?></td>
										<?php		if($sta_form["societe"]==$acc_societe AND ($acc_agence==0 OR $acc_agence==$sta_form["agence"]) ){ ?>
														<td>											
															<a href="action_voir_sta.php?action=<?=$sta_form["action"]?>&action_client=<?=$sta_form["action_client"]?>" >
																<?=$sta_form["produit_code"]?>
															</a>
														</td>
														<td>											
															<a href="action_voir_sta.php?action=<?=$sta_form["action"]?>&action_client=<?=$sta_form["action_client"]?>" >
																<?=$sta_form["produit_nom"]?>
															</a>
														</td>
										<?php		}else{ ?>
														<td><?=$sta_form["produit_code"]?></td>
														<td><?=$sta_form["produit_nom"]?></td>
										<?php		} ?>
													<td>
											<?php 		if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Attestations/" . $sta_form["societe"] . "/attestation_". $sta_form["action"] . "_" . $stagiaire . ".pdf")){ ?>
															<a href="documents/Attestations/<?= $sta_form["societe"]?>/attestation_<?=$sta_form["action"]?>_<?=$stagiaire?>.pdf" download="<?=$d_stagiaire["sta_nom"] . " " . $sta_form["produit_code"]?>.pdf" >
																<i class='fa fa-file-pdf-o' ></i>
															</a>
											<?php		}else{
															echo("&nbsp;");
														} ?>
				
													</td>
												</tr>
									<?php	} ?>
										</table>
									</div>
								</div>
					<?php	}else{ ?>
								<div class="col-md-12 text-center" style="padding:0;" >
									<div class="alert alert-warning" style="border-radius:0px;">
										Ce stagiaire n'a suivi aucune formation.
									</div>
								</div>
					<?php	} ?>
						</div>
						<div class="col-md-3" >
							<div class="panel" >
								<div class="panel-heading panel-head-sm">Historique client</div>
								<div class="panel-body">
									<table class="table" >
										<thead>
											<tr>
												<th>Code</th>
												<th>Nom</th>
												<th>Particulier</th>
											</tr>
										</thead>
										<tbody>
							<?php			foreach($d_clients as $client){ 
												$particulier="";
												if($client["cli_categorie"]==3){
													$particulier="OUI";
												} ?>
												<tr>
													<td><?=$client["cli_code"]?></td>
													<td><?=$client["cli_nom"]?></td>
													<td><?=$particulier?></td>
												</tr>
							<?php			} ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="stagiaire_liste.php" class="btn btn-default btn-sm" role="button">
						<span class="fa fa-left"></span>
						<span class="hidden-xs">Retour</span>
					</a>
				</div>
				<div class="col-xs-6 footer-middle">&nbsp;</div>
				<div class="col-xs-3 footer-right">
					<a href="stagiaire.php?stagiaire=<?=$stagiaire?>" class="btn btn-warning btn-sm" role="button">
						<i class="fa fa-pencil"></i> Modifier
					</a>
				</div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>  
		<script type="text/javascript">
			jQuery(document).ready(function () {
	   
			});
		</script>
	</body>
</html>
