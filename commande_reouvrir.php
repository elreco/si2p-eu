<?php 
/////////////////// MENU ACTIF /////////////////////// 
$menu_actif = 5;
/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');

///////////////////// Contrôles des parametres ////////////////////
$param_commande = 0;

if(!empty($_GET['commande'])){
    // si les get sont pas remplis
    $param_commande = intval($_GET['commande']);

}
if($param_commande == 0){
 
    echo("Impossible d'afficher la page");
    die();
}
///////////////////// FIN Contrôles des parametres ////////////////////

/////////////// TRAITEMENTS BDD ///////////////////////
/*// sélectionner la commande
$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id =" . $_GET['commande']);
$req->execute();
$commande = $req->fetch();*/


?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Commande</title> 
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">

    <form method="post" action="commande_reouvrir_enr.php" enctype="multipart/form-data">

        <div id="main">
    <?php   include "includes/header_def.inc.php"; ?>
            <section id="content_wrapper" >
                <section id="content" class="animated fadeIn">
                    <div class="row"> 
                        <div class="col-md-10 col-md-offset-1">
                            <div class="admin-form theme-primary ">
                                <div class="panel heading-border panel-primary">
                                    <div class="panel-body bg-light">
                                        <div class="content-header">
                                            <h2>Réouvrir la <b class="text-primary">demande d'achat</b></h2>            
                                        </div>
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <input type="hidden" name="commande" value="<?= $_GET['commande'] ?>">
                                                <div class="col-md-6">
                                                    <p class="text-left mb20" style="font-weight:bold;">Motif de réouverture :</p>
                                                    <div class="section">
                                                        <select id="chi_type" name="chi_type" class="select2">
                                                            <option value="1" selected="selected">Changement de prix</option>
                                                            <option value="2">Changement de produit</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-left mb20" style="font-weight:bold;">Commentaire :</p>
                                                    <textarea id="chi_txt" name="chi_txt" class="summernote"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>
        
        <footer id="content-footer" class="affix">
            <div class="row">
                <div class="col-xs-3 footer-left">
                    <a href="commande_voir.php?commande=<?= $_GET['commande'] ?>" class="btn btn-default btn-sm">
                        <i class='fa fa-long-arrow-left'></i> Retour
                    </a>
                </div>
                <div class="col-xs-6 footer-middle">

                </div>
                <div class="col-xs-3 footer-right">
                    <button type="submit" name="search" class="btn btn-success btn-sm">
                        <i class='fa fa-floppy-o'></i> Enregistrer
                    </button>
                </div>
            </div>
        </footer>
    </form>
    
<?php 
    include "includes/footer_script.inc.php"; ?>  
    <script src="vendor/plugins/summernote/summernote.min.js"></script>
    <script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
    <script src="vendor/plugins/select2/js/select2.min.js"></script>
    <script type="text/javascript">
   
    </script>

</body>
</html>