<?php 

include('includes/connexion_soc.php');

if(isset($_GET['id'])){
	// suppression suspect
	$req = $ConnSoc->prepare("DELETE FROM suspects WHERE sus_id = :sus_id");
	$req->bindParam("sus_id",$_GET['id']);
	$req->execute();

	// suppression des CORRESPONDANCES liées au suspect

	$req = $ConnSoc->prepare("SELECT sco_suspect, sco_id FROM suspects_correspondances WHERE sco_suspect = :sus_id");
	$req->bindParam(':sus_id', $_GET['id']);
	$req->execute();
	$suspects_correspondances = $req->fetchAll();

	foreach($suspects_correspondances as $s){
		$req = $ConnSoc->prepare("DELETE FROM suspects_correspondances WHERE sco_id = :sco_id");
		$req->bindParam("sco_id",$s['sco_id']);
		$req->execute();
	}

	// suppression des INFOS liées au suspect

	$req = $ConnSoc->prepare("SELECT sin_id, sin_suspect FROM suspects_infos WHERE sin_suspect = :sus_id");
	$req->bindParam(':sus_id', $_GET['id']);
	$req->execute();
	$suspects_infos = $req->fetchAll();
	
	foreach($suspects_infos as $s){
		$req = $ConnSoc->prepare("DELETE FROM suspects_infos WHERE sin_id = :sin_id");
		$req->bindParam("sin_id",$s['sin_id']);
		$req->execute();
	}

	// suppression des CONTACTS liées au suspect

	$req = $ConnSoc->prepare("SELECT sco_id, sco_ref_id FROM suspects_contacts WHERE sco_ref_id = :sus_id");
	$req->bindParam(':sus_id', $_GET['id']);
	$req->execute();
	$suspects_contacts = $req->fetchAll();
	
	foreach($suspects_contacts as $s){
		$req = $ConnSoc->prepare("DELETE FROM suspects_contacts WHERE sco_id = :sco_id");
		$req->bindParam("sco_id",$s['sco_id']);
		$req->execute();
	}

	// suppression des ADRESSES liées au suspect

	$req = $ConnSoc->prepare("SELECT sad_id, sad_ref_id FROM suspects_adresses WHERE sad_ref_id = :sad_ref_id");
	$req->bindParam(':sad_ref_id', $_GET['id']);
	$req->execute();
	$suspects_adresses = $req->fetchAll();
	
	foreach($suspects_adresses as $s){
		$req = $ConnSoc->prepare("DELETE FROM suspects_adresses WHERE sad_id = :sad_id");
		$req->bindParam("sad_id",$s['sad_id']);
		$req->execute();
	}

	Header('Location: client_liste.php?succes=3');

}

?>