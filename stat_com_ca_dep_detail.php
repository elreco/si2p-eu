<?php

	// STATISTIQUE CA PAR PRODUIT

	include "includes/controle_acces.inc.php";

	include "includes/connexion.php";
	include "includes/connexion_soc.php";
	include "includes/connexion_fct.php";

	$erreur_txt="";

	// CONTROLE ACCES

	if($_SESSION['acces']['acc_service'][1]!=1){
		echo("Accès refusé!");
		die();
	}

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}


	// TRAITEMENT DU FORM
	if(empty($acc_agence)){
		$agence=0;
		if(!empty($_GET["agence"])){
			$agence=intval($_GET["agence"]);
		}
	}else{
		$agence=$acc_agence;
	}
	
	$commercial=0;
	if(!empty($_GET["commercial"])){
		$commercial=intval($_GET["commercial"]);
	}
	
	$categorie=0;
	if(!empty($_GET["categorie"])){
		$categorie=intval($_GET["categorie"]);
	}
	
	$departement=0;
	if(!empty($_GET["departement"])){
		$departement=intval($_GET["departement"]);
		if($departement<10){
			$departement="0" . $departement;
		}
	}
	
	$periode_deb="";
	if(!empty($_GET["periode_deb"])){
		$DT_periode_deb=date_create_from_format('Y-m-d',$_GET["periode_deb"]);
		if(!is_bool($DT_periode_deb)){
			$periode_deb=$DT_periode_deb->format("Y-m-d");
		}
	}

	$periode_fin="";
	if(!empty($_GET["periode_fin"])){
		$DT_periode_fin=date_create_from_format('Y-m-d',$_GET["periode_fin"]);
		if(!is_bool($DT_periode_fin)){
			$periode_fin=$DT_periode_fin->format("Y-m-d");
		}
	}
	
	if(empty($periode_deb) OR empty($periode_fin)){
		$erreur_txt="Paramètres manquants";
	}
	

	
	if(empty($erreur_txt)){
		

		$sql_fac="SELECT cli_code,cli_nom
		,com_label_1,com_label_2
		,fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date_fr,fac_commercial
		,fli_montant_ht,fli_code_produit
		,act_adr_nom,act_adr_cp,act_adr_ville
		FROM Factures INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture) 
		INNER JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id AND Factures_Lignes.fli_action_cli_soc=" . $acc_societe . ") 
		INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
		INNER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)
		INNER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
		AND NOT fac_client=8891 AND fac_date>='" . $periode_deb . "' AND fac_date<='" . $periode_fin . "'";
		if(!empty($agence)){
			$sql_fac.=" AND fac_agence=" . $agence;	
		}
		if(!empty($commercial)){
			$sql_fac.=" AND fac_commercial=" . $commercial;	
		} 
		if($categorie==1){
			$sql_fac.=" AND NOT fac_cli_categorie=2";	
		}elseif($categorie==2){
			$sql_fac.=" AND fac_cli_categorie=2";	
		}
		if(!empty($departement)){
			if($departement=="00"){
				$sql_fac.=" AND (act_adr_cp='' OR ISNULL(act_adr_cp))";	
			}else{
				$sql_fac.=" AND act_adr_cp LIKE '" . $departement . "%'";	
			}
		}
		$sql_fac.=" ORDER BY cli_code,cli_nom,fac_numero;";
		$req=$ConnSoc->query($sql_fac);
		$d_donnees=$req->fetchAll();
		
		// AJOUT DES DONNEES GC
		
		if($acc_societe==4 AND ($agence==4 OR $agence==0)){
			
			die();
		
			
			$soc_bcl=0;
			
			$sql_fac_gc="SELECT cli_code,cli_nom
			,com_label_1,com_label_2
			,fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date_fr,fac_commercial
			,fli_montant_ht,fli_code_produit,fli_action_cli_soc,fli_action_client
			FROM Factures INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture) 
			INNER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)
			INNER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
			AND fac_date>='" . $periode_deb . "' AND fac_date<='" . $periode_fin . "'  AND NOT fli_action_cli_soc=4 AND NOT fli_action_cli_soc=0";
			if(!empty($agence)){
				$sql_fac_gc.=" AND fac_agence=" . $agence;	
			}
			if(!empty($commercial)){
				$sql_fac_gc.=" AND fac_commercial=" . $commercial;	
			} 
			if($categorie==1){
				$sql_fac_gc.=" AND NOT fac_cli_categorie=2";	
			}elseif($categorie==2){
				$sql_fac_gc.=" AND fac_cli_categorie=2";	
			}
			$sql_fac_gc.=" ORDER BY fli_action_cli_soc,cli_code,cli_nom,fac_numero;";
			$req_gc=$ConnSoc->query($sql_fac_gc);
			$d_donnees_gc=$req_gc->fetchAll();
			if(!empty($d_donnees_gc)){
				
				foreach($d_donnees_gc as $d_gc){
					
					if($d_gc["fli_action_cli_soc"]!=$soc_bcl){
						
						$soc_bcl=$d_gc["fli_action_cli_soc"];
						
						$ConnFct=connexion_fct($soc_bcl);
						
						$sql_act_gc="SELECT act_adr_nom,act_adr_cp,act_adr_ville FROM Actions,Actions_Clients WHERE act_id=acl_action AND acl_id=:action_client";
						if(!empty($departement)){
							if($departement=="00"){
								$sql_act_gc.=" AND (act_adr_cp='' OR ISNULL(act_adr_cp))";	
							}else{
								$sql_act_gc.=" AND act_adr_cp LIKE '" . $departement . "%'";	
							}
						}
						$sql_act_gc.=";";
						$req_act_gc=$ConnFct->prepare($sql_act_gc);					
					}
					
					$req_act_gc->bindParam(":action_client",$d_gc["fli_action_client"]);
					$req_act_gc->execute();
					$d_action_lieu=$req_act_gc->fetch();
					if(!empty($d_action_lieu)){
						
						$d_donnees[]=array(
							"cli_code" => $d_gc["cli_code"],
							"cli_nom" => $d_gc["cli_nom"],
							"com_label_1" => $d_gc["com_label_1"],
							"com_label_2" => $d_gc["com_label_2"],
							"fac_numero" => $d_gc["fac_numero"],
							"fac_date_fr" => $d_gc["fac_date_fr"],
							"fli_montant_ht" => $d_gc["fli_montant_ht"],
							"fli_code_produit" => $d_gc["fli_code_produit"],
							"act_adr_nom" => $d_action_lieu["act_adr_nom"],
							"act_adr_cp" => $d_action_lieu["act_adr_cp"],
							"act_adr_ville" => $d_action_lieu["act_adr_ville"]
						);

					}
					
				}
			
			}
			
		}
		
		// TITRE DE LA STAT 
		
		$titre="CA"; 
		if($categorie==1){			
			$titre.=" régional";
		}elseif($categorie==2){			
			$titre.=" grands-comptes";
		}
		
		if(!empty($commercial)){
			
			$sql="SELECT com_label_1,com_label_2 FROM Commerciaux WHERE com_id=:commercial;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":commercial",$commercial);
			$req->execute();
			$d_commercial=$req->fetch();
			if(!empty($d_commercial)){
				$titre.=" de " . $d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"];
			}
			
		}elseif(!empty($agence)){
			
			$sql="SELECT age_nom,soc_nom FROM Agences,Societes WHERE age_societe=soc_id AND age_id=:agence;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":agence",$agence);
			$req->execute();
			$d_agence=$req->fetch();
			if(!empty($d_agence)){
				$titre.=" de " . $d_agence["soc_nom"] . " - " . $d_agence["age_nom"];
			}
			
		}else{
			
			$sql="SELECT soc_nom FROM Societes WHERE soc_id=:acc_societe;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":acc_societe",$acc_societe);
			$req->execute();
			$d_societe=$req->fetch();
			if(!empty($d_societe)){
				$titre.=" de " . $d_societe["soc_nom"];
			}		
		}
		
		$titre.="<br/>Du " . $DT_periode_deb->format("d/m/Y") . " au " . $DT_periode_fin->format("d/m/Y");
		
		if(!empty($departement)){			
			$titre.="<br/>Formations réalisées dans le " . $departement;
		}
		
		
		
		
	}
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">

<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" >
					<div class="row ">
			<?php		if(!empty($erreur_txt)){
							echo("<p class='alert alert-danger'>" . $erreur_txt . "</p>");
						}else{ ?>

							<h1 class="text-center" ><?=$titre?></h1>

							<div class="table-responsive">
								<table class="table table-striped table-hover" >
									<thead>
										<tr class="dark">
											<th class="text-center" >Code client</th>
											<th class="text-center" >Nom client</th>
											<th class="text-center" >Commercial facture</th>
											<th class="text-center" >Numéro facture</th>
											<th class="text-center" >Date facture</th>
											<th class="text-center" >Produit</th>
											<th class="text-center" >CA</th>
											<th class="text-center" >Nom intervention</th>	
											<th class="text-center" >CP intervention</th>
											<th class="text-center" >Ville intervention</th>
										</tr>
									</thead>
									<tbody>
							<?php		$total_ca=0;
										foreach($d_donnees as $d){ 
											$total_ca=$total_ca+$d["fli_montant_ht"];?>
											<tr>
												<td><?=$d["cli_code"]?></td>
												<td><?=$d["cli_nom"]?></td>
												<td><?=$d["com_label_1"] . " " . $d["com_label_2"]?></td>
												<td><?=$d["fac_numero"]?></td>
												<td><?=$d["fac_date_fr"]?></td>
												<td><?=$d["fli_code_produit"]?></td>
												<td class="text-right" ><?=number_format($d["fli_montant_ht"],2,","," ")?></td>
												<td><?=$d["act_adr_nom"]?></td>
												<td><?=$d["act_adr_cp"]?></td>
												<td><?=$d["act_adr_ville"]?></td>
											</tr>
							<?php 		} ?>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="6" class="text-right" >Total :</td>
											<td class="text-right"><?=number_format($total_ca,2,","," ")?></td>
											<td colspan="3" >&nbsp;</td>								
										</tr>
									</tfoot>
								</table>
							</div>
				<?php	} ?>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="stat_com_ca_dep.php?agence=<?=$agence?>&commercial=<?=$commercial?>&periode_deb=<?=$DT_periode_deb->format("Y-m-d");?>&periode_fin=<?=$DT_periode_fin->format("Y-m-d");?>" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){



			});
		</script>
	</body>
</html>
