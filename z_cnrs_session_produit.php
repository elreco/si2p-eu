<?php  

	// migration donnée pour compatibilite nouveau systeme
	
	include('includes/connexion.php');

	$sql="INSERT INTO Cnrs_Produits (cpr_libelle) VALUES ('EPI');";
	$Conn->query($sql);
	
	$sql="INSERT INTO Cnrs_Produits (cpr_libelle) VALUES ('Maxi2P');";
	$Conn->query($sql);
	
	$sql="INSERT INTO Cnrs_Produits (cpr_libelle) VALUES ('Proxi2P');";
	$Conn->query($sql);
	
	// MIGRATION DES STAGIAIRES
	
	$sql_add="INSERT INTO Cnrs_Stagiaires (sta_id,sta_nom,sta_prenom,sta_naissance,sta_ref_1,sta_ref_2,sta_ref_3,sta_archive)
	VALUES (:sta_id,:sta_nom,:sta_prenom,:sta_naissance,:sta_ref_1,:sta_ref_2,:sta_ref_3,:sta_archive);";
	$req_add=$Conn->prepare($sql_add);
	
	$sql_up="UPDATE Sessions_Stagiaires SET sst_stagiaire=:new_stagiaire WHERE sst_stagiaire=:old_stagiaire;";
	$req_up=$Conn->prepare($sql_up);

	$sql="SELECT * FROM Stagiaires,Stagiaires_Clients WHERE sta_id=scl_stagiaire AND scl_client=17988 ORDER BY sta_id;";
	$req=$Conn->query($sql);
	$d_stagiaires=$req->fetchAll();
	if(!empty($d_stagiaires)){
		foreach($d_stagiaires as $sta){
			
			$sta_id=$sta["sta_id"]-400000;
			
			$sta_nom=trim($sta["sta_nom"]);
			$sta_prenom=trim($sta["sta_prenom"]);
			
			$req_add->bindParam(":sta_id",$sta_id);
			$req_add->bindParam(":sta_nom",$sta_nom);
			$req_add->bindParam(":sta_prenom",$sta_prenom);
			$req_add->bindParam(":sta_naissance",$sta["sta_naissance"]);
			$req_add->bindParam(":sta_ref_1",$sta["scl_ref_1"]);
			$req_add->bindParam(":sta_ref_2",$sta["scl_ref_2"]);
			$req_add->bindParam(":sta_ref_3",$sta["scl_ref_3"]);
			$req_add->bindParam(":sta_archive",$sta["scl_archive"]);
			try{
				$req_add->execute();				
			}catch(Exception $e){
				echo($e->getMessage());
				die();
			}
			
			$req_up->bindParam(":new_stagiaire",$sta_id);
			$req_up->bindParam(":old_stagiaire",$sta["sta_id"]);
			try{
				$req_up->execute();				
			}catch(Exception $e){
				echo($e->getMessage());
				die();
			}
		}
		
	}
	
	// MAJ SESSIONS ET AJOUT HORRAIRE

	$sql_add="INSERT INTO Sessions_Horaires (sho_session,sho_h_deb,sho_h_fin)
	VALUES (:sho_session,:sho_h_deb,:sho_h_fin);";
	$req_add=$Conn->prepare($sql_add);
	
	$sql_up="UPDATE Sessions SET ses_produit=:ses_produit,ses_demi=:ses_demi WHERE ses_id=:ses_id;";
	$req_up=$Conn->prepare($sql_up);
	
	$sql_sta="UPDATE Sessions_Stagiaires SET sst_horaire=:sst_horaire WHERE sst_session=:ses_id;";
	$req_sta=$Conn->prepare($sql_sta);
	
	$sql="SELECT * FROM Sessions ORDER BY ses_id;";
	$req=$Conn->query($sql);
	$d_sessions=$req->fetchAll();
	if(!empty($d_sessions)){
		foreach($d_sessions as $s){
			
			$ses_produit=0;
			if($s["ses_formation"]=="EPI"){
				$ses_produit=1;
			}elseif($s["ses_formation"]=="Maxi2P"){
				$ses_produit=2;
			}
			
			if($s["ses_h_deb"]<="13h00"){
				$ses_demi=1;
			}else{
				$ses_demi=2;
			}
			
			
			$req_up->bindParam(":ses_produit",$ses_produit);
			$req_up->bindParam(":ses_demi",$ses_demi);
			$req_up->bindParam(":ses_id",$s["ses_id"]);
			try{
				$req_up->execute();				
			}catch(Exception $e){
				echo("A");
				echo($e->getMessage());
				die();
			}
			
			// CREATION HORAIRE
			
			$req_add->bindParam(":sho_session",$s["ses_id"]);
			$req_add->bindParam(":sho_h_deb",$s["ses_h_deb"]);
			$req_add->bindParam(":sho_h_fin",$s["ses_h_fin"]);
			try{
				$req_add->execute();
				$horaire=$Conn->lastInsertId();
			}catch(Exception $e){
				echo("B");
				echo($e->getMessage());
				die();
			}
			
			$req_sta->bindParam(":sst_horaire",$horaire);
			$req_sta->bindParam(":ses_id",$s["ses_id"]);
			try{
				$req_sta->execute();				
			}catch(Exception $e){
				echo("C");
				echo($e->getMessage());
				die();
			}
			
		}
	}
	
	echo("terminée");
	
	
?>
