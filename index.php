﻿<?php 
session_start(); 

?>
<html>
<head>
	<meta charset="utf-8" >
	<title>Si2P - Formations incendie, sécurité et prévention</title>
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.min.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/login.css">
	<link href='https://fonts.googleapis.com/css?family=Audiowide' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="main">
		<div class="logo" style="margin-bottom:20px;">
			<img src="assets/img/login/logo.png" />
		</div>
		
		<div class="formulaire">
		
			<h1>Identification</h1>
			
			<div class="formulaire-body" >
	
				<form method="post" action="acces.php" id="formul" autocomplete="off">
					<label>Identifiant</label>
					<input type="text" name="ident" autocomplete="new-password">
					<!-- disables autocomplete --><input type="text" style="display:none">
					<label>Mot de passe</label>
					<input type="password" name="passe" autocomplete="new-password">
					<label>Espace</label>
					<select name="espace" >
						<option value="0" >Espace client / partenaire</option>
						<option value="1" >Espace collaborateur</option>
						<option value="2" >Espace GMS</option>
					</select>
					<input type="submit" value="Me connecter">
					<hr style="margin-top:20px;margin-bottom:20px;">
					<div class="mdp" >
						<a href="oublie.php" >Mot de passe oublié ?</a>
					</div>
				</form>
				
			</div>
		</div>
	</div>
	
	<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
	<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
	<script src="vendor/plugins/pnotify/pnotify.js"></script>
	<script>
		jQuery(document).ready(function(){	
		
	<?php 	// ALERT A DESTINATION DES UTRILISATEURS
			if(!empty($_SESSION['message'])){
				foreach($_SESSION['message'] as $m){ ?>
					new PNotify({
						title: "<?= $m['titre'] ?>",
						text: "<?= $m['message'] ?>",
						type: "<?= $m['type'] ?>", // all contextuals available(info,system,warning,etc)
						hide: true,
						delay: 4000
					});
	<?php 		}
				unset($_SESSION['message']);
			} ?>
		});
	</script>	
</body>
</html>