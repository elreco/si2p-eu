<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_fct.php");

// PARAMETRE
$utilisateur=0;
if(isset($_GET["utilisateur"])){
	if(!empty($_GET["utilisateur"])){
		$utilisateur=intval($_GET["utilisateur"]);
	}
}
$exercice=0;
if(isset($_GET["exercice"])){
	if(!empty($_GET["exercice"])){
		$exercice=intval($_GET["exercice"]);
	}
}

// PERSONNE CONNECTE
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
}
// VALEUR PAR DEFAUT
if($exercice==0){
	if(date("n")<4){
		$exercice=date("Y")-1;
	}else{
		$exercice=date("Y");
	}
}
$exercice_fin=$exercice+1;

if(date("n")<4){
	$exercice_courant=date("Y")-1;
}else{
	$exercice_courant=date("Y");
}

$erreur_txt="";
$warning_txt="";

// CONTROLE ACCES (reporter sur utilisateur rem fac )

if($_SESSION["acces"]["acc_droits"][26]){
	if($utilisateur>0 AND $exercice>0){
		$sql="SELECT uti_nom,uti_prenom,uti_profil,uti_agence,uti_societe FROM Utilisateurs LEFT JOIN Utilisateurs_Societes
		ON (Utilisateurs.uti_societe=Utilisateurs_Societes.uso_societe AND Utilisateurs.uti_agence=Utilisateurs_Societes.uso_agence)
		WHERE uti_id=" . $utilisateur . " AND uso_utilisateur=" . $acc_utilisateur;
		if($_SESSION['acces']["acc_profil"]==3){
			// le com de voit que sa rem
			$sql.=" AND uti_id=" . $acc_utilisateur;
		}elseif($_SESSION['acces']["acc_profil"]==15){
			// le RA voit sa rem et celle des com
				$sql.=" AND (uti_id=" . $acc_utilisateur . " OR uti_profil=3)";
		}elseif($_SESSION['acces']["acc_profil"]==10){
			// responsable d'exploitation
			$sql.=" AND (uti_id=" . $acc_utilisateur . " OR uti_profil IN (3,15) )";
			if($acc_utilisateur!=3){
				$sql.=" AND uti_societe!=4";
			}
		}
		$sql.=";";
		$req=$Conn->query($sql);

		$d_utilisateur=$req->fetch();
		if(!empty($d_utilisateur)){
			$titre="Rémunération " . $d_utilisateur["uti_nom"] . " " . $d_utilisateur["uti_prenom"] . " pour l'exercice " . $exercice . "/" . $exercice_fin;
		}else{
			$erreur_txt="Impossible d'afficher la page (err:001)!";
		}
	}else{
		$erreur_txt="Impossible d'afficher la page (err:002)!";
	}
}else{
	$erreur_txt="Impossible d'afficher la page (err:003)!";
}

// PARAMETRE DE LA REM
IF(empty($erreur_txt)){
	$sql="SELECT * FROM Utilisateurs_Rem_Param WHERE urp_utilisateur=" . $utilisateur . " AND urp_exercice=" . $exercice . ";";
	/*echo($sql);
	die();*/
	$req=$Conn->query($sql);
	$d_rem=$req->fetch();
	if(empty($d_rem)){
		$warning_txt="Paramètres de rémunération non renseignés!";
	}elseif(empty($d_rem["upr_rem_type"])){
		$warning_txt="Le type de rémunération n'est pas renseigné!";
	}

	/*if($exercice==2019 AND $_SESSION['acces']["acc_profil"]!=13 AND $_SESSION['acces']["acc_profil"]!=10 AND $_SESSION['acces']["acc_profil"]!=11 AND $_SESSION['acces']["acc_profil"]!=7 AND $_SESSION['acces']["acc_profil"]!=14){
		$erreur_txt="Paramètres de rémunération non renseignés!";

	}*/

}


IF(empty($erreur_txt) AND empty($warning_txt)){

	// CATEGORIE
	$d_categories=array();
	$sql="SELECT pca_id,pca_libelle FROM Produits_Categories ORDER BY pca_id;";
	$req=$Conn->query($sql);
	$d_result_cat=$req->fetchAll();
	if(!empty($d_result_cat)){
		foreach($d_result_cat as $cat){
			$d_categories[$cat["pca_id"]]=$cat["pca_libelle"];
		}
	}

	// Famille
	$d_familles=array();
	$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles  ORDER BY pfa_id;";
	$req=$Conn->query($sql);
	$d_result_fam=$req->fetchAll();
	if(!empty($d_result_fam)){
		foreach($d_result_fam as $fam){
			$d_familles[$fam["pfa_id"]]=$fam["pfa_libelle"];
		}
	}

	// Sous-Famille
	$d_s_familles=array();
	$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles ORDER BY psf_id;";
	$req=$Conn->query($sql);
	$d_result_s_fam=$req->fetchAll();
	if(!empty($d_result_s_fam)){
		foreach($d_result_s_fam as $s_fam){
			$d_s_familles[$s_fam["psf_id"]]=$s_fam["psf_libelle"];
		}
	}

	// Sous-Sous-Famille
	$d_s_s_familles=array();
	$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles ORDER BY pss_id;";
	$req=$Conn->query($sql);
	$d_result_s_s_fam=$req->fetchAll();
	if(!empty($d_result_s_s_fam)){
		foreach($d_result_s_s_fam as $s_s_fam){
			$d_s_s_familles[$s_s_fam["pss_id"]]=$s_s_fam["pss_libelle"];
		}
	}

	$lib_mois=array(
		"1" => "Janvier",
		"2" => "Février",
		"3" => "Mars",
		"4" => "Avril",
		"5" => "Mai",
		"6" => "Juin",
		"7" => "Juillet",
		"8" => "Aout",
		"9" => "Septembre",
		"10" => "Octobre",
		"11" => "Novembre",
		"12" => "Décembre"
	);

	// SUR LA SOC DE U
	$intra_inter=1;
	if (!empty($d_utilisateur["uti_agence"])) {
		$sql="SELECT age_intra_inter FROM Agences WHERE age_id=" . $d_utilisateur["uti_agence"] . " AND age_intra_inter=2;";
		$req=$Conn->query($sql);
		$d_agence=$req->fetch();
		if(!empty($d_agence)){
			$intra_inter=2;
		}
	} else {
		$sql="SELECT soc_intra_inter FROM Societes WHERE soc_id=" . $d_utilisateur["uti_societe"] . " AND soc_intra_inter=2;";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();
		if(!empty($d_societe)){
			$intra_inter=2;
		}
	}
	

	// LES PERIODES DE REM

	$d_periodes=array();
	$sql="SELECT urp_mois,urp_periode FROM Utilisateurs_Rem_Periode WHERE urp_exercice=" . $exercice . " AND urp_type=" . $intra_inter . " ORDER BY urp_mois;";
	$req=$Conn->query($sql);
	$d_result_per=$req->fetchAll();
	if(!empty($d_result_per)){
		foreach($d_result_per as $r){
			$d_periodes[$r["urp_mois"]]=$r["urp_periode"];
		}
	}else{
		$d_periodes=array(
			"1" => 4,
			"2" => 4,
			"3" => 4,
			"4" => 1,
			"5" => 1,
			"6" => 1,
			"7" => 2,
			"8" => 2,
			"9" => 2,
			"10" => 3,
			"11" => 3,
			"12" => 3
		);

	}

	/*echo("<pre>");
		print_r($d_periodes);
	echo("</pre>");
	die();*/

	// TABLEAU POUR ASSOCIE LA CLASSIFICATION PRODUIT A UNE LIGNE DE REM
	//  la clé cat fam ... peut varié selon les mois car possible de ne pas avoir d'objectif pour une famille sur un mois donnée
	//

	if(!empty($d_rem["urp_prime_produit"])){
		$cle=array(
			"0" => array( // mois
				"0" => array( // categorie
					"0" => array( // famille
						"0" => array( // sous-famille
							"0" => 0 // sous-sous famille
						)
					)
				)
			)
		);
	}

	// TABLEAU POUR STOCKER LES VALEURS DE REM

	$rem=array();

	function ini_tab_rem($tab_mois){
		global $rem;
		global $exercice;
		global $d_rem;

		$rem[$tab_mois]=array(
			"nb_famille" => 0, // nombre de famille produit identifié sur le mois -> permet de generer la clé en cas de nouvelle famille
			"total" => array(
				"objectif" => 0,
				"libelle" => "R&Eacute;SULTAT MENSUEL",
				"realise" => 0,
				"contentieux" => 0,
				"taux_reussite" => 0,
				"taux_rem" => 0,
				"montant_prime" => 0,
				"montant_rem" => 0,
				"rem_type" => 3
			),
			"interco" => array(
				"libelle" => "INTERCO",
				"realise" => 0,
				"taux_rem" => $d_rem["urp_prct_interco"],
				"montant_prime" => 0,
				"montant_rem" => 0,
				"rem_type" => 2
			),
			"total_rem" => 0
		);

		if(!empty($d_rem["urp_prime_produit"])){

			if($exercice>=2019){
				$val_rem_type=0;
			}else{
				$val_rem_type=1;
			}
			$rem[$tab_mois]["autre"]=array(
				"objectif" => 0,
				"libelle" => "AUTRES",
				"realise" => 0,
				"contentieux" => 0,
				"prct_objectif" => 0,
				"taux_reussite" => 0,
				"taux_rem" => 0,
				"montant_prime" => 0,
				"montant_rem" => 0,
				"categorie" => "autre",
				"famille" => 0,
				"sous_famille" => 0,
				"sous_sous_famille" => 0,
				"rem_type" => $val_rem_type
			);

			if($exercice>=2019){
				$rem[$tab_mois]["autre_1"]=array(
					"objectif" => 0,
					"libelle" => "Prime de lancement – Famille INCENDIE",
					"realise" => 0,
					"contentieux" => 0,
					"prct_objectif" => 0,
					"taux_reussite" => 0,
					"taux_rem" => 0,
					"montant_prime" => 0,
					"montant_rem" => 0,
					"categorie" => "autre_1",
					"famille" => 0,
					"sous_famille" => 0,
					"sous_sous_famille" => 0,
					"rem_type" => 4
				);
				$rem[$tab_mois]["autre_3"]=array(
					"objectif" => 0,
					"libelle" => "Prime de lancement – Famille SANTE",
					"realise" => 0,
					"contentieux" => 0,
					"prct_objectif" => 0,
					"taux_reussite" => 0,
					"taux_rem" => 0,
					"montant_prime" => 0,
					"montant_rem" => 0,
					"categorie" => "autre_3",
					"famille" => 0,
					"sous_famille" => 0,
					"sous_sous_famille" => 0,
					"rem_type" => 4
				);
				$rem[$tab_mois]["ca_no_obj"]=array(
					"objectif" => 0,
					"libelle" => "CA Hors objectif",
					"realise" => 0,
					"contentieux" => 0,
					"prct_objectif" => 0,
					"taux_reussite" => 0,
					"taux_rem" => $d_rem["urp_prct_no_obj"],
					"montant_prime" => 0,
					"montant_rem" => 0,
					"categorie" => "ca_no_obj",
					"famille" => 0,
					"sous_famille" => 0,
					"sous_sous_famille" => 0,
					"rem_type" => 5
				);
			}
		}

	}
	for($m=4;$m<=12;$m++){
		ini_tab_rem($m);
	}
	ini_tab_rem(1);
	ini_tab_rem(2);
	ini_tab_rem(3);


	// prime trimestrielle et annuelle (la prime trimestre est devennu période mais var conserve)
	$trimestre=array();
	for($t=0;$t<=6;$t++){
		if($exercice<2019){
			$lib_p="T".$t;
		}else{
			$lib_p="P".$t;
		}
		$trimestre[$t]=array(
			"objectif" => 0,
			"libelle" => $lib_p,
			"realise" => 0,
			"contentieux" => 0,
			"prct_objectif" => 0,
			"taux_reussite" => 0,
			"taux_rem" => 0,
			"montant_prime" => 0,
			"montant_rem" => 0
		);
	}


	// CONNEXION SUR LA SOCIETE ADMINISTRATIVE DE L'UTILISATEUR
	$ConnFct=connexion_fct($d_utilisateur["uti_societe"]);

	// OBJECTIF
	$nbLigne=0;
	$sql="SELECT cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille,
	SUM(cob_objectif_1) AS objectif_1,SUM(cob_objectif_2) AS objectif_2,SUM(cob_objectif_3) AS objectif_3,
	SUM(cob_objectif_4) AS objectif_4,SUM(cob_objectif_5) AS objectif_5,SUM(cob_objectif_6) AS objectif_6,
	SUM(cob_objectif_7) AS objectif_7,SUM(cob_objectif_8) AS objectif_8,SUM(cob_objectif_9) AS objectif_9,
	SUM(cob_objectif_10) AS objectif_10,SUM(cob_objectif_11) AS objectif_11,SUM(cob_objectif_12) AS objectif_12,
	SUM(cob_objectif_total) AS objectif_total,cob_autre
	FROM Commerciaux
	LEFT JOIN Commerciaux_Objectifs ON (Commerciaux.com_id=Commerciaux_Objectifs.cob_commercial)
	WHERE cob_exercice=" . $exercice;
	if($d_rem["upr_rem_type"]==1){
		// objectif perso
		$sql.=" AND com_ref_1=" . $utilisateur;
	}elseif($d_utilisateur["uti_agence"]>0){
		// autre profil objectif societe ou agence
		$sql.=" AND com_agence=" . $d_utilisateur["uti_agence"];
	}
	$sql.=" GROUP BY cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille,cob_autre ORDER BY objectif_total DESC;";
	//echo($sql);
	//die();
	$req=$ConnFct->query($sql);
	$d_objectifs=$req->fetchAll();
	if(!empty($d_objectifs)){
		foreach($d_objectifs as $obj){

			$libelle="";
			if(!empty($obj["cob_sous_sous_famille"])){
				//$libelle=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]] . "/" .$d_s_familles[$obj["cob_sous_famille"]] . "/" .$d_s_s_familles[$obj["cob_sous_sous_famille"]];
				$libelle=$d_s_s_familles[$obj["cob_sous_sous_famille"]];
			}elseif(!empty($obj["cob_sous_famille"])){
				//$libelle=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]] . "/" .$d_s_familles[$obj["cob_sous_famille"]];
				$libelle=$d_s_familles[$obj["cob_sous_famille"]];
			}elseif(!empty($obj["cob_famille"])){
				$libelle=$d_familles[$obj["cob_famille"]];
				//$libelle=$d_categories[$obj["cob_categorie"]] . "/" . $d_familles[$obj["cob_famille"]];
			}elseif(!empty($obj["cob_categorie"])){
				$libelle=$d_categories[$obj["cob_categorie"]];
			}else{
				$libelle="Divers";
			};
			for($m=1;$m<=12;$m++){

				$val_cle="";

				if(!empty($d_rem["urp_prime_produit"])){
					// si pas de rem produit la ventilation produit ne sert a rien
					if(empty($cle[$m][$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]])){
						if($obj["cob_autre"]==1){
							if($exercice<2019){
								$val_cle="autre";
								$cle[$m][$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]=$val_cle;
							}else{

								if($obj["cob_categorie"]==1 AND isset($rem[$m]["autre_" . $obj["cob_famille"]])){
									$val_cle="autre_" . $obj["cob_famille"];
									$cle[$m][$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]=$val_cle;
								}else{
									$val_cle="autre";
									$cle[$m][$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]=$val_cle;
								}

							}
						}elseif(!empty($obj["objectif_" . $m])){

							$rem[$m]["nb_famille"]++;
							$val_cle=$rem[$m]["nb_famille"];
							$rem[$m][$val_cle]=array(
								"objectif" => 0,
								"libelle" => $libelle,
								"realise" => 0,
								"contentieux" => 0,
								"prct_objectif" => 0,
								"taux_reussite" => 0,
								"taux_rem" => 0,
								"montant_prime" => 0,
								"montant_rem" => 0,
								"categorie" => $obj["cob_categorie"],
								"famille" => $obj["cob_famille"],
								"sous_famille" => $obj["cob_sous_famille"],
								"sous_sous_famille" => $obj["cob_sous_sous_famille"],
								"rem_type" => 1,
							);
							$cle[$m][$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]]=$val_cle;
						}

					}else{
						$val_cle=$cle[$m][$obj["cob_categorie"]][$obj["cob_famille"]][$obj["cob_sous_famille"]][$obj["cob_sous_sous_famille"]];
					}
					if(!empty($val_cle)){

						$rem[$m][$val_cle]["objectif"]=$rem[$m][$val_cle]["objectif"]+$obj["objectif_" . $m];
						$rem[$m]["total"]["objectif"]=$rem[$m]["total"]["objectif"]+$obj["objectif_" . $m];
					}
				}else{
					$rem[$m]["total"]["objectif"]=$rem[$m]["total"]["objectif"]+$obj["objectif_" . $m];
				}


				$periode=$d_periodes[$m];
				$trimestre[$periode]["objectif"]=$trimestre[$periode]["objectif"] + $obj["objectif_" . $m];
				$trimestre[0]["objectif"]=$trimestre[0]["objectif"] + $obj["objectif_" . $m];

			}

		}
	}else{
		$erreur_txt="Objectifs non renseignés!";
	}
}

IF(empty($erreur_txt) AND empty($warning_txt)){

	// LE FACTURES

	$sql="SELECT fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,MONTH(fac_date) AS mois,
	SUM(fli_montant_ht) AS montant_ht";
	if($exercice>=2019){
		$sql.=",acl_derogation,acl_rem_ca,acl_rem_famille";
	}
	$sql.=",fac_numero";
	$sql.=" FROM Factures_Lignes
	LEFT JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
	LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";
	if($exercice>=2019){
		$sql.=" LEFT JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id AND (fli_action_cli_soc=" . $d_utilisateur["uti_societe"] . " OR fli_action_cli_soc=0))
		LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)";
	}
	$sql.=" WHERE fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'";
	$sql.=" AND fli_categorie<4";
	if($exercice>=2019){
		//$sql.=" AND fac_nature=1";
		$sql.=" AND (
			ISNULL(acl_id) OR (
				(act_charge=0 OR ISNULL(act_charge) OR (act_ca/act_charge>=1.2)) AND (acl_derogation=0 OR ISNULL(acl_derogation) OR (acl_derogation>0 AND (acl_rem_ca>0 OR acl_rem_famille>0)))
			)
		)";
	}
	if($d_rem["upr_rem_type"]==1){
		// com facture perso
		$sql.=" AND com_ref_1=" . $utilisateur;
	}elseif($d_utilisateur["uti_agence"]>0){
		//autre facturation agence / societe
		$sql.=" AND com_agence=" . $d_utilisateur["uti_agence"];
	}
	$sql.=" GROUP BY fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,MONTH(fac_date)";
	if($exercice>=2019){
		$sql.=",acl_derogation,acl_rem_ca,acl_rem_famille";
	}
	$sql.=",fac_numero";
	$sql.=";";
	/*echo($sql);
	die();*/
	$req=$ConnFct->query($sql);
	$d_realises=$req->fetchAll();
	/*echo("<pre>");
		print_r($d_realises);
	echo("</pre>");
	die();*/
	if(!empty($d_realises)){
		foreach($d_realises as $realise){


			$montant_ht=0;
			if(!empty($realise["montant_ht"])){
				$montant_ht=$realise["montant_ht"];
			}
			$mois=0;
			if(!empty($realise["mois"])){
				$mois=$realise["mois"];
			}

			$rem_ca=1;
			$rem_famille=1;
			if($exercice>=2019){
				if(!empty($realise["acl_derogation"])){
					if(empty($realise["acl_rem_ca"])){
						$rem_ca=0;
					}
					if(empty($realise["acl_rem_famille"])){
						$rem_famille=0;
					}
				}
			}

			if($mois==5){

				//echo($realise["fac_numero"] . ";" . $montant_ht . "<br/>");

			}


			if(!empty($d_rem["urp_prime_produit"])){

				$cat_id=0;
				if(!empty($realise["fli_categorie"])){
					$cat_id=$realise["fli_categorie"];
				}
				$fam_id=0;
				if(!empty($realise["fli_famille"])){
					$fam_id=$realise["fli_famille"];
				}
				$s_fam_id=0;
				if(!empty($realise["fli_sous_famille"])){
					$s_fam_id=$realise["fli_sous_famille"];
				}
				$s_s_fam_id=0;
				if(!empty($realise["fli_sous_sous_famille"])){
					$s_s_fam_id=$realise["fli_sous_sous_famille"];
				}


				$val_cle=0;
				if(!empty($cle[$mois][$cat_id][$fam_id][$s_fam_id][$s_s_fam_id])){
					$val_cle=$cle[$mois][$cat_id][$fam_id][$s_fam_id][$s_s_fam_id];

					$rem[$mois][$val_cle]["realise"]+=$montant_ht;

				}elseif(!empty($cle[$mois][$cat_id][$fam_id][$s_fam_id][0])){
					$val_cle=$cle[$mois][$cat_id][$fam_id][$s_fam_id][0];
					$rem[$mois][$val_cle]["realise"]+=$montant_ht;

				}elseif(!empty($cle[$mois][$cat_id][$fam_id][0][0])){
					$val_cle=$cle[$mois][$cat_id][$fam_id][0][0];
					$rem[$mois][$val_cle]["realise"]+=$montant_ht;

				}elseif(!empty($cle[$mois][$cat_id][0][0][0])){
					$val_cle=$cle[$mois][$cat_id][0][0][0];
					$rem[$mois][$val_cle]["realise"]+=$montant_ht;

				}else{
					if(!empty($cle[$mois][0][0][0][0]) OR $exercice<2019){
						$val_cle=$cle[$mois][0][0][0][0];
						$rem[$mois][$val_cle]["realise"]+=$montant_ht;
					}else{
						// CA sans objectif
						$val_cle="ca_no_obj";
						$rem[$mois][$val_cle]["realise"]+=$montant_ht;
					}
				}
			}
			if($val_cle!=="ca_no_obj"){
				$rem[$mois]["total"]["realise"]+=$montant_ht;

				$periode=$d_periodes[$mois];
				$trimestre[$periode]["realise"]=$trimestre[$periode]["realise"] + $montant_ht;
				$trimestre[0]["realise"]=$trimestre[0]["realise"] + $montant_ht;
			}

		}
	}
	// GESTION DES AVOIRS

	if($exercice>=2077){

		die();

		// a partir de 2019, les avoirs impacts la rem du mois de la facture initiale

		// on cherche les avoirs qui affectent une facture de l'exercice en cours

		$sql="SELECT fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,MONTH(Factures.fac_date) AS mois
		,fli_montant_ht AS montant_ht,MONTH(Avoirs.fac_date) as mois_avoir
		FROM Factures as Avoirs LEFT JOIN Factures ON (Avoirs.fac_avoir=Factures.fac_id)
		LEFT JOIN Factures_Lignes ON (Avoirs.fac_id=Factures_Lignes.fli_facture)
		LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
		LEFT JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id AND (Factures_Lignes.fli_action_cli_soc=" . $d_utilisateur["uti_societe"] . " OR Factures_Lignes.fli_action_cli_soc=0))
		LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
		WHERE Factures.fac_date>='" . $exercice . "-04-01' AND Factures.fac_date<='" . intval($exercice+1) . "-03-31'
		AND fli_categorie<4
		AND (ISNULL(acl_id) OR act_charge=0 OR (act_ca/act_charge>=1.2))";
		if($d_rem["upr_rem_type"]==1){
			// com facture perso
			$sql.=" AND com_ref_1=" . $utilisateur;
		}elseif($d_utilisateur["uti_agence"]>0){
			//autre facturation agence / societe
			$sql.=" AND com_agence=" . $d_utilisateur["uti_agence"];
		}

		$sql.=";";
		$req=$ConnFct->query($sql);
		$d_avoirs=$req->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($d_avoirs)){
			foreach($d_avoirs as $realise){

				$montant_ht=0;
				if(!empty($realise["montant_ht"])){
					$montant_ht=$realise["montant_ht"];
				}
				$mois=0;
				if(!empty($realise["mois"])){
					$mois=$realise["mois"];
				}
				if(!empty($realise["mois_avoir"]) AND $exercice==2019){
					if($realise["mois_avoir"]==4 OR $realise["mois_avoir"]==5){
						$mois=$realise["mois_avoir"];
					}
				}


				if(!empty($d_rem["urp_prime_produit"])){

					$cat_id=0;
					if(!empty($realise["fli_categorie"])){
						$cat_id=$realise["fli_categorie"];
					}
					$fam_id=0;
					if(!empty($realise["fli_famille"])){
						$fam_id=$realise["fli_famille"];
					}
					$s_fam_id=0;
					if(!empty($realise["fli_sous_famille"])){
						$s_fam_id=$realise["fli_sous_famille"];
					}
					$s_s_fam_id=0;
					if(!empty($realise["fli_sous_sous_famille"])){
						$s_s_fam_id=$realise["fli_sous_sous_famille"];
					}


					$val_cle=0;
					if(!empty($cle[$mois][$cat_id][$fam_id][$s_fam_id][$s_s_fam_id])){
						$val_cle=$cle[$mois][$cat_id][$fam_id][$s_fam_id][$s_s_fam_id];
						$rem[$mois][$val_cle]["realise"]+=$montant_ht;

					}elseif(!empty($cle[$mois][$cat_id][$fam_id][$s_fam_id][0])){
						$val_cle=$cle[$mois][$cat_id][$fam_id][$s_fam_id][0];
						$rem[$mois][$val_cle]["realise"]+=$montant_ht;

					}elseif(!empty($cle[$mois][$cat_id][$fam_id][0][0])){
						$val_cle=$cle[$mois][$cat_id][$fam_id][0][0];
						$rem[$mois][$val_cle]["realise"]+=$montant_ht;

					}elseif(!empty($cle[$mois][$cat_id][0][0][0])){
						$val_cle=$cle[$mois][$cat_id][0][0][0];
						$rem[$mois][$val_cle]["realise"]+=$montant_ht;

					}elseif(!empty($cle[$mois][0][0][0][0])){
						$val_cle=$cle[$mois][0][0][0][0];
						$rem[$mois][$val_cle]["realise"]+=$montant_ht;
					}else{
						// CA sans objectif
						$val_cle="ca_no_obj";
						$rem[$mois][$val_cle]["realise"]+=$montant_ht;
					}
				}
				if($val_cle!="ca_no_obj"){
					$rem[$mois]["total"]["realise"]+=$montant_ht;

					$periode=$d_periodes[$mois];
					$trimestre[$periode]["realise"]=$trimestre[$periode]["realise"] + $montant_ht;
					$trimestre[0]["realise"]=$trimestre[0]["realise"] + $montant_ht;
				}

			}
		}


		if($exercice==2019){

			// sur 2018/2019 avoir impact le mois de l'avoir -> avoir 04/19 ne va pas impacter la rem de 03/19
			// pour assurer une cohérence entre les 2 système, les avoirs liés à une facture de l'ancien exercice impact me mois du nouvel exercice

			$sql="SELECT fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,MONTH(Avoirs.fac_date) AS mois
			,fli_montant_ht as montant_ht
			FROM Factures as Avoirs LEFT JOIN Factures ON (Avoirs.fac_avoir=Factures.fac_id)
			LEFT JOIN Factures_Lignes ON (Avoirs.fac_id=Factures_Lignes.fli_facture)
			LEFT JOIN Commerciaux ON (Avoirs.fac_commercial=Commerciaux.com_id)
			LEFT JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id AND (Factures_Lignes.fli_action_cli_soc=" . $d_utilisateur["uti_societe"] . " OR Factures_Lignes.fli_action_cli_soc=0))
			LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
			WHERE Avoirs.fac_date>='" . $exercice . "-04-01' AND Avoirs.fac_date<='" . intval($exercice+1) . "-03-31'
			AND Factures.fac_date<='" . intval($exercice) . "-03-31'
			AND fli_categorie<4
			AND (ISNULL(acl_id) OR act_charge=0 OR (act_ca/act_charge>=1.2))";
			if($d_rem["upr_rem_type"]==1){
				// com facture perso
				$sql.=" AND com_ref_1=" . $utilisateur;
			}elseif($d_utilisateur["uti_agence"]>0){
				//autre facturation agence / societe
				$sql.=" AND com_agence=" . $d_utilisateur["uti_agence"];
			}

			$sql.=";";
			$req=$ConnFct->query($sql);
			$d_avoirs=$req->fetchAll(PDO::FETCH_ASSOC);
			/*echo("<pre>");
				print_r($d_avoirs);
			echo("</pre>");
			die();*/
			if(!empty($d_avoirs)){
				foreach($d_avoirs as $realise){

					$montant_ht=0;
					if(!empty($realise["montant_ht"])){
						$montant_ht=$realise["montant_ht"];
					}
					$mois=0;
					if(!empty($realise["mois"])){
						$mois=$realise["mois"];
					}


					if(!empty($d_rem["urp_prime_produit"])){

						$cat_id=0;
						if(!empty($realise["fli_categorie"])){
							$cat_id=$realise["fli_categorie"];
						}
						$fam_id=0;
						if(!empty($realise["fli_famille"])){
							$fam_id=$realise["fli_famille"];
						}
						$s_fam_id=0;
						if(!empty($realise["fli_sous_famille"])){
							$s_fam_id=$realise["fli_sous_famille"];
						}
						$s_s_fam_id=0;
						if(!empty($realise["fli_sous_sous_famille"])){
							$s_s_fam_id=$realise["fli_sous_sous_famille"];
						}


						$val_cle=0;
						if(!empty($cle[$mois][$cat_id][$fam_id][$s_fam_id][$s_s_fam_id])){
							$val_cle=$cle[$mois][$cat_id][$fam_id][$s_fam_id][$s_s_fam_id];
							$rem[$mois][$val_cle]["realise"]+=$montant_ht;

						}elseif(!empty($cle[$mois][$cat_id][$fam_id][$s_fam_id][0])){
							$val_cle=$cle[$mois][$cat_id][$fam_id][$s_fam_id][0];
							$rem[$mois][$val_cle]["realise"]+=$montant_ht;

						}elseif(!empty($cle[$mois][$cat_id][$fam_id][0][0])){
							$val_cle=$cle[$mois][$cat_id][$fam_id][0][0];
							$rem[$mois][$val_cle]["realise"]+=$montant_ht;

						}elseif(!empty($cle[$mois][$cat_id][0][0][0])){
							$val_cle=$cle[$mois][$cat_id][0][0][0];
							$rem[$mois][$val_cle]["realise"]+=$montant_ht;

						}elseif(!empty($cle[$mois][0][0][0][0])){
							$val_cle=$cle[$mois][0][0][0][0];
							$rem[$mois][$val_cle]["realise"]+=$montant_ht;
						}else{
							// CA sans objectif
							$val_cle="ca_no_obj";
							$rem[$mois][$val_cle]["realise"]+=$montant_ht;
						}
					}
					if($val_cle!="ca_no_obj"){
						$rem[$mois]["total"]["realise"]+=$montant_ht;

						$periode=$d_periodes[$mois];
						$trimestre[$periode]["realise"]=$trimestre[$periode]["realise"] + $montant_ht;
						$trimestre[0]["realise"]=$trimestre[0]["realise"] + $montant_ht;
					}

				}
			}
		}
	}

	//ON RECHERCHE LE CA CONTENTIEUX

	$sql="SELECT fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,MONTH(fac_date_relance) AS mois,fli_montant_ht
	,fac_date_reg,fac_regle,fac_date_relance,fac_numero
	FROM Factures_Lignes
	LEFT JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
	LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";
	if($exercice==2018){
		$sql.=" WHERE fac_etat_relance=6 AND fac_date_relance>='" . $exercice . "-03-01' AND fac_date_relance<'" . intval($exercice+1) . "-02-01'";
		$sql.=" AND (MONTH(fac_date_relance)=4 OR ISNULL(fac_date_reg) OR DATEDIFF(fac_date_reg,fac_date_relance)>30)";
	}else{
		$sql.=" WHERE fac_etat_relance=6 AND fac_date_relance>='" . $exercice . "-03-01' AND fac_date_relance<'" . intval($exercice+1) . "-02-01'";
		$sql.=" AND (ISNULL(fac_date_reg) OR DATEDIFF(fac_date_reg,fac_date_relance)>30)";
	}
	$sql.=" AND fli_categorie<4";
	if($d_rem["upr_rem_type"]==1){
		// com facture perso
		$sql.=" AND com_ref_1=" . $utilisateur;
	}elseif($d_utilisateur["uti_agence"]>0){
		//autre facturation agence / societe
		$sql.=" AND com_agence=" . $d_utilisateur["uti_agence"];
	}
	//$sql.=" GROUP BY fli_categorie,fli_famille,fli_sous_famille,fli_sous_sous_famille,MONTH(fac_date_relance);";
	$req=$ConnFct->query($sql);
	$d_realises=$req->fetchAll();
	/*echo($sql);
	echo("<pre>");
		print_r($d_realises);
	echo("</pre>");
	die();*/
	if(!empty($d_realises)){
		foreach($d_realises as $realise){

			$cat_id=0;
			if(!empty($realise["fli_categorie"])){
				$cat_id=$realise["fli_categorie"];
			}
			$fam_id=0;
			if(!empty($realise["fli_famille"])){
				$fam_id=$realise["fli_famille"];
			}
			$s_fam_id=0;
			if(!empty($realise["fli_sous_famille"])){
				$s_fam_id=$realise["fli_sous_famille"];
			}
			$s_s_fam_id=0;
			if(!empty($realise["fli_sous_sous_famille"])){
				$s_s_fam_id=$realise["fli_sous_sous_famille"];
			}



			// mois de mise en contentieux
			$mois=0;
			if($exercice==2018){
				if(!empty($realise["mois"])){

					if($realise["mois"]==3 AND $exercice==2018){
						$mois=0;
					}elseif($realise["mois"]==4 AND $exercice==2018){
						$mois=4;
					}else{
						// nouvelle regle de calcul
						$mois=$realise["mois"]+1;
						if($mois==13){
							$mois=1;
						}
					}
				}
			}else{
				if(!empty($realise["mois"])){
					$mois=$realise["mois"]+1;
					if($mois==13){
						$mois=1;
					}
				}
			}


			if(!empty($mois)){

				$montant_ht=0;
				if(!empty($realise["fli_montant_ht"])){
					$montant_ht=$realise["fli_montant_ht"];
				}

				$val_cle=0;
				if(!empty($d_rem["urp_prime_produit"])){
					if(!empty($cle[$mois][$cat_id][$fam_id][$s_fam_id][$s_s_fam_id])){
						$val_cle=$cle[$mois][$cat_id][$fam_id][$s_fam_id][$s_s_fam_id];
						$rem[$mois][$val_cle]["contentieux"]+=$montant_ht;

					}elseif(!empty($cle[$mois][$cat_id][$fam_id][$s_fam_id][0])){
						$val_cle=$cle[$mois][$cat_id][$fam_id][$s_fam_id][0];
						$rem[$mois][$val_cle]["contentieux"]+=$montant_ht;

					}elseif(!empty($cle[$mois][$cat_id][$fam_id][0][0])){
						$val_cle=$cle[$mois][$cat_id][$fam_id][0][0];
						$rem[$mois][$val_cle]["contentieux"]+=$montant_ht;

					}elseif(!empty($cle[$mois][$cat_id][0][0][0])){
						$val_cle=$cle[$mois][$cat_id][0][0][0];
						$rem[$mois][$val_cle]["contentieux"]+=$montant_ht;

					}else{
						if(!empty($cle[$mois][0][0][0][0]) OR $exercice<2019){
							$val_cle=$cle[$mois][0][0][0][0];
							$rem[$mois][$val_cle]["contentieux"]+=$montant_ht;
						}else{
							// CA sans objectif
							$val_cle="ca_no_obj";
							$rem[$mois][$val_cle]["contentieux"]+=$montant_ht;
						}
					}
				}
				if($val_cle!=="ca_no_obj"){

					$rem[$mois]["total"]["contentieux"]+=$montant_ht;

					$periode=$d_periodes[$mois];
					$trimestre[$periode]["contentieux"]=$trimestre[$periode]["contentieux"] + $montant_ht;
					$trimestre[0]["contentieux"]=$trimestre[0]["contentieux"] + $montant_ht;
				}

			}
		}
	}

	// VENDEUR ON VA CHERCHER LE CA INTERCO
	if($d_utilisateur["uti_profil"]==3){

		$sql="SELECT soc_id FROM Societes LEFT JOIN Utilisateurs_Societes On (Societes.soc_id=Utilisateurs_Societes.uso_societe)
		WHERE uso_utilisateur=" . $utilisateur;
		if($d_utilisateur["uti_agence"]==0){
			$sql.=" AND NOT soc_id=" . $d_utilisateur["uti_societe"];
		}
		$sql.=";";
		$req=$Conn->query($sql);
		$d_scoietes=$req->fetchAll();
		if(!empty($d_scoietes)){
			foreach($d_scoietes as $soc){

				$ConnFct=connexion_fct($soc["soc_id"]);

				$sql="SELECT MONTH(fac_date) AS mois,SUM(fli_montant_ht) AS montant_ht FROM Factures_Lignes
				LEFT JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
				LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
				WHERE fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31' AND com_ref_1=" . $utilisateur;
				if($d_utilisateur["uti_agence"]>0 AND $d_utilisateur["uti_societe"]==$soc["soc_id"]){
					// com sur 2 agence du meme soc le CA uti_agence est deja compté
					$sql.=" AND NOT com_agence=" . $d_utilisateur["uti_agence"];
				}
				$sql.=" GROUP BY MONTH(fac_date);";
				$req=$ConnFct->query($sql);
				$d_realises=$req->fetchAll();
				if(!empty($d_realises)){
					foreach($d_realises as $realise){
						$mois=0;
						if(!empty($realise["mois"])){
							$mois=$realise["mois"];
						}
						$montant_ht=0;
						if(!empty($realise["montant_ht"])){
							$montant_ht=$realise["montant_ht"];
						}
						$rem[$mois]["interco"]["realise"]=$rem[$mois]["interco"]["realise"] + $montant_ht;
						$rem[$mois]["interco"]["montant_prime"]=$rem[$mois]["interco"]["montant_prime"] + $montant_ht;
					}
				}

			}
		}
	}


	// calcul de la rem mensuelle

	/* REM TYPE
	1 Famille
	2 INETRCO
	3 MENSUELLE
	4 LANCEMENT
	5 CA HORS OBJECTIFS*/

	foreach($rem as $mois_id => $mois){

		if($exercice<2019){
			// avant 2019 "autre" etait compte comme une famille
			$nbCatProduit=$mois["nb_famille"]+1;
		}else{
			$nbCatProduit=$mois["nb_famille"];
		}
		$prtc_total=0;

		arsort($mois);
		arsort($rem[$mois_id]);



		foreach($mois as $cat_id => $cat_rem){

			if($cat_id!="nb_famille" AND $cat_id!="total_rem"){

				// on exclut les clé sur lesquelles il n'y a pas de calcul

				if($cat_rem["rem_type"]!=2 AND $cat_rem["rem_type"]!=3 AND $cat_rem["rem_type"]!=5){

					// pourcentage objectif sur total

					if(!empty($mois["total"]["objectif"])){
						$prct=($cat_rem["objectif"]*100)/$mois["total"]["objectif"];
					}else{
						$prct=0;
					}

					$prct=number_format($prct,2);
					$rem[$mois_id][$cat_id]["prct_objectif"]=$prct;
				}

				if($cat_rem["rem_type"]!=2 AND $cat_rem["rem_type"]!=0 AND $cat_rem["rem_type"]!=5){

					// taux d'atteinte
					$taux_reussite=0;
					if(!empty($cat_rem["objectif"])){
						$taux_reussite=(($cat_rem["realise"]-$cat_rem["contentieux"])*100)/$cat_rem["objectif"];

					}

					$taux_rem=$taux_reussite;
					if($taux_reussite<85){
						$taux_rem=0;
					}elseif($taux_reussite<95){
						$taux_rem=70;
					}elseif($taux_reussite<100){
						$taux_rem=85;
					}elseif($taux_reussite==100){
						$taux_rem=100;
					}else{

						if($exercice>=2019){
							$taux_rem=floor($taux_reussite);
						}else{
							$taux_rem=ceil($taux_reussite/10)*10;
						}
					}
					if($taux_rem>200){
						$taux_rem=200;
					}

					$rem[$mois_id][$cat_id]["taux_reussite"]=number_format($taux_reussite,2,"."," ");
					$rem[$mois_id][$cat_id]["taux_rem"]=$taux_rem;

				}
				// REM POTENTIELLE
				if($cat_rem["rem_type"]==1){
					// pime famille
					if(!empty($nbCatProduit)){
						$rem[$mois_id][$cat_id]["montant_prime"]=round(($d_rem["urp_prime_produit"]/$nbCatProduit),2);
					}
				}elseif($cat_rem["rem_type"]==3){
					// prime mensuelle
					$rem[$mois_id][$cat_id]["montant_prime"]=round($d_rem["urp_prime_mois"],2);
				}elseif($cat_rem["rem_type"]==4){
					// prime lancement
					$rem[$mois_id][$cat_id]["montant_prime"]=round($d_rem["urp_prime_autre"],2);
				}


				// montant de la rem produit
				if($cat_rem["rem_type"]!=5){
					$rem[$mois_id][$cat_id]["montant_rem"]=$rem[$mois_id][$cat_id]["montant_prime"]*($rem[$mois_id][$cat_id]["taux_rem"]/100);
				}else{
					// PRIME CA HORS OBJECTIF
					$rem[$mois_id][$cat_id]["montant_rem"]=$rem[$mois_id][$cat_id]["realise"]*($rem[$mois_id][$cat_id]["taux_rem"]/100);
				}

				// totalise sur le montant de la rem mensuelle
				$rem[$mois_id]["total_rem"]=$rem[$mois_id]["total_rem"]+$rem[$mois_id][$cat_id]["montant_rem"];

				// formatage du montant de rem apres addition
				$rem[$mois_id][$cat_id]["montant_rem"]=number_format($rem[$mois_id][$cat_id]["montant_rem"],2);
			}
		}
		// formatage du montant du total rem apres la fin des calculs
		$rem[$mois_id]["total_rem"]=number_format($rem[$mois_id]["total_rem"],2);
	}



	// CALCUL DES REM PERIODES ET ANNUELLE

	// calcul de la rem trimestrielle
	foreach($trimestre as $t_id => $trim){

		// pourcentage objectif sur total
		$prct=($trim["objectif"]*100)/$trimestre[0]["objectif"];
		$prct=number_format($prct,2);
		$trimestre[$t_id]["prct_objectif"]=$prct;

		// taux d'atteinte
		$taux_reussite=0;
		if(!empty($trimestre[0]["objectif"])){
			if(empty($trim["objectif"]) OR $trim["objectif"] == 0){
				$taux_reussite=0;
			}else{
				$taux_reussite=(($trim["realise"]-$trim["contentieux"])*100)/$trim["objectif"];
				$taux_reussite=number_format($taux_reussite,2);
			}


		}

		// taux de remuneration
		if($exercice<2019){
			if($taux_reussite<85){
				$taux_rem=0;
			}elseif($taux_reussite<95){
				$taux_rem=70;
			}elseif($taux_reussite<100){
				$taux_rem=85;
			}elseif($taux_reussite==100){
				$taux_rem=100;
			}else{
				$taux_rem=ceil($taux_reussite/10)*10;
			}
		}else{
			if($taux_reussite<100){
				$taux_rem=0;
			}elseif($taux_reussite>=120){
				$taux_rem=130;
			}else{
				$taux_rem=floor($taux_reussite);
			}
		}
		$trimestre[$t_id]["taux_reussite"]=number_format($taux_reussite,2,"."," ");
		$trimestre[$t_id]["taux_rem"]=$taux_rem;

		// montant de la prime
		if($t_id==0){
			// prime produit
			$trimestre[$t_id]["montant_prime"]=$d_rem["urp_prime_annee"];
		}else{
			// prime mensuelle
			$trimestre[$t_id]["montant_prime"]=$d_rem["urp_prime_trim"];
		}

		$trimestre[$t_id]["montant_rem"]=number_format($trimestre[$t_id]["montant_prime"]*($trimestre[$t_id]["taux_rem"]/100),2);

		// formatage après calcul (avant = erreur)
		$trimestre[$t_id]["montant_prime"]=number_format($trimestre[$t_id]["montant_prime"],2);
	}

	// BONNI PROPECT

	if(!empty($d_rem["urp_obj_prospect"]) OR $exercice==2019){

		$propects=array();
		$sql="SELECT DISTINCT cli_id,cli_filiale_de,cli_code FROM Clients INNER JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client)
		WHERE cli_first_facture_date>='" . $exercice . "-04-01' AND cli_first_facture_date<='" . intval($exercice+1) . "-03-31'
		AND cso_societe=" . $d_utilisateur["uti_societe"];
		if($d_rem["upr_rem_type"]==1){
			// com facture perso
			$sql.=" AND cso_utilisateur=" . $utilisateur;
		}elseif($d_utilisateur["uti_agence"]>0){
			//autre facturation agence / societe
			$sql.=" AND cso_agence=" . $d_utilisateur["uti_agence"];
		}
		$sql.=" ORDER BY cli_filiale_de,cli_code";
		$req=$Conn->query($sql);
		$d_propects=$req->fetchAll();
		if(!empty($d_propects)){
			foreach($d_propects as $p){
				if(!empty($p["cli_filiale_de"])){
					if(!isset($propects[$p["cli_filiale_de"]])){
						$propects[$p["cli_filiale_de"]]=1;
					}
				}else{
					if(!isset($propects[$p["cli_id"]])){
						$propects[$p["cli_id"]]=1;
					}
				}
			}
		}
		$nb_prospect=count($propects);

		// NOMBRE DE CLIENT FACTURE
		// le recherche de client facturé se fait sur la soc de l'utilisateur

		$ConnFct=connexion_fct($d_utilisateur["uti_societe"]);

		// CLIENTS FACTURE SUR N

		$sql="SELECT COUNT(DISTINCT fac_client),SUM(fli_montant_ht) FROM Factures
		INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
		INNER JOIN Clients ON (Factures.fac_client=clients.cli_id AND Factures.fac_agence=clients.cli_agence)
		INNER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
		WHERE fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'
		AND fli_categorie<4";
		if($d_rem["upr_rem_type"]==1){
			// com facture perso
			$sql.=" AND com_ref_1=" . $utilisateur;
		}elseif($d_utilisateur["uti_agence"]>0){
			//autre facturation agence / societe
			$sql.=" AND com_agence=" . $d_utilisateur["uti_agence"];
		}
		$req=$ConnFct->query($sql);
		$d_client_fac_n=$req->fetch();

		// CLIENTS FACTURE SUR N-1

		$sql="SELECT COUNT(DISTINCT fac_client),SUM(fli_montant_ht) FROM Factures
		INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
		INNER JOIN Clients ON (Factures.fac_client=clients.cli_id AND Factures.fac_agence=clients.cli_agence)
		INNER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
		WHERE fac_date>='" . intval($exercice-1) . "-04-01' AND fac_date<='" . $exercice . "-03-31'
		AND fli_categorie<4";
		if($d_rem["upr_rem_type"]==1){
			// com facture perso
			$sql.=" AND com_ref_1=" . $utilisateur;
		}elseif($d_utilisateur["uti_agence"]>0){
			//autre facturation agence / societe
			$sql.=" AND com_agence=" . $d_utilisateur["uti_agence"];
		}
		$req=$ConnFct->query($sql);
		$d_client_fac_n_1=$req->fetch();

	}

}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			.objectif{
				color:#AA00AA;
			}
			.resultat{
				font-weight:bold;
			}
			.resultat a{
				color:#000;
			}
			.ligne-strip{
				background-color:#FFF;
			}
		</style>
	</head>

	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php		if(!empty($erreur_txt)){ ?>
						<p class="alert alert-danger text-center" >
							<?=$erreur_txt?>
						</p>
		<?php		}elseif(!empty($warning_txt)){ ?>
						<p class="alert alert-warning text-center" >
							<?=$warning_txt?>
						</p>
		<?php		}else{ ?>
						<h1><?=$titre?></h1>

				<?php 	$periode=0;
						foreach($rem as $bcl => $rem_mois){  ?>

							<div class="panel" >
								<div class="panel-heading panel-head-sm"><?=$lib_mois[$bcl]?></div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table" >
											<thead>
												<tr>
													<th>
										<?php			if(!empty($d_rem["urp_prime_produit"])){
															echo("Famille");
														} ?>
													</th>
													<th class="text-center" >% d'objectif</th>
													<th class="text-center" >Objectif</th>
													<th class="text-center" >CA réalisé <i class="fa fa-info-circle" data-toggle="tooltip" title="Cliquer sur les valeurs pour afficher le détail des factures" ></i></th>
													<th class="text-center text-danger" >Contentieux <i class="fa fa-info-circle" data-toggle="tooltip" title="Cliquer sur les valeurs pour afficher le détail des factures" ></i></th>
													<th class="text-center" >CA retenu</th>
													<th class="text-center" >Taux d'atteinte</th>
													<th class="text-center" >Prime potentielle</th>
													<th class="text-center" >Taux de rémunération</th>
													<th class="text-center" >Rémunération</th>
												</tr>
											</thead>
											<tbody>
					<?php						foreach($rem_mois as $k => $val){

													if($k!="nb_famille" AND $k!="total_rem" AND $val["rem_type"]==1){ ?>
														<tr>
															<td><?=$val["libelle"]?></td>
															<td class="text-center" ><?=$val["prct_objectif"]?> %</td>
															<td class="text-center" ><?=$val["objectif"]?> €</td>
															<td class="text-center" >
																<a class="text-dark" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=1&categorie=<?=$val["categorie"]?>&famille=<?=$val["famille"]?>&s_famille=<?=$val["sous_famille"]?>&s_s_famille=<?=$val["sous_sous_famille"]?>">
																	<?=$val["realise"]?> €</td>
																</a>
															<td class="text-center" >
																<a class="text-danger" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=2&categorie=<?=$val["categorie"]?>&famille=<?=$val["famille"]?>&s_famille=<?=$val["sous_famille"]?>&s_s_famille=<?=$val["sous_sous_famille"]?>">
																	<?=$val["contentieux"]?> €
																<a>
															</td>
															<td class="text-center" ><?=$val["realise"]-$val["contentieux"]?> €</td>
															<td class="text-center" ><?=$val["taux_reussite"]?>%</td>
															<td class="text-center" ><?=$val["montant_prime"]?>€</td>
															<td class="text-center" ><?=$val["taux_rem"]?> %</td>
															<td class="text-center" ><?=$val["montant_rem"]?> €</td>
														</tr>
					<?php							}
												}
												// PRIME DE LANCEMENT

												if(!empty($rem_mois["autre_1"]["objectif"])){ ?>
													<tr>
														<td><?=$rem_mois["autre_1"]["libelle"]?></td>
														<td class="text-center" ><?=$rem_mois["autre_1"]["prct_objectif"]?> %</td>
														<td class="text-center" ><?=$rem_mois["autre_1"]["objectif"]?> €</td>
														<td class="text-center" >
															<a class="text-dark" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=1&categorie=<?=$rem_mois["autre_1"]["categorie"]?>&famille=<?=$rem_mois["autre_1"]["famille"]?>&s_famille=<?=$rem_mois["autre_1"]["sous_famille"]?>&s_s_famille=<?=$rem_mois["autre_1"]["sous_sous_famille"]?>">
																<?=$rem_mois["autre_1"]["realise"]?> €</td>
															</a>
														<td class="text-center" >
															<a class="text-danger" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=2&categorie=<?=$rem_mois["autre_1"]["categorie"]?>&famille=<?=$rem_mois["autre_1"]["famille"]?>&s_famille=<?=$rem_mois["autre_1"]["sous_famille"]?>&s_s_famille=<?=$rem_mois["autre_1"]["sous_sous_famille"]?>">
																<?=$rem_mois["autre_1"]["contentieux"]?> €
															<a>
														</td>
														<td class="text-center" ><?=$rem_mois["autre_1"]["realise"]-$rem_mois["autre_1"]["contentieux"]?> €</td>
														<td class="text-center" ><?=$rem_mois["autre_1"]["taux_reussite"]?>%</td>
														<td class="text-center" ><?=$rem_mois["autre_1"]["montant_prime"]?>€</td>
														<td class="text-center" ><?=$rem_mois["autre_1"]["taux_rem"]?> %</td>
														<td class="text-center" ><?=$rem_mois["autre_1"]["montant_rem"]?> €</td>
													</tr>
										<?php	}
												if(!empty($rem_mois["autre_3"]["objectif"])){ ?>
													<tr>
														<td><?=$rem_mois["autre_3"]["libelle"]?></td>
														<td class="text-center" ><?=$rem_mois["autre_3"]["prct_objectif"]?> %</td>
														<td class="text-center" ><?=$rem_mois["autre_3"]["objectif"]?> €</td>
														<td class="text-center" >
															<a class="text-dark" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=1&categorie=<?=$rem_mois["autre_3"]["categorie"]?>&famille=<?=$rem_mois["autre_3"]["famille"]?>&s_famille=<?=$rem_mois["autre_3"]["sous_famille"]?>&s_s_famille=<?=$rem_mois["autre_3"]["sous_sous_famille"]?>">
																<?=$rem_mois["autre_3"]["realise"]?> €</td>
															</a>
														<td class="text-center" >
															<a class="text-danger" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=2&categorie=<?=$rem_mois["autre_3"]["categorie"]?>&famille=<?=$rem_mois["autre_3"]["famille"]?>&s_famille=<?=$rem_mois["autre_3"]["sous_famille"]?>&s_s_famille=<?=$rem_mois["autre_3"]["sous_sous_famille"]?>">
																<?=$rem_mois["autre_3"]["contentieux"]?> €
															<a>
														</td>
														<td class="text-center" ><?=$rem_mois["autre_3"]["realise"]-$rem_mois["autre_3"]["contentieux"]?> €</td>
														<td class="text-center" ><?=$rem_mois["autre_3"]["taux_reussite"]?>%</td>
														<td class="text-center" ><?=$rem_mois["autre_3"]["montant_prime"]?>€</td>
														<td class="text-center" ><?=$rem_mois["autre_3"]["taux_rem"]?> %</td>
														<td class="text-center" ><?=$rem_mois["autre_3"]["montant_rem"]?> €</td>
													</tr>
										<?php	}
												if(!empty($rem_mois["autre"]["objectif"]) AND $rem_mois["autre"]["rem_type"]!=1){
													// si rem type=1 alors autre s'affiche dans la partie prime produit
													// AFFICHE DANS CE CONTEXTE POUR COHERENCE DES TOTAUX ?>
													<tr>
														<td><?=$rem_mois["autre"]["libelle"]?></td>
														<td class="text-center" ><?=$rem_mois["autre"]["prct_objectif"]?> %</td>
														<td class="text-center" ><?=$rem_mois["autre"]["objectif"]?> €</td>
														<td class="text-center" >
															<a class="text-dark" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=1&categorie=<?=$rem_mois["autre"]["categorie"]?>&famille=<?=$rem_mois["autre"]["famille"]?>&s_famille=<?=$rem_mois["autre"]["sous_famille"]?>&s_s_famille=<?=$rem_mois["autre"]["sous_sous_famille"]?>">
																<?=$rem_mois["autre"]["realise"]?> €</td>
															</a>
														<td class="text-center" >
															<a class="text-danger" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=2&categorie=<?=$rem_mois["autre"]["categorie"]?>&famille=<?=$rem_mois["autre"]["famille"]?>&s_famille=<?=$rem_mois["autre"]["sous_famille"]?>&s_s_famille=<?=$rem_mois["autre"]["sous_sous_famille"]?>">
																<?=$rem_mois["autre"]["contentieux"]?> €
															<a>
														</td>
														<td class="text-center" ><?=$rem_mois["autre"]["realise"]-$rem_mois["autre"]["contentieux"]?> €</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
													</tr>
										<?php	} ?>
												<tr>
													<td><?=$rem_mois["total"]["libelle"]?></td>
													<td class="text-center" >&nbsp;</td>
													<td class="text-center" ><?=$rem_mois["total"]["objectif"]?> €</td>
													<td class="text-center" >
														<a class="text-dark" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=1&categorie=total">
															<?=$rem_mois["total"]["realise"]?> €
														</a>
													</td>
													<td class="text-center text-danger" >
														<a class="text-danger" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=2&categorie=total">
															<?=$rem_mois["total"]["contentieux"]?> €
														</a>
													</td>
													<td class="text-center" ><?=$rem_mois["total"]["realise"]-$rem_mois["total"]["contentieux"]?> €</td>
													<td class="text-center" ><?=$rem_mois["total"]["taux_reussite"]?> %</td>
													<td class="text-center" ><?=$rem_mois["total"]["montant_prime"]?> €</td>
													<td class="text-center" ><?=$rem_mois["total"]["taux_rem"]?> %</td>
													<td class="text-center" ><?=$rem_mois["total"]["montant_rem"]?> €</td>
												</tr>
									<?php		if(!empty($rem_mois["ca_no_obj"]["realise"])){ ?>
													<tr>
														<td><?=$rem_mois["ca_no_obj"]["libelle"]?></td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >
															<a class="text-dark" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=1&categorie=ca_no_obj">
																<?=$rem_mois["ca_no_obj"]["realise"]?> €
															</a>
														</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" ><?=$rem_mois["ca_no_obj"]["taux_rem"]?> %</td>
														<td class="text-center" ><?=$rem_mois["ca_no_obj"]["montant_rem"]?> €</td>
													</tr>
										<?php	}
												if(!empty($rem_mois["interco"]["realise"])){ ?>
													<tr>
														<td><?=$rem_mois["interco"]["libelle"]?></td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >
															<a class="text-dark" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&mois=<?=$bcl?>&ca=1&categorie=interco">
																<?=$rem_mois["interco"]["realise"]?> €
															</a>
														</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" >&nbsp;</td>
														<td class="text-center" ><?=$rem_mois["interco"]["taux_rem"]?> %</td>
														<td class="text-center" ><?=$rem_mois["interco"]["montant_rem"]?> €</td>
													</tr>
										<?php	} ?>
												<tr>
													<th colspan="9" class="text-right" >Rémunération totale : </th>
													<td class="text-center" ><?=$rem_mois["total_rem"]?> €</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>

			<?php			// PRIME TRIMETRIELLE

							$bcl_suiv=$bcl+1;
							if($bcl_suiv==13){
								$bcl_suiv=1;
							}
							if(($bcl!=3 AND $d_periodes[$bcl]!=$d_periodes[$bcl_suiv]) OR $bcl==3){
								$periode=$d_periodes[$bcl]; ?>

								<div class="panel" >
									<div class="panel-heading panel-head-sm"><?=$trimestre[$periode]["libelle"]?></div>
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table" >
												<thead>
													<tr>
														<th class="text-center" >% d'objectif</th>
														<th class="text-center" >Objectif</th>
														<th class="text-center" >CA réalisé <i class="fa fa-info-circle" data-toggle="tooltip" title="Cliquer sur les valeurs pour afficher le détail des factures" ></i></th>
														<th class="text-center text-danger" >Contentieux <i class="fa fa-info-circle" data-toggle="tooltip" title="Cliquer sur les valeurs pour afficher le détail des factures" ></i></th>
														<th class="text-center" >CA retenu</th>
														<th class="text-center" >Taux d'atteinte</th>
														<th class="text-center" >Prime potentielle</th>
														<th class="text-center" >Taux de rémunération</th>
														<th class="text-center" >Rémunération</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="text-center" ><?=$trimestre[$periode]["prct_objectif"]?> %</td>
														<td class="text-center" ><?=$trimestre[$periode]["objectif"]?> €</td>
														<td class="text-center" >
															<a class="text-dark" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&trimestre=<?=$periode?>&ca=1&categorie=total">
																<?=$trimestre[$periode]["realise"]?> €
															</a>
														</td>
														<td class="text-center text-danger" >
															<a class="text-danger" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&trimestre=<?=$periode?>&ca=2&categorie=total">
																<?=$trimestre[$periode]["contentieux"]?> €
															</a>
														</td>
														<td class="text-center" ><?=$trimestre[$periode]["realise"]-$trimestre[$periode]["contentieux"]?> €</td>
														<td class="text-center" ><?=$trimestre[$periode]["taux_reussite"]?>%</td>
														<td class="text-center" ><?=$trimestre[$periode]["montant_prime"]?>€</td>
														<td class="text-center" ><?=$trimestre[$periode]["taux_rem"]?> %</td>
														<td class="text-center" ><?=$trimestre[$periode]["montant_rem"]?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
					<?php	}

						} ?>

						<div class="panel" >
							<div class="panel-heading panel-head-sm">Prime Annuelle</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table" >
										<thead>
											<tr>
												<th class="text-center" >% d'objectif</th>
												<th class="text-center" >Objectif</th>
												<th class="text-center" >CA réalisé <i class="fa fa-info-circle" data-toggle="tooltip" title="Cliquer sur les valeurs pour afficher le détail des factures" ></i></th>
												<th class="text-center text-danger" >Contentieux <i class="fa fa-info-circle" data-toggle="tooltip" title="Cliquer sur les valeurs pour afficher le détail des factures" ></i></th>
												<th class="text-center" >CA retenu</th>
												<th class="text-center" >Taux d'atteinte</th>
												<th class="text-center" >Prime potentielle</th>
												<th class="text-center" >Taux de rémunération</th>
												<th class="text-center" >Rémunération</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-center" ><?=$trimestre[0]["prct_objectif"]?> %</td>
												<td class="text-center" ><?=$trimestre[0]["objectif"]?> €</td>
												<td class="text-center" >
													<a class="text-dark" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&ca=1&categorie=total">
														<?=$trimestre[0]["realise"]?> €
													</a>
												</td>
												<td class="text-center text-danger" >
													<a class="text-danger" href="utilisateur_rem_fac.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&ca=2&categorie=total">
														<?=$trimestre[0]["contentieux"]?> €
													</a>
												</td>
												<td class="text-center" ><?=$trimestre[0]["realise"]-$trimestre[0]["contentieux"]?> €</td>
												<td class="text-center" ><?=$trimestre[0]["taux_reussite"]?>%</td>
												<td class="text-center" ><?=$trimestre[0]["montant_prime"]?>€</td>
												<td class="text-center" ><?=$trimestre[0]["taux_rem"]?> %</td>
												<td class="text-center" ><?=$trimestre[0]["montant_rem"]?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
			<?php		if( ( ($exercice<2019 AND !empty($d_rem["urp_obj_prospect"])) OR $exercice==2019 ) AND ($_SESSION['acces']["acc_profil"]==10 OR $_SESSION['acces']["acc_profil"]==13 OR $acc_utilisateur==372)){ ?>
							<div class="panel" >
								<div class="panel-heading panel-head-sm">Prime prospection</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table" >
											<thead>
												<tr>
													<th class="text-center" colspan="2" >Clients régionaux<br/><?=$exercice-1 . "/" . $exercice?></th>
													<th class="text-center" colspan="2" >Clients régionaux<br/><?=$exercice . "/" . intval($exercice+1)?></th>
													<th class="text-center" >Objectif</th>
													<th class="text-center" >Résultat</th>
													<th class="text-center" >Prime</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="text-center" >
														<a href="utilisateur_rem_prospect_cli.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&n=1">
															<?=$d_client_fac_n_1[0]?>
														</a>
													</td>
													<td class="text-center" >
														<a href="utilisateur_rem_prospect_cli.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>&n=1">
															<?=number_format($d_client_fac_n_1[1],2,","," ")?> €
														</a>
													</td>
													<td class="text-center" >
														<a href="utilisateur_rem_prospect_cli.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>">
															<?=$d_client_fac_n[0]?>
														</a>
													</td>
													<td class="text-center" >
														<a href="utilisateur_rem_prospect_cli.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>">
															<?=number_format($d_client_fac_n[1],2,","," ")?> €
														</a>
													</td>
													<td class="text-center" ><?=$d_rem["urp_obj_prospect"]?></td>
													<td class="text-center" >
														<a href="utilisateur_rem_prospect.php?utilisateur=<?=$utilisateur?>&exercice=<?=$exercice?>">
															<?=$nb_prospect?>
														</a>
													</td>
													<td class="text-center" >
											<?php		if($nb_prospect>=$d_rem["urp_obj_prospect"] AND $d_client_fac_n[0]>=$d_client_fac_n_1[0] AND  $d_client_fac_n[1]>=$d_client_fac_n_1[1]){
															echo($d_rem["urp_prime_prospect"] . " €");
														}else{
															echo("0 €");
														} ?>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
			<?php		}
					} ?>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
			<?php	if(!empty($_SESSION['retour'])){ ?>
						<a href="<?=$_SESSION['retour']?>" class="btn btn-default btn-sm" role="button" >
							<span class="fa fa-left"></span>
							Retour
						</a>
			<?php 	} ?>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
			<?php	if(empty($erreur_txt)){
						if($exercice>2018){ ?>
							<a href="utilisateur_rem.php?exercice=<?=$exercice-1?>&utilisateur=<?=$utilisateur?>" class="btn btn-sm btn-default" >
								<i class="fa fa-arrow-left" ></i> Exercice <?=$exercice-1?> / <?=$exercice?>
							</a>
				<?php	}
						if($exercice<$exercice_courant){ ?>
							<a href="utilisateur_rem.php?exercice=<?=$exercice+1?>&utilisateur=<?=$utilisateur?>" class="btn btn-sm btn-default" >
								Exercice <?=$exercice+1?> / <?=$exercice+2?> <i class="fa fa-arrow-right" ></i>
							</a>
				<?php	}
					} ?>
				</div>
				<div class="col-xs-3 footer-right" >
		<?php		if ($utilisateur>0 AND $_SESSION["acces"]["acc_droits"][39]) { ?>
						<a href="utilisateur_rem_param.php?exercice=<?=$exercice?>&utilisateur=<?=$utilisateur?>" class="btn btn-warning btn-sm" role="button" >
							<span class="fa fa-pencil"></span>
							Mise à jour des paramètres
						</a>
			<?php	} ?>
				</div>
			</div>
		</footer>
<?php
		include "includes/footer_script.inc.php"; ?>
	</body>
</html>
