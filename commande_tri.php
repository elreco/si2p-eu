<?php 
/////////////////// MENU ACTIF /////////////////////// 
$menu_actif = "5-2";
/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
// session retour
$_SESSION['retour'] = "commande_tri.php"; 
// fin session retour

/////////////// TRAITEMENTS BDD ///////////////////////

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}

	
	// toutes les familels de fournisseurs pour le select
	$req = $Conn->prepare("SELECT * FROM fournisseurs_familles ORDER BY ffa_id");
	$req->execute();
	$fournisseurs_familles = $req->fetchAll();

	// rechercher la societe sur laquelle on est co
	$req = $Conn->prepare("SELECT soc_nom FROM societes WHERE soc_id =" . $acc_societe);
	$req->execute();
	$societe = $req->fetch();
	
	// LES AGENCES
	if(empty($acc_agence)){
		// rechercher l'agence sur laquelle on est co
		$req = $Conn->prepare("SELECT age_id,age_nom FROM agences WHERE age_societe = " . $acc_societe);
		$req->execute();
		$d_agences = $req->fetchAll();
		if(empty($d_agences)){
			unset($d_agences);
		}
	}
		


?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Commandes</title> 
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body class="sb-top sb-top-sm ">
	<form method="post" action="commande_liste.php" >
		<div id="main">
<?php 		include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn">
					<div class="row"> 
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary ">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="content-header">
											<h2>Recherche de <b class="text-primary">commandes</b></h2>            
										</div>
										<div class="col-md-10 col-md-offset-1">
								<?php		if(isset($d_agences)){ ?>
												<div class="row">												
													<div class="col-md-6">
														<label for="com_agence" >Agence :</label>
														<select name="com_agence" id="com_agence" class="select2" >
															<option value="0" >Agence ...</option>
								<?php						foreach($d_agences as $d_agence){
																echo("<option value='" . $d_agence["age_id"] . "' >" . $d_agence["age_nom"] . "</option>");
															} ?>
														</select>
													</div>
												</div>
								<?php		} ?>								
											<div class="row pt15">
												<div class="col-md-6">														
													<label for="com_fournisseur" >Fournisseur :</label>
													<select id="com_fournisseur" name="com_fournisseur" class="select2 select2-fournisseur" data-agence="<?=$acc_agence?>" >
														<option value="0" selected="selected">Fournisseur...</option>
													</select>																									
												</div>												
												<div class="col-md-6">
													<label for="com_numero" >Numéro de commande :</label>
													<div class="input-group input-primary">
														<span class="input-group-addon" style="color:#E31936;">
															DAXX- / BCXX-
														</span>
														<input class="gui-input numero_bc" id="com_numero" name="com_numero" type="text" placeholder="Numéro de commande" />
													</div>
												</div>											
											</div>
											<div class="row pt15">
												<div class="col-md-6">
													<label for="com_donneur_ordre" >Donneur d'ordres :</label>
													<select id="com_donneur_ordre" name="com_donneur_ordre" class="select2 select2-utilisateur">
														<option value="0" selected="selected">Donneur d'ordres...</option>
													</select>
												</div>
												<div class="col-md-6">
													<label for="com_donneur_ordre" >Type d'achat :</label>
													<select id="com_famille" name="com_famille" class="select2">
														<option value="0" selected="selected">Famille...</option>
												<?php 	foreach($fournisseurs_familles as $ffa){ ?>
															<option value="<?= $ffa['ffa_id'] ?>"><?= $ffa['ffa_libelle'] ?></option>
												<?php 	}?>
													</select>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span>Période de la commande</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<label for="com_date_deb" >Depuis le :</label>
													<div class="field prepend-icon">
														<input type="text" name="com_date_deb" id="com_date_deb" class="gui-input datepicker" placeholder="Depuis le" value="<?= date("01/m/Y"); ?>">
														<label for="com_date" class="field-icon">
															<i class="fa fa-calendar" aria-hidden="true"></i>
														</label>
													</div>
												</div>
												<div class="col-md-6">
													<label for="com_date_fin" >Jusqu'au :</label>
													<div class="field prepend-icon">
														<input type="text" name="com_date_fin" id="com_date_fin" class="gui-input datepicker" placeholder="Jusqu'au">
														<label for="com_date" class="field-icon">
															<i class="fa fa-calendar" aria-hidden="true"></i>
														</label>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span>Statut de la commande</span>
													</div>
												</div>
											</div>
											
											<div class="row">		
												<div class="col-md-2">
													<div class="option-group field">
														<label class="option option-system">
															<input type="checkbox" class="com-etat com-etat-0" data-statut="0" name="com_etat_0" value="on" >
															<span class="checkbox"></span>En cours de saisie
														</label>
													</div>
												</div>
												<div class="col-md-2">
													<div class="option-group field">
														<label class="option option-warning">
															<input type="checkbox" class="com-etat com-etat-1" data-statut="1" name="com_etat_1" value="on" >
															<span class="checkbox"></span>En cours de validation
														</label>
													</div>
												</div>
												<div class="col-md-2">
													<div class="option-group field">
														<label class="option option-success">
															<input type="checkbox" class="com-etat com-etat-2" data-statut="2" name="com_etat_2" value="on" >
															<span class="checkbox"></span>Validé
														</label>
													</div>
												</div>
												<!-- FG BAP non gérés
												<div class="col-md-2">
													<div class="option-group field">
														<label class="option option-dark">
															<input type="checkbox" class="com-etat com-etat-3" data-statut="3" name="com_etat_3" value="on" >
															<span class="checkbox"></span>Bon à payer
														</label>
													</div>
												</div>-->
												<div class="col-md-2">
													<div class="option-group field">
														<label class="option option-danger">
															<input type="checkbox" class="com-etat com-etat-4" data-statut="4" name="com_etat_4" value="on" >
															<span class="checkbox"></span>Annulé
														</label>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span>Opérations à réaliser</span>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-4">		
													<div class="option-group field">
														<label class="option option-dark">
															<input type="checkbox" class="com-ope com-ope-1" data-ope="1" name="com_ope_1" value="on" >
															<span class="checkbox"></span>Demandes d'achats à valider
														</label>
													</div>
												</div>
												<!--
												<div class="col-md-4">		
													<div class="option-group field">
														<label class="option option-dark">
															<input type="checkbox" class="com-ope com-ope-2" data-ope="2" name="com_ope_2" value="on" >
															<span class="checkbox"></span>Validation en BAP
														</label>
													</div>
												</div>-->
											</div>
										</div>
									<!-- FIN COL-MD-10 -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
				</div>
				<div class="col-xs-6 footer-middle">

				</div>
				<div class="col-xs-3 footer-right">
					<a href="commande.php" class="btn btn-success btn-sm">
						<i class='fa fa-plus'></i> Nouvelle demande d'achat
					</a>
					<button type="submit" name="search" class="btn btn-primary btn-sm">
						<i class='fa fa-search'></i> Rechercher
					</button>
				</div>
			</div>
		</footer>
	</form>
	
<?php 
	include "includes/footer_script.inc.php"; ?>  
	<script src="vendor/plugins/select2/js/select2.min.js"></script>

	<script type="text/javascript">  
		var statut=0;
		var ope=0;
		jQuery(document).ready(function (){
			$("#com_agence").change(function (){
				$("#com_fournisseur").data("agence",$(this).val());
			});
			  
			$(".com-etat").click(function(){
				if($(this).is(":checked")){
					statut=$(this).data("statut");
					$(".com-etat").each(function(){
						if(!$(this).hasClass("com-etat-" + statut)){
							$(this).prop("checked",false);
						}
					});
				};
			});
			$(".com-ope").click(function(){
				if($(this).is(":checked")){
					ope=$(this).data("ope");
					$(".com-ope").each(function(){
						if(!$(this).hasClass("com-ope-" + ope)){
							$(this).prop("checked",false);
						}
					});
				};
			});
			
		});
	</script>
</body>
</html>