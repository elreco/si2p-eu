<?php
$commerce_li = true;

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_get_pro_familles.php');
include('modeles/mod_sous_famille.php');


// AJOUT D'INFO SUR UN CLIENT
// ATENTION système marche si soc_client = acc_societe

	$client=0;
	if(!empty($_GET["client"])){
		$client=intval($_GET["client"]);
	}
	
	$info=0;
	if(!empty($_GET["info"])){
		$info=intval($_GET["info"]);
	}
	
	if($client>0){
		
		$sql="SELECT cli_code,cli_nom, cli_categorie FROM Clients WHERE cli_id=" . $client . ";";
		$req=$Conn->query($sql);
		$d_client=$req->fetch();
		
		$sql="SELECT cin_id,cin_libelle FROM Clients_Infos ORDER BY cin_libelle;";
		$req=$Conn->query($sql);
		$d_info_type=$req->fetchAll();

		if($info>0){
			
			$req=$Conn->query("SELECT * FROM Infos WHERE inf_id=" . $info . ";");
			$_info=$req->fetch();

		}else{
			
			$_info["inf_type"]=0;
			$_info["inf_famille"]=0;
			$_info["inf_sous_famille"]=0;
			$_info["inf_description"]="";
		}
		
		
		
	}else{
		$erreur=1;
	}


?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion - <?= $client['cli_nom'] ?></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
		<link href="vendor/plugins/summernote/summernote-bs3.css" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form id="filtre" action="client_info_mod_enr.php" method="post" class="admin-form form-inline form-inline-grid">
			<div>
				<input type="hidden" name="client" value="<?=$client?>" />
				<input type="hidden" name="info" value="<?=$info?>" />
			</div>
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" class="">
					<section id="content" class="">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
							
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											
											<div class="content-header">
										<?php	if($info>0){ ?>
													<h2><b class="text-primary"><?=$d_client['cli_code']?></b> - Edition d'une <b class="text-primary">info</b></h2>
										<?php	}else{ ?>
													<h2><b class="text-primary"><?=$d_client['cli_code']?></b> - Nouvelle <b class="text-primary">info</b></h2>
										<?php	} ?>
											</div>
											<div class="col-md-10 col-md-offset-1">		

												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<label for="inf_type">Type d'info</label>
															<select name="inf_type" id="inf_type" class="form-control" required >
													<?php		if(!empty($d_info_type)){
																	foreach($d_info_type as $dit){
																		if($dit['cin_id'] == 6){
																			if($d_client['cli_categorie'] == 2){
																				if($dit["cin_id"]==$_info["inf_type"]){
																					echo("<option value='" . $dit["cin_id"] . "' selected >" . $dit["cin_libelle"] . "</option>");
																				}else{
																					echo("<option value='" . $dit["cin_id"] . "' >" . $dit["cin_libelle"] . "</option>");
																				}
																			}
																		}else{
																			if($dit["cin_id"]==$_info["inf_type"]){
																				echo("<option value='" . $dit["cin_id"] . "' selected >" . $dit["cin_libelle"] . "</option>");
																			}else{
																				echo("<option value='" . $dit["cin_id"] . "' >" . $dit["cin_libelle"] . "</option>");
																			}
																		}
																	}
																} ?>
															</select>
														</div>
													</div> 
												</div>
												<div class="row">
													<div class="col-md-12">
														<p id="info_affichage" >fzfzef</p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section">
															<select name="inf_famille" class="select2 select2-famille" >
																<option value="0">Famille de produit...</option>
														 <?php 	$d_famille=get_pro_familles();
																if(!empty($d_famille)){
																	foreach($d_famille as $df){
																		if($df["pfa_id"]==$_info["inf_famille"]){
																			echo("<option value='" . $df["pfa_id"] . "' selected >" . $df["pfa_libelle"] . "</option>");
																		}else{
																			echo("<option value='" . $df["pfa_id"] . "' >" . $df["pfa_libelle"] . "</option>");
																		}
																	}
																} ?>
															</select>
														</div>
													</div>
													<div class="col-md-12">
														<div class="section">
															<select name="inf_sous_famille" class="select2 select2-famille-sous-famille" >
																<option value="0">Sous-famille de produit ...</option>
														<?php 	if (!empty($_info['inf_famille'])){
																	echo get_sous_famille_select($_info['inf_famille'], $_info['inf_sous_famille']);
																}?>
															</select>
														</div>
													</div>
													<div class="col-md-12">
														<div class="section">
															<select name="inf_sous_sous_famille" class="select2 select2-famille-sous-sous-famille" >
																<option value="0">Sous-Sous-famille de produit ...</option>
														<?php 	if (!empty($_info['inf_sous_famille'])){
																	  echo get_sous_sous_famille_select($_info['inf_famille'], $_info['inf_sous_famille']);
																}?>
															</select>
														</div>
													</div>
												</div>											
												<div class="row">
													<div class="col-md-12">
														<div class="section">                       
															<textarea class="gui-textarea summernote" id="comment" name="inf_description" placeholder="Description"><?=$_info['inf_description']?></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="client_voir.php?client=<?=$client?>&tab6" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right">
						<button type="submit" name="submit" class="btn btn-success btn-sm">
							<i class='fa fa-floppy-o'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>       
		<script src="vendor/plugins/summernote/summernote.min.js"></script>
		<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
		<script src="/vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
		
			var affichage_info=new Array(5);
			affichage_info[0]="";
			affichage_info[1]="L'info concerne le client et ces filiales. <br/> Elle est affiché sur les fiches clients.";
			affichage_info[2]="L'info concerne uniquement le client. <br/> Elle est affichée sur la fiche et sur les actions.";
			affichage_info[3]="L'info concerne le client et ces filiales. <br/> Elle est affichée sur les fiches clients et sur les actions.";
			affichage_info[4]="L'info concerne le client et ces filiales. <br/> Elle est affichée sur les fiches clients et sur les actions.";
			affichage_info[5]="L'info concerne le client et ces filiales. <br/> Elle est affichée sur les fiches clients, les actions et à l'édition des factures.";
			
			jQuery(document).ready(function (){
				$("#inf_type").change(function (){		
					console.log("inf_type change");
					$("#info_affichage").html(affichage_info[$(this).val()]);
				});
				$("#inf_type").trigger("change");
			
			});
		</script>
	</body>
</html>
