<?php

include "includes/controle_acces.inc.php";


include_once 'includes/connexion.php';
include 'modeles/mod_parametre.php';

// VERIFICATION SI C'EST UNE EDITION
$client = intval($_GET['client']);
if(empty($client)){
    echo("Probleme");
    die();
}

$req=$Conn->query("SELECT * FROM Clients WHERE cli_id = " . $client);
$d_client=$req->fetch();

// UTILISATEUR SI EDITION
if(!empty($client) && !empty($_GET['acc_ref_id'])){
	$req=$Conn->prepare("SELECT * FROM Acces WHERE acc_ref = 2 AND acc_ref_id = :acc_ref_id");
	$req->bindValue(":acc_ref_id",$_GET["acc_ref_id"]);
	$req->execute();
	$d_acces=$req->fetch();
	
}

$sql="SELECT * FROM Contacts WHERE con_ref_id=" . $client . ";";
$req=$Conn->query($sql);
$contacts=$req->fetchAll();
?>
<!DOCTYPE html>
<html lang="fr" >
<head>
    <meta charset="utf-8">
    <title>SI2P - Orion - Accès Client</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
	
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>

<body class="sb-top sb-top-sm ">
	
	<form action="client_acces_enr.php" id="admin-form" method="post" enctype="multipart/form-data" >
		<div id="main">
<?php		include "includes/header_def.inc.php";?>
			
			<section id="content_wrapper" class="">
				
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-12">
							<div class="admin-form theme-primary admin-form-label">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="content-header">
										<input type="hidden" name="client" value="<?= $_GET['client']?>">
											<?php if(!empty($_GET['acc_ref_id'])){ ?>
												<input type="hidden" name="acc_ref_id" value="<?= $d_acces['acc_ref_id']?>">
												<h2>Modifier l'accès client <b class="text-primary"><?= $d_client['cli_nom'] ?></b></h2>
											<?php }else{ ?>
												<h2>Nouvel <b class="text-primary">accès</b></h2>
											<?php } ?>
										</div>
										<div class="col-md-12">		


											
											<!-- BAS DU FORM -->
											
											<!-- Connexion -->
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40" >
														<span>Données de connexion</span>
													</div>
												</div>
                                            </div>
											<div class="row">
											<?php if(!empty($_GET['acc_ref_id'])){ ?>
													<input type="hidden" name="contact" value="<?= $d_acces['acc_ref_id'] ?>">
													<?php }?>
												<div class="col-md-12">
													<label for="adr_type" >Sélectionnez un contact : </label>
													<select id="contact" name="contact" class="form-control"
														<?php if(!empty($_GET['acc_ref_id'])){ ?>
															disabled
														<?php }?>
													>
													
												<?php 	
												foreach($contacts as $c){ ?>		
															<option value="<?= $c['con_id'] ?>" data-email="<?= $c['con_mail'] ?>"
															<?php if(!empty($_GET['acc_ref_id']) && $c['con_id'] == $d_acces['acc_ref_id']){ ?>
																selected
															<?php  
															}?>
															><?= $c['con_prenom'] ?> <?= $c['con_nom'] ?></option>
												<?php	} ?>
													</select>											
												</div>
											</div>
											
                                            <div class="row  mt10">
                                                <div class="col-md-6" >
													<div class="section">
														<label for="acc_uti_ident" >Identifiant :</label>
														<input type="email" name="acc_uti_ident" id="acc_uti_ident" class="gui-input" placeholder="Identifiant" 
														<?php if(!empty($d_acces['acc_uti_ident'])){ ?>
														value="<?= $d_acces['acc_uti_ident'] ?>"
														<?php } ?>>																																		
													</div>
												</div>
												<div class="col-md-5" >
													<div class="section">	
														<label for="acc_uti_passe" >Mot de passe :</label>
														<input type="text" name="acc_uti_passe" id="acc_uti_passe" class="gui-input" placeholder="Mot de passe" 
												<?php 	if(!empty($d_acces['acc_uti_passe'])){ ?>
															value="<?= $d_acces['acc_uti_passe'] ?>"
												<?php 	} ?> />	
														<div id="er_acc_uti_passe" ></div>
													</div>
												</div>
												<div class="col-md-1 pt20" >	
													<button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Générer un mot de passe" id="genere_passe" >
														<i class="fa fa-refresh" ></i>
													</button>
												</div>
                                            </div>
											<div class="row">
												<div class="col-md-4 pt20" >												
													
													<label class="option">
														<input type="checkbox" name="archive" value="1" 

														<?php if(!empty($d_acces['acc_archive'])){ ?>
															checked
														<?php } ?>/>
														<span class="checkbox"></span>Accès archivés
													</label>
																		  
												</div>	
											</div>
                                            
																												  
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</section>
				
			</section>
		</div>
	
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="client_voir.php?client=<?= $_GET['client'] ?>&tab14" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				
				</div>
				<div class="col-xs-6 footer-middle"></div>
				<div class="col-xs-3 footer-right">
					<button type="submit" class="btn btn-success btn-sm" >
						<i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php
	include "includes/footer_script.inc.php"; ?>	
	
	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script src="assets/js/custom.js"></script>
	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script src="vendor/plugins/select2/js/i18n/fr.js"></script>
	
	
	
	<script type="text/javascript">
		jQuery(document).ready(function () {
			<?php if(empty($d_acces)){ ?>
			
				$("#acc_uti_ident").val($("#contact").find(":selected").data('email'));
				$("#contact").change(function(){
					$("#acc_uti_ident").val($("#contact").find(":selected").data('email'));
				});	
			<?php } ?>

			$("#genere_passe").click(function () {
				password=generePasse();
				$("#acc_uti_passe").val(password);
			});
			$("#acc_uti_passe").blur(function(){
				console.log("TEST PASSE");
				if($(this).val()!=""){
					var passe_valide=testPasse($(this).val());
					if(!passe_valide){
						afficher_txt_user($("#er_acc_uti_passe"),"Mot de passe incorrect","text-danger",10000);
						$(this).val("");
					}
				}
			});
		});
		
	</script>
</body>
</html>
