<?php

	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_parametre.php');

	// toutes les familels de fournisseurs pour le select

	if(!empty($_POST)){
		$mil="";
		if(!empty($_POST['eal_agence'])){
			$mil.=" AND eal_agence = " . $_POST['eal_agence'];
		}
		if(!empty($_POST['eal_societe'])){
			$mil.=" AND eal_societe = " . $_POST['eal_societe'];
		}
		// CONSTRUCTION REQUETE
		$sql ="SELECT * FROM evacuations_alertes";
		if($mil!=""){
	      $sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	    }
	    $req=$Conn->query($sql);
		$evacuations_alertes=$req->fetchAll();

	}else{
		$req = $Conn->prepare("SELECT * FROM evacuations_alertes");
		$req->execute();
		$evacuations_alertes = $req->fetchAll();
	}
	$sql="SELECT soc_id,soc_nom FROM Societes WHERE NOT soc_archive ORDER BY soc_nom;";
	$req = $Conn->query($sql);
	$d_societes=$req->fetchAll();
	if(!empty($_POST['eal_agence'])){
		$sql="SELECT * FROM Agences WHERE age_societe = " . $_POST['eal_societe'] . " AND NOT age_archive";
		$req = $Conn->query($sql);
		$d_agences=$req->fetchAll();
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css">
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

	</head>

	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">

			<?php
				include "includes/header_def.inc.php";
			?>

			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">
				<!-- Begin: Content -->

				<section id="content" class="animated fadeIn">

					<div class="row" >

						<form action="evac_gestion_alerte.php" method="post" id="admin-form">


								<div class="col-md-4 mb10" >
									<div class="select2-sm" >
										<select name="eal_societe" id="societe" class="select2 select2-client-societe select2-societe" data-com_archive="0" required >
											<option value="0" >Sélectionnez une société</option>
									<?php	if(!empty($d_societes)){
												foreach($d_societes as $soc){
													if(!empty($_POST['eal_societe']) && $_POST['eal_societe'] == $soc['soc_id']){
														echo("<option selected value='" . $soc["soc_id"] . "' >" . $soc["soc_nom"] . "</option>");
													}else{
														echo("<option value='" . $soc["soc_id"] . "' >" . $soc["soc_nom"] . "</option>");
													}

												}
											} ?>
										</select>
									</div>
								</div>
								<div class="col-md-4" >
									<div class="select2-sm" >
										<select name="eal_agence" id="agence" class="select2 select2-societe-agence" data-com_archive="0" >
											<option value="0" >Sélectionnez une agence</option>
											<?php
											if(!empty($d_agences)){
												foreach($d_agences as $a){
													if(!empty($_POST['eal_agence']) && $_POST['eal_agence'] == $a['age_id']){
													?>
													<option selected value="<?= $a['age_id'] ?>" ><?= $a['age_nom'] ?></option>
												<?php }else{ ?>
													<option value="<?= $a['age_id'] ?>" ><?= $a['age_nom'] ?></option>
												<?php }
											}
										}?>
										</select>
									</div>
								</div>

							<div class="col-md-2">
								<button type="submit" class="btn btn-primary btn-sm" >
									<i class="fa fa-search"></i>
								</button>
							</div>
						</form>
						<div class="col-md-12" >
							<div class="table-responsive" >
								<table class="table table-hover table-striped" >
									<thead>
										<tr class="dark" >
											<th>Utilisateur</th>
											<th>Société</th>
											<th>Agence</th>
											<th>Mail</th>
											<th>Alerte</th>
											<th>Validation</th>
											<th>Modifier</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($evacuations_alertes as $g){
											$req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id = " . $g['eal_ref_id']);
											$req->execute();
											$utilisateur = $req->fetch();


											$req = $Conn->prepare("SELECT * FROM societes WHERE soc_id = " . $g['eal_societe']);
											$req->execute();
											$societe = $req->fetch();

											$req = $Conn->prepare("SELECT * FROM agences WHERE age_id = " . $g['eal_agence']);
											$req->execute();
											$agence = $req->fetch();
											?>
												<tr>
													<td><?= $utilisateur['uti_prenom'] ?> <?= $utilisateur['uti_nom'] ?></td>
													<td><?= $societe['soc_nom'] ?></td>
													<td><?= $agence['age_nom'] ?></td>
													<td><?= $utilisateur['uti_mail'] ?></td>
													<td>
														<?php if(!empty($g['eal_alerte'])){ ?>
															OUI
														<?php }else{ ?>
															NON
														<?php }?>
													</td>
													<td>
														<?php if(!empty($g['eal_valide'])){ ?>
															OUI
														<?php }else{ ?>
															NON
														<?php }?>
													</td>
													<td>
														<a href="evac_gestion_alerte_mod.php?eal_societe=<?= $g['eal_societe'] ?>&eal_agence=<?= $g['eal_agence'] ?>&eal_ref_id=<?= $g['eal_ref_id'] ?>" class="btn btn-sm btn-warning">
															<i class="fa fa-pencil"></i>
														</a>
														<a href="evac_gestion_alerte_suppr.php?eal_societe=<?= $g['eal_societe'] ?>&eal_agence=<?= $g['eal_agence'] ?>&eal_ref_id=<?= $g['eal_ref_id'] ?>" class="btn btn-sm btn-danger">
															<i class="fa fa-times"></i>
														</a>
													</td>
												</tr>
										<?php } ?>
									</tbody>
								</table>

							</div>
						</div>
					</div>

				</section>
				<!-- End: Content -->
			</section>

		</div>
		<!-- End: Main -->

		<footer id="content-footer" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >
				</div>
				<div class="col-xs-3 footer-right" >
					<a href="evac_gestion_alerte_mod.php" class="btn btn-success btn-sm">
						<i class="fa fa-plus" ></i>
						Nouveau validant
					</a>
				</div>
			</div>
		</footer>


	<?php
		include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/mask/jquery.mask.js" ></script>

		<!-- PLUGIN -->
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>



		<script type="text/javascript">

		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
