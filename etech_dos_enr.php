<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_document.php');
include('modeles/mod_upload.php');
include('modeles/mod_profil.php');
include('modeles/mod_erreur.php');
include('modeles/mod_societe.php');
include('modeles/mod_agence.php');
include('modeles/mod_parametre.php');
include('modeles/mod_utilisateur.php');
// CREATION //

if(isset($_POST['submit'])){
	$check = check_dos($_POST['doc_url'], $_POST['doc_nom_aff']);

	if(isset($_POST['dos'])){
		$d = $_POST['dos'];
	}
	

	if(empty($check)){
		$doc_id = insert_dossier($_POST['doc_nom_aff'],$_POST['doc_url'],$_SESSION['acces']['acc_ref_id'],date('Y-m-d'), 1);

		header("Location: etech.php?dos=" . $doc_id . "&succes=1");


	}else{
		if(isset($d)){
			header("Location: etech_dos.php?dos=" . $d . "&erreur=2");
		}else{
			header("Location: etech_dos.php?erreur=2");
		}
		
	}

}

// EDITION //

if(isset($_POST['submit2'])){

	$doc_id = $_POST['doc_id'];
	$check = check_dos_mod($_POST['doc_url'], $_POST['doc_nom_aff'], $_POST['doc_id']);
	if(empty($check)){

		$document = get_document($doc_id);

		$url_doc = $document['doc_url'] . $document['doc_nom_aff'] . "/";

		$url_doc_apres = $_POST['doc_url'] . $_POST['doc_nom_aff'] . "/";
		
		$update_docs = get_documents_like($document['doc_url'] . $document['doc_nom_aff'] . "/", $doc_id);

		foreach($update_docs as $u){

			

			$new_url=str_replace($url_doc,$url_doc_apres, $u['doc_url']);
			
			 update_document_dossier($u['doc_id'], $new_url);
			
		}

	
		update_dossier($doc_id, $_POST['doc_nom_aff'],$_POST['doc_url'],1);
		


		if(isset($_POST['dos'])){
			header("Location: etech.php?dos=" . $_POST['dos'] . "&succes=2");
		}else{
			header("Location: etech.php?succes=2");
		}


	}else{
		if(isset($_POST['dos'])){
			header("Location: etech_dos_mod.php?id=" . $_POST['doc_id'] . "&dos=" . $_POST['dos'] . "&erreur=3");
		}else{
			header("Location: etech_dos_mod.php?id=" . $_POST['doc_id'] . "&erreur=3");
		}
		
	}

}

?>