<?php

include "includes/controle_acces.inc.php";
include "modeles/mod_add_stagiaire.php";
include "modeles/mod_get_action_sessions.php";

// RECUPERATION DES FICHIER CSV STAGIAIRES POUR INTEGRATION


$dossier="documents/import/stagiaires";

$fichiers=scandir($dossier);
if(!empty($fichiers)){
	if(is_array($fichiers)){
		
		$conn_get_id=0;
		
		foreach($fichiers as $f){
			
			//echo("fichier : " . $f . "<br/>");
			$ftxt=substr($f,0,strlen($f)-4);
			//echo("ftxt : " . $ftxt . "<br/>");
			$act=explode("_",$ftxt);
			
			//echo("act length : " . count($act) . "<br/>");
			if(count($act)==5){
				
				//echo("TRAITEMENT<br/>");
				
				$societe=$act[1];
				$action_id=$act[2];
				$action_client_id=$act[3];
				$session_id=$act[4];
				
				if($societe!=$conn_get_id){
					
					$conn_get_id=$societe;
					
					include "connexion_get.php";
					
					// preparation des requetes
				
					$req_cli=$ConnGet->prepare("SELECT acl_client FROM Actions_Clients WHERE acl_id=:action_client");
					
					$sql_verif_action="SELECT ast_stagiaire FROM Actions_Stagiaires WHERE ast_stagiaire=:stagiaire AND ast_action_client=:action_client;";
					$verif_action=$ConnGet->prepare($sql_verif_action);
					
					$sql_ins_action="INSERT INTO Actions_Stagiaires (ast_stagiaire,ast_action_client,ast_confirme,ast_valide) VALUES (:stagiaire, :action_client, 1, 0);";
					$ins_action=$ConnGet->prepare($sql_ins_action);

					$sql_verif_session="SELECT ass_stagiaire FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire AND ass_session=:session;";
					$verif_session=$ConnGet->prepare($sql_verif_session);
					
					$sql_ins_session="INSERT INTO Actions_Stagiaires_Sessions (ass_session,ass_stagiaire) VALUES (:session, :stagiaire);";
					$ins_session=$ConnGet->prepare($sql_ins_session);
					
					$sql_action="UPDATE Actions SET act_nb_stagiaire=act_nb_stagiaire+:ajout WHERE act_id=:action;";
					$req_action=$ConnGet->prepare($sql_action);
				}
				
				$nbAjout=0;
				
				$req_cli->bindParam(":action_client",$action_client_id);
				$req_cli->execute();
				$client=$req_cli->fetch();
				
				$row = 0; // numero de ligne
				if (($handle = fopen($dossier ."/". $f, "r")) !== FALSE) {
					$i = 0;
					while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						
						//echo("BOUCLE SUR LIGNE<br/>");
						$row++;
						
						if($row>1){
							
							$data[0]=utf8_encode($data[0]);
							$ligne = explode(";", $data[0]);
							
							if(count($ligne)>2){
								if(!empty($ligne[0]) AND !empty($ligne[1])){
									
									echo("Nom : " . $ligne[0] . "<br/>");
									echo("Prénom : " . $ligne[1] . "<br/>");
																	
									$stagiaire=add_stagiaire($ligne[0],$ligne[1],$ligne[2],$ligne[3],$ligne[4],$ligne[5],$ligne[6],$ligne[7],$ligne[8],$ligne[9],$ligne[10],$ligne[11],$ligne[12],$ligne[13],$client["acl_client"],true);
									var_dump($stagiaire);
									echo("<br/><br/>");
									if(!empty($stagiaire)){
										if(is_int($stagiaire)){
											if($stagiaire>0){
												// le stagiaire a été crée ou existe et l'ID a été retourné
											
												//echo("Stagiaire : " . $stagiaire . "<br/>");
												// inscription à l'action
												
											
												$verif_action->bindParam(":stagiaire",$stagiaire);
												$verif_action->bindParam(":action_client",$action_client_id);
												$verif_action->execute();
												$result=$verif_action->fetch();
												if(empty($result)){
													
													$ins_action->bindParam(":stagiaire",$stagiaire);
													$ins_action->bindParam(":action_client",$action_client_id);
													$ins_action->execute();
												
													$nbAjout++;
												}
												
												
												
												// inscription aux sessions
												echo("session_id : " . $session_id . "<br/>");
												if($session_id>0){
												
													$verif_session->bindParam(":session",$session_id);
													$verif_session->bindParam(":stagiaire",$stagiaire);
													$verif_session->execute();
													$result=$verif_session->fetch();
													if(empty($result)){
														
														$ins_session->bindParam(":session",$session_id);
														$ins_session->bindParam(":stagiaire",$stagiaire);
														$ins_session->execute();
													}
												
												}else{
													
													$sessions=get_action_sessions($action_id,$action_client_id);
													foreach($sessions as $s){
														$verif_session->bindParam(":session",$s["ase_id"]);
														$verif_session->bindParam(":stagiaire",$stagiaire);
														$verif_session->execute();
														$result=$verif_session->fetch();
														if(empty($result)){
															//echo("session : " . $s["ase_id"] . "<br/>");
															$ins_session->bindParam(":session",$s["ase_id"]);
															$ins_session->bindParam(":stagiaire",$stagiaire);
															$ins_session->execute();
														}
														
													}
												}
											}
										}
										
									}

								}
							}
						}
					}
					fclose($handle);
					
					//unlink($dossier ."/". $f);
				}
				if($nbAjout>0){
					$req_action->bindParam(":ajout",$nbAjout);
					$req_action->bindParam(":action",$action_id);
					$req_action->execute();
				}
			}
		// fin foreach sur chaque fichier
		}
		//header("location: action_voir.php?action=" . $action_id . "&onglet=1");
	}
}
?>
