<?php
	// a mettre sur toute les pages de l'application
	// contient le session_start();
include "includes/controle_acces_cli.inc.php";
include "modeles/mod_parametre.php";
include "includes/connexion.php";
include "includes/connexion_soc.php";
include "includes/connexion_fct.php";

// SUR QUEL MENU NOUS SOMMES

$menu_actif = 3;


if(isset($_SESSION['retourClient'])){
	unset($_SESSION['retourClient']);
}

$sql="SELECT soc_id FROM Societes ORDER BY soc_id;";
$req = $Conn->query($sql);
$d_societes=$req->fetchAll();
if(!empty($d_societes)){
	foreach($d_societes as $soc){
		$ConnFct=connexion_fct($soc["soc_id"]);
		$sql="SELECT fac_id,fac_chrono,fac_numero,fac_date,fac_total_ht,fac_total_ttc,fac_regle,fac_date_reg_prev
		,cli_code,cli_nom 
		FROM Factures 
		LEFT OUTER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence) 
		WHERE fac_client IN(" . $_SESSION['acces']['acc_groupe_liste'] . ")";
		$sql.=" ORDER BY fac_date,fac_chrono,fac_numero;";
		$req = $ConnFct->prepare($sql);
		$req->execute();
		$factures_build = $req->fetchAll();
		foreach($factures_build as $k=>$f){
			$reste_du_test=$f['fac_total_ttc']-$f['fac_regle'];
			if($reste_du_test > 0.5){
				$factures[$soc['soc_id']] = $factures_build[$k];
			}
			
		}
		if(!empty($factures)){
			$count = count($factures);
		}
		
	}
}
// fin
//AND fac_regle<fac_total_ttc AND fac_date_reg_prev<=NOW()
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		
	<link rel="stylesheet" type="text/css" href="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
	
	<!-- CSS Si2P -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	
	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm no-scroll" >
		<div id="main" style="height:100%!important;">
<?php		
				include "includes/header_cli.inc.php"; 
		 ?>			
			<section id="content_wrapper" style="height:100%!important;">					
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn" style="height:100%!important;">
					<h1>Factures en attente</h1>
					
					<?php if(!empty($count)){ ?>
						<p class="alert alert-danger text-center" >
							Vous avez <strong><?= $count  ?> facture(s)</strong> en attente de règlement !
						</p>
						<?php }else{?>
							<p class="alert alert-success text-center" >
								Vous n'avez aucune facture en attente de règlement !
							</p>
						<?php }?>
						<p class="alert alert-info text-center" >
							Si la facture n'est pas disponible en pdf , merci de nous contacter.
						</p>
		<?php 		if(!empty($factures)){ 
					$total_ht=0;
					$total_ttc=0;
					$total_regle=0;
					$total_reste_du=0; ?>
	
					<div class="table-responsive">
						<table class="table table-striped table-hover" >
							<thead>
								<tr class="dark2" >
									<th>Chrono</th>
									<th>Client</th> 
									<th>Facture</th>
									<th>Date</th>
									<th class="text-right" >H.T.</th>
									<th class="text-right" >T.T.C.</th>
									<th class="text-right" >Réglé</th>
									<th class="text-right" >Reste Dû</th>
									<th class="text-center" >Date prévue de réglement</th>											
								</tr>
							</thead>
							<tbody>
					<?php		$page=array();
					
								foreach($factures as $k=>$f){
									
									$page[]=$f["fac_id"];

									// style de la ligne
									
									$style="";
									if(!empty($f['fac_nature'])){
										if(!empty($d_client_sous_categorie[$f['sus_sous_categorie']])){
											$style="background-color:#FED";				
										}
									}
									$fac_date="";
									if(!empty($f['fac_date'])){
										$dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
										$fac_date=$dt_fac_date->format("d/m/Y");
									}
									$fac_date_reg_prev="";
									if(!empty($f['fac_date_reg_prev'])){
										$dt_fac_date_reg_prev=date_create_from_format("Y-m-d",$f['fac_date_reg_prev']);
										$fac_date_reg_prev=$dt_fac_date_reg_prev->format("d/m/Y");
									}
									$reste_du=$f['fac_total_ttc']-$f['fac_regle'];
									
									$total_ht+=$f['fac_total_ht'];
									$total_ttc+=$f['fac_total_ttc'];
									$total_regle+=$f['fac_regle'];
									$total_reste_du+=$reste_du; 
									
									?>
									<tr>
										<td><?= $f['fac_chrono'] ?></td>
										<td><?= $f['cli_code']?></td>
										<td>
											<?php if(file_exists("documents/Societes/".$k."/Factures/".$f["fac_numero"].".pdf")){ ?>
											<a href="documents/Societes/<?=$k?>/Factures/<?=$f["fac_numero"]?>.pdf" target="_blank" >
												<i class="fa fa-file-pdf-o" ></i> <?=$f["fac_numero"]?>
											</a>
											<?php }else{?>
												<?=$f["fac_numero"]?>
											<?php }?>
										</td>																					
										<td><?=	$fac_date?></td>
										<td class="text-right" ><?=number_format($f['fac_total_ht'], 2, ',', ' '); ?></td>
										<td class="text-right" >
							<?php			
												echo(number_format($f['fac_total_ttc'], 2, ',', ' ')); ?>
											
										</td>
										<td class="text-right" ><?= number_format($f['fac_regle'], 2, ',', ' ');?></td>
										<td class="text-right" ><?=number_format($reste_du, 2, ',', ' ');?></td>
										<td class="text-center" ><?=$fac_date_reg_prev?></td>												
									</tr>
											<?php					
	} ?>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="4" >Total :</th>
									<td class="text-right" ><?=number_format($total_ht, 2, ',', ' ');?></td>
									<td class="text-right" ><?=number_format($total_ttc, 2, ',', ' ');?></td>
									<td class="text-right" ><?=number_format($total_regle, 2, ',', ' ');?></td>
									<td class="text-right" ><?=number_format($total_reste_du, 2, ',', ' ');?></td>
									<td>&nbsp;</td>
								</tr>
							</tfoot>
						</table>
					
					</div>
				<?php } ?>
				</section>
			</section>
		</div>
		<footer id="content-footer" >
			<div class="row">
				<div class="col-xs-3 text-center"></div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right">
		
				</div>
			</div>
		</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>

		<script type="text/javascript" src="vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css"></script>
		
		
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
		

		</script>
	</body>
</html>
