<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_add_notification.php');
include("modeles/mod_envoi_mail.php");

    $conge=0;
    if(!empty($_GET['conge'])){
        $conge=$_GET['conge'];
    }
    $uti=0;
    if(!empty($_GET['uti'])){
        $uti=$_GET['uti'];
    }
    $ok = 0;
    $refus = 0;
    if(isset($_GET['ok'])){
        $ok = 1;
        $con_uti_valide = 1;
    }elseif(isset($_GET['refus'])){
        $refus = 1;
        $con_uti_valide = 2;
    }
    $resp=0;
    if(!empty($_GET['resp'])){
        $resp=$_GET['resp'];
    }
    $req = $Conn->prepare("SELECT * FROM conges WHERE con_id = :con_id");
    $req->bindValue(':con_id', $conge);
    $req->execute();
    $_conge = $req->fetch();

    if(!empty($uti)){

        $req = $Conn->prepare("UPDATE conges SET con_uti_valide = :con_uti_valide, con_uti_valide_date=NOW() WHERE con_id = :con_id");
        $req->bindValue(':con_id', $conge);
        $req->bindValue(':con_uti_valide', $con_uti_valide);
        $req->execute();
        if($_conge['con_resp_valide'] == 0){
            add_notifications("<i class='fa fa-calendar'></i>","Vous devez valider la demande de congés " . $_conge['con_id'],"bg-info","conges_voir.php?conge=" . $_conge['con_id'],"","",$_conge['con_responsable'],0,0,0);
        }

    }elseif(!empty($resp)){
        if($_conge['con_responsable'] != $_SESSION['acces']['acc_ref_id']){
            $req = $Conn->prepare("SELECT uti_nom,uti_prenom FROM conges WHERE uti_id = :uti_id");
            $req->bindValue(':uti_id', $_SESSION['acces']['acc_ref_id']);
            $req->execute();
            $po = $req->fetch();

            $req = $Conn->prepare("UPDATE conges SET con_resp_po = 1, con_uti_valide = :con_uti_valide, con_uti_valide_date=NOW(),con_resp_po_prenom=:con_resp_po_prenom,con_resp_po_nom=:con_resp_po_nom, con_resp_po_uti= :con_resp_po_uti  WHERE con_id = :con_id");
            $req->bindValue(':con_id', $conge);
            $req->bindValue(':con_uti_valide', $con_uti_valide);
            $req->bindValue(':con_resp_po_uti', $_SESSION['acces']['acc_ref_id']);
            $req->bindValue(':con_resp_po_nom', $po['uti_nom']);
            $req->bindValue(':con_resp_po_prenom', $po['uti_prenom']);
            $req->execute();
        }else{
            $req = $Conn->prepare("UPDATE conges SET con_resp_valide = :con_uti_valide, con_resp_valide_date=NOW() WHERE con_id = :con_id");
            $req->bindValue(':con_id', $conge);
            $req->bindValue(':con_uti_valide', $con_uti_valide);
            $req->execute();
        }

        if($_conge['con_uti_valide'] == 0){
            add_notifications("<i class='fa fa-calendar'></i>","Vous devez valider la demande de congés " . $_conge['con_id'],"bg-info","conges_voir.php?conge=" .  $_conge['con_id'],"","",$_conge['con_utilisateur'],0,0,0);
        }
    }
    $sql="SELECT * FROM utilisateurs LEFT JOIN conges ON (conges.con_utilisateur = utilisateurs.uti_id) WHERE con_id = " . $_conge['con_id'];
    $req = $Conn->query($sql);
    $utilisateur = $req->fetch();
    // NOTIFICATIONS A AFFICHER
    $param_mail=array(
        "sujet" => "ORION : Demande de congés validée pour " . $utilisateur['uti_prenom'] . " " . $utilisateur['uti_nom'],
        "identite" => "ORION",
        "message" => "Les congés pour " . $utilisateur['uti_prenom'] . " " . $utilisateur['uti_nom'] . " ont été validés pour la période du " . convert_date_txt($_conge['con_date_deb']) . " au " . convert_date_txt($_conge['con_date_fin'])
    );

    $adr_mail=array(
        0 => array(
            "adresse" => "conges@si2p.net",
            "nom" => "Congés Si2P"
        ),
    );

    $mail=envoi_mail("si2p",$param_mail,$adr_mail);

    if($con_uti_valide == 2){
        $req = $ConnSoc->prepare("DELETE FROM plannings_dates WHERE pda_ref_1 = " . $conge);
        $req->execute();
    }elseif($con_uti_valide == 1){
        $req = $ConnSoc->prepare("UPDATE plannings_dates SET pda_confirme = 1, pda_style_txt=:pda_style_txt WHERE pda_ref_1 = :con_id");
        $req->bindValue(':con_id', $conge);
        $req->bindValue(':pda_style_txt', "#000000");
        $req->execute();
    }

    $_SESSION['message'][] = array(
        "titre" => "Succès",
        "type" => "success",
        "message" => "Votre validation a été enregistrée"
    );
    Header("Location: conges_voir.php?conge=" . $conge);
    die();





