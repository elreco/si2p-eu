<?php

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'modeles/mod_add_notification.php';

$erreur="";
if(!empty($_POST)){

	$sql = "SELECT uti_id, uti_societe,uti_agence FROM Utilisateurs WHERE uti_archive = 0;";
	$req = $Conn->query($sql);
	$d_utilisateurs=$req->fetchAll();

	foreach($d_utilisateurs as $uti){
		$agence = 0;
		if(!empty($uti['uti_agence'])){
			$agence = $uti['uti_agence'];
		}
		if(empty($liste_uti)){
			$liste_uti = $uti['uti_id'];
		}else{
			$liste_uti =  $uti['uti_id'] . "," . $liste_uti;
		}
	}
	if(!empty($_POST['not_societe'])){
		foreach($_POST['not_societe'] as $soc => $value){


			// PAS D'AGENCE
			add_notifications("<i class='fa fa-" . $_POST['icone'] . "'></i>",$_POST['texte'],$_POST['classe'],$_POST['url'],"","",$liste_uti,$soc,0,0, $_POST['not_type']);


		}


	}

	if(!empty($_POST['not_agence'])){

			foreach($_POST['not_agence'] as $soc => $value){
				// PAS D'AGENCE
				add_notifications("<i class='fa fa-" . $_POST['icone'] . "'></i>",$_POST['texte'],$_POST['classe'],$_POST['url'],"","",$liste_uti,$value,$soc,0, $_POST['not_type']);

			}
	}

}else{
	$erreur="Formulaire incomplet!";
}

if(!empty($erreur)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur
	);
}else{
	$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Notification envoyée"
	);
}

header("location : notification.php");

 ?>
