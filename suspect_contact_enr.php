<?php
include "includes/controle_acces.inc.php";

include("includes/connexion_soc.php");



// MAJ D'UN CONTACT SUSPECT

$suspect=0;
if(!empty($_POST["suspect"])){
	$suspect=intval($_POST["suspect"]);	
}
$contact=0;
if(!empty($_POST["contact"])){
	$contact=intval($_POST["contact"]);	
}

if(empty($suspect)){
	echo("Erreur!");
	die();
}


$erreur_txt="";

if(empty($_POST['sco_nom'])){
	
	$erreur_txt="Impossible de mettre à jour le contact";
	
}else{
	
	// TRAITEMENT DU FORM

	$sco_titre=0;
	if(!empty($_POST['sco_titre'])){
		$sco_titre=intval($_POST['sco_titre']);
	}

	$sco_fonction=0;
	$sco_fonction_nom="";
	if(!empty($_POST['sco_fonction'])){
		if($_POST['sco_fonction']=="autre"){
			$sco_fonction_nom=$_POST['sco_fonction_nom'];
		}else{
			$sco_fonction=intval($_POST['sco_fonction']);
		}
	}

	
	/*$sco_adresse=0;
	if(!empty($_POST["sco_adresse"])){
		$sco_adresse=intval($_POST['sco_adresse']);	
		
	}*/
	
	$sco_compta=0;
	if(!empty($_POST["sco_compta"])){
		$sco_compta=1;	
	}

	// FIN TRAITEMENT FORM
	
	// CONTROLE
	
	// contact avant modif
	
	/*if($contact>0){
		
		$sql="SELECT sco_adresse FROM Suspects_Contacts WHERE sco_id=" . $contact . ";";
		$req = $ConnSoc->query($sql);
		$d_contact_old=$req->fetch();
		if(empty($d_contact_old)){
			unset($d_contact_old);
		}
		
	}*/

	
	/*if($sco_adresse>0){
		
		if($sco_defaut==0){
			
			// si adresse lié n'a pas de contact on force adr_defaut
			$sql="SELECT sco_id FROM Suspects_Contacts WHERE sco_adresse=" . $sco_adresse . " AND sco_defaut AND NOT sco_id=" . $contact . ";";
			$req = $ConnSoc->query($sql);
			$d_contact_defaut=$req->fetch();
			if(empty($d_contact_defaut)){
				$sco_defaut=1;	
			}
		}
		
		// info sur adresse associé au contact
		$sql="SELECT sad_defaut FROM Suspects_Adresses WHERE sad_id=" . $sco_adresse . ";";
		$req = $ConnSoc->query($sql);
		$d_adresse=$req->fetch();
		if(empty($d_adresse)){
			unset($d_adresse);
		}	
		
	}*/
	
	
	// FIN CONTROLE

	
	if($contact>0){
		
		// MODIF
		
		$sql="UPDATE Suspects_Contacts SET 
		sco_fonction = :sco_fonction,
		sco_fonction_nom = :sco_fonction_nom,
		sco_titre = :sco_titre,
		sco_nom = :sco_nom,
		sco_prenom = :sco_prenom,
		sco_tel = :sco_tel,
		sco_portable = :sco_portable,
		sco_fax = :sco_fax,
		sco_mail = :sco_mail,
		sco_compta = :sco_compta,
		sco_comment = :sco_comment 
		WHERE sco_id=" . $contact . " AND sco_ref_id=" . $suspect . ";";
		$req = $ConnSoc->prepare($sql);
		$req->bindParam("sco_fonction",$sco_fonction);
		$req->bindParam("sco_fonction_nom",$sco_fonction_nom);
		$req->bindParam("sco_titre",$sco_titre);
		$req->bindParam("sco_nom",$_POST["sco_nom"]);
		$req->bindParam("sco_prenom",$_POST["sco_prenom"]);
		$req->bindParam("sco_tel",$_POST["sco_tel"]);
		$req->bindParam("sco_portable",$_POST["sco_portable"]);
		$req->bindParam("sco_fax",$_POST["sco_fax"]);
		$req->bindParam("sco_mail",$_POST["sco_mail"]);
		$req->bindParam("sco_compta",$sco_compta);
		$req->bindParam("sco_comment",$_POST["sco_comment"]);
		try{
			$req->execute();
		}catch( PDOException $Exception ){
			$erreur_txt=$Exception->getMessage();
			//$erreur_txt.="Erreur, le contact n'a pas été mis à jour!";
		}
		
	}else{
		
		// CREATION
		
		$req = $ConnSoc->prepare("INSERT INTO Suspects_Contacts (
		sco_ref, sco_ref_id, sco_fonction, sco_fonction_nom, sco_titre, sco_nom, sco_prenom, sco_tel, sco_portable, sco_fax, sco_mail, sco_compta
		,sco_comment) VALUES (
		1, :sco_ref_id, :sco_fonction, :sco_fonction_nom, :sco_titre, :sco_nom, :sco_prenom, :sco_tel, :sco_portable, :sco_fax, :sco_mail, :sco_compta
		,:sco_comment);");
		$req->bindParam("sco_ref_id",$suspect);
		$req->bindParam("sco_fonction",$sco_fonction);
		$req->bindParam("sco_fonction_nom",$sco_fonction_nom);
		$req->bindParam("sco_titre",$sco_titre);
		$req->bindParam("sco_nom",$_POST["sco_nom"]);
		$req->bindParam("sco_prenom",$_POST["sco_prenom"]);
		$req->bindParam("sco_tel",$_POST["sco_tel"]);
		$req->bindParam("sco_portable",$_POST["sco_portable"]);
		$req->bindParam("sco_fax",$_POST["sco_fax"]);
		$req->bindParam("sco_mail",$_POST["sco_mail"]);
		$req->bindParam("sco_compta",$sco_compta);
		$req->bindParam("sco_comment",$_POST["sco_comment"]);
		try {
			$req->execute();
			$contact = $ConnSoc->lastInsertId();
		}catch( PDOException $Exception ){
			$erreur_txt=$Exception->getMessage();
		}
	}
	
	if(empty($erreur_txt)){
		
		/*if(isset($d_contact_old)){
			
			if($d_contact_old["sco_adresse"]!=$sco_adresse AND !empty($d_contact_old["sco_adresse"]) AND $d_contact_old["sco_defaut"]==1){
				
				// le contact était le contact par défaut de l'ancienne adresse.
				
				$sql="SELECT sad_id FROM Suspects_Adresses WHERE sad_id=" . $d_contact_old["sco_adresse"] . " AND sad_defaut;";
				$req = $ConnSoc->query($sql);
				$d_adresse_old=$req->fetch();
				if(!empty($d_adresse_old)){
					
					// l'ancienne adresse est l'adresse par defaut du suspect
					// il faut mettre à jour la fiche suspect 
					
					$sql="UPDATE suspects SET 
					sus_contact = 0,
					sus_con_fct = 0,
					sus_con_fct_nom = '',
					sus_con_titre = 0,
					sus_con_nom = '',
					sus_con_prenom = '',
					sus_con_tel = '',
					sus_con_mail = ''
					WHERE sus_id=" . $suspect . ";";
					$ConnSoc->query($sql);
					
				}
			}
		}
		
		if(isset($d_adresse)){
			
			// le contact est lié à une adresse
			
			if($sco_defaut==1){
				
				$sql="UPDATE Suspects_Contacts SET sco_defaut = 0 WHERE sco_adresse=" . $sco_adresse . " AND NOT sco_id=" . $contact . ";";
				try {
					$req=$ConnSoc->query($sql);
				}catch( PDOException $Exception ){
					$erreur_txt=$Exception->getMessage();
				}
				
				if($d_adresse["sad_defaut"]==1){
					
					// il s'agit du contact par defaut de l'adresse par defaut => maj de la fiche suspect
					
					$sql="UPDATE suspects SET 
					sus_contact = :sus_contact,
					sus_con_fct = :sus_con_fct,
					sus_con_fct_nom = :sus_con_fct_nom,
					sus_con_titre = :sus_con_titre,
					sus_con_nom = :sus_con_nom,
					sus_con_prenom = :sus_con_prenom,
					sus_con_tel = :sus_con_tel,
					sus_con_mail = :sus_con_mail
					WHERE sus_id=" . $suspect . ";";
					$req=$ConnSoc->prepare($sql);
					$req->bindParam("sus_contact",$contact);
					$req->bindParam("sus_con_fct",$sco_fonction);
					$req->bindParam("sus_con_fct_nom",$sco_fonction_nom);
					$req->bindParam("sus_con_titre",$sco_titre);
					$req->bindParam("sus_con_nom",$_POST["sco_nom"]);
					$req->bindParam("sus_con_nom",$_POST["sco_nom"]);
					$req->bindParam("sus_con_prenom",$_POST["sco_prenom"]);
					$req->bindParam("sus_con_tel",$_POST["sco_tel"]);
					$req->bindParam("sus_con_mail",$_POST["sco_mail"]);				
					try {
						$req->execute();
					}catch( PDOException $Exception ){
						$erreur_txt=$Exception->getMessage();
					}
					
				}
			}
		}*/
	}
	// FIN GESTION COMPTE
}
if(!empty($_SESSION['retourContact'])){
	 $retour=$_SESSION['retourContact'];
	 if(!empty($erreur_txt)){
		$retour.="&erreur=" . $erreur_txt; 
	 }
}

$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Le contact du suspect a été actualisé"
	);
header("location : " . $retour);
