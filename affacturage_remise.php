<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('includes/connexion_soc.php');

	// ENREGISTREMENT D'UN FICHIER DE REMISE


	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	$erreur_txt="";

	$sql="SELECT soc_nom,soc_tva,soc_contrat_aff FROM Societes WHERE soc_id=" . $acc_societe . ";";
	$req=$Conn->query($sql);
	$d_societe=$req->fetch();
	if(!empty($d_societe)){
		if(empty($d_societe["soc_contrat_aff"])){
			$erreur_txt="Cette société ne dispose pas d'un contrat d'affacturage";
		}
	}else{
		$erreur_txt="Cette société ne dispose pas d'un contrat d'affacturage";
	}



	if(empty($erreur_txt)){
		
		// SI la de de reg est depasse de plus de 45 jours la facture ne peux pas etre remise
		$date_limite=date('Y-m-d', strtotime(date('Y-m-d') . ' -45 day'));

		$sql_fac="SELECT fac_id,fac_cli_nom,fac_numero,fac_total_ht,fac_total_ttc,fac_regle,fac_adresse,fac_opca
		,DATE_FORMAT(fac_date,'%d/%m/%Y') AS facture_date,DATE_FORMAT(fac_date_reg_prev,'%d/%m/%Y') AS reglement_date,fac_cli_ville,fac_cli_cp
		,cli_id,cli_code,cli_nom,cli_filiale_de,cli_groupe,cli_affacturable 
		FROM Factures,Clients WHERE fac_client=cli_id AND fac_agence=cli_agence
		AND fac_affacturage=1 AND NOT (fac_total_ttc-fac_regle)=0 AND fac_aff_remise=0 AND fac_blocage=1";
		$sql_fac.=" AND (fac_opca=0 OR ISNULL(fac_opca) OR (NOT fac_opca_type=5 AND NOT fac_opca_type=6) )";
		$sql_fac.=" AND ( (fac_nature=1 AND fac_total_ttc>fac_regle AND fac_date_reg_prev>='" . $date_limite . "') OR (fac_nature=2 AND fac_total_ttc<fac_regle) )
		ORDER BY fac_date,fac_id;";
		
		// supp du critere cli_affacturable -> remplace par fac_affacturage=1 // car rib factor obligatoire
		$req_fac=$ConnSoc->query($sql_fac);
		$d_factures=$req_fac->fetchAll();


		$sql_get_ad="SELECT cli_id,cli_code,cli_siren,cli_filiale_de,adr_siret FROM Clients,Adresses WHERE adr_ref_id=cli_id AND adr_ref=1 AND adr_type=2 AND adr_id=:adresse_fac;";
		$req_get_ad=$Conn->prepare($sql_get_ad);

		$sql_get_ad_2="SELECT adr_siret FROM Adresses
		WHERE adr_ref=1 AND adr_type=2 AND adr_defaut=1 AND adr_ref_id = :adr_ref_id;";
		$req_get_ad_2=$Conn->prepare($sql_get_ad_2);

	}


?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<form method="post" action="affacturage_remise_enr.php" >
			<div>
				<input type="hidden" name="aff_id" id="aff_id" value="0" >
			</div>
			<!-- Start: Main -->
			<div id="main">

				<?php
					include "includes/header_def.inc.php";
				?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">
				<?php	if(!empty($erreur_txt)){
							echo("<p class='alert alert-danger' >" . $erreur_txt . "</p>");
						}else{ ?>
							<div class="row">
								<div class="col-md-12">
									<div class="admin-form theme-primary ">
										<div class="panel heading-border panel-primary">
											<div class="panel-body bg-light">
												<div class="text-center">
													<div class="content-header">
														<h2>Nouvelle <b class="text-primary">remise</b></h2>
													</div>
												</div>

												<div class="row" >
													<div class="col-md-6" >

														<label for="Date de remise" >Date de la remise :</label>
														<span class="field prepend-icon">
															<input type="text" id="aff_date" name="aff_date" class="gui-input datepicker" placeholder="Date de remise" value="" required />
															<span class="field-icon">
																<i class="fa fa-calendar-o"></i>
															</span>
														</span>
													</div>
													<div class="col-md-6 pt25" >
														N° de contrat : <b><?=$d_societe["soc_contrat_aff"]?></b>
													</div>
												</div>

												<div class="table-responsive mt15">
													<table class="table table-striped table-hover" style="font-size:12px;" >
														<thead>
															<tr class="dark2" >
																<th>
																	<div class="option-group field">
																		<label class="option option-dark">
																			<input type="checkbox" id="facture_id_all" class="check-all" >
																			<span class="checkbox"></span>
																		</label>
																	</div>
																</th>
																<th>Code client</th>
																<th>Facturation</th>
																<th>Code fac.</th>
																<th>Nom fac.</th>
																<th>CP fac.</th>
																<th>Ville fac.</th>
																<th>SIRET</th>
																<th>SIRET juridique</th>
																<th>SIRET cp</th>
																<th>SIRET ville</th>
																<th>Facture</th>
																<th>Date</th>
																<th class="text-right" >H.T.</th>
																<th class="text-right" >T.T.C.</th>
																<th class="text-right" >Réglé</th>
																<th class="text-right" >Reste Dû</th>
																<th class="text-center" >Date prévue de règlement</th>
																<th>Infos</th>
															</tr>
														</thead>
														<tbody>
												<?php		$total=0;
															$total_ok=0;
															foreach($d_factures as $f){

																$err=false;
																$err_txt="";

																$reste_du=$f["fac_total_ttc"]-$f["fac_regle"];
																$reste_du=round($reste_du,2);
																if($reste_du>-1 AND $reste_du<1){
																	$err=true;
																	$err_txt="err";
																}
																
																$total=$total+$f["fac_total_ttc"];
																

																$adr_siret="";
																
																$faturation_lib="";
																$faturation_code="";
																
																$req_get_ad->bindParam(":adresse_fac",$f["fac_adresse"]);
																$req_get_ad->execute();
																$d_adresse=$req_get_ad->fetch();
																if(!empty($d_adresse)){
																	
																	if($d_adresse["cli_id"]!=$f["cli_id"]){
																		if(empty($f["fac_opca"])){
																			// pour qu'un client soit facturé sur un autre carnet, il doit appartenir à un groupe
																			if($f["cli_groupe"]==1){
																				if($d_adresse["cli_id"]==$f["cli_filiale_de"]){
																					$faturation_lib="Holding";
																				}elseif($d_adresse["cli_filiale_de"]==$f["cli_filiale_de"]){
																					$faturation_lib="Groupe";
																				}else{
																					$faturation_lib="Err carnet";
																					$err_txt="Err carnet";
																					$err=true;
																				}
																				
																			}else{
																				$err=true;
																				$err_txt="L'adresse de facturation ne figure pas sur le carnet du client!";
																			}
																		}else{
																			$faturation_lib="financeur";	
																		}
																	}else{
																		$faturation_lib="client";																	
																	}
																	$faturation_code=$d_adresse["cli_code"];
																	
																	if(!empty($d_adresse["adr_siret"]) AND !empty($d_adresse["cli_siren"])){
																		$adr_siret=$d_adresse["cli_siren"] . " " . $d_adresse["adr_siret"];
																	}else{
																	/*	// si l'adresse utilise sur la facture n'a pas de siret on regarde si l'adresse de facturation par defaut en a un.
																		// si c'est le cas on l'utilise 
																		$req_get_ad_2->bindParam(":adr_ref_id",$f["cli_id"]);
																		$req_get_ad_2->execute();
																		$d_adresse_2=$req_get_ad_2->fetch();
																		if(!empty($d_adresse_2)){

																			if(!empty($d_adresse_2["adr_siret"]) AND !empty($d_adresse["cli_siren"])){
																				$adr_siret=$d_adresse["cli_siren"] . " " . $d_adresse_2["adr_siret"];
																			}else{
																				$err=true;
																			}
																		}else{
																			$err=true;
																		}*/
																		$err_txt="err";
																		$err=true;
																	}
																}else{
																	$err=true;
																	$err_txt="Impossible d'identifier l'adresse de facturation";
																} 
																
																// SI OK -> ON LANCE LE CONTROLE DE SIRET
																$resultat="";
																$resultat_json=null;
																$siret_nom="";
																$siret_cp="";
																$siret_ville="";
																
																if(!$err){
																	
																	$f["fac_cli_cp"]=str_replace(" ","",$f["fac_cli_cp"]);
																	$f["fac_cli_ville"]=str_replace("CEDEX","",$f["fac_cli_ville"]);
																	$f["fac_cli_ville"]=str_replace("cedex","",$f["fac_cli_ville"]);
																	$f["fac_cli_ville"]=str_replace("Cedex","",$f["fac_cli_ville"]);
																	$f["fac_cli_ville"]=str_replace("-"," ",$f["fac_cli_ville"]);
																	$f["fac_cli_ville"]=trim($f["fac_cli_ville"]);
																	
																	$siret=str_replace(" ","",$adr_siret);
																	sleep(1);
																	$ch = curl_init();
																	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
																	curl_setopt($ch,CURLOPT_URL, "https://entreprise.data.gouv.fr/api/sirene/v1/siret/" . $siret);
																	$resultat_json=curl_exec($ch);
																	
																	curl_close($ch);
																	
																	$resultat=json_decode($resultat_json);
																	if(is_object($resultat)){
																		
																		if(empty($resultat->etablissement)){
																			$err_txt="Le SIRET n'existe pas!";	
																			$err=true;
																		}else{
																			
																			$info_client=$resultat->etablissement;
																			
																			$siret_nom=$info_client->l1_declaree;
																			$siret_cp=$info_client->code_postal;
																			$siret_ville=$info_client->libelle_commune;
																
																			
																			if($info_client->libelle_commune!=$f["fac_cli_ville"] OR $info_client->code_postal!=$f["fac_cli_cp"]){
																				$err_txt="L'adresse de facturation ne correspond pas au SIRET!";	
																				$err=true;
																			}else{
																				
																				//$err_txt="Nom juridique : " . $info_client->l1_declaree;	
																			}
																		}
																	}else{
																		$err_txt="Le SIRET n'a pas pu être testé!" . $resultat_json;	
																		$err=true;
																		
																		
																	}
																																	
																}
																
																if(!$err){
																	$total_ok=$total_ok+$f["fac_total_ttc"];
																}
																
																?>

																<tr <?php if($err) echo("class='warning'"); ?> >
																	<td>

																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" class="facture-id" name="fac_id[]" value="<?=$f["fac_id"]?>" <?php if(!$err) echo("checked"); ?> >
																				<span class="checkbox"></span>
																			</label>
																		</div>
																	</td>
																	<td><?=$f["cli_code"]?></td>
																	<td><?=$faturation_lib?></td>
																	<td><?=$faturation_code?></td>
																	<td><?=$f["fac_cli_nom"]?></td>
																	<td><?=$f["fac_cli_cp"]?></td>
																	<td><?=$f["fac_cli_ville"]?></td>
																	<td><?=$adr_siret?></td>
																	<td><?=$siret_nom?></td>
																	<td><?=$siret_cp?></td>
																	<td><?=$siret_ville?></td>
																	<td><?=$f["fac_numero"]?></td>
																	<td><?=$f["facture_date"]?></td>
																	<td class="text-right" ><?=round($f["fac_total_ht"],2)?></td>
																	<td class="text-right" ><?=round($f["fac_total_ttc"],2)?></td>
																	<td class="text-right" ><?=round($f["fac_regle"],2)?></td>
																	<td class="text-right" ><?=$reste_du?></td>
																	<td class="text-right" ><?=$f["reglement_date"]?></td>
																	<td><?=$err_txt?></td>
																</tr>
												<?php		} ?>
														</tbody>
														<thead>
															<tr>
																<th colspan="13" class="text-right" >Total TTC :</th>
																<td class="text-right" ><?=round($total,2)?></td>
																<td colspan="4" >&nbsp;</td>
															</tr>
															<tr>
																<th colspan="13" class="text-right" >Dont total TTC validé :</th>
																<td class="text-right" ><?=round($total_ok,2)?></td>
																<td colspan="4" >&nbsp;</td>
															</tr>
														</thead>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
				<?php	} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="affacturage_liste.php" class="btn btn-default btn-sm" >
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" >
					</div>
					<div class="col-xs-3 footer-right" >
	<?php			if(empty($erreur_txt)){  ?>
						<button type="submit" class="btn btn-success btn-sm" >
							<i class="fa fa-plus" ></i>
							Enregistrer
						</button>
	<?php			} ?>
					</div>
				</div>
			</footer>

		</form>
<?php	include "includes/footer_script.inc.php"; ?>

		<!-- BEGIN: PAGE SCRIPTS -->

		<!-- THEME -->
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<!--PLUGIN-->
		<script src="vendor/plugins/mask/jquery.mask.js" ></script>

		<script type="text/javascript">
			jQuery(document).ready(function() {



			});
			(jQuery);
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
