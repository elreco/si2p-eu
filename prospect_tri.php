<?php
$menu_actif = "1-4";

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';

require "modeles/mod_client.php";
require "modeles/mod_contact.php";

/*
require "modeles/mod_societe.php";
require "modeles/mod_agence.php";
require "modeles/mod_utilisateur.php";
require "modeles/mod_parametre.php";
require "modeles/mod_droit.php";
require "modeles/mod_get_commerciaux.php";
require "modeles/mod_get_activites_secteurs.php";*/

$_SESSION['retour'] = "prospect_tri.php";

if(isset($_SESSION['prospect_tri'])){ 
    unset($_SESSION['prospect_tri']);
}
if(isset($_SESSION['client_tableau'])){
	unset($_SESSION['client_tableau']);
}

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];    
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
    $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];  
}

// CATEGORIE DE CLIENT DISPO

$sql="SELECT cca_id,cca_libelle FROM clients_categories";
$sql.=" ORDER BY cca_libelle;";
$req=$Conn->query($sql);
$d_categories=$req->fetchAll();


$sql="SELECT cpr_id,cpr_libelle FROM clients_Prescripteurs WHERE NOT cpr_archive";
$sql.=" ORDER BY cpr_libelle;";
$req=$Conn->query($sql);
$d_prescripteurs=$req->fetchAll();


// LES COMMERCIAUX
	
$sql="SELECT com_id,com_label_1,com_label_2 FROM commerciaux";
$mil="";
if($_SESSION['acces']['acc_profil'] == 3 && !$_SESSION['acces']["acc_droits"][6]){
	$mil=" AND com_ref_1 = " . $_SESSION['acces']['acc_ref_id'];
}
if($acc_agence>0){
	$mil.=" AND com_agence= " . $acc_agence;
}
$mil.=" AND com_archive= 0";
if($mil!=""){
	$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
}
$sql.=" ORDER BY com_label_1,com_label_2";
$req = $ConnSoc->query($sql);
$d_commercial=$req->fetchAll();
$sql="SELECT ape_id,ape_code,ape_libelle FROM Ape
ORDER BY ape_code,ape_libelle";
$req = $Conn->query($sql);
$d_apes=$req->fetchAll();


// CATEGORIE DE CLIENT DISPO
$sql="SELECT cca_id,cca_libelle FROM clients_categories ORDER BY cca_libelle;";
$req=$Conn->query($sql);
$d_categories=$req->fetchAll();

// LISTE DES IMPORTS REALISE SUR LA SOCIETE
$sql="SELECT sim_id,sim_libelle FROM Suspects_imports ORDER BY sim_libelle;";
$req=$ConnSoc->query($sql);
$d_imports=$req->fetchAll();


?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Recherche prospect</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			
		</style>
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="prospect_liste.php" id="formulaire" >
			
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-success ">
									<div class="panel heading-border panel-success">
										<div class="panel-body bg-light">
											
												<div class="content-header">
													<h2>Recherche de <b class="text-success title-suscli">prospect</b></h2>
												</div>
												<div class="col-md-10 col-md-offset-1 text-right">
													<button type="button" onclick="recherche()" class="btn btn-success  btn-sm search_advanced" style="margin-bottom:15px;">
														<i class="fa fa-plus" aria-hidden="true"></i> Recherche avancée
													</button>
												</div>
												<div class="col-md-10 col-md-offset-1">
													<div class="row">
														<div class="col-md-12">
															<div class="section-divider mb40">
																<span>Prospect</span>
															</div>
														</div>
													</div>
													<div class="form2" style="text-align: left!important;">
														<div class="row">

															<div class="col-md-6">
																<div class="section">
																	<label for="cli_categorie" >Catégorie:</label>
																	<div class="field select">
																		<select name="cli_categorie" id="cli_categorie" onchange="sous_categorie(this.value)">
																		<option value="0">Catégorie...</option>																		
																<?php		if(!empty($d_categories)){
																				foreach($d_categories as $d_categorie){
																					echo("<option value='" . $d_categorie["cca_id"] . "' >" . $d_categorie["cca_libelle"] . "</option>");
																				}
																			}?>
																		
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
															<div class="col-md-6 cli_sous_categorie_bloc" style="display:none;">
															  <div class="section">
																<label for="cli_sous_categorie" >Sous-catégorie:</label>
																<span class="select">
																  <select id="cli_sous_categorie" name="cli_sous_categorie">
																	<option value="0">Sélectionner une sous catégorie...</option>
																	
																  </select>
																  <i class="arrow"></i>
																</span>
															  </div>
															</div>
												
														</div>
														<div class="row">

															<div class="col-md-6">
																<div class="section">
																	<label for="cli_id" >N° ID:</label>
																	<div class="field prepend-icon">
																		<input type="text" name="cli_id" id="cli_id" class="gui-input" placeholder="ID du client" />
																		<span class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-md-6 champ-client">
																<div class="section">
																	<label for="cli_groupe" >Type:</label>
																	<div class="field select">
																		<select name="cli_groupe" id="cli_groupe">
																			<option value="0">Type...</option>
																			<option value="1">Maison mère</option>
																			<option value="2">Groupe</option>
																			<option value="3">Entreprise seule</option>
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
														</div>
													</div>
													
													<div class="form1" style="text-align: left!important;">
												<!--		<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<label for="select2_prospect" >Recherche d'une fiche :</label>
																	<select id="select2_prospect" class="select2-prospect" >
																	</select>
																	<small>Permet de rechercher et d'afficher une fiche spécifique</small>
																</div>
															</div>
														</div>-->
														<div class="row">
															<div class="col-md-4">
																<div class="section">
																	<label for="cli_code" >Code:</label>
																	<div class="field prepend-icon">
																		<input type="text" name="cli_code" id="cli_code" class="gui-input nom" placeholder="Code">
																		<span for="cli_code" class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="section">
																	<label for="cli_nom" >Nom:</label>
																	<div class="field prepend-icon">
																		<input type="text" name="cli_nom" id="cli_nom" class="gui-input" placeholder="Nom" />
																		<span for="cli_nom" class="field-icon">
																			<i class="fa fa-building-o"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="section">
																	<label for="cli_reference" >Référence Interne:</label>
																	<div class="field prepend-icon">
																		<input type="text" name="cli_reference" id="cli_reference" class="gui-input" placeholder="Référence Interne">
																		<span for="cli_reference" class="field-icon">
																			<i class="fa fa-building-o"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="row">
												<?php 		if(count($d_commercial)==1){ ?>
																<input type="hidden" name="cli_commercial" id="cli_commercial" value="<?= $d_commercial[0]['com_id']?>" />
												<?php 		}else{ ?>
																<div class="col-md-12">
																	<div class="section">
																		<label for="cli_commercial" >Commercial:</label>
																		<select name="cli_commercial" id="cli_commercial"  class="select2">
																			<option value="">Commercial...</option>
																<?php 		if(!empty($d_commercial)){
																				foreach($d_commercial as $com){
																					echo("<option value='" . $com["com_id"] . "' >" . $com["com_label_1"] . " "  . $com["com_label_2"] . "</option>");
																				}
																			} ?>
																		</select>
																	</div>
																</div>
															<?php } ?>
														</div>
													</div>
													
													<div class="form2">
														
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<label for="cli_creation_date_deb" >Prospect enregistré entre:</label>
																	<div class="field prepend-icon">
																		<input type="text" id="cli_creation_date_deb" name="cli_creation_date_deb" class="gui-input date" placeholder="Prospect enregistré entre" />
																		<span class="field-icon"><i class="fa fa-calendar-o"></i></span>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label for="cli_creation_date_fin" >et le:</label>
																	<div class="field prepend-icon">
																		<input type="text" id="cli_creation_date_fin" name="cli_creation_date_fin" class="gui-input date" placeholder="et le">
																		<span class="field-icon">
																			<i class="fa fa-calendar-o"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<label for="cli_prescripteur" >Source:</label>
																	<span class="select">
																		<select id="cli_prescripteur"  name="cli_prescripteur" >
																			<option value="0">Sélectionner une source...</option>
																	<?php	if(!empty($d_prescripteurs)){
																				foreach($d_prescripteurs as $p){
																					echo("<option value='" . $p["cpr_id"] . "' >" . $p["cpr_libelle"] . "</option>");
																				}
																			} ?>
																		</select>
																		<i class="arrow"></i>
																	</span>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">	
																	<label for="cli_prescripteur" >Code APE:</label>
																	<select id="cli_ape"  name="cli_ape" class="select2" >
																		<option value="0">Code APE...</option>
																<?php	if(!empty($d_apes)){
																			foreach($d_apes as $ape){
																				echo("<option value='" . $ape["ape_id"] . "' >" . $ape["ape_code"] . "-" . $ape["ape_libelle"] . "</option>");
																			}
																		} ?>
																	</select>
																</div>
															</div>
														</div>
														
														<div class="row">
															<div class="col-md-4">
																<div class="section">
																	<label for="cli_classification" >Classification:</label>
																	<select id="cli_classification" class="select2 select2-classification"  name="cli_classification" >
																		<option value="0">Sélectionner une classification...</option>
															<?php 		if (isset($_GET['id'])): ?>
																			<?=get_client_classification_select($s['cli_classification']);?>
																  <?php else: ?>
																			<?=get_client_classification_select(0);?>
																  <?php endif;?>
																	</select>
																</div>
															</div>
													
															<div class="col-md-4">
																<div class="section">
																	<label for="cli_classification_type" >Type de classification:</label>
																	<select id="cli_classification_type" class="select2 select2-classification-type" name="cli_classification_type">
																		<option value="0">Type de classification...</option>																
																	</select>
																</div>
															</div>
															<div class="col-md-4">
																<div class="section">		
																	<label for="cli_classification_categorie" >Catégorie de classification:</label>
																	<select id="cli_classification_categorie" class="select2 select2-classification-categorie" name="cli_classification_categorie">
																		<option value="0">Catégorie de classification...</option>
																	</select>
																</div>
															</div>															
														</div>
														<div class="row">
															<div class="col-md-8">
																<div class="section">
																	<label for="cli_siren" >Siren:</label>
																	<div class="field prepend-icon">
																		<input type="text" name="cli_siren" id="cli_siren" class="gui-input siren" placeholder="Siren" >
																		<span class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="section">
																	<label for="cli_nic" >NIC:</label>
																	<div class="field prepend-icon">
																		<input type="text" name="cli_nic" id="cli_nic" class="gui-input siret" placeholder="NIC" disabled >
																		<span class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Adresses</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-8">
																<div class="section">
																	<label for="adr_type" >Type d'adresse:</label>
																	<div class="field select">
																		<select name="adr_type" id="adr_type" >
																			<option value="0">Type d'adresse...</option>
																			<option value="1" >Lieu d'intervention</option>
																			<option value="2">Facturation</option>
																			<option value="3">Envoi de facture</option>
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
															<div class="col-md-4 pt25">
																<div class="section">
																	<div class="option-group field">
																		<label class="option option-success">
																			<input type="checkbox" name="adr_defaut" value="adr_defaut">
																			<span class="checkbox"></span>Adresse par défaut
																		</label>

																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-4">
																<div class="section">
																	<label for="code_postal" >CP:</label>
																	<div class="field prepend-icon">
																		<input name="adr_cp" id="code_postal" type="text" class="gui-input code-postal" placeholder="Code postal">
																		<span for="adr_cp" class="field-icon">
																			<i class="fa fa-certificate"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-md-8">
																<div class="section">
																	<label for="adr_ville" >Ville:</label>
																	<div class="field prepend-icon">
																		<input type="text" name="adr_ville" id="adr_ville" class="gui-input nom" placeholder="Ville" />
																		<span class="field-icon">
																			<i class="fa fa fa-building"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													
													<div class="form1">
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Contact</span>
																</div>
															</div>
														</div>								
														<div class="row">

															<div class="col-md-6">
																<div class="section">
																	<label for="con_nom" >Nom du contact:</label>
																	<div class="field prepend-icon">
																		<input name="con_nom" id="con_nom" type="text" class="gui-input nom" placeholder="Nom" >
																		<span for="con_nom" class="field-icon">
																			<i class="fa fa-user"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label for="con_prenom" >Prénom du contact:</label>
																	<div class="field prepend-icon">
																		<input name="con_prenom" type="text" class="gui-input prenom" placeholder="Prénom" >
																		<span for="con_prenom" class="field-icon">
																			<i class="fa fa-user"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form2">
														<div class="row" >
															<div class="col-md-6">
																<div class="section">
																	<label for="con_mail" >Mail:</label>
																	<div class="field select">																	
																		<select name="con_mail" id="con_mail" >
																			<option value="0">Mail...</option>
																			<option value="1">Renseigné</option>
																			<option value="2">Non renseigné</option>
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label for="con_fonction" >Fonction du contact:</label>
																	<div class="field select">
																		<select name="con_fonction" id="con_fonction" >
																			<option value="0">Sélectionner une fonction...</option>
																	<?php	if (isset($_GET['id'])): ?>
																				<?=get_contact_fonction_select($s['con_fonction']);?>
																	<?php 	else: ?>
																				<?=get_contact_fonction_select(0);?>
																	<?php 	endif;?>
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
														</div>
														<div class="row" >
															<div class="col-md-6">
																<div class="section">
																	<label for="con_tel" >Téléphone du contact:</label>
																	<div class="field prepend-icon">
																		<input name="con_tel" id="con_tel" type="text" class="gui-input telephone" placeholder="Téléphone" >
																		<span class="field-icon">
																			<i class="fa fa-phone"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Correspondances</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<label for="cor_rappel_deb" >Prospect à rappeler entre :</label>
																	<div class="field prepend-icon">
																		<input type="text" id="cor_rappel_deb" name="cor_rappel_deb" class="gui-input date" placeholder="Client à rappeler entre le">
																		<span class="field-icon">
																			<i class="fa fa-calendar-o"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label for="cor_rappel_fin" >et le :</label>
																	<div class="field prepend-icon">
																		<input type="text" id="cor_rappel_fin" name="cor_rappel_fin" class="gui-input date" placeholder="et le">
																		<span class="field-icon">
																			<i class="fa fa-calendar-o"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>	
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<label for="cli_correspondance" >Correspondances :</label>
																	<div class="field select">
																		<select name="cli_correspondance" id="cli_correspondance" >
																			<option value="0">Correspondances...</option>
																			<option value="1">Avec</option>
																			<option value="2">Sans</option>
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label for="cli_depuis" >Depuis le :</label>
																	<div class="field prepend-icon">
																		<input type="text" id="cli_depuis" name="cli_depuis" class="gui-input date" placeholder="depuis le ...">
																		<span class="field-icon">
																			<i class="fa fa-calendar-o"></i>
																		</span>
																	</div>
																</div>		
															</div>
														</div>
														<div class="row text-center">
															<div class="col-md-6">
																<div class="section">
																	<label class="option" >
																		<input type="radio" name="aff_cor" value="1" >
																		<span class="radio"></span>Afficher date de la dernière correspondance
																	</label>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label class="option" >
																		<input type="radio" name="aff_cor" value="0" checked >
																		<span class="radio"></span>Afficher date du prochain rappel
																	</label>
																</div>		
															</div>
														</div>
														
														
														
														<div class="row champ-client">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Autres infos</span>
																</div>
															</div>
														</div>

														<div class="row champ-client" >
															<div class="col-md-4 pt25">
																<div class="section">
																	<div class="option-group field">
																		<label class="option option-success">
																			<input type="checkbox" name="cli_important" value="checked" >
																			<span class="checkbox"></span>Références
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-4 pt25">
																<div class="section">
																	<div class="option-group field">
																		<label class="option option-success">
																			<input type="checkbox" name="cli_archive" value="cli_archive">
																			<span class="checkbox"></span>
																			Prospects archivés
																		</label>
																	</div>
																</div>
															</div>																						
															<div class="col-md-4 text-left">
																<label for="cli_import" >Prospects issu d'un fichier importé</label>
																<div class="field select">
																	<select name="cli_import" id="cli_import" >
																		<option value="0">Liste des imports...</option>
																<?php	foreach($d_imports as $import){
																			echo("<option value='" . $import["sim_id"] . "' >" . $import["sim_libelle"] . "</option>");
																		} ?>																		
																	</select>
																	<i class="arrow simple"></i>
																</div>
															</div>
								
														</div>	

												<?php	if($_SESSION['acces']['acc_profil']==13){ ?>

															<div class="row" >
																<div class="col-md-4 pt25">
																	<div class="section">
																		<div class="option-group field">
																			<label class="option option-success">
																				<input type="checkbox" name="cli_reseau" value="on">
																				<span class="checkbox"></span>
																				Prospects réseau
																			</label>
																		</div>
																	</div>
																</div>
															</div>
												<?php	} ?>
																
													</div>										
												</div>
										
										</div>
									</div>
								</div>
							</div>
						</div>
				
					</section>
				</section>
			</div>		
			<!-- End: Main -->
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">

					</div>
					<div class="col-xs-6 footer-middle text-center"></div>
					<div class="col-xs-3 footer-right">
			<?php		if($_SESSION["acces"]["acc_droits"][23]){ ?>
							<a href="suspect.php" class="btn btn-success btn-sm" role="button">					
								<span class="fa fa-plus"></span>
								<span class="hidden-xs">Nouvelle fiche</span>
							</a>
			<?php		} ?>

						<button type="submit" name="search" class="btn btn-primary btn-sm">
							<i class='fa fa-search'></i> Rechercher
						</button>

					</div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	

		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery-ui-datepicker.min.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="vendor/plugins/holder/holder.min.js"></script>
		<script src="assets/js/custom.js"></script>
		
		<script type="text/javascript">


			jQuery(document).ready(function (){
				
				
				$(".form1").show();
				$(".form2").hide();

				$("#select2_prospect").change(function(){
					alert("A DEV");
				});

				// gestion du siren / siret
				$( "#cli_siren" ).focusout(function(){
					if($(this).val().length==11){
						$("#cli_nic").prop("disabled",false);
					}else{
						$("#cli_nic").val("");
						$("#cli_nic").prop("disabled",true);
					}
					
				});
				
				
				// Prospect à rappeler
			
				$("#cor_rappel_deb").datepicker({
					defaultDate: "+1w",
					dateFormat: "dd/mm/yy",
					closeText: 'Fermer',
					prevText: 'Précédent',
					nextText: 'Suivant',
					currentText: 'Aujourd\'hui',
					monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
					monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
					dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
					dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
					dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
					weekHeader: 'Sem.',
					numberOfMonths: 3,
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function (input, inst) {
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					onClose: function (selectedDate) {
						$("#cor_rappel_fin").datepicker("option", "minDate", selectedDate);
					}
				});

				$("#cor_rappel_fin").datepicker({
					dateFormat: "dd/mm/yy",
					closeText: 'Fermer',
					prevText: 'Précédent',
					nextText: 'Suivant',
					currentText: 'Aujourd\'hui',
					monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
					monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
					dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
					dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
					dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
					weekHeader: 'Sem.',
					defaultDate: "+1w",
					numberOfMonths: 3,
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function (input, inst) {
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					onClose: function (selectedDate) {
						$("#cor_rappel_deb").datepicker("option", "maxDate", selectedDate);
					}
				});
				
				// date d'enregsitrement des prospects

				$("#cli_creation_date_deb").datepicker({
					defaultDate: "+1w",
					dateFormat: "dd/mm/yy",
					closeText: 'Fermer',
					prevText: 'Précédent',
					nextText: 'Suivant',
					currentText: 'Aujourd\'hui',
					monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
					monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
					dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
					dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
					dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
					weekHeader: 'Sem.',
					numberOfMonths: 3,
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function (input, inst) {
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					onClose: function (selectedDate) {
						$("#cor_rappel_fin").datepicker("option", "minDate", selectedDate);
					}
				});

				$("#cli_creation_date_fin").datepicker({
					dateFormat: "dd/mm/yy",
					closeText: 'Fermer',
					prevText: 'Précédent',
					nextText: 'Suivant',
					currentText: 'Aujourd\'hui',
					monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
					monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
					dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
					dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
					dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
					weekHeader: 'Sem.',
					defaultDate: "+1w",
					numberOfMonths: 3,
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function (input, inst) {
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					onClose: function (selectedDate) {
						$("#cor_rappel_deb").datepicker("option", "maxDate", selectedDate);
					}
				});

				// avec ou sans correspondance depuis
				$("#cli_depuis").datepicker({
					dateFormat: "dd/mm/yy",
					closeText: 'Fermer',
					prevText: 'Précédent',
					nextText: 'Suivant',
					currentText: 'Aujourd\'hui',
					monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
					monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
					dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
					dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
					dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
					weekHeader: 'Sem.',
					defaultDate: "+1w",
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function (input, inst) {
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					}
				});

			});
		
			function sous_categorie(selected_id){
			  $.ajax({
				type:'POST',
				url: 'ajax/ajax_sous_categorie.php',
				//the script to call to get data
				data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
				//for example "id=5&parent=6"

				dataType: 'json',                //data format
				success: function(data)          //on recieve of reply
				{

				  if (data['0'] == undefined){

					$("#cli_sous_categorie").find("option:gt(0)").remove();
					$(".cli_sous_categorie_bloc").hide();
				  }else{
					$(".cli_sous_categorie_bloc").show();
					$("#cli_sous_categorie").find("option:gt(0)").remove();
					$.each(data, function(key, value) {
					  $('#cli_sous_categorie')
					  .append($("<option></option>")
						.attr("value",value["csc_id"])
						.text(value["csc_libelle"]));
					});
				  }
				}
			  });
			}

			function classification(selected_id){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_classification.php',
					data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php				
					dataType: 'json',                //data format
					success: function(data)          //on recieve of reply
					{

						if (data['0'] == undefined){

						  $("#cli_sous_classification").find("option:gt(0)").remove();

					  }else{
						  $.each(data, function(key, value) {
							$('#cli_sous_classification')
							.append($("<option></option>")
							  .attr("value",value["csc_id"])
							  .text(value["csc_libelle"]));
						});
					  }
					}
				});   
			}
			
			function recherche() {
				if ($('.search_advanced').html() == '<i class="fa fa-minus" aria-hidden="true"></i> Recherche simple') {

					$(".form1").show(400);
					$(".form2").hide(400);

					$('.search_advanced').html('<i class="fa fa-plus" aria-hidden="true"></i> Recherche avancée');
				} else {
					$(".form2").show(400);
					$('.search_advanced').html('<i class="fa fa-minus" aria-hidden="true"></i> Recherche simple');
				}

			}
		</script>
	</body>
</html>
