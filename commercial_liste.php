<?php

$menu_actif = "1-2";

include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");;
include_once("includes/connexion_soc.php");


$_SESSION["retour"]="commercial_liste.php";
			
// GESTION DES SOCIETES / AGENCES A AFFICHES

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}

$entites=array();

$sql="SELECT soc_nom,soc_agence,soc_intra_inter FROM societes WHERE soc_id=" . $_SESSION['acces']["acc_societe"];
$req=$Conn->prepare($sql);
$req->execute();
$societe=$req->fetch();

if(!empty($_SESSION['acces']["acc_agence"])){
	
	// on consulte une agence
	
	$sql="SELECT age_nom FROM agences WHERE age_id=" . $_SESSION['acces']["acc_agence"];
	$req=$Conn->query($sql);
	$d_agence=$req->fetch();
	if(!empty($d_agence)){	
		$entites[]=array(
			"societe" => $_SESSION['acces']["acc_societe"],
			"agence" => $_SESSION['acces']["acc_agence"],
			"libelle" => "Agence " . $d_agence["age_nom"]
		);
		
	}
	
}else{
	
	// on consulte une societe
	
	if(empty($societe['soc_agence'])){
		// la societe n'a pas d'agence
		$entites[]=array(
			"societe" => $_SESSION['acces']["acc_societe"],
			"agence" => 0,
			"libelle" => ""
		);
	}else{
		$sql="SELECT age_id,age_nom FROM agences WHERE age_societe=" . $_SESSION['acces']["acc_societe"];
		$req=$Conn->prepare($sql);
		$req->execute();
		$d_agences=$req->fetchAll(); 
		if(!empty($d_agences)){	
			foreach($d_agences as $agence){
				$entites[]=array(
					"societe" => $_SESSION['acces']["acc_societe"],
					"agence" => $agence["age_id"],
					"libelle" => "Agence " . $agence["age_nom"],
				);
			}
		}
	}
}


			
// LES COMMERCIAUX DE CHAQUE ENTITE

foreach($entites as $cle => $entite){
	
	$sql="SELECT * FROM Commerciaux WHERE com_type=1 AND com_agence = " . $entite["agence"] . " AND com_archive = 0";
	if(!$_SESSION['acces']["acc_droits"][35]){
		// profil commercial
		$sql.=" AND com_ref_1=" . $_SESSION['acces']["acc_ref_id"];
	}
	$sql.=" ORDER BY com_label_1,com_label_2;";
	$req=$ConnSoc->query($sql);
	$vendeurs=$req->fetchAll();
	if(!empty($vendeurs)){
		$entites[$cle]["vendeurs"]=$vendeurs;
	}else{
		$entites[$cle]["vendeurs"]="";
	}
	
	if($_SESSION['acces']["acc_droits"][35]){
		
		$sql="SELECT * FROM Commerciaux WHERE com_type=2 AND com_agence = " . $entite["agence"] . " AND com_archive = 0";
		$sql.=" ORDER BY com_label_1,com_label_2;";
		$req=$ConnSoc->query($sql);
		$revendeurs=$req->fetchAll();
		if(!empty($revendeurs)){
			$entites[$cle]["revendeurs"]=$revendeurs;
		}
		
		$sql="SELECT * FROM Commerciaux WHERE com_type=3 AND com_agence = " . $entite["agence"] . " AND com_archive = 0";
		$sql.=" ORDER BY com_label_1,com_label_2;";
		$req=$ConnSoc->query($sql);
		$indivis=$req->fetchAll();
		if(!empty($indivis)){
			$entites[$cle]["indivis"]=$indivis;
		}

		$sql="SELECT * FROM Commerciaux WHERE com_type=0 AND com_agence = " . $entite["agence"] . " AND com_archive = 0";
		$sql.=" ORDER BY com_label_1,com_label_2;";
		$req=$ConnSoc->query($sql);
		$autres=$req->fetchAll();
		if(!empty($indivis)){
			$entites[$cle]["autres"]=$autres;
		}
		
		$sql="SELECT * FROM Commerciaux WHERE com_agence = " . $entite["agence"] . " AND com_archive = 1";
		$sql.=" ORDER BY com_label_1,com_label_2;";
		$req=$ConnSoc->query($sql);
		$archives=$req->fetchAll();
		if(!empty($archives)){
			$entites[$cle]["archives"]=$archives;
		}
		
	}else{
		$entites[$cle]["revendeurs"]="";
		$entites[$cle]["indivis"]="";
		$entites[$cle]["archives"]="";
		$entites[$cle]["autres"]="";
	}
}	

// INFOS DES UTILISATEURS LIES AUX FICHES COMMERCIALES

$d_uti=array();
$sql="SELECT uti_id,uti_societe,uti_agence,uti_profil FROM Utilisateurs ORDER BY uti_id;";
$req=$Conn->query($sql);
$d_result=$req->fetchAll();
if(!empty($d_result)){
	foreach($d_result as $d_r){
		$d_uti[$d_r["uti_id"]]=array(
			"societe" => $d_r["uti_societe"],
			"agence" => $d_r["uti_agence"],
			"profil" => $d_r["uti_profil"]
		);
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
				
	<?php 		
					if(empty($_SESSION['acces']["acc_agence"])){
						// on consulte une societe ?>
						<div class="row">
							<div class="col-md-10">
								<h2>
									Société <?= $societe['soc_nom'] ?>
								</h2>
							</div>
							<div class="col-md-2 text-right">
	<?php						if($_SESSION['acces']["acc_profil"]!=3 OR $_SESSION["acces"]["acc_droits"][35]){ ?>	
									<a href="commercial_result.php" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Objectifs et Résultats">
										<i class="fa fa-line-chart"></i>
									</a>
	<?php						}
								if(($_SESSION['acces']["acc_service"][1]==1 OR $_SESSION['acces']["acc_service"][2]==1 OR $_SESSION['acces']["acc_service"][5]==1) AND $acc_societe!=4){ ?>
								<a href="commercial_result_pm.php?pm_type=1" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTRA">
										<i class="fa fa-truck"></i>
									</a>
									<a href="commercial_result_pm.php?pm_type=2" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTER">
										<i class="fa fa-building-o"></i>
									</a>
				<?php			} ?>
							</div>
						</div>
	<?php			}
					// ON AFFICHE CHAQUE ENTITE
					if(!empty($entites)){

						foreach($entites as $num => $a){
							if(!empty($a["libelle"])){ ?>
								<div class="row">
									<div class="col-md-10">
										<h2><?= $a['libelle']?></h2>
									</div>
					<?php			if($_SESSION['acces']["acc_profil"]!=3 OR $_SESSION["acces"]["acc_droits"][35]){ ?>
										<div class="col-md-2 text-right pt25">
											<a href="commercial_result.php?agence=<?=$a['agence']?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Objectifs et Résultats">
												<i class="fa fa-line-chart"></i>
											</a>
					<?php					if(($_SESSION['acces']["acc_service"][1]==1 OR $_SESSION['acces']["acc_service"][2]==1 OR $_SESSION['acces']["acc_service"][5]==1) AND $acc_societe!=4){ ?>
												<a href="commercial_result_pm.php?agence=<?=$a['agence']?>&pm_type=1" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTRA">
													<i class="fa fa-truck"></i>
												</a>
												<a href="commercial_result_pm.php?agence=<?=$a['agence']?>&pm_type=2" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTER">
													<i class="fa fa-building-o"></i>
												</a>
							<?php			} ?>
										</div>
					<?php			} ?>
								</div>
	<?php					}
							if(!empty($a["vendeurs"])){ ?>
								<div class="row">
									<div class="col-md-12">
										<div class="panel" > 
											<div class="panel-heading panel-head-sm" style="background-color:#7e8082">
												<span class="panel-title" style="color:white;">Commerciaux</span>
											</div>
											<div class="panel-body pn">
												<div class="table-responsive">
													<table class="table table-striped table-hover">
														<tbody>
					<?php									foreach($a["vendeurs"] as $c){
											
											
																$entite_rem=false;
																if(!empty($c['com_ref_1'])){
																	if(isset($d_uti[$c['com_ref_1']])){
																		if($d_uti[$c['com_ref_1']]["societe"]==$a["societe"] AND $d_uti[$c['com_ref_1']]["agence"]==$a["agence"] AND $d_uti[$c['com_ref_1']]["profil"]==3){
																			$entite_rem=true;
																		}
																	}
																}
						
					?>
																<tr>
																	<td style="width:80%;"><?= $c['com_label_1'] . " " . $c['com_label_2']?></td>
																	<td style="width:20%;" class="text-center" >																		
															<?php		if($_SESSION["acces"]["acc_droits"][14]){ ?>
																			<a href="commercial.php?commercial=<?= $c['com_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier">
																				<span class="fa fa-pencil"></span>
																			</a>
															<?php		}	?>
																		<a href="commercial_result.php?commercial=<?= $c['com_id'] ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Objectifs et Résultats">
																			<i class="fa fa-line-chart"></i>
																		</a>
														<?php			if(($_SESSION['acces']["acc_service"][1]==1 OR $_SESSION['acces']["acc_service"][2]==1 OR $_SESSION['acces']["acc_service"][5]==1) AND $acc_societe!=4){ ?>
																			<a href="commercial_result_pm.php?commercial=<?= $c['com_id'] ?>&pm_type=1" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTRA">
																				<i class="fa fa-truck"></i>
																			</a>
																			<a href="commercial_result_pm.php?commercial=<?= $c['com_id'] ?>&pm_type=2" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTER">
																				<i class="fa fa-building-o"></i>
																			</a>
														<?php			}
																		if($entite_rem){
																			if($_SESSION['acces']["acc_profil"]==10 OR $_SESSION['acces']["acc_profil"]==13){ ?>
																				<a href="utilisateur_rem.php?utilisateur=<?= $c['com_ref_1'] ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Rémunération">
																					<i class="fa fa-eur"></i>
																				</a>
														<?php				} 
																		} ?>
																	</td>
																</tr>				
					<?php									} ?>
														</tbody>
													</table>
												</div>
											</div>
							<?php			if($_SESSION["acces"]["acc_droits"][35]){
												// animateur des ventes ?>
												<div class="panel-footer text-right">
													
													<a <?php if(empty($a["agence"])){ ?>href="commercial_result.php?com_type=1"<?php }else{ ?>href="commercial_result.php?com_type=1&agence=<?=$a['agence']?>"<?php }?> class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Objectifs et Résultats">
														<i class="fa fa-line-chart"></i>
													</a>
							<?php					if(($_SESSION['acces']["acc_service"][1]==1 OR $_SESSION['acces']["acc_service"][2]==1 OR $_SESSION['acces']["acc_service"][5]==1) AND $acc_societe!=4){
														// le service com à normalement le droit 35 ?>
														<a href="commercial_result_pm.php?com_type=1&pm_type=1" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTRA">
															<i class="fa fa-truck"></i>
														</a>
														<a href="commercial_result_pm.php?com_type=1&pm_type=2" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTER">
															<i class="fa fa-building-o"></i>
														</a>
									<?php			} ?>
													
												</div>
							<?php			}
											 ?>
										</div>
									</div>
								</div>
					<?php	}
							if(!empty($a["revendeurs"])){ ?>
								<div class="row">
									<div class="col-md-12">
										<div class="panel" > 
											<div class="panel-heading panel-head-sm" style="background-color:#229922">
												<span class="panel-title" style="color:white;">Revendeurs</span>
											</div>
											<div class="panel-body pn">
												<div class="table-responsive">
													<table class="table table-striped table-hover">
														<tbody>
						<?php								foreach($a["revendeurs"] as $c){ ?>
																<tr>
																	<td style="width:80%;"><?= $c['com_label_1'] . " " . $c['com_label_2']?></td>
																	<td style="width:20%;" class="text-center" >
															<?php		if($_SESSION["acces"]["acc_droits"][14]){ ?>
																			<a href="commercial.php?commercial=<?= $c['com_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier">
																				<span class="fa fa-pencil"></span>
																			</a>
															<?php		} ?>
																		<a href="commercial_result.php?commercial=<?= $c['com_id'] ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Objectifs et Résultats">
																			<i class="fa fa-line-chart"></i>
																		</a>
															<?php		if(($_SESSION['acces']["acc_service"][1]==1 OR $_SESSION['acces']["acc_service"][2]==1 OR $_SESSION['acces']["acc_service"][5]==1) AND $acc_societe!=4){ ?>
																			<a href="commercial_result_pm.php?commercial=<?= $c['com_id'] ?>&pm_type=1" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTRA">
																				<i class="fa fa-truck"></i>
																			</a>
																			<a href="commercial_result_pm.php?commercial=<?= $c['com_id'] ?>&pm_type=2" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTER">
																				<i class="fa fa-building-o"></i>
																			</a>
														<?php			} ?>
																	</td>
																</tr>
						<?php								} ?>
														</tbody>
													</table>
												</div>
											</div>
											<div class="panel-footer text-right">
												<a <?php if(empty($a["agence"])){ ?>href="commercial_result.php?com_type=2"<?php }else{ ?>href="commercial_result.php?com_type=2&agence=<?=$a['agence']?>"<?php }?> class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Objectifs et Résultats">
													<i class="fa fa-line-chart"></i>
												</a>
						<?php					if(($_SESSION['acces']["acc_service"][1]==1 OR $_SESSION['acces']["acc_service"][2]==1 OR $_SESSION['acces']["acc_service"][5]==1) AND $acc_societe!=4){ ?>
													<a href="commercial_result_pm.php?com_type=2&pm_type=1" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTRA">
														<i class="fa fa-truck"></i>
													</a>
													<a href="commercial_result_pm.php?com_type=2&pm_type=2" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTER">
														<i class="fa fa-building-o"></i>
													</a>
								<?php			} ?>
											</div>
										</div>
									</div>
								</div>
					<?php	}
							if(!empty($a["indivis"])){ ?>
								<div class="row">
									<div class="col-md-12">							
										<div class="panel" > 
											<div class="panel-heading panel-head-sm" style="background-color:#f8B481">
												<span class="panel-title" style="color:white;">Agence</span>
											</div>
											<div class="panel-body pn">
												<div class="table-responsive">
													<table class="table table-striped table-hover">
														<tbody>
						<?php								foreach($a["indivis"] as $c){ 
																$entite_rem=false;
																if(!empty($c['com_ref_1'])){
																	if(isset($d_uti[$c['com_ref_1']])){
																		if($d_uti[$c['com_ref_1']]["societe"]==$a["societe"] AND $d_uti[$c['com_ref_1']]["agence"]==$a["agence"] AND $d_uti[$c['com_ref_1']]["profil"]==3){
																			$entite_rem=true;
																		}
																	}
																} ?>
																<tr>
																	<td style="width:80%;"><?= $c['com_label_2'] . " " . $c['com_label_1']?></td>
																	<td style="width:20%;" class="text-center" >
															<?php		if($_SESSION["acces"]["acc_droits"][14]){ ?>
																			<a href="commercial.php?commercial=<?= $c['com_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier">
																				<span class="fa fa-pencil"></span>
																			</a>
															<?php		} ?>
																		<a href="commercial_result.php?commercial=<?= $c['com_id'] ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Objectifs et Résultats">
																			<i class="fa fa-line-chart"></i>
																		</a>
															<?php		if(($_SESSION['acces']["acc_service"][1]==1 OR $_SESSION['acces']["acc_service"][2]==1 OR $_SESSION['acces']["acc_service"][5]==1) AND $acc_societe!=4){ ?>
																			<a href="commercial_result_pm.php?commercial=<?= $c['com_id'] ?>&pm_type=1" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTRA">
																				<i class="fa fa-truck"></i>
																			</a>
																			<a href="commercial_result_pm.php?commercial=<?= $c['com_id'] ?>&pm_type=2" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTER">
																				<i class="fa fa-building-o"></i>
																			</a>
														<?php			} ?>
																	</td>
																</tr>				
						<?php								} ?>
														</tbody>
													</table>
												</div>
											</div>
											<div class="panel-footer text-right">
												<a <?php if(empty($a["agence"])){ ?>href="commercial_result.php?com_type=3"<?php }else{ ?>href="commercial_result.php?com_type=3&agence=<?=$a['agence']?>"<?php }?> class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Objectifs et Résultats">
													<i class="fa fa-line-chart"></i>
												</a>
									<?php		if(($_SESSION['acces']["acc_service"][1]==1 OR $_SESSION['acces']["acc_service"][2]==1 OR $_SESSION['acces']["acc_service"][5]==1) AND $acc_societe!=4){ ?>
													<a href="commercial_result_pm.php?com_type=3&pm_type=1" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTRA">
														<i class="fa fa-truck"></i>
													</a>
													<a href="commercial_result_pm.php?com_type=3&pm_type=2" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTER">
														<i class="fa fa-building-o"></i>
													</a>
								<?php			} ?>
											</div>
										</div>
									</div>
								</div>
					<?php	}
							if(!empty($a["autres"])){ ?>
								<div class="admin-panels">
									<div class="row">
										<div class="col-md-12 admin-grid">									
											<div class="panel" > 
												<div class="panel-heading panel-head-sm" >
														<span class="panel-title">Autres</span>
												</div>
												<div class="panel-body pn">
													<div class="table-responsive">
														<table class="table table-striped table-hover">
															<tbody>
							<?php								foreach($a["autres"] as $c){ ?>
																	<tr>
																		<td><?= $c['com_label_2'] . " " . $c['com_label_1']?></td>
																		<td style="width:20%;" class="text-center" >
																<?php		if($_SESSION["acces"]["acc_droits"][14]){ ?>
																				<a href="commercial.php?commercial=<?= $c['com_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier">
																					<span class="fa fa-pencil"></span>
																				</a>
																<?php		} ?>
																			<a href="commercial_result.php?commercial=<?= $c['com_id'] ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Objectifs et Résultats">
																				<i class="fa fa-line-chart"></i>
																			</a>
																<?php		if(($_SESSION['acces']["acc_service"][1]==1 OR $_SESSION['acces']["acc_service"][2]==1 OR $_SESSION['acces']["acc_service"][5]==1) AND $acc_societe!=4){ ?>
																				<a href="commercial_result_pm.php?com_type=3&pm_type=1" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTRA">
																					<i class="fa fa-truck"></i>
																				</a>
																				<a href="commercial_result_pm.php?com_type=3&pm_type=2" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="PM INTER">
																					<i class="fa fa-building-o"></i>
																				</a>
															<?php			} ?>
																		</td>	
																	</tr>	
																			
							<?php								} ?>
															</tbody>
														</table>
													</div>
												</div>
												
											</div>
										</div>								
									</div>
								</div>
					<?php 	}
							if(!empty($a["archives"])){ ?>
								<div class="admin-panels">
									<div class="row">
										<div class="col-md-12 admin-grid">									
											<div class="panel" > 
												<div class="panel-heading panel-head-sm" >
													<span class="panel-title">Commerciaux archivés</span>
													<span class="pull-right" >
														<button class="btn btn-default btn-sm aff_archive" data-archive="archive_<?=$a['agence']?>" >
															<i class="fa fa-plus" id="btn_archive_<?=$a['agence']?>" ></i>
														</button>
													</span>
												</div>
												<div class="panel-body pn" style="display:none;" id="archive_<?=$a['agence']?>" >
													<div class="table-responsive">
														<table class="table table-striped table-hover">
															<tbody>
							<?php								foreach($a["archives"] as $c){ ?>
																	<tr>
																		<td><?= $c['com_label_1'] . " " . $c['com_label_2']?></td>
																		<td style="width:20%;" class="text-center" >
																<?php		if($_SESSION["acces"]["acc_droits"][14]){ ?>
																				<a href="commercial.php?commercial=<?= $c['com_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier">
																					<span class="fa fa-pencil"></span>
																				</a>
																<?php		} ?>
																			<a href="commercial_result.php?commercial=<?= $c['com_id'] ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Objectifs et Résultats">
																				<i class="fa fa-line-chart"></i>
																			</a>
																		</td>	
																	</tr>	
																			
							<?php								} ?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>								
									</div>
								</div>
					<?php 	}
					
						}
					} ?>	

					
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
				</div>
				<div class="col-xs-6 footer-middle text-center" >
		<?php		if($_SESSION["acces"]["acc_profil"]==13){ ?>				
						<a href="import/sync_commerciaux.php" class="btn btn-default btn-sm" role="button" >
							<span class="fa fa-refresh"></span>
							Import local
						</a>
						<a href="import/sync_commerciaux.php?srv=1" class="btn btn-default btn-sm" role="button" >
							<span class="fa fa-refresh"></span>
							Import srv
						</a>
		<?php		} ?>		
				</div>
				<div class="col-xs-3 footer-right" >
		<?php		if($_SESSION["acces"]["acc_droits"][14]){ ?>				
						<a href="commercial.php" class="btn btn-success btn-sm" role="button" >
							<span class="fa fa-plus"></span>
							Ajouter un commercial
						</a>
		<?php		} ?>
				</div>
			</div>
		</footer>
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript" >
			jQuery(document).ready(function(){
				$(".aff_archive").click(function(){
					$bloc_archive=$(this).data("archive");
					if($("#" + $bloc_archive).length>0){
						if($("#btn_" + $bloc_archive).prop("class")=="fa fa-minus"){
							// on masque
							$("#" + $bloc_archive).hide();
							$("#btn_" + $bloc_archive).prop("class","fa fa-plus")
						}else{
							$("#" + $bloc_archive).show();
							$("#btn_" + $bloc_archive).prop("class","fa fa-minus")
						}
					}
				});
			})
		</script>
		
	</body>
</html>
