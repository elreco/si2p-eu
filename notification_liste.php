<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	
	// LISTE DES NOTIFICATIONS D'UN UTILISATEUR

	$utilisateur=$_SESSION['acces']["acc_ref_id"];

	$sql="SELECT * FROM Notifications WHERE not_utilisateur=:not_utilisateur AND not_societe = :not_societe AND not_agence = :not_agence ORDER BY not_id DESC LIMIT 200;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":not_utilisateur",$utilisateur);
	$req->bindParam(":not_societe",$_SESSION['acces']["acc_societe"]);
	$req->bindParam(":not_agence",$_SESSION['acces']["acc_agence"]);
	$req->execute();
	$notifs=$req->fetchAll();

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->		
	</head>
	
	<body class="sb-top sb-top-sm" >
		<form method="post" action="notification_supp.php" >
			<!-- Start: Main -->
			<div id="main">
				<?php	
					include "includes/header_def.inc.php";
				?>	
					
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper">		 
					<!-- Begin: Content -->
								
					<section id="content" class="animated fadeIn">	
					
				<?php	if(!empty($notif)){ ?>
				
							<div class="panel">
								<div class="panel-heading">
									<span class="panel-title">Notifications</span>
								</div>
								<div class="panel-body">
									<div class="admin-form" >
									
										<table class="table table-hover table-striped" >
											<thead>
												<th>
													<label class="option">
														<input type="checkbox" name="notif" id="notif_all" value="" >
														<span class="checkbox"></span>
													</label>	
												</th>
												<th colspan="4" >&nbsp;</th>
											<tbody>
									<?php		$nb=0;
												foreach($notifs as $n){
													$nb++; ?>
													<tr>
														<td>
															<label class="option">
																<input type="checkbox" class="notif" name="notif_<?=$nb?>" value="<?=$n["not_id"]?>" >
																<span class="checkbox"></span>
															</label>
														</td>
														<td>
															<div class="badge <?=$n["not_classe"]?>">
																<?=$n["not_icone"]?>
															</div>
														</td>
														<td>
									<?php					if(!$n["not_vu"]){
																echo("<b>" . $n["not_texte"] . "</b>");
															}else{
																echo($n["not_texte"]);
															}; ?>
														</td>
														<td>
															<a href="<?=$n["not_url"]?>&notification=<?=$n["not_id"]?>" class="btn btn-info btn-sm" role="button">
																<span class="fa fa-eye"></span>
															</a>
														</td>
														<td>
															<a href="notification_supp.php?notification=<?=$n["not_id"]?>" class="btn btn-danger btn-sm" role="button">
																<span class="fa fa-times"></span>
															</a>
														</td>
													</tr>
									<?php		}  ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						
							<div>
								<input type="hidden" name="nb" value="<?=$nb?>" />
							</div>
							
				<?php	}else{ ?>
							<p class="alert alert-warning text-center" >
								Vous n'avez pas de notification.
							</p>
				<?php	} ?>
						
		
					</section>
					<!-- End: Content -->
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" ></div>
					<div class="col-xs-6 footer-middle" ></div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Supprimer la sélection" >
							<i class="fa fa-times"></i>							
						</button>
					</div>
				</div>
			</footer>
		</form>
		<?php
		include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" >
			jQuery(document).ready(function(){	
				$("#notif_all").click(function(){	
					console.log("SELECT ALL");
					if($(this).is(":checked")){
						$(".notif").prop("checked",true);
					}else{
						$(".notif").prop("checked",false);
					}
				});
			});
		</script>
	</body>
</html>
