<?php

include "includes/controle_acces.inc.php";

include('includes/connexion.php');


// ENREGISTREMENT D'UNE INFO CLIENT
// ATENTION système marche si soc_client = acc_societe

	$erreur=0;
	
	$client=0;
	if(!empty($_POST["client"])){
		$client=intval($_POST["client"]);
	}

	if($client>0){
		
		// TRAITEMENT DU FORM
		
		$info=0;
		if(!empty($_POST["info"])){
			$info=intval($_POST["info"]);
		}
		
		$inf_type=0;
		if(!empty($_POST["inf_type"])){
			$inf_type=intval($_POST["inf_type"]);
		}
		
		$inf_famille=0;
		if(!empty($_POST["inf_famille"])){
			$inf_famille=intval($_POST["inf_famille"]);
		}
		
		$inf_sous_famille=0;
		if(!empty($_POST["inf_sous_famille"])){
			$inf_sous_famille=intval($_POST["inf_sous_famille"]);
		}

		$inf_sous_sous_famille=0;
		if(!empty($_POST["inf_sous_sous_famille"])){
			$inf_sous_sous_famille=intval($_POST["inf_sous_sous_famille"]);
		}
		
		$inf_description=$_POST["inf_description"];
		
		
		
		if($info==0){
			
			// ajout
			$sql="INSERT INTO infos (inf_type,inf_description,inf_famille,inf_sous_famille,inf_sous_sous_famille,inf_auteur,inf_date,inf_client)
			VALUES (:inf_type,:inf_description,:inf_famille,:inf_sous_famille,:inf_sous_sous_famille,:inf_auteur,NOW(),:inf_client);";
			$req=$Conn->prepare($sql);
			$req->bindParam(":inf_type",$inf_type);
			$req->bindParam(":inf_description",$inf_description);
			$req->bindParam(":inf_famille",$inf_famille);
			$req->bindParam(":inf_sous_famille",$inf_sous_famille);
			$req->bindParam(":inf_sous_sous_famille",$inf_sous_sous_famille);
			$req->bindParam(":inf_auteur",$_SESSION['acces']["acc_ref_id"]);
			$req->bindParam(":inf_client",$client);	
			try {
				$req->execute();
			}catch( PDOException $Exception ){
				$erreur=1;
				$erreur_txt=$Exception->getMessage();
			}
			
		}else{	
			// Modif
			$sql="UPDATE infos SET 
			inf_type=:inf_type,
			inf_description=:inf_description,
			inf_famille=:inf_famille,
			inf_sous_famille=:inf_sous_famille,
			inf_sous_sous_famille=:inf_sous_sous_famille,
			inf_auteur=:inf_auteur,
			inf_date=NOW()
			WHERE inf_id=:info;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":inf_type",$inf_type);
			$req->bindParam(":inf_description",$inf_description);
			$req->bindParam(":inf_famille",$inf_famille);
			$req->bindParam(":inf_sous_famille",$inf_sous_famille);
			$req->bindParam(":inf_sous_sous_famille",$inf_sous_sous_famille);
			$req->bindParam(":inf_auteur",$_SESSION['acces']["acc_ref_id"]);
			$req->bindParam(":info",$info);
			try {
				$req->execute();
			}catch( PDOException $Exception ){
				$erreur=1;
				$erreur_txt="MODIF " . $Exception->getMessage();
			}
		}
	}else{
		$erreur=1;
		$erreur_txt="Données insufissantes";
	}
	
		
	$retour="";
	if($erreur==0){
		$_SESSION['message'][] = array(
			"titre" => "Succès",
			"type" => "success",
			"message" => "L'info a bien été ajoutée"
		);
		$retour="client_voir.php?tab6&client=" . $client;
	}elseif($client>0){
		$retour="client_voir.php?tab6&client=" . $client . "&erreur=" . $erreur_txt;
	}else{
		$retour="client_tri.php?erreur=" . $erreur_txt;	
	}
	header("location :" . $retour);
?>