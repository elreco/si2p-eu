<?php
////////////////// MENU ACTIF HEADER ///////////////////
$menu_actif = "3-3";
////////////////// INCLUDES ///////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_utilisateur.php');
// session retour

$_SESSION['retour'] = "conges_liste.php";
// INTERVENANT
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
// CONGES POUR L'UTILISATEUR
/* if($_SESSION['acces']['acc_profil'] == 7 OR $_SESSION['acces']['acc_profil'] == 12){ */
    $req = $Conn->prepare("SELECT * FROM conges_categories");
/* }else{
    if(!empty($_SESSION['acces']['acc_droits'])){
        foreach($_SESSION['acces']['acc_droits'] as $k=>$d){
            if($d){
                $list_droits[] = $k;
            }
        }
        $query_droits = implode(',', $list_droits);
    }else{
        $query_droits = 0;
    }
    $req = $Conn->prepare("SELECT * FROM conges_categories WHERE cca_droit = 0 OR cca_droit IN(" . $query_droits . ")");
} */
$req->execute();
$conges_categories = $req->fetchAll();
// TOUS LES CONGES
$req = $Conn->prepare("SELECT * FROM conges_categories");
$req->execute();
$conges_categories_special = $req->fetchAll();
foreach($conges_categories_special as $c){
    $ccategories[$c['cca_id']] = $c['cca_libelle'];
}
$req = $Conn->prepare("SELECT uti_nom, uti_prenom, uti_matricule FROM utilisateurs WHERE uti_id=" . $_SESSION['acces']["acc_ref_id"] . ";");
$req->execute();
$utilisateur = $req->fetch();
if($_SESSION['acces']['acc_profil'] == 7 OR $_SESSION['acces']['acc_profil'] == 12 OR $_SESSION['acces']['acc_profil'] == 10 OR $_SESSION['acces']['acc_profil'] == 15  OR $_SESSION['acces']['acc_profil'] == 13){
    $req = $Conn->prepare("SELECT uti_id,uti_nom, uti_prenom, uti_matricule FROM utilisateurs WHERE uti_archive = 0   AND uti_societe = " . $acc_societe . " AND uti_agence = " . $acc_agence);
}else{
	if(!empty($acc_agence)){
		$req = $Conn->prepare("SELECT uti_id,uti_nom, uti_prenom, uti_matricule FROM utilisateurs WHERE uti_archive = 0 AND (uti_responsable=" . $_SESSION['acces']["acc_ref_id"] . " OR  uti_id = ". $_SESSION['acces']["acc_ref_id"] . " ) AND uti_societe = " . $acc_societe . " AND uti_agence = " . $acc_agence );
	}else{
		$req = $Conn->prepare("SELECT uti_id,uti_nom, uti_prenom, uti_matricule FROM utilisateurs WHERE uti_archive = 0 AND (uti_responsable=" . $_SESSION['acces']["acc_ref_id"] . " OR  uti_id = ". $_SESSION['acces']["acc_ref_id"] . " ) AND uti_societe = " . $acc_societe);
	}

}
$req->execute();
$utilisateurs_resp = $req->fetchAll();

 // EVAC
 if(!empty($utilisateurs_resp)){
    // je suis reponsable d'au moins une personne
    foreach($utilisateurs_resp as $u){
        $list_utis[] = $u['uti_id'];
    }
    $query_ids = implode(',', $list_utis);
}else{
    $query_ids = $_SESSION['acces']["acc_ref_id"];
}
$req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id IN(" . $query_ids . ")");
$req->execute();
$utilisateurs = $req->fetchAll();
////////////////////FIN TRAITEMENTS PHP//////////////////////
// RECHERCHE
if(!empty($_POST)){
    $mil="";
    if(!empty($_POST['con_utilisateur'])){
        $req = $Conn->prepare("SELECT uti_nom,uti_prenom FROM utilisateurs WHERE uti_id = " . $_POST['con_utilisateur']);
	    $req->execute();
	    $uti_search = $req->fetch();
        $mil.=" AND con_utilisateur = " . $_POST['con_utilisateur'];
    }else{
        $mil.=" AND con_utilisateur IN(" . $query_ids . ")";
    }
    if(!empty($_POST['con_categorie'])){
        $mil.=" AND con_categorie = " . $_POST['con_categorie'];
    }
    if(!empty($_POST['date_deb'])){
		$date_deb = convert_date_sql($_POST['date_deb']);
		$mil.=" AND con_date_deb >= '" . $date_deb . "'";
	}

	if(!empty($_POST['date_fin'])){
		$date_fin = convert_date_sql($_POST['date_fin']);
		$mil.=" AND con_date_deb <= '" . $date_fin . "'";
	}


    if(!empty($_POST['con_uti_archive'])){
        $mil.=" AND uti_archive = 1";
    }

    $mil.=" AND con_societe = " . $acc_societe . " AND con_agence = " . $acc_agence;
	// CONSTRUCTION REQUETE
    $sql="SELECT * FROM conges";
    if(!empty($_POST['con_uti_archive'])){
        $sql.= " LEFT JOIN utilisateurs ON (utilisateurs.uti_id = conges.con_utilisateur)";
    }
	if($mil!=""){
      $sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
    }
    $sql .= " ORDER BY con_date_deb DESC;";

    $req=$Conn->query($sql);
	$conges=$req->fetchAll();
}else{
    // liste des congés
	// $date_deb_def = date('Y-m-d', strtotime('-2 years'));
	// $date_deb_def_aff = date('d/m/Y', strtotime('-2 years'));
    $req = $Conn->prepare("SELECT * FROM conges WHERE con_societe = " . $acc_societe . " AND con_agence = " . $acc_agence . " AND con_utilisateur IN(" . $query_ids . ")
     ORDER BY con_date_deb DESC LIMIT 100;");
	$req->execute();
	$conges = $req->fetchAll();
}
?>
<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>SI2P - Orion - Congés</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->


<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<style type="text/css">
.panel-tabs > li > a {
    color: #AAA;
    font-size: 14px;
    letter-spacing: 0.2px;
    line-height: 30px;
    /*padding: 9px 20px 11px;*/
    border-radius: 0;
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;
}
.nav2 > li > a {
    position: relative;
    display: block;
    /* padding: 10px 15px; */
}
</style>
<body class="sb-top sb-top-sm">


		<!-- Start: Main -->
		<div id="main">

			<?php include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

<!-- MESSAGES SUCCES -->

				<section id="content" class="animated fadeIn pr20 pl20">
				<h1>Congés</h1>
					<div class="row">
						<div class="col-md-12">
							<div class="panel mb10">
	                            <div class="panel-heading panel-head-sm">
	                                <span class="panel-icon">
	                                    <i class="fa fa-cogs" aria-hidden="true"></i>
	                                </span>
	                                <span class="panel-title"> Filtrer</span>
	                            </div>
	                            <div class="panel-body p20">
	                                <div class="row admin-form theme-primary">
	                                	<form action="conges_liste.php" method="POST">
                                        <div class="row">
                                            <div class="col-md-3">
                                            <label for="com_donneur_ordre" >Utilisateur :</label>
                                                <select name="con_utilisateur" class="select2">

                                                    <option value="0" selected="selected">Utilisateur...</option>
                                                    <?php foreach($utilisateurs as $u){ ?>
                                                        <option
                                                        <?php if(!empty($_POST['con_utilisateur']) && $_POST['con_utilisateur'] == $u['uti_id']){ ?>
                                                            selected
                                                        <?php }?>
                                                        value="<?= $u['uti_id'] ?>" ><?= $u['uti_prenom'] ?> <?= $u['uti_nom'] ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                            <label for="com_donneur_ordre" >Catégorie :</label>
                                                <select name="con_categorie" class="select2">
                                                    <option value="0" selected="selected">Catégorie...</option>
                                                    <?php foreach($conges_categories as $cca){ ?>
                                                        <option
                                                        <?php
                                                            if(!empty($_POST['con_categorie'])){
                                                                if($cca['cca_id'] == $_POST['con_categorie']){
                                                                    echo"selected";
                                                                }
                                                            }
                                                        ?>
                                                        value="<?= $cca['cca_id'] ?>"><?= $cca['cca_libelle'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                            <label for="date_deb">Période de :</label>
                                                <span class="field prepend-icon">
                                                    <input type="text" class="gui-input datepicker date" id="date_deb" name="date_deb"  placeholder="Selectionner une date"

                                                    <?php if(!empty($_POST['date_deb'])){ ?>
                                                        value="<?= $_POST['date_deb'] ?>"
                                                    <?php } ?>
                                                    >
                                                    <label class="field-icon">
                                                        <i class="fa fa-calendar-o"></i>
                                                    </label>
                                                </span>

                                            </div>
                                            <div class="col-md-2">
                                                <label for="date_fin">Jusqu'au :</label>
                                                <span class="field prepend-icon">
                                                    <input type="text" class="gui-input datepicker date" id="date_fin" name="date_fin"  placeholder="Selectionner une date"
                                                    <?php if(!empty($_POST['date_fin'])){ ?>
                                                        value="<?= $_POST['date_fin'] ?>"
                                                    <?php }?>
                                                    >
                                                    <label class="field-icon">
                                                        <i class="fa fa-calendar-o"></i>
                                                    </label>
                                                </span>
                                            </div>
                                            <div class="col-md-2 pt25">
                                                <button type="button" id="rh" class="btn btn-xs btn-primary">
                                                    Période RH
                                                </button>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-md-6">
                                                    <label class="option option-dark mt20">
                                                        <input type="checkbox" name="con_uti_archive" id="con_uti_archive" value="oui"
                                                        <?php
                                                            if(!empty($_POST['con_uti_archive'])){
                                                                    echo"checked";
                                                            }
                                                        ?>
                                                        >
                                                        <span class="checkbox"></span>
                                                        <label for="fou_blacklist">Afficher les utilisateurs archivés</label>
                                                    </label>
                                            </div>
                                            <div class="col-md-offset-4 col-md-1 pt15">
                                                <button type="submit" data-toggle="tooltip" data-placement="bottom" title="Filtrer" class="btn btn-primary">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                            <?php if(!empty($_POST)){ ?>
                                                <div class="col-md-1 pt15 text-left">
                                                    <a href="conges_liste.php" data-toggle="tooltip" data-placement="bottom" title="Annuler le filtre" class="btn btn-default">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            <?php }?>
                                        </div>


										</form>
                                    </div>


	                                </div>

	                            </div>

	                        </div>
						</div>
						<div class="row">


							<div class="col-md-12">

								<table class="table table-striped table-hover outprint" data-tri_col="4" data-tri_sens="desc" id="table_id" style="border:1px solid #e2e2e2;">
			                        <thead>
			                            <tr class="dark">
			                            	<th class="text-center">Matricule</th>
			                                <th class="text-center">Demandeur</th>
			                                <th class="text-center">Validation</th>
			                                <th class="text-center">Type</th>
			                                <th class="text-center">Période</th>
			                                <th class="text-center">NB Jour(s)</th>
			                                <th class="text-center">Responsable</th>
			                                <th class="text-center">Validation</th>
			                                <th class="text-center">Annulation</th>
											<?php if($_SESSION["acces"]["acc_profil"] == 7 OR $_SESSION["acces"]["acc_profil"] == 12 OR $_SESSION["acces"]["acc_profil"] == 13){ ?>
												<th class="text-center no-sort"><input type="checkbox" class="form-check-input selectall"> Intégration</th>
											<?php } ?>
											<th class="text-center no-sort">Actions</th>
			                            </tr>
			                        </thead>
			                        <tbody class="text-center">
				                       <?php
									   $i=1;
									   foreach($conges as $c){

										   $i++;
				                       	?>

				                       		<tr>
				                       			<td><?= $c['con_matricule'] ?></td>
				                       			<td><?= $c['con_prenom'] ?> <?= $c['con_nom'] ?></td>
				                       			<td>
                                                   <?php if($c['con_uti_valide'] == 2){ ?>
                                                    <strong class="text-danger">Refusé</span>
                                                   <?php } else { ?>
                                                   <?= convert_date_txt($c['con_uti_valide_date']) ?>
                                                   <?php } ?>
                                                </td>
				                       			<td><?= $ccategories[$c['con_categorie']] ?></td>
                                                <td data-order="<?= $c['con_date_deb'] ?>">
                                                <?php if(!empty($c['con_date_fin'])){ ?>
                                                    Du <?= convert_date_txt($c['con_date_deb']) ?> au <?= convert_date_txt($c['con_date_fin']) ?>
                                                <?php }else{ ?>
                                                    Le <?= convert_date_txt($c['con_date_deb']) ?>
                                                <?php } ?>
                                                </td>
				                       			<td><?= number_format($c['con_nb_jour'], 1, ',', ' ')?></td>
				                       			<td><?= $c['con_resp_prenom'] ?> <?= $c['con_resp_nom'] ?></td>
				                       			<td>
                                                   <?php if($c['con_resp_valide'] == 2){ ?>
                                                    <strong class="text-danger">Refusé</span>
                                                   <?php } else { ?>
                                                   <?= convert_date_txt($c['con_resp_valide_date']) ?>
                                                   <?php } ?>
                                                </td>
				                       			<td>
                                                   <?php if(!empty($c['con_annul'])){ ?>
                                                    Annulée le <strong><?= convert_date_txt($c['con_annul_date']) ?></strong>
                                                   <?php }?>
			                       				</td>
												<?php		if($_SESSION["acces"]["acc_profil"] == 7 OR $_SESSION["acces"]["acc_profil"] == 12 OR $_SESSION["acces"]["acc_profil"] == 13){ ?>
													<td>
														<?php		if($c['con_annul'] == 0 && $c['con_uti_valide'] != 2 && $c['con_resp_valide'] != 2){ ?>
															<input type="checkbox" class="form-check-input check-integre" data-conge="<?= $c['con_id'] ?>" value="<?= $i ?>"
															<?php if(!empty($c['con_integre'])){ ?>
																checked
															<?php } ?>
															>
														<?php } ?>
	                                                </td>
												<?php } ?>
                                                <td>
                                                    <a href="conges_voir.php?conge=<?=$c["con_id"]?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="Voir" >
														<i class="fa fa-eye"></i>
                                                    </a>
                                                </td>

				                       		</tr>

				                       <?php }?>

			                        </tbody>
	                    		</table>

							</div>
						</div>
					</div>





				</section>
			</section>

		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" style="padding-top:0;padding-bottom:0;">
			<div class="row">
				<div class="col-xs-4 footer-left pt7" >
					<!-- <a href="parametre.php" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-left"></i> Retour</a> -->
				</div>
				<div class="col-xs-4 footer-middle pt7"></div>
				<div class="col-xs-4 footer-right pt7">
		        	<a href="conge.php" class="btn btn-success btn-sm">
						<i class='fa fa-plus'></i> Nouvelle demande
					</a>
				</div>
			</div>
		</footer>

</form>



<?php
include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="vendor/plugins/jqueryprint.js"></script>
<!-- plugin pour les masques formulaires -->
<script src="assets/js/custom.js"></script>

<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script src="vendor/plugins/summernote/summernote.min.js"></script>
<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

<script src="vendor/plugins/jquery.numberformat.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<style type="text/css">
	.tooltip-inner {
    white-space:pre-wrap;
}
</style>
<script>
// DOCUMENT READY //a
jQuery(document).ready(function () {
	tri_col=$("#table_id").data("tri_col");
	tri_sens=$("#table_id").data("tri_sens");
// initilisation plugin datatables
$('#table_id').DataTable({
    "language": {
      "url": "vendor/plugins/DataTables/media/js/French.json"
    },
	"order":[tri_col,tri_sens],
    "paging": false,
    "searching": false,
    "info": false,
    "columnDefs": [ {
          "targets": 'no-sort',
          "orderable": false,
  	}]
});

$(".selectall").change(function(){
	$(".check-integre").prop('checked', $(this).prop("checked"));
});

checkbox_shift($(".check-integre"));

// quand on veut imprimer
// edition d'une ligne
$("#rh").click(function () {
    $.ajax({
        url: "ajax/ajax_periode_rh.php",
        type: "POST",
        dataType: 'json',
        success: function(data) {
            $("#date_deb").val(data['date_debut']);
            $("#date_fin").val(data['date_fin']);
            console.log(data);
        },
        error: function (data){
            modal_alerte_user(data.responseText);
        }
    });

});

$(".check-integre").change(function () {
	valide = $(this).prop("checked");
    $.ajax({
        url: "ajax/ajax_set_integre.php?con=" + $(this).data("conge") + "&valide=" + valide,
        type: "GET",
        dataType: 'json',
        success: function(data) {
        },
        error: function (data){
            modal_alerte_user(data.responseText);
        }
    });

});
////////////// FIN INTERRACTIONS UTILISATEUR /////////////////
});

///////////// FONCTIONS //////////////

//////////// FIN FONCTIONS //////////


</script>

</body>
</html>
