<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_get_juridique.php');




// VISUALISATION DES AVIS DE STAGES

$erreur_txt="";

$action_id=0;
if(!empty($_GET["action"])){
	$action_id=intval($_GET["action"]);
}

$action_client_id=0;
if(!empty($_GET["action_client"])){
	$action_client_id=intval($_GET["action_client"]);
}

if(empty($action_id)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();

}else{

	// personne connecte
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	if($acc_societe!=$conn_soc_id){
		$acc_societe=$conn_soc_id;

	}

	// SUR L'ACTION

	$sql="SELECT act_agence FROM Actions WHERE act_id=:action;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action",$action_id);
	$req->execute();
	$d_action=$req->fetch();

	// LES UTILISATEURS DE LA SOCIETE

	$d_utilisateurs=array();

	$sql="SELECT uti_id,uti_prenom,uti_nom FROM Utilisateurs";
	$req=$Conn->prepare($sql);
	$req->bindParam(":societe",$acc_societe);
	$req->execute();
	$d_u=$req->fetchAll();
	if(!empty($d_u)){
		foreach($d_u as $u){
			$d_utilisateurs[$u["uti_id"]]=$u["uti_prenom"] . " " . $u["uti_nom"];
		}
	}



	// LES AVIS DES STAGIAIRES

	$sql="SELECT sav_avis,sav_utilisateur,sav_sta_nom,sav_sta_prenom,sav_pt_fort,sav_pt_faible,sav_stagiaire
	,acl_pro_reference,acl_pro_libelle
	FROM Stagiaires_Avis,Actions_Clients WHERE sav_action_client=acl_id
	AND sav_action=:action";
	if(!empty($action_client_id)){
		$sql.=" AND sav_action_client=:action_client";
	}
	$sql.=" ORDER BY sav_utilisateur,sav_sta_nom,sav_sta_prenom;";
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action",$action_id);
	if(!empty($action_client_id)){
		$req->bindParam(":action_client",$action_client_id);
	}
	$req->execute();
	$d_avis=$req->fetchAll();
	if(!empty($d_avis)){
		// les avis dependent du lieu d'intervention -> donc identique sur une meme action

		// LES QUESTIONS POSEES

		$d_questions=array();
		$sql="SELECT aqu_id,aqu_texte FROM Avis_Questions,Avis_Questionner WHERE aqu_id=aqu_question AND aqu_avis=:avis ORDER BY aqu_place;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":avis",$d_avis[0]["sav_avis"]);
		$req->execute();
		$d_q=$req->fetchAll();
		if(!empty($d_q)){
			foreach($d_q as $q){
				$d_questions[$q["aqu_id"]]=array(
					"texte" => $q["aqu_texte"],
					"resultat" => 100
				);

			}

		}


	}


	$juridique=get_juridique($acc_societe,$d_action["act_agence"]);

	/*$data=presence_data($action_id,$action_client_id);

	if(empty($data)){
		$erreur_txt="impossible d'afficher la page";
		echo($erreur_txt);
		die();
	}*/
}
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#container_print{
				width:21cm;
			}

			.ligne-col-1{
				width:75%;
				padding-left:5px;
			}
			.ligne-col-2{
				width:25%;
			}


			.table-div{
				display:table;

			}

			.table-div > div{
				vertical-align:middle;
				display:table-ceil;
			}
			#form_signature{
				margin-bottom:100px;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm">

		<div id="zone_print" ></div>


		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php 		if(empty($erreur_txt)){ ?>

							<div class="row" >

								<!-- zone d'affichage du devis -->

								<div class="col-md-8"  >

									<div id="container_print" >

										<div id="page_print" >

								<?php
											foreach($d_avis as $p => $avis){

												foreach($d_questions as $q => $question){
													$d_questions[$q]["resultat"]=100;
												}

												$sql="SELECT sar_question,sar_reponse FROM stagiaires_avis_reponses WHERE
												sar_avis=:sar_avis AND sar_stagiaire=:sar_stagiaire AND sar_action=:sar_action AND sar_utilisateur=:sar_utilisateur;";
												$req=$ConnSoc->prepare($sql);
												$req->bindParam(":sar_avis",$avis["sav_avis"]);
												$req->bindParam(":sar_stagiaire",$avis["sav_stagiaire"]);
												$req->bindParam(":sar_action",$action_id);
												$req->bindParam(":sar_utilisateur",$avis["sav_utilisateur"]);
												$req->execute();
												$d_reponses=$req->fetchAll();
												if(!empty($d_reponses)){
													foreach($d_reponses as $rep){
														$d_questions[$rep["sar_question"]]["resultat"]=$rep["sar_reponse"];
													}
												} 	?>

												<div class="page" id="header_<?=$p?>" data-page="<?=$p?>" >
													<header>
														<table>
															<tr>
																<td class="w-30" >
														<?php		echo($juridique["nom"]);
																	if(!empty($juridique["agence"])){
																		echo("<br/>" . $juridique["agence"]);
																	};
																	if(!empty($juridique["ad1"])){
																		echo("<br/>" . $juridique["ad1"]);
																	};
																	if(!empty($juridique["ad2"])){
																		echo("<br/>" . $juridique["ad2"]);
																	};
																	if(!empty($juridique["ad3"])){
																		echo("<br/>" . $juridique["ad3"]);
																	};
																	echo("<br/>" . $juridique["cp"] . " " . $juridique["ville"]); ?>
																</td>
																<td class="w-40" >
																	<h1 class="text-center" >Avis de stage</h1>
																</td>
																<td class="w-30 text-right" >
														<?php		if(!empty($juridique["logo_1"])){ ?>
																		<img src="<?=$juridique["logo_1"]?>" />
														<?php		}else{
																		echo("&nbsp;");
																	} ?>
																</td>

															</tr>
														</table>
													</header>
												</div>

												<div id="head_<?=$p?>" class="head" >
													<section>
														<table class="tab-border" >
															<tr>
																<td class="w-50" >
																	Formateur :
																	<?php if(!empty($d_utilisateurs[$avis["sav_utilisateur"]])) { ?>
																		<strong><?=$d_utilisateurs[$avis["sav_utilisateur"]]?></strong>
																	<?php } ?>
																</td>
																<td class="w-50" >
																	Formation : <strong><?=$avis["acl_pro_libelle"]?></strong>
																</td>
															</tr>
															<tr>
																<td class="w-50" >
																	Stagiaire : <strong><?=$avis["sav_sta_prenom"] . " " . $avis["sav_sta_nom"]?></strong>
																</td>
															</tr>
														</table>
													</section>
												</div>

												<div id="ligne_header_<?=$p?>" >
													<section>
														<div class="table-div" >
															<div class="table-div-th ligne-col-1 text-center" >Question</div>
															<div class="table-div-th ligne-col-2 text-center" >Taux de satisfaction</div>
														</div>
													</section>
												</div>
										<?php	foreach($d_questions as $question){ ?>
													<div class="table-div ligne-body-<?=$p?>" >
														<div class="ligne-col-1" ><?=$question["texte"]?></div>
														<div class="ligne-col-2 text-right p5" ><?=$question["resultat"]?> %</div>
													</div>
										<?php	} ?>

												<div class="ligne-body-<?=$p?>" >
													<table class="tab-border mt15" >
														<tr>
															<td class="w-50" >
																<u>Points forts :</u>
																<br/>
																<?=$avis["sav_pt_fort"]?></strong>
															</td>
															<td class="w-50" >
																<u>Points faibles :</u>
																<br/>
																<?=$avis["sav_pt_faible"]?></strong>
															</td>
														</tr>
													</table>
												</div>

												<div id="footer_<?=$p?>" <?php if($p<count($d_avis)-1) echo("class='saut-page'"); ?> >
													<footer></footer>
												</div>

							<?php			} ?>
										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->

								<div class="col-md-4" >
									<!-- DEBUT ANNEXE -->

									<!-- FIN ANNEXE -->
								</div>

							</div>


		<?php		}else{ ?>

						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>

		<?php		} ?>

				</section>

				<!-- End: Content -->
			</section>


		</div>
		<!-- End: Main -->

		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="action_voir.php?action=<?=$action_id?>&onglet=3&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
					<button type="button" class="btn btn-sm btn-info ml15" id="print" >
						<i class="fa fa-print"></i> Imprimer
					</button>
				</div>
				<div class="col-xs-3 footer-right" ></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="assets/js/print.js"></script>
		<script type="text/javascript">
			var l_page=21;
			var h_page=27;
		</script>
	</body>
</html>
