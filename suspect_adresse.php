<?php


include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');

// EDITION D'UNE ADRESSE SUSPECT

if(!empty($_GET["suspect"])){
	$suspect=intval($_GET["suspect"]);
}else{
	echo("Erreur!");
	die();
}

$adresse=0;
if(!empty($_GET["adresse"])){
	$adresse=intval($_GET["adresse"]);	
};

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);	
}

// sur le suspect
$sql="SELECT sus_code,sus_categorie,sus_siren,sus_nom,sus_agence,sus_ident_tva FROM Suspects WHERE sus_id=" . $suspect . ";";
$req=$ConnSoc->query($sql);
$d_suspect=$req->fetch();
if(empty($d_suspect)){
	echo("Erreur!");
	die();
}

$d_adresse=array(
	"sad_id" => 0,
	"sad_type" => 0,
	"sad_nom" => $d_suspect["sus_nom"],
	"sad_service" => "",
	"sad_ad1" => "",
	"sad_ad2" => "",
	"sad_ad3" => "",
	"sad_cp" => "",
	"sad_ville" => "",
	"sad_defaut" => 0,
	"sad_libelle" => "",
	"sad_siret" => "",
	"sad_geo" => 1
);



if($adresse>0){
	
	$sql="SELECT * FROM Suspects_Adresses WHERE sad_id=" . $adresse . " AND sad_ref_id=" . $suspect . ";";
	$req=$ConnSoc->query($sql);
	$d_adresse=$req->fetch();
}

// chercher tous les contacts du suspect
$sql="SELECT * FROM Suspects_Contacts WHERE sco_ref_id=" . $suspect . ";";
$req=$ConnSoc->query($sql);
$contacts=$req->fetchAll();

$_SESSION['retourAdresse'] = "suspect_voir.php?suspect=" . $suspect;
if(isset($_GET["onglet"])){
	$_SESSION['retourAdresse'].="&onglet=" . $_GET["onglet"];
}
?>
<!DOCTYPE html>
<html>  
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		
		<form id="filtre" action="suspect_adresse_enr.php" method="post" class="admin-form form-inline form-inline-grid" >
			<div>
				<input type="hidden" name="suspect" value="<?=$suspect?>" />	
				<input type="hidden" id="adresse" name="adresse" value="<?=$adresse?>" />				
			</div>
			<div id="main">
		<?php 	include "includes/header_def.inc.php"; ?>
		
				<section id="content_wrapper" class="">
					<section id="content" class="">						
						<div class="row">
							<div class="col-md-10 col-md-offset-1">							
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
									
										<!-- contenu du form -->
										<div class="panel-body bg-light">
										
											<div class="content-header">									
												<h2>
													<b class="text-primary"><?=$d_suspect["sus_code"]?></b> -
											<?php	if($adresse>0){ ?>
														 Edition d'une 
											<?php	}else{ ?>
														Nouvelle
											<?php	} ?>
													<b class="text-primary">adresse</b>
												</h2>
											</div>
									<?php	if($adresse==0){ ?>
												<div class="row">
													<div class="col-md-12">
														<label for="sad_type" >Type d'adresse : </label>
														<select id="sad_type" name="sad_type" class="form-control">
													<?php 	if($d_suspect['sus_categorie'] != 3){ ?>
																<option value="1" selected >Intervention</option>
													<?php 	} ?>
															<option value="2" >Facturation</option>
													<?php 	if($d_suspect['sus_categorie'] != 3){ ?>		
																<option value="3" >Envoi de facture</option>
													<?php	} ?>
														</select>											
													</div>
												</div>
									<?php	} ?>
											
											<div class="row mt15">
												<div class="col-md-12">
													<label for="sad_libelle" >Libellé :</label>
													<input type="text" id="sad_libelle" name="sad_libelle" class="gui-input" placeholder="Libellé de l'adresse" value="<?=$d_adresse["sad_libelle"]?>" />										
													<span class="input-footer">
														<strong>Info:</strong> Permet d'identifier l'adresse dans le carnet. Par défaut: Entité + Ville
													</span>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-12">
													<label for="sad_nom" >Nom :</label>
													<input type="text" id="sad_nom" name="sad_nom" class="gui-input" placeholder="Entité" value="<?=htmlentities($d_adresse["sad_nom"])?>" />
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-12">
													<label for="sad_service" >Service :</label>
													<input type="text" id="sad_service" name="sad_service" class="gui-input" placeholder="Service" value="<?=htmlentities($d_adresse["sad_service"])?>" />
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-12">
													<label for="sad_ad1" >Adresse :</label>
													<input type="text" id="sad_ad1" name="sad_ad1" class="gui-input" placeholder="Adresse 1" value="<?=htmlentities($d_adresse["sad_ad1"])?>" />
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<input type="text" id="sad_ad2" name="sad_ad2" class="gui-input" placeholder="Adresse 2" value="<?=htmlentities($d_adresse["sad_ad2"])?>" />
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<input type="text" id="sad_ad3" name="sad_ad3" class="gui-input" placeholder="Adresse 3" value="<?=htmlentities($d_adresse["sad_ad3"])?>" />
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<label class="field">
														<input type="text" id="sad_cp" name="sad_cp" class="gui-input code-postal" placeholder="Code Postal" required value="<?=$d_adresse["sad_cp"]?>" />
													  </label>
												</div>
												<div class="col-md-8">
													<label class="field">
														<input type="text" id="sad_ville" name="sad_ville" class="gui-input nom" placeholder="Ville" required value="<?=htmlentities($d_adresse["sad_ville"])?>" />
													  </label>
												</div>
											</div>
											<div class="row mt15">
													<div class="col-md-12 text-center">
														Zone géographique
														<div class="section">
															<label for="sad_geo1" class="mt15 option option-info">
																<input type="radio" id="sad_geo1" class="adr-geo" name="sad_geo" value="1" <?php if($d_adresse["sad_geo"]==1) echo("checked"); ?> />
																<span class="radio"></span> France
															</label>
															<label for="sad_geo2" class=" mt15 option option-info">
																<input type="radio" id="sad_geo2" class="adr-geo" name="sad_geo" value="2" <?php if($d_adresse["sad_geo"]==2) echo("checked"); ?> />
																<span class="radio"></span> UE
															</label>
															<label for="sad_geo3" class=" mt15 option option-info">
																<input type="radio" id="sad_geo3" class="adr-geo" name="sad_geo" value="3" <?php if($d_adresse["sad_geo"]==3) echo("checked"); ?> />
																<span class="radio"></span> Autre
															</label>
														</div>
													</div>
												</div>
							<?php			if($d_suspect['sus_categorie'] != 3){
								
												// SI LE suspect N'EST PAS UN PARTICULIER
												
												if($d_adresse['sad_defaut']==0){
													// autre que particulier if($d_adresse["sad_defaut"]==0){ ?>
													<div class="row mt15">
														<div class="col-md-12">
															<label class="option block mn">
																<input type="checkbox" name="sad_defaut" value="on" />
																<span class="checkbox mn"></span> Adresse par défaut
															</label>
														</div>
													</div>
							<?php 				}else{ ?>
													<div class="row mt15">
														<div class="col-md-12">
															<b>Il s'agit de l'adresse par défaut.</b>
														</div>
													</div>
							<?php 				}
												if($adresse==0 OR $d_adresse["sad_type"]==2){ ?>
												
													<div class="row facturation mt15" <?php if($d_adresse["sad_type"]!=2) echo("style='display:none;'"); ?> >
														
														<div class="col-md-7">
															<label for="sus_siren" >Siren :</label>
															<div class="field prepend-icon">
																<input type="text" name="sus_siren" id="sus_siren" class="gui-input siren" placeholder="Siren" value="<?=$d_suspect['sus_siren']?>" <?php if(!empty($d_suspect['sus_siren'])) echo("readonly"); ?> />
																<span class="field-icon">
																	<i class="fa fa-barcode"></i>
																</span>
															</div>
													<?php 	if(!empty($d_suspect['sus_siren'])){ ?>
																<span class="input-footer">
																	<strong>Info: </strong>&Eacute;ditez la fiche suspect pour mettre à jour le siren.
																</span>
													<?php 	} ?>
														</div>
														<div class="col-md-4">
															<label for="sus_siren" >Nic :</label>
															<div class="field prepend-icon">
																<input type="text" name="sad_siret" id="sad_siret" class="gui-input siret" placeholder="Siret" value="<?=$d_adresse["sad_siret"]?>" />
																<span for="sus_siret" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</span>
															</div>
															<div id="result_test" ></div>
														</div>
														<div class="col-md-1 pt15">
															<button type="button" class="btn btn-warning btn-sm" id="check_siret" data-toggle="tooltip" data-placement="top" title="Vérifier les doublons" >
																<i class="fa fa-check" ></i>
															</button>
														</div>
													</div>
							<?php  				} ?>
												
							<?php				if(empty($d_suspect["sus_ident_tva"])){ ?>
													<div class="row" id="bloc_tva" <?php if($d_adresse["sad_geo"]<2) echo("style='display:none'"); ?>  >
														<div class="col-md-12" >
															<label for="sus_siren" >Numéro de TVA :</label>
															<div class="field prepend-icon">
																<input type="text" name="sus_ident_tva" id="sus_ident_tva" class="gui-input" placeholder="Numéro de TVA" value="" />
																<span for="sus_siret" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</span>
															</div>
														</div>
													</div>
							<?php				} ?>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Contacts</span> 
														</div>
													</div>
												</div>
												
												<div class="row mt15">
													<table class="table table-striped table-hover" >
													<thead>
														<tr class="dark">
															<th class="text-center">Nom du contact</th>
															<th class="text-center">Associé</th>
															<th class="text-center">Par défaut</th>													
														</tr>
													</thead>
													<tbody>
											<?php 	foreach($contacts as $c){ 
														$sql="SELECT * FROM Suspects_Adresses_Contacts WHERE sac_contact=" . $c['sco_id'] . " AND sac_adresse = " . $d_adresse['sad_id'];													
														$req=$ConnSoc->query($sql);
														$contact_adresse=$req->fetch(); ?>
														<tr>
															<td class="text-center">
																<p>
																	<?= $c['sco_prenom'] ?> <?= $c['sco_nom'] ?>
																</p>
															</td>
															<td class="text-center">
																<label class="option block mn">
																	<input type="checkbox" id="sac_contact_<?= $c['sco_id'] ?>"  class="sac_contact" name="sac_contact[]" value="<?= $c['sco_id']?>" <?php if(!empty($contact_adresse)) echo("checked") ?> />
																	<span class="checkbox mn"></span>																	
																</label>
															</td>
															<td class="text-center">
																<label class="option block mn">
												<?php				if(empty($contact_adresse)){ ?>
																		<input type="radio" id="sac_defaut_<?= $c['sco_id']?>" class="sac_defaut" name="sac_defaut" value="<?= $c['sco_id']?>" disabled />
												<?php				}else{ ?>
																		<input type="radio" id="sac_defaut_<?= $c['sco_id'] ?>" class="sac_defaut" name="sac_defaut" value="<?= $c['sco_id']?>" <?php if(!empty($contact_adresse['sac_defaut'])) echo("checked"); ?> />
												<?php				} ?>
																	<span class="radio mn"></span>																
																</label>
															</td>
														</tr>
														<?php }?>
														</tbody>
													</table>
													
												</div>
							<?php
							 	
											} 
											// FIN TRAITEMENT HORS PARTICULIER ?>
							
											
														
										</div>
										<!-- fin de contenu form -->
										
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-4 footer-left">
						<a href="<?=$_SESSION['retourAdresse']?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-4 footer-middle"></div>
					<div class="col-xs-4 footer-right">
						<button type="submit" class="btn btn-success btn-sm">
							<i class='fa fa-floppy-o'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php
		include "includes/footer_script.inc.php"; ?>   
		<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js" ></script>
		<script type="text/javascript" src="assets/js/custom.js" ></script>
		<script type="text/javascript">
			var societe="<?=$acc_societe?>";
			var agence="<?=$d_suspect["sus_agence"]?>";
			
			jQuery(document).ready(function(){
			
				$(".sac_contact").click(function(){
					if($(this).is(":checked")){
						$("#sac_defaut_" + $(this).val()).prop("disabled",false);
					}else if($("#sac_defaut_" + $(this).val()).prop("checked")){
						$(this).prop("checked",true);
					}else{
						$("#sac_defaut_" + $(this).val()).prop("disabled",true);
					}
					
				});

				$("#sad_type").change(function(){	
					if($(this).val()==2){
						$(".facturation").show();						
					}else{
						$(".facturation").hide();						
					}
				});
				
				$(".adr-geo").click(function(){
					console.log("adr-geo change");
					if($("#bloc_tva").length==1){
						if($(this).val()>1){
							$("#bloc_tva").show();
						}else{
							$("#bloc_tva").hide();
						}
					}
				});
				
				$("#check_siret").click(function(){
					siren=$("#sus_siren").val();
					siren=$("#sus_siren").val();
					check_siret_suspect($("#sus_siren").val(),$("#sad_siret").val(),$("#adresse").val(),result_check);
				});
				
			});
			
			function result_check(json){
				afficher_txt_user($("#result_test"),"Ce siret est libre","text-success",5000);
			}
		</script>
	</body>
</html>
