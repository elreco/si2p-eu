
	<header class="navbar navbar-fixed-top navbar-shadow">
		<div class="navbar-branding" style="width:400px;">
			<a class="navbar-brand" href="accueil.php">
				<b>Si2P</b> ORION - <?=$_SESSION['acces']['acc_prenom']?> <?=$_SESSION['acces']['acc_nom']?>
			</a>
		</div>
		<ul class="nav navbar-nav navbar-right">	
			<li class="menu-divider hidden-xs">
				<i class="fa fa-circle"></i>
			</li>
			<li class="dropdown menu-merge">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding:15px!important">
					<span class="hidden-xs"><?=$_SESSION['acces']['acc_prenom'] . " " . $_SESSION['acces']['acc_nom']?></span>
					<span class="caret caret-tp hidden-xs"></span>
				</a>
				<ul class="dropdown-menu list-group dropdown-persist w250" role="menu">			
					<li class="list-group-item">
<!-- 						<a href="#" class="animated animated-short fadeInUp">
							<span class="fa fa-gear"></span> Mon compte 
						</a> -->
					</li>
					<li class="dropdown-footer">
						<a href="deconnect.php" class="">
							<span class="fa fa-power-off pr5"></span> Déconnexion
						</a>
					</li>
				</ul>
			</li>
			<li class="toggle_sidemenu_t hidden-md hidden-lg">
				<span class="fa fa-caret-up hidden-md hidden-lg"></span>
			</li>
		</ul>
		</header>
	
	<aside id="sidebar_left" class="">
		<div class="sidebar-left-content nano-content">
			<ul class="nav sidebar-menu">
				<li  <?php if(isset($menu_actif) && $menu_actif == 0): ?> class="active" <?php endif; ?>>
					<a>
						<span class="glyphicon glyphicon-home"></span>
						<span class="sidebar-title">Accueil</span>
					</a>
				</li>
				
			</ul>
		</div>
	</aside>

