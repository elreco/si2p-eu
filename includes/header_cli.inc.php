
	<header class="navbar navbar-fixed-top navbar-shadow">
		<div class="navbar-branding" style="width:400px;">
			<a class="navbar-brand" href="accueil_client.php">
				<b>Si2P</b> ORION - <?=$_SESSION['acces']['acc_client_nom']?>
			</a>
			
		</div>
		<ul class="nav navbar-nav navbar-right">	
			<li class="menu-divider hidden-xs">
				<i class="fa fa-circle"></i>
			</li>
			<li class="dropdown menu-merge">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding:15px!important">
					<span class="hidden-xs"><?=$_SESSION['acces']['acc_prenom'] . " " . $_SESSION['acces']['acc_nom']?></span>
					<span class="caret caret-tp hidden-xs"></span>
				</a>
				<ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
					
					<li class="dropdown-footer">
						<a href="deconnect.php" class="">
							<span class="fa fa-power-off pr5"></span> Déconnexion
						</a>
					</li>
				</ul>
			</li>
			<li class="toggle_sidemenu_t hidden-md hidden-lg">
				<span class="fa fa-caret-up hidden-md hidden-lg"></span>
			</li>
		</ul>
		</header>
	
	<aside id="sidebar_left" class="">
		<div class="sidebar-left-content nano-content">
			<ul class="nav sidebar-menu">
				<li  <?php if(isset($menu_actif) && $menu_actif == 0): ?> class="active" <?php endif; ?>>
					<a href="accueil_client.php">
						<span class="glyphicon glyphicon-home"></span>
						<span class="sidebar-title">Accueil</span>
					</a>
				</li>
			
				<li <?php if(isset($menu_actif) && $menu_actif == 1): ?> class="active" <?php endif; ?>>
					<a href="stagiaire_client_tri.php" >
						<span class="fa fa-users"></span>
						<span class="sidebar-title">Stagiaires</span>
					</a>
				</li>
				<li <?php if(isset($menu_actif) && $menu_actif == 2): ?> class="active" <?php endif; ?>>
					<a href="session_client_liste.php">
						<span class="fa fa-calendar-o"></span>
						<span class="sidebar-title">Formations</span>
					</a>
				</li>
				<li <?php if(isset($menu_actif) && $menu_actif == 3): ?> class="active" <?php endif; ?>>
					<a href="facture_client_liste.php" >
						<span class="fa fa-file-text"></span>
						<span class="sidebar-title">Factures en attente</span>
					</a>
				</li>
			</ul>
		</div>
	</aside>

