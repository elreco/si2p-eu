<?php

session_start();
if(!isset($_SESSION["acces"]['acc_timer'])){
	// MARCHE PAS CAR PAS DE DESTROY AUTO COMME EN ASP 
	header("location : index.php");
	exit();
}
if(time()-$_SESSION["acces"]['acc_timer']>1200) 
{	header('Location: deconnect.php');
	exit();
}else{
	$_SESSION['acces']['acc_timer']=time();
}
if($_SESSION["acces"]['acc_ref'] != 2){
	header('Location: deconnect.php');
	exit();
}
?>