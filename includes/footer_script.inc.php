
	<div id="ModalDeconnect" class="modal fade" role="dialog">
		<div class="modal-dialog admin-form theme-primary">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title" >Déconnexion</h4>
				</div>
				<div class="modal-body">
					<p>Vous êtes sûr le point d'être déconnecté.</p>
					<p>Cliquez sur le bouton "Continuer" pour rester connecté</p>
				</div>
				<div class="modal-footer">
					<button type="button" id="reconnect" class="btn btn-success btn-sm" >
						<i class="fa fa-lock"></i>
						Continuer
					</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modal_alerte_user" class="modal fade" role="dialog">
		<div class="modal-dialog admin-form theme-primary">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title" >
						<i class="fa fa-exclamation" ></i> Attention
					</h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" >
						<i class="fa fa-times"></i> Fermer
					</button>
				</div>
			</div>
		</div>
	</div>

<?php
	if(!empty($_GET["erreur"]) OR (!empty($_SESSION['message']['aff']) && $_SESSION['message']['aff'] == "modal")){

		$modal_titre_class="";
		$modal_titre="";

		if(!empty($_SESSION['message']['type'])){
			Switch ($_SESSION['message']['type']) {
				case "danger":
					$modal_titre_class="danger";
					$modal_titre="Erreur";
					break;
				case "warning":
					$modal_titre_class="warning";
					$modal_titre="Attention";
					break;
				default:
					$modal_titre_class="success";
					$modal_titre="Succès";
			}
		}else{
			if($_GET["erreur"]<0){
				$modal_titre="Succès";
			}else{
				$modal_titre="Erreur";
			}
		}
		?>

		<div id="modal-error" class="modal fade" role="dialog" data-show="true" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header <?php if(!empty($modal_titre_class)) echo("bg-" . $modal_titre_class); ?>">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title" id="titreModal" ><?=$modal_titre?></h4>
					</div>
					<div class="modal-body">
					<?php if(!empty($_SESSION['message']['aff'])){
							echo $_SESSION['message']['message'];
						}else{
							if(intval($_GET["erreur"])){
								//get_erreur_txt($_GET["erreur"]);
							}else{
								echo($_GET["erreur"]);
							}
						}
							?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
							<i class="fa fa-close" ></i>
							Fermer
						</button>
					</div>
				</div>
			</div>
		</div>

<?php
}; ?>



<?php // alternative aux erreurs

if(!empty($_SESSION['notifications'])){
foreach($_SESSION['notifications'] as $not_id => $notifi){

?>
	<div id="modal-notif-<?= $not_id ?>" class="modal fade" role="dialog" data-show="true" >
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header <?php if(!empty($notifi["class"])){ ?><?= $notifi["class"] ?><?php }?>">
					<button type="button" class="close notification_vu" data-dismiss="modal">×</button>

						<h4 class="modal-title" id="titreModal">Notification Orion</h4>

				</div>
				<div class="modal-body">
				<?= $notifi["icone"]; ?> <?= $notifi["message"]; ?>
				</div>

				<div class="modal-footer">
				<?php if(!empty($notifi['url'])){ ?>
					<a href="<?= $notifi['url'] ?>" class="btn btn-info btn-sm notification_vu">
						<i class="fa fa-eye" ></i>
						Voir
					</a>
				<?php }?>
					<button type="button" class="btn btn-danger btn-sm notification_vu" data-dismiss="modal">
						<i class="fa fa-close" ></i>
						Fermer
					</button>
				</div>
			</div>
		</div>
	</div>
<?php }

} ?>
	<script src="vendor/jquery/jquery-2.0.0.min.js"></script>
	<!--<script src="vendor/jquery/jquery-1.11.1.min.js"></script>-->
	<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
	<script src="vendor/plugins/pnotify/pnotify.js"></script>
	<script src="assets/js/utility/utility.js"></script>
	<script src="assets/js/main.js"></script>
	<script src='vendor/plugins/jquery.sound.js'></script>
	<script src="assets/js/orion.js?v2-1-1"></script>
	<?php // alternative aux erreurs

if(!empty($_SESSION['notifications'])){
foreach($_SESSION['notifications'] as $not_id => $notifi){

?>
			<script>
				$("#modal-notif-<?= $not_id ?>").modal();
			</script>
	<?php }
	}
	 ?>
	<script>

		jQuery(document).ready(function(){
			$.fn.modal.Constructor.prototype.enforceFocus = function() {};
			<?php 	// ALERT A DESTINATION DES UTRILISATEURS
			if(!empty($_SESSION['message']) && empty($_SESSION['message']['aff'])){
				foreach($_SESSION['message'] as $m){
					if(empty($m['aff'])){ ?>
						new PNotify({
							title: "<?= $m['titre'] ?>",
							text: "<?= $m['message'] ?>",
							type: "<?= $m['type'] ?>", // all contextuals available(info,system,warning,etc)
							hide: true,
							mouse_reset: false,
							delay: 4000
						});
	<?php			}
				}
			unset($_SESSION['message']);
			}
			unset($_SESSION['message']);
			unset($_SESSION['notifications']);
			?>

			// actualiser les notifs vues
			$(".notification_vu").on('click', function() {

				<?php if(!empty($notif_json)){ ?>
					$.ajax({
						type: 'POST',
						url: 'ajax/ajax_actualise_notification.php',
						data : {
							data:<?= $notif_json ?>
						},
						dataType: 'json',
						success: function(data)
						{
							if(data == 1){
								$(".badge-notif").remove();
							}

						}

					});
				<?php }?>
			});
		});
	</script>

	<?php	 if(!empty($_SESSION['message']['aff'])){ ?>
			<script>
				$("#modal-error").modal();
			</script>
	<?php }
	 ?>

