<?php
	// LOGS
	// client
	$log_num_devis= "";
	$log_client_nom= "";
	$log_societe = "";
	$log_agence = "";

	if($_SERVER['PHP_SELF']=="/client_voir.php"){
		$log_client_nom = $d_client['cli_nom'] . " (" . $d_client['cli_code'] . ")";

	}elseif($_SERVER['PHP_SELF']=="/devis_voir.php"){
		$log_num_devis = $data["dev_numero"];
	}

	if(!empty($_SESSION['acces']['acc_societe'])){
		$sql="SELECT soc_code FROM societes WHERE soc_id = " . $_SESSION['acces']['acc_societe'];
		$req=$Conn->query($sql);
		$d_log_societe =$req->fetch();
		if(!empty($d_log_societe)){
			$log_societe = $d_log_societe['soc_code'];
		}
	}
	if(!empty($_SESSION['acces']['acc_agence'])){
		$sql="SELECT age_code FROM agences WHERE age_id = " . $_SESSION['acces']['acc_agence'];
		$req=$Conn->query($sql);
		$d_log_agence =$req->fetch();
		if(!empty($d_log_agence)){
			$log_agence = $d_log_agence['age_code'];
		}
	}

	$actual_link = $_SERVER["REQUEST_URI"];

	$sql="SELECT log_url FROM logs WHERE log_utilisateur = " . $_SESSION["acces"]["acc_ref_id"] . " ORDER BY log_date DESC  LIMIT 1";
	$req=$Conn->query($sql);
	$log_exist =$req->fetch();
	if($log_exist['log_url'] != $actual_link){
		$sql="INSERT INTO logs (log_utilisateur, log_uti_nom, log_uti_prenom, log_url, log_date, log_societe, log_agence, log_num_devis, log_client_nom)
		VALUES (:log_utilisateur, :log_uti_nom, :log_uti_prenom, :log_url, NOW(),:log_societe, :log_agence, :log_num_devis, :log_client_nom)";
		$req=$Conn->prepare($sql);
		$req->bindParam(":log_utilisateur",$_SESSION["acces"]["acc_ref_id"]);
		$req->bindParam(":log_uti_nom",$_SESSION["acces"]['acc_nom']);
		$req->bindParam(":log_uti_prenom",$_SESSION["acces"]['acc_prenom']);
		$req->bindParam(":log_societe",$log_societe);
		$req->bindParam(":log_agence",$log_agence);
		$req->bindParam(":log_num_devis",$log_num_devis);
		$req->bindParam(":log_client_nom",$log_client_nom);
		$req->bindParam(":log_url",$actual_link);
		$req->execute();
	}


	// DOCUMENTS UTI
	$sql="SELECT uti_id, uti_prenom,uti_nom
	FROM utilisateurs
	LEFT OUTER JOIN documents_utilisateurs ON (utilisateurs.uti_id = documents_utilisateurs.dut_utilisateur)
	LEFT JOIN documents ON (documents.doc_id = documents_utilisateurs.dut_document)
	WHERE dut_obligatoire = 1 AND (dut_vu = 0 OR ISNULL(dut_vu) ) AND uti_societe = " . $_SESSION['acces']['acc_societe'] . " AND uti_agence = " . $_SESSION['acces']['acc_agence'] . " GROUP BY uti_id ORDER BY uti_id;";
	$req=$Conn->query($sql);
	$number_non_consultes = count($req->fetchAll());


	// MENU ACTIF

	if(isset($menu_actif)){
		if(!empty($menu_actif)){
			$tab_menu=explode("-",$menu_actif);
			$_SESSION["menu"]["item"]=$menu_actif;
			$_SESSION["menu"]["rubrique"]=$tab_menu[0];
		}
	}
?>
<!-- Start: Header -->
<header class="navbar navbar-fixed-top navbar-shadow hidden-print" >
	<div class="navbar-branding" style="width:400px;">
		<a class="navbar-brand" href="accueil.php">
			<b>Si2P</b> ORION
		</a>
		<?php


		$cle_ref2=$_SESSION["acces"]['acc_societe'] . "-" . $_SESSION["acces"]['acc_agence'];

		foreach($_SESSION["acces"]["acc_liste_societe"] as $cle2=>$value){
			if($cle2==$cle_ref2){
				$societe_brand = $value;
			}
		}


		if(empty($societe_brand)){
			$_SESSION['message'][] = array(
				"titre" => "Erreur",
				"type" => "danger",
				"message" => "Connexion impossible, contactez l'administrateur système."
			);
			Header("Location: deconnect.php");
			die();
		}

		// gestion des notifications
		if($_SESSION["acces"]["acc_ref"]==1){
			$not_utilisateur=$_SESSION["acces"]["acc_ref_id"];
			$sql="SELECT * FROM Notifications WHERE not_utilisateur=:not_utilisateur AND not_societe = :not_societe AND not_agence = :not_agence  ORDER BY not_id DESC LIMIT 30;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":not_utilisateur",$not_utilisateur);
			$req->bindParam(":not_societe",$_SESSION['acces']["acc_societe"]);
			$req->bindParam(":not_agence",$_SESSION['acces']["acc_agence"]);
			$req->execute();
			$notif=$req->fetchAll();
			foreach($notif as $n){
				if (!empty($n['not_type']) && empty($n['not_vu'])) {
					$_SESSION['notifications'][$n['not_id']] = array();
					$_SESSION['notifications'][$n['not_id']]['type'] = 1;
					$_SESSION['notifications'][$n['not_id']]['message'] = $n['not_texte'];
					$_SESSION['notifications'][$n['not_id']]['class'] = $n['not_classe'];
					$_SESSION['notifications'][$n['not_id']]['url'] = $n['not_url'];
					$_SESSION['notifications'][$n['not_id']]['icone'] = $n['not_icone'];
				}
			}
			$sql="SELECT not_id FROM Notifications WHERE not_utilisateur=:not_utilisateur AND not_societe = :not_societe AND not_agence = :not_agence AND not_vu = 0;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":not_utilisateur",$not_utilisateur);
			$req->bindParam(":not_societe",$_SESSION['acces']["acc_societe"]);
			$req->bindParam(":not_agence",$_SESSION['acces']["acc_agence"]);
			$req->execute();
			$count_notif=$req->fetchAll();

			$notif_json = json_encode($notif);
		}

		?>
		<p class="navbar-brand">
			<span class="societe-brand"><i class="fa fa-building"></i> <?= $societe_brand ?></span>
		</p>
	</div>

	<ul class="nav navbar-nav navbar-right">
		<li class="dropdown menu-merge">
			<div class="navbar-btn btn-group">
				<button type="button" data-toggle="dropdown" class="btn btn-sm dropdown-toggle notification_vu">
					<span class="fa fa-bell-o fs14 va-m"></span>
				<?php
					if(!empty($count_notif)){
						echo("<span class='badge badge-danger badge-notif'>" . count ($count_notif) . "</span>");
					} ?>

				</button>
				<div class="dropdown-menu dropdown-persist w350 animated animated-shorter fadeIn" role="menu">
					<div class="panel mbn">
						<div class="panel-menu">
							<span class="panel-icon"><i class="fa fa-clock-o"></i></span>
							<span class="panel-title fw600">Notifications</span>
						</div>
				<?php	if(!empty($notif)){ ?>
							<div class="panel-body panel-scroller scroller-navbar scroller-overlay scroller-pn pn">
								<ol class="timeline-list">
				<?php				foreach($notif as $n){ ?>
										<li class="timeline-item">
											<div class="timeline-icon <?=$n["not_classe"]?>">
												<?=$n["not_icone"]?>
											</div>
											<div class="timeline-desc">
												<?=$n["not_texte"]?>
											</div>
											<div class="timeline-date">
											<?php if(!empty($n["not_url"])){ ?>
												<a href="<?=$n["not_url"]?>&notification=<?=$n["not_id"]?>"  class="btn btn-xs btn-primary">
													<i class="fa fa-eye"></i> voir
												</a>
											<?php } ?>
											</div>
										</li>
				<?php				} ?>
								</ol>
							</div>
				<?php	} ?>
						<div class="panel-footer text-center p7">
							<a href="notification_liste.php" class="link-unstyled"> Voir tout </a>
						</div>
					</div>
				</div>
			</div>
		</li>
<?php 	if($_SESSION['acces']['acc_service'][1]== 1 OR $_SESSION['acces']['acc_service'][2]== 1 OR $_SESSION['acces']['acc_service'][3]== 1 OR $_SESSION['acces']['acc_service'][4]==1  OR $_SESSION['acces']['acc_service'][5]==1){
			//temporaire ?>
			<li class="dropdown menu-merge">
				<div class="navbar-btn btn-group">
					<a href="parametre.php" class=" btn btn-sm">
						<!-- class: topbar-menu-toggle -->
						<span class="fa fa-cog"></span>
					</a>
				</div>
			</li>
<?php 	} ?>
		<li class="menu-divider hidden-xs">
			<i class="fa fa-circle"></i>
		</li>
		<li class="dropdown menu-merge">
			<a href="#" class="dropdown-toggle fw600 p20" data-toggle="dropdown" style="padding:15px!important;">
				<i class="fa fa-user" ></i>
				<!-- <img src="assets/img/avatars/1.png" alt="avatar" class="mw30 br64"> -->
				<span class="hidden-xs pl10"><?=$_SESSION["acces"]['acc_prenom'] . " " . $_SESSION["acces"]['acc_nom']?></span>
				<span class="caret caret-tp hidden-xs"></span>
			</a>
			<ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
				<li class="dropdown-header clearfix">
					<div class="pull-right mr10">
						<select id="user-role">
							<optgroup label="Changer de société:">

						<?php	if(!empty($_SESSION['acces']["acc_liste_societe"])){

									$cle_ref=$_SESSION["acces"]['acc_societe'] . "-" . $_SESSION["acces"]['acc_agence'];

									foreach($_SESSION['acces']["acc_liste_societe"] as $cle=>$value){
										if($cle==$cle_ref){
											echo("<option value='" . $cle . "' selected >" . $value . "</option>");
										}else{
											echo("<option value='" . $cle . "' >" . $value . "</option>");
										}
									}
								} ?>
							</optgroup>
						</select>

					</div>
				</li>
				<li class="list-group-item">
					<a href="mon_compte.php" class="animated animated-short">
						<span class="fa fa-gear"></span> Mon compte
					</a>
				</li>
				<?php
				if ($_ENV['APP_ENV'] && $_ENV['APP_ENV'] == 'local') { ?>
					<li class="list-group-item">
					<?php
						$ciphertext = base64_encode($_SESSION['acces']['acc_ref_id']);
					?>
						<a href="<?= $_ENV['PLATEFORME_URL'] ?>/login-from-orion/<?= $ciphertext ?>" class="animated animated-short">
							<span class="fa fa-user-plus"></span> Connexion à la plateforme
						</a>
					</li>
				<?php } ?>
				<li class="dropdown-footer">
					<a href="deconnect.php" class="">
						<span class="fa fa-power-off pr5"></span> Déconnexion
					</a>
				</li>
			</ul>
		</li>
		<li id="toggle_sidemenu_t">
			<span class="fa fa-caret-up"></span>
		</li>
	</ul>
</header>
<!-- End: Header -->

<!-- Start: Sidebar -->
<aside id="sidebar_left" class="hidden-print">
	<!-- Start: Sidebar Left Content -->
	<div class="sidebar-left-content nano-content">
		<ul class="nav sidebar-menu">

			<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["rubrique"] == 0) echo("class='active'"); ?> >
				<a href="accueil.php">
					<span class="glyphicon glyphicon-home"></span>
					<span class="sidebar-title">Accueil</span>
				</a>
			</li>

<?php		if($_SESSION["acces"]["acc_profil"]!=1){ ?>
				<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["rubrique"] == 1) echo("class='active'"); ?> >
					<a class="accordion-toggle" href="#">
						<span class="glyphicon glyphicon-shopping-cart"></span>
						<span class="sidebar-title">Commerce</span>

					</a>
					<ul class="nav sub-nav">
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "1-0") echo("class='active'"); ?> >
							<a href="accueil.php?menu=1"><i class="fa fa-dashboard"></i> Tableau de bord</a>
						</li>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "1-1") echo("class='active'"); ?> >
							<a href="produit_tri.php">Produits</a>
						</li>
			<?php		if($_SESSION['acces']['acc_profil'] !=1){ ?>
							<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "1-2") echo("class='active'"); ?> >
								<a href="commercial_liste.php">Commerciaux</a>
							</li>
			<?php 		}
						if($_SESSION["acces"]["acc_droits"][23]){ ?>
							<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "1-3") echo("class='active'"); ?> >
								<a href="suspect_tri.php">Suspects</a>
							</li>
			<?php		} ?>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "1-4") echo("class='active'"); ?> >
							<a href="prospect_tri.php">Prospects</a>
						</li>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "1-5") echo("class='active'"); ?> >
							<a href="client_tri.php?cli">Clients</a>
						</li>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "1-6") echo("class='active'"); ?> >
							<a href="devis_tri.php">
								Devis
							</a>
						</li>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "1-7") echo("class='active'"); ?> >
							<a href="facture_tri.php">Factures</a>
						</li>
				<?php 	if($_SESSION['acces']['acc_profil'] != 1){
							if($_SESSION['acces']['acc_service'][1]==1 OR $_SESSION['acces']['acc_service'][1]==5){ ?>
								<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "1-8") echo("class='active'"); ?> >
									<a href="client_gc_liste.php">
										Clients GC
									</a>
								</li>
				<?php 		}
						}
						if($_SESSION['acces']['acc_profil']==3 OR $_SESSION['acces']['acc_profil']==15 OR $_SESSION['acces']['acc_service'][1]==1 OR $_SESSION['acces']['acc_service'][3]==1 OR $_SESSION['acces']['acc_profil'] == 11 OR $_SESSION['acces']['acc_profil'] == 12 OR $_SESSION['acces']['acc_profil'] == 14){
							// COM, RA, Service Com (RE), Service Tech, DAF, RRH, DG ?>
							<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "1-9") echo("class='active'"); ?> >
								<a href="stat_commercial.php" >Statistiques</a>
							</li>
				<?php	} ?>
					</ul>
				</li>
<?php		} ?>

			<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["rubrique"] == 2) echo("class='active'"); ?> >
					<a class="accordion-toggle" href="#">
						<span class="glyphicon glyphicon-calendar"></span>
						<span class="sidebar-title">Formations / Planning</span>
					</a>
					<ul class="nav sub-nav">
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "2-0") echo("class='active'"); ?> >
							<a href="accueil.php?menu=2"><i class="fa fa-dashboard"></i> Tableau de bord</a>
						</li>
			<?php		if($_SESSION["acces"]["acc_profil"]!=1 AND $_SESSION["acces"]["acc_profil"]!=3){ ?>
								<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "2-1") echo("class='active'"); ?> >
									<a href="intervenant_liste.php">Intervenants</a>
								</li>
								<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "2-2") echo("class='active'"); ?> >
									<a href="vehicule_liste.php">Véhicules</a>
								</li>
				<?php		} ?>
							<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "2-3") echo("class='active'"); ?> >
								<a href="planning.php?menu_actif=2-3">Planning</a>
							</li>
				<?php		if($_SESSION["acces"]["acc_profil"]!=1){
								if($_SESSION['acces']['acc_agence']==4 AND $_SESSION['acces']['acc_droits'][8]){ ?>
									<li>
										<a href="action_tri_gc.php?menu_actif=2-4">Actions GC</a>
									</li>
						<?php	}else{ ?>
									<li>
										<a href="action_tri.php?menu_actif=2-4">Actions</a>
									</li>
						<?php	} ?>
								<li>
									<a href="stagiaire_tri.php?menu_actif=2-5">Stagiaires</a>
								</li>
				<?php		} ?>
							<li>
							<a href="rap_for_liste.php?menu_actif=2-6">Rapports hebdomadaires</a>
						</li>

				<?php	if($_SESSION['acces']['acc_service'][1]==1 OR in_array($_SESSION['acces']['acc_profil'], array('1', '4', '15', '10', '11', '14'))) { ?>
							<li>
								<a href="stat_formation.php" >Statistiques</a>
							</li>
				<?php	} ?>
					</ul>
				</li>

			<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["rubrique"] == 3) echo("class='active'"); ?> >

				<a class="accordion-toggle" href="#">
					<span class="fa fa-street-view"></span>
					<span class="sidebar-title">R.H.</span>
				</a>
				<ul class="nav sub-nav">
					<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "3-0") echo("class='active'"); ?> >
						<a href="accueil.php?menu=3"><i class="fa fa-dashboard"></i> Tableau de bord</a>
					</li>
		<?php		if(!empty($_SESSION["acces"]["acc_droits"]["22"])){ ?>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "3-1") echo("class='active'"); ?> >
							<a href="utilisateur_liste.php">
								Utilisateurs
							</a>
						</li>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "3-2") echo("class='active'"); ?> >
							<a href="conges_integration.php">
								Intégration congés
							</a>
						</li>
	<?php			} ?>
					<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "3-3") echo("class='active'"); ?> >
						<a href="conges_liste.php">
							Congés
						</a>
					</li>
				</ul>
			</li>

			<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["rubrique"] == 4) echo("class='active'"); ?> >
				<a class="accordion-toggle" href="#">
					<span class="fa fa-cogs"></span>
					<span class="sidebar-title">Technique</span>

				</a>
				<ul class="nav sub-nav">
					<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "4-0") echo("class='active'"); ?> >
						<a href="accueil.php?menu=4"><i class="fa fa-dashboard"></i> Tableau de bord</a>
					</li>
					<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "4-1") echo("class='active'"); ?> >
						<a href="etech.php" >
							Etech
						</a>
					</li>
					<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "4-2") echo("class='active'"); ?> >
						<a href="evac_liste.php">
							Rapports d'évacuation
						</a>
					</li>
			<?php 	if($_SESSION['acces']['acc_profil'] == 13 OR $_SESSION['acces']['acc_profil'] == 2 OR $_SESSION['acces']['acc_profil'] == 9  OR $_SESSION['acces']['acc_profil'] == 15  OR $_SESSION['acces']['acc_profil'] == 5  OR $_SESSION['acces']['acc_profil'] == 4){ ?>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "4-3") echo("class='active'"); ?> >
							<a href="etech_special.php">
								Docs non consultés (<?= $number_non_consultes ?> pers.)
							</a>
						</li>
			<?php	}
					if ($_SESSION['acces']['acc_service'][3]==1 OR $_SESSION['acces']["acc_profil"]==11 OR $_SESSION['acces']["acc_profil"]==14 OR $_SESSION['acces']["acc_profil"]==15 OR $_SESSION['acces']["acc_profil"]==10) {
						// Service tech, DAF, DG, RA et RE ?>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "4-4") echo("class='active'"); ?> >
							<a href="stat_technique.php">
								Statistiques
							</a>
						</li>
			<?php	} ?>
				</ul>
			</li>
			<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["rubrique"] == 5) echo("class='active'"); ?> >
				<a class="accordion-toggle" href="#">
					<span class="fa fa-line-chart"></span>
					<span class="sidebar-title">Compta</span>
				</a>
				<ul class="nav sub-nav sub-sub-nav">
					<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "5-0") echo("class='active'"); ?> >
						<a href="accueil.php?menu=5"><i class="fa fa-dashboard"></i> Tableau de bord</a>
					</li>
			<?php	if($_SESSION["acces"]["acc_profil"]!=1){ ?>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "5-1") echo("class='active'"); ?> >
							<a href="fournisseur_tri.php">
								Fournisseurs
							</a>
						</li>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "5-2") echo("class='active'"); ?> >
							<a href="commande_tri.php" >
								Commandes
							</a>
						</li>
		<?php		} ?>
					<li>
						<a href="ndf.php?ndf_menu" >
							Mes notes de frais
						</a>
					</li>
					<?php if (in_array($_SESSION['acces']['acc_profil'], [7, 13, 12, 8, 11, 15, 10, 9, 14])) { ?>
					<li>
						<a href="ndf_liste.php" >
							Liste des notes de frais
						</a>
					</li>
					<?php } ?>
			<?php 	if(!empty($_SESSION['acces']['acc_droits'][33])){ ?>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "5-3") echo("class='active'"); ?> >
							<a href="facture_relance_tri.php">
								Processus client
							</a>
						</li>
			<?php 	}
					if(!empty($_SESSION['acces']['acc_service'][2]==1)){ ?>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "5-4") echo("class='active'"); ?> >
							<a href="reglement_tri.php">
								Encaissements
							</a>
						</li>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "5-5") echo("class='active'"); ?> >
							<a href="affacturage_accueil.php">
								Affacturage
							</a>
						</li>
			<?php 	}  ?>
				</ul>
			</li>

	<?php 	if($_SESSION['acces']['acc_service'][5]== 1 OR $_SESSION['acces']['acc_profil'] == 13 OR $_SESSION['acces']['acc_profil'] == 10 OR $_SESSION['acces']['acc_profil'] == 9){ ?>
				<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["rubrique"] == 6) echo("class='active'"); ?> >
					<a class="accordion-toggle" href="#">
						<span class="glyphicon glyphicon-shopping-cart"></span>
						<span class="sidebar-title">Marketing</span>
					</a>

					<ul class="nav sub-nav">
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "6-0") echo("class='active'"); ?> >
							<a href="accueil.php?menu=6"><i class="fa fa-dashboard"></i> Tableau de bord</a>
						</li>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "6-1") echo("class='active'"); ?> >
							<a href="market_suspect_tri.php">Suspects</a>
						</li>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "6-2") echo("class='active'"); ?> >
							<a href="market_prospect_tri.php">Prospects</a>
						</li>
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "6-3") echo("class='active'"); ?> >
							<a href="market_client_tri.php">Clients</a>
						</li>
						<!--<li>
							<a href="market_stagiaire_tri.php?menu_actif=6-4">Stagiaires</a>
						</li>-->
						<li <?php if(isset($_SESSION["menu"]) && $_SESSION["menu"]["item"] == "6-4") echo("class='active'"); ?> >
							<a href="market_concours.php">Concours</a>
						</li>
					</ul>
				</li>
	<?php 	}

			if($_SESSION['acces']['acc_profil'] == 13){ ?>
				<li>
					<a class="accordion-toggle" href="#">
						<span class="glyphicon glyphicon-heart"></span>
						<span class="sidebar-title">Favoris</span>

					</a>
					<ul class="nav sub-nav">
						<li>
							<a href="aide/index.php" target="_blank" >
								<span class="fa fa-question"></span>
								Aides
							</a>
						</li>
					</ul>
				</li>
<?php 		} ?>

		</ul>
	</div>
</aside>
