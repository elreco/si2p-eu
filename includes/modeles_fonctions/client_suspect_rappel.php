<?php
include_once('modeles/mod_orion_utilisateur.php'); 

$rappel=array();


// LES CLIENTS
$sql="SELECT cli_id,cli_code,cli_nom,cor_rappeler_le,cor_commentaire,cor_id,cor_date
FROM Correspondances,Clients,Clients_Societes
WHERE cor_client=cli_id AND cli_id=cso_client AND cor_utilisateur=:acc_utilisateur AND cso_societe=:acc_societe";
if(!empty($acc_agence)){
	$sql.=" AND cso_agence=:acc_agence";
}
$sql.=" AND NOT ISNULL(cor_rappeler_le) AND NOT cor_rappel AND cor_rappeler_le<=NOW() ORDER BY cor_rappeler_le DESC,cor_date,cli_code;";
$req=$Conn->prepare($sql);
$req->bindParam("acc_utilisateur",$acc_utilisateur);
$req->bindParam("acc_societe",$acc_societe);
if(!empty($acc_agence)){
	$req->bindParam("acc_agence",$acc_agence);
}
$req->execute();
$cor = $req->fetchAll(PDO::FETCH_ASSOC);
foreach($cor as $k=>$c){
	//var_dump($c["date_rappel"]);
	$rappel[]=array(
		"rappeler_le" => $c["cor_rappeler_le"],
		"type" => 1,
		"client" => $c["cli_id"],
		"code" => $c["cli_code"],
		"nom" => $c["cli_nom"],
		"correspondance" => $c["cor_id"],
		"date" => $c["cor_date"],
		"commentaire" => $c["cor_commentaire"]
	);
}

// LES SUPECTS
$sql="SELECT sus_id,sus_code,sus_nom,sco_rappeler_le,sco_commentaire,sco_id,sco_date
FROM suspects_correspondances,Suspects
WHERE sco_suspect=sus_id AND sco_utilisateur=:acc_utilisateur
AND NOT ISNULL(sco_rappeler_le) AND NOT sco_rappel AND sco_rappeler_le<=NOW() ORDER BY sco_rappeler_le,sco_date,sus_code;";
$req=$ConnSoc->prepare($sql);
$req->bindParam("acc_utilisateur",$acc_utilisateur);
$req->execute();
$sco = $req->fetchAll(PDO::FETCH_ASSOC);
foreach($sco as $k=>$c){
	$rappel[]=array(
		"rappeler_le" => $c["sco_rappeler_le"],
		"type" => 2,
		"client" => $c["sus_id"],
		"code" => $c["sus_code"],
		"nom" => $c["sus_nom"],
		"correspondance" => $c["sco_id"],
		"date" => $c["sco_date"],
		"commentaire" => $c["sco_commentaire"]
	);
}

// BASE UTILISATEURS
$utilisateurs=orion_utilisateurs();
?>
<!-- EXEMPLE DE FONCTION POUR LES MODELES -->
<div class="panel panel-100" style="height:100%!important;" >
	<div class="panel-heading">
		<span class="panel-icon"><i class="fa fa-phone"></i></span>
		<span class="panel-title">Suspects et Clients à rappeler</span>
	</div>
	<div class="panel-body pn tableDefCont-<?=$num_fct?>" style="height:100%!important;">
<?php	$data_order=3;
		if($modele["mod_type"]<3){
			$data_order=5;
		} ?>
		<table class="table dataTable tableDefFix-<?=$num_fct?>" data-tri_col="<?=$data_order?>" data-tri_sens="asc" style="margin-top:0!important;">
			<thead class="tableDefHead-<?=$num_fct?>" >
				<tr class="dark" >
					<th>Type</th>
					<th>Code</th>
			<?php	if($modele["mod_type"]<3){ ?>
						<th>Nom</th>
						<th>Date</th>
			<?php	} ?>
					<th>Commentaires</th>
					<th>Rappeler le</th>
				</tr>
			</thead>
			<tbody>
<?php 			foreach($rappel as $c){
					$DT_rappeler_le=date_create_from_format('Y-m-d',$c["rappeler_le"]);
					if(!is_bool($DT_rappeler_le)){
						$rappeler_le=$DT_rappeler_le->format("d/m/Y");
					}

					$DT_date=date_create_from_format('Y-m-d',$c["date"]);
					if(!is_bool($DT_date)){
						$date=$DT_date->format("d/m/Y");
					} ?>
					<tr>
						<td>
			<?php 			if($c['type'] ==1){
								$url="client_voir.php?client=" . $c['client'] . "&tab5";  ?>
								Client
			<?php 			}else{
								$url="suspect_voir.php?suspect=" . $c['client']; ?>
								Suspect
			<?php 			} ?>
						</td>
						<td>
							<a href="<?=$url?>" >
								<?=$c['code']?>
							</a>
						</td>
			<?php		if($modele["mod_type"]<3){ ?>
							<td><?=$c['nom']?></td>
							<td>
								<span class="hidden" ><?=$c['date']?></span>
								<?=$date?>
							</td>
			<?php		} ?>
						<td><?= $c['commentaire']?></td>
						<td>
							<span class="hidden" ><?=$c['rappeler_le']?></span>
							<?=$rappeler_le?>
						</td>
					</tr>
		<?php 	} ?>
			</tbody>
		</table>
	</div>
</div>
