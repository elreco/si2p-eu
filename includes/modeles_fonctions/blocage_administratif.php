<?php

$full_acces=array();
$d_actions=array();

$d_agences=array();
$sql_age="SELECT age_id,age_code FROM Agences ORDER BY age_id;";
$req_age=$Conn->query($sql_age);
$results = $req_age->fetchAll(); 
if(!empty($results)){
    foreach($results as $r){

        $d_agences[$r["age_id"]]=$r["age_code"];

    }
}

$sql_soc="SELECT uso_societe,uso_agence,soc_code FROM Utilisateurs_Societes INNER JOIN Societes ON (Utilisateurs_Societes.uso_societe=Societes.soc_id) WHERE uso_utilisateur=" . $_SESSION['acces']['acc_ref_id'] . " ORDER BY uso_societe,uso_agence;";
$req_soc=$Conn->query($sql_soc);
$societes = $req_soc->fetchAll(); 
if(!empty($societes)){
    foreach($societes as $soc){

        $ConnFct=connexion_fct($soc["uso_societe"]);

        if(empty($soc["uso_agence"])){
            $full_acces[$soc["uso_societe"]]=1;
        }

        if( (!empty($full_acces[$soc["uso_societe"]]) AND empty($soc["uso_agence"]) ) OR ( empty($full_acces[$soc["uso_societe"]]) AND !empty($soc["uso_agence"]) ) ) {

            $sql_act="SELECT act_id,act_date_deb,act_agence,act_verrou_bc,act_verrou_marge
            ,'" . $soc["soc_code"] . "' AS soc_code,'" . $soc["uso_societe"] . "' AS soc_id 
            FROM Actions WHERE act_verrou_admin AND NOT act_archive";
            if(!empty($soc["uso_agence"])){
                $sql_act.=" AND act_agence=" . $soc["uso_agence"];
            }
            $sql_act.=" ORDER BY act_date_deb;";
            $req_act=$ConnFct->query($sql_act);
            $_result=$req_act->fetchAll();
            if(!empty($_result)){
              
                $d_actions=array_merge($d_actions,$_result);
             
            }
           

        }
    }

}
?>
<!-- EXEMPLE DE FONCTION POUR LES MODELES -->
<div class="panel panel-100">
    <div class="panel-heading">
        <span class="panel-icon"><i class="fa fa-file"></i></span>
        <span class="panel-title">Actions faisant l'objet d'un blocage administratif</span>
    </div>
    <div class="panel-body pn tableDefCont-<?=$num_fct?>" style="height:100%;">
    <?php   if(count($societes)>1){ ?>
                <p class="alert alert-info mt15" >
                    Connectez-vous sur l'entité concernée pouvoir accéder à l'action.
                </p>
    <?php   } ?>
        <table class="table dataTable tableDefFix-<?=$num_fct?>" data-tri_col="3" data-tri_sens="asc" style="margin-top:0!important;">	
            <thead>
                <tr class="dark">
                    <th style="text-align:center!important;">Societes</th>
                    <th style="text-align:center!important;">Agences</th>
                    <th style="text-align:center!important;">Action</th>
                    <th style="text-align:center!important;">Date de début</th>
                    <th style="text-align:center!important;">Blocage BC</th>
                    <th style="text-align:center!important;">Blocage Marge</th>
                </tr>
            </thead>											
            <tbody> 
        <?php 	foreach($d_actions as $act){ 
                    $act_date_deb="";
                    if(!empty($act["act_date_deb"])){
                        $DT_periode=date_create_from_format('Y-m-d',$act["act_date_deb"]);
                        if(!is_bool($DT_periode)){
                            $act_date_deb=$DT_periode->format("d/m/Y");
                        }
                    }
                    
            ?>
                    <tr>
                        <td  style="text-align:center!important;">
                            <?=$act["soc_code"]?>
                        </td>
                        <td>
                <?php       if(!empty($act["act_agence"])) {
                               echo($d_agences[$act["act_agence"]]);
                            } ?>  
                        </td>
                        <td style="text-align:center!important;">
                <?php       if($act["soc_id"]==intval($_SESSION['acces']["acc_societe"]) AND ( $act["act_agence"]==intval($_SESSION['acces']["acc_agence"]) OR empty(intval($_SESSION['acces']["acc_agence"]) ) ) ){ ?>
                                <a href="action_voir.php?action=<?=$act["act_id"]?>" >
                                    <?=$act["act_id"]?>
                                </a>
                <?php       }else{
                                echo($act["act_id"]);
                            } ?>
                        </td>
                        <td  style="text-align:center!important;" data-sort="<?=$act["act_date_deb"]?>" >
                            <?=$act_date_deb?>
                        </td>
                        <td class="text-center">
                <?php       if($act["act_verrou_bc"]){
                                echo("OUI");
                            } ?>
                        </td>
                        <td class="text-center">
                <?php       if($act["act_verrou_marge"]){
                                echo("OUI");
                            } ?>
                        </td>                  
                    </tr>
        <?php 	} ?>
            </tbody>
        </table>
    </div>
</div>