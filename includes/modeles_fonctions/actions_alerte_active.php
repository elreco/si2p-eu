<?php
// DIPLOME EXPIRANT DANS MOINS DE 2 MOIS POUR DES UTILISATEURS DONT LA SOCIETE ADMINISTRATIVE EST ACCESSIBLE A USER CONNECT

$sql="SELECT act_id,act_adr_ville,act_date_deb
,int_label_1
FROM Actions LEFT OUTER JOIN Intervenants ON (Actions.act_intervenant=Intervenants.int_id)
WHERE NOT act_archive AND (act_alerte_ok = 0 OR act_alerte_ok IS NULL) AND (act_alerte_date <= NOW() AND act_alerte_date IS NOT NULL)
ORDER BY act_date_deb,act_intervenant,act_id";
$req=$ConnSoc->query($sql);
$actions = $req->fetchAll();

?>

<div class="panel panel-100">
	<div class="panel-heading">
		<span class="panel-icon">
			<span class="glyphicon glyphicon-calendar" ></span>
		</span>
		<span class="panel-title">Liste des actions en "alerte"</span>
	</div>
	<div class="panel-body pn tableDefCont-<?=$num_fct?>" style="height:100%;">
<?php	$data_order=0; ?>
		<table class="table dataTable tableDefFix-<?=$num_fct?>" data-tri_col="<?=$data_order?>" data-tri_sens="asc" style="margin-top:0!important;">
			<thead class="tableDefHead-<?=$num_fct?>" >
				<tr class="dark">
					<th>Date</th>
					<th>Intervenant</th>
					<th>Lieu</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
	<?php		foreach($actions as $a){
					$DT_date=date_create_from_format('Y-m-d',$a["act_date_deb"]);
					if(!is_bool($DT_date)){
						$act_date_deb=$DT_date->format("d/m/Y");
					} ?>
					<tr>
						<td data-order="<?=$a["act_date_deb"]?>" ><?=$act_date_deb?></td>
						<td><?=$a["int_label_1"]?></td>
						<td><?=$a["act_adr_ville"]?></td>
						<td class="text-center" >
							<a href="action_voir.php?action=<?=$a["act_id"]?>" class="btn btn-sm btn-info" >
								<i class="fa fa-eye" ></i>
							</a>
						</td>
					</tr>
	<?php		} ?>
			</tbody>
		</table>
	</div>
</div>
