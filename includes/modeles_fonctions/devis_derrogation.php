<?php 
$sql="SELECT cli_code,cli_nom,dev_id,dev_numero,dev_date,dev_prospect,dev_esperance,dev_total_ht,dev_att_derogation
,com_label_1
FROM Devis,Clients,Commerciaux WHERE dev_client=cli_id AND dev_commercial=com_id AND dev_esperance != 0 ";
if(!empty($acc_agence)){
	$sql.=" AND dev_agence=" . $acc_agence;
}
if(!$_SESSION["acces"]["acc_droits"][6]){
	$sql.=" AND dev_utilisateur=" . $acc_utilisateur;
}
$sql .= "  AND dev_att_derogation>0";
$sql.=" ORDER BY dev_date,dev_chrono;";
$req=$ConnSoc->query($sql);
$devis = $req->fetchAll();

?>
<div class="panel panel-100">
	<div class="panel-heading">
		<span class="panel-icon"><i class="fa fa-euro"></i></span>
		<span class="panel-title">Liste des devis en attente de dérogation</span>
	</div>
	<div class="panel-body pn tableDefCont-<?=$num_fct?>" style="height:100%;">
	
<?php	$data_order=1;
		if($modele['mod_type']<3){
			$data_order=2;
		} ?>
		<table class="table dataTable tableDefFix-<?=$num_fct?>" data-tri_col="<?=$data_order?>" data-tri_sens="asc" style="margin-top:0!important;">		
			
			<thead class="tableDefHead-<?=$num_fct?>" >
				<tr class="dark" >
					<th>Code</th>
			<?php 	if($modele['mod_type']<3){ ?>
						<th>Nom</th> 
			<?php	} ?>			
					<th>Numéro</th>
					<th>Montant HT</th>
					<th>Niv.</th>
			<?php 	if($modele['mod_type']<3){ ?>
						<th>Date</th>
						<th>Commercial</th>
			<?php 	}?>												
				</tr>
			</thead>											
			<tbody> 
	<?php		foreach($devis as $d){
					$DT_date=date_create_from_format('Y-m-d',$d["dev_date"]);
					if(!is_bool($DT_date)){
						$dev_date=$DT_date->format("d/m/Y");
					}  ?>										
					<tr>
						<td><?=$d['cli_code']?></td>
				<?php 	if($modele['mod_type']<3){ ?>
							<td><?=$d['cli_nom']?></td>
				<?php	} ?>
						<td>
							<a href="devis_voir.php?devis=<?=$d['dev_id']?>" >
								<?=$d['dev_numero']?>
							</a>
						</td>
						<td class="text-right" ><?=$d['dev_total_ht']?></td>
						<td class="text-center" ><?=$d['dev_att_derogation']?></td>
				<?php 	if($modele['mod_type']<3){ ?>
							<td data-order="<?=$d["dev_date"]?>" >						
								<?=$dev_date?>
							</td>
							<td><?=$d['com_label_1']?></td>
				<?php	} ?>
					</tr>
		 <?php 	} 	?>
			</tbody>
		</table>
	</div>
</div>
