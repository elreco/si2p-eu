<?php 
// DIPLOME EXPIRANT DANS MOINS DE 2 MOIS POUR DES UTILISATEURS DONT LA SOCIETE ADMINISTRATIVE EST ACCESSIBLE A USER CONNECT
$pastDate=date('Y-m-d', strtotime('-1 year'));
$sql="SELECT cli_code,cli_nom,dev_id,dev_numero,dev_date,dev_prospect,dev_esperance,dev_total_ht
FROM Devis,Clients,Commerciaux WHERE dev_client=cli_id AND dev_commercial=com_id AND NOT dev_esperance=0 AND NOT dev_esperance=100 AND dev_date > '" . $pastDate . "'";
if(!$_SESSION["acces"]["acc_droits"][6]){
	$sql.=" AND dev_utilisateur=" . $acc_utilisateur;
}
if(!empty($acc_agence)){
	$sql.=" AND dev_agence=" . $acc_agence;
}
$sql.=" ORDER BY dev_date,dev_chrono;";
/* var_dump($sql);
die(); */
$req=$ConnSoc->query($sql);
$devis = $req->fetchAll();
?>
<div class="panel panel-100">
	<div class="panel-heading">
		<span class="panel-icon"><i class="fa fa-euro"></i></span>
		<span class="panel-title">Liste des devis en attente de validation</span>
	</div>
	<div class="panel-body pn tableDefCont-<?=$num_fct?>" style="height:100%;">
<?php	$data_order=2;
		if($modele["mod_type"]<3){
			$data_order=3;
		} ?>
		<table class="table dataTable tableDefFix-<?=$num_fct?>" data-tri_col="<?=$data_order?>" data-tri_sens="asc" style="margin-top:0!important;">				
			<thead class="tableDefHead-<?=$num_fct?>" >
				<tr class="dark" >                   
					<th>Code</th>
		<?php		if($modele["mod_type"]<3){ ?>
						<th>Nom</th> 
		<?php		} ?>					
					<th>Devis</th>
					<th>Date</th>
					<th>HT</th>
					<th>Fiabilité</th>										
				</tr>
			</thead>											
			<tbody> 
		<?php	foreach($devis as $d){
			
					$DT_date=date_create_from_format('Y-m-d',$d["dev_date"]);
					if(!is_bool($DT_date)){
						$dev_date=$DT_date->format("d/m/Y");
					} ?>										
					<tr>
						<td><?=$d['cli_code']?></td>
			<?php		if($modele["mod_type"]<3){ ?>
							<td><?=$d['cli_nom']?></td>
			<?php		} ?>
						<td>
							<a href="devis_voir.php?devis=<?=$d['dev_id']?>" >
								<?=$d['dev_numero']?>
							</a>
						</td>
						<td data-order="<?=$d["dev_date"]?>" >						
							<?=$dev_date?>
						</td>
						<td class="text-right" ><?=$d['dev_total_ht']?></td>
			<?php		switch ($d['dev_esperance']){
							case 30:
								echo("<td class='bg-danger' >30%</td>");
								break;
							case 50:
								echo("<td class='bg-warning' >50%</td>");
								break;	
							case 90:
								echo("<td class='bg-success' >90%</td>");
								break;									
							default:
								echo("<td>&nbsp;</td>");
						}	?>
					</tr>
		<?php 	} 	?>			
			</tbody>
		</table>
	</div>
</div>