<?php 

	// Factures dues

	$ca_du=0;
	$ca_echu=0;
	$ca_stop=0;

	$sql_fac_due="SELECT fac_total_ttc,fac_date_reg_prev,fac_relance_stop FROM Factures 
	LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
	WHERE fac_nature=1 AND fac_regle<fac_total_ttc";
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql_fac_due.=" AND com_ref_1=" . $acc_utilisateur;
	}
	if(!empty($acc_agence)){
		$sql_fac_due.=" AND fac_agence=" . $acc_agence;
	}
	$req_fac_dues=$ConnSoc->query($sql_fac_due);
	$d_fac_dues=$req_fac_dues->fetchAll(); 
	if(!empty($d_fac_dues)){
		foreach($d_fac_dues as $fac){
			
			$ca_du=$ca_du + $fac["fac_total_ttc"];

			if($fac["fac_date_reg_prev"]<=date("Y-m-d")){
				$ca_echu=$ca_echu + $fac["fac_total_ttc"];

				if($fac["fac_relance_stop"] == 1 ){
					$ca_stop=$ca_stop + $fac["fac_total_ttc"];
				}
			}

			
		}
	}

	// factures régle

	$sql_fac_reglees="SELECT SUM(DATEDIFF(fac_date_reg,fac_date)) as nb_j, COUNT(fac_id) as nb_fac FROM Factures 
	LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
	WHERE fac_nature=1 AND fac_regle=fac_total_ttc AND YEAR(fac_date)=" . date("Y");
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql_fac_reglees.=" AND com_ref_1=" . $acc_utilisateur;
	}
	if(!empty($acc_agence)){
		$sql_fac_reglees.=" AND fac_agence=" . $acc_agence;
	}
	$req_fac_reglees=$ConnSoc->query($sql_fac_reglees);
	$d_fac_reglees=$req_fac_reglees->fetch(); 
	

	// CA des actions non facturées terminées à la date d'hier

	$sql_action_hier="SELECT SUM(acl_ca) FROM Actions_Clients
	LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
	LEFT JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)
	INNER JOIN (
		SELECT acd_action_client, MAX(pda_date) AS max_date FROM Actions_Clients_Dates,plannings_dates WHERE acd_date=pda_id GROUP BY acd_action_client) AS groupe
	ON Actions_Clients.acl_id = groupe.acd_action_client
	WHERE NOT acl_facture AND NOT acl_archive AND NOT act_archive AND NOT acl_non_fac AND acl_confirme
	AND max_date<'" . date("Y-m-d") . "'";
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql_action_hier.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif(!empty($acc_agence)){
		$sql_action_hier.=" AND act_agence=" . $acc_agence;
	}
	$sql_action_hier.="";
	$req_action_hier=$ConnSoc->query($sql_action_hier);
	$d_action_hier=$req_action_hier->fetch();
	$ca_action_hier=0;
	if(!empty($d_action_hier)){
		if(!empty($d_action_hier[0])){
			$ca_action_hier=floatval($d_action_hier[0]);
		}
	}

	// CA des actions non facturées terminées à la date d'aujourd'hui

	$sql="SELECT SUM(acl_ca) FROM Actions_Clients
	LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
	LEFT JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)
	INNER JOIN (
		SELECT acd_action_client, MAX(pda_date) AS max_date FROM Actions_Clients_Dates,plannings_dates WHERE acd_date=pda_id GROUP BY acd_action_client) AS groupe
	ON Actions_Clients.acl_id = groupe.acd_action_client
	WHERE NOT acl_facture AND NOT acl_archive AND NOT act_archive AND NOT acl_non_fac AND acl_confirme
	AND max_date<='" . date("Y-m-d") . "'";
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}elseif(!empty($acc_agence)){
		$sql.=" AND act_agence=" . $acc_agence;
	}
	$sql.="";
	$req=$ConnSoc->query($sql);
	$d_action_ajd=$req->fetch();
	$ca_action_ajd=0;
	if(!empty($d_action_ajd)){
		if(!empty($d_action_ajd[0])){
			$ca_action_ajd=floatval($d_action_ajd[0]);
		}
	}

	?>
	  
	<div class="panel" >
    	<div class="panel-heading">
    		<span class="panel-title">Facturation</span>
    	</div>
    	<div class="panel-body" >
    		<div class="mb20 text-right">
		<?php	if(!empty($ca_echu)) { ?>
					<span class="fs11 text-muted ml10">
						<i class="fa fa-circle text-warning fs12 pr5" ></i> Taux de factures en blocage "Agence"
					</span>
		<?php	} ?>

		<?php	if(!empty($ca_du)) { ?>
					<span class="fs11 text-muted ml10">
						<i class="fa fa-circle text-info fs12 pr5" ></i> Taux de retard de règlements clients
					</span>
		<?php	} ?>

				<span class="fs11 text-muted ml10">
					<i class="fa fa-circle text-success fs12 pr5" ></i> Taux de retard de facturation
				</span>

    		</div>
			<div class="row" >
		<?php	if(!empty($ca_echu)) { ?>
					<div class="col-xs-3 text-center">
						<div class="info-circle" id="c1" title="" value="<?=($ca_stop/$ca_echu)*100?>" data-circle-color="warning"></div>	
					</div>
		<?php	}
				if(!empty($ca_du)) { ?>
					<div class="col-xs-3 text-center">
						<div class="info-circle" id="c2" title="" value="<?=($ca_echu/$ca_du)*100?>" data-circle-color="info"></div>	
					</div>
		<?php	}
				if(!empty($ca_action_ajd)){ ?>
					<div class="col-xs-3 text-center">
						<div class="info-circle" id="c3" title="" value="<?=($ca_action_hier/$ca_action_ajd)*100?>" data-circle-color="success"></div>	
					</div>
		<?php	} ?>
				<div class="col-xs-3 text-center" id="delai_cont" >
					
					<h3>Délai de paiement</h3>
			<?php	if(!empty($d_fac_reglees)) { 
						if(!empty($d_fac_reglees["nb_fac"])){ ?>
							<h1><?=number_format($d_fac_reglees["nb_j"]/$d_fac_reglees["nb_fac"],2,","," ")?> jours</h1>	
			<?php		}else{
							echo("<p>Pas de facture</p>");
						}
					} ?>				
				</div>
			</div>

			<div class="row" >
		<?php	if(!empty($ca_echu)) { ?>
					<div class="col-xs-3 text-center">
						<a href='fac_ratio_retard_reg.php?taux=1' class='btn btn-sm btn-default' >Détail</a>	
					</div>
		<?php	}
				if(!empty($ca_du)) { ?>
					<div class="col-xs-3 text-center">
						<a href='fac_ratio_retard_reg.php?taux=2' class='btn btn-sm btn-default' >Détail</a>		
					</div>
		<?php	}
				if(!empty($ca_action_ajd)){ ?>
					<div class="col-xs-3 text-center">
			<?php		if(!$_SESSION["acces"]["acc_droits"][35]){
							echo("<a href='fac_ratio_retard_fac_detail.php' class='btn btn-sm btn-default' >Détail</a>	");
						}else{
							echo("<a href='fac_ratio_retard_fac.php' class='btn btn-sm btn-default' >Détail</a>	");
						} ?>
							
					</div>
		<?php	} ?>
				<div class="col-xs-3 text-center" id="delai_cont" >
		<?php		if(!$_SESSION["acces"]["acc_droits"][35]){
						echo("<a href='fac_ratio_delai_reg_detail.php' class='btn btn-sm btn-default' >Détail</a>");
					}else{
						echo("<a href='fac_ratio_delai_reg.php' class='btn btn-sm btn-default' >Détail</a>");
					} ?>
						
				</div>
			</div>
   	 	</div>
  	</div>