<?php

$sql="SELECT * FROM evacuations WHERE (eva_tech_valide_date IS NULL AND eva_etat_valide=1 AND eva_tech = " . $_SESSION['acces']['acc_ref_id'] . ") OR (eva_assist_valide_date IS NULL AND eva_etat_valide=1 AND eva_assist = " . $_SESSION['acces']['acc_ref_id'] . ")";
$req=$Conn->query($sql);
$evacuations = $req->fetchAll();


?>

<!-- EXEMPLE DE FONCTION POUR LES MODELES -->
	<div class="panel panel-100">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-file"></i></span>
			<span class="panel-title">Liste des rapports d'évacuation à valider</span>
		</div>
		<div class="panel-body pn tableDefCont-<?=$num_fct?>" style="height:100%;">
	<?php	$data_order=1; ?>
			<table class="table dataTable tableDefFix-<?=$num_fct?>" data-tri_col="<?=$data_order?>" data-tri_sens="desc" style="margin-top:0!important;">
				<thead>
					<tr class="dark">
						<th class="text-center">Date exercice</th>
						<th class="text-center">Terminé le</th>
						<th class="text-center">Client</th>
						<th class="text-center">Intervenant</th>
						<th class="text-center">Contact</th>
						<th class="text-center no-sort">Actions</th>
					</tr>
				</thead>
				<tbody>
			<?php 	foreach($evacuations as $e){
				// INTERVENANT
				$req = $Conn->prepare("SELECT uti_nom, uti_prenom FROM utilisateurs WHERE uti_id=" . $e["eva_formateur"] . ";");
				$req->execute();
				$intervenant = $req->fetch();?>
				<tr  id="tr_<?= $e['eva_id'] ?>">
					<td  data-order="<?= $e['eva_date_exer'] ?>"><?= convert_date_txt($e['eva_date_exer']) ?></td>
					<td data-order="<?= $e['eva_date_termine'] ?>"><?= convert_date_txt($e['eva_date_termine']) ?></td>
					<td><?= $e['eva_client_nom'] ?></td>
					<td><?= $intervenant['uti_prenom'] ?> <?= $intervenant['uti_nom'] ?></td>
					<td><?= $e['eva_contact_nom'] ?></td>



					<td class="text-center">
						<a href="evac_voir.php?id=<?= $e['eva_id'] ?>" class="btn btn-info btn-xs">
							<i class="fa fa-eye"></i>
						</a>
					</td>
				</tr>
			<?php 	} ?>
				</tbody>
			</table>
		</div>
	</div>
