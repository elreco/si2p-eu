<?php 
$sql="SELECT *
 FROM utilisateurs_societes WHERE uso_utilisateur = " . $_SESSION['acces']['acc_ref_id'];
$req=$Conn->query($sql);
$utilisateurs_societes = $req->fetchAll();
// CONSTRUIRE LES VARIABLES $societes ET $agences
foreach($utilisateurs_societes as $uso){
	if(empty($societes)){
		$societes = $uso['uso_societe'];
	}else{
		$societes = $societes . "," . $uso['uso_societe'];
	}

	if(empty($societes)){
		$agences = $uso['uso_societe'];
	}else{
		$agences = $societes . "," . $uso['uso_agence'];
	}
}

// RECHERCHER LES UTILISATEURS
if(!empty($utilisateurs_societes)){
	$sql="SELECT DISTINCT uti_id,uti_nom,uti_prenom,uti_matricule,uti_tel,uti_fax,uti_mail
	,soc_nom,age_nom,pro_libelle 
	 FROM utilisateurs  
	LEFT OUTER JOIN Societes ON (Utilisateurs.uti_societe=Societes.soc_id)
	LEFT OUTER JOIN Agences ON (Utilisateurs.uti_agence=Agences.age_id)
	LEFT OUTER JOIN Profils ON (Utilisateurs.uti_profil=Profils.pro_id)
	 WHERE uti_societe IN (" . $societes . ") AND uti_agence IN (" . $agences . ") ORDER BY uti_nom, uti_prenom;";
	$req=$Conn->query($sql);
	$utilisateurs = $req->fetchAll();
}else{
	$utilisateurs = array();
}

?>

<!-- EXEMPLE DE FONCTION POUR LES MODELES -->
	<div class="panel panel-100">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-users"></i></span>
			<span class="panel-title">Liste des utilisateurs dont j'ai accès</span>
		</div>
		<div class="panel-body pn tableDefCont" style="height:100%;">
			<table class="table  dataTable tableDefFix" style="height:100%;">		
				<thead>
					<tr class="dark">
					<?php	if($_SESSION["acces"]["acc_service"][4]==1){
								echo("<th>Matricule</th>");
							} ?>
							<th>Nom</th>
							<th>Prénom</th>
							<th>Société</th>
							<th>Agence</th>
					<?php	if($_SESSION["acces"]["acc_service"][4]==1){
								echo("<th>Profil</th>");
							} ?>
							<th class="no-sort">Action(s)</th>
					</tr>
				</thead>											
				<tbody> 
			<?php			
							if(!empty($utilisateurs)){
								foreach($utilisateurs as $u){ ?>
									<tr>
							<?php		if($_SESSION["acces"]["acc_service"][4]==1){
											echo("<td>" . $u["uti_matricule"] . "</td>");
										} ?>
										<td><?=$u["uti_nom"]?></td>
										<td><?=$u["uti_prenom"]?></td>
										<td><?=$u["soc_nom"]?></td>
										<td><?=$u["age_nom"]?></td>
							<?php		if($_SESSION["acces"]["acc_service"][4]==1){
											echo("<td>" . $u["pro_libelle"] . "</td>");
										} ?>
										<td class="text-center">
										<?php 
										/*echo("<pre>");
										var_dump($_SESSION["acces"]["acc_droits"]);
										echo("</pre>");
										die();*/ ?>
									<?php	if($_SESSION["acces"]["acc_droits"][22]==1){ ?>	
												<a href="utilisateur_voir.php?utilisateur=<?=$u["uti_id"]?>" class="btn btn-info btn-sm" data-toggle="tooltip"
												   data-placement="bottom" title="Consulter la fiche de cet utilisateur" role="button">
													<span class="fa fa-eye"></span>
												</a>
									<?php	}
											if($_SESSION["acces"]["acc_droits"][21]==1){ ?>											
												<a href="utilisateur.php?utilisateur=<?=$u["uti_id"]?>" class="btn btn-warning btn-sm" data-toggle="tooltip"
												   data-placement="bottom" title="Modifier la fiche de cet utilisateur">
													<span class="fa fa-pencil"></span>
												</a>
									<?php 	} ?>
										</td>
									</tr>									
					<?php		}
							} ?>    
				</tbody>
			</table>
		</div>
	</div>