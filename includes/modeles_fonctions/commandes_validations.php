<?php 

$sql="SELECT com_id,cva_commande,com_date,com_annule, com_donneur_ordre, uti_nom, uti_prenom, com_numero,com_fou_nom, com_fou_code, com_ht, com_ttc, com_etat 
FROM commandes_validations 
LEFT JOIN commandes ON commandes.com_id=commandes_validations.cva_commande 
LEFT JOIN utilisateurs ON utilisateurs.uti_id = commandes.com_donneur_ordre 
LEFT JOIN utilisateurs_droits ON commandes_validations.cva_droit = utilisateurs_droits.udr_droit AND utilisateurs_droits.udr_utilisateur=" . $_SESSION['acces']['acc_ref_id'] . "
WHERE (cva_date is null or cva_date = '') AND com_etat = 1 AND com_annule = 0 
AND (cva_profil = ". $_SESSION['acces']['acc_profil'] . " OR cva_utilisateur = ". $_SESSION['acces']['acc_ref_id'] . " OR NOT ISNULL(udr_droit) ) 
ORDER BY com_date DESC";
$req=$Conn->query($sql);
$commandes_validations = $req->fetchAll();
$commandes = array();
foreach($commandes_validations as $k=>$cva){
	$commandes[$cva['cva_commande']] = $commandes_validations[$k];
}

?>

<!-- EXEMPLE DE FONCTION POUR LES MODELES -->
	<div class="panel panel-100">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-file"></i></span>
			<span class="panel-title">Liste des commandes en attente de validation</span>
		</div>
		<div class="panel-body pn tableDefCont-<?=$num_fct?>" style="height:100%;">
	<?php	$data_order=1; ?>
			<table class="table dataTable tableDefFix-<?=$num_fct?>" data-tri_col="<?=$data_order?>" data-tri_sens="desc" style="margin-top:0!important;">	
				<thead>
					<tr class="dark">
						<th>Numéro</th>
						<th>Date</th> 
						<th style="max-width:25%;">Fournisseur</th>
						<th>H.T. (€)</th>
						<th>T.T.C. (€)</th>
						<th>Ordre</th>
					</tr>
				</thead>											
				<tbody> 
			<?php 	foreach($commandes as $c){ ?>
						<tr>
							<td>
									<a href="commande_voir.php?commande=<?= $c['com_id'] ?>"><?= $c['com_numero'] ?></a>
							</td>
							<td data-sort="<?= $c['com_date'] ?>">
								<?= convert_date_txt($c['com_date']) ?>
							</td>
							<td>
								<?= $c['com_fou_nom'] ?> (<?= $c['com_fou_code'] ?>)
							</td>
							<td>

								<?= number_format($c['com_ht'], 2, ',', ' ') ?>
							</td> 
							<td>
								
								<?php 
									if(!empty($c['com_annule'])){
										echo "-";
									}else{
										switch ($c['com_etat']) {
											case 0:
												echo "-";
												break;
											case 1:
												echo number_format($c['com_ttc'], 2, ',', ' ');
												break;
											case 2:
												echo number_format($c['com_ttc'], 2, ',', ' ');
												break;
											case 3:
												echo number_format($c['com_ttc'], 2, ',', ' ');
												break;
										}
									}
									
								?>
								
							</td>
							<td>
								<?= $c['uti_prenom'] ?> <?= $c['uti_nom'] ?>
							</td>  
								

						</tr>
			<?php 	} ?>
				</tbody>
			</table>
		</div>
	</div>