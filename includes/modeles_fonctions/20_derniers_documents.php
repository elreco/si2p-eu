<?php
$sql="SELECT doc_id,doc_nom,doc_nom_aff,doc_date_creation,uti_prenom,uti_nom
 FROM documents 
LEFT JOIN documents_utilisateurs ON (documents.doc_id = documents_utilisateurs.dut_document)
LEFT OUTER JOIN utilisateurs ON (utilisateurs.uti_id=documents.doc_utilisateur)
WHERE uti_archive = 0 AND dut_utilisateur = " . $_SESSION['acces']['acc_ref_id'] . " ORDER BY doc_date_creation DESC LIMIT 20;";
$req=$Conn->query($sql);
$documents = $req->fetchAll();
?>
<!-- EXEMPLE DE FONCTION POUR LES MODELES -->
<div class="panel panel-100">
    <div class="panel-heading">
        <span class="panel-icon"><i class="fa fa-file"></i></span>
        <span class="panel-title">Liste des 20 derniers documents chargés</span>
    </div>
    <div class="panel-body pn tableDefCont-<?=$num_fct?>" style="height:100%;">
<?php	$data_order=1; ?>
        <table class="table dataTable tableDefFix-<?=$num_fct?>" data-tri_col="<?=$data_order?>" data-tri_sens="asc" style="margin-top:0!important;">	
            <thead>
                <tr class="dark">
                    <th style="text-align:center!important;">Nom</th>
                    <th style="text-align:center!important;">Date de dépôt</th>
                    <th style="text-align:center!important;">Déposé par</th>
                    <th style="text-align:center!important;">Télécharger</th>
                </tr>
            </thead>											
            <tbody> 
        <?php 	foreach($documents as $d){ ?>
                    <tr>
                        <td  style="text-align:center!important;">
                            <?= $d['doc_nom_aff'] ?>
                        </td>
                        <td style="text-align:center!important;" data-order="<?=$d['doc_date_creation']?>" ><?= convert_date_txt($d['doc_date_creation']) ?></td>
                        <td style="text-align:center!important;">
                            <?=$d['uti_prenom'] ?> <?=$d['uti_nom'] ?>
                        </td>
                        <td style="text-align:center!important;">
                            <span data-toggle="tooltip" title="Télécharger">
                                <a href="documents/etech/<?= $d['doc_nom'] ?>" class="btn btn-warning btn-sm telecharger-fichier" data-id="<?= $d['doc_id'] ?>" download>
                                    <i class="fa fa-download" aria-hidden="true"></i>
                                </a>
                            </span>
                        </td>
                            

                    </tr>
        <?php 	} ?>
            </tbody>
        </table>
    </div>
</div>