<?php 

$_SESSION["retourUtilisateur"]="accueil.php?menu=" . $mod_menu;


// DIPLOME EXPIRANT DANS MOINS DE 2 MOIS POUR DES UTILISATEURS DONT LA SOCIETE ADMINISTRATIVE EST ACCESSIBLE A USER CONNECT
$sql="SELECT idi_date_deb,idi_ref_id, idi_date_fin,dip_libelle,idi_fichier,dip_id, uti_id,uti_nom,uti_prenom FROM diplomes 
LEFT JOIN intervenants_diplomes ON (diplomes.dip_id = intervenants_diplomes.idi_diplome)
 LEFT JOIN utilisateurs ON (utilisateurs.uti_id=intervenants_diplomes.idi_ref_id AND idi_ref = 1)
WHERE  idi_date_fin < DATE_ADD(NOW(), INTERVAL +8 MONTH) AND NOT uti_archive";

// Hors service tech on affiche uniquement les utilisateurs de la soc logué
if($_SESSION['acces']['acc_service'][3]!=1){
	$sql.=" AND uti_societe=" . $acc_societe;
	if(!empty($acc_agence)){
		$sql.=" AND uti_agence=" . $acc_agence;
	}
}
$sql.=" ORDER BY dip_libelle";
$req=$Conn->query($sql);
$diplomes = $req->fetchAll();
?>

<!-- EXEMPLE DE FONCTION POUR LES MODELES -->
	<div class="panel panel-100">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-file-pdf-o"></i></span>
			<span class="panel-title">Liste des diplômes arrivant à échéance dans moins de 8 mois</span>
		</div>
		<div class="panel-body pn tableDefCont-<?=$num_fct?>" style="height:100%;">
	<?php	$data_order=2; ?>
			<table class="table dataTable tableDefFix-<?=$num_fct?>" data-tri_col="<?=$data_order?>" data-tri_sens="asc" style="margin-top:0!important;">							
				<thead class="tableDefHead-<?=$num_fct?>" >
					<tr class="dark">
						<th style="text-align:center!important;">Utilisateur</th>
						<th style="text-align:center!important;">Nom du diplôme</th>
						<th style="text-align:center!important;">Fin de validité</th>
						<th style="text-align:center!important;">Pdf</th>
					</tr>
				</thead>											
				<tbody> 
		<?php 		foreach($diplomes as $d){ 
						$class_perime="";
						if(!empty($d["idi_date_fin"])){
							$idi_date_fin=convert_date_txt($d["idi_date_fin"]);
							if($d["idi_date_fin"]<date("Y-m-d")){
								$class_perime="danger";	
								//$style_perime="style='color:#FFF;'";
							}
						}	?>
						<tr class="<?= $class_perime ?>">
							<td  style="text-align:center!important;">
				<?php			if($_SESSION["acces"]["acc_droits"][22]){ ?>
									<a href="utilisateur_voir.php?utilisateur=<?=$d['uti_id']?>" >
										<?= $d['uti_prenom'] ?> <?= $d['uti_nom'] ?>
									</a>
				<?php			}else{
									echo($d['uti_prenom'] . " " . $d['uti_nom']);
								} ?>
							</td>
							<td style="text-align:center!important;"><?=$d["dip_libelle"]?></td>
							<td style="text-align:center!important;" data-order="<?=$d['idi_date_fin']?>" >
								<?=convert_date_txt($d['idi_date_fin']) ?>
							</td>
							<?php					if(!empty($d["idi_fichier"]) AND file_exists("documents/utilisateurs/diplomes/" . $d["idi_fichier"])){ ?>
								<td style="text-align:center!important;">
									<a href="documents/utilisateurs/diplomes/<?=$d["idi_fichier"]?>" target="_blank" class="btn btn-success btn-sm">
										<i class="fa fa-file-pdf-o"></i>
									</a>
								</td>
		<?php					}else{
								echo("<td style='text-align:center!important;'>&nbsp;</td>");
							}; ?>

						</tr>
			<?php 	} ?>
				</tbody>
			</table>
		</div>
	</div>
