<?php 

	
$sql="SELECT act_id,act_date_deb
,acl_id,acl_pro_reference,acl_ca
,cli_nom,cli_code
,com_label_1
,int_label_1
 FROM Actions_Clients INNER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
INNER JOIN Plannings_Dates ON (Actions_Clients_Dates.acd_date=pda_id)
INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
LEFT JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)
LEFT JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)
LEFT JOIN Intervenants ON (Actions.act_intervenant=Intervenants.int_id)
WHERE NOT acl_facture AND act_date_deb>=NOW()
ORDER BY act_date_deb,act_id,acl_id;";
$req=$ConnSoc->query($sql);
$mod_actions_fac = $req->fetchAll();

?>

<!-- EXEMPLE DE FONCTION POUR LES MODELES -->
	<div class="panel panel-100">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-file"></i></span>
			<span class="panel-title">Actions à facturer</span>
		</div>
		<div class="panel-body pn tableDefCont" style="height:100%;">
			<table class="table  dataTable tableDefFix" style="height:100%;">		
				<thead>
					<tr class="dark">
						<th style="text-align:center!important;">ID</th>
						<th style="text-align:center!important;">Code client</th>
						<th style="text-align:center!important;">Nom client</th>
						<th style="text-align:center!important;">Commercial</th>
						<th style="text-align:center!important;">Produit</th>
						<th style="text-align:center!important;">Intervenant</th>
						<th style="text-align:center!important;">Date de début</th>
						<th style="text-align:center!important;">Date de fin</th>
						<th style="text-align:center!important;">CA</th>
					</tr>
				</thead>											
				<tbody> 
			<?php 	foreach($mod_actions_fac as $maf){ ?>
						<tr>
							<td  style="text-align:center!important;">
								<?=$maf['act_id'] . "-" . $maf['acl_id']?>
							</td>
							<td style="text-align:center!important;"><?=$maf['cli_code']?></td>
							<td style="text-align:center!important;"><?=$maf['cli_nom']?></td>
							<td style="text-align:center!important;"><?=$maf['com_label_1']?></td>
							<td style="text-align:center!important;"><?=$maf['acl_pro_reference']?></td>
							<td style="text-align:center!important;"><?=$maf['int_label_1']?></td>
							<td style="text-align:center!important;"><?=$maf['act_date_deb']?></td>
							<td style="text-align:center!important;"></td>
							<td style="text-align:center!important;"><?=$maf['acl_ca']?></td>							
							<td style="text-align:center!important;">								
								<a href="facture.php?action_client=<?=$maf['acl_id']?>" class="btn btn-warning btn-sm" >
									<i class="fa fa-doc" ></i>
								</a>							
							</td>
								

						</tr>
			<?php 	} ?>
				</tbody>
			</table>
		</div>
	</div>