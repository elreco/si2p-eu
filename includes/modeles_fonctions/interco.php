<?php
// INTERCOS
$ddateinterco = date('Y-m-d', strtotime('+10 days'));
$sql="SELECT DISTINCT acl_pro_reference, cli_id,cli_nom,acl_id,act_id, cli_interco_soc,act_id,act_adr_ville,act_date_deb
,int_label_1
FROM Actions LEFT OUTER JOIN Intervenants ON (Actions.act_intervenant=Intervenants.int_id)
LEFT JOIN Actions_Clients ON (Actions_Clients.acl_action = Actions.act_id)
INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id  AND cli_interco_soc != 0 )
WHERE NOT act_archive AND act_date_deb >= NOW() AND act_date_deb <= '" . $ddateinterco . "'
ORDER BY act_date_deb,act_intervenant,act_id";
$req=$ConnSoc->query($sql);
$actions = $req->fetchAll();

?>

<div class="panel panel-100">
	<div class="panel-heading">
		<span class="panel-icon">
			<span class="glyphicon glyphicon-calendar" ></span>
		</span>
		<span class="panel-title">Liste des actions intercos qui ne sont pas liées</span>
	</div>
	<div class="panel-body pn tableDefCont-<?=$num_fct?>" style="height:100%;">
<?php	$data_order=1; ?>
		<table class="table dataTable tableDefFix-<?=$num_fct?>" data-tri_col="<?=$data_order?>" data-tri_sens="asc" style="margin-top:0!important;">
			<thead class="tableDefHead-<?=$num_fct?>" >
				<tr class="dark">
                    <th>ID Action-ID Inscription Client</th>
					<th>Date</th>
					<th>Intervenant</th>
                    <th>Code produit</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
	<?php		foreach($actions as $a){
					$DT_date=date_create_from_format('Y-m-d',$a["act_date_deb"]);
					if(!is_bool($DT_date)){
						$act_date_deb=$DT_date->format("d/m/Y");
					} ?>
					<tr>
                        <td><?= $a['act_id'] ?>-<?= $a['acl_id'] ?></td>
						<td data-order="<?=$a["act_date_deb"]?>" ><?=$act_date_deb?></td>
						<td><?=$a["int_label_1"]?></td>
                        <td><?= $a['acl_pro_reference'] ?></td>
						<td class="text-center" >
							<a href="action_voir.php?action=<?=$a["act_id"]?>&action_client=<?= $a['acl_id'] ?>" class="btn btn-sm btn-info" >
								<i class="fa fa-eye" ></i>
							</a>
						</td>
					</tr>
	<?php		} ?>
			</tbody>
		</table>
	</div>
</div>
