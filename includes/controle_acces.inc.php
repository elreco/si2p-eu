<?php
session_start();
if (file_exists(__DIR__ . "/vendor/autoload.php")) {
	require_once(__DIR__ . "/vendor/autoload.php");
	$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
} else if (file_exists("./vendor/autoload.php")) {
	require_once("./vendor/autoload.php");
	$dotenv = Dotenv\Dotenv::createImmutable("./");
	$dotenv->load();
}

if(!isset($_SESSION["acces"]['acc_timer'])){
	// MARCHE PAS CAR PAS DE DESTROY AUTO COMME EN ASP
	header("location : index.php");
	exit();
}
if(time()-$_SESSION["acces"]['acc_timer']>1200)
{	header('Location: deconnect.php');
	exit();
}else{
	$_SESSION['acces']['acc_timer']=time();
}
if($_SESSION["acces"]['acc_ref'] != 1){
	header('Location: deconnect.php');
	exit();
}
?>
