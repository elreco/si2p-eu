<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';


$req = $Conn->prepare("SELECT * FROM modeles_fonctions
LEFT OUTER JOIN modeles_fonctions_profils ON (modeles_fonctions_profils.mfp_fonction=modeles_fonctions.mfo_id)
LEFT OUTER JOIN profils ON (modeles_fonctions_profils.mfp_profil=profils.pro_id)
ORDER BY mfo_nom,pro_libelle");
$req->execute();
$modeles_fonctions = $req->fetchAll();
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php
				include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn">
				
					<h1>Liste des Fonctions</h1>
					
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="dark" >
									<th>Nom de la fonction</th>
									<th>Include</th>
									<th>Profil</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
					<?php 		$fonction=0;
								foreach($modeles_fonctions as $m){
									if($m["mfo_id"]!=$fonction){
										if($fonction>0){?>
											<tr>
												<td><?= $mfo_nom?></td>
												<td><?= $mfo_include?></td>
												<td><?= $pro_libelle?></td>
												
												<td class="text-center" >
													<a href="modeles_fonctions.php?id=<?=$fonction?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier cette fonction">
														<span class="fa fa-pencil"></span>
													</a>

												</td>
											</tr>
						<?php			}
										$fonction=$m["mfo_id"];
										$mfo_nom=$m["mfo_nom"];
										$mfo_include=$m["mfo_include"];
										$pro_libelle="";
									}
									$pro_libelle.=$m["pro_libelle"] . "/";
								}
								if($fonction>0){?>
									<tr>
										<td><?= $mfo_nom?></td>
										<td><?= $mfo_include?></td>
										<td><?= $pro_libelle?></td>										
										<td class="text-center" >
											<a href="modeles_fonctions.php?id=<?=$fonction?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier cette fonction">
												<span class="fa fa-pencil"></span>
											</a>
										</td>
									</tr>
				<?php			}?>
							</tbody>
						</table>
					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-long-arrow-left"></span>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="modeles_fonctions.php" class="btn btn-success btn-sm" role="button" >
						<span class="fa fa-plus"></span>
						Nouvelle fonction
					</a>
				</div>
			</div>
		</footer>

		<?php
		include "includes/footer_script.inc.php"; ?>	
	</body>
</html>
