<?php
include "includes/controle_acces_cli.inc.php";
include('includes/connexion.php');

	// ENREGISTREMENT D'UNE FICHE STAGIAIRE
	
	$err_danger="";
	$err_warning="";
	
	if(isset($_POST)){
		
		if(empty($_POST['sta_nom']) OR empty($_POST['sta_prenom'])){
			
			$err_danger="Formulaire incomplet! La fiche n'a pas été enregistrée.";
		
		}else{
			
			$stagiaire=0;
			if(!empty($_POST['sta_id'])){
				$stagiaire=intval($_POST['sta_id']);
			}
			
			$sta_nom="";
			if(!empty($_POST['sta_nom'])){
				$sta_nom=trim($_POST['sta_nom']);
			}
			
			$sta_prenom="";
			if(!empty($_POST['sta_prenom'])){
				$sta_prenom=trim($_POST['sta_prenom']);
			}
			$sta_client=0;				
			if(!empty($_SESSION['acces']['acc_client'])){
				$sta_client=intval($_SESSION['acces']['acc_client']);
			}
			$sta_naissance=null;
			if(!empty($_POST['sta_naissance'])){
				$Dt_naiss=DateTime::createFromFormat('d/m/Y',$_POST['sta_naissance']);
				if(!is_bool($Dt_naiss)){
					$sta_naissance=$Dt_naiss->format("Y-m-d");
				}
			}
			
			
			
			$sta_titre=0;				
			if(!empty($_POST["sta_titre"])){
				$sta_titre=intval($_POST['sta_titre']);
			}
			
			/**************************************
				CONTROLE
			***************************************/
		
			
			
			
            $req=$Conn->query("SELECT cli_filiale_de,cli_code,cli_nom FROM CLients WHERE cli_id=" . $sta_client . ";");
            $d_client=$req->fetch();
            
			
			
			if(empty($err_danger)){
				
				// controle de doublon
				
				$mil="";
				$sql="SELECT sta_id FROM Stagiaires";
				$mil.=" AND sta_naissance=:sta_naissance";
				
				$sql.=" WHERE NOT sta_id=:stagiaire AND sta_nom=:sta_nom AND sta_prenom=:sta_prenom" . $mil . ";";
				$req=$Conn->prepare($sql);
				$req->bindParam("stagiaire",$stagiaire);
				$req->bindParam("sta_nom",$sta_nom);
				$req->bindParam("sta_prenom",$sta_prenom);
					$req->bindParam("sta_naissance",$sta_naissance);
				
				$d_doublon=$req->fetch();
				if(!empty($d_doublon)){
					$err_danger="Le stagiaire existe déjà.";
				}
				
			}
		}
		
		/****************************
			ENREGISTREMENT
		****************************/
		
		if(empty($err_danger)){
			
			if($stagiaire==0){
				
				$req = $Conn->prepare("INSERT INTO Stagiaires 
					(sta_nom, 
					sta_prenom, 
					sta_naissance,
					sta_mail_perso,
					sta_titre
					) 
					VALUES 
					(:sta_nom, 
					:sta_prenom, 
					:sta_naissance, 
					
					:sta_mail_perso,
					:sta_titre);"
				);
				$req->bindParam(':sta_nom', $sta_nom);
				$req->bindParam(':sta_prenom', $sta_prenom);
				$req->bindParam(':sta_naissance', $sta_naissance);
				$req->bindParam(':sta_mail_perso', $_POST['sta_mail_perso']);
                $req->bindParam(':sta_titre', $sta_titre);
				try{					
					$req->execute();					
					$stagiaire = $Conn->lastInsertId();
				}Catch(Exception $e){
					$err_danger="ADD " . $e->getMessage();
				}
				
				// on associe le client et le stagiaire
				/* if(empty($err_danger)){
					$req = $Conn->prepare("INSERT INTO Stagiaires_Clients (scl_stagiaire,scl_client) VALUES (:scl_stagiaire,:scl_client);");
					$req->bindParam(':scl_stagiaire', $stagiaire);
					$req->bindParam(':scl_client', $sta_client);
					try{					
						$req->execute();					
					}Catch(Exception $e){
						$err_warning=$e->getMessage();
					}
				} */
				
			}else{
				
				$req = $Conn->prepare("UPDATE Stagiaires SET
				sta_nom=:sta_nom, 
				sta_prenom=:sta_prenom, 
				sta_naissance=:sta_naissance, 
				
				sta_mail_perso=:sta_mail_perso,
				sta_titre=:sta_titre 
				WHERE sta_id=:stagiaire;");
				$req->bindParam(':sta_nom', $sta_nom);
				$req->bindParam(':sta_prenom', $sta_prenom);
				$req->bindParam(':sta_naissance', $sta_naissance);
				
				$req->bindParam(':sta_mail_perso', $_POST['sta_mail_perso']);
				$req->bindParam(':sta_titre', $sta_titre);
				$req->bindParam(':stagiaire', $stagiaire);
				try{					
					$req->execute();					
				}Catch(Exception $e){
					$err_danger="UPDATE ". $e->getMessage();
				}
				
				// on s'assure que le stagiaire est bien lié au client
				
			}
		}
				
	
		
	}else{
		$err_danger="Formulaire incomplet! La fiche n'a pas été enregistrée.";
	}
	
	if(!empty($err_danger)){
		
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $err_danger
		);
		
		if(!empty($stagiaire)){
			header("location : stagiaire_client_voir.php?stagiaire=" . $stagiaire);
			die();
		}else{
			header("location : stagiaire_client_liste.php");
			die();
		}
	}else{
		
		if(!empty($err_warning)){
			echo("WARNING");
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "warning",
				"message" => $err_warning
			);
		}else{
			$_SESSION['message'][] = array(
				"titre" => "Enregistrement terminé",
				"type" => "success",
				"message" => "Vos modifications ont bien été prise en compte."
			);
		}
		Header("Location: stagiaire_client_voir.php?stagiaire=" . $stagiaire);
		
		
	}
?>