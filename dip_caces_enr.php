 <?php
 
	include "includes/controle_acces.inc.php";
	
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");
	
	include('modeles/mod_upload.php');
	
	// ENREGISTREMENT D'UN DIPLOME CACES
	
	$erreur_txt="";
	$warning_txt="";
	
	$action_id=0;
	if(isset($_POST["action"])){
		if(!empty($_POST["action"])){
			$action_id=intval($_POST["action"]);
		}	
	}
	
	$stagiaire_id=0;
	if(isset($_POST["stagiaire"])){
		if(!empty($_POST["stagiaire"])){
			$stagiaire_id=intval($_POST["stagiaire"]);
		}	
	}
	
	$dossier_id=0;
	if(isset($_POST["dossier"])){
		if(!empty($_POST["dossier"])){
			$dossier_id=intval($_POST["dossier"]);
		}	
	}
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}
	if($acc_societe!=$conn_soc_id){
		$acc_societe=$conn_soc_id;
	}
	
	$erreur_txt="";
	
	//*********************************************
	//	VARIABLE A CALCULER
	//*********************************************
	
		$dca_date_pass=null;	// date du passage = date du test th si th ou date min de la pratique
		$dca_th_pass=0;			// resultat theorie du passage
		
		$dca_date_th=null;		// date d'obtention de la théorie
		$dca_passage_th=null;	// passage d'obtention de la théorie

		$dca_date_pra=null;		// date d'obtention de la pratique
		$dca_passage_pra=null;	// passage d'obtention de la pratique
		
		$passage=0;
		
		
		
		// UPLOAD DE LA PHOTO DU STAGIAIRE
		// elle n'est pas lié à l'enregistrement du diplome donc on peut la traite quelque soit le resultat de controle.
		
		if(!empty($stagiaire_id)){
			
			

			$dossier="Stagiaires";
			if (!file_exists('documents/' . $dossier)) {
				$warning_txt="Dossier introuvable. Impossible de charger la photo!";
			}
			
			if(empty($warning_txt)){
				
				$dossier.="/";		
				$extension=array("jpg","JPG");
		
				$taille=4194304;
				
			
		
				if (!empty($_FILES['sta_photo']['name'])) {
					
					$path = $_FILES['sta_photo']['name'];
					$ext = pathinfo($path, PATHINFO_EXTENSION);			
					//$logo1=  "logo_1_" . date('m-d-Y') . "." . $ext;
				
					$result1=upload("sta_photo",$dossier,"sta_phid_" . $stagiaire_id,$taille,$extension,1);
					if(!empty($result1)){
						switch ($result1) {
							case 992:
								$warning_txt="le poids de la photo ne doit pas dépasser les " . ($taille/1024/1024) . " Mo.";
								break;
							default:
								$warning_txt="La photo n'a pas été chargée [" . $result1 . "]";
								break;
						}
						//unset($logo1);
						
						
					}
				}
					
			}
		}

	//*********************************************
	//	CONTROLE
	//*********************************************
	

	
	if(!empty($action_id) AND !empty($stagiaire_id) AND !empty($acc_societe)){
		
		// CONTROLE DU FORM
		
		$dca_diplome=0;
		if(!empty($_POST["dca_diplome"])){
			$dca_diplome=intval($_POST["dca_diplome"]);
		}
		
		$dca_qualification=0;
		if(!empty($_POST["qualification"])){
			$dca_qualification=intval($_POST["qualification"]);
		}
			
		if(!empty($_POST["dca_sta_nom"]) AND !empty($_POST["dca_sta_prenom"]) AND !empty($_POST["dca_sta_naissance"])){
			$DT_sta_naissance=date_create_from_format('d/m/Y',$_POST["dca_sta_naissance"]);
			if(!is_bool($DT_sta_naissance)){
				$dca_sta_naissance=$DT_sta_naissance->format("Y-m-d");
			}else{
				$erreur_txt="Format de date de naissance incorrect!";
			}

		}else{
			$erreur_txt="L'identité du stagiaire doit-être renseigné!";
		}
		
		// SUR LA SOCIETE
		if(empty($erreur_txt)){
			$sql="SELECT soc_caces FROM Societes WHERE soc_id=:acc_societe AND soc_caces>0;";
			$req=$Conn->prepare($sql);
			$req->bindParam("acc_societe",$acc_societe);
			$req->execute();
			$d_societe=$req->fetch();
			if(empty($d_societe)){
				$erreur_txt="Le paramétrage actuel ne permet de réaliser l'opération demandée.";
			}
		}
			

		// DONNEE SUR LE DOSSIER CACES
		if(empty($erreur_txt)){

			if($dossier_id>0){

				$sql="SELECT * FROM Diplomes_Caces WHERE dca_id=:dca_id;";
				$req=$Conn->prepare($sql);
				$req->bindParam(":dca_id",$dossier_id);
				$req->execute();
				$d_dossier=$req->fetch();
				if(!empty($d_dossier)){
					
					$dernier_passage=0;
					for($bcl=1;$bcl<=6;$bcl++){
						if(!empty($d_dossier["dca_action_" . $bcl])){
							$dernier_passage=$bcl;
						}
						if($d_dossier["dca_action_" . $bcl]==$action_id AND $d_dossier["dca_action_soc_" . $bcl]==$acc_societe){
							$passage=$bcl;
							break;
						}
					}
					if(empty($passage) AND $dernier_passage<=5){
						$passage=$dernier_passage+1;
					}
					
					if(empty($passage)){		
					
						$erreur_txt="Impossible de récupérer le dossier!";
						
					}else{
						
						if(!empty($d_dossier["dca_passage_th"]) AND $d_dossier["dca_passage_th"]!=$passage){
							$dca_date_th=$d_dossier["dca_date_th"];		
							$dca_passage_th=$d_dossier["dca_passage_th"];	
						}
					
						if(!empty($d_dossier["dca_passage_pra"]) AND $d_dossier["dca_passage_pra"]!=$passage){
							$dca_date_pra=$d_dossier["dca_date_pra"];		
							$dca_passage_pra=$d_dossier["dca_passage_pra"];	
						}

						$dca_diplome=$d_dossier["dca_diplome"];
						$dca_qualification=$d_dossier["dca_qualification"];
					}
				
				}else{
					$erreur_txt="Impossible de récupérer le dossier!";
				}
				
			}else{
				$passage=1;
			}
			if(empty($passage)){
				$erreur_txt="Impossible de récupérer le dossier!";
			}
		}
		
		if(empty($erreur_txt)){
			if(empty($dca_diplome)){
				$erreur_txt="Impossible d'identifier le type de diplôme demandé!";
			}
		}
		if(empty($erreur_txt)){
			if(empty($dca_qualification)){
				$erreur_txt="Impossible d'identifier la qualification!";
			}	
		}

		// ON IDENTIFIE L'ACTION CLIENT
		/* correspondant à la participation du stagiaire $stagiaire_id sur l'action $action_id
		necessaire pour sortir la stat "taux de test CDT"
		sans ce paramètre, on ne peut pas faire le lien entre le dossier qui porte les catégories testés et l'ation client qui porte le CA
		*/
		if(empty($erreur_txt)){
			$sql="SELECT ast_action_client FROM Actions_Stagiaires WHERE ast_stagiaire=:ast_stagiaire AND ast_action=:ast_action;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":ast_stagiaire",$stagiaire_id);
			$req->bindParam(":ast_action",$action_id);
			$req->execute();
			$d_action_stagiaire=$req->fetch();
			if(empty($d_action_stagiaire)){
				$erreur_txt="Impossible d'identifier l'inscription du stagiaire!";
			}
		}

		// VALIDATION DE LA PARTIE THEORIQUE
		
		if(empty($erreur_txt)){
			
			// la th n'est pas valider -> form accéssible donc on le traite
			if(is_null($dca_passage_th)){
				
				echo("GESTION DE LA THEORIE<br/>");
				
				if(!empty($_POST["dca_date"])){
					$DT_date_th=date_create_from_format('Y-m-d',$_POST["dca_date"]);
					if(!is_bool($DT_date_th)){
						
						// la date du passage correspond à la date du test
						$dca_date_pass=$_POST["dca_date"];
						
						if(!empty($_POST["th_echec"])){
							$dca_th_pass=2;		
						}elseif(!empty($_POST["th_valide"])){
							
							$dca_th_pass=1;
							
							$dca_passage_th=$passage;
							$dca_date_th=$dca_date_pass;
							
						}
					}
				}
			}
		}
		
		//*********************************************
		//	TRAITEMENT DU FORM HORS CONTROLE
		//*********************************************
		
		$dca_aipr=0;
		$dca_aipr_date=null;
		if(!empty($_POST["dca_aipr"])){
			$dca_aipr=1;
			if(!empty($_POST["dca_aipr_date"])){
				$DT_periode=date_create_from_format('d/m/Y',$_POST["dca_aipr_date"]);
				if(!is_bool($DT_periode)){
					$dca_aipr_date=$DT_periode->format("Y-m-d");
				}

			}
		}
		
		
		
		//*********************************************
		//	ENREGISTREMENT
		//*********************************************
		
		if(empty($erreur_txt)){
			
			if($dossier_id>0){
				
				$sql="UPDATE Diplomes_Caces SET dca_sta_nom=:dca_sta_nom,dca_sta_prenom=:dca_sta_prenom,dca_sta_naissance=:dca_sta_naissance
				,dca_th_" . $passage . "=:dca_th_pass,dca_date_" . $passage . "=:dca_date_pass,dca_action_" . $passage . "=:dca_action_pass
				,dca_action_client_" . $passage . "=:dca_action_client_pass,dca_action_soc_" . $passage . "=:dca_action_soc_pass
				,dca_date_th=:dca_date_th,dca_passage_th=:dca_passage_th,dca_aipr=:dca_aipr,dca_aipr_date=:dca_aipr_date
				WHERE dca_id=:dca_id;";
				$req=$Conn->prepare($sql);
				//$req->bindParam(":dca_diplome",$dca_diplome);
				//$req->bindParam(":dca_caces",$d_societe["soc_caces"]);
				$req->bindParam(":dca_sta_nom",$_POST["dca_sta_nom"]);
				$req->bindParam(":dca_sta_prenom",$_POST["dca_sta_prenom"]);
				$req->bindParam(":dca_sta_naissance",$dca_sta_naissance);
				$req->bindParam(":dca_action_pass",$action_id);
				$req->bindParam(":dca_action_client_pass",$d_action_stagiaire["ast_action_client"]);
				$req->bindParam(":dca_action_soc_pass",$acc_societe);
				$req->bindParam(":dca_th_pass",$dca_th_pass);
				$req->bindParam(":dca_date_pass",$dca_date_pass);
				$req->bindParam(":dca_date_th",$dca_date_th);
				$req->bindParam(":dca_passage_th",$dca_passage_th);
				$req->bindParam(":dca_aipr",$dca_aipr);
				$req->bindParam(":dca_aipr_date",$dca_aipr_date);
				$req->bindParam(":dca_id",$dossier_id);
				try{
					$req->execute();
				}Catch(Exception $e){
					$erreur_txt="UP : " . $e->getMessage();
				}
				
			}else{
				
				$sql="INSERT INTO Diplomes_Caces (dca_caces,dca_qualification,dca_stagiaire,dca_diplome,dca_sta_nom,dca_sta_prenom,dca_sta_naissance,dca_action_1,dca_action_client_1,dca_action_soc_1,dca_th_1,dca_date_1,dca_date_th,dca_passage_th
				,dca_aipr,dca_aipr_date)
				VALUES (:dca_caces,:dca_qualification,:dca_stagiaire,:dca_diplome,:dca_sta_nom,:dca_sta_prenom,:dca_sta_naissance,:dca_action_1,:dca_action_client_1,:dca_action_soc_1,:dca_th_1,:dca_date_1,:dca_date_th,:dca_passage_th
				,:dca_aipr,:dca_aipr_date);";
				$req=$Conn->prepare($sql);
				$req->bindParam(":dca_caces",$d_societe["soc_caces"]);
				$req->bindParam(":dca_qualification",$dca_qualification);
				$req->bindParam(":dca_stagiaire",$stagiaire_id);
				$req->bindParam(":dca_diplome",$dca_diplome);
				$req->bindParam(":dca_sta_nom",$_POST["dca_sta_nom"]);
				$req->bindParam(":dca_sta_prenom",$_POST["dca_sta_prenom"]);
				$req->bindParam(":dca_sta_naissance",$dca_sta_naissance);
				$req->bindParam(":dca_action_1",$action_id);
				$req->bindParam(":dca_action_client_1",$d_action_stagiaire["ast_action_client"]);
				$req->bindParam(":dca_action_soc_1",$acc_societe);
				$req->bindParam(":dca_th_1",$dca_th_pass);
				$req->bindParam(":dca_date_1",$dca_date_pass);
				$req->bindParam(":dca_date_th",$dca_date_th);
				$req->bindParam(":dca_passage_th",$dca_passage_th);
				$req->bindParam(":dca_aipr",$dca_aipr);
				$req->bindParam(":dca_aipr_date",$dca_aipr_date);
				try{
					$req->execute();
					$dossier_id=$Conn->lastInsertId();
				}Catch(Exception $e){
					$erreur_txt="ADD : " . $e->getMessage();
				}
				
			}
			
			// MAJ DES CATEGORIES
			
			$date_pratique=null;
				
			if(empty($erreur_txt)){
				
				// IDENTITE DES TESTETURS
				
				$d_int=array();
				
				$sql="SELECT int_id,int_label_1,int_label_2 FROM Intervenants ORDER BY int_id;";
				$req=$ConnSoc->query($sql);
				$d_results=$req->fetchAll();
				if(!empty($d_results)){
					foreach($d_results as $r){
						$d_int[$r["int_id"]]=trim($r["int_label_2"] . " " . $r["int_label_1"]);
					}
				}
					
				
				// REQUETE PREP
			
				$sql_del="DELETE FROM Diplomes_Caces_Cat WHERE dcc_diplome=:diplome AND dcc_passage=:passage AND dcc_categorie=:categorie;";
				$req_del=$Conn->prepare($sql_del);
				$req_del->bindParam(":diplome",$dossier_id);
				$req_del->bindParam(":passage",$passage);
				
				$sql_add="INSERT INTO Diplomes_Caces_Cat (dcc_diplome,dcc_passage,dcc_categorie,dcc_date,dcc_testeur,dcc_testeur_identite,dcc_valide
				,dcc_opt_1,dcc_opt_1_valide,dcc_opt_2,dcc_opt_2_valide,dcc_opt_3,dcc_opt_3_valide,dcc_action,dcc_action_client,dcc_action_soc)
				VALUES (:diplome,:passage,:categorie,:dcc_date,:dcc_testeur,:dcc_testeur_identite,:dcc_valide
				,:dcc_opt_1,:dcc_opt_1_valide,:dcc_opt_2,:dcc_opt_2_valide,:dcc_opt_3,:dcc_opt_3_valide,:dcc_action,:dcc_action_client,:dcc_action_soc);";
				$req_add=$Conn->prepare($sql_add);
				$req_add->bindParam(":diplome",$dossier_id);
				$req_add->bindParam(":passage",$passage);
				
				$sql_up="UPDATE Diplomes_Caces_Cat SET 
				dcc_date=:dcc_date,
				dcc_testeur=:dcc_testeur,
				dcc_testeur_identite=:dcc_testeur_identite,
				dcc_valide=:dcc_valide,
				dcc_opt_1=:dcc_opt_1,
				dcc_opt_1_valide=:dcc_opt_1_valide,
				dcc_opt_2=:dcc_opt_2,
				dcc_opt_2_valide=:dcc_opt_2_valide,
				dcc_opt_3=:dcc_opt_3,
				dcc_opt_3_valide=:dcc_opt_3_valide,
				dcc_action=:dcc_action,
				dcc_action_client=:dcc_action_client,
				dcc_action_soc=:dcc_action_soc
				WHERE dcc_diplome=:diplome AND dcc_passage=:passage AND dcc_categorie=:categorie;";
				$req_up=$Conn->prepare($sql_up);
				$req_up->bindParam(":diplome",$dossier_id);
				$req_up->bindParam(":passage",$passage);
				
				$sql="SELECT qca_id,qca_opt_1,qca_ut_opt_1,qca_opt_2,qca_ut_opt_2,qca_opt_3,qca_ut_opt_3
				,dcc_diplome
				FROM Qualifications_Categories LEFT JOIN Diplomes_Caces_Cat ON (Qualifications_Categories.qca_id=Diplomes_Caces_Cat.dcc_categorie AND dcc_diplome=:diplome AND dcc_passage=:passage)
				WHERE qca_qualification=:dca_qualification;";
				$req=$Conn->prepare($sql);
				$req->bindParam(":dca_qualification",$dca_qualification);
				$req->bindParam(":diplome",$dossier_id);
				$req->bindParam(":passage",$passage);
				$req->execute();
				$d_categories=$req->fetchAll();
				if(!empty($d_categories)){
					
					foreach($d_categories as $cat){
						
						if(!empty($_POST["cat_" . $cat["qca_id"]])){
							
							// CATEGORIE EST SELECT DONC DEMANDE
							
							$dcc_date=null;
							if(!empty($_POST["date_" . $cat["qca_id"]])){
								$dcc_date=$_POST["date_" . $cat["qca_id"]];	
							}
							$dcc_testeur=0;
							if(!empty($_POST["testeur_" . $cat["qca_id"]])){
								$dcc_testeur=intval($_POST["testeur_" . $cat["qca_id"]]);	
							}
							$dcc_valide=0;
							$dcc_opt_1=0;
							$dcc_opt_1_valide=0;
							$dcc_opt_2=0;
							$dcc_opt_2_valide=0;
							$dcc_opt_3=0;
							$dcc_opt_3_valide=0;
							
							if(!empty($_POST["cat_valide_" . $cat["qca_id"]])){
								
								$dcc_valide=1;
					
								if(!empty($cat["qca_opt_1"])){
									if(empty($cat["qca_ut_opt_1"])){
										// incluse dans la categorie
										$dcc_opt_1=1;
										$dcc_opt_1_valide=1;
									}else{
										if(!empty($_POST["opt_" . $cat["qca_id"] . "_1"])){
											$dcc_opt_1=1;
											if(!empty($_POST["opt_" . $cat["qca_id"] . "_1_valide"])){
												$dcc_opt_1_valide=1;
											}
										}
										
									}
								}
								
								if(!empty($cat["qca_opt_2"])){
									if(empty($cat["qca_ut_opt_2"])){
										// incluse dans la categorie
										$dcc_opt_2=1;
										$dcc_opt_2_valide=1;
									}else{
										if(!empty($_POST["opt_" . $cat["qca_id"] . "_2"])){
											$dcc_opt_2=1;
											if(!empty($_POST["opt_" . $cat["qca_id"] . "_2_valide"])){
												$dcc_opt_2_valide=1;
											}
										}
										
									}
								}
								
								if(!empty($cat["qca_opt_3"])){
									if(empty($cat["qca_ut_opt_3"])){
										// incluse dans la categorie
										$dcc_opt_3=1;
										$dcc_opt_3_valide=1;
									}else{
										if(!empty($_POST["opt_" . $cat["qca_id"] . "_3"])){
											$dcc_opt_3=1;
											if(!empty($_POST["opt_" . $cat["qca_id"] . "_3_valide"])){
												$dcc_opt_3_valide=1;
											}
										}
										
									}
								}
								
							}
							

							if(!empty($cat["dcc_diplome"])){
								
								// LA CAT ETAIT DEJA AFFECTE ON MAJ
								
								$req_up->bindParam(":categorie",$cat["qca_id"]);
								$req_up->bindParam(":dcc_date",$dcc_date);
								$req_up->bindParam(":dcc_testeur",$dcc_testeur);
								$req_up->bindParam(":dcc_testeur_identite",$d_int[$dcc_testeur]);
								$req_up->bindParam(":dcc_valide",$dcc_valide);
								$req_up->bindParam(":dcc_opt_1",$dcc_opt_1);
								$req_up->bindParam(":dcc_opt_1_valide",$dcc_opt_1_valide);
								$req_up->bindParam(":dcc_opt_2",$dcc_opt_2);
								$req_up->bindParam(":dcc_opt_2_valide",$dcc_opt_2_valide);
								$req_up->bindParam(":dcc_opt_3",$dcc_opt_3);
								$req_up->bindParam(":dcc_opt_3_valide",$dcc_opt_3_valide);
								$req_up->bindParam(":dcc_action",$action_id);
								$req_up->bindParam(":dcc_action_client",$d_action_stagiaire["ast_action_client"]);
								$req_up->bindParam(":dcc_action_soc",$acc_societe);
								$req_up->execute();
								
							}else{
								
								// AJOUT DE LA CAT
								
								$req_add->bindParam(":categorie",$cat["qca_id"]);
								$req_add->bindParam(":dcc_date",$dcc_date);
								$req_add->bindParam(":dcc_testeur",$dcc_testeur);
								$req_add->bindParam(":dcc_testeur_identite",$d_int[$dcc_testeur]);
								$req_add->bindParam(":dcc_valide",$dcc_valide);
								$req_add->bindParam(":dcc_opt_1",$dcc_opt_1);
								$req_add->bindParam(":dcc_opt_1_valide",$dcc_opt_1_valide);
								$req_add->bindParam(":dcc_opt_2",$dcc_opt_2);
								$req_add->bindParam(":dcc_opt_2_valide",$dcc_opt_2_valide);
								$req_add->bindParam(":dcc_opt_3",$dcc_opt_3);
								$req_add->bindParam(":dcc_opt_3_valide",$dcc_opt_3_valide);
								$req_add->bindParam(":dcc_action",$action_id);
								$req_add->bindParam(":dcc_action_client",$d_action_stagiaire["ast_action_client"]);
								$req_add->bindParam(":dcc_action_soc",$acc_societe);
								$req_add->execute();
								
							}
							
							if($dcc_valide==1){
								if(is_null($date_pratique)){
									$date_pratique=$dcc_date;
								}elseif($date_pratique>$dcc_date){;
									$date_pratique=$dcc_date;
								}
							}
							
						}elseif(!empty($cat["dcc_diplome"])){
							
							// LA CATEGORIE ETAIT ASSOCIE ON DELETE
							
							$req_del->bindParam(":categorie",$cat["qca_id"]);
							$req_del->execute();
							
						}
					}				
				}
				// FIN MAJ PARTIE PRATIQUE				
		
			
				// MAJ DU DOSSIER EN FONCTION DE LA PRATIQUE
				
				if(is_null($dca_date_pra) AND !is_null($date_pratique)){
					// la pratique n'etait pas valide -> elle l'est sur ce passage
					$dca_date_pra=$date_pratique;
					$dca_passage_pra=$passage;
				}
				
				// calcul date d'obtention dossier
				
				$dca_date=null;
				$dca_date_validite=null;
				
				if(!is_null($dca_date_th) AND !is_null($dca_date_pra)){
					// le dossier est valide
					if($dca_date_th<$dca_date_pra){
						$dca_date=$dca_date_pra;
					}else{
						$dca_date=$dca_date_th;	
					}
					
					// calcul de la validtie
					
					$sql="SELECT qua_duree FROM Qualifications WHERE qua_id=:dca_qualification;";
					$req=$Conn->prepare($sql);
					$req->bindParam("dca_qualification",$dca_qualification);
					$req->execute();
					$d_qualification=$req->fetch();
					if(!empty($d_qualification)){
						var_dump($d_qualification);
						$Dt_validite=DateTime::createFromFormat('Y-m-d',$dca_date);
						$Dt_validite->add(new DateInterval('P' . $d_qualification["qua_duree"] . 'M')); 
						$Dt_validite->sub(new DateInterval('P1D')); 
						$dca_date_validite=$Dt_validite->format("Y-m-d");
					}
				}
				
				// MAJ DU DOSSIER
				
				$sql="UPDATE Diplomes_Caces SET dca_date=:dca_date, dca_date_validite=:dca_date_validite, dca_date_pra=:dca_date_pra, dca_passage_pra=:dca_passage_pra";
				// pas de th donc la date de passage devient la date de pratique
				if(is_null($dca_date_pass) AND !is_null($date_pratique)){
					$sql.=", dca_date_" . $passage . "=:dca_date_pass";
				}
				$sql.=" WHERE dca_id=:dossier_id";
				$req=$Conn->prepare($sql);	
				$req->bindParam(":dca_date",$dca_date);
				$req->bindParam(":dca_date_validite",$dca_date_validite);
				$req->bindParam(":dca_date_pra",$dca_date_pra);
				$req->bindParam(":dca_passage_pra",$dca_passage_pra);
				if(is_null($dca_date_pass) AND !is_null($date_pratique)){
					$req->bindParam(":dca_date_pass",$date_pratique);		
				}
				$req->bindParam(":dossier_id",$dossier_id);
				$req->execute();
			
				// MAJ DES INFOS Actions_Stagiaires

				$sql="UPDATE Actions_Stagiaires SET ast_diplome=:diplome,ast_diplome_statut=:diplome_statut WHERE ast_stagiaire=:ast_stagiaire AND ast_action=:ast_action;";
				$req=$ConnSoc->prepare($sql);	
				$req->bindParam(":diplome",$dossier_id);
				$req->bindValue(":diplome_statut",0);
				$req->bindParam(":ast_stagiaire",$stagiaire_id);
				$req->bindParam(":ast_action",$action_id);
				$req->execute();

			}
		}
	}else{
		$erreur_txt="Formulaire incomplet!";
	}
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Echec de l'enregistrement",
			"type" => "danger",
			"message" => $erreur_txt
		);
	}else{
		if(!empty($warning_txt)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "success",
				"message" => $warning_txt
			);
		}else{
			$_SESSION['message'][] = array(
				"titre" => "Enregistrement terminé",
				"type" => "success",
				"message" => "Dossier enregistré avec succes!" 
			);
		}
	}
	if(!empty($dossier_id) AND !empty($passage)){
		header("location : dip_caces_voir.php?dossier=" . $dossier_id . "&passage=" . $passage . "&societ=" . $conn_soc_id);
	}else{
		header("location : action_voir_sta.php?action=" . $action_id);
	}
	die();

		
