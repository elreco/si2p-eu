<?php
include("includes/connexion.php");

include_once 'modeles/mod_envoi_mail.php';

$erreur_txt="";

$sql="SELECT con_id,con_ref_id,con_nom,con_prenom,con_mail,con_hash_public FROM Contacts WHERE NOT con_mail_doublon=1 AND NOT con_mail='' AND NOT ISNULL(con_mail) AND NOT con_rgpd=1
AND con_id<=1000;";
$req=$Conn->query($sql);
$d_mailing=$req->fetchAll();
if(!empty($d_mailing)){
	foreach($d_mailing as $mail){
		
					
		// MAIL RGPD

		$adr=array(
			"0" => array(
				"nom" => $mail["con_prenom"] . " " . $mail["con_nom"],
				"adresse" => $mail["con_mail"]
			)
		);
		
		$param=array(
			"sujet" => "RGPD : choisissez les communications que vous souhaitez recevoir",
			"cle_public" => $mail["con_hash_public"]
		);

		$envoie_ok=envoi_mail("rgpd",$param,$adr);
		if(is_bool($envoie_ok)){
			
			$sql="UPDATE contacts SET con_rgpd = 1,con_hash_public = :con_hash_public WHERE con_id=:contact AND con_ref_id=:client;";
			$req = $Conn->prepare($sql);
			$req->bindParam("con_hash_public",$mail["con_hash_public"]);
			$req->bindParam("contact",$mail["con_id"]);
			$req->bindParam("client",$mail["con_ref_id"]);					
			try{
				$req->execute();
			}catch( PDOException $Exception ){
				$erreur_txt.=$Exception->getMessage() . "</br>";
				$erreur_txt.="Erreur 002! Le contact ". $mail["con_id"] ." n'a pas été mis à jour. Contacter l'administrateur du site.<br/>";
			}
		}else{
			var_dump($envoie_ok);
			$erreur_txt.="Erreur 001! Mail du contact ". $mail["con_id"] ." n'a pas été envoyé.<br/>";

		}
	}
}else{
	$erreur_txt="Pas de mail en attente!";
}


if(!empty($erreur_txt)){
	echo($erreur_txt);
}else{
	echo("TERMINE");
}
?>
