<?php
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'includes/connexion_fct.php';

// LISTE DES VISITES PRISE EN COMPTE POUR UN UTILISATEUR
$erreur_txt="";

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];    
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
    $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];  
}

$utilisateur=0;
if(!empty($_GET["utilisateur"])){
	$utilisateur=intval($_GET["utilisateur"]);
}
$societe=0;
if(!empty($_GET["societe"])){
	$societe=intval($_GET["societe"]);
}
$agence=0;
if(!empty($_GET["agence"])){
	$agence=intval($_GET["agence"]);
}


if($utilisateur==0 AND $societe==0){
	$erreur_txt="Vous ne pouvez pas accéder à cette page!";
}

if(empty($erreur_txt)){
	
	$_SESSION['retourClient']="market_concours_client.php?utilisateur=" . $utilisateur . "&societe=" . $societe . "&agence=" . $agence;
	
	if(!empty($utilisateur)){
		
		$sql="SELECT uti_nom,uti_prenom,uti_societe FROM Utilisateurs WHERE uti_id=" . $utilisateur . ";";
		$req=$Conn->query($sql);
		$d_utilisateur=$req->fetch();
		if(empty($d_utilisateur)){
			$erreur_txt="Vous ne pouvez pas accéder à cette page!";
		}else{
			if($utilisateur==432){
				// Patricia L joue pour NN
				// pas nécéssaire pour Jean car il ne fait pas partie du concours individuel
				$societe=3;
			}else{
				$societe=$d_utilisateur["uti_societe"];
			}
		}
		
	}else{
		
		$sql="SELECT soc_nom,age_nom FROM Societes LEFT JOIN Agences ON (Societes.soc_id=Agences.age_societe AND age_id=" . $agence . ")
		WHERE soc_id=" . $societe . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();
		if(empty($d_societe)){
			$erreur_txt="Vous ne pouvez pas accéder à cette page!";
			echo($erreur_txt);
			die();
		}
		
	}
}

// PRODUITS DU CONCOURS
$sql="SELECT pro_id,pro_pt_0,pro_pt_1,pro_pt_2 FROM Produits WHERE pro_pt_0>0 ORDER BY pro_id;";
$req=$Conn->query($sql);
$d_r_pro=$req->fetchAll();
if(!empty($d_r_pro)){
	foreach($d_r_pro as $p){
		$concours_pro[$p["pro_id"]]=array(
			"0" => $p["pro_pt_0"],
			"1" => $p["pro_pt_1"],
			"2" => $p["pro_pt_2"]
		);
	}
	$tab_pro=array_column ($d_r_pro,"pro_id");
	$liste_pro=implode($tab_pro,",");

}else{
	$erreur_txt="Aucun produit associé au concours";
}


// ON RECUPERE LES FACTURES DE PASSAGE EN CLIENT
if(empty($erreur_txt)){

	
	$sql_nv_cli="SELECT Clients.cli_first_facture,Holdings.cli_id FROM Clients LEFT JOIN Clients AS Holdings ON (Clients.cli_filiale_de=Holdings.cli_id)
	WHERE NOT Clients.cli_categorie=2
	AND (Clients.cli_first_facture_date>='2019-07-01' AND Clients.cli_first_facture_date<='2019-12-31' AND Clients.cli_first_facture_soc=" . $societe . ")
	AND (
		ISNULL(Holdings.cli_id) 
		OR (Holdings.cli_first_facture_date>='2019-07-01' AND Holdings.cli_first_facture_date<='2019-12-31' AND Holdings.cli_first_facture_soc=" . $societe . ")
	) AND NOT Clients.cli_hors_concours;";
	$req_nv_cli=$Conn->query($sql_nv_cli);
	$d_clients=$req_nv_cli->fetchAll();
	if(!empty($d_clients)){
		$tab_fac=array_column ($d_clients,"cli_first_facture");
		$liste_fac=implode(",",$tab_fac);
	}else{
		$erreur_txt="Pas de nouveau client sur la période du concours";
	}
}

if(empty($erreur_txt)){	

	$ConnFct=connexion_fct($societe);
	
	/*var_dump($liste_fac);
	
	die();*/
	
	$sql_fac="SELECT cli_id,cli_code,cli_nom
	,fac_id,fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date_fr
	,fli_code_produit,fli_qte,fli_produit
	,acl_derogation
	,com_label_1,com_label_2
	,DATE_FORMAT(act_date_deb,'%d/%m/%Y') AS act_date_deb_fr,MONTH(act_date_deb) as mois
	FROM Factures 
	INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
	INNER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
	INNER JOIN Clients ON (Factures.fac_client=Clients.cli_id)
	INNER JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id AND Factures_Lignes.fli_action_cli_soc=" . $societe . ")
	INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
	AND com_type=1 AND fac_id IN (" . $liste_fac . ") AND fac_nature=1 AND fli_produit IN (" . $liste_pro . ") AND acl_pro_inter=0 AND act_date_deb>'2019-07-01' AND NOT com_archive";
	// securité au cas ou l'action aurait été facturé par anticipation avant le 31/12 alors que l'action à lieu en janvier
	$sql_fac.=" AND act_date_deb<='2019-12-31'";
	if(!empty($utilisateur)){
		$sql_fac.=" AND com_ref_1=" . $utilisateur;
	}elseif(!empty($agence)){
		$sql_fac.=" AND fac_agence=" . $agence;
	}
	if($societe==7){
		$sql_fac.=" AND NOT com_id=174";
	}elseif($agence==3){
		$sql_fac.=" AND NOT com_id=196";
	}elseif($societe==2){
		$sql_fac.=" AND NOT com_id=114";
	}
	$sql_fac.=" ORDER BY fac_date;";
	$req_fac=$ConnFct->query($sql_fac);
	$d_factures_ligne=$req_fac->fetchAll();
	
	$nb_com=0;
	if(empty($utilisateur)){ 
		switch ($societe) {
			case 2:
				$nb_com=3;
				break;
			case 3:
				if($agence==3){
					$nb_com=3;
				}else{
					$nb_com=3;
				}
				break;
			case 4:
				$nb_com=1;
				break;
			case 5:
				$nb_com=3;
				break;
			case 7:
				$nb_com=2;
				break;
			case 8:
				$nb_com=1;
				break;
		}
	}

}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Marketing client</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="market_suspect_liste.php" id="formulaire" >
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
					
						<div class="content-header">
					<?php	if(!empty($utilisateur)){ ?>
								<h2>Points "produit" pour <b class="text-primary"><?=$d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"]?></b></h2>
					<?php	}elseif(!empty($agence)){ ?>		
								<h2>Points "produit" pour  <b class="text-primary"><?=$d_societe["soc_nom"] . " " . $d_societe["age_nom"]?></b></h2>
					<?php	}else{ ?>		
								<h2>Points "produit" pour  <b class="text-primary"><?=$d_societe["soc_nom"]?></b></h2>
					<?php	} ?>
						</div>
						
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="panel">
									<div class="panel-heading panel-head-sm">Nouveaux clients</div>
									<div class="panel-body" >
								<?php	if(!empty($d_factures_ligne)){ ?>			
											<table class="table" >
												<thead>
													<tr>
														<th>Code</th>
														<th>Nom</th>
														<th>Facture</th>
														<th>Date de facture</th>
														<th>Commercial</th>
														<th>Produit</th>
														<th>Date d'action</th>
														<th>Dérogation</th>
														<th>Point ind.</th>
														<th>Coefficient commercial</th>
														<th>Base point</th>
														<th>Qte</th>
														<th>Coeff période</th>
														<th class="text-center" >Nb Point</th>															
													</tr>
												</thead>
												<tbody>
									<?php			$total=0;
													foreach($d_factures_ligne as $fac){ 
													
														$dero=0;
														if(!empty($fac["acl_derogation"])){
															$dero=intval($fac["acl_derogation"]);
														}
														
														$pt_base=0;
														$pt=0;
														if(!empty($concours_pro[$fac["fli_produit"]])){
															$pt=$concours_pro[$fac["fli_produit"]][$dero];
															$pt_base=$concours_pro[$fac["fli_produit"]][$dero];
														}
														if(empty($utilisateur)){ 
															$pt=$pt/$nb_com;
														};
		
														$coeff=1;
														if($fac["mois"]==7 OR $fac["mois"]==8 OR $fac["mois"]==12 OR $fac["mois"]==1 OR $fac["mois"]==2){
															$coeff=2;
														}
														$total_ligne=$pt*$fac["fli_qte"]*$coeff;
														
														
													
														
														$total=$total + $total_ligne; ?>
														<tr>
															<td><?=$fac["cli_code"]?></td>
															<td><?=$fac["cli_nom"]?></td>
															<td><?=$fac["fac_numero"]?></td>
															<td><?=$fac["fac_date_fr"]?></td>
															<td><?=$fac["com_label_1"] . " " . $fac["com_label_2"]?></td>
															<td><?=$fac["fli_code_produit"]?></td>
															<td><?=$fac["act_date_deb_fr"]?></td>
															<td>
													<?php		if(!empty($dero)){
																	echo("Niv. " . $dero);
																}else{
																	echo("aucune");
																} ?>
															</td>		
															<td class="text-right" ><?=number_format($pt_base,2,","," ")?> pt</td>		
															<td class="text-right" ><?=$nb_com?></td>		
															<td class="text-right" ><?=number_format($pt,2,","," ")?> pt</td>															
															<td class="text-right" ><?=$fac["fli_qte"]?></td>
															<td class="text-right" ><?=$coeff?></td>
															<td class="text-right" ><?=number_format($total_ligne,2,","," ")?> pt</td>														
														</tr>
									<?php			} ?>
												</tbody>
												<tfoot>
													<tr>
														<th class="text-right" colspan="13" >Total :</th>
														<th class="text-right" ><?=number_format($total,2,","," ")?> pt</th>
													</tr>
												</tfoot>
											</table>
								<?php	}else{ ?>
											<p class="alert alert-warning" >Aucun produit.</p>
								<?php	} ?>
									</div>
								</div>
							</div>
						</div>
						
					</section>
				</section>
			</div>		
			<!-- End: Main -->
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
				<?php	if(!empty($utilisateur)){ ?>
							<a href="market_concours.php" class="btn btn-default btn-sm">
								<span class="fa fa-arrow-left"></span>
								<span class="hidden-xs">Retour</span>
							</a>
				<?php	}else{ ?>
							<a href="market_concours.php?agence=1" class="btn btn-default btn-sm">
								<span class="fa fa-arrow-left"></span>
								<span class="hidden-xs">Retour</span>
							</a>
				<?php	} ?>			
					</div>
					<div class="col-xs-6 footer-middle text-center"></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
				
			});
		</script>
	</body>
</html>
