<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";


	// SI C'EST UNE EDITION
	if(!empty($_GET['id'])){
		
		// attestation
		$req = $Conn->prepare("SELECT * FROM attestations WHERE att_id = :att_id ");
	    $req->bindParam(':att_id', $_GET['id']);
	    $req->execute();
	    $attestation = $req->fetch();
		
	    // competence
	    $req = $Conn->prepare("SELECT * FROM attestations_competences WHERE aco_attestation = :aco_attestation");
	    $req->bindParam(':aco_attestation', $_GET['id']);
	    $req->execute();
	    $competences = $req->fetchAll();
	}else{
		$attestation=array(
			"att_id" =>0,
			"att_titre" => "",
			"att_contenu" => "",
			"att_famille" => 0,
			"att_sous_famille" => 0,
			"att_sous_sous_famille" => 0
		);
	}
	// FIN EDITION
	
	// LES FAMILLES DE PRODUITS
	
	$sql="SELECT pfa_id,pfa_libelle FROM Produits_Familles ORDER BY pfa_libelle;";
	$req=$Conn->query($sql);
	$d_familles=$req->fetchAll();
	
	// SOUS - FAMILLE
	if(!empty($attestation["att_famille"])){
		$sql="SELECT psf_id,psf_libelle FROM Produits_Sous_Familles WHERE psf_pfa_id=" . $attestation["att_famille"] . " ORDER BY psf_libelle;";
		$req=$Conn->query($sql);
		$d_sous_familles=$req->fetchAll();
	}
	
	// SOUS SOUS FAMILLE
	if(!empty($attestation["att_sous_famille"])){
		$sql="SELECT pss_id,pss_libelle FROM Produits_Sous_Sous_Familles WHERE pss_psf_id=" . $attestation["att_sous_famille"] . " ORDER BY pss_libelle;";
		$req=$Conn->query($sql);
		$d_sous_sous_familles=$req->fetchAll();
	}
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Si2P</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		
		<!-- plugin -->
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote-bs3.css" />
		
		<!-- Personalisation du theme -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<!-- Favicon -->
		<link rel="shortcut icon" href="theme/assets/img/favicon.ico" >

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm onload-check" >
	
		<form method="post" action="attestation_enr.php" id="form">
		
			<!-- Start: Main -->
			<div id="main" >
				
		<?php	require "includes/header_def.inc.php"; ?>
				
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper" >
					<!-- DEBUT DU CONTENU -->
					<section id="content" class=""  >

						<div class="content-header">
							<?php if(!empty($attestation)){ ?>
								<h2>Modifier l'attestation <b class="text-primary" ><?= $attestation['att_titre'] ?></b></h2>
							<?php }else{ ?>
								<h2>Créer une <b class="text-primary" >attestation</b></h2>
							<?php } ?>
						</div>
						
						<div class="row" >
							<div class="col-md-offset-1 col-md-10" >
								<?php if(!empty($attestation)){ ?>
									<input type="hidden" name="att_id" value="<?= $attestation['att_id'] ?>">
								<?php }?>
								<div class="admin-form theme-primary">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											<div class="row" >
												<div class="col-md-4" >									
													<label for="att_famille">Famille :</label>
													<select id="att_famille" name="att_famille" class="select2 select2-famille" required >
														<option value="" <?php if($attestation["att_famille"]==0) echo("selected"); ?> >Famille</option>
											<?php		foreach($d_familles as $fam){ ?>
															<option value="<?=$fam["pfa_id"]?>" <?php if($attestation["att_famille"]==$fam["pfa_id"]) echo("selected"); ?> ><?=$fam["pfa_libelle"]?></option>
											<?php		} ?>
													</select>	
													<small class="text-danger texte_required" id="att_famille_required">
														Merci de selectionner une famille
													</small>
												</div>
												<div class="col-md-4" >										
													<label for="att_sous_famille">Sous-Famille :</label>
													<select id="att_sous_famille" name="att_sous_famille" class="select2 select2-famille-sous-famille" >
														<option value="" <?php if($attestation["att_sous_famille"]==0) echo("selected"); ?> >Sous-Famille</option>
											<?php		if(isset($d_sous_familles)){
															foreach($d_sous_familles as $s_fam){ ?>
																<option value="<?=$s_fam["psf_id"]?>" <?php if($attestation["att_sous_famille"]==$s_fam["psf_id"]) echo("selected"); ?> ><?=$s_fam["psf_libelle"]?></option>
											<?php			}
														} ?>
													</select>
													<small class="text-danger texte_required" id="att_sous_famille_required">
														Merci de selectionner une sous-famille
													</small>													
												</div>
												<div class="col-md-4" >											
													<label for="att_sous_sous_famille">Sous Sous-Famille :</label>
													<select id="att_sous_sous_famille" name="att_sous_sous_famille" class="select2 select2-famille-sous-sous-famille" >
														<option value="" <?php if($attestation["att_sous_sous_famille"]==0) echo("selected"); ?> >Sous Sous-Famille</option>
											<?php		if(isset($d_sous_sous_familles)){
															foreach($d_sous_sous_familles as $s_s_fam){ ?>
																<option value="<?=$s_s_fam["pss_id"]?>" <?php if($attestation["att_sous_sous_famille"]==$s_s_fam["pss_id"]) echo("selected"); ?> ><?=$s_s_fam["pss_libelle"]?></option>
											<?php			}
														} ?>
													</select>
													<small class="text-danger texte_required" id="att_sous_sous_famille_required">
														Merci de selectionner une sous sous-famille
													</small>													
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-6">													
													<label for="att_titre">Titre :</label>												
													<span class="field">
														<input type="text" name="att_titre" id="att_titre" class="gui-input" required placeholder="Titre de l'attestation"
														<?php if(!empty($attestation)){ ?>
															value="<?= $attestation['att_titre'] ?>"
														<?php }?>

														>
														<small class="text-danger texte_required" id="att_titre_required" ></small>
													</span>
												</div>
												<div class="col-md-12 mt20">												
													<label for="att_contenu">Contenu de l'attestation : </label>
													<textarea class="summernote" id="att_contenu" name="att_contenu" required ><?=$attestation['att_contenu']?></textarea>
													<small class="text-danger texte_required" id="att_contenu_required">
														Merci de saisir votre contenu d'attestation
													</small>
												</div>
											</div>
											<div class="section-divider mt20 mb20">
												<span>Ajout des compétences</span>
											</div>
											<?php if(empty($_GET['id'])){ ?>
											<div class="competence">
												<div class="row">
													<div class="col-md-5 col-md-offset-2">													
														<label for="aco_competence_1" >Compétence <span class="nombre_competence_init nombre_competence" data-value="1">1</span> :</label>
														<input type="text" name="aco_competence[1]" id="aco_competence_1" class="gui-input" required placeholder="Libellé de la compétence">
														<small class="text-danger texte_required" id="aco_competence_1_required" ></small>
													</div>
													<div class="col-md-2 mt25">
														<div class="option-group field">
															<label class="option">
																<input type="checkbox" name="aco_competence_pratique[1]" value="1">
																<span class="checkbox"></span> Pratique
															</label>
														</div>
													<!--  -->
													</div>
												</div>
											</div>
											<?php }else{ ?>
											<div class="competence">
												<?php 
												$i = 0;
												$last = end($competences);
												foreach($competences as $c){ 
													$i++;
													if($i != 1){ ?>
													<div class="row competence_input mt20" data-id="<?= $i ?>">
														<div class="col-md-5 col-md-offset-2">													
															<label for="aco_competence_<?= $i ?>" >Compétence <span 
															<?php if($c['aco_id'] == $last['aco_id']){ ?> 
																class="nombre_competence_init nombre_competence"
															<?php }else{ ?> 
																class="nombre_competence_init"
															<?php } ?> 
															data-value="<?= $i ?>"><?= $i ?></span> :</label>
															<input type="text" name="aco_competence[<?= $i ?>]" id="aco_competence_<?= $i ?>" class="gui-input" required placeholder="Libellé de la compétence" value="<?= $c['aco_competence'] ?>">
															<small class="text-danger texte_required" id="aco_competence_<?= $i ?>_required" ></small>
														</div>
														<input type="hidden" name="aco_id[<?= $i ?>]" value="<?= $c['aco_id'] ?>">
														<div class="col-md-2 mt25">
															<div class="option-group field">
																<label class="option">
																	<input type="checkbox" name="aco_competence_pratique[<?= $i ?>]" value="1"
																	<?php if(!empty($c['aco_pratique'])){ ?>
																		checked
																	<?php }?>
																		>
																	<span class="checkbox"></span> Pratique
																</label>
															</div>
														<!--  -->
														</div>
														<?php 
														if($last['aco_id'] == $c['aco_id']){ ?>
															<div class="col-md-1 mt20  btn-suppr" data-id="<?= $i ?>">
																<button type="button" onclick="suppr_competence(<?= $i ?>)" class="btn btn-danger">
																	<i class="fa fa-times"></i>
																</button>
															</div>
														<?php } ?>
													</div>
													<?php }else{ ?>
														<div class="row">
															<div class="col-md-5 col-md-offset-2">													
																<label for="aco_competence_1" >Compétence <span 
																<?php if($c['aco_id'] == $last['aco_id']){ ?> 
																	class="nombre_competence_init nombre_competence"
																<?php }else{ ?> 
																	class="nombre_competence_init"
																<?php } ?> 
																data-value="1">1</span> :</label>
																<input type="text" name="aco_competence[1]" id="aco_competence_1" class="gui-input" required placeholder="Libellé de la compétence" value="<?= $c['aco_competence'] ?>">
																<small class="text-danger texte_required" id="aco_competence_1_required" ></small>
															</div>
															<input type="hidden" name="aco_id[1]" value="<?= $c['aco_id'] ?>">
															<div class="col-md-2 mt25">
																<div class="option-group field">
																	<label class="option">
																		<input type="checkbox" name="aco_competence_pratique[1]" value="1"
																		<?php if(!empty($c['aco_pratique'])){ ?>
																			checked
																		<?php }?>
																			>
																		<span class="checkbox"></span> Pratique
																	</label>
																</div>
															<!--  -->
															</div>
															
														</div>
													<?php } ?>
												<?php } ?>
												</div>
											<?php } ?>
											
											<div class="row bouton_ajout_competence mt20">
												<div class="col-md-8 col-md-offset-2 text-right">
													<button type="button" id="bouton_ajout_competence" class="btn btn-sm btn-primary" onclick="this.blur();">
														<i class="fa fa-plus"></i> Ajouter une compétence
													</button>
												</div>
											</div>

											<!-- fin du contenu du form -->
										</div>
									</div>
								</div>
							</div>
						</div>

					</section>
					<!-- FIN DU CONTENU -->
				</section>
	
			</div>
			<!-- End: Main -->
			
			<!-- DEBUT DU FOOTER -->
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
					<?php if(!empty($_SESSION['retour'])){ ?>
						<a href="<?=$_SESSION["retour"]?>" class="btn btn-default btn-sm" >
							<i class='fa fa-left'></i> Retour
						</a>
					<?php }else{ ?>
						<a href="attestation_liste.php" class="btn btn-default btn-sm" >
							<i class='fa fa-left'></i> Retour
						</a>
					<?php } ?>
					</div>
					<div class="col-xs-6" ></div>
					<div class="col-xs-3 footer-right" >
						<button type="button" class="btn btn-success btn-sm"  id="sub_form">
							<i class='fa fa-save'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
			<!-- FIN DE FOOTER  -->
			
		</form>

		<!-- BEGIN: PAGE SCRIPTS -->

		<!-- jQuery -->
		
<?php	include "includes/footer_script.inc.php"; ?>	
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="vendor/plugins/summernote/summernote.min.js"></script>
		<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				// summernote
				$('.summernote_special').summernote({
					lang: 'fr-FR', // default: 'en-US'
					height: 255, //set editable area's height
					focus: false, //set focus editable area after Initialize summernote
					oninit: function() {},
					onChange: function(contents, $editable) {},
					  toolbar: [
					    // [groupName, [list of button]]
					    ['style', ['bold', 'italic', 'underline', 'clear']],
					    ['font', ['strikethrough', 'superscript', 'subscript']],
					    ['fontsize', ['fontsize']],
					    ['color', ['color']],
					    ['para', ['ul', 'ol', 'paragraph']],
					  ]
				});
				// fin summernote
				
				// envoyer le formulaire
				$("#sub_form").click(function(){
					
					if($("#att_sous_famille option").size()>1){
						$("#att_sous_famille").prop("required",true);
					}else{
						$("#att_sous_famille").prop("required",false);
					}
					if($("#att_sous_sous_famille option").size()>1){
						$("#att_sous_sous_famille").prop("required",true);
					}else{
						$("#att_sous_sous_famille").prop("required",false);
					}
					if(valider_form("#form")){
						// si validation de la question on demande confirmation
						$("#form").submit();
						
					}			
				});	
				
				// fin envoyer le formulaire
				//////////////////// INTERACTIONS ////////////////////
				// initialisation du nombre de client
				var nombre_competence = parseInt($(".nombre_competence").data("value"));

				$("#aco_competence_" + nombre_competence).change(function() {
					// si un client est sélectionné, afficher le bouton pour ajouter un nouveau client
					// à la formation
					if($(this).val() != null){
						console.log($(this).val());
						$(".bouton_ajout_competence").show(400);
					}else{
						$(".bouton_ajout_competence").hide(400);
					}
				});
				// intercation utilisateur : ajouter une compétence
				$("#bouton_ajout_competence").on('click', function() {

					nombre_competence = parseInt($(".nombre_competence").data("value"));
					nombre_competence = nombre_competence + 1;

					$(".nombre_competence").removeClass("nombre_competence");
					 // remove le nombre client
					ajout_competence(nombre_competence);

					$(".btn-suppr").each(function() {
						if($(this).data("id") != nombre_competence){
							$(this).remove();
						}
					});	
				});
				// fin intercation utilisateur : ajouter une compétence
				
				//////////////////// FIN INTERACTIONS ////////////////////
				

			});
			// FUNCTIONS //
			function suppr_competence(nombre){
					
					$(".competence_input").each(function() {
						if($(this).data("id") == nombre){
							$(this).remove();
						}
						$(".nombre_competence_init").each(function() {
							nb_comp = nombre - 1;
							if($(this).data("value") == nb_comp){
								$(this).addClass("nombre_competence");
							}
						});
						
						if($(this).data("id") == nb_comp){
							$(this).append('<div class="col-md-1 mt20 btn-suppr" data-id="' + nb_comp + '"><button type="button" onclick="suppr_competence(' + nb_comp + ')" class="btn btn-danger"><i class="fa fa-times"></i></button></div>')
						}
						
					});
				}
			function ajout_competence(nombre){
				str = "<div class='row competence_input mt20' data-id='" + nombre + "'><div class='col-md-5 col-md-offset-2'><label for='aco_competence_" + nombre + "' >Compétence <span class='nombre_competence_init nombre_competence' data-value='" + nombre + "'>" + nombre + "</span> :</label><input type='text' name='aco_competence[" + nombre + "]' id='aco_competence_" + nombre + "' class='gui-input' required placeholder='Libellé de la compétence'><small class='text-danger texte_required' id='aco_competence_" + nombre + "_required' ></small></div><div class='col-md-2 mt25'><div class='option-group field'><label class='option'><input type='checkbox' name='aco_competence_pratique[" + nombre + "]' value='1'><span class='checkbox'></span> Pratique</label></div></div><div class='col-md-1 mt20  btn-suppr' data-id='" + nombre + "'><button type='button' onclick='suppr_competence(" + nombre + ")' class='btn btn-danger'><i class='fa fa-times'></i></button></div></div>";
				$(".competence").append(str);
			}
			// FIN FUNCTIONS //
			function envoyer_form(form){
				form.submit();
			}
		</script>
	</body>
</html>