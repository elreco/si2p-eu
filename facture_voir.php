<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');

include('modeles/mod_parametre.php');
include('modeles/mod_facture_data.php');




// VISUALISATION D'UNE CONVOCATIONS

$erreur_txt="";

$facture_id=0;
if(!empty($_GET["facture"])){
	$facture_id=intval($_GET["facture"]);
}
if(empty($facture_id)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();

}else{

	// personne connecte
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	if($conn_soc_id!=$acc_societe){
		$acc_societe=$conn_soc_id;
	}

	// DONNEE POUR AFFICHAGE
	$data=array(
		"pages" => array()
	);

	$data["pages"][0]=facture_data($facture_id);
	if(empty($data["pages"][0])){
		$erreur_txt="impossible d'afficher la page";
		echo($erreur_txt);
		die();
	}

	// GESTION DE L'EDITION

	$sql="SELECT pca_id,pca_libelle,pca_planning FROM Produits_Categories";
	if(!$_SESSION["acces"]["acc_droits"][9]){
		$sql.=" WHERE NOT pca_compta";
	}
	$sql.=" ORDER BY pca_libelle;";
	$req=$Conn->query($sql);
	$d_pro_categories=$req->fetchAll();

	$drt_edit=false;
	if($_SESSION["acces"]["acc_droits"][29]){
		if(count($data["pages"])==1 AND !$data["pages"][0]["fac_blocage"]){
			$drt_edit=true;
		}
	}
	// AUTRE FACTURE ASSOCIE
	if($data["pages"][0]["fac_nature"]==1){
		$sql="SELECT fac_id,fac_numero,fac_date,fac_total_ht  FROM Factures WHERE fac_avoir=" . $facture_id . " ORDER BY fac_numero,fac_date;";
		$req=$ConnSoc->query($sql);
		$d_autres=$req->fetchAll();
	}else{
		$sql="SELECT fac_id,fac_numero,fac_date,fac_total_ht FROM Factures WHERE fac_id=" . $data["pages"][0]["fac_avoir"] . ";";
		$req=$ConnSoc->query($sql);
		$d_autres=$req->fetchAll();
	}
	$req = $ConnSoc->prepare("SELECT * FROM Factures_Commentaires
	 WHERE fco_facture = " . $facture_id . " ORDER BY fco_date DESC;");
	$req->execute();
	$factures_commentaires = $req->fetchAll();
}
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#container_print{
				width:21cm;
				font-size:12px;
			}
			.bandeau_alerte{
				background-color:yellow;
				border:2px solid black;
				padding:3px;
				text-align:center;
				font-weight:bold;
				margin:3px 0px;
			}
			.text-foot{
				font-size:10px;
			}

			.ligne-col-1{
				width:20%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-2{
				width:52%;
				padding:1px 5px;
				font-size:10px;
			}
			.ligne-col-3{
				width:11%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-4{
				width:6%;
				padding:1px 5px;
				vertical-align:middle;
			}
			.ligne-col-5{
				width:11%;
				padding:1px 5px;
				vertical-align:middle;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm">

		<div id="zone_print" ></div>


		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php 		if(empty($erreur_txt)){ ?>

							<div class="row" >

								<!-- zone d'affichage du devis -->

								<div class="col-md-8"  >

									<div id="container_print" >

										<div id="page_print" >

								<?php		/*print_r($data["pages"]);
											die();*/
											foreach($data["pages"] as $p => $page){ 	?>

												<div class="page" id="header_<?=$p?>" data-page="<?=$p?>" >
													<header>
														<table>
															<tr>
																<td class="w-40" >
														<?php		if(!empty($page["fac_logo_1"])){ ?>
																		<img src="Documents/Societes/logos/<?=$page["fac_logo_1"]?>" />
														<?php		}else{
																		echo("&nbsp;");
																	} ?>
																</td>
																<td class="w-60" >
															<?php	if($page["fac_nature"]==1){
																		if($page["fac_convention"]==1){
																			echo("<h1>Facture valant Convention</h1>");
																		}else{
																			echo("<h1>Facture client</h1>");
																		}
																	}else{
																		echo("<h1>Avoir client</h1>");
																	}?>
																</td>
															</tr>
														</table>
													</header>
												</div>

												<div id="head_<?=$p?>" class="head" >
													<section>
														<table>
															<tr>
																<td class="w-50 va-t" >
														<?php		echo("<b>" . $page["fac_soc_nom"] . "</b>");
																	if(!empty($page["fac_soc_agence"])){
																		echo("<br/><b>Agence " . $page["fac_soc_agence"] . "</b>");
																	};
																	if(!empty($page["fac_soc_ad1"])){
																		echo("<br/>" . $page["fac_soc_ad1"]);
																	};
																	if(!empty($page["fac_soc_ad2"])){
																		echo("<br/>" . $page["fac_soc_ad2"]);
																	};
																	if(!empty($page["fac_soc_ad3"])){
																		echo("<br/>" . $page["fac_soc_ad3"]);
																	};
																	echo("<br/><b>" . $page["fac_soc_cp"] . " " . $page["fac_soc_ville"] . "</b>");
																	if(!empty($page["fac_soc_tel"])){
																		echo("<br/>Tél. : " . $page["fac_soc_tel"]);
																	}
																	if(!empty($page["fac_soc_fax"])){
																		echo("<br/>Fax. : " . $page["fac_soc_fax"]);
																	}
																	if(!empty($page["fac_soc_mail"])){
																		echo("<br/>" . $page["fac_soc_mail"]);
																	}
																	if(!empty($page["fac_soc_site"])){
																		echo("<br/>" . $page["fac_soc_site"]);
																	}
																	echo("<br/><br/>");
																	echo("Affaire suivie : " . $page["fac_com_identite"]);
																	if(!empty($page["fac_reference"])){
																		echo("<br/>Référence : " . $page["fac_reference"]);
																	}
																	echo("<br/>");
																	echo("Code client : " . $page["fac_cli_code"]);
																	?>
																</td>
																<td class="w-50 va-t" >
														<?php		if(!empty($page["fac_env_adresse"])){
																		echo("<b>" . $page["fac_env_nom"] . "</b>");
																		if(!empty($page["fac_env_agence"])){
																			echo("<br/><b>" . $page["fac_soc_agence"] . "</b>");
																		};
																		if(!empty($page["fac_env_ad1"])){
																			echo("<br/>" . $page["fac_env_ad1"]);
																		};
																		if(!empty($page["fac_env_ad2"])){
																			echo("<br/>" . $page["fac_env_ad2"]);
																		};
																		if(!empty($page["fac_env_ad3"])){
																			echo("<br/>" . $page["fac_env_ad3"]);
																		};
																		echo("<br/><b>" . $page["fac_env_cp"] . " " . $page["fac_env_ville"] . "</b>");
																		echo("<br/><br/><i>Adresse de facturation :</i><br/>");
																	}

																	echo("<b>" . $page["fac_cli_nom"] . "</b>");
																	if(!empty($page["fac_opca"])){
																		if($page["fac_opca_type"]==7){
																			if(!empty($page["fac_opca_compte"])){
																				echo("<br/><b>pour " . $page["fac_opca_compte"] . "</b>");
																			}
																		}
																	}
																	if(!empty($page["fac_cli_service"])){
																		echo("<br/>" . $page["fac_cli_service"]);
																	};
																	if(!empty($page["fac_cli_ad1"])){
																		echo("<br/>" . $page["fac_cli_ad1"]);
																	};
																	if(!empty($page["fac_cli_ad2"])){
																		echo("<br/>" . $page["fac_cli_ad2"]);
																	};
																	if(!empty($page["fac_cli_ad3"])){
																		echo("<br/>" . $page["fac_cli_ad3"]);
																	};
																	echo("<br/><b>" . $page["fac_cli_cp"] . " " . $page["fac_cli_ville"] . "</b>"); ?>
																</td>
															</tr>
														</table>
														<table>
															<tr>
																<td><b>Numéro : <?=$page["fac_numero"]?></b></td>
																<td class="text-right" >
														<?php		$aff_adeq=false;
																	if(!empty($page["fac_date"])){
																		$DT_fac_date=date_create_from_format('Y-m-d',$page["fac_date"]);
																		if(!is_bool($DT_fac_date)){
																			echo(" Le " . utf8_encode(strftime("%A %d %B %Y",$DT_fac_date->getTimestamp())));
																			if($DT_fac_date->format("Y-m-d")<"2020-04-01"){
																				// avant le premier avril, il y avait un affacturage particulier sur adeq
																				$aff_adeq=true;
																			}
																		}
																	} ?>
																</td>
															</tr>
														</table>
													</section>
												</div>

												<div id="ligne_header_<?=$p?>" class="ligne-fac" >
													<div class="table-div" >
														<div class="table-div-th ligne-col-1 text-center" >Référence</div>
														<div class="table-div-th ligne-col-2 text-center" >Désignation</div>
														<div class="table-div-th ligne-col-3 text-center" >Prix HT</div>
														<div class="table-div-th ligne-col-4 text-center" >Qté</div>
														<div class="table-div-th ligne-col-5 text-center" >Montant HT</div>
													</div>
												</div>

										<?php	$tot_tva=array();
												if(!empty($page["lignes"])){
													foreach($page["lignes"] as $ligne){

														$ligne_texte=$ligne["fli_libelle"];
														if(!empty($ligne["fli_formation"])){
															$ligne_texte.="<br/>" . nl2br($ligne["fli_formation"]);
														}
														if(!empty($ligne["fli_complement"])){
															$ligne_texte.="<br/>" . nl2br($ligne["fli_complement"]);
														}
														if(!empty($ligne["fli_stagiaire"])){
															$ligne_texte.="<br/>" . nl2br($ligne["fli_stagiaire"]);
														}
														if(!empty($ligne["fli_exo"])){
															if(!empty($ligne_texte)){
																$ligne_texte.="<br/>";
															}
															$ligne_texte.="<b class='text-danger' >Exonération de TVA article 261-4-4°a du CGI</b>";
														}
														$ht=0;
														if(!empty($ligne["fli_qte"])){
															$ht=$ligne["fli_montant_ht"]/$ligne["fli_qte"];
															$ht=round($ht,2);
														}
														if(!isset($tot_tva[$ligne["fli_tva_taux"]])){
															$tot_tva[$ligne["fli_tva_taux"]]=0;
														}
														$tva=$ligne["fli_montant_ht"]*($ligne["fli_tva_taux"]/100);
														$tva=round($tva,2);														
														$tot_tva[$ligne["fli_tva_taux"]]=$tot_tva[$ligne["fli_tva_taux"]]+$tva;
														?>
														<div class="table-div ligne-body-<?=$p?> ligne-fac" id="ligne_<?=$ligne["fli_id"]?>" >
															<div class="ligne-col-1" >
																<span class="reference" ><?=$ligne["fli_code_produit"]?></span>
													<?php		if(!$drt_edit){ ?>
																	<div class="pull-right" >
																		<button type="button" class="btn btn-xs btn-warning btn-reference" data-toggle="tooltip" title="Modifier les références (OPCO et BC)" data-ligne="<?=$ligne["fli_id"]?>" >
																			<i class="fa fa-barcode" ></i>
																		</button>
																	</div>
													<?php		} ?>
															</div>
															<div class="ligne-col-2" >
																<?=$ligne_texte?>
																<div id="opca_num_<?=$ligne["fli_id"]?>" data-opca_num="<?=$ligne["fli_opca_num"]?>" >
															<?php	if(!empty($ligne["fli_opca_num"])){
																		echo("Numéro de prise en charge : " . $ligne["fli_opca_num"]);
																	} ?>
																</div>
																<div id="bc_num_<?=$ligne["fli_id"]?>" data-bc_num="<?=$ligne["fli_bc_num"]?>" >
															<?php	if(!empty($ligne["fli_bc_num"])){
																		echo("Numéro de BC : " . $ligne["fli_bc_num"]);
																	} ?>
																</div>
															</div>
															<div class="ligne-col-3 text-right" ><?=number_format($ht,2,","," ")?> €</div>
															<div class="ligne-col-4 text-right" ><?=$ligne["fli_qte"]?></div>
															<div class="ligne-col-5 text-right" ><?=number_format($ligne["fli_montant_ht"],2,","," ")?> €</div>
														</div>
										<?php		}
												}
												if(!empty($page["fac_libelle"])){ ?>
													<div class="table-div ligne-body-<?=$p?>" >
														<div class="" ><?=$page["fac_libelle"]?></div>
													</div>
										<?php	} ?>

												<div id="foot_<?=$p?>" >
													<table>
														<tr>
															<td class="w-60" >
														<?php	if($page["fac_affacturage"]==1 AND $acc_societe==17 AND $aff_adeq){ ?>
																	<table class="tab-border" >
																		<tr>
																			<td>
																				REFERENCE RIB A UTILISER : <?=$page["fac_affacturage_iban"]?>
																			</td>
																		</tr>
																	</table>
														<?php	}else{
																	if($page["fac_rib_change"]){ ?>
																		<div class="bandeau_alerte" >
																			ATTENTION : CHANGEMENT DE RIB
																		</div>
															<?php	} ?>
																	<table class="tab-border text-foot" >
																		<tr>
																			<td class="text-center" >IBAN</td>
																			<td class="text-center" >BIC</td>
																		</tr>
																		<tr>
																			<td class="text-center" ><?=$page["fac_rib_iban"]?></td>
																			<td class="text-center" ><?=$page["fac_rib_bic"]?></td>
																		</tr>
																	</table>
																	<p class="text-foot" >
																		En votre aimable règlement par <b><?=$base_reglement[$page["fac_reg_type"]]?></b>
																		<br/>
																		A régler avant le :
																<?php	if(!empty($page["fac_date_reg_prev"])){
																			$DT_periode=date_create_from_format('Y-m-d',$page["fac_date_reg_prev"]);
																			if(!is_bool($DT_periode)){
																				echo("<b>" . utf8_encode(strftime("%A %d %B %Y",$DT_periode->getTimestamp())) . "</b>");
																			}
																		} ?>
																	</p>
														<?php	} ?>
															</td>
															<td class="w-40 va-t" >

																<table class="tab-border" >
																	<tr id="cont_total_ht" >
																		<th class="text-right" >Total H.T.</th>
																		<td class="text-right" id="total_ht" ><?=number_format($page["fac_total_ht"],2,","," ")?> €</td>
																	</tr>
														<?php		foreach($tot_tva as $tva_taux => $tva_montant){ ?>
																		<tr id="tva_<?=$tva_taux?>" class="tva" >
																			<th class="text-right" >T.V.A. à <?=$tva_taux?> %</th>
																			<td class="text-right" ><?=number_format($tva_montant,2,","," ")?> €</td>
																		</tr>
														<?php		} ?>
																	<tr>
																		<th class="text-right" >Total T.T.C.</th>
																		<td class="text-right" id="total_ttc" ><?=number_format($page["fac_total_ttc"],2,","," ")?> €</td>
																	</tr>
																</table>

															</td>
														</tr>
													</table>
											<?php	if($page["fac_affacturage"]==1 AND $acc_societe==17 AND $aff_adeq){ ?>
														<p class="text-danger" >
															Pour être libératoire, votre règlement doit être effectué directement à l'ordre de Crédit Agricole Leasing & Factoring 12 place des Etats-Unis CS 20001 - 92548 MONTROUGE Cedex - France
															qui le reçoit par subrogation dans le cadre d'un contrat d'affacturage et devra être avisé de toute réclamation relative à cette créance.
														</p>
											<?php	}else{
														if($page["fac_compta_change"]){ ?>
															<div class="bandeau_alerte" >
																ATTENTION : CHANGEMENT D'ADRESSE
															</div>
										<?php			} ?>
														<table>
															<tr>
																<td class="w-60 text-foot" >
																	<p>
																		Merci d'adresser vos règlements à :<br/>
														<?php			echo("<b>" . $page["fac_soc_nom"] . "</b>");
																		echo("<br/><b>Service comptabilité</b>");
																		if(!empty($page["fac_compta_ad1"])){
																			echo("<br/>" . $page["fac_compta_ad1"]);
																		};
																		if(!empty($page["fac_compta_ad2"])){
																			echo("<br/>" . $page["fac_compta_ad2"]);
																		};
																		if(!empty($page["fac_compta_ad3"])){
																			echo("<br/>" . $page["fac_compta_ad3"]);
																		};
																		echo("<br/>" . $page["fac_compta_cp"] . " " . $page["fac_compta_ville"]); ?>
																	</p>
																</td>
																<td class="w-40 va-t text-foot" >
															<?php	echo("<br/>" . $page["fac_compta_mail"]);
																	if(!empty($page["fac_compta_tel"])){
																		echo("<br/>Tél. : " . $page["fac_compta_tel"]);
																	};
																	if(!empty($page["fac_compta_fax"])){
																		echo("<br/>Fax : " . $page["fac_compta_fax"]);
																	}; ?>
																</td>
															</tr>
														</table>
											<?php	} ?>
													<p class="text-foot" >
														Escompte pour règlement anticipé : 0% <br/>
														En cas de retard de paiement, une pénalité égale à 3 fois le taux d’intérêt légal et une indemnité forfaitaire pour frais de recouvrement de
														40 euros seront exigibles (Article L 446.1 du Code de Commerce)
													</p>
												</div>

												<div id="footer_<?=$p?>" >
													<footer>
														<table>
															<tr>
																<td class="w-15" >
														<?php		if(!empty($page["fac_qualite_1"])){ ?>
																		<img src="Documents/Societes/logos/<?=$page["fac_qualite_1"]?>" class="img-responsive" />
														<?php		}else{
																		echo("&nbsp;");
																	} ?>
																</td>
																<td class="w-70 text-center" ><?=$page["fac_footer"]?></td>
																<td class="w-15 text-right" >
														<?php		if(!empty($page["fac_qualite_2"])){ ?>
																		<img src="Documents/Societes/logos/<?=$page["fac_qualite_2"]?>" />
														<?php		}else{
																		echo("&nbsp;");
																	} ?>
																</td>
															</tr>
														</table>
													</footer>
												</div>

							<?php			} ?>
										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->

								<!-- DEBUT ANNEXE -->
								<div class="col-md-4" >
							<?php	if(count($data["pages"])==1 AND $data["pages"][0]["fac_blocage"] AND file_exists('documents/Societes/' . $acc_societe . '/Factures/'. $data["pages"][0]["fac_numero"] . '.pdf')){ ?>
										<div class="panel" id="bloc_pdf" >
											<div class="panel-body">
												<div class="row"  >
													<div class="col-md-6 pt5" >
														<a href="documents/Societes/<?=$acc_societe?>/Factures/<?=$data["pages"][0]["fac_numero"]?>.pdf?<?= time() ?>" target="_blank" >
															<i class="fa fa-file-pdf-o" ></i> <?=$data["pages"][0]["fac_numero"]?>
														</a>
													</div>
													<div class="col-md-6" >
														<a href="mail_prep.php?facture=<?=$facture_id?>" class="btn btn-sm btn-info" data-toggle="tooltip" title="Envoyer par mail" >
															<i class="fa fa-envelope-o" ></i>
														</a>
													</div>
												</div>
											</div>
										</div>
							<?php	}
									if(!empty($d_autres)){ ?>

										<div class="panel"  >
											<div class="panel-heading panel-head-sm">
								<?php			if($data["pages"][0]["fac_nature"]==1){
													echo("Avoir(s) associé(s)");
												}else{
													echo("Facture associé");
												} ?>
											</div>
											<div class="panel-body">
												<table class="table" >
								<?php				foreach($d_autres as $autre){ ?>
														<tr>
															<td>
																<a href="facture_voir.php?facture=<?=$autre["fac_id"]?>" ><?=$autre["fac_numero"]?></a>
															</td>
															<td><?=number_format($autre["fac_total_ht"],2,","," ")?> €</td>
														</tr>
								<?php				} ?>
												</table>
											</div>
										</div>

							<?php	} ?>
							<?php


							if(!empty($factures_commentaires)){


								?>

										<div class="panel"  >
											<div class="panel-heading panel-head-sm">
												Commentaires
											</div>
											<div class="panel-body pn">
											<div class="table-responsive">
												<table class="table mt15" >

                                                        <tbody>
                                                    <?php
                                                    $count = 1;
                                                    foreach($factures_commentaires as $fco){

														$req = $Conn->prepare("SELECT uti_nom, uti_prenom FROM Utilisateurs WHERE uti_id = " . $fco['fco_utilisateur']);
														$req->execute();
														$uti_facture_commentaire = $req->fetch();
                                                                ?>
                                                                <tr
                                                                <?php if ($count%2 == 1){  ?>
                                                                    style="background:#FAFAFA!important;"
                                                                <?php }?>
                                                                >
                                                                    <td><?= $uti_facture_commentaire['uti_prenom']?> <?= $uti_facture_commentaire['uti_nom']?></td>
                                                                    <td><?= convert_date_txt($fco['fco_date']); ?></td>



                                                                </tr>
                                                                <tr
                                                                <?php if ($count%2 == 1){  ?>
                                                                style="background:#FAFAFA!important;"
                                                                <?php }?>
                                                                >
                                                                    <td colspan="3">
                                                                        <?=$fco['fco_comment']?>
                                                                    </td>
                                                                </tr>
																<?php
																$count++;
															} ?>
                                                        </tbody>
                                                    </table>
                                                </div>
											</div>
										</div>

							<?php	} ?>
								</div>

								<!-- FIN ANNEXE -->

							</div>


		<?php		}else{ ?>

						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>

		<?php		} ?>

				</section>

				<!-- End: Content -->
			</section>


		</div>
		<!-- End: Main -->

		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
		<?php		if(isset($_SESSION["retourFacture"])){ ?>
						<a href="<?=$_SESSION["retourFacture"]?>" class="btn btn-default btn-sm" >
							Retour
						</a>
		<?php		} ?>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
		<?php		if($drt_edit){ ?>
						<button type="button" class="btn btn-sm btn-info" id="btn_pdf" >
							<i class="fa fa-pdf-o" ></i> Enregistrer en PDF
						</button>
		<?php		}elseif($data["pages"][0]["fac_nature"]==1 AND $_SESSION["acces"]["acc_droits"][29]){ ?>
						<a href="facture.php?avoir=<?=$data["pages"][0]["fac_id"]?>" class="btn btn-sm btn-warning"  >
							<i class="fa fa-euro-o" ></i> Faire un avoir
						</a>
		<?php		}

					// consultation d'une facture bloquée
					if(count($data["pages"])==1 AND $data["pages"][0]["fac_blocage"]){ ?>
						<a href="facture_pdf.php?facture=<?=$facture_id?>&societ=<?=$acc_societe?>" class="btn btn-sm btn-info" id="btn_refresh_pdf" <?php if(file_exists('documents/Societes/' . $acc_societe . '/Factures/'.$data["pages"][0]["fac_numero"] . '.pdf')) echo("style='display:none;'"); ?> >
							<i class="fa fa-refresh" ></i> Regénérer le PDF
						</a>
		<?php		 } ?>
				</div>

				<div class="col-xs-3 footer-right" >
		<?php		if($drt_edit){ ?>
						<a href="facture.php?facture=<?=$data["pages"][0]["fac_id"]?>" class="btn btn-sm btn-warning" >
							<i class="fa fa-pencil" ></i> Modifier
						</a>
		<?php		} ?>
				</div>
			</div>
		</footer>

		<!-- MODAL CONFIRMATION -->
		<div id="modal_confirmation_user" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Confirmer ?</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							Annuler
						</button>
						<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
							Confirmer
						</button>
					</div>
				</div>
			</div>
		</div>

		<!-- AJOUT ET EDITION DE PRODUIT -->
		<div id="modal_reference" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form method="post" action="ajax/ajax_facture_ref_set.php" id="form_fac_ref"  >
						<div>
							<input type="hidden" name="fli_facture" value="<?=$facture_id?>" />
							<input type="hidden" id="ref_ligne" name="fli_id" value="" />
							<input type="hidden" name="societ" value="<?=$acc_societe?>" />
						</div>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
							×
							 </button>
							<h4 class="modal-title">Modification des références</h4>
						</div>
						<div class="modal-body">
							<div class="admin-form theme-primary ">

								<div class="row mt5" >
									<div class="col-md-6" >
										<label for="opca_num" >Numéro de prise en charge OPCO :</label>
										<input type="text" name="fli_opca_num" class="gui-input" id="opca_num" placeholder="Numéro OPCO" value="" />
									</div>
									<div class="col-md-6" >
										<label for="bc_num" >Numéro de bon de commande :</label>
										<input type="text" name="fli_bc_num" class="gui-input" id="bc_num" placeholder="Numéro de BC" value="" />
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success btn-sm" id="sub_fac_ref"  >
								Enregistrer
							</button>
							<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
								Fermer
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>


<?php	include "includes/footer_script.inc.php"; ?>

		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>

		<script type="text/javascript" src="assets/js/print.js"></script>
		<script type="text/javascript">
			var l_page=21;
			var h_page=27;

			var tab_pro_categories=new Array();
	<?php	if(!empty($d_pro_categories)){
				foreach($d_pro_categories as $cat){ ?>
					tab_pro_categories[<?=$cat["pca_id"]?>]="<?=$cat["pca_planning"]?>";
	<?php		}
			} ?>

			var montant_min=0;
			var ligne_produit=0;
			var json_ligne="";

			jQuery(document).ready(function(){

				$(".btn-reference").click(function(){
					ligne=$(this).data("ligne");
					if(ligne>0){
						$("#ref_ligne").val(ligne);
						$("#opca_num").val($("#opca_num_" + ligne).data("opca_num"));
						$("#bc_num").val($("#bc_num_" + ligne).data("bc_num"));
						$("#modal_reference").modal("show");
					}else{
						$("#ref_ligne").val(0);
						$("#opca_num").val("");
						$("#bc_num").val("");
					}
				});

				$("#sub_fac_ref").click(function(){
					if($("#ref_ligne").val()>0){
						envoyer_form_ajax($("#form_fac_ref"),editer_ref_ligne,$("#ref_ligne").val(),"");
						$("#modal_reference").modal("hide");
					}
				});

				$("#btn_pdf").click(function(){
					modal_confirmation_user("La facture ne sera plus modifiable. Êtes-vous sûr de vouloir continuer ?",generer_pdf);
				});

			});

			/*********************************
				FONCTION FACTURE
			**********************************/

			function generer_pdf(){
				document.location="facture_pdf.php?facture=<?=$facture_id?>";
			}

			function editer_ref_ligne(json,ligne){

				if(json){
					if(json.ligne==ligne){
						$("#opca_num_" + ligne).data("opca_num",json.opca_num);
						if(json.opca_num!=""){
							$("#opca_num_" + ligne).html("Numéro de prise en charge : " + json.opca_num);
						}else{
							$("#opca_num_" + ligne).html("");
						}
						$("#bc_num_" + ligne).data("bc_num",json.bc_num);
						if(json.bc_num!=""){
							$("#bc_num_" + ligne).html("Numéro de BC : " + json.bc_num);
						}else{
							$("#bc_num_" + ligne).html("");
						}
						$("#btn_refresh_pdf").show();
						$("#bloc_pdf").hide();

						mise_en_page();
					}
				}

			}



		</script>
	</body>
</html>
