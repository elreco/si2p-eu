 <?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');
include('modeles/mod_parametre.php');
include('modeles/mod_add_notification.php');

/*****************************************************
 *  CLOTURE DE LA DEMANDE D'ACHAT PAR LE DONNEUR D'ORDRE
 *******************************************************/
/* La validation est gérée via commande_valide_date MAIS 
S'il n'y a pas de grille de validation 
OU s'il n'y a qu'une personne en validation ET que cette personne est l'utilisateur qui à cloturé la DA
ALORS ce script peut déclencher la validation de la DA et le passage en BC avec les traitements annexe
*/



///////////////////// Contrôles des parametres ////////////////////
$param_commande = 0;

if(!empty($_GET['commande'])){
    // si les get sont pas remplis
    $param_commande = intval($_GET['commande']);

}
if($param_commande == 0){

	echo("Impossible d'afficher la page");
	die();
}
///////////////////// FIN Contrôles des parametres ////////////////////

    $acc_societe=0;
    if(isset($_SESSION['acces']["acc_societe"])){
        $acc_societe=intval($_SESSION['acces']["acc_societe"]);  
    }

    // ON VERIFIE QUE LA CLOTURE EST AUTORISE

    // La commande

	$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = " . $_GET['commande']);
	$req->execute();
    $commande = $req->fetch();
    if (!empty($commande)) {

        if(empty($commande['com_agence'])){
            $commande['com_agence'] = 0;
        }

        if(empty($commande['com_liv_nom']) OR empty($commande['com_liv_cp']) OR empty($commande['com_liv_ville'])){
            if(empty($commande['com_action'])){
                $_SESSION['message'] = array(
                    "aff" => "modal",
                    "titre" => "Erreur",
                    "type" => "danger",
                    "message" => "Il faut renseigner l'adresse de livraison"
                );
                header("location : commande_voir.php?commande=" . $_GET['commande']);
                die();
            }
        }
        if(empty($commande['com_fac_nom']) OR empty($commande['com_fac_cp']) OR empty($commande['com_fac_ville'])){
            $_SESSION['message'] = array(
                "aff" => "modal",
                "titre" => "Erreur",
                "type" => "danger",
                "message" => "Il faut renseigner l'adresse de facturation"
            );
            header("location : commande_voir.php?commande=" . $_GET['commande']);
            die();
        }
    } else {

        $_SESSION['message'] = array(
            "aff" => "modal",
            "titre" => "Paramètre absent",
            "type" => "danger",
            "message" => "Impossible d'identifier la DA à clôturer!"
        );
        header("location : commande_voir.php?commande=" . $_GET['commande']);
        die();

    }

    // si le commande est liée à une action.

    $act_charge=0;
    if(!empty($commande["com_action"])) {

        if(empty($commande["com_societe"])){
            $_SESSION['message'] = array(
                "aff" => "modal",
                "titre" => "Connexion",
                "type" => "danger",
                "message" => "Vous êtes connecté sur une société autre que celle de la DA et de l'action associée. Il est impossible de clôturer la DA."
            );
            header("location : commande_voir.php?commande=" . $_GET['commande']);
            die();
        }

        $ConnFct=connexion_fct($commande["com_societe"]);

      
        // l'action
        $sql="SELECT act_pro_sous_famille FROM Actions WHERE act_id=:action AND NOT act_archive;";
        $req=$ConnFct->prepare($sql);
        $req->bindParam(":action",$commande['com_action']);
        $req->execute();
        $d_action=$req->fetch();
        if(empty($d_action)) {
            $_SESSION['message'] = array(
                "aff" => "modal",
                "titre" => "Action non identifiée!",
                "type" => "danger",
                "message" => "L'action liée à cette DA n'a pas pu être identifiée. Clôture refusée!"
            );
            header("location : commande_voir.php?commande=" . $_GET['commande']);
            die();
        }

        // l'action doit disposer de CA validé

        $act_ca=0;

        $sql="SELECT SUM(acl_ca) FROM Actions_Clients WHERE acl_action=:action AND NOT acl_archive AND acl_confirme;";
        $req=$ConnFct->prepare($sql);
        $req->bindParam(":action",$commande['com_action']);
        $req->execute();
        $d_ca=$req->fetch();
        if(!empty($d_ca)){
            if($d_ca[0]>0){
                $act_ca=$d_ca[0];   
            }
        }
        if(empty($act_ca)){
            $_SESSION['message'] = array(
                "aff" => "modal",
                "titre" => "Action non confirmée!",
                "type" => "danger",
                "message" => "L'action ne dispose pas de CA validé. Clôture refusée!"
            );
            header("location : commande_voir.php?commande=" . $_GET['commande']);
            die();
        }

        // on regarde si on doit proratiser le CA (mixe intervenant interne et externe)

        $demi_j_total=0;
        $demi_j_st=0;

        $sql="SELECT COUNT(pda_id) as nb_demi_j,int_type FROM Plannings_Dates,Intervenants WHERE pda_intervenant=int_id AND pda_type=1 AND pda_ref_1=:action AND NOT pda_archive
        GROUP BY int_type";
        $req=$ConnFct->prepare($sql);
        $req->bindParam(":action",$commande['com_action']);
        $req->execute();
        $d_dates=$req->fetchAll();
        if (!empty($d_dates)) {

            foreach($d_dates as $d){

                $demi_j_total=$demi_j_total + $d["nb_demi_j"];

                if ($d["int_type"]==3 OR $d["int_type"]==2) {

                    $demi_j_st=$demi_j_st + $d["nb_demi_j"];
                }
            }

        }else{
            $_SESSION['message'] = array(
                "aff" => "modal",
                "titre" => "Action sans date!",
                "type" => "danger",
                "message" => "L'action ne semble pas disposer de date!"
            );
            header("location : commande_voir.php?commande=" . $_GET['commande']);
            die();
        }
        
        // 2 typologie de ST -> on proratise le CA
        if($demi_j_total!=$demi_j_st){

            $act_ca=round(($act_ca/$demi_j_total)*$demi_j_st,2);

        }

        $act_charge=$commande['com_ht'];

        // on regarde s'il y a d'autre DA cloturées pour cette action

        $req = $Conn->prepare("SELECT com_id,com_ht,com_etat FROM commandes WHERE com_societe=:societe AND com_action=:action AND NOT com_id=:commande 
        AND NOT com_annule AND com_etat>0;");
        $req->bindParam(":commande",$commande['com_id']);
        $req->bindParam(":action",$commande['com_action']);
        $req->bindParam(":societe",$commande['com_societe']);
        $req->execute();
        $d_commandes_cloturees = $req->fetchAll();
        if (!empty($d_commandes_cloturees)) {

            foreach($d_commandes_cloturees as $com_clos){

                $act_charge=$act_charge + $com_clos['com_ht'];

            }
        }

        // calcul de la marge

        
        $marge=$act_ca-$act_charge;
        $marge_taux=0;
        if(!empty($act_charge)){
            $marge_taux=round(($marge/$act_charge)*100,2);
        }

        // niveau de déro requis
        $niv_valide=1;

        $req = $Conn->prepare("SELECT psf_marge_montant_1,psf_marge_taux_1
        ,psf_marge_montant_2,psf_marge_taux_2
        FROM Produits_Sous_Familles WHERE psf_id=:sous_famille;");
        $req->bindParam(":sous_famille",$d_action['act_pro_sous_famille']);
        $req->execute();
        $d_sous_famille = $req->fetch();
        if (!empty($d_sous_famille)) {

            if(!empty($d_sous_famille["psf_marge_montant_2"]) AND $marge <= $d_sous_famille["psf_marge_montant_2"]){
                $niv_valide=3;
            } elseif(!empty($d_sous_famille["psf_marge_taux_2"]) AND $marge_taux <= $d_sous_famille["psf_marge_taux_2"]){
                $niv_valide=3;
            }elseif(!empty($d_sous_famille["psf_marge_montant_1"]) AND $marge <= $d_sous_famille["psf_marge_montant_1"]){
                $niv_valide=2;
            } elseif(!empty($d_sous_famille["psf_marge_taux_1"]) AND $marge_taux <= $d_sous_famille["psf_marge_taux_1"]){
                $niv_valide=2;
            }
        }

        // on recupère la validation correspondant à la déro

        $sql="SELECT * FROM commandes_grilles WHERE  cgr_societe = " . $commande['com_societe'] . " 
        AND cgr_montant_min <= " . $commande['com_ht'] . " AND cgr_montant_max >= " .  $commande['com_ht'] . " AND cgr_famille=2";
        if(!empty($commande['com_agence'])){
            $sql.=" AND cgr_agence=" . $commande['com_agence'];
        }
        $req = $Conn->query($sql);
        $grille_st = $req->fetch();
        if(!empty($grille_st)){

            if( !empty($grille_st["cgr_utilisateur_" . $niv_valide]) OR !empty($grille_st["cgr_profil_" . $niv_valide]) OR !empty($grille_st["cgr_droit_" . $niv_valide]) OR ($niv_valide==1 AND !empty($grille_st["cgr_responsable_1"]) ) ){
                $validation_marge=array(
                    "cgr_utilisateur_1" => $grille_st["cgr_utilisateur_" . $niv_valide],
                    "cgr_profil_1" => $grille_st["cgr_profil_" . $niv_valide],
                    "cgr_droit_1" => $grille_st["cgr_droit_" . $niv_valide],
                    "cgr_responsable_1" => 0
                );
                if($niv_valide==1 AND !empty($grille_st["cgr_responsable_1"]) ){
                    $validation_marge["cgr_responsable_1"]=$grille_st["cgr_responsable_1"];
                }

            }else{
                $_SESSION['message'] = array(
                    "aff" => "modal",
                    "titre" => "Grille de validation",
                    "type" => "danger",
                    "message" => "La grille de validation 'sous-traitance' est incomplète! Contacter le DAF."
                );
            }



        }else{
            $_SESSION['message'] = array(
                "aff" => "modal",
                "titre" => "Grille de validation",
                "type" => "danger",
                "message" => "Les paramètres actuels ne permettent pas d'identifier la grille de validation. Clôture la DA refusée. Contactez le DAF"
            );
            header("location : commande_voir.php?commande=" . $_GET['commande']);
            die();
        }
    }
    // fin traitement action
            
    // on recupère les familles d'achats lié au BC pour construire les grilles
   
    $fam="";
	$req = $Conn->prepare("SELECT DISTINCT cli_famille FROM commandes_lignes WHERE cli_commande = " . $_GET['commande']);
	$req->execute();
	$familles = $req->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($familles)){
		$tab_fam=array_column ($familles,"cli_famille");
		$fam=implode($tab_fam,",");
	}
	if(empty($fam)){

		$_SESSION['message'] = array(
            "aff" => "modal",
			"titre" => "DA vide",
			"type" => "danger",
			"message" => "Votre DA ne contient aucune ligne. Clôture refusée."
		);
		header("location : commande_voir.php?commande=" . $_GET['commande']);
        die();
        
	}else{

        // construire la grille
        // fam ne peut pas etre vide puisqu'il y a un controle avant
        $sql="SELECT * FROM commandes_grilles WHERE  cgr_societe = " . $commande['com_societe'] . " AND cgr_montant_min <= " . $commande['com_ht'] . " AND cgr_montant_max >= " .  $commande['com_ht'] . " 
        AND cgr_famille IN (" . $fam . ") AND NOT cgr_famille=2";
        if(!empty($commande['com_agence'])){
            $sql.=" AND cgr_agence = " . $commande['com_agence'];
        }
	    $req = $Conn->query($sql);
        $validations = $req->fetchAll();
        if(empty($validations) AND empty($commande["com_action"])){
            // il n'y a pas de grille et ce n'est pas une DA ST

            $_SESSION['message'] = array(
                "aff" => "modal",
                "titre" => "Grille de validation",
                "type" => "danger",
                "message" => "La grille de validation pour cette DA n'est pas renseignée. Clôture refusée."
            );
            header("location : commande_voir.php?commande=" . $_GET['commande']);
            die();

        }elseif (!empty($commande["com_action"])){
            // on ajout la grille st marge à la grille de validation 
            $validations[]=$validation_marge;
        }
    }
    
    // aller chercher le fournisseur et calculer le dépassement.

    $mois_actuel = date("n");
    if($mois_actuel < 4){
        $year = date("Y",strtotime("-1 year"));
    }else{
        $year = date("Y");
    }
    $date = "01/04/" . $year;
    $date_sql = convert_date_sql($date);


    $req = $Conn->prepare("SELECT SUM(com_ht) FROM commandes WHERE com_fournisseur = " . $commande['com_fournisseur'] . " AND com_date >= " . $date_sql);
    $req->execute();
    $somme_ht = $req->fetch();

    $req = $Conn->prepare("SELECT fou_montant_max, fou_depassement_autorise FROM fournisseurs WHERE fou_id = " . $commande['com_fournisseur']);
    $req->execute();
    $f = $req->fetch();

    if($f['fou_montant_max'] > 0){

        if($f['fou_montant_max'] < $somme_ht["SUM(com_ht)"]){
            $pourcentage = $somme_ht["SUM(com_ht)"] - $f['fou_montant_max'];
            if($f["fou_depassement_autorise"] == 1){ // le dépassement est autorisé

                // envoyer notif à DAF
                add_notifications("<i class='fa fa-truck'></i>",$_SESSION['acces']['acc_prenom'] . " " . $_SESSION['acces']['acc_nom'] . " a clôturé un bon de commande (" . $commande['com_numero'] . ") qui a un dépassement de " . $pourcentage . " €","bg-danger","commande_voir.php?commande=" . $_GET['commande'],11,"","",$_SESSION['acces']['acc_societe'],$_SESSION['acces']['acc_agence'],0);
                // fin envoyer notif à DAF

            }else{
                Header('Location: commande_voir.php?erreur=1&depassement=' . $pourcentage);
            }
        }
    }

    // FIN DES CONTROLES
    // la clotures est autorisé!

    // ***** DONNEE COMPLEMENTAIRE POUR L'ENREGISTREMENT *****

    // Informations sur le donneur d'ordre.
    $req = $Conn->prepare("SELECT uti_responsable,uti_profil,uti_nom,uti_prenom FROM utilisateurs WHERE uti_id = " . $commande['com_donneur_ordre']);
    $req->execute();
    $donneur_ordre = $req->fetch();

    // Responsable du donneur d'ordre
    $req = $Conn->prepare("SELECT uti_id, uti_prenom, uti_nom FROM utilisateurs WHERE uti_id = " . $donneur_ordre['uti_responsable']);
    $req->execute();
    $responsable_do = $req->fetch();


    // *** ENREGISTREMENT DE LA CLOTURE

    // requetes préparées

    // ajout d'une validation sur le BC
    $req_add_valide = $Conn->prepare("INSERT INTO commandes_validations (cva_commande, cva_utilisateur, cva_profil, cva_droit, cva_nom_prenom, cva_date) VALUES (:cva_commande, :cva_utilisateur, :cva_profil, :cva_droit, :cva_nom_prenom, :cva_date);");
    $req_add_valide->bindParam(":cva_commande",$param_commande);

    // recherche d'un doublon de validation
    $req_get_doublon = $Conn->prepare("SELECT * FROM commandes_validations WHERE cva_commande = :cva_commande AND cva_utilisateur = :cva_utilisateur;");
    $req_get_doublon->bindParam(":cva_commande",$param_commande);

     // recherche d'un doublon de validation
     $req_get_doublon_droit = $Conn->prepare("SELECT * FROM commandes_validations WHERE cva_commande = :cva_commande AND cva_droit = :cva_droit;");
     $req_get_doublon_droit->bindParam(":cva_commande",$param_commande);


    // recherche d'un utilisateur (validant)
    $req_get_validant = $Conn->prepare("SELECT uti_nom, uti_prenom FROM utilisateurs WHERE uti_id = :validant;");

    // recherche d'un profil (validant)
    $req_get_profil_validant = $Conn->prepare("SELECT pro_libelle FROM profils WHERE pro_id = :profil_validant;");

	foreach($validations as $v){

        for($bcl_validation=1;$bcl_validation<=4;$bcl_validation++){

            if(!empty($v['cgr_utilisateur_' . $bcl_validation])){ 
                
                // si utilisateur

                // VOIR SI DOUBLON

                $req_get_doublon->bindParam(":cva_utilisateur",$v['cgr_utilisateur_' . $bcl_validation]);
                $req_get_doublon->execute();
                $commandes_validations_exist = $req_get_doublon->fetch();
                if(empty($commandes_validations_exist)){

                    $req_get_validant ->bindParam(":validant",$v['cgr_utilisateur_' . $bcl_validation]);
                    $req_get_validant->execute();
                    $d_validant = $req_get_validant->fetch();
                    $validant = $d_validant['uti_prenom'] . " " . $d_validant['uti_nom'];

                    // validation auto si le DO est lui même dans la grille de validation 
                    // Attention : la validation auto concerne le DO pas l'uti connecte!
                    $cva_date=null;
                    if( $v['cgr_utilisateur_' . $bcl_validation] == $commande['com_donneur_ordre']){
                        $cva_date=date("Y-m-d");
                    }
                     // insérer la validation
                    $req_add_valide->bindParam(":cva_utilisateur",$v['cgr_utilisateur_' . $bcl_validation]);
                    $req_add_valide->bindValue(":cva_profil",0);
                    $req_add_valide->bindValue(":cva_droit",0);
                    $req_add_valide->bindParam(":cva_nom_prenom",$validant);
                    $req_add_valide->bindParam(":cva_date",$cva_date);
                    $req_add_valide->execute();

                    if(empty($cva_date)){
                        // notification
                        add_notifications("<i class='fa fa-truck'></i>",$_SESSION['acces']['acc_prenom'] . " " . $_SESSION['acces']['acc_nom'] . " a clôturé un bon de commande (" . $commande['com_numero'] . ") et vous devez le valider","bg-info","commande_voir.php?commande=" . $_GET['commande'],"","",$v['cgr_utilisateur_' . $bcl_validation],$_SESSION['acces']['acc_societe'],$_SESSION['acces']['acc_agence'],0);
                        // fin notification
                    }
                }

            }elseif(!empty($v['cgr_profil_' . $bcl_validation])){ 

                // si profil

            
                $cva_date=null;
                $cva_utilisateur=0;
                $cva_nom_prenom=null;

                $doublon=false;

                if($donneur_ordre["uti_profil"]==$v['cgr_profil_' . $bcl_validation]){  
                    
                    // Le DO à le profil correspondant à la validation
                    $cva_date=date("Y-m-d");
                    $cva_utilisateur=$commande['com_donneur_ordre'];
                    $cva_nom_prenom=$donneur_ordre['uti_prenom'] . " " . $donneur_ordre['uti_nom'];

                    // on verifie qu'une validation pour ce user n'existe pas déjà.
                    $req_get_doublon->bindParam(":cva_utilisateur",$cva_utilisateur);
                    $commandes_validations_exist = $req_get_doublon->fetch();
                    if(!empty($commandes_validations_exist)){
                        $doublon=true;
                    }

                }else{

                    $req_get_profil_validant ->bindParam(":profil_validant",$v['cgr_profil_' . $bcl_validation]);
                    $req_get_profil_validant ->execute();
                    $profil = $req_get_profil_validant->fetch();
                    if(!empty($profil)){
                        $cva_nom_prenom=$profil['pro_libelle'];
                    }

                }

                if(!$doublon){

                    // insérer la validation
                    $req_add_valide->bindParam(":cva_utilisateur",$cva_utilisateur);
                    $req_add_valide->bindValue(":cva_profil",$v['cgr_profil_' . $bcl_validation]);
                    $req_add_valide->bindValue(":cva_droit",0);
                    $req_add_valide->bindParam(":cva_nom_prenom",$cva_nom_prenom);
                    $req_add_valide->bindParam(":cva_date",$cva_date);
                    $req_add_valide->execute();

                    // notification
                    add_notifications("<i class='fa fa-truck'></i>",$_SESSION['acces']['acc_prenom'] . " " . $_SESSION['acces']['acc_nom'] . " a clôturé un bon de commande (" . $commande['com_numero'] . ") et vous devez le valider","bg-info","commande_voir.php?commande=" . $_GET['commande'],$v['cgr_profil_' . $bcl_validation],"","",$_SESSION['acces']['acc_societe'],$_SESSION['acces']['acc_agence'],0);
                    // fin notification
                   
                }

            }elseif(!empty($v['cgr_responsable_' . $bcl_validation])){

                // validation par le responsable du DO

                // on verifie qu'une validation pour ce user n'existe pas déjà.
                $req_get_doublon->bindParam(":cva_utilisateur",$responsable_do['uti_id']);
                $commandes_validations_exist = $req_get_doublon->fetch();
                if(empty($commandes_validations_exist)){

                    // on insert la validation
                    // on ne gere pas le cas ou le user connecté serait le responsable du DO donc validation auto.
                
                    $req_add_valide->bindParam(":cva_utilisateur",$responsable_do['uti_id']);
                    $req_add_valide->bindValue(":cva_profil",0);
                    $req_add_valide->bindValue(":cva_droit",0);
                    $req_add_valide->bindValue(":cva_nom_prenom",$responsable_do['uti_prenom'] . " " . $responsable_do['uti_nom']);
                    $req_add_valide->bindValue(":cva_date",null);
                    $req_add_valide->execute();
                    add_notifications("<i class='fa fa-truck'></i>",$_SESSION['acces']['acc_prenom'] . " " . $_SESSION['acces']['acc_nom'] . " a clôturé un bon de commande (" . $commande['com_numero'] . ") et vous devez le valider","bg-info","commande_voir.php?commande=" . $_GET['commande'],"","",$responsable_do['uti_id'],$_SESSION['acces']['acc_societe'],$_SESSION['acces']['acc_agence'],0);
                }

            }elseif(!empty($v['cgr_droit_' . $bcl_validation])){

                // validation un utilisateur ayant un droit spécifique

                // on verifie qu'une validation pour ce user n'existe pas déjà.
                $req_get_doublon_droit->bindParam(":cva_droit",$v['cgr_droit_' . $bcl_validation]);
                $droit_validation_exist = $req_get_doublon_droit->fetch();
                if(empty($droit_validation_exist)){

                    // on recupère le libelle du droit

                    $req_get_droit = $Conn->prepare("SELECT drt_nom FROM Droits WHERE drt_id=:droit;");
                    $req_get_droit->bindParam(":droit",$v['cgr_droit_' . $bcl_validation]);
                    $req_get_droit->execute();
                    $d_droit=$req_get_droit->fetch();


                    // on insert la validation
                    // on ne gere pas le cas ou le user connecté serait le responsable du DO donc validation auto.
                
                    $req_add_valide->bindValue(":cva_utilisateur",0);
                    $req_add_valide->bindValue(":cva_profil",0);
                    $req_add_valide->bindParam(":cva_droit",$v['cgr_droit_' . $bcl_validation]);
                    $req_add_valide->bindValue(":cva_nom_prenom",$d_droit["drt_nom"]);
                    $req_add_valide->bindValue(":cva_date",null);
                    $req_add_valide->execute();
                    add_notifications("<i class='fa fa-truck'></i>",$_SESSION['acces']['acc_prenom'] . " " . $_SESSION['acces']['acc_nom'] . " a clôturé un bon de commande (" . $commande['com_numero'] . ") et vous devez le valider","bg-info","commande_voir.php?commande=" . $_GET['commande'],"",$v['cgr_droit_' . $bcl_validation],"",$_SESSION['acces']['acc_societe'],$_SESSION['acces']['acc_agence'],0);
                }
            }

        }
    }
    // fin de la boucle sur les grilles de validation

    // TRAITEMENT ANNEXE

    // etat du BC

    $req = $Conn->prepare("SELECT * FROM commandes_validations WHERE cva_commande = " . $commande['com_id'] . " AND ISNULL(cva_date);");
    $req->execute();
    $non_valide = $req->fetchAll();
    if(!empty($non_valide)){

        // il reste des utilisateurs qui n'ont pas validés la DA

        // mettre l'état de la DA a en cours de validation
        $req = $Conn->prepare("UPDATE commandes SET com_etat = 1 WHERE com_id = " . $_GET['commande']);
        $req->execute();

    }else{

        // Tous les utilisateurs ont validé la DA -> elle passe en BC

        $commande["com_numero"]= str_replace("DA","BC",$commande["com_numero"]);

        $req = $Conn->prepare("UPDATE commandes SET com_etat = 2,com_numero='" .  $commande["com_numero"] . "' WHERE com_id = " . $commande['com_id']);
        $req->execute();

        // on notifie le DO
        // notifications
        add_notifications("<i class='fa fa-truck'></i>","Votre bon de commande " . $commande['com_numero'] . " est validé","bg-info","commande_voir.php?commande=" . $_GET['commande'],"","",$commande['com_donneur_ordre'],$_SESSION['acces']['acc_societe'],$_SESSION['acces']['acc_agence'],0);
        // fin notifications

        // on genere le PDF
        // générer un pdf
        include('modeles/mod_commande_pdf.php');
        // fin générer un pdf


    }


	// CHARGE SUR L'ACTION
	if(!empty($commande['com_action'])){

		$sql="UPDATE Actions SET act_charge=:charge WHERE act_id=:action;";
		$req=$ConnFct->prepare($sql);
		$req->bindParam(":charge",$act_charge);
		$req->bindParam(":action",$commande['com_action']);
		$req->execute();

	}
    Header('Location: commande_voir.php?commande=' . $_GET['commande'] . '&succes=4');

?>
