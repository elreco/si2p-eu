<?php

	include "includes/controle_acces.inc.php";
	
	include_once 'includes/connexion.php';
	
	include 'modeles/mod_parametre.php';
	include 'modeles/mod_get_passe_validite.php';
	include 'modeles/mod_upload.php';
	include 'modeles/mod_document.php';
	include 'modeles/mod_add_utilisateur_etech.php';
	include 'modeles/mod_set_utilisateur_etech.php';
	// SCRIPT POUR L'AJOUT D'UN NOUVEL Utilisateur
	
	$erreur="";
	if(!empty($_POST['uti_id'])){
		$utilisateur = intval($_POST['uti_id']);

	}else{
		$utilisateur=0;
	}
	
	
	// champ required
	
	if(!empty($_POST["uti_nom"])){
		$uti_nom=trim($_POST["uti_nom"]);
	}else{
		$erreur="Formulaire incomplet";
	}
	
	if(!empty($_POST["uti_prenom"])){
		$uti_prenom=$_POST["uti_prenom"];
	}else{
		$erreur="Formulaire incomplet";
	}
	
	if(!empty($_POST["uti_profil"])){
		$uti_profil=intval($_POST["uti_profil"]);
	}else{
		$erreur="Formulaire incomplet";
	}
	
	if(!empty($_POST["uti_societe"])){
		$uti_societe=$_POST["uti_societe"];
	}else{
		$erreur="Formulaire incomplet";
	}

	if(empty($erreur)){
		// CONTROLE AVANT CREATION
		// SI C'EST UNE EDITION
		$req=$Conn->prepare("SELECT uti_id FROM Utilisateurs WHERE uti_nom=:uti_nom AND uti_prenom=:uti_prenom AND uti_id != :uti_id;");
		$req->bindParam(":uti_nom",$uti_nom);
		$req->bindParam(":uti_prenom",$uti_prenom);
		$req->bindParam(":uti_id",$utilisateur);
		$req->execute();
		
			
		$d_doublon=$req->fetch();
		if(!empty($d_doublon)){
			$erreur="L'utilisateur " . $uti_prenom . " " . $uti_nom . " existe déjà.";
		}
	}
	
	if(empty($erreur)){

		// RECUPERER L'UTILISATEUR AVANT EDITION

		$req=$Conn->prepare("SELECT uti_societe, uti_profil, uti_agence FROM Utilisateurs WHERE uti_id = :uti_id;");
		$req->bindParam(":uti_id",$utilisateur);
		$req->execute();
		$d_utilisateur_old = $req->fetch();
		
		// AUTRE CHAMP DU FORM
		$uti_titre=0;
		if(!empty($_POST["uti_titre"])){
			$uti_titre=intval($_POST["uti_titre"]);
		}
		
		$uti_service=0;
		if(!empty($_POST["uti_service"])){
			$uti_service=intval($_POST["uti_service"]);
		}
		
		$uti_responsable=0;
		if(!empty($_POST["uti_responsable"])){
			$uti_responsable=intval($_POST["uti_responsable"]);
		}
		
		$uti_contrat=0;
		if(!empty($_POST["uti_contrat"])){
			$uti_contrat=intval($_POST["uti_contrat"]);
		}
		
		$uti_population=0;
		if(!empty($_POST["uti_population"])){
			$uti_population=intval($_POST["uti_population"]);
		}
		
		$uti_charge=0;
		if(!empty($_POST["uti_charge"])){
			$uti_charge=floatval($_POST["uti_charge"]);
		}
		
		$uti_charge_dom=0;
		if(!empty($_POST["uti_charge_dom"])){
			$uti_charge_dom=floatval($_POST["uti_charge_dom"]);
		}
		
		$uti_dist_dom=0;
		if(!empty($_POST["uti_dist_dom"])){
			$uti_dist_dom=intval($_POST["uti_dist_dom"]);
		}
		
		$uti_carte_affaire=0;
		if(!empty($_POST["uti_carte_affaire"])){
			$uti_carte_affaire=1;
		}
		
		$uti_date_entree=NULL;
		if(!empty($_POST["uti_date_entree"])){
			$uti_date_entree=convert_date_sql($_POST["uti_date_entree"]);
		}
		
		$uti_tranche_km=0;
		if(!empty($_POST["uti_tranche_km"])){
			$uti_tranche_km=intval($_POST["uti_tranche_km"]);
		}
		$uti_agence=0;
		if(!empty($_POST["uti_agence"])){
			$uti_agence=intval($_POST["uti_agence"]);
		}
		
		$uti_veh_cv=0;
		if(!empty($_POST["uti_veh_cv"])){
			$uti_veh_cv=intval($_POST["uti_veh_cv"]);
		}
		$uti_archive=0;
		if(!empty($_POST["uti_archive"])){
			$uti_archive=intval($_POST["uti_archive"]);
		}
		$uti_saisie_rapport=0;
		if(!empty($_POST["uti_saisie_rapport"])){
			$uti_saisie_rapport=intval($_POST["uti_saisie_rapport"]);
		}
		// EDITION
		if(!empty($_POST['uti_id'])){

			// ARCHIVAGE
			$sql="UPDATE Utilisateurs SET 
			uti_titre=:uti_titre,
			uti_nom=:uti_nom,
			uti_prenom=:uti_prenom,
			uti_ad1=:uti_ad1,
			uti_ad2=:uti_ad2,
			uti_ad3=:uti_ad3,
			uti_cp=:uti_cp,
			uti_ville=:uti_ville,
			uti_tel_perso=:uti_tel_perso,
			uti_mobile_perso=:uti_mobile_perso,
			uti_mail_perso=:uti_mail_perso,
			uti_tel=:uti_tel,
			uti_fax=:uti_fax,
			uti_mobile=:uti_mobile,
			uti_mail=:uti_mail,
			uti_matricule=:uti_matricule,
			uti_societe=:uti_societe,
			uti_agence=:uti_agence,
			uti_profil=:uti_profil,
			uti_service=:uti_service,
			uti_responsable=:uti_responsable,
			uti_fonction=:uti_fonction,
			uti_contrat=:uti_contrat,
			uti_population=:uti_population,
			uti_charge=:uti_charge,
			uti_charge_dom=:uti_charge_dom,
			uti_dist_dom=:uti_dist_dom,
			uti_carte_affaire=:uti_carte_affaire,
			uti_date_entree=:uti_date_entree,
			uti_tranche_km=:uti_tranche_km,
			uti_veh_cv=:uti_veh_cv,
			uti_saisie_rapport=:uti_saisie_rapport,
			uti_archive = :uti_archive WHERE uti_id = :uti_id";
			$req = $Conn->prepare($sql);
			$req->bindParam(":uti_id",$_POST['uti_id']);
			$req->bindParam(":uti_archive",$uti_archive);
		}else{
			// CREATION
			$sql="INSERT INTO Utilisateurs (uti_titre,uti_nom,uti_prenom,uti_ad1,uti_ad2,uti_ad3,uti_cp,uti_ville,uti_tel_perso,uti_mobile_perso,uti_mail_perso,uti_tel,uti_fax,uti_mobile,uti_mail,uti_matricule,uti_societe,uti_agence,uti_profil,uti_service,uti_responsable,uti_fonction,uti_contrat,uti_population,uti_charge,uti_charge_dom,uti_dist_dom,uti_carte_affaire,uti_date_entree,uti_tranche_km,uti_veh_cv,uti_saisie_rapport)";
			$sql.=" VALUES (:uti_titre,:uti_nom,:uti_prenom,:uti_ad1,:uti_ad2,:uti_ad3,:uti_cp,:uti_ville,:uti_tel_perso,:uti_mobile_perso,:uti_mail_perso,:uti_tel,:uti_fax,:uti_mobile,:uti_mail,:uti_matricule,:uti_societe,:uti_agence,:uti_profil,:uti_service,:uti_responsable,:uti_fonction,:uti_contrat,:uti_population,:uti_charge,:uti_charge_dom,:uti_dist_dom,:uti_carte_affaire,:uti_date_entree,:uti_tranche_km,:uti_veh_cv,:uti_saisie_rapport)";
			$req = $Conn->prepare($sql);
		}
		
		
		$req->bindParam(":uti_titre",$uti_titre);
		$req->bindParam(":uti_nom",$uti_nom);
		$req->bindParam(":uti_prenom",$uti_prenom);
		$req->bindParam(":uti_ad1",$_POST["uti_ad1"]);
		$req->bindParam(":uti_ad2",$_POST["uti_ad2"]);
		$req->bindParam(":uti_ad3",$_POST["uti_ad3"]);
		$req->bindParam(":uti_cp",$_POST["uti_cp"]);
		$req->bindParam(":uti_ville",$_POST["uti_ville"]);
		$req->bindParam(":uti_tel_perso",$_POST["uti_tel_perso"]);
		$req->bindParam(":uti_mobile_perso",$_POST["uti_mobile_perso"]);
		$req->bindParam(":uti_mail_perso",$_POST["uti_mail_perso"]);
		$req->bindParam(":uti_tel",$_POST["uti_tel"]);
		$req->bindParam(":uti_fax",$_POST["uti_fax"]);
		$req->bindParam(":uti_mobile",$_POST["uti_mobile"]);
		$req->bindParam(":uti_mail",$_POST["uti_mail"]);
		$req->bindParam(":uti_matricule",$_POST["uti_matricule"]);
		$req->bindParam(":uti_societe",$uti_societe);
		$req->bindParam(":uti_agence",$uti_agence);
		$req->bindParam(":uti_profil",$uti_profil);
		$req->bindParam(":uti_service",$uti_service);
		$req->bindParam(":uti_responsable",$uti_responsable);
		$req->bindParam(":uti_fonction",$_POST["uti_fonction"]);
		$req->bindParam(":uti_contrat",$uti_contrat);
		$req->bindParam(":uti_population",$uti_population);
		$req->bindParam(":uti_charge",$uti_charge);
		$req->bindParam(":uti_charge_dom",$uti_charge_dom);
		$req->bindParam(":uti_dist_dom",$uti_dist_dom);
		$req->bindParam(":uti_carte_affaire",$uti_carte_affaire);
		$req->bindParam(":uti_date_entree",$uti_date_entree);
		$req->bindParam(":uti_tranche_km",$uti_tranche_km);
		$req->bindParam(":uti_veh_cv",$uti_veh_cv);
		$req->bindParam(":uti_saisie_rapport",$uti_saisie_rapport);
		try{
			$req->execute();
			if(empty($_POST['uti_id'])){
				$utilisateur=$Conn->lastInsertId();
			}
			
		}Catch(Exception $e){
			$erreur=$e->getMessage();
		}
	}
	
	// TRAITEMENT
	
	if(empty($erreur)){
		
		$warning=array();
		
		// CREATION DE L'ACCES
		
		if(!empty($_POST["acc_uti_ident"]) AND !empty($_POST["acc_uti_passe"])){
			
			$acc_passe_modif=get_passe_validite();
			
			if(!is_bool($acc_passe_modif)){
				if(!empty($_POST['uti_id'])){
					// AJOUTER CONTROLE SI LE MOT DE PASSE EST DIFFERENT
					$sql="SELECT acc_uti_passe FROM Acces  
					WHERE acc_ref = 1 AND acc_ref_id = " . $utilisateur;
					$req = $Conn->prepare($sql);
					$req->execute();
					$acces = $req->fetch();
					if($acces['acc_uti_passe'] != $_POST["acc_uti_passe"]){
						// + ARCHIVE
						// EDITION
						if(!empty($acces)){
							$sql="UPDATE Acces SET acc_uti_ident =:acc_uti_ident,acc_uti_passe=:acc_uti_passe,acc_passe_modif=:acc_passe_modif,acc_profil=:acc_profil,acc_mail=:acc_mail,acc_archive = :acc_archive   
							WHERE acc_ref = 1 AND acc_ref_id = " . $utilisateur;
							$req = $Conn->prepare($sql);
							$req->bindParam(":acc_uti_ident",$_POST["acc_uti_ident"]);
							$req->bindParam(":acc_uti_passe",$_POST["acc_uti_passe"]);
							$req->bindParam(":acc_passe_modif",$acc_passe_modif);
							$req->bindParam(":acc_profil",$uti_profil);
							$req->bindParam(":acc_mail",$_POST["uti_mail"]);
							$req->bindParam(":acc_archive",$uti_archive);
						}else{
							if(!empty($_POST["acc_uti_ident"]) && !empty($_POST["acc_uti_passe"])){
								// CREATION
								$sql="INSERT INTO Acces (acc_ref,acc_ref_id,acc_uti_ident,acc_uti_passe,acc_passe_modif,acc_profil,acc_mail)
								VALUES (1," . $utilisateur . ",:acc_uti_ident,:acc_uti_passe,:acc_passe_modif,:acc_profil,:acc_mail)";
								$req = $Conn->prepare($sql);
								$req->bindParam(":acc_uti_ident",$_POST["acc_uti_ident"]);
								$req->bindParam(":acc_uti_passe",$_POST["acc_uti_passe"]);
								$req->bindParam(":acc_passe_modif",$acc_passe_modif);
								$req->bindParam(":acc_profil",$uti_profil);
								$req->bindParam(":acc_mail",$_POST["uti_mail"]);
							}else{
								// DELETE
								$sql="DELETE FROM Acces WHERE acc_ref = 1 AND acc_ref_id = ". $utilisateur;
								$req = $Conn->prepare($sql);
								$req->execute();
							} 
							
						}
						
					}else{
						$sql="UPDATE Acces SET acc_uti_ident =:acc_uti_ident,acc_uti_passe=:acc_uti_passe,acc_profil=:acc_profil,acc_mail=:acc_mail,acc_archive = :acc_archive   
						WHERE acc_ref = 1 AND acc_ref_id = " . $utilisateur;
						$req = $Conn->prepare($sql);
						$req->bindParam(":acc_uti_ident",$_POST["acc_uti_ident"]);
						$req->bindParam(":acc_uti_passe",$_POST["acc_uti_passe"]);
						$req->bindParam(":acc_profil",$uti_profil);
						$req->bindParam(":acc_mail",$_POST["uti_mail"]);
						$req->bindParam(":acc_archive",$uti_archive);

					}
					

				}else{
					// CREATION
					$sql="INSERT INTO Acces (acc_ref,acc_ref_id,acc_uti_ident,acc_uti_passe,acc_passe_modif,acc_profil,acc_mail)
					VALUES (1," . $utilisateur . ",:acc_uti_ident,:acc_uti_passe,:acc_passe_modif,:acc_profil,:acc_mail)";
					$req = $Conn->prepare($sql);
					$req->bindParam(":acc_uti_ident",$_POST["acc_uti_ident"]);
					$req->bindParam(":acc_uti_passe",$_POST["acc_uti_passe"]);
					$req->bindParam(":acc_passe_modif",$acc_passe_modif);
					$req->bindParam(":acc_profil",$uti_profil);
					$req->bindParam(":acc_mail",$_POST["uti_mail"]);
				}
				
				try{
					$req->execute();	
				}Catch(Exception $e){
					echo($e->getMessage());
					$warning[]="L'accès n'a pas été enregistré";
				}
			}
		}else{ // DELETE ACCES
			$sql="DELETE FROM Acces WHERE acc_ref = 1 AND acc_ref_id = ". $utilisateur;
			$req = $Conn->prepare($sql);
			$req->execute();
		}
		
		// CHARGEMENT DE LA CARTE GRISE
		
		if (!empty($_FILES['uti_carte_grise']['name'])){
			$path = $_FILES['uti_carte_grise']['name'];
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			// on injecte la date du fichier pour eviter la mise ne cache
			$uti_carte_grise="carte_grise_" . $utilisateur . "_" . date('m-d-Y');
			$upload_erreur = upload('uti_carte_grise',"Utilisateurs/",$uti_carte_grise, 0, array("pdf"), 1); 
		
			if($upload_erreur==0){
				$uti_carte_grise.="." . $ext;
				$sql="UPDATE Utilisateurs SET uti_carte_grise='" . $uti_carte_grise . "' WHERE uti_id=". $utilisateur . ";";
				try{
					$req = $Conn->query($sql);
				}Catch(Exception $e){
					$warning[]="La carte grise n'a pas été chargée.";
				}
				
			}else{
				$warning[]="La carte grise n'a pas été chargée.";
			}
		}
		// INSERTION DU MODèLE PAR DEFAUT
		$req = $Conn->prepare("INSERT INTO Modeles (mod_utilisateur, mod_type, mod_fct_1, mod_menu) VALUES (:mod_utilisateur, :mod_type, :mod_fct_1, :mod_menu)");
		$req->bindParam("mod_utilisateur", $utilisateur);
		$req->bindValue("mod_type", 1);
		$req->bindValue("mod_menu", 1);
		$req->bindValue("mod_fct_1", 1);
		$req->execute();
		// TRAITEMENT CREATION D'UN UTILISATEUR
		if(empty($_POST['uti_id'])){ 
		
			// AFFECTATION DES SOCIETES
			if($uti_profil!=1 AND $uti_profil!=3 AND $uti_profil!=4 AND $uti_profil!=5 AND $uti_profil!=15){
				
				//echo("FULL ACCESS");
				
				// par défaut accès à toutes les sociétes
				
				$soc_id=0;
				
				$r_soc=$Conn->query("SELECT soc_id,age_id FROM Societes LEFT OUTER JOIN Agences ON (Societes.soc_id=Agences.age_societe AND Societes.soc_agence=1 AND NOT Agences.age_archive=1)
				WHERE NOT soc_archive ORDER BY soc_id,age_id;");
				$d_societes=$r_soc->fetchAll();
				if(!empty($d_societes)){
					foreach($d_societes as $s){
						$req = $Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur,uso_societe,uso_agence)
								VALUES (:uso_utilisateur, :uso_societe, :uso_agence)");
						$age_id=0;
						if(!empty($s["age_id"])){
							$age_id=intval($s["age_id"]);
						}
						
						if(!empty($age_id)){
							
							// il y a une agence on affecte aussi la societe
							if($s["soc_id"]!=$soc_id){
								
								// pour affecter une seul fois la societe si plusieur agence
								$soc_id=$s["soc_id"];
								$zero=0;
								
								$req->bindParam(':uso_utilisateur', $utilisateur);
								$req->bindParam(':uso_societe', $s["soc_id"]);
								$req->bindParam(':uso_agence',$zero);
								try{
									$req->execute();	
								}Catch(Exception $e){
									
								}
							}
						}
		
						$req->bindParam(':uso_utilisateur', $utilisateur);
						$req->bindParam(':uso_societe', $s["soc_id"]);
						$req->bindParam(':uso_agence',$age_id);
						$req->execute();
						try{
							$req->execute();	
						}Catch(Exception $e){
							
						}
					}
				}

			}else{
				
				//echo("ACESS AGENCE");
				
				if(empty($_POST["uti_agence"])){
					$req = $Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur,uso_societe,uso_agence)
								VALUES (:uso_utilisateur, :uso_societe, :uso_agence)");
					// ON DONNE L'ACCES SUR LES AGENCES 
					$r_age=$Conn->query("SELECT age_id FROM Agences WHERE age_societe=" . $uti_societe . " AND age_archive = 0 
					ORDER BY age_id;");
					$r_age->execute();
					$d_agences=$r_age->fetchAll();
					if(!empty($d_agences)){
						foreach($d_agences as $a){
							
							$req->bindParam(':uso_utilisateur', $utilisateur);
							$req->bindParam(':uso_societe', $uti_societe);
							$req->bindParam(':uso_agence', $a["age_id"]);
							$req->execute();
							try{
								$req->execute();	
							}Catch(Exception $e){
								
							}
						}
					}
					$uso_agence = 0;
					// ON DONNE L'ACCES SUR LA SOCIETE
					$req = $Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur,uso_societe,uso_agence)
								VALUES (:uso_utilisateur, :uso_societe, :uso_agence)");
					$req->bindParam(':uso_utilisateur', $utilisateur);
					$req->bindParam(':uso_societe', $uti_societe);
					$req->bindParam(':uso_agence', $uso_agence);
					$req->execute();
					try{
						$req->execute();	
					}Catch(Exception $e){
						
					}
					
				}else{
					
					// acces à l'agence uniquement
					$req = $Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur,uso_societe,uso_agence)
								VALUES (:uso_utilisateur, :uso_societe, :uso_agence)");
					$req->bindParam(':uso_utilisateur', $utilisateur);
					$req->bindParam(':uso_societe', $uti_societe);
					$req->bindParam(':uso_agence', $uti_agence);
					$req->execute();
					try{
						$req->execute();	
					}Catch(Exception $e){
						
					}
				}
			}
			
			// DROIT PAR DEFAUT DU PROFIL
			$r_droits = $Conn->prepare("SELECT udr_droit,udr_reaffecte FROM Utilisateurs_Droits WHERE udr_profil=" . $uti_profil . " AND udr_utilisateur=0;");
			$r_droits->execute();
			$d_droits=$r_droits->fetchAll();
			if(!empty($d_droits)){
				foreach($d_droits as $d){
					$req = $Conn->prepare("INSERT INTO Utilisateurs_Droits (udr_droit,udr_profil,udr_utilisateur,udr_reaffecte)
						VALUES (:udr_droit,0,:udr_utilisateur,:udr_reaffecte)");
			
			
			
					
					$req->bindParam(':udr_droit', $d["udr_droit"]);
					$req->bindParam(':udr_utilisateur',$utilisateur);
					$req->bindParam(':udr_reaffecte',$d["udr_reaffecte"]);
					$req->execute();
					try{
						$req->execute();	
					}Catch(Exception $e){
						
					}
				}
			}
			
			// AFFECTATIONS DES DROITS ETECH
			
			$maj_etech=add_utilisateur_etech($utilisateur,$uti_profil, $uti_societe, $uti_agence);

		}else{ 
		
			// **** EDITION **** 
			
			// si le profil n'est pas le meme qu'avant
			if($d_utilisateur_old['uti_profil'] != $uti_profil){
				$r_droits = $Conn->prepare("DELETE FROM Utilisateurs_Droits WHERE udr_utilisateur=" . $_POST['uti_id']);
				$r_droits->execute();

				$r_droits = $Conn->prepare("SELECT udr_droit,udr_reaffecte FROM Utilisateurs_Droits WHERE udr_profil=" . $uti_profil . " AND udr_utilisateur=0;");
				$r_droits->execute();
				$d_droits=$r_droits->fetchAll();
				if(!empty($d_droits)){
					foreach($d_droits as $d){
						$req = $Conn->prepare("INSERT INTO Utilisateurs_Droits (udr_droit,udr_profil,udr_utilisateur,udr_reaffecte)
							VALUES (:udr_droit,0,:udr_utilisateur,:udr_reaffecte)");
				
				
				
						
						$req->bindParam(':udr_droit', $d["udr_droit"]);
						$req->bindParam(':udr_utilisateur',$utilisateur);
						$req->bindParam(':udr_reaffecte',$d["udr_reaffecte"]);
						$req->execute();
						try{
							$req->execute();	
						}Catch(Exception $e){
							
						}
					}
				}
				
			}
			// MAJ DES SOCIETES ET PROFILS ETECH
			if($d_utilisateur_old['uti_societe'] != $uti_societe OR $d_utilisateur_old['uti_agence'] != $uti_agence OR $d_utilisateur_old['uti_profil'] != $uti_profil){
				$maj_etech=set_utilisateur_etech($utilisateur,$uti_profil, $uti_societe, $uti_agence);
			}
			
			// EDITION SI LE societe ou agence A CHANGE
			if($d_utilisateur_old['uti_societe'] != $uti_societe OR $d_utilisateur_old['uti_agence'] != $uti_agence){
				$req=$Conn->query("DELETE FROM Utilisateurs_societes WHERE uso_utilisateur = ". $utilisateur);
				if(empty($_POST["uti_agence"])){

					$req = $Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur,uso_societe,uso_agence)
								VALUES (:uso_utilisateur, :uso_societe, :uso_agence)");
					// ON DONNE L'ACCES SUR LES AGENCES 
					$r_age=$Conn->query("SELECT age_id FROM Agences WHERE age_societe=" . $uti_societe . " AND age_archive = 0 
					ORDER BY age_id;");
					$r_age->execute();
					$d_agences=$r_age->fetchAll();
					if(!empty($d_agences)){
						foreach($d_agences as $a){
							
							$req->bindParam(':uso_utilisateur', $utilisateur);
							$req->bindParam(':uso_societe', $uti_societe);
							$req->bindParam(':uso_agence', $a["age_id"]);
							$req->execute();
							try{
								$req->execute();	
							}Catch(Exception $e){
								
							}
						}
					}
					$uso_agence = 0;
					// ON DONNE L'ACCES SUR LA SOCIETE
					$req = $Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur,uso_societe,uso_agence)
								VALUES (:uso_utilisateur, :uso_societe, :uso_agence)");
					$req->bindParam(':uso_utilisateur', $utilisateur);
					$req->bindParam(':uso_societe', $uti_societe);
					$req->bindParam(':uso_agence', $uso_agence);
					$req->execute();
					try{
						$req->execute();	
					}Catch(Exception $e){
						
					}
					
				}else{
					
					// acces à l'agence uniquement
					$req = $Conn->prepare("INSERT INTO Utilisateurs_Societes (uso_utilisateur,uso_societe,uso_agence)
								VALUES (:uso_utilisateur, :uso_societe, :uso_agence)");
					$req->bindParam(':uso_utilisateur', $utilisateur);
					$req->bindParam(':uso_societe', $uti_societe);
					$req->bindParam(':uso_agence', $uti_agence);
					$req->execute();
					try{
						$req->execute();	
					}Catch(Exception $e){
						
					}
				}
			}
		}
	}
	if(!empty($erreur)){
		
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur
		);
		if(!empty($_POST['uti_id'])){
			$adresse="utilisateur.php?utilisateur=" . $_POST['uti_id'];
		}else{
			$adresse="utilisateur.php";
		}
		
		
		echo("<pre>");
		print_r($_SESSION['message']);
		echo("</pre>");

	}else{
		
		if(!empty($warning)){
			foreach($warning as $w){
				$_SESSION['message'][] = array(
					"titre" => "Attention",
					"type" => "warning",
					"message" => $w
				);
			}
		}else{
			$_SESSION['message'][] = array(
				"titre" => "Succès",
				"type" => "success",
				"message" => "L'utilisateur a bien été actualisé"
			);
		}
		$adresse="utilisateur_voir.php?utilisateur=" . $utilisateur;
	
	
	}
	header("location : " . $adresse);
?>

