<?php
include "includes/controle_acces.inc.php";
include("includes/connexion.php");
include("includes/connexion_soc.php");
include("includes/connexion_fct.php");
include("modeles/mod_parametre.php");
// VARIABLES
$rel_etat_relance=0;
if(!empty($_POST['rel_etat_relance'])){
    $rel_etat_relance = $_POST['rel_etat_relance'];
}
$origine="";
if(!empty($_GET['origine'])){
	$origine=$_GET['origine'];
}
$relance=0;
if(!empty($_POST['relance'])){
    $relance = $_POST['relance'];
}

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
    if($_SESSION['acces']["acc_ref"]==1){
        $acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
    }
}
$sql="SELECT uti_nom, uti_prenom FROM Utilisateurs WHERE uti_id = " . $acc_utilisateur;
$req = $Conn->query($sql);
$utilisateur=$req->fetch();
// ALLER CHERCHER LA RELANCE

$opca=0;
$client=0;

$sql="SELECT rel_opca,rel_client FROM Relances WHERE rel_id = " . $relance;
$req = $Conn->query($sql);
$d_relance=$req->fetch();
if(!empty($d_relance)){
	$opca = $d_relance['rel_opca'];
	$client = $d_relance['rel_client'];
}

if($client==0){
	die("probleme");
}
// ETAPES RELANCES

$actu_fac=true;
$sql="SELECT ret_id,ret_traitement,ret_libelle FROM Relances_Etats ORDER BY ret_j_deb;";
$req = $Conn->query($sql);
$d_relances=$req->fetchAll();
$tab_relance = array();
$tab_relance[0] = $actu_fac;
foreach($d_relances as $d){
    if($d['ret_id'] == $rel_etat_relance){
        $actu_fac=false;
    }
    $tab_relance[$d['ret_id']] = $actu_fac;
}
if(empty($_POST['rel_date'])){
    $rel_date = date('Y-m-d');
}else{
    $rel_date = convert_date_sql($_POST['rel_date']);
}
if(empty($_POST['rel_date_rappel'])){
    $rel_date_rappel = date('Y-m-d H:i');
}else{
    $date = $_POST['rel_date_rappel'];
    $rel_date_rappel = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$date)));

}
// MAJ RELANCE

$sql="UPDATE Relances SET rel_utilisateur=:rel_utilisateur,rel_uti_identite=:rel_uti_identite,
rel_date=:rel_date, rel_titre=:rel_titre, rel_comment=:rel_comment, rel_date_rappel=:rel_date_rappel, rel_contact=:rel_contact,rel_con_nom=:rel_con_nom,rel_con_tel=:rel_con_tel, rel_con_prenom=:rel_con_prenom
 WHERE rel_id =" . $relance;
$req = $Conn->prepare($sql);
$req->bindValue(":rel_utilisateur",$acc_utilisateur);
$req->bindValue(":rel_uti_identite",$utilisateur['uti_prenom'] . " " . $utilisateur['uti_nom']);
$req->bindValue(":rel_date",$rel_date);
$req->bindValue(":rel_titre",$_POST['rel_titre']);
$req->bindValue(":rel_comment",$_POST['rel_comment']);
$req->bindValue(":rel_date_rappel",$rel_date_rappel);
$req->bindValue(":rel_contact",$_POST['rel_contact']);
$req->bindValue(":rel_con_nom",$_POST['rel_con_nom']);
$req->bindValue(":rel_con_prenom",$_POST['rel_con_prenom']);
$req->bindValue(":rel_con_tel",$_POST['rel_con_tel']);
$req->execute();


if(empty($d_relance['rel_courrier']) AND empty($d_relance['rel_envoie'])){
	for ($i=1; $i < $_POST['nbFac']; $i++) {

		$facture=0;
        if(!empty($_POST['fac_id_' . $i])){
            $facture = $_POST['fac_id_' . $i];
        }

        $facture_societe = $acc_societe;

        $facture_numero=0;
        if(!empty($_POST['fac_numero_' . $i])){
            $facture_numero = $_POST['fac_numero_' . $i];
        }

        if($facture>0){
        	//ON LIE LA FACTURE A LA RELANCE
        	if(!empty($_POST['ligne_fac_' . $i])){
        		$sql="SELECT * FROM Relances_Factures WHERE rfa_relance =" . $relance . " AND rfa_facture_soc = " . $_POST['fac_societe_' . $i] . " AND rfa_facture = " . $facture;
				$req = $Conn->query($sql);
				$d_relance_facture_exist=$req->fetch();
				if(empty($d_relance_facture_exist)){
					$sql="INSERT INTO Relances_Factures (rfa_facture, rfa_facture_soc, rfa_facture_num, rfa_relance, rfa_etat_relance)
					VALUES (:rfa_facture, :rfa_facture_soc, :rfa_facture_num, :rfa_relance, :rfa_etat_relance)";
					$req = $ConnSoc->prepare($sql);
					$req->bindValue(":rfa_facture",$facture);
					$req->bindValue(":rfa_facture_num",$facture_numero);
					$req->bindValue(":rfa_facture_soc",$_POST['fac_societe_' . $i]);
					$req->bindValue(":rfa_relance",$relance);
					$req->bindValue(":rfa_etat_relance",$rel_etat_relance);
					$req->execute();
				}else{
					$sql="UPDATE Relances_Factures SET  rfa_etat_relance = :rfa_etat_relance, rfa_facture_num=:rfa_facture_num
					 WHERE rfa_relance =" . $relance . " AND rfa_facture = " . $facture;
					$req = $ConnSoc->prepare($sql);
					$req->bindValue(":rfa_etat_relance",$rel_etat_relance);
					$req->bindValue(":rfa_facture_num",$facture_numero);
					$req->execute();
				}


        	}else{
        		//LA FACTURE N'EST PAS OU PLUS COCHE
        		$sql="DELETE FROM Relances_Factures WHERE rfa_facture_soc = " . $_POST['fac_societe_' . $i] . " AND rfa_relance = " . $relance . " AND rfa_facture=" . $facture;
				$req = $ConnSoc->query($sql);
        	}

        	// ON RECHERCHE LE NV STATUS
        	$sql="SELECT ret_id,rfa_date FROM Relances_Factures,Relances_Etats WHERE rfa_etat_relance=ret_id AND rfa_facture_soc=" . $_POST['fac_societe_' . $i] . " AND rfa_facture=" . $facture ." ORDER BY ret_j_deb DESC;";
			$req = $Conn->query($sql);
			$d_rel_fac=$req->fetch();
			if(!empty($d_rel_fac)){
				$fac_etat_relance=$d_rel_fac['ret_id'];
				$fac_date_relance=$d_rel_fac['rfa_date'];
			}

			$ConnFct = connexion_fct($_POST['fac_societe_' . $i]);
			$sql="UPDATE Factures SET  fac_etat_relance = :fac_etat_relance, fac_date_relance=:fac_date_relance
					 WHERE fac_id = " . $facture;
			$req = $ConnFct->prepare($sql);
			$req->bindValue(":fac_etat_relance",$fac_etat_relance);
			$req->bindValue(":fac_date_relance",$fac_date_relance);
			$req->execute();
        }
	}
}

// REDIRECTIONS
$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Votre relance est enregistrée"
    );
if($origine=="histo"){
    $adresse="facture_relance_histo.php?client=" . $client;
}else{
    $adresse="facture_relance_voir.php?client=" . $client;
}

header("location : " . $adresse);
die();
