<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_competence.php');
	include('modeles/mod_diplome.php');

	$erreur=0;
	
	$com_id=0;
	if(!empty($_POST["competence"])){
		$com_id=$_POST["competence"];
	};
	
	$com_libelle="";
	if(!empty($_POST["com_libelle"])){
		$com_libelle=$_POST["com_libelle"];
	}else{
		$erreur=1;
	};
	
	
	// ENREGISTREMENT D'UNE COMPETENCE
	
	if($erreur==0){

		if($com_id>0){
			
			// MISE A JOUR
			
			update_competence($com_id,$com_libelle);
				
		
		}else{

			$com_id=insert_competence($com_libelle);
			
			if($com_id==0){
				$erreur=1;
			};
		};	
		
		// GESTIONS DES DIPLOMES ASSOCIEES
		if($erreur==0){
			
			$diplomes=get_diplomes();
			
			if(!empty($diplomes)){
				
				foreach($diplomes as $value){
				
					$diplome_id=$value["dip_id"];
					
					
					$dco_obligatoire=0;
					if(isset($_POST["obligatoire_" . $diplome_id])){
						if($_POST["obligatoire_" . $diplome_id]=="on"){
							$dco_obligatoire=1;	
						}
					}
					$dco_interne=0;
					if(isset($_POST["interne_" . $diplome_id])){
						if($_POST["interne_" . $diplome_id]=="on"){
							$dco_interne=1;	
						}
					}

					$dco_externe=0;
					if(isset($_POST["externe_" . $diplome_id])){
						if($_POST["externe_" . $diplome_id]=="on"){
							
							$dco_externe=1;	
						}
					}
					
							
					if($dco_interne==1 OR $dco_externe==1){
						
						// on associe le diplome
						
						$lien=get_competence_diplome($com_id,$diplome_id);
						
						if(empty($lien)){
							insert_competence_diplome($com_id,$diplome_id,$dco_obligatoire,$dco_interne,$dco_externe);	
						}else{
							update_competence_diplome($com_id,$diplome_id,$dco_obligatoire,$dco_interne,$dco_externe);
						}
						
					}else{
						delete_competence_diplome($com_id,$diplome_id);
					}
				}
			}
		}
		
		// RECALCUL DES COMPETENCES DES INTERVENANTS EN FONCTION DE LA NOUVELLE AFFECTATION DIPLOMES
		
		// ON REGARDE SI DES UTILISATEURS N'AYANT PAS LA COMPETENCE PEUVENT L'OBTENIR SUITE CHANEG DIPLOME
		
		$sql_add="INSERT INTO Intervenants_Competences (ico_ref, ico_ref_id,ico_competence,ico_valide) VALUES (:ico_ref,:ico_ref_id,". $com_id . ",0);";
		$req_add=$Conn->prepare($sql_add);

		$sql="SELECT uti_id,uti_nom,uti_prenom,ico_ref_id FROM Utilisateurs LEFT OUTER JOIN Intervenants_Competences ON (Utilisateurs.uti_id=Intervenants_Competences.ico_ref_id AND ico_competence=" . $com_id . ") 
		WHERE ISNULL(ico_ref_id) ORDER BY uti_id;";
		$req=$Conn->query($sql);
		$d_utilisateurs=$req->fetchAll();
		if(!empty($d_utilisateurs)){
			foreach($d_utilisateurs as $d_uti){

				$sql="SELECT idi_diplome FROM Diplomes_Competences LEFT OUTER JOIN Intervenants_Diplomes 
				ON (dco_diplome=idi_diplome AND idi_ref=1 AND idi_ref_id=" . $d_uti["uti_id"] . " AND (ISNULL(idi_date_fin) OR idi_date_fin>NOW()) )"; 				
				$sql.=" WHERE dco_interne AND dco_obligatoire AND dco_competence=" . $com_id . " AND ISNULL(idi_diplome);";
				$req=$Conn->query($sql);
				$d_uti_add=$req->fetch();
				if(empty($d_uti_add)){	
					$req_add->bindValue("ico_ref",1);
					$req_add->bindParam("ico_ref_id",$d_uti["uti_id"]);
					$req_add->execute();
						
					
				}
			}
		}

		
		//CAS 1 on verifie que tous les intervenants qui avaient la competence sont toujours en mesure de la garder
		
		$sql_del="DELETE FROM Intervenants_Competences WHERE ico_ref=:ico_ref AND ico_ref_id=:ico_ref_id AND ico_competence=:ico_competence;";
		$req_del=$Conn->prepare($sql_del);
		
		$sql="SELECT DISTINCT ico_ref,ico_ref_id,dco_diplome,idi_diplome FROM Intervenants_Competences LEFT JOIN Diplomes_Competences ON (
		Intervenants_Competences.ico_competence=Diplomes_Competences.dco_competence AND Diplomes_Competences.dco_obligatoire AND (
		(Intervenants_Competences.ico_ref=2 AND Diplomes_Competences.dco_externe) OR (Intervenants_Competences.ico_ref=1 AND Diplomes_Competences.dco_interne)) 
		)
		LEFT OUTER JOIN Intervenants_Diplomes ON (
		Intervenants_Competences.ico_ref=Intervenants_Diplomes.idi_ref AND Intervenants_Competences.ico_ref_id=Intervenants_Diplomes.idi_ref_id 
		AND Diplomes_Competences.dco_diplome=Intervenants_Diplomes.idi_diplome AND ( ISNULL(Intervenants_Diplomes.idi_date_fin) OR Intervenants_Diplomes.idi_date_fin>NOW())
		)";
		$sql.=" WHERE ico_competence=" . $com_id . " AND NOT ico_valide AND NOT ISNULL(dco_diplome) AND ISNULL(idi_diplome) ORDER BY ico_ref,ico_ref_id";
		$req=$Conn->query($sql);
		$d_int_supp=$req->fetchAll();
		if(!empty($d_int_supp)){
			foreach($d_int_supp as $d_int_s){
				
				$req_del->bindParam("ico_ref",$d_int_s["ico_ref"]);
				$req_del->bindParam("ico_ref_id",$d_int_s["ico_ref_id"]);
				$req_del->bindParam("ico_competence",$com_id);
				$req_del->execute();
				
			}
		}
		
		
	};	
		
	header('Location: competence_liste.php?erreur=' . $erreur);      
	
?>

