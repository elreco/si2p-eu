<?php
include "includes/controle_acces.inc.php";

include_once("includes/connexion.php");;
include_once("includes/connexion_soc.php");
include_once("includes/connexion_fct.php");
include_once("modeles/mod_parametre.php");

// CONTROLE ACCES

$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
    if($_SESSION['acces']["acc_ref"]==1){
        $acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
    }
}

if($_SESSION['acces']['acc_service'][1] !=1 AND $_SESSION['acces']['acc_profil']!=14 AND $_SESSION['acces']['acc_service'][3] !=1 AND $_SESSION['acces']['acc_profil']!=11 AND $_SESSION['acces']['acc_profil']!=12 AND $_SESSION['acces']['acc_profil']!=15 AND ($_SESSION['acces']['acc_profil']!=3 OR !$_SESSION["acces"]["acc_droits"][35] ) ){
    $erreur_txt="Accès refusé!";
    // SERVICE COm (RE), DG, SERVICE TECH, DAF, RA, ADV (drt35)
}

$synthese_dg=false;
if($_SESSION['acces']['acc_service'][3] ==1 OR $_SESSION['acces']['acc_profil']==11 OR $_SESSION['acces']['acc_profil']==12 OR $_SESSION['acces']['acc_profil']==14 OR $acc_utilisateur==403){
    //les RE (hors Christine), RA et ADV ne voit pas le CA-1
    // service tech, DAF, RRH, DG et christine
    $synthese_dg=true;
}


if(!isset($erreur_txt)){

   


    $agence_type=0;
    if(isset($_GET["type"])){
        if(!empty($_GET["type"])){
            $agence_type=intval($_GET["type"]);
        }
    }
    if(empty($agence_type) AND !$synthese_dg){

        $erreur_txt="Paramètre absent. Impossible d'afficher cette page!";
    }
}

if(!isset($erreur_txt)){
    // exercice en cours
  
    if(date("n")<4){
        $exercice=date("Y")-1;
    }else{
        $exercice=date("Y");
    }
    

    
    // Titre
    if($agence_type==2){
        $titre="Tableau de pilotage INTER";
    }elseif($agence_type==1){
        $titre="Tableau de pilotage INTRA";  
    }else{
        $titre="Tableau de pilotage RESEAU";  
    }

    // société / agence de l'utilisateur.

    $sql="SELECT soc_id,soc_nom,age_id,age_nom,soc_agence,soc_intra_inter,age_intra_inter FROM Utilisateurs_Societes 
    INNER JOIN Societes ON (Utilisateurs_Societes.uso_societe=Societes.soc_id)
    LEFT JOIN Agences ON (Utilisateurs_Societes.uso_agence=Agences.age_id)
    WHERE uso_utilisateur=" . $acc_utilisateur . " AND NOT soc_archive AND NOT soc_id IN (10,14)
    AND ( 
            (
                soc_agence=0 AND uso_agence=0";
                // pas d'agence -> la soc doit correspondre au critère agence_type
                if($agence_type==2){
                    $sql.=" AND soc_intra_inter=2";
                }elseif($agence_type==1){
                    $sql.=" AND NOT soc_intra_inter=2";
                }
            $sql.=" ) OR (
                soc_agence=1 AND uso_agence>0";
                // agence -> l'agence doit correspondre au critère agence_type
                if($agence_type==2){
                    $sql.=" AND age_intra_inter=2";
                }elseif($agence_type==1){
                    $sql.=" AND NOT age_intra_inter=2";
                }
            $sql.=")
    )";

    if(empty($agence_type)){
        //finalement on compte CONTAMIN
       // $sql.=" AND NOT soc_id=18";
    }
    $sql.=" ORDER BY soc_nom,age_nom;";
    /*echo($sql);
    die();*/

    $req=$Conn->query($sql);
    $societes=$req->fetchAll(); 
    if(!empty($societes)){

        $bcl_gcs=false;
        $bcl_gfc=false;

        $data=array();

        if(count($societes)>1){

            // U a ccès à plusieurs structures, il va falloir sommer 
        // if($_SESSION["acces"]["acc_droits"][28]){

            // SI droit stat réseau -> on crée une clé total pour sommer l'ensemble des sociétés 
            $data["total"]=array(
                "cumul" => 1,   // permet de ne pas afficher les données dans la boucle -> le totam est en bas de page.
                "nom" => "Total",
                "objectif" => array(
                    "1" => 0,
                    "2" => 0,
                    "3" => 0,
                    "4" => 0,
                    "5" => 0,
                    "6" => 0,
                    "7" => 0,
                    "8" => 0,
                    "9" => 0,
                    "10" => 0,
                    "11" => 0,
                    "12" => 0,
                    "total" => 0
                ),
                "realise" => array(
                    "1" => 0,
                    "2" => 0,
                    "3" => 0,
                    "4" => 0,
                    "5" => 0,
                    "6" => 0,
                    "7" => 0,
                    "8" => 0,
                    "9" => 0,
                    "10" => 0,
                    "11" => 0,
                    "12" => 0,
                    "total" => 0
                ),
                "valide" => array(
                    "1" => 0,
                    "2" => 0,
                    "3" => 0,
                    "4" => 0,
                    "5" => 0,
                    "6" => 0,
                    "7" => 0,
                    "8" => 0,
                    "9" => 0,
                    "10" => 0,
                    "11" => 0,
                    "12" => 0,
                    "total" => 0
                ),
                "option" => array(
                    "1" => 0,
                    "2" => 0,
                    "3" => 0,
                    "4" => 0,
                    "5" => 0,
                    "6" => 0,
                    "7" => 0,
                    "8" => 0,
                    "9" => 0,
                    "10" => 0,
                    "11" => 0,
                    "12" => 0,
                    "total" => 0
                ),
                "ca" => array(
                    "1" => 0,
                    "2" => 0,
                    "3" => 0,
                    "4" => 0,
                    "5" => 0,
                    "6" => 0,
                    "7" => 0,
                    "8" => 0,
                    "9" => 0,
                    "10" => 0,
                    "11" => 0,
                    "12" => 0,
                    "total" => 0
                ),
                "ca_prec" => array(
                    "1" => 0,
                    "2" => 0,
                    "3" => 0,
                    "4" => 0,
                    "5" => 0,
                    "6" => 0,
                    "7" => 0,
                    "8" => 0,
                    "9" => 0,
                    "10" => 0,
                    "11" => 0,
                    "12" => 0,
                    "total" => 0
                )
            );
        }
        foreach($societes as $soc){

            $cle_tot="total";        // si cette variable est vide les données de cette boucle ne seront pas aditionné en bas de page
            $cle_tot_soc="";

            $cle=$soc["soc_id"] . "-" . $soc["age_id"];
            $titre_element=$soc["soc_nom"] . " " . $soc["age_nom"];

            if($soc["soc_agence"]){
                // la boucle concernent une agence -> on prévoit un total societe qui sera affiché s'il l'utilisateur à accès à au moins 2 agences.
                $cle_tot_soc=$soc["soc_id"];
            }
           

            // fusion des données GFC ANGERS ET GFC ROUEN

            if($soc["age_id"]==1 OR $soc["age_id"]==18){
                $cle=$soc["soc_id"] . "-1";     // on force un clé unique pour les deux agences
                $titre_element=$soc["soc_nom"] . " (hors Grands Comptes)"; // on personnalise le titre du tableau.
            }

            if(count($societes)<=1){
                // si U n'a accès qu'a une seule entité, il est inutile de prévoir un total
                $cle_tot="";  
            }

            if($synthese_dg){

                if($agence_type==2){

                    // cumul INTER -> on exclut GCS
                    if($soc["age_id"]==21){
                        $cle_tot="";
                    }else{
                        $bcl_gcs=true;
                    }

                }elseif($agence_type==1){

                    // cumul INTRA -> on exclut GFC
                    if($soc["soc_id"]==4){
                        $cle_tot="";
                    }else{
                        $bcl_gfc=true;
                    }
                    
                    
                }else{

                    $cle="reseau"; // toutes les données sur une clé unique
                    $titre_element="Réseau (avec GFC)";

                    // cumul reseau -> on exclut GFC
                    if($soc["soc_id"]==4){
                        $cle_tot="";
                    }
                }
            }

            if(empty($data[$cle])){

                // on initialise de tableau de base (le tableau total est déjà crée)
                // condition empty pour ne pas ecraser le tableau a chaque boucle quand cle=reseau

                $data[$cle]=array(
                    "cumul" => 0,
                    "nom" => $titre_element,
                    "societe_id" => $soc["soc_id"],
                    "objectif" => array(
                        "1" => 0,
                        "2" => 0,
                        "3" => 0,
                        "4" => 0,
                        "5" => 0,
                        "6" => 0,
                        "7" => 0,
                        "8" => 0,
                        "9" => 0,
                        "10" => 0,
                        "11" => 0,
                        "12" => 0,
                        "total" => 0
                    ),
                    "realise" => array(
                        "1" => 0,
                        "2" => 0,
                        "3" => 0,
                        "4" => 0,
                        "5" => 0,
                        "6" => 0,
                        "7" => 0,
                        "8" => 0,
                        "9" => 0,
                        "10" => 0,
                        "11" => 0,
                        "12" => 0,
                        "total" => 0
                    ),
                    "valide" => array(
                        "1" => 0,
                        "2" => 0,
                        "3" => 0,
                        "4" => 0,
                        "5" => 0,
                        "6" => 0,
                        "7" => 0,
                        "8" => 0,
                        "9" => 0,
                        "10" => 0,
                        "11" => 0,
                        "12" => 0,
                        "total" => 0
                    ),
                    "option" => array(
                        "1" => 0,
                        "2" => 0,
                        "3" => 0,
                        "4" => 0,
                        "5" => 0,
                        "6" => 0,
                        "7" => 0,
                        "8" => 0,
                        "9" => 0,
                        "10" => 0,
                        "11" => 0,
                        "12" => 0,
                        "total" => 0
                    ),
                    "ca" => array(
                        "1" => 0,
                        "2" => 0,
                        "3" => 0,
                        "4" => 0,
                        "5" => 0,
                        "6" => 0,
                        "7" => 0,
                        "8" => 0,
                        "9" => 0,
                        "10" => 0,
                        "11" => 0,
                        "12" => 0,
                        "total" => 0
                    ),
                    "ca_prec" => array(
                        "1" => 0,
                        "2" => 0,
                        "3" => 0,
                        "4" => 0,
                        "5" => 0,
                        "6" => 0,
                        "7" => 0,
                        "8" => 0,
                        "9" => 0,
                        "10" => 0,
                        "11" => 0,
                        "12" => 0,
                        "total" => 0
                    )
                );
            }

            // bcl sur une societe qui a au moins une agence -> on initialise le tableau pour sommer N agence de la societe
            if(!empty($cle_tot_soc)){
                
                if(empty($data[$cle_tot_soc])){

                    $data[$cle_tot_soc]=array(
                        "cumul" => 2,
                        "afficher" => false,    // s'il n'y a qu'une seule agence INTRA ou INTER sur la societe ça ne sert à rien de sommer
                        "nom" => $soc["soc_nom"],
                        "objectif" => array(
                            "1" => 0,
                            "2" => 0,
                            "3" => 0,
                            "4" => 0,
                            "5" => 0,
                            "6" => 0,
                            "7" => 0,
                            "8" => 0,
                            "9" => 0,
                            "10" => 0,
                            "11" => 0,
                            "12" => 0,
                            "total" => 0
                        ),
                        "realise" => array(
                            "1" => 0,
                            "2" => 0,
                            "3" => 0,
                            "4" => 0,
                            "5" => 0,
                            "6" => 0,
                            "7" => 0,
                            "8" => 0,
                            "9" => 0,
                            "10" => 0,
                            "11" => 0,
                            "12" => 0,
                            "total" => 0
                        ),
                        "valide" => array(
                            "1" => 0,
                            "2" => 0,
                            "3" => 0,
                            "4" => 0,
                            "5" => 0,
                            "6" => 0,
                            "7" => 0,
                            "8" => 0,
                            "9" => 0,
                            "10" => 0,
                            "11" => 0,
                            "12" => 0,
                            "total" => 0
                        ),
                        "option" => array(
                            "1" => 0,
                            "2" => 0,
                            "3" => 0,
                            "4" => 0,
                            "5" => 0,
                            "6" => 0,
                            "7" => 0,
                            "8" => 0,
                            "9" => 0,
                            "10" => 0,
                            "11" => 0,
                            "12" => 0,
                            "total" => 0
                        ),
                        "ca" => array(
                            "1" => 0,
                            "2" => 0,
                            "3" => 0,
                            "4" => 0,
                            "5" => 0,
                            "6" => 0,
                            "7" => 0,
                            "8" => 0,
                            "9" => 0,
                            "10" => 0,
                            "11" => 0,
                            "12" => 0,
                            "total" => 0
                        ),
                        "ca_prec" => array(
                            "1" => 0,
                            "2" => 0,
                            "3" => 0,
                            "4" => 0,
                            "5" => 0,
                            "6" => 0,
                            "7" => 0,
                            "8" => 0,
                            "9" => 0,
                            "10" => 0,
                            "11" => 0,
                            "12" => 0,
                            "total" => 0
                        )
                    );
                }else{
                    // le tableau somme societe existe déjà (donc U a accès a au moins 2 agence).
                    // on valide l'affichage du tableau somme societe
                    // Attention, il ne s'agit pas du total societe mais de la somme des agences auxquelles U à accès.
                    $data[$cle_tot_soc]["afficher"]=true;
                }
            }

            //echo("SOCIETE : " . $soc["soc_id"] . "<br/>");

            $ConnFct=connexion_fct($soc["soc_id"]);

            // LES OBJECTIFS

            $sql="SELECT SUM(cob_objectif_1) AS objectif_1,SUM(cob_objectif_2) AS objectif_2,SUM(cob_objectif_3) AS objectif_3,
            SUM(cob_objectif_4) AS objectif_4,SUM(cob_objectif_5) AS objectif_5,SUM(cob_objectif_6) AS objectif_6,
            SUM(cob_objectif_7) AS objectif_7,SUM(cob_objectif_8) AS objectif_8,SUM(cob_objectif_9) AS objectif_9,
            SUM(cob_objectif_10) AS objectif_10,SUM(cob_objectif_11) AS objectif_11,SUM(cob_objectif_12) AS objectif_12
            FROM Commerciaux_Objectifs";
            if(!empty($soc["age_id"])){
                $sql.=",Commerciaux WHERE com_id=cob_commercial AND com_agence=" . $soc["age_id"] . 
                " AND cob_exercice= " . $exercice;
            }else{
                $sql.=" WHERE cob_exercice= " . $exercice;
            }
            $sql.=";";
            $req=$ConnFct->query($sql);
            $d_objectifs=$req->fetchAll(); 
            if(!empty($d_objectifs)){
                foreach($d_objectifs as $obj){

                    for($bcl=1;$bcl<=12;$bcl++){
                        $data[$cle]["objectif"][$bcl]=$data[$cle]["objectif"][$bcl] + $obj["objectif_" . $bcl];
                        $data[$cle]["objectif"]["total"]=$data[$cle]["objectif"]["total"] + $obj["objectif_" . $bcl];   
                        
                        if(!empty($cle_tot)){
                            $data[$cle_tot]["objectif"][$bcl]=$data[$cle_tot]["objectif"][$bcl] + $obj["objectif_" . $bcl]; 
                            $data[$cle_tot]["objectif"]["total"]=$data[$cle_tot]["objectif"]["total"] + $obj["objectif_" . $bcl]; 
                        }

                        if(!empty($cle_tot_soc)){
                            $data[$cle_tot_soc]["objectif"][$bcl]=$data[$cle_tot_soc]["objectif"][$bcl] + $obj["objectif_" . $bcl]; 
                            $data[$cle_tot_soc]["objectif"]["total"]=$data[$cle_tot_soc]["objectif"]["total"] + $obj["objectif_" . $bcl]; 
                        }
                    }

                }

            }

            // LE REALISE N
            /* Note : nous sommes obligé de passé par Factures_Lignes pour pouvoir exclure les F3 F4 */

            $sql="SELECT SUM(fli_montant_ht) AS montant_ht,MONTH(fac_date) AS mois 
            FROM Factures_Lignes
	        LEFT JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
            WHERE fli_categorie<4 AND fac_date>='" . $exercice . "-04-01' AND fac_date<='" . intval($exercice+1) . "-03-31'";
            if(!empty($soc["age_id"])){
                $sql.=" AND fac_agence=" . $soc["age_id"];
            }
            $sql.=" GROUP BY MONTH(fac_date);";
            $req=$ConnFct->query($sql);
            $d_realises=$req->fetchAll();
            if(!empty($d_realises)){
                foreach($d_realises as $realise){

                    $mois=0;
                    if(!empty($realise["mois"])){
                        $mois=$realise["mois"];
                    }

                    $montant_ht=0;
                    if(!empty($realise["montant_ht"])){
                        $montant_ht=$realise["montant_ht"];
                    }

                    $data[$cle]["realise"][$mois]= $data[$cle]["realise"][$mois] + $montant_ht;
                    $data[$cle]["realise"]["total"]= $data[$cle]["realise"]["total"] + $montant_ht;

                    $data[$cle]["ca"][$mois]=$data[$cle]["ca"][$mois] + $montant_ht;
                    $data[$cle]["ca"]["total"]=$data[$cle]["ca"]["total"] + $montant_ht;

                    if(!empty($cle_tot)){
                        $data[$cle_tot]["realise"][$mois]=$data[$cle_tot]["realise"][$mois] + $montant_ht;
                        $data[$cle_tot]["realise"]["total"]=$data[$cle_tot]["realise"]["total"] + $montant_ht;

                        $data[$cle_tot]["ca"][$mois]=$data[$cle_tot]["ca"][$mois] + $montant_ht;
                        $data[$cle_tot]["ca"]["total"]=$data[$cle_tot]["ca"]["total"] + $montant_ht;
                    }

                    if(!empty($cle_tot_soc)){
                        $data[$cle_tot_soc]["realise"][$mois]=$data[$cle_tot_soc]["realise"][$mois] + $montant_ht;
                        $data[$cle_tot_soc]["realise"]["total"]=$data[$cle_tot_soc]["realise"]["total"] + $montant_ht;

                        $data[$cle_tot_soc]["ca"][$mois]=$data[$cle_tot_soc]["ca"][$mois] + $montant_ht;
                        $data[$cle_tot_soc]["ca"]["total"]=$data[$cle_tot_soc]["ca"]["total"] + $montant_ht;
                    }

                }
            }

            // LE REALISE N-1

            if($synthese_dg){

                $sql="SELECT SUM(fli_montant_ht) AS montant_ht,MONTH(fac_date) AS mois 
                FROM Factures_Lignes
                LEFT JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
                WHERE fli_categorie<4 AND fac_date>='" . intval($exercice-1) . "-04-01' AND fac_date<='" . $exercice . "-03-31'";
                if(!empty($soc["age_id"])){
                    $sql.=" AND fac_agence=" . $soc["age_id"];
                }
                $sql.=" GROUP BY MONTH(fac_date);";
                $req=$ConnFct->query($sql);
                $d_realises=$req->fetchAll();
                if(!empty($d_realises)){
                    foreach($d_realises as $realise){

                        $mois=0;
                        if(!empty($realise["mois"])){
                            $mois=$realise["mois"];
                        }

                        $montant_ht=0;
                        if(!empty($realise["montant_ht"])){
                            $montant_ht=$realise["montant_ht"];
                        }

                        $data[$cle]["ca_prec"][$mois]= $data[$cle]["ca_prec"][$mois] + $montant_ht;
                        $data[$cle]["ca_prec"]["total"]= $data[$cle]["ca_prec"]["total"] + $montant_ht;

                        if(!empty($cle_tot)){
                            $data[$cle_tot]["ca_prec"][$mois]=$data[$cle_tot]["ca_prec"][$mois] + $montant_ht;     
                            $data[$cle_tot]["ca_prec"]["total"]=$data[$cle_tot]["ca_prec"]["total"] + $montant_ht;                
                        }

                        if(!empty($cle_tot_soc)){
                            $data[$cle_tot_soc]["ca_prec"][$mois]=$data[$cle_tot_soc]["ca_prec"][$mois] + $montant_ht;     
                            $data[$cle_tot_soc]["ca_prec"]["total"]=$data[$cle_tot_soc]["ca_prec"]["total"] + $montant_ht;                
                        }
                    }
                }
            }


            // LE PREVI

            /* Note : meme si on le l'utilise pas, on conserve acl_id pour le distinct
            */
 
            $sql="SELECT DISTINCT acl_id,MONTH(pda_date) AS mois,acl_ca AS montant_ht,acl_facture_ht AS facture_ht,acl_confirme
            FROM Actions_Clients
            LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)

            INNER JOIN Actions_Clients_Dates ON (Actions_Clients_Dates.acd_action_client=acl_id)
            INNER JOIN plannings_dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)

            INNER JOIN (
                SELECT acd_action_client, MAX(pda_date) AS max_date FROM Actions_Clients_Dates,plannings_dates WHERE acd_date=pda_id GROUP BY acd_action_client) AS groupe
            ON Actions_Clients.acl_id = groupe.acd_action_client AND plannings_dates.pda_date = groupe.max_date


            WHERE pda_date>='" . $exercice . "-04-01' AND pda_date<='" . intval($exercice+1) . "-03-31'";
            $sql.=" AND act_pro_categorie<4 AND NOT act_archive AND NOT acl_archive AND NOT acl_non_fac AND NOT acl_ca_nc AND NOT acl_facture";
            if(!empty($soc["age_id"])){
                $sql.=" AND act_agence=" . $soc["age_id"];
            }
            $sql.=";";
            //$sql.=" GROUP BY act_pro_categorie,act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,MONTH(pda_date),acl_confirme;";
            $req=$ConnFct->query($sql);
            $d_realises=$req->fetchAll();
            if(!empty($d_realises)){
                /*echo("<pre>");
                    print_r($d_realises);
                echo("</pre>");
                die();*/
                foreach($d_realises as $realise){

                    
                    $mois=0;
                    if(!empty($realise["mois"])){
                        $mois=$realise["mois"];
                    }

                    $montant_ht=0;
                    if(!empty($realise["montant_ht"])){
                        $montant_ht=$realise["montant_ht"];
                    }

                    // on déduit ce qui est déjà facturé.
                    $facture_ht=0;
                    if(!empty($realise["facture_ht"])){
                        $facture_ht=$realise["facture_ht"];
                        $montant_ht=$montant_ht-$facture_ht;
                    }
                    

                    $cle_ventile="option";
                    if(!empty($realise["acl_confirme"])){
                        $cle_ventile="valide";
                    }

                    $data[$cle][$cle_ventile][$mois]=$data[$cle][$cle_ventile][$mois] + $montant_ht;

                    // on inclut le previ dans le CA du mois (en plus du CA facturé "realise")
                    $data[$cle]["ca"][$mois]=$data[$cle]["ca"][$mois] + $montant_ht;
                    $data[$cle]["ca"]["total"]=$data[$cle]["ca"]["total"] + $montant_ht;

                    if(!empty($cle_tot)){
                        $data[$cle_tot][$cle_ventile][$mois]=$data[$cle_tot][$cle_ventile][$mois] + $montant_ht;
                        $data[$cle_tot][$cle_ventile]["total"]=$data[$cle_tot][$cle_ventile]["total"] + $montant_ht;

                        $data[$cle_tot]["ca"][$mois]=$data[$cle_tot]["ca"][$mois] + $montant_ht; 
                        $data[$cle_tot]["ca"]["total"]=$data[$cle_tot]["ca"]["total"] + $montant_ht;                  
                    }

                    if(!empty($cle_tot_soc)){
                        $data[$cle_tot_soc][$cle_ventile][$mois]=$data[$cle_tot_soc][$cle_ventile][$mois] + $montant_ht;
                        $data[$cle_tot_soc][$cle_ventile]["total"]=$data[$cle_tot_soc][$cle_ventile]["total"] + $montant_ht;

                        $data[$cle_tot_soc]["ca"][$mois]=$data[$cle_tot_soc]["ca"][$mois] + $montant_ht; 
                        $data[$cle_tot_soc]["ca"]["total"]=$data[$cle_tot_soc]["ca"]["total"] + $montant_ht;                  
                    }

                }
            }

            // LE PREVI GC

            if($soc["age_id"]==4){

				$sql="SELECT soc_id FROM Societes WHERE NOT soc_id=4 AND NOT soc_archive ORDER BY soc_id;";
				$req=$Conn->query($sql);
				$d_societe_region=$req->fetchAll();
				if(!empty($d_societe_region)){

					foreach($d_societe_region as $region){

                        $ConnReg=connexion_fct($region["soc_id"]);
                        
                        /* Note : la jointure Clients permet un cohérence des valeurs avec le tableau des commerciaux.
                        Sans cette jointure et les critères associés, les actions encore CN pour des clients qui ne sont plus GC ressortiraient.
                        Or ces actions sont exclut du tab com etant donné que la collecte des données ce fait en partant de la liste des clients GC */


						$sql_gc="SELECT DISTINCT acl_id
						,MONTH(pda_date) AS mois
						,acl_ca_gfc AS montant_ht,acl_facture_gfc_ht AS facture_ht,acl_confirme
						FROM Actions_Clients
						LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)			
                        INNER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)

						INNER JOIN Actions_Clients_Dates ON (Actions_Clients_Dates.acd_action_client=Actions_Clients.acl_id)
						INNER JOIN plannings_dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)

						INNER JOIN (
							SELECT acd_action_client, MAX(pda_date) AS max_date FROM Actions_Clients_Dates,plannings_dates WHERE acd_date=pda_id GROUP BY acd_action_client) AS groupe
						ON Actions_Clients.acl_id = groupe.acd_action_client AND plannings_dates.pda_date = groupe.max_date


						WHERE pda_date>='" . $exercice . "-04-01' AND pda_date<='" . intval($exercice+1) . "-03-31'";
                        $sql_gc.=" AND act_pro_categorie<4 AND NOT act_archive AND NOT acl_archive AND NOT acl_ca_nc AND NOT acl_non_fac_gfc AND acl_facturation=1";

                        // pour une cohérance avec tab_om cf note
                        $sql_gc.=" AND cli_categorie=2 AND cli_sous_categorie IN (1,3)";
                        
                        $req_gc=$ConnReg->query($sql_gc);
						$d_realises_gc=$req_gc->fetchAll();
						if(!empty($d_realises_gc)){

							foreach($d_realises_gc as $realise){

                                $mois=0;
								if(!empty($realise["mois"])){
								    $mois=$realise["mois"];
								}


								$montant_ht=0;
								if(!empty($realise["montant_ht"])){
									$montant_ht=$realise["montant_ht"];
								}
								$facture_ht=0;
								if(!empty($realise["facture_ht"])){
									$facture_ht=$realise["facture_ht"];
									$montant_ht=$montant_ht-$facture_ht;
								}

								$cle_ventile="option";
								if(!empty($realise["acl_confirme"])){
									$cle_ventile="valide";
                                }
                                
                                $data[$cle][$cle_ventile][$mois]=$data[$cle][$cle_ventile][$mois] + $montant_ht;
                                $data[$cle][$cle_ventile]["total"]=$data[$cle][$cle_ventile]["total"] + $montant_ht;

                                // on inclut le previ dans le CA du mois (en plus du CA facturé "realise")
                                $data[$cle]["ca"][$mois]=$data[$cle]["ca"][$mois] + $montant_ht;
                                $data[$cle]["ca"]["total"]=$data[$cle]["ca"]["total"] + $montant_ht;

                                if(!empty($cle_tot)){
                                    $data[$cle_tot][$cle_ventile][$mois]=$data[$cle_tot][$cle_ventile][$mois] + $montant_ht;
                                    $data[$cle_tot][$cle_ventile]["total"]=$data[$cle_tot][$cle_ventile]["total"] + $montant_ht;
                                    
                                    $data[$cle_tot]["ca"][$mois]=$data[$cle_tot]["ca"][$mois] + $montant_ht;            
                                    $data[$cle_tot]["ca"]["total"]=$data[$cle_tot]["ca"]["total"] + $montant_ht;           
                                }

                                if(!empty($cle_tot_soc)){
                                    $data[$cle_tot_soc][$cle_ventile][$mois]=$data[$cle_tot_soc][$cle_ventile][$mois] + $montant_ht;
                                    $data[$cle_tot_soc][$cle_ventile]["total"]=$data[$cle_tot_soc][$cle_ventile]["total"] + $montant_ht;
                                    
                                    $data[$cle_tot_soc]["ca"][$mois]=$data[$cle_tot_soc]["ca"][$mois] + $montant_ht;            
                                    $data[$cle_tot_soc]["ca"]["total"]=$data[$cle_tot_soc]["ca"]["total"] + $montant_ht;           
                                }
							}
						}
					}
				}
		    }

           /* $data[$cle]["cumul_08"]=intval($data[$cle]["ca"]["4"]) + intval($data[$cle]["ca"]["5"]) + intval($data[$cle]["ca"]["6"]) + intval($data[$cle]["ca"]["7"]) + intval($data[$cle]["ca"]["8"]);
            $data[$cle]["cumul_08_prec"]=intval($data[$cle]["ca_prec"]["4"]) + intval($data[$cle]["ca_prec"]["5"]) + intval($data[$cle]["ca_prec"]["6"]) + intval($data[$cle]["ca_prec"]["7"]) + intval($data[$cle]["ca_prec"]["8"]);
           
            $data[$cle]["delta_t1"]=(intval($data[$cle]["ca"]["4"]) + intval($data[$cle]["ca"]["5"]) + intval($data[$cle]["ca"]["6"])) - (intval($data[$cle]["ca_prec"]["4"]) + intval($data[$cle]["ca_prec"]["5"]) + intval($data[$cle]["ca_prec"]["6"]));
            $data[$cle]["delta_t2"]=(intval($data[$cle]["ca"]["7"]) + intval($data[$cle]["ca"]["8"]) + intval($data[$cle]["ca"]["9"])) - (intval($data[$cle]["ca_prec"]["7"]) + intval($data[$cle]["ca_prec"]["8"]) + intval($data[$cle]["ca_prec"]["9"]));
          

            if(!empty($cle_tot)){
                $data[$cle_tot]["cumul_08"]=intval($data[$cle_tot]["ca"]["4"]) + intval($data[$cle_tot]["ca"]["5"]) + intval($data[$cle_tot]["ca"]["6"]) + intval($data[$cle_tot]["ca"]["7"]) + intval($data[$cle_tot]["ca"]["8"]);
                $data[$cle_tot]["cumul_08_prec"]=intval($data[$cle_tot]["ca_prec"]["4"]) + intval($data[$cle_tot]["ca_prec"]["5"]) + intval($data[$cle_tot]["ca_prec"]["6"]) + intval($data[$cle_tot]["ca_prec"]["7"]) + intval($data[$cle_tot]["ca_prec"]["8"]);
               
                $data[$cle_tot]["delta_t1"]=(intval($data[$cle_tot]["ca"]["4"]) + intval($data[$cle_tot]["ca"]["5"]) + intval($data[$cle_tot]["ca"]["6"])) - (intval($data[$cle_tot]["ca_prec"]["4"]) + intval($data[$cle_tot]["ca_prec"]["5"]) + intval($data[$cle_tot]["ca_prec"]["6"]));
                $data[$cle_tot]["delta_t2"]=(intval($data[$cle_tot]["ca"]["7"]) + intval($data[$cle_tot]["ca"]["8"]) + intval($data[$cle_tot]["ca"]["9"])) - (intval($data[$cle_tot]["ca_prec"]["7"]) + intval($data[$cle_tot]["ca_prec"]["8"]) + intval($data[$cle_tot]["ca_prec"]["9"]));         
            }*/
            // fin boucle societe

        }

        if($synthese_dg){

            if($agence_type==2){
                if($bcl_gcs){
                    $data["total"]["nom"]="TOTAL INTER (sans GCS)";
                }else{
                    $data["total"]["nom"]="TOTAL INTER";
                }
            }elseif($agence_type==1){
                if($bcl_gfc){
                    $data["total"]["nom"]="TOTAL INTRA (sans GFC)";
                }else{
                    $data["total"]["nom"]="TOTAL INTRA";
                }
            }else{
                $data["total"]["nom"]="Réseau (sans GFC)";
            }

        }

    }else{
        $erreur_txt="Aucune société ne correspond à votre demande.";
    }

    /*echo("<pre>");
        print_r($data);
    echo("</pre>");*/

}
//die();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#zone_print{
				display:none;
			}
			.objectif{
				color:#AA00AA;
			}
			.resultat{
				font-weight:bold;
			}
			.resultat a{
				color:#000;
			}
			.ligne-strip{
				background-color:#eee;
			}
			.taux-atteinte{
				color:blue;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 landscape;
				margin:5mm;
			}
			html{
				background-color:#fff!important
			}
			body{
				background-color:#fff!important
			}
			#zone_print{
				background-color:#fff!important;
				font-size:8pt;
			}
		</style>
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="zone_print" ></div>

		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">

		<?php		if(isset($erreur_txt)){ ?>
						<p class="alert alert-danger text-center" >
							<?=$erreur_txt?>
						</p>
		<?php		}else{ ?>

						<div class="row" >
							<div class="col-md-12" id="page_print" >

								<div class="panel" >
									<div class="panel-body br-n">

										<h1><?=$titre?></h1>
										<div class="table-responsive">
											<table class="table" >
                                    <?php       $soc_bcl=null;
                                                
                                                foreach($data as $d){ 
                                        
                                                    if($d["cumul"]==0){ 

                                                        if(!empty($soc_bcl) AND $soc_bcl!=$d["societe_id"]){

                                                            // changement de societe -> il faut peut-être afficher le total de la societe precédente.
                                                            // si modif ici -> penser a modifier aussi la derniere ligne en fin de boucle
                                                            if(!empty($data[$soc_bcl])){

                                                                if($data[$soc_bcl]["afficher"]==1){ ?>

                                                                    <tr class="dark2" >
                                                                        <th><?=$data[$soc_bcl]["nom"]?></th>
                                                                        <th>Avril</th>
                                                                        <th>Mai</th>
                                                                        <th>Juin</th>
                                                                        <th>Juillet</th>
                                                                        <th>Août</th>
                                                                        <th>Septembre</th>
                                                                        <th>Octobre</th>
                                                                        <th>Novembre</th>
                                                                        <th>Décembre</th>
                                                                        <th>Janvier</th>
                                                                        <th>Fevrier</th>
                                                                        <th>Mars</th>
                                                                        <th>CUMUL</th>	
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Objectif</td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["4"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["5"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["6"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["7"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["8"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["9"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["10"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["11"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["12"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["1"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["2"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["3"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["total"],0,","," ")?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>CA réalisé du mois (CA N)</td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["4"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["5"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["6"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["7"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["8"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["9"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["10"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["11"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["12"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["1"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["2"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["3"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["total"],0,","," ")?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>CA actions validées</td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["4"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["5"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["6"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["7"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["8"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["9"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["10"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["11"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["12"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["1"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["2"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["3"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["total"],0,","," ")?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>CA actions options</td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["4"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["5"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["6"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["7"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["8"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["9"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["10"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["11"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["12"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["1"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["2"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["3"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["total"],0,","," ")?></td>
                                                                    
                                                                    </tr>
                                                                    <tr>
                                                                        <td>CA (avec option et validé)</td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["4"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["5"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["6"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["7"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["8"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["9"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["10"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["11"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["12"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["1"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["2"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["3"],0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["total"],0,","," ")?></td>
                                                                        
                                                                    </tr>
                                                            <?php   if($synthese_dg){ ?> 
                                                                        <tr>
                                                                            <td>CA N-1</td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["4"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["5"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["6"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["7"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["8"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["9"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["10"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["11"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["12"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["1"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["2"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["3"],0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["total"],0,","," ")?></td>                                                                            
                                                                        </tr>       
                                                                        <tr>
                                                                            <td>Différence du CA du mois de l'année dernière (N)-(N-1)</td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["4"])-intval($d["ca_prec"]["4"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["5"])-intval($d["ca_prec"]["5"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["6"])-intval($d["ca_prec"]["6"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["7"])-intval($d["ca_prec"]["7"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["8"])-intval($d["ca_prec"]["8"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["9"])-intval($d["ca_prec"]["9"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["10"])-intval($d["ca_prec"]["10"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["11"])-intval($d["ca_prec"]["11"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["12"])-intval($d["ca_prec"]["12"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["1"])-intval($d["ca_prec"]["1"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["2"])-intval($d["ca_prec"]["2"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["3"])-intval($d["ca_prec"]["3"]),0,","," ")?></td>
                                                                            <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["total"])-intval($d["ca_prec"]["total"]),0,","," ")?></td>                                                                           
                                                                        </tr>
                                                            <?php   } ?>
                                                                    <tr>
                                                                        <td>Ecart objectif</td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["4"])-intval($d["objectif"]["4"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["5"])-intval($d["objectif"]["5"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["6"])-intval($d["objectif"]["6"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["7"])-intval($d["objectif"]["7"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["8"])-intval($d["objectif"]["8"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["9"])-intval($d["objectif"]["9"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["10"])-intval($d["objectif"]["10"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["11"])-intval($d["objectif"]["11"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["12"])-intval($d["objectif"]["12"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["1"])-intval($d["objectif"]["1"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["2"])-intval($d["objectif"]["2"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["3"])-intval($d["objectif"]["3"]),0,","," ")?></td>
                                                                        <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["total"])-intval($d["objectif"]["total"]),0,","," ")?></td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Dépassement objectif</td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["4"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["4"])/intval($data[$soc_bcl]["objectif"]["4"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["5"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["5"])/intval($data[$soc_bcl]["objectif"]["5"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["6"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["6"])/intval($data[$soc_bcl]["objectif"]["6"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["7"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["7"])/intval($data[$soc_bcl]["objectif"]["7"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["8"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["8"])/intval($data[$soc_bcl]["objectif"]["8"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["9"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["9"])/intval($data[$soc_bcl]["objectif"]["9"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["10"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["10"])/intval($data[$soc_bcl]["objectif"]["10"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["11"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["11"])/intval($data[$soc_bcl]["objectif"]["11"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["12"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["12"])/intval($data[$soc_bcl]["objectif"]["12"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["1"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["1"])/intval($data[$soc_bcl]["objectif"]["1"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["2"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["2"])/intval($data[$soc_bcl]["objectif"]["2"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["3"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["3"])/intval($data[$soc_bcl]["objectif"]["3"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                        <td class="text-right" >
                                                                <?php       if(!empty($data[$soc_bcl]["objectif"]["total"])){
                                                                                echo(number_format((intval($data[$soc_bcl]["realise"]["total"])/intval($data[$soc_bcl]["objectif"]["total"]))*100-100,2,","," "));
                                                                            } ?> %
                                                                        </td>
                                                                    </tr>

                                    <?php                       }
                                                            }


                                                        }

                                                        $soc_bcl=$d["societe_id"];

                                                        /* les clés "cumul" sont créer en premier pour etre alimenté en parcourant les données 
                                                        mais elles doivent être affichées en dernier" */
                                                        ?>									
                                                        <tr class="dark2" >
                                                            <th><?=$d["nom"]?></th>
                                                            <th>Avril</th>
                                                            <th>Mai</th>
                                                            <th>Juin</th>
                                                            <th>Juillet</th>
                                                            <th>Août</th>
                                                            <th>Septembre</th>
                                                            <th>Octobre</th>
                                                            <th>Novembre</th>
                                                            <th>Décembre</th>
                                                            <th>Janvier</th>
                                                            <th>Fevrier</th>
                                                            <th>Mars</th>
                                                            <th>CUMUL</th>	
                                                        </tr>
                                                        <tr>
                                                            <td>Objectif</td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["4"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["5"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["6"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["7"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["8"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["9"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["10"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["11"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["12"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["1"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["2"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["3"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["total"],0,","," ")?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>CA réalisé du mois (CA N)</td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["4"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["5"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["6"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["7"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["8"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["9"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["10"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["11"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["12"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["1"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["2"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["3"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["total"],0,","," ")?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>CA actions validées</td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["4"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["5"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["6"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["7"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["8"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["9"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["10"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["11"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["12"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["1"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["2"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["3"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["total"],0,","," ")?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>CA actions options</td>
                                                            <td class="text-right" ><?=number_format($d["option"]["4"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["5"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["6"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["7"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["8"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["9"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["10"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["11"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["12"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["1"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["2"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["3"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["total"],0,","," ")?></td>
                                                           
                                                        </tr>
                                                        <tr>
                                                            <td>CA (avec option et validé)</td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["4"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["5"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["6"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["7"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["8"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["9"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["10"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["11"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["12"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["1"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["2"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["3"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["total"],0,","," ")?></td>
                                                            
                                                        </tr>
                                             <?php      if($synthese_dg){ ?> 
                                                            <tr>
                                                                <td>CA N-1</td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["4"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["5"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["6"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["7"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["8"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["9"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["10"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["11"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["12"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["1"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["2"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["3"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($d["ca_prec"]["total"],0,","," ")?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Différence du CA du mois de l'année dernière (N)-(N-1)</td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["4"])-intval($d["ca_prec"]["4"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["5"])-intval($d["ca_prec"]["5"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["6"])-intval($d["ca_prec"]["6"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["7"])-intval($d["ca_prec"]["7"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["8"])-intval($d["ca_prec"]["8"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["9"])-intval($d["ca_prec"]["9"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["10"])-intval($d["ca_prec"]["10"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["11"])-intval($d["ca_prec"]["11"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["12"])-intval($d["ca_prec"]["12"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["1"])-intval($d["ca_prec"]["1"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["2"])-intval($d["ca_prec"]["2"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["3"])-intval($d["ca_prec"]["3"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["total"])-intval($d["ca_prec"]["total"]),0,","," ")?></td>                                                 
                                                            </tr>
                                             <?php      } ?>
                                                        
                                                        <tr>
                                                            <td>Ecart objectif</td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["4"])-intval($d["objectif"]["4"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["5"])-intval($d["objectif"]["5"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["6"])-intval($d["objectif"]["6"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["7"])-intval($d["objectif"]["7"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["8"])-intval($d["objectif"]["8"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["9"])-intval($d["objectif"]["9"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["10"])-intval($d["objectif"]["10"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["11"])-intval($d["objectif"]["11"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["12"])-intval($d["objectif"]["12"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["1"])-intval($d["objectif"]["1"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["2"])-intval($d["objectif"]["2"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["3"])-intval($d["objectif"]["3"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["total"])-intval($d["objectif"]["total"]),0,","," ")?></td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td>Dépassement objectif</td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["4"])){
                                                                    echo(number_format((intval($d["realise"]["4"])/intval($d["objectif"]["4"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["5"])){
                                                                    echo(number_format((intval($d["realise"]["5"])/intval($d["objectif"]["5"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["6"])){
                                                                    echo(number_format((intval($d["realise"]["6"])/intval($d["objectif"]["6"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["7"])){
                                                                    echo(number_format((intval($d["realise"]["7"])/intval($d["objectif"]["7"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["8"])){
                                                                    echo(number_format((intval($d["realise"]["8"])/intval($d["objectif"]["8"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["9"])){
                                                                    echo(number_format((intval($d["realise"]["9"])/intval($d["objectif"]["9"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["10"])){
                                                                    echo(number_format((intval($d["realise"]["10"])/intval($d["objectif"]["10"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["11"])){
                                                                    echo(number_format((intval($d["realise"]["11"])/intval($d["objectif"]["11"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["12"])){
                                                                    echo(number_format((intval($d["realise"]["12"])/intval($d["objectif"]["12"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["1"])){
                                                                    echo(number_format((intval($d["realise"]["1"])/intval($d["objectif"]["1"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["2"])){
                                                                    echo(number_format((intval($d["realise"]["2"])/intval($d["objectif"]["2"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["3"])){
                                                                    echo(number_format((intval($d["realise"]["3"])/intval($d["objectif"]["3"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["total"])){
                                                                    echo(number_format((intval($d["realise"]["total"])/intval($d["objectif"]["total"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                        </tr>
                                    <?php           }
                                                } 

                                                // AFFICHAGE DU TOTAL SOCIETE SI LA BOUCLE SE TERMINE SUR UNE SOCIETE AVEC AGENCE.

                                                if(!empty($soc_bcl)){
                                                    if(!empty($data[$soc_bcl])){

                                                        if($data[$soc_bcl]["afficher"]==1){ ?>

                                                            <tr class="dark2" >
                                                                <th><?=$data[$soc_bcl]["nom"]?></th>
                                                                <th>Avril</th>
                                                                <th>Mai</th>
                                                                <th>Juin</th>
                                                                <th>Juillet</th>
                                                                <th>Août</th>
                                                                <th>Septembre</th>
                                                                <th>Octobre</th>
                                                                <th>Novembre</th>
                                                                <th>Décembre</th>
                                                                <th>Janvier</th>
                                                                <th>Fevrier</th>
                                                                <th>Mars</th>
                                                                <th>CUMUL</th>	
                                                            </tr>
                                                            <tr>
                                                                <td>Objectif</td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["4"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["5"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["6"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["7"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["8"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["9"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["10"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["11"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["12"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["1"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["2"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["3"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["objectif"]["total"],0,","," ")?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>CA réalisé du mois (N)</td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["4"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["5"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["6"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["7"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["8"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["9"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["10"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["11"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["12"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["1"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["2"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["3"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["realise"]["total"],0,","," ")?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>CA actions validées</td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["4"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["5"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["6"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["7"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["8"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["9"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["10"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["11"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["12"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["1"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["2"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["3"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["valide"]["total"],0,","," ")?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>CA actions options</td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["4"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["5"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["6"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["7"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["8"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["9"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["10"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["11"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["12"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["1"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["2"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["3"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["option"]["total"],0,","," ")?></td>
                                                            
                                                            </tr>
                                                            <tr>
                                                                <td>CA (avec option et validé)</td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["4"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["5"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["6"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["7"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["8"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["9"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["10"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["11"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["12"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["1"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["2"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["3"],0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format($data[$soc_bcl]["ca"]["total"],0,","," ")?></td>
                                                                
                                                            </tr>
                                                    <?php   if($synthese_dg){ ?> 
                                                                <tr>
                                                                    <td>CA N-1</td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["4"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["5"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["6"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["7"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["8"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["9"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["10"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["11"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["12"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["1"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["2"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["3"],0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format($data[$soc_bcl]["ca_prec"]["total"],0,","," ")?></td>                                                                            
                                                                </tr>       
                                                                <tr>
                                                                    <td>Différence du CA du mois de l'année dernière (N)-(N-1)</td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["4"])-intval($d["ca_prec"]["4"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["5"])-intval($d["ca_prec"]["5"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["6"])-intval($d["ca_prec"]["6"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["7"])-intval($d["ca_prec"]["7"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["8"])-intval($d["ca_prec"]["8"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["9"])-intval($d["ca_prec"]["9"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["10"])-intval($d["ca_prec"]["10"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["11"])-intval($d["ca_prec"]["11"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["12"])-intval($d["ca_prec"]["12"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["1"])-intval($d["ca_prec"]["1"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["2"])-intval($d["ca_prec"]["2"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["3"])-intval($d["ca_prec"]["3"]),0,","," ")?></td>
                                                                    <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["total"])-intval($d["ca_prec"]["total"]),0,","," ")?></td>                                                                           
                                                                </tr>
                                                    <?php   } ?>
                                                            <tr>
                                                                <td>Ecart objectif</td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["4"])-intval($d["objectif"]["4"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["5"])-intval($d["objectif"]["5"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["6"])-intval($d["objectif"]["6"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["7"])-intval($d["objectif"]["7"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["8"])-intval($d["objectif"]["8"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["9"])-intval($d["objectif"]["9"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["10"])-intval($d["objectif"]["10"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["11"])-intval($d["objectif"]["11"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["12"])-intval($d["objectif"]["12"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["1"])-intval($d["objectif"]["1"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["2"])-intval($d["objectif"]["2"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["3"])-intval($d["objectif"]["3"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($data[$soc_bcl]["realise"]["total"])-intval($d["objectif"]["total"]),0,","," ")?></td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>Dépassement objectif</td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["4"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["4"])/intval($data[$soc_bcl]["objectif"]["4"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["5"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["5"])/intval($data[$soc_bcl]["objectif"]["5"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["6"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["6"])/intval($data[$soc_bcl]["objectif"]["6"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["7"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["7"])/intval($data[$soc_bcl]["objectif"]["7"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["8"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["8"])/intval($data[$soc_bcl]["objectif"]["8"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["9"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["9"])/intval($data[$soc_bcl]["objectif"]["9"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["10"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["10"])/intval($data[$soc_bcl]["objectif"]["10"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["11"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["11"])/intval($data[$soc_bcl]["objectif"]["11"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["12"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["12"])/intval($data[$soc_bcl]["objectif"]["12"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["1"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["1"])/intval($data[$soc_bcl]["objectif"]["1"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["2"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["2"])/intval($data[$soc_bcl]["objectif"]["2"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["3"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["3"])/intval($data[$soc_bcl]["objectif"]["3"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                                <td class="text-right" >
                                                        <?php       if(!empty($data[$soc_bcl]["objectif"]["total"])){
                                                                        echo(number_format((intval($data[$soc_bcl]["realise"]["total"])/intval($data[$soc_bcl]["objectif"]["total"]))*100-100,2,","," "));
                                                                    } ?> %
                                                                </td>
                                                            </tr>

                            <?php                       }
                                                    }
                                                }


                                                // AFFICHAGE DES CUMULS
                                                
                                                foreach($data as $d){ 
                                        
                                                    if($d["cumul"]==1){ 
                                                        /* les clés "cumul" sont créer en premier pour etre alimenté en parcourant les données 
                                                        mais elles doivent être affichées en dernier" */
                                                        ?>									
                                                        <tr class="dark2" >
                                                            <th><?=$d["nom"]?></th>
                                                            <th>Avril</th>
                                                            <th>Mai</th>
                                                            <th>Juin</th>
                                                            <th>Juillet</th>
                                                            <th>Août</th>
                                                            <th>Septembre</th>
                                                            <th>Octobre</th>
                                                            <th>Novembre</th>
                                                            <th>Décembre</th>
                                                            <th>Janvier</th>
                                                            <th>Fevrier</th>
                                                            <th>Mars</th>
                                                            <th>CUMUL</th>	
                                                        </tr>
                                                        <tr>
                                                            <td>Objectif</td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["4"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["5"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["6"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["7"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["8"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["9"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["10"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["11"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["12"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["1"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["2"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["3"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["objectif"]["total"],0,","," ")?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>CA réalisé du mois (CA N)</td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["4"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["5"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["6"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["7"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["8"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["9"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["10"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["11"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["12"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["1"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["2"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["3"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["realise"]["total"],0,","," ")?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>CA actions validées</td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["4"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["5"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["6"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["7"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["8"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["9"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["10"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["11"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["12"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["1"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["2"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["3"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["valide"]["total"],0,","," ")?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>CA actions options</td>
                                                            <td class="text-right" ><?=number_format($d["option"]["4"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["5"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["6"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["7"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["8"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["9"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["10"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["11"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["12"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["1"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["2"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["3"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["option"]["total"],0,","," ")?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>CA (avec option et validé)</td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["4"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["5"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["6"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["7"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["8"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["9"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["10"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["11"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["12"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["1"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["2"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["3"],0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format($d["ca"]["total"],0,","," ")?></td>
                                                        </tr>

                                                <?php   if($synthese_dg){ ?> 
                                                            <tr>
                                                                <td>CA N-1</td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["4"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["5"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["6"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["7"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["8"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["9"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["10"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["11"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["12"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["1"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["2"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["3"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["ca_prec"]["total"]),0,","," ")?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Différence du CA du mois de l'année dernière (N)-(N-1)</td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["4"])-intval($d["ca_prec"]["4"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["5"])-intval($d["ca_prec"]["5"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["6"])-intval($d["ca_prec"]["6"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["7"])-intval($d["ca_prec"]["7"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["8"])-intval($d["ca_prec"]["8"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["9"])-intval($d["ca_prec"]["9"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["10"])-intval($d["ca_prec"]["10"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["11"])-intval($d["ca_prec"]["11"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["12"])-intval($d["ca_prec"]["12"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["1"])-intval($d["ca_prec"]["1"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["2"])-intval($d["ca_prec"]["2"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["3"])-intval($d["ca_prec"]["3"]),0,","," ")?></td>
                                                                <td class="text-right" ><?=number_format(intval($d["realise"]["total"])-intval($d["ca_prec"]["total"]),0,","," ")?></td>
                                                            </tr>
                                                <?php   } ?>
                                                        
                                                        <tr>
                                                            <td>Ecart objectif</td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["4"])-intval($d["objectif"]["4"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["5"])-intval($d["objectif"]["5"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["6"])-intval($d["objectif"]["6"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["7"])-intval($d["objectif"]["7"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["8"])-intval($d["objectif"]["8"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["9"])-intval($d["objectif"]["9"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["10"])-intval($d["objectif"]["10"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["11"])-intval($d["objectif"]["11"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["12"])-intval($d["objectif"]["12"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["1"])-intval($d["objectif"]["1"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["2"])-intval($d["objectif"]["2"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["3"])-intval($d["objectif"]["3"]),0,","," ")?></td>
                                                            <td class="text-right" ><?=number_format(intval($d["realise"]["total"])-intval($d["objectif"]["total"]),0,","," ")?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Dépassement objectif</td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["4"])){
                                                                    echo(number_format((intval($d["realise"]["4"])/intval($d["objectif"]["4"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["5"])){
                                                                    echo(number_format((intval($d["realise"]["5"])/intval($d["objectif"]["5"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["6"])){
                                                                    echo(number_format((intval($d["realise"]["6"])/intval($d["objectif"]["6"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["7"])){
                                                                    echo(number_format((intval($d["realise"]["7"])/intval($d["objectif"]["7"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["8"])){
                                                                    echo(number_format((intval($d["realise"]["8"])/intval($d["objectif"]["8"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["9"])){
                                                                    echo(number_format((intval($d["realise"]["9"])/intval($d["objectif"]["9"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["10"])){
                                                                    echo(number_format((intval($d["realise"]["10"])/intval($d["objectif"]["10"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["11"])){
                                                                    echo(number_format((intval($d["realise"]["11"])/intval($d["objectif"]["11"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["12"])){
                                                                    echo(number_format((intval($d["realise"]["12"])/intval($d["objectif"]["12"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["1"])){
                                                                    echo(number_format((intval($d["realise"]["1"])/intval($d["objectif"]["1"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["2"])){
                                                                    echo(number_format((intval($d["realise"]["2"])/intval($d["objectif"]["2"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["3"])){
                                                                    echo(number_format((intval($d["realise"]["3"])/intval($d["objectif"]["3"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                            <td class="text-right" >
                                                    <?php       if(!empty($d["objectif"]["total"])){
                                                                    echo(number_format((intval($d["realise"]["total"])/intval($d["objectif"]["total"]))*100-100,2,","," "));
                                                                } ?> %
                                                            </td>
                                                        </tr>
                                    <?php           }
                                                } ?>


                                            </table>                               
										</div>
									</div>
								</div>
							</div>
						</div>

			<?php	} ?>

				</section>

			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="stat_commercial.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-left"></span>
						Retour
					</a>
			<?php	if(!isset($erreur_txt)){ ?>
						<!--<button type="button" class="btn btn-sm btn-info ml15" id="print" >
							<i class="fa fa-print"></i> Imprimer
						</button>-->
			<?php	} ?>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
            <?php   if(empty($agence_type)){ ?>
                     <!--   <p class="text-warning" >
                            CONTAMIN n'est pas comptabilisé dans les résultats ci-dessous.
                        <p>-->
            <?php   } ?>
                </div>
				<div class="col-xs-3 footer-right" ></div>
			</div>
		</footer>


<?php
		include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function (){
				

				$("#print").click(function(){

					$("#zone_print").html($("#page_print").html());

					$("#main").hide();
					$("#content-footer").hide();
					$("#zone_print").show();

					window.print();

					$("#zone_print").hide();
					$("#main").show();
					$("#content-footer").show();

					$("#main").show();
					$("#content-footer").show();

				});
			});
		</script>

	</body>
</html>
