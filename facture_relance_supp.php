<?php
include "includes/controle_acces.inc.php";
include("includes/connexion.php");
include("includes/connexion_soc.php");
include("includes/connexion_fct.php");

$relance=0;
if(!empty($_POST['relance'])){
    $relance = $_POST['relance'];
}

$origine="";
if(!empty($_GET['origine'])){
	$origine=$_GET['origine'];
}

$sql="SELECT ret_id,ret_traitement,ret_libelle FROM Relances_Etats WHERE rfa_relance=" . $relance;
$req = $ConnSoc->query($sql);
$d_relances_etats=$req->fetchAll();

foreach ($d_relances_etats as $dre) {
	$facture = $dre['rfa_facture'];
	$fac_etat_relance=0;

	$sql="SELECT DISTINCT ret_id,ret_j_deb FROM Relances_Factures,Relances_Etats WHERE rfa_etat_relance=ret_id";
	$sql=$sql . " AND rfa_facture=" . $facture;
	$sql=$sql . " AND rfa_relance!=" . $relance . " AND ret_j_deb>0";
	$sql=$sql . "  ORDER BY ret_j_deb DESC;";
	$req = $ConnSoc->query($sql);
	$Relances_Factures=$req->fetch();

	if(!empty($Relances_Factures)){
		$fac_etat_relance = $Relances_Factures['ret_id'];
	}

	$sql="UPDATE Factures SET fac_etat_relance = ". $fac_etat_relance." WHERE fac_id = " . $facture;
	$req = $ConnSoc->query($sql);

	$sql="SELECT rel_client FROM Relances WHERE rel_id = " . $relance;
	$req = $ConnSoc->query($sql);
	$rel_client=$req->fetch();

	$sql="DELETE FROM Relances WHERE rel_id = " . $relance;
	$req = $ConnSoc->query($sql);

	$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Relance supprimée"
    );
	if($origine=="histo"){
        $adresse="facture_relance_histo.php?client=" . $client;
    }else{
        $adresse="facture_relance_voir.php?client=" . $client;
    }

    header("location : " . $adresse);
	die();
}
