<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");

include "modeles/mod_parametre.php";
include "modeles/mod_droit.php";
include "modeles/mod_erreur.php";

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');


// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];	
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];	
}
$acc_drt_base_client=false;
if($_SESSION['acces']['acc_ref']==1){
	if(!empty(get_droit_utilisateur(6,$_SESSION['acces']['acc_ref_id']))){
		$acc_drt_base_client=true;
	}
}

/*------------------------------
 GESTION DE L'AFFICHAGE
------------------------------*/

$mois=0;
$annee=0;
if(!isset($_SESSION["pla_affichage"])){
	$_SESSION["pla_affichage"]=0;
}

// L'UTILISATEUR A FILTRE L'AFFICHAGE
if(isset($_POST["filt_aff"])){

	if(!empty($_POST["periode_mois"])){
		// l'utilisateur affiche un mois entier
		
		$tab_mois=explode("/",$_POST["periode_mois"]);
		$mois=$tab_mois[0];
		$annee=$tab_mois[1];		
		
		$jour_deb=date("N", mktime(0, 0, 0, $mois, 1, $annee));	
		$deb_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1), $annee);
		$date_deb=date_create(date("Y-m-d",$deb_mk));						

		$jour_fin=date("N", mktime(0, 0, 0, $mois+1, 0, $annee));	
		$fin_mk=mktime(0, 0, 0, $mois+1, 5-$jour_fin, $annee);
		$date_fin=date_create(date("Y-m-d",$fin_mk));
		
		$_SESSION["pla_affichage"]=0;
		$_SESSION["pla_mois"]=$mois;
		$_SESSION["pla_annee"]=$annee;
		$_SESSION["pla_periode_deb"]=$date_deb->format("d/m/Y");
		$_SESSION["pla_periode_fin"]=$date_fin->format("d/m/Y");
		
		
	}elseif(!empty($_POST["periode_deb"])){
		
		// l'utilisateur affiche une periode perso

		$c_date_deb=convert_date_sql($_POST["periode_deb"]);
		$date_deb = new DateTime($c_date_deb);

		$c_date_fin=convert_date_sql($_POST["periode_fin"]);
		$date_fin = new DateTime($c_date_fin);
		
		$_SESSION["pla_affichage"]=1;
		$_SESSION["pla_mois"]=0;
		$_SESSION["pla_annee"]=0;
		$_SESSION["pla_periode_deb"]=$_POST["periode_deb"];
		$_SESSION["pla_periode_fin"]=$_POST["periode_fin"];
		
		/*echo("POST periode_deb : <br/>");
		echo($_POST["periode_deb"]);
		echo("<br/>");
		echo("POST periode_fin : <br/>");
		echo($_POST["periode_fin"]);
		echo("<br/>");*/
		//die();
		
	}

}else{

	if($_SESSION["pla_affichage"]==1){
		
		// periode perso
		
		if(!empty($_GET["periode_deb"])){
			
			//echo("Source : get<br/>");
			
			$c_date_deb=convert_date_sql($_GET["periode_deb"]);
			$_SESSION["pla_periode_deb"]=$_GET["periode_deb"];
			
			$c_date_fin=convert_date_sql($_GET["periode_fin"]);
			$_SESSION["pla_periode_fin"]=$_GET["periode_fin"];
			
		}elseif(!empty($_SESSION["pla_periode_deb"])){
			
		/*	echo("Source : session<br/>");
			echo("pla_periode_deb : ");
				echo($_SESSION["pla_periode_deb"]);
			echo("<br/>");
			echo("pla_periode_fin : ");
				echo($_SESSION["pla_periode_fin"]);
			echo("<br/>");*/
			//die();
			
			$c_date_deb=convert_date_sql($_SESSION["pla_periode_deb"]);
			$c_date_fin=convert_date_sql($_SESSION["pla_periode_fin"]);
		
		}else{
			
			//echo("Source : defaut<br/>");

			$jour_deb=date("N");	
			$deb_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1), $annee);
			$c_date_deb=date("d/m/Y",$deb_mk);
			$_SESSION["pla_periode_deb"]=$c_date_deb;
			
			$fin_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1)+6, $annee);
			$c_date_fin=date("d/m/Y",$fin_mk);
			$_SESSION["pla_periode_fin"]=$c_date_fin;
			
		}
		$date_deb = new DateTime($c_date_deb);
		$date_fin = new DateTime($c_date_fin);	
		
	}else{
		
		if(isset($_GET["mois"]) AND isset($_GET["annee"])){
			$mois=$_GET["mois"];
			$annee=$_GET["annee"];
		}else{
			$mois=date("n");
			$annee=date("Y");
		}
		$jour_deb=date("N", mktime(0, 0, 0, $mois, 1, $annee));	
		$deb_mk=mktime(0, 0, 0, $mois, 1-($jour_deb-1), $annee);
		$date_deb=date_create(date("Y-m-d",$deb_mk));
		
		$jour_fin=date("N", mktime(0, 0, 0, $mois+1, 0, $annee));	
		$fin_mk=mktime(0, 0, 0, $mois+1, 5-$jour_fin, $annee);
		$date_fin=date_create(date("Y-m-d",$fin_mk));
		$_SESSION["pla_periode_deb"]=$date_deb->format("d/m/Y");
		$_SESSION["pla_periode_fin"]=$date_fin->format("d/m/Y");
	}
}
// GESTION DE LA PAGINATION
if($_SESSION["pla_affichage"]==1){

	$interval_date = $date_deb->diff($date_fin);
	
	$prec_fin = new DateTime($date_deb->format("Y-m-d"));
	$prec_fin->sub(new DateInterval('P2D'));
	
	$prec_deb=new DateTime($prec_fin->format("Y-m-d"));
	$prec_deb->sub(new DateInterval('P'. $interval_date->format('%a') .'D'));
	
	$suiv_deb=new DateTime($date_fin->format("Y-m-d"));
	$suiv_deb->add(new DateInterval('P2D'));
	
	$suiv_fin=new DateTime($suiv_deb->format("Y-m-d"));
	$suiv_fin->add(new DateInterval('P'. $interval_date->format('%a') .'D'));

}else{
	$time_mois_suiv=mktime(0, 0, 0, $mois+1, 1, $annee);		
	$mois_suiv=date("n",$time_mois_suiv);
	$annee_suiv=date("Y",$time_mois_suiv);
	
	$time_mois_prec=mktime(0, 0, 0, $mois-1, 1, $annee);	
	$mois_prec=date("n",$time_mois_prec);
	$annee_prec=date("Y",$time_mois_prec);
}

	/*------------------------------
		DONNEE NECESSAIRE 
	------------------------------*/

		//SOCIETES / AGENCES

	$sql="SELECT soc_agence FROM Societes WHERE soc_id=:acc_societe;";
	$req = $Conn->prepare($sql);
	$req->bindParam(":acc_societe",$acc_societe);
	$req->execute();
	$societe = $req->fetch();
	
		// LES COMMERCIAUX
		
	$sql="SELECT com_id,com_label_1,com_label_2 FROM Commerciaux";
	if($acc_agence>0){
		$sql.=" WHERE com_agence=:acc_agence";
	}
	$sql.=" ORDER BY com_label_1,com_label_2";
	$req = $ConnSoc->prepare($sql);
	if($acc_agence>0){
			$req->bindParam(":acc_agence",$acc_agence);
	}
	$req->execute();
	$commerciaux = $req->fetchAll();
	
		// LES VEHICULES
		
	$sql="SELECT veh_id,veh_libelle FROM Vehicules WHERE veh_societe=:acc_societe";
	if($acc_agence>0){
		$sql.=" AND veh_agence=:acc_agence";
	}
	$sql.=" ORDER BY veh_libelle,veh_libelle";
	$req = $Conn->prepare($sql);
	$req->bindParam(":acc_societe",$acc_societe);
	if($acc_agence>0){
			$req->bindParam(":acc_agence",$acc_agence);
	}
	$req->execute();
	$vehicules = $req->fetchAll();
	
	
/*--------------------------------------
		FONCTION 
----------------------------------------*/
// RETOURNE L'ENTETE D'UNE Semaine

Function w_planning_head($semaine,$date_deb){
	
	$f_date=new DateTime($date_deb->format('Y-m-d'));
	
	echo("<div class='row mt15 ml15 mr15 bg-dark'  >");						
		echo("<div class='col-sm-1' >");
			echo("Semaine " . $semaine);
		echo("</div>");
		echo("<div class='col-sm-11' >");						
			echo("<div class='row' >");								
				$f_date->sub(new DateInterval('P1D'));
				for($j=0;$j<=5;$j++){
					$f_date->add(new DateInterval('P1D')); 
					$f_date->setTimezone(new DateTimeZone('Europe/Paris'));
					echo("<div class='col-sm-2 text-center' >");
						$date_f_fr=$f_date->getTimestamp();
						echo(strftime("%A %d",$date_f_fr));
					echo("</div>");
				}									
			echo("</div>");
		
			echo("<div class='row' >");	
			for ($m = 0; $m <= 5; $m++){ 
					echo("<div class='col-sm-1 text-center' >");
						echo("Matin");
					echo("</div>");
					echo("<div class='col-sm-1 text-center' >");
						echo("Après-Midi");
					echo("</div>");
			}; 								
			echo("</div>");
		echo("</div>");
	echo("</div>");
}

function w_planning_ligne($int_id,$int_label,$date_deb,$planning,$semaine){

	$f_date=new DateTime($date_deb->format('Y-m-d'));
				
	echo("<div class='row ml15 mr15' >");					
		echo("<div class='col-sm-1 planning_intervenant' >");	
			echo($int_label);
		echo("</div>");
		echo("<div class='col-sm-11' >");
			echo("<div class='row' >");
		
				for($j=0;$j<=5;$j++){

					$date_lib=$f_date->format('Y-m-d');
					
					
					for($d=1;$d<=2;$d++){
						
						$case_id=$int_id . "_" . $date_lib . "_" . $d;
						
						if(!empty($planning[$date_lib][$d])){
							$case=$planning[$date_lib][$d];
							$class_case="";
							$class_contenu="";
							$data_contenu="";
							switch ($case["type"]) {
								case 0:
									$class_case="";
									break;
								case 1:
									$class_case=" action";
									$class_contenu=" action-" . $case["ref"];
									$data_contenu=" data-action='" . $case["ref"] . "'";
									break;
								case 3:
									$class_case=" case_info click";
									break;
							} 
							echo("<div class='col-sm-1 planning_case" . $class_case . "' id='case_" . $case_id . "' >");							
								echo("<div class='drag" . $class_contenu . "' style='" . $case["style"] . "' id='contenu_" . $case_id . "' " . $data_contenu . " >");
									echo($case["texte"]);
								echo("</div>");						
							echo("</div>");
						}else{ 
							echo("<div class='col-sm-1 planning_case libre click' id='case_" . $case_id . "' >&nbsp;</div>");										
						}
					}
					$f_date->add(new DateInterval('P1D'));
				}
			echo("</div>");
		echo("</div>");
	echo("</div>");
};
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" >
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

	</head>

	<body class="sb-top sb-top-sm" >
		<!-- Start: Main -->
		<div id="main">
			<?php
				include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<header id="topbar"  >
					<div class="topbar-left">
						<ol class="breadcrumb">							
							<li>
								Planning
							</li>
							<li class="crumb-trail">
					<?php		if($_SESSION["pla_affichage"]==1){ 
									echo("Période du " . strftime("%d %b %Y",$date_deb->getTimestamp()) . " au " . strftime("%d %b %Y",$date_fin->getTimestamp()));
								}else{
									echo(strftime("%B %Y",mktime(0, 0, 0, $mois, 1, $annee)));
								} ?>
							</li>
						</ol>
					</div>
				</header>
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn">

						
				<?php
				
					$planning=array();				
					
					$bcl_date_deb = new DateTime($date_deb->format('Y-m-d'));
					
					while ($bcl_date_deb<$date_fin){
								
						
						$bcl_date_fin = new DateTime($bcl_date_deb->format('Y-m-d'));
						$bcl_date_fin->add(new DateInterval('P5D'));
			
						$semaine=$bcl_date_deb->format("W");
						$annee=$bcl_date_deb->format("Y");

						w_planning_head($semaine,$bcl_date_deb);
						
						$sql="SELECT * FROM ((";
						
						$sql.="Intervenants LEFT JOIN plannings_intervenants ON (Intervenants.int_id=plannings_intervenants.pin_intervenant)";
						$sql.=")";
						
						$sql.=" LEFT OUTER JOIN plannings_dates ON (Intervenants.int_id=plannings_dates.pda_intervenant";
							$sql.=" AND plannings_dates.pda_date>='" . $bcl_date_deb->format('Y-m-d') . "' AND plannings_dates.pda_date<='" . $bcl_date_fin->format('Y-m-d') . "'";
						$sql.="))";
						
						$sql.=" WHERE pin_semaine=" . $semaine . " AND pin_annee=" . $annee;					
						if($acc_agence>0){
							$sql.=" AND int_agence=" . $acc_agence;
						}
						$sql.=" ORDER BY pin_place,int_id,pda_date,pda_demi";
						$req = $ConnSoc->query($sql);
						$result = $req->fetchAll();

						if(!empty($result)){ 
							$int_id=0;		
							foreach($result as $r)
							{	
								if($int_id!=$r["int_id"]){
									
									if($int_id>0){									
										w_planning_ligne($int_id,$int_label,$bcl_date_deb,$planning,$semaine);
									}
									$int_id=$r["int_id"];
									$int_label=$r["int_label_1"] . " ". $r["int_label_2"];	
									$planning=array();
									
								}							
								if(!empty($r["pda_date"])){
									$planning[$r["pda_date"]][$r["pda_demi"]]=array(
										"type" => $r["pda_type"],
										"ref" => $r["pda_ref_1"],
										"texte" => $r["pda_texte"],
										"style" => $r["pda_style"]
										
									);
								}
							}
							w_planning_ligne($int_id,$int_label,$bcl_date_deb,$planning,$semaine);
						};				
						$bcl_date_deb->add(new DateInterval('P7D'));											
					}
					// FIN WHILE ?>
					
					
				</section>
				
				<!-- End: Content -->
			</section>
		</div>
		
		
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
			<?php	if($_SESSION["pla_affichage"]==1){ ?>
						<a href="planning.php?periode_deb=<?=$prec_deb->format("d/m/Y")?>&periode_fin=<?=$prec_fin->format("d/m/Y")?>" class="btn btn-default btn-sm" >
							<i class="fa fa-arrow-left"></i>					
							Période précédente
						</a>
			<?php	}else{ ?>
						<a href="planning.php?mois=<?=$mois_prec?>&annee=<?=$annee_prec?>" class="btn btn-default btn-sm" >
							<i class="fa fa-arrow-left"></i>					
							<?=strftime("%B %Y",$time_mois_prec)?>
						</a>	
			<?php	} ?>
					
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-affiche" >
						<i class="fa fa-calendar" aria-hidden="true"></i>
					</button>
			<?php	if($_SESSION["pla_affichage"]==1){ ?>
						<a href="planning.php?periode_deb=<?=$suiv_deb->format("d/m/Y")?>&periode_fin=<?=$suiv_fin->format("d/m/Y")?>" class="btn btn-default btn-sm" >											
							Période suivante
							<i class="fa fa-arrow-right"></i>	
						</a>
			<?php	}else{ ?>
						<a href="planning.php?mois=<?=$mois_suiv?>&annee=<?=$annee_suiv?>" class="btn btn-default btn-sm" >											
							<?=strftime("%B %Y",$time_mois_suiv)?>
							<i class="fa fa-arrow-right"></i>	
						</a>
			<?php	} ?>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="intervenant_cree.php" class="btn btn-success btn-sm" role="button" title="Ajouter une ligne de planning" data-placement="top" data-toggle="tooltip" >
						<i class="fa fa-user" ></i>
					</a>
			<?php	if($acc_agence==0 AND $societe["soc_agence"]!=1){ ?>
						<span title="Connectez-vous sur une agence pour organiser le planning" data-placement="top" data-toggle="tooltip" >
							<button class="btn btn-warning btn-sm disabled" >
								<i class="fa fa-user" ></i>
							</button>	
						</span>
			<?php	}else{ ?>
						<a href="planning_intervenant.php" class="btn btn-warning btn-sm" role="button" title="Organiser les lignes" data-placement="top" data-toggle="tooltip" >
							<i class="fa fa-user" ></i>
						</a>	
			<?php	} ?>
									
				</div>
			</div>
		</footer>
		
		
		<!-- MENU CONTEXTUEL -->
		<div id="context-add" class="context-menu" >		
			<ul class="dropdown-menu" >				
				<li>
					<a href="#" id="ctx_add_action" >Action</a>							
				</li>
				<li>
					<a tabindex="-1" href="#">Congés</a>					
				</li>
				<li class="dropdown-submenu">
					<a tabindex="-1" href="#">Autres</a>
					<ul class="dropdown-menu " >
				<?php
					$req=$Conn -> query("SELECT pca_id,pca_libelle,pca_couleur FROM Plannings_Cases WHERE pca_type=0 ORDER BY pca_libelle;");
					$case = $req->fetchAll();
					if(!empty($case)){
						foreach($case as $c){ 
							echo("<li>");
								echo("<a href='#' class='add-case' data-case='" . $c["pca_id"] . "' >");
									echo ("<span class='badge' style='background-color:" . $c["pca_couleur"] . "' >&nbsp;</span>");
									echo ($c["pca_libelle"]);									
								echo("</a>");
							echo("</li>");							
						}
					} ?>						
					</ul>
				</li>
			</ul>
		</div>
		<div id="context-info" class="context-menu" >		
			<ul class="dropdown-menu" >			
			<?php
				if(!empty($case)){
					foreach($case as $c){ 
						echo("<li>");
							echo("<a href='#' class='add-case' data-case='" . $c["pca_id"] . "' >");
								echo ("<span class='badge' style='background-color:" . $c["pca_couleur"] . "' >&nbsp;</span>");
								echo ($c["pca_libelle"]);									
							echo("</a>");
						echo("</li>");							
					}
				} ?>
				<li>
					<a href='#' class='supp-info' >
						<i class="fa fa-times" ></i>
						Supprimer					
					</a>
				</li>	
			</ul>
		</div>		
		<div id="context-action" class="context-menu" >		
			<ul class="dropdown-menu" >
				<li>
					<a href='#' class="no-select" id="ctx_act_add_cli" >
						<i class="fa fa-add" ></i>
						Ajouter un client					
					</a>
				</li>
				<li>
					<a href='#' class="no-select" id="ctx_act_aff_date" >
						<i class="fa fa-add" ></i>
						Afficher les dates				
					</a>
				</li>					
				
				<li class="select_required" >
					<a href='#' id="ctx_act_add_date" >
						<i class="fa fa-add" ></i>
						Ajouter la sélection					
					</a>
				</li>
				<li class="select_required" >
					<a href='#' id="ctx_act_move_cli" >
						<i class="fa fa-add" ></i>
						Transférer client				
					</a>
				</li>				
			</ul>
		</div>
		
		<!-- MODAL -->
		<div id="modal-confirme" class="modal fade" role="dialog" >
			<div class="modal-dialog">			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
						<p id="modal-text" ></p>
						<p>Etes-vous sur de vouloir continuer ?</p>
					</div>
					<div class="modal-footer">
						<button type="button" id="confirme_oui" class="btn btn-success btn-sm" data-dismiss="modal" data-confirmation=""  >Oui</button>
						<button type="button" id="confirme_non" class="btn btn-default btn-sm" data-dismiss="modal">Non</button>
					</div>
				</div>
			</div>
		</div>
	
		<div id="modal_action" class="modal fade" role="dialog" >
			<div class="modal-dialog modal-lg">			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Nouvelle action</h4>
					</div>
					
					<form method="post" action="action_cree_enr.php" id="form_add_action" >
						<div>
							<input type="hidden" id="mdl_act_liste_case" name="mdl_act_liste_case" value="" />
							<input type="hidden" id="mdl_act_action" name="mdl_act_action" value="0" />
						</div>
						<div class="modal-body admin-form">
							
							<div class="row" >
								<div class="col-md-12" >	
									<div class="section" >
										<select name="mdl_act_client" id="mdl_act_client" class="client-select" required >
											<option value="0" selected >Client</option>					
										</select>	
									</div>
								</div>
							</div>		
							<div class="row" >
								<!-- colonne 1 -->
								<div id="modal_act_col_1" class="col-md-6" >	
									<div class="section-divider mb40" >
										<span>Lieu d'intervention</span>
									</div>
									<div class="row" >
										<div class="col-md-4" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr-ref" name="mdl_act_adr_ref" id="mdl_act_adr_client" value="1" checked >
													<span class="radio"></span>Client
												</label>
											</div>
										</div>
										<div class="col-md-4" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr-ref" name="mdl_act_adr_ref" id="mdl_act_adr_si2p" value="2" >
													<span class="radio"></span>Agence 
												</label>
											</div>
										</div>
										<div class="col-md-4" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="adr-ref" name="mdl_act_adr_ref" id="mdl_act_adr_autre" value="3" >
													<span class="radio"></span>Autres 
												</label>
											</div>
										</div>
									</div>	
									<div class="row" class="cont-adr-client"  >
										<div class="col-md-12" >	
											<div class="section" >
												<select name="mdl_act_adr_ref_id" id="mdl_act_adr_ref_id" class="select2 adresse-client" >
													<option value="0" >Selectionnez une adresse</option>										
												</select>
											</div>
										</div>
									</div>
									<div class="cont-adresse" >
										<div class="row">
											<div class="col-md-12" >	
												<input type="text" name="mdl_act_act_adr_nom" id="mdl_act_adr_nom" class="gui-input nom champ_adresse" placeholder="Nom" />	
												<input type="text" name="mdl_act_act_adr_service" id="mdl_act_adr_service" class="gui-input champ_adresse" placeholder="Service" />		
												<input type="text" name="mdl_act_act_adr1" id="mdl_act_adr1" class="gui-input champ_adresse" placeholder="Adresse" />		
												<input type="text" name="mdl_act_act_adr2" id="mdl_act_adr2" class="gui-input champ_adresse" placeholder="Complément 1" />		
												<input type="text" name="mdl_act_act_adr3" id="mdl_act_adr3" class="gui-input champ_adresse" placeholder="Complément 2" />		
											</div>
										</div>
										<div class="row">
											<div class="col-md-3" >	
												<input type="text" name="mdl_act_act_adr_cp" id="mdl_act_adr_cp" class="gui-input code-postal champ_adresse" placeholder="CP" />		
											</div>
											<div class="col-md-9" >	
												<input type="text" name="mdl_act_act_adr_ville" id="mdl_act_adr_ville" class="gui-input nom champ_adresse" placeholder="Ville" />		
											</div>
										</div>
									</div>

									<!-- CONTACT -->
									<div class="section-divider mb40" >
										<span>Contact</span>
									</div>								
									<div class="row" >
										<div class="col-md-9" >	
											<div class="section" >
												<select name="mdl_act_contact" id="mdl_act_contact" class="select2" >
													<option value="0" >Selectionnez un contact</option>										
												</select>
											</div>
										</div>
										<div class="col-md-3" >	
											<div class="section mt10" >
												 <label class="option">
													<input type="checkbox" name="mdl_act_contact_autre" id="contact_autre" >
													<span class="checkbox"></span>Autres 
												</label>
											</div>
										</div>
									</div>								
									<div class="row">
										<div class="col-md-6" >	
											<input type="text" name="mdl_act_con_nom" id="mdl_act_con_nom" class="gui-input nom champ-contact" placeholder="Nom" />	
										</div>
										<div class="col-md-6" >	
											<input type="text" name="mdl_act_con_prenom" id="mdl_act_con_prenom" class="gui-input prenom champ-contact" placeholder="Prénom" />	
										</div>
									</div>
									<div class="row">
										<div class="col-md-6" >	
											<input type="text" name="mdl_act_con_tel" id="mdl_act_con_portable" class="gui-input telephone champ-contact" placeholder="Téléphone" />		
										</div>
										<div class="col-md-6" >	
											<input type="text" name="mdl_act_con_portable" id="mdl_act_con_portable" class="gui-input telephone champ-contact" placeholder="Portable" />		
										</div>
									</div>
									
								</div>
								<!-- fin colonne 1 -->
								
								<!-- colonne 2 -->
								<div class="col-md-6" id="modal_act_col_2" >	
									<div class="section-divider mb40" >
										<span>Formation</span>
									</div>								
									<div class="row" >
										
										<div class="col-md-6" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="produit_type" name="mdl_act_produit_type" id="mdl_act_produit_intra" checked value="1" >
													<span class="radio"></span>Produit intra
												</label>
											</div>
										</div>
										
										<div class="col-md-6" >	
											<div class="section" >
												 <label class="option">
													<input type="radio" class="produit_type" name="mdl_act_produit_type" id="mdl_act_produit_inter" value="2" >
													<span class="radio"></span>Produit inter
												</label>
											</div>
										</div>
										
									</div>
									<div class="row" >
										<div class="col-md-12" >	
											<div class="section" >
												<select name="mdl_act_produit" id="mdl_act_produit" class="select2" required >
													<option value="0" >Selectionnez un produit ...</option>					
												</select>
											</div>
										</div>
									</div>
									<div id="modal_act_session" >
										<div class="row" >
											<div class="col-md-10" >
												<h4 class="text-right">Nombre de session / demi-journée :</h4>
											</div>
											<div class="col-md-2" >	
												<input type="text" name="mdl_act_nb_session" id="mdl_act_nb_session" class="gui-input" type="number" value="1"  min="1" step="1" max="4" />		
											</div>
										</div>
										
										<div class="row" >
											<div class="col-md-6">
												<h4 class="text-left">Matin</h4>
											</div>
											<div class="col-md-6">
												<h4 class="text-left">Après-Midi</h4>
											</div>
										</div>
										<div class="row" id="session_1" >
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_deb_matin_1" name="mdl_act_h_deb_1_1" class="gui-input" placeholder="Heure de début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_fin_matin_1" name="mdl_act_h_fin_1_1" class="gui-input" placeholder="Heure de fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_deb_ap_1" name="mdl_act_h_deb_2_1" class="gui-input" placeholder="Heure de début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_fin_ap_1" name="mdl_act_h_fin_2_1" class="gui-input" placeholder="Heure de fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
										</div>
										<div class="row" id="session_2" style="display:none;" >
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_deb_matin_2" name="mdl_act_h_deb_1_2" class="gui-input" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_fin_matin_2" name="mdl_act_h_fin_1_2" class="gui-input" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_deb_ap_2" name="mdl_act_h_deb_2_2" class="gui-input" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_fin_ap_2" name="mdl_act_h_fin_2_2" class="gui-input" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
										</div>
										<div class="row" id="session_3" style="display:none;" >
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_deb_matin_3" name="mdl_act_h_deb_1_3" class="gui-input" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_fin_matin_3" name="mdl_act_h_fin_1_3" class="gui-input" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_deb_ap_3" name="mdl_act_h_deb_2_3" class="gui-input" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_fin_ap_3" name="mdl_act_h_fin_2_3" class="gui-input" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
										</div>
										<div class="row" id="session_4" style="display:none;" >
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_deb_matin_4" name="mdl_act_h_deb_1_4" class="gui-input" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_fin_matin_4" name="mdl_act_h_fin_1_4" class="gui-input" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_deb_ap_4" name="mdl_act_h_deb_2_4" class="gui-input" placeholder="Début" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-md-3">
												<div class="section">
													<span class="prepend-icon">
														<input type="text" id="mdl_act_h_fin_ap_4" name="mdl_act_h_fin_2_4" class="gui-input" placeholder="Fin" value="">
														<span class="field-icon">
															<i class="fa fa-clock-o"></i>
														</span>
													</span>
												</div>
											</div>
										</div>
									</div>	
									<div class="row" id="modal_act_vehicule"  >
										<div class="col-md-12" >	
											<div class="section" >
												<select name="mdl_act_vehicule" id="mdl_act_vehicule" class="select2" >
													<option value="0" >Selectionnez un vehicule ...</option>
									<?php			if(!empty($vehicules)){
														foreach($vehicules as $v){
															echo("<option value='" . $v["veh_id"] . "' >" . $v["veh_libelle"] . "</option>");
														}
													} ?>		
												</select>
											</div>
										</div>
									</div>
									<div class="section-divider mb40" >
										<span>Commerce</span>
									</div>
									<div class="row" >
										<div class="col-md-12" >	
											<div class="section" >
												<select name="mdl_act_commercial" id="mdl_act_commercial" class="select2" required >
													<option value="0" >Selectionnez un commercial ...</option>	
									<?php			if(!empty($commerciaux)){
														foreach($commerciaux as $c){
															echo("<option value='" . $c["com_id"] . "' >" . $c["com_label_1"] . " " . $c["com_label_1"] . "</option>");
														}
													} ?>
												</select>
											</div>
										</div>
									</div>
									<div class="row" >
										<div class="col-md-12" >	
											<div class="section" id="cont_ca_intra" >
												<label>CA Formation</label>
												<div class="field prepend-icon">
													<input id="mdl_act_ca_intra" name="mdl_act_ca_intra" class="gui-input numerique" type="number" value="0" min="0.00" step="0.01">
													<label class="field-icon" for="ca_intra">
														 <i class="fa fa-euro" ></i>
													</label>
												</div>
											</div>
											<div class="section" id="cont_ca_inter" style="display:none;"  >
												<label>CA / stagiaire</label>
												<div class="field prepend-icon">
													<input id="mdl_act_ca_inter" name="mdl_act_ca_inter" class="gui-input numerique" type="number" value="0" min="0.00" step="0.01">
													<label class="field-icon" for="ca_inter">
														 <i class="fa fa-euro" ></i>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- fin colonne 2 -->
							</div>
						</div>					
						<div class="modal-footer">
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-save" ></i>
								Enregistrer
							</button>
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>	
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<!-- GESTION DE LA PERIODE A AFFICHER -->
		<div id="modal-affiche" class="modal fade" role="dialog" >
			<div class="modal-dialog">			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Affichage</h4>
					</div>
					<form method="post" action="planning.php" >
						<div>
							<input type="hidden" name="filt_aff" value="filt" />
						</div>
						<div class="modal-body admin-form">					
							<div class="row">
								<div class="col-md-12">
									<div class="section-divider mb40">
										<span>Affichage par mois</span>
									</div>
								</div>
							</div>
							<div class="row" >
								<div class="col-md-6">									
									<div class="field prepend-icon">
							<?php		if($_SESSION["pla_affichage"]==1){ ?>
											<input type="text" id="periode_mois" name="periode_mois" class="gui-input champ_date" placeholder="Selectionner un mois" />
							<?php		}else{ ?>
											<input type="text" id="periode_mois" name="periode_mois" class="gui-input champ_date" placeholder="Selectionner un mois" value="<?=$mois."/".$annee?>" />
							<?php		} ?>
										<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
									</div>									
								</div>							
							</div>
						
							
							<div class="row">
								<div class="col-md-12">
									<div class="section-divider mb40">
										<span>Affichage personalisé</span>
									</div>
								</div>
							</div>
							<div class="row" >
								<div class="col-md-6">
									Du
									<div class="field prepend-icon">
							<?php		if($_SESSION["pla_affichage"]==1){ ?>
											<input type="text" id="periode_deb" name="periode_deb" class="gui-input" placeholder="Selectionner une date" value="<?=$date_deb->format("d/m/Y")?>" />
							<?php		}else{ ?>
											<input type="text" id="periode_deb" name="periode_deb" class="gui-input" placeholder="Selectionner une date" />
							<?php		} ?>
										<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
									</div>									
								</div>
								<div class="col-md-6">
									Au
									<div class="field prepend-icon">
										
							<?php		if($_SESSION["pla_affichage"]==1){ ?>
											<input type="text" id="periode_fin" name="periode_fin" class="gui-input" placeholder="Selectionner une date" value="<?=$date_fin->format("d/m/Y")?>" />
							<?php		}else{ ?>
											<input type="text" id="periode_fin" name="periode_fin" class="gui-input" placeholder="Selectionner une date" />
							<?php		} ?>
										<label class="field-icon"><i class="fa fa-calendar-o"></i></label>
									</div>
								</div>						
							</div>					
						</div>					
						<div class="modal-footer">
							<button type="submit" class="btn btn-success btn-sm" >
								<i class="fa fa-view" ></i>
								Afficher
							</button>
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>	
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<!-- MODAL CLIENT // AFFICHE LES INSCRIT A UNE ACTION -->		
		<div id="modal_client" class="modal fade" role="dialog" >
			<div class="modal-dialog">			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Transfert de clients</h4>
					</div>
					<form method="post" action="#" >
						<div>
							<input type="hidden" name="mdl_cli_nb_client" id="mdl_cli_nb_client" value="0" />
							<input type="hidden" name="mdl_cli_action" id="mdl_cli_action" value="0" />
						</div>
						<div class="modal-body admin-form" >
							<div id="mdl_cli_input" >
							
							</div>
							<div class="section-divider mb40" >
								<span>Lieu d'intervention</span>
							</div>
							<div class="row" >
								<div class="col-md-4" >	
									<div class="section" >
										 <label class="option">
											<input type="radio" class="adr-ref" name="mdl_cli_adr_ref" value="1" id="mdl_cli_adr_client" checked >
											<span class="radio"></span>Client
										</label>
									</div>
								</div>
								<div class="col-md-4" >	
									<div class="section" >
										 <label class="option">
											<input type="radio" class="adr-ref" name="mdl_cli_adr_ref" value="2" id="mdl_cli_adr_si2p" >
											<span class="radio"></span>Agence 
										</label>
									</div>
								</div>
								<div class="col-md-4" >	
									<div class="section" >
										 <label class="option">
											<input type="radio" class="adr-ref" name="mdl_cli_adr_ref" value="3" id="mdl_cli_adr_autre" >
											<span class="radio"></span>Autres 
										</label>
									</div>
								</div>
							</div>	
							<div class="row" class="cont-adr-client"  >
								<div class="col-md-12" >	
									<div class="section" >
										<select name="mdl_cli_adr_ref_id" class="select2 adresse-client" >
											<option value="0" >Selectionnez une adresse</option>										
										</select>
									</div>
								</div>
							</div>
							<div class="cont-adresse" >
								<div class="row">
									<div class="col-md-12" >	
										<input type="text" name="mdl_cli_adr_nom" id="mdl_cli_adr_nom" class="gui-input nom champ_adresse" placeholder="Nom" />	
										<input type="text" name="mdl_cli_adr_service" id="mdl_cli_adr_service" class="gui-input champ_adresse" placeholder="Service" />		
										<input type="text" name="mdl_cli_adr1" id="mdl_cli_adr1" class="gui-input champ_adresse" placeholder="Adresse" />		
										<input type="text" name="mdl_cli_adr2" id="mdl_cli_adr2" class="gui-input champ_adresse" placeholder="Complément 1" />		
										<input type="text" name="mdl_cli_adr3" id="mdl_cli_adr3" class="gui-input champ_adresse" placeholder="Complément 2" />		
									</div>
								</div>
								<div class="row">
									<div class="col-md-3" >	
										<input type="text" name="mdl_cli_adr_cp" id="adr_cp" class="gui-input code-postal champ_adresse" placeholder="CP" />		
									</div>
									<div class="col-md-9" >	
										<input type="text" name="mdl_cli_adr_ville" id="adr_ville" class="gui-input nom champ_adresse" placeholder="Ville" />		
									</div>
								</div>
							</div>

							<!-- CONTACT -->
							<div class="section-divider mb40" >
								<span>Contact</span>
							</div>								
							<div class="row" >
								<div class="col-md-9" >	
									<div class="section" >
										<select name="mdl_cli_contact" class="select2 contact-client" >
											<option value="0" >Selectionnez un contact</option>										
										</select>
									</div>
								</div>
								<div class="col-md-3" >	
									<div class="section mt10" >
										 <label class="option">
											<input type="checkbox" name="mdl_cli_contact_autre" class="contact-autre" >
											<span class="checkbox"></span>Autres 
										</label>
									</div>
								</div>
							</div>								
							<div class="row">
								<div class="col-md-6" >	
									<input type="text" name="mdl_cli_con_nom" id="mdl_cli_con_nom" class="gui-input nom champ-contact" placeholder="Nom" />	
								</div>
								<div class="col-md-6" >	
									<input type="text" name="mdl_cli_con_prenom" id="mdl_cli_con_prenom" class="gui-input prenom champ-contact" placeholder="Prénom" />	
								</div>
							</div>
							<div class="row">
								<div class="col-md-6" >	
									<input type="text" name="mdl_cli_con_tel" id="mdl_cli_con_portable" class="gui-input telephone champ-contact" placeholder="Téléphone" />		
								</div>
								<div class="col-md-6" >	
									<input type="text" name="mdl_cli_con_portable" id="mdl_cli_con_portable" class="gui-input telephone champ-contact" placeholder="Portable" />		
								</div>
							</div>							
						</div>					
						<div class="modal-footer">
							<button type="button" class="btn btn-success btn-sm" id="mdl_cli_submit" >
								<i class="fa fa-long-arrow-right" ></i>
								Transférer
							</button>
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Annuler
							</button>	
						</div>
					</form>
				</div>
			</div>
		</div>
		
		

		
		<?php
		include "includes/footer_script.inc.php"; ?>	
		<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery-ui-monthpicker.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>
		
		<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
		<script type="text/javascript" src="/assets/js/custom.js"></script>
		
		
		
		<script type="text/javascript" >
			
			// VARIABLE GLOBAL 
			var elt_source;
			var nb_demi_j_select=0; // nb de demi jour selectionné au moment ou on affiche le modal action
			var type_case=0;		// utiliser pour la recuperation de la selection utilisateur 
			var case_click_drt;		// on memorise la case qui ouvre le m ctx pour les fonctions de traitement.
			var contenu_click_drt;	// on memorise le contenu equivalant.
			var action=0;			// id de l'action de référence determiné par le click droit
			var prefixe_modal=""	// certaine fonction peuvent être utilise sur des modals ou champ difféfrent 
										// cette variable permet de rendre les fonction générique					
			
			var tab_adresse=new Array(); // memorise les adresses dispo pour les utiliser sans repasser par une requete ajax
			var tab_contact=new Array();
			
			$(document).ready( function() {
				$(".select2").select2();
				// code requis pour select2 dans modal petit ecran
				$.fn.modal.Constructor.prototype.enforceFocus = function() {};
				// initialisation des event en fonction des classes affectées au case
				case_ini();
				
				// PERIODE D'AFFICHAGE PERSO	
		
				$('#periode_mois').monthpicker();
				$('#periode_mois').change(function(){
					$("#periode_deb").val("");
					$("#periode_fin").val("");
				});									
				$("#periode_deb").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=1) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},
					onSelect: function (input, inst){
						$("#periode_mois").val("");
						var date_fin=$('#periode_deb').datepicker( "getDate" );
						date_fin.setDate(date_fin.getDate() + 5);
						var max_fin=new Date();
						max_fin.setDate(date_fin.getDate() + 84);
						$("#periode_fin").datepicker( "setDate",date_fin);
						$("#periode_fin").datepicker( "option", "minDate", date_fin);
						$("#periode_fin").datepicker( "option", "maxDate", max_fin);
					}
				});
				$("#periode_fin").datepicker({
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function(input, inst){
						var newclass = 'admin-form';
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}		
					},
					beforeShowDay: function (date) {
						if (date.getDay()!=6) { // La semaine commence à 0 = Dimanche
							return [false, ''];
						} else {
							return [true, ''];
						}
					},
					
				});			
				
				//-------------------------------------
				// 	GESTION DES MENU CONTEXTUEL
				//------------------------------------		
				
					// AFFICHAGE DES MENU CONTEXTUEL
				$(document).on("contextmenu",".planning_case",function(e){
					
					if(e.pageX/$(document).width()>0.75){
						$(".dropdown-submenu").addClass( "pull-left");
					}else{
						$(".dropdown-submenu").removeClass( "pull-left");
					}
					
					case_click_drt=$(this);	// on memorise la case qui ouvre le m ctx pour les fonctions de traitement.
					contenu_click_drt=$(this).find("div"); // on memorise le contenu equivalant.					
					action=0; // on efface action à chaque ouverture de de m ctx il sera calculé si necessaire
					if($(this).hasClass("libre")&&$(this).hasClass("case_select")){
						
						$("#context-add").css({top:e.pageY,left:e.pageX}).show();					
						
					}else if($(this).hasClass( "case_info" )&&$(this).hasClass("case_select")){
						
						$("#context-info").css({top:e.pageY,left:e.pageX}).show();
						
					}else if($(this).hasClass("action")){

						if(contenu_click_drt.data("action")){
							action=contenu_click_drt.data("action");
						}	
						if(action>0){
							if($(".case_select").length>0){
								$(".select_required").show();
								$(".no-select").hide();
							}else{
								$(".select_required").hide();
								$(".no-select").show();
							}
							$("#context-action").css({top:e.pageY,left:e.pageX}).show();
						}
					}
					
					// desactive le comportement par défaut du clique droit
					e.preventDefault();
					return false;
				});
				
					// TRAITEMENT DES CHOIX CONTEXTUEL
				
					// MENU #context-add
				$(".add-case").click(function(){
					type_case=$(this).data("case"); 	// public
					enregistrer_case();
					$(".context-menu").hide();	
				});				
								
				$(".supp-info").click(function(){
					
					$("#modal-confirme #modal-text").text("Vous êtes sur le point d'éffacer le contenu des cases sélectionnées");
					$('#modal-confirme').modal('show');
					
					//$("#confirme_oui").off() si plusieur utilisation du modal confirme			
					$("#confirme_oui").click(function(){
						supprimer_info();
					});
					
					$(".context-menu").hide();	
				});
				
				// ajout d'une nouvelle action
				$("#ctx_add_action").click(function(){
					afficher_modal_action();				
					nb_demi_j_select=$(".case_select").length;					
				});	
					// MENU #context-action
					
				// ajout de date sur une action existante
				$("#ctx_act_add_date").click(function(){
					ajouter_date_action(action);
					$(".context-menu").hide();	
				});		
				// ajout d'un client sur une action existante
				$("#ctx_act_add_cli").click(function(){	
					console.log("#ctx_act_add_cli click");
					afficher_modal_action();
				});
				
				// afficher les dates lié à une action
				$("#ctx_act_aff_date").click(function(){
					$(".action-" + action).each(function(){ 
						$(".action-" + action).addClass("action_select");
					});	
					$(".context-menu").hide();	
				});
				
				// transfert de client de l'action de ref vers nouvelle action
				$("#ctx_act_move_cli").click(function(){
					afficher_client_action(action);					
					$(".context-menu").hide();	
				});
				
			
					// FERMETURE DES MENU CONTEXTUEL
					
				$(document).on("contextmenu click","*:not(.planning_case)",function(e){
					$(".context-menu").hide();
					//e.preventDefault();
					//return false;
				});	
				
				//--------- FIN MENU CONTEXTUEL ---------
		
				
				/* --------------------------------------
					INI LIE AUX ACTIONS 
				-----------------------------------------*/	
				// INTERACTION ACTION DIRECT				
				$(".action").click(function() {
					console.log($(this).find("div").hasClass("action_select"));
					if(!$(this).find("div").hasClass("action_select")){
						action=$(this).find("div").data("action");
						document.location.href="action_voir.php?action=" + action;
					}else{
						selectionner_case($(this));
					}
				});
				
				/* --------------------------------------
					INTERACTION MODAL
				-----------------------------------------*/	
				
				// GENERIQUE (les traitements ci-dessous nécessite modal_prefixe
				$(".adr-ref").change(function(){
					afficher_type_adresse();
				});
				$(".adresse-client").change(function(){
					afficher_adresse($(this).val());
				});
				
				
				
				$("#contact").change(function(){
					afficher_contact($(this).val());
				});
				$("#contact_autre").click(function(){
					$("#contact").select2("val","0");
				});
				$(".produit_type").click(function(){
					if($('input[name=produit_type]:checked').val()==1){
						$("#cont_ca_inter").hide();
						$("#cont_ca_intra").show();
					}else{
						$("#cont_ca_inter").show();
						$("#cont_ca_intra").hide();
					}
					actualiser_produit_client();										
				});
				$("#produit").change(function(){		
					actualiser_produit();
				});				
				$("#nb_session").change(function(){
					afficher_sessions($(this).val());
				});				
				$('#form_add_action').on('submit', function(e) {
					e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire
					//var $this = $(this); // L'objet jQuery du formulaire					
					console.log("action : " + action);
					if(action==0){
						enregistrer_action($(this));					
					}else{
						enregistrer_action_client($(this));
					}					
				});
				
				// MODAL-CLIENT

				
				$("#modal_client_submit").click(function(){
					transferer_client();
				});
				//--------- FIN INI LIE AUX ACTIONS------------------
				
			});
			//--------- FIN DE DOCUMENT READY ------------------
			
//--------------- CODE GENERIQUE --------------------------------
			$(document).ready( function() {
				// MODAL-ACTION
				$(".client-select").select2({
					minimumInputLength: 1,
					ajax: {
						url: "ajax/ajax_client_select.php",
						dataType: 'json',
						delay: 250,
						method: 'GET',
						data: function (params) {
							return {
								q: params.term, // les termes de la requête
							};
						},
						processResults: function (data) {
							// data correspond au tableau json des résultat
							// data.nom permet d'accéder à l'information $retour["nom"]
							
							return {
								results: data,
							};
						},
					},
					templateResult: function(data) {
						// permet de chosir quelles informations seront affichées dans la liste de résultat
						// parmit les éléments de data
						//if (data.loading) return data.text;
						return data.nom;
					},
					templateSelection: function(data) {
						// permet de chosir quelles informations seront affichées dans le select
						// parmit les éléments de data
						// une fois que l'utilisateur aura fait un choix.
						
						//if (data.loading) return data.text;
						// Sinon affichage du placeholder (data.text).
						//if (data.text) return data.text;

						return data.nom;
					},
				});
				$(".client-select").change(function(){
					if(action==0){
							// nouvelle action
						get_adresses($(this).val(),1,1);
						get_contacts($(this).val(),1,1);
						//actualiser_client($(this).val());
					}
				});
			})
			
			//-------------------------------------
			// 		FONCTION MULTI MODAL
			//------------------------------------			
			function actualiser_adresses(json_adresses){	
				//console.log("actualiser_adresses(" + json_adresses + ")");
				$("#" + prefixe_modal + "adr_ref_id option[value!='0']").remove();
				var select_value=0;
				if(json_adresses){
					// on memorise les adressses
					var tab_adresse=new Array();
					for(i=0;i<=json_adresses.length-1;i++){
						tab_adresse[json_adresses[i].id]=new Array(7);
						tab_adresse[json_adresses[i].id][0]=json_adresses[i].text;
						tab_adresse[json_adresses[i].id][1]=json_adresses[i].service;
						tab_adresse[json_adresses[i].id][2]=json_adresses[i].ad1;
						tab_adresse[json_adresses[i].id][3]=json_adresses[i].ad2;
						tab_adresse[json_adresses[i].id][4]=json_adresses[i].ad3;
						tab_adresse[json_adresses[i].id][5]=json_adresses[i].cp;
						tab_adresse[json_adresses[i].id][6]=json_adresses[i].ville;
						tab_adresse[json_adresses[i].id][7]=json_adresses[i].defaut;
						if(json_adresses[i].defaut){
							select_value=json_adresses[i].id;
						}
					}
					// place apres le tab_ car déclenche un $("#adr_ref_id").change 
					$("#" + prefixe_modal + "adr_ref_id").select2({
						data: json_adresses				
					})
					console.log("select_value : " + select_value);
					$("#" + prefixe_modal + "adr_ref_id").val(select_value).trigger("change");
				}	
			};
			function actualiser_contacts(json_contacts){	
				console.log("actualiser_contacts(" + json_contacts + ")");
				$("#" + prefixe_modal + "contact option[value!='0']").remove();
				if(json_contacts){
					// on memorise les contacts
					var tab_contact=new Array();
					for(i=0;i<=json_contacts.length-1;i++){
						tab_contact[json_contacts[i].id]=new Array(6);
						tab_contact[json_contacts[i].id][0]=json_contacts[i].nom;
						tab_contact[json_contacts[i].id][1]=json_contacts[i].prenom;
						tab_contact[json_contacts[i].id][2]=json_contacts[i].tel;
						tab_contact[json_contacts[i].id][3]=json_contacts[i].portable;
						tab_contact[json_contacts[i].id][4]=json_contacts[i].defaut;

					}
					// place apres le tab_ car déclenche un $("#adr_ref_id").change 
					$("#" + prefixe_modal + "contact").select2({
						data: json_contacts				
					})
				}	
			};
			
			//-------------------------------------
			// 		FONCTION GENERIQUE
			//------------------------------------
			
			// retourne les adresses des clients passés en paramètre  
			function get_adresses(liste_ref_id,ref,type){
				//console.log("liste_ref_id : " + liste_ref_id);
				//console.log("ref : " + ref);
				//console.log("type : " + type);
				$.ajax({			
					type:'POST',
					url: 'ajax/ajax_get_adresses.php',
					data : 'liste_ref_id=' + liste_ref_id + "&ref=" + ref + "&type=" + type, 
					dataType: 'JSON',                
					success: function(data){					
						actualiser_adresses(data);
					},
					error: function() {
						
					}
				});
			}
			// retourne les contacts d'un client
			function get_contacts(ref_id,ref){
				//console.log("liste_ref_id : " + liste_ref_id);
				//console.log("ref : " + ref);
				//console.log("type : " + type);
				$.ajax({			
					type:'POST',
					url: 'ajax/ajax_get_contacts.php',
					data : 'ref_id=' + ref_id + "&ref=" + ref, 
					dataType: 'JSON',                
					success: function(data){					
						actualiser_contacts(data);
					},
					error: function() {
						
					}
				});
			}
			
			// 		FIN FONCTION GENERIQUE
			
//--------------- FIN CODE GENERIQUE --------------------------------
			
			// actualise la liste des adresses d'interventions disponibles
			// le tableau json est genere par la fonction get_adresses
			
			
			
			
			//-------------------------------------
			// 		FONCTION INTERACTION PLANNING
			//------------------------------------
			// vide une case pour la rendre disponible		
			function case_vide(case_id){
				$("#contenu_" + case_id).remove();
				$("#case_" + case_id).html("&nbsp;");
				$("#case_" + case_id).removeClass("action");			
				// la case devient cliquable
				$("#case_" + case_id).addClass("libre");
				$("#case_" + case_id).addClass("click");			
				
			}
			// reinitialise les event lié au case en fonction des classes (evite de recharger)
			function case_ini(){	
			
				$(".click").off();	
				$(".click").click(function(){
					selectionner_case($(this));
				});
				/*$(".click").mousehover(function(){
					selectionner_case($(this));
				});	*/				
				$(".libre").droppable({
					drop : function(event,ui){							
						var id_source=elt_source.replace("contenu_","");
						var id_cible=(this).id.replace("case_","");
						deplacer_case(id_source,id_cible);
					}
				});	
				$('.drag').draggable({
					snap : ".drag",
					revert : 'invalid', // renvoi les bloc s'il ne sont pas depose en zone drop
					cursor : "move",
					zIndex: 9999,
					start : function(){
						elt_source=(this).id;		
					}
				});
			}

			// select ou deselection d'une case
			function selectionner_case(elt){	
			
				console.log("selectionner_case()");
				console.log("action : " + action);
				console.log("class : " + elt.find("div").hasClass("action-" + action));
				
				if(action==0 || elt.find("div").hasClass("action-" + action) ){
					if(elt.hasClass( "case_select" )){
						// annule select
						elt.removeClass("case_select");	
						if($(".case_select").length==0){
							case_ini();
						}							
					}else{
						// select
						if($(".case_select").length==0){
							if(elt.hasClass( "case_info" )){
								$(".libre").off(); // .libre ne sont plus select	
							}else{
								$(".case_info").off(); // .info ne sont plus select	
							}
						}
						elt.addClass("case_select");	
					}
				}
			}		
			
			// retourne la liste des case selectionnées
			function recuperer_case_select(){
				var liste_case="";		
				var ident_case="";							
				$(".case_select").each(function(){ 
					ident_case=(this).id.replace("case_", "");
					liste_case=liste_case +  ident_case + ",";
				});	
				return liste_case;
			}
			
			// requete ajax pour enregistrer les dates selectionnés par l'utilisateur
			function enregistrer_case(){			
				var liste_case=recuperer_case_select();
				$.ajax({			
					type:'GET',
					url: 'ajax/ajax_add_case.php',
					data : 'type_case=' + type_case + "&liste_case=" + liste_case, 
					dataType: 'json',                
					success: function(data)
					{	ecrire_case(data);
						case_ini();							
					},
					error: function() {
						console.log("ERREUR AJAX");		
					}
				});
			};			
			
			// déplace une case
			function deplacer_case(id_source,id_cible){		
				$.ajax({			
					type:'GET',
					url: 'ajax/ajax_move_case.php',
					data : 'id_source=' + id_source + "&id_cible=" + id_cible, 
					dataType: 'json',                
					success: function(data)
					{	case_vide(id_source);
						ecrire_case(data);
						case_ini();
											
					},
					error: function() {
						console.log("ERREUR");		
					}
				});
			}			
			
			function supprimer_info(){
				console.log("supprimer_info()");
				var liste_case=recuperer_case_select();					
				$.ajax({			
					type:'GET',
					url: 'ajax/ajax_supp_case.php',
					data : "&liste_case=" + liste_case, 
					dataType: 'html',                
					success: function()
					{	$(".case_select").each(function(){ 
							ident_case=(this).id.replace("case_", "");
							$(this).removeClass("case_select");
							$(this).removeClass("case_info");	
							case_vide(ident_case);	
												
						});
						case_ini();					
					},
					error: function() {
						console.log("ERREUR AJAX");		
					}
				});			
			}
			
			// ajouts des cases sur le planning suite à une requete ajax
			function ecrire_case(data_case){
				
				if(data_case.date.length){
					var classe="";
					var data_contenu="";
					var class_contenu="";
					switch(data_case["type"]){
						case "0":
							classe="";
							break;
						case "1":
							classe=" action";
							class_contenu=" action-" + data_case["ref_1"];
							data_contenu=" data-action='" + data_case["ref_1"] + "'";
							break;
						case "3":
							classe=" case_info click";
							break;
					}
					
					for(i=0;i<data_case.date.length;i++){
						
						var contenu="";
						var case_id=data_case["date"][i];
						
						case_contenu="<div class='drag" + class_contenu + "' style='" + data_case["style"] + "' id='contenu_" + case_id + "'" + data_contenu + " >";
						case_contenu+=data_case["texte"];
						case_contenu+="</div>";	
						
						if(data_case["type"]==1){
							$("#case_" + case_id).removeClass("click"); 
						}
						$("#case_" + case_id).removeClass("case_select");	// la case n'est plus select
						$("#case_" + case_id).removeClass("libre"); 		// la case n'est plus libre
						$("#case_" + case_id).off();						// on supprime les ecouteurs = interaction possible
						
						$("#case_" + case_id).html(case_contenu);
						$("#case_" + case_id).addClass(classe);
					}
					case_ini();
				}else{
					alert("PAS DE DATE");
				}
			}
			
			// ----	FIN INTERACTION PLANNING ------
			
			//-------------------------------------
			// 		FONCTION INTERCATION ACTION
			//------------------------------------
				// ajouter des dates à une actions de reference
				function ajouter_date_action(action){
					var liste_case=recuperer_case_select();
					if(action>0 && liste_case!=""){
						$.ajax({			
							type:'POST',
							url: 'ajax/ajax_action_date_add.php',
							data : 'action=' + action + "&liste_case=" + liste_case, 
							dataType: 'json',                
							success: function(data)
							{	ecrire_case(data);
								action=0;
								case_ini();							
							},
							error: function() {
								//console.log("ERREUR AJAX");		
							}
						});
					}
				}
				
			// ----	FIN INTERCATION ACTION ------
			
			//-------------------------------------
			// 		FONCTION MODAL
			//------------------------------------
			
				// GENERIQUE
				
			// affiche les champs selon le type d'adresse
			function afficher_type_adresse(){	
				console.log("afficher_type_adresse()");
				switch($('input[name=' + prefixe_modal + 'adr_ref]:checked').val()){
					case "1":
						// client
						$(".cont-adr-client").show();
						$(".cont-adresse").show();
						break;
					case "2":
						// agence
						$(".cont-adr-client").hide();
						$(".cont-adresse").hide();
						break;
						
					default:
						// autre
						$(".cont-adr-client").hide();
						$(".cont-adresse").show();
						afficher_adresse(0);
						break;
				} 
			}
			
			
			// renseigne les champ lié à une adresse
			function afficher_adresse(adresse_id){
				console.log("F - afficher_adresse(" + adresse_id + ")");
				console.log("json_adresses " + json_adresses);
				if(adresse!=null){
					// securite contre les maj select2 data: qui déclenche un .change
					if(adresse!=0){
						$("#adr_nom").val(tab_adresse[adresse][0]);
						$("#adr_service").val(tab_adresse[adresse][1]);
						$("#adr_ad1").val(tab_adresse[adresse][2]);
						$("#adr_ad2").val(tab_adresse[adresse][3]);
						$("#adr_ad3").val(tab_adresse[adresse][4]);
						$("#adr_cp").val(tab_adresse[adresse][5]);
						$("#adr_ville").val(tab_adresse[adresse][6]);					
					}else{
						console.log("VIDER");
						$(".champ_adresse").val("");
					}
				}
			}
			
			// MODAL ACTION
			
			// initialise les champ du modal selon le context et affiche		
			function afficher_modal_action(){
				console.log("afficher_modal_action()");
				prefixe_modal="mdl_act_"; // attention ancien code pas complètement générique
				action=0;
				if(contenu_click_drt.data("action")){
					action=contenu_click_drt.data("action");
				}
				if(action>0){
					// ajout de client
					$("#modal_action .modal-dialog").removeClass("modal-lg");
					$("#modal_act_col_1").hide();
					$("#modal_act_col_2").removeClass("col-md-6");
					$("#modal_act_col_2").addClass("col-md-12");
					$("#modal_act_vehicule").hide();
					$("#modal_act_session").hide();
					$("#modal_action .modal-title").html("Nouveau client");											
				}else{
					// nouvelle action
					$("#modal_action .modal-dialog").addClass("modal-lg");
					$("#modal_act_col_1").show();
					$("#modal_act_col_2").removeClass("col-md-12");
					$("#modal_act_col_2").addClass("col-md-6");
					$("#modal_act_vehicule").show();
					$("#modal_act_session").show();
					$("#modal_action .modal-title").html("Nouvelle action");
				}
				if($("#Sclient").val()>0){
					actualiser_produit_client();
				}
				
				$(".context-menu").hide();
				$('#modal_action').modal('show');					
				
			}
			
			// l'utilisateur selectionne un client
			function actualiser_client(client){				
				$.ajax({			
					type:'GET',
					url: 'ajax/ajax_client_get.php',
					data : 'client=' + client, 
					dataType: 'json',                
					success: function(data){
						$("#commercial").select2("val",data.cli_commercial);
						if(action==0){						
							actualiser_adresse_client(data.adresses);
						/*	if(data.adresses){
								if($('input[name=adr_ref]:checked').val()==1){
									// intervention chez le client
									$("#adr_ref_id").select2("val",data.adresses[0].id);	
									afficher_adresse(data.adresses[0].id);
								}
							}*/
							actualiser_contact_client(data.contacts);					
						}
						actualiser_produit_client();
					},
					error: function() {
						$("#commercial").select2("val",0);
					}
				});	
			}
			
			// actualise la liste des adresses d'interventions disponibles 			
			var tab_adresse=new Array();
			function actualiser_adresse_client(adresses){
				$("#adr_ref_id option[value!='0']").remove();
				if(adresses){				
					tab_adresse=new Array();
					for(i=0;i<=adresses.length-1;i++){
						tab_adresse[adresses[i].id]=new Array(6);
						tab_adresse[adresses[i].id][0]=adresses[i].text;
						tab_adresse[adresses[i].id][1]=adresses[i].service;
						tab_adresse[adresses[i].id][2]=adresses[i].ad1;
						tab_adresse[adresses[i].id][3]=adresses[i].ad2;
						tab_adresse[adresses[i].id][4]=adresses[i].ad3;
						tab_adresse[adresses[i].id][5]=adresses[i].cp;
						tab_adresse[adresses[i].id][6]=adresses[i].ville;
					}
					// place apres le tab_ car déclenche un $("#adr_ref_id").change 
					$("#adr_ref_id").select2({
						data: adresses				
					})
					if($('input[name=adr_ref]:checked').val()==1){
						// adresse du client
						$("#adr_ref_id").val(adresses[0].id);
						$("#adr_ref_id").val(adresses[0].id).trigger("change");
						afficher_adresse(adresses[0].id);
					}					
				
				}			
			};
						
			
			
			
			
			// ACTUALISE LA LISTE DES CONTACTS
			var tab_contact=new Array();
			function actualiser_contact_client(contacts){
				tab_contact=new Array();
				$("#contact option[value!='0']").remove();
				if(contacts){
					$("#contact").select2({
						data: contacts		
					})
					for(i=0;i<=contacts.length-1;i++){
						tab_contact[contacts[i].id]=new Array(6);
						tab_contact[contacts[i].id][0]=contacts[i].text;
						tab_contact[contacts[i].id][1]=contacts[i].prenom;
						tab_contact[contacts[i].id][2]=contacts[i].tel;
						tab_contact[contacts[i].id][3]=contacts[i].portable;
					}
					if(!$("#contact_autre").is(':checked')){
						// contact par defaut du client
						$("#contact").val(contacts[0].id).trigger("change");
						afficher_contact(contacts[0].id);
					}					
				
				}			
			};
			
			// affiche les données d'un contact
			function afficher_contact(contact){
				console.log("contact id : " + contact);
				if(contact!=0 && contact!=null && tab_contact.length>0){
					$("#contact_autre").prop("checked",false);
					$("#con_nom").val(tab_contact[contact][0]);
					$("#con_prenom").val(tab_contact[contact][1]);
					$("#con_tel").val(tab_contact[contact][2]);
					$("#con_portable").val(tab_contact[contact][3]);					
				}else{
					$(".champ-contact").val("");
				}
			}
				
			// actualise la liste des produits pouvent etre utiliser dans l'action
			function actualiser_produit_client(){
				
				
				var client=$("#client").select2("val");
				var type= $('input[name=produit_type]:checked').val();

				$("#produit option[value!='0']").remove();
				$.ajax({			
					type:'GET',
					url: 'ajax/ajax_produit.php',
					data : 'client=' + client + "&type=" + type + "&action=" + action, 
					dataType: 'json',                
					success: function(data)
					{	$("#produit").select2({
							data: data		
						})					
					},
					error: function() {
						
					}
				});
			}	
			
			// actualiser les données selon le produit selection 
			function actualiser_produit(){
				var produit=$("#produit").val();
				var type= $('input[name=produit_type]:checked').val();
				var client=$("#client").val();
				
				if(produit>0){
					$.ajax({			
						type:'POST',
						url: 'ajax/ajax_produit_get.php',
						data : 'produit=' + produit + "&type=" + type, 
						dataType: 'json',                
						success: function(data)
						{	if(type==1){
									// intra
								var ca_demi_j=data.ht_intra/data.nb_demi_jour;
								$("#ca_intra").val(ca_demi_j*nb_demi_j_select);
							}else{
									// inter
								$("#ca_inter").val(data.ht_inter);
							}
							if(data.nb_demi_jour>1){
								$("#nb_session").val(1);
								$("#nb_session").prop("disabled",true);
								afficher_sessions(1);
							}else{
								$("#nb_session").prop("disabled",false);
							}
						},
						error: function() {
							
						}
					});
				}else{
					$("#ca_inter").val(0);
					$("#ca_intra").val(0);
				}
			}
		
			// affiche les zones de saisie en fonction du nombre de session
			function afficher_sessions(nb_session){
				for(i=1;i<=4;i++){
					if(i<=nb_session){
						$("#session_" + i).show();
					}else{
						$("#session_" + i).hide();
					}
				}			
			}
		
			function enregistrer_action(e){
				console.log("enregistrer_action()");
				var $this = e; // L'objet jQuery du formulaire
				
				var liste_case=recuperer_case_select();
				var client = $('#client').val();
				var produit = $('#produit').val();
				
				console.log("client : " + client);
				console.log("produit : " + produit);
				
				if( liste_case === '' || client === 0 || produit === 0) {
					alert('Les champs doivent êtres remplis');
				}else{
					$('#liste_case').val(liste_case);
					$('#action').val(0);
					$.ajax({
						url: "ajax/ajax_action_cree.php", // Le nom du fichier indiqué dans le formulaire
						type: $this.attr('method'), // La méthode indiquée dans le formulaire (get ou post)
						data: $this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
						dataType: 'json',
						success: function(json) { // Je récupère la réponse du fichier PHP
							if(json.erreur==0){
								ecrire_case(json);								
								$('#modal_action').modal('hide');
							}
						}
					});
				}
			}
			
			function enregistrer_action_client(e){
				
				console.log("enregistrer_action_client()");
				var $this = e; // L'objet jQuery du formulaire

				console.log("this :" . $this);
				
				var client = $('#client').select2("val");
				var produit = $('#produit').select2("val");
				console.log($('#produit'));
				console.log("client : " + client);
				console.log("produit : " + produit);
				console.log("AJAX");

				if( client === 0 || produit === 0) {
					alert('Les champs doivent êtres remplis');
				}else{
					$('#action').val(action);
					$.ajax({
						url: "ajax/ajax_action_client.php", // Le nom du fichier indiqué dans le formulaire
						type: $this.attr('method'), // La méthode indiquée dans le formulaire (get ou post)
						data: $this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
						dataType: 'html',
						success: function(html) { // Je récupère la réponse du fichier PHP											
							$('#modal_action').modal('hide');
							action=0; // ffin d'opération lié a une action => action=0 permet de débloqué certaine interaction planning
						},
						error: function() {
							//console.log("ERREUR");
						}
					});
				}
			}
			//----- FONCTION MODAL ACTION --------------
			
			//-------------------------------------
			// 		FONCTION MODAL CLIENT
			//------------------------------------
			
			// affiche tous les clients qui participe à une action 
			function afficher_client_action(action){
				$.ajax({			
					type:'POST',
					url: 'ajax/ajax_action_get_clients.php',
					data : 'action=' + action, 
					dataType: 'json',                
					success: function(data){
						var contenu="";
						var champ="";
						var nb_client=0;
						$.each(data, function(key, val){
							champ="";
							champ="<div class='row' >";
								champ+="<div class='col-md-1' >";
									if(val.intervention==1){
										champ+="<div class='section text-center pt5' >";
											champ+="<i class='fa fa-map-marker' data-toggle='tooltip' data-placement='top' title='Site d&apos;intervention' ></i>";
										champ+="</div>";											
									}else{
										nb_client++;
										champ+="<div class='section' >";
											champ+="<label class='option' >";
												champ+="<input type='checkbox' id='mdl_cli_client_num_" + nb_client + "' name='mdl_cli_client_num_" + nb_client + "' value='" + val.action_client + "' class='checkbox-client' />";
												champ+="<span class='checkbox' ></span>";
											champ+="</label>";
											champ+="<input type='hidden' id='mdl_cli_client_" + nb_client + "' name='mdl_cli_client_" + nb_client + "' value='" + val.client + "' />";
										champ+="</div>";
										
									}
								champ+="</div>";
								champ+="<div class='col-md-1 pt5' >";
									champ+=val.action_client;
								champ+="</div>";
								champ+="<div class='col-md-2 pt5' >";
									champ+=val.code;
								champ+="</div>";
								champ+="<div class='col-md-5 pt5' >";
									champ+=val.nom;
								champ+="</div>";
								champ+="<div class='col-md-3 pt5' >";
									champ+=val.pro_reference;
								champ+="</div>";
								
							champ+="</div>";
							contenu+=champ;							
						});            
						$("#mdl_cli_input").html(contenu);						
						$('#mdl_cli_nb_client').val(nb_client);
						$('#mdl_cli_action').val(action);
						
						// selection ou deselection des clients inscrits à une action
						// place ici car les element sont ajouter apres doc.ready
						$(".checkbox-client").on("click",function(){
							actualiser_modal_client();
						});
						$('#modal_client').modal('show');	
					},
					error: function() {
					
					}
				});	
				
			}
			// mise a jour des données de #modal_client en fonction des clients selectionne
			
			var prefixe_modal="";
			function actualiser_modal_client(){				
				
				prefixe_modal="mdl_cli_";	
				json_adresses="";
				
				var client_checked=recuperer_client_check();
				if(client_checked!=""){
					// on recherche les adresses ouvant servir de lieu d'inter
					get_adresses(client_checked,1,1);
				}else{
					actualiser_adresses();
				}
			}
			
			// retourne les id des clients select sous form de liste
			function recuperer_client_check(){
				//console.log("recuperer_client_check()");
				var nb_client=$("#mdl_cli_nb_client").val();
				var client_client_check="";
				
				//console.log("nb_client : " + nb_client);
				for(i=1;i<=nb_client;i++){
					if($("#mdl_cli_client_num_" + i).is(":checked")){
						//console.log(i + "checked");
						client_client_check=client_client_check + $("#mdl_cli_client_" + i).val();
					}
				}
				//console.log("return client_client_check : " + client_client_check);
				return client_client_check;
			}
			
			function transferer_client(){
			/*	$.ajax({
						/*url: "ajax/ajax_action_client.php", // Le nom du fichier indiqué dans le formulaire
						type: $this.attr('method'), // La méthode indiquée dans le formulaire (get ou post)
						data: $this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
						dataType: 'html',
						success: function(html) { // Je récupère la réponse du fichier PHP											
							$('#modal_action').modal('hide');
							action=0; // ffin d'opération lié a une action => action=0 permet de débloqué certaine interaction planning
						},
						error: function() {
							//console.log("ERREUR");
						}
					});
				}*/
			}
			//----- FONCTION MODAL ACTION --------------
			
			
			
			
		</script>	
	</body>
</html>