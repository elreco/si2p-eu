<?php

$menu_actif = 1;

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_fct.php');
include('includes/connexion_soc.php');

//include('modeles/mod_contact.php');
//include('modeles/mod_adresse.php');
//include('modeles/mod_societe.php');
//include('modeles/mod_agence.php');
//include('modeles/mod_utilisateur.php');
include('modeles/mod_parametre.php');
//include('modeles/mod_droit.php');

include('modeles/mod_get_client.php');
include('modeles/mod_get_commerciaux.php');
include "modeles/mod_get_cli_categories.php";
include "modeles/mod_get_cli_classifications.php";
include "modeles/mod_get_activites_secteurs.php";
include "modeles/mod_get_activites_sous_secteurs.php";
require "modeles/mod_clients_get.php";


	// MODIFICATION D'UN CLIENT

	$erreur=0;

	$client=0;
	if(!empty($_GET['client'])){
		$client=intval($_GET['client']);
	}
	if($client==0){
		echo("Erreur : impossible d'afficher la page (err:001)!");
		die();
	}

	// LA PERSONNE CONNECTE

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
		  $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	// DROIT SPECIFIQUE A LA MODIF DE LA FICHE

	$aff_restreint=false; // si true U ne peux pas modifier les éléments code / nom / categorie / organisation groupe
	$edit_categorie=true;
	// Attention si modif de l'ecran mod -> retporter sur enr


	// DONNEE DU CLIENT

	$d_client = get_client($client);
	if(empty($d_client)){
		echo("Erreur : impossible d'afficher la page (err:002)!");
		die();
	}

	// DONNEE DU CLIENT LIE A LA SOC CONSULTER
	// ON UTILISE LA BASE 0 pour jointure agence

	$sql="SELECT cso_agence,cso_commercial,cso_rib,cso_rib_change,cso_archive,age_nom FROM Clients_Societes LEFT OUTER JOIN Agences ON (Clients_Societes.cso_agence=Agences.age_id)
	WHERE cso_societe=" . $acc_societe . " AND  cso_client=" . $client;
	if($acc_agence>0){
		$sql.=" AND cso_agence = " . $acc_agence;
	}
	if(!$_SESSION["acces"]["acc_droits"][6]){
		$sql.=" AND cso_utilisateur=" . $acc_utilisateur;
	}
	$req=$Conn->query($sql);
	$d_client_societe=$req->fetchAll();
	if(empty($d_client_societe)){
		echo("Erreur : impossible d'afficher la page (err:003)!");
		die();
	}

	// categorie du client
	$req = $Conn->prepare("SELECT cca_gc,cca_libelle FROM Clients_Categories WHERE cca_id=" . $d_client["cli_categorie"] . ";");
	$req->execute();
	$d_categorie = $req->fetch();
	if(!empty($d_categorie)){

		if($d_client["cli_categorie"]==2){
			// GC
			if(!$_SESSION['acces']["acc_droits"][8] AND $_SESSION['acces']["acc_service"][1]!=1){
				$aff_restreint=true;
				$edit_categorie=false;
			}elseif($_SESSION['acces']["acc_service"][1]!=1){
				$edit_categorie=false;
			}
		}elseif($d_client["cli_categorie"]==5){
			// NATIONAL
			if($_SESSION['acces']["acc_service"][1]!=1){
				$edit_categorie=false;
				$aff_restreint=true;
			}
		}
	}
	if($edit_categorie){
		// U a accès on contruit la liste des categorie
		$sql="SELECT cca_id,cca_libelle,cca_gc FROM Clients_Categories ORDER BY cca_libelle;";
		$req = $Conn->prepare($sql);
		$req->execute();
		$d_cli_categories = $req->fetchAll();
	}

	// SOUS- CATEGORIE
	if(isset($d_cli_categories) OR ($d_client["cli_categorie"]==2 AND $_SESSION['acces']["acc_droits"][8]) ){
		// l'uti a acces au categorie donc au sous-categorie
		$req = $Conn->prepare("SELECT csc_id,csc_libelle FROM Clients_Sous_Categories WHERE csc_categorie=" . $d_client["cli_categorie"] . ";");
		$req->execute();
		$d_cli_sous_categories = $req->fetchAll();
	}elseif(!empty($d_client["cli_sous_categorie"])){
		// U n'a pas accès on affihce juste le libelle de la sous categorie
		$req = $Conn->prepare("SELECT csc_id,csc_libelle FROM Clients_Sous_Categories WHERE csc_id=" . $d_client["cli_sous_categorie"] . ";");
		$req->execute();
		$d_sous_categorie = $req->fetch();
	}else{
		$d_sous_categorie["csc_libelle"]="";
	}

	// LES COMMERCIAUX POUVANT ETRE UTILISE
	$commerciaux=array();
	$sql="SELECT com_id,com_agence,com_label_1,com_label_2 FROM Commerciaux WHERE NOT com_archive";
	if($acc_agence>0){
		$sql.=" AND com_agence=" . $acc_agence;
	}
	if(!$_SESSION["acces"]["acc_droits"][6]){
		// base client
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
	}
	$sql.=" ORDER BY com_label_1,com_label_2";
	$req = $ConnSoc->query($sql);
	$result=$req->fetchAll();
	if(!empty($result)){
		foreach($result as $r){
			$commerciaux[$r["com_agence"]][]=$r;
		}
	}

	// FINANCEUR

	// type de financeur
	$req = $Conn->prepare("SELECT csc_id,csc_libelle FROM Clients_Sous_Categories WHERE csc_categorie=4 ORDER BY csc_libelle;");
	$req->execute();
	$d_opca_type= $req->fetchAll();

	// liste des OPCA
	if($d_client["cli_categorie"]!=4){
		if(!empty($d_client["cli_opca_type"])){
			$d_opca=get_clients(4,$d_client["cli_opca_type"],0,null);
		}
	}



	// RIB
	if($_SESSION['acces']["acc_droits"][9]){

		$sql="SELECT rib_id,rib_nom FROM Rib WHERE rib_societe=" . $acc_societe. ";";
		$req = $Conn->query($sql);
		$d_rib = $req->fetchAll();
	}

	// Classification

	$d_classifications=get_cli_classifications();

	if(!empty($d_client["cli_classification"])){
		$sql="SELECT ccc_id,ccc_libelle FROM Clients_classifications_categories WHERE ccc_classification=" . $d_client["cli_classification"] . ";";
		$req=$Conn->query($sql);
		$d_classification_categorie=$req->fetchAll();
	}

	if(!empty($d_client["cli_classification"])){
		$sql="SELECT cct_id,cct_libelle FROM Clients_classifications_types WHERE cct_classification=" . $d_client["cli_classification"] . ";";
		$req=$Conn->query($sql);
		$d_classification_type=$req->fetchAll();
	}

	// CODE APE
	$sql="SELECT * FROM Ape ORDER BY ape_code,ape_libelle";
	$req = $Conn->query($sql);
	$d_apes = $req->fetchAll();

	// PRESCRIPTEURS

	$prescripteur_verrou=false;
	$d_prescripteur=array(
		"cpr_ouvert" => 0,
		"cpr_libelle " => ""
	);
	if(!empty($d_client["cli_prescripteur"])){
		$sql="SELECT cpr_libelle,cpr_import,cpr_ouvert FROM Clients_Prescripteurs WHERE cpr_id=" . $d_client["cli_prescripteur"] . ";";
		$req=$Conn->query($sql);
		$d_prescripteur=$req->fetch();
		if(!empty($d_prescripteur)){
			if($d_prescripteur["cpr_import"]==1){
				$prescripteur_verrou=true;
			}
		}else{
			$d_prescripteur=array(
				"cpr_ouvert" => 0,
				"cpr_libelle " => ""
			);
		}
	}


	if(!$prescripteur_verrou){
		$sql="SELECT cpr_id,cpr_libelle,cpr_ouvert FROM Clients_Prescripteurs WHERE NOT cpr_archive AND NOT cpr_import;";
		$req=$Conn->query($sql);
		$d_prescripteurs=$req->fetchAll();
	}



	// DONNE STAGIAIRE POUR LES PARTICULIERS

	$sta_naissance="";
	if($d_client["cli_categorie"]==3 AND !empty($d_client["cli_stagiaire"])){
		$req = $Conn->prepare("SELECT DATE_FORMAT(sta_naissance,'%d/%m/%Y') AS con_naissance,sta_ville_naiss,sta_cp_naiss FROM Stagiaires WHERE sta_id=" . $d_client["cli_stagiaire"] . ";");
		$req->execute();
		$d_stagiaire = $req->fetch();
		$sta_naissance=$d_stagiaire["con_naissance"];
	}


	// INITIALISATION DE L'AFFICHAGE
	if($d_client["cli_categorie"]!=3){
		$bloc_no_particulier="";
	}else{
		$bloc_no_particulier="style=display:none;'";
	}


	// DONNEE GROUPE

	if($d_client['cli_groupe']==1){
		if($d_client['cli_filiale_de']>0){
			// SUR LA MM
			$req = $Conn->prepare("SELECT cli_id, cli_nom, cli_code FROM clients WHERE cli_id = :id");
			$req->bindParam(':id', $d_client['cli_filiale_de']);
			$req->execute();
			$maison_mere = $req->fetch();
		}

		// on verifie la presence de filiale
		// une fiche avec des filiales doit rester un groupe
		$req = $Conn->prepare("SELECT cli_id FROM clients WHERE cli_fil_de = :id");
		$req->bindParam(':id', $client);
		$req->execute();
		$d_client_fil = $req->fetch();
	}

	// Adresse d'intervention par defaut

	$sql="SELECT * FROM Adresses WHERE adr_ref_id=" . $client . " AND adr_defaut AND adr_type=1;";
	$req = $Conn->query($sql);
	$adresse_inter=$req->fetch();

	// Adresse de facturation par defaut

	$sql="SELECT * FROM Adresses WHERE adr_ref_id=" . $client . " AND adr_defaut AND adr_type=2;";
	$req = $Conn->query($sql);
	$adresse_fac=$req->fetch();
	if(empty($adresse_fac)){
		$adresse_fac=array(
			"adr_id" => 0,
			"adr_libelle" => "",
			"adr_nom" => "",
			"adr_service" => "",
			"adr_ad1" => "",
			"adr_ad2" => "",
			"adr_ad3" => "",
			"adr_cp" => "",
			"adr_ville" => "",
			"adr_geo" => 0
		);
	}

	// CONTACT PAR DEFAUT

	$verrou_impaye=false;

	if(!empty($d_client["cli_contact"])){
		$sql_con="SELECT * FROM Contacts WHERE con_id=" . $d_client["cli_contact"] . ";";
		$req = $Conn->query($sql_con);
		$contact = $req->fetch();
		if(!empty($contact)){

			if($contact["con_compta"]){

				// IL S'AGIT D'UN CONTACT IMPAYE -> il en faut au moins  par fiche
				$sql_con="SELECT con_id FROM Contacts WHERE con_ref=1 AND con_ref_id=" . $client . " AND NOT con_id=" .$d_client["cli_contact"] . " AND con_compta;";
				$req = $Conn->query($sql_con);
				$contacts_compta = $req->fetchAll();
				if(empty($contacts_compta)){
					$verrou_impaye=true;
				}

			}
		}else{
			$verrou_impaye=true;
		}
	}else{
		$contact=array(
			"con_id" => 0,
			"con_fonction" => 0,
			"con_fonction_nom" => "",
			"con_titre" => "",
			"con_nom" => "",
			"con_prenom" => "",
			"con_tel" => "",
			"con_fax" => "",
			"con_portable" => "",
			"con_mail" => "",
			"con_compta" => 0
		);
	}

	// CONTACT FONCTION

	$sql="SELECT * FROM Contacts_Fonctions ORDER BY cfo_libelle;";
	$req = $Conn->query($sql);
	$d_contact_fct = $req->fetchAll();


	// SI COMPTA

	if($_SESSION['acces']["acc_droits"][9]){

		$sql="SELECT soc_id,soc_nom FROM Societes ORDER BY soc_nom;";
		$req=$Conn->query($sql);
		$d_societes=$req->fetchAll();

		if(!empty($d_client["cli_interco_soc"])){
			$sql="SELECT age_id,age_nom FROM Agences WHERE age_societe=:age_societe ORDER BY age_nom;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":age_societe",$d_client["cli_interco_soc"]);
			$req->execute();
			$d_agences=$req->fetchAll();
		}


	}



?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - <?= $d_client['cli_nom']; ?></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

		<style type="text/css" >
			.content-header {
				margin-bottom: 8px !important;
			}

			.ui-priority-secondary {
				display: none !important;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm ">
		 <form method="post" action="client_mod_enr.php" id="form_client" >
			<div>
				<input type="hidden" name="client" value="<?=$d_client['cli_id']?>" />
				<input type="hidden" name="adr_fac" id="adresse_fac" value="<?=$adresse_fac['adr_id']?>" />
				<?php if(!empty($adresse_inter)){ ?>
				<input type="hidden" name="adr_int" value="<?=$adresse_inter['adr_id']?>" />
			<?php }?>
				<input type="hidden" name="contact" value="<?=$contact['con_id']?>" />
			</div>

			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" class="">
					<section id="content" class="">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-info ">
									<div class="panel heading-border panel-info">
										<div class="panel-body bg-light">
											<div class="content-header text-center">
												<h2>Modification du client <b class="text-info"><?= $d_client['cli_nom'] ?></b></h2>
											</div>
					  <?php 				if(isset($_GET['error']) && $_GET['error'] == 3): ?>
												<div class="col-md-12">
													<div class="alert alert-danger">
														Le Siret existe déjà dans notre base de données.
													</div>
												</div>
					  <?php 				endif; ?>
											<div class="col-md-10 col-md-offset-1">

												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Identité</span>
														</div>
													</div>
												</div>

												<!-- CODE NOM -->
									<?php		if(!$aff_restreint){ ?>
													<div class="row">
														<div class="col-md-4">
															<div class="section">
																<label for="cli_code" >Code client :</label>
																<div class="field prepend-icon">
																	<input type="text" id="cli_code" name="cli_code" class="gui-input cli_code nom" placeholder="Code" required value="<?= $d_client['cli_code'] ?>">
																	<span class="field-icon">
																		<i class="fa fa-code"></i>
																	</span>
																</div>
															</div>
														</div>
														<div class="col-md-8">
															<div class="section">
																<label for="cli_reference" >Nom :</label>
																<div class="field prepend-icon">
																	<input type="text" name="cli_nom" required class="gui-input" placeholder="Nom" value="<?= $d_client['cli_nom'] ?>">
																	<span class="field-icon">
																		<i class="fa fa-building-o"></i>
																	</span>
																</div>
															</div>
														</div>
													</div>
									<?php		}else{ ?>
													<div class="row">
														<div class="col-md-4">Code client : <b><?=$d_client['cli_code'] ?></b></div>
														<div class="col-md-8">Nom : <b><?=$d_client['cli_nom']?></b></div>
									<?php 		} ?>

												<!-- COMMERCIAUX -->
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>
													<?php		if(count($d_client_societe)>1){
																	echo("Commerciaux");
																}else{
																	echo("Commercial");
																} ?>
															</span>
														</div>
													</div>
												</div>
									<?php 		foreach($d_client_societe as $d_cli_soc){
													if(!empty($commerciaux[$d_cli_soc["cso_agence"]])){ ?>
														<div class="row">
													<?php 	if(!empty($d_cli_soc["age_nom"])){ ?>
																<div class="col-md-4 pt25">
																	Agence : <b><?=$d_cli_soc["age_nom"]?></b>
																	<input type="hidden" name="cso_agence_<?=$d_cli_soc["cso_agence"]?>" value="cso_agence_<?=$d_cli_soc["cso_agence"]?>" />
																</div>
													<?php	}?>
															<div class="col-md-4">
																<div class="section">
																	<label for="cso_commercial_<?=$d_cli_soc["cso_agence"]?>" >Commercial :</label>
																	<select name="cso_commercial_<?=$d_cli_soc["cso_agence"]?>" id="cso_commercial_<?=$d_cli_soc["cso_agence"]?>" class="select2" >
																		<option value="0">Commercial...</option>
										<?php 							foreach($commerciaux[$d_cli_soc["cso_agence"]] as $com){
																			if($com["com_id"]==$d_cli_soc['cso_commercial']){
																				echo("<option value='" . $com["com_id"] . "' selected >" . $com["com_label_1"] . " "  . $com["com_label_2"] . "</option>");
																			}else{
																				echo("<option value='" . $com["com_id"] . "' >" . $com["com_label_1"] . " "  . $com["com_label_2"] . "</option>");
																			}
																		} ?>
																	</select>
																</div>
															</div>
															<div class="col-md-4 pt20 text-center">
																<div class="option-group field">
																	<label class="option option-dark">
																		<input type="checkbox" name="cso_archive_<?=$d_cli_soc["cso_agence"]?>" value="cso_archive_<?=$d_cli_soc["cso_agence"]?>" <?php if(!empty($d_cli_soc["cso_archive"])) echo("checked"); ?> >
																		<span class="checkbox"></span>Archivé
																	</label>
																</div>
															</div>
														</div>
									<?php			}
												} ?>


									<?php		if(!$aff_restreint){ ?>
													<!-- CATEGORIE / SOUS CATEGORIE -->
													<div class="row">
														<div class="col-md-12">
															<div class="section-divider mb40">
																<span>Catégorie</span>
															</div>
														</div>
													</div>
													<div class="row"  >
														<div class="col-md-6">
															<div class="section">
														<?php	if(isset($d_cli_categories)){ ?>
																	<label for="cli_categorie" >Catégorie :</label>
																	<select id="cli_categorie" name="cli_categorie" class="select2">
															<?php		if(!empty($d_cli_categories)){
																			foreach($d_cli_categories as $cc){
																				if($cc["cca_id"]==$d_client['cli_categorie']){
																					echo("<option value='" . $cc["cca_id"] . "' selected >" . $cc["cca_libelle"] . "</option>");
																				}else{
																					echo("<option value='" . $cc["cca_id"] . "' >" . $cc["cca_libelle"] . "</option>");
																				}
																			}
																		} ?>
																	</select>
														<?php	}else{ ?>
																	<input type="hidden" id="cli_categorie" name="cli_categorie" value="<?= $d_client['cli_categorie'] ?>">
														<?php		echo("Catégorie : <b>" . $d_categorie["cca_libelle"] . "</b>");
																} ?>
															</div>
														</div>

												<?php	if(isset($d_cli_sous_categories)){ ?>
															<div class="col-md-6" id="sous_categorie_bloc" <?php if(empty($d_cli_sous_categories)) echo("style='display:none;'"); ?> >
																<div class="section">
																	<label for="cli_sous_categorie" >Sous-catégorie :</label>
																	<select id="cli_sous_categorie" name="cli_sous_categorie" class="select2" >
																		<option value="0">Sélectionner une sous catégorie...</option>
															<?php		if(!empty($d_cli_sous_categories)){
																			foreach($d_cli_sous_categories as $cc){
																				if($cc["csc_id"]==$d_client['cli_sous_categorie']){
																					echo("<option value='" . $cc["csc_id"] . "' selected >" . $cc["csc_libelle"] . "</option>");
																				}else{
																					echo("<option value='" . $cc["csc_id"] . "' >" . $cc["csc_libelle"] . "</option>");
																				}
																			}
																		} ?>
																	</select>
																</div>
															</div>
												<?php	}else{ ?>
															<div class="col-md-6">
																<div class="section">
																	<input type="hidden" id="cli_sous_categorie" name="cli_sous_categorie" value="<?= $d_client['cli_sous_categorie'] ?>">
																	<?=$d_sous_categorie["csc_libelle"]?>
																</div>
															</div>
												<?php	} ?>
													</div>
													<!-- DONNEE GC -->
											<?php	if($_SESSION['acces']["acc_droits"][8]){ ?>
														<div id="bloc_gc" <?php if($d_client["cli_categorie"]!=2) echo("style='display:none;'"); ?> >
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Données GC</span>
																	</div>
																</div>
															</div>
															<div class="col-md-6" >
																<div class="section text-center">
																	<label class="option option-info">
																		<input type="checkbox" name="cli_contrat" value="on" <?php if($d_client['cli_contrat'] == 1) echo("checked") ?> />
																		<span class="checkbox"></span>
																		<label for="cli_contrat">Contrat validé</label>
																	</label>
																</div>
															</div>
														</div>
											<?php	} ?>
													<!-- GROUPE -->
													<div id="bloc_groupe" <?php if($d_client['cli_categorie']==3) echo("style='display:none;'"); ?> >
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Groupe et Filiale </span>
																</div>
															</div>
														</div>
														<div class="row" >
															<div class="col-md-6 text-center">
													<?php		if($d_client["cli_groupe"]==1 AND !empty($d_client_fil)){ ?>
																	<p>
																		Groupe : <b>OUI</b><br/>
																		<input type="hidden" name="cli_groupe" id="cli_groupe" value="oui" />
																	</p>
													<?php		}else{ ?>
																	<div class="section">
																		<label class="option option-info">
																			<input type="checkbox" name="cli_groupe" id="cli_groupe" value="oui" <?php if($d_client['cli_groupe'] == 1) echo("checked"); ?> />
																			<span class="checkbox"></span>
																			<label for="cli_groupe">Groupe</label>
																		</label>
																	</div>
													<?php		} ?>
															</div>
															<div class="col-md-6 maisonm" <?php if($d_client['cli_groupe'] == 0) echo("style='display:none;'"); ?> >
																<div class="row" >
																	<div class="col-md-6">
																		<div class="section">
																			<label for="cli_maison_mere" class=" mt15 option option-info">
																				<input type="radio" id="cli_maison_mere" name="cli_maison_mere" value="oui" <?php if($d_client['cli_filiale_de'] == "0") echo("checked"); ?> />
																				<span class="radio"></span> Ce client est la maison mère
																			</label>
																		</div>
																	</div>
																	<div class="col-md-6">
																		<div class="section">
																			<label for="cli_maison_mere_non" class=" mt15 option option-dark">
																				<input type="radio" id="cli_maison_mere_non" name="cli_maison_mere" value="non" <?php if($d_client['cli_filiale_de'] != "0") echo("checked"); ?> />
																				<span class="radio"></span> Ce client est une filiale de :
																			</label>
																		</div>
																	</div>
																</div>

																<div class="row" id="choix_mm" <?php if($d_client['cli_filiale_de']== "0") echo("style='display:none;'"); ?> >
																	<div class="col-md-12">
																		<select name="cli_filiale_de" id="cli_filiale_de" class="select2 select2-client" id="filiale" data-groupe="2" data-region="1" data-client_categorie="<?=$d_client['cli_categorie']?>" data-client_s_categorie="<?=$d_client['cli_sous_categorie']?>" <?php if($d_client['cli_filiale_de'] != "0") echo("required"); ?> >
																<?php		if(!empty($maison_mere)){
																				echo("<option value='" . $maison_mere["cli_id"] . "' selected>" .  $maison_mere["cli_nom"] . "</option>");
																			}else{
																				echo("<option value='' selected>Maison mère...</option>");
																			} ?>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!--FIN GROUPE -->
										<?php	} ?>

												<!--ADRESSES -->
												<div class="row">

													<!--ADRESSE D'INTERVENTION -->
													<div class="col-md-6 bloc-no-particulier"<?=$bloc_no_particulier?> >
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Adresse d'intervention par défaut</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_libelle1" id="adr_libelle1" class="gui-input" placeholder="Libellé"  value="<?=$adresse_inter['adr_libelle']?>">
																		<label for="cli_ad1" class="field-icon">
																			<i class="fa fa-tag"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>

														<div class="row">
														  <div class="col-md-12">
															<div class="section">
															  <div class="field prepend-icon">
																<input type="text" name="adr_nom1" id="adr_nom1" class="gui-input  required-pro" placeholder="Nom" <?php if(!empty($adresse_inter) && $d_client["cli_categorie"]!=3) echo("required"); ?>  value="<?=$adresse_inter['adr_nom']?>">
																<label for="cli_ad1" class="field-icon">
																  <i class="fa fa-tag"></i>
																</label>
															  </div>
															</div>
														  </div>
														  <div class="col-md-12">
															<div class="section">
															  <div class="field prepend-icon">
																<input type="text" name="adr_service1" id="adr_service1" class="gui-input" placeholder="Service"  value="<?=$adresse_inter['adr_service']?>">
																<label for="cli_ad1" class="field-icon">
																  <i class="fa fa-tag"></i>
																</label>
															  </div>
															</div>
														  </div>
														</div>
														<div class="row">
														  <div class="col-md-12">
															<div class="section">
															  <div class="field prepend-icon">
																<input type="text" name="adr_ad11" id="adr_ad11" class="gui-input  required-pro" placeholder="Adresse" <?php if(!empty($adresse_inter) && $d_client["cli_categorie"]!=3) echo("required='required'"); ?> value="<?=$adresse_inter['adr_ad1']?>">
																<label for="cli_ad1" class="field-icon">
																  <i class="fa fa-map-marker"></i>
																</label>
															  </div>
															</div>
														  </div>
														</div>
														<div class="row">
														  <div class="col-md-12">
															<div class="section">
															  <div class="field prepend-icon">
																<input type="text" name="adr_ad21" class="gui-input" id="adr_ad21" placeholder="Adresse (Complément 1)" value="<?=$adresse_inter['adr_ad2']?>">
																<label for="cli_ad2" class="field-icon">
																  <i class="fa fa-map-marker"></i>
																</label>
															  </div>
															</div>
														  </div>
														</div>
														<div class="row">
														  <div class="col-md-12">
															<div class="section">
															  <div class="field prepend-icon">
																<input type="text" name="adr_ad31" id="adr_ad31" class="gui-input" placeholder="Adresse (Complément 2)" value="<?=$adresse_inter['adr_ad3']?>">
																<label for="cli_ad3" class="field-icon">
																  <i class="fa fa-map-marker"></i>
																</label>
															  </div>
															</div>
														  </div>
														</div>
														<div class="row">
														  <div class="col-md-3">
															<div class="section">
															  <div class="field prepend-icon">
																<input type="text" id="adr_cp1" name="adr_cp1" class="gui-input code-postal  required-pro" <?php if(!empty($adresse_inter) && $d_client["cli_categorie"]!=3) echo("required"); ?> placeholder="Code postal"  value="<?=$adresse_inter['adr_cp']?>">
																<label for="cli_cp" class="field-icon">
																  <i class="fa fa fa-certificate"></i>
																</label>
															  </div>
															</div>
														  </div>
														  <div class="col-md-9">
															<div class="section">
															  <div class="field prepend-icon">
																<input type="text" name="adr_ville1" id="adr_ville1" class="gui-input nom  required-pro" placeholder="Ville" <?php if(!empty($adresse_inter) && $d_client["cli_categorie"]!=3) echo("required"); ?>  value="<?=$adresse_inter['adr_ville']?>">
																<label for="cli_ville" class="field-icon">
																  <i class="fa fa fa-building"></i>
																</label>
															  </div>
															</div>
														  </div>
														</div>
														<div class="row text-center" >
															<div class="col-md-12">Zone géographique</div>
														</div>
														<div class="row text-center" >
															<div class="col-md-12">
																<label for="cli_geo11" class=" mt15 option option-info">
																	<input type="radio" id="cli_geo11" name="cli_geo1" value="1" <?php if($adresse_inter['adr_geo'] == 1) echo("checked"); ?> />
																	<span class="radio"></span> France
																</label>
																<label for="cli_geo21" class=" mt15 option option-info">
																	<input type="radio" id="cli_geo21" name="cli_geo1" value="2" <?php if($adresse_inter['adr_geo'] == 2) echo("checked"); ?> >
																	<span class="radio"></span> UE
																</label>
																<label for="cli_geo31" class=" mt15 option option-info">
																	<input type="radio" id="cli_geo31" name="cli_geo1" value="3" <?php if($adresse_inter['adr_geo'] == 3) echo("checked"); ?> >
																	<span class="radio"></span> Autre
																</label>
															</div>
														</div>
														<div class="row mt15 text-center">
															<div class="col-md-12">
																<label class="option option-info">
																	<input type="checkbox" id="similaire" value="oui">
																	<span class="checkbox"></span>Utiliser comme adresse de facturation
																</label>
															</div>
														</div>
													</div>
													<!--FIN ADRESSE D'INTERVENTION -->

													<!--ADRESSE DE FACTURATION PAR DEFAUT -->
													<div class="col-md-6">

														  <div class="row">
															<div class="col-md-12">
															  <div class="section-divider mb40">
																<span>Adresse de facturation</span>
															  </div>
															</div>
														  </div>
														  <div class="row">
															<div class="col-md-12">
															  <div class="section">
																<div class="field prepend-icon">
																  <input type="text" name="adr_libelle2" id="adr_libelle2" class="gui-input" placeholder="Libellé"  value="<?=$adresse_fac['adr_libelle']?>">
																  <label for="cli_ad1" class="field-icon">
																	<i class="fa fa-tag"></i>
																  </label>
																</div>
															  </div>
															</div>
														  </div>

														  <div class="row">
															<div class="col-md-12">
															  <div class="section">
																<div class="field prepend-icon">
																  <input type="text" name="adr_nom2" id="adr_nom2" class="gui-input" placeholder="Nom" <?php if(!empty($adresse_fac)) echo("required"); ?>  value="<?=$adresse_fac['adr_nom']?>">
																  <label for="adr_nom2" class="field-icon">
																	<i class="fa fa-tag"></i>
																  </label>
																</div>
															  </div>
															</div>
															<div class="col-md-12">
															  <div class="section">
																<div class="field prepend-icon">
																  <input type="text" name="adr_service2" id="adr_service2" class="gui-input" placeholder="Service" value="<?=$adresse_fac['adr_service']?>">
																  <label for="cli_ad1" class="field-icon">
																	<i class="fa fa-tag"></i>
																  </label>
																</div>
															  </div>
															</div>
														  </div>
														  <div class="row">
															<div class="col-md-12">
															  <div class="section">
																<div class="field prepend-icon">
																  <input type="text" name="adr_ad12" id="adr_ad12" class="gui-input" placeholder="Adresse" value="<?=$adresse_fac['adr_ad1']?>" <?php if(!empty($adresse_fac)) echo("required"); ?> >
																  <label for="cli_ad1" class="field-icon">
																	<i class="fa fa-map-marker"></i>
																  </label>
																</div>
															  </div>
															</div>
														  </div>
														  <div class="row">
															<div class="col-md-12">
															  <div class="section">
																<div class="field prepend-icon">
																  <input type="text" name="adr_ad22" class="gui-input" id="adr_ad22" placeholder="Adresse (Complément 1)" value="<?=$adresse_fac['adr_ad2']?>">
																  <label for="cli_ad2" class="field-icon">
																	<i class="fa fa-map-marker"></i>
																  </label>
																</div>
															  </div>
															</div>
														  </div>
														  <div class="row">
															<div class="col-md-12">
															  <div class="section">
																<div class="field prepend-icon">
																  <input type="text" name="adr_ad32" id="adr_ad32" class="gui-input" placeholder="Adresse (Complément 2)" value="<?=$adresse_fac['adr_ad3']?>">
																  <label for="cli_ad3" class="field-icon">
																	<i class="fa fa-map-marker"></i>
																  </label>
																</div>
															  </div>
															</div>
														  </div>
														  <div class="row">
															<div class="col-md-3">
															  <div class="section">
																<div class="field prepend-icon">
																  <input type="text" id="adr_cp2" name="adr_cp2" class="gui-input code-postal" <?php if(!empty($adresse_fac)) echo("required"); ?> placeholder="Code postal"  value="<?=$adresse_fac['adr_cp']?>">
																  <label for="cli_cp" class="field-icon">
																	<i class="fa fa fa-certificate"></i>
																  </label>
																</div>
															  </div>
															</div>
															<div class="col-md-9">
															  <div class="section">
																<div class="field prepend-icon">
																  <input type="text" name="adr_ville2" id="adr_ville2" class="gui-input nom" placeholder="Ville" <?php if(!empty($adresse_fac)) echo("required"); ?>  value="<?=$adresse_fac['adr_ville']?>">
																  <label for="cli_ville" class="field-icon">
																	<i class="fa fa fa-building"></i>
																  </label>
																</div>
															  </div>
															</div>
														</div>
														<div class="row text-center bloc-no-particulier"<?=$bloc_no_particulier?> >
															<div class="col-md-12">Zone géographique</div>
														</div>
														<div class="row text-center bloc-no-particulier"<?=$bloc_no_particulier?> >
															<label for="cli_geo12" class=" mt15 option option-info">
																<input type="radio" id="cli_geo12" name="cli_geo2" value="1" <?php if($adresse_fac['adr_geo'] == 1) echo("checked"); ?> />
																<span class="radio"></span> France
															</label>
															<label for="cli_geo22" class=" mt15 option option-info">
																<input type="radio" id="cli_geo22" name="cli_geo2" value="2" <?php if($adresse_fac['adr_geo'] == 2) echo("checked"); ?> >
																<span class="radio"></span> UE
															</label>
															<label for="cli_geo32" class=" mt15 option option-info">
																<input type="radio" id="cli_geo32" name="cli_geo2" value="3" <?php if($adresse_fac['adr_geo'] == 3) echo("checked"); ?> >
																<span class="radio"></span> Autre
															</label>
														</div>

														<div class="row bloc-no-particulier mt15"<?=$bloc_no_particulier?> >

															<div class="col-md-5">
																<label>SIREN :</label>
																<div class="field prepend-icon">
																	<input type="text" name="cli_siren" id="cli_siren" class="gui-input siren" placeholder="Siren" value="<?=$d_client['cli_siren']?>" <?php if(!empty($d_client['cli_siren'])) echo("required"); ?> >
																	<span class="field-icon">
																		<i class="fa fa-barcode"></i>
																	</span>
																</div>
																<div id="cli_siren_err" ></div>
															</div>
															<div class="col-md-4">
																<label>NIC :</label>
																<div class="field prepend-icon">
															<?php 	if(!empty($adresse_fac['adr_siret'])){ ?>
																		<input type="text" name="adr_siret" id="adr_siret" class="gui-input siret" placeholder="NIC" value="<?=$adresse_fac['adr_siret']?>" required >
															<?php	}else{ ?>
																		<input type="text" name="adr_siret" id="adr_siret" class="gui-input siret" placeholder="NIC" value="" <?php if(empty($d_client['cli_siren'])) echo("disabled"); ?> >
															<?php	} ?>
																	<span class="field-icon">
																		<i class="fa fa-barcode"></i>
																	</span>
																</div>
																<div id="adr_siret_err" ></div>
															</div>
														</div>

														<div class="row bloc-no-particulier mt15"<?=$bloc_no_particulier?> >
															<div class="col-md-9">
																<label>SIRET :</label>
																<div class="field prepend-icon">
																	<input type="text" name="cli_siret" id="cli_siret" class="gui-input siretlong" placeholder="SIRET" value="<?php if(!empty($adresse_fac['adr_siret'])) echo($d_client['cli_siren'] . " " . $adresse_fac['adr_siret']);?>" <?php if(!empty($adresse_fac['adr_siret'])) echo("required"); ?> >
																	<span class="field-icon">
																		<i class="fa fa-barcode"></i>
																	</span>
																</div>
																<div id="cli_siret_err" ></div>
															</div>
															<div class="col-md-3 pt20">
																<div class="section">
																	<button type="button" id="verif_siret" class="btn btn-warning btn-sm">Vérifier le siret</button>
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-md-12" id="siret_ok" style="display:none;">
																<div class="section">
																	<div class="alert alert-success">Le Siret est disponible.</div>
																</div>
															</div>
														</div>
														
											<?php 		if(!empty($_SESSION['acces']['acc_droits'][36])){ ?>
															<div class="row">
																<div class="col-md-12" >
																	<label class="option option-dark">
																		<input type="checkbox" id="siret_no_check" value="no_check" name="siret_no_check">
																		<span class="checkbox"></span>
																		<label for="siret_no_check">Désactiver la vérification Siret</label>
																	</label>
																</div>
															</div>
												<?php 	}	?>
													</div>
												</div>
												<!--FIN ADRESSES -->

												<!-- CONTACT & FACTURATION -->

												<div class="row">

													<!--CONTACT -->
													<div class="col-md-6">

														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
															<?php	if($d_client["cli_categorie"]==3){ ?>
																		<span id="contact_libelle" >Coordonnées du particulier</span>
															<?php	}else{ ?>
																		<span id="contact_libelle" >Contact par défaut du lieu d'intervention</span>
															<?php	} ?>
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-md-4">
																<div class="section">
																	<label for="con_fonction" >Sélectionner une fonction :</label>
																	<div class="field select">
																		<select name="con_fonction" id="con_fonction" >
																			<option value="0">Sélectionner une fonction...</option>
																<?php		if(!empty($d_contact_fct)){
																				foreach($d_contact_fct as $df){
																					if($df["cfo_id"]==$contact['con_fonction']){
																						echo("<option value='" . $df["cfo_id"] . "' selected >" . $df["cfo_libelle"] . "</option>");
																					}else{
																						echo("<option value='" . $df["cfo_id"] . "' >" . $df["cfo_libelle"] . "</option>");
																					}
																				}
																			} ?>
																			<option value="autre" <?php if(!empty($contact['con_fonction_nom'])) echo("selected"); ?> >Autre fonction...</option>
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
															<div class="col-md-8 con_fonction_nom_style" <?php if(empty($contact['con_fonction_nom'])) echo("style='display:none;'"); ?> >
																<div class="section">
																	<label for="con_fonction_nom" >Fonction :</label>
																	<div class="field prepend-icon" >
																		<input type="text" name="con_fonction_nom" id="con_fonction_nom" class="gui-input" placeholder="Autre fonction"  value="<?= $contact['con_fonction_nom'] ?>">
																		<span class="field-icon">
																			<i class="fa fa-tag"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<div class="field select">
																		<select name="con_titre" id="con_titre" >
																		  <option value="0">Civilité...</option>
																		  <?php foreach($base_civilite as $b => $c): ?>
																			<?php if($b > 0): ?>
																			  <option value="<?= $b ?>" <?php if($b == $contact['con_titre']): ?>selected<?php endif; ?>><?= $c ?></option>
																			<?php endif; ?>
																		  <?php endforeach; ?>


																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<label for="con_nom" >Nom :</label>
																	<div class="field prepend-icon">
																		<input type="text" name="con_nom" id="con_nom" class="gui-input nom" placeholder="Nom" value="<?=$contact['con_nom']?>" <?php if(!empty($contact['con_nom'])) echo("required"); ?> />
																		<span class="field-icon">
																			<i class="fa fa-user"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label for="con_prenom" >Prénom :</label>
																	<div class="field prepend-icon">
																		<input type="text" name="con_prenom" id="con_prenom" class="gui-input prenom" placeholder="Prénom"  value="<?=$contact['con_prenom']?>">
																		<span class="field-icon">
																			<i class="fa fa-tag"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<label for="con_tel" >Téléphone :</label>
																	<div class="field prepend-icon">
																		<input name="con_tel" id="con_tel" type="tel" class="gui-input telephone" placeholder="Numéro de téléphone"  value="<?=$contact['con_tel']?>">
																		<span class="field-icon">
																			<i class="fa fa fa-phone"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label for="con_fax" >Fax :</label>
																	<div class="field prepend-icon">
																		<input name="con_fax" id="con_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax"  value="<?=$contact['con_fax']?>">
																		<span class="field-icon">
																			<i class="fa fa-phone-square"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<label for="con_portable" >Portable :</label>
																	<div class="field prepend-icon">
																		<input name="con_portable" id="con_portable" type="tel" class="gui-input telephone" placeholder="Numéro de portable"  value="<?=$contact['con_portable']?>">
																		<span class="field-icon">
																			<i class="fa fa fa-mobile"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label for="con_mail" >Mail :</label>
																	<div class="field prepend-icon">
																		<input type="email" name="con_mail" id="con_mail" class="gui-input" placeholder="Adresse Email"  value="<?=$contact['con_mail']?>">
																		<span class="field-icon">
																			<i class="fa fa-envelope"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
													<?php		if(!$verrou_impaye){ ?>
																	<div class="option-group field mt15">
																		<label class="option option-dark">
																			<input type="checkbox" name="con_compta" value="con_compta" <?php if($contact['con_compta'] == 1) echo("checked"); ?> >
																			<span class="checkbox"></span> Contact "impayé"
																		</label>
																	</div>
													<?php		}else{ ?>
																	<i class="fa fa-check-square-o" ></i> Contact "impayé"
																	<input type="hidden" name="con_compta" value="on" />

													<?php		} ?>
															</div>
															<div class="col-md-6 bloc-particulier" <?php if($d_client["cli_categorie"]!=3) echo("style='display:none;'"); ?> >
																<label for="sta_naissance" >Date de naissance :</label>
																<span class="field prepend-icon">
																	<input type="text" id="sta_naissance" name="sta_naissance" class="gui-input datepicker required-perso" placeholder="Date de naissance" value="<?=$sta_naissance?>" >
																	<span class="field-icon">
																		<i class="fa fa-calendar-o"></i>
																	</span>
																</span>
															</div>
														</div>
														<div class="row bloc-particulier mt15" <?php if($d_client["cli_categorie"]!=3) echo("style='display:none;'"); ?> >
															<div class="col-md-6">
																<label for="sta_cp_naiss" >Département de naissance :</label>
																<div class="field prepend-icon">
																	<input type="text" id="sta_cp_naiss" name="sta_cp_naiss" class="gui-input" placeholder="Code postal de naissance" value="<?php if(isset($d_stagiaire)) echo($d_stagiaire["sta_cp_naiss"])?>" />
																	<span class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</span>
																</div>
															</div>
															<div class="col-md-6" >
																<label for="sta_ville_naiss" >Ville de naissance :</label>
																<div class="field prepend-icon">
																	<input type="text" id="sta_ville_naiss" name="sta_ville_naiss" class="gui-input nom" placeholder="Ville de naissance" value="<?php if(isset($d_stagiaire)) echo($d_stagiaire["sta_ville_naiss"])?>" />
																	<span class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</span>
																</div>
															</div>
														</div>

													</div>
													<!--FIN CONTACT -->

													<!--FACTURATION -->
													<div class="col-md-6">

														<div class="row bloc-no-do">
															<div class="col-md-12">
																<div class="section-divider">
																	<span>Financeur</span>
																</div>
															</div>
														</div>
														<div class="row bloc-no-do" <?php if($d_client["cli_categorie"]==4) echo("style='display:none;'"); ?> >
															<div class="col-md-12 pt20">
																<label class="option option-info">
																	<input type="checkbox" name="cli_facture_opca" id="cli_facture_opca" value="oui" <?php if($d_client['cli_facture_opca'] == 1): ?>checked<?php endif; ?>>
																	<span class="checkbox"></span>
																	<label for="cli_facture_opca">Facturer un financeur</label>
																</label>
															</div>
														</div>
														<div class="row mt15 bloc-no-do" id="bloc_financeur" <?php if($d_client["cli_categorie"]==4) echo("style='display:none;'"); ?> >
															<div class="col-md-6">
																<label for="cli_opca_type" >Type de financeur :</label>
																<select id="cli_opca_type"  name="cli_opca_type" class="select2" >
																	<option value="0">Sélectionner type...</option>
														<?php		foreach($d_opca_type as $do_type){
																		if($do_type["csc_id"]==$d_client["cli_opca_type"]){
																			echo("<option value='" . $do_type["csc_id"] . "' selected >" . $do_type["csc_libelle"]. "</option>");
																		}else{
																			echo("<option value='" . $do_type["csc_id"] . "' >" .  $do_type["csc_libelle"] . "</option>");
																		}
																	} ?>
																</select>
															</div>
															<div class="col-md-6">
																<label for="cli_opca" >Financeur :</label>
																<select id="cli_opca"  name="cli_opca" class="select2" >
																	<option value="0">Sélectionner un financeur...</option>
														<?php		if(isset($d_opca)){
																		foreach($d_opca as $do){
																			if($do["id"]==$d_client["cli_opca"]){
																				echo("<option value='" . $do["id"] . "' selected >" . $do["text"] . "</option>");
																			}else{
																				echo("<option value='" . $do["id"] . "' >" .  $do["text"] . "</option>");
																			}
																		}
																	}	?>
																</select>
															</div>
														</div>

														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb10">
																	<span>Facturation</span>
																</div>
															</div>
														</div>
														<div class="row mt15">
															<div class="col-md-5 pt20">
																<div class="option-group field">
																	<label class="option option-info">
																		<input type="checkbox" name="cli_releve" value="cli_releve" <?php if($d_client['cli_releve'] == 1) echo("checked") ?> />
																		<span class="checkbox"></span>Relevés de factures
																	</label>
																</div>
															</div>
															<div class="col-md-7 bloc-no-particulier"<?=$bloc_no_particulier?> >
																<label for="cli_opca" >Identification TVA :</label>
																<div class="field prepend-icon">
																	<input type="text" name="cli_ident_tva" id="cli_ident_tva" class="gui-input" placeholder="Identification TVA" value="<?= $d_client['cli_ident_tva'] ?>">
																	<span class="field-icon">
																		<i class="fa fa-barcode"></i>
																	</span>
																</div>
															</div>
														</div>

												<?php	// BLOC FACTURATION : PARTIE RESERVE A LA COMPTA

														if($_SESSION['acces']["acc_droits"][9]){ ?>
															<div class="row mt15" >
																<div class="col-md-6" >
																	<label for="cso_rib" >RIB :</label>
																	<select name="cso_rib" id="cso_rib" class="select2" required >
																		<option value="0" >Selectionnez un RIB</option>
																<?php	if(!empty($d_rib)){
																			foreach($d_rib as $rib){
																				if($rib["rib_id"]==$d_client_societe[0]["cso_rib"]){
																					echo("<option value='" . $rib["rib_id"] . "' selected >" . $rib["rib_nom"] . "</option>");
																				}else{
																					echo("<option value='" . $rib["rib_id"] . "' >" . $rib["rib_nom"] . "</option>");
																				}
																			}
																		} ?>
																	</select>
																</div>
																<div class="col-md-6 pt25" >
																	<div class="option-group field">
																		<label class="option option-info">
																			<input type="checkbox" name="cso_rib_change" value="cso_rib_change" <?php if($d_client_societe[0]['cso_rib_change'] == 1) echo("checked") ?> />
																			<span class="checkbox"></span>Changement de RIB
																		</label>
																	</div>
																</div>
															</div>

															<div class="row mt15" >
																<div class="col-md-12" >
																	<div class="option-group field">
																		<label class="option option-info">
																			<input type="checkbox" name="cli_blackliste" value="cli_blackliste" <?php if($d_client['cli_blackliste']) echo("checked") ?> />
																			<span class="checkbox"></span>Blacklisté
																		</label>
																	</div>
																</div>
															</div>

															<div class="section-divider mb10" >
																<span>AFFACTURAGE</span>
															</div>

															<div class="no-affacturage alert alert-danger" <?php if($d_client['cli_affacturable']!=0) echo("style='display:none;'"); ?> >
																Les OPCA, les pôles emplois, les particuliers, les INTERCO et les clients sans SIREN ne sont pas affacturable!
															</div>
															<div class="row mt15 affacturage" id="affacturage" <?php if($d_client['cli_affacturable']==0) echo("style='display:none;'"); ?> >
																<div class="col-md-6">
																	<div class="option-group field">
																		<label class="option option-info">
																			<input type="checkbox" id="cli_non_affacturable" name="cli_non_affacturable" value="on" <?php if($d_client['cli_affacturable'] == 2) echo("checked") ?> />
																			<span class="checkbox"></span>Client non affacturable
																		</label>
																	</div>
																</div>
																<div class="col-md-5">
																	<div class="option-group field">
																		<label class="option option-info">
																			<input type="checkbox" id="cli_affacturage" name="cli_affacturage" value="on" <?php if($d_client['cli_affacturage'] == 1) echo("checked") ?> />
																			<span class="checkbox"></span>Client en affacturage
																		</label>
																	</div>
																</div>
															</div>
															<div class="row mt15 affacturage" id="affacturage_motif" <?php if($d_client['cli_affacturable']!=2) echo("style='display:none;'"); ?> >
																<div class="col-md-12" >
																	<label for="cli_affacturable_txt" >Motif "non affacturable":</label>
																	<input type="text" name="cli_affacturable_txt" id="cli_affacturable_txt" class="gui-input" placeholder="" value="<?php if($d_client['cli_affacturable'] == 2) echo($d_client['cli_affacturable_txt']) ?>">
																</div>
															</div>
															<div class="section-divider mb10" >
																<span>Client INTERCO</span>
															</div>
															<div class="row" >
																<div class="col-md-6">
																	<label for="cli_interco_soc" >Société INTERCO :</label>
																	<select name="cli_interco_soc" id="cli_interco_soc" class="select2 select2-societe" >
																		<option value="0">Société INTERCO ...</option>
										<?php 							foreach($d_societes as $soc_interco){
																			if($soc_interco["soc_id"]==$d_client['cli_interco_soc']){
																				echo("<option value='" . $soc_interco["soc_id"] . "' selected >" . $soc_interco["soc_nom"] . "</option>");
																			}else{
																				echo("<option value='" . $soc_interco["soc_id"] . "' >" . $soc_interco["soc_nom"] . "</option>");
																			}
																		} ?>
																		<option value="999" <?php if($d_client['cli_interco_soc']==999) echo("selected"); ?> >Autres</option>
																	</select>
																</div>
																<div class="col-md-6">
																	<label for="cli_interco_age" >Agence INTERCO :</label>
																	<select name="cli_interco_age" id="cli_interco_age" class="select2 select2-societe-agence" >
																		<option value="0">Agence INTERCO ...</option>
										<?php 							if(isset($d_agences)){
																			foreach($d_agences as $age_interco){
																				if($age_interco["age_id"]==$d_client['cli_interco_age']){
																					echo("<option value='" . $age_interco["age_id"] . "' selected >" . $age_interco["age_nom"] . "</option>");
																				}else{
																					echo("<option value='" . $age_interco["age_id"] . "' >" . $age_interco["age_nom"] . "</option>");
																				}
																			}
																		} ?>
																	</select>
																</div>
															</div>
												<?php	}
														//if($acc_societe==17){ ?>
														<!-- ARRET DE L'AFFACTURAGE ADEQ AU 01/04/2020 -->
															<!--<div class="section-divider mb10" >
																<span>Affacturage ADEQUATION</span>
															</div>
															<div class="row mt15" >
																<div class="col-md-12" >
																	<label for="cli_affacturage_iban" >IBAN affacturage :</label>
																	<div class="field prepend-icon">
																		<input type="text" name="cli_affacturage_iban" id="cli_affacturage_iban" class="gui-input" placeholder="IBAN" value="<?= $d_client['cli_affacturage_iban'] ?>">
																		<span class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</span>
																	</div>
																</div>
															</div>-->

												<?php	//} ?>


														<div class="section-divider" >
															<span>Modalité de règlement</span>
														</div>
														<div class="row mt5">
															<div class="col-md-12">
																<div class="section">
																	<span class="select">
																		<select id="cli_reg_type" name="cli_reg_type" <?php if(!empty($d_client['cli_reg_type'])) echo("required"); ?> >
																			<option value="" >Mode de règlement...</option>
																<?php 		foreach($base_reglement as $k => $n){
																				if($k > 0){ ?>
																					<option value="<?= $k ?>" <?php if($d_client['cli_reg_type'] == $k) echo("selected"); ?> ><?= $n ?></option>
																<?php 			}
																			} ?>
																		</select>
																		<i class="arrow"></i>
																	</span>
																</div>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12">
														<?php	if($d_client["cli_reg_formule"]==1){ ?>
																	<label class="option">
																		<input type="radio" id="reg_formule_1" class="reg-formule" name="reg_formule" value="1" checked >
																		<span class="radio"></span>

																	</label>
																	Date + <input type="number" max="60" size="2" name="reg_nb_jour_1" value="<?=$d_client["cli_reg_nb_jour"]?>" class="champ-reg-formule reg-formule-1" /> jours
														<?php	}else{ ?>
																	<label class="option">
																		<input type="radio" id="reg_formule_1" class="reg-formule" name="reg_formule" value="1" />
																		<span class="radio"></span>
																	</label>
																	Date + <input type="number" max="60" size="2" name="reg_nb_jour_1" value="0" class="champ-reg-formule reg-formule-1" disabled /> jours
														<?php	} ?>
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12">
																<label class="option">
																	<input type="radio" id="reg_formule_2" class="reg-formule" name="reg_formule" value="2" <?php if($d_client["cli_reg_formule"]==2) echo("checked"); ?> >
																	<span class="radio"></span>
																</label>
																Date + 45j + fin de mois
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12">
																<label class="option">
																	<input type="radio" id="reg_formule_3" class="reg-formule" name="reg_formule" value="3" <?php if($d_client["cli_reg_formule"]==3) echo("checked"); ?> >
																	<span class="radio"></span>
																</label>
																Date + Fin de mois + 45j
															</div>
														</div>
														<div class="row mt5" >
															<div class="col-md-12">
														<?php	if($d_client["cli_reg_formule"]==4){ ?>
																	<label class="option">
																		<input type="radio" id="reg_formule_4" class="reg-formule" name="reg_formule" value="4" checked />
																		<span class="radio"></span>
																	</label>
																	Date + <input type="number" max="30" size="2" maxlength="2" name="reg_nb_jour_4" value="<?=$d_client["cli_reg_nb_jour"]?>" class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
																	+ Fin de mois +  <input type="number" size="2" maxlength="2" max="20" name="reg_fdm_4" value="<?=$d_client["cli_reg_fdm"]?>" class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
														<?php	}else{ ?>
																	<label class="option">
																		<input type="radio" id="reg_formule_4" class="reg-formule" name="reg_formule" value="4" />
																		<span class="radio"></span>
																	</label>
																	Date + <input type="number" max="30" size="2" maxlength="2"name="reg_nb_jour_4" value="0" disabled class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
																	+ Fin de mois +  <input type="number" size="2" maxlength="2" max="20" name="reg_fdm_4" value="0" disabled class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
														<?php	} ?>
															</div>
														</div>
													</div>
													<!-- FIN DE FACTURATION -->
												</div>

												<!-- AUTRE INFO -->
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Autres infos</span>
														</div>
													</div>
												</div>
												<div class="row bloc-no-particulier"<?=$bloc_no_particulier?> >
													<div class="col-md-4">
														<div class="section">
															<label for="cli_classification" >Classification</label>
															<select id="cli_classification" name="cli_classification" class="select2 select2-classification" <?php if(!empty($d_client['cli_classification'])) echo("required"); ?> >
																<option value="">Sélectionner une classification...</option>
														<?php	if(!empty($d_classifications)){
																	foreach($d_classifications as $dc){
																		if($dc["ccl_id"]==$d_client['cli_classification']){
																			echo("<option value='" . $dc["ccl_id"] . "' selected >" . $dc["ccl_libelle"] . "</option>");
																		}else{
																			echo("<option value='" . $dc["ccl_id"] . "' >" . $dc["ccl_libelle"] . "</option>");
																		}
																	}
																} ?>
															</select>
														</div>
													</div>
													<div class="col-md-4">
														<div class="section">
															<label for="cli_classification_type" >Type de classification</label>
															<select id="cli_classification_type" name="cli_classification_type" class="select2 select2-classification-type" >
																<option value="0">Sélectionner un type de classification...</option>
														<?php	if(!empty($d_classification_type)){
																	foreach($d_classification_type as $dct){
																		if($dct["cct_id"]==$d_client['cli_classification_type']){
																			echo("<option value='" . $dct["cct_id"] . "' selected >" . $dct["cct_libelle"] . "</option>");
																		}else{
																			echo("<option value='" . $dct["cct_id"] . "' >" . $dct["cct_libelle"] . "</option>");
																		}
																	}
																} ?>
															</select>
														</div>
													</div>
													<div class="col-md-4">
														<div class="section">
															<label for="cli_classification_categorie" >Catégorie de classification</label>
															<select id="cli_classification_categorie" name="cli_classification_categorie" class="select2 select2-classification-categorie" >
																<option value="0">Sélectionner une catégorie de classification...</option>
														<?php	if(!empty($d_classification_categorie)){
																	foreach($d_classification_categorie as $dcc){
																		if($dcc["ccc_id"]==$d_client['cli_classification_categorie']){
																			echo("<option value='" . $dcc["ccc_id"] . "' selected >" . $dcc["ccc_libelle"] . "</option>");
																		}else{
																			echo("<option value='" . $dcc["ccc_id"] . "' >" . $dcc["ccc_libelle"] . "</option>");
																		}
																	}
																} ?>
															</select>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-md-4">
														<div class="section">
															<label for="cli_stagiaire_type" >Type de stagiaire</label>
															<select id="cli_stagiaire_type" name="cli_stagiaire_type" class="select2" >
																<option value="0">Type de stagiaire...</option>
													<?php 		foreach($base_type_stagiaire as $k => $n){
																	if($k > 0){
																		if($k == $d_client['cli_stagiaire_type']){
																			echo("<option value='" . $k . "' selected >" . $n . "</option>");
																		}else{
																			echo("<option value='" . $k . "' >" . $n . "</option>");
																		}
																	}
																} ?>
															</select>
														</div>
													</div>
													<div class="col-md-4 bloc-no-particulier"<?=$bloc_no_particulier?> >
														<label for="cli_tel" >Téléphone (standard):</label>
														<input type="text" name="cli_tel" id="cli_tel" class="gui-input telephone" placeholder="Téléphone" value="<?=$d_client["cli_tel"]?>" />
													</div>
												</div>



												<div class="row">
													<div class="col-md-4 bloc-no-particulier"<?=$bloc_no_particulier?> >
														<label for="ape" >APE :</label>
														<select id="ape"  name="cli_ape" class="select2" <?php if(!empty($d_client['cli_ape'])) echo("required"); ?> >
															<option value="">Sélectionner un code APE...</option>
													<?php	if(!empty($d_apes)){
																foreach($d_apes as $d_ape){
																	if($d_client["cli_ape"]==$d_ape["ape_id"]){
																		echo("<option value='" . $d_ape["ape_id"] . "' selected >" . $d_ape["ape_code"] . "-" . $d_ape["ape_libelle"] . "</option>");
																	}else{
																		echo("<option value='" . $d_ape["ape_id"] . "' >" . $d_ape["ape_code"] . "-" . $d_ape["ape_libelle"] . "</option>");
																	}
																}
															} ?>
														</select>
													</div>
											<?php	if(!$prescripteur_verrou){ ?>
														<div class="col-md-4">
															<div class="section">
																<label for="cli_prescripteur" >Source :</label>
																<select id="cli_prescripteur"  name="cli_prescripteur"class="select2" >
																	<option value="0">Sélectionner source...</option>
															<?php	if(!empty($d_prescripteurs)){
																		foreach($d_prescripteurs as $dp){
																			if($d_client["cli_prescripteur"]==$dp["cpr_id"]){
																				echo("<option value='" . $dp["cpr_id"] . "' selected >" . $dp["cpr_libelle"] . "</option>");
																			}else{
																				echo("<option value='" . $dp["cpr_id"] . "' >" . $dp["cpr_libelle"] . "</option>");
																			}
																		}
																	} ?>
																</select>
															</div>
														</div>
														<div class="col-md-4" id="bloc_prescripteur_info" <?php if($d_prescripteur["cpr_ouvert"]!=1 AND $d_client["cli_prescripteur"]!=-1) echo("style='display:none;'"); ?> >
															<label for="cli_prescripteur_info" ></label>
															<input type="text" name="cli_prescripteur_info" id="cli_prescripteur_info" class="gui-input" placeholder="Merci de préciser votre source" value="<?=$d_client["cli_prescripteur_info"]?>" />
														</div>

											<?php	}else{ ?>
														<div class="col-md-4 pt25">
															Source : <strong><?=$d_prescripteur["cpr_libelle"]?></strong>
														</div>
														<div class="col-md-4 pt25">
															<?=$d_prescripteur["cpr_libelle"]?> : <strong><?=$d_client["cli_prescripteur_info"]?></strong>
														</div>
											<?php	} ?>
												</div>

												<div class="row bloc-no-particulier"<?=$bloc_no_particulier?> >
													<div class="col-md-6">
														<div class="section" >
															<label for="cli_capital" >Capital</label>
															<div class="input-group">
																<input type="text" id="cli_capital" name="cli_capital" class="gui-input" placeholder="Capital" value="<?=$d_client["cli_capital"]?>" />
																<div class="input-group-addon">€</div>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section" >
															<label for="cli_soc_type" >Type de société</label>
															<input type="text" name="cli_soc_type" id="cli_soc_type" class="gui-input" placeholder="Type de société" value="<?=$d_client["cli_soc_type"]?>" />
														</div>
													</div>
												</div>
												<div class="row ">
													<div class="col-md-6 bloc-no-particulier" <?php if($d_client["cli_categorie"]==3) echo("style='display:none;'"); ?> >
														<div class="section" >
															<label for="cli_immat_lieu" >Lieu d'immatriculation</label>
															<input type="text" id="cli_immat_lieu" name="cli_immat_lieu" class="gui-input" placeholder="Lieu d'immatriculation" value="<?=$d_client["cli_immat_lieu"]?>" />
														</div>
													</div>
												</div>
												<div class="row bloc-no-particulier">
													<div class="col-md-6">
														<div class="section">
															<label for="cli_reference" >Référence interne :</label>
															<div class="field prepend-icon">
																<input type="text" name="cli_reference" id="cli_reference" class="gui-input" placeholder="Référence interne du client" value="<?= $d_client['cli_reference'] ?>">
																<span class="field-icon">
																	<i class="fa fa-code"></i>
																</span>
															</div>
														</div>
													</div>
													<div class="col-md-6 pt25">
														<div class="section text-center">
															<label class="option option-info">
																<input type="checkbox" name="cli_important" id="cli_important" value="oui" <?php if($d_client['cli_important'] == 1): ?>checked<?php endif; ?>>
																<span class="checkbox"></span>
																<label for="cli_important">Client référence</label>
															</label>
														</div>
													</div>
												</div>

											</div>
											<!--FIN DE AUTRE INFO -->

										<!-- FIN PANEL -->
                                        </div>
									</div>
								</div>
							</div>
						</div>
					</section>
                    <!-- End: Content -->

				<!-- End: Content -->
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
				  <div class="col-xs-3 footer-left">
					<a href="client_voir.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-default btn-sm">
					  <i class="fa fa-long-arrow-left"></i>
					  Retour
					</a>
				  </div>
				  <div class="col-xs-6 footer-middle"></div>
				  <div class="col-xs-3 footer-right">

					<button type="submit" class="btn btn-success btn-sm">
					  <i class='fa fa-floppy-o'></i> Enregistrer
					</button>
				  </div>
			  </div>
			</footer>
		</form>
<?php 	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="assets/js/custom.js"></script>
		<script src="/assets/admin-tools/admin-forms/js/jquery.validate.min.js" ></script>
		<script src="/assets/admin-tools/admin-forms/js/jquery.validate.french.js" ></script>


		<script type="text/javascript">

			//var acc_societe="<?=$acc_societe?>";

			var tab_categorie=new Array();
			tab_categorie[0]=0;
	<?php	if(isset($d_cli_categories)){
				foreach($d_cli_categories as $cc){ ?>
					tab_categorie[<?=$cc["cca_id"]?>]="<?=$cc["cca_gc"]?>";
	<?php		}
			} ?>


			var source=new Array();
			source[0]=new Array();
			source[0]["libelle"]="";
			source[0]["ouvert"]=0;
			source[-1]=new Array();
			source[-1]["libelle"]="Autre";
			source[-1]["ouvert"]=1;
	<?php	if(!empty($d_prescripteurs)){
				foreach($d_prescripteurs as $prescripteur){ ?>
					source[<?=$prescripteur["cpr_id"]?>]=new Array();
					source[<?=$prescripteur["cpr_id"]?>]["libelle"]="<?=$prescripteur["cpr_libelle"]?>";
					source[<?=$prescripteur["cpr_id"]?>]["ouvert"]="<?=$prescripteur["cpr_ouvert"]?>";
	<?php		}
			} ?>

			jQuery(document).ready(function (){

				$("#cli_categorie").change(function(){
					get_sous_categories($(this).val(),actualiser_sous_categorie,"","");
					afficher_bloc_categorie($(this).val());
					$("#cli_filiale_de").val("").trigger("change");
					$("#cli_filiale_de").data("client_categorie",$(this).val());
					$("#cli_filiale_de").data("client_s_categorie",0);
					check_affacturable();

				});

				$("#cli_sous_categorie").change(function(){
					$("#cli_filiale_de").val("").trigger("change");
					$("#cli_filiale_de").data("client_s_categorie",$(this).val());
					check_affacturable();
				});

				$('#cli_groupe').click(function() {
					if($("#cli_groupe").is(':checked')){
						$(".maisonm").show();
						if($("#cli_maison_mere_non").is(":checked")){
							$("#cli_filiale_de").prop("required",true);
						}else{
							$("#cli_filiale_de").prop("required",false);
						}
					}else{
						$(".maisonm").hide();
						$("#cli_filiale_de").prop("required",false);
					}
				});
				$("#cli_maison_mere").click(function(){
					// c'est la maison mère
					$("#choix_mm").hide();
					$("#cli_filiale_de").val("0");
					$("#cli_filiale_de").prop("required",false);
				});

				$("#cli_maison_mere_non").click(function(){
					$("#choix_mm").show();
					$("#cli_filiale_de").prop("required",true);
					console.log("Chat");

				});


				// ADRESSE

				$("#similaire").click(function(){
					if($("#similaire").is(':checked')){
						$("#adr_libelle2").val($("#adr_libelle1").val());
						$("#adr_nom2").val($("#adr_nom1").val());
						$("#adr_service2").val($("#adr_service1").val());
						$("#adr_libelle2").val($("#adr_libelle1").val());
						$("#adr_ad12").val($("#adr_ad11").val());
						$("#adr_ad22").val($("#adr_ad21").val());
						$("#adr_ad32").val($("#adr_ad31").val());
						$("#adr_cp2").val($("#adr_cp1").val());
						$("#adr_ville2").val($("#adr_ville1").val());
						$("#cli_geo12").prop("checked",$("#cli_geo11").prop("checked"));
						$("#cli_geo22").prop("checked",$("#cli_geo21").prop("checked"));
						$("#cli_geo32").prop("checked",$("#cli_geo31").prop("checked"));
					}
				});

				$("#adr_libelle1").keyup(function(){
					if($("#similaire").is(':checked')){
						$("#adr_libelle2").val($("#adr_libelle1").val());
					}
				});

				$("#adr_nom1").keyup(function() {
					if($("#similaire").is(':checked')){
						$("#adr_nom2").val($("#adr_nom1").val());
					}
				});

				$("#adr_service1").keyup(function() {
					if($("#similaire").is(':checked')){
						$("#adr_service2").val($("#adr_service1").val());
					}
				});

				$("#adr_ad11").keyup(function(event) {
					if($("#similaire").is(':checked')){
					   $("#adr_ad12").val($("#adr_ad11").val());
					}
				});

				$("#adr_ad21").keyup(function(event) {
					if($("#similaire").is(':checked')){
					   $("#adr_ad22").val($("#adr_ad21").val());
					}
				});

				$("#adr_ad31").keyup(function(event) {
					if($("#similaire").is(':checked')){
					   $("#adr_ad32").val($("#adr_ad31").val());
					}
				});

				$("#adr_cp1").keyup(function(event) {
					if($("#similaire").is(':checked')){
					   $("#adr_cp2").val($("#adr_cp1").val());
					}
				});

				$("#adr_ville1").keyup(function(event) {
					if($("#similaire").is(':checked')){
					   $("#adr_ville2").val($("#adr_ville1").val());
					}
				});

				$('input[type=radio][name=cli_geo1]').change(function() {
					if($("#similaire").is(":checked")){
						for (var i = 1; i<4; i++){
							if($("#cli_geo"+ i + "1").is(':checked')){
								$("#cli_geo"+ i + "2").prop("checked", true);
							}else{
								$("#cli_geo"+ i + "2").prop("checked", false);
							}
						};
					}
				});


				// GESTION DES MODALITES DE REGLEMENTS
				$("input[name=reg_formule]").click(function (){
					reg_formule=$("input[name=reg_formule]:checked").val();
					$('.champ-reg-formule').each(function(){
						if($(this).hasClass("reg-formule-" + reg_formule)){
							$(this).prop("disabled",false);
						}else{
							$(this).val(0);
							$(this).prop("disabled",true);
						}
					});
				});

				$(".champ-reg-formule").blur(function(){
					//console.log($(this).val());
					//console.log($(this).prop("max"));
					if( ($(this).val()*1)>($(this).prop("max")*1) ){
						$(this).val($(this).prop("max"));
					}
				});

				// SIREN / SIRET
				$("#cli_siren").focusout(function() {
					if ( $("#cli_siren").val().length != 11){
						$("#cli_siret").val("");
						$("#adr_siret").val("");
						$("#adr_siret").prop("disabled",true);
					}else{
						check_siren_val();
					}
					check_affacturable();
				});

				$("#cli_siren").keyup(function() {
					if($(this).val().length== 11){
						$("#adr_siret").prop("disabled",false);
						$("#adr_siret").focus();
					}
				});

				$("#adr_siret").focusout(function() {
					if($(this).val().length== 6){
						$("#cli_siret").val($("#cli_siren").val() + " " + $(this).val());
					}else{
						$("#cli_siret").val("");
					}
				});

				$("#cli_siret").focusout(function(){
					if ($(this).val().length != 18){
						$("#cli_siren").val('');
						$("#adr_siret").val("");
						$("#adr_siret").prop("disabled",true);
					}else{
						$("#cli_siren").val($(this).val().substring(0,11));
						$("#adr_siret").val($(this).val().substring(12));
						$("#adr_siret").prop("disabled",false);
						check_siren_val();
					}
				});

				// verif du siret
				$( "#verif_siret" ).click(function(){
					check_siret($("#cli_siren").val(),$("#adr_siret").val(),$("#adresse_fac").val(),afficher_check_siret,"","");
				});


				// Contact
				$("#con_fonction").click(function(){
					if($(this).val()=="autre"){
						$(".con_fonction_nom_style").show(400);
					}else{
						$(".con_fonction_nom_style").hide(400);
					}
				});


				// AFFACTURAGE

				$("#cli_non_affacturable").click(function(){
					if($(this).is(":checked")){
						$("#cli_affacturage").prop("checked",false);
					}
					check_affacturable();
				});
				$("#cli_affacturage").click(function(){
					if($(this).is(":checked")){
						$("#cli_non_affacturable").prop("checked",false);
					}
					check_affacturable();
				});

				$("#cli_interco_soc").change(function(){
					check_affacturable();
				});

				$("#cli_prescripteur").change(function(){
					$("#cli_prescripteur_info").val("");
					if(source[$(this).val()]["ouvert"]==1){

						$("#bloc_prescripteur_info label").html(source[$(this).val()]["libelle"]);
						$("#bloc_prescripteur_info").show();
					}else{
						$("#bloc_prescripteur_info").hide();
					}
				});

				// Financeur

				$("#cli_facture_opca").click(function(){
					if($(this).is(":checked")){
						$("#bloc_financeur").show();
					}else{
						$("#bloc_financeur").hide();
					}
				});

				$("#cli_opca_type").change(function(){
					if($(this).val()>0){
						get_clients(4,$(this).val(),0,null,actualiser_select2,"#cli_opca",0);
					}else{
						$("#cli_opca option[value!='0'][value!='']").remove();
					}
				});


			});

			//-----------------------------------
			// FONCTION SPE CLIENT
			//-----------------------------------

			function check_siren_val(){
				if($("#cli_siren").val()=="123 456 789"||$("#cli_siren").val()=="000 000 000"||$("#cli_siren").val()=="111 111 111"||$("#cli_siren").val()=="222 222 222"||$("#cli_siren").val()=="333 333 333"||$("#cli_siren").val()=="444 4444 444"||$("#cli_siren").val()=="555 555 555"||$("#cli_siren").val()=="666 666 666"||$("#cli_siren").val()=="777 777 777"||$("#cli_siren").val()=="888 888 888"||$("#cli_siren").val()=="999 999 999"||$("#cli_siren").val()=="987 654 321"){
					afficher_txt_user($("#cli_siren_err"),"Format incorrect!","danger",5000);
					$("#cli_siren").val("");
					$("#cli_siret").val("");
					$("#adr_siret").val("");
					$("#adr_siret").prop("disabled",true);
					check_affacturable();
				}else{
					$("#adr_siret").prop("disabled",false);
				}
			}

			function actualiser_sous_categorie(json){
				$("#cli_sous_categorie option[value!='0']").remove();
				if(json!=""){
					$("#cli_sous_categorie").select2({
						data: json
					});
					$("#sous_categorie_bloc").show();
				}else{
					$("#sous_categorie_bloc").hide();
				}
			}

			// GESTION DES CATEGORIE

			// afficher / masque les éléments en fonction de la categorie selectionné
			function afficher_bloc_categorie(categorie){
				console.log("afficher_bloc_categorie(" + categorie+ ")");
				if(categorie>0){

					if(categorie==4){
						// DO
						$(".bloc-no-do").hide();
					}else{
						$(".bloc-no-do").show();
					}

					if(categorie==3){
						// PARTICULIER
						$("#contact_libelle").text("Coordonnées du particulier");
						$(".bloc-no-particulier").hide();
						$(".bloc-particulier").show();
						// champ obligatoire uniquement pour les particuliers
						$(".required-perso").prop("required",true);
						// champ obligatoire uniquement pour les pro
						$(".required-pro").prop("required",false);
						$("#bloc_gc").hide();
						$("#bloc_groupe").hide();
					}else{
						// SOCIETE
						$("#contact_libelle").text("Contact par défaut du lieu d'intervention");
						$(".bloc-no-particulier").show();
						$(".bloc-particulier").hide();
						$(".required-perso").prop("required",false);
						$(".required-pro").prop("required",true);
						$("#bloc_groupe").show();
						if(categorie==2){
							$("#bloc_gc").show();
						}else{
							$("#bloc_gc").hide();
						}
					}
				}

			}

			function afficher_check_siret(json,siren,siret){
				if(json){
					if(json==""){
						$("#siret_ok").show().delay(3000).fadeOut(500);
					}else{
						var erreur_txt="";

						erreur_txt="<p>Le siret " + siren + " " + siret + " est déjà utilisé pour :</p>";
						erreur_txt=erreur_txt+"<ul>";
							$.each(json, function( index, item ) {
								if(item.source=="cli"){
									erreur_txt=erreur_txt+"<li>";
										erreur_txt=erreur_txt+ "le client " + item.code;
										if(item.societes!=""){
											erreur_txt=erreur_txt+"sur les sociétés :<ul>";
												$.each(item.societes, function(j,soc){
													erreur_txt=erreur_txt+ "<li>" + soc + "</li>";
												})
											erreur_txt=erreur_txt+"</ul>";
										}
									erreur_txt=erreur_txt+"</li>";
								}else{
									erreur_txt=erreur_txt+"<li>";
										erreur_txt=erreur_txt+ "le suspect " + item.code;
									erreur_txt=erreur_txt+"</li>";
								}
							})
						erreur_txt=erreur_txt+"</ul></p>";
						modal_alerte_user(erreur_txt,"");
					}
				}
			}


			function check_affacturable(){
		<?php	if($_SESSION['acces']["acc_droits"][9]){ ?>
					if($("#cli_categorie").val()==3 || $("#cli_sous_categorie").val()==5 || $("#cli_sous_categorie").val()==6 || $("#cli_siren").val()=="" || $("#cli_interco_soc").val()>0){
						$(".no-affacturage").show();
						$(".affacturage").hide();
					}else{
						$(".no-affacturage").hide();
						$("#affacturage").show();
						if($("#cli_non_affacturable").is(":checked")){
							$("#affacturage_motif").show();
						}else if($("#cli_affacturage").is(":checked")){
							$("#affacturage_motif").hide();
						}else{
							$("#affacturage_motif").hide();
						}
					}
		<?php	} ?>
			}

		</script>

	</body>
</html>
