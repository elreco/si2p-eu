 <?php

	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");

	// ENREGISTREMENT DES DONNEES DATES
	// SI action_client -> on met aussi à jour les dates de présences du client


	$erreur_txt="";
	$warning_txt="";

	if(!isset($_POST)){
		$erreur_txt="Formulaire incomplet";
	}elseif(empty($_POST)){
		$erreur_txt="Formulaire incomplet";
	}

	// ELEMENT OBLIGATOIRE

	if(empty($erreur_txt)){

		$action_id=0;
		if(!empty($_POST["action"])){
			$action_id=intval($_POST["action"]);
		}
		if(empty($action_id)){
			$erreur_txt="Formulaire incomplet";
		}
	}

	// CONTROLE

	if(empty($erreur_txt)){
		// info sur l'action
		$sql="SELECT act_multi_session,act_agence,act_pro_sous_famille,act_pro_sous_sous_famille FROM Actions WHERE act_id=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$action_id);
		$req->execute();
		$d_action=$req->fetch();
		if(empty($d_action)){
			$erreur_txt="Les dates n'ont pas pu être chargées.";
		}
	}

	// DONNEE COOMPLEMENTAIRE
	if(empty($erreur_txt)){

		// autre element du form

		$action_client_id=0;
		if(!empty($_POST["action_client"])){
			$action_client_id=intval($_POST["action_client"]);
		}

		$act_date_h_def_1=0;
		if(!empty($_POST["act_date_h_def_1"])){
			$act_date_h_def_1=intval($_POST["act_date_h_def_1"]);
		}

		$act_date_h_def_2=0;
		if(!empty($_POST["act_date_h_def_2"])){
			$act_date_h_def_2=intval($_POST["act_date_h_def_2"]);
		}


		// sur la personne connecte

		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			$acc_agence=$_SESSION['acces']["acc_agence"];
		}
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];
		}
		if($conn_soc_id!=$acc_societe){
			$acc_societe=$conn_soc_id;
			$acc_agence=0;
		}

		// vehicule

		$d_vehicules=array();
		$sql="SELECT veh_id,veh_couleur FROM Vehicules WHERE NOT veh_archive AND veh_societe=". $acc_societe;
		if(!empty($d_action["act_agence"])){
			$sql.=" AND (veh_agence=" . $d_action["act_agence"] . " OR veh_agence=0 OR ISNULL(veh_agence))";
		}
		$sql.=" ORDER BY veh_id;";
		$req=$Conn->query($sql);
		$d_result=$req->fetchAll();
		if(!empty($d_result)){
			foreach($d_result as $r){
				$d_vehicules[$r["veh_id"]]=$r["veh_couleur"];
			}
		}

		// etat de confirmation

		$acl_confirme=true;
		$sql="SELECT acl_id FROM Actions_Clients WHERE acl_action=:action_id AND NOT acl_confirme;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$d_result=$req->fetchAll();
		if(!empty($d_result)){
			$acl_confirme=false;
		}

		// famille de l'action

		$txt_fam="color:#000";
		$bg_fam="";

		if(!empty($d_action["act_pro_sous_sous_famille"])){
			$sql="SELECT psf_couleur_bg,psf_couleur_txt,pss_couleur_bg,pss_couleur_txt FROM Produits_Sous_Familles,Produits_Sous_Sous_Familles WHERE pss_psf_id=psf_id AND pss_id=" . $d_action["act_pro_sous_sous_famille"] . ";";
			$req=$Conn->query($sql);
			$d_famille=$req->fetch();
			if(!empty($d_famille)){
				if(!empty($d_famille["pss_couleur_bg"])){
					$txt_fam=$d_famille["pss_couleur_txt"];
					$bg_fam=$d_famille["pss_couleur_bg"];
				}else{
					$txt_fam=$d_famille["psf_couleur_txt"];
					$bg_fam=$d_famille["psf_couleur_bg"];

				}
			}
		}else{
			$sql="SELECT psf_couleur_bg,psf_couleur_txt FROM Produits_Sous_Familles WHERE psf_id=" . $d_action["act_pro_sous_famille"] . ";";
			$req=$Conn->query($sql);
			$d_famille=$req->fetch();
			if(!empty($d_famille)){
				$bg_fam=$d_famille["psf_couleur_bg"];
				$txt_fam=$d_famille["psf_couleur_txt"];
			}
		}
	}

	// ENREGISTREMENT

	if(empty($erreur_txt)){

		// REQUETES

		// maj donnee dates

		$sql_up="UPDATE Plannings_Dates SET
		pda_categorie=:pda_categorie,
		pda_vehicule=:pda_vehicule,
		pda_salle=:pda_salle,
		pda_style_bg=:pda_style_bg,
		pda_style_txt=:pda_style_txt
		WHERE pda_id=:pda_id;";
		$req_up=$ConnSoc->prepare($sql_up);

		// clients dates

		$sql_add_cli="INSERT INTO Actions_Clients_Dates (acd_date,acd_action_client) VALUES (:acd_date,:acd_action_client);";
		$req_add_cli=$ConnSoc->prepare($sql_add_cli);
		$req_add_cli->bindParam(":acd_action_client",$action_client_id);

		$sql_del_cli="DELETE FROM Actions_Clients_Dates WHERE acd_date=:acd_date AND acd_action_client=:acd_action_client;";
		$req_del_cli=$ConnSoc->prepare($sql_del_cli);
		$req_del_cli->bindParam(":acd_action_client",$action_client_id);


		// ON PARCOURS LES DATES
		if(!empty($action_client_id)){
			$sql="SELECT pda_id,pda_vehicule,pda_style_bg,pda_style_txt,acd_date FROM Plannings_Dates LEFT JOIN Actions_Clients_Dates ON (Plannings_Dates.pda_id=Actions_Clients_Dates.acd_date AND Actions_Clients_Dates.acd_action_client=" . $action_client_id . ")
			WHERE pda_type=1 AND pda_ref_1=" . $action_id;
		}else{
			$sql="SELECT pda_id,pda_vehicule,pda_style_bg,pda_style_txt FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=" . $action_id;
		}
		$req=$ConnSoc->query($sql);
		$d_dates=$req->fetchAll();
		/*echo("<pre>");
			print_r($d_dates);
		echo("</pre>");
		die();*/
		if(!empty($d_dates)){

			$vehicule_ref=null;
			$multi_vehicule=false;

			$salle_ref=null;
			$multi_salle=false;

			foreach($d_dates as $d){

				$pda_categorie=0;
				if(!empty($_POST["pda_categorie_" . $d["pda_id"]])){
					$pda_categorie=intval($_POST["pda_categorie_" . $d["pda_id"]]);
				}
				$pda_salle=0;
				if(!empty($_POST["pda_salle_" . $d["pda_id"]])){
					$pda_salle=intval($_POST["pda_salle_" . $d["pda_id"]]);
				}

				if(is_null($salle_ref)){
					$salle_ref=$pda_salle;
				}elseif($salle_ref!=$pda_salle){
					$multi_salle=true;
				}

				$pda_vehicule=0;
				if(!empty($_POST["pda_vehicule_" . $d["pda_id"]])){
					$pda_vehicule=intval($_POST["pda_vehicule_" . $d["pda_id"]]);
				}

				if(is_null($vehicule_ref)){
					$vehicule_ref=$pda_vehicule;
				}elseif($vehicule_ref!=$pda_vehicule){
					$multi_vehicule=true;
				}

				$pda_style_bg="";
				$pda_style_txt="";
				if(!empty($d_vehicules[$pda_vehicule])){
					$pda_style_bg=$d_vehicules[$pda_vehicule];
				}else{
					$pda_style_bg=$bg_fam;
					$pda_style_txt=$txt_fam;
				}
				$req_up->bindParam(":pda_categorie",$pda_categorie);
				$req_up->bindParam(":pda_salle",$pda_salle);
				$req_up->bindParam(":pda_vehicule",$pda_vehicule);
				$req_up->bindParam(":pda_style_bg",$pda_style_bg);
				$req_up->bindParam(":pda_style_txt",$pda_style_txt);
				$req_up->bindParam(":pda_id",$d["pda_id"]);
				try{
					$req_up->execute();
				}Catch(Exception $e){
					$erreur_txt=$e->getMessage();
					break;
				}
				if(empty($erreur_txt)){

					// SI ON A PASSE UNE ACTION CLIENT EN PARAM -> ON PEUT METTRE A JOUR LES JOURS DE PRESENCE
					if(!empty($action_client_id)){

						if(!empty($d["acd_date"]) AND empty($_POST["present_" . $d["pda_id"]])){
							// client ne participe plus
							$req_del_cli->bindParam(":acd_date",$d["pda_id"]);
							$req_del_cli->execute();
						}elseif(empty($d["acd_date"]) AND !empty($_POST["present_" . $d["pda_id"]])){
							// cleint participe
							$req_add_cli->bindParam(":acd_date",$d["pda_id"]);
							$req_add_cli->execute();

						}

					}
				}
			}
		}
	}
	if(empty($erreur_txt)){

		// MAJ DES SESSION

		// requete

		$sql_up_ses="UPDATE Actions_Sessions SET  ase_h_deb=:ase_h_deb,ase_h_fin=:ase_h_fin WHERE ase_id=:session;";
		$req_up_ses=$ConnSoc->prepare($sql_up_ses);

		$sql_del_ses="DELETE FROM Actions_Sessions WHERE ase_id=:session;";
		$req_del_ses=$ConnSoc->prepare($sql_del_ses);


		// MAJ DES SESSION EXISTANTES

		$sql="SELECT * FROM Actions_Sessions WHERE ase_action=" . $action_id . ";";
		$req=$ConnSoc->query($sql);
		$d_sessions=$req->fetchAll();
		/*echo("<pre>");
			print_r($d_sessions);
		echo("</pre>");
		die();*/
		if(!empty($d_sessions)){
			foreach($d_sessions as $s){

				$ase_date=0;
				if(!empty($_POST["ase_date_" . $s["ase_id"]])){
					$ase_date=intval($_POST["ase_date_" . $s["ase_id"]]);
				}

				if(!empty($ase_date)){

					$ase_h_deb="";
					if(!empty($_POST["ase_h_deb_" . $s["ase_id"]])){
						if(strlen($_POST["ase_h_deb_" . $s["ase_id"]])==5){
							$ase_h_deb=$_POST["ase_h_deb_" . $s["ase_id"]];
						}else{
							$warning_txt="Le format " . $_POST["ase_h_deb_" . $s["ase_id"]] . " est incorrect. Horaire non enregistrée.<br/>";
						}
					}

					$ase_h_fin="";
					if(!empty($_POST["ase_h_fin_" . $s["ase_id"]])){
						if(strlen($_POST["ase_h_fin_" . $s["ase_id"]])==5){
							$ase_h_fin=$_POST["ase_h_fin_" . $s["ase_id"]];
						}else{
							$warning_txt.="Le format " . $_POST["ase_h_fin_" . $s["ase_id"]] . " est incorrect. Horaire non enregistrée.";
						}
					}

					$req_up_ses->bindParam("ase_h_deb",$ase_h_deb);
					$req_up_ses->bindParam("ase_h_fin",$ase_h_fin);
					$req_up_ses->bindParam("session",$s["ase_id"]);
					try{
					$req_up_ses->execute();
					}Catch(Exception $e){
						$erreur_txt=$e->getMessage();
					}
				}else{
					$req_del_ses->bindParam("session",$s["ase_id"]);
					try{
						$req_del_ses->execute();
					}Catch(Exception $e){
						$erreur_txt=$e->getMessage();
					}
				}
			}
		}
	}
	if(empty($erreur_txt)){

		// AJOUT DE NOUVELLE SESSION

		// requete
		$sql_add_ses="INSERT INTO Actions_Sessions (ase_date,ase_h_deb,ase_h_fin,ase_action) VALUES (:ase_date,:ase_h_deb,:ase_h_fin,:ase_action);";
		$req_add_ses=$ConnSoc->prepare($sql_add_ses);
		$req_add_ses->bindParam(":ase_action",$action_id);


		if(!empty($_POST["ase_date"])){
			$session_date=$_POST["ase_date"];
			$session_deb=$_POST["ase_h_deb"];
			$session_fin=$_POST["ase_h_fin"];
			print_r($session_date);

			foreach($session_date as $k => $v){

				$ase_h_deb="";
				if(isset($session_deb[$k])){
					if(!empty($session_deb[$k])){
						$ase_h_deb=$session_deb[$k];
					}
				}
				$ase_h_fin="";
				if(isset($session_fin[$k])){
					if(!empty($session_fin[$k])){
						$ase_h_fin=$session_fin[$k];
					}
				}
				if(!empty($v) AND !empty($ase_h_deb) AND !empty($ase_h_fin) ){
					$req_add_ses->bindParam(":ase_date",$v);
					$req_add_ses->bindParam(":ase_h_deb",$ase_h_deb);
					$req_add_ses->bindParam(":ase_h_fin",$ase_h_fin);

					try{
						$req_add_ses->execute();
					}Catch(Exception $e){
						$erreur_txt=$e->getMessage();
					}
				}
			}
		}
	}
	if(empty($erreur_txt)){

		// ON MET A JOUR L'ACTION

		if($multi_vehicule){
			$vehicule_ref=0;
		}
		if($multi_salle){
			$salle_ref=0;
		}
		$sql="UPDATE Actions SET act_date_h_def_1=:act_date_h_def_1,act_date_h_def_2=:act_date_h_def_2,act_vehicule=:act_vehicule,act_salle=:act_salle
		WHERE act_id=:action_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":act_date_h_def_1",$act_date_h_def_1);
		$req->bindParam(":act_date_h_def_2",$act_date_h_def_2);
		$req->bindParam(":act_vehicule",$vehicule_ref);
		$req->bindParam(":act_salle",$salle_ref);
		$req->bindParam(":action_id",$action_id);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur_txt=$e->getMessage();
		}

	}
	if(!empty($erreur_txt)){
		if(!empty($action_id)){
			$_SESSION['message'][] = array(
				"titre" => "Erreur",
				"type" => "danger",
				"message" => $erreur_txt
			);
			header("location: action_voir.php?action=" . $action_id . "&societ=" . $conn_soc_id);

		}else{
			echo($erreur_txt);
			die();
		}
	}else{

		if(!empty($warning_txt)){
			$_SESSION['message'][] = array(
				"titre" => "Attention",
				"type" => "warning",
				"message" => $warning_txt
			);
		}else{
			$_SESSION['message'][] = array(
				"titre" => "Enregistrement terminé",
				"type" => "success",
				"message" => "Les données ont été mises à jour!"
			);
		}
		header("location: action_voir.php?action=" . $action_id . "&societ=" . $conn_soc_id);
		die();
	}
?>
