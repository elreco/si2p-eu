<?php
// ENREGISTREMENT D'UN DEVIS

include "includes/controle_acces.inc.php";
include_once("includes/connexion_soc.php");
include_once("includes/connexion.php");
include('modeles/mod_parametre.php');

// FIN DES CONTROLES

// ENREGISTREMENT
if(!empty($_GET['id'])){
	$sql_edit="UPDATE Evacuations SET eva_date_termine = NULL, eva_tech_valide_date = NULL, eva_tech_text = NULL,  eva_assist_valide_date = NULL, eva_assist_text = NULL,  eva_etat_valide = 2 WHERE eva_id = " . $_GET['id'];
	$req_edit=$Conn->prepare($sql_edit);
	$req_edit->execute();
}
$_SESSION['message'][] = array(
	"titre" => "Succès",
	"type" => "success",
	"message" => "Le rapport est maintenant réouvert"
);
Header("Location: evac_voir.php?id=" . $_GET['id']);
die();
