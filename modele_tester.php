<?php 
	include "includes/controle_acces.inc.php";
	
	include_once 'includes/connexion.php';
	
	include 'modeles/mod_parametre.php';
	include 'modeles/mod_get_passe_validite.php';
	include 'modeles/mod_upload.php';
	include 'modeles/mod_document.php';
	include 'modeles/mod_add_utilisateur_etech.php';
	include 'modeles/mod_set_utilisateur_etech.php';
$sql="SELECT * FROM Utilisateurs";
$req = $Conn->prepare($sql);
$req->execute();
$utilisateurs = $req->fetchAll();

// INSERTION DU MODèLE PAR DEFAUT
foreach($utilisateurs as $u){
	$req = $Conn->prepare("INSERT INTO Modeles (mod_utilisateur, mod_type, mod_fct_1, mod_menu) VALUES (:mod_utilisateur, :mod_type, :mod_fct_1, :mod_menu)");
	$req->bindParam("mod_utilisateur", $u['uti_id']);
	$req->bindValue("mod_type", 1);
	$req->bindValue("mod_menu", 0);
	$req->bindValue("mod_fct_1", 1);
	$req->execute();
}


