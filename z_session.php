﻿<?php 
/////////////////// INCLUDES /////////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');
// session retour

// fin session retour

/////////////// TRAITEMENTS BDD ///////////////////////
if(isset($_GET['id'])){
    $_GET['id'] = intval($_GET['id']);
}


if(isset($_GET['id'])){
	
		// EDITION 
    $req = $Conn->prepare("SELECT * FROM sessions WHERE ses_id = " . $_GET['id']);
    $req->execute();
    $session = $req->fetch();
	if(!empty($session)){
		
		// LES SESSIONS EXISTANTES
		
		$sql_h="SELECT * FROM Sessions_Horaires WHERE sho_session=" . $_GET['id'] . " ORDER BY sho_h_deb;";
		$req_h=$Conn->query($sql_h);
		$d_h=$req_h->fetchAll();
	}
}else{
	$session=array(
		"ses_formation" => "",
		"ses_date" => "",
		"ses_demi" => 1,
		"ses_produit" => 0
	);
}

	// PRODUITS DISPONIBLE
	$req = $Conn->query("SELECT cpr_id,cpr_libelle FROM Cnrs_Produits ORDER BY cpr_libelle;");
	$d_produits=$req->fetchAll();
	

?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Ajouter une session</title> 
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!-- Admin forms -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    
	<!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
   
   <!-- ORION.CSS a mettre toujours à la fin -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
	<form method="post" action="session_enr.php" enctype="multipart/form-data">
		<!-- Start: Main -->
		<div id="main">
		<?php include "includes/header_cli.inc.php"; ?>
		<!-- Start: Content-Wrapper -->
			<section id="content_wrapper" >
				<!--DEBUT CONTENT -->
				<section id="content" class="animated fadeIn">
					<!--DEBUT ROW -->
					<div class="row"> 
					<!--DEBUT COL-MD-10 -->
						<div class="col-md-10 col-md-offset-1">
						  <!--DEBUT ADMIN FORM -->
							<div class="admin-form theme-primary ">
								<!--DEBUT PANEL HEADING -->
								<div class="panel heading-border panel-primary">
								  <!--DEBUT PANEL BODY -->
									<div class="panel-body bg-light">
										<div class="content-header">
											<?php if(isset($_GET['id'])){ ?>
												<h2>Modification d'une <b class="text-primary">formation</b></h2>
											<?php }else{ ?>
												<h2>Ajouter une <b class="text-primary">formation</b></h2>
											<?php } ?>
													  
										</div>
										<div class="col-md-10 col-md-offset-1"> 
										
								<?php 		if(isset($_GET['id'])){ ?>
												<input type="hidden" name="ses_id" value="<?= $_GET['id'] ?>">
								<?php 		} ?>
								
											<div class="row" >												
												<div class="col-md-2 mt10 text-right" >Date de la formation :</div>
												<div class="col-md-2" >
													<input type="text" name="ses_date" class="gui-input gui-input-sm date datepicker" placeholder="" value="<?= convert_date_txt($session['ses_date']) ?>" required />
												</div>
												<div class="col-md-2" >
													<select name="ses_demi" class="form-control" required >
														<option value="" >&nbsp;</option>
														<option value="1" <?php if($session['ses_demi']==1) echo("selected"); ?> >Matin</option>
														<option value="2" <?php if($session['ses_demi']==2) echo("selected"); ?> >Après-Midi</option>
														<option value="3" <?php if($session['ses_demi']==3) echo("selected"); ?> >Toute la journée</option>
													</select>	
												</div>
												<div class="col-md-2 mt10 text-right" >Formation :</div>
												<div class="col-md-2" >
													<select name="ses_produit" class="form-control" required >
														<option value="" >Formation ...</option>
											<?php		if(!empty($d_produits)){
															foreach($d_produits as $p){
																if($p["cpr_id"]==$session['ses_produit']){
																	echo("<option value='" . $p["cpr_id"] . "' selected >" . $p["cpr_libelle"] . "</option>");
																}else{
																	echo("<option value='" . $p["cpr_id"] . "' >" . $p["cpr_libelle"] . "</option>");
																}
															}
														} ?>
													</select>												
												</div>
											</div>
											<div class="row mt15" >												
												<div class="col-md-2 mt10 text-right" >Lieu :</div>
												<div class="col-md-4" >
													<input type="text" name="ses_lieu" class="gui-input gui-input-sm nom" placeholder="" 
													<?php if(isset($_GET['id'])){ ?>
														value="<?= $session['ses_lieu'] ?>"
													<?php } ?> required/>
												</div>												
											</div>
											
											
											<div class="row mt25">
												<div class="col-md-2 mt10 text-right" >Feuilles de présence :</div>
												<div class="col-md-10 ">
													<span class="field prepend-icon file">
														<span class="button btn-primary">Choisir</span>
															<input id="file_presence" type="file" name="file_presence" class="gui-file">
															<input type="text" class="gui-input" id="uploader_presence" placeholder="Sélectionner le fichier PDF" />
															<span class="field-icon">
																<i class="fa fa-upload"></i>
															</span>
														</span>
													</label>
												</div>
												<!--<div class="col-md-6 ">
													<label class="text-left" for="file_attestation" >Attestations :</label>
													<span class="field prepend-icon file">
														<span class="button btn-primary">Choisir</span>
															<input id="file_attestation" type="file" name="file_attestation" class="gui-file">
															<input type="text" class="gui-input" id="uploader_attestation" placeholder="Sélectionner le fichier" />
															<span class="field-icon">
																<i class="fa fa-upload"></i>
															</span>
														</span>
													</label>
												</div>-->
											</div>
											<h2 class="text-center" >Liste des  <b class="text-primary">sessions</b></h2>
											<div class="row mt25">
												<div class="col-md-12">												
													<table class="table" id="session" >
														<thead>
															<tr>
																<th>Heure de début</th>
																<th>Heure de fin</th>															
															</tr>
														</thead>
														<tbody>
											<?php			$nbH=0;
															if(!empty($d_h)){
																foreach($d_h as $h){ 
																	$nbH++;?>
																	<tr>
																		<td>
																			<input type="hidden" name="sho_id_<?=$nbH?>" value="<?=$h['sho_id']?>" />
																			<input type="text" name="sho_h_deb_<?=$nbH?>" class="gui-input gui-input-sm heure" placeholder="" value="<?= $h['sho_h_deb'] ?>" />
																		</td>
																		<td>
																			<input type="text" name="sho_h_fin_<?=$nbH?>" class="gui-input gui-input-sm heure" placeholder="" value="<?= $h['sho_h_fin'] ?>" />
																		</td>
																	</tr>
											<?php 				} 
															} 
															$nbH++; ?>
															<tr>
																<td>
																	<input type="hidden" name="sho_id_<?=$nbH?>" value="0" />
																	<input type="text" name="sho_h_deb_<?=$nbH?>" class="gui-input gui-input-sm heure" placeholder="" value="" />
																</td>
																<td>
																	<input type="text" name="sho_h_fin_<?=$nbH?>" class="gui-input gui-input-sm heure" placeholder="" value="" />
																</td>
															</tr>
														</tbody>
													<table>
													<div>
														<input type="hidden" name="nbH" id="nbH" value="<?=$nbH?>" />
													</div>
													<small>Effacer les horaires pour supprimer une session</small>
												</div>							
											</div>	
											<div class="row mt25">
												<div class="col-md-12">
													<button type="button" id="add_session" class="btn btn-sm btn-success" >
														<i class="fa fa-plus" ></i>Ajouter une session
													</button>
												</div>
											</div>
										</div>
									<!-- FIN COL-MD-10 -->
									</div>
								<!-- FIN PANEL BODY -->
								</div>
							<!-- FIN PANEL HEADING -->
							</div>
						<!-- FIN ADMIN FORM -->
						</div>
					</div>

				</section>

			</section>

		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
		<?php		if(!empty($_SESSION["retour_formation"])){
						$url_retour=$_SESSION["retour_formation"];
					}else{
						$url_retour="session_liste.php";	
					} ?>
					<a href="session_liste.php" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
								Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle">
					<div class="progress" style="margin-bottom:0; margin-top:7px;">

						<div class="progress-bar progress-bar-success progress-bar-striped bar" role="progressbar" aria-valuemin="0" aria-valuemax="100">
							<span class="percent text-center"></span>
						</div>
					</div>
				</div>
				<div class="col-xs-3 footer-right">
					
					<button type="submit" name="submit" id="upload-btn" class="btn btn-success btn-sm">
						<i class='fa fa-save'></i> Enregistrer
					</button>
					<button type="button" id="cancel-btn" class="btn btn-danger btn-sm" style="display:none;">
						<i class='fa fa-times'></i> Annuler
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php 
	include "includes/footer_script.inc.php"; ?>  

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<!-- SCRIPT SELECT2 -->
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script src="vendor/plugins/upload/jquery.form.js"></script>

<script type="text/javascript">

	jQuery(document).ready(function(){	

		//////// ÉVENEMENTS UTILISATEURS //////////
		$("#file_presence").change(function() {
			$("#uploader_presence").val($(this).val());
		});
		$("#file_attestation").change(function() {
			$("#uploader_attestation").val($(this).val());
		});
		
		$("#add_session").click(function(){
			console.log("TEST");
			nbH=$("#nbH").val();
			nbH=1*nbH+1;
			html="<tr>";
			html=html + "<td>";
					html=html + "<input type='hidden' name='sho_id_" + nbH + "' value='0' />";
					html=html + "<input type='text' name='sho_h_deb_" + nbH + "' class='gui-input gui-input-sm heure' placeholder='' value='' />";
				html=html + "</td>";
				html=html + "<td>";
					html=html + "<input type='text' name='sho_h_fin_" + nbH + "' class='gui-input gui-input-sm heure' placeholder='' value='' />";
				html=html + "</td>";
			html=html + "</tr>";
			
			$("#session > tbody:last").append(html);
			
			console.log("TEST");
			
			$("#nbH").val(nbH);
		});
		
	});
	
	
// upload du fichier
    /*$(function() {

        var bar = $('.bar');
        var percent = $('.percent');
        var status = $('#status');
        $(".progress").hide();

		
        $('form').ajaxForm({

            timeout: 10000000000000,

            beforeSend: function(xhr) {

                status.empty();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.text(percentVal);
                $( "#cancel-btn" ).show();
                $( "#upload-btn" ).hide();
                $( "#cancel-btn" ).click(function() {
                    xhr.abort();
                    $( "#cancel-btn" ).hide();
                    $( "#upload-btn" ).show();
                });

            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.text(percentVal);
                $(".progress").show();
                $( "#cancel-btn" ).show();
                $( "#upload-btn" ).hide();

            },
            complete: function(xhr) {
                $( "#cancel-btn" ).hide();
                $( "#upload-btn" ).show();

                status.html(xhr.responseText);
                if (status.text() == 0) {
                    $(".progress").show();
                    var delay = 1000; //Your delay in milliseconds

					setTimeout(function(){ 
				<?php 	if(!empty($_GET['id'])){ ?>
							window.location = "session_liste.php?succes=2";
				<?php 	}else{ ?>
							window.location = "session_stagiaire.php?succes=1";
				<?php 	}?>
					}, delay);
					status.hide();

                }else{
                    $(".progress").hide();
                }
            }

        });


    }); */

    //////// FIN ÉVENEMENTS UTILISATEURS //////
</script>

</body>
</html>