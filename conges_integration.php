<?php

$menu_actif = "3-2";

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';

$sql="SELECT * FROM societes WHERE soc_archive=0 ";
$req = $Conn->query($sql);
$societes = $req->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Paramètres</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
     <form action="conges_integration_enr.php" method="POST" id="admin-form">
    <!-- Start: Main -->
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>

       
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">
            
            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-center">

                                        <div class="content-header">
                                            <h2>Générer CSV <b class="text-primary">congés</b></h2>

                                        </div>

                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="section">
                                                        <label for="fpr_type" class="text-left">Société :</label>
                                                        <select name="con_societe" id="con_societe" class="select2 select2-societe" >
                                                            
                                                            <?php foreach($societes as $s){ ?>
                                                                <option value="<?= $s['soc_id'] ?>"><?= $s['soc_code'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <label for="fpr_type" class="text-left">Agence :</label>
                                                        
                                                    <select name="con_agence" id="con_agence" class="select2 select2-agence select2-societe-agence" required>
                                                        <option value="0">Agence...</option>
                                                    </select>
                                                </div>
                                            </div>

                                    </div>
                              </div>


                          </div>
                      </div>

                  </div>
              </div>

          </div>
      </section>
      <!-- End: Content -->
  </section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="parametre.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-save'></i> Générer CSV
            </button>
        </div>
    </div>
</footer>
<?php
	include "includes/footer_script.inc.php"; ?>	

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
</form>
</body>
</html>
