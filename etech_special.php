<?php

	$menu_actif = "4-3";
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	// TOUS LES THEMES
	$sql="SELECT uti_id, uti_prenom,uti_nom, uti_societe, uti_agence 
 FROM utilisateurs 
LEFT OUTER JOIN documents_utilisateurs ON (utilisateurs.uti_id = documents_utilisateurs.dut_utilisateur)
LEFT JOIN documents ON (documents.doc_id = documents_utilisateurs.dut_document)
WHERE uti_archive = 0 AND dut_obligatoire = 1 AND (dut_vu = 0 OR ISNULL(dut_vu) ) GROUP BY uti_id ORDER BY uti_id;";
$req=$Conn->query($sql);
$utilisateurs = $req->fetchAll();
	

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Documents non consultés</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		 <link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link href="assets/admin-tools/admin-forms/css/admin-forms-orion.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php	
				include "includes/header_def.inc.php";
			?>	
				
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
				
	            
	                		
				<section id="content" class="animated fadeIn">
				<h1>Utilisateurs qui ont des documents obligatoires non consultés</h1>
					<div class="row" >
						<div class="col-md-12" >
							<div class="table-responsive" >
								<table class="table table-hover table-striped" data-tri_col="1" data-tri_sens="asc" id="tableUtiFix">
									<thead>
										<tr class="dark" >
											<th>Prénom</th>											
											<th>Nom</th>
											<th>Société</th>
											<th>Agence</th>											
											<th class="no-sort">Action</th>
										</tr>															
									</thead>
									<tbody>
								<?php												
										if(!empty($utilisateurs)){										
											foreach($utilisateurs as $u){  
												if(!empty($u['uti_agence'])){
													$sql="SELECT age_nom FROM agences WHERE age_id=" . $u['uti_agence'];
													$req=$Conn->query($sql);
													$agence=$req->fetch();
												}else{
													$agence="";
												}
												$sql="SELECT soc_nom FROM societes WHERE soc_id=" . $u['uti_societe'];
												$req=$Conn->query($sql);
												$societe=$req->fetch();
												?>
												<tr>												
													<td><?=$u["uti_prenom"]?></td>
													<td><?= $u['uti_nom'] ?></td>
													<td><?= $societe['soc_nom'] ?></td>
													<td>
													<?php if(!empty($agence['age_nom'])){ ?>
														<?= $agence['age_nom'] ?>
													<?php } ?>
													</td>																										
													<td class="text-center" >
														<a href="etech_special_liste.php?id=<?= $u["uti_id"] ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" title="Voir les documents non consultés de l'utilisateur" >
															<i class="fa fa-eye"></i>
														</a>
													</td>									
												</tr>	
								<?php		
											};
										}; ?>																			
									</tbody>
								</table>
							</div>
						</div>
					</div>
	
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					
				</div>
				<div class="col-xs-6">
					
				</div>
				<div class="col-xs-3 footer-right" >
					
				</div>
			</div>
		</footer>
		
	<?php
		include "includes/footer_script.inc.php"; ?>	                        

		<script src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script type="text/javascript">
			tri_col=$("#tableUtiFix").data("tri_col");
			tri_sens=$("#tableUtiFix").data("tri_sens");
			$('#tableUtiFix').dataTable({
            "language": {
                "url": "/vendor/plugins/DataTables/media/js/French.json"
            },
			
            paging: false,
            searching: false,
			"order":[tri_col,tri_sens],
            info: false,
            columnDefs: [
  				{ targets: 'no-sort', orderable: false }
			]
        });
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
