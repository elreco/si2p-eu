 <?php
    include "includes/controle_acces.inc.php";
    include('includes/connexion.php');
    include('modeles/mod_add_notification.php');
    //////////////////////////////////////////////////////////////
    ///////////// SI C'EST LE DEMANDEUR QUI VALIDE ///////////////
    //////////////////////////////////////////////////////////////

    if (isset($_GET['demandeur'])) {
        // changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("UPDATE ndf SET ndf_valide = NOW(), ndf_statut = 1 WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();
        // update depassements
        $req = $Conn->prepare("SELECT * FROM ndf_lignes WHERE nli_ndf = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();
        $nlis = $req->fetchAll();
        foreach ($nlis as $nl) {
            $req = $Conn->prepare("UPDATE ndf_lignes SET nli_depassement_accorde = NULL, nli_depassement_valide = :nli_depassement_valide WHERE nli_id = :nli_id");
            $req->bindParam(':nli_id', $nl['nli_id']);
            $req->bindParam(':nli_depassement_valide', $nl['nli_depassement_valide']);
            $req->execute();
        }
        // fin changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();
        $ndf = $req->fetch();
        // fin changer le statut de la ndf et mettre à jour la date de validation
        // chercher le responsable
        $req = $Conn->prepare("SELECT uti_responsable, uti_nom, uti_prenom, uti_societe, uti_agence FROM utilisateurs WHERE uti_id = :uti_id");
        $req->bindParam(':uti_id', $_GET['demandeur']);
        $req->execute();
        $uti = $req->fetch();
        // fin
        // notifications au dr
        if  (!empty($ndf['ndf_ra'])) {
            $noti_utilisateur = $ndf['ndf_ra'];
        }else {
            $noti_utilisateur = $ndf['ndf_re'];
        }
        $_SESSION['message'][] = array(
            "titre" => "Note de frais",
            "type" => "success",
            "message" => "Vous avez bien clôturé votre note de frais, elle est en attente de validation."
        );
        add_notifications("<i class='fa fa-file-o'></i>", "La note de frais de " . $uti['uti_prenom'] . " " . $uti['uti_nom'] . " doit être validée", "bg-info", "ndf.php?id=" . $_GET['id'], "", "", $noti_utilisateur, $uti['uti_societe'], $uti['uti_agence'], 0);
        // fin notifications
        Header("Location: ndf.php?id=" . $_GET['id']);
        die();
    }



    /////////////////////////////////////////////////////////////
    /////////////////SI C'EST LE RESP. EXPLOITATION QUI VALIDE //////////////////
    /////////////////////////////////////////////////////////////

    if (isset($_GET['re'])) {

        // changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("UPDATE ndf SET ndf_re = :ndf_re,ndf_re_valide= NOW(), ndf_statut = 3 WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->bindParam(':ndf_re', $_GET['re']);
        $req->execute();

        // fin changer le statut de la ndf et mettre à jour la date de validation

        // fin changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();
        $ndf = $req->fetch();

        $req = $Conn->prepare("SELECT uti_nom, uti_prenom, uti_societe, uti_agence FROM utilisateurs WHERE uti_id = :uti_id");
        $req->bindParam(':uti_id', $ndf['ndf_utilisateur']);
        $req->execute();
        $uti = $req->fetch();

        // notifications a compta
        add_notifications("<i class='fa fa-file-o'></i>", "La note de frais de " . $uti['uti_prenom'] . " " . $uti['uti_nom'] . " doit être validée", "bg-info", "ndf.php?id=" . $_GET['id'], "8,11", "", "", $uti['uti_societe'], $uti['uti_agence'], 0);
        // fin notifications
        $_SESSION['message'][] = array(
            "titre" => "Note de frais",
            "type" => "success",
            "message" => "Vous avez bien validé la note de frais."
        );
        Header("Location: ndf.php?id=" . $_GET['id']);
        die();
    }

    /////////////////////////////////////////////////////////////
    /////////////////SI C'EST LE RESP. AGENCE QUI VALIDE //////////////////
    /////////////////////////////////////////////////////////////

    if (isset($_GET['ra'])) {

        // changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("UPDATE ndf SET ndf_ra = :ndf_ra,ndf_ra_valide= NOW(), ndf_statut = 2 WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->bindParam(':ndf_ra', $_GET['ra']);
        $req->execute();

        // fin changer le statut de la ndf et mettre à jour la date de validation

        // fin changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();
        $ndf = $req->fetch();

        $req = $Conn->prepare("SELECT uti_nom, uti_prenom, uti_societe, uti_agence FROM utilisateurs WHERE uti_id = :uti_id");
        $req->bindParam(':uti_id', $ndf['ndf_utilisateur']);
        $req->execute();
        $uti = $req->fetch();

        // notifications a compta
        add_notifications("<i class='fa fa-file-o'></i>", "La note de frais de " . $uti['uti_prenom'] . " " . $uti['uti_nom'] . " doit être validée", "bg-info", "ndf.php?id=" . $_GET['id'], "", "", $ndf['ndf_re'], $uti['uti_societe'], $uti['uti_agence'], 0);
        // fin notifications
        $_SESSION['message'][] = array(
            "titre" => "Note de frais",
            "type" => "success",
            "message" => "Vous avez bien validé la note de frais."
        );
        Header("Location: ndf.php?id=" . $_GET['id']);
        die();
    }

    /////////////////////////////////////////////////////////////
    ////////////////SI C'EST LE RH QUI VALIDE ///////////////////
    /////////////////////////////////////////////////////////////

    if (isset($_GET['rh'])) {


        // changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("UPDATE ndf SET ndf_rh = :ndf_rh,ndf_rh_valide= NOW(), ndf_statut = 5 WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->bindParam(':ndf_rh', $_GET['rh']);
        $req->execute();

        // fin changer le statut de la ndf et mettre à jour la date de validation

        // fin changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();
        $ndf = $req->fetch();

        $req = $Conn->prepare("SELECT uti_nom, uti_prenom, uti_societe, uti_agence FROM utilisateurs WHERE uti_id = :uti_id");
        $req->bindParam(':uti_id', $ndf['ndf_utilisateur']);
        $req->execute();
        $uti = $req->fetch();

        // notifications a compta
        add_notifications("<i class='fa fa-file-o'></i>", "La note de frais de " . $uti['uti_prenom'] . " " . $uti['uti_nom'] . " est maintenant validée RH", "bg-info", "ndf.php?id=" . $_GET['id'], "8,11", "", "", $uti['uti_societe'], $uti['uti_agence'], 0);
        // fin notifications
        $_SESSION['message'][] = array(
            "titre" => "Note de frais",
            "type" => "success",
            "message" => "La note de frais est validée RH."
        );
        Header("Location: ndf.php?id=" . $_GET['id']);
        die();
    }

    /////////////////////////////////////////////////////////////
    ////////////////SI C'EST LA COMPTA QUI VALIDE ///////////////
    /////////////////////////////////////////////////////////////

    if (isset($_GET['compta'])) {


        // changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("UPDATE ndf SET ndf_compta = :ndf_compta,ndf_compta_valide= NOW(), ndf_statut = 4 WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->bindParam(':ndf_compta', $_GET['compta']);
        $req->execute();

        // fin changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();
        $ndf = $req->fetch();

        $req = $Conn->prepare("SELECT uti_nom, uti_prenom, uti_societe, uti_agence FROM utilisateurs WHERE uti_id = :uti_id");
        $req->bindParam(':uti_id', $ndf['ndf_utilisateur']);
        $req->execute();
        $uti = $req->fetch();

        // notifications a compta
        add_notifications("<i class='fa fa-file-o'></i>", "La note de frais de " . $uti['uti_prenom'] . " " . $uti['uti_nom'] . " doit être validée", "bg-info", "ndf.php?id=" . $_GET['id'], "7,12", "", "", $uti['uti_societe'], $uti['uti_agence'], 0);
        // fin notifications
        $_SESSION['message'][] = array(
            "titre" => "Note de frais",
            "type" => "success",
            "message" => "La note de frais est validée compta."
        );
        Header("Location: ndf.php?id=" . $_GET['id']);
        die();
    }

    /////////////////////////////////////////////////////////////
    ////////////////SI C'EST VALIDE RH => EN COURS DE PAIEMENT ///////////////
    /////////////////////////////////////////////////////////////

    if (isset($_GET['paiement'])) {


        // changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("UPDATE ndf SET ndf_statut = 6 WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();

        // fin changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();
        $ndf = $req->fetch();

        $req = $Conn->prepare("SELECT uti_nom, uti_prenom, uti_societe, uti_agence FROM utilisateurs WHERE uti_id = :uti_id");
        $req->bindParam(':uti_id', $ndf['ndf_utilisateur']);
        $req->execute();
        $uti = $req->fetch();

        // notifications a compta
        add_notifications("<i class='fa fa-file-o'></i>", "Votre note de frais est maintenant en cours de paiement", "bg-info", "ndf.php?id=" . $_GET['id'], "", "", $ndf['ndf_utilisateur'], $uti['uti_societe'], $uti['uti_agence'], 0);
        // fin notifications
        $_SESSION['message'][] = array(
            "titre" => "Note de frais",
            "type" => "success",
            "message" => "La note de frais est en cours de paiement."
        );

        if (isset($_GET['retour_ndf'])) {
            Header("Location: ndf.php?id=" . $_GET['id']);
        } else {
            Header("Location: ndf_liste.php");
        }

        die();
    }


    if (isset($_GET['payee'])) {


        // changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("UPDATE ndf SET ndf_paiement= NOW(), ndf_statut = 7 WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();

        // fin changer le statut de la ndf et mettre à jour la date de validation
        $req = $Conn->prepare("SELECT * FROM ndf WHERE ndf_id = :ndf_id");
        $req->bindParam(':ndf_id', $_GET['id']);
        $req->execute();
        $ndf = $req->fetch();

        $req = $Conn->prepare("SELECT uti_nom, uti_prenom, uti_societe, uti_agence FROM utilisateurs WHERE uti_id = :uti_id");
        $req->bindParam(':uti_id', $ndf['ndf_utilisateur']);
        $req->execute();
        $uti = $req->fetch();

        // notifications a compta
        add_notifications("<i class='fa fa-file-o'></i>", "Votre note de frais est maintenant payée", "bg-success", "ndf.php?id=" . $_GET['id'], "", "", $ndf['ndf_utilisateur'], $uti['uti_societe'], $uti['uti_agence'], 0);
        // fin notifications
        $_SESSION['message'][] = array(
            "titre" => "Note de frais",
            "type" => "success",
            "message" => "La note de frais est payée."
        );

        if (isset($_GET['retour_ndf'])) {
            Header("Location: ndf.php?id=" . $_GET['id']);
        } else {
            Header("Location: ndf_liste.php");
        }
        die();
    }
    ?>