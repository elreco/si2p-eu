<?php

	include "includes/controle_acces.inc.php";

	include('includes/connexion.php');
	include('includes/connexion_soc.php');

	include "modeles/mod_parametre.php";
	include "modeles/mod_affacturage_fichier.php";

	$erreur_txt="";

	$remise=0;
	if(!empty($_POST["remise"])){
		$remise=intval($_POST["remise"]);
	}

	$aff_date=null;
	if(!empty($_POST["aff_date"])){
		$DT_aff_date=date_create_from_format('d/m/Y',$_POST["aff_date"]);
		if(!is_bool($DT_aff_date)){
			$aff_date=$DT_aff_date->format("Y-m-d");
		}
	}

	$d_factures=null;
	if(!empty($_POST["fac_id"])){
		$d_factures=$_POST["fac_id"];
	}

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	// ENREGISTREMENT

	if($remise==0 AND !empty($aff_date)){
			// AND !empty($d_factures)){ => critere supprimer pour pouvoir remettre uniquement des reg et ajustement 
			// risque de creer des remise vide mais pas de solution rapide => créer un listing des reglement a remettre comme pour les factures
			// (FG)

		// ON CREE LA REMISE

		$sql_rem="INSERT INTO Affacturages (aff_date) VALUES (:aff_date);";
		$req_rem=$ConnSoc->prepare($sql_rem);
		$req_rem->bindParam(":aff_date",$aff_date);
		try{
			$req_rem->execute();
			$remise=$ConnSoc->lastInsertId();
		}Catch(Exception $e){
			$erreur_txt="ADD Remise : " . $e->getMessage();
		}

		if(empty($erreur_txt)){

			// REQUETE PREP

			$sql_up_fac="UPDATE Factures SET fac_aff_remise=:remise WHERE fac_id=:facture;";
			$rem_up_fac=$ConnSoc->prepare($sql_up_fac);

			// on passe en revu les facture selectionne par l'utilisateur

			$mod_factures=array();

			if(!empty($d_factures)){
				foreach($d_factures as $fac){

					// on assoice la facture a la remise
					$rem_up_fac->bindParam(":remise",$remise);
					$rem_up_fac->bindParam(":facture",$fac);
					$rem_up_fac->execute();

					$mod_factures[]=array(
						"fac_id" => $fac
					);

				}
			}
			//if(!empty($mod_factures)){
				// FG cf ligne 38
				$fichier=affacturage_fichier($remise,$aff_date,$mod_factures);
				var_dump($fichier);
				if(!is_bool($fichier)){
					$erreur_txt=$fichier["texte"];
					
					// les factures qui n'ont pas été inclusent dans le fichier suite à une erreur ne doivent pas être lié à la remise
					if(!empty($fichier["factures"])){
						
						foreach($fichier["factures"] as $fac_bug){
							
							$rem_up_fac->bindValue(":remise",0);
							$rem_up_fac->bindParam(":facture",$fac_bug);
							$rem_up_fac->execute();
							
						}
						
					}
					
				}
			//}

		}
	}else{
		$erreur_txt="Formulaire incomplet!";
	}

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"aff" => "",
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);

	}

	header("Location: affacturage_liste.php");

?>
