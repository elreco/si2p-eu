<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";
include('modeles/mod_get_juridique.php');
include('modeles/mod_parametre.php');
include('includes/connexion.php');
include('includes/connexion_soc.php');

// VISUALISATION D'UN DEVIS

$erreur=0;

$eva_id=0;
if(!empty($_GET["id"])){
	$eva_id=intval($_GET["id"]);
}
$_SESSION['retour'] = "evac_voir.php?id=".$_GET['id'];

$acc_utilisateur=0;

if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
}

// recherche du devis
if($eva_id>0){

	$eva_pdf=false;
	$sql="SELECT * FROM Evacuations WHERE eva_id=" . $eva_id;
	$req=$Conn->query($sql);
	$eva=$req->fetch();

	$sql="SELECT cli_classification, cli_classification_categorie, cli_classification_type FROM Clients WHERE cli_id=" . $eva['eva_client'];
	$req=$Conn->query($sql);
	$d_client=$req->fetch();

	if(!empty($d_client["cli_classification"])){
		$sql="SELECT ccl_libelle FROM clients_Classifications WHERE ccl_id=" . $d_client["cli_classification"] . ";";
		$req=$Conn->query($sql);
		$d_classification=$req->fetch();
	}

	if(!empty($d_client["cli_classification_categorie"])){
		$sql="SELECT ccc_libelle FROM Clients_classifications_categories WHERE ccc_id=" . $d_client["cli_classification_categorie"] . ";";
		$req=$Conn->query($sql);
		$d_classification_categorie=$req->fetch();
	}

	if(!empty($d_client["cli_classification_type"])){
		$sql="SELECT cct_libelle FROM Clients_classifications_types WHERE cct_id=" . $d_client["cli_classification_type"] . ";";
		$req=$Conn->query($sql);
		$d_classification_type=$req->fetch();
	}
	/*if(!empty($eva)){

		$eva_pdf=file_exists("documents/Evacuations/Evac_" . $acc_societe . "/" . $eva["eva_id"] . ".pdf");

	}else{
		$erreur=1;
	}*/
}

if($erreur==0){

	// ADRESSE RESEAU
	$juridique=get_juridique($eva["eva_societe"],$eva["eva_agence"]);
	/*echo("<pre>");
		print_r($juridique);
	echo("</pre>");
	die();*/

	// CLASSIFICATION PRODUIT
}
$sql="SELECT * FROM Evacuations_Chronologies LEFT JOIN evacuations_chronologies_param ON (evacuations_chronologies_param.ecp_id = Evacuations_Chronologies.ech_chronologie) WHERE ech_evacuation=" . $eva_id . " ORDER BY ecp_position";
$req=$Conn->query($sql);
$chronologies =$req->fetchAll();


$sql="SELECT * FROM Evacuations_Bilans LEFT JOIN evacuations_bilans_param ON (evacuations_bilans_param.ebp_id = Evacuations_Bilans.ebi_bilan) WHERE ebi_evacuation=" . $eva_id. " AND ebi_position != 0 ORDER BY ebi_position";
$req=$Conn->query($sql);
$remarques =$req->fetchAll();
$sql="SELECT * FROM Evacuations_Chronologies_Param";
$req=$Conn->prepare($sql);
$req->execute();
$chronologies_param = count($req->fetchAll());
$pourcentage = (count($chronologies) * 100) /$chronologies_param;
// $sql="SELECT * FROM Evacuations_Valides  LEFT JOIN utilisateurs ON (utilisateurs.uti_id = evacuations_valides.evv_utilisateur)  WHERE evv_evacuation=". $eva_id;
// $req=$Conn->query($sql);
// $evacuation_valides =$req->fetchAll();
// $autorise_reouverture = 0;
// foreach($evacuation_valides as $ev){
// 	if($_SESSION['acces']['acc_ref_id'] == $ev['uti_id']){
// 		$autorise_reouverture = 1;
// 	}
// }
// IL FAUT PRENDRE LES VALIDANTS, A SAVOIR YANNICK ET L'ASSISTANTE DE REGION
// YANNICK
$sql="SELECT uti_id, uti_prenom, uti_nom FROM Utilisateurs WHERE uti_id = 198";
$req=$Conn->query($sql);
$yannick =$req->fetch();

// ASSISTANTE
$sql="SELECT uti_id, uti_prenom, uti_nom FROM Utilisateurs WHERE uti_id = " . $eva['eva_assist'];
$req=$Conn->query($sql);
$assist_region =$req->fetch();


$sql="SELECT * FROM evacuations_parametres";
$req=$Conn->prepare($sql);
$req->execute();
$parametre = $req->fetch();

$sql="SELECT uti_id, uti_prenom, uti_nom, uti_eva_pas_valide_tech FROM utilisateurs WHERE uti_id = " . $eva['eva_formateur'];
$req=$Conn->prepare($sql);
$req->execute();
$utilisateur = $req->fetch();


?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >


		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.2/rateit.min.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
		@media print
		{
		  /* table { break-after:always;overflow: hidden; }
		  tr    { page-break-inside:avoid;overflow: hidden; break-after:always!important; }
		  td    { page-break-inside:avoid; overflow: hidden;break-after:always!important; }
		  thead { display:table-header-group }
		  tfoot { display:table-footer-group }
		  table tr td {
			vertical-align: middle;
		  } */
		  /* Chrome 29+ */

		@media all and (-webkit-min-device-pixel-ratio:0) and (min-resolution: .001dpcm) {
			.selector:not(*:root),table { page-break-after:always;}
			.selector:not(*:root),tr    { page-break-inside:avoid;overflow: hidden; break-after:always!important; }
			.selector:not(*:root),td    { page-break-inside:avoid; overflow: hidden;break-after:always!important; }
			.selector:not(*:root),thead { display:table-header-group }
			.selector:not(*:root),tfoot { display:table-footer-group }
			.selector:not(*:root),table tr td {
				vertical-align: middle;
			}
		}

		  .page-headers, .page-headers-space {
			height: 100px;
		  }

		  .page-footers, .page-footers-space {
			height: 100px;

		  }

		  .page-footers {
			position: fixed;
			bottom: 0;
			width: 100%;
		  }

		  .page-headers {
			position: fixed;
			top: 0mm;
			width: 100%;
		  }

		  .page {
			break-after: always!important;
		  }
		}
			#zone_print{
				display:none;
				margin:0px!important;
			}


			#controleH{
				background-color:red;
				border:1px solid blue;
				break-after:always!important;
			}

			/* GENERAL */

			#container_print{
				width:21cm;
				margin:auto;
				padding:0.5cm; /* remplace marge d'impression */
				background-color:#fff;
			}

			#page_print{
				width:100%;
			}

			#page_print table{
				width:100%;
			}

			/* HEADER */

			#page_print header{

			}
			#page_print header .print-logo{
				width:4cm;
			}
			#page_print header img{
				max-width:4cm;
				max-height:2.5cm;
			}

			/* FOOTER */

			#page_print footer{
				font-size:7pt;
			}
			#page_print footer .print-logo{
				width:3cm;
			}
			#page_print footer img{
				max-width:3cm;
				max-height:2cm;
			}
			/* Styles go here */


			/* GRILLE PRINT */
			.print-col{
				display:inline-block;
				vertical-align:top;
			}
			.print-col-33{
				width:32%;
			}
			.print-col-50{
				width:49%;
			}
			.print-col-100{
				width:99%;
			}

			.ligne{
				border-bottom:1px solid #e31936;
				border-left:1px solid #e31936;
				border-right:1px solid #e31936;
				background-color:#FFF!important;
				width:100%;
			}
			.ligne>div{
				vertical-align:middle;
				border-left:1px solid #e31936;
				height:100%!important;
				font-size:9pt;
			}

			.table-div{
				border:1px solid #333;
			}
			.table-div > tr {
			  border-bottom: 1px solid #333!important;
			}
			.table-div > tr > td {
			  border-right: 1px solid #333!important;
			}
			.ligne-col-1{
				width:20%;
				border-left:0px!important;
				padding-left:5px;
			}
			.ligne-col-2{
				width:80%;
				padding:5px;
			}
			.ligne-col-3{
				width:10%;
				padding-right:5px;
			}
			.ligne-col-4{
				width:5%;
				padding-right:5px;
			}
			.ligne-col-5{
				width:13%;
				padding-right:5px;
			}
			.ligne-head{
				color:#FFF;
				background-color:#e31936!important;
			}
			.ligne-head > div{
				padding:5px;
			}
			.bodyp{
				margin-top:10px;
			}
			.degrade{background:#e31936};

		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
			html{
				background-color:#fff!important;
				margin:0px!important;
				padding:0px!important;;
			}
			body{
				background-color:#fff!important;
				margin:0px!important;;
				padding:0px!important;;
			}

		</style>
	</head>
	<body class="sb-top sb-top-sm document-affiche">
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">
		<?php 		if($erreur==0){ ?>

							<div class="row">

								<!-- zone d'affichage du devis -->

								<div class="col-md-8">

									<div id="container_print">

										<div id="page_print">

											<header class="page-headers" style="background:white;">
												<table>
													<tr>
														<td class="print-logo">
												<?php		if(!empty($juridique["logo_1"])){ ?>
																<img src="<?=$juridique["logo_1"]?>" />
												<?php		}else{
																echo("&nbsp;");
															} ?>
														</td>
														<td class="text-center"></td>
														<td class="print-logo text-right" >
												<?php		if(!empty($juridique["logo_2"])){ ?>
																<img src="<?=$juridique["logo_2"]?>" />
												<?php		}else{
																echo("&nbsp;");
															} ?>
														</td>
													</tr>
												</table>
											</header>

											<table>

											   <thead>
												 <tr>
												   <td>
													 <div class="page-headers-space"></div>
												   </td>
												 </tr>
											   </thead>
											   <tbody>
											      <tr>
											        <td>

													<h1 class="text-center degrade" style="color:white;margin:0;">RAPPORT D'EVACUATION</h1>
													<div class="border" style="margin-bottom: 400px; margin-top: 400px;border-top:1px solid #e31936;border-bottom:1px solid #e31936;">
														<div class="print-col print-col-50 text-center">

															<p style="margin: 9.5px 0 9.5px;">
													<?php		echo("<strong>" . $eva["eva_client_nom"] . "</strong>");
																echo("<br/> " . $eva["eva_adr_nom"]);
																echo("<br/> " . $eva["eva_adr_ad1"]);
																if(!empty($eva["eva_adr_ad2"])){
																	echo("<br/>" . $eva["eva_adr_ad2"]);
																}
																if(!empty($eva["eva_adr_ad3"])){
																	echo("<br/>" . $eva["eva_adr_ad3"]);
																}
																echo("<br/>" . $eva["eva_adr_cp"] . " " . $eva["eva_adr_ville"]);

																?>
															</p>

														</div>
														<div class="print-col print-col-50 text-center" >
															<p  style="margin: 9.5px 0 9.5px;">
															<?php
																if(!empty($eva["eva_contact_nom"])){
																	echo("Responsable Client : " . $eva["eva_contact_prenom"] . " " . $eva["eva_contact_nom"]);
																}
																if(!empty($eva["eva_for_nom"])){
																	echo("<br/> Exercice réalisé par : " . $eva["eva_for_nom"]);
																}
																if(!empty($eva["eva_com_nom"])){
																	echo("<br/> Affaire suivie par : " . $eva["eva_com_nom"]);
																}
																if(!empty($eva["eva_date_exer"])){
																	echo("<br/> Date de l'exercice : " . convert_date_txt($eva["eva_date_exer"]));
																}
																if(!empty($d_classification["ccl_libelle"])){
																	echo("<br/>" . $d_classification["ccl_libelle"]);
																}
																if(!empty($d_classification_categorie["ccc_libelle"])){
																	echo(" - " . $d_classification_categorie["ccc_libelle"]);
																}
																if(!empty($d_classification_type["cct_libelle"])){
																	echo(" <br> " . $d_classification_type["cct_libelle"]);
																}
																?>

															</p>
														</div>
													</div>
													<div class="page">
														<table>
															<tbody class="table-div ligne-body ligne-fac">
																<div class="ligne ligne-head text-left pl5 mt10 mb10">
																	<div class="ligne-col-1" >Thème de l'exercice</div>
																</div>
																<div class="text-left">
																	<?= nl2br($eva['eva_theme']) ?>
																</div>
															</tbody>
														</table>
														<table>
														<thead class="ligne-fac">
															<tr class="mt10 mb10">
																<div class="ligne ligne-head text-left pl5 mt10 mb10">
																	<div class="ligne-col-1">Chronologie</div>
																</div>
															</tr>
															<tr class="table-div" >
																<th class="table-div-th ligne-col-1 text-center" >Nom</th>
																<th class="table-div-th ligne-col-2 text-center" >Description</th>
															</tr>
														</thead>

															<tbody class="table-div ligne-body ligne-fac">
															<?php foreach($chronologies as $k=>$c){ ?>
																<tr >
																	<td class="ligne-col-1" >
																		<?= $c['ecp_libelle'] ?>
																	</td>
																	<td class="ligne-col-2" >
																		<?= nl2br(stripslashes($c['ech_valeur'])) ?>
																	</td>
																</tr>
															<?php } ?>
															</tbody>
														</table>
													</div>
											<div class="page">
												<div class="ligne ligne-head text-left pl5 mt10 mb10">
													<div class="ligne-col-1">Bilan</div>
												</div>
												<table>
												<thead class="ligne-fac">
													<tr class="table-div" >
														<th class="table-div-th ligne-col-1 text-center">Remarques</th>
														<th class="table-div-th ligne-col-2 text-center">Préconisations</th>
													</tr>
												</thead>

													<tbody class="table-div ligne-body ligne-fac">
													<?php
													foreach($remarques as $r){
														if(!empty($r['ebi_position'])){
														?>

															<tr>
																<td  class="ligne-col-1" ><?= nl2br(stripslashes($r['ebi_remarque'])) ?></td>

																<td  class="ligne-col-2" ><?= nl2br(stripslashes($r['ebi_preconisation'])) ?></td>
															</tr>

													<?php }
													}
													?>
													</tbody>
												</table>
											</div>
											<div class="page">
												<div class="mt10 mb10">
													<div class="ligne ligne-head text-left pl5" >
														<div class="ligne-col-1 pl5">Conclusion</div>
													</div>
												</div>
												<div class="ligne-fac">
														<?= nl2br(strip_tags($eva['eva_conclusion'])) ?>
												</div>
												<section>
													<?php if(!empty($eva['eva_date_termine'])){ ?>
														<p style="text-align: right">Le <?= convert_date_txt($eva['eva_date_termine']) ?></p>
													<?php }?>
												</section>
											</div>
										<!-- FIN PAGE PRINT -->
											</td>
										  </tr>
										</tbody>

											<tfoot>
											  <tr>
												<td>
												  <div class="page-footers-space"></div>
												</td>
											  </tr>
											</tfoot>

										</table>
										<footer  class="page-footers" style="background:white;">
											<table>
												<tr>
													<td class="print-logo" >
											<?php		if(!empty($juridique["qualite_1"])){ ?>
															<img src="<?=$juridique["qualite_1"]?>" class="img-responsive" />
											<?php		}else{
															echo("&nbsp;");
														} ?>
													</td>
													<td class="text-center" ><?=$juridique["footer"]?></td>
													<td class="text-right print-logo" >
											<?php		if(!empty($juridique["qualite_2"])){ ?>
															<img src="<?=$juridique["qualite_2"]?>" />
											<?php		}else{
															echo("&nbsp;");
														} ?>
													</td>
												</tr>
											</table>
										</footer>
									</div>
								</div>
							</div>
								<!-- fin zone d'affichage du devis -->

						<?php 	if($eva['eva_etat_valide'] == 2){ ?>
									<div class="col-md-4">
										<!-- DEBUT ANNEXE -->
										<div class="panel">
											<div class="panel-body">
												<div class="row">
													<div class="col-md-4 text-center">
														<a href="evac_chronologies.php?id=<?= $eva_id ?>" class="btn btn-success btn-sm">
															<i class="fa fa-plus"></i> Saisir la chrono
														</a>
													</div>
													<div class="col-md-4 text-center">
														<a href="evac_remarques.php?id=<?= $eva_id ?>" class="btn btn-success btn-sm">
															<i class="fa fa-plus"></i> Saisir les remarques
														</a>
													</div>
													<div class="col-md-4 text-center">
														<a href="evac_conclusion.php?id=<?= $eva_id ?>" class="btn btn-success btn-sm">
															<i class="fa fa-plus"></i> Saisir la conclusion
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
						<?php 	}
								if($eva['eva_etat_valide'] == 1 OR (!empty($eva['eva_tech_text']) OR !empty($eva['eva_assist_text']))){ ?>
									<div class="col-md-4">
										<div class="panel">
											<div class="panel-body">
												<h2>Validations</h2>
												<div class="row">
									<?php 			if($utilisateur['uti_eva_pas_valide_tech'] == 0 OR $eva['eva_demande_validation_tech']){ ?>
														<div class="col-md-12">

															<h5><?= $yannick['uti_prenom']  ?> <?= $yannick['uti_nom']  ?></h5>
													<?php	if(empty($eva['eva_tech_valide_date']) && $eva['eva_etat_valide'] == 1){
																if($yannick['uti_id'] == $_SESSION['acces']['acc_ref_id']){ ?>
																	<form action="evac_valide.php?id=<?= $eva_id ?>&utilisateur=<?= $eva['eva_tech'] ?>&societe=<?= $eva['eva_societe'] ?>&agence=<?= $eva['eva_agence'] ?>&tech" method="POST">
																		<button type="submit" name="valide" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Valider</button>
																		<button type="submit" name="retour" class="btn btn-sm btn-warning"><i class="fa fa-times"></i> Retour en validation</button>
																		<textarea class="form-control mt10" rows="5" name="eva_tech_text" placeholder="Votre commentaire"></textarea>
																	</form>
														<?php 	}else{ ?>
																	<p class="text-warning"> <i>Pas encore validé</i> </p>
														<?php 	}
															}else{
																if(!empty($eva['eva_tech_valide_date'])){ ?>
																	<p style="color:green;"><?= $yannick['uti_prenom']  ?> <?= $yannick['uti_nom']  ?><br>
																	<i>Le <?= convert_date_txt($eva['eva_tech_valide_date']) ?></i></p>
														<?php 	}else{?>
																	<p class="text-danger"><?= $yannick['uti_prenom']  ?> <?= $yannick['uti_nom']  ?><br>
																	<i>Retour en validation</i></p>
														<?php 	} ?>

													<?php	} ?>
													<?php if(!empty($eva['eva_tech_text'])){ ?>
														<br><p><?= $eva['eva_tech_text'] ?></p>
													<?php 			} ?>
														</div>
														<hr>

											<?php 	}elseif($utilisateur['uti_eva_pas_valide_tech'] == 1 && $eva['eva_etat_valide'] != 3){
														if(empty($eva['eva_demande_validation_tech'])){ ?>
															<form action="evac_demande_validation_tech.php?id=<?= $eva_id ?>" method="POST">
																<button type="submit" name="valide" class="btn btn-sm btn-info"><i class="fa fa-check"></i> Demander une validation technique</button>
															</form>
												<?php 	}
													}

													if(!empty($eva['eva_assist'])){ ?>

														<div class="col-md-12">

															<h5><?= $assist_region['uti_prenom']  ?> <?= $assist_region['uti_nom']  ?></h5>
												<?php		if(empty($eva['eva_assist_valide_date'])){
																if($assist_region['uti_id'] == $_SESSION['acces']['acc_ref_id']){ ?>
																	<form action="evac_valide.php?id=<?= $eva_id ?>&utilisateur=<?= $assist_region['uti_id'] ?>&societe=<?= $eva['eva_societe'] ?>&agence=<?= $eva['eva_agence'] ?>&assist" method="POST">
																		<button type="submit"  name="valide" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Valider</button>
																		<button type="submit" name="retour" class="btn  btn-sm btn-warning"><i class="fa fa-times"></i> Retour en validation</button>
																		<textarea class="form-control mt10" rows="5" name="eva_assist_text" placeholder="Votre commentaire"></textarea>
																	</form>
													<?php 		} else{ ?>
																	<p class="text-warning"> <i>Pas encore validé</i> </p>
													<?php 		}
															}else{
																if(!empty($eva['eva_assist_valide_date'])){ ?>
																	<p style="color:green;"><?= $assist_region['uti_prenom']  ?> <?= $assist_region['uti_nom']  ?><br>
																	<i>Le <?= convert_date_txt($eva['eva_assist_valide_date']) ?></i></p>
													<?php 		}else{ ?>
																	<p class="text-danger"><?= $assist_region['uti_prenom']  ?> <?= $assist_region['uti_nom']  ?><br>
																	<i>Retour en validation</i></p>
													<?php 		} ?>

													<?php   } ?>
													<?php 	if(!empty($eva['eva_assist_text'])){ ?>
															<br><p><?= $eva['eva_assist_text'] ?></p>
											<?php 		} ?>
											 			</div>
										<?php		} ?>
												</div>
											</div>
										</div>
									</div>
					<?php 		} ?>
								<div class="col-md-4">
									<!-- DEBUT ANNEXE -->
									<?php if($eva['eva_etat_valide'] == 1  && $_SESSION['acces']['acc_profil'] != 1){ ?>
									<div class="panel">
										<div class="panel-body">
											<h2 class="text-left">Informations de validation</h2>
											<div class="row text-center">

												<?php switch ($eva['eva_etat_controle']) {
				                       					case 1: ?>
				                       						<i class="fa fa-circle" style="color:green"></i> Complet
				                       					<?php	break;

				                       					case 2: ?>

				                       						<i class="fa fa-circle" style="color:orange"></i> Incomplet
				                       						<p>
				                       							<ul  style="list-style-type:none; padding:0">
				                       								<?php if(empty($eva['eva_controle_theme'])){ ?>
				                       									<li>Thème : <strong><?= strlen($eva['eva_theme']). "/" .$parametre['epa_theme_alert'] ?> caractères</strong></li>
				                       								<?php } ?>
				                       								<?php if(empty($eva['eva_controle_chrono'])){ ?>
				                       									<li>Chronologie : <strong><?= $pourcentage."/" . $parametre['epa_chrono_alert']  ?> %</strong></li>
				                       								<?php } ?>
				                       								<?php if(empty($eva['eva_controle_remarque'])){ ?>
				                       									<li>Remarque : <strong><?= count($remarques). "/" . $parametre['epa_remarque_alert'] ?> remarques</strong></li>
				                       								<?php } ?>
				                       								<?php if(empty($eva['eva_controle_conclusion'])){ ?>
				                       									<li>Conclusion : <strong><?= strlen($eva['eva_conclusion']). "/" . $parametre['epa_conclusion_alert'] ?> caractères</strong></li>
				                       								<?php } ?>
				                       							</ul>
				                       						</p>
				                       					<?php	break;
				                       					case 3: ?>
				                       						<i class="fa fa-circle" style="color:red"></i> Incomplet
				                       						<p>
																<ul  style="list-style-type:none; padding:0">
				                       								<?php if(empty($eva['eva_controle_theme'])){ ?>
				                       									<li>Thème : <strong><?= strlen($eva['eva_theme']). "/" .$parametre['epa_theme_alert'] ?> caractères</strong></li>
				                       								<?php } ?>
				                       								<?php if(empty($eva['eva_controle_chrono'])){ ?>
				                       									<li>Chronologie : <strong><?= $pourcentage."/" . $parametre['epa_chrono_alert']  ?> %</strong></li>
				                       								<?php } ?>
				                       								<?php if(empty($eva['eva_controle_remarque'])){ ?>
				                       									<li>Remarque : <strong><?= count($remarques). "/" . $parametre['epa_remarque_alert'] ?> remarques</strong></li>
				                       								<?php } ?>
				                       								<?php if(empty($eva['eva_controle_conclusion'])){ ?>
				                       									<li>Conclusion : <strong><?= strlen($eva['eva_conclusion']). "/" . $parametre['epa_conclusion_alert'] ?> caractères</strong></li>
				                       								<?php } ?>
				                       							</ul>
				                       						</p>
				                       					<?php	break;
				                       					case 0: ?>
															<i class="fa fa-circle" style="color:orange"></i> En cours de rédaction
				                       					<?php	break;
				                       				} ?>
											</div>
										</div>
									</div>
									<?php }?>
									<!-- DEBUT ANNEXE -->
									<?php if( $eva['eva_etat_controle'] == 1){ ?>
										<div class="panel">
											<div class="panel-body">
												<div class="row text-center">
													<h5>Note</h2>
													<div class="rateit" data-rateit-step="1"
														<?php if(!empty($eva['eva_avis'])){ ?>
															data-rateit-value="<?= $eva['eva_avis'] ?>"
														<?php }?>
															<?php
																if($_SESSION['acces']['acc_ref_id'] != 198){
															?>
															data-rateit-ispreset="true" data-rateit-readonly="true"
														<?php }?>
													></div>
												</div>
											</div>
										</div>

										<?php }?>

								</div>

		<?php		}else{ ?>

						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>

		<?php		} ?>

				</section>

				<!-- End: Content -->
			</section>


		</div>
		<!-- End: Main -->

		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="evac_liste.php" class="btn btn-default btn-sm">
					  <i class="fa fa-long-arrow-left"></i>
					  Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
					<?php if($eva['eva_etat_valide'] == 3){ ?>
						<button type="button" class="btn btn-sm btn-info ml15" id="print" >
							<i class="fa fa-print"></i> Imprimer
						</button>
					<?php }?>
				</div>
				<div class="col-xs-3 footer-right">
					<!-- <button type="button" class="btn btn-sm btn-success" id="btn_ligne_add" data-toggle="tooltip" title="Ajouter une nouvelle ligne" >
						<i class="fa fa-plus"></i> Produit
					</button> -->
				<?php if($eva['eva_etat_valide']==3){ ?>
					<!-- <a href="evac_pdf.php?id=<?=$eva['eva_id'] ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Enregistrer en pdf" >
						<i class="fa fa-file-pdf-o"></i> Télécharger le pdf
					</a> -->
				<?php } ?>

				<?php if($eva['eva_etat_valide'] == 2){ ?>
					<a href="evac.php?id=<?=$eva_id?>" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Modifier le rapport" >
						<i class="fa fa-pencil"></i> Modifier
					</a>
				<?php }?>

				<?php if($eva['eva_etat_valide']==2){ ?>

					<a href="evac_termine_enr.php?id=<?=$eva_id?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Terminer le rapport" >
						<i class="fa fa-check"></i> Terminer le rapport
					</a>
				<?php }?>
				<?php  if($eva['eva_etat_valide']==1 OR ($eva['eva_etat_valide'] == 3 && $_SESSION['acces']['acc_profil'] != 1)){ ?>
					<a href="evac_reouvrire_enr.php?id=<?=$eva_id?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="Réouvrir le rapport" >
						<i class="fa fa-folder"></i> Réouvrir le rapport
					</a>
				<?php }?>
				</div>
			</div>
		</footer>




<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.2/jquery.rateit.min.js"></script>
		<script type="text/javascript">
		var l_page=21;
		var h_page=29.7;
		var staticHeight = 0;
		var pageHeight = 27;

			jQuery(document).ready(function () {
				$('.rateit').bind('rated reset', function (e) {
			         var ri = $(this);

			         //if the use pressed reset, it will get value: 0 (to be compatible with the HTML range control), we could check if e.type == 'reset', and then set the value to  null .
			         var value = ri.rateit('value'); // if the product id was in some hidden field: ri.closest('li').find('input[name="productid"]').val()

			         //maybe we want to disable voting?
			         // ri.rateit('readonly', true);

					 $.ajax({
			             type: "post",
			             url: "ajax/ajax_evac_note.php",
			             data: "eva=<?= $eva['eva_id'] ?>&note=" + value,
			             dataType: "json",
			             success: function (data) {

			             }
			         });
			     });


				//mise_en_page();

				$("#print").click(function(){

					$("#zone_print").html($("#container_print").html());

					$("#main").hide();
					$("#content-footer").hide();
					$(".btn").hide();

					$("#zone_print").show();

					window.print();
					$("#zone_print").hide();

					$("#main").show();
					$("#content-footer").show();
					$(".btn").show();


				});


			});




		</script>
	</body>
</html>
