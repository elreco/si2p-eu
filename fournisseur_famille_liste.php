<?php
include "includes/controle_acces.inc.php";
include_once("includes/connexion.php");
// DROITS

// REQUETES
// FAMILLES
$req = $Conn->prepare("SELECT * FROM fournisseurs_familles ORDER BY ffa_libelle");
$req->execute();
$familles = $req->fetchAll();
// DROITS
$req = $Conn->prepare("SELECT * FROM droits");
$req->execute();
$droits = $req->fetchAll();

foreach($droits as $d){
	$droit[$d['drt_id']] = $d['drt_nom'];
}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php
include "includes/header_def.inc.php";
?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="dark" >
									<th>ID</th>
									<th>Libellé</th>
									<th>Accessible avec le droit</th>
									<th>Modifier</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($familles as $f): ?>
								<tr>
									<td><?= $f['ffa_id'] ?></td>
									<td><?= $f['ffa_libelle'] ?></td>
									<td>
									<?php if(!empty($f['ffa_droit'])){ ?>
									<?= $droit[$f['ffa_droit']] ?>
									<?php }else{ ?>
									Disponible pour tout le monde
									<?php } ?>
										
									</td>
									<td class="text-center" >
										<a href="fournisseur_famille.php?id=<?= $f['ffa_id'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier cette famille de fournisseur">
											<span class="fa fa-pencil"></span>
										</a>
									</td>
								</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" role="button" >
						<span class="fa fa-long-arrow-left"></span>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<a href="fournisseur_famille.php" class="btn btn-success btn-sm" role="button" >
						<span class="fa fa-plus"></span>
						Nouvelle famille de fournisseur
					</a>
				</div>
			</div>
		</footer>
<?php
		include "includes/footer_script.inc.php"; ?>	
		
	</body>
</html>
