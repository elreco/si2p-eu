<?php 
include "includes/controle_acces.inc.php";
include "includes/connexion.php";
include "modeles/mod_upload.php";
include "modeles/mod_parametre.php";

function checkIsAValidDate($myDateString){
    return (bool)strtotime($myDateString);
}

if(isset($_POST)){

	$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_id=" . $_POST['document']);
	$req->execute();
	$document = $req->fetch();
	// si le document est remplacé

	if(!empty($_FILES['doc'])){
		$nom = pathinfo($_FILES['doc']['name']); // nom du document (infos)

		
		// construction du nom du  document
		$nom_fichier = $document['fdo_id'] . "_" . clean($document['fdo_libelle']);

		// upload $erreur != 0 si erreur 
		// vérifier si le dossier du fournisseur exist , sinon le créer
		if (!file_exists('documents/fournisseurs/' . $_POST['fournisseur'] . '/intervenants/' . $_POST['intervenant'])) {
		    mkdir('documents/fournisseurs/' . $_POST['fournisseur'] . '/intervenants/' . $_POST['intervenant'], 0777, true);
		}
		$erreur = upload('doc', 'fournisseurs/' . $_POST['fournisseur'] . "/intervenants/" . $_POST['intervenant'] . "/",$nom_fichier, 0, 0, 1);
	}else{
	// sinon erreur = 0
		$erreur = 0;
	}


	$date = convert_date_sql($_POST['doc_date']);

		
		// si il n'y a pas d'erreur
		if($erreur == 0){

			// mise a jour fournisseur_fichier
			$fdo_libelle = clean($document['fdo_libelle']);
			if(!isset($_POST['remplacer'])){ // si ce n'est pas une édition
				
					$req = $Conn->prepare("INSERT INTO fournisseurs_fichiers (ffi_fournisseur, ffi_fichier,ffi_intervenant, ffi_nom, ffi_ext, ffi_date) VALUES (:ffi_fournisseur, :ffi_fichier,:ffi_intervenant, :ffi_nom, :ffi_ext, :ffi_date)");
					$req->bindParam(':ffi_fournisseur', $_POST['fournisseur']);
					$req->bindParam(':ffi_fichier', $_POST['document']);
					$req->bindParam(':ffi_nom', $fdo_libelle);
					$req->bindParam(':ffi_ext', $nom['extension']);
					$req->bindParam(':ffi_date', $date);
					$req->bindParam(':ffi_intervenant', $_POST['intervenant']);
					$req->execute();

				

				
				

			}else{
				if(!empty($_FILES['doc'])){ // si le fichier est remplacé, mettre à jour l'extension
					$req = $Conn->prepare("UPDATE fournisseurs_fichiers SET ffi_date = :ffi_date, ffi_ext = :ffi_ext WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_fichier = :ffi_fichier AND ffi_intervenant = :ffi_intervenant");
					$req->bindParam(':ffi_fournisseur', $_POST['fournisseur']);
					$req->bindParam(':ffi_fichier', $_POST['document']);
					$req->bindParam(':ffi_ext', $nom['extension']);
					$req->bindParam(':ffi_intervenant', $_POST['intervenant']);
					$req->bindParam(':ffi_date', $date);
					$req->execute();
				}else{
					$req = $Conn->prepare("UPDATE fournisseurs_fichiers SET ffi_date = :ffi_date,ffi_ext = :ffi_ext WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_fichier = :ffi_fichier");
					$req->bindParam(':ffi_fournisseur', $_POST['fournisseur']);
					$req->bindParam(':ffi_fichier', $_POST['document']);
					
					$req->bindParam(':ffi_date', $date);
					$req->bindParam(':ffi_intervenant', $_POST['intervenant']);
					$req->execute();
				}
				
			}
			/*///////////////////// MISE A JOUR ETAT DOCUMENTS FOURNISSEUR ///////////////////
			// mise a jour de l'état
			$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id=" . $_POST['fournisseur']);
			$req->execute();
			$fournisseur = $req->fetch();

			$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_type = 1"); // documents du fournisseur
			$req->execute();
			$documents = $req->fetchAll();
			
			$txt = "";
			// verif etat du document 1 par 1
			foreach($documents as $d){
				$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur=" . $_POST['fournisseur'] . " AND ffi_fichier = " . $d['fdo_id']);
				$req->execute();
				$fichier = $req->fetch();

				if(($d['fdo_obligatoire'] == 1 && empty($fichier)) OR ($d['fdo_periodicite_mois'] != 0 && date('Y-m-d') > date('Y-m-d', strtotime("+" . $d['fdo_periodicite_mois'] . " months", strtotime($fichier['ffi_date']))))){
					$statut = 1; // danger
				

				}elseif(($d['fdo_optionnel'] == 1 && empty($fichier)) OR ($d['fdo_periodicite_mois'] != 0 && date('Y-m-d') > date('Y-m-d', strtotime("+" . $d['fdo_periodicite_mois'] . " months", strtotime($fichier['ffi_date']))))){
					$statut = 2; // warning

				}else{
					$statut = 3; // succes
				}
				if($statut == 1 OR $statut == 2){
					if(empty($txt)){
						$txt = $d['fdo_libelle'] . "<br>";
					}else{
						$txt .= $d['fdo_libelle'] . "<br>";
					}
				}else{
					$txt = "";
				}
				

			}

			if(!empty($statut)){
				$req = $Conn->prepare("UPDATE fournisseurs SET fou_admin_etat = :fou_admin_etat, fou_admin_txt = :fou_admin_txt WHERE fou_id = :fou_id");
				$req->bindParam(':fou_id', $_POST['fournisseur']);
				$req->bindParam(':fou_admin_etat', $statut);
				$req->bindParam(':fou_admin_txt', $txt);
				$req->execute();
			}
			///////////////////// FIN MISE A JOUR ETAT DOCUMENTS FOURNISSEUR ///////////////////*/

		}else{
			echo $erreur;
		}



	


}



?>