<?php

include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include 'modeles/mod_parametre.php';

// CHECK LE PARAMETRE
if(empty($_GET['utilisateur'])){
	echo("Probleme");
	die();
}

$utilisateur = intval($_GET['utilisateur']);
if(empty($utilisateur)){
	echo("Probleme");
	die();
}
// FIN CHECK LES PARAMETRES
// LES SERVICES

$req=$Conn->query("SELECT ser_id,ser_libelle FROM Services ORDER BY ser_libelle;");
$d_services=$req->fetchAll();

asort($base_population);

// LES SOCIETES
$req=$Conn->query("SELECT soc_id,soc_nom FROM Societes WHERE soc_archive=0 ORDER BY soc_nom;");
$d_societes=$req->fetchAll();

// LES PROFILS
$req=$Conn->query("SELECT pro_id,pro_libelle FROM Profils ORDER BY pro_libelle;");
$d_profils=$req->fetchAll();
?>
<!DOCTYPE html>
<html lang="fr" >
<head>
    <meta charset="utf-8">
    <title>SI2P - Orion - Utilisateur</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
	
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>

<body class="sb-top sb-top-sm ">
	
	<form action="utilisateur_cree_enr.php" id="admin-form" method="post" enctype="multipart/form-data" >
		<div id="main">
<?php		include "includes/header_def.inc.php";?>
			
			<section id="content_wrapper" class="">
				
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-12">
							<div class="admin-form theme-primary admin-form-label">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="content-header">
											<h2>Nouvel <b class="text-primary">utilisateur</b></h2>
										</div>
										<div class="col-md-12">																												
											<div class="row">									
												<!-- colone de gauche -->
												<div class="col-md-6 bg-light dark">	
													<div class="row" >
													
														<div class="col-md-12" >
															<div class="section-divider mb40" >
																<span>Informations générales</span>
															</div>
														</div>
														
													</div>
													<div class="row" >
													
														<div class="col-md-4" >
															<div class="section">
																<label class="field select">
																	<select name="uti_titre" id="uti_titre" >
																		<option value="">Civilité...</option>
															<?php		foreach($base_civilite as $cle => $valeur){
																			if($cle>0){
																				echo("<option value='" . $cle . "' >" . $valeur . "</option>");
																			};																
																		}; ?>																																	
																	</select>
																	<i class="arrow simple"></i>
																</label>
															</div>
														</div>
														<div class="col-md-4">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="text" name="uti_nom" id="uti_nom" class="gui-input nom" placeholder="Nom" required >
																	<label for="uti_nom" class="field-icon">
																		<i class="fa fa-user"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="col-md-4">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="text" name="uti_prenom" id="uti_prenom" class="gui-input prenom" placeholder="Prénom" required >
																	<label for="uti_prenom" class="field-icon">
																		<i class="fa fa-user"></i>
																	</label>
																</div>
															</div>
														</div>
														
													</div>
													
													<div class="row">
														<div class="col-md-12">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="text" name="uti_ad1" id="uti_ad1" class="gui-input" placeholder="Adresse" >
																	<label for="uti_ad1" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="text" name="uti_ad2" class="gui-input" id="uti_ad2" placeholder="Adresse (Complément 1)">
																	<label for="uti_ad2" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="text" name="uti_ad3" id="uti_ad3" class="gui-input" placeholder="Adresse (Complément 2)">
																	<label for="uti_ad3" class="field-icon">
																		<i class="fa fa-map-marker"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>
													
													<div class="row">
														<div class="col-md-3">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="text" id="uti_cp" name="uti_cp" class="gui-input code-postal" placeholder="Code postal" >
																	<label for="uti_cp" class="field-icon">
																		<i class="fa fa fa-certificate"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="col-md-9">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="text" name="uti_ville" id="uti_ville" class="gui-input nom" placeholder="Ville" >
																	<label for="uti_ville" class="field-icon">
																		<i class="fa fa fa-building"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<!-- colone de droite -->
												<div class="col-md-6">	
												
													<!-- contact perso -->
													<div class="row">
														<div class="col-md-12">
															<div class="section-divider mb40" >
																<span>Contact perso</span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="tel" name="uti_tel_perso" id="uti_tel_perso"  class="gui-input telephone" placeholder="Numéro de téléphone" >
																	<label for="uti_tel_perso" class="field-icon">
																		<i class="fa fa fa-phone"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="tel" name="uti_mobile_perso" id="uti_mobile_perso" class="gui-input telephone" placeholder="Numéro de mobile" >
																	<label for="uti_mobile_perso" class="field-icon">
																		<i class="fa fa-mobile"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>
													<div class="row">												
														<div class="col-md-12">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="email" name="uti_mail_perso" id="uti_mail_perso" class="gui-input" placeholder="Adresse Email" >
																	<label for="uti_mail" class="field-icon">
																		<i class="fa fa-envelope"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>
															
													<!-- contact pro -->		
													<div class="row">
														<div class="col-md-12">
															<div class="section-divider mb40" >
																<span>Contact pro</span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="tel" name="uti_tel" id="uti_tel" class="gui-input telephone" placeholder="Numéro de téléphone" >
																	<label for="uti_tel" class="field-icon">
																		<i class="fa fa fa-phone"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="section">
																<div class="field prepend-icon">
																	<input name="uti_fax" id="uti_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax" >
																	<label for="uti_fax" class="field-icon">
																		<i class="fa fa-fax"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>														
													<div class="row">
														<div class="col-md-6">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="tel" name="uti_mobile" id="uti_mobile" class="gui-input telephone" placeholder="Numéro de mobile" >
																	<label for="uti_mobile" class="field-icon">
																		<i class="fa fa-mobile"></i>
																	</label>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="section">
																<div class="field prepend-icon">
																	<input type="email" name="uti_mail" id="uti_mail" class="gui-input" placeholder="Adresse Email" >
																	<label for="uti_mail" class="field-icon">
																		<i class="fa fa-envelope"></i>
																	</label>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- colone de droite -->
											</div>
											
											<!-- BAS DU FORM -->
											
											<!-- Connexion -->
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40" >
														<span>Données de connexion</span>
													</div>
												</div>
											</div>
											<div class="row" >
												<div class="col-md-6" >
													<div class="section">
														<label for="acc_uti_ident" >Identifiant :</label>
														<input type="text" name="acc_uti_ident" id="acc_uti_ident" class="gui-input" placeholder="Identifiant" >																																		
													</div>
												</div>
												<div class="col-md-5" >
													<div class="section">	
														<label for="acc_uti_passe" >Mot de passe :</label>
														<input type="text" name="acc_uti_passe" id="acc_uti_passe" class="gui-input" placeholder="Mot de passe" >																																		
													</div>
												</div>	
												<div class="col-md-1 pt20" >	
													<button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Générer un mot de passe" id="genere_passe" >
														<i class="fa fa-refresh" ></i>
													</button>
												</div>
											</div>
													
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40" >
														<span>Données Administratives</span>
													</div>
												</div>
											</div>
											
											<div class="row">							
												<div class="col-md-4" >
													<div class="section">	
														<label for="uti_matricule" >Matricule :</label>
														<input type="text" name="uti_matricule" id="uti_matricule" class="gui-input" placeholder="Matricule" >																																		
													</div>
												</div>
												<div class="col-md-4">
													<div class="section">
														<label for="uti_societe" >Société :</label>
														<div class="select">
															<select id="uti_societe" name="uti_societe" class="select2-societe" required data-selecteur="#uti_agence" >
																<option value="">Sélectionner la société...</option>
													<?php		if(!empty($d_societes)){
																	foreach($d_societes as $s){
																		echo("<option value='" . $s["soc_id"] . "' >" . $s["soc_nom"] . "</option>");
																	}
																} ?>															
															</select>
															<i class="arrow simple"></i>
														</div>
													</div>
												</div>                                           
												<div class="col-md-4">
													<div class="section">
														<label for="uti_societe" >Agence :</label>
														<select id="uti_agence" name="uti_agence" class="select2" >
															<option value="">Sélectionner une agence...</option>
														</select>
													</div>
												</div>												
											</div>
											<div class="row">											
												<div class="col-md-4" >
													<div class="section">
														<label for="uti_profil" >Profil :</label>
														<div class="select">
															<select id="uti_profil" name="uti_profil" required >
																<option value="" >Sélectionner un profil...</option>
													<?php		if(!empty($d_profils)){
																	foreach($d_profils as $p){
																		echo("<option value='" . $p["pro_id"] . "' >" . $p["pro_libelle"] . "</option>");
																	}
																} ?>																		
															</select>
															<i class="arrow simple"></i>
														</div>
													</div>
												</div>
												<div class="col-md-4" >
													<div class="section">
														<label for="uti_service" >Service :</label>
														<div class="select">
															<select id="uti_service" name="uti_service" >
																<option value="" >Sélectionner un service...</option>
														<?php	if(!empty($d_services)){
																	foreach($d_services as $s){
																		echo("<option value='" . $s["ser_id"] . "' >" . $s["ser_libelle"] . "</option>");
																	}
																} ?>	
															</select>
															<i class="arrow simple"></i>
														</div>
													</div>
												</div>
												<div class="col-md-4" >
													<div class="section">
														<label for="uti_responsable" >Responsable :</label>
														<select id="uti_responsable" class="select2 select2-utilisateur" name="uti_responsable" >
															<option value="" >Sélectionner le responsable...</option>																					
														</select>
													</div>
												</div>
											</div>
											<div class="row">													
												<div class="col-md-4" >
													<div class="section">	
														<label for="uti_fonction" >Fonction :</label>
														<input type="text" name="uti_fonction" id="uti_fonction" class="gui-input" placeholder="Fonction" >																																		
													</div>
												</div>												
												<div class="col-md-4" >
													<div class="section">
														<label for="uti_contrat" >Contrat :</label>
														<div class="select">
															<select id="uti_contrat" name="uti_contrat" >
																<option value="">Type de contrat...</option>
													<?php		foreach($base_contrat as $cle => $valeur){
																	if($cle>0){
																		echo("<option value='" . $cle . "' >" . $valeur . "</option>");
																	};																
																}; ?>				
															</select>
															<i class="arrow simple"></i>
														</div>
													</div>															
												</div>
												<div class="col-md-4" >
													<div class="section">
														<label for="uti_population" >Population :</label>
														<div class="select">
															<select id="uti_population" name="uti_population" >
																<option value="">Population...</option>
													<?php		foreach($base_population as $cle => $valeur){
																	if($cle>0){
																		echo("<option value='" . $cle . "' >" . $valeur . "</option>");
																	};																
																}; ?>				
															</select>
															<i class="arrow simple"></i>
														</div>
													</div>															
												</div>
											</div>
											
											<div class="row">											
												<div class="col-md-4" >
													<label for="uti_charge" >Charge :</label>
													<div class="input-group">
														<input type="text" name="uti_charge" id="uti_charge" min="0" max="999.99" class="form-control input-float" placeholder="Charge" value="" />
														<span class="input-group-addon" >€</span>
													</div>								
												</div>
												<div class="col-md-4" >
													<label for="uti_charge_dom" >Charges domicile :</label>
													<div class="input-group">
														<input type="text" name="uti_charge_dom" id="uti_charge_dom" min="0" max="999.99" class="gui-input input-float" placeholder="Charge domicile" value="" />								
														<span class="input-group-addon" >€</span>
													</div>
												</div>
												<div class="col-md-4" >
													<label for="uti_dist_dom" >Distance Bureau-Domicile :</label>
													<div class="input-group">
														<input type="text" name="uti_dist_dom" id="uti_dist_dom" min="0" max="999" class="gui-input input-int" placeholder="Distance Bureau-Domicile" />
														<span class="input-group-addon" >Km</span>
													</div>
												</div>
											</div>
											
											<div class="row mt20">											
												<div class="col-md-4 pt20" >												
													
													<label class="option">
														<input type="checkbox" name="uti_carte_affaire" value="1" />
														<span class="checkbox"></span>Possède une carte affaire
													</label>
																		  
												</div>										
												<div class="col-md-4" >												
													<label for="uti_dist_dom" >Date d'embauche :</label>
													<span class="field prepend-icon">
														<input type="text" id="uti_date_entree" name="uti_date_entree" class="gui-input datepicker" placeholder="Date d'embauche" >
														<span class="field-icon">
															<i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40" >
														<span>Véhicule</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3 text-right" >
													<strong>Kilomètres annuel :</strong>
												</div>												
												
												<div class="col-md-3" >
													<label class="option">
														<input type="radio" name="uti_tranche_km" value="1" />
														<span class="radio mr5"></span> &lt;= 5000 Km
													</label>
												</div>
												<div class="col-md-3" >
													<label class="option">
														<input type="radio" name="uti_tranche_km" value="2" />
														<span class="radio mr5"></span> &lt;= 20000 Km
													</label>
												</div>
												<div class="col-md-3" >
													<label class="option">
														<input type="radio" name="uti_tranche_km" value="3" />
														<span class="radio mr5"></span> &gt; 20000 Km
													</label>
												</div>
											</div>
											
											<div class="row mt20">
												<div class="col-md-6" >
													<label for="uti_veh_cv" >Puissance du véhicule :</label>
													<div class="input-group">
														<input type="text" name="uti_veh_cv" id="uti_veh_cv" min="0" max="20" class="gui-input input-int" placeholder="Puissance" />
														<span class="input-group-addon" >CV</span>
													</div>															
												</div>
												
												<div class="col-md-6" >
													<label for="uti_carte_grise" >Carte grise :</label>
													<span class="field prepend-icon file">
														<span class="button">Parcourir</span>
														<input type="file" class="gui-file" name="uti_carte_grise" id="uti_carte_grise" onChange="document.getElementById('file1').value = this.value;">
														<input type="text" class="gui-input" id="file1" placeholder="Selectionner la carte grise">
														<span class="field-icon">
															<i class="fa fa-upload"></i>
														</span>
													</span>
													<small>fichier .pdf uniquement</small>
												</div>
											</div>
											<!-- FIN BAS DU FORM -->
																												  
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</section>
				
			</section>
		</div>
	
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="utilisateur_liste.php" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle"></div>
				<div class="col-xs-3 footer-right">
					<button type="submit" class="btn btn-success btn-sm" >
						<i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php
	include "includes/footer_script.inc.php"; ?>	
	
	<script src="vendor/plugins/mask/jquery.mask.js"></script>
	<script src="assets/js/custom.js"></script>
	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script src="vendor/plugins/select2/js/i18n/fr.js"></script>
	
	
	
	<script type="text/javascript">
		jQuery(document).ready(function () {
			$("#genere_passe").click(function () {
				password=generePasse();
				$("#acc_uti_passe").val(password);
			});
		});
		
	</script>
</body>
</html>
