<?php

	include "includes/controle_acces.inc.php";

	include('includes/connexion.php');
	include('modeles/mod_parametre.php');
	
	// toutes les familels de fournisseurs pour le select
	$req = $Conn->prepare("SELECT * FROM fournisseurs_familles ORDER BY ffa_id");
	$req->execute();
	$fournisseurs_familles = $req->fetchAll();
	
	$cgr_famille=0;
	if(!empty($_POST)){
		if(!empty($_POST['cgr_famille'])){
			$cgr_famille=intval($_POST['cgr_famille']);
		}
	}elseif(!empty($_GET)){
		if(!empty($_GET['famille'])){
			$cgr_famille=intval($_GET['famille']);
		}
	}

	if(empty($_SESSION['acces']['acc_agence'])){
		$_SESSION['acces']['acc_agence'] = 0;
	}
	if(empty($cgr_famille)){
		$req = $Conn->prepare("SELECT * FROM commandes_grilles WHERE cgr_agence = " . $_SESSION['acces']['acc_agence'] . " AND cgr_societe = " . $_SESSION['acces']['acc_societe'] . " AND cgr_famille = 1 ORDER BY cgr_montant_min ASC");
		$req->execute();
		$grilles = $req->fetchAll();
	}else{
		$req = $Conn->prepare("SELECT * FROM commandes_grilles WHERE cgr_agence = " . $_SESSION['acces']['acc_agence'] . " AND cgr_societe = " . $_SESSION['acces']['acc_societe'] . " AND cgr_famille = " . $cgr_famille . " ORDER BY cgr_montant_min ASC");
		$req->execute();
		$grilles = $req->fetchAll();
	}
	


	?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css">
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css">
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
		
			<?php	
				include "includes/header_def.inc.php";
			?>	
			
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
							
				<section id="content" class="animated fadeIn">

					<div class="row" >

						<form action="validations_bc.php" method="post" id="admin-form">
	
							<div class="col-md-4 col-md-offset-4 mb10">
		                        <div class="section">
		                            <select id="cgr_famille" name="cgr_famille" required class="select2">
							<?php 		foreach($fournisseurs_familles as $ffa){ ?>
											<option value="<?= $ffa['ffa_id']?>" <?php if($cgr_famille==$ffa['ffa_id']) echo("selected"); ?> ><?= $ffa['ffa_libelle'] ?></option>
							<?php 		}?>
		                            </select>
		                        </div>
		                    </div>

							<div class="col-md-2 mt5">
								<button type="submit" class="btn btn-primary btn-sm" >
									<i class="fa fa-search"></i>										
								</button>
							</div>
						</form>

				<?php	if ($cgr_famille==2) { ?>
							<div class="col-md-12 alert alert-info" >							
								Cette grille, associée à la grille des dérogations de marge, permet d'identifier la personne devant valider une DA en fonction de la marge générée par l'action de formation. 
							</div>
				<?php	} ?>
						<div class="col-md-12" >
							<div class="table-responsive" >
								<table class="table table-hover table-striped" >
									<thead>
										<tr class="dark" >										
											<th>ID</th>
											<th>Montant minimum</th>
									<?php	if ($cgr_famille==2) { ?>	
												<th>Niveau 0</th>
												<th>Niveau 1</th>
												<th>Niveau 2</th>
												<th>&nbsp;</th>
									<?php	} else { ?>
												<th>Validant 1</th>
												<th>Validant 2</th>
												<th>Validant 3</th>
												<th>Validant 4</th>
									<?php	} ?>
											<th>Actions</th>
										</tr>															
									</thead>
									<tbody>
										<?php foreach($grilles as $g){ ?>
												<tr>
													<td><?= $g['cgr_id'] ?></td>
													<td><?= $g['cgr_montant_min'] ?></td>
													<td>
												<?php 	if(!empty($g['cgr_responsable_1'])){
															echo("Responsable du DO");
														}elseif(!empty($g['cgr_utilisateur_1'])){ 
															$req = $Conn->prepare("SELECT uti_nom, uti_prenom FROM utilisateurs WHERE uti_id = " . $g['cgr_utilisateur_1']);
															$req->execute();
															$uti = $req->fetch();
															echo($uti['uti_prenom'] . " " . $uti['uti_nom']);
														}elseif(!empty($g['cgr_droit_1'])){ 
															$req = $Conn->prepare("SELECT drt_nom FROM droits WHERE drt_id = " . $g['cgr_droit_1']);
															$req->execute();
															$drt = $req->fetch();
															echo('droit "' . $drt['drt_nom'] . '"');
														}else{ 
															$req = $Conn->prepare("SELECT pro_libelle FROM profils WHERE pro_id = " . $g['cgr_profil_1']);
															$req->execute();
															$profil = $req->fetch();
															echo($profil['pro_libelle']); 
														} ?>
													</td>
													<td>
												<?php 	if(!empty($g['cgr_utilisateur_2'])){ 
															$req = $Conn->prepare("SELECT uti_nom, uti_prenom FROM utilisateurs WHERE uti_id = " . $g['cgr_utilisateur_2']);
															$req->execute();
															$uti = $req->fetch();
															echo($uti['uti_prenom'] . " " . $uti['uti_nom']);
														}elseif(!empty($g['cgr_droit_2'])){ 
															$req = $Conn->prepare("SELECT drt_nom FROM droits WHERE drt_id = " . $g['cgr_droit_2']);
															$req->execute();
															$drt = $req->fetch();
															echo('droit "' . $drt['drt_nom'] . '"');
														}else{ 
															$req = $Conn->prepare("SELECT pro_libelle FROM profils WHERE pro_id = " . $g['cgr_profil_2']);
															$req->execute();
															$profil = $req->fetch();
															echo($profil['pro_libelle']);
														} ?>															
													</td>
													<td>
												<?php 	if(!empty($g['cgr_utilisateur_3'])){ 
															$req = $Conn->prepare("SELECT uti_nom, uti_prenom FROM utilisateurs WHERE uti_id = " . $g['cgr_utilisateur_3']);
															$req->execute();
															$uti = $req->fetch();
															echo($uti['uti_prenom'] . " " . $uti['uti_nom']);
														}elseif(!empty($g['cgr_droit_3'])){ 
															$req = $Conn->prepare("SELECT drt_nom FROM droits WHERE drt_id = " . $g['cgr_droit_3']);
															$req->execute();
															$drt = $req->fetch();
															echo('droit "' . $drt['drt_nom'] . '"');
														}else{ 
															$req = $Conn->prepare("SELECT pro_libelle FROM profils WHERE pro_id = " . $g['cgr_profil_3']);
															$req->execute();
															$profil = $req->fetch();
															echo($profil['pro_libelle']);
														} ?>															
													</td>
													<td>
												<?php 	if(!empty($g['cgr_utilisateur_4'])){ 
															$req = $Conn->prepare("SELECT uti_nom, uti_prenom FROM utilisateurs WHERE uti_id = " . $g['cgr_utilisateur_4']);
															$req->execute();
															$uti = $req->fetch();
															echo($uti['uti_prenom'] . " " . $uti['uti_nom']);
														}elseif(!empty($g['cgr_droit_4'])){ 
															$req = $Conn->prepare("SELECT drt_nom FROM droits WHERE drt_id = " . $g['cgr_droit_4']);
															$req->execute();
															$drt = $req->fetch();
															echo('droit "' . $drt['drt_nom'] . '"');
														}else{ 
															$req = $Conn->prepare("SELECT pro_libelle FROM profils WHERE pro_id = " . $g['cgr_profil_4']);
															$req->execute();
															$profil = $req->fetch();
															echo($profil['pro_libelle']);
														} ?>															
													</td>													
													<td>
														<a href="validations_bc_validant.php?id=<?= $g['cgr_id'] ?>" class="btn btn-sm btn-warning">
															<i class="fa fa-pencil"></i>
														</a>
														<a href="validations_bc_delete.php?id=<?= $g['cgr_id'] ?>" class="btn btn-sm btn-danger">
															<i class="fa fa-times"></i>
														</a>
													</td>									
												</tr>	
										<?php } ?>														
									</tbody>
								</table>
							
							</div>
						</div>
					</div>
	
				</section>
				<!-- End: Content -->
			</section>
			
		</div>
		<!-- End: Main -->
		
		<footer id="content-footer" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle" >
				</div>
				<div class="col-xs-3 footer-right" >
					<a href="validations_bc_validant.php" class="btn btn-success btn-sm">
						<i class="fa fa-plus" ></i>
						Nouvelle grille
					</a>
				</div>
			</div>
		</footer>
									
		
	<?php
		include "includes/footer_script.inc.php"; ?>	
		<script src="vendor/plugins/mask/jquery.mask.js" ></script>
		
		<!-- PLUGIN -->
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		
		
		
		<script type="text/javascript">
				
		</script>
		<!-- END: PAGE SCRIPTS -->
	</body>
</html>
