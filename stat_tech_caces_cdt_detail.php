<?php
	
    // STAT CACES 1
    /*
    Affiche le nombre de jours de formation et de test par formateur. 
    La stat indique également quelles catégories ont été testées au moins un fois sur la période.
    */
	include "includes/controle_acces.inc.php";
	//include "modeles/mod_parametre.php";
	include "includes/connexion.php";
    include "includes/connexion_soc.php";

    // controle d'accès
    
    if($_SESSION["acces"]["acc_ref"]!=1 OR ($_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']["acc_profil"]!=11 AND $_SESSION['acces']["acc_profil"]!=14 AND $_SESSION['acces']["acc_profil"]!=15 AND $_SESSION['acces']["acc_profil"]!=10) ){
        // SERVICE TECH, DAF, DG, RA et RE
        $erreur="Accès refusé!";
		echo($erreur);
		die();
	}

	
	
	// DONNEE FORM

	$erreur_txt="";
	if (isset($_SESSION["stat_tech"])) {

        $qualification=0;
		if(!empty($_SESSION["stat_tech"]["qualification"])){
			$qualification=intval($_SESSION["stat_tech"]["qualification"]);
		}
		
		$periode_deb="";
		if(!empty($_SESSION["stat_tech"]["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_SESSION["stat_tech"]["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}
		
		$periode_fin="";
		if(!empty($_SESSION["stat_tech"]["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_SESSION["stat_tech"]["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
        }

	}else{
		$erreur_txt="Paramètres absents";
		
    }
    
    if (empty($periode_deb) OR empty($periode_fin) OR empty($qualification)) {
        $erreur_txt="Formulaire incomplet!";
    }
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : stat_technique.php");
		die();
    }

    // CRITERE GET -> choix des valeurs à détailler

    $categorie=0;
    if(!empty($_GET["categorie"])){
        $categorie=intval($_GET["categorie"]);
    }

    $intra_inter=0;
    if(!empty($_GET["intra_inter"])){
        $intra_inter=intval($_GET["intra_inter"]);
    }
	
	
	// LE PERSONNE CONNECTE
	
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]); 
	}
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]); 
	}
	
    // QUALIFICATIONS
    
    // qualif select
    
    if (!empty($categorie)){

        $sql="SELECT qca_libelle,qua_libelle FROM Qualifications,Qualifications_Categories WHERE qua_id=qca_qualification AND qca_id=" . $categorie . ";";
        $req=$Conn->query($sql);
        $d_qualification=$req->fetch();
        if(!empty($d_qualification)){
            $titre="Qualification " . $d_qualification["qua_libelle"] . " - catégorie " . $d_qualification["qca_libelle"];
        }else{
            $titre="";
            $erreur_txt="Qualification non identifié!";
        }

    } else {

        $sql="SELECT qua_libelle FROM Qualifications WHERE qua_id=" . $qualification . ";";
        $req=$Conn->query($sql);
        $d_qualification=$req->fetch();
        if(!empty($d_qualification)){
            $titre="Qualification " . $d_qualification["qua_libelle"];
        }else{
            $titre="";
            $erreur_txt="Qualification non identifié!";
        }
    }
   


    if (empty($erreur_txt)) {

        // Liste des produits de la qualifications de type CACES
        // exclusion des attestations

        $liste_pro="";
        $sql="SELECT pro_id,pro_code_produit FROM Produits WHERE pro_qualification=" . $qualification . " AND NOT pro_code_produit LIKE 'A-%';";
        $req=$Conn->query($sql);
        $src_pro=$req->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($src_pro)){
            /*echo("<pre>");
                print_r($src_pro);
            echo("</pre>");*/
            $tab_pro=array_column ($src_pro,"pro_id");
            $liste_pro=implode($tab_pro,",");
        }
        if (empty($liste_pro)) {
            $erreur_txt="Aucun produit n'est associé à la qualification demandé.";
        }
    }
    if (empty($erreur_txt)) {

        $titre.="<br/>Tests réalisés ";
        if ($intra_inter==1) {
            $titre.="en INTRA<br/>";
        } elseif ($intra_inter==2) {
            $titre.="en INTER<br/>";
        }
        $titre.="entre le " . $DT_periode_deb->format("d/m/Y") . " et le " . $DT_periode_fin->format("d/m/Y") ;

     
        // LES CATEGORIES DE QUALIF

     
        $sql="SELECT qca_id,qca_libelle FROM Qualifications_Categories WHERE qca_qualification=" . $qualification . " AND NOT qca_archive ORDER BY qca_libelle;";
        $req=$Conn->query($sql);
        $d_categories=$req->fetchAll(PDO::FETCH_ASSOC);
       
        // LES TEST CACES
        /* en plus des données de chaque test, on recupère le nombre total de test généré par l'action client
        en associant cette donnée au CA test de chaque action, nous pouvons connaitre le CA de chaque test.
        */

        $data_actions_clients=array();

        $sql="SELECT diplomes_caces_cat.dcc_categorie,diplomes_caces_cat.dcc_action,diplomes_caces_cat.dcc_action_client
        ,DATE_FORMAT(diplomes_caces_cat.dcc_date,'%d/%m/%Y') AS dcc_date,diplomes_caces_cat.dcc_testeur_identite,diplomes_caces_cat.dcc_valide
        ,dca_id,dca_sta_nom,dca_sta_prenom
        ,total_cat
        ,qca_libelle
        FROM diplomes_caces_cat INNER JOIN diplomes_caces ON (diplomes_caces_cat.dcc_diplome=diplomes_caces.dca_id)
        LEFT JOIN Qualifications_Categories ON (diplomes_caces_cat.dcc_categorie=Qualifications_Categories.qca_id)
        LEFT JOIN (
			
            SELECT COUNT(dcc_diplome) as total_cat, dcc_action_client,dcc_action FROM diplomes_caces_cat WHERE dcc_action_soc=" . $acc_societe . " GROUP BY dcc_action_client,dcc_action
            
		) AS total_caces_cat ON (diplomes_caces_cat.dcc_action_client = total_caces_cat.dcc_action_client AND diplomes_caces_cat.dcc_action = total_caces_cat.dcc_action)
        WHERE dcc_diplome=dca_id";
        $sql.=" AND dca_diplome=2 AND dcc_date>='" . $periode_deb . "' AND dcc_date<='" . $periode_fin . "' AND dca_qualification=" . $qualification;
        $sql.=" AND dcc_action_soc=" . $acc_societe;
        if($categorie>0){
            $sql.=" AND dcc_categorie=" . $categorie;
        }
        $sql.=";";
        $req=$Conn->query($sql);
        $d_tests=$req->fetchAll(PDO::FETCH_ASSOC);
        if(empty($d_tests)){
            $erreur_txt="Aucun test réalisé sur la période demandée.";
        }

    
        // CA TEST
        // on recupère le CA test de chaque Actions_clients.
        if (empty($erreur_txt)) {

            $sql="SELECT acl_id,acl_produit,acl_pro_inter,COUNT(pda_id) AS nb_jour,ca,total_jour FROM Actions_Clients 
            INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
            INNER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
            INNER JOIN Plannings_Dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
            LEFT JOIN (

                SELECT SUM(fli_montant_ht) as ca, fli_action_client FROM Factures_Lignes GROUP BY fli_action_client

            ) AS Factures ON (Actions_Clients.acl_id = Factures.fli_action_client)
            
            LEFT JOIN (
                SELECT COUNT(acd_date) as total_jour, acd_action_client FROM Actions_Clients_Dates GROUP BY acd_action_client
            ) AS Dates ON (Actions_Clients.acl_id = Dates.acd_action_client)

            WHERE acl_produit IN (" . $liste_pro . ") AND NOT acl_archive AND NOT act_archive AND pda_categorie=2
            AND pda_date>='" . $periode_deb . "' AND pda_date<='" . $periode_fin . "'";
            if (!empty($intra_inter)) {
                if ($intra_inter==1) {
                    $sql.=" AND NOT acl_pro_inter"; 
                } else {
                    $sql.=" AND acl_pro_inter"; 
                }
            }
            $sql.=" GROUP BY acl_id,acl_produit,ca,total_jour";
            $req=$ConnSoc->query($sql);
            $d_sources=$req->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($d_sources)) {			
                foreach ($d_sources as $s){

                    $ca_test=0;
                    if (!empty($s["total_jour"])) {
                        $ca_test=($s["ca"]/$s["total_jour"])*$s["nb_jour"];
                    }

                    if (empty($data_actions_clients[$s["acl_id"]])) {
                        $data_actions_clients[$s["acl_id"]]=$ca_test;
                    }
                }

            } else { 
                $erreur_txt="Aucune action ne correspond à votre recherche.";
            }
        }
    }
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<!-- PERSO -->	
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
		
		<div id="main" >
<?php		include "includes/header_def.inc.php";  ?>			
			<section id="content_wrapper" >					
			
				<section id="content" class="animated fadeIn" >


                    <h1 class="text-center" >
                        <?=$titre?>
                    </h1>

            <?php	if (!empty($erreur_txt)) {   ?>
                    
                        <p class="alert alert-danger" ><?=$erreur_txt?></p>

        <?php       } else {  ?>

                        <div class="row" >
                            <div class="col-md-12" >
                            
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" >
                                        <thead>
                                            <tr class="dark">
                                                <td class="text-center">Catégorie</td>
                                                <td class="text-center">Date du test</td>
                                                <td class="text-center">Testeur</td>
                                                <td class="text-center">Validé</td>
                                                <td class="text-center">Ajourné</td>
                                                <td class="text-center">Dossier</td>
                                                <td class="text-center">Stagiaire</td>
                                                <td class="text-center">Action</td>
                                                <td class="text-center">Inscription</td>
                                                <td class="text-center">CA (€)</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php		$total_valide=0;
                                            $total_echec=0;
                                            $total_ca=0;

                                            foreach($d_tests as $t => $test){ 
                                    
                                                if (empty($intra_inter) OR isset($data_actions_clients[$test["dcc_action_client"]]) ) { ?>
                                                    <tr>
                                                        <td><?=$test["qca_libelle"]?></td>
                                                        <td><?=$test["dcc_date"]?></td>
                                                        <td><?=$test["dcc_testeur_identite"]?></td>
                                                        <td>
                                                    <?php   if($test["dcc_valide"]) {
                                                                echo("1");
                                                                $total_valide++;
                                                            } ?>
                                                        </td>
                                                        <td>
                                                    <?php   if(!$test["dcc_valide"]) {
                                                                echo("1");
                                                                $total_echec++;
                                                            } ?>
                                                        </td>
                                                        <td><?=$test["dca_id"]?></td>
                                                        <td><?=$test["dca_sta_nom"] . " " . $test["dca_sta_prenom"]?></td>
                                                        <td><?=$test["dcc_action"]?></td>
                                                        <td><?=$test["dcc_action_client"]?></td>                                                 
                                            <?php       if (empty ($test["dcc_action_client"])) {
                                                            echo("<td class='danger' >Dossier a régulariser</td>");
                                                        } elseif (!isset($data_actions_clients[$test["dcc_action_client"]])) {                                                       
                                                            echo("<td class='danger' >Facturation incorrecte</td>");
                                                        } elseif (empty($test["total_cat"])) {
                                                            echo("<td class='danger' >Nb test inconnu</td>");
                                                        } else {
                                                            $ca=$data_actions_clients[$test["dcc_action_client"]]/$test["total_cat"];
                                                            $ca=round($ca,2);
                                                            $total_ca=$total_ca + $ca;
                                                            echo("<td class='text-right' >" . number_format($ca,2,","," ") . "</td>"); 
                                                        } ?>
                                                    </tr>
                                <?php		    } 
                                            } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3" >Total :</th>
                                                <td class="text-right" ><?=$total_valide?></td>
                                                <td class="text-right" ><?=$total_echec?></td>
                                                <td colspan="4" ></td>
                                                <td class="text-right" ><?=number_format($total_ca,2,","," ")?></td>
                                            </tr>
                                        </tfoot>
                                    </table>							
                                </div>
                                
                            </div>
						</div>
		<?php	    } ?>

				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
            <div class="row">
                <div class="col-xs-3 footer-left">
                    <a href="stat_tech_caces_cdt.php" class="btn btn-sm btn-default"  >
                        <i class="fa fa-left-arrow" ></i>Retour
                    </a>					
                </div>
                <div class="col-xs-6 footer-middle text-center" style=""></div>
                <div class="col-xs-3 footer-right"></div>
            </div>
        </footer>
<?php   if (!empty($warning_txt)) { ?>

            <div id="modal-error" class="modal fade" role="dialog" data-show="true" id="modal_warning">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-warning">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title" id="titreModal" >Avertissement</h4>
                        </div>
                        <div class="modal-body"><?=$warning_txt?></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                <i class="fa fa-close" ></i>
                                Fermer
                            </button>
                        </div>
                    </div>
                </div>
            </div>
<?php	}
        include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function () {

        <?php   if (!empty($warning_txt)) { ?>
                    $("#modal_warning").modal("show");
        <?php   } ?>
			});
			(jQuery);
		</script>
	</body>
</html>
