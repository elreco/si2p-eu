<?php
//////////////// INCLUDES /////////////////////
include "includes/controle_acces_cli.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');
include('modeles/mod_parametre.php');
require "modeles/mod_erreur.php";
//////////////// REQUETES VERS LE SERVEUR /// ///////////////

$_SESSION["retour"]="session_client_liste.php";

$critere=array(
	"ses_produit" => 0,
	"ses_date_deb" => "",
	"ses_date_fin" => "",
	"ses_lieu" => "",
	"filtre" => 0
);

if(isset($_GET["raz"])){

	if(isset($_SESSION["critere_session"])){
		unset($_SESSION["critere_session"]);
	}

}elseif(!empty($_POST)){

	if(!empty($_POST['ses_produit'])){
		$critere["filtre"]=1;
		$critere["ses_produit"]=intval($_POST['ses_produit']);
	}
	if(!empty($_POST['ses_date_deb'])){
		$critere["filtre"]=1;
		$critere["ses_date_deb"]=$_POST['ses_date_deb'];
	}
	if(!empty($_POST['ses_date_fin'])){
		$critere["filtre"]=1;
		$critere["ses_date_fin"]=$_POST['ses_date_fin'];
	}
	if(!empty($_POST['ses_lieu'])){
		$critere["filtre"]=1;
		$critere["ses_lieu"]=$_POST['ses_lieu'];
	}
	$_SESSION["critere_session"]=$critere;

}elseif(isset($_SESSION["critere_session"])){

	$critere=$_SESSION["critere_session"];
}

$sql="SELECT soc_id FROM Societes ORDER BY soc_id;";
$req = $Conn->query($sql);
$d_societes=$req->fetchAll();
if(!empty($d_societes)){
	foreach($d_societes as $soc){
		$ConnFct=connexion_fct($soc["soc_id"]);
        $sql="SELECT DISTINCT cli_id,cli_code,cli_nom,act_demi_deb, act_date_deb, act_adr_ville, acl_id, act_id, act_produit, acl_produit
        FROM  actions
        LEFT JOIN actions_clients ON (actions_clients.acl_action = actions.act_id)

			LEFT JOIN Clients ON (actions_clients.acl_client = clients.cli_id)
		WHERE acl_archive = 0 AND acl_client IN (" . $_SESSION['acces']['acc_groupe_liste'] . ")";
		if(!empty($critere["ses_date_deb"])){
			$sql.=" AND act_date_deb>='" . convert_date_sql($critere["ses_date_deb"]) . "'";
		};
		if(!empty($critere["ses_date_fin"])){
			 $sql.=" AND act_date_deb<='" . convert_date_sql($critere["ses_date_fin"]) . "'";
		};
		if(!empty($critere["ses_lieu"])){
			$sql.=" AND act_adr_ville LIKE '" . $critere["ses_lieu"] . "%'";
		};
        $sql.=" ORDER BY act_date_deb DESC;";
        $req = $ConnFct->query($sql);
        $sessions_build = $req->fetchAll();

        foreach($sessions_build as $k=>$sb){
            $sessions[$soc["soc_id"]][] = $sessions_build[$k];
        }
    }
}
/* var_dump($sessions);
die(); */
?>

<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>Si2P - ORION</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="Si2P">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->

<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

<link rel="shortcut icon" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="sb-top sb-top-sm">
    <div id="main">
<?php	include "includes/header_cli.inc.php"; ?>
        <section id="content_wrapper">



			<section id="content" class="animated fadeIn">

				<h1>Liste des formations</h1>
				<!-- FORMULAIRE RECHERCHE -->
                <form action="session_client_liste.php" method="POST">
					<div class="row mb20 admin-form">
						<div class="col-md-2">
							<label for="fpr_type">Date du :</label>
							<input type="text" name="ses_date_deb" class="gui-input date datepicker" placeholder="Date du" value="<?=$critere["ses_date_deb"]?>" />
						</div>
						<div class="col-md-2">

							<label for="fpr_type">Au :</label>

							<input type="text" name="ses_date_fin" class="gui-input date datepicker" placeholder="Au" value="<?=$critere["ses_date_fin"]?>" />
						</div>
						<div class="col-md-3">
							<label for="fpr_type">Lieu :</label>
							<input type="text" name="ses_lieu" class="gui-input" placeholder="Lieu" value="<?=$critere["ses_lieu"]?>" />
						</div>
						<div class="col-sm-1">
							<button type="submit" name="search_produit" class="btn btn-primary mt20" data-toggle="tooltip" title="trier">
								Filtrer
							</button>
						</div>
				<?php 	if($critere["filtre"]==1){ ?>
							<div class="col-sm-2">
								<a href="session_client_liste.php?raz" class="btn btn-info mt20" data-toggle="tooltip" title="Effacer tous les critères de selection" >
									Remettre à zéro
								</a>
							</div>
                <?php	} ?>
					</div>
				</form>
				<!-- FIN FORMULAIRE RECHERCHE -->

		<?php 	if(!empty($sessions)){ ?>

					<div class="table-responsive">
						<table class="table table-striped table-hover" id="table_id">
							<thead>
								<tr class="dark">

									<th>Date</th>
									<th>Client</th>
									<th>Lieu</th>
									<th>Formation</th>
                                    <th>Centre de formation</th>
									<th class="text-center" >Attestations</th>
									<?php if($_SESSION['acces']['acc_client'] == 16670 OR $_SESSION['acces']['acc_holding'] == 16670){ ?>
										<th class="text-center" >Feuille de présence</th>
									<?php }?>
									<th class="no-sort">Actions</th>
								</tr>
							</thead>
							<tbody>
					<?php
					foreach($d_societes as $soc){
						if(!empty($sessions[$soc['soc_id']])){


						foreach($sessions[$soc['soc_id']] as $s){
							$sql="SELECT pro_code_produit, pro_libelle FROM Produits WHERE pro_id = " . $s['acl_produit'];
							$req = $Conn->query($sql);
							$d_produit=$req->fetch();
							$sql="SELECT soc_nom FROM Societes WHERE soc_id = " . $soc['soc_id'];
							$req = $Conn->query($sql);
							$d_societe=$req->fetch();

                        ?>
								<tr>

									<td  data-sort="<?= $s['act_date_deb'] ?>">
									   <?= convert_date_fr($s['act_date_deb']); ?>

									</td>
									<td><?= $s['cli_nom'] ?> (<?= $s['cli_code'] ?>)</td>
									<td>
										<?= $s['act_adr_ville']; ?>
									</td>
									 <td>
										<?= $d_produit["pro_code_produit"] ?> (<?= $d_produit["pro_libelle"] ?>)
									</td>
                                    <td>
                                        <?= $d_societe['soc_nom'] ?>
                                    </td>
									<td class="text-center" >
							<?php		if(file_exists("documents/Societes/".$soc['soc_id'] ."/Attestations/attestation_ses_".$s['act_id']."_".$s["acl_id"].".pdf")){
											$download_nom="Attestation groupée " . $d_produit["pro_code_produit"] . " " . convert_date_txt($s['act_date_deb']); ?>
											<a href="documents/Societes/<?= $soc['soc_id'] ?>/Attestations/attestation_ses_<?= $s['act_id'] ?>_<?= $s["acl_id"] ?>.pdf" download="<?=$download_nom?>" data-toggle="tooltip" title="Télécharger l'attestation groupée" class="btn btn-success btn-sm">
												<i class="fa fa-file-pdf-o"></i>
											</a>
							<?php		}else{
											echo("<span class='text-danger' >Non disponible</span>");
										} ?>
									</td>
									<?php if($_SESSION['acces']['acc_client'] == 16670 OR $_SESSION['acces']['acc_holding'] == 16670){ ?>
										<td class="text-center">
											<?php
											if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $soc['soc_id'] . "/Presences/monop_presence_". $s["acl_id"] . ".pdf")){
												$download_nom="Feuille de présence " . $d_produit["pro_code_produit"] . " " . convert_date_txt($s['act_date_deb']); ?>
												<a href="documents/Societes/<?= $soc['soc_id'] ?>/Presences/monop_presence_<?=$s["acl_id"]?>.pdf" download="<?=$download_nom?>" data-toggle="tooltip" title="Télécharger la feuille de présence" class="btn btn-success btn-sm">
													<i class="fa fa-file-pdf-o"></i>
												</a>
											<?php }else{
												echo("<span class='text-danger' >Non disponible</span>");
											}?>
										</td>
									<?php }?>
									<td>
										<a href="session_client_stagiaire.php?id=<?= $s['act_id'] ?>&acl_id=<?= $s['acl_id'] ?>&soc=<?= $soc['soc_id'] ?>" class="btn btn-success btn-sm" data-toggle="tooltip" title="Voir la liste des inscrits">
											<i class="fa fa-eye"></i>
										</a>
									</td>
								</tr>
					<?php 	}
					}
					} ?>
						</tbody>
					</table>
				</div>
	<?php 	}else{ ?>
				<div class="col-md-12 text-center" style="padding:0;" >
					<div class="alert alert-warning" style="border-radius:0px;">
					  Aucune session.
					</div>
				</div>
	<?php 	} ?>
            </section>
        </section>
    </div>
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">


            </div>
            <div class="col-xs-6 footer-middle">&nbsp;</div>
            <div class="col-xs-3 footer-right">




            </div>
        </div>
    </footer>

	<!-- MODAL SUPPRESSION PRODUIT -->
	<div id="modalsuppr" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Supprimer une session</h4>
				</div>

				<div class="modal-body">
					Êtes-vous sûr de vouloir supprimer cette session ?

				</div>

				<div class="modal-footer">
					<a href="" id="delete-btn" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
				</div>
			</div>

		</div>
	</div>
<!-- FIN MODAL SUPPRESSION PRODUIT -->
    <style type="text/css">
        #table_id_filter label .form-control{
            width:300px;
        }

    </style>
<?php
include "includes/footer_script.inc.php"; ?>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
/////////////////////// INTERACTIONS UTILISATEUR /////////////////////////
jQuery(document).ready(function () {
    $(document).on('click', '.last_session', function() {
        sta_id = $(this).data("id");
        get_sessions(sta_id);
    });
    // fin suppression onclick suppr
    $(document).on('click', '.delete', function() {
        $('#delete-btn').attr("href","session_delete.php?id=" + $(this).data('id'));
    });
    /*$('#table_id').DataTable({
        "language": {
        "url": "/vendor/plugins/DataTables/media/js/French.json",
        searchPlaceholder: "Rechercher une formation (exemple : 04 Avril 2017)"
        },
        "aaSorting": []

    });*/
	$('#table_id').DataTable({
        "language": {
        "url": "vendor/plugins/DataTables/media/js/French.json"
        },
        "paging": false,
        "searching": false,
        "info": false,
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false,
        }]
    });
    $(".datepicker").datepicker({
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        showButtonPanel: false,
        beforeShow: function(input, inst) {
            var newclass = 'admin-form';
            var themeClass = $(this).parents('.admin-form').attr('class');
            var smartpikr = inst.dpDiv.parent();
            if (!smartpikr.hasClass(themeClass)) {
                inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
            }
        }
    });
});


///////////////////////////// FONCTIONS ////////////////////////////////

function get_sessions(sta_id){
    $.ajax({
        type:'POST',
        url: 'ajax/ajax_sessions_stagiaires.php',
        data : 'sta_id=' + sta_id,
        dataType: 'json',
        success: function(data)
        {
            console.log(data);
            $.each(data, function(key, value) {
                $('.body-liste').append($("<li></li>").text(value['ses_date'] + " de " + value['ses_h_deb'] + " à " + value['ses_h_fin']));
            });

        }
    });
}


</script>
</body>
</html>
