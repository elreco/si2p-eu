<?php
    include "includes/controle_acces.inc.php";
    include("modeles/mod_parametre.php");
	include("includes/connexion.php");
	include("includes/connexion_soc.php");
    include("includes/connexion_fct.php");
    include "modeles/mod_envoi_mail.php";



    // CALCULER LE NOMBRE DE FACTURES SELECTIONNEES
    $count = 0;
    foreach($_POST['check_list'] as $soc=>$cl){
        foreach($cl as $c){
            $count = $count + 1;
        }
    }
    // ERREUR
    if($count > 50){
        $_SESSION['message'][] = array(
    		"titre" => "Erreur",
    		"type" => "danger",
    		"message" => "Vous ne pouvez pas sélectionner plus de 50 factures"
        );
        header("location : facture_relance_liste.php");
        die();
    }
    // VERIF SESSION
    $acc_agence=0;
    if(isset($_SESSION['acces']["acc_agence"])){
        $acc_agence=$_SESSION['acces']["acc_agence"];
    }
    $acc_societe=0;
    if(isset($_SESSION['acces']["acc_societe"])){
        $acc_societe=$_SESSION['acces']["acc_societe"];
    }
    $acc_utilisateur=0;
    if(!empty($_SESSION['acces']["acc_ref"])){
        if($_SESSION['acces']["acc_ref"]==1){
            $acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
        }
    }
    $sql="SELECT uti_nom, uti_prenom FROM Utilisateurs WHERE uti_id = " . $acc_utilisateur;
    $req = $Conn->query($sql);
    $utilisateur=$req->fetch();

    $sql="SELECT soc_nom FROM Societes WHERE soc_id = " . $acc_societe;
    $req = $Conn->query($sql);
    $d_societe=$req->fetch();
    $sujet_societe = $d_societe['soc_nom'];
    if(!empty($acc_agence)){
        $sql="SELECT age_nom FROM Agences WHERE age_id = " . $acc_agence;
        $req = $Conn->query($sql);
        $d_agence=$req->fetch();
        $sujet_societe = $d_agence['age_nom'];
    }
    // FIN SESSION
    $erreur = "";

    $emails = [];
    foreach($_POST['check_list'] as $soc=>$cl){
        foreach($cl as $c){

            $ConnFct = connexion_fct($soc);
            // SELECT LA FACTURE
            $sql="SELECT fac_id, fac_etat_relance, fac_client, fac_numero FROM Factures WHERE fac_id=" . $c;
            $req = $ConnFct->prepare($sql);
            $req->execute();
            $d_facture = $req->fetch();

            // SELECT LES CONTACTS IMPAYES
            if(!empty($_POST['facture_opca'][$d_facture['fac_client']])){
                $sql="SELECT cli_id, cli_nom, cli_code,cli_releve,cli_affacturage,cli_affacturage_iban,cli_groupe,cli_reg_fdm,cli_ident_tva,cli_filiale_de,cli_blackliste
                ,cli_filiale_de,cli_interco_soc,cli_reg_formule,cli_reg_nb_jour,cli_facture_opca
                ,cli_categorie,cli_sous_categorie
                FROM Clients WHERE cli_id = " . $_POST['facture_opca'][$d_facture['fac_client']];
                $req = $Conn->query($sql);
                $d_client=$req->fetch();
                if(empty($d_client)){
                    $sql="SELECT cli_id, cli_nom, cli_code,cli_releve,cli_affacturage,cli_affacturage_iban,cli_groupe,cli_reg_fdm,cli_ident_tva,cli_filiale_de,cli_blackliste
                    ,cli_filiale_de,cli_interco_soc,cli_reg_formule,cli_reg_nb_jour,cli_facture_opca
                    ,cli_categorie,cli_sous_categorie
                    FROM Clients WHERE cli_id = " . $d_facture['fac_client'];
                    $req = $Conn->query($sql);
                    $d_client=$req->fetch();
                }
            }else{
                $sql="SELECT cli_id, cli_nom, cli_code,cli_releve,cli_affacturage,cli_affacturage_iban,cli_groupe,cli_reg_fdm,cli_ident_tva,cli_filiale_de,cli_blackliste
                ,cli_filiale_de,cli_interco_soc,cli_reg_formule,cli_reg_nb_jour,cli_facture_opca
                ,cli_categorie,cli_sous_categorie
                FROM Clients WHERE cli_id = " . $d_facture['fac_client'];
                $req = $Conn->query($sql);
                $d_client=$req->fetch();
            }


            if(!empty($d_client['cli_groupe']) && empty($_POST['facture_opca'][$d_facture['fac_client']])){
                if(!empty($d_client['cli_filiale_de'])){
                    // CONTACTS IMPAYES
                    $sql="SELECT DISTINCT con_id,con_fonction,con_fonction_nom,con_titre,con_nom,con_prenom,con_tel,con_portable,con_fax,con_mail,con_compta,con_comment,aco_contact
                    FROM Contacts LEFT OUTER JOIN Adresses_Contacts ON (Contacts.con_id=Adresses_Contacts.aco_contact AND aco_defaut)
                    WHERE con_ref_id=" . $d_client['cli_filiale_de'] . " AND con_ref=1 AND con_compta = 1;";
                }else{
                    // CONTACTS IMPAYES
                    $sql="SELECT DISTINCT con_id,con_fonction,con_fonction_nom,con_titre,con_nom,con_prenom,con_tel,con_portable,con_fax,con_mail,con_compta,con_comment,aco_contact
                    FROM Contacts LEFT OUTER JOIN Adresses_Contacts ON (Contacts.con_id=Adresses_Contacts.aco_contact AND aco_defaut)
                    WHERE con_ref_id=" . $d_client['cli_id'] . " AND con_ref=1 AND con_compta = 1;";
                }
            }else{
                // CONTACTS IMPAYES
                $sql="SELECT DISTINCT con_id,con_fonction,con_fonction_nom,con_titre,con_nom,con_prenom,con_tel,con_portable,con_fax,con_mail,con_compta,con_comment,aco_contact
                FROM Contacts LEFT OUTER JOIN Adresses_Contacts ON (Contacts.con_id=Adresses_Contacts.aco_contact AND aco_defaut)
                WHERE con_ref_id=" . $d_client['cli_id'] . " AND con_ref=1 AND con_compta = 1;";
            }

            $req=$Conn->query($sql);
            $contact=$req->fetch();
            // ERREUR CONTACT
            if(empty($contact['con_nom']) OR empty($contact['con_mail'])){
                $erreur .= "<li> La facture " . $d_facture['fac_numero'] . " ne dispose pas de contact par défaut ou il manque l'adresse email</li>";
            }else{

                $url_facture_form=$_SERVER["DOCUMENT_ROOT"] . "/Documents/Societes/" . $soc . "/Factures/".  $d_facture["fac_numero"] . ".pdf";
                $url_facture="/Documents/Societes/" . $soc . "/Factures/".  $d_facture["fac_numero"] . ".pdf";
                if(!file_exists($url_facture_form)){
                    $erreur .= "<li> La facture " . $d_facture['fac_numero'] . " n'est pas générée en pdf</li>";
                }else{
                    $fac_id = $d_facture['fac_id'];
                    $etat_relance = intval($d_facture['fac_etat_relance']);
                    if(empty($etat_relance)){
                        $etat_relance = 0;
                    }
                    $prochaine_etape = $etat_relance+1;
                    $client = $d_facture['fac_client'];
                    // CREER LA RELANCE
                    $rel_date = date('Y-m-d');
                    $rel_date_rappel = date('Y-m-d H:i');

                    $sql="INSERT INTO Relances (rel_client, rel_opca, rel_utilisateur,rel_uti_identite, rel_etat_relance, rel_date, rel_titre, rel_comment, rel_date_rappel, rel_contact,rel_con_nom,rel_con_tel, rel_con_prenom, rel_envoie, rel_envoie_date,rel_envoie_uti)
                    VALUES (:rel_client, :rel_opca, :rel_utilisateur,:rel_uti_identite, :rel_etat_relance, :rel_date, :rel_titre, :rel_comment, :rel_date_rappel, :rel_contact,:rel_con_nom,:rel_con_tel, :rel_con_prenom, 1, NOW(),:rel_envoie_uti)";
                    $req = $Conn->prepare($sql);
                    $req->bindValue(":rel_client",$client);
                    $req->bindValue(":rel_opca",0);
                    $req->bindValue(":rel_utilisateur",$acc_utilisateur);
                    $req->bindValue(":rel_uti_identite",$utilisateur['uti_prenom'] . " " . $utilisateur['uti_nom']);
                    $req->bindValue(":rel_envoie_uti",$acc_utilisateur);
                    $req->bindValue(":rel_etat_relance",$prochaine_etape);
                    $req->bindValue(":rel_date",$rel_date);
                    $req->bindValue(":rel_titre", "");
                    $req->bindValue(":rel_comment", "Envoi automatique via la liste des relances");
                    $req->bindValue(":rel_date_rappel",$rel_date_rappel);
                    $req->bindValue(":rel_contact",$contact['con_id']);
                    $req->bindValue(":rel_con_nom",$contact['con_nom']);
                    $req->bindValue(":rel_con_prenom",$contact['con_prenom']);
                    $req->bindValue(":rel_con_tel",$contact['con_tel']);
                    $req->execute();
                    $relance = $Conn->lastInsertId();

                    $sql="INSERT INTO Relances_Factures (rfa_facture, rfa_facture_soc, rfa_facture_num, rfa_relance)
                    VALUES (:rfa_facture, :rfa_facture_soc, :rfa_facture_num, :rfa_relance)";
                    $req = $Conn->prepare($sql);
                    $req->bindValue(":rfa_facture",$d_facture['fac_id']);
                    $req->bindValue(":rfa_facture_soc",$soc);
                    $req->bindValue(":rfa_facture_num",$d_facture['fac_numero']);
                    $req->bindValue(":rfa_relance",$relance);
                    $req->execute();

                    // UPDATE ETAT RELANCE
                    $sql="UPDATE relances_Factures SET rfa_etat_relance=" . $prochaine_etape . "
                     WHERE rfa_relance =" . $relance . " AND rfa_facture=" . $fac_id . " AND rfa_facture_soc=" . $soc;
                    $req = $Conn->prepare($sql);
                    $req->execute();

                    $sql="UPDATE Factures SET fac_etat_relance=" . $prochaine_etape . ",fac_date_relance=NOW() WHERE fac_id=" . $fac_id;
                    $req = $ConnFct->prepare($sql);
                    $req->execute();
                    // ENVOYER LE MAIL
                    if($prochaine_etape == 1){
    					$sujet=" Factures arrivant à échéance " . $d_facture['fac_numero'] . " - " .  $sujet_societe;
    					$body_mail="Madame, Monsieur,<br><br>";
    					$body_mail=$body_mail . "Veuillez trouver, ci-joint, le relevé de vos factures en nos comptes.<br><br>";
    					$body_mail=$body_mail . "Nous vous rappelons que celle(s)-ci arrive(nt) à échéance dans les jours à venir.<br><br>";

    					$body_mail=$body_mail . "Nous restons à votre entière disposition pour tout complément d'information.<br><br>";
    					$body_mail=$body_mail . "Veuillez croire, Madame, Monsieur, en l'assurance de notre considération distinguée.<br><br>";
    				}else{
    					$sujet=$d_societe['soc_nom'] . " Factures en attente de règlement " . $d_facture['fac_numero'] . " - " .  $sujet_societe;
    					$body_mail="Madame, Monsieur,<br><br>";
    					$body_mail=$body_mail . "Sauf erreur ou omission de notre part, la mise à jour de votre compte client présente à ce jour un solde débiteur.<br><br>";
    					$body_mail=$body_mail . "En effet, les factures ci-jointes n'ont pas encore été honorées.<br><br>";

    					$body_mail=$body_mail . "L'échéance étant dépassée, nous vous demandons pour la bonne règle de nos écritures, de nous adresser le règlement";
    					$body_mail=$body_mail . " par courrier, ou de nous informer en cas de litige.<br><br>";
    					$body_mail=$body_mail . "Nous vous prions de croire, Madame, Monsieur, en l'assurance de notre considération distinguée.<br><br>";
    				}
                    // CONSTRUCTION DU TABLEAU
                    if(empty($emails)){
                            $emails = array();
                    }

                    //
                    if(empty($emails[$d_client['cli_id']]['factures'])){
                        $emails[$d_client['cli_id']]['factures'] = array();
                    }

                    $emails[$d_client['cli_id']]['sujet'] = $sujet;
                    $emails[$d_client['cli_id']]['body_mail'] = $body_mail;
                    $emails[$d_client['cli_id']]['factures'][$d_facture['fac_id']]['fac_numero'] = $d_facture['fac_numero'];
                    $emails[$d_client['cli_id']]['factures'][$d_facture['fac_id']]['url_facture_form'] = $url_facture_form;
                    $emails[$d_client['cli_id']]['contact'] = $contact['con_prenom'] . " " . $contact['con_nom'];
                    $emails[$d_client['cli_id']]['contact_mail'] = $contact['con_mail'];
                }

            }



        }
    }

    foreach($emails as $cli=>$e){

        $adr = [];
        $param = [];
        $param=array(
            "sujet" => $e['sujet'],
            "body_mail" => $e['body_mail'],
            "pj" => array(),
            "copie_cache" => array()
        );
        foreach($e['factures'] as $_fac_id => $fac){
            $param["pj"][]=array(
                "fichier" => $fac['url_facture_form'],
                "nom" => $fac['fac_numero'] . ".pdf"
            );
        }

        $adr[] = array(
            "adresse" => $e['contact_mail'],
            //"adresse" =>"alexandre.le.corre@si2p.mobi",
            "nom" => $e['contact']
        );
        // SIGNATURE
        $acc_utilisateur=0;
        if(!empty($_SESSION['acces']["acc_ref"])){
            if($_SESSION['acces']["acc_ref"]==1){
                $acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
            }
        }

        $req=$Conn->query("SELECT uti_nom,uti_prenom,uti_fonction,uti_mail,uti_tel,uti_mobile
        ,soc_logo_1,soc_qualite_1,soc_qualite_2,soc_site,soc_nom
        ,age_logo_1,age_qualite_1,age_qualite_2,age_site
        FROM Utilisateurs INNER JOIN Societes ON (Utilisateurs.uti_societe=Societes.soc_id)
        LEFT JOIN Agences ON (Utilisateurs.uti_agence=Agences.age_id) WHERE uti_id=" . $acc_utilisateur . ";");
        $d_utilisateur=$req->fetch();
        if(!empty($d_utilisateur)){

            if(!empty($d_utilisateur["uti_mail"]) AND !empty($_POST["copie"])){

                $param["copie_cache"]=array(
                    "nom" => $d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"],
                    "adresse" => $d_utilisateur["uti_mail"]
                );
            }


                if(!empty($d_utilisateur["age_logo_1"])){
                    $logo_1=$d_utilisateur["age_logo_1"];
                }else{
                    $logo_1=$d_utilisateur["soc_logo_1"];
                }

                $qualite_1="";
                if(!empty($d_utilisateur["age_qualite_1"])){
                    $qualite_1=$d_utilisateur["age_qualite_1"];
                }elseif(!empty($d_utilisateur["soc_qualite_1"])){
                    $qualite_1=$d_utilisateur["soc_qualite_1"];
                }

                $qualite_2="";
                if(!empty($d_utilisateur["age_qualite_2"])){
                    $qualite_2=$d_utilisateur["age_qualite_2"];
                }elseif(!empty($d_utilisateur["soc_qualite_2"])){
                    $qualite_2=$d_utilisateur["soc_qualite_2"];
                }

                $site="";
                if(!empty($d_utilisateur["age_site"])){
                    $site=$d_utilisateur["age_site"];
                }elseif(!empty($d_utilisateur["soc_site"])){
                    $site=$d_utilisateur["soc_site"];
                }

                $signature="<div style='color:#7e8082;font-family:Century Gothic,Verdana;font-size:11pt;width:350px;' >";
                    $signature.="<div style='border-bottom:1px solid #e31936;padding:5px;' >";
                        $signature.="<img src='https://www.si2p.eu/Documents/Societes/logos/" . $logo_1 . "' style='margin:5px;max-width:180px;' />";
                        $signature.="<div style='text-align:right;' >";
                            $signature.="<div style='margin-bottom:5px;font-size:11pt;' ><b>" . $d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"] . "</b></div>";
                            $signature.="<div style='color:#e31936;margin-bottom:5px;' >" . $d_utilisateur["uti_fonction"] . "</div>";
                            if(!empty($d_utilisateur["uti_tel"])){
                                $signature.="<div>" . $d_utilisateur["uti_tel"] . "</div>";
                            }
                            if(!empty($d_utilisateur["uti_mobile"])){
                                $signature.="<div>" . $d_utilisateur["uti_mobile"] . "</div>";
                            }
                            $signature.="<div>";
                                if(!empty($d_utilisateur["uti_mail"])){
                                    $signature.="<a href='mailto:" . $d_utilisateur["uti_mail"] . "' style='color:#2d589e;font-size:11pt;text-decoration:underline;font-family:Century Gothic;' >";
                                        $signature.=$d_utilisateur["uti_mail"];
                                    $signature.="</a>";
                                    $signature.="- ";
                                }
                                if(!empty($site)){
                                    $signature.="<a style='color:#2d589e;font-size:11pt;font-weight:bold;text-decoration:underline;font-family:Century Gothic;' href='" . $site . "'>";
                                        $signature.=$site;
                                    $signature.="</a>";
                                }
                            $signature.="</div>";
                        $signature.="</div>";
                    $signature.="</div>";
                    $signature.="<div style='background-color:#efefef;font-size:10pt;padding:5px;text-align:center;' >";
                        $signature.="<b>" . $d_utilisateur["soc_nom"] . "</b> <span style='color:#e31936;' > | </span> formation <span style='color:#e31936;'> | </span> sécurité <span style='color:#e31936;'> | </span> prévention";
                    $signature.="</div>";
                $signature.="</div>";
                if(!empty($qualite_1) OR !empty($qualite_2)){
                    $signature.="<div style='width:350px;' >";
                        if(!empty($qualite_1)){
                            $signature.="<div style='float:left;margin-top:27px;' >";
                                $signature.="<img src='https://www.si2p.eu/Documents/Societes/logos/" . $qualite_1 . "' />";
                            $signature.="</div>";
                        }
                        if(!empty($qualite_2)){
                            $signature.="<div style='float:left;' >";
                                $signature.="<img src='https://www.si2p.eu/Documents/Societes/logos/" . $qualite_2 . "' />";
                            $signature.="</div>";
                        }

                        $signature.="<div style='clear:both;' > </div>";
                    $signature.="</div>";
                }

                $param["body_mail"].=$signature;



        }

        $envoie_ok=envoi_mail("",$param,$adr, 1);
        if(!is_bool($envoie_ok)){
            $erreur .= "<li> Erreur d'envoi de mail facture " . $e['fac_numero'] . " : " . $envoie_ok . "</li>";
        }
    }


    if(!empty($erreur)){
        $_SESSION['message'][] = array(
    		"titre" => "Attention",
    		"type" => "warning",
    		"message" => "Veuillez prêter attention aux factures suivantes"
        );
        $_SESSION['erreur_relance'] = $erreur;
        header("location : facture_relance_etape_erreur.php");
        die();
    }else{
        $_SESSION['message'][] = array(
    		"titre" => "Succès",
    		"type" => "success",
    		"message" => "Etat relance est enregistrée"
        );

        header("location : facture_relance_liste.php");
        die();
    }
