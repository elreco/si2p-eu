<?php


// SUPPRESSION D'UN SUSPECT


include('includes/connexion_soc.php');
include('modeles/mod_del_suspect.php');

if(!empty($_POST)){

	$suspect=0;
	if(!empty($_POST['suspect'])){
		$suspect=intval($_POST['suspect']);
	}
	
	$erreur=0;
	
	if($suspect>0){
		
		
		
		$result=del_suspect($suspect);
		if(!is_bool($result)){
			$erreur=1;
			var_dump($result);
		}elseif(!$result){
			$erreur=1;
		}
		
	}else{
		
		if(!empty($_POST['check_list'])){
			
			foreach($_POST['check_list'] as $sus){
				
				$suspect=intval($sus);
				$result=del_suspect($suspect);				
				if(!is_bool($result)){
					var_dump($result);
					$erreur=1;
					break;
				}elseif(!$result){
					$erreur=1;
				}
			}
		}else{
			$erreur=1;
		}
	}
}

if($erreur==1){
	if(!empty($erreur)){
		$_SESSION['notifications']["type"]=1;
		$_SESSION['notifications']["message"]="Echec de la suppression";
	}
}
Header('Location: suspect_liste.php');