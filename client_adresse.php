<?php


include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');

// EDITION D'UNE ADRESSE client

if(!empty($_GET["client"])){
	$client=intval($_GET["client"]);
}else{
	echo("Erreur!");
	die();
}

$adresse=0;
if(!empty($_GET["adresse"])){
	$adresse=intval($_GET["adresse"]);	
};

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);	
}

// sur le client
$sql="SELECT cli_code,cli_categorie,cli_siren,cli_nom,cli_ident_tva FROM clients WHERE cli_id=" . $client . ";";
$req=$Conn->query($sql);
$d_client=$req->fetch();
if(empty($d_client)){
	echo("Erreur!");
	die();
}

$d_adresse=array(
	"adr_id" => 0,
	"adr_type" => 0,
	"adr_nom" => $d_client["cli_nom"],
	"adr_service" => "",
	"adr_ad1" => "",
	"adr_ad2" => "",
	"adr_ad3" => "",
	"adr_cp" => "",
	"adr_ville" => "",
	"adr_defaut" => 0,
	"adr_libelle" => "",
	"adr_siret" => "",
	"adr_geo" => 1
);



if($adresse>0){
	
	$sql="SELECT * FROM Adresses WHERE adr_id=" . $adresse . " AND adr_ref_id=" . $client . ";";
	$req=$Conn->query($sql);
	$d_adresse=$req->fetch();
}

// chercher tous les contacts du client
$sql="SELECT * FROM Contacts WHERE con_ref_id=" . $client . ";";
$req=$Conn->query($sql);
$contacts=$req->fetchAll();

//$_SESSION['retourAdresse'] = "client_voir.php?client=" . $client;
if(isset($_GET["onglet"])){
	$_SESSION['retourAdresse'].="&onglet=" . $_GET["onglet"];
}
?>
<!DOCTYPE html>
<html>  
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		
		<form id="filtre" action="client_adresse_enr.php" method="post" class="admin-form form-inline form-inline-grid" >
			<div>
				<input type="hidden" name="client" value="<?=$client?>" />	
				<input type="hidden" id="adresse" name="adresse" value="<?=$adresse?>" />				
			</div>
			<div id="main">
		<?php 	include "includes/header_def.inc.php"; ?>
		
				<section id="content_wrapper" class="">
					<section id="content" class="">						
						<div class="row">
							<div class="col-md-10 col-md-offset-1">							
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
									
										<!-- contenu du form -->
										<div class="panel-body bg-light">
										
											<div class="content-header">									
												<h2>
													<b class="text-primary"><?=$d_client["cli_code"]?></b> -
											<?php	if($adresse>0){ ?>
														 Edition d'une 
											<?php	}else{ ?>
														Nouvelle
											<?php	} ?>
													<b class="text-primary">adresse</b>
												</h2>
											</div>
									<?php	if($adresse==0){ ?>
												<div class="row">
													<div class="col-md-12">
														<label for="adr_type" >Type d'adresse : </label>
														<select id="adr_type" name="adr_type" class="form-control">
													<?php 	if($d_client['cli_categorie'] != 3){ ?>
																<option value="1" selected >Intervention</option>
													<?php 	} ?>
															<option value="2" >Facturation</option>
													<?php 	if($d_client['cli_categorie'] != 3){ ?>		
																<option value="3" >Envoi de facture</option>
													<?php	} ?>
														</select>											
													</div>
												</div>
									<?php	} ?>
											
											<div class="row mt15">
												<div class="col-md-12">
													<label for="adr_libelle" >Libellé :</label>
													<input type="text" id="adr_libelle" name="adr_libelle" class="gui-input" placeholder="Libellé de l'adresse" value="<?=$d_adresse["adr_libelle"]?>" />										
													<span class="input-footer">
														<strong>Info:</strong> Permet d'identifier l'adresse dans le carnet. Par défaut: Entité + Ville
													</span>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-12">
													<label for="adr_nom" >Nom :</label>
													<input type="text" id="adr_nom" name="adr_nom" class="gui-input" placeholder="Entité" value="<?=htmlentities($d_adresse["adr_nom"])?>" />
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-12">
													<label for="adr_service" >Service :</label>
													<input type="text" id="adr_service" name="adr_service" class="gui-input" placeholder="Service" value="<?=htmlentities($d_adresse["adr_service"])?>" />
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-12">
													<label for="adr_ad1" >Adresse :</label>
													<input type="text" id="adr_ad1" name="adr_ad1" class="gui-input" placeholder="Adresse 1" value="<?=htmlentities($d_adresse["adr_ad1"])?>" />
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<input type="text" id="adr_ad2" name="adr_ad2" class="gui-input" placeholder="Adresse 2" value="<?=htmlentities($d_adresse["adr_ad2"])?>" />
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<input type="text" id="adr_ad3" name="adr_ad3" class="gui-input" placeholder="Adresse 3" value="<?=htmlentities($d_adresse["adr_ad3"])?>" />
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<label class="field">
														<input type="text" id="adr_cp" name="adr_cp" class="gui-input code-postal" placeholder="Code Postal" required value="<?=$d_adresse["adr_cp"]?>" />
													  </label>
												</div>
												<div class="col-md-8">
													<label class="field">
														<input type="text" id="adr_ville" name="adr_ville" class="gui-input nom" placeholder="Ville" required value="<?=htmlentities($d_adresse["adr_ville"])?>" />
													  </label>
												</div>
											</div>
											<div class="row mt15">
													<div class="col-md-12 text-center">
														Zone géographique
														<div class="section">
															<label for="adr_geo1" class="mt15 option option-info">
																<input type="radio" id="adr_geo1" class="adr-geo" name="adr_geo" value="1" <?php if($d_adresse["adr_geo"]==1) echo("checked"); ?> />
																<span class="radio"></span> France
															</label>
															<label for="adr_geo2" class=" mt15 option option-info">
																<input type="radio" id="adr_geo2" class="adr-geo" name="adr_geo" value="2" <?php if($d_adresse["adr_geo"]==2) echo("checked"); ?> />
																<span class="radio"></span> UE
															</label>
															<label for="adr_geo3" class=" mt15 option option-info">
																<input type="radio" id="adr_geo3" class="adr-geo" name="adr_geo" value="3" <?php if($d_adresse["adr_geo"]==3) echo("checked"); ?> />
																<span class="radio"></span> Autre
															</label>
														</div>
													</div>
												</div>
							<?php			if($d_client['cli_categorie'] != 3){
								
												// SI LE client N'EST PAS UN PARTICULIER
												
												if($d_adresse['adr_defaut']==0){
													// autre que particulier if($d_adresse["adr_defaut"]==0){ ?>
													<div class="row mt15">
														<div class="col-md-12">
															<label class="option block mn">
																<input type="checkbox" name="adr_defaut" value="on" />
																<span class="checkbox mn"></span> Adresse par défaut
															</label>
														</div>
													</div>
							<?php 				}else{ ?>
													<div class="row mt15">
														<div class="col-md-12">
															<b>Il s'agit de l'adresse par défaut.</b>
														</div>
													</div>
							<?php 				}
												if($adresse==0 OR $d_adresse["adr_type"]==2){ ?>
												
													<div class="row facturation mt15" <?php if($d_adresse["adr_type"]!=2) echo("style='display:none;'"); ?> >
														
														<div class="col-md-5">
															<label for="cli_siren" >Siren :</label>
															<div class="field prepend-icon">
																<input type="text" name="cli_siren" id="cli_siren" class="gui-input siren" placeholder="Siren" value="<?=$d_client['cli_siren']?>" <?php if(!empty($d_client['cli_siren'])) echo("readonly"); ?> />
																<span class="field-icon">
																	<i class="fa fa-barcode"></i>
																</span>
															</div>
													<?php 	if(!empty($d_client['cli_siren'])){ ?>
																<span class="input-footer">
																	<strong>Info: </strong>&Eacute;ditez la fiche client pour mettre à jour le siren.
																</span>
													<?php 	} ?>
														</div>
														<div class="col-md-2">
															<label for="cli_siren" >Nic :</label>
															<div class="field prepend-icon">
																<input type="text" name="adr_siret" id="adr_siret" class="gui-input siret" placeholder="Nic" value="<?=$d_adresse["adr_siret"]?>" <?php if(!empty($d_adresse["adr_siret"])) echo("required"); ?> />
																<span for="cli_siret" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</span>
															</div>
															<div id="result_test" ></div>
														</div>
														<div class="col-md-1 pt15">
															<button type="button" class="btn btn-warning btn-sm" id="check_siret" data-toggle="tooltip" data-placement="top" title="Vérifier les doublons" >
																<i class="fa fa-check" ></i>
															</button>
														</div>
											<?php		if(!empty($_SESSION['acces']['acc_droits'][36])){ ?>
															
															<div class="col-md-4 pt25" >
																<label class="option option-dark">
																	<input type="checkbox" id="siret_no_check" value="no_check" name="siret_no_check">
																	<span class="checkbox"></span>
																	<label for="siret_no_check">Désactiver la vérification Siret</label>
																</label>
															</div>
												<?php 	}	?>		
														
														
													</div>
							<?php  				} ?>
												
							<?php				if(empty($d_client["cli_ident_tva"])){ ?>
													<div class="row mt15" id="bloc_tva" <?php if($d_adresse["adr_geo"]<2) echo("style='display:none'"); ?>  >
														<div class="col-md-12" >
															<label for="cli_siren" >Numéro de TVA :</label>
															<div class="field prepend-icon">
																<input type="text" name="cli_ident_tva" id="cli_ident_tva" class="gui-input" placeholder="Numéro de TVA" value="" />
																<span for="cli_siret" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</span>
															</div>
														</div>
													</div>
							<?php				} ?>
												
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Contacts</span> 
														</div>
													</div>
												</div>
												
												<div class="row mt15">
													<table class="table table-striped table-hover" >
													<thead>
														<tr class="dark">
															<th class="text-center">Nom du contact</th>
															<th class="text-center">Associé</th>
															<th class="text-center">Par défaut</th>													
														</tr>
													</thead>
													<tbody>
														<?php foreach($contacts as $c){ 
														$sql="SELECT * FROM Adresses_Contacts WHERE aco_contact=" . $c['con_id'] . " AND aco_adresse = " . $d_adresse['adr_id'];
														
														$req=$Conn->query($sql);
														$contact_adresse=$req->fetch();
														?>
														<tr>
															<td class="text-center">
																<p>
																	<?= $c['con_prenom'] ?> <?= $c['con_nom'] ?>
																</p>
															</td>
															<td class="text-center">
																<label class="option block mn">
																	<input type="checkbox" class="aco_contact" name="aco_contact[<?= $c['con_id'] ?>]" value="<?= $c['con_id'] ?>" 
																	<?php if(!empty($contact_adresse)){ ?>
																		checked
																	<?php }?>
																	/>
																	<span class="checkbox mn"></span>
																	<input id="aco_contact_<?= $c['con_id'] ?>" name="aco_contact[<?= $c['con_id'] ?>]" type="hidden" value="<?= $c['con_id'] ?>" />
																</label>
															</td>
															<td class="text-center">
																<label class="option block mn">
																	<input type="radio" class="aco_defaut" name="aco_defaut[]" value="<?= $c['con_id'] ?>" 
																	
																	<?php if(!empty($contact_adresse['aco_defaut'])){ ?>
																		checked
																	<?php }?>/>
																	
																	<span class="radio mn"></span>
																	
																</label>
															</td>
														</tr>
														<?php }?>
														</tbody>
													</table>
													
												</div>
							<?php
							 	
											} 
											// FIN TRAITEMENT HORS PARTICULIER ?>
							
											
														
										</div>
										<!-- fin de contenu form -->
										
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-4 footer-left">
						<a href="<?=$_SESSION['retourAdresse']?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-4 footer-middle"></div>
					<div class="col-xs-4 footer-right">
						<button type="submit" class="btn btn-success btn-sm">
							<i class='fa fa-floppy-o'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php
		include "includes/footer_script.inc.php"; ?>   
		<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js" ></script>
		<script type="text/javascript" src="assets/js/custom.js" ></script>
		<script type="text/javascript">
			var societe="<?=$acc_societe?>";
			var agence=0;
			function update_contacts(value, checked_radio, checked_checkbox){
				if(checked_checkbox == true && checked_radio == true){
					$(".aco_contact[value=" + value + "]").prop("disabled", true);

					$(".aco_defaut[value=" + value + "]").prop("disabled", false);
					
				}else if(checked_checkbox == true && checked_radio == false){
					$(".aco_contact[value=" + value + "]").prop("disabled", false);
					$(".aco_defaut[value=" + value + "]").prop("disabled", false);
					$("#aco_contact_" + value).attr("value",value);
				}else if(checked_checkbox == false && checked_radio == false){
					$(".aco_contact[value=" + value + "]").prop("disabled", false);
					$(".aco_defaut[value=" + value + "]").prop("disabled", true);
					$("#aco_contact_" + value).attr("value","");
				}else{
					$(".aco_contact[value=" + value + "]").prop("disabled", false);
					$(".aco_defaut[value=" + value + "]").prop("disabled", false);

				}
				
			}
			
			jQuery(document).ready(function(){
				// GESTION DU RADIO ET CHECKBOX CONTACTS
				$(".aco_contact").each(function() {
					if($(this).is(":checked")){
						checked_checkbox = true;
					}else{
						checked_checkbox = false;
					}

					if($(".aco_defaut[value=" + $(this).val() + "]").is(":checked")){
						checked_radio = true;
					}else{
						checked_radio = false;
					}
					update_contacts($(this).val(), checked_radio, checked_checkbox);
				});
				
				
				$(".aco_contact").change(function() {
					$(".aco_contact").each(function(index, el) {
						if($(this).is(":checked")){
							checked_checkbox = true;
						}else{
							checked_checkbox = false;
						}

						if($(".aco_defaut[value=" + $(this).val() + "]").is(":checked")){
							checked_radio = true;
						}else{
							checked_radio = false;
						}

						update_contacts($(this).val(), checked_radio, checked_checkbox);
					});
				});
				
				$(".aco_defaut").change(function() {

					$(".aco_contact").each(function() {

						if($(".aco_defaut[value=" + $(this).val() + "]").is(":checked")){
							checked_checkbox = true;
						}else{
							checked_checkbox = false;

						}

						if($(this).is(":checked")){
							checked_radio = true;
						}else{
							checked_radio = false;
						}
						update_contacts($(this).val(), checked_radio, checked_checkbox);
					});
				});
				
				// FIN GESTION DU RADIO ET CONTACT
				$("#adr_type").change(function(){	
					if($(this).val()==2){
						$(".facturation").show();						
					}else{
						$(".facturation").hide();						
					}
				});
				
				$(".adr-geo").click(function(){
					console.log("adr-geo change");
					if($("#bloc_tva").length==1){
						if($(this).val()>1){
							$("#bloc_tva").show();
						}else{
							$("#bloc_tva").hide();
						}
					}
				});
				
				// verif du siret
				$( "#check_siret" ).click(function(){
					check_siret($("#cli_siren").val(),$("#adr_siret").val(),$("#adresse").val(),afficher_check_siret,"","");
				});
				
			});
			
			function afficher_check_siret(json,siren,siret){
				if(json){
					if(json==""){
						afficher_txt_user($("#result_test"),"Ce siret est libre","text-success",5000);
					}else{
						var erreur_txt="";
						
						erreur_txt="<p>Le siret " + siren + " " + siret + " est déjà utilisé pour :</p>";		
						erreur_txt=erreur_txt+"<ul>";
							$.each(json, function( index, item ) {
								if(item.source=="cli"){
									erreur_txt=erreur_txt+"<li>";	
										erreur_txt=erreur_txt+ "le client " + item.code;
										if(item.societes!=""){
											erreur_txt=erreur_txt+"sur les sociétés :<ul>";
												$.each(item.societes, function(j,soc){
													erreur_txt=erreur_txt+ "<li>" + soc + "</li>";	
												})
											erreur_txt=erreur_txt+"</ul>";
										}
									erreur_txt=erreur_txt+"</li>";	
								}else{
									erreur_txt=erreur_txt+"<li>";	
										erreur_txt=erreur_txt+ "le suspect " + item.code;										
									erreur_txt=erreur_txt+"</li>";
								}								
							})
						erreur_txt=erreur_txt+"</ul></p>";
						modal_alerte_user(erreur_txt,"");
					}
				}
			}
		</script>
	</body>
</html>
