<?php

// AFFICHE LA LISTE DES FACTURES

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');
include('modeles/mod_get_correspondances.php');
include('modeles/mod_get_adresses.php');
include('modeles/mod_orion_utilisateur.php');
include('modeles/mod_get_client_infos.php');

// DONNEE UTILE AU PROGRAMME
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
$etape=0;
if(!empty($_SESSION['etape'])){
    $etape = $_SESSION['etape'];
}
$etape_chrono=0;
if(!empty($_SESSION['etape_chrono'])){
    $etape_chrono = $_SESSION['etape_chrono'];
}

if($etape > 0){
    $etape_ref = $etape;
}else{
    $etape_ref = $etape_chrono;
}

$client=0;
if(!empty($_GET['client'])){
	$client=intval($_GET['client']);
}else{
    echo "probleme";
    die();
}

// RETOUR
$origine="poste";
$origine_fac="actvoir";
if(isset($_GET['opca'])){
    $_SESSION['retour'] = "facture_relance_voir.php?opca&client=" . $client;
	$_SESSION['retourContact'] = "facture_relance_voir.php?opca&client=" . $client;
	$_SESSION['retourClient'] = "facture_relance_voir.php?opca&client=" . $client;
	$_SESSION['retourAdresse'] = "facture_relance_voir.php?opca&client=" . $client;
	$_SESSION['retourFacture'] = "facture_relance_voir.php?opca&client=" . $client;
	$_SESSION['retourliste'] = "facture_relance_voir.php?opca&client=" . $client;
	$_SESSION['retourRelance'] = "facture_relance_voir.php?opca&client=" . $client;
}else{
    $_SESSION['retour'] = "facture_relance_voir.php?client=" . $client;
	$_SESSION['retourContact'] = "facture_relance_voir.php?client=" . $client;
	$_SESSION['retourClient'] = "facture_relance_voir.php?client=" . $client;
	$_SESSION['retourAdresse'] = "facture_relance_voir.php?client=" . $client;
	$_SESSION['retourFacture'] = "facture_relance_voir.php?client=" . $client;
	$_SESSION['retourliste'] = "facture_relance_voir.php?client=" . $client;
	$_SESSION['retourRelance'] = "facture_relance_voir.php?client=" . $client;
}


$sql="SELECT cli_id, cli_nom, cli_code,cli_releve,cli_affacturage,cli_affacturage_iban,cli_groupe,cli_reg_fdm,cli_ident_tva,cli_filiale_de,cli_blackliste
,cli_filiale_de,cli_interco_soc,cli_reg_formule,cli_reg_nb_jour,cli_facture_opca
,cli_categorie,cli_sous_categorie
FROM Clients WHERE cli_id = " . $client;
$req = $Conn->query($sql);
$d_client=$req->fetch();


// CONTACTS IMPAYES
$sql="SELECT DISTINCT con_id,con_fonction,con_fonction_nom,con_titre,con_nom,con_prenom,con_tel,con_portable,con_fax,con_mail,con_compta,con_comment,aco_contact
FROM Contacts LEFT OUTER JOIN Adresses_Contacts ON (Contacts.con_id=Adresses_Contacts.aco_contact AND aco_defaut)
WHERE con_ref_id=" . $client . " AND con_ref=1 AND con_compta = 1;";
$req=$Conn->query($sql);
$contacts=$req->fetchAll();

$sql="SELECT DISTINCT con_id,con_fonction,con_fonction_nom,con_titre,con_nom,con_prenom,con_tel,con_portable,con_fax,con_mail,con_compta,con_comment,aco_contact
FROM Contacts LEFT OUTER JOIN Adresses_Contacts ON (Contacts.con_id=Adresses_Contacts.aco_contact AND aco_defaut)
WHERE con_ref_id=" . $client . " AND con_ref=1 AND con_compta = 0;";
$req=$Conn->query($sql);
$contacts_autres=$req->fetchAll();

// CONTACTS CLIENT

//contact par defaut
$d_con_fonction_nom=array(
    "0" => ""
);
$sql="SELECT * FROM contacts_fonctions ORDER BY cfo_libelle";
$req=$Conn->query($sql);
$d_contact_fonctions=$req->fetchAll();
foreach($d_contact_fonctions as $d){
    $d_con_fonction_nom[$d["cfo_id"]]=$d["cfo_libelle"];
}


$req="SELECT * FROM societes WHERE soc_archive = 0";
$req=$Conn->query($req);
$d_societes=$req->fetchAll();

$adresses_fac = get_adresses(1,$d_client['cli_id'],2);
if(!empty($adresses_fac)){
    if($adresses_fac[0]["adr_defaut"]==1){
        $adresse_fac_def=$adresses_fac[0];
        $adresse_fac="";
        if(!empty($adresse_fac_def["adr_nom"])){
            $adresse_fac=$adresse_fac_def["adr_nom"];
        }
        if(!empty($adresse_fac_def["adr_service"])){
            if(!empty($adresse_fac)){
                $adresse_fac.="<br/>";
            }
            $adresse_fac.=$adresse_fac_def["adr_service"];
        }
        if(!empty($adresse_fac_def["adr_ad1"])){
            if(!empty($adresse_fac)){
                $adresse_fac.="<br/>";
            }
            $adresse_fac.=$adresse_fac_def["adr_ad1"];
        }
        if(!empty($adresse_fac_def["adr_ad2"])){
            if(!empty($adresse_fac)){
                $adresse_fac.="<br/>";
            }
            $adresse_fac.=$adresse_fac_def["adr_ad2"];
        }
        if(!empty($adresse_fac_def["adr_ad3"])){
            if(!empty($adresse_fac)){
                $adresse_fac.="<br/>";
            }
            $adresse_fac.=$adresse_fac_def["adr_ad3"];
        }
        if(!empty($adresse_fac)){
            $adresse_fac.="<br/>";
        }
        $adresse_fac.=$adresse_fac_def["adr_cp"] . " " . $adresse_fac_def["adr_ville"];
    }
}
$drt_edit_adr=false;
if($d_client['cli_categorie']!=3 AND (empty($d_client['cli_interco_soc']) OR $_SESSION["acces"]["acc_droits"][9])){
    $drt_edit_adr=true;
}
$d_info_type=array();
$sql="SELECT cin_id,cin_libelle FROM Clients_Infos ORDER BY cin_libelle;";
$req=$Conn->query($sql);
$d_result=$req->fetchAll();
foreach($d_result as $d){
    $d_info_type[$d["cin_id"]]=$d["cin_libelle"];
}
$infos=get_client_infos($client,0,0,0,0);
$sql="SELECT cli_agence,cli_rib,cli_rib_change,cli_archive,com_label_1,com_label_2 FROM Clients LEFT OUTER JOIN Commerciaux ON (Clients.cli_commercial=Commerciaux.com_id)
WHERE cli_id = " . $client;
if($acc_agence>0){
    $sql.=" AND cli_agence=" . $acc_agence;
}
if(!$_SESSION['acces']["acc_droits"][6]){
    $sql.=" AND cli_utilisateur=" . $acc_utilisateur;
}
$sql.=";";
$req=$ConnSoc->query($sql);
$d_client_societe=$req->fetchAll();

?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
		<!-- CSS PLUGINS -->
        <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php
include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">
                        <h1>Poste client <?= $d_client['cli_nom'] ?>
                        <?php if($d_client['cli_groupe'] == 1 && empty($d_client['cli_filiale_de'])){ ?>
                            <span class="text-primary" style="font-size:15px;">Maison mère</span>
                        <?php }elseif($d_client['cli_groupe'] == 1 && !empty($d_client['cli_filiale_de'])){
                            $req="SELECT cli_code FROM Clients WHERE cli_id = " . $d_client['cli_filiale_de'];
                            $req=$Conn->query($req);
                            $maison_mere=$req->fetch();
                            ?>
                            <span class="text-primary" style="font-size:15px;">Filiale de <?= $maison_mere['cli_code'] ?></span>
                        <?php }?>
                        </h1>
                        <div class="row mt15">
                            <div class="col-md-6">
		                		<div class="panel mb10">
		                            <div class="panel-heading panel-head-sm">
		                                <span class="panel-icon">
		                                    <i class="fa fa-file-o" aria-hidden="true"></i>
		                                </span>
		                                <span class="panel-title"> Contacts suivi des impayés</span>
		                            </div>
		                            <div class="panel-body pn">
                                    <?php 	if(!empty($contacts)){
										?>
                                        <table class="table dataTable table-striped table-hover pn" id="tab_contact" >
                                            <thead>
                                                <tr class="dark">
                                                    <th class="text-center" >Fonction</th>
                                                    <th class="text-center" >Nom</th>
                                                    <th class="text-center" >Tel</th>
                                                    <th class="text-center" >Portable</th>
                                                    <th class="text-center" >Fax</th>
                                                    <th class="text-center" >Mail</th>
                                                    <th class="text-center">Commentaire</th>
                                                    <th class="text-center no-sort" >Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    <?php 		foreach($contacts as $c){

                                                    $comment="";
                                                    if(!empty($c['con_comment'])){
                                                        $comment=str_replace(CHR(13),"<br/>",$c['con_comment']);
                                                    }?>
                                                    <tr id="contact_<?=$c['con_id']?>" class="contact" >
                                                        <td>
                                    <?php 					if(!empty($c['con_fonction'])){
                                                                echo($d_con_fonction_nom[$c['con_fonction']]);
                                                            }else{
                                                                echo($c['con_fonction_nom']);
                                                            } ?>
                                                        </td>
                                                        <td>
                                                        <?php foreach($base_civilite as $k => $v): ?>
                                                            <?php if($k == $c['con_titre']): ?>
                                                            <?= $v; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                        <?= $c['con_prenom']; ?> <?= $c['con_nom']; ?>
                                                        </td>
                                                        <td><?= $c['con_tel']; ?></td>
                                                        <td><?= $c['con_portable']; ?></td>
                                                        <td><?= $c['con_fax']; ?></td>
                                                        <td><?= $c['con_mail']; ?></td>
                                                        <td><?=$c['con_comment']?></td>
                                                        <td>
                                                            <a href="client_contact.php?client=<?=$client?>&contact=<?=$c['con_id']?>&onglet=12" class="btn btn-warning btn-sm" >
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                    <?php 	if(empty($c['aco_contact'])){ ?>
                                                                <button type="button" class="btn btn-danger btn-sm delcontact" data-contact="<?=$c['con_id']?>" >
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                    <?php 	} ?>
                                                        </td>
                                                    </tr>
                                    <?php 		} ?>
                                            </tbody>
                                        </table>
                            <?php 	} ?>

                                    <div class="alert alert-warning" id="no_contact" <?php if(!empty($contacts)) echo("style='display:none;'"); ?> >
                                        Pas de contact "impayé" pour ce client.
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-md-6">
		                		<div class="panel mb10">
		                            <div class="panel-heading panel-head-sm">
		                                <span class="panel-icon">
		                                    <i class="fa fa-file-o" aria-hidden="true"></i>
		                                </span>
		                                <span class="panel-title"> Autres contacts</span>
		                            </div>
		                            <div class="panel-body pn">
                                    <?php 	if(!empty($contacts_autres)){ ?>
                                        <table class="table dataTable table-striped pn table-hover" id="tab_contact" >
                                            <thead>
                                                <tr class="dark">
                                                    <th class="text-center" >Fonction</th>
                                                    <th class="text-center" >Nom</th>
                                                    <th class="text-center" >Tel</th>
                                                    <th class="text-center" >Portable</th>
                                                    <th class="text-center" >Fax</th>
                                                    <th class="text-center" >Mail</th>
                                                    <th class="text-center" >Commentaire</th>
                                                    <th class="text-center no-sort" >Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    <?php 		foreach($contacts_autres as $c){

                                                    $comment="";
                                                    if(!empty($c['con_comment'])){
                                                        $comment=str_replace(CHR(13),"<br/>",$c['con_comment']);
                                                    }?>
                                                    <tr id="contact_<?=$c['con_id']?>" class="contact" >
                                                        <td>
                                    <?php 					if(!empty($c['con_fonction'])){
                                                                echo($d_con_fonction_nom[$c['con_fonction']]);
                                                            }else{
                                                                echo($c['con_fonction_nom']);
                                                            } ?>
                                                        </td>
                                                        <td>
                                                        <?php foreach($base_civilite as $k => $v): ?>
                                                            <?php if($k == $c['con_titre']): ?>
                                                            <?= $v; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                        <?= $c['con_prenom']; ?> <?= $c['con_nom']; ?>
                                                        </td>
                                                        <td><?= $c['con_tel']; ?></td>
                                                        <td><?= $c['con_portable']; ?></td>
                                                        <td><?= $c['con_fax']; ?></td>
                                                        <td><?= $c['con_mail']; ?></td>
                                                        <td><?=$c['con_comment']?></td>
                                                        <td>
                                                            <a href="client_contact.php?client=<?=$client?>&contact=<?=$c['con_id']?>&onglet=12" class="btn btn-warning btn-sm" >
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                    <?php 	if(empty($c['aco_contact'])){ ?>
                                                                <button type="button" class="btn btn-danger btn-sm delcontact" data-contact="<?=$c['con_id']?>" >
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                    <?php 	} ?>
                                                        </td>
                                                    </tr>
                                    <?php 		} ?>
                                            </tbody>
                                        </table>
                            <?php 	} ?>

                                        <div class="alert alert-warning mb0" id="no_contact" <?php if(!empty($contacts_autres)) echo("style='display:none;'"); ?> >
                                            Pas d'autres contacts pour ce client.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt15">
                            <div class="col-md-12">
                                <form method="post" action="facture_relance_enr.php" id="formul">
                                <input type="hidden" name="client" value="<?=$client?>" />
                                <?php
                                if(!empty($d_societes)){
                                    $nbFac = 0;
                                    foreach($d_societes as $d_societe){
									// PAGINATION
									if (isset($_GET['page']) && isset($_GET['societe']) && $_GET['societe'] == $d_societe['soc_id']) {
									    $page = $_GET['page'];
										$societe = $_GET['societe'];
									} else {
									    $page = 1;
									}

									$no_of_records_per_page = 20;
									$offset = ($page-1) * $no_of_records_per_page;

                                    $ConnFct = connexion_fct($d_societe['soc_id']);
                                    // FACTURES CLIENT
                                    $listeCom ="";
                                    if($_SESSION['acces']['acc_profil'] == 3){
                                        $sql="SELECT com_id FROM Commerciaux WHERE com_utilisateur =" . $acc_utilisateur;
                                        $req = $Conn->query($sql);
                                        $d_commerciaux=$req->fetchAll();

                                        foreach($d_commerciaux as $c){
                                            $listCom = $listeCom . "," . $c['com_id'];
                                        }
                                    }
                                    $total_ht=0;
                                    $total_ttc=0;
                                    $total_regle=0;
                                    $total_reste_du=0;

                                    $sql_fac="SELECT DISTINCT fac_id,fac_numero,fac_date,fac_total_ht,fac_date_reg_prev,fac_total_ttc,fac_regle,fac_nature,fac_etat_relance,fac_date_reg_rel,fac_opca";
                                    $sql_fac=$sql_fac . " ,fac_date_relance,fac_relance_stop";
                                    $sql_fac=$sql_fac . ",cli_id,cli_code";
                                    $sql_fac=$sql_fac . " FROM Factures,Clients";
                                    $sql_fac=$sql_fac . " WHERE fac_client=cli_id AND (fac_total_ttc-fac_regle>0.5) AND fac_nature=1";



                                    if(!empty($d_client['cli_categorie']) AND !empty($d_client['cli_sous_categorie']) AND $d_client['cli_categorie'] == 4){

                                        $sql_fac=$sql_fac . " AND (fac_opca=" . $client . " OR fac_client=" . $client . ")";
                                    }else{
                                        if($d_client['cli_groupe'] == 1 && empty($d_client['cli_filiale_de'])){
                                            $sql_fac=$sql_fac . " AND (cli_id=" . $client . " OR cli_filiale_de=" . $client . ")";
                                        }else{
                                            $sql_fac=$sql_fac . " AND cli_id=" . $client;
                                        }
                                    }

                                    if($_SESSION['acces']['acc_profil'] == 3){

                                        $sql_fac=$sql_fac . " AND fac_commercial IN (" . $listeCom . ")";
                                    }


									/*var_dump($sql_fac);
									echo("<br/>");*/
                                    $req=$ConnFct->query($sql_fac);

									$sql_fac=$sql_fac . " ORDER BY fac_date_reg_prev,fac_numero ";
									// $sql_fac = $sql_fac . " LIMIT $offset, $no_of_records_per_page";
									$req=$ConnFct->query($sql_fac);
									$factures = $req->fetchAll();

                                    $listeFac=array();


                                    $sql="SELECT * FROM Relances_Etats ORDER BY ret_j_deb";
                                    $req = $Conn->query($sql);
                                    $relances=$req->fetchAll();
                                    $d_statut_libelle=array();

                                    $nbEtat = 0;

                                    $rel_titre="";
                                    $rel_date_rappel="";
                                    $ret_traitement=0;
                                    $relance_nom[0] = array(
                                        "ret_id" => 0,
                                        "ret_libelle"=>"",
                                        "ret_code" => "",
                                        "ret_j_deb" => 0,
                                        "ret_j_fin" => 0,
                                        "ret_traitement" => 0
                                    );
                                    $d_statut_libelle=array();
                                    foreach($relances as $re){
                                        $d_statut_libelle[$re['ret_id']]= $re['ret_libelle'];
                                        $nbEtat = $nbEtat +1;
                                        $relance_nom[$nbEtat]["ret_id"] = $re['ret_id'];
                                        $relance_nom[$nbEtat]["ret_code"] = $re['ret_code'];
                                        $relance_nom[$nbEtat]["ret_libelle"] = $re['ret_libelle'];
                                        $relance_nom[$nbEtat]["ret_j_deb"] = $re['ret_j_deb'];
                                        $relance_nom[$nbEtat]["ret_j_fin"] = $re['ret_j_fin'];
                                        $relance_nom[$nbEtat]["ret_traitement"] = $re['ret_traitement'];

                                        if($re['ret_id'] == $etape){
                                            $rel_titre=$re['ret_libelle'];
                                            $ret_traitement=$re['ret_traitement'];
                                            if($re['ret_traitement'] == 1){
                                                $demain = new DateTime();
                                                $demain->modify('+1 day');
                                                $rel_date_rappel = $demain->format('Y-m-d');
                                            }
                                        }

                                    }

                                ?>


                                    <?php if(!empty($factures)){ ?>

                                        <h4 class="text-primary text-left"><?= $d_societe['soc_code'] ?></h4>

                                        <div class="table-responsive mt15" id="table_societe_<?= $d_societe['soc_id'] ?>">

                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr class="dark2" >
                                                        <th class="no-sort"><input type="checkbox" onClick="selectMultiFac(this,<?=$d_societe['soc_id']?>);" /></th>
                                                        <th>Facture</th>
                                                        <?php
                                                        if($d_client['cli_groupe'] == 1 && empty($d_client['cli_filiale_de'])){ ?>
                                                                <th>Client</th>
                                                        <?php } ?>
                                                        <th>Date</th>
                                                        <th>Date de réglement</th>
                                                        <th>Date de suspension</th>
                                                        <th class="text-right">Retard</th>
                                                        <th class="text-right">TTC</th>
                                                        <th class="text-right">Réglé</th>
                                                        <th class="text-right">Reste dû</th>
                                                        <th class="text-center">&Eacute;tat relance</th>
                                                        <th>Date relance</th>
                                                        <th>OPCA</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        <?php

                                                    foreach($factures as $f){


                                                        $facture = $f['fac_id'];
                                                        $fac_date="";
                                                        if(!empty($f['fac_date'])){
                                                            $dt_fac_date=date_create_from_format("Y-m-d",$f['fac_date']);
                                                            $fac_date=$dt_fac_date->format("d/m/Y");
                                                        }
                                                        $fac_date_reg_prev="";
                                                        if(!empty($f['fac_date_reg_prev'])){
                                                            $dt_fac_date_reg_prev=date_create_from_format("Y-m-d",$f['fac_date_reg_prev']);
                                                            $fac_date_reg_prev=$dt_fac_date_reg_prev->format("d/m/Y");
                                                        }
                                                        $fac_date_reg_rel="";
                                                        if(!empty($f['fac_date_reg_rel'])){
                                                            $dt_fac_date_reg_rel=date_create_from_format("Y-m-d",$f['fac_date_reg_rel']);
                                                            $fac_date_reg_rel=$dt_fac_date_reg_rel->format("d/m/Y");
                                                        }


                                                        // style de la ligne
                                                        $facture_opca=false;
                                                        $fac_opca=0;
                                                        if(!empty($f['fac_opca'])){
                                                            $fac_opca = $f['fac_opca'];
                                                            if(!empty($d_client['cli_categorie']) && !empty($d_client['cli_sous_categorie']) && $d_client['cli_categorie'] == 4 AND $d_client['cli_sous_categorie'] ==5){
                                                                $facture_opca=false;
                                                            }else{
                                                                $facture_opca=true;
                                                            }
                                                        }



                                                        // RETARD
                                                        $retard=0;

                                                        if($f['fac_nature'] == 1){
                                                            if(!empty($f['fac_date_reg_rel'])){
                                                                $datetime1 = new DateTime($f['fac_date_reg_rel']);
                                                                $datetime2 = new DateTime();
                                                                $interval = $datetime1->diff($datetime2);
                                                                $retard = $interval->format('%r%a');
                                                            }else{
                                                                $datetime1 = new DateTime($f['fac_date_reg_prev']);
                                                                $datetime2 = new DateTime();
                                                                $interval = $datetime1->diff($datetime2);
                                                                $retard = $interval->format('%r%a');
                                                            }
                                                        }else{
                                                            $fac_date_reg = " ";
                                                        }
                                                        if(empty($f['fac_relance_stop'])){

                                                            if(empty($listeFac[$d_societe['soc_id']])){
                                                                $listeFac[$d_societe['soc_id']]=$facture;

                                                            }else{
                                                                $listeFac[$d_societe['soc_id']]=$listeFac[$d_societe['soc_id']] . "," . $facture;

                                                            }

											                $nbFac=$nbFac+1;
                                                        }

                                                         // OPCA
                                                         if(!empty($f['fac_opca'])){
                                                            $sql="SELECT cli_nom FROM Clients WHERE cli_id = " . $f['fac_opca'];
                                                            $req = $Conn->query($sql);
                                                            $d_opca=$req->fetch();
                                                         }



                                                        $reste_du=$f['fac_total_ttc']-$f['fac_regle'];

                                                        $total_ht+=$f['fac_total_ht'];
                                                        $total_ttc+=$f['fac_total_ttc'];
                                                        $total_regle+=$f['fac_regle'];
                                                        $total_reste_du+=$reste_du;



                                                        $min_relance=0;
									                    $etat_relance=0;
                                                        foreach($relance_nom as $k=>$ren){
                                                            if($f['fac_etat_relance'] == $ren['ret_id']){

                                                                $code_relance = $ren['ret_code'];
                                                                $traite_relance = $ren['ret_traitement'];
                                                                $num_relance = $k;
                                                            }
                                                        }
                                                        if($traite_relance == 0){
                                                            if($num_relance<$nbEtat){
                                                                if($retard>=$relance_nom[$num_relance+1]["ret_j_deb"]){
                                                                    $min_relance =$num_relance;
                                                                }else{
                                                                    $min_relance=999;
                                                                }
                                                            }
                                                        }else{
                                                            $min_relance = $num_relance;

                                                        }

                                                        $etat_relance = $f['fac_etat_relance'];
                                                        $style_tr="";
                                                        $style_a="";
                                                        $id_tr="";
                                                        if($f['fac_nature'] == 1){
                                                            $id_tr="id='tab_ligne_" . $nbFac . "'";
                                                            $id_numero="id='numero_ligne_" . $nbFac . "'";
                                                            $id_client="id='client_ligne_" . $nbFac . "'";
                                                            $id_date="id='date_ligne_" . $nbFac . "'";
                                                            $id_regle="id='regle_ligne_" . $nbFac . "'";
                                                            $id_du="id='du_ligne_" . $nbFac . "'";
                                                            $id_etat="id='etat_ligne_" . $nbFac . "'";
                                                            $id_opca="id='opca_ligne_" . $nbFac . "'";

                                                            if($f['fac_relance_stop']){
																if($f['fac_relance_stop'] == 1){
																	$style_tr="style='color:orange;'";
																}elseif($f['fac_relance_stop'] == 2){
																	$style_tr="style='color:green;'";
																}elseif($f['fac_relance_stop'] == 3){
																	$style_tr="style='color:red;'";
																}

                                                            }elseif($facture_opca){
                                                                $style_tr="style='color:purple;'";
                                                            }
                                                            if($etape > 0 AND $etat_relance == $etape){
                                                                $class_tr="class='ligne_fac_select'";
                                                            }else{
                                                                $class_tr="class='ligne_fac'";
                                                            }
                                                        }

                                                        ?>
                                                        <tr  <?=$id_tr?> <?=$class_tr?> <?=$style_tr?> >
                                                            <td>
                                                            <?php if($f['fac_nature'] == 1 && !$f['fac_relance_stop']){ ?>
                                                                <?php if($etat_relance == $etape && $etape>0){ ?>

                                                                        <input type="checkbox" id="ligne_fac_<?=$nbFac?>" name="ligne_fac[<?=$nbFac?>]" value="<?=$nbFac?>" checked onClick="choixFac(<?=$nbFac?>);" >


                                                                <?php }else{ ?>
                                                                        <input type="checkbox" id="ligne_fac_<?=$nbFac?>" name="ligne_fac[<?=$nbFac?>]" value="<?=$nbFac?>" onClick="choixFac(<?=$nbFac?>);" >

                                                                <?php }?>
                                                                <input type="hidden" id="etat_relance_<?=$nbFac?>" value="<?=$etat_relance?>" />
                                                                <input type="hidden" id="fac_societe_<?=$nbFac?>" name="fac_societe_<?=$nbFac?>" value="<?=$d_societe['soc_id']?>" />
                                                                <input type="hidden" id="num_relance_<?=$nbFac?>" value="<?=$num_relance?>" />
                                                                <input type="hidden" id="min_relance_<?=$nbFac?>" value="<?=$min_relance?>" />
                                                                <input type="hidden" name="fac_id_<?=$nbFac?>" value="<?=$facture?>" />
                                                                <input type="hidden" id="fac_numero_<?=$nbFac?>" name="fac_numero_<?=$nbFac?>" value="<?=$f['fac_numero']?>" />
                                                                <input type="hidden" name="fac_etat_relance_<?=$nbFac?>" id="fac_etat_relance_<?=$nbFac?>" value="<?=$f['fac_etat_relance']?>" />
                                                            <?php }?>
                                                            </td>
                                                            <td>
                                                                <a href="facture_voir.php?facture=<?=$facture?>&origine=<?=$origine_fac?>&societ=<?= $d_societe['soc_id'] ?>" <?=$class_tr?> <?=$style_tr?> <?=$id_numero?> >
                                                                    <?=$f['fac_numero']?>
                                                                </a>
                                                            </td>

                                                            <?php if($d_client['cli_groupe'] == 1 && empty($d_client['cli_filiale_de'])){ ?>
                                                                <td><a href="facture_relance_voir.php?client=<?= $f['cli_id'] ?>" <?=$class_tr?> <?=$style_tr?> <?=$id_client?> >
                                                                    <?= $f['cli_code']?>
                                                                </a> </td>
                                                            <?php } ?>


                                                            <td><?=convert_date_txt($f['fac_date'])?></td>
                                                            <td>
                                                                <?php if($_SESSION['acces']['acc_profil']!=3){ ?>
                                                                    <a href="facture_relance_facture_mod.php?facture=<?=$facture?>&societe=<?= $d_societe['soc_id'] ?>" <?=$class_tr?> <?=$style_tr?> <?=$id_date?> >
                                                                        <?=$fac_date_reg_prev?>
                                                                    </a>
                                                                <?php }else{?>
                                                                    <?= $fac_date_reg_prev ?>
                                                                <?php }?>
                                                            </td>
                                                            <td><?=$fac_date_reg_rel?></td>
                                                            <td class="text-right"><?= $retard ?> jours</td>
                                                            <td class="text-right"><?= number_format($f['fac_total_ttc'], 2, ',', ' ') ?></td>
                                                            <td class="text-right">
                                                <?php			if($_SESSION["acces"]["acc_droits"][30]){ ?>
                                                                    <a href="reglement.php?facture=<?=$f['fac_id']?>&societ=<?= $d_societe['soc_id'] ?>" <?=$class_tr?> <?=$style_tr?> <?=$id_regle?>>
                                                                        <?=number_format($f['fac_regle'], 2, ',', ' ');?>
                                                                    </a>
                                                <?php			}else{ ?>
                                                                    <?= number_format($f['fac_regle'], 2, ',', ' '); ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td class="text-right">
                                                            <?php			if($_SESSION["acces"]["acc_droits"][30]){ ?>
                                                                <a href="reglement.php?facture=<?=$f['fac_id']?>&societ=<?= $d_societe['soc_id'] ?>" <?=$class_tr?> <?=$style_tr?> <?=$id_du?>>
                                                                    <?= number_format($reste_du, 2, ',', ' ');?>
                                                                </a>
                                            <?php			}else{ ?>
                                                                <?= number_format($reste_du, 2, ',', ' '); ?>
                                                           <?php } ?>
                                                            </td>

                                                            <td class="text-center" >
                                                                <?php if(!empty($f['fac_etat_relance'])){ ?>
                                                                    <a href="facture_relance.php?facture=<?= $f['fac_id'] ?>&societe=<?= $d_societe['soc_id'] ?>&client=<?= $client ?>&origine=<?= $origine ?>" <?=$style_a?> <?=$class_tr?> <?=$style_tr?> <?=$id_etat?> >
                                                                        <?= $code_relance ?>
                                                                    </a>
                                                                <?php }else{?>
                                                                <a href="#" <?=$id_etat?>>Pas d'état de relance</a>
                                                                <?php }?>
                                                            </td>
                                                            <td><?= convert_date_txt($f['fac_date_relance']) ?></td>
                                                            <td>
                                                            <?php if($facture_opca){  ?>
                                                                <a href="facture_relance_voir.php?client=<?=$fac_opca?>&etape=<?=$etape?>&opca" <?=$class_tr?> <?=$style_tr?> <?=$id_opca?> >
                                                                    <?= $d_opca['cli_nom']?>
                                                                </a>
                                                            <?php } ?>
                                                            </td>
                                                        </tr>
                            <?php					}
                ?>
                                                </tbody>
                                                <tfoot>

                                                    <tr>
                                                    <?php if($d_client['cli_groupe'] == 1 && empty($d_client['cli_filiale_de'])){ ?>
                                                            <th colspan="7" class="text-right" >Total :</th>
                                                    <?php }else{ ?>
                                                            <th colspan="6" class="text-right" >Total :</th>
                                                    <?php }?>
                                                        <td class="text-right" ><?=number_format($total_ttc, 2, ',', ' ');?></td>
                                                        <td class="text-right" ><?=number_format($total_regle, 2, ',', ' ');?></td>
                                                        <td class="text-right" ><?=number_format($total_reste_du, 2, ',', ' ');?></td>
                                                        <td colspan="3" class="text-center" >&nbsp;</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                        <?php			}
                                    }
                                }?>
                                <input type="hidden" value="<?=$nbFac?>" name="nbFac" id="nbFac" />
                                <div class="table-responsive mt15">
                                    <table class="table table-striped table-hover" >
                                        <thead>
                                            <tr class="dark2" >
                                                <th>Utilisateur</th>
                                                <th>Contact</th>
                                                <th>Commentaires</th>
                                                <th>Rappeler</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12"><?= $_SESSION['acces']['acc_prenom'] ?> <?= $_SESSION['acces']['acc_nom'] ?></div>
                                                        <div class="col-md-12"><input type="text" name="rel_date" id="rel_date" class="form-control datepicker date" placeholder="Date" value="<?= date("d/m/Y") ?>"></div>
                                                    </div>
                                                </td>
                                                <td>
                                                <div class="cont-contact">
                                                        <div class="row cont-contact-liste" >
                                                            <div class="col-md-12" >
                                                                <div class="section" >
                                                                    <select name="rel_contact_lib" id="contact" class="select2 contact" >
                                                                        <option value="0" >Selectionnez un contact client</option>
                                                            <?php		if(!empty($contacts)){
                                                                            foreach($contacts as $c){
                                                                                echo("<option value='" . $c["con_id"] . "' >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
                                                                            }
                                                                        }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" value="0" name="rel_contact" id="rel_contact" />
                                                        </div>
                                                        <div class="row mt5">
                                                            <div class="col-md-6" >
                                                                <input type="text" name="rel_con_nom" id="con_nom" class="form-control nom champ-contact" placeholder="Nom" />
                                                            </div>
                                                            <div class="col-md-6" >
                                                                <input type="text" name="rel_con_prenom" id="con_prenom" class="form-control prenom champ-contact" placeholder="Prénom"  />
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6 mt5" >
                                                                <input type="text" name="rel_con_tel" id="con_tel" class="form-control telephone champ-contact" placeholder="Téléphone"  />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="row">
                                                        <?php if($_SESSION['acces']['acc_profil'] != 3){

                                                            ?>
                                                            <div class="col-md-12">
                                                                <select class="select2" name="rel_etat_relance" id="rel_etat_relance" onChange="choixEtape();">
                                                                    <option value="0"></option>
                                                                    <?php foreach($relances as $re){
                                                                        if(!empty($re['ret_id']) && $re['ret_id'] != 0){
                                                                            if($etape_ref==$re['ret_id']){
                                                                        ?>
                                                                            <option value="<?= $re['ret_id'] ?>" selected><?= $re['ret_libelle'] ?></option>
                                                                        <?php }else{ ?>
                                                                            <option value="<?= $re['ret_id'] ?>"><?= $re['ret_libelle'] ?></option>
                                                                        <?php }?>

                                                                    <?php }
                                                                } ?>
                                                                </select>
                                                                <a href="javascript:void(0)" onClick="actuFacture();" class="mt5">
                                                                    Sélectionner les factures correspondantes
                                                                </a>
                                                            </div>
                                                        <?php }else{ ?>
                                                            <input type="hidden" name="rel_etat_relance" id="rel_etat_relance" value="0" />
                                                        <?php }?>
                                                        <?php if(!empty($d_client['cli_categorie']) AND !empty($d_client['cli_sous_categorie']) AND $d_client['cli_categorie'] == 4 AND $d_client['cli_sous_categorie'] ==5){ ?>
                                                        <input type="hidden" name="opca" id="opca" value="<?= $client ?>" />
                                                        <?php }?>
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control form-control-sm" name="rel_titre" id="rel_titre" size="40" maxlength="50" value="">
                                                        </div>
                                                        <div class="col-md-12 mt5">
                                                            <textarea class="form-control" name="rel_comment" cols="60" rows="4" ></textarea>
                                                        </div>
                                                    </div>
                                                    <td>
                                                        <input id="rel_date_rappel" type="text" name="rel_date_rappel" class="form-control datetimepicker" placeholder="Date" value="">
                                                    </td>
                                                </td>
                                        </tbody>

                                    </table>
                                </div>

                                    <h4 class="text-primary text-left mt15">Historiques</h4>
                                    <div class="table-responsive mt15">

                                        <?php
                                            // HISTORIQUES
                                            $sql="SELECT DISTINCT rel_id,rel_comment,rel_date,rel_utilisateur,rel_con_nom,rel_con_prenom,rel_con_tel,rel_etat_relance,rel_date_rappel,rel_opca
                                            ,rel_envoie,rel_envoie_date,rel_envoie_uti,rel_courrier,rel_courrier_date,rel_courrier_uti
                                            FROM Relances LEFT OUTER JOIN Relances_Factures ON (Relances.rel_id=Relances_Factures.rfa_relance)";
                                            // if($d_client['cli_categorie'] == 4 AND $d_client['cli_sous_categorie'] ==5){
                                            //     $sql = $sql . " WHERE  (rel_opca = " . $client;
                                            // }else{
                                            //     $sql = $sql . " WHERE  rel_client = " . $client;
                                            // }
											$sql = $sql . " WHERE rel_date  > '2018-01-01'";
											if(!empty($d_client['cli_categorie'])  AND !empty($d_client['cli_sous_categorie']) AND $d_client['cli_categorie'] == 4 AND $d_client['cli_sous_categorie'] ==5){
                                                $sql = $sql . " AND rel_opca = " . $client;
                                            }else{
                                                $sql = $sql . " AND  rel_client = " . $client;
                                            }
                                            if(!empty($listeFac)){
												$sql = $sql . " AND  (";
													$it = 0;
	                                                foreach($listeFac as $soc=>$l){
														if($it ==0){
															$sql = $sql . "  (rfa_facture_soc = " . $soc . " AND rfa_facture IN(" . $l . "))";
														}else{
															$sql = $sql . "  OR (rfa_facture_soc = " . $soc . " AND rfa_facture IN(" . $l . "))";
														}

														$it = $it++;
	                                                }
												$sql = $sql . " )";
                                            }
                                             $sql = $sql . " ORDER BY rel_date DESC,rel_id DESC LIMIT 100;";
                                            $req = $Conn->query($sql);

                                            $historiques=$req->fetchAll();

                                            if(!empty($historiques)){
                                        ?>
                                        <table class="table table-striped table-hover" >
                                            <thead>
                                                <tr class="dark2" >
                                                    <th class="no-sort">&nbsp;</th>
                                                    <th>Utilisateur</th>
                                                    <th>Contact</th>
                                                    <th   style="width:20%;">Commentaire</th>
                                                    <th>Rappel</th>
                                                    <th>Factures</th>
                                                    <th colspan="2" >Mail</th>
                                                    <th colspan="2" >LR/AR</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                   <?php
                                                   foreach($historiques as $h){

                                                       $sql="SELECT uti_nom,uti_prenom FROM utilisateurs WHERE uti_id=" . $h['rel_utilisateur'];
                                                       $req=$Conn->query($sql);
                                                       $utilisateur=$req->fetch();
                                                       $sql="SELECT uti_nom,uti_prenom FROM utilisateurs WHERE uti_id=" . $h['rel_envoie_uti'];
                                                       $req=$Conn->query($sql);
                                                       $envoi_utilisateur=$req->fetch();
                                                       $sql="SELECT uti_nom,uti_prenom FROM utilisateurs WHERE uti_id=" . $h['rel_courrier_uti'];
                                                       $req=$Conn->query($sql);
                                                       $courrier_utilisateur=$req->fetch();
                                                       $sql="SELECT cli_nom FROM clients WHERE cli_id=" . $h['rel_opca'];
                                                       $req=$Conn->query($sql);
                                                       $cli_opca=$req->fetch();
                                                       $sql="SELECT rfa_facture,rfa_facture_num FROM Relances_Factures WHERE rfa_relance=" . $h['rel_id'] . "  ORDER BY rfa_facture_num;";
                                                       $req=$Conn->query($sql);
                                                       $rfa=$req->fetchAll();
                                                       ?>
                                                        <tr>
                                                            <td>
                                                                <?php if($h['rel_utilisateur'] == $acc_utilisateur){ ?>
                                                                    <a href="facture_relance_mod.php?relance=<?=$h['rel_id']?>" class="btn btn-sm btn-warning">
                                                                        <i class="fa fa-pencil"></i>
                                                                    </a>
                                                                <?php }?>
                                                            </td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <?= convert_date_txt($h['rel_date']) ?>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <?= $utilisateur['uti_prenom'] ?> <?= $utilisateur['uti_nom'] ?>
                                                                    </div>
                                                                </div>

                                                            </td>
                                                            <td>
                                                                <?= $h['rel_con_prenom'] ?> <?= $h['rel_con_nom'] ?><br>
                                                                Tel : <?= $h['rel_con_tel'] ?>
                                                            </td>
                                                            <td>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                <?= $cli_opca['cli_nom'] ?>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <?= $h['rel_comment'] ?>
                                                                </div>
                                                            </div>

                                                            </td>
                                                            <td>
                                                                <?php if(!empty($h['rel_date_rappel'])){
                                                                    $date = new DateTime($h['rel_date_rappel']);
                                                                    echo $date->format('d/m/Y H:i');
                                                                }?>
                                                            </td>
                                                            <td>
                                                                <?php if(!empty($rfa)){ ?>
                                                                <table style="border:none;">
                                                                    <?php foreach($rfa as $r){ ?>

                                                                        <tr>
                                                                            <td style="border:none;">
                                                                                <?= $r['rfa_facture_num'] ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php }?>
                                                                </table>
                                                                <?php }?>
                                                            </td>

                                                           <?php

                                                                    if($h['rel_etat_relance'] == 1){
                                                                    ?>
                                                                    <td class="text-center">
                                                                        <a href="mail_prep.php?facture_relance=<?=$h['rel_id']?>">
                                                                            Mail Client
                                                                        </a>
                                                                    </td>
                                                                    <?php }else{ ?>
                                                                    <td class="text-center">
                                                                        <a href="mail_prep.php?facture_relance=<?=$h['rel_id']?>">
                                                                            Mail Client
                                                                        </a>
                                                                    </td>
                                                                <?php } ?>


                                                            <?php
                                                             if(empty($relance_nom[$h['rel_etat_relance']]['ret_traitement'])){
                                                            if($h['rel_envoie'] == 1){


                                                                ?>
                                                                <td class="success">
                                                                    Mail envoyé le <?= convert_date_txt($h['rel_envoie_date']) ?><br>
                                                                    par <?= $envoi_utilisateur['uti_prenom'] ?> <?= $envoi_utilisateur['uti_nom'] ?>
                                                                </td>
                                                            <?php }else{ ?>
                                                                <td class="danger">
                                                                    Mail non envoyé
                                                                </td>
                                                            <?php } ?>
                                                        <?php }else{ ?>
                                                            <td colspan="2" >&nbsp;</td>
                                                        <?php } ?>
                                                        <?php if($h['rel_etat_relance'] == 5){ ?>
                                                            <td class="text-center" >
                                                                <a href="facture_relance_lr_voir.php?relance=<?=$h['rel_id']?>" >
                                                                   LR/AR

                                                                </a>
                                                            </td>
                                                            <?php if($h['rel_courrier']){ ?>
                                                                <td class="success" >
                                                                    Courrier imprimé le <?=convert_date_txt($h['rel_courrier_date'])?><br/>
                                                                    par <?= $courrier_utilisateur['uti_prenom'] ?> <?= $courrier_utilisateur['uti_nom'] ?>
                                                                </td>
                                                            <?php }else{ ?>
                                                                <td class="danger">
                                                                    Courrier non envoyé
                                                                </td>
                                                            <?php }?>
                                                        <?php }else{?>
                                                            <td colspan="2" >&nbsp;</td>
                                                        <?php }?>

                                                   <?php }?>
                                            </tbody>

                                        </table>
                                <?php 			}else{ ?>
                                        <div class="col-md-12 text-center mt15" style="padding:0;" >
                                            <div class="alert alert-warning" style="border-radius:0px;">
                                                Aucun historique.
                                            </div>
                                        </div>
                    <?php			} ?>
                                    </div>
                                    <h4 class="text-primary text-left mt15">Infos complémentaires</h4>
                                    <div class="row mt15">
                                        <div class="col-md-4">
                                            <div class="panel panel-info">
                                                <div class="panel-heading panel-head-sm">
                                                    <span class="panel-icon">
                                                        <i class="fa fa-file"></i>
                                                    </span>
                                                    <span class="panel-title">Compta</span>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="col-md-12" style="margin-bottom:15px;">
                                                        <strong>Facturer à l'OPCA:</strong>
                                                <?php 	if($d_client['cli_facture_opca'] == 1){
                                                            echo("Oui");
                                                        }else{
                                                            echo("Non");
                                                        }?>
                                                    </div>
                                                    <div class="col-md-12" style="margin-bottom:15px;">
                                                        <strong>OPCA:</strong>
                                                <?php 	if(!empty($opca)){
                                                            echo($opca['cli_nom']);
                                                        }else{
                                                            echo("<i>Pas d'OPCA</i>");
                                                        } ?>
                                                    </div>
                                                    <div class="col-md-12" style="margin-bottom:15px;">
                                                        <strong>Relevés de factures :</strong>
                                                <?php 	if(empty($d_client['cli_releve'])){
                                                            echo("Non");
                                                        }else{
                                                            echo("Oui");
                                                        } ?>
                                                    </div>
                                                    <div class="col-md-12" style="margin-bottom:15px;">
                                                        <strong>Identification TVA:</strong>
                                                    <?php if(empty($d_client['cli_ident_tva'])){
                                                            echo("<i>Pas d'identification TVA</i>");
                                                        }else{
                                                            echo($d_client['cli_ident_tva']);
                                                        } ?>
                                                    </div>
                                            <?php 	if(!empty($d_client['cli_affacturage'])){ ?>
                                                        <div class="col-md-12 mb15" ><strong>Client en affacturage</strong></div>
                                                        <div class="col-md-12 mb15" >
                                                            <strong>IBAN :</strong>
                                            <?php			if(!empty($d_client['cli_affacturage_iban'])){
                                                                echo($d_client['cli_affacturage_iban']);
                                                            }else{
                                                                echo("non renseigné");
                                                            }	?>
                                                        </div>
                                            <?php	} ?>


                                                    <div class="col-md-12" style="margin-bottom:15px;"><strong>Mode de règlement:</strong>
                                                            <?php if(empty($d_client['cli_reg_type'])): ?>
                                                            <i>Pas de mode de règlement</i>
                                                            <?php else: ?>
                                                            <?php foreach($base_reglement as $k => $n): ?>
                                                            <?php if($k > 0){ ?>
                                                                <?php if($d_client['cli_reg_type'] == $k): ?><?= $n ?><?php endif; ?>
                                                                <?php } ?>
                                                            <?php endforeach; ?>
                                                            <?php endif; ?>
                                                    </div>
                                                    <div class="col-md-12" style="margin-bottom:15px;">
                                                        <strong>Type de règlement:</strong>

                                                            <?php if($d_client['cli_reg_formule'] == 1): ?>

                                                            Date + <?php if(!empty($d_client['cli_reg_nb_jour'])): ?><?= $d_client['cli_reg_nb_jour'] ?><?php else: ?>0<?php endif; ?> jours

                                                            <?php endif; ?>
                                                            <?php if($d_client['cli_reg_formule'] == 2): ?>
                                                            Date + 45j + fin de mois
                                                            <?php endif; ?>
                                                            <?php if($d_client['cli_reg_formule'] == 3): ?>
                                                            Date + fin de mois + 45j
                                                            <?php endif; ?>
                                                            <?php if($d_client['cli_reg_formule'] == 4): ?>
                                                            Date + <?php if(!empty($d_client['cli_reg_nb_jour'])): ?><?= $d_client['cli_reg_nb_jour'] ?><?php else: ?>0<?php endif; ?> jours + fin de mois + <?php if(!empty($d_client['cli_reg_fdm'])): ?><?= $d_client['cli_reg_fdm'] ?><?php else: ?>0<?php endif; ?> jours
                                                            <?php endif; ?>
                                                    </div>
                                                    <div class="col-md-12" style="margin-bottom:15px;">
                                                        <strong>RIB :</strong>
                                                <?php	if(isset($d_rib)){
                                                            echo($d_rib["rib_nom"]);
                                                        }else{
                                                            echo("Pas de RIB");
                                                        } ?>
                                                    </div>
                                                    <div class="col-md-12" style="margin-bottom:15px;">
                                                        <strong>Blacklisté :</strong>
                                                <?php 	if(empty($d_client["cli_blackliste"])){
                                                            echo("Non");
                                                        }else{
                                                            echo("Oui");
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                        <blockquote class="blockquote-primary">
                                            <p>Adresses de facturation.</p>
                                        </blockquote>
                                <?php		if(!empty($adresses_fac)){
                                            foreach($adresses_fac as $a){
                                                $text_adresse="";
                                                if(!empty($a['adr_nom'])){
                                                    $text_adresse=$a['adr_nom'];
                                                }
                                                if(!empty($a['adr_service'])){
                                                    if(!empty($text_adresse)){
                                                        $text_adresse.="<br/>";
                                                    }
                                                    $text_adresse.=$a['adr_service'];
                                                }
                                                if(!empty($a['adr_ad1'])){
                                                    if(!empty($text_adresse)){
                                                        $text_adresse.="<br/>";
                                                    }
                                                    $text_adresse.=$a['adr_ad1'];
                                                }
                                                if(!empty($a['adr_ad2'])){
                                                    if(!empty($text_adresse)){
                                                        $text_adresse.="<br/>";
                                                    }
                                                    $text_adresse.=$a['adr_ad2'];
                                                }
                                                if(!empty($a['adr_ad3'])){
                                                    if(!empty($text_adresse)){
                                                        $text_adresse.="<br/>";
                                                    }
                                                    $text_adresse.=$a['adr_ad3'];
                                                }
                                                $text_adresse.="<br/>" . $a['adr_cp'] . " " . $a['adr_ville'];

                                                if(!empty($a['adr_siret']) AND !empty($d_client["cli_siren"])){
                                                    $text_adresse.="<br/>" . $d_client["cli_siren"] . " " . $a['adr_siret'];
                                                } ?>
                                                <div class="panel adresse-2" id="adresse_<?=$a['adr_id']?>" >
                                                    <div class="panel-heading panel-head-sm">
                                                        <span class="panel-title"><?=$a['adr_libelle']?></span>
                                                    </div>
                                                    <div class="panel-body">
                                                        <?=$text_adresse?>
                                                    </div>
                                            <?php	if($drt_edit_adr){ ?>
                                                        <div class="panel-footer" >
                                                            <a href="client_adresse.php?client=<?=$client?>&adresse=<?=$a['adr_id']?>&onglet=11" class="btn btn-warning btn-sm" >
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                <?php 		if($a['adr_defaut'] != 1){ ?>
                                                                <div class="pull-right" >
                                                                    <button type="button" data-adresse="<?= $a['adr_id']?>" class="btn btn-danger btn-sm deladresse">
                                                                        <i class="fa fa-times"></i>
                                                                    </button>
                                                                </div>
                                                <?php 		} ?>
                                                        </div>
                                            <?php	} ?>
                                                </div>
                                    <?php	}
                                        } ?>
                                        <div class="alert alert-warning" id="no_adresse_2" <?php if(!empty($adresses_fac)) echo("style='display:none;'"); ?> >
                                            Pas d'adresse de facturation pour ce client.
                                        </div>

                                        </div>
                                        <div class="col-md-4">
                                        <blockquote class="blockquote-primary">
                                            <p>Infos comptabilité.</p>
                                        </blockquote>
                                        <table class="table table-striped table-hover" >
													<thead>
														<tr class="dark">
															<th>Description</th>
														</tr>
													</thead>
									<?php 			if(!empty($infos)){ ?>
														<tbody>
									<?php					foreach($infos as $in){
																if($in['inf_type'] == 4){
																$type_info="";
																if(!empty($in['inf_type'])){
																	$type_info=$d_info_type[$in['inf_type']];
																}
																$info_famille="";
																if(!empty($famille_produit[$in['inf_famille']])){
																	$info_famille=$famille_produit[$in['inf_famille']]["pfa_libelle"];
																}
																$info_sous_famille="";
																if(!empty($sous_famille_produit[$in['inf_sous_famille']])){
																	$info_sous_famille=$sous_famille_produit[$in['inf_sous_famille']]["psf_libelle"];
																}
																$info_sous_sous_famille="";
																if(!empty($sous_sous_famille_produit[$in['inf_sous_sous_famille']])){
																	$info_sous_sous_famille=$sous_sous_famille_produit[$in['inf_sous_sous_famille']]["pss_libelle"];
																}
																$info_auteur="";
																if(!empty($utilisateurs[$in['inf_auteur']])){
																	$info_auteur=$utilisateurs[$in['inf_auteur']]["identite"];
																}

																$classe="info-client info-cli-type-" . $in['inf_type'] . " info-cli-fam-" . $in['inf_famille'] . " info-cli-sous-fam-" . $in['inf_sous_famille'] . "  info-cli-sous-sous-fam-" . $in['inf_sous_famille']; ?>
																<tr id="ligne_info_<?=$in['inf_id']?>" class="<?=$classe?>" >

																	<td><?= $in['inf_description']; ?></td>

																</tr>
                                    <?php
                                                        } ?>

														</tbody>
                                    <?php			}
                                    }?>
												</table>
                                        </div>
                                    </div>

                            </div>

                        </div>

					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a
                        <?php if(!empty($_SESSION['retourFactureRelance'])){ ?>
                        href="<?= $_SESSION['retourFactureRelance'] ?>"
                        <?php }else{ ?>
                            href="facture_relance_liste.php"
                        <?php }?>
                        class="btn btn-default btn-sm" role="button">
							<span class="fa fa-long-arrow-left"></span>
							<span class="hidden-xs">Retour</span>
						</a>
					</div>
					<!-- <div class="col-xs-6 footer-middle">&nbsp;</div> -->
					<div class="col-xs-9 footer-right">
                        <button type="submit" class="btn btn-success btn-sm" >
                                <i class='fa fa-floppy-o'></i> Enregistrer
                        </button>
                        <a href="client_contact.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-success btn-sm" >
                            <i class='fa fa-plus'></i> Ajouter un contact
                        </a>
                        <a href="client_adresse.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-success btn-sm" >
                            <i class='fa fa-plus'></i> Ajouter une adresse
                        </a>
                        <a href="client_voir.php?client=<?= $d_client['cli_id'] ?>" class="btn btn-info btn-sm">
                            <i class='fa fa-eye'></i> Voir le client
                        </a>

                        <?php if($d_client['cli_groupe'] == 1 && !empty($d_client['cli_filiale_de'])){ ?>
                        <a href="facture_relance_voir.php?client=<?= $d_client['cli_filiale_de'] ?>" class="btn btn-info btn-sm" >
                            <i class='fa fa-eye'></i> Voir la maison mère
                        </a>
                        <?php } ?>

                    </div>
				</div>
			</footer>
		</form>
        <!-- modal de confirmation -->
			<div id="modal_confirmation_user" class="modal fade" role="dialog" >
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title">Confirmer ?</h4>
						</div>
						<div class="modal-body">
							<p>Message</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								Annuler
							</button>
							<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
								Confirmer
							</button>
						</div>
					</div>
				</div>
			</div>

			<!-- FIN MODAL -->
        <style>
            .note-editable{
                background-color: white !important;
            }
        </style>
<?php	include "includes/footer_script.inc.php"; ?>
        <script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
        <script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
        <script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>
        <script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="vendor/plugins/moment/moment.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/fr.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript">
            var client="<?=$d_client["cli_id"]?>";
            var tab_contact=new Array(3);
            tab_contact[0]=new Array(3);
			<?php	if(!empty($contacts)){
						foreach($contacts as $con){ ?>
							tab_contact[<?=$con["con_id"]?>]=new Array(3);
							tab_contact[<?=$con["con_id"]?>]["nom"]="<?=$con["con_nom"]?>";
							tab_contact[<?=$con["con_id"]?>]["prenom"]="<?=$con["con_prenom"]?>";
							tab_contact[<?=$con["con_id"]?>]["tel"]="<?=$con["con_tel"]?>";
							tab_contact[<?=$con["con_id"]?>]["portable"]="<?=$con["con_portable"]?>";
			<?php		}
					} ?>
            $(document).ready(function () {
                $('.dataTable').DataTable({
                    "language": {
                    "url": "vendor/plugins/DataTables/media/js/French.json"
                    },
                    "paging": false,
                    "searching": false,
                    "info": false,
                    "columnDefs": [ {
                        "targets": 'no-sort',
                        "orderable": false,
                    }]
                });
                // INTERACTION CONTACT
				$(".datetimepicker").datetimepicker({
                    locale: 'fr'
                });
				$(".delcontact").click(function(){
					contact=$(this).data("contact");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer ce contact?",confirmer_contact_supp,contact);
                });
                $("#contact").change(function(){
                    afficher_contact($(this).val());
                });
                $(".deladresse").click(function(){
					adresse=$(this).data("adresse");
					modal_confirmation_user("Êtes-vous sûr de vouloir supprimer cette adresse?",confirmer_adresse_supp,adresse);
				});
            });
            function afficher_contact(contact){
                if(contact>0){
                    $("#con_nom").val(tab_contact[contact]["nom"]);
                    $("#con_prenom").val(tab_contact[contact]["prenom"]);
                    $("#con_tel").val(tab_contact[contact]["tel"]);
                    $("#con_portable").val(tab_contact[contact]["portable"]);
                }else{
                    $(".champ-contact").val("");
                }
            }
            function confirmer_adresse_supp(adresse){
				del_client_adresse(adresse,client,supprimer_adresse_client,adresse);
			}
            function confirmer_contact_supp(contact){
				del_client_contact(contact,client,supprimer_contact_client,contact);
            }
            // modif de l'affichage suite à supp
			function supprimer_contact_client(json){
				if(json.contact){
					$("#contact_" + json.contact).remove();
				}
				if($(".contact").length==0){
					$("#tab_contact").remove();
					$("#no_contact").show();
				}
            }
            var nbFac=0;
			var tab_lien=new Array();
			var rel_etat_relance=0;
			var min_relance=0;
			var etat_relance=0;
			var num_relance=0;
			var valide_select=false;
			var fac_numero="";

			var tab_relance=new Array();

			var tab_relance_nom=new Array();
		<?php for ($i=0; $i < $nbEtat; $i++) { ?>

				tab_relance[<?=$relance_nom[$i]['ret_id']?>]=new Array(2);
				tab_relance[<?=$relance_nom[$i]['ret_id']?>][0]=parseInt("<?=$i?>");
				tab_relance[<?=$relance_nom[$i]['ret_id']?>][1]="<?=$relance_nom[$i]['ret_libelle']?>";
				tab_relance[<?=$relance_nom[$i]['ret_id']?>][2]=parseInt("<?=$relance_nom[$i]['ret_traitement']?>");

				tab_relance_nom[<?=$i?>]="<?=$relance_nom[$i]['ret_libelle']?>";
        <?php }?>


			function controleFac(num_fac){
				valide_select=true;
				rel_etat_relance=document.getElementById("rel_etat_relance").value;
				if(rel_etat_relance>0){
					num_relance=tab_relance[rel_etat_relance][0];
					min_relance=document.getElementById("min_relance_" + num_fac).value;
					etat_relance=document.getElementById("etat_relance_" + num_fac).value;
                    fac_numero=document.getElementById("fac_numero_" + num_fac).value;

					/* if(num_relance<min_relance){
						alert("L'opération '" + tab_relance[rel_etat_relance][1] + "' n'est plus disponible pour la facture " + fac_numero);
						valide_select=false;
					} *//* else if(num_relance>etat_relance){
						alert("La facture " + fac_numero + " n'en n'est pas encore au stade '" + tab_relance[rel_etat_relance][1] + "'");
						valide_select=false;
					}; */
				}
				if(valide_select){
					selectFac(num_fac);
				}else{
					deselectFac(num_fac);
				};

			}

			function selectFac(num_fac){
				document.getElementById("ligne_fac_" + num_fac).checked=true;
				document.getElementById("tab_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				document.getElementById("numero_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				if(document.getElementById("date_ligne_" + num_fac)){
					document.getElementById("date_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
				if(document.getElementById("regle_ligne_" + num_fac)){
					document.getElementById("regle_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
				if(document.getElementById("du_ligne_" + num_fac)){
					document.getElementById("du_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
				document.getElementById("etat_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				if(document.getElementById("opca_ligne_" + num_fac)){
					document.getElementById("opca_ligne_" + num_fac).setAttribute("class","ligne_fac_select");
				};
			}

			function deselectFac(num_fac){
				document.getElementById("ligne_fac_" + num_fac).checked=false;
				document.getElementById("tab_ligne_" + num_fac).removeAttribute("class");
				document.getElementById("numero_ligne_" + num_fac).setAttribute("class","ligne_fac");
				if(document.getElementById("date_ligne_" + num_fac)){
					document.getElementById("date_ligne_" + num_fac).setAttribute("class","ligne_fac");
				};
				if(document.getElementById("regle_ligne_" + num_fac)){
					document.getElementById("regle_ligne_" + num_fac).setAttribute("class","ligne_fac");
				};
				if(document.getElementById("du_ligne_" + num_fac)){
					document.getElementById("du_ligne_" + num_fac).setAttribute("class","ligne_fac");
				}
				document.getElementById("etat_ligne_" + num_fac).setAttribute("class","ligne_fac");
				if(document.getElementById("opca_ligne_" + num_fac)){
					document.getElementById("opca_ligne_" + num_fac).setAttribute("class","ligne_fac");
				}
			}


			function selectMultiFac(elt,soc){
				nbFac=document.getElementById("nbFac").value;
				for(i=1;i<=nbFac;i++){
					if(document.getElementById("ligne_fac_" + i)){
						if(elt.checked){
							if(document.getElementById("fac_societe_" + i).value==soc){
								controleFac(i);
							};
						}else{
							if(document.getElementById("fac_societe_" + i).value==soc){
								deselectFac(i);
							}
						};
					};
				};
			}
			function choixFac(num_fac){
				if(document.getElementById("ligne_fac_" + num_fac).checked){
					controleFac(num_fac);
				}else{
					deselectFac(num_fac);
				};
			}
			function actuFacture(){
				rel_etat_relance=document.getElementById("rel_etat_relance").value;
				num_relance=tab_relance[rel_etat_relance][0];
                nbFac=document.getElementById("nbFac").value;
				for(i=1;i<=nbFac;i++){

					if((document.getElementById("etat_relance_" + i).value==num_relance)&&(num_relance>0)){
                        selectFac(i);

					}else{
						deselectFac(i);
					};
				};
			}
            var date_jour="<?=date("d/m/Y")?>"
            <?php
                $demain = new DateTime();
                $demain->modify('+1 day');
            ?>
			var date_demain="<?= $demain->format('Y-m-d H:i') ?>"
			function choixEtape(){
				rel_etat_relance=document.getElementById("rel_etat_relance").value;
				if(tab_relance[rel_etat_relance][2]==1){
					document.getElementById("rel_date").value=date_jour;
					document.getElementById("rel_date_rappel").value=date_demain;
					document.getElementById("rel_date").setAttribute("readonly","readonly");
					document.getElementById("rel_date_rappel").setAttribute("readonly","readonly");
				}else{
					document.getElementById("rel_date_rappel").value="";
					document.getElementById("rel_date").removeAttribute("readonly");
					document.getElementById("rel_date_rappel").removeAttribute("readonly");
				};

				nbFac=document.getElementById("nbFac").value;
				if(rel_etat_relance>0){
					document.getElementById("rel_titre").value=tab_relance[rel_etat_relance][1];
					for(i=1;i<=nbFac;i++){
						if(document.getElementById("ligne_fac_" + i).checked){
							controleFac(i);
						};
					};
				}else{
					num_relance=0;
					for(i=1;i<=nbFac;i++){
						if((document.getElementById("num_relance_" + i).value<num_relance)||(num_relance==0)){
							num_relance=document.getElementById("num_relance_" + i).value;
						};
					};
					document.getElementById("rel_titre").value=tab_relance_nom[num_relance];
				}


			}
		</script>
	</body>
</html>
