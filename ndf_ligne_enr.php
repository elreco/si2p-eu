<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');

// si la note de frais est déjà créée
$nli_date = $_POST['ndf_date_annee'] . "-" . $_POST['ndf_date_mois'] . "-" . $_POST['nli_date'];

if (!empty($_POST['id'])) {
    // INSERER LA NDF

    // aller chercher l'utilisateur concerné par la ndf
    $req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id = :uti_id");
    $req->bindValue(':uti_id', $_POST['utilisateur']);
    $req->execute();
    $utilisateur = $req->fetch();
    // fin aller chercher l'utilisateur concerné par la ndf

    // LE CA
    $sql = "SELECT * FROM utilisateurs
    LEFT JOIN utilisateurs_societes ON (utilisateurs_societes.uso_utilisateur = utilisateurs.uti_id)
     WHERE uso_agence = " . $utilisateur['uti_agence'] . " AND uso_societe = " . $utilisateur['uti_societe'] . " AND uti_profil = 15 AND uti_archive = 0";
    $req = $Conn->query($sql);
    $chef_agence = $req->fetch();

    $ra = 0;
    if (!empty($chef_agence)) {
        $ra = $chef_agence['uti_id'];
    }

    // INSERER LA LIGNE

    // avoir le numéro de la ndf
    $req = $Conn->prepare("SELECT MAX(nli_numero) FROM ndf_lignes WHERE nli_ndf = :nli_ndf");
    $req->bindValue(':nli_ndf', $_POST['id']);
    $req->execute();
    $derniere_ligne = $req->fetch();
    // fin avoir le numéro de la ndf

    // format ttc
    $_POST['nli_ttc'] = number_format($_POST['nli_ttc'], 2, '.', '');
    $_POST['nli_ttc'] = floatval($_POST['nli_ttc']);
    if (!empty($_POST['nli_montant_tva'])) {
        $_POST['nli_montant_tva'] = number_format($_POST['nli_montant_tva'], 2, '.', '');
        $_POST['nli_montant_tva'] = floatval($_POST['nli_montant_tva']);
    }
    if (!empty($_POST['nli_depassement_accorde'])) {
        $_POST['nli_depassement_accorde'] = number_format($_POST['nli_depassement_accorde'], 2, '.', '');
        $_POST['nli_depassement_accorde'] = floatval($_POST['nli_depassement_accorde']);
    }


    //fin format ttc

    // numéro
    $numero = $derniere_ligne["MAX(nli_numero)"] + 1;
    // fin numéro

    if (!empty($_POST['ligne'])) {
        ///////////////// EDITION ///////////////////
        /////////////// maj ttc ndf


        // select ttc de la ndf
        $req = $Conn->prepare("SELECT nli_ttc, nli_qte, nli_compte FROM ndf_lignes WHERE nli_id = :nli_id");
        $req->bindValue(':nli_id', $_POST['ligne']);
        $req->execute();
        $nli = $req->fetch();

        ////////////// fin maj ttc ndf
        // calculer les dépassements
        // récupérer les categories de profils
        $req = $Conn->prepare("SELECT * FROM ndf_categories_profils WHERE ncp_profil = :ncp_profil AND ncp_categorie = :ncp_categorie AND ncp_type = :ncp_type");
        $req->bindValue("ncp_categorie", $_POST['nli_categorie']);
        $req->bindValue("ncp_profil", $utilisateur['uti_profil']);
        $req->bindValue("ncp_type", $_POST['nli_type']);
        $req->execute();
        $cp = $req->fetch();

        newInvitations($_POST, $_POST['ligne'], $_POST['id'], true);

        // calculer pris en charge
        $nli_pris_en_charge = $_POST['nli_ttc'];
        $nli_depassement_valide = 0;
        if(!empty($_POST['nli_depassement_accorde'])) {
            $nli_depassement_accorde =$_POST['nli_depassement_accorde'];
        } else {
            $nli_depassement_accorde = 0;
        }

        if ($_POST['nli_region'] == 1 && !empty($cp['ncp_plafond_1']) && $_POST['nli_ttc'] > ($cp['ncp_plafond_1'] * $_POST['nli_qte'])) {
            $nli_pris_en_charge = $cp['ncp_plafond_1'] * $_POST['nli_qte'];
            $nli_pris_en_charge = getMultiplier($_POST) * $nli_pris_en_charge;
            $nli_depassement_valide = $_POST['nli_ttc'] - $nli_pris_en_charge;
        } elseif ($_POST['nli_region'] == 2 && !empty($cp['ncp_plafond_2']) && $_POST['nli_ttc'] > ($cp['ncp_plafond_2'] * $_POST['nli_qte'])) {
            $nli_pris_en_charge = $cp['ncp_plafond_2'] * $_POST['nli_qte'];
            $nli_pris_en_charge = getMultiplier($_POST) * $nli_pris_en_charge;
            $nli_depassement_valide = $_POST['nli_ttc'] - $nli_pris_en_charge;
        }

        $nli_pris_en_charge = $nli_depassement_accorde + $nli_pris_en_charge;


        $nli_depassement_valide = $nli_depassement_valide - $nli_depassement_accorde;
        $nli_depassement_valide = $nli_depassement_valide < 0 ? 0 : $nli_depassement_valide;
        $nli_depassement_valide = round($nli_depassement_valide, 2);
        $montant_tva = 0;
        /* if (empty($_POST['nli_montant_tva'])) {
            $ttc = $_POST['nli_ttc'];
            // catégorie ligne
            $sql = "SELECT nca_tva FROM ndf_categories WHERE nca_id = " . $_POST['nli_categorie'];
            $req = $Conn->query($sql);
            $d_tva = $req->fetch();
            // montant ht = ttc
            if (empty($nli_depassement_accorde)) {
                $ht = $ttc;
            } else {
                $taux = $d_tva['nca_tva'];
                $ht = $ttc / (1 + ($taux / 100));
            }

            $montant_tva = floatval($ttc - $ht);
        } else { */
            $montant_tva = !empty($_POST['nli_montant_tva']) ? $_POST['nli_montant_tva'] : 0;
        /* } */
        if ($montant_tva) {
            $montant_tva = number_format($montant_tva, 2, '.', '');
        }
        // fin calculer les dépassements
        $nli_compte = $nli['nli_compte'];
        if (!empty($_POST['nli_compte'])) {
            $nli_compte = $_POST['nli_compte'];
        }
        // éditer la ligne
        $req = $Conn->prepare("UPDATE ndf_lignes SET nli_date = :nli_date, nli_vehicule = :nli_vehicule, nli_vehicule_categorie = :nli_vehicule_categorie, nli_ndf = :nli_ndf, nli_categorie = :nli_categorie, nli_type =:nli_type, nli_commentaire= :nli_commentaire, nli_qte = :nli_qte, nli_ttc = :nli_ttc, nli_region=:nli_region, nli_depassement_valide= :nli_depassement_valide, nli_depassement_accorde = :nli_depassement_accorde, nli_pris_en_charge = :nli_pris_en_charge, nli_montant_tva=:nli_montant_tva, nli_compte = :nli_compte WHERE nli_id = :nli_id");
        $req->bindValue(':nli_date', $nli_date);
        $req->bindValue(':nli_ndf', $_POST['id']);
        $req->bindValue(':nli_categorie', $_POST['nli_categorie']);
        $req->bindValue(':nli_type', $_POST['nli_type']);
        $req->bindValue(':nli_commentaire', $_POST['nli_commentaire']);
        $req->bindValue(':nli_qte', $_POST['nli_qte']);
        $req->bindValue(':nli_ttc', $_POST['nli_ttc']);
        $req->bindValue(':nli_region', $_POST['nli_region']);
        $req->bindValue(':nli_id', $_POST['ligne']);
        $req->bindValue(':nli_depassement_valide', $nli_depassement_valide);
        $req->bindValue(':nli_depassement_accorde', $nli_depassement_accorde);
        $req->bindValue(':nli_pris_en_charge', $nli_pris_en_charge);
        $req->bindValue(':nli_montant_tva', $montant_tva);
        $req->bindValue(':nli_vehicule', $_POST['nli_vehicule']);
        $req->bindValue(':nli_vehicule_categorie', $_POST['nli_vehicule_categorie']);
        $req->bindValue(':nli_compte', $nli_compte);
        $req->execute();
        // fin éditer la ligne

        $req = $Conn->prepare("SELECT SUM(nli_ttc) FROM ndf_lignes WHERE nli_refus = 0 AND nli_ndf = " . $_POST['id']);
        $req->execute();
        $ndf_ttc = $req->fetch();

        $req = $Conn->prepare("UPDATE ndf SET ndf_ttc = :ndf_ttc WHERE ndf_id = :ndf_id");
        $req->bindValue(':ndf_ttc', $ndf_ttc['SUM(nli_ttc)']);
        $req->bindValue(':ndf_id', $_POST['id']);
        $req->execute();
        ///////////////// FIN EDITION ///////////////////
    } else {
        //////////////////// AJOUT /////////////////
        // insérer la ligne

        // calculer les dépassements
        // récupérer les categories de profils
        $req = $Conn->prepare("SELECT * FROM ndf_categories_profils WHERE ncp_profil = :ncp_profil AND ncp_categorie = :ncp_categorie AND ncp_type = :ncp_type");
        $req->bindValue("ncp_categorie", $_POST['nli_categorie']);
        $req->bindValue("ncp_profil", $utilisateur['uti_profil']);
        $req->bindValue("ncp_type", $_POST['nli_type']);
        $req->execute();
        $cp = $req->fetch();

        // calculer pris en charge
        $nli_pris_en_charge = $_POST['nli_ttc'];
        $nli_depassement_valide = 0;
        if (!empty($_POST['nli_depassement_accorde'])) {
            $nli_depassement_accorde = $_POST['nli_depassement_accorde'];
        } else {
            $nli_depassement_accorde = 0;
        }

        if ($_POST['nli_region'] == 1 && !empty($cp['ncp_plafond_1']) && $_POST['nli_ttc'] > ($cp['ncp_plafond_1'] * $_POST['nli_qte'])) {
            $nli_pris_en_charge = $cp['ncp_plafond_1'] * $_POST['nli_qte'];
            $nli_pris_en_charge = getMultiplier($_POST) * $nli_pris_en_charge;
            $nli_depassement_valide = $_POST['nli_ttc'] - $nli_pris_en_charge;
        } elseif ($_POST['nli_region'] == 2 && !empty($cp['ncp_plafond_2']) && $_POST['nli_ttc'] > ($cp['ncp_plafond_2'] * $_POST['nli_qte'])) {
            $nli_pris_en_charge = $cp['ncp_plafond_2'] * $_POST['nli_qte'];
            $nli_pris_en_charge = getMultiplier($_POST) * $nli_pris_en_charge;
            $nli_depassement_valide = $_POST['nli_ttc'] - $nli_pris_en_charge;
        }

        $nli_pris_en_charge = $nli_depassement_accorde + $nli_pris_en_charge;
        $nli_depassement_valide = $nli_depassement_valide - $nli_depassement_accorde;
        $nli_depassement_valide = $nli_depassement_valide < 0 ? 0 : $nli_depassement_valide;
        $nli_depassement_valide = round($nli_depassement_valide, 2);

        $montant_tva = 0;
        if (empty($_POST['nli_montant_tva'])) {
            $ttc = $_POST['nli_ttc'];
            // catégorie ligne
            $sql = "SELECT nca_tva FROM ndf_categories WHERE nca_id = " . $_POST['nli_categorie'];
            $req = $Conn->query($sql);
            $d_tva = $req->fetch();
            // montant ht = ttc
            if (empty($nli_depassement_accorde)) {
                $ht = $ttc;
            } else {
                $taux = $d_tva['nca_tva'];
                $ht = $ttc / (1 + ($taux / 100));
            }

            $montant_tva = floatval($ttc - $ht);
        } else {
            $montant_tva = $_POST['nli_montant_tva'];
        }
        if ($montant_tva) {
            $montant_tva = number_format($montant_tva, 2, '.', '');
        }

        $nli_compte = 0;
        if (!empty($_POST['nli_compte'])) {
            $nli_compte = $_POST['nli_compte'];
        }
        // fin calculer les dépassements
        // fin calculer pris en charge
        $req = $Conn->prepare("INSERT INTO ndf_lignes (nli_date, nli_ndf, nli_categorie, nli_type, nli_commentaire, nli_qte, nli_ttc, nli_numero, nli_region, nli_depassement_valide, nli_pris_en_charge, nli_depassement_accorde, nli_vehicule, nli_vehicule_categorie, nli_montant_tva, nli_compte) VALUES (:nli_date, :nli_ndf, :nli_categorie, :nli_type, :nli_commentaire, :nli_qte, :nli_ttc, :nli_numero, :nli_region, :nli_depassement_valide, :nli_pris_en_charge, :nli_depassement_accorde, :nli_vehicule, :nli_vehicule_categorie, :nli_montant_tva, :nli_compte)");
        $req->bindValue(':nli_date', $nli_date);
        $req->bindValue(':nli_ndf', $_POST['id']);
        $req->bindValue(':nli_categorie', $_POST['nli_categorie']);
        $req->bindValue(':nli_type', $_POST['nli_type']);
        $req->bindValue(':nli_commentaire', $_POST['nli_commentaire']);
        $req->bindValue(':nli_qte', $_POST['nli_qte']);
        $req->bindValue(':nli_ttc', $_POST['nli_ttc']);
        $req->bindValue(':nli_numero', $numero);
        $req->bindValue(':nli_region', $_POST['nli_region']);
        $req->bindValue(':nli_depassement_valide', $nli_depassement_valide);
        $req->bindValue(':nli_depassement_accorde', $nli_depassement_accorde);
        $req->bindValue(':nli_montant_tva', $montant_tva);
        $req->bindValue(':nli_pris_en_charge', $nli_pris_en_charge);
        $req->bindValue(':nli_vehicule', $_POST['nli_vehicule']);
        $req->bindValue(':nli_vehicule_categorie', $_POST['nli_vehicule_categorie']);
        $req->bindValue(':nli_compte', $nli_compte);
        $req->execute();
        $nliId = $Conn->lastInsertId();
        newInvitations($_POST, $nliId, $_POST['id'], true);
        // fin insérer la ligne
        /////////////// maj ttc ndf

        $req = $Conn->prepare("SELECT SUM(nli_ttc) FROM ndf_lignes WHERE nli_refus = 0 AND nli_ndf = " . $_POST['id']);
        $req->execute();
        $ndf_ttc = $req->fetch();

        $req = $Conn->prepare("UPDATE ndf SET ndf_ttc = :ndf_ttc WHERE ndf_id = :ndf_id");
        $req->bindValue(':ndf_ttc', $ndf_ttc['SUM(nli_ttc)']);
        $req->bindValue(':ndf_id', $_POST['id']);
        $req->execute();
        ////////////// fin maj ttc ndf
        //////////////////// FIN AJOUT /////////////////
    }



    $_SESSION['message'][] = array(
        "titre" => "Note de frais",
        "type" => "success",
        "message" => "La dépense a été enregistrée."
    );
    Header("Location: ndf.php?id=" . $_POST['id']);
    die();
    // FIN INSERTION DE LA LIGNE
} else { // si la note de frais n'est pas créée
    // INSERER LA NDF

    // aller chercher l'utilisateur concerné par la ndf
    $req = $Conn->prepare("SELECT * FROM utilisateurs WHERE uti_id = :uti_id");
    $req->bindValue(':uti_id', $_POST['utilisateur']);
    $req->execute();
    $utilisateur = $req->fetch();
    // fin aller chercher l'utilisateur concerné par la ndf
    // LE CA
    $sql = "SELECT * FROM utilisateurs
    LEFT JOIN utilisateurs_societes ON (utilisateurs_societes.uso_utilisateur = utilisateurs.uti_id)
     WHERE uso_agence = " . $utilisateur['uti_agence'] . " AND uso_societe = " . $utilisateur['uti_societe'] . " AND uti_profil = 15 AND uti_archive = 0";
    $req = $Conn->query($sql);
    $chef_agence = $req->fetch();

    $ra = 0;
    if (!empty($chef_agence)) {
        $ra = $chef_agence['uti_id'];
    }
    // insérer dans la table ndf
    $date = $_POST['ndf_date_annee'] . "-" . $_POST['ndf_date_mois'] . "-" . $_POST['nli_date']; // construction de la date

    $req = $Conn->prepare("SELECT * FROM baremes_kilometres WHERE bki_date_deb <= :bki_date_deb AND bki_date_fin >= :bki_date_deb");
    $req->bindValue(':bki_date_deb', date("Y-m-d"));
    $req->execute();
    $bareme_kilometre = $req->fetch();

    $req = $Conn->prepare("INSERT INTO
            ndf (ndf_quinzaine_deb, ndf_quinzaine_fin, ndf_utilisateur, ndf_devise, ndf_date, ndf_re, ndf_ra, ndf_societe, ndf_agence, ndf_mois, ndf_annee, ndf_bareme, ndf_puissance, ndf_tranche_km)
            VALUES (:ndf_quinzaine_deb, :ndf_quinzaine_fin, :ndf_utilisateur, 'EUR', :ndf_date, :ndf_re, :ndf_ra, :ndf_societe, :ndf_agence, :ndf_mois, :ndf_annee, :ndf_bareme, :ndf_puissance, :ndf_tranche_km)");
    $req->bindValue(':ndf_quinzaine_deb', $_POST['ndf_quinzaine_deb']);
    $req->bindValue(':ndf_quinzaine_fin', $_POST['ndf_quinzaine_fin']);
    $req->bindValue(':ndf_utilisateur', $_POST['utilisateur']);
    $req->bindValue(':ndf_date', $date);
    $req->bindValue(':ndf_re', $utilisateur['uti_responsable']);
    $req->bindValue(':ndf_ra', $ra);
    $req->bindValue(':ndf_societe', $utilisateur['uti_societe']);
    $req->bindValue(':ndf_agence', $utilisateur['uti_agence']);
    $req->bindValue(':ndf_mois', date("n"));
    $req->bindValue(':ndf_annee', date("Y"));
    $req->bindValue(':ndf_bareme', $bareme_kilometre['bki_id']);
    $req->bindValue(':ndf_puissance', $utilisateur['uti_veh_cv']);
    $req->bindValue(':ndf_tranche_km', $utilisateur['uti_tranche_km']);
    $req->execute();

    $ndf_id = $Conn->lastInsertId();
    // fin insérer dans la table ndf
    // FIN INSERTION NDF

    // CALCUL DE L'AVANCE
    $req = $Conn->prepare("SELECT * FROM ndf WHERE
    ndf_societe = " . $_SESSION['acces']['acc_societe'] . " AND
    ndf_agence = " . $_SESSION['acces']['acc_agence'] . " AND
    ndf_utilisateur = " . $_POST['utilisateur'] . "
    AND (ndf_statut = 5 OR ndf_statut = 6 OR ndf_statut = 7)
    AND ndf_id =
    (SELECT ndf_id FROM ndf
    WHERE ndf_societe = " . $_SESSION['acces']['acc_societe'] . "
    AND ndf_agence = " . $_SESSION['acces']['acc_agence'] . "
    AND ndf_utilisateur = " . $_POST['utilisateur'] . "
    AND ndf_quinzaine_fin < '" . $_POST['ndf_quinzaine_deb'] . "'
     ORDER BY ndf_quinzaine_fin DESC LIMIT 1)
     AND ndf_id != " . $ndf_id);
	$req->execute();
    $ndf_precedente = $req->fetch();

    if (!empty($ndf_precedente)) {
        // JE VAIS CHERCHER LES AVANCES DE LA NOTE PRECEDENTE
        $req = $Conn->prepare("SELECT nav_ttc FROM ndf_avances WHERE nav_ndf = " . $ndf_precedente['ndf_id']);
        $req->execute();
        $ndf_avances = $req->fetch();

        // SI LE TOTAL TTC EST INFERIEUR A L'AVANCE, JE L'AJOUTE A LA NOTE ACTUELLE
        if ($ndf_precedente['ndf_ttc'] < $ndf_avances['nav_ttc']) {
            $nav_date = date('Y-m-d');
            $diff = $ndf_avances['nav_ttc'] - $ndf_precedente['ndf_ttc'];
            if ($diff > 0) {
                $req = $Conn->prepare("INSERT INTO ndf_avances (nav_ndf, nav_utilisateur, nav_date, nav_ttc, nav_commentaire) VALUES (:nav_ndf, :nav_utilisateur, :nav_date, :nav_ttc, :nav_commentaire)");
                $req->bindValue(':nav_ndf', $ndf_id);
                $req->bindValue(':nav_utilisateur', $_POST['utilisateur']);
                $req->bindValue(':nav_date', $nav_date);
                $req->bindValue(':nav_ttc', $diff);
                $req->bindValue(':nav_commentaire', "Reste de l'avance de la note précédente");
                $req->execute();

                $req = $Conn->prepare("UPDATE ndf_avances SET nav_ttc = :nav_ttc WHERE nav_ndf = " . $ndf_precedente['ndf_id']);
                $req->bindValue(':nav_ttc', $ndf_precedente['ndf_ttc']);
                $req->bindValue(':nav_commentaire', "Avance initiale de " . $ndf_avances['nav_ttc'] . " €");
                $req->execute();
            }
        }

    }
    // FIN CALCUL DE L'AVANCE

    // INSERER LA LIGNE

    // avoir le numéro de la ndf
    $req = $Conn->prepare("SELECT MAX(nli_numero) FROM ndf_lignes WHERE nli_refus = 0 AND nli_ndf = :nli_ndf");
    $req->bindValue(':nli_ndf', $ndf_id);
    $req->execute();
    $derniere_ligne = $req->fetch();
    // fin avoir le numéro de la ndf

    // format ttc
    $_POST['nli_ttc'] = number_format($_POST['nli_ttc'], 2, '.', '');
    $_POST['nli_ttc'] = floatval($_POST['nli_ttc']);
    if (!empty($_POST['nli_montant_tva'])) {
        $_POST['nli_montant_tva'] = number_format($_POST['nli_montant_tva'], 2, '.', '');
        $_POST['nli_montant_tva'] = floatval($_POST['nli_montant_tva']);
    }
    if (!empty($_POST['nli_depassement_accorde'])) {
        $_POST['nli_depassement_accorde'] = number_format($_POST['nli_depassement_accorde'], 2, '.', '');
        $_POST['nli_depassement_accorde'] = floatval($_POST['nli_depassement_accorde']);
    }

    // calcul montant tva par défaut

    //fin format ttc
    $montant_tva = 0;
    if (empty($_POST['nli_montant_tva'])) {
        $ttc = $_POST['nli_ttc'];
        // catégorie ligne
        $sql = "SELECT nca_tva FROM ndf_categories WHERE nca_id = " . $_POST['nli_categorie'];
        $req = $Conn->query($sql);
        $d_tva = $req->fetch();
        // montant ht = ttc
        if (empty($nli_depassement_accorde)) {
            $ht = $ttc;
        } else {
            $taux = $d_tva['nca_tva'];
            $ht = $ttc / (1 + ($taux / 100));
        }

        $montant_tva = floatval($ttc - $ht);
    } else {
        $montant_tva = $_POST['nli_montant_tva'];
    }
    if ($montant_tva) {
        $montant_tva = number_format($montant_tva, 2, '.', '');
    }
    // numéro
    $numero = $derniere_ligne["MAX(nli_numero)"] + 1;
    // fin numéro
    // calculer les dépassements
    // récupérer les categories de profils
    $req = $Conn->prepare("SELECT * FROM ndf_categories_profils WHERE ncp_profil = :ncp_profil AND ncp_categorie = :ncp_categorie AND ncp_type = :ncp_type");
    $req->bindValue("ncp_categorie", $_POST['nli_categorie']);
    $req->bindValue("ncp_profil", $utilisateur['uti_profil']);
    $req->bindValue("ncp_type", $_POST['nli_type']);
    $req->execute();
    $cp = $req->fetch();

    $nli_pris_en_charge = $_POST['nli_ttc'];
    $nli_depassement_valide = 0;
    if (!empty($_POST['nli_depassement_accorde'])) {
        $nli_depassement_accorde = $_POST['nli_depassement_accorde'];
    } else {
        $nli_depassement_accorde = 0;
    }

    if ($_POST['nli_region'] == 1 && !empty($cp['ncp_plafond_1']) && $_POST['nli_ttc'] > ($cp['ncp_plafond_1'] * $_POST['nli_qte'])) {
        $nli_pris_en_charge = $cp['ncp_plafond_1'] * $_POST['nli_qte'];
        $nli_pris_en_charge = getMultiplier($_POST) * $nli_pris_en_charge;
        $nli_depassement_valide = $_POST['nli_ttc'] - $nli_pris_en_charge;
    } elseif ($_POST['nli_region'] == 2 && !empty($cp['ncp_plafond_2']) && $_POST['nli_ttc'] > ($cp['ncp_plafond_2'] * $_POST['nli_qte'])) {
        $nli_pris_en_charge = $cp['ncp_plafond_2'] * $_POST['nli_qte'];
        $nli_pris_en_charge = getMultiplier($_POST) * $nli_pris_en_charge;
        $nli_depassement_valide = $_POST['nli_ttc'] - $nli_pris_en_charge;
    }
    $nli_pris_en_charge = $nli_depassement_accorde + $nli_pris_en_charge;
    $nli_depassement_valide = $nli_depassement_valide - $nli_depassement_accorde;
    $nli_depassement_valide = $nli_depassement_valide < 0 ? 0 : $nli_depassement_valide;
    $nli_depassement_valide = round($nli_depassement_valide, 2);

    $nli_compte = 0;
    if (!empty($_POST['nli_compte'])) {
        $nli_compte = $_POST['nli_compte'];
    }
    // fin calculer pris en charge
    // insérer la ligne
    $req = $Conn->prepare("INSERT INTO ndf_lignes (nli_date, nli_ndf, nli_categorie, nli_type, nli_commentaire, nli_qte, nli_ttc, nli_numero, nli_region, nli_depassement_valide, nli_pris_en_charge, nli_depassement_accorde, nli_vehicule, nli_vehicule_categorie, nli_montant_tva, nli_compte) VALUES (:nli_date, :nli_ndf, :nli_categorie, :nli_type, :nli_commentaire, :nli_qte, :nli_ttc, :nli_numero, :nli_region, :nli_depassement_valide, :nli_pris_en_charge, :nli_depassement_accorde, :nli_vehicule, :nli_vehicule_categorie, :nli_montant_tva, :nli_compte)");
    $req->bindValue(':nli_date', $nli_date);
    $req->bindValue(':nli_ndf', $ndf_id);
    $req->bindValue(':nli_categorie', $_POST['nli_categorie']);
    $req->bindValue(':nli_type', $_POST['nli_type']);
    $req->bindValue(':nli_commentaire', $_POST['nli_commentaire']);
    $req->bindValue(':nli_qte', $_POST['nli_qte']);
    $req->bindValue(':nli_ttc', $_POST['nli_ttc']);
    $req->bindValue(':nli_numero', $numero);
    $req->bindValue(':nli_region', $_POST['nli_region']);
    $req->bindValue(':nli_depassement_valide', $nli_depassement_valide);
    $req->bindValue(':nli_depassement_accorde', $nli_depassement_accorde);
    $req->bindValue(':nli_pris_en_charge', $nli_pris_en_charge);
    $req->bindValue(':nli_vehicule', $_POST['nli_vehicule']);
    $req->bindValue(':nli_vehicule_categorie', $_POST['nli_vehicule_categorie']);
    $req->bindValue(':nli_montant_tva', $montant_tva);
    $req->bindValue(':nli_compte', $nli_compte);
    $req->execute();
    $nliId = $Conn->lastInsertId();
    // fin insérer la ligne
    // fin select de la ndf
    $ttc = $_POST['nli_ttc'];

    $req = $Conn->prepare("UPDATE ndf SET ndf_ttc = :ndf_ttc WHERE ndf_id = :ndf_id");
    $req->bindValue(':ndf_ttc', $ttc);
    $req->bindValue(':ndf_id', $ndf_id);
    $req->execute();

    newInvitations($_POST, $nliId, $ndf_id, true);

    $_SESSION['message'][] = array(
        "titre" => "Note de frais",
        "type" => "success",
        "message" => "La dépense a été enregistrée."
    );

    Header("Location: ndf.php?id=" . $ndf_id);
    die();
    // FIN INSERTION DE LA LIGNE
}


function newInvitations($post, $ligne, $ndf, $update = false) {

    global $Conn;
    if ($update) {
        $req = $Conn->prepare("DELETE FROM ndf_invitations WHERE nin_ndf = :nin_ndf AND nin_ligne = :nin_ligne");
        $req->bindValue(':nin_ndf', $ndf);
        $req->bindValue(':nin_ligne', $ligne);
        $req->execute();
    }
    if ($post['nli_categorie'] == 30 OR $post['nli_categorie'] == 36) {
        // INVITATION CLIENT
        if (!empty($post['nin_autre_client'])) {
            $req = $Conn->prepare("INSERT INTO ndf_invitations (nin_ndf, nin_ligne, nin_autre_client) VALUES
            (:nin_ndf, :nin_ligne, :nin_autre_client) ");
            $req->bindValue(':nin_ndf', $ndf);
            $req->bindValue(':nin_ligne', $ligne);
            $req->bindValue(':nin_autre_client', $post['nin_autre_client']);
            $req->execute();
        }

    } else if ($post['nli_categorie'] == 31 OR $post['nli_categorie'] == 37) {
        // INVITATION INTERNE
        foreach ($post['nin_utilisateur_invite'] as $i=>$u) {
            if (!empty($u)) {
                $req = $Conn->prepare("INSERT INTO ndf_invitations (nin_ndf, nin_ligne, nin_utilisateur_invite) VALUES
                (:nin_ndf, :nin_ligne, :nin_utilisateur_invite) ");
                $req->bindValue(':nin_ndf', $ndf);
                $req->bindValue(':nin_ligne', $ligne);
                $req->bindValue(':nin_utilisateur_invite', $u);
                $req->execute();
            }
        }
    } else if ($post['nli_categorie'] == 16) {
        // HOTEL
        foreach ($post['nin_utilisateur_invite_2'] as $i=>$u) {
            if (!empty($u) && !empty($post['nin_date'][$i])) {
                $req = $Conn->prepare("INSERT INTO ndf_invitations (nin_qte, nin_ndf, nin_ligne, nin_utilisateur_invite, nin_date) VALUES
                (:nin_qte, :nin_ndf, :nin_ligne, :nin_utilisateur_invite, :nin_date) ");
                $req->bindValue(':nin_qte', $post['nin_qte'][$i]);
                $req->bindValue(':nin_ndf', $ndf);
                $req->bindValue(':nin_ligne', $ligne);
                $req->bindValue(':nin_utilisateur_invite', $u);
                $req->bindValue(':nin_date', $post['ndf_date_annee'] . "-" . $post['ndf_date_mois'] . "-" . $post['nin_date'][$i]);
                $req->execute();
            }

        }
    }
}

function getMultiplier($post) {
    $multiplier = 1;
    if ($post['nli_categorie'] == 30 OR $post['nli_categorie'] == 36) {
        // INVITATION CLIENT
        $multiplier = 1;
    } else if ($post['nli_categorie'] == 31 OR $post['nli_categorie'] == 37) {
        // INVITATION INTERNE
        $multiplier = 1;

    } else if ($post['nli_categorie'] == 16) {
        // HOTEL
        $multiplier = 1;
    }
    return $multiplier;
}