<?php

	// DETAIL DU RATION RETARD DE REGLEMENT

	include "includes/controle_acces.inc.php";

	include "includes/connexion.php";
	include "includes/connexion_soc.php";


    // CONTROLE ACCES
    
    if(!$_SESSION["acces"]["acc_droits"][35]){

		$_SESSION['message'][] = array(
			"aff" => "",
			"titre" => "Accès refusé!",
			"type" => "danger",
			"message" => "Vous n'êtes pas autorisé à accéder à cette page!" 
		);
	
		header("location : " . $_SESSION["retour"]);
        die();
        
	}

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}

    // TRAITEMENT
    
	$data=array();
	
	$sql="SELECT acl_ca,acl_id,acl_commercial
    ,com_label_1,com_label_2 
    ,max_date
    FROM Actions_Clients
	LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
	LEFT JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)
	INNER JOIN (
		SELECT acd_action_client, MAX(pda_date) AS max_date FROM Actions_Clients_Dates,plannings_dates WHERE acd_date=pda_id GROUP BY acd_action_client) AS groupe
	ON Actions_Clients.acl_id = groupe.acd_action_client
    WHERE NOT acl_facture AND NOT acl_archive AND NOT act_archive AND max_date<=NOW() AND NOT acl_non_fac AND acl_confirme";
	if(!$_SESSION["acces"]["acc_droits"][35]){
		$sql.=" AND com_ref_1=" . $acc_utilisateur;
    }elseif(!empty($commercial) AND $commercial != -1){
		$sql.=" AND acl_commercial=" . $commercial;
    }
    
    if(!empty($acc_agence)){
		$sql.=" AND act_agence=" . $acc_agence;
	}
	$sql.=" ORDER BY max_date";
    $req=$ConnSoc->query($sql);
    $d_actions=$req->fetchAll(); 
    if(!empty($d_actions)){

        foreach($d_actions as $action){

            if(!isset($data[$action["acl_commercial"]])){
                $data[$action["acl_commercial"]]=array(
                    "identite" => $action["com_label_1"] . " " . $action["com_label_2"],
                    "ajd" => 0,
                    "hier" => 0
                );
            }

            $data[$action["acl_commercial"]]["ajd"]=$data[$action["acl_commercial"]]["ajd"] + $action["acl_ca"];
            if($action["max_date"]<date("Y-m-d")){
                $data[$action["acl_commercial"]]["hier"]=$data[$action["acl_commercial"]]["hier"] + $action["acl_ca"];
            }

        }
    }

    
	$titre="";

    if(!empty($acc_agence)) {

        $sql="SELECT soc_nom,age_nom FROM Agences,Societes WHERE soc_id=age_societe AND age_id=" . $acc_agence . ";";
        $req=$Conn->query($sql);
        $d_agence=$req->fetch();
        if(!empty($d_agence)){
            $titre="Retard de facturation sur " . $d_agence["soc_nom"] . " " . $d_agence["age_nom"];
        }

    }else{

        $sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
        $req=$Conn->query($sql);
        $d_societe=$req->fetch();
        if(!empty($d_societe)){
            $titre="Retard de facturation sur " . $d_societe["soc_nom"];
        }

    }
	
	

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">

<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" >
					<div class="row ">

                        <h1 class="text-center" ><?=$titre?></h1>
                    
						<div class="table-responsive">
                            <table class="table table-striped table-hover" >
                                <thead>
                                    <tr class="dark">
                                        <th>Commercial</th>	
                                        <th class="text-right" >CA J-1</th>
                                        <th class="text-right" >CA J</th>
                                        <th class="text-right" >Retard de facturation (%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                        <?php		if(!empty($data)){

                                        $total_ajd=0;
                                        $total_hier=0;
                                        foreach($data as $com_id => $d){

                                            $total_ajd=$total_ajd + $d["ajd"];
                                            $total_hier=$total_hier + $d["hier"]; 
                                            
                                            $ratio=0;
                                            if(!empty($d["ajd"])){
                                                $ratio=($d["hier"]/$d["ajd"])*100; 
                                            } ?>

                                            <tr>
                                                <td>
                                                    <a href="fac_ratio_retard_fac_detail.php?commercial=<?=$com_id?>">
                                                        <?=$d["identite"]?>
                                                    </a>
                                                </td>
                                                <td class="text-right" ><?=number_format($d["hier"],2,","," ");?></td>
                                                <td class="text-right" ><?=number_format($d["ajd"],2,","," ");?></td>                                               
                                                <td class="text-right" ><?=number_format($ratio,2)?></td>
                                            </tr>
                        <?php			}

                                        $ratio=0;
                                        if(!empty($total_ajd)){
                                            $ratio=($total_hier/$total_ajd)*100;
                                        }
                                        ?>
                                        <tr>
                                            <th>
                                                <a href="fac_ratio_retard_fac_detail.php?commercial=-1">
                                                    Total :
                                                </a>
                                            </th>
                                            <td class="text-right" ><?=number_format($total_hier,2,","," ")?></td>
                                            <td class="text-right" ><?=number_format($total_ajd,2,","," ")?></td>
                                            <td class="text-right" ><?=number_format($ratio,2,","," ")?></td>
                                        </tr>
                        <?php		} ?>
                                </tbody>
                            </table>
						</div>

					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="<?=$_SESSION["retour"]?>" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){



			});
		</script>
	</body>
</html>
