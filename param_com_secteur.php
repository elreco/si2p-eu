<?php

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');

if($_SESSION['acces']["acc_service"][1]!=1 AND $_SESSION['acces']["acc_service"][5]!=1){
	echo("Accès refusé!");
	die();
}

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}
	
	$sql="SELECT soc_agence,soc_nom FROM Societes WHERE soc_id=:societe;";
	$req=$Conn->prepare($sql);
	$req->bindParam(":societe",$acc_societe);
	$req->execute();
	$d_societe=$req->fetch();
	if(!empty($d_societe)){
		if($d_societe["soc_agence"] AND empty($acc_agence)){
			$erreur_txt="Vous devez être connecté sur une agence pour pouvoir gérer la répartition géographique des vendeurs.";
		}
	}else{
		echo("Paramètre absent!");
		die();
	}
	
	if(!isset($erreur_txt)){
		
		if(!empty($acc_agence)){
			$sql="SELECT age_nom FROM Agences WHERE age_id=:agence;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":agence",$acc_agence);
			$req->execute();
			$d_agence=$req->fetch();
			if(empty($d_agence)){
				echo("Paramètre absent!");
				die();
			}
		}
		
		
		// ON SELECTION LES DEPARTEMENTS DE L'AGENCE
		
		$sql="SELECT sde_commercial,sde_arrondissement,sde_departement,dep_numero,dep_nom,dep_id
		FROM Societes_Departements INNER JOIN Departements ON (Societes_Departements.sde_departement=Departements.dep_numero)
		WHERE sde_societe=:societe";
		if(!empty($acc_agence)){
			$sql.=" AND sde_agence=:agence";
		}
		$sql.=" ORDER BY dep_numero,dep_nom;";
		$req=$Conn->prepare($sql);
		$req->bindParam(":societe",$acc_societe);
		if(!empty($acc_agence)){
			$req->bindParam(":agence",$acc_agence);
		}
		$req->execute();
		$d_departements=$req->fetchAll();
		if(empty($d_departements)){
			$erreur_txt="Aucun département n'est affecté à cette agence.";
		}

	}
	
	if(!isset($erreur_txt)){
		
		$d_commerciaux=array();
		
		$sql="SELECT com_id,com_label_1,com_label_2 FROM Commerciaux WHERE com_type=1 AND NOT com_archive";
		if(!empty($acc_agence)){
			$sql.=" AND com_agence=:agence";
		}
		$sql.=" ORDER BY com_label_1,com_label_2;";
		$req=$ConnSoc->prepare($sql);
		if(!empty($acc_agence)){
			$req->bindParam(":agence",$acc_agence);
		}
		$req->execute();
		$d_r_com=$req->fetchAll();
		if(!empty($d_r_com)){
			foreach($d_r_com as $com){
				$d_commerciaux[$com["com_id"]]=$com["com_label_1"] . " " . $com["com_label_2"];
			}
		}
	}

?>
<!DOCTYPE html>
<html>  
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />


		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		
		<form id="filtre" action="param_com_secteur_enr.php" method="post" >
			<div>
				<input type="hidden" name="" value="" />			
			</div>
			<div id="main">
		<?php 	include "includes/header_def.inc.php"; ?>
		
				<section id="content_wrapper" class="">
					<section id="content" class="">		
				<?php	if(isset($erreur_txt)){
							echo("<p class='alert alert-danger text-center' >" . $erreur_txt . "</p>");
						}else{ ?>
							<div class="row">
								<div class="col-md-10 col-md-offset-1">							
									<div class="admin-form theme-primary ">
										<div class="panel heading-border panel-primary">
									
											<div class="panel-body bg-light">
											
												<div class="content-header">
													<h2>
												<?php	echo($d_societe["soc_nom"]);
														if(isset($d_agence)){
															echo(" agence " . $d_agence["age_nom"]);
														} ?>
													</h2>
													<h2><b class="text-primary">Répartition géographique des vendeurs</b></h2>
												</div>
												
												<table class="table" >
													<thead>
														<tr>
															<th rowspan="2" >Département</th>
															<th rowspan="2" >Nom</th>
															<th rowspan="2" >Arrondissement</th>
															<th rowspan="2" >Commercial du secteur</th>
															<th rowspan="2" >Réaffecter les fiches</th>
															<th colspan="3" class="text-center" >Source</th>
															<th colspan="2" class="text-center" >Catégorie</th>
															<th class="text-center" >Correspondance</th>
														</tr>
														<tr>															
															<th>Suspects</th>
															<th>Prospects</th>
															<th>Clients</th>
															<th>Inclure GC Mixte</th>
															<th>Inclure GC CN</th>
															<th>Inclure les rappels non traités</th>
														</tr>
													</thead>
													<tbody>	
										<?php			foreach($d_departements as $dep){ 
										
															if(!empty($dep["sde_arrondissement"])){
																$cle="_" . $dep["sde_departement"] . "_" . $dep["sde_arrondissement"];
															}else{
																$cle="_" . $dep["sde_departement"] . "_00";
															}?>
										
															<tr>
																<td><?=$dep["dep_numero"]?></td>
																<td><?=$dep["dep_nom"]?></td>
																<td>
															<?php	if(!empty($dep["sde_arrondissement"])){														
																		echo($dep["sde_arrondissement"]);
																	} ?>
																</td>
																<td>
																	<select class="select2" name="commercial<?=$cle?>" id="commercial<?=$cle?>" data-cle="<?=$cle?>" >
																		<option value="" >Commercial du secteur</th>
															<?php		foreach($d_commerciaux as $com_id => $com){
																			if($com_id==$dep["sde_commercial"]){
																				echo("<option selected value='" . $com_id . "' >" . $com . "</option>");
																			}else{
																				echo("<option value='" . $com_id . "' >" . $com . "</option>");
																			}
																		} ?>
																	</select>
																</td>
																<td class="text-center" >
																	<label class="option option-dark">
																		<input type="checkbox" id="reaffect<?=$cle?>" name="reaffect<?=$cle?>" data-cle="<?=$cle?>" class="reaffect" >
																		<span class="checkbox"></span>
																	</label>
																</td>
																<td class="text-center" >
																	<label class="option option-dark">
																		<input type="checkbox" name="suspect<?=$cle?>" disabled class="option<?=$cle?>" >
																		<span class="checkbox"></span>
																	</label>
																</td>
																<td class="text-center" >
																	<label class="option option-dark">
																		<input type="checkbox" name="prospect<?=$cle?>" disabled class="option<?=$cle?>" >
																		<span class="checkbox"></span>
																	</label>
																</td>
																<td class="text-center" >
																	<label class="option option-dark">
																		<input type="checkbox" name="client<?=$cle?>" disabled class="option<?=$cle?>" >
																		<span class="checkbox"></span>
																	</label>
																</td>
																<td class="text-center" >
																	<label class="option option-dark">
																		<input type="checkbox" name="gc_mixte<?=$cle?>" disabled class="option<?=$cle?>" >
																		<span class="checkbox"></span>
																	</label>
																</td>
																<td class="text-center" >
																	<label class="option option-dark">
																		<input type="checkbox" name="gc_cn<?=$cle?>" disabled class="option<?=$cle?>" >
																		<span class="checkbox"></span>
																	</label>
																</td>
																<td class="text-center" >
																	<label class="option option-dark">
																		<input type="checkbox" name="rappel<?=$cle?>" disabled class="option<?=$cle?>" >
																		<span class="checkbox"></span>
																	</label>
																</td>
															</tr>
											<?php		} ?>
													</tbody>
												</table>
													
											</div>
											<!-- fin de contenu form -->
											
										</div>
									</div>
								</div>
							</div>
				<?php	} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-4 footer-left">
						<a href="parametre.php" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-4 footer-middle text-center">
						<button type="button" class="btn btn-system btn-sm" data-toggle="modal" data-target="#modal_aide" >
							<i class="fa fa-info" ></i>
						</button>
					</div>
					<div class="col-xs-4 footer-right">					
				<?php	if(!isset($erreur_txt)){ ?>
							<button type="submit" class="btn btn-success btn-sm">
								<i class='fa fa-floppy-o'></i> Enregistrer
							</button>
				<?php	} ?>
					</div>
				</div>
			</footer>
		</form>
		
		<div id="modal_aide" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="mdl_doc_titre" >
							<i class="fa fa-info" ></i> - <span>Aide</span>
						</h4>
					</div>
					<div class="modal-body" >
						<p>
							<b>Commercial du secteur</b> : il s'agit du commercial par défaut. Cette valeur permet d'affecter un commercial aux suspects importés par le service marketing. La modification de cette valeur n'a pas d'impact sur les fiches existantes à moins d'utiliser l'option "réaffecter les fiches". 
						</p>
						<p>
							<b>Réaffecter les fiches</b> : cette option permet d'enregistrer le commercial du secteur sur toutes les fiches clients de l'agence qui répondent aux critères "source". Une réaffectation sans source n'aura aucun effet.
						</p>
						<p>
							<b>Catégorie</b> : par défaut, l'outil ne réaffectera pas le commercial aux fiches grands-comptes accords mixtes et contrats nationnaux (les contrats de référencements seront réaffectés). Vous pouvez modifier ce comportement par défaut en utilisant les options "catégorie".
						</p>
						<p>
							<b>Correspondance</b> : si vous activez cette option, les rappels <b>non traités</b> seront automatiquement réaffectés au commercial. Ils apparaîtront dans son tableau de bord "Clients et suspects à rappeler".
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
							<i class="fa fa-close" ></i>
							Fermer
						</button>
					</div>
				</div>
			</div>
		</div>
<?php
		include "includes/footer_script.inc.php"; ?>  
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<script type="text/javascript">		
			jQuery(document).ready(function(){
				
				$(".reaffect").click(function(){
					$cle=$(this).data("cle");
					if(!$(this).is(":checked")){
						$(".option" + $cle).prop("checked",false);
						$(".option" + $cle).prop("disabled",true);
					}else{
						if($("#commercial" + $cle).val()>0){
							$(".option" + $cle).prop("disabled",false);							
						}else{
							modal_alerte_user("Merci de selectionner un commerial pour pouvoir réaffecter le secteur.","");
							$(this).prop("checked",false);
							$(".option" + $cle).prop("checked",false);
							$(".option" + $cle).prop("disabled",true);
						}
					}				
				});
			
			});
		</script>
	</body>
</html>
