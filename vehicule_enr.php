<?php

	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	$erreur=0;

	if(isset($_POST)){

		// LA PERSONNE LOGUE
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			if(!empty($_SESSION['acces']["acc_societe"])){
				$acc_societe=$_SESSION['acces']["acc_societe"];
			}
		}
		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			if(!empty($_SESSION['acces']["acc_agence"])){
				$acc_agence=$_SESSION['acces']["acc_agence"];
			}
		}

		$vehicule=0;
		if(!empty($_POST['vehicule'])){
			$vehicule=intval($_POST['vehicule']);
		}
		$veh_code="";
		if(!empty($_POST['veh_code'])){
			$veh_code=$_POST['veh_code'];
		}else{
			$erreur=1;
			echo("erreur veh_code<br/>");
		}
		$veh_immat="";
		if(!empty($_POST['veh_immat'])){
			$veh_immat=$_POST['veh_immat'];
		}
		$veh_libelle="";
		if(!empty($_POST['veh_libelle'])){
			$veh_libelle=$_POST['veh_libelle'];
		}else{
			$erreur=1;
			echo("erreur veh_libelle<br/>");
		}
		$veh_societe=0;
		if(!empty($_POST['veh_societe'])){
			$veh_societe=$_POST['veh_societe'];
		}else{
			$erreur=1;
			echo("erreur veh_societe<br/>");
		}

		$veh_agence=0;
		if(!empty($_POST['veh_agence'])){
			$veh_agence=$_POST['veh_agence'];
		}
		$veh_couleur="#FFFFFF";
		if(!empty($_POST['veh_couleur'])){
			$veh_couleur=$_POST['veh_couleur'];
		}

		$veh_archive=0;
		if(!empty($_POST['veh_archive'])){
			$veh_archive=1;
		}

		if($erreur==0){

			if($vehicule>0){

				echo("UPDATE");

				$sql="UPDATE Vehicules SET veh_code=:veh_code, veh_libelle=:veh_libelle, veh_couleur=:veh_couleur";
				$sql.=", veh_agence=:veh_agence, veh_societe=:veh_societe,veh_archive=:veh_archive,veh_immat=:veh_immat";
				$sql.=" WHERE veh_id=:veh_id";
				$req = $Conn->prepare($sql);
				$req->bindParam(':veh_code', $veh_code);
				$req->bindParam(':veh_libelle', $veh_libelle);
				$req->bindParam(':veh_couleur', $veh_couleur);
				$req->bindParam(':veh_agence', $veh_agence);
				$req->bindParam(':veh_societe', $veh_societe);
				$req->bindParam(':veh_immat', $veh_immat);
				$req->bindParam(':veh_archive', $veh_archive);
				$req->bindParam(':veh_id', $vehicule);
				$req->execute();

			}else{

				echo("INSERT");

				$sql="INSERT INTO Vehicules (veh_code, veh_libelle,veh_couleur,veh_agence,veh_societe,veh_immat)";
				$sql.=" VALUES (:veh_code, :veh_libelle, :veh_couleur, :veh_agence, :veh_societe, :veh_immat);";
				$req = $Conn->prepare($sql);
				$req->bindParam(':veh_code', $veh_code);
				$req->bindParam(':veh_libelle', $veh_libelle);
				$req->bindParam(':veh_couleur', $veh_couleur);
				$req->bindParam(':veh_agence', $veh_agence);
				$req->bindParam(':veh_societe', $veh_societe);
				$req->bindParam(':veh_immat', $veh_immat);
				$req->execute();
			}
		}
	}

	$url="vehicule_liste.php?erreur=" . $erreur;
	if($veh_archive==1){
		$url.="&archive=1";
	}

	Header('Location: ' . $url);


?>
