<?php

include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';

include 'modeles/mod_parametre.php';
include 'modeles/mod_get_passe_validite.php';
include 'modeles/mod_upload.php';
include 'modeles/mod_document.php';
include 'modeles/mod_add_utilisateur_etech.php';
include 'modeles/mod_set_utilisateur_etech.php';
// SCRIPT POUR L'AJOUT D'UN NOUVEL Utilisateur

$erreur="";
if(!empty($_POST['archive'])){
	$archive = 1;
}else{
	$archive = 0;
}

if(!empty($_POST['contact'])){
	$con_id = $_POST['contact'];
	
	$sql="SELECT con_mail FROM Contacts WHERE con_id = ". $con_id;
	$req = $Conn->prepare($sql);
	$req->execute();
	$d_contact = $req->fetch();

	if(!empty($d_contact['con_mail'])){
		$email = $d_contact['con_mail'];
	}else{
		$email = $_POST["acc_uti_ident"];
	}
}
$acc_passe_modif=get_passe_validite();

	$acc_passe_modif=get_passe_validite();
		
		if(!is_bool($acc_passe_modif)){
			// AJOUTER CONTROLE SI LE MOT DE PASSE EST DIFFERENT
			
			$sql="SELECT acc_uti_passe FROM Acces  
				WHERE acc_ref = 2 AND acc_ref_id = " . $con_id;
				$req = $Conn->prepare($sql);
				$req->execute();
				$acces = $req->fetch();
			if(!empty($acces)){
				
				if($acces['acc_uti_passe'] != $_POST["acc_uti_passe"]){
					// + ARCHIVE
					// EDITION

					
						$sql="UPDATE Acces SET acc_uti_ident =:acc_uti_ident,acc_uti_passe=:acc_uti_passe,acc_passe_modif=:acc_passe_modif,acc_profil=:acc_profil,acc_mail=:acc_mail,acc_archive = :acc_archive   
						WHERE acc_ref = 2 AND acc_ref_id = :con_id";
						$req = $Conn->prepare($sql);
						$req->bindValue(":acc_uti_ident",$_POST["acc_uti_ident"]);
						$req->bindValue(":acc_uti_passe",$_POST["acc_uti_passe"]);
						$req->bindValue(":acc_passe_modif",$acc_passe_modif);
						$req->bindValue(":acc_profil",0);
						$req->bindValue(":acc_mail",$email);
						$req->bindValue(":acc_archive",$archive);
						$req->bindValue(":con_id",$con_id);
					
					
				}else{
					$sql="UPDATE Acces SET acc_uti_ident =:acc_uti_ident,acc_uti_passe=:acc_uti_passe,acc_profil=:acc_profil,acc_mail=:acc_mail,acc_archive = :acc_archive   
					WHERE acc_ref = 2 AND acc_ref_id = :con_id";
					$req = $Conn->prepare($sql);
					$req->bindValue(":acc_uti_ident",$_POST["acc_uti_ident"]);
					$req->bindValue(":acc_uti_passe",$_POST["acc_uti_passe"]);
					$req->bindValue(":acc_profil",0);
					$req->bindValue(":acc_mail",$email);
					$req->bindValue(":acc_archive",$archive);
					$req->bindValue(":con_id",$con_id);

				}
				

			}else{
				// CREATION
				$sql="INSERT INTO Acces (acc_ref,acc_ref_id,acc_uti_ident,acc_uti_passe,acc_passe_modif,acc_profil,acc_mail)
				VALUES (2," . $con_id . ",:acc_uti_ident,:acc_uti_passe,:acc_passe_modif,:acc_profil,:acc_mail)";
				$req = $Conn->prepare($sql);
				$req->bindValue(":acc_uti_ident",$_POST["acc_uti_ident"]);
				$req->bindValue(":acc_uti_passe",$_POST["acc_uti_passe"]);
				$req->bindValue(":acc_passe_modif",$acc_passe_modif);
				$req->bindValue(":acc_profil",0);
				$req->bindValue(":acc_mail",$email);
			}
			
			try{
				$req->execute();	
			}Catch(Exception $e){
				echo($e->getMessage());
				die();
				$erreur="L'accès n'a pas été enregistré";
			}
		}

if(!empty($erreur)){
	
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur
	);
		$adresse="client_acces.php?client=" . $_POST['client'];
	
	
	echo("<pre>");
	print_r($_SESSION['message']);
	echo("</pre>");

}else{
	
	
	$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "L'accès client a bien été actualisé"
	);
	$adresse="client_voir.php?client=" . $_POST['client'] . "&tab14";


}
header("location : " . $adresse);
?>

