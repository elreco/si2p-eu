<?php

    include "includes/controle_acces.inc.php";
    include_once("includes/connexion_soc.php");
    include_once("includes/connexion.php");
    include_once("includes/connexion_fct.php");
    include_once("modeles/mod_parametre.php");

    $erreur=0;
    $erreur_txt="";

    // PARAMETRES
    $action_id=0;
    if(isset($_GET["action"])){
       if(!empty($_GET["action"])){
           $action_id=$_GET["action"];
       }
    }
    $action_client=0;
    if(isset($_GET["action_client"])){
       if(!empty($_GET["action_client"])){
           $action_client=$_GET["action_client"];
       }
    }

    $aci=0;
    if(isset($_GET["aci"])){
       if(!empty($_GET["aci"])){
           $aci=$_GET["aci"];
       }
    }
    $conn_soc_id=0;
    if(isset($_GET["societ"])){
       if(!empty($_GET["societ"])){
           $conn_soc_id=$_GET["societ"];
       }
    }

   if(empty($action_id) OR empty($action_client)){
       $erreur_txt="Impossible d'afficher la page";
   }
   // FIN PARAMETRES

    if(empty($erreur_txt)){

        // DONNES SOCIETE ET AGENCE
        $acc_societe=0;
        if(isset($_SESSION['acces']["acc_societe"])){
           $acc_societe=$_SESSION['acces']["acc_societe"];
        }
        $acc_agence=0;
        if(isset($_SESSION['acces']["acc_agence"])){
           $acc_agence=$_SESSION['acces']["acc_agence"];
        }

       // SUR L'ACTION
       $sql="SELECT * FROM Actions WHERE act_id=:action;";
       $req=$ConnSoc->prepare($sql);
       $req->bindParam(":action",$action_id);
       $req->execute();
       $d_action=$req->fetch();
       // FAMILLE
       $famille = $d_action['act_pro_famille'];
       $sous_famille = $d_action['act_pro_sous_famille'];
       $sous_sous_famille = $d_action['act_pro_sous_sous_famille'];

       // SUR L'ACTION CLIENT
       $sql="SELECT * FROM Actions_Clients
       LEFT JOIN Clients ON (Actions_Clients.acl_client = Clients.cli_id)
        WHERE acl_id=:action_client;";
       $req=$ConnSoc->prepare($sql);
       $req->bindParam(":action_client",$action_client);
       $req->execute();
       $d_action_client=$req->fetch();
       $acl_produit = $d_action_client['acl_produit'];
       if(!empty($d_action_client['cli_interco_soc'])){
           // societe interco
           $sql="SELECT * FROM Societes WHERE soc_id=:soc_id;";
           $req=$Conn->prepare($sql);
           $req->bindParam(":soc_id",$d_action_client['cli_interco_soc']);
           $req->execute();
           $d_societe_interco=$req->fetch();
       }else{
           die("probleme : ce n'est pas une interco");
       }

       // C'EST UNE MODIFICATION
       $d_action_client_interco  = array();
        if(!empty($aci)){
           // SUR L'ACTION CLIENT
           $sql="SELECT * FROM Actions_Clients_Intercos WHERE aci_id=:aci;";
           $req=$ConnSoc->prepare($sql);
           $req->bindParam(":aci",$aci);
           $req->execute();
           $d_action_client_interco=$req->fetch();
       }else{
           // ALLER CHERCHER LES DATES
           $sql="SELECT * FROM actions_clients_dates
            LEFT JOIN actions_clients ON (actions_clients.acl_id = actions_clients_dates.acd_action_client)
            LEFT JOIN plannings_dates ON (plannings_dates.pda_id = actions_clients_dates.acd_date)
           WHERE acd_action_client = :action_client
           ORDER BY pda_date ASC";
           $req = $ConnSoc->prepare($sql);
           $req->bindParam(":action_client",$action_client);
           $req->execute();
           $d_actions_dates = $req->fetchAll();
           // FIN ALLER CHERCHER LES DATES
           $date_deb = $d_actions_dates[0]['pda_date'];
           $date_fin = $d_actions_dates[0]['pda_date'];
           // ALLER CHERCHER LA DATE DE FIN
           foreach($d_actions_dates as $k=>$acd){
               if($acd['pda_date'] > $date_fin){
                   $date_fin = $acd['pda_date'];
               }
           }
           ///////////////////// INTERCO //////////////////////////
           if(!empty($d_actions_dates)){
               $ConnFct=connexion_fct($d_action_client['cli_interco_soc']);
               // CHERCHER LES ACTIONS CORRESPONDANTES SUR LE PLANNING
               $sql="SELECT DISTINCT cli_code,act_id, acl_id, act_date_deb, acl_pro_libelle FROM plannings_dates
               LEFT JOIN Actions_Clients_Dates ON (Actions_Clients_Dates.acd_date = plannings_dates.pda_id)
               LEFT JOIN Actions_Clients ON (Actions_Clients.acl_id = Actions_Clients_Dates.acd_action_client)
               LEFT JOIN Actions ON (Actions.act_id = Actions_clients.acl_action)
               LEFT JOIN Clients ON (Clients.cli_id = Actions_Clients.acl_client)
               LEFT JOIN Intervenants ON (Intervenants.int_id = Actions.act_intervenant)
               WHERE pda_date = :date_deb
                AND act_pro_famille = :act_pro_famille AND acl_produit = :acl_produit
               AND  act_archive = 0 AND acl_archive = 0 AND act_pro_sous_famille = :act_pro_sous_famille AND act_pro_sous_sous_famille = :act_pro_sous_sous_famille AND int_type = 2
                AND int_ref_1 = :int_ref_1 AND int_ref_2 = :int_ref_2";
               $req = $ConnFct->prepare($sql);
               $req->bindParam(":date_deb",$date_deb);
               $req->bindParam(":acl_produit",$acl_produit);
               $req->bindParam(":act_pro_famille",$famille);
               $req->bindParam(":act_pro_sous_famille",$sous_famille);
               $req->bindParam(":act_pro_sous_sous_famille",$sous_sous_famille);
               $req->bindParam(":int_ref_1",$acc_societe);
               $req->bindParam(":int_ref_2",$acc_agence);
               $req->execute();
               $d_actions_interco = $req->fetchAll();
               foreach($d_actions_interco as $k=>$dai){
                   $d_actions_interco[$k]['act_date_deb'] = convert_date_txt($dai['act_date_deb']);
                }

           }
           /////////////////// FIN INTERCO ///////////////////////
       }
    }
    $afficher_message_1 = false;
    $afficher_message_2 = false;
    $earlier = new DateTime();
    $later = new DateTime($date_deb);
    $diff = $later->diff($earlier)->format("%a");
    if ($date_deb < date("Y-m-d")) {
        $afficher_message_1 = true;
    } else if ($diff < 7){
        $afficher_message_2 = true;
    }
    if(!empty($erreur_txt)){
       echo($erreur_txt);
       die();
    }
?>
       <!DOCTYPE html>
       <html>
           <head>
               <meta charset="utf-8">
               <title>SI2P - Orion</title>
               <meta name="keywords" content=""/>
               <meta name="description" content="">
               <meta name="author" content="">
               <meta name="viewport" content="width=device-width, initial-scale=1.0">
               <!-- CSS THEME -->
               <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
               <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />

               <!-- CSS PLUGINS -->
               <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
               <link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

               <!-- CSS Si2P -->
               <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

               <!-- Favicon -->
               <link rel="shortcut icon" href="assets/img/favicon.png">

               <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
               <!--[if lt IE 9]>
               <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
               <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
               <![endif]-->
           </head>
   <body class="sb-top sb-top-sm ">
       <form method="post" action="action_client_interco_enr.php">
           <div>
               <input type="hidden" class="client-n-societe"  name="aci_action_client_interco_soc" value="<?= $d_action_client['cli_interco_soc'] ?>">
               <input type="hidden" name="aci_action_client" value="<?=$action_client?>">
               <input type="hidden" name="action" value="<?=$action_id?>">
               <input type="hidden" name="societ" value="<?=$conn_soc_id?>">
           </div>
           <div id="main">
   <?php	include "includes/header_def.inc.php"; ?>
               <section id="content_wrapper">
                   <section id="content" class="animated">
                       <?php if(!empty($d_action_client['cli_interco_soc'])){ ?>
                        <div class="row">
                           <div class="col-md-12">
                               <div class="admin-form theme-primary ">

                                   <div class="panel heading-border panel-primary">

                                       <div class="panel-body bg-light">

                                           <div class="content-header mtn mbn">
                                       <?php	echo("<h2>Interco <b class='text-primary' >Action N°". $action_id . "</b>  / Inscription Client <b class='text-primary' >Action N°". $action_client . "</b></h2>"); ?>
                                           </div>

                                           <!-- LIEU D'INTERVENTION ET CONTACT -->

                                           <div class="row mt30">
                                               <div class="col-md-6 col-md-offset-3">
                                                   <p class="alert alert-warning">Votre action doit comporter <strong>les mêmes horaires</strong> que l'action qui facture ainsi que la même famille de produits.</p>
                                               </div>
                                            </div>
                                           <!-- <div class="row mt20">
                                               <div class="col-md-6 col-md-offset-3" >
                                                   <div class="section" >
                                                       <label for="client" >Client : <strong>(Falcutatif)</strong></label>
                                                       <select name="aci_client" id="client" class="select2 select2-client-autre-societe">
                                           <?php			if(!empty($d_action_client_interco) && $d_action_client_interco["aci_client"]>0){ ?>
                                                               <option value='' selected='selected' >Client</option>
                                                               <option value="<?=$d_action_client_interco["aci_client"]?>" selected="selected" ><?=$d_action_client_interco["aci_nom"] . " (" . $d_action_client_interco["aci_code"] . ")"?></option>
                                           <?php			}else{
                                                               echo("<option value='' selected='selected' >Client</option>");
                                                           }?>

                                                       </select>
                                                       <small class="text-danger texte_required" id="client_required">
                                                           Merci de selectionner un client
                                                       </small>
                                                   </div>
                                               </div>
                                           </div> -->
                                           <input type="hidden" name="aci_client" value="" id="client">
                                           <div class="row">


                                               <div class="col-md-8 col-md-offset-2" >
                                                   <div class="section" >
                                                       <label for="client" >Sélectionnez l'action sur le planning de <?= $d_societe_interco['soc_nom'] ?> :</label>
                                                       <select name="aci_action_client_interco" id="interco_select" class="select2" required >
                                                           <option value="0">Sélectionnez une action...</option>
                                                           <?php foreach($d_actions_interco as $dai){ ?>
                                                               <option value="<?= $dai['act_id'] ?>-<?= $dai['acl_id'] ?>"><?= $dai['act_id'] ?> - <?= $dai['acl_id'] ?> — <?= $dai['cli_code'] ?> — <?=  $dai['acl_pro_libelle']?> - <?=  $dai['act_date_deb']?></option>
                                                           <?php } ?>
                                                       </select>

                                                       <p class="alert alert-info  mt10">Si vous ne trouvez pas l'action interco : merci de prendre contact avec l'agence concernée, pour vérifier si l'action est bien créée et que l'intervenant interco a bien une société et une agence.</p>

                                                   </div>
                                               </div>
                                           </div>

                                       </div>
                                   </div>
                               </div>
                           </div>
                       <div>
                       <?php }else{ ?>
                           <div class="alert alert-danger">
                               L'action n'est pas une interco
                           </div>
                       <?php } ?>
                   </section>
               </section>
           </div>
       <footer id="content-footer" class="affix">
           <div class="row">
               <div class="col-xs-3 footer-left">
                   <a href="action_voir.php?action=<?=$action_id?>&action_client=<?= $action_client ?>&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
                       Retour
                   </a>
               </div>
               <div class="col-xs-6 footer-middle"></div>
               <div class="col-xs-3 footer-right">
                   <button type="submit" class="btn btn-sm btn-success" >
                       <i class="fa fa-save" ></i> Enregistrer
                   </button>
               </div>
           </div>
       </footer>
   </form>
   <div id="modal_warning" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Attention</h4>
                </div>

                <div class="modal-body">
                <?php if($afficher_message_2) { ?>
                    Lien interco à moins de 7 jours de la formation. Merci de pensez à prevenir votre formateur afin qu'il retélécharge la formation
                <?php } else if ($afficher_message_1) ?>
                    La formation est déjà passée.
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Compris</button>
                </div>
            </div>

        </div>
    </div>
<?php
       include "includes/footer_script.inc.php"; ?>
       <script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
       <script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>
       <script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
       <script type="text/javascript" src="assets/js/custom.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
           <?php if($afficher_message_1 OR $afficher_message_2) { ?>
            $('#modal_warning').modal('show');
           <?php } ?>
           // INIT
            // $("#client option").click(function(){
            //     get_actions($(this).val());
            // });
            // if($("#client").val()){
            //     get_actions($("#client").val());
            // }
            get_client_interco($("#interco_select").val());
            $("#interco_select").change(function(){
                get_client_interco($(this).val());
            });
            // CLIENTS D'UNE AUTRE SOCIETE
            $(".select2-client-autre-societe").select2({
                minimumInputLength: 1,
                ajax: {
                    url: "ajax/ajax_client_autre_societe.php",
                    dataType: 'json',
                    delay: 250,
                    method: 'GET',
                    data: function (params) {
                        return {
                            q: params.term, // les termes de la requête
                            societe: "<?= $d_action_client['cli_interco_soc'] ?>", //$(".select2-client-categorie").val(),
                            agence: "<?= $d_action_client['cli_interco_age'] ?>"
                        };
                    },
                    processResults: function (data) {
                        // data correspond au tableau json des résultat
                        // data.nom permet d'accéder à l'information $retour["nom"]
                        return {
                            results: data,
                        };
                    },
                },
                templateResult: function(data) {
                    // permet de chosir quelles informations seront affichées dans la liste de résultat
                    // parmit les éléments de data
                    //if (data.loading) return data.text;
                    return data.nom;
                },
                templateSelection: function(data) {
                    // permet de chosir quelles informations seront affichées dans le select
                    // parmit les éléments de data
                    // une fois que l'utilisateur aura fait un choix.

                    //if (data.loading) return data.text;
                    // Sinon affichage du placeholder (data.text).
                    if(data.nom){
                        // revoyer par la requete ajax
                        return data.nom;
                    }else{
                        // cree par defaut à l'ini du plugin
                        return data.text;
                    }
                    //if (data.text) return data.text;

                    //console.log("C data : " + data);
                    //	return data.nom;
                },
            });
       });



            // RECUPERER LES ACTIONS DE LA SOCIETE
           function get_actions(client){
               $.ajax({
                   url : 'ajax/ajax_get_societe_action.php',
                   type : 'GET',
                   data : 'famille=<?= $famille ?>&action=<?= $action_id ?>&action_client=<?= $action_client ?>&client=' + client + '&societe=<?= $d_action_client['cli_interco_soc'] ?>',
                   dataType : 'json',
                   success : function(json, statut){
                       console.log(json);
                       $("#interco_select").find("option:gt(0)").remove();
                       $.each(json, function(key, value) {
                         $('#interco_select')
                         .append($("<option></option>")
                           .attr("value",value["act_id"] + "-" + value["acl_id"])
                           .text(value["act_id"] + "-" + value["acl_id"] + " — " + value["cli_code"] + " — " + value['acl_pro_libelle'] + " — " + value['act_date_deb']));
                       });
                       $("#interco_select").trigger( "change" );
                   },
                   error : function(resultat, statut, erreur){
                       $("#interco_select").find("option:gt(0)").remove();
                   }
               });
           }

           // RECUPERER LES ACTIONS DE LA SOCIETE
          function get_client_interco(action_client){
              $.ajax({
                  url : 'ajax/ajax_get_client_interco.php',
                  type : 'GET',
                  data : 'action_client=' + action_client + '&societe=<?= $d_action_client['cli_interco_soc'] ?>',
                  dataType : 'json',
                  success : function(json, statut){
                      $('#client').val(json["cli_id"]);
                  },
                  error : function(resultat, statut, erreur){
                      $("#client").find("option:gt(0)").remove();
                  }
              });
          }

       </script>
   </body>
</html>
