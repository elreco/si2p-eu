<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	include('modeles/mod_competence.php');
	include('modeles/mod_erreur.php');
	

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link href="assets/admin-tools/admin-forms/css/admin-forms-orion.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php	
				include "includes/header_def.inc.php";
			?>	
				
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">		 
				<!-- Begin: Content -->
							
				<section id="content" class="animated fadeIn">
				
					<div class="row" >
						<div class="col-md-12" >
														
							<div class="table-responsive" >
								<table class="table table-hover table-striped" >
									<thead>
										<tr class="dark" >
											<th>ID</th>
											<th>Compétences</th>
											<th>Diplômes demandés</th>	
											<th>&nbsp;</th>											
										</tr>															
									</thead>
									<tbody>
								<?php	$donnees=get_competences();												
										if(!empty($donnees)){										
											foreach($donnees as $value){ ?>
												<tr>
													<td><?=$value["com_id"]?></td>													
													<td><?=$value["com_libelle"]?></td>
													<td>
								<?php					$diplomes=get_competence_diplomes($value["com_id"]);													
														if(!empty($diplomes)){
															
															$txt_diplome="";
															
															foreach($diplomes as $donnee_dip){
																
																$ligne_diplome=$donnee_dip["dip_libelle"];
																if($donnee_dip["dco_interne"] AND $donnee_dip["dco_externe"]){
																	$ligne_diplome.= " pour tous les intervenants";																
																}elseif($donnee_dip["dco_interne"]){
																	$ligne_diplome.= " seulement pour les formateurs interne";																
																}elseif($donnee_dip["dco_externe"]){
																	$ligne_diplome.= " seulement pour les intervenants extérieurs";																
																}
																if($donnee_dip["dco_obligatoire"]){
																	$ligne_diplome="<b>" . $ligne_diplome . "</b>";
																}
																if($txt_diplome!=""){
																	$txt_diplome.="<br/>";
																}
																$txt_diplome.=$ligne_diplome;
															}
															echo($txt_diplome);
															
														}else{
															echo("aucun diplôme demandé");
														} ?>											
													</td>																										
													<td class="text-center" >
														<span data-toggle="modal" data-target="#formEdit" >
															<a href="competence.php?competence=<?=$value["com_id"]?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier" >
																<i class="fa fa-pencil"></i>
															</a>
														</span>
													</td>									
												</tr>	
								<?php		
											};
										}; ?>																			
									</tbody>
								</table>
							</div>
						</div>
					</div>
	
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="parametre.php" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
					<button class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-legende">
						<i class="fa fa-question" ></i>
					</button>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
		<?php		if($_SESSION['acces']['acc_profil']==13){ ?>
						<a href="import/sync_competence.php" class="btn btn-default btn-sm" >
							<i class="fa fa-refresh"></i>
							Importer
						</a>		
		<?php		} ?>	
				</div>
				<div class="col-xs-3 footer-right" >
					<a href="competence.php" class="btn btn-success btn-sm" >
						<i class="fa fa-plus" ></i>
						Nouvelle compétence
					</a>
				</div>
			</div>
		</footer>
		
		<div id="modal-legende" class="modal fade" role="dialog" data-show="true" >
			<div class="modal-dialog">			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title" id="titreModal" >Info</h4>
					</div>
					<div class="modal-body">
						<ul>
							<li>Les <b>diplômes en gras</b> sont obligatoire pour valider la compétence.</li>
						</ul>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
							<i class="fa fa-close" ></i>
							Fermer
						</button>					
					</div>
				</div>			
			</div>
		</div>
		<?php
		include "includes/footer_script.inc.php"; ?>	
	</body>
</html>
