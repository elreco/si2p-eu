<?php
	include "includes/controle_acces.inc.php";
	
	include_once("includes/connexion.php");
	include_once("includes/connexion_soc.php");

	include 'modeles/mod_parametre.php';
	include 'modeles/mod_utilisateur.php';
	include 'modeles/mod_acces.php';
	include 'modeles/mod_droit.php';

	// ENREGISTREMENT D'UN NOUVELLE INTERVENANT = LIGNE DE PLANNING

	// PERSONNE CONNECTE
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	$com_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$com_agence=$_SESSION['acces']["acc_agence"];	
	}
	$acc_profil=0;
	if(isset($_SESSION['acces']["acc_profil"])){
		$acc_profil=$_SESSION['acces']["acc_profil"];	
	}

	// TRAITEMENT DES PARAMETRES

	$erreur=0;

	$com_type=0;
	if(isset($_POST["com_type"])){
		$com_type=$_POST["com_type"];
	}

	if(!empty($_POST["indivi_utilisateur"])){
		$com_ref_1=$_POST["indivi_utilisateur"];
	}else{
		$com_ref_1=0;
	}
	// C'EST UNE EDITION
	$com_id = 0;
	if(!empty($_POST['com_id'])){
		$com_id = $_POST['com_id'];
	}
	$com_archive = 0;
	if(!empty($_POST['com_archive'])){
		$com_archive = 1;
	}else{
		$com_archive = 0;
	}
	
	if($erreur==0){
		switch($com_type){
			
			case 1:	
				// FORMATEUR INTERNE
				
				$com_ref_2=0;
				if(!empty($_POST["com_agence"])){
					$com_agence=$_POST["com_agence"];
				}
			
				if(!empty($_POST["utilisateur"])){
					
					// UTILISATEUR EXISTANT
					$com_ref_1=$_POST["utilisateur"];

					$sql="SELECT uti_id,uti_nom,uti_prenom,uti_agence FROM Utilisateurs WHERE uti_id=:utilisateur;";
					$req=$Conn->prepare($sql);
					$req->bindParam("utilisateur",$com_ref_1);
					$req->execute();
					$info_uti=$req->fetch();
					if(!empty($info_uti)){
						$com_label_1=$info_uti["uti_nom"];
						$com_label_2=$info_uti["uti_prenom"];
						$com_ref_1=$info_uti["uti_id"];
					}
				}elseif($com_archive==1 AND !empty($com_id)){
					$sql="SELECT com_label_1,com_label_2 FROM Commerciaux WHERE com_id=:com_id;";
					$req=$ConnSoc->prepare($sql);
					$req->bindParam("com_id",$com_id);
					$req->execute();
					$info_com=$req->fetch();
					if(!empty($info_com)){
						$com_label_1=$info_com["com_label_1"];
						$com_label_2=$info_com["com_label_2"];
						$com_ref_1=0;
					}
				}else{
					$erreur=1;
				}
				if($erreur==0){
					// UPDATE
					if(!empty($com_id)){
						$sql="UPDATE Commerciaux SET com_type=:com_type,com_ref_1=:com_ref_1,com_ref_2=0,com_agence=:com_agence,com_label_1=:com_label_1,com_label_2=:com_label_2, com_archive =:com_archive ";
						$sql.=" WHERE com_id = :com_id";
						$req = $ConnSoc->prepare($sql);
						$req->bindParam(":com_type",$com_type);
						$req->bindParam(":com_ref_1",$com_ref_1);
						$req->bindParam(":com_agence",$com_agence);
						$req->bindParam(":com_label_1",$com_label_1);
						$req->bindParam(":com_label_2",$com_label_2);
						$req->bindParam(":com_archive",$com_archive);
						$req->bindParam(":com_id",$com_id);						
						$req->execute();
					// INSERT
					}else{
						$sql="INSERT INTO Commerciaux (com_type,com_ref_1,com_ref_2,com_agence,com_label_1,com_label_2)";
						$sql.=" VALUE (:com_type, :com_ref_1, 0, :com_agence, :com_label_1, :com_label_2)";
						$req = $ConnSoc->prepare($sql);
						$req->bindParam(":com_type",$com_type);
						$req->bindParam(":com_ref_1",$com_ref_1);
						$req->bindParam(":com_agence",$com_agence);
						$req->bindParam(":com_label_1",$com_label_1);
						$req->bindParam(":com_label_2",$com_label_2);						
						$req->execute();
						$com=$ConnSoc->lastInsertId();
					}
				}
				break;
				
			case 2: 
				
				// REVENDEUR
				
				if(!empty($_POST["revendeur_agence"])){
					$com_agence=$_POST["revendeur_agence"];
				}
				if(!empty($com_id)){
					$sql="UPDATE Commerciaux SET com_type=:com_type,com_ref_1=0,com_ref_2=0,com_agence=:com_agence,com_label_1=:com_label_1,com_label_2=:com_label_2, com_ad1=:com_ad1,com_ad2=:com_ad2,com_ad3=:com_ad3,com_cp=:com_cp,com_ville=:com_ville,com_tel=:com_tel,com_fax=:com_fax,com_mail=:com_mail,com_portable=:com_portable,com_archive=:com_archive";
					$sql.=" WHERE com_id = :com_id";
					$req = $ConnSoc->prepare($sql);
					$req->bindParam(":com_id",$com_id);
					$req->bindParam(":com_archive",$com_archive);
					$req->bindParam(":com_type",$com_type);
					$req->bindParam(":com_agence",$com_agence);
					$req->bindParam(":com_label_1",$_POST['revendeur_label_1']);
					$req->bindParam(":com_label_2",$_POST['revendeur_label_2']);
					$req->bindParam(":com_ad1",$_POST['revendeur_ad1']);
					$req->bindParam(":com_ad2",$_POST['revendeur_ad2']);
					$req->bindParam(":com_ad3",$_POST['revendeur_ad3']);
					$req->bindParam(":com_cp",$_POST['revendeur_cp']);
					$req->bindParam(":com_ville",$_POST['revendeur_ville']);
					$req->bindParam(":com_tel",$_POST['revendeur_tel']);
					$req->bindParam(":com_fax",$_POST['revendeur_fax']);
					$req->bindParam(":com_mail",$_POST['revendeur_mail']);
					$req->bindParam(":com_portable",$_POST['revendeur_portable']);
					$req->execute();
				}else{
					$sql="INSERT INTO Commerciaux (com_type,com_ref_1,com_ref_2,com_agence,com_label_1,com_label_2, com_ad1,com_ad2,com_ad3,com_cp,com_ville,com_tel,com_fax,com_mail,com_portable,com_archive)";
					$sql.=" VALUE (:com_type, 0, 0, :com_agence, :com_label_1, :com_label_2, :com_ad1,:com_ad2,:com_ad3,:com_cp,:com_ville,:com_tel,:com_fax,:com_mail,:com_portable,0)";
					$req = $ConnSoc->prepare($sql);
					$req->bindParam(":com_type",$com_type);
					$req->bindParam(":com_agence",$com_agence);
					$req->bindParam(":com_label_1",$_POST['revendeur_label_1']);
					$req->bindParam(":com_label_2",$_POST['revendeur_label_2']);
					$req->bindParam(":com_ad1",$_POST['revendeur_ad1']);
					$req->bindParam(":com_ad2",$_POST['revendeur_ad2']);
					$req->bindParam(":com_ad3",$_POST['revendeur_ad3']);
					$req->bindParam(":com_cp",$_POST['revendeur_cp']);
					$req->bindParam(":com_ville",$_POST['revendeur_ville']);
					$req->bindParam(":com_tel",$_POST['revendeur_tel']);
					$req->bindParam(":com_fax",$_POST['revendeur_fax']);
					$req->bindParam(":com_mail",$_POST['revendeur_mail']);
					$req->bindParam(":com_portable",$_POST['revendeur_portable']);
					$req->execute();

					$com=$ConnSoc->lastInsertId();
				}
				

				break;
				
			case 3: 
				// INDIVI
				
				
				if(!empty($_POST["indivi_agence"])){
					$com_agence=$_POST["indivi_agence"];
				}
				if(!empty($com_id)){
					$sql="UPDATE Commerciaux SET com_type=:com_type,com_ref_1=:com_ref_1,com_ref_2=0,com_agence=:com_agence,com_label_1=:com_label_1,com_label_2=:com_label_2, com_ad1=:com_ad1,com_ad2=:com_ad2,com_ad3=:com_ad3,com_cp=:com_cp,com_ville=:com_ville,com_tel=:com_tel,com_fax=:com_fax,com_mail=:com_mail,com_portable=:com_portable,com_archive=:com_archive";
					$sql.=" WHERE com_id = :com_id";
					$req = $ConnSoc->prepare($sql);
					$req->bindParam(":com_id",$com_id);
					$req->bindParam(":com_archive",$com_archive);
					$req->bindParam(":com_type",$com_type);
					$req->bindParam(":com_agence",$com_agence);
					$req->bindParam(":com_ref_1",$com_ref_1);
					$req->bindParam(":com_label_1",$_POST['indivi_label_1']);
					$req->bindParam(":com_label_2",$_POST['indivi_label_2']);
					$req->bindParam(":com_ad1",$_POST['indivi_ad1']);
					$req->bindParam(":com_ad2",$_POST['indivi_ad2']);
					$req->bindParam(":com_ad3",$_POST['indivi_ad3']);
					$req->bindParam(":com_cp",$_POST['indivi_cp']);
					$req->bindParam(":com_ville",$_POST['indivi_ville']);
					$req->bindParam(":com_tel",$_POST['indivi_tel']);
					$req->bindParam(":com_fax",$_POST['indivi_fax']);
					$req->bindParam(":com_mail",$_POST['indivi_mail']);
					$req->bindParam(":com_portable",$_POST['indivi_portable']);
					$req->execute();
				}else{
					$sql="INSERT INTO Commerciaux (com_type,com_ref_1,com_ref_2,com_agence,com_label_1,com_label_2, com_ad1,com_ad2,com_ad3,com_cp,com_ville,com_tel,com_fax,com_mail,com_portable,com_archive)";
					$sql.=" VALUE (:com_type, :com_ref_1, 0, :com_agence, :com_label_1, :com_label_2, :com_ad1,:com_ad2,:com_ad3,:com_cp,:com_ville,:com_tel,:com_fax,:com_mail,:com_portable,0)";
					$req = $ConnSoc->prepare($sql);
					$req->bindParam(":com_type",$com_type);
					$req->bindParam(":com_agence",$com_agence);
					$req->bindParam(":com_label_1",$_POST['indivi_label_1']);
					$req->bindParam(":com_label_2",$_POST['indivi_label_2']);
					$req->bindParam(":com_ref_1",$com_ref_1);
					$req->bindParam(":com_ad1",$_POST['indivi_ad1']);
					$req->bindParam(":com_ad2",$_POST['indivi_ad2']);
					$req->bindParam(":com_ad3",$_POST['indivi_ad3']);
					$req->bindParam(":com_cp",$_POST['indivi_cp']);
					$req->bindParam(":com_ville",$_POST['indivi_ville']);
					$req->bindParam(":com_tel",$_POST['indivi_tel']);
					$req->bindParam(":com_fax",$_POST['indivi_fax']);
					$req->bindParam(":com_mail",$_POST['indivi_mail']);
					$req->bindParam(":com_portable",$_POST['indivi_portable']);
					$req->execute();
					$com=$ConnSoc->lastInsertId();
				}
				
				
				break;
			case "0":
		
				// AUTRE
				
				
				if(!empty($_POST["autre_agence"])){
					$com_agence=$_POST["autre_agence"];
				}
				if(!empty($com_id)){
					$sql="UPDATE Commerciaux SET com_type=:com_type,com_ref_1=0,com_ref_2=0,com_agence=:com_agence,com_label_1=:com_label_1,com_label_2=:com_label_2, com_ad1=:com_ad1,com_ad2=:com_ad2,com_ad3=:com_ad3,com_cp=:com_cp,com_ville=:com_ville,com_tel=:com_tel,com_fax=:com_fax,com_mail=:com_mail,com_portable=:com_portable,com_archive=:com_archive";
					$sql.=" WHERE com_id = :com_id";
					$req = $ConnSoc->prepare($sql);
					$req->bindParam(":com_id",$com_id);
					$req->bindParam(":com_archive",$com_archive);
					$req->bindParam(":com_type",$com_type);
					$req->bindParam(":com_agence",$com_agence);
					$req->bindParam(":com_label_1",$_POST['autre_label_1']);
					$req->bindParam(":com_label_2",$_POST['autre_label_2']);
					$req->bindParam(":com_ad1",$_POST['autre_ad1']);
					$req->bindParam(":com_ad2",$_POST['autre_ad2']);
					$req->bindParam(":com_ad3",$_POST['autre_ad3']);
					$req->bindParam(":com_cp",$_POST['autre_cp']);
					$req->bindParam(":com_ville",$_POST['autre_ville']);
					$req->bindParam(":com_tel",$_POST['autre_tel']);
					$req->bindParam(":com_fax",$_POST['autre_fax']);
					$req->bindParam(":com_mail",$_POST['autre_mail']);
					$req->bindParam(":com_portable",$_POST['autre_portable']);
					$req->execute();
				}else{
					$sql="INSERT INTO Commerciaux (com_type,com_ref_1,com_ref_2,com_agence,com_label_1,com_label_2, com_ad1,com_ad2,com_ad3,com_cp,com_ville,com_tel,com_fax,com_mail,com_portable,com_archive)";
					$sql.=" VALUE (:com_type, 0, 0, :com_agence, :com_label_1, :com_label_2, :com_ad1,:com_ad2,:com_ad3,:com_cp,:com_ville,:com_tel,:com_fax,:com_mail,:com_portable,0)";
					$req = $ConnSoc->prepare($sql);
					$req->bindParam(":com_type",$com_type);
					$req->bindParam(":com_agence",$com_agence);
					$req->bindParam(":com_label_1",$_POST['autre_label_1']);
					$req->bindParam(":com_label_2",$_POST['autre_label_2']);
					$req->bindParam(":com_ad1",$_POST['autre_ad1']);
					$req->bindParam(":com_ad2",$_POST['autre_ad2']);
					$req->bindParam(":com_ad3",$_POST['autre_ad3']);
					$req->bindParam(":com_cp",$_POST['autre_cp']);
					$req->bindParam(":com_ville",$_POST['autre_ville']);
					$req->bindParam(":com_tel",$_POST['autre_tel']);
					$req->bindParam(":com_fax",$_POST['autre_fax']);
					$req->bindParam(":com_mail",$_POST['autre_mail']);
					$req->bindParam(":com_portable",$_POST['autre_portable']);
					$req->execute();
					$com=$ConnSoc->lastInsertId();
				}
				break;
		}

		
		// FIN GESTION Type
		
		// ON AJOUTE LE COMMERCIAL
	

	}
	if(!empty($erreur)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => "Une erreur est survenue. Veuillez réessayer"
		);
		header('Location: commercial.php');
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Succès",
			"type" => "success",
			"message" => "Le commercial a été créé"
		);
		header('Location: commercial_liste.php');
	}
	
	
?>