<?php
	
	// SYNTHESE DES TEST CACES


	include "includes/controle_acces.inc.php";
	//include "modeles/mod_parametre.php";
	include "includes/connexion.php";
	include "includes/connexion_soc.php";

	if($_SESSION["acces"]["acc_ref"]!=1 OR ($_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']["acc_profil"]!=11 AND $_SESSION['acces']["acc_profil"]!=14 AND $_SESSION['acces']["acc_profil"]!=15 AND $_SESSION['acces']["acc_profil"]!=10) ){
        // SERVICE TECH, DAF, DG, RA et RE
        $erreur="Accès refusé!";
		echo($erreur);
		die();
	}
	
	
	// DONNEE FORM

	$erreur_txt="";
	if(!empty($_POST)){
		
		//print_r($_POST);
		
		$testeur=0;
		if(!empty($_POST["testeur"])){
			$testeur=intval($_POST["testeur"]);
		}
		
		$qualification=0;
		if(!empty($_POST["qualification"])){
			$qualification=intval($_POST["qualification"]);
		}
		
		$periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}
		
		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
		}
		
		$_SESSION["stat_tech"]=array(
			"testeur" => $testeur,
			"qualification" => $qualification,
			"periode_deb" => $_POST["periode_deb"],
			"periode_fin" => $_POST["periode_fin"],
		);

	}elseif (isset($_SESSION["stat_tech"])) {

		$testeur=0;
		if(!empty($_SESSION["stat_tech"]["testeur"])){
			$testeur=intval($_SESSION["stat_tech"]["testeur"]);
		}
		
		$qualification=0;
		if(!empty($_SESSION["stat_tech"]["qualification"])){
			$qualification=intval($_SESSION["stat_tech"]["qualification"]);
		}
		
		$periode_deb="";
		if(!empty($_SESSION["stat_tech"]["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_SESSION["stat_tech"]["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}
		
		$periode_fin="";
		if(!empty($_SESSION["stat_tech"]["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_SESSION["stat_tech"]["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
		}

	}else{
		$erreur_txt="Formulaire incomplet!";
	}
	if(empty($periode_deb) OR empty($periode_fin)){
		$erreur_txt="Formulaire incomplet!";
	}

	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : stat_technique.php");
		die();
	}
	
	
	// LE PERSONNE CONNECTE
	
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]); 
	}
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]); 
	}
	
	// TITRE 
	
	$titre="";
	
	if(!empty($qualification)){
		$sql="SELECT qua_libelle FROM Qualifications WHERE qua_id=" . $qualification . ";";
		$req=$Conn->query($sql);
		$d_qualification=$req->fetch();
		if(!empty($d_qualification)){
			$titre.=" '" . $d_qualification["qua_libelle"] . "'";
		}
	}
	
	
	
	
	if(!empty($testeur)){
		
		$sql="SELECT int_label_1,int_label_2 FROM Intervenants WHERE int_id=" . $testeur . ";";
		$req=$ConnSoc->query($sql);
		$d_intervenant=$req->fetch();
		if(!empty($d_intervenant)){
			$titre.=" réalisés par " . $d_intervenant["int_label_1"] . " " . $d_intervenant["int_label_2"];
		}
	}
	
	//PAS de notion de societe / agence sur les diplomes UN CACES peut-etre multi soc
	// On liste les testeur de la societe pour identifier les tests a comptabilisé
	
	$sql="SELECT int_id,int_label_1,int_label_2 FROM Intervenants WHERE NOT int_type=0";
	if(!empty($acc_agence)){
		$sql.=" AND int_agence=" . $acc_agence;
	}
	$sql.=" ORDER BY int_label_1,int_label_2";
	$req=$ConnSoc->query($sql);
	$d_intervenants=$req->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($d_intervenants)){
		
		$tab_int=array_column ($d_intervenants,"int_id");
		$liste_int=implode($tab_int,",");
		
	}else{
		
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => "Impossible de générer la statistique!"
		);
		header("location : stat_technique.php");
		die();
	}
	
	if(empty($titre)){
		$titre="Tests réalisés";
	}else{
		$titre="Tests " . $titre . "<br/>";
	}
	$titre.=" entre le " . $DT_periode_deb->format("d/m/Y") . " et le " . $DT_periode_fin->format("d/m/Y") ;
	
	
	
	// LES TEST CACES
	
	$stat=array();
	$stat[0]=array(
		"valide" => 0,
		"echec" => 0,
		"total" => 0
	);
	
	$sql="SELECT qua_libelle,qca_id,qca_libelle,dcc_valide,COUNT(dcc_diplome) AS nb_categorie 
	FROM qualifications,qualifications_categories,diplomes_caces_cat,diplomes_caces
	WHERE qca_qualification=qua_id AND qca_id=dcc_categorie AND dcc_diplome=dca_id
	AND dca_diplome=2 AND dcc_date>='" . $periode_deb . "' AND dcc_date<='" . $periode_fin . "'";
	$sql.=" AND (
		(dca_action_soc_1=" . $acc_societe . " AND dcc_passage=1)
		OR (dca_action_soc_2=" . $acc_societe . " AND dcc_passage=2)
		OR (dca_action_soc_3=" . $acc_societe . " AND dcc_passage=3)
		OR (dca_action_soc_4=" . $acc_societe . " AND dcc_passage=4)
		OR (dca_action_soc_5=" . $acc_societe . " AND dcc_passage=5)
		OR (dca_action_soc_6=" . $acc_societe . " AND dcc_passage=6)
	)";
	if($testeur>0){
		$sql.=" AND dcc_testeur=" . $testeur;
	}else{
		
		$sql.=" AND dcc_testeur IN (" . $liste_int . ")";
	}
	if($qualification>0){
		$sql.=" AND qua_id=" . $qualification;
	}
	
	$sql.=" GROUP BY qca_id,dcc_valide ORDER BY qua_libelle,qca_libelle";
	$req=$Conn->query($sql);
	$d_categories=$req->fetchAll();
	if(!empty($d_categories)){
		foreach($d_categories as $cat){
			
			if(empty($stat[$cat["qca_id"]])){
				$stat[$cat["qca_id"]]=array(
					"qualification" => $cat["qua_libelle"],
					"categorie" => $cat["qca_libelle"],
					"valide" => 0,
					"echec" => 0,
					"total" => 0
				);
			}
			
			if(empty($cat["dcc_valide"])){
				$stat[$cat["qca_id"]]["echec"]=$stat[$cat["qca_id"]]["echec"] + $cat["nb_categorie"];
				$stat[0]["echec"]=$stat[0]["echec"] + $cat["nb_categorie"];
				
			}else{
				$stat[$cat["qca_id"]]["valide"]=$stat[$cat["qca_id"]]["valide"] + $cat["nb_categorie"];
				$stat[0]["valide"]=$stat[0]["valide"] + $cat["nb_categorie"];
			}
			$stat[$cat["qca_id"]]["total"]=$stat[$cat["qca_id"]]["total"] + $cat["nb_categorie"];
			$stat[0]["total"]=$stat[0]["total"] + $cat["nb_categorie"];
		}
		
	}
	
	
	
	/*echo("<pre>");
		print_r($cle_x);
	echo("</pre>");
	die();*/
	
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<!-- PERSO -->	
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
		
		<div id="main" >
<?php		
			if($_SESSION["acces"]["acc_ref"]==3){
				include "includes/header_sta.inc.php"; 
			}elseif($_SESSION["acces"]["acc_ref"]==2){
				include "includes/header_cli.inc.php"; 
			}else{
				include "includes/header_def.inc.php"; 
			} ?>			
			<section id="content_wrapper" >					
				<!-- Begin: Content -->
				<section id="content" class="animated fadeIn" >
				
			<?php	if(!empty($stat)){ ?>
			
						<div id="page_print" >
							<h1 class="text-center" >
								<?=$titre?>
							</h1>
							
							<div class="row" >
								<div class="col-md-8 col-md-offset-2" >
							
									<div class="panel mt10 panel-info" > 
										<div class="panel-heading panel-head-sm" >
											<span class="panel-title">Recherche rapide.</span>									
										</div>
										<div class="panel-body p5" >

											<form method="post" action="stat_tech_caces_test.php" >
												<div>
													<input type="hidden" name="qualification" value="<?=$qualification?>" />
													<input type="hidden" name="periode_deb" value="<?=$DT_periode_deb->format("d/m/Y")?>" />
													<input type="hidden" name="periode_fin" value="<?=$DT_periode_fin->format("d/m/Y")?>" />													
												</div>
												<div class="admin-form" >
													<div class="row mt15" >
														<div class="col-md-10" >
															<label for="caces_testeur" >Testeur</label>
															<select class="select2" id="caces_testeur" name="testeur" >
																<option value='0' >Testeur ...</option>
														<?php	foreach($d_intervenants as $int){
																	if($int["int_id"]==$testeur){
																		echo("<option value='" . $int["int_id"] . "' selected >" . $int["int_label_1"] . " " . $int["int_label_2"] . "</option>");
																	}else{
																		echo("<option value='" . $int["int_id"] . "' >" . $int["int_label_1"] . " " . $int["int_label_2"] . "</option>");
																	}
																} ?>
														
															</select>
														</div>
														<div class="col-md-2 text-center mt20" >
															<button type="submit" class="btn btn-md btn-info" >
																Afficher
															</button>
														</div>
													</div>
												</div>
												
											</form>
										</div>
									</div>
								</div>
							</div>
									
							
							<div class="row" >
								<div class="col-md-12" >
								
									<div class="table-responsive">
										<table class="table table-striped table-hover" >
											<thead>
												<tr class="dark">
													<th>Qualification</th>
													<th>Catégorie</th>
													<th>Nb tests</th>
													<th>Nb tests validés</th>
													<th>Nb tests ajournés</th>							
												</tr>
											</thead>
											<tbody>
									<?php		foreach($stat as $k => $s){ 
													if($k>0){ ?>
														<tr>
															<td><?=$s["qualification"]?></td>
															<td><?=$s["categorie"]?></td>
															<td>
																<a href="stat_tech_caces_test_detail.php?categorie=<?=$k?>&statut=0" >
																	<?=$s["total"]?>
																</a>
															</td>
															<td>
																<a href="stat_tech_caces_test_detail.php?categorie=<?=$k?>&statut=1" >
																	<?=$s["valide"]?>
																</a>
															</td>
															<td>
																<a href="stat_tech_caces_test_detail.php?categorie=<?=$k?>&statut=2" >
																	<?=$s["echec"]?>
																</a>
															</td>
														</tr>
									<?php			}
												} ?>
											</tbody>
											<tfoot>
												<tr>
													<th colspan="2" class="text-right" >Total :</th>
													<td><a href="stat_tech_caces_test_detail.php?statut=0" ><?=$stat[0]["total"]?></a></td>
													<td><a href="stat_tech_caces_test_detail.php?statut=1" ><?=$stat[0]["valide"]?></a></td>
													<td><a href="stat_tech_caces_test_detail.php?statut=2" ><?=$stat[0]["echec"]?></a></td>
												</tr>
											</tfoot>
										</table>							
									</div>
									
								</div>
							</div>
						</div>
		<?php		}else{ ?>
		
						<p class="alert alert-warning" >
							Pas de résultat
						</p>
						
		<?php		} ?>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="stat_technique.php" class="btn btn-sm btn-default"  >
							<i class="fa fa-left-arrow" ></i>Retour
						<a>					
					</div>
					<div class="col-xs-6 footer-middle text-center" style=""></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function () {
				
			
				
			});
			(jQuery);
		</script>
	</body>
</html>
