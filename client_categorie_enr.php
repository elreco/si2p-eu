<?php
include "includes/controle_acces.inc.php";
include("../includes/connexion.php");
require("modeles/mod_client.php");

if(!empty($_POST)){
	if(isset($_POST['cca_id'])){
		update_client_categorie($_POST['cca_id'], $_POST['cca_libelle']);
		header('Location: /client_categorie_liste.php');
	}else{
		insert_client_categorie($_POST['cca_libelle']);
		header('Location: /client_categorie_liste.php');
	}

}
?>
