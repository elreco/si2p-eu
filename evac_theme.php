<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'modeles/mod_parametre.php';
if (isset($_GET['id'])) {
    $req=$Conn->query("SELECT * FROM evacuations_themes WHERE eth_id =" . $_GET['id']);
	$theme=$req->fetch();
}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Theme</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
<form action="evac_theme_enr.php" method="POST" id="admin-form">
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">

            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-left">

                                        <div class="content-header">
                                            <h2>Ajouter un <b class="text-primary">thème</b></h2>

                                        </div>

                                        <div class="col-md-10 col-md-offset-1">


                                                <?php if (isset($_GET['id'])): ?>
                                                    <input type="hidden" name="eth_id" value="<?=$theme['eth_id']?>"></input>
                                                <?php endif;?>
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="section">
                                                      <div class="field prepend-icon">
                                                        <input type="text" name="eth_theme" class="gui-input" id="eth_theme" placeholder="Nom du thème"
                                                        <?php if(isset($_GET['id'])): ?>
                                                            value="<?=$theme['eth_theme']?>"
                                                        <?php endif; ?>>
                                                        <label for="eth_theme" class="field-icon">
                                                          <i class="fa fa-book"></i>
                                                      </label>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-md-12">
                                                <div class="section">
                                                    <textarea name="eth_libelle" class="form-control" id="eth_libelle" placeholder="Détail" rows="3" cols="50"><?php if(isset($_GET['id'])): ?><?=str_replace('<br />', '', $theme['eth_libelle'])?><?php endif; ?></textarea>
                                                </div>
                                          </div>
                                      </div>

                                  </div>
                              </div>


                          </div>
                      </div>

                  </div>
              </div>

          </div>
      </section>
      <!-- End: Content -->
  </section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="evac_theme_liste.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-save'></i> Enregistrer
            </button>
        </div>
    </div>
</footer>
</form>
<?php
	include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>

</body>
</html>
