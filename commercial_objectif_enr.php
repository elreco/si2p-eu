<?php
$menu_actif = 1;
include "includes/controle_acces.inc.php";



$acc_utilisateur=0;
if(isset($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
	}
}

if ($_SESSION['acces']["acc_profil"]!=10 AND $acc_utilisateur!=98 AND $acc_utilisateur!=403){
	header("location : deconnect.php");
	die();
}

include_once("includes/connexion.php");;
include_once("includes/connexion_soc.php");

// PARAMETRE


if(isset($_POST["commercial"])){
	if(!empty($_POST["commercial"])){
		$commercial=intval($_POST["commercial"]);
	}
}
if(isset($_POST["exercice"])){
	if(!empty($_POST["exercice"])){
		$exercice=intval($_POST["exercice"]);
	}
}
if($commercial==0 OR $exercice==0){
	echo("Impossible d'afficher la page!");
	die();
}

$cob_valide=0;
if(!empty($_POST["cob_valide"])){
	$cob_valide=1;
}

// DROIT 
$drt_validation=false;
if($acc_utilisateur==98 OR $acc_utilisateur==403){
	$drt_validation=true;	
}

// PREPARATION DES REQUETES

// verif si obj existe deja
$sql_get_obj="SELECT cob_commercial,cob_valide,cob_valide_utilisateur,cob_valide_date FROM Commerciaux_Objectifs 
WHERE cob_commercial=" . $commercial . " AND cob_exercice=" . $exercice . " AND cob_categorie=:cob_categorie
AND cob_famille=:cob_famille AND cob_sous_famille=:cob_sous_famille AND cob_sous_sous_famille=:cob_sous_sous_famille;";
$req_get_obj=$ConnSoc->prepare($sql_get_obj);

// ajout obj
$sql_add_obj="INSERT INTO Commerciaux_Objectifs (
cob_commercial,cob_exercice,cob_categorie,cob_famille,cob_sous_famille,cob_sous_sous_famille,
cob_objectif_1,cob_objectif_2,cob_objectif_3,cob_objectif_4,cob_objectif_5,cob_objectif_6,cob_objectif_7,cob_objectif_8,
cob_objectif_9,cob_objectif_10,cob_objectif_11,cob_objectif_12,cob_objectif_total,
cob_valide,cob_valide_utilisateur,cob_valide_date,cob_autre
) VALUES (" . $commercial . "," . $exercice . ",:cob_categorie,:cob_famille,:cob_sous_famille,:cob_sous_sous_famille,
:cob_objectif_1,:cob_objectif_2,:cob_objectif_3,:cob_objectif_4,:cob_objectif_5,:cob_objectif_6,:cob_objectif_7,:cob_objectif_8,
:cob_objectif_9,:cob_objectif_10,:cob_objectif_11,:cob_objectif_12,:cob_objectif_total,
:cob_valide,:cob_valide_utilisateur,:cob_valide_date,:cob_autre
);";
$req_add_obj=$ConnSoc->prepare($sql_add_obj);

// update obj
$sql_up_obj="UPDATE Commerciaux_Objectifs SET
cob_objectif_1=:cob_objectif_1,cob_objectif_2=:cob_objectif_2,cob_objectif_3=:cob_objectif_3,cob_objectif_4=:cob_objectif_4,
cob_objectif_5=:cob_objectif_5,cob_objectif_6=:cob_objectif_6,cob_objectif_7=:cob_objectif_7,cob_objectif_8=:cob_objectif_8,
cob_objectif_9=:cob_objectif_9,cob_objectif_10=:cob_objectif_10,cob_objectif_11=:cob_objectif_11,cob_objectif_12=:cob_objectif_12,
cob_objectif_total=:cob_objectif_total,cob_valide=:cob_valide,cob_valide_utilisateur=:cob_valide_utilisateur,
cob_valide_date=:cob_valide_date,cob_autre=:cob_autre
WHERE cob_commercial=" . $commercial . " AND cob_exercice=" . $exercice . " AND cob_categorie=:cob_categorie
AND cob_famille=:cob_famille AND cob_sous_famille=:cob_sous_famille AND cob_sous_sous_famille=:cob_sous_sous_famille;";
$req_up_obj=$ConnSoc->prepare($sql_up_obj);

// DELETE OBJECTIF
$sql_del_obj="DELETE FROM Commerciaux_Objectifs WHERE cob_commercial=" . $commercial . " AND cob_exercice=" . $exercice;
$sql_del_obj.=" AND cob_categorie=:cob_categorie AND 
cob_famille=:cob_famille AND cob_sous_famille=:cob_sous_famille AND cob_sous_sous_famille=:cob_sous_sous_famille;";
$req_del_obj=$ConnSoc->prepare($sql_del_obj);

// SECURITE
if(!$drt_validation){

	$sql_get_valide="SELECT cob_categorie FROM Commerciaux_Objectifs WHERE cob_commercial=" . $commercial . " AND cob_exercice=" . $exercice . " AND cob_valide;";
	$req_get_valide=$ConnSoc->query($sql_get_valide);
	$d_valide=$req_get_valide->fetchAll();
	if (!empty($d_valide)) {

		// tentative de maj d'objectif validés par une personne non habilité
		header("location : deconnect.php");
		die();
	}
	
}

// TRAITEMENT

$zero=0;
$un=1; 

// obj categorie
$sql="SELECT pca_id FROM Produits_Categories ORDER BY pca_id;";
$req=$Conn->query($sql);
$d_categories=$req->fetchAll();
if(!empty($d_categories)){
	foreach($d_categories as $categorie){
		
		if(!empty($_POST["cat_" . $categorie["pca_id"] . "_total"])){
			
			$obj=array(
				"1" => 0,
				"2" => 0,
				"3" => 0,
				"4" => 0,
				"5" => 0,
				"6" => 0,
				"7" => 0,
				"8" => 0,
				"9" => 0,
				"10" => 0,
				"11" => 0,
				"12" => 0,
				"total" => 0
			);
			
			$cob_autre=0;
			if(!empty($_POST["cat_autre_" . $categorie["pca_id"]])){
				$cob_autre=1;
			}
			
			for($i=1;$i<=12;$i++){
				if(!empty($_POST["cat_" . $categorie["pca_id"] . "_" . $i])){
					$obj[$i]=intval($_POST["cat_" . $categorie["pca_id"] . "_" . $i]);
					$obj["total"]+=intval($_POST["cat_" . $categorie["pca_id"] . "_" . $i]);				
				}
			}
			if($cob_autre AND empty($obj["total"])){
				$obj["total"]=intval($_POST["cat_" . $categorie["pca_id"] . "_total"]);	
			}

			if(!empty($obj["total"])){
				$req_get_obj->bindParam(":cob_categorie",$categorie["pca_id"]);
				$req_get_obj->bindParam(":cob_famille",$zero);
				$req_get_obj->bindParam(":cob_sous_famille",$zero);
				$req_get_obj->bindParam(":cob_sous_sous_famille",$zero);
				$req_get_obj->execute();
				$existe=$req_get_obj->fetch();
				if($existe){
					
					
					// MISE A JOUR
					$req_up_obj->bindParam(":cob_categorie",$categorie["pca_id"]);
					$req_up_obj->bindParam(":cob_famille",$zero);
					$req_up_obj->bindParam(":cob_sous_famille",$zero);
					$req_up_obj->bindParam(":cob_sous_sous_famille",$zero);
					$req_up_obj->bindParam(":cob_objectif_1",$obj["1"]);
					$req_up_obj->bindParam(":cob_objectif_2",$obj["2"]);
					$req_up_obj->bindParam(":cob_objectif_3",$obj["3"]);
					$req_up_obj->bindParam(":cob_objectif_4",$obj["4"]);
					$req_up_obj->bindParam(":cob_objectif_5",$obj["5"]);
					$req_up_obj->bindParam(":cob_objectif_6",$obj["6"]);
					$req_up_obj->bindParam(":cob_objectif_7",$obj["7"]);
					$req_up_obj->bindParam(":cob_objectif_8",$obj["8"]);
					$req_up_obj->bindParam(":cob_objectif_9",$obj["9"]);
					$req_up_obj->bindParam(":cob_objectif_10",$obj["10"]);
					$req_up_obj->bindParam(":cob_objectif_11",$obj["11"]);
					$req_up_obj->bindParam(":cob_objectif_12",$obj["12"]);
					$req_up_obj->bindParam(":cob_objectif_total",$obj["total"]);
					if(!$drt_validation OR $cob_valide==$existe["cob_valide"]){
						$req_up_obj->bindParam(":cob_valide",$existe["cob_valide"]);
						$req_up_obj->bindParam(":cob_valide_utilisateur",$existe["cob_valide_utilisateur"]);
						$req_up_obj->bindParam(":cob_valide_date",$existe["cob_valide_date"]);
					}else{
						if($cob_valide==1){
							$req_up_obj->bindParam(":cob_valide",$cob_valide);
							$req_up_obj->bindParam(":cob_valide_utilisateur",$acc_utilisateur);
							$req_up_obj->bindValue(":cob_valide_date",date("Y-m-d"));
						}else{
							$req_up_obj->bindParam(":cob_valide",$cob_valide);
							$req_up_obj->bindValue(":cob_valide_utilisateur",0);
							$req_up_obj->bindValue(":cob_valide_date",null);
						}
					}
					$req_up_obj->bindParam(":cob_autre",$cob_autre);
					$req_up_obj->execute();
				}else{
					$req_add_obj->bindParam(":cob_categorie",$categorie["pca_id"]);
					$req_add_obj->bindParam(":cob_famille",$zero);
					$req_add_obj->bindParam(":cob_sous_famille",$zero);
					$req_add_obj->bindParam(":cob_sous_sous_famille",$zero);
					$req_add_obj->bindParam(":cob_objectif_1",$obj["1"]);
					$req_add_obj->bindParam(":cob_objectif_2",$obj["2"]);
					$req_add_obj->bindParam(":cob_objectif_3",$obj["3"]);
					$req_add_obj->bindParam(":cob_objectif_4",$obj["4"]);
					$req_add_obj->bindParam(":cob_objectif_5",$obj["5"]);
					$req_add_obj->bindParam(":cob_objectif_6",$obj["6"]);
					$req_add_obj->bindParam(":cob_objectif_7",$obj["7"]);
					$req_add_obj->bindParam(":cob_objectif_8",$obj["8"]);
					$req_add_obj->bindParam(":cob_objectif_9",$obj["9"]);
					$req_add_obj->bindParam(":cob_objectif_10",$obj["10"]);
					$req_add_obj->bindParam(":cob_objectif_11",$obj["11"]);
					$req_add_obj->bindParam(":cob_objectif_12",$obj["12"]);
					$req_add_obj->bindParam(":cob_objectif_total",$obj["total"]);
					if(!$drt_validation OR $cob_valide==0){
						$req_add_obj->bindValue(":cob_valide",$cob_valide);
						$req_add_obj->bindValue(":cob_valide_utilisateur",0);
						$req_add_obj->bindValue(":cob_valide_date",null);
					}else{
						$req_add_obj->bindParam(":cob_valide",$cob_valide);
						$req_add_obj->bindParam(":cob_valide_utilisateur",$acc_utilisateur);
						$req_add_obj->bindValue(":cob_valide_date",date("Y-m-d"));					
					}
					$req_add_obj->bindParam(":cob_autre",$cob_autre);
					$req_add_obj->execute();
				}
				
			}
		}else{
			
			// DELETE OBJ
			
			$req_del_obj->bindParam(":cob_categorie",$categorie["pca_id"]);
			$req_del_obj->bindParam(":cob_famille",$zero);
			$req_del_obj->bindParam(":cob_sous_famille",$zero);
			$req_del_obj->bindParam(":cob_sous_sous_famille",$zero);
			$req_del_obj->execute();
			
		}
	}
}

// OBJECTIF SUR LES FAMILLES ...
$sql="SELECT * FROM Produits_Familles 
LEFT OUTER JOIN Produits_Sous_Familles ON (Produits_Familles.pfa_id=Produits_Sous_Familles.psf_pfa_id)
LEFT OUTER JOIN Produits_Sous_Sous_Familles ON (Produits_Sous_Familles.psf_id=Produits_Sous_Sous_Familles.pss_psf_id)
ORDER BY pfa_id,psf_id,pss_id;";
$req=$Conn->query($sql);
$d_familles=$req->fetchAll();
if(!empty($d_familles)){
	$fam_id=0;
	$s_fam_id=0;
	$s_s_fam_id=0;
	foreach($d_familles as $famille){
		
		if($famille["pfa_id"]!=$fam_id){	
		
			// OBJECTIF FAMILLE
			$fam_id=$famille["pfa_id"];
			
			if(!empty($_POST["fam_" . $fam_id . "_total"])){
			
				$obj=array(
					"1" => 0,
					"2" => 0,
					"3" => 0,
					"4" => 0,
					"5" => 0,
					"6" => 0,
					"7" => 0,
					"8" => 0,
					"9" => 0,
					"10" => 0,
					"11" => 0,
					"12" => 0,
					"total" => 0
				);
				
				$cob_autre=0;
				if(!empty($_POST["fam_autre_" . $fam_id])){
					$cob_autre=1;
				}
				
				for($i=1;$i<=12;$i++){
					if(!empty($_POST["fam_" . $fam_id . "_" . $i])){
						$obj[$i]=intval($_POST["fam_" . $fam_id . "_" . $i]);
						$obj["total"]+=intval($_POST["fam_" . $fam_id . "_" . $i]);				
					}
				}
				
				if($cob_autre AND empty($obj["total"])){
					$obj["total"]=intval($_POST["fam_" . $fam_id . "_total"]);	
				}
				
				
				
				if(!empty($obj["total"])){
					$req_get_obj->bindParam(":cob_categorie",$un);
					$req_get_obj->bindParam(":cob_famille",$fam_id);
					$req_get_obj->bindParam(":cob_sous_famille",$zero);
					$req_get_obj->bindParam(":cob_sous_sous_famille",$zero);
					$req_get_obj->execute();
					$existe=$req_get_obj->fetch();
					if($existe){
						// MISE A JOUR
						$req_up_obj->bindParam(":cob_categorie",$un);
						$req_up_obj->bindParam(":cob_famille",$fam_id);
						$req_up_obj->bindParam(":cob_sous_famille",$zero);
						$req_up_obj->bindParam(":cob_sous_sous_famille",$zero);
						$req_up_obj->bindParam(":cob_objectif_1",$obj["1"]);
						$req_up_obj->bindParam(":cob_objectif_2",$obj["2"]);
						$req_up_obj->bindParam(":cob_objectif_3",$obj["3"]);
						$req_up_obj->bindParam(":cob_objectif_4",$obj["4"]);
						$req_up_obj->bindParam(":cob_objectif_5",$obj["5"]);
						$req_up_obj->bindParam(":cob_objectif_6",$obj["6"]);
						$req_up_obj->bindParam(":cob_objectif_7",$obj["7"]);
						$req_up_obj->bindParam(":cob_objectif_8",$obj["8"]);
						$req_up_obj->bindParam(":cob_objectif_9",$obj["9"]);
						$req_up_obj->bindParam(":cob_objectif_10",$obj["10"]);
						$req_up_obj->bindParam(":cob_objectif_11",$obj["11"]);
						$req_up_obj->bindParam(":cob_objectif_12",$obj["12"]);
						$req_up_obj->bindParam(":cob_objectif_total",$obj["total"]);
						if(!$drt_validation OR $cob_valide==$existe["cob_valide"]){
							$req_up_obj->bindParam(":cob_valide",$existe["cob_valide"]);
							$req_up_obj->bindParam(":cob_valide_utilisateur",$existe["cob_valide_utilisateur"]);
							$req_up_obj->bindParam(":cob_valide_date",$existe["cob_valide_date"]);
						}else{
							if($cob_valide==1){
								$req_up_obj->bindParam(":cob_valide",$cob_valide);
								$req_up_obj->bindParam(":cob_valide_utilisateur",$acc_utilisateur);
								$req_up_obj->bindValue(":cob_valide_date",date("Y-m-d"));
							}else{
								$req_up_obj->bindParam(":cob_valide",$cob_valide);
								$req_up_obj->bindValue(":cob_valide_utilisateur",0);
								$req_up_obj->bindValue(":cob_valide_date",null);
							}
						}
						$req_up_obj->bindParam(":cob_autre",$cob_autre);
						$req_up_obj->execute();
					}else{
						$req_add_obj->bindParam(":cob_categorie",$un);
						$req_add_obj->bindParam(":cob_famille",$fam_id);
						$req_add_obj->bindParam(":cob_sous_famille",$zero);
						$req_add_obj->bindParam(":cob_sous_sous_famille",$zero);
						$req_add_obj->bindParam(":cob_objectif_1",$obj["1"]);
						$req_add_obj->bindParam(":cob_objectif_2",$obj["2"]);
						$req_add_obj->bindParam(":cob_objectif_3",$obj["3"]);
						$req_add_obj->bindParam(":cob_objectif_4",$obj["4"]);
						$req_add_obj->bindParam(":cob_objectif_5",$obj["5"]);
						$req_add_obj->bindParam(":cob_objectif_6",$obj["6"]);
						$req_add_obj->bindParam(":cob_objectif_7",$obj["7"]);
						$req_add_obj->bindParam(":cob_objectif_8",$obj["8"]);
						$req_add_obj->bindParam(":cob_objectif_9",$obj["9"]);
						$req_add_obj->bindParam(":cob_objectif_10",$obj["10"]);
						$req_add_obj->bindParam(":cob_objectif_11",$obj["11"]);
						$req_add_obj->bindParam(":cob_objectif_12",$obj["12"]);
						$req_add_obj->bindParam(":cob_objectif_total",$obj["total"]);
						if(!$drt_validation OR $cob_valide==0){
							$req_add_obj->bindValue(":cob_valide",$cob_valide);
							$req_add_obj->bindValue(":cob_valide_utilisateur",0);
							$req_add_obj->bindValue(":cob_valide_date",null);
						}else{
							$req_add_obj->bindParam(":cob_valide",$cob_valide);
							$req_add_obj->bindParam(":cob_valide_utilisateur",$acc_utilisateur);
							$req_add_obj->bindValue(":cob_valide_date",date("Y-m-d"));					
						}
						$req_add_obj->bindParam(":cob_autre",$cob_autre);
						$req_add_obj->execute();
					}
					
				}
			}else{
				
				// DELETE OBJ
				
				$req_del_obj->bindParam(":cob_categorie",$un);
				$req_del_obj->bindParam(":cob_famille",$fam_id);
				$req_del_obj->bindParam(":cob_sous_famille",$zero);
				$req_del_obj->bindParam(":cob_sous_sous_famille",$zero);
				$req_del_obj->execute();
				
			}
		}
		
		if(!empty($famille["psf_id"])){
			if($famille["psf_id"]!=$s_fam_id){
				// OBJECTIF SOUS-FAMILLE
				
				$s_fam_id=$famille["psf_id"];
				
				if(!empty($_POST["s_fam_" . $s_fam_id . "_total"])){
				
					$obj=array(
						"1" => 0,
						"2" => 0,
						"3" => 0,
						"4" => 0,
						"5" => 0,
						"6" => 0,
						"7" => 0,
						"8" => 0,
						"9" => 0,
						"10" => 0,
						"11" => 0,
						"12" => 0,
						"total" => 0
					);
					
					$cob_autre=0;
					if(!empty($_POST["s_fam_autre_" . $s_fam_id])){
						$cob_autre=1;
					}
					
					for($i=1;$i<=12;$i++){
						if(!empty($_POST["s_fam_" . $s_fam_id . "_" . $i])){
							$obj[$i]=intval($_POST["s_fam_" . $s_fam_id . "_" . $i]);
							$obj["total"]+=intval($_POST["s_fam_" . $s_fam_id . "_" . $i]);				
						}
					}
					if($cob_autre AND empty($obj["total"])){
						$obj["total"]=intval($_POST["s_fam_" . $s_fam_id . "_total"]);	
					}
					
					
					if(!empty($obj["total"])){
						$req_get_obj->bindParam(":cob_categorie",$un);
						$req_get_obj->bindParam(":cob_famille",$fam_id);
						$req_get_obj->bindParam(":cob_sous_famille",$s_fam_id);
						$req_get_obj->bindParam(":cob_sous_sous_famille",$zero);
						$req_get_obj->execute();
						$existe=$req_get_obj->fetch();
						if($existe){
							// MISE A JOUR
							$req_up_obj->bindParam(":cob_categorie",$un);
							$req_up_obj->bindParam(":cob_famille",$fam_id);
							$req_up_obj->bindParam(":cob_sous_famille",$s_fam_id);
							$req_up_obj->bindParam(":cob_sous_sous_famille",$zero);
							$req_up_obj->bindParam(":cob_objectif_1",$obj["1"]);
							$req_up_obj->bindParam(":cob_objectif_2",$obj["2"]);
							$req_up_obj->bindParam(":cob_objectif_3",$obj["3"]);
							$req_up_obj->bindParam(":cob_objectif_4",$obj["4"]);
							$req_up_obj->bindParam(":cob_objectif_5",$obj["5"]);
							$req_up_obj->bindParam(":cob_objectif_6",$obj["6"]);
							$req_up_obj->bindParam(":cob_objectif_7",$obj["7"]);
							$req_up_obj->bindParam(":cob_objectif_8",$obj["8"]);
							$req_up_obj->bindParam(":cob_objectif_9",$obj["9"]);
							$req_up_obj->bindParam(":cob_objectif_10",$obj["10"]);
							$req_up_obj->bindParam(":cob_objectif_11",$obj["11"]);
							$req_up_obj->bindParam(":cob_objectif_12",$obj["12"]);
							$req_up_obj->bindParam(":cob_objectif_total",$obj["total"]);
							if(!$drt_validation OR $cob_valide==$existe["cob_valide"]){
								$req_up_obj->bindParam(":cob_valide",$existe["cob_valide"]);
								$req_up_obj->bindParam(":cob_valide_utilisateur",$existe["cob_valide_utilisateur"]);
								$req_up_obj->bindParam(":cob_valide_date",$existe["cob_valide_date"]);
							}else{
								if($cob_valide==1){
									$req_up_obj->bindParam(":cob_valide",$cob_valide);
									$req_up_obj->bindParam(":cob_valide_utilisateur",$acc_utilisateur);
									$req_up_obj->bindValue(":cob_valide_date",date("Y-m-d"));
								}else{
									$req_up_obj->bindParam(":cob_valide",$cob_valide);
									$req_up_obj->bindValue(":cob_valide_utilisateur",0);
									$req_up_obj->bindValue(":cob_valide_date",null);
								}
							}
							$req_up_obj->bindParam(":cob_autre",$cob_autre);
							$req_up_obj->execute();
						}else{
							$req_add_obj->bindParam(":cob_categorie",$un);
							$req_add_obj->bindParam(":cob_famille",$fam_id);
							$req_add_obj->bindParam(":cob_sous_famille",$s_fam_id);
							$req_add_obj->bindParam(":cob_sous_sous_famille",$zero);
							$req_add_obj->bindParam(":cob_objectif_1",$obj["1"]);
							$req_add_obj->bindParam(":cob_objectif_2",$obj["2"]);
							$req_add_obj->bindParam(":cob_objectif_3",$obj["3"]);
							$req_add_obj->bindParam(":cob_objectif_4",$obj["4"]);
							$req_add_obj->bindParam(":cob_objectif_5",$obj["5"]);
							$req_add_obj->bindParam(":cob_objectif_6",$obj["6"]);
							$req_add_obj->bindParam(":cob_objectif_7",$obj["7"]);
							$req_add_obj->bindParam(":cob_objectif_8",$obj["8"]);
							$req_add_obj->bindParam(":cob_objectif_9",$obj["9"]);
							$req_add_obj->bindParam(":cob_objectif_10",$obj["10"]);
							$req_add_obj->bindParam(":cob_objectif_11",$obj["11"]);
							$req_add_obj->bindParam(":cob_objectif_12",$obj["12"]);
							$req_add_obj->bindParam(":cob_objectif_total",$obj["total"]);
							if(!$drt_validation OR $cob_valide==0){
								$req_add_obj->bindValue(":cob_valide",$cob_valide);
								$req_add_obj->bindValue(":cob_valide_utilisateur",0);
								$req_add_obj->bindValue(":cob_valide_date",null);
							}else{
								$req_add_obj->bindParam(":cob_valide",$cob_valide);
								$req_add_obj->bindParam(":cob_valide_utilisateur",$acc_utilisateur);
								$req_add_obj->bindValue(":cob_valide_date",date("Y-m-d"));					
							}
							$req_add_obj->bindParam(":cob_autre",$cob_autre);
							$req_add_obj->execute();
						}
						
					}
				}else{
					
					// DELETE OBJ
					
					$req_del_obj->bindParam(":cob_categorie",$un);
					$req_del_obj->bindParam(":cob_famille",$fam_id);
					$req_del_obj->bindParam(":cob_sous_famille",$s_fam_id);
					$req_del_obj->bindParam(":cob_sous_sous_famille",$zero);
					$req_del_obj->execute();
					
				}
				
			}
		}
		if(!empty($famille["pss_id"])){
			if($famille["pss_id"]!=$s_s_fam_id){
				// OBJECTIF SOUS-SOUS-FAMILLE
				
				$s_s_fam_id=$famille["pss_id"];
				
				if(!empty($_POST["s_s_fam_" . $s_s_fam_id . "_total"])){
				
					$obj=array(
						"1" => 0,
						"2" => 0,
						"3" => 0,
						"4" => 0,
						"5" => 0,
						"6" => 0,
						"7" => 0,
						"8" => 0,
						"9" => 0,
						"10" => 0,
						"11" => 0,
						"12" => 0,
						"total" => 0
					);
					
					$cob_autre=0;
					if(!empty($_POST["s_s_fam_autre_" . $s_fam_id])){
						$cob_autre=1;
					}
					for($i=1;$i<=12;$i++){
						if(!empty($_POST["s_s_fam_" . $s_s_fam_id . "_" . $i])){
							$obj[$i]=intval($_POST["s_s_fam_" . $s_s_fam_id . "_" . $i]);
							$obj["total"]+=intval($_POST["s_s_fam_" . $s_s_fam_id . "_" . $i]);				
						}
					}
					if($cob_autre AND empty($obj["total"])){
						$obj["total"]=intval($_POST["s_s_fam_" . $s_s_fam_id . "_total"]);	
					}
					
					
					
					if(!empty($obj["total"])){
						$req_get_obj->bindParam(":cob_categorie",$un);
						$req_get_obj->bindParam(":cob_famille",$fam_id);
						$req_get_obj->bindParam(":cob_sous_famille",$s_fam_id);
						$req_get_obj->bindParam(":cob_sous_sous_famille",$s_s_fam_id);
						$req_get_obj->execute();
						$existe=$req_get_obj->fetch();
						if($existe){
							// MISE A JOUR
							$req_up_obj->bindParam(":cob_categorie",$un);
							$req_up_obj->bindParam(":cob_famille",$fam_id);
							$req_up_obj->bindParam(":cob_sous_famille",$s_fam_id);
							$req_up_obj->bindParam(":cob_sous_sous_famille",$s_s_fam_id);
							$req_up_obj->bindParam(":cob_objectif_1",$obj["1"]);
							$req_up_obj->bindParam(":cob_objectif_2",$obj["2"]);
							$req_up_obj->bindParam(":cob_objectif_3",$obj["3"]);
							$req_up_obj->bindParam(":cob_objectif_4",$obj["4"]);
							$req_up_obj->bindParam(":cob_objectif_5",$obj["5"]);
							$req_up_obj->bindParam(":cob_objectif_6",$obj["6"]);
							$req_up_obj->bindParam(":cob_objectif_7",$obj["7"]);
							$req_up_obj->bindParam(":cob_objectif_8",$obj["8"]);
							$req_up_obj->bindParam(":cob_objectif_9",$obj["9"]);
							$req_up_obj->bindParam(":cob_objectif_10",$obj["10"]);
							$req_up_obj->bindParam(":cob_objectif_11",$obj["11"]);
							$req_up_obj->bindParam(":cob_objectif_12",$obj["12"]);
							$req_up_obj->bindParam(":cob_objectif_total",$obj["total"]);
							if(!$drt_validation OR $cob_valide==$existe["cob_valide"]){
								$req_up_obj->bindParam(":cob_valide",$existe["cob_valide"]);
								$req_up_obj->bindParam(":cob_valide_utilisateur",$existe["cob_valide_utilisateur"]);
								$req_up_obj->bindParam(":cob_valide_date",$existe["cob_valide_date"]);
							}else{
								if($cob_valide==1){
									$req_up_obj->bindParam(":cob_valide",$cob_valide);
									$req_up_obj->bindParam(":cob_valide_utilisateur",$acc_utilisateur);
									$req_up_obj->bindValue(":cob_valide_date",date("Y-m-d"));
								}else{
									$req_up_obj->bindParam(":cob_valide",$cob_valide);
									$req_up_obj->bindValue(":cob_valide_utilisateur",0);
									$req_up_obj->bindValue(":cob_valide_date",null);
								}
							}
							$req_up_obj->bindParam(":cob_autre",$cob_autre);
							$req_up_obj->execute();
						}else{
							$req_add_obj->bindParam(":cob_categorie",$un);
							$req_add_obj->bindParam(":cob_famille",$fam_id);
							$req_add_obj->bindParam(":cob_sous_famille",$s_fam_id);
							$req_add_obj->bindParam(":cob_sous_sous_famille",$s_s_fam_id);
							$req_add_obj->bindParam(":cob_objectif_1",$obj["1"]);
							$req_add_obj->bindParam(":cob_objectif_2",$obj["2"]);
							$req_add_obj->bindParam(":cob_objectif_3",$obj["3"]);
							$req_add_obj->bindParam(":cob_objectif_4",$obj["4"]);
							$req_add_obj->bindParam(":cob_objectif_5",$obj["5"]);
							$req_add_obj->bindParam(":cob_objectif_6",$obj["6"]);
							$req_add_obj->bindParam(":cob_objectif_7",$obj["7"]);
							$req_add_obj->bindParam(":cob_objectif_8",$obj["8"]);
							$req_add_obj->bindParam(":cob_objectif_9",$obj["9"]);
							$req_add_obj->bindParam(":cob_objectif_10",$obj["10"]);
							$req_add_obj->bindParam(":cob_objectif_11",$obj["11"]);
							$req_add_obj->bindParam(":cob_objectif_12",$obj["12"]);
							$req_add_obj->bindParam(":cob_objectif_total",$obj["total"]);
							if(!$drt_validation OR $cob_valide==0){
								$req_add_obj->bindValue(":cob_valide",$cob_valide);
								$req_add_obj->bindValue(":cob_valide_utilisateur",0);
								$req_add_obj->bindValue(":cob_valide_date",null);
							}else{
								$req_add_obj->bindParam(":cob_valide",$cob_valide);
								$req_add_obj->bindParam(":cob_valide_utilisateur",$acc_utilisateur);
								$req_add_obj->bindValue(":cob_valide_date",date("Y-m-d"));					
							}
							$req_add_obj->bindParam(":cob_autre",$cob_autre);
							$req_add_obj->execute();
						}
						
					}
				}else{
					
					// DELETE OBJ
					
					$req_del_obj->bindParam(":cob_categorie",$un);
					$req_del_obj->bindParam(":cob_famille",$fam_id);
					$req_del_obj->bindParam(":cob_sous_famille",$s_fam_id);
					$req_del_obj->bindParam(":cob_sous_sous_famille",$s_s_fam_id);
					$req_del_obj->execute();
					
				}
			}
		}
		
		
	}
}

// OBJECTIF AUTRE
if(!empty($_POST["cat_0_total"])){
			
	$obj=array(
		"1" => 0,
		"2" => 0,
		"3" => 0,
		"4" => 0,
		"5" => 0,
		"6" => 0,
		"7" => 0,
		"8" => 0,
		"9" => 0,
		"10" => 0,
		"11" => 0,
		"12" => 0,
		"total" => 0
	);
	
	$cob_autre=0;
	if(!empty($_POST["cat_autre_0"])){
		$cob_autre=1;
	}
	
	for($i=1;$i<=12;$i++){
		if(!empty($_POST["cat_0_" . $i])){
			$obj[$i]=intval($_POST["cat_0_" . $i]);
			$obj["total"]+=intval($_POST["cat_0_" . $i]);				
		}
	}
	if($cob_autre AND empty($obj["total"])){
		$obj["total"]=intval($_POST["cat_0_total"]);	
	}
	
	
	
	if(!empty($obj["total"])){
		$req_get_obj->bindParam(":cob_categorie",$zero);
		$req_get_obj->bindParam(":cob_famille",$zero);
		$req_get_obj->bindParam(":cob_sous_famille",$zero);
		$req_get_obj->bindParam(":cob_sous_sous_famille",$zero);
		$req_get_obj->execute();
		$existe=$req_get_obj->fetch();
		if($existe){
			// MISE A JOUR
			$req_up_obj->bindParam(":cob_categorie",$zero);
			$req_up_obj->bindParam(":cob_famille",$zero);
			$req_up_obj->bindParam(":cob_sous_famille",$zero);
			$req_up_obj->bindParam(":cob_sous_sous_famille",$zero);
			$req_up_obj->bindParam(":cob_objectif_1",$obj["1"]);
			$req_up_obj->bindParam(":cob_objectif_2",$obj["2"]);
			$req_up_obj->bindParam(":cob_objectif_3",$obj["3"]);
			$req_up_obj->bindParam(":cob_objectif_4",$obj["4"]);
			$req_up_obj->bindParam(":cob_objectif_5",$obj["5"]);
			$req_up_obj->bindParam(":cob_objectif_6",$obj["6"]);
			$req_up_obj->bindParam(":cob_objectif_7",$obj["7"]);
			$req_up_obj->bindParam(":cob_objectif_8",$obj["8"]);
			$req_up_obj->bindParam(":cob_objectif_9",$obj["9"]);
			$req_up_obj->bindParam(":cob_objectif_10",$obj["10"]);
			$req_up_obj->bindParam(":cob_objectif_11",$obj["11"]);
			$req_up_obj->bindParam(":cob_objectif_12",$obj["12"]);
			$req_up_obj->bindParam(":cob_objectif_total",$obj["total"]);
			if(!$drt_validation OR $cob_valide==$existe["cob_valide"]){
				$req_up_obj->bindParam(":cob_valide",$existe["cob_valide"]);
				$req_up_obj->bindParam(":cob_valide_utilisateur",$existe["cob_valide_utilisateur"]);
				$req_up_obj->bindParam(":cob_valide_date",$existe["cob_valide_date"]);
			}else{
				if($cob_valide==1){
					$req_up_obj->bindParam(":cob_valide",$cob_valide);
					$req_up_obj->bindParam(":cob_valide_utilisateur",$acc_utilisateur);
					$req_up_obj->bindValue(":cob_valide_date",date("Y-m-d"));
				}else{
					$req_up_obj->bindValue(":cob_valide",$cob_valide);
					$req_up_obj->bindValue(":cob_valide_utilisateur",0);
					$req_up_obj->bindValue(":cob_valide_date",null);
				}
			}
			$req_up_obj->bindParam(":cob_autre",$cob_autre);
			$req_up_obj->execute();
		}else{
			$req_add_obj->bindParam(":cob_categorie",$zero);
			$req_add_obj->bindParam(":cob_famille",$zero);
			$req_add_obj->bindParam(":cob_sous_famille",$zero);
			$req_add_obj->bindParam(":cob_sous_sous_famille",$zero);
			$req_add_obj->bindParam(":cob_objectif_1",$obj["1"]);
			$req_add_obj->bindParam(":cob_objectif_2",$obj["2"]);
			$req_add_obj->bindParam(":cob_objectif_3",$obj["3"]);
			$req_add_obj->bindParam(":cob_objectif_4",$obj["4"]);
			$req_add_obj->bindParam(":cob_objectif_5",$obj["5"]);
			$req_add_obj->bindParam(":cob_objectif_6",$obj["6"]);
			$req_add_obj->bindParam(":cob_objectif_7",$obj["7"]);
			$req_add_obj->bindParam(":cob_objectif_8",$obj["8"]);
			$req_add_obj->bindParam(":cob_objectif_9",$obj["9"]);
			$req_add_obj->bindParam(":cob_objectif_10",$obj["10"]);
			$req_add_obj->bindParam(":cob_objectif_11",$obj["11"]);
			$req_add_obj->bindParam(":cob_objectif_12",$obj["12"]);
			$req_add_obj->bindParam(":cob_objectif_total",$obj["total"]);
			if(!$drt_validation OR $cob_valide==0){
				$req_add_obj->bindValue(":cob_valide",$cob_valide);
				$req_add_obj->bindValue(":cob_valide_utilisateur",0);
				$req_add_obj->bindValue(":cob_valide_date",null);
			}else{
				$req_add_obj->bindParam(":cob_valide",$cob_valide);
				$req_add_obj->bindParam(":cob_valide_utilisateur",$acc_utilisateur);
				$req_add_obj->bindValue(":cob_valide_date",date("Y-m-d"));					
			}
			$req_add_obj->bindParam(":cob_autre",$cob_autre);
			$req_add_obj->execute();
		}
		
	}
}else{
	
	// DELETE OBJ
	
	$req_del_obj->bindParam(":cob_categorie",$zero);
	$req_del_obj->bindParam(":cob_famille",$zero);
	$req_del_obj->bindParam(":cob_sous_famille",$zero);
	$req_del_obj->bindParam(":cob_sous_sous_famille",$zero);
	$req_del_obj->execute();
	
}

header("location : commercial_result.php?exercice=" . $exercice . "&commercial=" . $commercial);

?>
