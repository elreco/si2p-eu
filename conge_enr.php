<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_add_notification.php');
include("modeles/mod_envoi_mail.php");

if(!empty($_POST)){

    $acc_agence=0;
    if(isset($_SESSION['acces']["acc_agence"])){
        $acc_agence=$_SESSION['acces']["acc_agence"];
    }
    $acc_societe=0;
    if(isset($_SESSION['acces']["acc_societe"])){
        $acc_societe=$_SESSION['acces']["acc_societe"];
    }
    if(!empty($_POST['con_demi_deb'])){
        $demi_deb = $_POST['con_demi_deb'];
    }else{
        $demi_deb = 0;
    }
    if(!empty($_POST['con_demi_fin'])){
        $demi_fin = $_POST['con_demi_fin'];
    }else{
        $demi_fin = 0;
    }
    $date_deb = convert_date_sql($_POST['con_date_deb']);
    $date_fin = convert_date_sql($_POST['con_date_fin']);

    if($date_deb > $date_fin){
        $_SESSION['message'][] = array(
            "titre" => "Erreur",
            "type" => "danger",
            "message" => "La date de début doit être inférieure à la date de fin"
        );
        if(!empty($_POST['con_id'])){
            Header("Location: conge.php?id=" . $_POST['id']);
        }else{
            Header("Location: conge.php");
        }
         die();
    }/* elseif($date_deb == $date_fin && $demi_fin == $demi_deb){
        $_SESSION['message'][] = array(
            "titre" => "Erreur",
            "type" => "danger",
            "message" => "La date de début doit être différente de la date de fin"
        );
        if(!empty($_POST['con_id'])){
            Header("Location: conge.php?id=" . $_POST['con_id']);
        }else{
         Header("Location: conge.php");
        }
         die();
    } */
    // CONTROLES
    // controle conges existants
    if(!empty($_POST['con_id'])){
        $sql="SELECT * FROM conges WHERE con_utilisateur = " . $_POST['con_utilisateur'] . " AND con_date_fin >= '" . $date_deb .  "' AND con_date_deb <= '" . $date_fin .  "' AND con_id !=" . $_POST['con_id'] . " AND con_annul = 0 AND con_societe = " . $acc_societe . " AND con_agence = " . $acc_agence;
    }else{
        $sql="SELECT * FROM conges WHERE con_utilisateur = " . $_POST['con_utilisateur'] . " AND con_date_fin >= '" . $date_deb .  "' AND con_date_deb <= '" . $date_fin .  "' AND con_annul = 0 AND con_societe = " . $acc_societe . " AND con_agence = " . $acc_agence;
    }
    $req = $Conn->query($sql);
    $conges_exist = $req->fetch();
    if(!empty($conges_exist)){
        $_SESSION['message'][] = array(
            "titre" => "Erreur",
            "type" => "danger",
            "message" => "Des congés sont déjà enregistrés sur cette période"
        );
        if(!empty($_POST['con_id'])){
            Header("Location: conge.php?id=" . $_POST['id']);
        }else{
        Header("Location: conge.php");
        }
        die();
    }

    // Chercher l'utilisateur

    $sql="SELECT * FROM utilisateurs WHERE uti_id = " . $_POST['con_utilisateur'];
    $req = $Conn->query($sql);
    $utilisateur = $req->fetch();

    $sql="SELECT * FROM utilisateurs WHERE uti_id = " . $_SESSION['acces']['acc_ref_id'];
    $req = $Conn->query($sql);
    $utilisateur_connecte = $req->fetch();
    // controle conges existant

    $sql="SELECT * FROM utilisateurs WHERE uti_id = " . $utilisateur['uti_responsable'];
    $req = $Conn->query($sql);
    $responsable = $req->fetch();

    // LE CE
    $sql="SELECT * FROM utilisateurs
    LEFT JOIN utilisateurs_societes ON (utilisateurs_societes.uso_utilisateur = utilisateurs.uti_id)
     WHERE uso_agence = " . $acc_agence . " AND uso_societe = " . $acc_societe . " AND uti_profil = 15 AND uti_archive = 0";
    $req = $Conn->query($sql);
    $chef_agence = $req->fetch();

    $sql="SELECT * FROM intervenants WHERE int_type=1 AND int_ref_1 = " . $_POST['con_utilisateur'] . " AND int_archive = 0";
    $req = $ConnSoc->query($sql);
    $intervenant = $req->fetch();
    if(!empty($intervenant)){
        if(!empty($_POST['con_id'])){
            $sql="SELECT * FROM plannings_dates WHERE pda_intervenant = " . $intervenant['int_id'] . " AND pda_date >= '" . $date_deb .  "' AND pda_date <= '" . $date_fin .  "' AND pda_ref_1 != " . $_POST['con_id'] . " AND pda_type !=0 AND pda_archive = 0";
        }else{
            $sql="SELECT * FROM plannings_dates WHERE pda_intervenant = " . $intervenant['int_id'] . " AND pda_date >= '" . $date_deb .  "' AND pda_date <= '" . $date_fin .  "' AND pda_type !=0 AND pda_archive = 0";
        }

        $req = $ConnSoc->query($sql);
        $planning_dates_exists = $req->fetch();
        /* echo("<pre>");
        var_dump($planning_dates_exists);
        echo("</pre>");
        die(); */
        if(!empty($planning_dates_exists)){
            $_SESSION['message'][] = array(
                "titre" => "Erreur",
                "type" => "danger",
                "message" => "Les dates ne sont pas disponibles"
            );
            if(!empty($_POST['con_id'])){
                Header("Location: conge.php?id=" . $_POST['con_id']);
            }else{
            Header("Location: conge.php");
            }
            die();
        }

    }

    // SELECTIONNER LE BON UTI VALIDE

    $sql="SELECT * FROM conges_categories WHERE cca_id = " . $_POST['con_categorie'];
    $req = $Conn->query($sql);
    $con_categorie = $req->fetch();

    if($con_categorie['cca_validation'] == 0){
        $con_uti_valide = 3;
        $con_uti_valide_date = date("Y-m-d");
        $pda_style_txt = "#ffffff";
    }else{
        $con_uti_valide = 0;
        $con_uti_valide_date = NULL;
        $pda_style_txt = "#000000";
    }
    if(!empty($_POST['con_id'])){
        $req = $Conn->prepare("UPDATE conges SET con_utilisateur = :con_utilisateur, con_uti_valide = :con_uti_valide, con_uti_valide_date=:con_uti_valide_date, con_responsable=:con_responsable, con_resp_nom=:con_resp_nom, con_resp_prenom=:con_resp_prenom,
        con_resp_valide=:con_resp_valide,
        con_resp_valide_date=:con_resp_valide_date, con_resp_po=:con_resp_po,con_resp_po_prenom=:con_resp_po_prenom,con_resp_po_nom=:con_resp_po_nom, con_resp_po_uti=:con_resp_po_uti, con_nom=:con_nom, con_prenom=:con_prenom, con_matricule=:con_matricule, con_categorie=:con_categorie,
        con_date_deb=:con_date_deb, con_demi_deb=:con_demi_deb, con_date_fin=:con_date_fin, con_demi_fin=:con_demi_fin, con_nb_jour=:con_nb_jour,
        con_comment=:con_comment WHERE con_id = :con_id");

        $req->bindValue(':con_id', $_POST['con_id']);
        $req->bindValue(':con_utilisateur', $_POST['con_utilisateur']);
        $req->bindValue(':con_uti_valide', $con_uti_valide);
        $req->bindValue(':con_uti_valide_date', $con_uti_valide_date);
        $req->bindValue(':con_responsable', $responsable['uti_id']);
        $req->bindValue(':con_resp_nom', $responsable['uti_nom']);
        $req->bindValue(':con_resp_prenom', $responsable['uti_prenom']);
        $req->bindValue(':con_resp_valide', 0);
        $req->bindValue(':con_resp_valide_date', $con_uti_valide_date);
        $req->bindValue(':con_resp_po', 0);
        $req->bindValue(':con_resp_po_uti', 0);
        $req->bindValue(':con_resp_po_nom', "");
        $req->bindValue(':con_resp_po_prenom', "");
        $req->bindValue(':con_nom', $utilisateur['uti_nom']);
        $req->bindValue(':con_prenom', $utilisateur['uti_prenom']);
        $req->bindValue(':con_matricule', $utilisateur['uti_matricule']);
        $req->bindValue(':con_categorie', $_POST['con_categorie']);
        $req->bindValue(':con_date_deb', $date_deb);
        $req->bindValue(':con_date_fin', $date_fin);
        $req->bindValue(':con_demi_deb', $demi_deb);
        $req->bindValue(':con_demi_fin', $demi_fin);
        $req->bindValue(':con_nb_jour', $_POST['con_nb_jour']);
        $req->bindValue(':con_comment', $_POST['con_comment']);
        $req->execute();
        $conge_id = $_POST['con_id'];
        if($_SESSION['acces']['acc_ref_id'] != $_POST['con_utilisateur']){
            if($con_categorie['cca_validation'] != 0){
                $req = $Conn->prepare("UPDATE conges SET con_uti_valide = 0, con_uti_valide_date=NULL WHERE con_id = :con_id");
                $req->bindValue(':con_id', $conge_id);
                $req->execute();
                // NOTIFICATION
                // vous devez revalider votre demande de congés
                add_notifications("<i class='fa fa-calendar'></i>","Votre demande de congés " . $conge_id . " doit être revalidée","bg-info","conges_voir.php?conge=" .  $conge_id,"","",$_POST['con_utilisateur'],0,0,0);
            }
        }
        $req = $ConnSoc->prepare("DELETE FROM plannings_dates
        WHERE pda_ref_1 = " . $conge_id);
        $req->execute();
        // CREER LA LIGNE DE PLANNING
        $begin = new DateTime($date_deb);
        $end   = new DateTime($date_fin);
        if(!empty($intervenant)){
                for($i = $begin; $i <= $end; $i->modify('+1 day')){
                    if($i->format("N") != 0 && $i->format("N") != 6){
                        $sql="SELECT * FROM plannings_dates  WHERE pda_intervenant = " . $intervenant['int_id'] . " AND pda_date = '" . $i->format("Y-m-d") . "' AND pda_demi = 1 AND pda_archive = 0";
                        $req = $ConnSoc->query($sql);
                        $pla_exists = $req->fetch();
                        if(empty($pla_exists)){
                            if($i->format("Y-m-d") != $date_deb OR $demi_deb != 2){
                                $req = $ConnSoc->prepare("INSERT INTO plannings_dates
                                (pda_intervenant, pda_date, pda_demi, pda_type, pda_ref_1, pda_texte, pda_categorie, pda_archive, pda_style_bg, pda_style_txt)
                                VALUES (:pda_intervenant, :pda_date, :pda_demi, :pda_type, :pda_ref_1, :pda_texte, :pda_categorie, :pda_archive, :pda_style_bg, :pda_style_txt)");
                                $req->bindValue(':pda_intervenant', $intervenant['int_id']);
                                $req->bindValue(':pda_date',  $i->format("Y-m-d"));
                                $req->bindValue(':pda_demi', 1);
                                $req->bindValue(':pda_type', 2);
                                $req->bindValue(':pda_ref_1', $conge_id);
                                $req->bindValue(':pda_texte', $con_categorie['cca_libelle']);
                                $req->bindValue(':pda_categorie', 1);
                                $req->bindValue(':pda_archive', 0);
                                $req->bindValue(':pda_style_bg', $con_categorie['cca_couleur_bg']);
                                $req->bindValue(':pda_style_txt', $pda_style_txt);
                                $req->execute();
                            }
                        }

                        if($i->format("Y-m-d") != $date_fin OR $demi_fin != 1){
                            $sql="SELECT * FROM plannings_dates  WHERE pda_intervenant = " . $intervenant['int_id'] . " AND pda_date = '" . $i->format("Y-m-d") . "' AND pda_demi = 2 AND pda_archive = 0";
                            $req = $ConnSoc->query($sql);
                            $pla_exists = $req->fetch();
                            if(empty($pla_exists)){
                                $req = $ConnSoc->prepare("INSERT INTO plannings_dates
                                (pda_intervenant, pda_date, pda_demi, pda_type, pda_ref_1, pda_texte, pda_categorie, pda_archive, pda_style_bg, pda_style_txt)
                                VALUES (:pda_intervenant, :pda_date, :pda_demi, :pda_type, :pda_ref_1, :pda_texte, :pda_categorie, :pda_archive, :pda_style_bg, :pda_style_txt)");
                                $req->bindValue(':pda_intervenant', $intervenant['int_id']);
                                $req->bindValue(':pda_date',  $i->format("Y-m-d"));
                                $req->bindValue(':pda_demi', 2);
                                $req->bindValue(':pda_type', 2);
                                $req->bindValue(':pda_ref_1', $conge_id);
                                $req->bindValue(':pda_texte', $con_categorie['cca_libelle']);
                                $req->bindValue(':pda_categorie', 1);
                                $req->bindValue(':pda_archive', 0);
                                $req->bindValue(':pda_style_bg', $con_categorie['cca_couleur_bg']);
                                $req->bindValue(':pda_style_txt', $pda_style_txt);
                                $req->execute();
                            }

                        }
                    }

            }

            if($con_uti_valide == 3){
                $req = $ConnSoc->prepare("UPDATE plannings_dates SET pda_confirme = 1 WHERE pda_ref_1 = :con_id");
                $req->bindValue(':con_id', $conge_id);
                $req->execute();
            }
        }

    }else{
        $req = $Conn->prepare("INSERT INTO conges
        (con_societe, con_agence, con_utilisateur, con_uti_valide, con_uti_valide_date, con_responsable, con_resp_nom, con_resp_prenom,
        con_resp_valide,
        con_resp_valide_date, con_resp_po,con_resp_po_prenom,con_resp_po_nom, con_resp_po_uti, con_nom, con_prenom, con_matricule, con_categorie,
        con_date_deb, con_demi_deb, con_date_fin, con_demi_fin, con_nb_jour,
        con_comment, con_creation_uti, con_creation_date)
        VALUES (:con_societe, :con_agence, :con_utilisateur, :con_uti_valide, :con_uti_valide_date, :con_responsable,
        :con_resp_nom, :con_resp_prenom, :con_resp_valide,
        :con_resp_valide_date, :con_resp_po,:con_resp_po_prenom,:con_resp_po_nom,
        :con_resp_po_uti, :con_nom, :con_prenom, :con_matricule, :con_categorie, :con_date_deb, :con_demi_deb,
        :con_date_fin, :con_demi_fin, :con_nb_jour,
        :con_comment, :con_creation_uti, NOW())");
        $req->bindValue(':con_societe', $acc_societe);
        $req->bindValue(':con_agence', $acc_agence);
        $req->bindValue(':con_utilisateur', $_POST['con_utilisateur']);
        $req->bindValue(':con_uti_valide', $con_uti_valide);
        $req->bindValue(':con_uti_valide_date', $con_uti_valide_date);
        $req->bindValue(':con_responsable', $responsable['uti_id']);
        $req->bindValue(':con_resp_nom', $responsable['uti_nom']);
        $req->bindValue(':con_resp_prenom', $responsable['uti_prenom']);
        $req->bindValue(':con_resp_valide', 0);
        $req->bindValue(':con_resp_valide_date', $con_uti_valide_date);
        $req->bindValue(':con_resp_po', 0);
        $req->bindValue(':con_resp_po_uti', 0);
        $req->bindValue(':con_resp_po_nom', "");
        $req->bindValue(':con_resp_po_prenom', "");
        $req->bindValue(':con_nom', $utilisateur['uti_nom']);
        $req->bindValue(':con_prenom', $utilisateur['uti_prenom']);
        $req->bindValue(':con_matricule', $utilisateur['uti_matricule']);
        $req->bindValue(':con_categorie', $_POST['con_categorie']);
        $req->bindValue(':con_date_deb', $date_deb);
        $req->bindValue(':con_date_fin', $date_fin);
        $req->bindValue(':con_demi_deb', $demi_deb);
        $req->bindValue(':con_demi_fin', $demi_fin);
        $req->bindValue(':con_nb_jour', floatval($_POST['con_nb_jour']));
        $req->bindValue(':con_comment', $_POST['con_comment']);
        $req->bindValue(':con_creation_uti', $_SESSION['acces']['acc_ref_id']);
        $req->execute();
        $conge_id = $Conn->lastInsertId();

        if($_POST['con_utilisateur'] != $_SESSION['acces']['acc_ref_id'] && $responsable['uti_id'] =! $_SESSION['acces']['acc_ref_id']){
            // C'est le responsable ou la rh
            add_notifications("<i class='fa fa-calendar'></i>","Vous devez valider la demande de congés " . $conge_id,"bg-info","conges_voir.php?conge=" .  $conge_id,"","",$_POST['con_utilisateur'],0,0,0);
        }elseif($_POST['con_utilisateur'] != $_SESSION['acces']['acc_ref_id']){
            // C'est le responsable ou la rh
            add_notifications("<i class='fa fa-calendar'></i>","Vous devez valider la demande de congés " . $conge_id,"bg-info","conges_voir.php?conge=" .  $conge_id,"","",$_POST['con_utilisateur'],0,0,0);
        }elseif($_POST['con_utilisateur'] == $_SESSION['acces']['acc_ref_id']){
            $req = $Conn->prepare("UPDATE conges SET con_uti_valide = 1, con_uti_valide_date=NOW() WHERE con_id = :con_id");
            $req->bindValue(':con_id', $conge_id);
            $req->execute();
        }
        // CREER LA LIGNE DE PLANNING
        $begin = new DateTime($date_deb);
        $end   = new DateTime($date_fin);
        if(!empty($intervenant)){
            for($i = $begin; $i <= $end; $i->modify('+1 day')){
                    if($i->format("N") != 0 && $i->format("N") != 6){

                        $sql="SELECT * FROM plannings_dates  WHERE pda_intervenant = " . $intervenant['int_id'] . " AND pda_date = '" . $i->format("Y-m-d") . "' AND pda_demi = 1 AND pda_archive = 0";
                        $req = $ConnSoc->query($sql);
                        $pla_exists = $req->fetch();
                        if(empty($pla_exists)){
                            if($i->format("Y-m-d") != $date_deb OR $demi_deb != 2){
                                $req = $ConnSoc->prepare("INSERT INTO plannings_dates
                                (pda_intervenant, pda_date, pda_demi, pda_type, pda_ref_1, pda_texte, pda_categorie, pda_archive, pda_style_bg, pda_style_txt)
                                VALUES (:pda_intervenant, :pda_date, :pda_demi, :pda_type, :pda_ref_1, :pda_texte, :pda_categorie, :pda_archive, :pda_style_bg, :pda_style_txt)");
                                $req->bindValue(':pda_intervenant', $intervenant['int_id']);
                                $req->bindValue(':pda_date',  $i->format("Y-m-d"));
                                $req->bindValue(':pda_demi', 1);
                                $req->bindValue(':pda_type', 2);
                                $req->bindValue(':pda_ref_1', $conge_id);
                                $req->bindValue(':pda_texte', $con_categorie['cca_libelle']);
                                $req->bindValue(':pda_categorie', 1);
                                $req->bindValue(':pda_archive', 0);
                                $req->bindValue(':pda_style_bg', $con_categorie['cca_couleur_bg']);
                                $req->bindValue(':pda_style_txt', $pda_style_txt);
                                $req->execute();
                            }
                        }
                        $sql="SELECT * FROM plannings_dates  WHERE pda_intervenant = " . $intervenant['int_id'] . " AND pda_date = '" . $i->format("Y-m-d") . "' AND pda_demi = 2 AND pda_archive = 0";
                            $req = $ConnSoc->query($sql);
                            $pla_exists = $req->fetch();
                            if(empty($pla_exists)){
                                if($i->format("Y-m-d") != $date_fin OR $demi_fin != 1){
                                    $req = $ConnSoc->prepare("INSERT INTO plannings_dates
                                    (pda_intervenant, pda_date, pda_demi, pda_type, pda_ref_1, pda_texte, pda_categorie, pda_archive, pda_style_bg, pda_style_txt)
                                    VALUES (:pda_intervenant, :pda_date, :pda_demi, :pda_type, :pda_ref_1, :pda_texte, :pda_categorie, :pda_archive, :pda_style_bg, :pda_style_txt)");
                                    $req->bindValue(':pda_intervenant', $intervenant['int_id']);
                                    $req->bindValue(':pda_date',  $i->format("Y-m-d"));
                                    $req->bindValue(':pda_demi', 2);
                                    $req->bindValue(':pda_type', 2);
                                    $req->bindValue(':pda_ref_1', $conge_id);
                                    $req->bindValue(':pda_texte', $con_categorie['cca_libelle']);
                                    $req->bindValue(':pda_categorie', 1);
                                    $req->bindValue(':pda_archive', 0);
                                    $req->bindValue(':pda_style_bg', $con_categorie['cca_couleur_bg']);
                                    $req->bindValue(':pda_style_txt', $pda_style_txt);
                                    $req->execute();
                                }
                            }

                    }

            }
            if($con_uti_valide == 3){
                $req = $ConnSoc->prepare("UPDATE plannings_dates SET pda_confirme = 1 WHERE pda_ref_1 = :con_id");
                $req->bindValue(':con_id', $conge_id);
                $req->execute();
            }
        }

    }
    // NOTIFICATIONS A AFFICHER
    $param_mail=array(
        "sujet" => "ORION : Demande de congés pour " . $utilisateur['uti_prenom'] . " " . $utilisateur['uti_nom'],
        "identite" => "ORION",
        "message" => $utilisateur_connecte['uti_prenom'] . " " . $utilisateur_connecte['uti_nom'] . " a fait une demande
        de congés de type " . $con_categorie['cca_libelle'] . " pour la période du " . convert_date_txt($date_deb) . " au " . convert_date_txt($date_fin) .
        "<br><br> <strong>Voici le commentaire :</strong> <br>" .
        $_POST['con_comment']
    );

    $adr_mail=array(
        "0" => array(
            "adresse" => $responsable['uti_mail'],
            "nom" => $responsable['uti_prenom'] . " " . $responsable['uti_nom']
        ),
    );
    $adr_mail=array(
        "1" => array(
            "adresse" => $utilisateur['uti_mail'],
            "nom" => $utilisateur['uti_prenom'] . " " . $utilisateur['uti_nom']
        ),
    );
    if(!empty($chef_agence)){
        $adr_mail["2"] = array(
            "adresse" => $chef_agence['uti_mail'],
            "nom" => $chef_agence['uti_prenom'] . " " . $chef_agence['uti_nom']
        );
    }

    $mail=envoi_mail("si2p",$param_mail,$adr_mail);

    $_SESSION['message'][] = array(
        "titre" => "Succès",
        "type" => "success",
        "message" => "Votre demande de congés est enregistrée"
    );
    Header("Location: conges_voir.php?conge=" . $conge_id);
    die();
}
