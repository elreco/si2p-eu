<?php

// ECRAN DE CREATION D'UN SUSPECT

include "includes/controle_acces.inc.php";
error_reporting( error_reporting() & ~E_NOTICE );

include_once("includes/connexion.php");
include_once("includes/connexion_soc.php");

require "modeles/mod_get_commerciaux.php";
require "modeles/mod_parametre.php";
require "modeles/mod_get_cli_classifications.php";
require "modeles/mod_get_activites_secteurs.php";

if(!$_SESSION["acces"]["acc_droits"][23]){
	header("location: deconnect.php");
	die();
}

// sur la personne connecte

$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=intval($_SESSION['acces']["acc_agence"]);
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);
}


// categorie accessible pour le suspect
$sql="SELECT cca_id,cca_libelle FROM Clients_Categories WHERE NOT cca_id=5";
if($acc_agence!=4 OR !$_SESSION['acces']["acc_droits"][8]){
	$sql.=" AND NOT cca_id=2";
}
$sql.=" ORDER BY cca_libelle";
$req = $Conn->query($sql);
$d_categories = $req->fetchAll();

// LA SOCIETE

$sql="SELECT soc_nom,soc_agence FROM Societes WHERE soc_id=" . $acc_societe . ";";
$req = $Conn->query($sql);
$d_societe=$req->fetch();

//agence si societe gere
if($d_societe["soc_agence"]==1 AND $acc_agence==0){
	$sql="SELECT age_id,age_nom FROM Agences WHERE age_societe=" . $acc_societe . ";";
	$req = $Conn->query($sql);
	$d_agences=$req->fetchAll();
}elseif($acc_agence>0){
	$sql="SELECT age_id,age_nom FROM Agences WHERE age_id=" . $acc_agence . ";";
	$req = $Conn->query($sql);
	$d_agence=$req->fetch();
}

// LES COMMERCIAUX
if(!isset($d_agences)){
	$sql="SELECT com_id,com_label_1,com_label_2 FROM commerciaux";
	$mil=" AND NOT com_archive";
	if($_SESSION['acces']['acc_profil'] == 3 && !$_SESSION['acces']["acc_droits"][6]){
		$mil.=" AND com_ref_1 = " . $_SESSION['acces']['acc_ref_id'];
	}
	if($acc_agence>0){
		$mil.=" AND com_agence= " . $acc_agence;
	}
	if($mil!=""){
		$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
	}
	$sql.=" ORDER BY com_label_1,com_label_2";
	$req = $ConnSoc->query($sql);
	$d_commerciaux=$req->fetchAll();
}else{
	$d_commerciaux=array();
}


// FONCTIONS DES CONTACTS

$sql="SELECT cfo_id,cfo_libelle FROM Contacts_Fonctions ORDER BY cfo_libelle;";
$req = $Conn->query($sql);
$d_contact_fonctions=$req->fetchAll();


// Classification

$d_classifications=get_cli_classifications();

// secteur

$d_secteurs=get_activites_secteurs();

// LES TYPES DE FINANCEURS
$sql="SELECT csc_id,csc_libelle FROM Clients_Sous_Categories WHERE csc_categorie=4 ORDER BY csc_libelle;";
$req = $Conn->query($sql);
$d_opca_type = $req->fetchAll();

// les sources (ancien prescripteur)

$sql="SELECT cpr_id,cpr_libelle,cpr_ouvert FROM Clients_Prescripteurs WHERE NOT cpr_archive AND NOT cpr_import ORDER BY cpr_libelle;";
$req = $Conn->query($sql);
$d_sources = $req->fetchAll();

// on écrase la recherche
unset($_SESSION['client_tableau']);

if(isset($_GET["menu_actif"])){
	unset($_SESSION['retour']);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion - Nouveau suspect</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

		<!-- Plugin -->
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<!-- Si2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

	</head>
	<body class="sb-top sb-top-sm ">

		<form method="post" action="suspect_enr.php" id="form_suspect" >
<?php		if(count($d_commerciaux)==1){ ?>
				<input type="hidden" name="sus_commercial" id="commercial" value="<?=$d_commerciaux[0]['com_id']?>" />
<?php 		} ?>
			<input type="hidden" name="prospect" id="prospect" value="0" />
			<div id="main">
  <?php			include "includes/header_def.inc.php";   ?>
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper" >
					<section id="content" class="animated fadeIn">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<!-- deb PANEL FORM -->
								<div class="admin-form theme-dark ">
									<div class="panel heading-border panel-dark">

										<div class="panel-body bg-light">


												<div class="content-header">
													<h2>Nouvelle <b class="text-dark">fiche</b></h2>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Informations générales</span>
														</div>
													</div>
												</div>

												<div class="row" id="suspect_ok" style="display:none;" >
													<div class="col-md-12 alert alert-success" ></div>
												</div>
												<div class="row mb20 verification">
													<div class="col-md-12 text-center">
														<label for="part_client" class=" mt15 option option-dark">
															<input type="radio" id="part_client" class="part_client" name="part_client" value="1" checked="">
															<span class="radio"></span> Client
														</label>
														<label for="part_client_1" class=" mt15 option option-dark">
															<input type="radio" id="part_client_1" class="part_client" name="part_client" value="2">
															<span class="radio"></span> Particulier
														</label>
													</div>
												</div>
												<div class="row verification-client particulier-show" >
													<div class="col-md-4">
														<label for="code" >Code : <span style="color:red" class="verification">*</span></label>
														<div class="field prepend-icon">
															<input type="text" name="sus_code" id="code" class="gui-input nom" maxlength="200" placeholder="Code"  />
															<span class="field-icon">
																<i class="fa fa-code"></i>
															</span>
														</div>
													</div>
													<div class="col-md-8">
														<label for="nom" >Nom : </label>
														<div class="field prepend-icon">
															<input type="text" name="sus_nom" id="nom" class="gui-input" maxlength="100" placeholder="Nom"  />
															<span class="field-icon">
																<i class="fa fa-building-o"></i>
															</span>
														</div>
													</div>
												</div>

												<div class="row mt15 verification">

													<div class="col-md-8 verification-client" >
														<label for="verif_siren" >Siren : <span style="color:red" class="verification">*</span></label>
														<div class="field prepend-icon">
															<input type="text" name="verif_siren" id="verif_siren" class="gui-input siren" placeholder="Siren" data-prefixe="verif_" >
															<span class="field-icon">
																<i class="fa fa-barcode"></i>
															</span>
														</div>
														<div id="verif_siren_err" class="text-danger" ></div>
													</div>
													<div class="col-md-4  verification-client" >
														<label for="verif_nic" >Nic : <span style="color:red" class="verification">*</span></label>
														<div class="field prepend-icon">
															<input type="text" name="verif_nic" id="verif_nic" class="gui-input siret" placeholder="Nic" disabled data-prefixe="verif_" />
															<span class="field-icon">
																<i class="fa fa-barcode"></i>
															</span>
														</div>
													</div>
												</div>

												<div class="row mt15 verification  verification-client">
													<div class="col-md-12" >
														<label for="verif_siret" >Siret : <span style="color:red" class="verification">*</span></label>
														<div class="field prepend-icon">
															<input type="text" name="verif_siret" id="verif_siret" class="gui-input siretlong" placeholder="Siret" data-prefixe="verif_" />
															<span class="field-icon">
																<i class="fa fa-barcode"></i>
															</span>
														</div>
													</div>
												</div>
												<!-- PARTICULIER  -->
												<div class="row verification-particulier-hide">
													<div class="col-md-2">
														<div class="section">
															<label for="con_nom_verif" >Civilité : </label>
															<div class="field select">
																<select class="con_civilite_verif">
																	<option value="0">Civilité...</option>
																	<option value="1">Monsieur</option>
																	<option value="2">Madame</option>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
													<div class="col-md-4">
														<div class="section">
															<label for="con_nom_verif" >Nom : <span style="color:red" class="verification">*</span></label>
															<div class="field prepend-icon">
																<input type="text" name="con_nom_verif" id="con_nom_verif" class="typing-particulier gui-input nom" placeholder="Nom" value="">
																<label for="con_nom" class="field-icon">
																	<i class="fa fa-user"></i>
																</label>
															</div>
														</div>
													</div>
													<div class="col-md-3">
														  <div class="section">
															  <label for="con_prenom" >Prénom : <span style="color:red" class="verification">*</span></label>
															<div class="field prepend-icon">
															  <input type="text" name="con_prenom_verif" id="con_prenom_verif" class="typing-particulier gui-input prenom" placeholder="Prénom" value="">
															  <label for="con_prenom" class="field-icon">
																<i class="fa fa-tag"></i>
															  </label>
															</div>
														  </div>
													</div>
													<div class="col-md-3" style="">
														 <label for="sus_stagiaire_naiss_verif" >Date de naissance : <span style="color:red" class="verification">*</span></label>
														<label for="sus_stagiaire_naiss" class="field prepend-icon">
															<input type="text" id="sus_stagiaire_naiss_verif" name="sus_stagiaire_naiss_verif" class="typing-particulier gui-input datepicker date hasDatepicker" placeholder="Date de naissance">
															<span class="field-icon">
																<i class="fa fa-calendar-o"></i>
															</span>
														</label>
													</div>



												</div>
												<!-- <div class="row verification-particulier">
													<div class="col-md-4">
														<label for="code" >Code :</label>
														<div class="field prepend-icon">
															<input type="text" readonly name="sus_code_particulier" id="code_particulier" class="gui-input nom" maxlength="20" placeholder="Code"  />
															<span class="field-icon">
																<i class="fa fa-code"></i>
															</span>
														</div>
													</div>
													<div class="col-md-8">
														<label for="nom" >Nom :</label>
														<div class="field prepend-icon">
															<input type="text" readonly name="sus_nom_particulier" id="nom_particulier" class="gui-input" maxlength="100" placeholder="Nom"  />
															<span class="field-icon">
																<i class="fa fa-building-o"></i>
															</span>
														</div>
													</div>
												</div> -->
												<div class="row pull-right verification" style="margin-top:5px;">
													<span style="color:red">*</span> Champs nécessaires pour la vérification
												</div>
												<div class="row mt15 verification">
													<div class="col-md-12 text-center">
														<div class="section">
														  <button type="button" id="verif" class="btn btn-success btn-sm">Vérifier</button>
														  <button type="button" id="ignorer" class="btn btn-warning btn-sm">Ignorer</button>
														</div>
													</div>
												</div>

												<div class="row verification" id="suspect_doublon" style="display:none;margin-top:10px;" ></div>

												<!-- form apres verif -->
												<div class="reste-form" style="display:none;">


													<div class="row mt15">
														<div class="col-md-4 pt20">
															Société : <b><?=$d_societe["soc_nom"]?></b>
														</div>

												<?php 	if(isset($d_agences)){ ?>
															<div class="col-md-4" >
																<div class="section">
																	<label for="agence" >Agence :</label>
																	<select id="agence" name="sus_agence" class="select2" required>
																		<option value="0">Agence...</option>
												<?php 					foreach($d_agences as $a){
																			echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																		} ?>
																	</select>
																</div>
															</div>
												<?php	}elseif(isset($d_agence)){ ?>
															<div class="col-md-4 pt20">
																Agence : <b><?=$d_agence["age_nom"]?></b>
																<input type="hidden" name="sus_agence" value="<?= $d_agence["age_id"] ?>" />
															</div>
												<?php 	}
														if(count($d_commerciaux)>1 OR isset($d_agences)){ ?>
															<div class="col-md-4">
																<div class="section">
																	<label for="commercial" >Commercial :</label>
																	<select name="sus_commercial" id="commercial" class="select2" required>
																		<option value="0">Commercial...</option>
										<?php							if(!empty($d_commerciaux)){
																			foreach($d_commerciaux as $com){
																				echo("<option value='" . $com["com_id"] . "' >" . $com["com_label_1"] . " "  . $com["com_label_2"] . "</option>");
																			}
																		} ?>
																	</select>
																</div>
															</div>
											<?php		} ?>
													</div>

													<div class="row mt15">
														<div class="col-md-6">
															<div class="section">
																<label for="categorie">Catégorie</label>
																<select id="categorie" name="sus_categorie" class="select2" required >
																	<!-- <option value="0" >Sélectionner une catégorie...</option> -->
														<?php		if(!empty($d_categories)){
																		foreach($d_categories as $dc){
																			echo("<option value='" . $dc["cca_id"] . "' >" . $dc["cca_libelle"] . "</option>");
																		}
																	} ?>
																</select>
															</div>
														</div>
														<div class="col-md-6" id="sous_categorie_bloc" style="display:none;" >
															<label for="sous_categorie">Sous-catégorie</label>
															<select id="sous_categorie" name="sus_sous_categorie" class="select2" >
																<option value="0">Sélectionner une sous catégorie...</option>
															</select>
														</div>
													</div>
													<div class="row">

														<!-- COLONNE INTERVENTION -->
														<div class="col-md-6">

															<div class="row bloc-no-particulier">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Adresse d'intervention</span>
																	</div>
																</div>
															</div>

															<div class="row bloc-no-particulier">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_libelle1" id="adr_libelle1" class="gui-input" placeholder="Libellé"  value="<?=$s['adr_libelle']?>">
																		<label for="cli_ad1" class="field-icon">
																			<i class="fa fa-tag"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row bloc-no-particulier">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_nom1" id="adr_nom1" class="gui-input" placeholder="Nom"  value="<?=$s['adr_nom']?>">
																		<label for="cli_ad1" class="field-icon">
																				<i class="fa fa-tag"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row bloc-no-particulier">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_service1" id="adr_service1" class="gui-input" placeholder="Service"  value="<?=$s['adr_service']?>">
																		<label for="cli_ad1" class="field-icon">
																			<i class="fa fa-tag"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row bloc-no-particulier">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_ad11" id="adr_ad11" class="gui-input" placeholder="Adresse"  value="<?=$s['adr_ad1']?>">
																		<label for="cli_ad1" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row bloc-no-particulier">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_ad21" class="gui-input" id="adr_ad21" placeholder="Adresse (Complément 1)" value="<?=$s['adr_ad2']?>">
																		<label for="cli_ad2" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>

															<div class="row bloc-no-particulier">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_ad31" id="adr_ad31" class="gui-input" placeholder="Adresse (Complément 2)" value="<?=$s['adr_ad3']?>">
																		<label for="cli_ad3" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>

															<div class="row bloc-no-particulier">
																<div class="col-md-3">
																	<div class="field prepend-icon">
																		<input type="text" id="adr_cp1" name="adr_cp1" class="gui-input code-postal" placeholder="Code postal"  value="<?=$s['adr_cp']?>">
																		<label for="cli_cp" class="field-icon">
																			<i class="fa fa fa-certificate"></i>
																		</label>
																	</div>
																</div>
																<div class="col-md-9">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_ville1" id="adr_ville1" class="gui-input nom" placeholder="Ville"  value="<?=$s['adr_ville']?>">
																		<label for="cli_ville" class="field-icon">
																			<i class="fa fa fa-building"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row mt15 bloc-no-particulier">
																<div class="col-md-12 text-center">Zone géographique</div>
															</div>
															<div class="row bloc-no-particulier">
																<div class="col-md-12 text-center">
																	<label for="geo11" class=" mt15 option option-dark">
																		<input type="radio" id="geo11" class="geo1" name="sus_geo1" value="1" checked>
																		<span class="radio"></span> France
																	</label>
																	<label for="geo21" class=" mt15 option option-dark">
																		<input type="radio" id="geo21" class="geo1" name="sus_geo1" value="2">
																		<span class="radio"></span> UE
																	</label>
																	<label for="geo31" class=" mt15 option option-dark">
																		<input type="radio" id="geo31" class="geo1" name="sus_geo1" value="3" >
																		<span class="radio"></span> Autre
																	</label>
																</div>
															</div>

															<div class="row mt15 bloc-no-particulier">
																<div class="col-md-12">
																	<label class="option option-dark">
																		<input type="checkbox" id="similaire" value="oui">
																		<span class="checkbox"></span>
																		<label for="similaire">Utiliser comme adresse de facturation</label>
																	</label>
																</div>
															</div>

															<!-- CONTACT PAR DEFAUT -->
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span class="contact_libelle">Contact par défaut du lieu d'intervention</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																		<div class="field select">
																			<select name="con_fonction" id="fonction" >
																				<option value="0">Sélectionner une fonction...</option>
																		<?php	if(!empty($d_contact_fonctions)){
																					foreach($d_contact_fonctions as $dcf){
																						echo("<option value='" . $dcf["cfo_id"] . "'>" . $dcf["cfo_libelle"] . "</option>");
																					}
																				} ?>
																				<option value="autre" >Autre fonction</option>
																			</select>
																			<i class="arrow simple"></i>
																		</div>
																	</div>
																</div>
																<div class="col-md-6" id="fonction_nom_bloc" style="display:none;">
																	<div class="section">
																		<div class="field prepend-icon">
																		  <input type="text" name="con_fonction_nom" id="fonction_nom" class="gui-input" placeholder="Autre fonction"  >
																		  <label for="cli_nom" class="field-icon">
																			<i class="fa fa-tag"></i>
																		  </label>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<div class="field select">
																			<select name="con_titre" id="con_titre" >
																				<option value="0">Civilité...</option>
																				<option value="1">Monsieur</option>
																				<option value="2">Madame</option>
																			</select>
																			<i class="arrow simple"></i>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																		<div class="field prepend-icon">
																			<input type="text" name="con_nom" id="con_nom" class="gui-input nom typing-particulier-2" placeholder="Nom"  value="<?=$s['con_nom']?>">
																			<label for="con_nom" class="field-icon">
																				<i class="fa fa-user"></i>
																			</label>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	  <div class="section">
																		<div class="field prepend-icon">
																		  <input type="text" name="con_prenom" id="con_prenom" class="gui-input prenom typing-particulier-2" placeholder="Prénom"  value="<?=$s['cli_nom']?>">
																		  <label for="con_prenom" class="field-icon">
																			<i class="fa fa-tag"></i>
																		  </label>
																		</div>
																	  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																	<div class="field prepend-icon">
																	  <input name="con_tel" id="con_tel" type="tel" class="gui-input telephone" placeholder="Numéro de téléphone"  value="<?=$s['cli_tel']?>">
																	  <label for="cli_tel" class="field-icon">
																		<i class="fa fa fa-phone"></i>
																	  </label>
																	</div>
																	</div>
																</div>
																<div class="col-md-6">
																	  <div class="section">
																		<div class="field prepend-icon">
																		  <input name="con_fax" id="con_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax"  value="<?=$s['cli_fax']?>">
																		  <label for="cli_fax" class="field-icon">
																			<i class="fa fa-phone-square"></i>
																		  </label>
																		</div>
																	  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	  <div class="section">
																		<div class="field prepend-icon">
																		  <input name="con_portable" id="con_portable" type="tel" class="gui-input telephone" placeholder="Numéro de portable"  value="<?=$s['con_portable']?>">
																		  <label for="cli_tel" class="field-icon">
																			<i class="fa fa fa-mobile"></i>
																		  </label>
																		</div>
																	  </div>
																</div>
																<div class="col-md-6">
																	  <div class="section">
																		<div class="field prepend-icon">
																		  <input type="email" name="con_mail" id="con_mail" class="gui-input" placeholder="Adresse Email"  value="<?=$s['cli_mail']?>">
																		  <label for="cli_mail" class="field-icon">
																			<i class="fa fa-envelope"></i>
																		  </label>
																		</div>
																	  </div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6 pt15">

																	<label class="option option-dark">
																		  <input type="checkbox" name="con_compta" id="con_compta" value="on" />
																		  <span class="checkbox"></span> Contact "impayé"
																	</label>

																</div>
																<div class="col-md-6 bloc-particulier" style="display:none;" >
																	<label for="sus_stagiaire_naiss" class="field prepend-icon">
																		<input type="text" id="sus_stagiaire_naiss" name="sus_stagiaire_naiss" class="gui-input datepicker date typing-particulier-2" placeholder="Date de naissance">
																		<span class="field-icon">
																			<i class="fa fa-calendar-o"></i>
																		</span>
																	</label>
																</div>
															</div>
															<div class="row bloc-particulier mt15" style="display:none;" >
																<div class="col-md-6" >
																	<label for="sus_stagiaire_naiss_cp" class="field prepend-icon">
																		<input type="text" id="sus_stagiaire_naiss_cp" name="sus_stagiaire_naiss_cp" class="gui-input code-postal" placeholder="CP de naissance" />
																		<span class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</span>
																	</label>
																</div>
																<div class="col-md-6" >
																	<label for="sus_stagiaire_naiss_ville" class="field prepend-icon">
																		<input type="text" id="sus_stagiaire_naiss_ville" name="sus_stagiaire_naiss_ville" class="gui-input nom" placeholder="Ville de naissance" />
																		<span class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</span>
																	</label>
																</div>
															</div>
															<!-- FIN CONTACT PAR DEFAUT -->

														</div>
														<!-- FIN DE INTERVENTION -->

														<!-- FACTURATION -->

														<div class="col-md-6">
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																	  <span>Adresse de facturation</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_libelle2" id="adr_libelle2" class="gui-input" placeholder="Libellé"  value="<?=$s['adr_libelle']?>">
																		<label for="cli_ad1" class="field-icon">
																			<i class="fa fa-tag"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_nom2" id="adr_nom2" class="gui-input" placeholder="Nom"  value="<?=$s['adr_nom']?>">
																		<label for="adr_nom2" class="field-icon">
																			<i class="fa fa-tag"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_service2" id="adr_service2" class="gui-input" placeholder="Service"  value="<?=$s['adr_service']?>">
																		<label for="cli_ad1" class="field-icon">
																			<i class="fa fa-tag"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_ad12" id="adr_ad12" class="gui-input" placeholder="Adresse"  value="<?=$s['adr_ad1']?>">
																		<label for="cli_ad1" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_ad22" class="gui-input" id="adr_ad22" placeholder="Adresse (Complément 1)" value="<?=$s['adr_ad2']?>">
																		<label for="cli_ad2" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_ad32" id="adr_ad32" class="gui-input" placeholder="Adresse (Complément 2)" value="<?=$s['adr_ad3']?>">
																		<label for="cli_ad3" class="field-icon">
																		  <i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-3">
																	<div class="field prepend-icon">
																		<input type="text" id="adr_cp2" name="adr_cp2" class="gui-input code-postal" placeholder="Code postal"  >
																		<span for="cli_cp" class="field-icon">
																			<i class="fa fa fa-certificate"></i>
																		</span>
																	</div>
																</div>
																<div class="col-md-9">
																	<div class="field prepend-icon">
																		<input type="text" name="adr_ville2" id="adr_ville2" class="gui-input nom" placeholder="Ville"  value="<?=$s['adr_ville']?>">
																		<label for="cli_ville" class="field-icon">
																			<i class="fa fa fa-building"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="row mt15 text-center">
																<div class="col-md-12">
																	Zone géographique
																</div>
															</div>
															<div class="row text-center">
																<div class="col-md-12">
																	<label for="cli_geo12" class=" mt15 option option-dark">
																		<input type="radio" id="geo12" name="sus_geo2" value="1" checked>
																		<span class="radio"></span> France
																	</label>
																	<label for="cli_geo22" class=" mt15 option option-dark">
																		<input type="radio" id="geo22" name="sus_geo2" value="2">
																		<span class="radio"></span> UE
																	</label>
																	<label for="cli_geo32" class=" mt15 option option-dark">
																		<input type="radio" id="geo32" name="sus_geo2" value="3">
																		<span class="radio"></span> Autre
																	</label>
																</div>
															</div>

															<div class="row mt15 bloc-no-particulier">
																<div class="col-md-6">
																	<label for="siren" >Siren :</label>
																	<div class="field prepend-icon">
																		<input type="text" name="sus_siren" id="siren" class="gui-input siren" placeholder="Siren" data-prefixe="" >
																		<span class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</span>
																	</div>
																	<div id="siren_err" class="text-danger" ></div>
																</div>
																<div class="col-md-6">
																	<label for="nic" >Nic :</label>
																	<div class="field prepend-icon">
																		<input type="text" name="sus_nic" id="nic" class="gui-input siret" placeholder="Nic" disabled data-prefixe="" />
																		<span class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="row mt15 bloc-no-particulier">
																<?php if(!empty($_SESSION['acces']['acc_droits'][36])){ ?>
																	<div class="col-md-6">
																<?php }else{ ?>
																	<div class="col-md-12">
																<?php }?>

																	<label for="siret" >Siret :</label>
																	<div class="field prepend-icon">
																		<input type="text" name="sus_siret" id="siret" class="gui-input siretlong" placeholder="Siret" data-prefixe="" />
																		<span class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</span>
																	</div>
																</div>
																<?php if(!empty($_SESSION['acces']['acc_droits'][36])){ ?>

																	<div class="col-md-6" style="margin-top:25px;">
																		<label class="option option-dark">
																			<input type="checkbox" id="option_verif" value="oui" name="option_verif">
																			<span class="checkbox"></span>
																			<label for="option_verif">Désactiver la vérification Siret</label>
																		</label>
																	</div>
																<?php }	?>
															</div>

															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Facturation</span>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section">
																		<span class="select">
																			<select id="reg_type" name="sus_reg_type">
																				<option value="0">Mode de règlement...</option>
                                                    <?php 						foreach($base_reglement as $k => $n){
																					if($k > 0){
																						if($k==2){?>
																							<option value="<?=$k?>" selected ><?=$n?></option>
														<?php 							}else{ ?>
																							<option value="<?=$k?>"><?=$n?></option>
														<?php 							}
																					}
																				} ?>
																			</select>
																			<i class="arrow"></i>
																		</span>
																	</div>
																</div>
															</div>

															<div class="row mt5" >
																<div class="col-md-12">
																	<label class="option">
																		<input type="radio" id="reg_formule_1" class="reg-formule" name="reg_formule" value="1" checked/>
																		<span class="radio"></span>
																	</label>
																	Date + <input type="number" max="60" size="2" name="reg_nb_jour_1" value="30" class="champ-reg-formule reg-formule-1" /> jours
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
																	<label class="option">
																		<input type="radio" id="reg_formule_2" class="reg-formule" name="reg_formule" value="2" >
																		<span class="radio"></span>
																	</label>
																	Date + 45j + fin de mois
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
																	<label class="option">
																		<input type="radio" id="reg_formule_3" class="reg-formule" name="reg_formule" value="3" >
																		<span class="radio"></span>
																	</label>
																	Date + Fin de mois + 45j
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
																	<label class="option">
																		<input type="radio" id="reg_formule_4" class="reg-formule" name="reg_formule" value="4" />
																		<span class="radio"></span>
																	</label>
																	Date + <input type="number" max="30" size="2" maxlength="2" name="reg_nb_jour_4" value="0" disabled class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
																	+ Fin de mois +  <input type="number" size="2" maxlength="2" max="20" name="reg_fdm_4" value="0" disabled class="champ-reg-formule reg-formule-4" style="width:100px;" /> jours
																</div>
															</div>

															<div class="row">
																<div class="col-md-6 pt10">
																	<div class="option-group field">
																		<label class="option option-dark">
																			<input type="checkbox" name="sus_releve" value="on" checked>
																			<span class="checkbox"></span>Relevés de factures
																		</label>
																	</div>
																</div>
																<div class="col-md-6 bloc-no-particulier">
																	<div class="field prepend-icon">
																		<input type="text" name="sus_ident_tva" id="ident_tva" class="gui-input" placeholder="Identification TVA" >
																		<span for="cli_ident_tva" class="field-icon">
																		  <i class="fa fa-barcode"></i>
																		</span>
																	</div>
																</div>
															</div>

															<!-- FINANCEUR -->
															<div class="row bloc-no-do">
																<div class="col-md-12">
																	<div class="section-divider">
																		<span>Financeur</span>
																	</div>
																</div>
															</div>

															<div class="row bloc-no-do">
																<div class="col-md-12">
																	<label class="option option-dark">
																		<input type="checkbox" name="sus_facture_opca" id="facture_opca" value="oui">
																		<span class="checkbox"></span>Facturer un financeur
																	</label>
																</div>
															</div>
															<div class="row mt15" id="bloc_financeur" style="display:none;" >
																<div class="col-md-6">
																	<label for="opca_type" >Type de financeur : </label>
																	<select id="opca_type"  name="sus_opca_type" class="select2" >
																		<option value="0">Type de financeur ...</option>
														<?php 			if(!empty($d_opca_type)){
																			foreach($d_opca_type as $opca){
																				echo("<option value='" . $opca["csc_id"] . "' >" . $opca["csc_libelle"] . "</option>");
																			}
																		} ?>
																	</select>
																</div>
																<div class="col-md-6">
																	<label for="opca" >Financeur : </label>
																	<select id="opca"  name="sus_opca" class="select2" >
																		<option value="0">Sélectionner un financeur...</option>
																	</select>
																</div>
															</div>


														</div>
														<!-- fin de facturation -->
													</div>

													<!-- AUTRE INFO -->
													<div class="section-divider mb40">
														<span>Autres infos</span>
													</div>

													<div class="row mt15 bloc-no-particulier">
														<div class="col-md-4">
															<label for="classification">Classification :</label>
															<select id="classification" name="sus_classification" class="select2 select2-classification" >
																<option value="0">Sélectionner une classification...</option>
														<?php		if(!empty($d_classifications)){
																		foreach($d_classifications as $classifications){
																			echo("<option value='" . $classifications["ccl_id"] . "' >" . $classifications["ccl_libelle"] . "</option>");
																		}
																	}  ?>
															</select>
														</div>
														<div class="col-md-4">
															<label for="sous_classification">Type de classification :</label>
															<select id="sous_classification" name="sus_classification_type" class="select2 select2-classification-type" >
																<option value="0">Type de classification...</option>
															</select>
														</div>
														<div class="col-md-4">
															<label for="classification-categorie">Catégorie de classification :</label>
															<select id="classification-categorie" name="sus_classification_categorie" class="select2 select2-classification-categorie" >
																<option value="0">Catégorie de classification...</option>
															</select>
														</div>
													</div>

													<div class="row mt15">
														<div class="col-md-4">
															<label for="stagiaire_type">Type de stagiaire :</label>
															<select id="stagiaire_type" name="sus_stagiaire_type" class="select2" >
																<option value="0">Type de stagiaire...</option>
													<?php		if(!empty($base_type_stagiaire)){
																	foreach($base_type_stagiaire as $k => $n){
																		if($k>0){
																			echo("<option value='" . $k . "' >" . $n . "</option>");
																		}
																	}
																}  ?>
															</select>
														</div>
														<div class="col-md-4 bloc-no-particulier">
															<div class="section" >
																<label for="sus_tel" >Téléphone (standard) </label>
																<input type="text" id="sus_tel" name="sus_tel" class="gui-input telephone" placeholder="Téléphone" value="" />
															</div>
														</div>
													</div>
													<div class="row mt15">
														<div class="col-md-4 bloc-no-particulier">
															<label for="ape" >Code APE :</label>
															<select id="ape" name="sus_ape" class="select2">
															</select>
														</div>
														<div class="col-md-4">
															<label for="prescripteur" >Source :</label>
															<select id="prescripteur"  name="sus_prescripteur" class="select2"  >
																<option value="0">Sélectionner une source...</option>
														<?php	if(!empty($d_sources)){
																	foreach($d_sources as $source){ ?>
																		<option value="<?=$source["cpr_id"]?>" ><?=$source["cpr_libelle"]?></option>
														<?php		}
																} ?>
																<option value="-1">Autre</option>
															</select>
														</div>
														<div class="col-md-4" id="bloc_source_detail" style="display:none;" >
															<label for="prescripteur_info" >Merci de préciser votre source</label>
															<input type="text" name="sus_prescripteur_info" id="prescripteur_info" class="gui-input" placeholder="Merci de préciser votre source" value="" />
														</div>
													</div>
													<div class="row mt15 bloc-no-particulier">
														<div class="col-md-6">
															<div class="section" >
																<label for="sus_capital" >Capital</label>
																<div class="input-group">
																	<input type="text" id="sus_capital" name="sus_capital" class="gui-input" placeholder="Capital" value="" />
																	<div class="input-group-addon">€</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="section" >
																<label for="sus_soc_type" >Type de société</label>
																<input type="text" name="sus_soc_type" id="sus_soc_type" class="gui-input" placeholder="Type de société" value="" />
															</div>
														</div>
													</div>
													<div class="row mt15 bloc-no-particulier">
														<div class="col-md-6" >
															<div class="section" >
																<label for="sus_immat_lieu" >Lieu d'immatriculation</label>
																<input type="text" id="sus_immat_lieu" name="sus_immat_lieu" class="gui-input" placeholder="Lieu d'immatriculation" value="" />
															</div>
														</div>
													</div>

												</div>


										</div>
									</div>
								</div>
								<!-- FIN PANEL FORM -->
                            </div>
						</div>
					</section>
				</section>
			</div>

			<footer id="content-footer" class="affix">
				<div class="row">
                    <div class="col-xs-3 footer-left" >
				<?php 	if(isset($_SESSION['retour'])){ ?>
							<a href="<?=$_SESSION['retour']?>" class="btn btn-default btn-sm" >
								<i class="fa fa-long-arrow-left"></i> Retour
							</a>
				<?php 	} ?>
					</div>
                    <div class="col-xs-6 footer-middle"></div>
                    <div class="col-xs-3 footer-right" >
                    	<button type="button" id="btn_prospect" style="display:none;" class="btn btn-info btn-sm">
							<i class="fa fa-check" ></i> Valider la fiche
						</button>
						<button type="submit" name="submit" id="sus_submit" style="display:none;" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
                    </div>
				</div>
			</footer>
		</form>
 <?php	include "includes/footer_script.inc.php"; ?>


		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>

		<script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>

		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<script src="vendor/plugins/holder/holder.min.js"></script>
		<script src="assets/js/custom.js"></script>

		<script type="text/javascript">

			var societe="<?=$acc_societe?>";
			var agence="<?=$acc_agence?>";

			// memo des source

			var source=new Array();
			source[0]=new Array();
			source[0]["libelle"]="";
			source[0]["ouvert"]=0;
			source[-1]=new Array();
			source[-1]["libelle"]="Autre";
			source[-1]["ouvert"]=1;
	<?php	if(!empty($d_sources)){
				foreach($d_sources as $source){ ?>
					source[<?=$source["cpr_id"]?>]=new Array();
					source[<?=$source["cpr_id"]?>]["libelle"]="<?=$source["cpr_libelle"]?>";
					source[<?=$source["cpr_id"]?>]["ouvert"]="<?=$source["cpr_ouvert"]?>";
	<?php		}
			} ?>


			jQuery(document).ready(function(){
				$(".con_civilite_verif").change(function() {
					$("#con_titre").val($(this).val());
				});
				// CREATION COE NOM PARTICULIER
				$(".typing-particulier-2").keyup(function(){
					// s'applique sur les champ après verif (le radio client / particulier n'existe plus)
					if($("#categorie").val()==3){
						prenom_particulier=$("#con_nom").val();
						nom_particulier=$("#con_prenom").val();
						naissance_particulier=$("#sus_stagiaire_naiss").val();
						$("#code").val("P-" + prenom_particulier.replace(/\s/g, '') + nom_particulier.replace(/\s/g, '') + naissance_particulier.replace(/\//g, ''));
						$("#nom").val(prenom_particulier + " " + nom_particulier);
					}
				});

				// BLOC VERIFIATION
				if($(".part_client").val() == 1){
					// CLIENT
					$(".verification-particulier").hide();
					$(".verification-particulier-hide").hide();
					$(".verification-client").show();
					$("#categorie").val(1);
				}else{
					$(".verification-particulier").show();
					$(".verification-particulier-hide").show();
					$(".verification-client").hide();
					$("#categorie").val(3);
				}
				$(".part_client").change(function(){
					if($(this).val() == 1){
						// CLIENT
						$(".verification-particulier").hide();
						$(".verification-particulier-hide").hide();
						$(".verification-client").show();
						$("#categorie").val(1);
						$("#categorie").trigger('change');
					}else{
						$(".verification-particulier").show();
						$(".verification-particulier-hide").show();
						$(".verification-client").hide();
						$("#categorie").val(3);
						$("#categorie").trigger('change');
					}
				});

				// SIREN SIRET POUR LA PARTIE VERIF


				$(".siren").focusout(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length!=11){
						$("#" + prefixe + "nic").val("");
						$("#" + prefixe + "nic").prop("disabled",true);

						$("#" + prefixe + "nic").val("");
					}else{
						check_siren_val(prefixe);
					}
				});
				$(".siren").keyup(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length==11){
						$("#" + prefixe + "nic").prop("disabled",false);
						if($("#" + prefixe + "nic").val()!=""){
							$("#" + prefixe + "siret").val($(this).val() + " " + $("#" + prefixe + "nic").val());
						}
					}
				});

				$(".siret").focusout(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length!=6){
						$("#" + prefixe + "siret").val("");
					}
				});
				$(".siret").keyup(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length==6){
						if($("#" + prefixe + "siren").val()!=""){
							$("#" + prefixe + "siret").val($("#" + prefixe + "siren").val() + " " + $(this).val());
						}
					}
				});

				$(".siretlong").focusout(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length!=18){
						$("#" + prefixe + "nic").val("");
					}else{
						check_siren_val(prefixe);
					}
				});
				$(".siretlong").keyup(function(){
					prefixe=$(this).data("prefixe");
					if($(this).val().length==18){
						$("#" + prefixe + "siren").val($(this).val().substring(0,11));
						$("#" + prefixe + "nic").prop("disabled",false);
						$("#" + prefixe + "nic").val($(this).val().substring(12));
					}
				});

				// declenche la verif avant l'ouverture du formulaire
				$("#verif" ).click(function(){

					if($("input[name='part_client']:checked").val() == 1){
						// CLIENT
						code=$("#code").val();
						nom=$("#nom").val();
						siren=$("#verif_siren").val();
						siret=$("#verif_nic").val();
						if(code == "" || siren=="" || siret==""){

							modal_alerte_user("Merci de renseigner le code et le siret avant de lancer la vérification.");
						}else{
							//check_client(0,0,0,code,nom,siren,siret,afficher_check,"","");
							check_suspect(0,code,nom,siren,siret,afficher_check,"","");
						}
					}else{
						// CLIENT
						prenom_particulier=$("#con_prenom_verif").val();
						nom_particulier=$("#con_nom_verif").val();
						naissance_particulier=$("#sus_stagiaire_naiss_verif").val();
						if(prenom_particulier=="" || nom_particulier=="" || naissance_particulier==""){
							modal_alerte_user("Le nom prénom et date de naissance sont obligatoires pour vérifier l'existence du stagiaire, si vous ne disposez pas de ces informations vous pouvez ignorer la vérification. Attention vous ne pourrez pas valider la fiche tant que vous n'aurez pas validé ces informations.");
						}else{
							//check_client(0,0,0,code,nom,siren,siret,afficher_check,"","");

							check_particulier(0,nom_particulier,prenom_particulier,naissance_particulier,afficher_check,"","");
						}
					}


				});
				$("#ignorer").click(function(){
					confirmer_add();
				});


				//**********************
				// INTERACTION FORM
				//**********************

				// code APE
				$("#ape").select2({
					minimumInputLength: 2,
					ajax: {
						url: "ajax/ajax_get_ape.php",
						dataType: 'json',
						delay: 250,
						method: 'GET',
						data: function (params) {
							return {
								code: params.term, // les termes de la requête
							};
						},
						processResults: function (data) {
							return {
								results: data,
							};
						},
					},
					templateResult: function(data) {
						return data.nom;
					},
					templateSelection: function(data) {
						if(data.nom){
							// revoyer par la requete ajax
							return data.nom;
						}else{
							// cree par defaut à l'ini du plugin
							return data.text;
						}
					},
				});



				// Nom
				$("#nom").keyup(function(){
					$("#adr_nom1").val($(this).val());
					if($("#similaire").is(":checked")){
						$("#adr_nom2").val($(this).val());
					}
				})

				if($("#categorie").val()==3){
					$(".contact_libelle").text("Coordonnées du particulier");
				}else{
					$(".contact_libelle").text("Contact par défaut du lieu d'intervention");
				}

				// categorie du client

				get_sous_categories($(this).val(),actualiser_sous_categorie,"","")
				if($(this).val()==5){
					$(".bloc-interco").show();
				}else{
					$(".bloc-interco").hide();
				}
				if($(this).val()==3){
					console.log("bloc-no-particulier hide");
					$("#con_compta").prop("checked", true);
					$("#ape").hide();
					$(".bloc-no-particulier").hide();
					$(".bloc-particulier").show();
					$(".contact_libelle").text("Coordonnées du particulier");
				}else{
					$("#con_compta").prop("checked", false);
					$("#ape").show();
					$(".bloc-no-particulier").show();
					$(".bloc-particulier").hide();
					$(".contact_libelle").text("Contact par défaut du lieu d'intervention");
				}
				if($("#categorie").val()==4){
					$(".bloc-no-do").hide();
				}
				$("#categorie").on("change",function(){
					get_sous_categories($(this).val(),actualiser_sous_categorie,"","");

					// client national
					if($(this).val()==5){
						$(".bloc-interco").show();
					}else{
						$(".bloc-interco").hide();
					}

					if($(this).val()==4){
						$(".bloc-no-do").hide();
					}else{
						$(".bloc-no-do").show();
					}
					if($(this).val()==3){
						console.log("bloc-no-particulier hide");

						$("#con_compta").prop("checked", true);
						$("#ape").hide();
						$(".bloc-no-particulier").hide();
						$(".bloc-particulier").show();
						$(".contact_libelle").text("Coordonnées du particulier");

						$(".typing-particulier-2").keyup(function() {
							prenom_particulier=$("#con_nom").val();
							nom_particulier=$("#con_prenom").val();
							naissance_particulier=$("#sus_stagiaire_naiss").val();
							$("#code").val("P-" + prenom_particulier.replace(/\s/g, '') + nom_particulier.replace(/\s/g, '') + naissance_particulier.replace(/\//g, ''));
		  				    $("#nom").val(prenom_particulier + " " + nom_particulier);
						});

					}else{
						$("#con_compta").prop("checked", false);
						$("#ape").show();
						$(".bloc-no-particulier").show();
						$(".bloc-particulier").hide();
						$(".contact_libelle").text("Contact par défaut du lieu d'intervention");

						$(".typing-particulier-2").off('keyup keydown keypress');
					}
				});

				// choix du commercial
				$("#agence").change(function(){
					get_commerciaux(societe,$(this).val(),0,actualiser_select2,"#commercial",$("#commercial").val());
				});

				// Adresse

				$("#similaire").click(function(){
					if($(this).is(":checked")){
						$("#adr_nom2").val($("#adr_nom1").val());
						$("#adr_service2").val($("#adr_service1").val());
						$("#adr_ad12").val($("#adr_ad11").val());
						$("#adr_ad22").val($("#adr_ad21").val());
						$("#adr_ad32").val($("#adr_ad31").val());
						$("#adr_cp2").val($("#adr_cp1").val());
						$("#adr_ville2").val($("#adr_ville1").val());
						$("#geo12").prop("checked",$("#geo11").prop("checked"));
						$("#geo22").prop("checked",$("#geo21").prop("checked"));
						$("#geo32").prop("checked",$("#geo31").prop("checked"));
					}
				});

				$("#adr_nom1").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_nom2").val($(this).val());
					}
				})

				$("#adr_service1").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_service2").val($(this).val());
					}
				})

				$("#adr_ad11").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_ad12").val($(this).val());
					}
				})

				$("#adr_ad21").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_ad22").val($(this).val());
					}
				})

				$("#adr_ad31").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_ad32").val($(this).val());
					}
				})

				$("#adr_cp1").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_cp2").val($(this).val());
					}
				})

				$("#adr_ville1").keyup(function(){
					if($("#similaire").is(":checked")){
						$("#adr_ville2").val($(this).val());
					}
				})

				$("#geo11").click(function(){
					if($("#similaire").is(":checked")){
						$("#adr_ville2").val($(this).val());
					}
				})

				$(".geo1").click(function(){
					if($("#similaire").is(":checked")){
						$("#geo12").prop("checked",$("#geo11").prop("checked"));
						$("#geo22").prop("checked",$("#geo21").prop("checked"));
						$("#geo32").prop("checked",$("#geo31").prop("checked"));
					}
				})

				// contact
				$("#fonction").change(function(){
					if($(this).val()=="autre"){
						$("#fonction_nom_bloc").show();
					}else{
						$("#fonction_nom_bloc").hide();
						$("#fonction_nom").val("");
					}

				});


				// GESTION DES MODALITES DE REGLEMENTS
				$("input[name=reg_formule]").click(function (){
					reg_formule=$("input[name=reg_formule]:checked").val();
					$('.champ-reg-formule').each(function(){
						if($(this).hasClass("reg-formule-" + reg_formule)){
							$(this).prop("disabled",false);
						}else{
							$(this).val(0);
							$(this).prop("disabled",true);
						}
					});
				});
				$(".champ-reg-formule").blur(function(){
					//console.log($(this).val());
					//console.log($(this).prop("max"));
					if( ($(this).val()*1)>($(this).prop("max")*1) ){
						$(this).val($(this).prop("max"));
					}
				});


				$("#prescripteur").change(function(){
					$("#prescripteur_info").val("");
					if(source[$(this).val()]["ouvert"]==1){

						$("#bloc_source_detail label").html(source[$(this).val()]["libelle"]);
						$("#bloc_source_detail").show();
					}else{
						$("#bloc_source_detail").hide();
					}
				});

				// ENVOIE DU FORM
				$("#btn_prospect").click(function(){
					$("#prospect").val("1");
					$("#sus_submit").trigger("click");
				});

				if($("#facture_opca").is(":checked")){
					$("#bloc_financeur").show();
				}else{
					$("#bloc_financeur").hide();

				}
				$("#facture_opca").click(function(){
					if($(this).is(":checked")){
						$("#bloc_financeur").show();
					}else{
						$("#bloc_financeur").hide();

					}
				});

				$("#opca_type").change(function(){
					if($(this).val()>0){
						get_clients(4,$(this).val(),0,null,actualiser_select2,"#opca",0);
					}else{
						$("#opca option[value!='0'][value!='']").remove();
					}
				});

			});

			//**********************
			// FONCTION
			//**********************

			// CONTROLE AVANT CREATION

			function check_suspect(suspect,code,nom,siren,siret,callback,param1,param2){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_check_suspect.php',
					data : "suspect=" + suspect + "&code=" + code + "&nom=" + nom + "&siren=" + siren + "&siret=" + siret,
					dataType: 'json',
					success: function(data){
						callback(data,param1,param2);
						fill_api(suspect,siren, siret,fill_api_json);
					},
					error: function(data) {
						modal_alerte_user(data.responseText);
					}
				});
			}
			function check_particulier(suspect,nom_particulier,prenom_particulier,naissance,callback,param1,param2){
				$.ajax({
					type:'POST',
					url: 'ajax/ajax_check_particulier.php',
					data : "nom_particulier=" + nom_particulier + "&prenom_particulier=" + prenom_particulier + "&naissance=" + naissance,
					dataType: 'json',
					success: function(data){
						callback(data,param1,param2);
					},
					error: function(data) {
						modal_alerte_user(data.responseText);
					}
				});
			}
			function fill_api(suspect,siren, siret, callback){

				$.ajax({
					type:'GET',
					url: 'ajax/ajax_fill_api.php',
					data : "suspect=" + suspect + "&siren=" + siren + "&siret=" + siret,
					dataType: 'json',
					success: function(data){
						callback(data);
					},
					error: function(data) {
						alert(data.responseText);
						//modal_alerte_user(data.responseText);
					}
				});
			}
			function fill_api_json(json){
				// REMPLIR LES INPUTS
				if(json.cli_nom){
					$("#nom").val(json.cli_nom);
					if(json.cli_ape != ""){
						$('#ape').append($('<option>', {
						    value: json.cli_ape,
						    text: json.cli_ape_code + "-" + json.cli_ape_nom
						}));
					}

					$("#ape").val(json.cli_ape);
					$("#ape").trigger('change');
					$("#adr_nom1").val(json.cli_nom);
					$("#adr_nom2").val(json.cli_nom);
					$("#adr_ad11").val(json.cli_adr_ad1);
					$("#adr_ad21").val(json.cli_adr_ad2);
					$("#adr_cp1").val(json.cli_adr_cp);
					$("#adr_ville1").val(json.cli_adr_ville);
					$("#ident_tva").val(json.cli_ident_tva);

						$("#similaire").prop("checked",true);

					$("#adr_nom2").val($("#adr_nom1").val());
					$("#adr_service2").val($("#adr_service1").val());
					$("#adr_ad12").val($("#adr_ad11").val());
					$("#adr_ad22").val($("#adr_ad21").val());
					$("#adr_ad32").val($("#adr_ad31").val());
					$("#adr_cp2").val($("#adr_cp1").val());
					$("#adr_ville2").val($("#adr_ville1").val());
					$("#geo12").prop("checked",$("#geo11").prop("checked"));
					$("#geo22").prop("checked",$("#geo21").prop("checked"));
					$("#geo32").prop("checked",$("#geo31").prop("checked"));
				}
			}
			function afficher_check(json){
				if(json.resultat==0){
					$("#suspect_ok div").html("Aucun doublon n'a été trouvé");
					$("#suspect_ok").show();
					confirmer_add();
				}else{
					$("#suspect_doublon").html("<div class='col-md-12 alert alert-" + json.resultat_class + "' >" + json.resultat_text + "</div>");
					$("#suspect_doublon").show();
					if(json.resultat==3){
						$("#ignorer").hide();
					}else{
						$("#ignorer").show();
					}
				}

			}

			// affiche le form pour enregistrer le nouveau suspect
			function confirmer_add(){
				$('.particulier-show').show();
				$("#siren").val($("#verif_siren").val());
				$("#nic").val($("#verif_nic").val());
				$("#siret").val($("#verif_siret").val());
				if($("#verif_siren").val()!=""){
					$("#nic").prop("disabled",false);
				}else{
					$("#nic").prop("disabled",true);
				}
				$("#adr_nom1" ).val($("#nom").val());
				$("#adr_nom2" ).val($("#nom").val());
				if($('#categorie').val() == 3){
					var civilite = "";
					if($(".con_civilite_verif").val() != 0){
						civilite = $(".con_civilite_verif").children("option:selected").text();
					}
					$("#adr_nom2").val(civilite + " " + $("#con_nom_verif").val() + " " + $("#con_prenom_verif").val());

					prenom_particulier=$("#con_nom_verif").val();
					nom_particulier=$("#con_prenom_verif").val();
					naissance_particulier=$("#sus_stagiaire_naiss_verif").val();
					$("#code").val("P-" + prenom_particulier.replace(/\s/g, '') + nom_particulier.replace(/\s/g, '') + naissance_particulier.replace(/\//g, ''));
					$("#nom").val(prenom_particulier + " " + nom_particulier);
				}
				$(".verification").hide();
				$(".verification-particulier-hide").hide();
				$("#suspect_doublon").hide();

				$("#con_nom").val($("#con_nom_verif").val());
				$("#con_prenom").val($("#con_prenom_verif").val());
				$("#sus_stagiaire_naiss").val($("#sus_stagiaire_naiss_verif").val());
				$(".reste-form").show();
				$("#sus_submit").show();
				$("#btn_prospect").show();


			}

			// GESTION DES CATEGORIE

			function actualiser_sous_categorie(json){
				$("#sous_categorie option[value!='0']").remove();
				if(json!=""){
					$("#sous_categorie").select2({
						data: json
					});
					$("#sous_categorie_bloc").show();
				}else{
					$("#sous_categorie_bloc").hide();
				}
			}

			function check_siren_val(prefixe){
				if($("#" + prefixe + "siren").val()=="123 456 789"||$("#" + prefixe + "siren").val()=="000 000 000"||$("#" + prefixe + "siren").val()=="111 111 111"||$("#" + prefixe + "siren").val()=="222 222 222"||$("#" + prefixe + "siren").val()=="333 333 333"||$("#" + prefixe + "siren").val()=="444 4444 444"||$("#" + prefixe + "siren").val()=="555 555 555"||$("#" + prefixe + "siren").val()=="666 666 666"||$("#" + prefixe + "siren").val()=="777 777 777"||$("#" + prefixe + "siren").val()=="888 888 888"||$("#" + prefixe + "siren").val()=="999 999 999"||$("#" + prefixe + "siren").val()=="987 654 321"){
					afficher_txt_user($("#" + prefixe + "siren_err"),"Saisie incorrecte!","danger",5000);
					$("#" + prefixe + "siren").val("");
					$("#" + prefixe + "nic").val("");
					$("#" + prefixe + "nic").prop("disabled",true);
					$("#" + prefixe + "siret").val("");
				}else{
					$("#" + prefixe + "nic").prop("disabled",false);
				}
			}


		</script>
	</body>
</html>
