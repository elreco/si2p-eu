<?php
$commerce_li = true;
include "includes/controle_acces.inc.php";
if($_SESSION['acces']['acc_profil'] == 2):
  Header('Location: index.php');
endif;
error_reporting( error_reporting() & ~E_NOTICE );

include_once 'includes/connexion.php';


include_once 'modeles/mod_upload.php';
require("modeles/mod_produit.php");
require("modeles/mod_famille.php");
require("modeles/mod_sous_famille.php");
require("modeles/mod_categorie.php");
require("modeles/mod_qualification.php");
require("modeles/mod_niveau.php");
require("modeles/mod_tva.php");
require("modeles/mod_compte.php");
require("modeles/mod_utilisateur.php");

require("modeles/mod_societe.php");
require("modeles/mod_client.php");


	$p = array();
	if (isset($_GET['id'])) {

		$p = get_produit($_GET['id']);

		if(!empty($p["pro_client"])){

			$sql="SELECT cli_id,cli_code,cli_nom FROM Clients WHERE cli_id=" . $p["pro_client"] . ";";
			$req=$Conn->query($sql);
			$d_client=$req->fetch();
		}

		$att_check_intra="";
		$att_input_intra="disabled";
		$min_dr_prct_intra=0;
		$min_prct_intra=0;
		if(!empty($p['pro_ht_intra'])){
			$att_check_intra="checked";
			$att_input_intra="required";
			if(!empty($p["pro_min_intra"])){
				$min_prct_intra=($p["pro_min_intra"]*100)/$p['pro_ht_intra'];
			}
			if(!empty($p["pro_min_dr_intra"])){
				$min_dr_prct_intra=($p["pro_min_dr_intra"]*100)/$p['pro_ht_intra'];
			}
		}

		$att_check_inter="";
		$att_input_inter="disabled";
		$min_dr_prct_inter=0;
		$min_prct_inter=0;
		if(!empty($p['pro_ht_inter'])){
			$att_check_inter="checked";
			$att_input_inter="required";
			if(!empty($p["pro_min_inter"])){
				$min_prct_inter=($p["pro_min_inter"]*100)/$p['pro_ht_inter'];
			}
			if(!empty($p["pro_min_dr_inter"])){
				$min_dr_prct_inter=($p["pro_min_dr_inter"]*100)/$p['pro_ht_inter'];
			}
		}

	}
?>
<!DOCTYPE html>
<html>
<!-- Mirrored from admindesigns.com/demos/absolute/1.1/layout_horizontal-sm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 09:52:52 GMT -->
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>SI2P - Orion - Paramètres</title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

	<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<!--
	<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
	-->

<!-- Favicon -->
<link rel="shortcut icon" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

	<body class="sb-top sb-top-sm ">
		<form method="post" action="produit_enr.php" id="form_produit" >
			<div>
				<input type="hidden" name="pro_id" id="pro_id" value="<?=$_GET['id']?>" />
			</div>
			<!-- Start: Main -->
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper" >

					<section id="content" class="animated fadeIn">

						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											<div class="text-center">
												<div class="content-header">
													<h2><?php if(isset($_GET['id'])): ?>Editer le produit <b class="text-primary"><?=$p['pro_code_produit']?></b><?php else: ?>Ajouter un <b class="text-primary">produit</b><?php endif; ?></h2>
												</div>
											</div>

											<div class="col-md-10 col-md-offset-1">

												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Informations générales</span>
														</div>
													</div>
												</div>

								<?php 			if(isset($_GET['error'])): ?>
													<div class="row">
														<div class="alert alert-danger">Le produit existe déjà !</div>
													</div>
								<?php 			endif; ?>


												<div class="row">
                                                    <div class="col-md-2">
                                                        <div class="section">
															<label style="margin-bottom:5px;"> Produit</label>
															<div class="field prepend-icon">
																<input type="text" name="pro_code" id="pro_code" class="gui-input chp_pro_code nom" placeholder="Produit" value="<?=$p['pro_code']?>" maxlength="6" >
																<label for="pro_code" class="field-icon">
																	<i class="fa fa-bookmark" aria-hidden="true"></i>
																</label>
															</div>
														</div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="section">
															<label for="pro_code_duree" style="margin-bottom:5px;"> Durée</label>
															<div class="field prepend-icon">
																<input type="text" name="pro_code_duree" id="pro_code_duree" class="gui-input chp_pro_code" placeholder="Durée" value="<?=$p['pro_code_duree']?>" >

															</div>
														</div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label style="margin-bottom:5px;">Heures ou jours</label>
                                                        <div class="sectio field select">

                                                            <select name="pro_code_unit" id="pro_code_unit" class="chp_pro_code" >
                                                                <option value="1"
                                                                <?php if($p['pro_code_unit'] == 1){ ?>
                                                                    selected
                                                                <?php } ?>
                                                                >Heures</option>
                                                                <option value="2"
                                                                <?php if($p['pro_code_unit'] == 2){ ?>
                                                                    selected
                                                                <?php } ?>
                                                                >Jours</option>
                                                            </select>
                                                            <i class="arrow simple"></i>
														</div>

                                                    </div>
                                                    <div class="col-md-3">
                                                        <label style="margin-bottom:5px;">Type</label>
                                                        <div class="section field select">

                                                            <select name="pro_code_type" id="pro_code_type" class="chp_pro_code" >
                                                                <option value="">Type</option>
                                                                <option value="R"
                                                                <?php if($p['pro_code_type'] == "R"){ ?>
                                                                    selected
                                                                <?php } ?>
                                                                >Recyclage</option>
                                                                <option value="I"
                                                                <?php if($p['pro_code_type'] == "I"){ ?>
                                                                    selected
                                                                <?php } ?>
                                                                >Initiale</option>
                                                                <option value="RAN"
                                                                <?php if($p['pro_code_type'] == "RAN"){ ?>
                                                                    selected
                                                                <?php } ?>
                                                                >Remise à niveau</option>
                                                                <option value="MC"
                                                                <?php if($p['pro_code_type'] == "MC"){ ?>
                                                                    selected
                                                                <?php } ?>
                                                                >Module complémentaire</option>
                                                                <option value="MAC"
                                                                <?php if($p['pro_code_type'] == "MAC"){ ?>
                                                                    selected
                                                                <?php } ?>
                                                                >Maintien actualisation compétences</option>
                                                                <option value="TEST"
                                                                <?php if($p['pro_code_type'] == "TEST"){ ?>
                                                                    selected
                                                                <?php } ?>
                                                                >TEST</option>
                                                            </select>
                                                            <i class="arrow simple"></i>
														</div>
                                                    </div>
													<div class="col-md-3">
														<div class="section">
															<label style="margin-bottom:5px;"> Code produit</label>
															<div class="field prepend-icon">
																<input type="text" name="pro_code_produit" id="pro_code_produit" class="gui-input" placeholder="Code produit" required value="<?=$p['pro_code_produit']?>" >
																<label for="pro_code_produit" class="field-icon">
																	<i class="fa fa-bookmark" aria-hidden="true"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="section">
															<label style="margin-bottom:5px;"> Libellé produit</label>
															<div class="field prepend-icon">
																<input type="text" name="pro_libelle" id="pro_libelle" class="gui-input" placeholder="Produit" required value="<?=$p['pro_libelle']?>" >
																<label for="pro_libelle" class="field-icon">
																	<i class="fa fa-bookmark" aria-hidden="true"></i>
																</label>
															</div>
														</div>
                                                    </div>
                                                </div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Tarification</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<div class="section mt30">
																	<label class="option">
																		<input type="checkbox" id="intra" <?=$att_check_intra?> >
																		<span class="checkbox"></span>
																		<label for="intra">Produit intra</label>
																	</label>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section" style="text-align:left!important;">
																	<label style="margin-bottom:5px;"> Prix Intra</label>
																	<div class="field append-icon">
																		<input type="text" name="pro_ht_intra" id="pro_ht_intra" class="prix-intra gui-input input-float" placeholder="Prix Intra" value="<?=$p['pro_ht_intra']?>" <?=$att_input_intra?> />
																		<label for="pro_ht_intra" class="field-icon">
																			<i class="fa fa-euro" aria-hidden="true"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section" style="text-align:left!important;">
																	<label style="margin-bottom:5px;">Prix mini commercial</label>
																	<div class="field append-icon">
																		<input type="text" name="pro_min_intra" id="pro_min_intra" class="gui-input input-float prix-intra" placeholder="Prix mini Intra" value="<?=$p['pro_min_intra']?>" <?=$att_input_intra?> />
																		<label for="pro_min_intra" class="field-icon">
																			<i class="fa fa-euro" aria-hidden="true"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<label for="" >% HT Intra</label>
																<input type="text" id="pro_min_intra_prct" class="gui-input input-float prix-intra" placeholder="% du TG INTRA" value="<?=$min_prct_intra?>" <?=$att_input_intra?> />
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section" >
																	<label>Prix mini DR</label>
																	<div class="field append-icon">
																		<input type="text" name="pro_min_dr_intra" id="pro_min_dr_intra" class="gui-input input-float prix-intra" placeholder="Prix mini Intra" value="<?=$p['pro_min_dr_intra']?>" <?=$att_input_intra?> />
																		<label for="pro_min_dr_intra" class="field-icon">
																			<i class="fa fa-euro" aria-hidden="true"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<label for="" >% HT Intra</label>
																<input type="text" id="pro_min_dr_intra_prct" class="gui-input input-float prix-intra" placeholder="% du TG INTRA" value="<?=$min_dr_prct_intra?>" <?=$att_input_intra?> />
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row" >
															<div class="col-md-12">
																<div class="section mt30" >
																	<label class="option">
																		<input type="checkbox" id="inter" <?=$att_check_inter?> />
																		<span class="checkbox"></span>
																		<label for="inter">Produit inter</label>
																	</label>
																</div>
															</div>
														</div>
														<div class="row" >
															<div class="col-md-12">
																<div class="section" style="text-align:left!important;">
																  <label style="margin-bottom:5px;"> Prix Inter</label>
																  <div class="field append-icon">
																	<input type="text" name="pro_ht_inter" id="pro_ht_inter" class="gui-input prix-inter input-float" placeholder="Prix Inter" value="<?=$p['pro_ht_inter']?>" <?=$att_input_inter?> />
																	<label for="pro_ht_inter" class="field-icon">
																	  <i class="fa fa-euro" aria-hidden="true"></i>
																	</label>
																  </div>
																</div>
															</div>
														</div>
														<div class="row" >
															<div class="col-md-6">
																<div class="section" style="text-align:left!important;">
																  <label style="margin-bottom:5px;"> Prix mini commercial</label>
																  <div class="field append-icon">
																	<input type="text" name="pro_min_inter" id="pro_min_inter" class="gui-input prix-inter input-float" placeholder="Prix Inter" value="<?=$p['pro_min_inter']?>" <?=$att_input_inter?> />
																	<label for="pro_min_inter" class="field-icon">
																	  <i class="fa fa-euro" aria-hidden="true"></i>
																	</label>
																  </div>
																</div>
															</div>
															<div class="col-md-6">
																<label for="" >% HT Inter</label>
																<input type="text" id="pro_min_inter_prct" class="gui-input input-float prix-inter" placeholder="% du TG INTER" value="<?=$min_prct_inter?>" <?=$att_input_inter?> />
															</div>
														</div>
														<div class="row" >
															<div class="col-md-6">
																<div class="section" style="text-align:left!important;">
																  <label style="margin-bottom:5px;"> Prix mini DR</label>
																  <div class="field append-icon">
																	<input type="text" name="pro_min_dr_inter" id="pro_min_dr_inter" class="gui-input input-float prix-inter" placeholder="Prix Inter" value="<?=$p['pro_min_dr_inter']?>" <?=$att_input_inter?> />
																	<label for="pro_min_dr_inter" class="field-icon">
																	  <i class="fa fa-euro" aria-hidden="true"></i>
																	</label>
																  </div>
																</div>
															</div>
															<div class="col-md-6">
																<label for="" >% HT Inter</label>
																<input type="text" id="pro_min_dr_inter_prct" class="gui-input input-float prix-inter" placeholder="% du TG INTER" value="<?=$min_dr_prct_inter?>" <?=$att_input_inter?> />
															</div>
														</div>
													</div>
												</div>
										   		<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Classification du produit</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label>Catégorie</label>
															<div class="field select">

																<select name="pro_categorie" id="pro_categorie" required="">
																	<option value="">Catégorie ...</option>
													  <?php 		if (isset($_GET['id'])):
																		echo get_categorie_select($p['pro_categorie']);
																	else:
																		echo get_categorie_select(0);
																	endif;?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
												</div>
												<div class="row" id="bloc_famille" <?php if($p['pro_categorie']!=1) echo("style='display:none;'"); ?> >


													<div class="col-md-4">
														<div class="section">
															<label>Famille</label>
															<div class="field select">
																<select name="pro_famille" id="pro_famille" onchange="sous_famille(this.value)">
																  <option value="">Famille ...</option>
																  <?php if (isset($_GET['id'])):
																  echo get_famille_select($p['pro_famille']);
																  else:
																	echo get_famille_select(0);
																  endif;?>


																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
													<div class="col-md-4">
														<div class="section">
															<label>Sous-Famille</label>
															<div class="field select">
																<select name="pro_sous_famille" id="pro_sous_famille" onchange="sous_sous_famille(this.value)">
																	  <option value="0">Sous-famille ...</option>
																	  <?php if (isset($_GET['id'])):
																	  echo get_sous_famille_select($p['pro_famille'], $p['pro_sous_famille']);
																	  endif; ?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
													<div class="col-md-4">
														<div class="section">
															<label>Sous-Famille</label>
															<div class="field select">
																<select name="pro_sous_sous_famille" id="pro_sous_sous_famille">
																	  <option value="0">Sous-Sous-famille ...</option>
																	  <?php if (isset($_GET['id'])):
																	  echo get_sous_sous_famille_select($p['pro_sous_famille'], $p['pro_sous_sous_famille']);
																	  endif; ?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
												</div>



												<div class="row">

													<div class="col-md-12">
														<div class="section">
														  <div class="option-group field">
															<label class="option option-primary">
															  <input type="radio" id="recur" name="pro_recurrent" value="1" <?php if($p['pro_recurrent'] == 1): ?>checked=""<?php endif; ?>>
															  <span class="radio"></span>Formation à refaire tous les:
															</label>
															<label class="option option-primary">
															  <input type="radio" id="deduc" name="pro_recurrent" value="2" <?php if($p['pro_qualifiant'] > 0): ?>checked=""<?php endif; ?>>
															  <span class="radio"></span>Qualification (diplômes, certificats...)
															</label>
															<label class="option option-primary">
															  <input type="radio" id="autre" name="pro_recurrent" value="pro_autre" <?php if(($p['pro_recurrent'] == 0 && $p['pro_qualifiant'] == 0)): ?>checked=""<?php endif; ?>>
															  <span class="radio"></span>Autre
															</label>
														  </div>
														</div>

													</div>
													<div class="col-md-12 recur">
														<div class="section">
															<div class="field prepend-icon">
																<input type="number" name="pro_recur_mois" id="pro_recur_mois" class="gui-input" placeholder="Nombre de mois de récurrence" value="<?=$p['pro_recur_mois']?>">
																<label for="pro_tva" class="field-icon">
																	<i class="fa fa-calendar" aria-hidden="true"></i>
																</label>
															</div>
														</div>
													</div>
													<div class="col-md-12 deduc">
														<div class="section">
															<label>Qualification</label>
															<div class="field select">
																<select name="pro_qualification" id="pro_qualification" onchange="qualification(this.value)">
																	<option value="">Qualification ...</option>
															  <?php	if (isset($_GET['id'])):
																		echo get_qualification_select($p['pro_qualification']);
																	else:
																		echo get_qualification_select(0);
																	endif; ?>
																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
														<div class="section">
															<label>Nombre de mois de qualification</label>
															<div class="field prepend-icon">
																<input type="number" name="pro_qualif_mois" id="pro_qualif_mois" class="gui-input" placeholder="Nombre de mois de qualification" value="<?=$p['pro_qualif_mois']?>">
																<label for="pro_qualif_mois" class="field-icon">
																	<i class="fa fa-calendar" aria-hidden="true"></i>
																</label>
															</div>
														</div>
													</div>
												</div>

												<div class="row">

													  <div class="col-md-3">
														<div class="section">
														<label>Niveau</label>
														  <div class="field select">
															<select name="pro_niveau" id="pro_niveau">
															  <option value="">Niveau ...</option>
															  <?php if (isset($_GET['id'])):
															  echo get_niveau_select($p['pro_niveau']);
															  else:
																echo get_niveau_select(0);
															  endif; ?>


															</select>
															<i class="arrow simple"></i>
														  </div>
														</div>

													  </div>
													  <div class="col-md-3">
														<div class="section">
															<label>Nombre de demi-jours</label>
														  <div class="field prepend-icon">
															<input type="number" min="0" step="1" name="pro_nb_demi_jour" id="pro_nb_demi_jour" class="gui-input" placeholder="Nombre de demi-jours" value="<?=$p['pro_nb_demi_jour']?>">
															<label for="pro_nb_demi_jour" class="field-icon">
															  <i class="fa fa-calendar" aria-hidden="true"></i>
															</label>
														  </div>
														</div>
													  </div>
													  <div class="col-md-3">
														<div class="section">
															<label>Durée des demi-jours (en heures)</label>
														  <div class="field prepend-icon">
															<input type="number" min="0" pattern="[0-9]+([\,|\.][0-9]+)?" step="0.25" name="pro_duree_demi_jour" id="pro_duree_demi_jour" class="gui-input" placeholder="Durée des demi-jours (en heures)" value="<?=$p['pro_duree_demi_jour']?>">
															<label for="pro_duree_demi_jour" class="field-icon">
															  <i class="fa fa-calendar" aria-hidden="true"></i>
															</label>
														  </div>
														</div>
													  </div>
													  <div class="col-md-3">
														<div class="section">
														<label>Durée de preparation (en heures)</label>
														  <div class="field prepend-icon">
															<input type="number" min="0" pattern="[0-9]+([\,|\.][0-9]+)?" step="0.25" name="pro_duree_prep" id="pro_duree_prep" class="gui-input" placeholder="Durée de preparation (en heures)" value="<?=$p['pro_duree_prep']?>">
															<label for="pro_duree_prep" class="field-icon">
															  <i class="fa fa-calendar" aria-hidden="true"></i>
															</label>
														  </div>
														</div>
													  </div>
												</div>
                                                <div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Gestion des stagiaires</span>
														</div>
													</div>
												</div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="section">
														  <div class="option-group field">
															<label class="option option-primary">
															  <input type="radio" name="pro_gest_sta" value="1" <?php if(empty($p['pro_gestion_sta']) OR $p['pro_gestion_sta'] == 1): ?>checked=""<?php endif; ?>>
															  <span class="radio"></span>Participe à toutes les sessions
															</label>
															<label class="option option-primary">
															  <input type="radio" name="pro_gest_sta" value="2" <?php if($p['pro_gestion_sta'] > 1): ?>checked=""<?php endif; ?>>
															  <span class="radio"></span>Participe à une session
															</label>
														  </div>
														</div>
                                                    </div>
                                                </div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Produit spécifique</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-2 text-right pt10"> Client :</div>
													<div class="col-md-9">
														<div class="section" >
															<select name="pro_client" id="pro_client" class="select2-client" data-groupe="1" >
																<option value='0'>Client</option>
													<?php		if(!empty($d_client)){
																	echo("<option value='" . $d_client["cli_id"] . "' selected >" .  $d_client["cli_nom"]. " (" . $d_client["cli_code"] . ")</option>");
																} ?>

															</select>
														</div>
													</div>
													<div class="col-md-1 pt5">
														<button type="button" class="btn btn-sm btn-danger" id="supp_client" >
															<i class="fa fa-times" ></i>
														</button>
													</div>
												</div>

												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Comptabilité</span>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-md-2 text-right pt10"> TVA :</div>
													<div class="col-md-4">
														<div class="section">
															<div class="field select">
																<select name="pro_tva" id="pro_tva" required >
																  <option value="">TVA ...</option>
																  <?php foreach($base_tva as $k => $v): ?>
																	<?php if($k >0): ?>
																	  <option value="<?=$k?>" <?php if(isset($p['pro_tva']) && $p['pro_tva'] == $k): ?> selected<?php endif; ?>><?= $v ?></option>
																	<?php endif; ?>
																  <?php endforeach; ?>


																</select>
																<i class="arrow simple"></i>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">
															<label class="option">
																<input type="checkbox" name="pro_deductible" id="pro_deductible" <?php if(!empty($p['pro_deductible'])) echo("checked"); ?> value="on" />
																<span class="checkbox"></span>
																<label for="pro_deductible">Déductible</label>
															</label>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Challenge commercial</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<label>Nb. point "hors dérogation"</label>
														<input type="text" name="pro_pt_0" id="pro_pt_0" class="gui-input input-float" placeholder="point 'hors dérogation'" value="<?=$p['pro_pt_0']?>" />
													</div>
													<div class="col-md-4">
														<label>Nb. point dérogation N1</label>
														<input type="text" name="pro_pt_1" id="pro_pt_1" class="gui-input input-float" placeholder="point dérogation N1" value="<?=$p['pro_pt_1']?>" <?php if(empty($p['pro_pt_0'])) echo("disabled"); ?> />
													</div>
													<div class="col-md-4">
														<label>Nb. point dérogation N2</label>
														<input type="text" name="pro_pt_2" id="pro_pt_2" class="gui-input input-float" placeholder="point dérogation N2" value="<?=$p['pro_pt_2']?>" <?php if(empty($p['pro_pt_0'])) echo("disabled"); ?> />
													</div>
												</div>


												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Autres Infos</span>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-md-6">
														<p><small>Information à destination des clients a afficher sur les devis</small></p>
														<div class="section">
															<label class="field prepend-icon">
																<textarea class="summernote-img" id="pro_devis_txt" name="pro_devis_txt" placeholder="Info client"><?=$p['pro_devis_txt']?></textarea>
															</label>
														</div>
													</div>
													<div class="col-md-6">
														<p><small>Information à destination des stagiaires a afficher sur les convocations</small></p>
														<div class="section">
															<label class="field prepend-icon">
																<textarea class="summernote-img" id="pro_reference_sta" name="pro_reference_sta" placeholder="Info stagiaire"><?=$p['pro_reference_sta']?></textarea>
															</label>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label class="option">
																<input type="checkbox" name="pro_rapport_evac" id="pro_rapport_evac" value="pro_rapport_evac" <?php if(!empty($p['pro_rapport_evac'])): ?>checked<?php endif; ?>>
																<span class="checkbox"></span>
																<label for="pro_rapport_evac">Nécessite un rapport d'évacuation</label>
															</label>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">
															<label class="option">
																<input type="checkbox" name="pro_archive" id="pro_archive" value="on" <?php if(!empty($p['pro_archive'])) echo("checked"); ?> >
																<span class="checkbox"></span>
																<label for="pro_deductible">Archivé</label>
															</label>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label class="option">
																<input type="checkbox" name="pro_vehicule" id="pro_vehicule" value="pro_vehicule" <?php if(!empty($p['pro_vehicule'])): ?>checked<?php endif; ?>>
																<span class="checkbox"></span>
																<label for="pro_vehicule">Nécessite un véhicule</label>
															</label>
														</div>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End: Content -->
				</section>
				<!-- End: Content WRAPPER -->

			</div>
			<!-- End: Main -->

			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="<?= $_SESSION["retour"] ?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" name="submit" id="form_submit" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>

		</form>
  <?php

		include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="assets/js/custom.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
		<script type="text/javascript">

			jQuery(document).ready(function (){

				// calcul du code

				var code_produit="";
				var code_duree="";
				var code_id="";
		<?php	if(!empty($_GET['id'])){ ?>
					code_id="<?=$_GET['id']?>";
		<?php	} ?>

				$('.chp_pro_code').change(function(){

					code_duree="";
					if($('#pro_code_duree').val()!=""){
						$('#pro_code_duree').val($('#pro_code_duree').val().replace(",","."));
						if($('#pro_code_unit').val()==2){
							// J
							code_duree=$('#pro_code_duree').val() + "J";
						}else{
							// H
							min=$('#pro_code_duree').val() % 1;
							code_duree=$('#pro_code_duree').val()-min + "H";
							if(min>0){
								code_duree=code_duree + 60*min;
							}
						}
					};
					code_produit=$('#pro_code').val();
					if(($('#pro_code_duree').val()!="")&&($('#pro_code_duree').val()!=0)){
						code_produit=code_produit + "-" + code_duree;
					}
					if($('#pro_code_type').val()!=""){
						code_produit=code_produit + "-" + $('#pro_code_type').val();
					}
					if(code_id!=""){
						code_produit=code_produit + "-" + code_id;
					}
					$('#pro_code_produit').val(code_produit);
				});


				$("#intra").change(function() {
					if($(this).is(":checked")){
						$(".prix-intra").attr("disabled", false);
						$(".prix-intra").attr("required", true);
					}else{
						$(".prix-intra").val('');
						$(".prix-intra").attr("disabled", true);
						$(".prix-intra").attr("required", false);
					}
				});

				$("#inter").change(function() {
					if($(this).is(":checked")){
						$(".prix-inter").attr("disabled", false);
						$(".prix-inter").attr("required", true);
					}else{
						$(".prix-inter").val('');
						$(".prix-inter").attr("disabled", true);
						$(".prix-inter").attr("required", false);
					}
				});


				// calcul intra / inter
				$("#pro_min_intra").blur(function(){
					calcul_prct("min_intra","intra");
				});
				$("#pro_min_intra_prct").blur(function(){
					calcul_prix("min_intra","intra");
				});
				$("#pro_min_dr_intra").blur(function(){
					calcul_prct("min_dr_intra","intra");
				});
				$("#pro_min_dr_intra_prct").blur(function(){
					calcul_prix("min_dr_intra","intra");
				});

				$("#pro_min_inter").blur(function(){
					calcul_prct("min_inter","inter");
				});
				$("#pro_min_inter_prct").blur(function(){
					calcul_prix("min_inter","inter");
				});
				$("#pro_min_dr_inter").blur(function(){
					calcul_prct("min_dr_inter","inter");
				});
				$("#pro_min_dr_inter_prct").blur(function(){
					calcul_prix("min_dr_inter","inter");
				});



				$("#pro_categorie").change(function(){
					if($(this).val()==1){
						$("#bloc_famille").show();

					}else{
						$("#pro_famille").val("");
						$("#pro_sous_famille").val(0);
						$("#pro_sous_sous_famille").val(0);
						$("#bloc_famille").hide();
					}
				});

				if($('#deduc').is(':checked')) {
					$(".deduc").show();
					$(".recur").hide();
				}
				if($('#recur').is(':checked')) {
				  $(".deduc").hide();
				  $(".recur").show();
				}
				if($('#autre').is(':checked')) {
				  $(".deduc").hide();
				  $(".recur").hide();
				}
				$('#deduc').click(function() {
				 $(".deduc").show();
				 $(".recur").hide();
			   });
				$('#autre').click(function() {
				 $(".deduc").hide();
				 $(".recur").hide();
			   });
				$('#recur').click(function() {
				 $(".deduc").hide();
				 $(".recur").show();
			   });

				//$("#form_produit").validate();
				$("#form_produit").submit(function(){
					console.log("pro_famille : " + $("#pro_famille").val());
					if($("#pro_categorie").val()==1 && $("#pro_famille").val()==""){
						$("#pro_famille").rules("add", {
							required: true
						});
                    }
				});

				$("#supp_client").click(function(){
					$("#pro_client").select2("val",0);
				});

				$("#pro_pt_0").keyup(function(){
					if($(this).val()!=""){
						float_value=$(this).val().replace(",",".");
						if($.isNumeric(float_value)){
							if(float_value>0){
								$("#pro_pt_1").prop("disabled",false);
								$("#pro_pt_2").prop("disabled",false);
							}
						}
					}
				});

				$("#pro_pt_0").blur(function(){
					if($(this).val()=="" || $(this).val()==0){
						$("#pro_pt_1").val(0);
						$("#pro_pt_1").prop("disabled",true);
						$("#pro_pt_2").val(0);
						$("#pro_pt_2").prop("disabled",true);
					}
				});

			});

			function qualification(selected_id){
				$.ajax({
				  type:'POST',
				  url: '/ajax/ajax_qualification.php',
					  //the script to call to get data
					  data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
					//for example "id=5&parent=6"

					  dataType: 'json',                //data format
					  success: function(data)          //on recieve of reply
					  {


						$('#pro_qualif_mois').attr("value", data["qua_duree"]);

					  }


					});
			}

			function sous_famille(selected_id){
				//-----------------------------------------------------------------------
				// 2) Send a http request with AJAX http://api.jquery.com/jQuery.ajax/
				//-----------------------------------------------------------------------

				$.ajax({
				  type:'POST',
				  url: 'ajax/ajax_sous_famille.php',
				  //the script to call to get data
				  data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
				  //for example "id=5&parent=6"

				  dataType: 'json',                //data format
				  success: function(data)          //on recieve of reply
				  {

					if (data['0'] == undefined){

					  $("#pro_sous_famille").find("option:gt(0)").remove();

					}else{
					  $("#pro_sous_famille").find("option:gt(0)").remove();
					  $.each(data, function(key, value) {
						$('#pro_sous_famille')
						.append($("<option></option>")
						  .attr("value",value["psf_id"])
						  .text(value["psf_libelle"]));
					  });
					}
				  }
				});
			}
			function sous_sous_famille(selected_id){
				//-----------------------------------------------------------------------
				// 2) Send a http request with AJAX http://api.jquery.com/jQuery.ajax/
				//-----------------------------------------------------------------------

				$.ajax({
				  type:'POST',
				  url: 'ajax/ajax_sous_sous_famille.php',
				  //the script to call to get data
				  data : 'field=' + selected_id,                    //you can insert url argumnets here to pass to api.php
				  //for example "id=5&parent=6"

				  dataType: 'json',                //data format
				  success: function(data)          //on recieve of reply
				  {

					if (data['0'] == undefined){

					  $("#pro_sous_sous_famille").find("option:gt(0)").remove();

					}else{
					  $("#pro_sous_sous_famille").find("option:gt(0)").remove();
					  $.each(data, function(key, value) {
						$('#pro_sous_sous_famille')
						.append($("<option></option>")
						  .attr("value",value["pss_id"])
						  .text(value["pss_libelle"]));
					  });
					}
				  }
				});
			}

			function envoyer_form(){
				alerte="";
				if($("#pro_categorie").val()=="" || $("#pro_categorie").val()==0){
					alerte+="<li>Vous devez selectionner une categorie.</li>";
				}
				if($("#pro_famille").val()=="" || $("#pro_famille").val()==0){
					alerte+="<li>Vous devez selectionner une famille</li>.";
				}
				console.log(alerte);
				if(alerte==""){
					console.log("ENVOIE");
					$("#form_produit").submit();
				}else{
					message="Impossible de continuer pour les raisons suivantes :";
					message+="<ul>";
						message+=alerte;
					message+="</ul>";
					modal_alerte_user(message);
				}
			}

			function calcul_prct(libelle,src){
				tg=0;
				if($("#pro_ht_" + src).val()!=""){
					tg=$("#pro_ht_" + src).val();
				};
				prix=0;
				if($("#pro_" + libelle).val()!=""){
					prix=$("#pro_" + libelle).val();
				};
				if(tg!=0){
					prct=(prix/tg)*100;
				}
				$("#pro_" + libelle + "_prct").val(prct);
			}

			function calcul_prix(libelle,src){
				tg=0;
				if($("#pro_ht_" + src).val()!=""){
					tg=$("#pro_ht_" + src).val();
				};
				prct=0;
				if($("#pro_" + libelle + "_prct").val()!=""){
					prct=$("#pro_" + libelle + "_prct").val();
				};
				prix=parseFloat((tg*prct)/100).toFixed(2);
				$("#pro_" + libelle).val(prix);
				$("#pro_" + libelle).trigger("blur");
			};

		</script>
	</body>
</html>
