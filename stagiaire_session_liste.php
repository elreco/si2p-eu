<?php  
//////////////// INCLUDES /////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php'); 
require "modeles/mod_erreur.php";
//////////////// REQUETES VERS LE SERVEUR /// ///////////////
if(!empty($_POST)){
    $mil="";
    if(!empty($_POST['sta_nom'])){
        $mil.=" AND sta_nom LIKE :sta_nom";
        $critere["sta_nom"]="%" . $_POST['sta_nom'] . "%"; 
    };

    if(!empty($_POST['sta_prenom'])){
        $mil.=" AND sta_prenom LIKE :sta_prenom";
        $critere["sta_prenom"]="%" . $_POST['sta_prenom'] . "%"; 
    };

    if(!empty($_POST['ses_date'])){
        $ses_date = convert_date_sql($_POST['ses_date']);
        $mil.=" AND ses_date LIKE :ses_date";
        $critere["ses_date"]=$ses_date; 
    };

    $sql="SELECT DISTINCT * FROM sessions_stagiaires 
    LEFT JOIN sessions ON sessions.ses_id = sessions_stagiaires.sst_session 
    LEFT JOIN stagiaires ON stagiaires.sta_id = sessions_stagiaires.sst_stagiaire ";
    if($mil!=""){
        $sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
    }
    $req = $Conn->prepare($sql);
    if(!empty($critere)){
        foreach($critere as $c => $v){
          $req->bindValue($c,$v);
        }
    };
    
    $req->execute();
    $stagiaires = $req->fetchAll();
}else{
    $req = $Conn->prepare("SELECT DISTINCT * FROM sessions_stagiaires 
    LEFT JOIN sessions ON sessions.ses_id = sessions_stagiaires.sst_session 
    LEFT JOIN stagiaires ON stagiaires.sta_id = sessions_stagiaires.sst_stagiaire");
    $req->execute();
    $stagiaires = $req->fetchAll();
}



?>

<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>Si2P - ORION</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="Si2P">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->

<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

<link rel="shortcut icon" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="sb-top sb-top-sm">
    <!-- Start: Main -->
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            
            
            <header id="topbar">
                <div class="text-center">
                    <h4 style="margin:0;">Liste des <small>stagiaires</small></h4>
                </div>
             
              
            </header>
            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">
                <!-- FORMULAIRE RECHERCHE -->
                <form action="stagiaire_session_liste.php" method="POST">
                <div class="row mb20 admin-form">
                    <div class="col-md-3">
                   
                        <label for="fpr_type">Nom du stagiaire :</label>
                        
                        <input type="text" name="sta_nom" class="gui-input" placeholder="Nom du stagiaire" <?php if(!empty($_POST['sta_nom'])){ ?> value="<?= $_POST['sta_nom'] ?>" <?php } ?>/>
                    </div>
                    <div class="col-md-3">
                        <label for="fpr_type">Prénom du stagiaire :</label>
                        
                        <input type="text" name="sta_prenom" class="gui-input" placeholder="Prénom du stagiaire" <?php if(!empty($_POST['sta_prenom'])){ ?> value="<?= $_POST['sta_prenom'] ?>" <?php } ?>/>
                    </div>
                    <div class="col-md-3">
                        <label for="fpr_type">Date de la session :</label>
                        
                        
                                              
                                               
                        
                            <input type="text" name="ses_date" id="date_session" class="gui-input datepicker date" placeholder="Selectionner une date" <?php if(!empty($_POST['ses_date'])){ ?> value="<?= $_POST['ses_date'] ?>" <?php } ?>/>
                            
                       
                                              
                                            
                    </div>
                    

                    <div class="col-sm-1">
                        <button type="submit" name="search_produit" class="btn btn-primary mt20">Filtrer</button>
                        
                        
                        
                    </div>
                    <?php if(!empty($_POST)){ ?>
                        <div class="col-sm-2">
                            <a href="stagiaire_session_liste.php" class="btn btn-info mt20">Remettre à zéro</a>
                        </div> 
                    <?php } ?>
                </div>
                <!-- FIN FORMULAIRE RECHERCHE -->
            <?php if(!empty($stagiaires)){ ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="table_id">
                        <thead>
                            <tr class="dark">
                                <th>Stagiaire</th>
                                <th>Session</th> 
                                <th>Document</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($stagiaires as $s){ ?>
                       
                            <tr>
                                <td>
                                   <?= $s['sta_prenom'] ?> <?= $s['sta_nom'] ?>
                                </td>
                                <td>
                                    <?= convert_date_fr($s['ses_date']) ?> de <?= $s['ses_h_deb'] ?> à <?= $s['ses_h_fin'] ?>
                                </td>
                                <td>
                                    <a href="documents/sessions_stagiaires/<?= $s['sst_fichier'] ?>.<?= $s['sst_ext'] ?>" class="btn btn-sm btn-warning" download data-toggle="tooltip" data-title="Télécharger le <?= $s['sst_ext'] ?>"><i class="fa fa-download"></i></a>
                                </td>
                                
                        
                            </tr>

                        <?php } ?>

                        </tbody>
                    </table>

                </div>
            <?php }else{ ?>
                <div class="col-md-12 text-center" style="padding:0;" >
                    <div class="alert alert-warning" style="border-radius:0px;">
                      Aucun stagiaire.
                    </div>
                </div>
            <?php } ?>
            </section>
        <!-- End: Content -->
        </section>
    </div>

    <!-- End: Main -->
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">
              
              
            </div>
            <div class="col-xs-6 footer-middle">&nbsp;</div>
            <div class="col-xs-3 footer-right">
            
                <a href="stagiaire_session.php" class="btn btn-success btn-sm" role="button">
                    <span class="fa fa-plus"></span>
                    <span class="hidden-xs">Nouveau stagiaire</span>
                </a>
            

            </div>
        </div>
    </footer>
<?php
include "includes/footer_script.inc.php"; ?>  
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
/////////////////////// INTERACTIONS UTILISATEUR /////////////////////////
jQuery(document).ready(function () {
});
</script>
</body>
</html>
