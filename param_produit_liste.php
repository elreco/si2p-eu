<?php


include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';


include_once 'modeles/mod_orion_pro_categories.php';
include_once 'modeles/mod_orion_pro_familles.php';
include_once 'modeles/mod_orion_pro_s_familles.php';
include_once 'modeles/mod_orion_qualifications.php';



	// TRAITEMENT DU FORM

	if(isset($_POST['search'])){
		

		$pro_categorie=0;
		if(!empty($_POST['pro_categorie'])){
			$pro_categorie=intval($_POST['pro_categorie']);
		}

		$pro_famille=0;
		if(!empty($_POST['pro_famille'])){
			$pro_famille=intval($_POST['pro_famille']);
		}

		$pro_sous_famille=0;
		if(!empty($_POST['pro_sous_famille'])){
			$pro_sous_famille=intval($_POST['pro_sous_famille']);
		}
		
		$pro_sous_sous_famille=0;
		if(!empty($_POST['pro_sous_sous_famille'])){
			$pro_sous_sous_famille=intval($_POST['pro_sous_sous_famille']);
		}
		
		$pro_deductible=null;
		if(!empty($_POST['pro_deductible'])){
			$pro_deductible = 1;
		}

		$pro_archive=0;
		if(!empty($_POST['pro_archive'])){
			$pro_archive=1;
		}

		$pro_tri= array(
			"pro_categorie" => $pro_categorie,
			"pro_famille" => $pro_famille,
			"pro_sous_famille" => $pro_sous_famille,
			"pro_sous_sous_famille" => $pro_sous_sous_famille,
			"pro_deductible" => $pro_deductible,
			"pro_archive" => $pro_archive
		);

		$_SESSION['pro_tri'] =$pro_tri;

	}else{
		if(!empty($_SESSION['pro_tri'])){
			$pro_tri=$_SESSION['pro_tri'];
		}
	}
	
	if(isset($pro_tri)){
		
		if($pro_tri["pro_categorie"]>1 OR !empty($pro_tri["pro_deductible"]) OR !empty($pro_tri["pro_archive"]) OR ($pro_tri["pro_categorie"]==1 AND !empty($pro_tri["pro_famille"])) ){

			$sql="SELECT * FROM produits";
			$mil="";
			if(!empty($pro_tri["pro_categorie"])){
				$mil.=" AND pro_categorie=" . $pro_tri["pro_categorie"];
			}
			if(!empty($pro_tri["pro_famille"])){
				$mil.=" AND pro_famille =" . $pro_tri["pro_famille"];
			}
			if(!empty($pro_tri["pro_sous_famille"])){
				$mil.=" AND pro_sous_famille = " . $pro_tri["pro_sous_famille"];
			}
			 if(!empty($pro_tri["pro_sous_sous_famille"])){
				$mil.=" AND pro_sous_sous_famille = " . $pro_tri["pro_sous_sous_famille"];
			}
			if($pro_tri["pro_deductible"] == 1){
				$mil.=" AND pro_deductible = 1";
			}
			if($pro_tri["pro_archive"]==1){
				$mil.=" AND pro_archive = 1";
			}else{
				$mil.=" AND pro_archive = 0";
			}
			if($mil!=""){
				$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
			}
			$sql.=" ORDER BY pro_code_produit";
			/*echo($sql);
			die();*/
			$req = $Conn->query($sql);
			$produits = $req->fetchAll();
		}else{
			$produits=array();
		}
	}else{
		
		$pro_tri= array(
			"pro_categorie" => 0,
			"pro_famille" => 0,
			"pro_sous_famille" => 0,
			"pro_sous_sous_famille" => 0,
			"pro_deductible" => 0,
			"pro_archive" => 0
		);
	}
	
	$orion_pro_categories=orion_produits_categories();
	$orion_pro_familles=orion_produits_familles();
	$orion_pro_s_familles=orion_produits_sous_familles();
	$orion_pro_s_s_familles=orion_produits_sous_sous_familles();
	
	// LE PLAN COMPTABLES
	
	  $sql="SELECT * FROM Comptes WHERE cpt_numero LIKE '7%' ORDER BY cpt_numero";
	  $req=$Conn->query($sql);
	  $d_comptes=$req->fetchAll();
	
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />


		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm" >

		<!-- Start: Main -->
		<div id="main">
			<?php
			include "includes/header_def.inc.php";
			?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper">

				<section id="content" class="animated fadeIn">
				
					<div class="admin-form theme-primary ">
						<div class="panel heading-border panel-primary">
							<div class="panel-body bg-light">
							
								<div class="content-header">
									<h2>Base produit : Mise à jour des <b class="text-primary">données comptables</b></h2>
								</div>
							
								<form method="post" action="param_produit_liste.php" id="form" >
								
									<div class="row">
										<div class="col-md-3">
											<label for="pro_categorie" >Catégorie :</label>
											<div class="field select">											
												<select name="pro_categorie" id="pro_categorie" >
													<option value="">Catégorie ...</option>
											<?php 	foreach($orion_pro_categories as $cat => $d_cat){
														if($cat==$pro_tri["pro_categorie"]){
															echo("<option value='" . $cat . "' selected >" . $d_cat["pca_libelle"] . "</option>");
														}else{
															echo("<option value='" . $cat . "' >" . $d_cat["pca_libelle"] . "</option>");
														} 
													} ?>
												</select>
												<i class="arrow simple"></i>
											</div>
										</div>
										
										<div class="col-md-3 pt10 text-center">
											<label class="option">
												<input type="checkbox" name="pro_deductible" id="pro_deductible" value="pro_deductible" <?php if(!empty($pro_tri['pro_deductible'])): ?> checked <?php endif; ?>>
												<span class="checkbox"></span>
												<label for="pro_deductible">Déductible</label>
											</label>
										</div>
										
										<div class="col-md-3 pt10 text-center">                          
											<label class="option">
												<input type="checkbox" name="pro_archive" id="pro_archive" value="pro_archive" <?php if(!empty($pro_tri['pro_archive'])): ?> checked <?php endif; ?>>
												<span class="checkbox"></span>
												<label for="pro_archive">Archivé</label>
											</label>
										</div>
										
										<div class="col-md-3 text-center">
											<button type="submit" class="btn btn-sm btn-info" name="search" >
												<i class="fa fa-search" ></i> Afficher les produits
											</button>
										</div>
									</div>
									 
									<div class="row mt20" id="bloc_famille" <?php if($pro_tri["pro_categorie"]!=1) echo("style='display:none;'"); ?> >						
										<div class="col-md-3">
											<label for="pro_famille" >Famille :</label>
											<select name="pro_famille" id="pro_famille" class="select2 select2-famille" >
												<option value="0">Famille ...</option>
									<?php 			foreach($orion_pro_familles as $fam => $d_fam){
														if($fam==$pro_tri["pro_famille"]){
															echo("<option value='" . $fam . "' selected >" . $d_fam["pfa_libelle"] . "</option>");
														}else{
															echo("<option value='" . $fam . "' >" . $d_fam["pfa_libelle"] . "</option>");
														} 
													} ?>
											</select>
										</div>                      
										<div class="col-md-3">	
											<label for="pro_sous_famille" >Sous-famille :</label>
											<select name="pro_sous_famille" id="pro_sous_famille" class="select2 select2-famille-sous-famille" >
												<option value="">Sous-famille ...</option>
									<?php		if($pro_tri["pro_famille"]>0){
													foreach($orion_pro_s_familles as $s_fam => $d_s_fam){
														if($d_s_fam["psf_pfa_id"]==$pro_tri["pro_famille"]){
															if($s_fam==$pro_tri["pro_sous_famille"]){
																echo("<option value='" . $s_fam . "' selected >" . $d_s_fam["psf_libelle"] . "</option>");
															}else{
																echo("<option value='" . $s_fam . "' >" . $d_s_fam["psf_libelle"] . "</option>");
															} 
														}
													}
												} ?>
											</select>
										</div>
										<div class="col-md-3">	
											<label for="pro_sous_sous_famille" >Sous-Sous-famille :</label>
											<select name="pro_sous_sous_famille" id="pro_sous_sous_famille" class="select2 select2-famille-sous-sous-famille">
												<option value="">Sous-Sous-famille ...</option>		
									<?php		if($pro_tri["pro_sous_famille"]>0){
													foreach($orion_pro_s_s_familles as $s_s_fam => $d_s_s_fam){
														if($d_s_s_fam["pss_psf_id"]==$pro_tri["pro_sous_famille"]){
															if($s_s_fam==$pro_tri["pro_sous_sous_famille"]){
																echo("<option value='" . $s_s_fam . "' selected >" . $d_s_s_fam["pss_libelle"] . "</option>");
															}else{
																echo("<option value='" . $s_s_fam . "' >" . $d_s_s_fam["pss_libelle"] . "</option>");
															} 
														}
													}
												} ?>												
											</select>
										</div>								
									</div>
								
						
									<h1>Liste des prouits</h1>
									
							<?php	if(!empty($produits)){ ?>
										<div class="table-responsive mt15">
											<table class="table table-striped table-hover">
												<thead>
													<tr class="dark" >
														<th rowspan="2" >Catégorie</th>
														<th rowspan="2" >Famille</th>
														<th rowspan="2" >Sous-famille</th>
														<th rowspan="2" >Sous-sous-famille</th>
														<th rowspan="2" >Libellé</th>
														<th rowspan="2" >Code</th>
														<th rowspan="2"  >Archivé</th>
														<th class="text-center" rowspan="2"  >Déductible</th>
														<th class="text-center" colspan="2" >NON EXO.</th>
														<th class="text-center" colspan="2" >EXO.</th>
													</tr>
													<tr>
														<th class="text-center" >Compte "hors-groupe"</th>
														<th class="text-center" >Compte "groupe"</th>		
														<th class="text-center" >Compte "hors-groupe"</th>
														<th class="text-center" >Compte "groupe"</th>														
													</tr>
												</thead>									
												<tbody>
									<?php 			foreach($produits as $s){
														$cat_lib="";
														if(!empty($s['pro_categorie'])){
															$cat_lib=$orion_pro_categories[$s['pro_categorie']]["pca_libelle"];
														}
														$fam_lib="";
														if(!empty($s['pro_famille'])){
															$fam_lib=$orion_pro_familles[$s['pro_famille']]["pfa_libelle"];
														}
														$s_fam_lib="";
														if(!empty($s['pro_sous_famille'])){
															$s_fam_lib=$orion_pro_s_familles[$s['pro_sous_famille']]["psf_libelle"];
														}
														$s_s_fam_lib="";
														if(!empty($s['pro_sous_sous_famille'])){
															$s_s_fam_lib=$orion_pro_s_s_familles[$s['pro_sous_sous_famille']]["pss_libelle"];
															//$s_s_fam_lib="Ordinateurs";
														}										
														$style="";
														if(!empty($orion_pro_s_s_familles[$s['pro_sous_sous_famille']]["pss_couleur_bg"])){
															$style="background-color:" . $orion_pro_s_s_familles[$s['pro_sous_sous_famille']]["pss_couleur_bg"] . ";";
															if(!empty($orion_pro_s_s_familles[$s['pro_sous_sous_famille']]["pss_couleur_txt"])){
																$style.="color:" . $orion_pro_s_s_familles[$s['pro_sous_sous_famille']]["pss_couleur_txt"] . ";";
															}
														}elseif(!empty($orion_pro_s_familles[$s['pro_sous_famille']]["psf_couleur_bg"])){
															$style="background-color:" . $orion_pro_s_familles[$s['pro_sous_famille']]["psf_couleur_bg"] . ";";
															if(!empty($orion_pro_s_familles[$s['pro_sous_famille']]["psf_couleur_txt"])){
																$style.="color:" . $orion_pro_s_familles[$s['pro_sous_famille']]["psf_couleur_txt"] . ";";
															}
														} ?>
														<tr>
															<td>
																<?=$cat_lib?>
																<input type="hidden" name="produit_<?=$s["pro_id"]?>" value="<?=$s["pro_id"]?>" />
															</td>
															<td><?=$fam_lib?></td>
															<td><?=$s_fam_lib?></td>
															<td><?=$s_s_fam_lib?></td>
															<td><?= $s['pro_libelle'] ?></td>
															<td style="<?=$style?>" ><?= $s['pro_code_produit'] ?></td>
															<td class="text-center" >
														<?php	if(!empty($s['pro_archive'])){
																	echo("OUI");
																}else{
																	echo("NON");
																} ?>
															</td>
															<td class="text-center" >
																<label class="option">
																	<input type="checkbox" name="pro_deductible_<?=$s["pro_id"]?>" value="on" <?php if(!empty($s['pro_deductible'])) echo("checked"); ?> />
																	<span class="checkbox"></span>
																</label>
															</td>															
															<td class="text-center" >
																<select name="pro_ven_cpt_<?=$s["pro_id"]?>" class="select2">
																	<option value="">Compte "hors-groupe" ...</option>		
														<?php		foreach($d_comptes as $compte){
																		if($s['pro_ven_cpt']==$compte["cpt_id"]){
																			echo("<option value='" . $compte["cpt_id"] . "' selected >" . $compte["cpt_numero"] . "</option>");
																		}else{
																			echo("<option value='" . $compte["cpt_id"] . "' >" . $compte["cpt_numero"] . "</option>");
																		} 
																	} ?>												
																</select>
															</td>
															<td class="text-center" >
																<select name="pro_ven_cpt_si2p_<?=$s["pro_id"]?>" class="select2">
																	<option value="">Compte "groupe" ...</option>		
														<?php		foreach($d_comptes as $compte){
																		if($s['pro_ven_cpt_si2p']==$compte["cpt_id"]){
																			echo("<option value='" . $compte["cpt_id"] . "' selected >" . $compte["cpt_numero"] . "</option>");
																		}else{
																			echo("<option value='" . $compte["cpt_id"] . "' >" . $compte["cpt_numero"] . "</option>");
																		} 
																	} ?>												
																</select>
															</td>
															
															<td class="text-center" >
																<select name="pro_ven_cpt_exo_<?=$s["pro_id"]?>" class="select2">
																	<option value="">Compte "hors-groupe" ...</option>		
														<?php		foreach($d_comptes as $compte){
																		if($s['pro_ven_cpt_exo']==$compte["cpt_id"]){
																			echo("<option value='" . $compte["cpt_id"] . "' selected >" . $compte["cpt_numero"] . "</option>");
																		}else{
																			echo("<option value='" . $compte["cpt_id"] . "' >" . $compte["cpt_numero"] . "</option>");
																		} 
																	} ?>												
																</select>
															</td>
															<td class="text-center" >
																<select name="pro_ven_cpt_si2p_exo_<?=$s["pro_id"]?>" class="select2">
																	<option value="">Compte "groupe" ...</option>		
														<?php		foreach($d_comptes as $compte){
																		if($s['pro_ven_cpt_si2p_exo']==$compte["cpt_id"]){
																			echo("<option value='" . $compte["cpt_id"] . "' selected >" . $compte["cpt_numero"] . "</option>");
																		}else{
																			echo("<option value='" . $compte["cpt_id"] . "' >" . $compte["cpt_numero"] . "</option>");
																		} 
																	} ?>												
																</select>
															</td>
														</tr>
								<?php 				} ?>
												</tbody>
											</table>						
										</div>
							<?php	}else{ ?>
										<p class="alert alert-warning" >
											Aucun produit ne correspond à votre recherche.
										</p>
								
							<?php	} ?>
								</form>
							</div>
						</div>
					</div>					
				</section>
				<!-- End: Content -->
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >
			<?php	if(!empty($_SESSION["retour"])){ ?>
						<a href="<?=$_SESSION["retour"]?>" class="btn btn-default btn-sm" >
							<span class="fa fa-long-arrow-left"></span>
							Retour
						</a>
			<?php	} ?>
				</div>
				<div class="col-xs-6 footer-middle" >&nbsp;</div>
				<div class="col-xs-3 footer-right" >
					<button type="button" id="sub_produit" class="btn btn-success btn-sm" id="sub_produit" >
						<span class="fa fa-save"></span>
						Enregistrer les valeurs
					</a>
				</div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>


		<script type="text/javascript" >
		
			jQuery(document).ready(function (){
				
				$("#pro_categorie").change(function(){
					if($(this).val()==1){
						$("#bloc_famille").show();
					}else{
						$("#pro_famille").val(0);
						$("#pro_sous_famille").val("");
						$("#pro_sous_sous_famille").val("");
						$("#bloc_famille").hide();
					}
				});
				
				$("#sub_produit").click(function(){
					$("#form").prop("action","param_produit_enr.php");
					$("#form").submit();
				});
				
				
			});
		</script>
		
	</body>
</html>
