 <?php
 
	// ENREGISTREMENT D'UN DEVIS
	
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");
	
	
	/*include 'modeles/mod_droit.php';
	include 'modeles/mod_get_societe.php';
	include 'modeles/mod_get_cli_categories.php';
	include 'modeles/mod_get_commerciaux.php';
	include 'modeles/mod_categorie.php';
	
	include 'modeles/mod_get_pro_categories.php';
	include 'modeles/mod_get_pro_familles.php';*/
	
	include 'modeles/mod_get_tva_taux.php';
	include 'modeles/mod_get_client_produits.php';
	include 'modeles/mod_add_notification.php';
	include 'modeles/mod_planning_periode_lib.php';
	include 'modeles/mod_planning_periode.php';
	
	$devis_id=0;
	if(isset($_POST["devis"])){
		if(!empty($_POST["devis"])){
			$devis_id=intval($_POST["devis"]);
		}	
	}
	
	$dev_client=0;
	if(isset($_POST["dev_client"])){
		if(!empty($_POST["dev_client"])){
			$dev_client=intval($_POST["dev_client"]);
		}	
	}
	
	$auto=0;// creation d'un devis sans passer par devis.php (ex : fiche client)
	if(isset($_POST["auto"])){
		if(!empty($_POST["auto"])){
			$auto=intval($_POST["auto"]);
		}	
	}elseif(isset($_GET["auto"])){
		if(!empty($_GET["auto"])){
			$auto=intval($_GET["auto"]);
		}
	}
	
	// Si création auto -> possibilite d'avoir des parametre en GET
	if(!empty($auto)){
		
		if(empty($dev_client)){
			if(isset($_GET["dev_client"])){
				if(!empty($_GET["dev_client"])){
					$dev_client=intval($_GET["dev_client"]);
				}	
			}
		}
		
		if(empty($dev_action)){
			if(isset($_GET["dev_action"])){
				if(!empty($_GET["dev_action"])){
					$dev_action=intval($_GET["dev_action"]);
				}	
			}
		}
		
	}
	
	// personne connecte
	
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	if(empty($dev_client) OR empty($acc_societe)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => "Client inconnu!" 
		);
		header("location : " . $_SESSION["retourDevis"]);
		die();
	}
	
	// DONNEE UTILISE DANS MODE AUTO OU FORM
	
	// variables
	
	$erreur_txt="";
	$complet=true;	// si false donnée non calculé => renvoie vers devis.php
	$dev_total_ht=0;
	$dev_total_ttc=0;
	$devis_lignes=array();
	$dev_commercial=0;
	$dev_agence=0;
	$dev_action_com_ref=0;
	$dev_action_com_ref_id=0;
	$dev_prospect=1;
	$dev_com_identite=0;
	$dev_esperance=30;
	$dev_validite="3 mois";
	$dev_reference="";
	$dev_libelle="";
	$dev_commande=0;
	$dev_commande_date=null;
	$dev_bordereau=0;
	
	$dev_adresse=0;
	$dev_adr_service="";
	$dev_adr1="";
	$dev_adr2="";
	$dev_adr3="";
	$dev_adr_cp="";
	$dev_adr_ville="";
	$dev_adr_geo=0;
	
	$dev_contact=0;
	$dev_con_titre=0;
	$dev_con_identite="";
	$dev_con_tel="";
	$dev_con_portable="";
	$dev_con_mail="";
	$adr_add=0;
	$adr_maj=0;
	$con_add=0;
	$con_maj=0;
	
	
	$dev_reg_type=2;
	$dev_reg_formule=1;
	$dev_reg_nb_jour=0;
	$dev_reg_fdm=0;
	
	$dev_att_derogation=0; // indique si le devis en en attente de dérogation tarifaire
	
	$dev_correspondance=0;

	// base produit du client
	
	$tab_produit=get_client_produits($dev_client,0,0,0,1,0,0,0,0,0,0);
	
	// la societe
		
	$sql="SELECT soc_tva_exo,soc_agence,soc_id FROM Societes WHERE soc_id=" . $acc_societe . ";";
	$req=$Conn->query($sql);
	$d_societe=$req->fetch();
	
	
	if(!empty($auto)){
		
		
		// CALCUL DES ELEMENTS A INSERER DANS LE DEVIS
		
		$DTime_date = date_create_from_format('d/m/Y',date("d/m/Y"));
		$dev_date=$DTime_date->format("Y-m-d");
		
		$sql="SELECT cli_nom,cli_first_facture,cli_reg_formule,cli_reg_nb_jour,cli_reg_fdm,cso_agence,cso_commercial,cli_reg_type,cli_categorie FROM Clients LEFT JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client)
		WHERE cso_societe=" . $acc_societe . " AND NOT cso_archive AND cli_id=" . $dev_client;
		if(!empty($acc_agence)){
			$sql.=" AND cso_agence=" . $acc_agence;
		}
		if(!$_SESSION["acces"]["acc_droits"][6]){
			$sql.=" AND cso_utilisateur=" . $acc_utilisateur;
		}
		$sql.=";";
		$req=$Conn->query($sql);
		$d_client=$req->fetchAll();
		
		if(!empty($d_client)){

			if(!empty($d_client[0]["cli_first_facture"])){
				$dev_prospect=0;	
			}
			
			if(count($d_client)==1){
				$dev_commercial=$d_client[0]["cso_commercial"];
			}else{
				$complet=false;
			}
			$dev_reg_type=$d_client[0]["cli_reg_type"];
			$dev_reg_formule=$d_client[0]["cli_reg_formule"];
			$dev_reg_nb_jour=$d_client[0]["cli_reg_nb_jour"];
			$dev_reg_fdm=$d_client[0]["cli_reg_fdm"];
			
		}else{
			$_SESSION['message'][] = array(
				"titre" => "Erreur",
				"type" => "danger",
				"message" => "Client inconnu!" 
			);
			header("location : " . $_SESSION["retourDevis"]);
			die();	
		}
		
		// ADRESSE ET CONTACT DU DEVIS (INTERVENTION)
		
		$sql="SELECT * FROM Adresses 
		LEFT OUTER JOIN Adresses_Contacts ON (Adresses.adr_id=Adresses_Contacts.aco_adresse)
		LEFT OUTER JOIN Contacts ON (Adresses_Contacts.aco_contact=Contacts.con_id)
		WHERE adr_ref=1 AND adr_ref_id=" . $dev_client . " AND adr_defaut";
		if($d_client[0]["cli_categorie"]==3){
			$sql.=" AND adr_type=2";
		}else{
			$sql.=" AND adr_type=1";
		}
		$sql.=";";
		$req=$Conn->query($sql);
		$d_adresse=$req->fetch();
		if(!empty($d_adresse)){
			$dev_adresse=$d_adresse["adr_id"];
			if(!empty($d_adresse["adr_nom"])){
				$dev_adr_nom=$d_adresse["adr_nom"];
			}else{
				$dev_adr_nom=$d_client[0]["cli_nom"];
			}
			$dev_adr_service=$d_adresse["adr_service"];
			$dev_adr1=$d_adresse["adr_ad1"];
			$dev_adr2=$d_adresse["adr_ad2"];
			$dev_adr3=$d_adresse["adr_ad3"];
			$dev_adr_cp=$d_adresse["adr_cp"];
			$dev_adr_ville=$d_adresse["adr_ville"];
			$dev_adr_geo=$d_adresse["adr_geo"];
			
			if(!empty($d_adresse["con_id"])){
				$dev_contact=$d_adresse["con_id"];
				if($d_adresse["con_titre"]==1){
					$dev_con_identite="M. ";
				}elseif($d_adresse["con_titre"]==2){
					$dev_con_identite="Mme ";
				}
				if(!empty($d_adresse["con_prenom"])){
					$dev_con_identite.=$d_adresse["con_prenom"] . " ";
				}
				if(!empty($d_adresse["con_nom"])){					
					$dev_con_identite.=$d_adresse["con_nom"] . " ";
				}
				$dev_con_identite=trim($dev_con_identite);
				
				$dev_con_titre=$d_adresse["con_titre"];
				$dev_con_nom=$d_adresse["con_nom"];
				$dev_con_prenom=$d_adresse["con_prenom"];
				$dev_con_tel=$d_adresse["con_tel"];
				$dev_con_portable=$d_adresse["con_portable"];
				$dev_con_mail=$d_adresse["con_mail"];
			}
		}else{
			$complet=false;
		}
		
		// LIGNES DE DEVIS MOD AUTO

		$dli_place=0;
		$place=0;
		if(!empty($dev_action)){
			
			// BASE SUR ACTION

			$sql="SELECT act_id,act_produit,act_date_deb FROM Actions WHERE act_id=" . $dev_action . " AND NOT act_archive;";
			$req=$ConnSoc->query($sql);
			$d_actions=$req->fetch();
			if(!empty($d_actions)){	
			
				$produit=$d_actions["act_produit"];
				
				if(empty($tab_produit[$produit])){
					
					// LE PRODUIT N'EXISTE PAS -> IL EST CERTAINEMENT ARCHIVE					
					$sql_pro_cor="SELECT ID,TYPE FROM Produits_Correspondances WHERE ID_OLD=" . $produit . " AND TYPE=2;";
					$req_pro_cor=$Conn->query($sql_pro_cor);
					$pro_cor=$req_pro_cor->fetch();
					if(!empty($pro_cor)){
						$produit=$pro_cor["ID"];
					}else{
						$produit=0;
					}

				}

				if(empty($produit)){
					$_SESSION['message'][] = array(
						"titre" => "Erreur",
						"type" => "danger",
						"message" => "Le produit n'a pas pu être chargé!" 
					);
					header("location : " . $_SESSION["retourDevis"]);
					die();
				}

				$dli_place++;
				
				// CONSTRUCTION DU CORPS DU DEVIS
				
				$exo=0;
				
				if($d_societe["soc_tva_exo"]==1 AND $tab_produit[$produit]["deductible"]){
					$tva_id=3;
					$tva_taux=0;
					$tva_periode=3;
					$exo=1;
				}else{
					$tva_id=$tab_produit[$produit]["tva"];
					
					$tab_tva=get_tva_taux($d_actions["act_date_deb"],$tva_id);
					
					$tva_periode=$tab_tva[$tva_id]["tpe_id"];
					$tva_taux=floatval($tab_tva[$tva_id]["tpe_taux"]);
					
				}
				
				$ttc=$tab_produit[$produit]["ht_inter"]*(1+($tva_taux/100));
				
				$dev_total_ht+=$tab_produit[$produit]["ht_inter"];
				$dev_total_ttc+=$ttc;
				
				$remise=$tab_produit[$produit]["tg_inter"]-$tab_produit[$produit]["ht_inter"];
				$remise=round($remise,2);
				
				$taux_remise=0;
				if(!empty($tab_produit[$produit]["tg_inter"])){					
					$taux_remise=($remise*100)/$tab_produit[$produit]["tg_inter"];
					$taux_remise=round($taux_remise,2);
				}
				
				$dli_dates="";
				$sql="SELECT pda_date,pda_demi FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=" . $dev_action . " AND NOT pda_archive ORDER BY pda_date,pda_demi;";
				$req=$ConnSoc->query($sql);
				$d_dates=$req->fetchAll(PDO::FETCH_ASSOC);
				if(!empty($d_dates)){
					$dli_dates=planning_periode($d_dates);
				}

				$devis_lignes[]=array(
					"dli_id" => 0,
					"dli_produit" => $produit,
					"dli_categorie" => $tab_produit[$produit]["categorie"],
					"dli_famille" => $tab_produit[$produit]["famille"],
					"dli_sous_famille" => $tab_produit[$produit]["sous_famille"],
					"dli_sous_sous_famille" => $tab_produit[$produit]["sous_sous_famille"],
					"dli_type" => 2,
					"dli_reference" => $tab_produit[$produit]["reference"],
					"dli_libelle" => $tab_produit[$produit]["libelle"],
					"dli_texte" => $tab_produit[$produit]["devis_txt"],
					"dli_action" => $dev_action,
					"dli_action_client" => 0,
					"dli_action_cli_soc" => 0,
					"dli_dates" => $dli_dates,
					"dli_stagiaires_txt" => "",
					"dli_tg" => $tab_produit[$produit]["tg_inter"],
					"dli_ht" => $tab_produit[$produit]["ht_inter"],
					"dli_ht_min" => $tab_produit[$produit]["cpr_min_inter"],
					"dli_qte" => 1,
					"dli_remise" => $remise,
					"dli_taux_remise" => $taux_remise,
					"dli_montant_ht" => $tab_produit[$produit]["ht_inter"],			
					"dli_tva_id" => $tva_id,
					"dli_tva_periode" => $tva_periode,
					"dli_tva_taux" => $tva_taux,
					"dli_deductible" => $tab_produit[$produit]["deductible"],
					"dli_exo" => $exo,
					"dli_place" => $dli_place,
					"dli_derogation" => $tab_produit[$produit]["derogation"],
					"dli_rem_famille" => $tab_produit[$produit]["rem_famille"],
					"dli_rem_ca" => $tab_produit[$produit]["rem_ca"]
				);
			}
			
		}else{
			
			// BASE SUR UNE LISTE DE PRODUIT
			
			$tab_tva=get_tva_taux($dev_date,0);
		
			if(!empty($_POST["dev_produit_intra"])){
				if(is_array($_POST["dev_produit_intra"])){
					foreach($_POST["dev_produit_intra"] as $intra){
						
						if($d_societe["soc_tva_exo"]==1 AND $tab_produit[$intra]["deductible"]){
							$tva_id=3;
						}else{
							$tva_id=$tab_produit[$intra]["tva"];
						}
						
						$tva_taux=floatval($tab_tva[$tva_id]["tpe_taux"]);
						
						
						$ttc=$tab_produit[$intra]["ht_intra"]*(1+($tva_taux/100));
						
						$dev_total_ht+=$tab_produit[$intra]["ht_intra"];
						$dev_total_ttc+=$ttc;
						
						$exo=0;
						
						if($tab_produit[$intra]["deductible"] AND $d_societe["soc_tva_exo"]){
							$exo=1;
						}
						
						$remise=$tab_produit[$intra]["tg_intra"]-$tab_produit[$intra]["ht_intra"];
						$remise=round($remise,2);
						
						$taux_remise=0;
						if(!empty($tab_produit[$intra]["tg_intra"])){					
							$taux_remise=($remise*100)/$tab_produit[$intra]["tg_intra"];
							$taux_remise=round($taux_remise,2);
						}
						
						$devis_txt="";
						if(!empty($tab_produit[$intra]["devis_txt"])){
							$devis_txt=htmlspecialchars($tab_produit[$intra]["devis_txt"]);
						}
						
						$dli_place++;
		
						$devis_lignes[]=array(
							"dli_id" => 0,
							"dli_produit" => $intra,
							"dli_categorie" => $tab_produit[$intra]["categorie"],
							"dli_famille" => $tab_produit[$intra]["famille"],
							"dli_sous_famille" => $tab_produit[$intra]["famille"],
							"dli_sous_sous_famille" => $tab_produit[$intra]["famille"],
							"dli_type" => 1,
							"dli_reference" => $tab_produit[$intra]["reference"],
							"dli_libelle" => $tab_produit[$intra]["libelle"],
							"dli_texte" => $devis_txt,
							"dli_action" => 0,
							"dli_action_client" => 0,
							"dli_action_cli_soc" => 0,
							"dli_dates" => "",
							"dli_stagiaires_txt" => "",
							"dli_tg" => $tab_produit[$intra]["tg_intra"],
							"dli_ht" => $tab_produit[$intra]["ht_intra"],
							"dli_ht_min" => $tab_produit[$intra]["cpr_min_intra"],
							"dli_remise" => $remise,
							"dli_taux_remise" => $taux_remise,
							"dli_qte" => 1,
							"dli_montant_ht" => $tab_produit[$intra]["ht_intra"],			
							"dli_tva_id" => $tva_id,
							"dli_tva_periode" => $tab_tva[$tva_id]["tpe_id"],
							"dli_tva_taux" => $tva_taux,
							"dli_exo" => $exo,
							"dli_place" => $dli_place,
							"dli_derogation" => $tab_produit[$intra]["derogation"],
							"dli_rem_famille" => $tab_produit[$intra]["rem_famille"],
							"dli_rem_ca" => $tab_produit[$intra]["rem_ca"]
						);
					}
				}
			}
			
			if(!empty($_POST["dev_produit_inter"])){
				if(is_array($_POST["dev_produit_inter"])){
					foreach($_POST["dev_produit_inter"] as $inter){
						
						if($d_societe["soc_tva_exo"]==1 AND $tab_produit[$inter]["deductible"]){
							$tva_id=3;
						}else{
							$tva_id=$tab_produit[$inter]["tva"];
						}
						$tva_taux=floatval($tab_tva[$tva_id]["tpe_taux"]);
						
						$ttc=$tab_produit[$inter]["ht_inter"]*(1+($tva_taux/100));
						
						$dev_total_ht+=$tab_produit[$inter]["ht_inter"];
						$dev_total_ttc+=$ttc;
						
						$remise=$tab_produit[$inter]["tg_inter"]-$tab_produit[$inter]["ht_inter"];
						$remise=round($remise,2);
						
						$taux_remise=0;
						if(!empty($tab_produit[$inter]["tg_inter"])){					
							$taux_remise=($remise*100)/$tab_produit[$inter]["tg_inter"];
							$taux_remise=round($taux_remise,2);
						}
						
						$exo=0;
						if($tab_produit[$inter]["deductible"] AND $d_societe["soc_tva_exo"]){
							$exo=1;
						}
						
						$devis_txt="";
						if(!empty($tab_produit[$inter]["devis_txt"])){
							$devis_txt=htmlspecialchars($tab_produit[$inter]["devis_txt"]);
						}
		
						$place++;
						
						$devis_lignes[]=array(
							"dli_id" => 0,
							"dli_produit" => $inter,
							"dli_categorie" => $tab_produit[$inter]["categorie"],
							"dli_famille" => $tab_produit[$inter]["famille"],
							"dli_sous_famille" => $tab_produit[$inter]["famille"],
							"dli_sous_sous_famille" => $tab_produit[$inter]["famille"],
							"dli_type" => 2,
							"dli_reference" => $tab_produit[$inter]["reference"],
							"dli_libelle" => $tab_produit[$inter]["libelle"],
							"dli_texte" => $devis_txt,
							"dli_action" => 0,
							"dli_action_client" => 0,
							"dli_action_cli_soc" => 0,
							"dli_dates" => "",
							"dli_stagiaires_txt" => "",
							"dli_tg" => $tab_produit[$inter]["tg_inter"],
							"dli_ht" => $tab_produit[$inter]["ht_inter"],
							"dli_ht_min" => $tab_produit[$inter]["cpr_min_inter"],
							"dli_qte" => 1,
							"dli_remise" => $remise,
							"dli_taux_remise" => $taux_remise,
							"dli_montant_ht" => $tab_produit[$inter]["ht_inter"],			
							"dli_tva_id" => $tva_id,
							"dli_tva_periode" => $tab_tva[$tva_id]["tpe_id"],
							"dli_tva_taux" => $tva_taux,
							"dli_deductible" => $tab_produit[$inter]["deductible"],
							"dli_exo" => $exo,
							"dli_place" => $place,
							"dli_derogation" => $tab_produit[$inter]["derogation"],
							"dli_rem_famille" => $tab_produit[$inter]["rem_famille"],
							"dli_rem_ca" => $tab_produit[$inter]["rem_ca"]
						);
					}
				}
			}
		}
		/*****************************************
				FIN MODE AUTO
		*****************************************/
		
	}else{
		
		/*****************************************
				MODE FORMULAIRE
		*****************************************/
		
		// ELEMENT DU FORM 
		
		// adresse
		$dev_adresse=0;
		if(isset($_POST["dev_adresse"])){
			if(!empty($_POST["dev_adresse"])){
				$dev_adresse=intval($_POST["dev_adresse"]);
			}	
		}
		$dev_adr_nom=$_POST["dev_adr_nom"];
		$dev_adr_service=$_POST["dev_adr_service"];
		$dev_adr1=$_POST["dev_adr1"];
		$dev_adr2=$_POST["dev_adr2"];
		$dev_adr3=$_POST["dev_adr3"];
		$dev_adr_cp=$_POST["dev_adr_cp"];
		$dev_adr_ville=$_POST["dev_adr_ville"];
		
		$dev_adr_geo=1;
		if(isset($_POST["dev_adr_geo"])){
			if(!empty($_POST["dev_adr_geo"])){
				$dev_adr_geo=intval($_POST["dev_adr_geo"]);
			}	
		}
		
		
		if(isset($_POST["adr_add"])){
			if(!empty($_POST["adr_add"])){
				$adr_add=1;
			}	
		}
		if(isset($_POST["adr_maj"])){
			if(!empty($_POST["adr_maj"])){
				$adr_maj=1;
			}	
		}

		// contact
		
		$dev_contact=0;
		if(isset($_POST["dev_contact"])){
			if(!empty($_POST["dev_contact"])){
				$dev_contact=intval($_POST["dev_contact"]);
			}	
		}
		$dev_con_titre=0;
		if(isset($_POST["dev_con_titre"])){
			if(!empty($_POST["dev_con_titre"])){
				$dev_con_titre=intval($_POST["dev_con_titre"]);
			}	
		}
		$dev_con_nom=$_POST["dev_con_nom"];
		$dev_con_prenom=$_POST["dev_con_prenom"];
		$dev_con_tel=$_POST["dev_con_tel"];
		$dev_con_portable=$_POST["dev_con_portable"];
		$dev_con_mail=$_POST["dev_con_mail"];
	
		if($dev_con_titre==1){
			$dev_con_identite="M. ";
		}elseif($dev_con_titre==2){
			$dev_con_identite="Mme ";
		}
		if(!empty($dev_con_prenom)){
			$dev_con_identite.=$dev_con_prenom . " ";
		}
		if(!empty($dev_con_nom)){
			$dev_con_identite.=$dev_con_nom . " ";
		}
		$dev_con_identite=trim($dev_con_identite);
		
		$con_add=0;
		if(isset($_POST["con_add"])){
			if(!empty($_POST["con_add"])){
				$con_add=1;
			}	
		}
		$con_maj=0;
		if(isset($_POST["con_maj"])){
			if(!empty($_POST["con_maj"])){
				$con_maj=1;
			}	
		}
		
		// etat de fiabilité
		
		$dev_esperance=0;
		if(isset($_POST["dev_esperance"])){
			if(!empty($_POST["dev_esperance"])){
				$dev_esperance=intval($_POST["dev_esperance"]);
			}	
		}
		if($dev_esperance==999){
		
			if(isset($d_devis["dev_commande"])){
					
				if(!$d_devis["dev_commande"]){
					$dev_commande=1;
					$DTime_dev_commande_date=new DateTime();
					$dev_commande_date=$DTime_dev_commande_date->format("Y-m-d");
				}else{
					$dev_commande=$d_devis["dev_commande"];
					$dev_commande_date=$d_devis["dev_commande_date"];
				}
			}else{
				//echo("CHAT <br/>");
			}
		}else{
			$dev_commande=0;
			$dev_commande_date=NULL;
		}
		
		// modalite de règlement

		if(isset($_POST["dev_reg_type"])){
			if(!empty($_POST["dev_reg_type"])){
				$dev_reg_type=intval($_POST["dev_reg_type"]);
			}	
		}
		if(isset($_POST["dev_reg_formule"])){
			if(!empty($_POST["dev_reg_formule"])){
				$dev_reg_formule=intval($_POST["dev_reg_formule"]);
			}	
		}
		if(isset($_POST["dev_reg_nb_jour_" . $dev_reg_formule])){
			if(!empty($_POST["dev_reg_nb_jour_" . $dev_reg_formule])){
				$dev_reg_nb_jour=intval($_POST["dev_reg_nb_jour_" . $dev_reg_formule]);
			}	
		}
		if(isset($_POST["dev_reg_fdm_" . $dev_reg_formule])){
			if(!empty($_POST["dev_reg_fdm_" . $dev_reg_formule])){
				$dev_reg_fdm=intval($_POST["dev_reg_fdm_" . $dev_reg_formule]);
			}	
		}
		
		// autre donnée
		
		$dev_commercial=0;
		if(isset($_POST["dev_commercial"])){
			if(!empty($_POST["dev_commercial"])){
				$dev_commercial=intval($_POST["dev_commercial"]);
			}	
		}
		
		$dev_reference=$_POST["dev_reference"];
		
		$DTime_date = date_create_from_format('d/m/Y',$_POST["dev_date"]);
		$dev_date=$DTime_date->format("Y-m-d");
		
		$dev_validite=$_POST["dev_validite"];
		
		$dev_bordereau=0;
		if(isset($_POST["dev_bordereau"])){
			if(!empty($_POST["dev_bordereau"])){
				$dev_bordereau=1;
			}	
		}
		
		$dev_libelle=$_POST["dev_libelle"];
		
		if(isset($_POST["dev_correspondance"])){
			if(!empty($_POST["dev_correspondance"])){
				$dev_correspondance=intval($_POST["dev_correspondance"]);
			}	
		}
		
		
		// DONNEE COMPLEMENTAIRE MODE FORM
		
		// tva en vigeur
		$tab_tva=get_tva_taux($dev_date,0);
		
		//le client (si on n'a pas déja recuperer les infos en mode auto
		if(!isset($d_client)){
			
			$sql="SELECT cli_first_facture FROM Clients WHERE cli_id=" . $dev_client . ";";
			$req=$Conn->query($sql);
			$d_client=$req->fetch();
			if(!empty($d_client)){
				if(!empty($d_client["cli_first_facture"])){
					$dev_prospect=0;					
				}
			}
		}
		
		// CONSTRUCTION DES LIGNES

		$num_ligne=0;
		if(isset($_POST["num_ligne"])){
			if(!empty($_POST["num_ligne"])){
				$num_ligne=intval($_POST["num_ligne"]);
			}	
		}
		
		$dli_place=0;
		
		for($bcl=1;$bcl<=$num_ligne;$bcl++){
			
			$dli_produit=0;
			if(!empty($_POST["dli_produit_" . $bcl])){
				$dli_produit=intval($_POST["dli_produit_" . $bcl]);			
			}
			
			$dli_id=0;
			if(!empty($_POST["dli_id_" . $bcl])){
				$dli_id=intval($_POST["dli_id_" . $bcl]);			
			}
			
			if(!isset($tab_produit[$dli_produit])){
				$dli_produit=0;
			};
			
			if(!empty($dli_produit)){
				$dli_type=0;
				if(!empty($_POST["dli_type_" . $bcl])){
					$dli_type=intval($_POST["dli_type_" . $bcl]);			
				}
				
				$dli_ht=0;
				if(!empty($_POST["dli_ht_" . $bcl])){
					$dli_ht=floatval($_POST["dli_ht_" . $bcl]);
					$dli_ht=round($dli_ht,2);
				}

				$dli_derogation=0;
				if(!empty($_POST["dli_derogation_" . $bcl])){
					$dli_derogation=intval($_POST["dli_derogation_" . $bcl]);			
				}
				
				if($dli_type==2){	
			
					$dli_tg=$tab_produit[$dli_produit]["tg_inter"];	
					if($dli_derogation==0){
						$dli_ht_min=$tab_produit[$dli_produit]["min_inter"];							
					}elseif($dli_derogation==1){
						$dli_ht_min=$tab_produit[$dli_produit]["min_dr_inter"];		
					}else{
						$dli_ht_min=0;
					}
					
					if($dli_ht<$tab_produit[$dli_produit]["min_dr_inter"] AND $dli_derogation<2){
						$dev_att_derogation=2;
					}elseif($dli_ht<$tab_produit[$dli_produit]["min_inter"] AND $dli_derogation<1){
						if($dev_att_derogation==0){
							$dev_att_derogation=1;
						}
					}

				}else{
					
					$dli_tg=$tab_produit[$dli_produit]["tg_intra"];
					if($dli_derogation==0){
						$dli_ht_min=$tab_produit[$dli_produit]["min_intra"];	
					}elseif($dli_derogation==1){
						$dli_ht_min=$tab_produit[$dli_produit]["min_dr_intra"];		
					}else{
						$dli_ht_min=0;
					}

					if($dli_ht<$tab_produit[$dli_produit]["min_dr_intra"] AND $dli_derogation<2){
						$dev_att_derogation=2;
					}elseif($dli_ht<$tab_produit[$dli_produit]["min_intra"] AND $dli_derogation<1){
						if($dev_att_derogation==0){
							$dev_att_derogation=1;
						}
					}
				}
				
				if($dli_derogation==0){
					$dli_rem_famille=1;
					$dli_rem_ca=1;
				}else{
					$dli_rem_famille=0;
					if(!empty($_POST["dli_rem_famille_" . $bcl])){
						$dli_rem_famille=1;
					}
					$dli_rem_ca=0;
					if(!empty($_POST["dli_rem_ca_" . $bcl])){
						$dli_rem_ca=1;
					}
				}
					
					
				
				$dli_libelle=$_POST["dli_libelle_" . $bcl];		
				$dli_texte=str_replace('"',"'",$_POST["dli_texte_" . $bcl]);
				$dli_dates=str_replace('"',"'",$_POST["dli_dates_" . $bcl]);
				$dli_stagiaires_txt=str_replace('"',"'",$_POST["dli_stagiaires_txt_" . $bcl]);
				
				
				$dli_qte=0;
				if(!empty($_POST["dli_qte_" . $bcl])){
					$dli_qte=floatval($_POST["dli_qte_" . $bcl]);			
				}
				$dli_remise=$dli_tg-$dli_ht;
				$dli_remise=round($dli_remise,2);
				
				$dli_taux_remise=0;
				if(!empty($dli_tg)){
					$dli_taux_remise=($dli_remise*100)/$dli_tg;
					$dli_taux_remise=round($dli_taux_remise,2);
				}

				$dli_montant_ht=$dli_ht*$dli_qte;
				$dli_montant_ht=round($dli_montant_ht,2);
				
				// gestion de la TVA
				
				$dli_exo=0;
				$dli_tva_id=0;
				if( ($d_societe["soc_tva_exo"] AND $tab_produit[$dli_produit]["deductible"]) ){		
					$dli_tva_id=3;
					$dli_exo=1;				
				}elseif($dev_adr_geo>1){
					$dli_tva_id=3;
				}elseif($_SESSION['acces']["acc_droits"][9]){
					$dli_tva_id=0;
					if(!empty($_POST["dli_tva_id_" . $bcl])){
						$dli_tva_id=intval($_POST["dli_tva_id_" . $bcl]);			
					}
				}else{
					$dli_tva_id=$tab_produit[$dli_produit]["tva"];
				}
				
				$dli_tva_taux=$tab_tva[$dli_tva_id]["tpe_taux"];
				
				$dev_total_ht=$dev_total_ht+$dli_montant_ht;
				$dev_total_ttc=$dev_total_ttc + $dli_montant_ht + ($dli_montant_ht*($dli_tva_taux/100));
				
				
				$dli_action=0;
				if(!empty($_POST["dli_action_" . $bcl])){
					$dli_action=intval($_POST["dli_action_" . $bcl]);			
				}
				$dli_action_client=0;
				if(!empty($_POST["dli_action_client_" . $bcl])){
					$dli_action_client=intval($_POST["dli_action_client_" . $bcl]);			
				}
				$dli_action_cli_soc=0;
				if(!empty($_POST["dli_action_cli_soc_" . $bcl])){
					$dli_action_cli_soc=intval($_POST["dli_action_cli_soc_" . $bcl]);			
				}
				
				
				// MEMO DES VALEURS DANS LE TABLEAU DE REF POUR LES MAJ LIGNES
				
				$dli_place++;
				
				$devis_lignes[]=array(
					"dli_id" => $dli_id,
					"dli_produit" => $dli_produit,
					"dli_categorie" => $tab_produit[$dli_produit]["categorie"],
					"dli_famille" => $tab_produit[$dli_produit]["famille"],
					"dli_sous_famille" => $tab_produit[$dli_produit]["sous_famille"],
					"dli_sous_sous_famille" => $tab_produit[$dli_produit]["sous_sous_famille"],
					"dli_type" => $dli_type,
					"dli_reference" => $tab_produit[$dli_produit]["reference"],
					"dli_libelle" => $dli_libelle,
					"dli_texte" => $dli_texte,
					"dli_action" => $dli_action,
					"dli_action_client" => $dli_action_client,
					"dli_action_cli_soc" => $dli_action_cli_soc,
					"dli_dates" => $dli_dates,
					"dli_stagiaires_txt" => $dli_stagiaires_txt,
					"dli_tg" => $dli_tg,
					"dli_ht" => $dli_ht,
					"dli_ht_min" => $dli_ht_min,
					"dli_remise" => $dli_remise,
					"dli_taux_remise" => $dli_taux_remise,
					"dli_qte" => $dli_qte,				
					"dli_montant_ht" => $dli_montant_ht,			
					"dli_tva_id" => $dli_tva_id,
					"dli_tva_periode" => $tab_tva[$dli_tva_id]["tpe_id"],
					"dli_tva_taux" => $dli_tva_taux,
					"dli_exo" => $dli_exo,
					"dli_place" => $dli_place,
					"dli_derogation" => $dli_derogation,
					"dli_rem_famille" => $dli_rem_famille,
					"dli_rem_ca" => $dli_rem_ca
				);
			}elseif(!empty($dli_id)){
				
				$devis_lignes[]=array(
					"dli_id" => $dli_id,
					"dli_produit" => 0
				);
				
			}
		}
		/*****************************************
				FIN MODE FORMULAIRE
		*****************************************/
	}
	// FIN DES DIFFERENT MODE DE CREATION DE DEVIS 
	
	/*****************************************
		ON CONTROLE LES ELEMENTS OBLIGATOIRES 
	*****************************************/
	
	if(empty($erreur_txt)){
		if(empty($dev_adr_nom) OR (empty($dev_adr1) AND empty($dev_adr2) AND empty($dev_adr3)) OR empty($dev_adr_cp) OR empty($dev_adr_ville)){
			$erreur_txt="Adresse non renseignée ou incomplète.";
		}
	}
	if(empty($erreur_txt)){
		if(empty($dev_commercial)){
			$erreur_txt="Commercial non renseigné.";
		}
	}
	if(empty($erreur_txt)){
		if(empty($dev_con_nom)){
			$erreur_txt="Contact client non renseigné";
		}
	}
	
	/*****************************************
		DONNEE COMPLEMENTAITRE TOUT MODE CONFONDU
	*****************************************/

	if(empty($erreur_txt)){
		
		/*
		hotfix/devis-duplicate-id
		$id_heracles=0;
		if(isset($_POST["id_heracles"])){
			if(!empty($_POST["id_heracles"])){
				$id_heracles=intval($_POST["id_heracles"]);
			}	
		}
		*/
		
		// SUR LE Commercial
		$dev_utilisateur=0;
		$dev_com_identite="";
		$sql="SELECT com_type,com_ref_1,com_agence,com_label_1,com_label_2 FROM Commerciaux WHERE com_id=" . $dev_commercial . ";";
		$req=$ConnSoc->query($sql);
		$d_commercial=$req->fetch();
		if(!empty($d_commercial)){
			if($d_societe["soc_agence"]){
				$dev_agence=$d_commercial["com_agence"];
			}
			$dev_com_identite=$d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"];
			
			if(!empty($d_commercial["com_ref_1"])){
				$dev_utilisateur=$d_commercial["com_ref_1"];
			}
		}
		// sur l'utilisateur
		//echo("utilisateur : " . $utilisateur . "<br/>");
		if($dev_utilisateur>0){	
			$sql="SELECT uti_titre,uti_nom,uti_prenom FROM Utilisateurs WHERE uti_id=" . $dev_utilisateur . ";";
			$req=$Conn->query($sql);
			$d_utilisateur=$req->fetch();
			if(!empty($d_utilisateur)){
				$dev_com_identite=$d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"];
			}
	
		}
		// SUR LE DEVIS
		
		if($devis_id>0){
			
			// en modifictaion
			$sql="SELECT dev_client,dev_commande,dev_chrono,dev_att_derogation FROM Devis WHERE dev_id=" . $devis_id . ";";
			$req=$ConnSoc->query($sql);
			$d_devis=$req->fetch();	
			if(!empty($d_devis)){
				$dev_chrono=$d_devis["dev_chrono"];
				
				if($d_devis["dev_client"]!=$dev_client){
					$dev_correspondance=0;
				}
			}		
		
		}else{

			// en creation
			$dev_chrono=1;
			$sql="SELECT MAX(dev_chrono) FROM Devis;";
			$req=$ConnSoc->query($sql);
			$result=$req->fetch();
			if(!empty($result)){
				$dev_chrono=$result[0];
				$dev_chrono++;				
			}
			
			/*if(empty($id_heracles)){
				$erreur_txt="Erreur de synchronisation Héraclès";
			}else{
				$dev_chrono=$id_heracles;
			}*/
			
			/*hotfix/devis-duplicate-id
			if(!empty($id_heracles)){
				$dev_chrono=$id_heracles;
			}*/
		}
		if($acc_societe==16){
			$dev_numero="D" . $DTime_date->format("y") . str_pad($dev_chrono,5,0,STR_PAD_LEFT);
		}else{
			$dev_numero="DV" . $DTime_date->format("y") . "-" . str_pad($dev_chrono,5,0,STR_PAD_LEFT);
		}

	}
	
	//FIN DONNEE COMPLEMENTAITRE MODE FORM OU AUTO
	
	/*****************************************
		ENREGISTREMENT
	*****************************************/
	
	//echo("CREATION DEVIS");
	//echo(" erreur : " . $erreur_txt);
	
	if(empty($erreur_txt)){
		
		$new_devis=false;
		
		// AJOUT / MAJ D'UNE ADRESSE D'INTERVENTION
		
		if($adr_add==1 AND empty($dev_adresse)){
			
			$sql="INSERT INTO Adresses (adr_ref,adr_ref_id,adr_type,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_geo
			) VALUES (1,:adr_ref_id,1,:adr_nom,:adr_service,:adr_ad1,:adr_ad2,:adr_ad3,:adr_cp,:adr_ville,:adr_geo);";
			$req=$Conn->prepare($sql);
			$req->bindValue(":adr_ref_id",$dev_client);
			$req->bindValue(":adr_nom",$dev_adr_nom);
			$req->bindValue(":adr_service",$dev_adr_service);
			$req->bindValue(":adr_ad1",$dev_adr1);
			$req->bindValue(":adr_ad2",$dev_adr2);
			$req->bindValue(":adr_ad3",$dev_adr3);
			$req->bindValue(":adr_cp",$dev_adr_cp);
			$req->bindValue(":adr_ville",$dev_adr_ville);
			$req->bindValue(":adr_geo",$dev_adr_geo);
			$req-> execute();
			$dev_adresse=$Conn->lastInsertId();
			
		}elseif($adr_maj==1 AND !empty($dev_adresse)){
			
			$sql="UPDATE Adresses SET
			adr_nom=:adr_nom,
			adr_service=:adr_service,
			adr_ad1=:adr_ad1,
			adr_ad2=:adr_ad2,
			adr_ad3=:adr_ad3,
			adr_cp=:adr_cp,
			adr_ville=:adr_ville,
			adr_geo=:adr_geo
			WHERE adr_id=:adr_id AND adr_ref_id=:adr_ref_id;";
			$req=$Conn->prepare($sql);
			$req->bindValue(":adr_id",$dev_adresse);
			$req->bindValue(":adr_ref_id",$dev_client);
			$req->bindValue(":adr_nom",$dev_adr_nom);
			$req->bindValue(":adr_service",$dev_adr_service);
			$req->bindValue(":adr_ad1",$dev_adr1);
			$req->bindValue(":adr_ad2",$dev_adr2);
			$req->bindValue(":adr_ad3",$dev_adr3);
			$req->bindValue(":adr_cp",$dev_adr_cp);
			$req->bindValue(":adr_ville",$dev_adr_ville);
			$req->bindValue(":adr_geo",$dev_adr_geo);
			$req-> execute();	
		}
		
		// AJOUT / MAJ D'UN CONTACT
		
		if($con_add==1 AND empty($dev_contact)){
			
			$sql="INSERT INTO Contacts (con_ref,con_ref_id,con_titre,con_nom,con_prenom,con_tel,con_portable,con_mail
			) VALUES (1,:con_ref_id,:con_titre,:con_nom,:con_prenom,:con_tel,:con_portable,:con_mail);";
			$req=$Conn->prepare($sql);
			$req->bindValue(":con_ref_id",$dev_client);
			$req->bindValue(":con_titre",$dev_con_titre);
			$req->bindValue(":con_nom",$dev_con_nom);
			$req->bindValue(":con_prenom",$dev_con_prenom);
			$req->bindValue(":con_tel",$dev_con_tel);
			$req->bindValue(":con_portable",$dev_con_portable);
			$req->bindValue(":con_mail",$dev_con_mail);
			$req-> execute();
			$dev_contact=$Conn->lastInsertId();
			
			// on associe le contact à l'adresse
			if(!empty($dev_adresse)){
				$sql="INSERT INTO Adresses_Contacts (aco_contact,aco_adresse) VALUES (:aco_contact,:aco_adresse);";
				$req=$Conn->prepare($sql);
				$req->bindValue(":aco_contact",$dev_contact);
				$req->bindValue(":aco_adresse",$dev_adresse);			
				$req-> execute();
			}
			
		}elseif($con_maj==1 AND !empty($dev_contact)){
			
			$sql="UPDATE Contacts SET
			con_titre=:con_titre,
			con_nom=:con_nom,
			con_prenom=:con_prenom,
			con_tel=:con_tel,
			con_portable=:con_portable,
			con_mail=:con_mail
			WHERE con_id=:con_id AND con_ref_id=:con_ref_id;";
			$req=$Conn->prepare($sql);
			$req->bindValue(":con_id",$dev_contact);
			$req->bindValue(":con_ref_id",$dev_client);
			$req->bindValue(":con_titre",$dev_con_titre);
			$req->bindValue(":con_nom",$dev_con_nom);
			$req->bindValue(":con_prenom",$dev_con_prenom);
			$req->bindValue(":con_tel",$dev_con_tel);
			$req->bindValue(":con_portable",$dev_con_portable);
			$req->bindValue(":con_mail",$dev_con_mail);
			$req-> execute();	
		}
		
		// MAJ DES DEVIS
		if($devis_id>0){
			
			$sql_up="UPDATE Devis SET 
			dev_agence=:dev_agence,
			dev_commercial=:dev_commercial,
			dev_com_identite=:dev_com_identite,
			dev_numero=:dev_numero,
			dev_chrono=:dev_chrono,
			dev_date=:dev_date,
			dev_prospect=:dev_prospect,
			dev_client=:dev_client,
			dev_adresse=:dev_adresse,
			dev_adr_service=:dev_adr_service,
			dev_adr_nom=:dev_adr_nom,
			dev_adr1=:dev_adr1,
			dev_adr2=:dev_adr2,
			dev_adr3=:dev_adr3,
			dev_adr_cp=:dev_adr_cp,
			dev_adr_ville=:dev_adr_ville,
			dev_adr_geo=:dev_adr_geo,
			dev_contact=:dev_contact,
			dev_con_identite=:dev_con_identite,
			dev_con_titre=:dev_con_titre,
			dev_con_nom=:dev_con_nom,
			dev_con_prenom=:dev_con_prenom,
			dev_con_tel=:dev_con_tel,
			dev_con_portable=:dev_con_portable,	
			dev_con_mail=:dev_con_mail,
			dev_reg_type=:dev_reg_type,
			dev_reg_formule=:dev_reg_formule,
			dev_reg_nb_jour=:dev_reg_nb_jour,
			dev_reg_fdm=:dev_reg_fdm,
			dev_total_ht=:dev_total_ht,
			dev_total_ttc=:dev_total_ttc,
			dev_reference=:dev_reference,
			dev_libelle=:dev_libelle,
			dev_validite=:dev_validite,
			dev_esperance=:dev_esperance,
			dev_commande=:dev_commande,
			dev_commande_date=:dev_commande_date,
			dev_action_com_ref=:dev_action_com_ref,
			dev_action_com_ref_id=:dev_action_com_ref_id,
			dev_utilisateur=:dev_utilisateur,
			dev_bordereau=:dev_bordereau,
			dev_att_derogation=:dev_att_derogation,
			dev_correspondance=:dev_correspondance
			WHERE dev_id=:devis;";
			$req_up=$ConnSoc->prepare($sql_up);
			$req_up->bindValue(":dev_agence",$dev_agence);
			$req_up->bindValue(":dev_commercial",$dev_commercial);
			$req_up->bindValue(":dev_com_identite",$dev_com_identite);
			$req_up->bindValue(":dev_numero",$dev_numero);
			$req_up->bindValue(":dev_chrono",$dev_chrono);
			$req_up->bindValue(":dev_date",$dev_date);
			$req_up->bindValue(":dev_prospect",$dev_prospect);
			$req_up->bindValue(":dev_client",$dev_client);
			$req_up->bindValue(":dev_adresse",$dev_adresse);
			$req_up->bindValue(":dev_adr_service",$dev_adr_service);
			$req_up->bindValue(":dev_adr_nom",$dev_adr_nom);
			$req_up->bindValue(":dev_adr1",$dev_adr1);
			$req_up->bindValue(":dev_adr2",$dev_adr2);
			$req_up->bindValue(":dev_adr3",$dev_adr3);
			$req_up->bindValue(":dev_adr_cp",$dev_adr_cp);
			$req_up->bindValue(":dev_adr_ville",$dev_adr_ville);
			$req_up->bindValue(":dev_adr_geo",$dev_adr_geo);
			$req_up->bindValue(":dev_contact",$dev_contact);
			$req_up->bindValue(":dev_con_titre",$dev_con_titre);	
			$req_up->bindValue(":dev_con_nom",$dev_con_nom);
			$req_up->bindValue(":dev_con_prenom",$dev_con_prenom);
			$req_up->bindValue(":dev_con_identite",$dev_con_identite);
			$req_up->bindValue(":dev_con_tel",$dev_con_tel);
			$req_up->bindValue(":dev_con_portable",$dev_con_portable);
			$req_up->bindValue(":dev_con_mail",$dev_con_mail);	
			$req_up->bindValue(":dev_reg_type",$dev_reg_type);
			$req_up->bindValue(":dev_reg_formule",$dev_reg_formule);
			$req_up->bindValue(":dev_reg_nb_jour",$dev_reg_nb_jour);
			$req_up->bindValue(":dev_reg_fdm",$dev_reg_fdm);
			$req_up->bindValue(":dev_total_ht",$dev_total_ht);
			$req_up->bindValue(":dev_total_ttc",$dev_total_ttc);
			$req_up->bindValue(":dev_reference",$dev_reference);
			$req_up->bindValue(":dev_libelle",$dev_libelle);
			$req_up->bindValue(":dev_validite",$dev_validite);
			$req_up->bindValue(":dev_esperance",$dev_esperance);
			$req_up->bindValue(":dev_commande",$dev_commande);
			$req_up->bindValue(":dev_commande_date",$dev_commande_date);
			$req_up->bindValue(":dev_action_com_ref",$dev_action_com_ref);
			$req_up->bindValue(":dev_action_com_ref_id",$dev_action_com_ref_id);
			$req_up->bindValue(":dev_utilisateur",$dev_utilisateur);
			$req_up->bindValue(":dev_bordereau",$dev_bordereau);
			$req_up->bindValue(":dev_att_derogation",$dev_att_derogation);
			$req_up->bindValue(":dev_correspondance",$dev_correspondance);
			$req_up->bindValue(":devis",$devis_id);
			try{
				$req_up->execute();
			}Catch(Exception $e){
				$erreur_txt="UPDATE : " . $e->getMessage();
			}
			
			
		}else{
			
			// CREATION D'UN DEVIS

			$sql_add="INSERT INTO DEVIS (dev_agence,dev_commercial,dev_com_identite,dev_numero,dev_chrono,dev_date,dev_prospect,dev_client,
			dev_adresse,dev_adr_service,dev_adr_nom,dev_adr1,dev_adr2,dev_adr3,dev_adr_cp,dev_adr_ville,dev_adr_geo,dev_contact,dev_con_identite,dev_con_titre,dev_con_nom,dev_con_prenom,
			dev_con_tel,dev_con_portable,dev_con_mail,dev_reg_type,dev_reg_formule,dev_reg_nb_jour,dev_reg_fdm,dev_total_ht,dev_total_ttc,
			dev_reference,dev_libelle,dev_validite,dev_esperance,dev_commande,dev_commande_date,dev_action_com_ref,dev_action_com_ref_id,
			dev_utilisateur,dev_bordereau,dev_att_derogation,dev_correspondance";
			/*hotfix/devis-duplicate-id
			if(!empty($id_heracles)){
				$sql_add.=",dev_id";
			}*/
			$sql_add.=") VALUES (
			:dev_agence,
			:dev_commercial,
			:dev_com_identite,
			:dev_numero,
			:dev_chrono,
			:dev_date,
			:dev_prospect,
			:dev_client,
			:dev_adresse,
			:dev_adr_service,
			:dev_adr_nom,
			:dev_adr1,
			:dev_adr2,
			:dev_adr3,
			:dev_adr_cp,
			:dev_adr_ville,
			:dev_adr_geo,
			:dev_contact,
			:dev_con_identite,
			:dev_con_titre,
			:dev_con_nom,
			:dev_con_prenom,
			:dev_con_tel,
			:dev_con_portable,
			:dev_con_mail,
			:dev_reg_type,
			:dev_reg_formule,
			:dev_reg_nb_jour,
			:dev_reg_fdm,
			:dev_total_ht,
			:dev_total_ttc,
			:dev_reference,
			:dev_libelle,
			:dev_validite,
			:dev_esperance,
			:dev_commande,
			:dev_commande_date,
			:dev_action_com_ref,
			:dev_action_com_ref_id,
			:dev_utilisateur,
			:dev_bordereau,
			:dev_att_derogation,
			:dev_correspondance";
			/*hotfix/devis-duplicate-id
			if(!empty($id_heracles)){
				$sql_add.=",:dev_id";
			}*/
			$sql_add.=");";
			$req_add=$ConnSoc->prepare($sql_add);
			$req_add->bindValue(":dev_agence",$dev_agence);
			$req_add->bindValue(":dev_commercial",$dev_commercial);
			$req_add->bindValue(":dev_com_identite",$dev_com_identite);
			$req_add->bindValue(":dev_numero",$dev_numero);
			$req_add->bindValue(":dev_chrono",$dev_chrono);
			$req_add->bindValue(":dev_date",$dev_date);
			$req_add->bindValue(":dev_prospect",$dev_prospect);
			$req_add->bindValue(":dev_client",$dev_client);
			$req_add->bindValue(":dev_adresse",$dev_adresse);
			$req_add->bindValue(":dev_adr_service",$dev_adr_service);
			$req_add->bindValue(":dev_adr_nom",$dev_adr_nom);
			$req_add->bindValue(":dev_adr1",$dev_adr1);
			$req_add->bindValue(":dev_adr2",$dev_adr2);
			$req_add->bindValue(":dev_adr3",$dev_adr3);
			$req_add->bindValue(":dev_adr_cp",$dev_adr_cp);
			$req_add->bindValue(":dev_adr_ville",$dev_adr_ville);
			$req_add->bindValue(":dev_adr_geo",$dev_adr_geo);
			$req_add->bindValue(":dev_contact",$dev_contact);
			$req_add->bindValue(":dev_con_titre",$dev_con_titre);
			$req_add->bindValue(":dev_con_nom",$dev_con_nom);
			$req_add->bindValue(":dev_con_prenom",$dev_con_prenom);
			$req_add->bindValue(":dev_con_identite",$dev_con_identite);
			$req_add->bindValue(":dev_con_tel",$dev_con_tel);
			$req_add->bindValue(":dev_con_portable",$dev_con_portable);
			$req_add->bindValue(":dev_con_mail",$dev_con_mail);	
			$req_add->bindValue(":dev_reg_type",$dev_reg_type);
			$req_add->bindValue(":dev_reg_formule",$dev_reg_formule);
			$req_add->bindValue(":dev_reg_nb_jour",$dev_reg_nb_jour);
			$req_add->bindValue(":dev_reg_fdm",$dev_reg_fdm);
			$req_add->bindValue(":dev_total_ht",$dev_total_ht);
			$req_add->bindValue(":dev_total_ttc",$dev_total_ttc);
			$req_add->bindValue(":dev_reference",$dev_reference);
			$req_add->bindValue(":dev_libelle",$dev_libelle);
			$req_add->bindValue(":dev_validite",$dev_validite);
			$req_add->bindValue(":dev_esperance",$dev_esperance);
			$req_add->bindValue(":dev_commande",$dev_commande);
			$req_add->bindValue(":dev_commande_date",$dev_commande_date);
			$req_add->bindValue(":dev_action_com_ref",$dev_action_com_ref);
			$req_add->bindValue(":dev_action_com_ref_id",$dev_action_com_ref_id);
			$req_add->bindValue(":dev_utilisateur",$dev_utilisateur);
			$req_add->bindValue(":dev_bordereau",$dev_bordereau);
			$req_add->bindValue(":dev_att_derogation",$dev_att_derogation);
			$req_add->bindValue(":dev_correspondance",$dev_correspondance);
			/*
			hotfix/devis-duplicate-id
			if(!empty($id_heracles)){
				$req_add->bindValue(":dev_id",$id_heracles);
			}*/
			try{
				$req_add->execute();
			}Catch(Exception $e){
				$erreur_txt="ADD " . $e->getMessage();
			}
			if($devis_id==0){
				$devis_id=$ConnSoc->lastInsertId(); 				
			}
			$new_devis=true;
		}
	}
	if(empty($erreur_txt)){

		// ENREGISTREMENT DES LIGNES
		
		if(!empty($devis_lignes)){
			
			$sql_add="INSERT INTO Devis_Lignes (dli_devis,dli_produit,dli_categorie,dli_famille,dli_sous_famille,dli_sous_sous_famille,dli_type,dli_reference
			,dli_libelle,dli_texte,dli_dates,dli_tg,dli_ht,dli_ht_min,dli_remise,dli_taux_remise,dli_qte,dli_montant_ht,dli_tva_id,dli_tva_periode
			,dli_tva_taux,dli_action,dli_action_client,dli_action_cli_soc,dli_place,dli_exo,dli_stagiaires_txt,dli_derogation,dli_rem_famille,dli_rem_ca
			) VALUES (
			:dli_devis,
			:dli_produit,
			:dli_categorie,
			:dli_famille,
			:dli_sous_famille,
			:dli_sous_sous_famille,
			:dli_type,
			:dli_reference,
			:dli_libelle,
			:dli_texte,
			:dli_dates,
			:dli_tg,
			:dli_ht,
			:dli_ht_min,
			:dli_remise,
			:dli_taux_remise,
			:dli_qte,
			:dli_montant_ht,
			:dli_tva_id,
			:dli_tva_periode,
			:dli_tva_taux,
			:dli_action,
			:dli_action_client,
			:dli_action_cli_soc,
			:dli_place,
			:dli_exo,
			:dli_stagiaires_txt,
			:dli_derogation,
			:dli_rem_famille,
			:dli_rem_ca);";
			$req_add=$ConnSoc->prepare($sql_add);
				
			$sql_up="UPDATE Devis_Lignes SET 
			dli_produit=:dli_produit,
			dli_categorie=:dli_categorie,
			dli_famille=:dli_famille,
			dli_sous_famille=:dli_sous_famille,
			dli_sous_sous_famille=:dli_sous_sous_famille,
			dli_type=:dli_type,
			dli_reference=:dli_reference,
			dli_libelle=:dli_libelle,
			dli_texte=:dli_texte,
			dli_dates=:dli_dates,
			dli_stagiaires_txt=:dli_stagiaires_txt,
			dli_tg=:dli_tg,
			dli_ht=:dli_ht,
			dli_ht_min=:dli_ht_min,
			dli_remise=:dli_remise,
			dli_taux_remise=:dli_taux_remise,
			dli_qte=:dli_qte,
			dli_montant_ht=:dli_montant_ht,
			dli_tva_id=:dli_tva_id,
			dli_tva_periode=:dli_tva_periode,
			dli_tva_taux=:dli_tva_taux,
			dli_action=:dli_action,
			dli_action_client=:dli_action_client,
			dli_action_cli_soc=:dli_action_cli_soc,
			dli_place=:dli_place,
			dli_exo=:dli_exo,
			dli_derogation=:dli_derogation,
			dli_rem_famille=:dli_rem_famille,
			dli_rem_ca=:dli_rem_ca
			WHERE dli_id=:dli_id;";
			$req_up=$ConnSoc->prepare($sql_up);
			
			$sql_del="DELETE FROM Devis_Lignes WHERE dli_id=:dli_id AND dli_devis=:dli_devis;";
			$req_del=$ConnSoc->prepare($sql_del);
			
			foreach($devis_lignes as $ligne){
				
				if($ligne["dli_produit"]>0){

					if($ligne["dli_id"]==0){				
						$req_add->bindParam(":dli_devis",$devis_id);					
						$req_add->bindParam(":dli_produit",$ligne["dli_produit"]);
						$req_add->bindParam(":dli_categorie",$ligne["dli_categorie"]);
						$req_add->bindParam(":dli_famille",$ligne["dli_famille"]);
						$req_add->bindParam(":dli_sous_famille",$ligne["dli_sous_famille"]);
						$req_add->bindParam(":dli_sous_sous_famille",$ligne["dli_sous_sous_famille"]);
						$req_add->bindParam(":dli_type",$ligne["dli_type"]);
						$req_add->bindParam(":dli_reference",$ligne["dli_reference"]);
						$req_add->bindParam(":dli_libelle",$ligne["dli_libelle"]);
						$req_add->bindParam(":dli_texte",$ligne["dli_texte"]);
						$req_add->bindParam(":dli_action",$ligne["dli_action"]);
						$req_add->bindParam(":dli_action_client",$ligne["dli_action_client"]);
						$req_add->bindParam(":dli_action_cli_soc",$ligne["dli_action_cli_soc"]);
						$req_add->bindParam(":dli_dates",$ligne["dli_dates"]);
						$req_add->bindParam(":dli_stagiaires_txt",$ligne["dli_stagiaires_txt"]);	
						$req_add->bindParam(":dli_tg",$ligne["dli_tg"]);
						$req_add->bindParam(":dli_ht",$ligne["dli_ht"]);
						$req_add->bindParam(":dli_ht_min",$ligne["dli_ht_min"]);
						$req_add->bindParam(":dli_remise",$ligne["dli_remise"]);
						$req_add->bindParam(":dli_taux_remise",$ligne["dli_taux_remise"]);
						$req_add->bindParam(":dli_qte",$ligne["dli_qte"]);					
						$req_add->bindParam(":dli_montant_ht",$ligne["dli_montant_ht"]);
						$req_add->bindParam(":dli_tva_id",$ligne["dli_tva_id"]);
						$req_add->bindParam(":dli_tva_periode",$ligne["dli_tva_periode"]);
						$req_add->bindParam(":dli_tva_taux",$ligne["dli_tva_taux"]);
						$req_add->bindParam(":dli_place",$ligne["dli_place"]);		
						$req_add->bindParam(":dli_exo",$ligne["dli_exo"]);	
						$req_add->bindParam(":dli_derogation",$ligne["dli_derogation"]);
						$req_add->bindParam(":dli_rem_famille",$ligne["dli_rem_famille"]);
						$req_add->bindParam(":dli_rem_ca",$ligne["dli_rem_ca"]);									
						$req_add->execute();
					}else{
						
						$req_up->bindParam(":dli_id",$ligne["dli_id"]);
						$req_up->bindParam(":dli_produit",$ligne["dli_produit"]);
						$req_up->bindParam(":dli_categorie",$ligne["dli_categorie"]);
						$req_up->bindParam(":dli_famille",$ligne["dli_famille"]);
						$req_up->bindParam(":dli_sous_famille",$ligne["dli_sous_famille"]);
						$req_up->bindParam(":dli_sous_sous_famille",$ligne["dli_sous_sous_famille"]);
						$req_up->bindParam(":dli_type",$ligne["dli_type"]);
						$req_up->bindParam(":dli_reference",$ligne["dli_reference"]);
						$req_up->bindParam(":dli_libelle",$ligne["dli_libelle"]);
						$req_up->bindParam(":dli_texte",$ligne["dli_texte"]);
						$req_up->bindParam(":dli_action",$ligne["dli_action"]);
						$req_up->bindParam(":dli_action_client",$ligne["dli_action_client"]);
						$req_up->bindParam(":dli_action_cli_soc",$ligne["dli_action_cli_soc"]);
						$req_up->bindParam(":dli_dates",$ligne["dli_dates"]);
						$req_up->bindParam(":dli_stagiaires_txt",$ligne["dli_stagiaires_txt"]);
						$req_up->bindParam(":dli_tg",$ligne["dli_tg"]);
						$req_up->bindParam(":dli_ht",$ligne["dli_ht"]);
						$req_up->bindParam(":dli_ht_min",$ligne["dli_ht_min"]);
						$req_up->bindParam(":dli_remise",$ligne["dli_remise"]);
						$req_up->bindParam(":dli_taux_remise",$ligne["dli_taux_remise"]);
						$req_up->bindParam(":dli_qte",$ligne["dli_qte"]);					
						$req_up->bindParam(":dli_montant_ht",$ligne["dli_montant_ht"]);
						$req_up->bindParam(":dli_tva_id",$ligne["dli_tva_id"]);
						$req_up->bindParam(":dli_tva_periode",$ligne["dli_tva_periode"]);
						$req_up->bindParam(":dli_tva_taux",$ligne["dli_tva_taux"]);
						$req_up->bindParam(":dli_place",$ligne["dli_place"]);	
						$req_up->bindParam(":dli_exo",$ligne["dli_exo"]);
						$req_up->bindParam(":dli_derogation",$ligne["dli_derogation"]);
						$req_up->bindParam(":dli_rem_famille",$ligne["dli_rem_famille"]);
						$req_up->bindParam(":dli_rem_ca",$ligne["dli_rem_ca"]);	
						$req_up->execute();
					}
				}elseif(!empty($ligne["dli_id"])){

					$req_del->bindParam(":dli_devis",$devis_id);					
					$req_del->bindParam(":dli_id",$ligne["dli_id"]);
					$req_del->execute();

				}
			}
		}
		// FIN MAJ LIGNE			
				
	}
	// TRAITEMENT ANNEXE
	
	if(empty($erreur_txt)){
	
		// DEROGATION
		$notif_derog=0;
		if(isset($d_devis)){
			if($dev_att_derogation>$d_devis["dev_att_derogation"]){
				$notif_derog=1;
			}elseif($dev_att_derogation==0 AND !empty($d_devis["dev_att_derogation"]) AND $acc_utilisateur!=$dev_utilisateur){
				$notif_derog=2;	
			}
		}elseif($dev_att_derogation>0){
			$notif_derog=1;
		}
		
		if($notif_derog==1){
			
			if($dev_att_derogation==1){
				$liste_droit=27;
			}else{
				$liste_droit=31;
			}
			$notif="Le devis " . $dev_numero . " nécessite une dérogation de niveau " . $dev_att_derogation . ".";
			
			add_notifications("<i class='fa fa-euro' ></i>",$notif,"bg-warning","devis_voir.php?devis=" . $devis_id,"",$liste_droit,"",$acc_societe,$dev_agence,1);
			
		}elseif($notif_derog==2 AND !empty($dev_utilisateur)){
			
			$notif="Le devis " . $dev_numero . " peut désormais être imprimé.";
			
			add_notifications("<i class='fa fa-euro' ></i>",$notif,"bg-success","devis_voir.php?devis=" . $devis_id,"","",$dev_utilisateur,$acc_societe,$dev_agence,1);
			
		}
		
		
		if($new_devis AND !empty($dev_correspondance)){
		
			$sql_cor="UPDATE Correspondances SET cor_devis=:devis, cor_devis_num=:devis_numero, cor_devis_soc=:devis_societe WHERE cor_id=:correspondance AND cor_devis=0;";
			$req_cor=$Conn->prepare($sql_cor);
			$req_cor->bindParam(":devis",$devis_id);
			$req_cor->bindParam(":devis_numero",$dev_numero);
			$req_cor->bindParam(":devis_societe",$acc_societe);
			$req_cor->bindParam(":correspondance",$dev_correspondance);
			$req_cor->execute();
		
		}
	}
	
	
	
	// FIN TRAITEMENt
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
	}
	//die();
	if(!empty($erreur_txt)){
		header("location:" . $_SESSION["retourDevis"]);
	}elseif(!$complet){
		header("location: devis.php?devis=" . $devis_id);
	}else{
		header("location: devis_voir.php?devis=" . $devis_id);
	}
	die();
?>