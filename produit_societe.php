<?php
include "includes/controle_acces.inc.php";

if(!$_SESSION["acces"]["acc_droits"][24]){
	Header('Location: deconnect.php');
	die();
}

include_once 'includes/connexion.php';

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=intval($_SESSION['acces']["acc_agence"]);	
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=intval($_SESSION['acces']["acc_societe"]);	
}

$produit=0;
if(!empty($_GET["produit"])){
	$produit=intval($_GET["produit"]);
}

if($acc_agence>0){
	$sql="SELECT soc_nom,age_nom FROM Societes LEFT JOIN Agences ON (Societes.soc_id=Agences.age_societe)
	WHERE age_id=" . $acc_agence . ";";
	$req=$Conn->query($sql);
	$d_entite=$req->fetch();
}else{
	$sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
	$req=$Conn->query($sql);
	$d_entite=$req->fetch();
}
// LE Produit

$sql="SELECT pro_id,pro_code_produit,pro_reference_sta,pro_devis_txt,pso_reference_sta,pso_devis_txt,pro_libelle
FROM Produits LEFT OUTER JOIN Produits_Societes 
ON (Produits.pro_id=Produits_Societes.pso_produit AND pso_societe=" . $acc_societe . " AND pso_agence=" . $acc_agence . ")
WHERE pro_id=" . $produit . ";";
$req=$Conn->query($sql);
$d_produit=$req->fetch();
if(empty($d_produit)){
	echo("Impossible d'afficher la page!");
	die();
}
?>
<!DOCTYPE html>
<html>
<!-- Mirrored from admindesigns.com/demos/absolute/1.1/layout_horizontal-sm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 09:52:52 GMT -->
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>SI2P - Orion - Paramètres</title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	
	<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

</head>

	<body class="sb-top sb-top-sm ">
		<form method="post" action="produit_societe_enr.php" id="form_produit" >
			<div>
				<input type="hidden" name="produit" id="produit" value="<?=$produit?>" />
			</div>
			<!-- Start: Main -->
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper" >

					<section id="content" class="animated fadeIn">
					
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">
											<div class="text-center">
												<div class="content-header">
													<h2>Produit <b class="text-primary"><?=$d_produit['pro_code_produit'] . "-" . $d_produit['pro_libelle']?></b></h2>																											
												</div>
											</div>
                      
											<div class="col-md-10 col-md-offset-1">
					  
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Données génériques</span>
														</div>
													</div>
												</div>
												
												<div class="row"> 
													<div class="col-md-6" >
														<p><b>Texte devis :</b></p>												
										<?php 			if(empty($d_produit["pro_devis_txt"])){ ?>
															<div class="alert alert-danger">Non renseigné!</div>														
										<?php 			}else{ ?>													
															<p><?=$d_produit["pro_devis_txt"]?></p>
										<?php 			} ?>
													</div>
													<div class="col-md-6" >
														<p><b>Texte convocation :</b></p>												
										<?php 			if(empty($d_produit["pro_reference_sta"])){ ?>
															<div class="alert alert-danger">Non renseigné!</div>														
										<?php 			}else{ ?>													
															<p><?=$d_produit["pro_reference_sta"]?></p>
										<?php 			} ?>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>
																Spécificité <?=$d_entite["soc_nom"]?>
													<?php		if(!empty($d_entite["age_nom"])){
																	echo(" " . $d_entite["age_nom"]);											
																} ?>
															</span>
														</div>
													</div>
												</div>								
												<div class="row">
													<div class="col-md-6">
														<p><small>Information à destination des clients à afficher sur les devis</small></p>
														<div class="section">
															<label class="field prepend-icon">
																<textarea id="pso_devis_txt" class="summernote-img" name="pso_devis_txt" placeholder="Info client"><?=$d_produit['pso_devis_txt']?></textarea>
																
															</label>
														</div>
													</div>
													<div class="col-md-6">
														<p><small>Information à destination des stagiaires à afficher sur les convocations</small></p>
														<div class="section">
															<label class="field prepend-icon">
																<textarea id="pso_reference_sta" class="summernote-img" name="pso_reference_sta" placeholder="Info stagiaire"><?=$d_produit['pso_reference_sta']?></textarea>
																
															</label>
														</div>
													</div>
												</div>
											
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End: Content -->
				</section>
				<!-- End: Content WRAPPER -->
		
			</div>
			<!-- End: Main -->
			
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="<?= $_SESSION["retour"] ?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" name="submit" id="form_submit" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		   
		</form>
  <?php
    
		include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script> 
		<script type="text/javascript">
		
			jQuery(document).ready(function (){	
			
				
			});
		</script>
	</body>
</html>
