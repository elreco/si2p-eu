<?php

	// STATISTIQUE CA PAR PRODUIT

	include "includes/controle_acces.inc.php";

	include "includes/connexion.php";
	include "includes/connexion_soc.php";
	include "includes/connexion_fct.php";

	$erreur_txt="";

	// CONTROLE ACCES

	if($_SESSION['acces']['acc_service'][1]!=1 AND $_SESSION['acces']['acc_service'][3]!=1){
		echo("Accès refusé!");
		die();
	}elseif(empty($_POST) AND empty($_GET)){
		$erreur_txt="Formulaire incomplet";
	}

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}


	// TRAITEMENT DU FORM
	if(empty($erreur_txt)){
		if(empty($acc_agence)){
			$agence=0;
			if(!empty($_POST["agence"])){
				$agence=intval($_POST["agence"]);
			}elseif(!empty($_GET["agence"])){
				$agence=intval($_GET["agence"]);
			}
		}else{
			$agence=$acc_agence;
		}

		$commercial=0;
		if(!empty($_POST["commercial"])){
			$commercial=intval($_POST["commercial"]);
		}elseif(!empty($_GET["commercial"])){
			$commercial=intval($_GET["commercial"]);
		}

		$periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}elseif(!empty($_GET["periode_deb"])){
			$DT_periode_deb=date_create_from_format('Y-m-d',$_GET["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}

		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
		}elseif(!empty($_GET["periode_fin"])){
			$DT_periode_fin=date_create_from_format('Y-m-d',$_GET["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
		}


		if(empty($periode_deb) OR empty($periode_fin)){
			$erreur_txt="A Formulaire incomplet";
		}
	}

	if(empty($erreur_txt)){

		// TRAITEMENT

		/* on prend fli_montant_ht et pas fac_ht car la jointure ne marche pas sur les communications.
		donc sum de fli_montant_ht != fac_total_ht
		*/

		$stat=array(
			"00" => array (
				"dep" => "00",
				"nb_geo" => 0,
				"ca_geo" => 0,
				"nb_gc" => 0,
				"ca_gc" => 0,
				"nb_tot" => 0,
				"ca_tot" => 0
			),
			"tot" => array (
				"dep" => "tot",
				"nb_geo" => 0,
				"ca_geo" => 0,
				"nb_gc" => 0,
				"ca_gc" => 0,
				"nb_tot" => 0,
				"ca_tot" => 0
			)
		);

		// oblige de passer par un tableau car group by ne marche pas avec act_adr_cp
		$client_unique=array();
		$sql="SELECT act_adr_cp,acl_client,fli_montant_ht,fac_cli_categorie,fac_numero,acl_id
		FROM Actions,Actions_Clients,Factures_Lignes,Factures
		WHERE act_id=acl_action AND acl_id=fli_action_client AND fli_facture=fac_id
		AND fli_action_cli_soc=:acc_societe AND NOT fac_client=8891 AND fac_date>=:periode_deb AND fac_date<=:periode_fin";
		if(!empty($agence)){
			$sql.=" AND fac_agence=:agence";
		}
		if(!empty($commercial)){
			$sql.=" AND fac_commercial=:commercial";
		}
		$sql.=" ORDER BY act_adr_cp;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":acc_societe",$acc_societe);
		$req->bindParam(":periode_deb",$periode_deb);
		$req->bindParam(":periode_fin",$periode_fin);
		if(!empty($agence)){
			$req->bindParam(":agence",$agence);
		}
		if(!empty($commercial)){
			$req->bindParam(":commercial",$commercial);
		}
		$req->execute();
		$d_donnees=$req->fetchAll();
		if(!empty($d_donnees)){
			foreach($d_donnees as $d){

				if(!empty($d["act_adr_cp"])){
					$cp=trim($d["act_adr_cp"]);
					$cp=substr($cp,0,2);
					$dep=intval($cp);
					if($dep<10){
						$dep="0" . $dep;
					}
				}else{
					$dep="00";
				}



				//	if($dep!="00"){

				if(!isset($stat[$dep])){
					$stat[$dep]=array(
						"dep" => $dep,
						"nb_geo" => 0,
						"ca_geo" => 0,
						"nb_gc" => 0,
						"ca_gc" => 0,
						"nb_tot" => 0,
						"ca_tot" => 0
					);
				}

				if($d["fac_cli_categorie"]==2){
					// GC
					$stat[$dep]["ca_gc"]=$stat[$dep]["ca_gc"]+$d["fli_montant_ht"];
					$stat["tot"]["ca_gc"]=$stat["tot"]["ca_gc"]+$d["fli_montant_ht"];

					if(!isset($client_unique[$dep . "-" . $d["acl_client"]])){

						$client_unique[$dep . "-" . $d["acl_client"]]=true;

						$stat[$dep]["nb_gc"]=$stat[$dep]["nb_gc"]+1;
						$stat["tot"]["nb_gc"]=$stat["tot"]["nb_gc"]+1;

						$stat[$dep]["nb_tot"]=$stat[$dep]["nb_tot"]+1;
						$stat["tot"]["nb_tot"]=$stat["tot"]["nb_tot"]+1;

					}
				}else{

					$stat[$dep]["ca_geo"]=$stat[$dep]["ca_geo"]+$d["fli_montant_ht"];
					$stat["tot"]["ca_geo"]=$stat["tot"]["ca_geo"]+$d["fli_montant_ht"];

					if(!isset($client_unique[$dep . "-" . $d["acl_client"]])){

						$client_unique[$dep . "-" . $d["acl_client"]]=true;

						$stat[$dep]["nb_geo"]=$stat[$dep]["nb_geo"]+1;
						$stat["tot"]["nb_geo"]=$stat["tot"]["nb_geo"]+1;

						$stat[$dep]["nb_tot"]=$stat[$dep]["nb_tot"]+1;
						$stat["tot"]["nb_tot"]=$stat["tot"]["nb_tot"]+1;

					}

				}
				$stat[$dep]["ca_tot"]=$stat[$dep]["ca_tot"]+$d["fli_montant_ht"];

				$stat["tot"]["ca_tot"]=$stat["tot"]["ca_tot"]+$d["fli_montant_ht"];

				//}
			}

		}

		// DONNEES GC

		if($acc_societe==4 AND ($agence==4 OR $agence==0)){

			// on part de gfc pour liste les factures a prendre en compte

			$soc_bcl=0;

			$sql="SELECT fli_montant_ht,fli_action_client,fli_action_cli_soc
			,fac_client,fac_cli_categorie
			FROM Factures_Lignes,Factures
			WHERE fli_facture=fac_id AND NOT fli_action_cli_soc=4 AND NOT fli_action_cli_soc=0 AND fac_date>=:periode_deb AND fac_date<=:periode_fin";
			if(!empty($agence)){
				$sql.=" AND fac_agence=:agence";
			}
			if(!empty($commercial)){
				$sql.=" AND fac_commercial=:commercial";
			}
			$sql.=" ORDER BY fli_action_cli_soc;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":periode_deb",$periode_deb);
			$req->bindParam(":periode_fin",$periode_fin);
			if(!empty($agence)){
				$req->bindParam(":agence",$agence);
			}
			if(!empty($commercial)){
				$req->bindParam(":commercial",$commercial);
			}
			$req->execute();
			$d_donnees_gc=$req->fetchAll();
			/*echo("<pre>");
				print_r($d_donnees_gc);
			echo("</pre>");*/

			if(!empty($d_donnees_gc)){

				foreach($d_donnees_gc as $d_gc){

					if($d_gc["fli_action_cli_soc"]!=$soc_bcl){

						$soc_bcl=$d_gc["fli_action_cli_soc"];

						$ConnFct=connexion_fct($soc_bcl);

						$sql_act_gc="SELECT act_adr_cp FROM Actions,Actions_Clients WHERE act_id=acl_action AND acl_id=:action_client;";
						$req_act_gc=$ConnFct->prepare($sql_act_gc);
					}


					// pour chaque action, on va recuperer le departement

					$req_act_gc->bindParam(":action_client",$d_gc["fli_action_client"]);
					$req_act_gc->execute();
					$d_action=$req_act_gc->fetch();
					if(!empty($d_action)){

						if(!empty($d_action["act_adr_cp"])){
							$cp=trim($d_action["act_adr_cp"]);
							$cp=substr($cp,0,2);
							$dep=intval($cp);
							if($dep<10){
								$dep="0" . $dep;
							}
						}else{
							$dep="00";
						}

						if(!isset($stat[$dep])){
							$stat[$dep]=array(
								"dep" => $dep,
								"nb_geo" => 0,
								"ca_geo" => 0,
								"nb_gc" => 0,
								"ca_gc" => 0,
								"nb_tot" => 0,
								"ca_tot" => 0
							);
						}

						if($d_gc["fac_cli_categorie"]==2){

							$stat[$dep]["ca_gc"]=$stat[$dep]["ca_gc"]+$d_gc["fli_montant_ht"];
							$stat["tot"]["ca_gc"]=$stat["tot"]["ca_gc"]+$d_gc["fli_montant_ht"];

							if(!isset($client_unique[$dep . "-" . $d_gc["fac_client"]])){

								$client_unique[$dep . "-" . $d_gc["fac_client"]]=true;

								$stat[$dep]["nb_gc"]=$stat[$dep]["nb_gc"]+1;
								$stat["tot"]["nb_gc"]=$stat["tot"]["nb_gc"]+1;

								$stat[$dep]["nb_tot"]=$stat[$dep]["nb_tot"]+1;
								$stat["tot"]["nb_tot"]=$stat["tot"]["nb_tot"]+1;

							}
						}else{

							$stat[$dep]["ca_geo"]=$stat[$dep]["ca_geo"]+$d_gc["fli_montant_ht"];
							$stat["tot"]["ca_geo"]=$stat["tot"]["ca_geo"]+$d_gc["fli_montant_ht"];

							if(!isset($client_unique[$dep . "-" . $d_gc["fac_client"]])){

								$client_unique[$dep . "-" . $d_gc["fac_client"]]=true;

								$stat[$dep]["nb_geo"]=$stat[$dep]["nb_geo"]+1;
								$stat["tot"]["nb_geo"]=$stat["tot"]["nb_geo"]+1;

								$stat[$dep]["nb_tot"]=$stat[$dep]["nb_tot"]+1;
								$stat["tot"]["nb_tot"]=$stat["tot"]["nb_tot"]+1;

							}

						}

						$stat[$dep]["ca_tot"]=$stat[$dep]["ca_tot"]+$d_gc["fli_montant_ht"];
						$stat["tot"]["ca_tot"]=$stat["tot"]["ca_tot"]+$d_gc["fli_montant_ht"];

					}
				}

			}
		}
		asort($stat);


		// TITRE DE LA STAT

		if(!empty($commercial)){

			$sql="SELECT com_label_1,com_label_2 FROM Commerciaux WHERE com_id=:commercial;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":commercial",$commercial);
			$req->execute();
			$d_commercial=$req->fetch();
			if(!empty($d_commercial)){
				$titre="Répartition du CA de " . $d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"];
			}

		}elseif(!empty($agence)){

			$sql="SELECT age_nom,soc_nom FROM Agences,Societes WHERE age_societe=soc_id AND age_id=:agence;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":agence",$agence);
			$req->execute();
			$d_agence=$req->fetch();
			if(!empty($d_agence)){
				$titre="Répartition du CA de " . $d_agence["soc_nom"] . " - " . $d_agence["age_nom"];
			}

		}else{

			$sql="SELECT soc_nom FROM Societes WHERE soc_id=:acc_societe;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":acc_societe",$acc_societe);
			$req->execute();
			$d_societe=$req->fetch();
			if(!empty($d_societe)){
				$titre="Répartition du CA de " . $d_societe["soc_nom"];
			}
		}

		$titre.="<br/>Du " . $DT_periode_deb->format("d/m/Y") . " au " . $DT_periode_fin->format("d/m/Y");

	}
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">

<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" >
					<div class="row ">
			<?php		if(!empty($erreur_txt)){
							echo("<p class='alert alert-danger'>" . $erreur_txt . "</p>");
						}else{ ?>
							<h1 class="text-center" ><?=$titre?></h1>
							<div class="alert alert-info">
								Les factures à Si2P GFC ne sont pas prises en compte dans les calculs.
							</div>
							<div class="table-responsive">
								<table class="table table-striped table-hover" >
									<thead>
										<tr class="dark">
											<th rowspan="2" class="text-center" >Département</th>
											<th colspan="2" class="text-center" >Géographique</th>
											<th colspan="2" class="text-center" >Grands-Comptes</th>
											<th colspan="2" class="text-center" >Total</th>
										</tr>
										<tr class="dark">
											<th class="text-center" >Nb. clients</th>
											<th class="text-center" >CA</th>
											<th class="text-center">Nb. clients</th>
											<th class="text-center" >CA</th>
											<th class="text-center" >Nb. clients</th>
											<th class="text-center" >CA</th>
										</tr>
									</thead>
									<tbody>
							<?php	foreach($stat as $num_dep => $s){
										if($num_dep!="00" AND $num_dep!="tot" ){ ?>
											<tr>
												<td><?=$num_dep?></td>
												<td class="text-right" >
											<?php	if(!empty($s["nb_geo"])){
														echo($s["nb_geo"]);
													} ?>
												</td>
												<td class="text-right" >
											<?php	if(!empty($s["ca_geo"])){ ?>
														<a href="stat_com_ca_dep_detail.php?agence=<?=$agence?>&commercial=<?=$commercial?>&categorie=1&departement=<?=$num_dep?>&periode_deb=<?=$periode_deb?>&periode_fin=<?=$periode_fin?>" >
															<?=number_format($s["ca_geo"],2,","," ")?>
														</a>
											<?php	} ?>
												</td>
												<td class="text-right" >
											<?php	if(!empty($s["nb_gc"])){
														echo($s["nb_gc"]);
													} ?>
												</td>
												<td class="text-right" >
											<?php	if(!empty($s["ca_gc"])){ ?>
														<a href="stat_com_ca_dep_detail.php?agence=<?=$agence?>&commercial=<?=$commercial?>&categorie=2&departement=<?=$num_dep?>&periode_deb=<?=$periode_deb?>&periode_fin=<?=$periode_fin?>" >
															<?=number_format($s["ca_gc"],2,","," ")?>
														</a>
											<?php	} ?>

												</td>
												<td class="text-right" >
											<?php	if(!empty($s["nb_tot"])){
														echo($s["nb_tot"]);
													} ?>
												</td>
												<td class="text-right" >
											<?php	if(!empty($s["ca_tot"])){ ?>
														<a href="stat_com_ca_dep_detail.php?agence=<?=$agence?>&commercial=<?=$commercial?>&departement=<?=$num_dep?>&periode_deb=<?=$periode_deb?>&periode_fin=<?=$periode_fin?>" >
															<?=number_format($s["ca_tot"],2,","," ")?>
														</a>
											<?php	} ?>
												</td>
											</tr>
							<?php		}
									}

									if(!empty($stat["00"]["ca_tot"])){ ?>

										<tr>
											<td>Actions sans CP</td>
								<?php		if(!empty($stat["00"]["ca_geo"])){ ?>
												<td class="text-right"><?=$stat["00"]["nb_geo"]?></td>
												<td class="text-right" >
													<a href="stat_com_ca_dep_detail.php?agence=<?=$agence?>&commercial=<?=$commercial?>&categorie=1&periode_deb=<?=$periode_deb?>&periode_fin=<?=$periode_fin?>&departement=00" >
														<?=number_format($stat["00"]["ca_geo"],2,","," ")?>
													</a>
												</td>
								<?php		}else{ ?>
												<td class="text-right"></td>
												<td class="text-right" ></td>
								<?php		}
											if(!empty($stat["00"]["ca_gc"])){ ?>
												<td class="text-right" ><?=$stat["00"]["nb_gc"]?></td>
												<td class="text-right" >
													<a href="stat_com_ca_dep_detail.php?agence=<?=$agence?>&commercial=<?=$commercial?>&categorie=2&periode_deb=<?=$periode_deb?>&periode_fin=<?=$periode_fin?>&departement=00" >
														<?=number_format($stat["00"]["ca_gc"],2,","," ")?>
													</a>
												</td>
								<?php		}else{ ?>
												<td class="text-right"></td>
												<td class="text-right" ></td>
								<?php		} ?>
											<td class="text-right" ><?=$stat["00"]["nb_tot"]?></td>
											<td class="text-right" >
												<a href="stat_com_ca_dep_detail.php?agence=<?=$agence?>&commercial=<?=$commercial?>&periode_deb=<?=$periode_deb?>&periode_fin=<?=$periode_fin?>&departement=00" >
													<?=number_format($stat["00"]["ca_tot"],2,","," ")?>
												</a>
											</td>
										</tr>
							<?php 	} ?>

									</tbody>
									<tfoot>
										<tr>
											<td>Total :</td>
											<td class="text-right"><?=$stat["tot"]["nb_geo"]?></td>
											<td class="text-right" >
												<a href="stat_com_ca_dep_detail.php?agence=<?=$agence?>&commercial=<?=$commercial?>&categorie=1&periode_deb=<?=$periode_deb?>&periode_fin=<?=$periode_fin?>" >
													<?=number_format($stat["tot"]["ca_geo"],2,","," ")?>
												</a>
											</td>
											<td class="text-right" ><?=$stat["tot"]["nb_gc"]?></td>
											<td class="text-right" >
												<a href="stat_com_ca_dep_detail.php?agence=<?=$agence?>&commercial=<?=$commercial?>&categorie=2&periode_deb=<?=$periode_deb?>&periode_fin=<?=$periode_fin?>" >
													<?=number_format($stat["tot"]["ca_gc"],2,","," ")?>
												</a>
											</td>
											<td class="text-right" ><?=$stat["tot"]["nb_tot"]?></td>
											<td class="text-right" >
												<a href="stat_com_ca_dep_detail.php?agence=<?=$agence?>&commercial=<?=$commercial?>&periode_deb=<?=$periode_deb?>&periode_fin=<?=$periode_fin?>" >
													<?=number_format($stat["tot"]["ca_tot"],2,","," ")?>
												</a>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
	<?php				} ?>
					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="stat_commercial.php" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){



			});
		</script>
	</body>
</html>
