<?php

	// STATISTIQUE CA PAR PRODUIT

	include "includes/controle_acces.inc.php";

	include "includes/connexion.php";
	include "includes/connexion_fct.php";


	// CONTROLE ACCES
	//die();
	if($_SESSION['acces']['acc_service'][1]!=1 AND $_SESSION['acces']['acc_service'][2]!=1 AND $_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']['acc_service'][5]!=1 AND $_SESSION['acces']["acc_profil"]!=15){
		echo("Impossible d'afficher la page!");
		die();
	}elseif(!isset($_SESSION["crit_stat"])){
		echo("Formulaire incomplet");
		die();
	}elseif(empty($_SESSION["crit_stat"])){
		echo("Formulaire incomplet");
		die();
	}

	// CRITERE

	$crit_stat=$_SESSION["crit_stat"];

	$DT_periode=date_create_from_format('Y-m-d',$crit_stat["periode_deb"]);
	if(!is_bool($DT_periode)){
		$periode_deb_fr=$DT_periode->format("d/m/Y");
	}

	$DT_periode=date_create_from_format('Y-m-d',$crit_stat["periode_fin"]);
	if(!is_bool($DT_periode)){
		$periode_fin_fr=$DT_periode->format("d/m/Y");
	}

	$produit_id=0;
	if(!empty($_GET["produit"])){
		$produit_id=intval($_GET["produit"]);
	}

	// RETOUR

	$_SESSION["retourFacture"]="stat_com_produit_fac.php?produit=" . $produit_id;
	$_SESSION['retour'] = "stat_com_produit_fac.php?produit=" . $produit_id;
	$_SESSION["retourClient"]="stat_com_produit_fac.php?produit=" . $produit_id;
	$_SESSION["retour_action"]="stat_com_produit_fac.php?produit=" . $produit_id;
	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	$acc_utilisateur=0;
	if(!empty($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
		}
	}

	// TRAITEMENT

	$d_soc_src=array();

	if($crit_stat["reseau"]){

		$sql="SELECT uso_societe,uso_agence FROM Utilisateurs_Societes WHERE uso_utilisateur=" . $acc_utilisateur . " ORDER BY uso_societe,uso_agence DESC;";
		$req=$Conn->query($sql);
		$d_societes=$req->fetchAll();
		if(!empty($d_societes)){
			foreach($d_societes as $s){
				if(!isset($d_soc_src[$s["uso_societe"]])){
					if(!empty($s["uso_agence"])){
						$d_soc_src[$s["uso_societe"]]=$s["uso_agence"];
					}else{
						$d_soc_src[$s["uso_societe"]]="";
					}
				}else{
					if(!empty($s["uso_agence"])){
						$d_soc_src[$s["uso_societe"]].="," . $s["uso_agence"];
					}else{
						$d_soc_src[$s["uso_societe"]]="";
					}
				}
			}
		}

	}elseif($crit_stat["ca_gc"]){

		// recherche sur GFC
		$d_soc_src[$acc_societe]="";
		if(!empty($crit_stat["agence"])){
			$d_soc_src[$acc_societe]=$crit_stat["agence"];
		}

		// on ajoutes toutes les autres societes pour la ca gc
		$sql="SELECT soc_id FROM Societes WHERE NOT soc_id=4 AND NOT soc_archive ORDER BY soc_id;";
		$req=$Conn->query($sql);
		$d_societes=$req->fetchAll();
		if(!empty($d_societes)){
			foreach($d_societes as $s){
				$d_soc_src[$s["soc_id"]]="";
			}
		}

	}else{

		$d_soc_src[$acc_societe]="";
		if(!empty($crit_stat["agence"])){
			$d_soc_src[$acc_societe]=$crit_stat["agence"];
		}

	}

	// LE CLIENT CONCERNE

	if(!empty($crit_stat["client"])){

		$sql="SELECT cli_code FROM Clients WHERE cli_id=" . $crit_stat["client"] . ";";
		$req=$Conn->query($sql);
		$d_client=$req->fetch();
	}

	// SUR LE PRODUIT
	if(!empty($produit_id)){
		$sql="SELECT pro_code_produit FROM Produits WHERE pro_id=" . $produit_id . ";";
		$req=$Conn->query($sql);
		$d_produit=$req->fetch();
	}

	/*echo("<pre>");
		print_r($d_produits);
	echo("</pre>");
	die();*/

	$total_ca=0;
	$total_qte=0;
	$d_factures=array();
	foreach($d_soc_src as $s_id => $s_age_list){

		$ConnFct=connexion_fct($s_id);

		$sql="SELECT fli_montant_ht,fli_qte,fli_code_produit,fli_intra_inter
		,fac_numero,DATE_FORMAT(fac_date,'%d/%m/%Y') AS fac_date_fr,fac_commercial,fac_id," . $s_id . " AS societ
		,cli_code,cli_nom, cli_id
		,com_label_1,com_label_2
		FROM Factures_Lignes INNER JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
		LEFT JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)
		LEFT JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)";
		if($crit_stat["ca_gc"]==1 AND $s_id!=4){
			// demande du ca GC depuis GFC
			// on n'ajoute une jointure pour ne recupérer que les CA facture en direct
			// le CA facture via GFC sera comptabilisé sur la boucle $s_id==4
			// on ne peut pas se contenter de la table facture car sur la facture vers GFC cli_categorie=2
			$sql.=" LEFT JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id)";
		}
		$sql.=" WHERE fac_date>='". $crit_stat["periode_deb"] . "' AND fac_date<='" . $crit_stat["periode_fin"] . "'";
		if(!empty($s_age_list)){
			$sql.=" AND fac_agence IN (" . $s_age_list. ")";
		}
		if(!empty($crit_stat["pro_type"])){
			$sql.=" AND fli_intra_inter=" . $crit_stat["pro_type"];
		}
		if(!empty($produit_id)){
			$sql.=" AND fli_produit=" . $produit_id;
		}elseif(!empty($crit_stat["produit_list"])){
			$sql.=" AND fli_produit IN (" . $crit_stat["produit_list"] . ")";
		}else{
			if(!empty($crit_stat["pro_categorie"])){
				$sql.=" AND fli_categorie=" . $crit_stat["pro_categorie"];
			}
			if(!empty($crit_stat["pro_famille"])){
				$sql.=" AND fli_famille=" . $crit_stat["pro_famille"];
			}
			if(!empty($crit_stat["pro_sous_famille"])){
				$sql.=" AND fli_sous_famille=" . $crit_stat["pro_sous_famille"];
			}
			if(!empty($crit_stat["pro_sous_sous_famille"])){
				$sql.=" AND fli_sous_sous_famille=" . $crit_stat["pro_sous_sous_famille"];
			}
		}
		if(!empty($crit_stat["client"])){
			if(empty($crit_stat["filiale"])){
				// client seul
				$sql.=" AND fac_client=" . $crit_stat["client"];
			}else{
				// MM et filiale
				$sql.=" AND (cli_id=" . $crit_stat["client"] . " OR cli_filiale_de=" . $crit_stat["client"] . ")";
			}
		}
		if($crit_stat["ca_gc"]==1 AND $s_id!=4){
			// les fac à gfc sont fac_cli_categorie=2 donc obligé d'utilisé acl_facturation pour ne pas comptabiliser 2 fois le CA
			//
			$sql.=" AND fac_cli_categorie=2 AND (NOT fli_categorie=1 OR acl_facturation=0)";
		}

		$sql.=" ORDER BY fac_chrono;";
		$req=$ConnFct->query($sql);
		$d_results_fac=$req->fetchAll();
		if(!empty($d_results_fac)){
			$d_factures = array_merge($d_factures, $d_results_fac);
		}
	}

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >

		<div id="main">

<?php		include "includes/header_def.inc.php"; ?>

			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" >
					<div class="row ">

						<h1 class="text-center" >
					<?php	if(isset($d_client)){
								echo($d_client["cli_code"]);
								if(!empty($filiale)){
									echo(" et filiale");
								}
								echo("<br/>");
							}

							echo("CA");
							if($crit_stat["pro_type"]==1){
								echo(" INTRA");
							}elseif($crit_stat["pro_type"]==2){
								echo(" INTER");
							}
							if(!empty($produit_id)){
								echo(" " . $d_produit["pro_code_produit"] . " du " . $periode_deb_fr . " au " . $periode_fin_fr);
							}else{
								echo(" par produit du " . $periode_deb_fr . " au " . $periode_fin_fr);
							}


							?>
						</h1>

						<div class="table-responsive">
							<table class="table table-striped table-hover" >
								<thead>
									<tr class="dark">
										<th>Code</th>
										<th>Nom</th>
										<th>Facture</th>
										<th>Date</th>
										<th>Commercial</th>
										<th>Produit</th>
										<th>INTRA / INTER</th>
										<th>Qte</th>
										<th>CA</th>
									</tr>
								</thead>
								<tbody>
						<?php		if(!empty($d_factures)){
										foreach($d_factures as $f){
											$total_qte=$total_qte + $f["fli_qte"];
											$total_ca=$total_ca + $f["fli_montant_ht"];
											?>
											<tr>
												<td>
												<a href="client_voir.php?client=<?= $f["cli_id"] ?>&societ=<?=$f["societ"]?>">
														<?=$f["cli_code"]?>
													</a>
											</td>
												<td><?=$f["cli_nom"]?></td>
												<td>
													<a href="facture_voir.php?facture=<?=$f["fac_id"]?>&societ=<?=$f["societ"]?>" >
														<?=$f["fac_numero"]?>
													</a>
												</td>
												<td><?=$f["fac_date_fr"]?></td>
												<td><?=$f["com_label_2"] . " " . $f["com_label_1"]?></td>
												<td><?=$f["fli_code_produit"]?></td>
												<td>
											<?php	if($f["fli_intra_inter"]==1){
														echo("INTRA");
													}else{
														echo("INTER");
													}?>
												</td>
												<td class="text-right" ><?=$f["fli_qte"]?></td>
												<td class="text-right" ><?=$f["fli_montant_ht"]?></td>
											</tr>
						<?php			} ?>
										<tr>
											<th colspan="7" >Total :</th>
											<td class="text-right" ><?=$total_qte?></td>
											<td class="text-right" ><?=number_format($total_ca,2,","," ")?></td>
										</tr>
						<?php		} ?>
								</tbody>
							</table>
						</div>

					</div>
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="stat_com_produit.php" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function(){



			});
		</script>
	</body>
</html>
