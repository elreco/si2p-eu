<?php

	include_once 'includes/connexion.php';
	include_once 'connexion_soc.php';
	

	include('modeles/mod_famille.php');
	include('modeles/mod_sous_famille.php');
	include('modeles/mod_categorie.php');
	
	$erreur=0;
	
	$categorie=0;
	$famille=0;
	$sous_famille=0;
	
	if(!empty($_POST)){
		if(isset($_POST['client'])){
			$client=intval($_POST['client']);
		}
		if(isset($_POST['categorie'])){
			$categorie=intval($_POST['categorie']);
		}
		if(isset($_POST['famille'])){
			$famille=intval($_POST['famille']);
		}
		if(isset($_POST['sous_famille'])){
			$sous_famille=intval($_POST['sous_famille']);
		}
		
	}elseif(!empty($_GET)){
		
		if(isset($_GET['client'])){
			$client=intval($_GET['client']);
		}
		if(isset($_GET['categorie'])){
			$categorie=intval($_GET['categorie']);
		}
		if(isset($_GET['famille'])){
			$famille=intval($_GET['famille']);
		}
		if(isset($_GET['sous_famille'])){
			$sous_famille=intval($_GET['sous_famille']);
		}
	}

	if($client>0){
		
		// SUR LE Clients
		
		$client_produit=$client;
		$sql="SELECT cli_id,cli_groupe,cli_filiale_de FROM Clients WHERE cli_id=" . $client . ";";
		$req=$ConnSoc->query($sql);
		$d_client=$req->fetch();
		if(!empty($d_client)){
			if($d_client["cli_filiale_de"]>0){
				$client_produit=$d_client["cli_filiale_de"];	
			}
		}
			
		$produit_client=array();
		$sql="SELECT cpr_produit,cpr_ht_intra,cpr_ht_inter,cpr_archive FROM Clients_Produits WHERE cpr_client=" . $client_produit . " ORDER BY cpr_produit;";
		$req=$ConnSoc->query($sql);
		$result=$req->fetchAll();
		if(!empty($result)){
			foreach($result as $r){
				$produit_client[$r["cpr_produit"]]=array(
					"ht_intra" => $r["cpr_ht_intra"],
					"ht_inter" => $r["cpr_ht_inter"],
					"archive" => $r["cpr_archive"]
				);	
				
			}
		}

		$sql="SELECT pro_id,pro_libelle,pro_reference,pro_reference_inter,pro_intra,pro_inter,
		pca_libelle,pfa_libelle,psf_libelle
		FROM (((Produits LEFT JOIN Produits_Categories ON (Produits.pro_categorie=Produits_Categories.pca_id))
		LEFT JOIN Produits_Familles ON (Produits.pro_famille=Produits_Familles.pfa_id))
		LEFT JOIN Produits_Sous_Familles ON (Produits.pro_categorie=Produits_Sous_Familles.psf_id))
		WHERE NOT pro_archive";
		if($categorie>0){
			$sql.=" AND pro_categorie=" . $categorie;	
		}
		if($famille>0){
			$sql.=" AND pro_famille=" . $famille;	
		}
		if($sous_famille>0){
			$sql.=" AND pro_sous_famille=" . $sous_famille;	
		}
		$sql.=" ORDER BY pro_reference,pro_reference_inter;";
		$req=$Conn->query($sql);
		$d_produits=$req->fetchAll();
		
	
		
	}else{
		$erreur=1;
	}
	

?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
	
		
		<!-- Start: Main -->
		<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn">				
					
					<div class="admin-form theme-primary ">
						<div class="panel heading-border panel-primary">
							<div class="panel-body bg-light">
								<div class="content-header text-center">
									<h2>Base <b class="text-primary">produits</b></h2>
								</div>
								
								<form method="post" action="client_produit_ajout.php" >
									<div>
										<input type="hidden" name="client" value="<?=$client?>" />
									</div>	
									<div class="row" >
										<div class="col-md-3">
											<select class="select2" name="categorie"   >
												<option value="0" >Catégorie...</option>
												<?= get_categorie_select($categorie); ?>
											</select>
										</div>
										<div class="col-md-3">
											<select class="select2" name="famille"  >
												<option value="0">Famille...</option>
												<?= get_famille_select($famille); ?>
											</select>
										</div>
										<div class="col-md-3">
											<select class="select2" name="sous_famille" >
												<option value="0">Sous-famille ...</option>
												<?= get_sous_famille_select($famille,$sous_famille); ?>
											</select>
										</div>					
										<div class="col-md-3">
											<button type="submit" class="btn btn-primary">Filtrer</button>
										</div>
									</div>
								</form>
								
								<form method="post" action="client_produit_enr.php" id="form_save" >
									<div>
										<input type="hidden" name="client" value="<?=$client?>" />
									</div>	
									<table class="table table-striped table-hover mt10" >
										<thead>
											<tr class="info">
												<th>Catégorie</th>
												<th>Famille</th>
												<th>Sous-famille</th>
												<th>Libellé</th>
												<th>Référence intra</th>
												<th>Prix intra</th>
												<th>Référence inter</th>
												<th>Prix inter</th>	
												<th>
													<label class="option">
														<input type="checkbox" value="" id="case_archive_all" class="check-all" />
														<span class="checkbox"></span>Archivé
													</label>
												</th>													
											</tr>
										</thead>
										<tbody>
								<?php		if(!empty($d_produits)){
												foreach($d_produits as $pv){ 
													$ht_intra=0;
													if(!empty($produit_client[$pv["pro_id"]]["ht_intra"])){
														$ht_intra=$produit_client[$pv["pro_id"]]["ht_intra"];
													}
													$ht_inter=0;
													if(!empty($produit_client[$pv["pro_id"]]["ht_inter"])){
														$ht_inter=$produit_client[$pv["pro_id"]]["ht_inter"];
													}
													$archive=0;
													if(!empty($produit_client[$pv["pro_id"]]["archive"])){
														$archive=$produit_client[$pv["pro_id"]]["archive"];
													}
													
													?>
													<tr>
														<td><?=$pv["pca_libelle"]?></td>
														<td><?=$pv["pfa_libelle"]?></td>
														<td><?=$pv["psf_libelle"]?></td>
														<td><?=$pv["pro_libelle"]?></td>
												<?php	if($pv["pro_intra"]){ ?>
															<td><?=$pv["pro_reference"]?></td>
															<td>
																<input type="text" name="ht_intra_<?=$pv["pro_id"]?>" value="<?=$ht_intra?>" class="gui-input float" />
															</td>
												<?php	}else{
															echo("<td colspan='2' >&nbsp;</td>");
														}
														if($pv["pro_inter"]){ ?>
															<td><?=$pv["pro_reference_inter"]?></td>
															<td>
																<input type="text" name="ht_inter_<?=$pv["pro_id"]?>" value="<?=$ht_inter?>" class="gui-input float" />
															</td>
												<?php	}else{
															echo("<td colspan='2' >&nbsp;</td>");
														} ?>
														<td>
															<label class="option">
																<input type="checkbox" name="archive_<?=$pv["pro_id"]?>" value="on" class="case-archive" <?php if($archive) echo("checked"); ?> />
																<span class="checkbox"></span>
															</label>
														</td>
													</tr>
								<?php			}
											}else{ ?>
												<tr>
													<td class="warning" colspan="8" >
														Aucun produit ne correspond à votre recherche.														
													</td>
												</tr>
								<?php		} ?>										
										</tbody>
									</table>
								</form>		
							</div>
						</div>
					</div>
			   
				</section>
				<!-- End: Content -->
			</section>
			<!-- End: Content WRAPPER -->
		</div>
		<!-- End: Main -->	
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="client_voir.php?client=<?=$client?>&tab13" class="btn btn-default btn-sm" >
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle"></div>
				<div class="col-xs-3 footer-right" >
					<button type="button" class="btn btn-success btn-sm" id="btn_save" >
					  <i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>	

		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				$("#btn_save").click(function(){
					console.log("SUBMIT");
					$("#form_save").submit();	
				});
				$(".float").blur(function(){
					numerique=$(this).val();
					numerique=numerique.replace(",",".");
					numerique=parseFloat(numerique);
					if(isNaN(numerique)){
						numerique=0;
					}			
					$(this).val(numerique);
				});
			});
		</script>
	</body>
</html>
