<?php 
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_document.php');
include('modeles/mod_upload.php');
include('modeles/mod_erreur.php');
include('modeles/mod_parametre.php');
include('modeles/mod_profil.php');
include('modeles/mod_societe.php');
include('modeles/mod_agence.php');
include('modeles/mod_utilisateur.php');
include('modeles/mod_produit.php');

if(!isset($_POST['submit2']) && !isset($_POST['delete_doc'])){


  $nom = pathinfo($_FILES['cdo_doc']['name']);



  $erreur = upload('cdo_doc', 'clients/', clean($nom['filename']), 0, 0, 0);

  if($erreur == 0){


    $req = $ConnSoc->prepare("INSERT INTO clients_documents (cdo_client, cdo_type, cdo_nom, cdo_fichier, cdo_ext, cdo_pro_famille, cdo_pro_sous_famille) VALUES (:cdo_client, :cdo_type, :cdo_nom, :cdo_fichier, :cdo_ext, :cdo_pro_famille, :cdo_pro_sous_famille)");
    $req->bindParam(':cdo_client', $_GET['client']);
    $req->bindParam(':cdo_type', $_POST['cdo_type']);
    $req->bindParam(':cdo_nom', $_POST['cdo_nom']);
    $req->bindParam(':cdo_fichier', clean($nom['filename'])); 
    $req->bindParam(':cdo_ext', $nom['extension']);
    $req->bindParam(':cdo_pro_famille', $_POST['cdo_famille']);
    $req->bindParam(':cdo_pro_sous_famille', $_POST['cdo_sous_famille']);
    $req->execute();

    Header("Location: client_voir.php?client=" . $_GET['client'] . "&tab7");
  }else{
    Header("Location: client_voir.php?client=" . $_GET['client'] . "&error=5&tab7");
  }
}elseif(isset($_POST['submit2']) && !isset($_POST['delete_doc'])){
  $erreur = 0;
  if(!empty($_FILES['cdo_doc']['tmp_name'])){
   
    $nom = pathinfo($_FILES['cdo_doc']['name']);
    $erreur = upload('cdo_doc', 'clients/', clean($nom['filename']), 0, 0, 0);

  }

  if($erreur == 0){

    if(empty($_FILES['cdo_doc']['tmp_name'])){

      $req = $ConnSoc->prepare("SELECT * FROM clients_documents WHERE cdo_id = " . $_POST['cdo_id']);
      $req->execute();
      $donnee = $req->fetch();
      $nom['filename'] = $donnee['cdo_fichier'];
      $nom['extension'] = $donnee['cdo_ext'];
    }else{
      $nom['filename'] = clean($nom['filename']);
      $nom['extension'] = $nom['extension'];
    }
    $req = $ConnSoc->prepare("UPDATE clients_documents SET cdo_client = :cdo_client, cdo_type = :cdo_type, cdo_nom =:cdo_nom, cdo_fichier = :cdo_fichier, cdo_ext =:cdo_ext, cdo_pro_famille = :cdo_pro_famille, cdo_pro_sous_famille = :cdo_pro_sous_famille WHERE cdo_id = :cdo_id");
    $req->bindParam(':cdo_client', $_GET['client']);
    $req->bindParam(':cdo_id', $_POST['cdo_id']);
    $req->bindParam(':cdo_type', $_POST['cdo_type']);
    $req->bindParam(':cdo_nom', $_POST['cdo_nom']);
    $req->bindParam(':cdo_fichier', $nom['filename']);
    $req->bindParam(':cdo_ext', $nom['extension']);
    $req->bindParam(':cdo_pro_famille', $_POST['cdo_pro_famille']);
    $req->bindParam(':cdo_pro_sous_famille', $_POST['cdo_pro_sous_famille']);
    $req->execute();

    Header("Location: client_voir.php?client=" . $_GET['client'] . "&tab7");
  }else{
    
    Header("Location: client_voir.php?client=" . $_GET['client'] . "&error=5&tab7");
  }
}

if(isset($_POST['delete_doc'])){
  
  $req = $ConnSoc->prepare("SELECT * FROM clients_documents WHERE cdo_id = " . $_POST['cdo_id']);
  $req->execute();
  $donnee = $req->fetch();
  $req = $ConnSoc->prepare("DELETE FROM clients_documents WHERE cdo_id = :cdo_id");
  $req->bindParam("cdo_id",$_POST['cdo_id']);
  $req->execute();
  unlink('documents/clients/' . $donnee['cdo_fichier'] ."." . $donnee['cdo_ext']);
  Header("Location: client_voir.php?client=" . $_GET['client'] . "&tab7&succes=15");
}
?>