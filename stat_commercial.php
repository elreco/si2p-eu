<?php

// ECRAN D'ACCUEIL DES STATISTIQUES DE L'ONGLET COMMERCE

$menu_actif = "1-9";

include "includes/controle_acces.inc.php";
include "modeles/mod_parametre.php";
include "includes/connexion.php";
include "includes/connexion_soc.php";


$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
// controle d'accès
/* Jérôme a validé le fait que toutes les stats qui lui sont accéssibles le soient également pour le service tech.
a revalider pour chaque nouvelle stat
*/

$acces_stats = array();

if ($_SESSION['acces']['acc_service'][1] == 1 or $_SESSION['acces']['acc_service'][3] == 1 OR $_SESSION['acces']['acc_profil'] == 11 OR $_SESSION['acces']['acc_profil'] == 14) {
	// service commercial (RE), DT, DAF, DG
	$acces_stats["stat_1"] = true;	// CA clients par famille de produit
	$acces_stats["stat_2"] = true;	// CA par produits
	$acces_stats["stat_3"] = true;	// HISTO CA par client
	$acces_stats["stat_4"] = true;	// Répartition GEO du CA
	$acces_stats["stat_5"] = true;	// CA par code APE
	$acces_stats["stat_6"] = true;	// CA par qualification
	$acces_stats["stat_7"] = true;	// Tableau de pilotage
} elseif ($_SESSION['acces']['acc_profil'] == 15 or ($_SESSION['acces']['acc_profil'] == 3 AND $_SESSION["acces"]["acc_droits"][35]) ){
	$acces_stats["stat_2"] = true;
	$acces_stats["stat_3"] = true;
	$acces_stats["stat_7"] = true;
} elseif ($_SESSION['acces']['acc_profil'] == 3) {
	$acces_stats["stat_3"] = true;
}



if($_SESSION['acces']['acc_profil']==12){
	// RRH
	$acces_stats["stat_7"] = true;	// Tableau de pilotage
}
if ($_SESSION['acces']['acc_profil'] == 14 OR $_SESSION['acces']['acc_profil'] == 11) {
	// stat prospection -> DG
	$acces_stats["stat_8"] = true;
}



if (empty($acces_stats)) {
	header("location :deconnect.php");
	die();
}



// LE PERSONNE CONNECTE

$acc_societe = 0;
if (isset($_SESSION['acces']["acc_societe"])) {
	$acc_societe = intval($_SESSION['acces']["acc_societe"]);
}

$acc_agence = 0;
if (isset($_SESSION['acces']["acc_agence"])) {
	$acc_agence = intval($_SESSION['acces']["acc_agence"]);
}

$acc_utilisateur = 0;
if (!empty($_SESSION['acces']["acc_ref"])) {
	if ($_SESSION['acces']["acc_ref"] == 1) {
		$acc_utilisateur = intval($_SESSION['acces']["acc_ref_id"]);
	}
}

// supp des criteres mémorisés
if (isset($_SESSION["crit_stat"])) {
	unset($_SESSION["crit_stat"]);
}
// supp session retour
if (isset($_SESSION["retourFacture"])) {
	unset($_SESSION["retourFacture"]);
}

// AGENCE

if (empty($acc_agence)) {
	$sql = "SELECT age_id,age_nom FROM Agences WHERE age_societe=" . $acc_societe . " ORDER BY age_nom";
	$req = $Conn->query($sql);
	$d_agences = $req->fetchAll();
	if (empty($d_agences)) {
		unset($d_agences);
	}
}

// LES COMMERCIAUX

$sql = "SELECT com_id,com_label_1,com_label_2 FROM Commerciaux";
if ($acc_agence > 0) {
	$sql .= " WHERE com_agence=" . $acc_agence;
	$sql .= " AND com_archive = 0";
} else {
	$sql .= " WHERE com_archive = 0";
}
if (!$_SESSION["acces"]["acc_droits"][6] && ($_SESSION['acces']['acc_profil'] == 3 and !$_SESSION["acces"]["acc_droits"][35])) {
	$sql .= " AND com_type=1 AND com_ref_1=" . $acc_utilisateur;
}
$sql .= " ORDER BY com_label_1,com_label_2";
$req = $ConnSoc->query($sql);
$d_commerciaux = $req->fetchAll();

// CATEGORIE DE PRODUITS

$sql = "SELECT pca_id,pca_libelle,pca_planning FROM Produits_Categories ORDER BY pca_libelle";
$req = $Conn->query($sql);
$d_pro_categories = $req->fetchAll();

// FAMILLES DE PRODUITS

$sql = "SELECT pfa_id,pfa_libelle FROM Produits_Familles ORDER BY pfa_libelle";
$req = $Conn->query($sql);
$d_pro_familles = $req->fetchAll();

// DEPARTEMENTS

$sql = "SELECT * FROM Departements";
$req = $Conn->query($sql);
$departements = $req->fetchAll();

// QUALIFICATIONS
$sql = "SELECT * FROM Qualifications";
$req = $Conn->query($sql);
$d_qualifications = $req->fetchAll();

// EXERCICE EN COURS

if(date("m")<4) {
	$exercice=date("Y")-1;
}else{
	$exercice=date("Y");
}


?>
<!DOCTYPE html>
<html>

<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
</head>

<body class="sb-top sb-top-sm">
	<div id="main">
		<?php
		if ($_SESSION["acces"]["acc_ref"] == 3) {
			include "includes/header_sta.inc.php";
		} elseif ($_SESSION["acces"]["acc_ref"] == 2) {
			include "includes/header_cli.inc.php";
		} else {
			include "includes/header_def.inc.php";
		} ?>
		<section id="content_wrapper">
			<!-- Begin: Content -->
			<section id="content" class="animated fadeIn">


				<h1 class="text-center">Statistiques</h1>

				<div class="row">
					<?php if (!empty($acces_stats["stat_1"])) { ?>
						<!-- STAT 1 // CA clients par famille de produit -->
						<div class="col-md-3">
							<div class="panel mt10 panel-primary">
								<div class="panel-heading panel-head-sm">
									<span class="panel-title">CA clients par famille de produit</span>
								</div>
								<div class="panel-intro">
									Affiche le CA facturé de chaque client par famille de produit.
									Cette statistique ne prend en compte que les produits "Formation et Audits" et "Communications".
								</div>
								<div class="panel-body">
									<form method="post" action="stat_com_cli_pfam.php">
										<div class="admin-form">
											<div class="row mt15">
												<div class="col-md-12">
													<label for="com_stat_1">Commercial</label>
													<select class="select2" id="com_stat_1" name="commercial">
														<option value='0'>Commercial ...</option>
														<?php if (!empty($d_commerciaux)) {
															foreach ($d_commerciaux as $com) {
																echo ("<option value='" . $com["com_id"] .  "' >" . $com["com_label_1"] . " " . $com["com_label_2"] . "</option>");
															}
														} ?>
													</select>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-6">
													<label for="periode_deb_stat_1">Période du</label>
													<span class="field prepend-icon">
														<input type="text" id="periode_deb_stat_1" name="periode_deb" class="gui-input datepicker" placeholder="Facturé entre le" required />
														<span class="field-icon"><i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
												<div class="col-md-6">
													<label for="periode_fin_stat_1">au</label>
													<span class="field prepend-icon">
														<input type="text" id="periode_fin_stat_1" name="periode_fin" class="gui-input datepicker" placeholder="Facturé entre le" required />
														<span class="field-icon"><i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
											</div>

											<div class="row mt15">
												<div class="col-md-12">
													<div class="col-md-6">
														<div class="radio-custom mb5">
															<input id="com_src_stat_1" name="com_src" type="radio" value="1" checked>
															<label for="com_src_stat_1">Commercial fiche client</label>
														</div>
													</div>

													<div class="col-md-6">
														<div class="radio-custom mb5">
															<input id="com_src_stat_2" name="com_src" type="radio" value="2">
															<label for="com_src_stat_2">Commercial facture</label>
														</div>
													</div>
												</div>
											</div>

											<div class="row mt15">
												<div class="col-md-12 text-center">
													<button type="submit" class="btn btn-md btn-primary">
														Lancer les calculs
													</button>
												</div>
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>

					<?php	} ?>

					<!-- STAT 2 // CA par produit -->
					<?php if (!empty($acces_stats["stat_2"])) { ?>
						<div class="col-md-3">
							<div class="panel mt10 panel-primary">
								<div class="panel-heading panel-head-sm">
									<span class="panel-title">CA par produits</span>
								</div>
								<div class="panel-intro">
									Affiche le CA facturé par code produit pour la période sélectionnée.
								</div>
								<div class="panel-body">
									<form method="post" action="stat_com_produit.php">
										<div class="admin-form">
											<?php if ($_SESSION["acces"]["acc_droits"][28]) { ?>
												<div class="row mt15">
													<div class="col-md-4">
														<label class="option option-dark">
															<input type="checkbox" name="reseau" id="reseau_2" value="on" class="reseau" data-stat="2" />
															<span class="checkbox"></span>Réseau
														</label>
													</div>
													<div class="col-md-8">
														<small>Cette option permet d'obtenir les valeurs pour l'ensemble des sociétés auxquelles vous avez accès.</small>
													</div>
												</div>
											<?php	}
											if (isset($d_agences)) { ?>
												<div class="row mt15 no-reseau-2">
													<div class="col-md-12">
														<label for="agence_2">Agence :</label>
														<select name="agence" id="agence_2" class="select2">
															<option value="0">Agence ...</option>
															<?php foreach ($d_agences as $agence) {
																echo ("<option value='" . $agence["age_id"] . "' >" . $agence["age_nom"] . "</option>");
															} ?>
														</select>
													</div>
												</div>
											<?php	} ?>

											<div class="row mt15">
												<div class="col-md-6">
													<label for="periode_deb_stat_2">Période du</label>
													<span class="field prepend-icon">
														<input type="text" id="periode_deb_stat_2" name="periode_deb" class="gui-input datepicker" placeholder="Facturé entre le" required />
														<span class="field-icon"><i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
												<div class="col-md-6">
													<label for="periode_fin_stat_2">au</label>
													<span class="field prepend-icon">
														<input type="text" id="periode_fin_stat_2" name="periode_fin" class="gui-input datepicker" placeholder="Facturé entre le" required />
														<span class="field-icon"><i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider">
														<span>Produits</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<label for="pro_categorie_2">Catégorie :</label>
													<select name="pro_categorie" id="pro_categorie_2" class="select2 stat-pro-cat" data-stat="2">
														<option value="0">Catégorie...</option>
														<?php foreach ($d_pro_categories as $cat) {
															echo ("<option value='" . $cat["pca_id"] . "' >" . $cat["pca_libelle"] . "</option>");
														} ?>
													</select>
												</div>
											</div>
											<?php	/* FG pas de lien entre le type et le choix du produit
													si on appel un type intra et qu'on selectionne un code qui n'existe quand inter = pas de résultat */ ?>
											<div class="row mt15 bloc-formation-2" style="display:none;">
												<div class="col-md-12">
													<label for="pro_type_2">Type :</label>
													<select name="pro_type" id="pro_type_2" class="select2 stat-pro-type" data-stat="2">
														<option value="0">Type...</option>
														<option value="1">INTRA</option>
														<option value="2">INTER</option>
													</select>
												</div>
											</div>
											<div class="row mt15 bloc-formation-2" style="display:none;">
												<div class="col-md-12">
													<label for="pro_famille_2">Famille :</label>
													<select name="pro_famille" id="pro_famille_2" class="select2 stat-pro-fam" data-stat="2">
														<option value="0">Famille...</option>
														<?php foreach ($d_pro_familles as $fam) {
															echo ("<option value='" . $fam["pfa_id"] . "' >" . $fam["pfa_libelle"] . "</option>");
														} ?>
													</select>
												</div>
											</div>
											<div class="row mt15 bloc-formation-2" style="display:none;">
												<div class="col-md-12">
													<label for="pro_sous_famille_2">Sous-Famille :</label>
													<select name="pro_sous_famille" id="pro_sous_famille_2" class="select2 stat-pro-sous-fam" data-stat="2">
														<option value="0">Sous-Famille...</option>
													</select>
												</div>
											</div>
											<div class="row mt15 bloc-formation-2" style="display:none;">
												<div class="col-md-12">
													<label for="pro_sous_sous_famille_2">Sous Sous-Famille :</label>
													<select name="pro_sous_sous_famille" id="pro_sous_sous_famille_2" class="select2 stat-pro-sous-sous-fam" data-stat="2">
														<option value="0">Sous Sous-Famille...</option>
													</select>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-12">
													<label for="produit_2">Produits :</label>
													<select name="produit_list[]" id="produit_2" class="select2" multiple="multiple">
														<option value="0">Produits...</option>
													</select>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-9">
													<label for="client_2">Client :</label>
													<select name="client" id="client_2" class="select2-client-n">
														<option value="0">Client...</option>
													</select>
												</div>
												<div class="col-md-3">
													<div class="option-group field mt25">
														<label class="option option-dark">
															<input type="checkbox" name="filiale" value="on">
															<span class="checkbox"></span>et filiale
														</label>
													</div>
												</div>
											</div>
											<?php if ($acc_societe == 4 and ($acc_agence == 0 or $acc_agence == 4) and $_SESSION["acces"]["acc_droits"][8]) { ?>
												<div class="row">
													<div class="col-md-12">
														<div class="option-group field mt25">
															<label class="option option-dark">
																<input type="checkbox" name="ca_gc" value="on" class="check-reseau-2" />
																<span class="checkbox"></span>Inclure le CA GC hors région (référencements et accords mixtes).
															</label>
														</div>
													</div>
												</div>
											<?php		} ?>

											<!--	FG 15/01/2020
												critere non focntionnel sur les resultats 
												commente pour release 2.2.0
													<div class="row mt15" >
														<div class="col-md-12" >
															<label for="departements" >Départements (de facturation) :</label>
															<select name="departements[]" multiple="multiple" class="select2" id="departements" style="width:100%" >
																<?php foreach ($departements as $d) { ?>
																	<option value="<?= $d['dep_id'] ?>" ><?= $d['dep_nom'] ?> (<?= $d['dep_numero'] ?>)</option>
																<?php } ?>
															</select>
														</div>
													</div>-->
											<div class="row mt15">
												<div class="col-md-12 text-center">
													<button type="submit" class="btn btn-md btn-primary">
														Lancer les calculs
													</button>
												</div>
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>
					<?php	} ?>
					<!-- STAT 3 // HISTO CA par client -->
					<?php if (!empty($acces_stats["stat_3"])) { ?>
						<div class="col-md-3">
							<div class="panel mt10 panel-primary">
								<div class="panel-heading panel-head-sm">
									<span class="panel-title">Historique du CA par client</span>
								</div>
								<div class="panel-intro">
									Affiche le CA par client sur le N dernières années.
								</div>
								<div class="panel-body">
									<form method="post" action="stat_com_histo.php">
										<div class="admin-form">
											<?php if ($_SESSION["acces"]["acc_droits"][28]) { ?>
												<div class="row mt15">
													<div class="col-md-4">
														<label class="option option-dark">
															<input type="checkbox" name="reseau" id="histo_reseau" value="on" class="reseau" data-stat="histo">
															<span class="checkbox"></span>Réseau
														</label>
													</div>
													<div class="col-md-8">
														<small>Cette option permet d'obtenir les valeurs pour l'ensemble des sociétés auxquelles vous avez accès.</small>
													</div>
												</div>
											<?php	}
											if (isset($d_agences)) { ?>
												<div class="row mt15 no-reseau-histo">
													<div class="col-md-12">
														<label for="histo_agence">Agence :</label>
														<select name="agence" id="histo_agence" class="select2 select2-com-agence">
															<option value="0">Agence ...</option>
															<?php foreach ($d_agences as $agence) {
																echo ("<option value='" . $agence["age_id"] . "' >" . $agence["age_nom"] . "</option>");
															} ?>
														</select>
													</div>
												</div>
											<?php	} ?>

											<div class="row">
												<div class="col-md-12">
													<div class="section-divider">
														<span>Période</span>
													</div>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-6 text-center">
													<div class="radio-custom mb5">
														<input id="histo_per_compta" name="periode" type="radio" value="1" checked>
														<label for="histo_per_compta">Année comptable</label>
													</div>
												</div>
												<div class="col-md-6 text-center">
													<div class="radio-custom mb5">
														<input id="histo_per_civile" name="periode" type="radio" value="2">
														<label for="histo_per_civile">Année civile</label>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider">
														<span>Historique</span>
													</div>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-4 text-center">
													<div class="radio-custom mb5">
														<input id="histo_3ans" name="annee" type="radio" checked value="3">
														<label for="histo_3ans">3 ans</label>
													</div>
												</div>
												<div class="col-md-4 text-center">
													<div class="radio-custom mb5">
														<input id="histo_5ans" name="annee" type="radio" value="5">
														<label for="histo_5ans">5 ans</label>
													</div>
												</div>
												<div class="col-md-4 text-center">
													<div class="radio-custom mb5">
														<input id="histo_10ans" name="annee" type="radio" value="10">
														<label for="histo_10ans">10 ans</label>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<div class="section-divider">
														<span>Produits</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<label for="pro_categorie_3">Catégorie :</label>
													<select name="pro_categorie" id="pro_categorie_3" class="select2 stat-pro-cat" data-stat="3">
														<option value="0">Catégorie...</option>
														<?php foreach ($d_pro_categories as $cat) {
															echo ("<option value='" . $cat["pca_id"] . "' >" . $cat["pca_libelle"] . "</option>");
														} ?>
													</select>
												</div>
											</div>
											<?php	/* FG pas de lien entre le type et le choix du produit
													si on appel un type intra et qu'on selectionne un code qui n'existe quand inter = pas de résultat */ ?>
											<div class="row mt15 bloc-formation-3" style="display:none;">
												<div class="col-md-12">
													<label for="pro_type_3">Type :</label>
													<select name="pro_type" id="pro_type_3" class="select2 stat-pro-type" data-stat="3">
														<option value="0">Type...</option>
														<option value="1">INTRA</option>
														<option value="2">INTER</option>
													</select>
												</div>
											</div>
											<div class="row mt15 bloc-formation-3" style="display:none;">
												<div class="col-md-12">
													<label for="pro_famille_3">Famille :</label>
													<select name="pro_famille" id="pro_famille_3" class="select2 stat-pro-fam" data-stat="3">
														<option value="0">Famille...</option>
														<?php foreach ($d_pro_familles as $fam) {
															echo ("<option value='" . $fam["pfa_id"] . "' >" . $fam["pfa_libelle"] . "</option>");
														} ?>
													</select>
												</div>
											</div>
											<div class="row mt15 bloc-formation-3" style="display:none;">
												<div class="col-md-12">
													<label for="pro_sous_famille_3">Sous-Famille :</label>
													<select name="pro_sous_famille" id="pro_sous_famille_3" class="select2 stat-pro-sous-fam" data-stat="3">
														<option value="0">Sous-Famille...</option>
													</select>
												</div>
											</div>
											<div class="row mt15 bloc-formation-3" style="display:none;">
												<div class="col-md-12">
													<label for="pro_sous_sous_famille_3">Sous Sous-Famille :</label>
													<select name="pro_sous_sous_famille" id="pro_sous_sous_famille_3" class="select2 stat-pro-sous-sous-fam" data-stat="3">
														<option value="0">Sous Sous-Famille...</option>
													</select>
												</div>
											</div>
											<div class="row no-reseau-histo">
												<div class="col-md-12">
													<div class="section-divider ">
														<span>Commercial</span>
													</div>
												</div>
											</div>
											<div class="row mt15 no-reseau-histo">
												<div class="col-md-12">
													<label for="histo_commercial">Commercial du client :</label>
													<select name="commercial" id="histo_commercial" class="select2 select2-commercial">
														<option value="0">Commercial...</option>
														<?php foreach ($d_commerciaux as $com) { ?>
															<option value="<?= $com["com_id"] ?>"><?= $com["com_label_1"] . " " . $com["com_label_2"] ?></option>
														<?php	} ?>
													</select>
												</div>
											</div>

											<div class="row mt15">
												<div class="col-md-12 text-center">
													<button type="submit" class="btn btn-md btn-primary">
														Lancer les calculs
													</button>
												</div>
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>
					<?php	} ?>

					<!-- STAT 4 // Répartition GEO du CA -->
					<?php if (!empty($acces_stats["stat_4"])) { ?>
						<div class="col-md-3">
							<div class="panel mt10 panel-primary">
								<div class="panel-heading panel-head-sm">
									<span class="panel-title">Répartition géographique du CA</span>
								</div>
								<div class="panel-intro">
									Affiche la répartition du CA par département en fonction du lieu de formation en distinguant les clients regionnaux des grands-comptes.<br /><b>Important :</b> la facturation vers GFC et CA "Communication" ne sont pas comptabilisés dans cette stat.
								</div>
								<div class="panel-body">
									<form method="post" action="stat_com_ca_dep.php">
										<div class="admin-form">
											<?php if (isset($d_agences)) { ?>
												<div class="row mt15">
													<div class="col-md-12">
														<label for="ca_dep_agence">Agence :</label>
														<select name="agence" id="ca_dep_agence" class="select2 select2-com-agence" data-select_commercial="#ca_dep_commercial">
															<option value="0">Agence ...</option>
															<?php foreach ($d_agences as $agence) {
																echo ("<option value='" . $agence["age_id"] . "' >" . $agence["age_nom"] . "</option>");
															} ?>
														</select>
													</div>
												</div>
											<?php	} ?>
											<div class="row mt15">
												<div class="col-md-12">
													<label for="ca_dep_commercial">Commercial :</label>
													<select name="commercial" id="ca_dep_commercial" class="select2">
														<option value="0">Commercial ...</option>
														<?php foreach ($d_commerciaux as $com) {
															echo ("<option value='" . $com["com_id"] . "' >" . $com["com_label_1"] . " " . $com["com_label_2"] . "</option>");
														} ?>
													</select>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-6">
													<label for="ca_dep_periode_deb">Période du</label>
													<span class="field prepend-icon">
														<input type="text" id="ca_dep_periode_deb" name="periode_deb" class="gui-input datepicker" placeholder="Facturé entre le" required />
														<span class="field-icon"><i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
												<div class="col-md-6">
													<label for="ca_dep_periode_fin">au</label>
													<span class="field prepend-icon">
														<input type="text" id="ca_dep_periode_fin" name="periode_fin" class="gui-input datepicker" placeholder="Facturé entre le" required />
														<span class="field-icon"><i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-12 text-center">
													<button type="submit" class="btn btn-md btn-primary">
														Lancer les calculs
													</button>
												</div>
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>
					<?php	} ?>
				</div>

				<div class="row">
					<!-- STAT 5 // CA par code APE -->
					<?php if (!empty($acces_stats["stat_5"])) { ?>
						<div class="col-md-3">
							<div class="panel mt10 panel-primary">
								<div class="panel-heading panel-head-sm">
									<span class="panel-title">CA par code APE</span>
								</div>
								<div class="panel-intro">
									Affiche le CA facturé de chaque code APE.
									Cette statistique ne prend en compte que les produits "Formation et Audits" et "Communications".
								</div>
								<div class="panel-body">
									<form method="post" action="stat_com_ape.php">
										<div class="admin-form">
											<div class="row mt15">
												<div class="col-md-12">
													<label for="com_stat_5">Commercial</label>
													<select class="select2 commercial" id="com_stat_5" name="commercial" data-stat="5">
														<option value='0'>Commercial ...</option>
														<?php if (!empty($d_commerciaux)) {
															foreach ($d_commerciaux as $com) {
																echo ("<option value='" . $com["com_id"] .  "' >" . $com["com_label_1"] . " " . $com["com_label_2"] . "</option>");
															}
														} ?>
													</select>
												</div>
											</div>
											<div class="row mt15 commercial-require-5" style="display:none;">
												<div class="col-md-12">
													<div class="col-md-6">
														<div class="radio-custom mb5">
															<input id="com_src_stat_5" name="com_src" type="radio" value="1" checked>
															<label for="com_src_stat_5">Commercial fiche client</label>
														</div>
													</div>

													<div class="col-md-6">
														<div class="radio-custom mb5">
															<input id="com_src_stat_4" name="com_src" type="radio" value="2">
															<label for="com_src_stat_4">Commercial facture</label>
														</div>
													</div>
												</div>
											</div>

											<div class="row mt15">
												<div class="col-md-6">
													<label for="periode_deb_stat_5">Période du</label>
													<span class="field prepend-icon">
														<input type="text" id="periode_deb_stat_5" name="periode_deb" class="gui-input datepicker" placeholder="Facturé entre le" required />
														<span class="field-icon"><i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
												<div class="col-md-6">
													<label for="periode_fin_stat_5">au</label>
													<span class="field prepend-icon">
														<input type="text" id="periode_fin_stat_5" name="periode_fin" class="gui-input datepicker" placeholder="Facturé entre le" required />
														<span class="field-icon"><i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<div class="section-divider ">
														<span>Affichage</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 text-center">
													<div class="radio-custom mb5">
														<input id="aff_ape" name="affichage" type="radio" value="1" checked />
														<label for="aff_ape">Afficher par code APE</label>
													</div>
												</div>
												<div class="col-md-6 text-center">
													<div class="radio-custom mb5">
														<input id="aff_div" name="affichage" type="radio" value="2" />
														<label for="aff_div">Afficher par division</label>
													</div>
												</div>
											</div>


											<div class="row mt15">
												<div class="col-md-12 text-center">
													<button type="submit" class="btn btn-md btn-primary">
														Lancer les calculs
													</button>
												</div>
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>
					<?php 	} ?>

					<?php if (!empty($acces_stats["stat_6"])) { ?>
						<div class="col-md-3">
							<div class="panel mt10 panel-primary">
								<div class="panel-heading panel-head-sm">
									<span class="panel-title">CA Qualification</span>
								</div>
								<div class="panel-intro">
									Affiche le CA par client dans le cadre d'une qualification sur une période donnée.
								</div>
								<div class="panel-body">
									<form method="post" action="stat_com_qualif.php">
										<div class="admin-form">


											<div class="row mt15">
												<div class="col-md-12">
													<label for="com_stat_5">Qualification :</label>
													<select class="select2" id="com_stat_5" name="qualification" data-stat="5" required>
														<option value=''>Qualification ...</option>
														<?php if (!empty($d_qualifications)) {
															foreach ($d_qualifications as $qualif) {
																echo ("<option value='" . $qualif["qua_id"] .  "' >" . $qualif["qua_libelle"] . "</option>");
															}
														} ?>
													</select>
												</div>
											</div>

											<div class="row mt15">
												<div class="col-md-6">
													<label for="periode_deb_stat_6">Période du</label>
													<span class="field prepend-icon">
														<input type="text" id="periode_deb_stat_6" name="periode_deb" class="gui-input datepicker" placeholder="Facturé entre le" required />
														<span class="field-icon"><i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
												<div class="col-md-6">
													<label for="periode_fin_stat_6">au</label>
													<span class="field prepend-icon">
														<input type="text" id="periode_fin_stat_6" name="periode_fin" class="gui-input datepicker" placeholder="Facturé entre le" required />
														<span class="field-icon"><i class="fa fa-calendar-o"></i>
														</span>
													</span>
												</div>
											</div>

											<div class="row mt15">
												<div class="col-md-12 text-center">
													<button type="submit" class="btn btn-md btn-primary">
														Lancer les calculs
													</button>
												</div>
											</div>

										</div>

									</form>
								</div>
							</div>
						</div>
					<?php 	} ?>

			<?php 	if (!empty($acces_stats["stat_7"])) { ?>
						<div class="col-md-3">
							<div class="panel mt10 panel-primary">
								<div class="panel-heading panel-head-sm">
									<span class="panel-title">Tableau de pilotage</span>
								</div>
								<div class="panel-intro">
									Evolution du CA entre l'exercice N et N-1.
								</div>
								<div class="panel-body">

									<div class="row mt15">
										<div class="col-md-6 text-center">
											<a href="stat_pilotage.php?type=1" class="btn btn-md btn-primary">
												Tableau INTRA
											</a>
										</div>
										<div class="col-md-6 text-center">
											<a href="stat_pilotage.php?type=2" class="btn btn-md btn-primary">
												Tableau INTER
											</a>
										</div>
									</div>
							<?php	if($_SESSION['acces']["acc_profil"]==9 OR $_SESSION['acces']["acc_profil"]==11 OR $_SESSION['acces']["acc_profil"]==12 OR $_SESSION['acces']["acc_profil"]==14 OR $acc_utilisateur==403){ 
										// DT DAF, RRH, DG et Christine ?>
										<div class="row mt15">
											<div class="col-md-12 text-center">
												<a href="stat_pilotage.php" class="btn btn-md btn-primary">
													Tableau Réseau
												</a>
											</div>
										</div>
							<?php	} ?>
								</div>
							</div>
						</div>
		<?php 		}
					if (!empty($acces_stats["stat_8"])) { ?>
						<div class="col-md-3">
							<div class="panel mt10 panel-primary">
								<div class="panel-heading panel-head-sm">
									<span class="panel-title">Prospection</span>
								</div>
								<div class="panel-body admin-form" >
									<form method="post" action="stat_com_prospection.php">
										<div class="row mt15">
											<div class="col-md-12">
												<label for="exercice_5">Exercice :</label>
												<select class="select2" id="exercice_5" name="exercice" data-stat="5" required>													
											<?php 	for ($bcl_exercice=$exercice-10;$bcl_exercice<=$exercice;$bcl_exercice++) {
														if($bcl_exercice == $exercice){
															echo ("<option value='" . $bcl_exercice .  "' selected >" . $bcl_exercice . "/" . intval($bcl_exercice+1) . "</option>");
														} else {
															echo ("<option value='" . $bcl_exercice .  "' >" . $bcl_exercice . "/" . intval($bcl_exercice+1) . "</option>");
														}
														
													} ?>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="section-divider">
													<span>Type de client</span>
												</div>
											</div>
										</div>
										<div class="row mt15">
											<div class="col-md-4 text-center">
												<div class="radio-custom mb5">
													<input id="cli_cat_geo" name="cli_categorie" type="radio" checked value="1">
													<label for="cli_cat_geo">Client géographique</label>
												</div>
											</div>
											<div class="col-md-4 text-center">
												<div class="radio-custom mb5">
													<input id="cli_cat_gc" name="cli_categorie" type="radio" value="2">
													<label for="cli_cat_gc">Client GC</label>
												</div>
											</div>
											<div class="col-md-4 text-center">
												<div class="radio-custom mb5">
													<input id="cli_cat_all" name="cli_categorie" type="radio" value="0" checked >
													<label for="cli_cat_all">Tous les clients</label>
												</div>
											</div>
										</div>
										<div class="row mt15">
											<div class="col-md-12 text-center">
												<button type="submit" class="btn btn-md btn-primary">
													Lancer les calculs
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
		<?php 		} ?>
				</div>
			</section>
		</section>
	</div>
	<footer id="content-footer" class="affix">
		<div class="row">
			<div class="col-xs-3 text-center"></div>
			<div class="col-xs-6 footer-middle text-center" style=""></div>
			<div class="col-xs-3 footer-right"></div>
		</div>
	</footer>

	<?php include "includes/footer_script.inc.php"; ?>
	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<!-- SCRIPT SPE -->
	<script type="text/javascript">
		var produit_cat = new Array();
		produit_cat[0] = 0;

		<?php foreach ($d_pro_categories as $cat) { ?>
			produit_cat[<?= $cat["pca_id"] ?>] = "<?= $cat["pca_planning"] ?>";
		<?php	} ?>

		jQuery(document).ready(function() {

			$(".reseau").click(function() {
				stat = $(this).data("stat");
				if (stat != "") {
					if ($(this).is(":checked")) {
						$(".no-reseau-" + stat).hide();
						$(".check-reseau-" + stat).prop("checked", true);
						$(".check-reseau-" + stat).prop("disabled", true);
					} else {
						$(".no-reseau-" + stat).show();
						$(".check-reseau-" + stat).prop("disabled", false);
					}
				}
			});

			$(".stat-pro-cat").change(function() {

				$stat = $(this).data("stat");
				if ($(this).val() > 0) {
					if (produit_cat[$(this).val()] == 1) {
						$(".bloc-formation-" + $stat).show();
						if ($("#produit_" + $stat).length > 0) {
							actualiser_select2("", "#produit_" + $stat, 0);
						}
					} else {
						masquer_formation($stat);
						if ($("#produit_" + $stat).length > 0) {
							get_produits(0, $(this).val(), 0, 0, 0, null, null, actualiser_select2, "#produit_" + $stat, 0);
						}
					}
				} else {
					masquer_formation($stat);
					if ($("#produit_" + $stat).length > 0) {
						get_produits(0, $(this).val(), 0, 0, 0, null, null, actualiser_select2, "#produit_" + $stat, 0);
					}
				}


			});


			$(".stat-pro-fam").change(function() {
				$stat = $(this).data("stat");
				if ($stat > 0) {
					actualiser_select2("", "#pro_sous_sous_famille_" + $stat, 0);
					actualiser_select2("", "#produit_" + $stat, 0);
					if ($(this).val() > 0) {
						get_sous_familles($(this).val(), actualiser_select2, "#pro_sous_famille_" + $stat, 0);
					} else {
						actualiser_select2("", "#pro_sous_famille_" + $stat, 0);

					}
				}
			});

			$(".stat-pro-sous-fam").change(function() {
				$stat = $(this).data("stat");
				if ($stat > 0) {
					actualiser_select2("", "#pro_sous_sous_famille_" + $stat, 0);
					actualiser_select2("", "#produit_" + $stat, 0);
					if ($(this).val() > 0) {
						get_sous_sous_familles($(this).val(), afficher_sous_sous_famille, $stat, "");
					}
				}
			});

			$(".stat-pro-sous-sous-fam").change(function() {
				$stat = $(this).data("stat");
				if ($stat > 0) {
					get_produits(0, 1, $("#pro_famille_" + $stat).val(), $("#pro_sous_famille_" + $stat).val(), $("#pro_sous_sous_famille_" + $stat).val(), null, null, actualiser_select2, "#produit_" + $stat, 0);
				}
			});

			$(".commercial").change(function() {
				$stat = $(this).data("stat");
				if ($stat > 0) {
					if ($(this).val() != "" && $(this).val() != 0) {
						$(".commercial-require-" + $stat).show();
					} else {
						$(".commercial-require-" + $stat).hide();
					}
				}
			});

		});

		function afficher_sous_sous_famille(json, stat) {
			if (json == "") {
				if ($("#produit_" + $stat).length > 0) {
					// il n'y a pas de sous sous famille, on peut afficher les produits
					get_produits(0, 1, $("#pro_famille_" + $stat).val(), $("#pro_sous_famille_" + $stat).val(), 0, null, null, actualiser_select2, "#produit_" + $stat, 0);
				}

			} else {
				actualiser_select2(json, "#pro_sous_sous_famille_" + $stat, 0);
			}


		}

		function masquer_formation($stat) {

			$("#pro_famille_" + $stat).val(0).trigger("change");
			$(".bloc-formation-" + $stat).hide();

		}
	</script>
</body>

</html>