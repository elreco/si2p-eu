<?php
include "includes/controle_acces.inc.php";

// TRAITEMENT DU FORM client_correspondance

include("includes/connexion.php");
include("modeles/mod_set_contact.php");
include("modeles/mod_set_correspondance.php");

if(isset($_POST)){
	
	$client=0;
	if(!empty($_POST['client'])){
		$client=intval($_POST['client']);
	}
	
	$cor_date_sql="";
	if(!empty($_POST['cor_date'])){
		$date = date_create_from_format('d/m/Y',$_POST['cor_date']);
		if(!is_bool($date)){
			$cor_date_sql=$date->format("Y-m-d");
		}
	}

	if($client>0 AND !empty($cor_date_sql)){
		
		$categorie=0;
		if(!empty($_POST['categorie'])){
			$categorie=intval($_POST['categorie']);
		}
		
		$edit_correspondance=array();
		
		if(!empty($_POST['correspondance'])){
			$edit_correspondance["cor_id"]=intval($_POST['correspondance']);
		}
		if($_SESSION['acces']["acc_ref"]==1){
			$edit_correspondance["cor_utilisateur"]=$_SESSION['acces']["acc_ref_id"];
		}
		$edit_correspondance["cor_client"]=$client;
		$edit_correspondance["cor_date"]=$cor_date_sql;
		
		$edit_correspondance["cor_type"]=1;
		if(!empty($_POST['cor_type'])){
			$edit_correspondance["cor_type"]=intval($_POST['cor_type']);
		}
		
		$edit_correspondance["cor_raison"]=0;
		$edit_correspondance["cor_raison_info"]="";
		if(!empty($_POST['cor_raison'])){
			$edit_correspondance["cor_raison"]=intval($_POST['cor_raison']);
			if($edit_correspondance["cor_raison"]==-1){
				$edit_correspondance["cor_raison_info"]=$_POST['cor_raison_info'];
			}
		}

		// contact
	
		$nv_contact=false;	
		$edit_correspondance["cor_contact"]=0;
		$edit_correspondance["cor_contact_nom"]="";
		$edit_correspondance["cor_contact_prenom"]="";
		$edit_correspondance["cor_contact_tel"]="";
		if(!empty($_POST['cor_contact'])){
			if($_POST['cor_contact']=="nouveau"){
				if($categorie!=3){
					$nv_contact=true;
				}
			}else{
				$edit_correspondance["cor_contact"]=intval($_POST['cor_contact']);
				$edit_correspondance["cor_contact_nom"]=$_POST['cor_contact_nom'];
				$edit_correspondance["cor_contact_prenom"]=$_POST['cor_contact_prenom'];
				$edit_correspondance["cor_contact_tel"]=$_POST['cor_contact_tel'];
			}
		}

		$edit_correspondance["cor_commentaire"]=$_POST['cor_commentaire'];
		
		$cor_rappel_sql="";
		if(!empty($_POST['cor_rappeler_le'])){
			$rappel = date_create_from_format('d/m/Y',$_POST['cor_rappeler_le']);
			if(!is_bool($rappel)){
				$edit_correspondance["cor_rappeler_le"]=$rappel->format("Y-m-d");
			}
		}
		
		if(!empty($_POST['cor_rappel'])){
			$edit_correspondance["cor_rappel"]=1;
		}else{
			$edit_correspondance["cor_rappel"]=0;
		}

		// GESTION DE L'ENREGISTREMENT D'UN NOUVEAU CONTACT
		
		if($nv_contact){
			
			if(!empty($_POST['con_nom'])){
				$con_nom=$_POST['con_nom'];
			}else{
				$nv_contact=false;
			}
			if(!empty($_POST['con_tel'])){
				$con_tel=$_POST['con_tel'];
			}else{
				$nv_contact=false;
			}
		}
		if($nv_contact){
			
			$con_fonction=0;
			$con_fonction_nom="";
			if(!empty($_POST["con_fonction"])){
				if($_POST["con_fonction"]=="autre"){					
					$con_fonction_nom=$_POST['con_fonction_nom'];
				}else{
					$con_fonction=intval($_POST["con_fonction"]);
				}
			}
			
			$con_titre=0;
			if(!empty($_POST["con_titre"])){
				$con_titre=intval($_POST["con_titre"]);
			}
			
			$edit_contact=array(
				"con_ref_id" => $client,
				"con_fonction" => $con_fonction,
				"con_fonction_nom" => $con_fonction_nom,
				"con_titre" => $con_titre,
				"con_nom" => $con_nom,
				"con_prenom" => $_POST["con_prenom"],
				"con_tel" => $con_tel,
				"con_portable" => $_POST["con_portable"],
				"con_fax" => $_POST["con_fax"],
				"con_mail" => $_POST["con_mail"]
			);
		}

		// ENREGISTREMENT
		
		
		
		// nouveau contact
		
		
		
		if($nv_contact){	
			
			$contact=set_contact($edit_contact);
			if(is_int($contact)){
				$edit_correspondance["cor_contact"]=$contact;
				$edit_correspondance["cor_contact_nom"]=$edit_contact['con_nom'];
				$edit_correspondance["cor_contact_prenom"]=$edit_contact['con_prenom'];
				$edit_correspondance["cor_contact_tel"]=$edit_contact['con_tel'];
			}
		}
		
		$correspondance=set_correspondance($edit_correspondance);
		if(is_bool($correspondance)){

			echo("Erreur : impossible d'enregsitrer la correspondance!");
			die();	
		}
		$_SESSION['message'][] = array(
			"titre" => "Succès",
			"type" => "success",
			"message" => "La correspondance a bien été ajoutée"
		);
		if(!empty($_POST["cor_devis"])){
			$_SESSION['retourDevis']="client_voir.php?client=" . $client;
			header("location : devis.php?client=" . $client . "&correspondance=" . $correspondance);
		}else{
			header("location : " . $_SESSION['retourCorresp']);
		}

	}else{
		echo("Erreur paramètre!");
		die();
	}
	
}else{
	echo("Erreur form!");
	die();
}
