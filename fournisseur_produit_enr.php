<?php 
include "includes/controle_acces.inc.php";
include "includes/connexion.php";
// ajout d'un produit
// initialisation de la catégorie
if(empty($_POST['pro_categorie'])){
	$_POST['pro_categorie'] = 0;
}

if(empty($_POST['fpr_tarif'])){
	$_POST['fpr_tarif'] = 0;
}

if(empty($_POST['pro_sous_sous_famille'])){
	$_POST['pro_sous_sous_famille'] = 0;
}
if(empty($_POST['pro_sous_famille'])){
	$_POST['pro_sous_famille'] = 0;
}

if(empty($_POST['pro_famille'])){
	$_POST['pro_famille'] = 0;
}
if(empty($_POST['produit'])){

	// si le produit n'est pas renseigné
	if($_POST['pro_type'] == 2 OR $_POST['pro_type'] == 5){
		if($_POST['fpr_pro_id'] == 0){
			Header('Location : fournisseur_produit.php?fournisseur=' . $_POST['fournisseur'] . '&error=1');
		}
	}else{
		if(empty($_POST['fpr_reference'])){
			Header('Location : fournisseur_produit.php?fournisseur=' . $_POST['fournisseur'] . '&error=2');
		}
	}
		
	
	$create = 1;
	if(!empty($_POST['fpr_pro_id'])){
		
		$req = $Conn->prepare("SELECT * FROM fournisseurs_produits WHERE fpr_pro_id = :fpr_pro_id AND fpr_fournisseur = :fpr_fournisseur");
		$req->bindParam(':fpr_pro_id', $_POST['fpr_pro_id']);
		$req->bindParam(':fpr_fournisseur', $_POST['fournisseur']);
		$req->execute();
		$fournisseur_produit_exist = $req->fetch();

		$req = $Conn->prepare("SELECT * FROM produits WHERE pro_id = :fpr_pro_id");
		$req->bindParam(':fpr_pro_id', $_POST['fpr_pro_id']);
		$req->execute();
		$produit_trouve = $req->fetch();

		if(!empty($fournisseur_produit_exist)){
			$create = 0;
		}
	}
	if($create && !empty($produit_trouve)){
		// ajout du produit
		$req = $Conn->prepare("INSERT INTO fournisseurs_produits (fpr_fournisseur, fpr_pro_id, fpr_pro_reference, fpr_type, fpr_tarif, fpr_pro_famille, fpr_pro_sous_famille,fpr_pro_sous_sous_famille,fpr_descriptif,fpr_categorie, fpr_reference, fpr_libelle) VALUES (:fpr_fournisseur, :fpr_pro_id, :fpr_pro_reference, :fpr_type, :fpr_tarif, :fpr_pro_famille, :fpr_pro_sous_famille,:fpr_pro_sous_sous_famille,  :fpr_descriptif,:fpr_categorie, :fpr_reference, :fpr_libelle)");
		$req->bindParam(':fpr_pro_id', $_POST['fpr_pro_id']);
		$req->bindParam(':fpr_pro_famille', $produit_trouve['pro_famille']);
		$req->bindParam(':fpr_pro_sous_famille', $produit_trouve['pro_sous_famille']);
		$req->bindParam(':fpr_pro_sous_sous_famille', $produit_trouve['pro_sous_sous_famille']);
		$req->bindParam(':fpr_pro_reference', $_POST['fpr_pro_reference']);
		$req->bindParam(':fpr_type', $_POST['pro_type']);
		$req->bindParam(':fpr_categorie', $_POST['fpr_categorie']);
		$req->bindParam(':fpr_tarif', $_POST['fpr_tarif']);
		$req->bindParam(':fpr_descriptif', $_POST['fpr_descriptif']);
		$req->bindParam(':fpr_libelle', $_POST['fpr_libelle']);
		$req->bindParam(':fpr_reference', $_POST['fpr_reference']);
		$req->bindParam(':fpr_fournisseur', $_POST['fournisseur']);

		$req->execute();

		Header('Location : fournisseur_voir.php?fournisseur=' . $_POST['fournisseur'] . '&tab=2&succes=7');
	}else{
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => "Le produit Si2P existe déjà pour ce fournisseur"
		);
		Header('Location : fournisseur_produit.php?fournisseur=' . $_POST['fournisseur']);
	}
	

		
	
// edition
}else{
		// si le produit n'est pas renseigné
		if($_POST['pro_type'] == 2 OR $_POST['pro_type'] == 5){
			if($_POST['fpr_pro_id'] == 0){
				Header('Location : fournisseur_produit.php?fournisseur=' . $_POST['fournisseur'] . '&edition=' . $_POST['produit']);
			}
		}else{
			if(empty($_POST['fpr_reference'])){
				Header('Location : fournisseur_produit.php?fournisseur=' . $_POST['fournisseur'] . '&edition=' . $_POST['produit']);
			}
		}
		$create = 1;
		if(!empty($_POST['fpr_pro_id'])){
			$req = $Conn->prepare("SELECT * FROM fournisseurs_produits WHERE fpr_pro_id = :fpr_pro_id AND fpr_fournisseur = :fpr_fournisseur AND fpr_id !=  :fpr_id");
			$req->bindParam(':fpr_pro_id', $_POST['fpr_pro_id']);
			$req->bindParam(':fpr_fournisseur', $_POST['fournisseur']);
			$req->bindParam(':fpr_id', $_POST['produit']);
			$req->execute();
			$fournisseur_produit_exist = $req->fetch();

			$req = $Conn->prepare("SELECT * FROM produits WHERE pro_id = :fpr_pro_id");
			$req->bindParam(':fpr_pro_id', $_POST['fpr_pro_id']);
			$req->execute();
			$produit_trouve = $req->fetch();

			if(!empty($fournisseur_produit_exist)){
				$create = 0;
			}
		}
		if($create){
			$req = $Conn->prepare("UPDATE fournisseurs_produits SET fpr_pro_id = :fpr_pro_id, fpr_pro_reference = :fpr_pro_reference, fpr_type =:fpr_type,fpr_tarif = :fpr_tarif,fpr_categorie = :fpr_categorie, fpr_type = :fpr_type, fpr_reference = :fpr_reference, fpr_libelle = :fpr_libelle, fpr_descriptif =:fpr_descriptif, fpr_pro_famille = :fpr_pro_famille, fpr_pro_sous_famille = :fpr_pro_sous_famille,fpr_pro_sous_sous_famille = :fpr_pro_sous_sous_famille WHERE fpr_id =  :fpr_id");
				$req->bindParam(':fpr_pro_id', $_POST['fpr_pro_id']);
				$req->bindParam(':fpr_pro_famille', $produit_trouve['pro_famille']);
				$req->bindParam(':fpr_pro_sous_famille', $produit_trouve['pro_sous_famille']);
				$req->bindParam(':fpr_pro_sous_sous_famille', $produit_trouve['pro_sous_sous_famille']);
				$req->bindParam(':fpr_pro_reference', $_POST['fpr_pro_reference']);
				$req->bindParam(':fpr_type', $_POST['pro_type']);
				$req->bindParam(':fpr_categorie', $_POST['fpr_categorie']);
				$req->bindParam(':fpr_tarif', $_POST['fpr_tarif']);
				$req->bindParam(':fpr_descriptif', $_POST['fpr_descriptif']);
				$req->bindParam(':fpr_libelle', $_POST['fpr_libelle']);
				$req->bindParam(':fpr_reference', $_POST['fpr_reference']);
				$req->bindParam(':fpr_id', $_POST['produit']);
			$req->execute();
			$_SESSION['message'][] = array(
				"titre" => "Erreur",
				"type" => "success",
				"message" => "Le produit du fournisseur a été actualisé"
			);
			Header('Location : fournisseur_voir.php?fournisseur=' . $_POST['fournisseur'] . '&tab=2');
		}else{
				$_SESSION['message'][] = array(
					"titre" => "Erreur",
					"type" => "danger",
					"message" => "Le produit Si2P existe déjà pour ce fournisseur"
				);
				Header('Location : fournisseur_produit.php?fournisseur=' . $_POST['fournisseur'] . '&edition=' . $_POST['produit']);
		}
		
	
}

// suppression d'un produit
/*if(isset($_GET['suppr'])){
	$req = $Conn->prepare("DELETE FROM fournisseurs_produits WHERE fpr_id = :fpr_id");
	$req->bindParam("fpr_id",$_GET['suppr']);
	$req->execute();

	Header('Location : fournisseur_voir.php?fournisseur=' . $_POST['fournisseur'] . '&tab=2&succes=9');
}*/




?>