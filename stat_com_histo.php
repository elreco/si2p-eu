<?php

// STATISTIQUE CA PAR PRODUIT

include "includes/controle_acces.inc.php";

include "includes/connexion.php";
include "includes/connexion_soc.php";
include "includes/connexion_fct.php";


// CONTROLE ACCES

if ($_SESSION['acces']['acc_service'][1] != 1 and $_SESSION['acces']['acc_service'][2] != 1 and $_SESSION['acces']['acc_service'][3] != 1 and $_SESSION['acces']['acc_service'][5] != 1 and $_SESSION['acces']["acc_profil"] != 15 and $_SESSION['acces']["acc_profil"] != 3) {
	echo ("Accès refusé!");
	die();
} elseif (empty($_SESSION['stat_com_histo']) && empty($_POST)) {
	echo ("Formulaire incomplet");
	die();
}
// RETOUR
$_SESSION['retour'] = "stat_com_histo.php";

// LE PERSONNE CONNECTE

$acc_societe = 0;
if (isset($_SESSION['acces']["acc_societe"])) {
	$acc_societe = intval($_SESSION['acces']["acc_societe"]);
}
$acc_agence = 0;
if (isset($_SESSION['acces']["acc_agence"])) {
	$acc_agence = intval($_SESSION['acces']["acc_agence"]);
}

$acc_utilisateur = 0;
if (!empty($_SESSION['acces']["acc_ref"])) {
	if ($_SESSION['acces']["acc_ref"] == 1) {
		$acc_utilisateur = intval($_SESSION['acces']["acc_ref_id"]);
	}
}


// TRAITEMENT DU FORM

// enregistrement en session pour sauvegarder
if (!empty($_POST)) {
	$_SESSION['stat_com_histo'] = $_POST;
}

$reseau = false;
if ($_SESSION["acces"]["acc_droits"][28]) {
	if (!empty($_SESSION['stat_com_histo']["reseau"])) {
		$reseau = true;
	}
}


$agence = 0;
$commercial = 0;
if (!$reseau) {

	if (empty($acc_agence)) {
		if (!empty($_SESSION['stat_com_histo']["agence"])) {
			$agence = intval($_SESSION['stat_com_histo']["agence"]);
		}
	} else {
		$agence = $acc_agence;
	}

	$commercial = 0;
	if (!empty($_SESSION['stat_com_histo']["commercial"])) {
		$commercial = intval($_SESSION['stat_com_histo']["commercial"]);
	}
}

$periode = 0;
if (!empty($_SESSION['stat_com_histo']["periode"])) {
	$periode = intval($_SESSION['stat_com_histo']["periode"]);
}

$annee = 0;
if (!empty($_SESSION['stat_com_histo']["annee"])) {
	$annee = intval($_SESSION['stat_com_histo']["annee"]);
}

$pro_categorie = 0;
if (!empty($_SESSION['stat_com_histo']["pro_categorie"])) {
	$pro_categorie = intval($_SESSION['stat_com_histo']["pro_categorie"]);
}

$pro_famille = 0;
if (!empty($_SESSION['stat_com_histo']["pro_famille"])) {
	$pro_famille = intval($_SESSION['stat_com_histo']["pro_famille"]);
}

$pro_sous_famille = 0;
if (!empty($_SESSION['stat_com_histo']["pro_sous_famille"])) {
	$pro_sous_famille = intval($_SESSION['stat_com_histo']["pro_sous_famille"]);
}

$pro_sous_sous_famille = 0;
if (!empty($_SESSION['stat_com_histo']["pro_sous_sous_famille"])) {
	$pro_sous_sous_famille = intval($_SESSION['stat_com_histo']["pro_sous_sous_famille"]);
}

$pro_type = 0;
if (!empty($_SESSION['stat_com_histo']["pro_type"])) {
	$pro_type = intval($_SESSION['stat_com_histo']["pro_type"]);
}

$periode = 0;
if (!empty($_SESSION['stat_com_histo']["periode"])) {
	$periode = intval($_SESSION['stat_com_histo']["periode"]);
}

$annee = 0;
if (!empty($_SESSION['stat_com_histo']["annee"])) {
	$annee = intval($_SESSION['stat_com_histo']["annee"]);
}

if ($periode == 1) {
	// année comptable

	if (date("m") < 4) {
		$periode_fin = date("Y") . "-" . "03-31";
		$exercice = date("Y") - 1;
	} else {
		$periode_fin = date("Y") + 1 . "-" . "03-31";
		$exercice = date("Y");
	}
} else {

	$exercice = date("Y");

	// annee civile

	$periode_fin = date("Y") . "-" . "12-31";
}

$DT_periode_fin = date_create_from_format('Y-m-d', $periode_fin);

$DT_periode_deb = date_create_from_format('Y-m-d', $periode_fin);
$DT_periode_deb->sub(new DateInterval('P' . $annee . 'Y'));
if (!is_bool($DT_periode_deb)) {
	$periode_deb = $DT_periode_deb->format("Y-m-d");
}

// DONNEE ORION 0

$d_categories = array(
	"0" => ""
);
$sql = "SELECT cca_id,cca_libelle FROM Clients_Categories ORDER BY cca_id;";
$req = $Conn->query($sql);
$results = $req->fetchAll();
if (!empty($results)) {
	foreach ($results as $r) {
		$d_categories[$r["cca_id"]] = $r["cca_libelle"];
	}
}

$d_sous_categories = array(
	"0" => ""
);
$sql = "SELECT csc_id,csc_libelle FROM Clients_Sous_Categories ORDER BY csc_id;";
$req = $Conn->query($sql);
$results = $req->fetchAll();
if (!empty($results)) {
	foreach ($results as $r) {
		$d_sous_categories[$r["csc_id"]] = $r["csc_libelle"];
	}
}

// TITRE DE LA STAT

$titre = "Historique du CA";
if ($pro_type == 1) {
	$titre .= " INTRA";
} elseif ($pro_type == 2) {
	$titre .= " INTER";
}

if ($_SESSION['acces']["acc_profil"] != 3 or $_SESSION["acces"]["acc_droits"][35]) {
	if ($reseau) {

		$titre .= " Réseau";
	} elseif (!empty($commercial)) {

		$sql = "SELECT com_label_1,com_label_2 FROM Commerciaux WHERE com_id=" . $commercial . ";";
		$req = $ConnSoc->query($sql);
		$d_commercial = $req->fetch();
		if (!empty($d_commercial)) {
			$titre .= " des clients de " . $d_commercial["com_label_2"] . " " . $d_commercial["com_label_1"];
		}
	} elseif (!empty($agence)) {

		$sql = "SELECT age_nom,soc_nom FROM Agences,Societes WHERE age_societe=soc_id AND age_id=" . $agence . ";";
		$req = $Conn->query($sql);
		$d_agence = $req->fetch();
		if (!empty($d_agence)) {
			$titre .= " " . $d_agence["soc_nom"] . " " . $d_agence["age_nom"];
		}
	} else {
		$sql = "SELECT soc_nom FROM Societes WHERE soc_id=" . $acc_societe . ";";
		$req = $Conn->query($sql);
		$d_societe = $req->fetch();
		if (!empty($d_societe)) {
			$titre = " " . $d_societe["soc_nom"];
		}
	}
	$titre .= "<br/>";
} else {
	$titre .= " ";
}

if ($periode == 1) {
	$titre .= "sur les " . $annee . " derniers exercices.";
} else {
	$titre .= "sur les " . $annee . " dernières années.";
}





// TRAITEMENT

$d_soc_src = array();

if (!$reseau) {
	$d_soc_src[$acc_societe] = "";
	if (!empty($agence)) {
		$d_soc_src[$acc_societe] = $agence;
	}
} else {

	$sql = "SELECT uso_societe,uso_agence FROM Utilisateurs_Societes WHERE uso_utilisateur=" . $acc_utilisateur . " ORDER BY uso_societe,uso_agence DESC;";
	$req = $Conn->query($sql);
	$d_societes = $req->fetchAll();
	if (!empty($d_societes)) {
		foreach ($d_societes as $s) {
			if (!isset($d_soc_src[$s["uso_societe"]])) {
				if (!empty($s["uso_agence"])) {
					$d_soc_src[$s["uso_societe"]] = $s["uso_agence"];
				} else {
					$d_soc_src[$s["uso_societe"]] = "";
				}
			} else {
				if (!empty($s["uso_agence"])) {
					$d_soc_src[$s["uso_societe"]] .= "," . $s["uso_agence"];
				} else {
					$d_soc_src[$s["uso_societe"]] = "";
				}
			}
		}
	}
}

// ON LISTE LE CA

$d_clients = array(
	"0" => array(
		"0" => 0,
		"1" => 0,
		"2" => 0,
		"3" => 0,
		"4" => 0,
		"5" => 0,
		"6" => 0,
		"7" => 0,
		"8" => 0,
		"9" => 0,
		"total" => 0
	)
);


foreach ($d_soc_src as $s_id => $s_age_list) {

	$ConnFct = connexion_fct($s_id);

	$sql = "SELECT SUM(fli_montant_ht) AS ht,YEAR(fac_date) AS fac_annee,MONTH(fac_date) AS fac_mois,fac_client
		,cli_commercial,cli_code,cli_nom,cli_id,cli_categorie,cli_sous_categorie,cli_cp
		,com_label_1,com_label_2
		FROM Factures_Lignes INNER JOIN Factures ON (Factures_Lignes.fli_facture=Factures.fac_id)
		INNER JOIN Clients ON (Factures.fac_client=Clients.cli_id AND Factures.fac_agence=Clients.cli_agence)
		LEFT JOIN Commerciaux ON (Clients.cli_commercial=Commerciaux.com_id)
		WHERE fac_date>'" . $periode_deb . "' AND fac_date<='" . $periode_fin . "'";

	// critère auto
	if (!$_SESSION["acces"]["acc_droits"][6] && ($_SESSION['acces']['acc_profil'] == 3 and !$_SESSION["acces"]["acc_droits"][35])) {
		// commercial non annimateur des ventes
		$sql .= " AND com_type=1 AND com_ref_1=" . $acc_utilisateur;
	}


	if (!empty($s_age_list)) {
		$sql .= " AND fac_agence IN (" . $s_age_list . ")";
	}
	if (!empty($pro_type)) {
		$sql .= " AND fli_intra_inter=" . $pro_type;
	}
	if (!empty($pro_categorie)) {
		$sql .= " AND fli_categorie=" . $pro_categorie;
	}
	if (!empty($pro_famille)) {
		$sql .= " AND fli_famille=" . $pro_famille;
	}
	if (!empty($pro_sous_famille)) {
		$sql .= " AND fli_sous_famille=" . $pro_sous_famille;
	}
	if (!empty($pro_sous_sous_famille)) {
		$sql .= " AND fli_sous_sous_famille=" . $pro_sous_sous_famille;
	}
	if (!empty($commercial)) {
		$sql .= " AND cli_commercial=" . $commercial;
	}
	$sql .= " GROUP BY fac_client,fac_annee,fac_mois,fac_client
		,cli_commercial,cli_code,cli_nom,cli_id,cli_categorie,cli_sous_categorie,cli_cp
		,com_label_1,com_label_2;";
	$req = $ConnFct->query($sql);
	$d_results_fac = $req->fetchAll();
	if (!empty($d_results_fac)) {
		foreach ($d_results_fac as $fac) {

			$cle = $fac["fac_client"] . "-" . $fac["cli_commercial"];

			if (!isset($d_clients[$cle])) {

				$d_clients[$cle] = array(
					"cli_code" => $fac["cli_code"],
					"cli_id" => $fac["cli_id"],
					"cli_nom" => $fac["cli_nom"],
					"cli_categorie" => $fac["cli_categorie"],
					"cli_sous_categorie" => $fac["cli_sous_categorie"],
					"cli_commercial" => $fac["com_label_1"] . " " . $fac["com_label_2"],
					"cli_cp" => $fac["cli_cp"],
					"0" => 0,
					"1" => 0,
					"2" => 0,
					"3" => 0,
					"4" => 0,
					"5" => 0,
					"6" => 0,
					"7" => 0,
					"8" => 0,
					"9" => 0,
					"total" => 0
				);
			}

			if ($periode == 1) {


				// année comptable

				if ($fac["fac_mois"] < 4) {
					$cle_ca = $exercice - ($fac["fac_annee"] - 1);
				} else {
					$cle_ca = $exercice - $fac["fac_annee"];
				}
			} else {

				$cle_ca = $exercice - $fac["fac_annee"];
			}

			$d_clients[$cle][$cle_ca] = $d_clients[$cle][$cle_ca] + $fac["ht"];
			$d_clients[$cle]["total"] = $d_clients[$cle]["total"] + $fac["ht"];

			$d_clients[0][$cle_ca] = $d_clients[0][$cle_ca] + $fac["ht"];
			$d_clients[0]["total"] = $d_clients[0]["total"] + $fac["ht"];
		}
	}
}

asort($d_clients);

?>
<!DOCTYPE html>
<html>

<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

	<link rel="stylesheet" type="text/css" href="/vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">


	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
</head>

<body class="sb-top sb-top-sm">

	<div id="main">

		<?php include "includes/header_def.inc.php"; ?>

		<section id="content_wrapper">
			<section id="content" class="animated fadeIn">
				<div class="row ">

					<h1 class="text-center"><?= $titre ?></h1>

					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tab">
							<thead>
								<tr class="dark">
									<th>Code</th>
									<th>Nom</th>
									<th>Catégorie</th>
									<th>Sous-Catégorie</th>
									<th>Commercial</th>
									<th>Département</th>
									<?php for ($bcl = $annee - 1; $bcl >= 0; $bcl--) {
										echo ("<th>");
										if ($periode == 1) {
											echo ($exercice - $bcl . "/" . intval(($exercice - $bcl) + 1));
										} else {
											echo ($exercice - $bcl);
										}
										echo ("</th>");
									} ?>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
								<?php if (!empty($d_clients)) {
									foreach ($d_clients as $k => $c) {
										if (!empty($c["total"]) and $k != "0") { ?>
											<tr>
												<td>
													<a href="client_voir.php?client=<?= $c["cli_id"] ?>">
														<?= $c["cli_code"] ?>
													</a>
												</td>
												<td><?= $c["cli_nom"] ?></td>
												<td><?= $d_categories[$c["cli_categorie"]] ?></td>
												<td><?= $d_sous_categories[$c["cli_sous_categorie"]] ?></td>
												<td><?= $c["cli_commercial"] ?></td>
												<td>
											<?php	if(!empty($c["cli_cp"])) { 
														echo(substr($c["cli_cp"],0,2));
													} ?>
												</td>
												<?php for ($bcl = $annee - 1; $bcl >= 0; $bcl--) {
													echo ("<td class='text-right' >" . number_format($c[$bcl], 2, ",", " ") . "</td>");
												} ?>
												<td class='text-right'><?= number_format($c["total"], 2, ",", " ") ?></td>
											</tr>
									<?php				}
									} ?>
									<tr>
										<th colspan="5" class="text-right">Total :</th>
										<?php for ($bcl = $annee - 1; $bcl >= 0; $bcl--) {
											echo ("<td class='text-right' >" . number_format($d_clients[0][$bcl], 2, ",", " ") . "</td>");
										} ?>
										<td class='text-right'><?= number_format($d_clients[0]["total"], 2, ",", " ") ?></td>
									</tr>
								<?php		} ?>
							</tbody>
						</table>
					</div>

				</div>
			</section>
		</section>
	</div>
	
	<footer id="content-footer" class="affix">
		<div class="row">
			<div class="col-xs-3 footer-left">
				<a href="stat_commercial.php" class="btn btn-sm btn-default">
					<i class="fa fa-arrow-left"></i> Retour
				</a>
			</div>
			<div class="col-xs-6 footer-middle" style=""></div>
			<div class="col-xs-3 footer-right"></div>
		</div>
	</footer>

	<?php include "includes/footer_script.inc.php"; ?>
	<!-- SCRIPT SPE -->
	<script type="text/javascript">
		jQuery(document).ready(function() {


		});
	</script>
</body>

</html>