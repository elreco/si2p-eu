<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';
include_once 'includes/connexion_fct.php';
include_once 'modeles/mod_parametre.php';

$client = 0;
if (isset($_GET['client'])){
   $client = $_GET['client'];
}
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$origine="";
if(!empty($_GET['origine'])){
	$origine=$_GET['origine'];
}
$societe="";
if(!empty($_GET['societe'])){
	$societe=$_GET['societe'];
}
$ConnFct = connexion_fct($societe);
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}
$facture=0;
if(!empty($_GET['facture'])){
    $facture = $_GET['facture'];
}

// RECHERCHER LA FACTURE
$sql="SELECT * FROM Factures WHERE fac_id =" . $facture;
$req = $ConnFct->query($sql);
$d_facture=$req->fetch();

$reste_du = $d_facture['fac_total_ttc']-$d_facture['fac_regle'];

$req = $ConnFct->prepare("SELECT * FROM Factures_Commentaires
 WHERE fco_facture = " . $facture . " ORDER BY fco_date DESC;");
$req->execute();
$factures_commentaire = $req->fetch();
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Relances</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
<form action="facture_relance_fac_mod_enr.php" method="POST" id="admin-form">
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">

            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-left">

                                        <div class="content-header">
                                                <h2>Informations de relance :  <b class="text-primary"><?= $d_facture['fac_numero'] ?></b></h2>
                                                 </div>
                                        <input type="hidden" name="facture" value="<?=$facture?>" />
                                        <input type="hidden" name="societe" value="<?=$societe?>" />
                                        <?php if(!empty($factures_commentaire)){ ?>
                                            <input type="hidden" name="fco_id" value="<?=$factures_commentaire['fco_id']?>" />
                                        <?php } ?>
                                        <div class="col-md-12">

                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="table-responsive mt15">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                   <th style="vertical-align: middle;border:0;">Date :</th>
                                                                    <td><?=convert_date_txt($d_facture['fac_date'])?></td>
                                                                    <th style="vertical-align: middle;border:0;">Date d'échéance :</th>
                                                                    <td><?=convert_date_txt($d_facture['fac_date_reg_prev'])?></td>
                                                                    <th style="vertical-align: middle;border:0;">Date de suspension :</th>
                                                                    <td>
                                                                        <input id="fac_date_reg_rel" type="text" name="fac_date_reg_rel" class="form-control date datepicker" placeholder="Date" value="<?=convert_date_txt($d_facture['fac_date_reg_rel'])?>">
                                                                    </td>
                                                                    <th style="vertical-align: middle;border:0;">Montant :</th>
                                                                    <td class="droite" ><?= number_format($d_facture['fac_total_ttc'], 2, ',', ' ') ?> €</td>
                                                                    <th style="vertical-align: middle;border:0;">Réglé :</th>
                                                                    <td class="droite" ><?= number_format($d_facture['fac_regle'], 2, ',', ' ') ?> €</td>
                                                                    <th style="vertical-align: middle;border:0;">Reste dû :</th>
                                                                    <td class="droite" ><?= number_format($reste_du, 2, ',', ' ') ?> €</td>

                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row mt15">
                                                <?php if($reste_du>0){ ?>
                                                <div class="col-md-3">
                                                    <div class="section">
                                                        <label class="option">
                                                            <input type="radio" class="type" name="relance_statut" id="relance_actif" value="0" onClick="stopProcess();" value="0"
                                                                <?php if(empty($d_facture['fac_relance_stop']) AND empty($d_facture['fac_relance_perdu'])){?>
                                                                    checked
                                                                <?php }?>
                                                            />
                                                            <span class="radio"></span>Processus actif
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="section">
                                                        <label class="option">
                                                            <input type="radio" class="type" name="relance_statut" id="relance_stop" onClick="stopProcess();" value="1"
                                                            <?php if(!empty($d_facture['fac_relance_stop']) && $d_facture['fac_relance_stop'] == 1){?>
                                                                    checked
                                                                <?php }?>
                                                            />
                                                            <span class="radio"></span>Processus suspendu par l'agence
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="section">
                                                        <label class="option">
                                                            <input type="radio" class="type" name="relance_statut" id="relance_stop_2" onClick="stopProcess();" value="2"
                                                            <?php if(!empty($d_facture['fac_relance_stop']) && $d_facture['fac_relance_stop'] == 2){?>
                                                                    checked
                                                                <?php }?>
                                                            />
                                                            <span class="radio"></span>Processus suspendu par la compta
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="section">
                                                        <label class="option">
                                                            <input type="radio" class="type" name="relance_statut" id="relance_stop_3" onClick="stopProcess();" value="3"
                                                            <?php if(!empty($d_facture['fac_relance_stop']) && $d_facture['fac_relance_stop'] == 3){?>
                                                                    checked
                                                                <?php }?>
                                                            />
                                                            <span class="radio"></span>Client douteux
                                                        </label>
                                                    </div>
                                                </div>

                                                    <div class="col-md-12 mt15">
                                                        <label>Motif de la suspension</label>
                                                        <textarea class="form-control" name="motif_stop" id="motif_stop" cols="60" rows="4"><?php if(!empty($factures_commentaire)){ ?><?=$factures_commentaire['fco_comment']?><?php }?></textarea>
                                                    </div>
                                                <?php }else{ ?>
                                                    <div class="alert alert-success">
                                                        Cette facture est réglée. Processus terminé.
                                                    </div>
                                                <?php }?>

                                            </div>

                                        </div>

                                      </div>

                                  </div>
                              </div>


                          </div>
                      </div>

                  </div>
              </div>

          </div>
      </section>
      <!-- End: Content -->
  </section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="<?= $_SESSION['retourFacture'] ?>" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-save'></i> Enregistrer
            </button>
        </div>
    </div>
</footer>
</form>
<?php
	include "includes/footer_script.inc.php"; ?>

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="vendor/plugins/moment/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/fr.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        function stopProcess(){
                if(document.getElementById("motif_stop")){
                    if(document.getElementById("relance_actif").checked){
                        document.getElementById("motif_stop").value="";
                        document.getElementById("motif_stop").setAttribute("disabled","disabled");
                    }else{
                        <?php if(!empty($factures_commentaire['fco_comment'])){ ?>
                        document.getElementById("motif_stop").value="<?=$factures_commentaire['fco_comment']?>";
                        <?php } ?>
                        document.getElementById("motif_stop").removeAttribute("disabled");
                    };
                };
            }
    </script>
</body>
</html>
