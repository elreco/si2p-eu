<?php

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";
include('includes/connexion_soc.php');
include('includes/connexion.php');

include('modeles/mod_parametre.php');

// SAISIE D'UN REGLEMENT
$erreur_txt="";
$facture_id=0;
if(!empty($_GET["facture"])){
	$facture_id=intval($_GET["facture"]);
}

if(empty($facture_id)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();

}elseif(!$_SESSION["acces"]["acc_droits"][30]){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();

}else{

	// personne connecte
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}

	// SUR LA FACTURE
	$sql="SELECT fac_total_ttc,fac_cli_nom,fac_cli_code,fac_numero,fac_regle,fac_rib,fac_reg_type FROM Factures WHERE fac_id=" . $facture_id . ";";
	$req=$ConnSoc->query($sql);
	$d_facture=$req->fetch();


	// LES REGLEMENTS DEJA ENREGISTRES
	$sql="SELECT reg_id,reg_facture,DATE_FORMAT(reg_date,'%d/%m/%Y') AS reg_date_releve,reg_montant,reg_type,reg_reference,reg_banque,DATE_FORMAT(reg_date_enr,'%d/%m/%Y') AS reg_date_saisie,reg_uti_enr
	,DATE_FORMAT(reg_rb_date,'%d/%m/%Y') AS reg_rb_date_fr
	FROM Reglements WHERE reg_facture=" . $facture_id . " ORDER BY reg_date;";
	$req=$ConnSoc->query($sql);
	$d_reglements=$req->fetchAll();
	
	// LE RIB DE LA FACTURE
	$sql="SELECT rib_banque FROM Rib WHERE rib_id=" . $d_facture["fac_rib"] . ";";
	$req=$Conn->query($sql);
	$d_rib=$req->fetch();

	// LES BANQUES

	$sql="SELECT * FROM Banques ORDER BY ban_libelle;";
	$req=$Conn->query($sql);
	$d_banque=$req->fetchAll();
	
	// LES MOYENS DE PAIEMENTS
	
	$sql="SELECT * FROM Reglements_Types ORDER BY rty_libelle;";
	$req=$Conn->query($sql);
	$d_reg_type=$req->fetchAll();
	
	
	// LES UTILISATEURS

	$d_utilisateurs=array();
	
	$sql="SELECT uti_id,uti_nom,uti_prenom FROM Utilisateurs ORDER BY uti_id;";
	$req=$Conn->query($sql);
	$resultats=$req->fetchAll();
	if(!empty($resultats)){
		foreach($resultats as $r){
			$d_utilisateurs[$r["uti_id"]]=$r["uti_prenom"] . " " . $r["uti_nom"];
		}
	}

}
	?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
		<?php if(!empty($_GET['societ'])){ ?>
			<form method="post" action="reglement_enr.php?societ=<?= $_GET['societ'] ?>" >
		<?php }else{?>
			<form method="post" action="reglement_enr.php" >
		<?php }?>

			<div>
				<input type="hidden" name="facture" value="<?=$facture_id?>" />
			</div>
			<div id="main">
		<?php	include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated fadeIn">
			<?php 		if(empty($erreur_txt)){ ?>

							<div class="admin-form theme-primary ">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">

										<div class="content-header">
											<h2>Règlement pour la facture <b class="text-primary"><?=$d_facture["fac_numero"]?></b></h2>
										</div>

										<div class="row" >
											<div class="col-md-4" >
												<strong>Total T.T.C. :</strong> <?=number_format($d_facture["fac_total_ttc"],2,","," ")?> €
											</div>
											<div class="col-md-4" >
												<strong>Code client :</strong> <?=$d_facture["fac_cli_code"]?>
											</div>
											<div class="col-md-4" >
												<strong>Nom :</strong> <?=$d_facture["fac_cli_nom"]?>
											</div>
										</div>

										<div class="table-responsive">
											<table class="table mt15" id="reglement" >
												<thead>
													<tr class="dark2" >
														<th>Date du relevé</th>													
														<th>Montant</th>
														<th>Type</th>
														<th>Commentaires</th>
														<th>Banque</th>
														<th>Date de saisie</th>
														<th>Saisie par</th>
														<th>Remboursement</th>
													</tr>
												</thead>
												<tbody>
										<?php		foreach($d_reglements as $r => $reglement){ ?>
														<tr>
															<td>
																<span  class="field prepend-icon">
																	<input type="text" id="reg_date_<?=$r?>" name="reg_date[]" class="gui-input gui-date reg-date" size="10" placeholder="" value="<?=$reglement["reg_date_releve"]?>" />
																	<span class="field-icon">
																		<i class="fa fa-calendar-o"></i>
																	</span>
																</span>
																<small class="text-danger texte_required" id="reg_date_<?=$r?>_alerte"></small>
																<input type="hidden" name="reg_id[]" value="<?=$reglement["reg_id"]?>" />
															</td>
															<td>
																<span class="field prepend-icon">
																	<input type="text" name="reg_montant[]" class="gui-input input-euro reglement" placeholder="Montant" size="7" value="<?=$reglement["reg_montant"]?>" />
																	<span class="field-icon">
																		<i class="fa fa-eur"></i>
																	</span>
																</span>
															</td>
															<td>
																<span class="field select">
																	<select name="reg_type[]" class="reg-type" >																	
															<?php		foreach($d_reg_type as $reg_type){
																			if($reg_type["rty_id"]==$reglement["reg_type"]){
																				echo("<option value='" . $reg_type["rty_id"] . "' selected >" . $reg_type["rty_libelle"] .  "</option>");
																			}elseif(!$reg_type["rty_archive"]){
																				echo("<option value='" . $reg_type["rty_id"] . "' >" . $reg_type["rty_libelle"] .  "</option>");
																			}
																		} ?>
																	</select>
																	<i class="arrow simple"></i>
																</span>
															</td>
															<td>
																<input type="text" name="reg_reference[]" class="gui-input reg-reference" placeholder="Commentaires" value="<?=$reglement["reg_reference"]?>" />
															</td>
															<td>
																<span class="field select">
																	<select name="reg_banque[]" >
															<?php		foreach($d_banque as $banque){
																			if($banque["ban_id"]==$reglement["reg_banque"]){
																				echo("<option value='" . $banque["ban_id"] . "' selected >" . $banque["ban_code"] .  "</option>");
																			}elseif(!$banque["ban_archive"]){
																				echo("<option value='" . $banque["ban_id"] . "' >" . $banque["ban_code"] .  "</option>");
																			}
																		} ?>
																	</select>
																	<i class="arrow simple"></i>
																</span>
															</td>
															<td>
																<span  class="field prepend-icon">
																	<input type="text" id="reg_date_enr_<?=$r?>" name="reg_date_enr[]" class="gui-input gui-date reg-date-enr" size="10" placeholder="" value="<?=$reglement["reg_date_saisie"]?>" />
																	<span class="field-icon">
																		<i class="fa fa-calendar-o"></i>
																	</span>
																</span>	
																<small class="text-danger texte_required" id="reg_date_enr_<?=$r?>_alerte"></small>

															</td>
															<td>
														<?php	if(!empty($d_utilisateurs[$reglement["reg_uti_enr"]])){
																	echo($d_utilisateurs[$reglement["reg_uti_enr"]]);
																}else{
																	echo("&nbsp;");
																} ?>
															</td>
															<td>
																<span  class="field prepend-icon">
																	<input type="text" id="reg_rb_date_<?=$r?>" name="reg_rb_date[]" class="gui-input gui-date" size="10" placeholder="" value="<?=$reglement["reg_rb_date_fr"]?>" />
																	<span class="field-icon">
																		<i class="fa fa-calendar-o"></i>
																	</span>
																</span>	
																<small class="text-danger texte_required" id="reg_rb_date_<?=$r?>_alerte"></small>
															</td>
															
														</tr>
										<?php		} ?>
													<tr>
														<td>
															<span  class="field prepend-icon">
																<input type="text" id="reg_date" name="reg_date[]" size="10" class="gui-input gui-date reg-date" placeholder="" value="" />
																<span class="field-icon">
																	<i class="fa fa-calendar-o"></i>
																</span>
															</span>
															<small class="text-danger texte_required" id="reg_date_alerte"></small>
															<input type="hidden" name="reg_id[]" value="0" />
														</td>
														<td>
															<span  class="field append-icon">
																<input type="text" name="reg_montant[]" size="7" class="gui-input input-euro reglement" placeholder="Montant" value="<?=$d_facture["fac_total_ttc"]-$d_facture["fac_regle"]?>" />
																<span class="field-icon">
																	<i class="fa fa-eur"></i>
																</span>
															</span>
														</td>
														<td>
															<span class="field select">
																<select name="reg_type[]" class="reg-type" >
														<?php		foreach($d_reg_type as $r){
																		if(!$r["rty_archive"]){
																			if($r["rty_id"]==$d_facture["fac_reg_type"]){
																				echo("<option value='" . $r["rty_id"] . "' selected >" . $r["rty_libelle"] .  "</option>");
																			}else{
																				echo("<option value='" . $r["rty_id"] . "' >" . $r["rty_libelle"] .  "</option>");
																			}
																		}
																	} ?>
																</select>
																<i class="arrow simple"></i>
															</span>
														</td>
														<td>
															<input type="text" name="reg_reference[]" class="gui-input reg-reference" placeholder="Commentaires" value="" />
														</td>
														<td>
															<span class="field select">
																<select name="reg_banque[]" class="" >
														<?php		foreach($d_banque as $banque){
																		if(!$banque["ban_archive"]){
																			if($d_rib["rib_banque"]==$banque["ban_id"]){
																				echo("<option value='" . $banque["ban_id"] . "' selected >" . $banque["ban_code"] .  "</option>");
																			}else{
																				echo("<option value='" . $banque["ban_id"] . "' >" . $banque["ban_code"] .  "</option>");
																			}
																		}
																	} ?>
																</select>
																<i class="arrow simple"></i>
															</span>
														</td>
														<td>
															<span  class="field prepend-icon">
																<input type="text" id="reg_date_enr" name="reg_date_enr[]" size="10" class="gui-input gui-date reg-date-enr" placeholder="" value="<?=date("d/m/Y")?>" />
																<span class="field-icon">
																	<i class="fa fa-calendar-o"></i>
																</span>
															</span>
															<small class="text-danger texte_required" id="reg_date_enr_alerte"></small>
														</td>
														<td>&nbsp;</td>
														<td>
															<span  class="field prepend-icon">
																<input type="text" id="reg_rb_date" name="reg_rb_date[]" class="gui-input gui-date" size="10" placeholder="" value="" />
																<span class="field-icon">
																	<i class="fa fa-calendar-o"></i>
																</span>
															</span>	
															<small class="text-danger texte_required" id="reg_rb_date_alerte"></small>
														</td>
														
													</tr>
												</tbody>
												<tfoot>
													<tr>
														<th class="text-right" >Reste dû :</th>
														<td>
															<span  class="field append-icon">
																<input type="text" id="fac_reste_du" size="7" class="gui-input input-euro" placeholder="Reste dû" readonly value="0" />
																<span class="field-icon">
																	<i class="fa fa-eur"></i>
																</span>
															</span>
														</td>
														<td colspan="5" >&nbsp;</td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>

			<?php		}else{ ?>

							<p class="alert alert-danger text-center" >
								Impossible d'afficher cette page!
							</p>

			<?php		} ?>

					</section>

					<!-- End: Content -->
				</section>


			</div>
			<!-- End: Main -->

			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
			<?php		if(isset($_SESSION["retourFacture"])){ ?>
							<a href="<?=$_SESSION["retourFacture"]?>" class="btn btn-default btn-sm" >
								Retour
							</a>
			<?php		} ?>
					</div>
					<div class="col-xs-6 footer-middle text-center" >
						<button type="button" class="btn btn-sm btn-success" id="btn_add" >
							<i class="fa fa-plus" ></i> Ajouter une ligne
						</button>
					</div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-sm btn-success" id="btn_sub" >
							<i class="fa fa-save" ></i> Enregistrer
						</button>

					</div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>

		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			var total=parseFloat("<?=$d_facture["fac_total_ttc"]?>");

			jQuery(document).ready(function(){

				$(document).on("blur",".reglement",function(){
					regle=0;
					reste_du=0;

					nb_reg=$(".reglement").length;
					for(k=0;k<nb_reg;k++){
						val=0;
						if($(".reglement:eq(" + k + ")" ).val()!=""){
							val=parseFloat($(".reglement:eq(" + k + ")" ).val());
						}
						/*console.log(index);
						console.log(k);*/
						console.log(k + " : " + val);

						if(val!=0){
							$(".reg-date:eq(" + k + ")" ).prop("required",true);
							$(".reg-type:eq(" + k + ")" ).prop("required",true);
							$(".reg-reference:eq(" + k + ")" ).prop("required",true);
							$(".reg-date-enr:eq(" + k + ")" ).prop("required",true);
						}else{
							$(".reg-date:eq(" + k + ")" ).prop("required",false);
							$(".reg-type:eq(" + k + ")" ).prop("required",false);
							$(".reg-reference:eq(" + k + ")" ).prop("required",false);
							$(".reg-date-enr:eq(" + k + ")" ).prop("required",false);
						}

						regle=regle+val;


					};
					reste_du=(total-regle).toFixed(2);

					$("#fac_reste_du").val(reste_du);

				});

				$("#btn_add").click(function(){
					ecrire_ligne();
				});

			});
			
			var ligne_add=0;
			function ecrire_ligne(){
				ligne_add++;
				
				html="<tr>";
					html=html + "<td>";
						html=html + "<span  class='field prepend-icon' >";
							html=html + "<input type='text' id='date_add_" + ligne_add + "' name='reg_date[]' size='10' class='gui-input gui-date reg-date' placeholder='' value='' />";
							html=html + "<span class='field-icon' >";
								html=html + "<i class='fa fa-calendar-o' ></i>";
							html=html + "</span>";
						html=html + "</span>";
						html=html + "<small class='text-danger texte_required' id='date_add_" + ligne_add + "_alerte' ></small>";

						html=html + "<input type='hidden' name='reg_id[]' value='0' />";

					html=html + "</td>";
					html=html + "<td>";
						html=html + "<span  class='field append-icon' >";
							html=html + "<input type='text' name='reg_montant[]' size='7' class='gui-input input-euro reglement' placeholder='Montant' value='' />";
							html=html + "<span class='field-icon' >";
								html=html + "<i class='fa fa-eur' ></i>";
							html=html + "</span>";
						html=html + "</span>";
					html=html + "</td>";
					
					html=html + "<td>";					
						html=html + "<span class='field select' >";
							html=html + "<select name='reg_type[]' class='reg-type' >";
					<?php		foreach($d_reg_type as $r){
									if(!$r["rty_archive"]){
										if($r["rty_id"]==$d_facture["fac_reg_type"]){ ?>
											html=html + "<option value='<?=$r["rty_id"]?>' selected ><?=$r["rty_libelle"]?></option>";
					<?php				}else{ ?>
											html=html + "<option value='<?=$r["rty_id"]?>' ><?=$r["rty_libelle"]?></option>";
					<?php				}
									}
								} ?>
							html=html + "</select>";
							html=html + "<i class='arrow simple'></i>";
						html=html + "</span>";
					html=html + "</td>";
					html=html + "<td>";
						html=html + "<input type='text' name='reg_reference[]' class='gui-input reg-reference' placeholder='Référence' value='' />";
					html=html + "</td>";
					html=html + "<td>";
					
						html=html + "<span class='field select' >";
							html=html + "<select name='reg_banque[]' class='' >";
					<?php		foreach($d_banque as $banque){
									if(!$banque["ban_archive"]){
										if($d_rib["rib_banque"]==$banque["ban_id"]){ ?>
											html=html + "<option value='<?=$banque["ban_id"]?>' selected ><?=$banque["ban_code"]?></option>";
						<?php			}else{ ?>
											html=html + "<option value='<?=$banque["ban_id"]?>' ><?=$banque["ban_code"]?></option>";
						<?php			}
									}
								} ?>
							html=html + "</select>";
							html=html + "<i class='arrow simple'></i>";
						html=html + "</span>";				
					html=html + "</td>";
					
					html=html + "<td>";
						html=html + "<span  class='field prepend-icon' >";
							html=html + "<input type='text' id='date_add_enr_" + ligne_add + "' name='reg_date_enr[]' size='10' class='gui-input gui-date reg-date-enr' placeholder='' value='<?=date("d/m/Y")?>' />";
							html=html + "<span class='field-icon' >";
								html=html + "<i class='fa fa-calendar-o' ></i>";
							html=html + "</span>";
						html=html + "</span>";
						html=html + "<small class='text-danger texte_required' id='date_add_enr_" + ligne_add + "_alerte' ></small>";
					html=html + "</td>";
					
					html=html + "<td>&nbsp;</td>";
					
					html=html + "<td>";
						html=html + "<span  class='field prepend-icon'>";
							html=html + "<input type='text' id='rb_date_add_" + ligne_add + "' name='reg_rb_date[]' class='gui-input gui-date' size='10' placeholder='' value='' />";
							html=html + "<span class='field-icon' >";
								html=html + "<i class='fa fa-calendar-o' ></i>";
							html=html + "</span>";
						html=html + "</span>";
						html=html + "<small class='text-danger texte_required' id='rb_date_add_" + ligne_add + "_alerte' ></small>";
					html=html + "</td>";
														
				html=html + "</tr>";

				$('#reglement tbody tr:last').after(html);

				//$('#reglement tbody tr:last select').select2();

				// ON ajoute la gestion du champ date
				/*$("#reglement tbody tr:last .datepicker").datepicker({
					defaultDate: "+1w",
					dateFormat: "dd/mm/yy",
					closeText: 'Fermer',
					prevText: 'Précédent',
					nextText: 'Suivant',
					currentText: 'Aujourd\'hui',
					monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
					monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
					dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
					dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
					dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
					weekHeader: 'Sem.',
					numberOfMonths: 1,
					prevText: '<i class="fa fa-chevron-left"></i>',
					nextText: '<i class="fa fa-chevron-right"></i>',
					beforeShow: function (input, inst) {
						var themeClass = $(this).parents('.admin-form').attr('class');
						var smartpikr = inst.dpDiv.parent();
						if (!smartpikr.hasClass(themeClass)) {
							inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
						}
					},
					onClose: function (selectedDate) {
						$("#datepicker-to").datepicker("option", "minDate", selectedDate);
					}
				});*/
				
				$("#reglement tbody tr:last .gui-date").blur(function(){
					if($(this).val()!=""){
						var date_valide=testerdate($(this).val());
						if(!date_valide){
							$(this).val("");
							if($("#" + this.id + "_alerte").length>0){				
								$("#" + this.id + "_alerte").html("format de date incorrect");
								$("#" + this.id + "_alerte").show().delay(5000).fadeOut();
							}
						}
					}
				});

				$("#reglement tbody tr:last .input-float").focusout(function(){
					convertir_en_float($(this));
				});


			}
		</script>
	</body>
</html>
