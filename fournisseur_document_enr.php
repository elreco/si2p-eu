<?php

include "includes/controle_acces.inc.php";
include("includes/connexion.php");


	if(empty($_POST['fdp_libelle'])){
		$erreur_txt="Formulaire incomplet!";

	}else{

		$fdp_type=1;
		if(!empty($_POST['fdp_type'])){
			$fdp_type=intval($_POST['fdp_type']);
		}

		$fdp_entreprise=0;
		$fdp_indiv=0;
		$fdp_une_seule_fois=0;
		if($fdp_type==1){

			// document fournisseur
			if(!empty($_POST['fdp_entreprise'])){
				$fdp_entreprise=1;
			}
			if(!empty($_POST['fdp_indiv'])){
				$fdp_indiv=1;
			}
			if(!empty($_POST['fdp_une_seule_fois'])){
				$fdp_une_seule_fois=1;
			}

		}

		$fdp_obligatoire=0;
		if(!empty($_POST['fdp_obligatoire'])){
			$fdp_obligatoire=1;
		}

		$fdp_periodicite_mois=0;
		if(!empty($_POST['fdp_periodicite_mois'])){
			$fdp_periodicite_mois=intval($_POST['fdp_periodicite_mois']);
		}
	}

	if(!isset($erreur_txt)){

		if(isset($_POST['fdp_id'])){

				/*var_dump($fdp_entreprise);
				die();*/

				$req = $Conn->prepare("UPDATE fournisseurs_documents_Param SET fdp_libelle = :fdp_libelle, fdp_type=:fdp_type, fdp_entreprise=:fdp_entreprise, fdp_indiv=:fdp_indiv,
				fdp_obligatoire = :fdp_obligatoire, fdp_une_seule_fois = :fdp_une_seule_fois, fdp_periodicite_mois = :fdp_periodicite_mois WHERE fdp_id = :fdp_id");
				$req->bindParam(':fdp_id', $_POST['fdp_id']);
				$req->bindParam(':fdp_libelle', $_POST['fdp_libelle']);
				$req->bindParam(':fdp_type', $fdp_type);
				$req->bindParam(':fdp_entreprise', $fdp_entreprise);
				$req->bindParam(':fdp_indiv', $fdp_indiv);
				$req->bindParam(':fdp_obligatoire', $fdp_obligatoire);
				$req->bindParam(':fdp_une_seule_fois', $fdp_une_seule_fois);
				$req->bindParam(':fdp_periodicite_mois', $fdp_periodicite_mois);
				$req->execute();

		}else{


			$req = $Conn->prepare("INSERT INTO fournisseurs_documents_Param (fdp_libelle, fdp_type, fdp_obligatoire, fdp_entreprise, fdp_indiv, fdp_une_seule_fois, fdp_periodicite_mois)
			 VALUES (:fdp_libelle, :fdp_type, :fdp_obligatoire, :fdp_entreprise, :fdp_indiv, :fdp_une_seule_fois, :fdp_periodicite_mois)");
			$req->bindParam(':fdp_libelle', $_POST['fdp_libelle']);
			$req->bindParam(':fdp_type', $fdp_type);
			$req->bindParam(':fdp_entreprise', $fdp_entreprise);
			$req->bindParam(':fdp_indiv', $fdp_indiv);
			$req->bindParam(':fdp_obligatoire', $fdp_obligatoire);
			$req->bindParam(':fdp_une_seule_fois', $fdp_une_seule_fois);
			$req->bindParam(':fdp_periodicite_mois', $fdp_periodicite_mois);
			$req->execute();
		}


		$_SESSION['message'][] = array(
			"aff" => "",
			"titre" => "Enregistrement terminé",
			"type" => "success",
			"message" => "Le document a été enregistré."
		);

	}else{

		$_SESSION['message'][] = array(
			"aff" => "",
			"titre" => "Echec de l'enregistrement",
			"type" => "danger",
			"message" => $erreur_txt
		);
	
	}
	Header('Location: fournisseur_document_liste.php');
	die();
	?>