<?php
 // inluce connexion
include('includes/connexion.php');
session_start();

if(!empty($_GET['id'])){
	// attestation
	$req = $Conn->prepare("SELECT * FROM attestations WHERE att_id = :att_id ");
    $req->bindParam(':att_id', $_GET['id']);
    $req->execute();
    $attestation = $req->fetch();
    // competence
    $req = $Conn->prepare("SELECT * FROM attestations_competences WHERE aco_attestation = :aco_attestation");
    $req->bindParam(':aco_attestation', $_GET['id']);
    $req->execute();
    $competences = $req->fetchAll();

    $req = $Conn->prepare("INSERT INTO attestations (att_titre, att_famille, att_sous_famille, att_sous_sous_famille, att_contenu, att_statut, att_date_creation) 
	VALUES (:att_titre, :att_famille, :att_sous_famille, :att_sous_sous_famille, :att_contenu, 0, NOW())");
    $req->bindParam(':att_titre', $attestation['att_titre']);
    $req->bindParam(':att_contenu', $attestation['att_contenu']);
	 $req->bindParam(':att_famille', $attestation['att_famille']);
    $req->bindParam(':att_sous_famille', $attestation['att_sous_famille']);
	 $req->bindParam(':att_sous_sous_famille', $attestation['att_sous_sous_famille']);
    $req->execute();
    $attestation_id = $Conn->lastInsertId();

    if(!empty($competences)){
    	foreach($competences as $c){
    		$req = $Conn->prepare("INSERT INTO attestations_competences (aco_competence, aco_attestation, aco_pratique) VALUES (:aco_competence, :aco_attestation, :aco_pratique)");
		    $req->bindParam(':aco_competence', $c['aco_competence']);
		    $req->bindParam(':aco_attestation', $attestation_id);
		    $req->bindParam(':aco_pratique', $c['aco_pratique']);
		    $req->execute();
    	}
    }
    $_SESSION['message'][] = array(
        "titre" => "Attestation",
        "type" => "success",
        "message" => "Vous avez dupliqué votre attestation."
    );
    Header("Location: attestation_liste.php");

}

	
	


?>