<?php
include "includes/controle_acces.inc.php";
include("includes/connexion.php");
include("includes/connexion_fct.php");

$erreur_txt="";
$warning_txt="";

if(isset($_POST)){
	if(!empty($_POST)){
		
		$fournisseur_id=0;
		if(!empty($_POST['fournisseur'])){
			$fournisseur_id=intval( $_POST['fournisseur']);
		}
		
		$intervenant_id=0;
		if(!empty($_POST['intervenant'])){
			$intervenant_id=intval($_POST['intervenant']);
		}
		
		if(empty($fournisseur_id) OR empty($_POST['fin_nom']) OR empty($_POST['fin_prenom']) ){
			$erreur_txt="Formulaire incomplet!";
		}
		
	}else{
		$erreur_txt="Formulaire incomplet!";
	}
}else{
	$erreur_txt="Formulaire incomplet!";
}

if(empty($erreur_txt)){
	
	// CONTROLE
		
	$sql="SELECT fou_type,fou_code FROM Fournisseurs WHERE fou_id=" . $fournisseur_id . ";";
	$req = $Conn->query($sql);
	$d_fournisseur=$req->fetch();
	if(empty($d_fournisseur)){
		$erreur_txt="Impossible d'identifier le fournisseur!";
	}
	if(empty($erreur_txt) ){
		if($d_fournisseur["fou_type"]==3){
			$sql="SELECT fin_id FROM Fournisseurs_Intervenants WHERE fin_fournisseur=" . $fournisseur_id . " AND NOT fin_id=" . $intervenant_id . ";";
			$req = $Conn->query($sql);
			$d_fou_intervenant=$req->fetch();
			if(!empty($d_fou_intervenant)){
				$erreur_txt="Un auto-entrepreneur ne peut avoir qu'un seul intervenant!";
			}
		}
	}
	
	// ENREGISTREMENT
	
	if(empty($erreur_txt) ){
		
		if(empty($intervenant_id)){
			
			// création d'un intervenant
			$req = $Conn->prepare("INSERT INTO fournisseurs_intervenants (fin_nom, fin_prenom, fin_ad1, fin_ad2, fin_ad3, fin_cp, fin_ville, fin_tel, fin_portable, fin_fax, fin_mail, fin_fournisseur, fin_archive) VALUES (:fin_nom,:fin_prenom,:fin_ad1,:fin_ad2,:fin_ad3,:fin_cp,:fin_ville,:fin_tel,:fin_portable,:fin_fax,:fin_mail, :fin_fournisseur,0) ");
			$req->bindParam(':fin_prenom', $_POST['fin_prenom']);
			$req->bindParam(':fin_nom', $_POST['fin_nom']);
			$req->bindParam(':fin_ad1', $_POST['fin_ad1']);
			$req->bindParam(':fin_ad2', $_POST['fin_ad2']);
			$req->bindParam(':fin_ad3', $_POST['fin_ad3']);
			$req->bindParam(':fin_cp', $_POST['fin_cp']);
			$req->bindParam(':fin_ville', $_POST['fin_ville']);
			$req->bindParam(':fin_portable', $_POST['fin_portable']);
			$req->bindParam(':fin_fax', $_POST['fin_fax']);
			$req->bindParam(':fin_mail', $_POST['fin_mail']);
			$req->bindParam(':fin_tel', $_POST['fin_tel']);
			$req->bindParam(':fin_fournisseur', $fournisseur_id);
			try{
				$req->execute();
				$intervenant_id = $Conn->lastInsertId();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
			}

		}else{
			
			// update d'un intervenant
			if(!empty($_POST['fin_archive'])){
				$fin_archive = 1;
			}else{
				$fin_archive = 0;
			}
			
			$req = $Conn->prepare("UPDATE fournisseurs_intervenants SET fin_nom = :fin_nom, fin_prenom = :fin_prenom, fin_ad1 = :fin_ad1, fin_ad2 = :fin_ad2, fin_ad3 =:fin_ad3, fin_cp=:fin_cp, fin_ville=:fin_ville, fin_tel=:fin_tel, fin_portable=:fin_portable, fin_fax=:fin_fax, fin_mail=:fin_mail, fin_fournisseur = :fin_fournisseur, fin_archive = :fin_archive WHERE fin_id = :intervenant");
			$req->bindParam(':fin_prenom', $_POST['fin_prenom']);
			$req->bindParam(':fin_nom', $_POST['fin_nom']);
			$req->bindParam(':fin_ad1', $_POST['fin_ad1']);
			$req->bindParam(':fin_ad2', $_POST['fin_ad2']);
			$req->bindParam(':fin_ad3', $_POST['fin_ad3']);
			$req->bindParam(':fin_cp', $_POST['fin_cp']);
			$req->bindParam(':fin_ville', $_POST['fin_ville']);
			$req->bindParam(':fin_portable', $_POST['fin_portable']);
			$req->bindParam(':fin_fax', $_POST['fin_fax']);
			$req->bindParam(':fin_mail', $_POST['fin_mail']);
			$req->bindParam(':fin_tel', $_POST['fin_tel']);
			$req->bindParam(':fin_fournisseur', $_POST['fournisseur']);
			$req->bindParam(':intervenant', $_POST['intervenant']);
			$req->bindParam(':fin_archive', $fin_archive);
			try{
				$req->execute();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
			}
			
			// TRAITEMENT ANNEXE
			
			if(empty($erreur_txt)){
				
				// MAJ DES FICHES PLANNINGS
				
				/* $sql="SELECT DISTINCT fso_societe FROM Fournisseurs_Societes WHERE fso_fournisseur=" . $fournisseur_id . ";";	
				$req = $Conn->query($sql);
				$d_societe=$req->fetchAll();
				if(!empty($d_societe)){
					
					$int_label_1=$d_fournisseur["fou_code"] . "-" . $_POST['fin_nom'];
					$int_label_2=$_POST['fin_prenom'];
					
					foreach($d_societe as $soc){
						
						$ConnFct=connexion_fct($soc["fso_societe"]);
						
						$req = $ConnFct->prepare("UPDATE Intervenants SET int_label_1 = :int_label_1, int_label_2 = :int_label_2 
						WHERE int_type=3 AND int_ref_1=:int_ref_1 AND int_ref_2=:int_ref_2;");							
						$req->bindParam(':int_label_1', $int_label_1);
						$req->bindParam(':int_label_2', $int_label_2);
						$req->bindParam(':int_ref_1', $fournisseur_id);
						$req->bindParam(':int_ref_2', $intervenant_id);
						try{
							$req->execute();
						}Catch(Exception $e){
							$warning_txt=$e->getMessage();
						}					
					}
				} */

				// MAJ DU CONTACT
				
				if($d_fournisseur["fou_type"]==3 AND empty($warning_txt)){
					$sql="SELECT fco_id FROM Fournisseurs_Contacts WHERE fco_fournisseur=" . $fournisseur_id . ";";	
					$req = $Conn->query($sql);
					$d_contacts=$req->fetchAll();
					if(!empty($d_contacts)){
						if(count($d_contacts)>1){
							$warning_txt="L'auto entrepreneur dispose de plusieur contact!";
						}else{
							$req = $Conn->prepare("UPDATE Fournisseurs_Contacts SET 
							fco_nom = :fin_nom,
							fco_prenom = :fin_prenom,
							fco_tel = :fin_tel,
							fco_portable = :fin_portable,
							fco_fax = :fin_fax,
							fco_mail = :fin_mail
							WHERE fco_fournisseur=:fin_fournisseur AND fco_id=:contact_id;");							
							$req->bindParam(':fin_prenom', $_POST['fin_prenom']);
							$req->bindParam(':fin_nom', $_POST['fin_nom']);						
							$req->bindParam(':fin_portable', $_POST['fin_portable']);
							$req->bindParam(':fin_fax', $_POST['fin_fax']);
							$req->bindParam(':fin_mail', $_POST['fin_mail']);
							$req->bindParam(':fin_tel', $_POST['fin_tel']);
							$req->bindParam(':fin_fournisseur', $fournisseur_id);
							$req->bindParam(':contact_id', $d_contacts[0]["fco_id"]);
							try{
								$req->execute();
							}Catch(Exception $e){
								$warning_txt="UPDATE CONTACT :" . $e->getMessage();
							}			
						}
						
					}else{
						$req = $Conn->prepare("INSERT INTO Fournisseurs_Contacts (fco_nom, fco_prenom, fco_tel, fco_portable, fco_fax, fco_mail, fco_fournisseur, fco_defaut) 
						VALUES (:fin_nom,:fin_prenom,:fin_tel,:fin_portable,:fin_fax,:fin_mail,:fin_fournisseur,1) ");
						$req->bindParam(':fin_prenom', $_POST['fin_prenom']);
						$req->bindParam(':fin_nom', $_POST['fin_nom']);						
						$req->bindParam(':fin_portable', $_POST['fin_portable']);
						$req->bindParam(':fin_fax', $_POST['fin_fax']);
						$req->bindParam(':fin_mail', $_POST['fin_mail']);
						$req->bindParam(':fin_tel', $_POST['fin_tel']);
						$req->bindParam(':fin_fournisseur', $fournisseur_id);
						try{
							$req->execute();						
						}Catch(Exception $e){
							$warning_txt=$e->getMessage();
						}
					}
				}
			}
		}
	}
}
	if(!empty($erreur_txt)){
			$_SESSION['message'][] = array(
				"titre" => "Enregistrement terminé",
				"type" => "danger",
				"message" => $erreur_txt
			);
			if(!empty($_SESSION["retour"])){
				Header('Location: ' . $_SESSION["retour"]);
			}

	}else{
		if(!empty($warning_txt)){
			$_SESSION['message'][] = array(
				"titre" => "Enregistrement terminé",
				"type" => "warning",
				"message" => $warning_txt
			);
		}else{
			$_SESSION['message'][] = array(
				"titre" => "Enregistrement terminé",
				"type" => "success",
				"message" => "L'intervenant a été enregistré"
			);
		}
		Header('Location: fournisseur_voir.php?fournisseur=' . $fournisseur_id . '&tab=5&intervenant=' . $intervenant_id);
	}
	die();
	