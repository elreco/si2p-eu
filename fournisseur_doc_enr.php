<?php 
include "includes/controle_acces.inc.php";
include "includes/connexion.php";
include "modeles/mod_upload.php";
include "modeles/mod_parametre.php";

function checkIsAValidDate($myDateString){
    return (bool)strtotime($myDateString);
}

if(isset($_POST)){

	$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_id=" . $_POST['document']);
	$req->execute();
	$document = $req->fetch();
	// societe du document, si modele = une seule fois alors...
		if($document['fdo_une_seule_fois'] == 1){
			$societe = 0; // est chargé une seule fois
		}else{
			$societe = $_SESSION['acces']['acc_societe']; // est chargé sur toutes les sociétés
		}


	// si le document est remplacé

	if(!empty($_FILES['doc'])){
		$nom = pathinfo($_FILES['doc']['name']); // nom du document (infos)

		
		// construction du nom du  document
		$nom_fichier = $societe . "_" . $document['fdo_id'] . "_" . clean($document['fdo_libelle']);

		// upload $erreur != 0 si erreur 
		// vérifier si le dossier du fournisseur exist , sinon le créer
		if (!file_exists('documents/fournisseurs/' . $_POST['fournisseur'])) {
		    mkdir('documents/fournisseurs/' . $_POST['fournisseur'], 0777, true);
		}
		$erreur = upload('doc', 'fournisseurs/' . $_POST['fournisseur'] . "/",$nom_fichier, 0, 0, 1);
	}else{
	// sinon erreur = 0
		$erreur = 0;
	}


	$date = convert_date_sql($_POST['doc_date']);

		
		// si il n'y a pas d'erreur
		if($erreur == 0){

			// mise a jour fournisseur_fichier
			$fdo_libelle = clean($document['fdo_libelle']);
			if(!isset($_POST['remplacer'])){ // si ce n'est pas une édition
				
					$req = $Conn->prepare("INSERT INTO fournisseurs_fichiers (ffi_fournisseur, ffi_fichier, ffi_nom, ffi_ext, ffi_date,ffi_societe) VALUES (:ffi_fournisseur, :ffi_fichier, :ffi_nom, :ffi_ext, :ffi_date, :ffi_societe)");
					$req->bindParam(':ffi_fournisseur', $_POST['fournisseur']);
					$req->bindParam(':ffi_fichier', $_POST['document']);
					$req->bindParam(':ffi_nom', $fdo_libelle);
					$req->bindParam(':ffi_ext', $nom['extension']);
					$req->bindParam(':ffi_societe', $societe);
					$req->bindParam(':ffi_date', $date);
					$req->execute();

				

				
				

			}else{
				if(!empty($_FILES['doc'])){ // si le fichier est remplacé, mettre à jour l'extension
					$req = $Conn->prepare("UPDATE fournisseurs_fichiers SET ffi_date = :ffi_date, ffi_societe = :ffi_societe WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_fichier = :ffi_fichier");
					$req->bindParam(':ffi_fournisseur', $_POST['fournisseur']);
					$req->bindParam(':ffi_fichier', $_POST['document']);

					$req->bindParam(':ffi_societe', $societe);
					$req->bindParam(':ffi_date', $date);
					$req->execute();
				}else{
					$req = $Conn->prepare("UPDATE fournisseurs_fichiers SET ffi_date = :ffi_date,ffi_ext = :ffi_ext, ffi_societe = :ffi_societe WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_fichier = :ffi_fichier");
					$req->bindParam(':ffi_fournisseur', $_POST['fournisseur']);
					$req->bindParam(':ffi_fichier', $_POST['document']);
					$req->bindParam(':ffi_ext', $nom['extension']);
					$req->bindParam(':ffi_societe', $societe);
					$req->bindParam(':ffi_date', $date);
					$req->execute();
				}
				
			}
			///////////////////// MISE A JOUR ETAT DOCUMENTS FOURNISSEUR ///////////////////
			// mise a jour de l'état
			$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id=" . $_POST['fournisseur']);
			$req->execute();
			$fournisseur = $req->fetch();

			$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_type = 1"); // documents du fournisseur
			$req->execute();
			$documents = $req->fetchAll();
			
			$txt = "";
			// verif etat du document 1 par 1
			foreach($documents as $d){
				$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fournisseur=" . $_POST['fournisseur'] . " AND ffi_fichier = " . $d['fdo_id']);
				$req->execute();
				$fichier = $req->fetch();

				if(($d['fdo_obligatoire'] == 1 && empty($fichier)) OR ($d['fdo_periodicite_mois'] != 0 && date('Y-m-d') > date('Y-m-d', strtotime("+" . $d['fdo_periodicite_mois'] . " months", strtotime($fichier['ffi_date']))))){
					$statut = 1; // danger
				

				}elseif(($d['fdo_optionnel'] == 1 && empty($fichier)) OR ($d['fdo_periodicite_mois'] != 0 && date('Y-m-d') > date('Y-m-d', strtotime("+" . $d['fdo_periodicite_mois'] . " months", strtotime($fichier['ffi_date']))))){
					$statut = 2; // warning

				}else{
					$statut = 3; // succes
				}
				if($statut == 1 OR $statut == 2){
					if(empty($txt)){
						$txt = $d['fdo_libelle'] . "<br>";
					}else{
						$txt .= $d['fdo_libelle'] . "<br>";
					}
				}else{
					$txt = "";
				}
				

			}

			if(!empty($statut)){
				 $req = $Conn->prepare("SELECT * FROM fournisseurs_validations WHERE fva_fournisseur = :fva_fournisseur AND fva_societe = :fva_societe");
                $req->bindParam(':fva_fournisseur', $_POST['fournisseur']);
                $req->bindParam(':fva_societe', $_SESSION['acces']['acc_societe']);
                $req->execute();
                $fou_valide = $req->fetch();

                if(empty($fou_valide)){
                     $req = $Conn->prepare("INSERT INTO fournisseurs_validations (fva_admin_etat, fva_admin_txt, fva_fournisseur, fva_societe) VALUES (:fva_admin_etat, :fva_admin_txt, :fva_fournisseur, :fva_societe)");
                    $req->bindParam(':fva_fournisseur', $_POST['fournisseur']);
                    $req->bindParam(':fva_societe', $_SESSION['acces']['acc_societe']);
                    $req->bindParam(':fva_admin_etat', $statut);
                    $req->bindParam(':fva_admin_txt', $txt);
                    $req->execute();
                }else{
                     $req = $Conn->prepare("UPDATE fournisseurs_validations SET fva_admin_etat = :fva_admin_etat, fva_admin_txt = :fva_admin_txt  WHERE fva_fournisseur = :fva_fournisseur AND fva_societe = :fva_societe");
                    $req->bindParam(':fva_fournisseur', $_POST['fournisseur']);
                    $req->bindParam(':fva_societe', $_SESSION['acces']['acc_societe']);
                    $req->bindParam(':fva_admin_etat', $statut);
                    $req->bindParam(':fva_admin_txt', $txt);
                    $req->execute();
                }
			}
			///////////////////// FIN MISE A JOUR ETAT DOCUMENTS FOURNISSEUR ///////////////////

		}else{
			echo $erreur;
		}



	


}
/*if(isset($_POST['submitedition'])){

	$req = $Conn->prepare("SELECT * FROM fournisseurs_documents WHERE fdo_id=" . $_GET['doc']);
	$req->execute();
	$document = $req->fetch();
	$nom = pathinfo($_FILES['doc']['name']);
	$erreur = upload('doc', 'fournisseurs/',$_GET['fournisseur'] . "_" . $_GET['doc'], 0, 0, 1);
	var_dump($erreur);
	
	$req = $Conn->prepare("UPDATE fournisseurs_fichiers SET ffi_ext = :ffi_ext, ffi_nom = :ffi_nom, ffi_date = NOW() WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_fichier = :ffi_fichier");
			$req->bindParam(':ffi_fournisseur', $_GET['fournisseur']);
			$req->bindParam(':ffi_fichier', $_GET['doc']);
			$req->bindParam(':ffi_nom', $document['fdo_libelle']);
			$req->bindParam(':ffi_ext', $nom['extension']);
			$req->execute();

	if($erreur == 0){
		Header('Location : fournisseur_voir.php?fournisseur=' . $_GET['fournisseur'] . '&tab=4&succes=13');
	}else{

	}
}
if(isset($_GET['suppr'])){

	$req = $Conn->prepare("SELECT * FROM fournisseurs_fichiers WHERE ffi_fichier =  " . $_GET['suppr'] . " AND ffi_fournisseur = " . $_GET['fournisseur']);
	$req->execute();
	$document = $req->fetch();

	unlink('documents/fournisseurs/' . $_GET['fournisseur'] . "_" . $_GET['suppr'] . "." . $document['ffi_ext']);
	$req = $Conn->prepare("DELETE FROM fournisseurs_fichiers WHERE ffi_fournisseur = :ffi_fournisseur AND ffi_fichier = :ffi_fichier");
			$req->bindParam(':ffi_fournisseur', $_GET['fournisseur']);
			$req->bindParam(':ffi_fichier', $_GET['suppr']);
			$req->execute();

		Header('Location : fournisseur_voir.php?fournisseur=' . $_GET['fournisseur'] . '&tab=4&succes=14');

}*/


?>