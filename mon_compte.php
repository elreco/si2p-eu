<?php

// ECRAN DE VISU COMPTE DE L'UTILISATEUR CONNECTE

include "includes/controle_acces.inc.php";
include "includes/connexion.php";

include "modeles/mod_parametre.php";


// PARAMETRE
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}

if(empty($acc_utilisateur)){
	$erreur_text="Impossible d'afficher la page!";
}

// SUR UTILISATEUR
if(empty($erreur_text)){
	
	$sql="SELECT uti_id,uti_prenom,uti_nom,uti_titre,uti_societe,uti_agence,uti_fonction,uti_tel,uti_mobile,uti_mail,uti_fax, uti_autorise_signature FROM Utilisateurs WHERE uti_id=" . $acc_utilisateur . ";";
	$req=$Conn->query($sql);
	$d_utilisateur=$req->fetch();
	if(empty($d_utilisateur)){
		
		$erreur_text="Impossible d'afficher la page!";
		
	}else{
		
		$identite=$d_utilisateur["uti_prenom"] . " " . $d_utilisateur["uti_nom"];
		if($d_utilisateur["uti_titre"]>0){
			$identite=$base_civilite[$d_utilisateur["uti_titre"]] . " " . $identite;	
		};
		
	}
}

if(empty($erreur_text)){
	
	// SOCIETE
	
	if(!empty($d_utilisateur["uti_societe"])){
		$sql="SELECT soc_nom FROM Societes WHERE soc_id=" . $d_utilisateur["uti_societe"] . ";";
		$req=$Conn->query($sql);
		$d_societe=$req->fetch();
		if(empty($d_societe)){
			unset($d_societe);
		}
	}
	if(!empty($d_utilisateur["uti_agence"])){
		$sql="SELECT age_nom FROM Agences WHERE age_id=" . $d_utilisateur["uti_agence"] . ";";
		$req=$Conn->query($sql);
		$d_agence=$req->fetch();
		if(empty($d_agence)){
			unset($d_agence);
		}
	}
}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Utilisateurs</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link href="assets/admin-tools/admin-forms/css/admin-forms.css" rel="stylesheet" type="text/css">
	
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body class="sb-top sb-top-sm ">

	<div id="main">
<?php	if($_SESSION["acces"]["acc_ref"]==3){
			include "includes/header_sta.inc.php"; 
		}else{
			include "includes/header_def.inc.php"; 
		} ?>
		<section id="content_wrapper" class="">
			<section id="content" class="">
			
		<?php	if(!empty($erreur_text)){ ?>
					<h1 class="bg-danger text-center pt25 pb25" ><?=$erreur_text?></h1>
		<?php	}else{ ?>
					
					<h1>Mon compte</h1>
					
					<div class="panel" >
						<div class="panel-heading panel-head-sm"><?=$identite?></div>
						<div class="panel-body">
							<div class="row">								
								<div class="col-md-12">
									<div class="row" >
								<?php	if(isset($d_societe)){ ?>											
											<div class="col-md-4" >
												Societe : <strong><?=$d_societe["soc_nom"]?></strong>
											</div>
									<?php	if(isset($d_agence)){ ?>											
												<div class="col-md-4" >
													Agence : <strong><?=$d_agence["age_nom"]?></strong>
												</div>											
									<?php 	} 
										} ?>
										<div class="col-md-4" >
											Fonction : <strong><?=$d_utilisateur["uti_fonction"]?></strong>
										</div>
									</div>
									<div class="row mt25" >										
										<div class="col-md-4" >
											Téléphone : <strong><?=$d_utilisateur["uti_tel"]?></strong>
										</div>
										<div class="col-md-4" >
											Portable : <strong><?=$d_utilisateur["uti_mobile"]?></strong>
										</div>
										<div class="col-md-4" >
											Fax : <strong><?=$d_utilisateur["uti_fax"]?></strong>
										</div>
									</div>
									<div class="row mt25" >	
										<div class="col-md-4" >
											Mail : <strong><?=$d_utilisateur["uti_mail"]?></strong>
										</div>
									</div>

									<div class="row mt25" >	
										<div class="col-md-2 text-center admin-form" >
												<p>Autoriser orion a utiliser ma signature sur les documents.</p>
												<span class="switch switch-success">													
													<input class="autorise_signature" id="1" type="checkbox" 
													<?php if(!empty($d_utilisateur["uti_autorise_signature"])){ ?>
														checked
													<?php } ?>>
													<label data-off="NON" data-on="OUI" for="1"></label>
												</span>
										</div>
										<div class="col-md-4 text-center" >
											<a href="signature_mail.php" class="btn btn-lg btn-info" >
												<i class="fa fa-envelope-o" ></i>
												Gérer ma signature mail
											</a>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
	<?php		} ?>
			</section>
		</section>
	</div>
	<footer id="content-footer" class="affix">
		<div class="row">
			<div class="col-xs-3 footer-left"></div>
			<div class="col-xs-3 footer-middle"></div>
			<div class="col-xs-6 footer-right"></div>
        </div>
	</footer>

<?php	
	include "includes/footer_script.inc.php"; ?>	
	<script type="text/javascript">

		jQuery(document).ready(function () {
			$(".autorise_signature").click(function () { 
				var ok = 0;
				if($(this).is(":checked")){
					ok = 1;
				}else{
					ok = 0;
				}
				$.ajax({
					type: "POST",
					url: "ajax/ajax_autorise_signature.php",
					data: "field=" + ok,
					dataType: "html",
					success: function (response) {
					}
				});
				
			});
		});	
	</script>
</body>
</html>