<?php

	include "includes/controle_acces.inc.php";

	include('includes/connexion.php');
	include('includes/connexion_soc.php');

	include "modeles/mod_parametre.php";
	include "modeles/mod_demande_garantie.php";

	$erreur_txt="";

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}
    $acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
	}

	// ENREGISTREMENT

		if(empty($erreur_txt)){

			// REQUETE PREP





				$fichier=demande_garanties();
				if(is_string($fichier)){
					$erreur_txt=$fichier;
				}


		}


	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"aff" => "",
			"titre" => "Erreur",
			"type" => "warning",
			"message" => "Certains clients ne sont pas compatibles pour la demande de garanties"
		);
		$_SESSION['garanties']['erreur'] = $erreur_txt;
	}

	header("Location: demandes_de_garanties.php");

?>
