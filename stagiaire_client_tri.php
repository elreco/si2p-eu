<?php
include "includes/controle_acces_cli.inc.php";

include_once 'includes/connexion.php';

$_SESSION['retour'] = "stagiaire_client_tri.php";

	
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Recherche stagiaire</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="stagiaire_client_liste.php" id="formulaire" >
			<div id="main">
<?php			include "includes/header_cli.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">										
											<div class="content-header">
												<h2>Recherche de <b class="text-primary title-suscli">stagiaire</b></h2>
											</div>												
											<div class="col-md-10 col-md-offset-1">
												<div class="row mb20">
													<div class="col-md-6">
														<label for="sta_nom">Nom du stagiaire :</label>                       
														<input type="text" name="sta_nom" id="sta_nom" class="gui-input" placeholder="Nom du stagiaire" value="" />
													</div>
													<div class="col-md-6">
														<label for="sta_prenom">Prénom du stagiaire :</label>
														<input type="text" name="sta_prenom" id="sta_prenom" class="gui-input" placeholder="Prénom du stagiaire" />
													</div>
												</div>
												<!-- <div class="row mb20">
													<div class="col-md-6">
														<label for="fpr_type">Formation suivi entre le :</label>
														<input type="text" name="ses_date_deb" class="gui-input date datepicker" placeholder="Date du" value="" />
													</div>
													<div class="col-md-6">
														<label for="fpr_type">et le :</label>
														<input type="text" name="ses_date_fin" class="gui-input date datepicker" placeholder="Au" value="" />		
													</div>
												</div>
												<div class="row mb20">
													<div class="col-md-6">
														<label for="fpr_type">Formation suivie :</label>															
														<input type="text" name="produit" id="produit" class="gui-input" placeholder="Nom du produit" />
													</div>
													<div class="col-md-6">
														<label for="ses_lieu">Lieu :</label>
														<input type="text" name="ses_lieu" id="ses_lieu" class="gui-input" placeholder="Lieu" />
													</div>
												</div> -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>		
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left"></div>
					<div class="col-xs-6 footer-middle"></div>
					<div class="col-xs-3 footer-right">
						<button type="submit" name="search" class="btn btn-primary btn-sm">
							<i class='fa fa-search'></i> Rechercher
						</button>

					</div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	
		<script src="assets/admin-tools/admin-forms/js/jquery-ui-datepicker.min.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script src="vendor/plugins/holder/holder.min.js"></script>
		<script src="assets/js/custom.js"></script>
		<script type="text/javascript">


			jQuery(document).ready(function (){
			
			});
		
			
		</script>
	</body>
</html>
