<?php

	// LISTE LA PLANNIFICATION ET LES ANNULATIONS ENTRE J et J +30

	//$menu_actif = 1;
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
    include "includes/connexion_soc.php";
    include "includes/connexion_fct.php";

	// CONTROLE D'ACCES
	// cf item stat_formation.php stat_2

    if($_SESSION['acces']['acc_service'][1]!=1 AND !in_array($_SESSION['acces']['acc_profil'], array('1', '4', '15', '10', '11', '14'))) {
		echo("Accès refusé!");
		die();
    }
    
    $erreur_txt="";
    if(!empty($_POST)){


        $periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}
		
		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
        }

        $indicateur=0;
        if(!empty($_POST["indicateur"])){
            $indicateur=intval($_POST["indicateur"]);
        }

        $_SESSION["stat_form"]=array(
            "periode_deb" => $periode_deb,
            "periode_fin" => $periode_fin,
            "indicateur" => $indicateur
        );

    }else{

        $periode_deb="";
		if(!empty( $_SESSION["stat_form"]["periode_deb"])){
            $periode_deb=$_SESSION["stat_form"]["periode_deb"];
			$DT_periode_deb=date_create_from_format('Y-m-d',$periode_deb);
		}
		
		$periode_fin="";
		if(!empty($_SESSION["stat_form"]["periode_fin"])){
            $periode_fin=$_SESSION["stat_form"]["periode_fin"];
			$DT_periode_fin=date_create_from_format('Y-m-d',$periode_fin);
		}

        $indicateur=0;
        if(!empty($_SESSION["stat_form"]["indicateur"])){
            $indicateur=intval($_SESSION["stat_form"]["indicateur"]);
        }

    }

    if(empty($periode_deb) OR empty($periode_fin) OR empty($indicateur)){
        $erreur_txt="Formulaire incomplet!";

        $_SESSION['message'][] = array(
            "aff" => "",
            "titre" => "Erreur!",
            "type" => "danger",
            "message" => $erreur_txt
        );
        header("location : stat_formation.php");
        die();

    }

	// LE PERSONNE CONNECTE

	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]);
    }

    $acc_utilisateur=0;
    if(!empty($_SESSION['acces']["acc_ref"])){
        if($_SESSION['acces']["acc_ref"]==1){
            $acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
        }
    }

    $sql="SELECT COUNT(sar_avis) as nb_question,SUM(sar_reponse) as score";
    switch ($indicateur) {
        case 1:
            $sql.=",act_pro_sous_famille as cle";
            break;
        case 2:
            $sql.=",acl_produit as cle";
            break;
        case 3:
            $sql.=",sar_utilisateur as cle";
            break;
    }
    $sql.=" FROM Stagiaires_Avis_Reponses 
    INNER JOIN Actions_Clients ON (Stagiaires_Avis_Reponses.sar_action_client=Actions_Clients.acl_id)
    INNER JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
    WHERE NOT acl_archive AND NOT act_archive
    AND act_date_deb>='" . $periode_deb . "' AND act_date_deb<='" . $periode_fin . "'";
    if(!empty($acc_agence)){
        $sql.=" AND act_agence=" . $acc_agence;
    }
    if($_SESSION['acces']['acc_profil'] == 1){
        $sql.=" AND sar_utilisateur=" . $acc_utilisateur;
    }
    switch ($indicateur) {
        case 1:
            $sql.=" GROUP BY act_pro_sous_famille";
            break;
        case 2:
            $sql.=" GROUP BY acl_produit";
            break;
        case 3:
            $sql.=" GROUP BY sar_utilisateur";
            break;
    }
    /*echo($sql);
    die();*/
    $req=$ConnSoc->query($sql);
    $d_avis=$req->fetchAll();



    $titre="Taux de satisfaction";

    switch ($indicateur) {
        case 1:
            $titre_col="Sous-famille";
            $titre.=" par sous-famille de produit";

            $sql="SELECT psf_libelle as label_2,pfa_libelle as label_1,psf_id as cle FROM Produits_Sous_Familles,Produits_Familles WHERE psf_pfa_id=pfa_id ORDER BY psf_id";
            $req=$Conn->query($sql);
            $d_conso=$req->fetchAll(PDO::FETCH_ASSOC);       
        
            break;
        case 2:
            $titre_col="Produit";
            $titre.=" par produit";

            $sql="SELECT pro_id as cle,pro_code_produit as label_1 FROM Produits ORDER BY pro_id";
            $req=$Conn->query($sql);
            $d_conso=$req->fetchAll(PDO::FETCH_ASSOC);      

            break;
        case 3:

            $sql="SELECT uti_id as cle,uti_nom as label_1, uti_prenom as label_2 FROM Utilisateurs ORDER BY uti_id";
            $req=$Conn->query($sql);
            $d_conso=$req->fetchAll(PDO::FETCH_ASSOC);  

            $titre_col="Formateur";
            $titre.=" par formateur";
            break;
    }

    $titre.="<br/>entre le " . $DT_periode_deb->format("d/m/Y") . " et le " . $DT_periode_fin->format("d/m/Y");

    

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>Si2P - ORION</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="Si2P">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
	<!-- PERSO -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="sb-top sb-top-sm" >
		<div id="main" >
<?php
			if($_SESSION["acces"]["acc_ref"]==1){
				include "includes/header_def.inc.php";
			} ?>
			<section id="content_wrapper" >
				<section id="content" class="animated fadeIn" style="overflow:scroll;" >

                    <h1 class="text-center"><?=$titre?></h1>

       <?php        if (!empty($d_avis)) { ?>
                        <table class="table table-striped table-hover" >
                            <thead>
                                <tr class="dark">
                                    <th><?=$titre_col?></th>
                                    <th>Taux de satisfaction (%)</th>
                                </tr>                                    
                            </thead>
                            <tbody>
                <?php           $total_question=0; 
                                $total_score=0;
                                foreach ($d_avis as $avis) { 

                                    $libelle="";
                                    if(isset($d_conso)){

                                       
                                        $k=array_search($avis["cle"],array_column ($d_conso,"cle"));
                                                              
                                       
                                        $libelle=$d_conso[$k]["label_1"];
                                        if(!empty($d_conso[$k]["label_2"])){
                                            $libelle.=" " . $d_conso[$k]["label_2"];
                                        }
                                      

                                        
                                    }

                                    $taux=0;
                                    if(!empty($avis["nb_question"])){
                                        $taux=($avis["score"]/$avis["nb_question"]); 
                                    }

                                    $total_question=$total_question + $avis["nb_question"]; 
                                    $total_score=$total_score + $avis["score"];  ?>

                                    <tr>
                                        <td>
                                            <a href="stat_form_avis_detail.php?cle=<?=$avis["cle"]?>" ><?=$libelle?></a>
                                        </td>
                                        <td class="text-right" ><?=number_format($taux,2,","," ")?></td>    
                                    </tr>
                <?php           } 
                                $taux=0;
                                if(!empty($total_question)){
                                    $taux=($total_score/$total_question); 
                                }
                                ?>
                                <tr>
                                    <td>
                                        <a href="stat_form_avis_detail.php" >
                                            Taux de satisfaction général :
                                        </a> 
                                    </td>
                                    <td class="text-right" ><?=number_format($taux,2,","," ")?></td>
                                </tr>
                            </tbody>
                        </table>
            <?php   } 
                    ?>               
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 text-center">
                    <a href="stat_formation.php" class="btn btn-md btn-default" >
                        Retour
                    </a>
                </div>
				<div class="col-xs-6 footer-middle text-center" style=""></div>
				<div class="col-xs-3 footer-right"></div>
			</div>
		</footer>

<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){

				

			});
		</script>
	</body>
</html>
