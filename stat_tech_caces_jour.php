<?php
	
    // STAT CACES 1
    /*
    Affiche le nombre de jours de formation et de test par formateur. 
    La stat indique également quelles catégories ont été testées au moins un fois sur la période.
    */
	include "includes/controle_acces.inc.php";
	//include "modeles/mod_parametre.php";
	include "includes/connexion.php";
    include "includes/connexion_soc.php";

    // controle d'accès
    
    if($_SESSION["acces"]["acc_ref"]!=1 OR ($_SESSION['acces']['acc_service'][3]!=1 AND $_SESSION['acces']["acc_profil"]!=11 AND $_SESSION['acces']["acc_profil"]!=14 AND $_SESSION['acces']["acc_profil"]!=15 AND $_SESSION['acces']["acc_profil"]!=10) ){
        // SERVICE TECH, DAF, DG, RA et RE
        $erreur="Accès refusé!";
		echo($erreur);
		die();
	}

	
	
	// DONNEE FORM

	$erreur_txt="";
	if(!empty($_POST)){

		$qualification=0;
		if(!empty($_POST["qualification"])){
			$qualification=intval($_POST["qualification"]);
		}
		
		$periode_deb="";
		if(!empty($_POST["periode_deb"])){
			$DT_periode_deb=date_create_from_format('d/m/Y',$_POST["periode_deb"]);
			if(!is_bool($DT_periode_deb)){
				$periode_deb=$DT_periode_deb->format("Y-m-d");
			}
		}
		
		$periode_fin="";
		if(!empty($_POST["periode_fin"])){
			$DT_periode_fin=date_create_from_format('d/m/Y',$_POST["periode_fin"]);
			if(!is_bool($DT_periode_fin)){
				$periode_fin=$DT_periode_fin->format("Y-m-d");
			}
		}
		
		if (empty($periode_deb) OR empty($periode_fin) OR empty($qualification)) {
			$erreur_txt="Formulaire incomplet!";
		}
	}else{
		$erreur_txt="Formulaire incomplet!";
		
	}
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
		header("location : stat_technique.php");
		die();
	}
	
	
	// LE PERSONNE CONNECTE
	
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=intval($_SESSION['acces']["acc_agence"]); 
	}
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]); 
	}
	
    // QUALIFICATIONS
    
    // qualif select
    
    $sql="SELECT qua_libelle FROM Qualifications WHERE qua_id=" . $qualification . ";";
    $req=$Conn->query($sql);
    $d_qualification=$req->fetch();
    if(!empty($d_qualification)){
        $titre="Qualification " . $d_qualification["qua_libelle"];
        $titre.="<br/> entre le " . $DT_periode_deb->format("d/m/Y") . " et le " . $DT_periode_fin->format("d/m/Y") ;
    }else{
        $titre="";
        $erreur_txt="Qualification non identifié!";
    }

    // liste des qualifications

    $sql="SELECT qua_id,qua_libelle FROM Qualifications WHERE qua_caces ORDER BY qua_libelle;";
    $req=$Conn->query($sql);
    $d_qualifications=$req->fetchAll();
    


    if (empty($erreur_txt)) {

        // Liste des produits de la qualifications

        $liste_pro="";
        $sql="SELECT pro_id FROM Produits WHERE pro_qualification=" . $qualification . ";";
        $req=$Conn->query($sql);
        $src_pro=$req->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($src_pro)){
            $tab_pro=array_column ($src_pro,"pro_id");
            $liste_pro=implode($tab_pro,",");
        }
        if (empty($liste_pro)) {
            $erreur_txt="Aucun produit n'est associé à la qualification demandé.";
        }
    }


    if (empty($erreur_txt)) {

        // LES CATEGORIES DE QUALIF

        $sql="SELECT qca_id,qca_libelle FROM Qualifications_Categories WHERE qca_qualification=" . $qualification . " AND NOT qca_archive ORDER BY qca_libelle;";
        $req=$Conn->query($sql);
        $d_categories=$req->fetchAll(PDO::FETCH_ASSOC);
        $tab_rowspan=1;
        if (!empty($d_categories)) {
            $tab_rowspan=2;
        }
    
        // DONNEES PLANNING POUR LE NOMBRE DE JOUR

        $data=array();

        $sql="SELECT COUNT(pda_id) AS nb_demi_j,pda_intervenant,pda_categorie
        ,int_label_1,int_label_2
        FROM Plannings_Dates 
        INNER JOIN Intervenants ON (Plannings_Dates.pda_intervenant=Intervenants.int_id)
        INNER JOIN Actions ON (Actions.act_id=Plannings_Dates.pda_ref_1 AND pda_type=1)
        INNER JOIN (
            SELECT DISTINCT acl_action FROM Actions_Clients WHERE NOT acl_archive AND acl_produit IN (" . $liste_pro . ") 
        ) AS Actions_Qualif ON (Actions_Qualif.acl_action = Actions.act_id)
        WHERE pda_date>='" . $periode_deb . "' AND pda_date<='" . $periode_fin . "' AND NOT pda_archive";
        if (!empty($acc_agence)) {
            $sql.=" AND act_agence=" . $acc_agence;
        }
        $sql.=" GROUP BY pda_intervenant,pda_categorie";
        $req=$ConnSoc->query($sql);
        $d_plannings=$req->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($d_plannings) ) {

            foreach ($d_plannings as $p){

                if (empty($data[$p["pda_intervenant"]])) {

                    $data[$p["pda_intervenant"]]=array(
                        "intervenant" => $p["int_label_1"] . " " . $p["int_label_2"],
                        "formation" => 0,
                        "test" => 0,
                        "categories" => array()
                    );

                    foreach ($d_categories as $cat) {
                        $data[$p["pda_intervenant"]]["categories"][$cat["qca_id"]]=false;
                        
                    }

                }

                if ($p["pda_categorie"]==2) {

                    $data[$p["pda_intervenant"]]["test"]=$data[$p["pda_intervenant"]]["test"] + $p["nb_demi_j"];

                } else {

                    $data[$p["pda_intervenant"]]["formation"]=$data[$p["pda_intervenant"]]["formation"] + $p["nb_demi_j"];

                }

            }

        } else {
            $erreur_txt="Aucun résultat!";
        }
    }

    if (empty($erreur_txt) AND !empty($d_categories)) {

        // LES TEST CACES

        $sql="SELECT DISTINCT dcc_categorie,dcc_testeur FROM diplomes_caces_cat,diplomes_caces
        WHERE dcc_diplome=dca_id
        AND dca_diplome=2 AND dcc_date>='" . $periode_deb . "' AND dcc_date<='" . $periode_fin . "' AND dca_qualification=" . $qualification . "
        AND dcc_valide";
        $sql.=" AND (
            (dca_action_soc_1=" . $acc_societe . " AND dcc_passage=1)
            OR (dca_action_soc_2=" . $acc_societe . " AND dcc_passage=2)
            OR (dca_action_soc_3=" . $acc_societe . " AND dcc_passage=3)
            OR (dca_action_soc_4=" . $acc_societe . " AND dcc_passage=4)
            OR (dca_action_soc_5=" . $acc_societe . " AND dcc_passage=5)
            OR (dca_action_soc_6=" . $acc_societe . " AND dcc_passage=6)
        )";
        $sql.=" ORDER BY dcc_testeur,dcc_categorie";
        $req=$Conn->query($sql);
        $d_tests=$req->fetchAll();
        if(!empty($d_tests)){
            foreach($d_tests as $test){
                
                if(!empty($data[$test["dcc_testeur"]])){

                    $data[$test["dcc_testeur"]]["categories"][$test["dcc_categorie"]]=true;
                }
            }
            
        }
    }
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">

		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

		<!-- PERSO -->	
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
		
		<div id="main" >
<?php		include "includes/header_def.inc.php";  ?>			
			<section id="content_wrapper" >					
			
				<section id="content" class="animated fadeIn" >


                    <h1 class="text-center" >
                        <?=$titre?>
                    </h1>

                    <div class="row" >
                        <div class="col-md-8 col-md-offset-2" >
                    
                            <div class="panel mt10 panel-info" > 
                                <div class="panel-heading panel-head-sm" >
                                    <span class="panel-title">Recherche rapide.</span>									
                                </div>
                                <div class="panel-body p5" >

                                    <form method="post" action="stat_tech_caces_jour.php" >
                                        <div>													
                                            <input type="hidden" name="periode_deb" value="<?=$DT_periode_deb->format("d/m/Y")?>" />
                                            <input type="hidden" name="periode_fin" value="<?=$DT_periode_fin->format("d/m/Y")?>" />													
                                        </div>
                                        <div class="admin-form" >
                                            <div class="row mt15" >
                                                <div class="col-md-10" >
                                                    <label for="qualification" >Qualification</label>
                                                    <select class="select2" id="qualification" name="qualification" >                                                      
                                                <?php	foreach($d_qualifications as $qualif){
                                                            if($qualif["qua_id"]==$qualification){
                                                                echo("<option value='" . $qualif["qua_id"] . "' selected >" . $qualif["qua_libelle"] . "</option>");
                                                            }else{
                                                                echo("<option value='" . $qualif["qua_id"] . "' >" . $qualif["qua_libelle"] . "</option>");
                                                            }
                                                        } ?>
                                                
                                                    </select>
                                                </div>
                                                <div class="col-md-2 text-center mt20" >
                                                    <button type="submit" class="btn btn-md btn-info" >
                                                        Afficher
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

            
            <?php	if (!empty($erreur_txt)) {   ?>
                    
                        <p class="alert alert-danger" ><?=$erreur_txt?></p>

        <?php       } else {  ?>

                        <div class="row" >
                            <div class="col-md-12" >
                            
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" >
                                        <thead>
                                            <tr class="dark">
                                                <th rowspan="<?=$tab_rowspan?>" >Intervenants</th>
                                                <th rowspan="<?=$tab_rowspan?>" >Nombre de jours de formation</th>
                                                <th rowspan="<?=$tab_rowspan?>" >Nombre de jours de test</th>
                                        <?php   if (!empty($d_categories)) {
                                                    echo("<th colspan='" . count($d_categories). "' >Catégories testées</th>");
                                                } ?>						
                                            </tr>
                                        <?php  if (!empty($d_categories)) {
                                                echo("<tr>");
                                                    foreach($d_categories as $cat){
                                                        echo("<td>" . $cat["qca_libelle"] . "</td>");
                                                    }
                                                echo("</tr>");
                                            } ?>	
                                        </thead>
                                        <tbody>
                                <?php		$total_form=0;
                                            $total_test=0;
                                            foreach($data as $s){  
                                        
                                                $total_form=$total_form + $s["formation"];
                                                $total_test=$total_test + $s["test"];

                                                ?>
                                                <tr>
                                                    <td><?=$s["intervenant"]?></td>
                                                    <td class="text-right" ><?=$s["formation"]/2?></td>
                                                    <td class="text-right" ><?=$s["test"]/2?></td>

                                    <?php           if (!empty($d_categories)) {

                                                        foreach($d_categories as $cat){

                                                            if($s["categories"][$cat["qca_id"]]) {
                                                                echo("<td>OUI</td>");
                                                            } else {
                                                                echo("<td>&nbsp;</td>");
                                                            }
                                                        }

                                                    } ?>
                                                </tr>
                                <?php		} ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Total :</th>
                                                <td class="text-right" ><?=$total_form/2?></td>
                                                <td class="text-right" ><?=$total_test/2?></td>
                                            <?php   if (!empty($d_categories)) {
                                                    echo("<td colspan='" . count($d_categories). "' >&nbsp;</td>");
                                                } ?>
                                            </tr>
                                        </tfoot>
                                    </table>							
                                </div>
                                
                            </div>
						</div>
		<?php	    } ?>

				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="stat_technique.php" class="btn btn-sm btn-default"  >
							<i class="fa fa-left-arrow" ></i>Retour
						</a>					
					</div>
					<div class="col-xs-6 footer-middle text-center" style=""></div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>

		<!-- SCRIPT SPE -->
		<script type="text/javascript">
			jQuery(document).ready(function () {
				
			
				
			});
			(jQuery);
		</script>
	</body>
</html>
