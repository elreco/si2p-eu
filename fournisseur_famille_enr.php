<?php
include "includes/controle_acces.inc.php";
// error_reporting( error_reporting() & ~E_NOTICE );
include "includes/connexion.php";

// si c'est une création
if (!empty($_POST) && !isset($_POST['ffa_id'])) {

    $req = $Conn->prepare("INSERT INTO fournisseurs_familles (ffa_libelle,ffa_droit) VALUES(:ffa_libelle,:ffa_droit)");
    $req->bindParam('ffa_libelle', $_POST['ffa_libelle']);
    $req->bindParam('ffa_droit', $_POST['ffa_droit']);
    $req->execute();
// c'est une édition
}else{

    $req = $Conn->prepare("UPDATE fournisseurs_familles SET ffa_libelle=:ffa_libelle,ffa_droit=:ffa_droit WHERE ffa_id = :ffa_id");
    $req->bindParam('ffa_libelle', $_POST['ffa_libelle']);
    $req->bindParam('ffa_droit', $_POST['ffa_droit']);
    $req->bindParam('ffa_id', $_POST['ffa_id']);
    $req->execute();

}
$_SESSION['message'][] = array(
    "titre" => "Succès",
    "message" => "La famille de fournisseur a été enregistrée",
    "type" => "success"

    );
Header("Location: fournisseur_famille_liste.php");