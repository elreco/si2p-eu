 <?php

	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");
	include_once("includes/connexion_fct.php");

	// LISTE DES Actions

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}
	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	if(!empty($_POST)){

		$sql_param=array();
		$aff_param=array();

		$mil="";
		$periode_lib="";
		
		$opt_recherche=0;
		if(!empty($_POST["option_recherche"])){
			$opt_recherche=intval($_POST["option_recherche"]);
		}
		$aff_param["opt_recherche"]=$opt_recherche;

		$opt_blocage=0;
		if(!empty($_POST["option_blocage"])){
			$opt_blocage=intval($_POST["option_blocage"]);
		}
		$aff_param["opt_blocage"]=$opt_blocage;

		$sql_deb="SELECT act_id,act_adr_ville,act_date_deb,act_adr_cp";
		$sql_deb.=",acl_non_fac,acl_id,acl_pro_inter,acl_creation_date,acl_ca,acl_pro_reference,acl_ca_unit,acl_confirme,acl_for_nb_sta_resa,acl_for_nb_sta,acl_client";
		$sql_deb.=",cli_code,cli_nom,cli_affacturable";
		$sql_deb.=",com_label_1";
		$sql_deb.=",COUNT(acd_date) AS nb_demi_j";
		$sql_deb.=",int_label_1";
		$sql_deb.=" FROM Actions LEFT OUTER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)
		LEFT OUTER JOIN Clients ON (Actions_Clients.acl_client=Clients.cli_id)
		LEFT OUTER JOIN Actions_Clients_Dates ON (Actions_Clients.acl_id=Actions_Clients_Dates.acd_action_client)
		LEFT OUTER JOIN Intervenants ON (Actions.act_intervenant=Intervenants.int_id)
		LEFT OUTER JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)";



		// CRITERE POUR TOUTES LES SOC

		$act_id=0;
		if(!empty($_POST["act_id"])){
			$act_id=intval($_POST["act_id"]);
		}
		if(!empty($act_id)){
			$mil.=" AND act_id=:act_id";
			$sql_param["act_id"]=$act_id;
		}
		$acl_id=0;
		if(!empty($_POST["acl_id"])){
			$acl_id=intval($_POST["acl_id"]);
		}
		if(!empty($acl_id)){
			$mil.=" AND acl_id=:acl_id";
			$sql_param["acl_id"]=$acl_id;
		}

		if(empty($mil)){

			if(!empty($_POST["act_date_deb"])){
				$DT_periode_deb=date_create_from_format('d/m/Y',$_POST["act_date_deb"]);
				if(!is_bool($DT_periode_deb)){
					$periode_deb=$DT_periode_deb->format("Y-m-d");
					$mil.=" AND act_date_deb>=:date_deb";
					$sql_param["date_deb"]=$periode_deb;
				}
			}
			if(!empty($_POST["act_date_fin"])){
				$DT_periode_fin=date_create_from_format('d/m/Y',$_POST["act_date_fin"]);
				if(!is_bool($DT_periode_fin)){
					$periode_fin=$DT_periode_fin->format("Y-m-d");
					$mil.=" AND act_date_deb<=:date_fin";
					$sql_param["date_fin"]=$periode_fin;
				}
			}

			if(!empty($sql_param["date_deb"]) AND empty($sql_param["date_fin"])){
				$aff_param["periode_lib"]="Actions à partir du " . $DT_periode_deb->format("d/m/Y");
			}elseif(empty($sql_param["date_deb"]) AND !empty($sql_param["date_fin"])){
				$aff_param["periode_lib"]="Actions jusqu'au " . $DT_periode_fin->format("d/m/Y");
			}elseif(!empty($sql_param["date_deb"]) AND !empty($sql_param["date_fin"])){
				$aff_param["periode_lib"]="Actions entre le " . $DT_periode_deb->format("d/m/Y") . " et le " . $DT_periode_fin->format("d/m/Y");
			}else{
				$aff_param["periode_lib"]="";
			}


			if(!empty($_POST["client"])){
				$mil.=" AND acl_client=:client";
				$sql_param["client"]=$_POST["client"];
			}
			if(!empty($_POST["cli_code"])){
				$mil.=" AND cli_code LIKE :cli_code";
				$sql_param["cli_code"]=$_POST["cli_code"] . "%";
			}
			if(!empty($_POST["commercial"])){
				$mil.=" AND acl_commercial=:commercial";
				$sql_param["commercial"]=$_POST["commercial"];
			}
			if(!empty($_POST["intervenant"])){
				$mil.=" AND act_intervenant=:intervenant";
				$sql_param["intervenant"]=$_POST["intervenant"];
			}
			if(!empty($_POST["type"])){
				$mil.=" AND act_pro_type=:type";
				$sql_param["type"]=$_POST["type"];
			}
			if(!empty($_POST["famille"])){
				$mil.=" AND act_pro_famille=:famille";
				$sql_param["famille"]=$_POST["famille"];
			}
			if(!empty($_POST["sous_famille"])){
				$mil.=" AND act_pro_sous_famille=:sous_famille";
				$sql_param["sous_famille"]=$_POST["sous_famille"];
			}
			if(!empty($_POST["sous_sous_famille"])){
				$mil.=" AND act_pro_sous_sous_famille=:sous_sous_famille";
				$sql_param["sous_sous_famille"]=$_POST["sous_sous_famille"];
			}
			if(!empty($_POST["cp"])){
				$mil.=" AND act_adr_cp LIKE :cp";
				$sql_param["cp"]=$_POST["cp"] . "%";
			}
			if(!empty($_POST["ville"])){
				$mil.=" AND act_adr_ville=:ville";
				$sql_param["ville"]=$_POST["ville"];
			}
		}

		// CRITERE POUR SOC LOG

		$mil_soc="";

		// critere auto
		if(!empty($acc_agence)){
			$mil_soc.= " AND act_agence = " . $acc_agence;
		}
		if(!$_SESSION['acces']["acc_droits"][6]){
			$mil_soc.=" AND com_type=1 AND com_ref_1=" . $acc_utilisateur;
		}

		$aff_param["search_lib"]="";
		if($opt_recherche==1){
			$mil.=" AND (act_archive=1 OR acl_archive=1)";
			$aff_param["search_lib"]="Actions archivées";
		}else{
			if($opt_recherche!=3){
				$mil.=" AND act_archive=0 AND acl_archive=0";
			}else{
				$mil.=" AND act_archive=0";
			}
			switch($opt_recherche){
				case 2:
					$mil.=" AND acl_confirme=0";
					$aff_param["search_lib"]="actions non-confirmées";
					break;
				case 3:
					$mil.=" AND ISNULL(acl_client)";
					$aff_param["search_lib"]="actions sans client";
					break;
				case 4:
					$mil_soc.=" AND (NOT act_alerte_ok AND act_alerte_date<=NOW())";
					$aff_param["search_lib"]="alertes actives";
					break;
				case 5:
					$mil.=" AND acl_ca_nc";
					$aff_param["search_lib"]=" actions sans tarif";
					break;
				case 6:
					$mil_soc.=" AND acl_non_fac=1";
					$aff_param["search_lib"]="actions non facturables";
					break;
			}
		}
		
		if($opt_blocage==1){
			$mil.=" AND act_verrou_admin";
			$aff_param["search_lib"]="Actions bloquées";
		}elseif($opt_blocage==2){
			$mil.=" AND act_verrou_marge";
			$aff_param["search_lib"]="Actions bloquées : Dérogation marge";
		}elseif($opt_blocage==3){
			$mil.=" AND act_verrou_bc";
			$aff_param["search_lib"]="Actions bloquées : Sous-traitance sans BC";
		}

		$sql_fin=" GROUP BY act_id,act_adr_ville,act_date_deb";
		$sql_fin.=",acl_id,acl_pro_inter,acl_creation_date,acl_ca,acl_pro_reference,acl_client";
		$sql_fin.=",cli_code,cli_nom,cli_affacturable";
		$sql_fin.=",com_label_1";
		$sql_fin.=",int_label_1";
		$sql_fin.=" ORDER BY act_date_deb,act_intervenant,act_id,acl_id";

		$sql_action=$sql_deb;
		if(!empty($mil)){
			$sql_action.=" WHERE " . substr($mil, 5);
			if(!empty($mil_soc)){
				$sql_action.=$mil_soc;
			}
		}elseif(!empty($mil_soc)){
			$sql_action.=" WHERE " . substr($mil_soc, 5);
		}

		$sql_action.=$sql_fin;

		$_SESSION["sql"]=$sql_action;
		$_SESSION["sql_param"]=$sql_param;
		$_SESSION["aff_param"]=$aff_param;

	}else{
		$sql_action=$_SESSION["sql"];
		$sql_param=$_SESSION["sql_param"];
		$aff_param=$_SESSION["aff_param"];
	}

	if(!empty($sql_param)){
		$req=$ConnSoc->prepare($sql_action);
		foreach($sql_param as $k => $v){
			$req->bindValue($k,$v);
		}
		$req->execute();
	}else{
		$req=$ConnSoc->query($sql_action);
	}

	$actions=$req->fetchAll();

	if($acc_agence==4 AND $_SESSION["acces"]["acc_droits"][8]){

		$action_gc=array();

		// ACTIONS GRAND-COMPTES

		if(empty($_SESSION["sql_gc"])){

			$mil_soc.=" AND acl_facturation=1 AND cli_categorie=2";

			switch($opt_recherche){
				case 4:
					$mil_soc.=" AND (NOT act_alerte_ok AND act_alerte_date<=NOW())";
					$aff_param["search_lib"]="alertes actives";
					break;
				case 6:
					$mil_soc.=" AND acl_non_fac=1";
					$aff_param["search_lib"]="actions non facturables";
					break;
			}

			$sql_action_gc=$sql_deb;
			if(!empty($mil)){
				$sql_action_gc.=" WHERE " . substr($mil, 5);
				if(!empty($mil_soc)){
					$sql_action_gc.=$mil_soc;
				}
			}elseif(!empty($mil_soc)){
				$sql_action_gc.=" WHERE " . substr($mil_soc, 5);
			}
			$sql_action_gc.=$sql_fin;

			$_SESSION["sql_action_gc"]=$sql_action_gc;


		}else{
			$sql_action_gc=$_SESSION["sql_gc"];
		}

		$sql="SELECT soc_id FROM Societes WHERE NOT soc_id=" . $acc_societe . " AND soc_id IN (2,3,5) ;";
		$req=$Conn->query($sql);
		$d_societes=$req->fetchAll();
		if(!empty($d_societes)){
			foreach($d_societes as $d_soc){

				$ConnFct=connexion_fct($d_soc["soc_id"]);

				//echo($sql_action_gc);
				if(!empty($sql_param)){

					$req=$ConnFct->prepare($sql_action_gc);
					foreach($sql_param as $k => $v){
						/*var_dump($k);
						echo("<br/>");
						var_dump($v);
						echo("<br/>");echo("<br/>");
						*/

						$req->bindValue($k,$v);
					}
					$req->execute();
				}else{
					$req=$ConnFct->query($sql_action_gc);
				}
				$actions_gc[$d_soc["soc_id"]]=$req->fetchAll();
			}
		}
	}



	// retour

	$_SESSION["retourFacture"]="action_liste.php";
	$_SESSION["retourClient"]="action_liste.php";
	$_SESSION["retour_action"]="action_liste.php";

	// GESTION DU MENU
	if(isset($_GET["menu_actif"])){
		$_SESSION["menu_actif"]=explode("-",$_GET["menu_actif"]);
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion - Recherche client</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

		<!-- PLUGIN -->
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<!-- PERSO Si2P -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

	   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="action_liste.php">
			<div id="main">
    <?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">


					<section id="content" class="animated">
				<?php	if(!empty($aff_param["periode_lib"])){
							echo("<h1>" . $aff_param["periode_lib"] . "</h1>");
						}
						if(!empty($aff_param["search_lib"])){
							echo("<h1>");
								echo(count($actions) . " " . $aff_param["search_lib"]);
							echo("</h1>");
						}


						if(!empty($actions)){ ?>
							<div class="table-responsive" >
								<table class="table table-striped table-hover" id="tabAction" data-tri_col="7" data-tri_sens="asc">
									<thead id="tabRappelHead" >
										<tr class="dark" >
											<th>ID</th>
											<th>Code client</th>
											<th>Nom client</th>
											<th>Commercial</th>
											<th>Produits</th>
											<th>Lieu</th>
											<th>Intervenant</th>
											<th>Date de début</th>
											<th>Date de création</th>
											<th>Nb 1/2 j</th>
											<th>CA (€)</th>
											<th>CA projet (€)</th>
											<th class="no-sort">&nbsp;</th>
										</tr>
									</thead>
									<tbody id="tabRappelBody" >
					<?php				$total_ca=0;
										$total_ca_projet=0;
										$total_demi_jour=0;

										foreach($actions as $a){

											$act_date_deb="";
											if(!empty($a["act_date_deb"])){
												$date_deb=new DateTime($a["act_date_deb"]);
												$act_date_deb=$date_deb->format("d/m/Y");
											}
											$acl_creation_date="";
											if(!empty($a["acl_creation_date"])){
												$creation_date=new DateTime($a["acl_creation_date"]);
												$acl_creation_date=$creation_date->format("d/m/Y");
											}

											// style de la ligne
											$class_act="";
											if($_SESSION['acces']['acc_service'][2]==1){
												if($a['cli_affacturable']!=1){
													$class_act="class='warning'";
												}
											}

											$ca=0;
											$ca_projet=0;
											if($a["acl_pro_inter"]==0){
												if($a["acl_confirme"]){
													$ca=$a["acl_ca"];
												}else{
													$ca_projet=$a["acl_ca"];
												}
											}else{
												// INTER
												if($a["acl_confirme"]){
													$ca=$a["acl_ca"];
												}else{
													$ca_projet=$a["acl_ca"];
													//$ca_projet=$a["acl_ca_unit"]*($a["acl_for_nb_sta_resa"]+$a["acl_for_nb_sta"]);
												}
											}
											$total_ca=$total_ca+$ca;
											$total_ca_projet=$total_ca_projet+$ca_projet;
											
											$total_demi_jour=$total_demi_jour + $a["nb_demi_j"];
											?>
											<tr <?=$class_act?> >
												<td><?=$a["act_id"] . "-" . $a["acl_id"]?></td>
										<?php	if(!empty($a["acl_id"])){ ?>
													<td>
										<?php			if($_SESSION['acces']['acc_service'][2]==1){ ?>
															<a href="client_voir.php?client=<?=$a['acl_client']?>" >
																<?=$a['cli_code']?>
															</a>
										<?php			}else{
															echo($a['cli_code']);
														} ?>
													</td>
													<td><?=$a["cli_nom"]?></td>
													<td><?=$a["com_label_1"]?></td>
										<?php	}else{ ?>
													<td colspan="3" class="text-danger text-center" >Pas de client</td>
										<?php	} ?>
												<td><?=$a["acl_pro_reference"]?></td>
												<td><?=$a["act_adr_cp"] . " " . $a["act_adr_ville"]?></td>
												<td><?=$a["int_label_1"]?></td>
												<td data-order="<?= $a['act_date_deb'] ?>"><?=$act_date_deb?></td>
												<td data-order="<?= $a['acl_creation_date'] ?>"><?=$acl_creation_date?></td>
												<td class="text-right" ><?=$a["nb_demi_j"]?></td>
												<td data-order="<?= $ca ?>" class="text-right text-success" >
											<?php	if(!empty($ca)){
														echo(number_format($ca, 2, ',', ' '));
													} ?>
                                                    <?php		if(!empty($a["acl_non_fac"])){ ?>
                                                        <span class="badge badge-danger">
                                                            NF
                                                        </span>
                                                    <?php } ?>
												</td>
												<td data-order="<?= $ca_projet ?>" class="text-right text-warning" >
										<?php		if(!empty($ca_projet)){
														echo(number_format($ca_projet, 2, ',', ' '));
													} ?>
												</td>

												<td class="text-center" >
													<a href="action_voir.php?action=<?=$a["act_id"]?>&action_client=<?=$a["acl_id"]?>" class="btn btn-info btn-sm" >
														<i class="fa fa-eye" ></i>
													</a>
												</td>

											</tr>
					<?php				} ?>

									</tbody>
									<tfoot>
										<tr>
											<th colspan="9" class="text-right" >Total :</th>
											<td class="text-right" ><?=$total_demi_jour?></td>
											
											<td class="text-right text-success" >
										<?php	if(!empty($total_ca)){
													echo("<b>" . number_format($total_ca, 2, ',', ' ') . "</b>");
												} ?>
											</td>
											<td class="text-right text-warning" >
									<?php		if(!empty($total_ca_projet)){
													echo("<b>" . number_format($total_ca_projet, 2, ',', ' ') . "</b>");
												} ?>
											</td>
											<td>&nbsp;</td>
										</tr>
									</tfoot>
								</table>
							</div>
				<?php	}else{ ?>
							<p class="alert alert-warning" >
								Aucune action ne correspond à votre recherche.
							</p>
				<?php	} ?>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
						<a href="action_tri.php" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i> Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle">
			<?php		if($_SESSION['acces']['acc_service'][2]==1){	?>
							<button type="button" class="btn btn-system btn-sm" data-toggle="modal" data-target="#modal_aide" >
								<i class="fa fa-info" ></i>
							</button>
			<?php		} ?>
					</div>
					<div class="col-xs-3 footer-right">

					</div>
				</div>
			</footer>
		</form>

		<div id="modal_aide" class="modal fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close modal-annule" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="mdl_doc_titre" >
							<i class="fa fa-info" ></i> - <span>Aide</span>
						</h4>
					</div>
					<div class="modal-body" >
						<table class="table" >
							<caption>Code couleur</caption>
							<tr class="warning" >
								<td>Client non affacturable</td>
							</tr>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm modal-annule" data-dismiss="modal">
							<i class="fa fa-close" ></i>
							Fermer
						</button>
					</div>
				</div>
			</div>
		</div>
<?php
		include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="/vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="/vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css"></script>
		<script type="text/javascript" src="/vendor/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="vendor/plugins/mask/jquery.mask.js"></script>
		<script type="text/javascript" src="assets/js/custom.js"></script>


		<script type="text/javascript">

			jQuery(document).ready(function (){

                tri_col=$("#tabAction").data("tri_col");
            	tri_sens=$("#tabAction").data("tri_sens");
                // initilisation plugin datatables
                $('#tabAction').DataTable({
                    "language": {
                      "url": "vendor/plugins/DataTables/media/js/French.json"
                    },
                	"order":[tri_col,tri_sens],
                    "paging": false,
                    "searching": false,
                    "info": false,
                    "columnDefs": [ {
                          "targets": 'no-sort',
                          "orderable": false,
                  	}]
                });
				// ----- FIN DOC READY -----
			});


		</script>
	</body>
</html>
