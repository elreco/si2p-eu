<?php 
include('includes/connexion.php');

$erreur_txt="";

if(isset($_GET)){
	$grille=0;
	if(!empty($_GET['id'])){
		$grille=intval($_GET['id']);
	}
	if(empty($grille)){
		$erreur_txt="B Impossible de supprimer la grille";
	}
}else{
	$erreur_txt="A Impossible de supprimer la grille";
}

if(empty($erreur_txt)){

	$req = $Conn->query("SELECT cgr_famille FROM commandes_grilles WHERE cgr_id = " . $grille);
	$d_grille = $req->fetch();
	if(!empty($d_grille)){
		
		$req = $Conn->prepare("DELETE FROM commandes_grilles WHERE cgr_id = " . $grille);
		$req->execute();

	}else{
		$erreur_txt="Impossible de supprimer la grille";
	}
}

// ON MAJ LES TRANCHE
if(empty($erreur_txt)){
	
	$req_max = $Conn->prepare("UPDATE commandes_grilles SET 
	cgr_montant_max=:cgr_montant_max
	WHERE cgr_id=:cgr_id;");
	
	$req = $Conn->query("SELECT cgr_id,cgr_montant_min,cgr_montant_max FROM commandes_grilles WHERE cgr_famille= " . $d_grille["cgr_famille"] . " ORDER BY cgr_montant_min");
	$d_tranches = $req->fetchAll();
	if(!empty($d_tranches)){
		foreach($d_tranches as $k => $t){
			if($k<count($d_tranches)-1){
				$num=intval($k+1);
				$cgr_montant_max=floatval($d_tranches[$num]["cgr_montant_min"]);
				$cgr_montant_max=$cgr_montant_max-0.01;
			}else{
				$cgr_montant_max=99999999;
			}
			$req_max->bindParam(":cgr_id",$t["cgr_id"]);
			$req_max->bindParam(":cgr_montant_max",$cgr_montant_max);
			$req_max->execute();
		}
	}
}
if(!empty($erreur_txt)){
	$_SESSION['message'][] = array(
		"titre" => "Erreur",
		"type" => "danger",
		"message" => $erreur_txt
	);
}
Header('Location: validations_bc.php');
die();
?>