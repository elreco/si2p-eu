 <?php
 
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");
	
	include_once("modeles/mod_get_ressources_vehicules.php");
	include_once("modeles/mod_get_ressources_salles.php");
	
	// ECRAN DE MAJ D'UN ACTION
	
	$erreur=0;
	$erreur_txt="";
	
	$action_id=0;
	if(isset($_GET["action"])){
		if(!empty($_GET["action"])){
			$action_id=$_GET["action"];
		}	
	}
	if(empty($action_id)){
		$erreur_txt="Impossible d'afficher la page";
	}
	
	if(empty($erreur_txt)){
		
		// ON RECUPERE LES DONNES NECESSAIRE A L'AFFICHAGE
		
		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			$acc_societe=$_SESSION['acces']["acc_societe"];  
		}
		if($conn_soc_id!=$acc_societe){
			$acc_societe=$conn_soc_id;
		}
		
		// SUR L'ACTION
		
		$sql="SELECT * FROM Actions WHERE act_id=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action",$action_id);
		$req->execute();
		$action=$req->fetch();
		if(!empty($action)){
			$dossier_date=date_create_from_format('Y-m-d',$action["act_dossier_date"]);
		}else{
			$erreur_txt="Impossible d'afficher la page";
		}
	}
	
	if(empty($erreur_txt)){
		
		// FAMILLES
		
		$action_head="";
		$action_bg="";
		$action_txt="";
		
		$sql="SELECT pfa_libelle FROM Produits_Familles WHERE pfa_id=" . $action["act_pro_famille"] . ";";
		$req=$Conn->query($sql);
		$d_pro_fam=$req->fetch();
		if(!empty($d_pro_fam)){
			$action_head.=" " . $d_pro_fam["pfa_libelle"];
		}
		
		if(!empty($action["act_pro_sous_famille"])){
			$sql="SELECT psf_libelle,psf_couleur_bg,psf_couleur_txt FROM Produits_Sous_Familles WHERE psf_id=" . $action["act_pro_sous_famille"] . ";";
			$req=$Conn->query($sql);
			$d_pro_s_fam=$req->fetch();
			if(!empty($d_pro_s_fam)){
				$action_head.=" " . $d_pro_s_fam["psf_libelle"];
				if(!empty($d_pro_s_fam["psf_couleur_bg"])){
					$action_bg=$d_pro_s_fam["psf_couleur_bg"];
					$action_txt=$d_pro_s_fam["psf_couleur_txt"];
				}
			}
		}
		if(!empty($action["act_pro_sous_sous_famille"])){
			$sql="SELECT pss_libelle,pss_couleur_bg,pss_couleur_txt FROM Produits_Sous_Sous_Familles WHERE pss_id=" . $action["act_pro_sous_sous_famille"] . ";";
			$req=$Conn->query($sql);
			$d_pro_s_s_fam=$req->fetch();
			if(!empty($d_pro_s_s_fam)){
				$action_head.=" " . $d_pro_s_s_fam["pss_libelle"];
				if(!empty($d_pro_s_s_fam["pss_couleur_bg"])){
					$action_bg=$d_pro_s_s_fam["pss_couleur_bg"];
					$action_txt=$d_pro_s_s_fam["pss_couleur_txt"];
				}
			}
		}
		if(!empty($action_bg)){
			$action_bg="style='background-color:#" . $action_bg . ";color:" . $action_txt . "'";	
		}
		
		
		// LISTE DES CLIENTS INSCRIT
		
		$sql="SELECT cli_id,cli_code,cli_nom FROM Clients,Actions_Clients WHERE cli_id=acl_client AND acl_action=" . $action_id . ";";
		$req=$ConnSoc->query($sql);
		$d_action_client=$req->fetchAll();
		
		// INTERVENTION CHEZ UN CLIENT
		
		$d_adresses=array();
		$d_contacts=array();
		
		if($action["act_adr_ref"]==1 AND $action["act_adr_ref_id"]>0){
			
			// -> ON VA RECUPERER LES ADRESSES
			$sql="SELECT * FROM Clients,Adresses WHERE cli_id=adr_ref_id AND adr_ref=1 AND cli_id=" . $action["act_adr_ref_id"] . " AND adr_type=1 ORDER BY adr_nom,adr_cp,adr_ville;";
			$req=$Conn->query($sql);
			$d_adresses=$req->fetchAll();
			
			// -> SI ADRESSES DE SELECTIONNE -> ON VA CHERHCER LES CONTACTS ASSOCIES
			
			if(!empty($action["act_adresse"])){
				$sql="SELECT * FROM Contacts LEFT OUTER JOIN Adresses_Contacts ON (Contacts.con_id=Adresses_Contacts.aco_contact) 
				WHERE (aco_adresse=" . $action["act_adresse"] . " OR ISNULL(aco_adresse)) AND con_ref_id=". $action["act_adr_ref_id"] . ";";
				$req=$Conn->query($sql);
				$d_contacts=$req->fetchAll();
			}
		}
		
		// ON RECUPERE LES DATES DE FORMATIONS POUR LE CALCUL DES RESSOURCES
			
		$sql="SELECT pda_date,pda_demi FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=" . $action_id . " AND NOT pda_archive;";
		$req=$ConnSoc->query($sql);
		$d_dates=$req->fetchAll(PDO::FETCH_NUM);

		// VEHICULES LIBRE
		$d_vehicules=get_ressources_vehicules($d_dates,$action["act_agence"],$action_id);
	
		// SALLE LIBRE 
		$d_salles=get_ressources_salles($d_dates,$action["act_agence"],$action_id);
			
		
		/*
		
		$sql="SELECT sal_id,sal_nom FROM Salles";
		if(!empty($action["act_agence"])){
			$sql.=" WHERE sal_agence=" . $action["act_agence"];
		}
		$sql.=" ORDER BY sal_nom;";
		$req=$ConnSoc->query($sql);
		$d_salles=$req->fetchAll();
		*/
		
		
		/*$sql="SELECT veh_id,veh_libelle FROM Vehicules WHERE NOT veh_archive AND veh_societe=". $acc_societe;
		if(!empty($action["act_agence"])){
			$sql.=" AND (veh_agence=" . $action["act_agence"] . " OR veh_agence=0 OR ISNULL(veh_agence))";
		}
		$sql.=" ORDER BY veh_libelle;";
		$req=$Conn->query($sql);
		$d_vehicules=$req->fetchAll();*/
	}
	
	if(!empty($erreur_txt)){
		echo($erreur_txt);
	}else{
?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<title>SI2P - Orion</title>
				<meta name="keywords" content=""/>
				<meta name="description" content="">
				<meta name="author" content="">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<!-- CSS THEME -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
				<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />

				<!-- CSS PLUGINS -->
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
				<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
				
				<!-- CSS Si2P -->
				<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
				
				<!-- Favicon -->
				<link rel="shortcut icon" href="assets/img/favicon.png">
			   
				<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
				<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
			</head>
			<body class="sb-top sb-top-sm ">
				<form method="post" action="action_mod_enr.php" id="form_modal_client" >
					<div>
						<input type="hidden" name="action" value="<?=$action_id?>" />
						<input type="hidden" name="societ" value="<?=$conn_soc_id?>" />
					</div>
					<div id="main">
			<?php		include "includes/header_def.inc.php"; ?>
						<section id="content_wrapper">						
							<section id="content" class="animated">	

								<div class="row" >
							
									<div class="col-md-12">
										
										<div class="admin-form theme-primary ">												
											
											<div class="panel heading-border panel-primary">
											
												<div class="panel-body bg-light">

													<div class="content-header mtn mbn">
												<?php	echo("<h2>Modification <b class='text-primary' >Action N°". $action_id . "</b></h2>"); ?>
													</div>		

													<div class="row" >
														<!-- COL 1 -->
														<div class="col-md-4" >
															<!-- LIEU D'INTERVENTION ET CONTACT -->
															<div class="section-divider mb40" >
																<span>Lieu d'intervention</span>
															</div>
															<div class="row" >
																<div class="col-md-4" >	
																	<div class="section" >
																		 <label class="option">
																			<input type="radio" class="adr-ref" name="adr_ref" value="1" id="adr_client" <?php if($action["act_adr_ref"]==1) echo("checked");?> />
																			<span class="radio"></span>Client
																		</label>
																	</div>
																</div>
																<div class="col-md-4" >	
																	<div class="section" >
																		 <label class="option">
																			<input type="radio" class="adr-ref" name="adr_ref" value="2" id="adr_si2p" <?php if($action["act_adr_ref"]==2) echo("checked");?> />
																			<span class="radio"></span>Agence 
																		</label>
																	</div>
																</div>
																<div class="col-md-4" >	
																	<div class="section" >
																		 <label class="option">
																			<input type="radio" class="adr-ref" name="adr_ref" value="3" id="adr_autre" <?php if($action["act_adr_ref"]==3) echo("checked");?> />
																			<span class="radio"></span>Autres 
																		</label>
																	</div>
																</div>
															</div>	
															<div class="row cont-adr-client" <?php if($action["act_adr_ref"]!=1) echo("style='display:none;'"); ?> >
																<div class="col-md-12" >	
																	<div class="section" >
																		<label for="client" >Client :</label>
																		<select name="client" id="client" class="select2 get-client" data-adr_type="1"  data-adr_contact="0" >
																			<option value="0" >Clients ... </option>
																<?php		if(!empty($d_action_client)){
																				foreach($d_action_client as $cli){																						
																					if($action["act_adr_ref_id"]==$cli["cli_id"]){													
																						echo("<option value='" . $cli["cli_id"] . "' selected >" . $cli["cli_code"] . "-" . $cli["cli_nom"] . "</option>");
																					}else{
																						echo("<option value='" . $cli["cli_id"] . "' >" . $cli["cli_code"] . "-" . $cli["cli_nom"] . "</option>");
																					}
																				} 
																			} ?>	
																		</select>
																	</div>
																</div>
															</div>
															<div class="row cont-adr-client" <?php if($action["act_adr_ref"]!=1) echo("style='display:none;'"); ?> >
																<div class="col-md-12" >	
																	<div class="section" >
																		<label for="adresse" >Adresse :</label>
																		<select name="adresse" id="adresse" class="select2" >
																			<option value="0" >Selectionnez une adresse</option>
																<?php		if(!empty($d_adresses)){
																				foreach($d_adresses as $ad){		
																					$adr_nom="";
																					if(!empty($ad["adr_nom"])){
																						$adr_nom=$ad["adr_nom"];
																					}else{
																						$adr_nom=$ad["cli_nom"];
																					}
																					$adr_nom.=$ad["adr_cp"] . " " . $ad["adr_ville"];
																					if($action["act_adresse"]==$ad["adr_id"]){													
																						echo("<option value='" . $ad["adr_id"] . "' selected >" . $adr_nom . "</option>");
																					}else{
																						echo("<option value='" . $ad["adr_id"] . "' >" . $adr_nom . "</option>");
																					}
																				} 
																			}?>	
																		</select>
																	</div>
																</div>
															</div>
															<div class="cont-adresse" <?php if($action["act_adr_ref"]==2) echo("style='display:none;'"); ?> >
																<div class="row">
																	<div class="col-md-12" >	
																		<input type="text" name="adr_nom" id="adr_nom" class="gui-input nom champ_adresse adr-nom" placeholder="Nom" value="<?=$action["act_adr_nom"]?>" />	
																		<input type="text" name="adr_service" id="adr_service" class="gui-input champ_adresse adr-service" placeholder="Service" value="<?=$action["act_adr_service"]?>" />		
																		<input type="text" name="adr1" id="adr1" class="gui-input champ_adresse adr1" placeholder="Adresse" value="<?=$action["act_adr1"]?>" />		
																		<input type="text" name="adr2" id="adr2" class="gui-input champ_adresse adr2" placeholder="Complément 1" value="<?=$action["act_adr2"]?>" />		
																		<input type="text" name="adr3" id="adr3" class="gui-input champ_adresse adr3" placeholder="Complément 2" value="<?=$action["act_adr3"]?>" />		
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-3" >	
																		<input type="text" name="adr_cp" id="adr_cp" class="gui-input code-postal champ_adresse adr-cp" placeholder="CP" value="<?=$action["act_adr_cp"]?>" />		
																	</div>
																	<div class="col-md-9" >	
																		<input type="text" name="adr_ville" id="adr_ville" class="gui-input nom champ_adresse adr-ville" placeholder="Ville" value="<?=$action["act_adr_ville"]?>" />		
																	</div>
																</div>
															</div>

															<!-- CONTACT -->
															<div class="cont-contact"  <?php if($action["act_adr_ref"]==2) echo("style='display:none;'"); ?> >
																<div class="section-divider" >
																	<span>Contact</span>
																</div>								
																<div class="row cont-contact-liste" >
																	<div class="col-md-8" >	
																		<div class="section" >
																			<label for="contact" >Contact :</label>
																			<select name="contact" id="contact" class="select2 contact" >
																				<option value="0" >Selectionnez un contact client</option>
																	<?php		if(!empty($d_contacts)){
																					foreach($d_contacts as $c){
																						if($action["act_contact"]==$c["con_id"]){													
																							echo("<option value='" . $c["con_id"] . "' selected >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
																						}else{
																							echo("<option value='" . $c["con_id"] . "' >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
																						}
																					} 
																				}?>																					
																			</select>
																		</div>
																	</div>
																	
																</div>								
																<div class="row">
																	<div class="col-md-6" >	
																		<input type="text" name="con_nom" id="con_nom" class="gui-input nom champ-contact" placeholder="Nom" value="<?=$action["act_con_nom"]?>" />	
																	</div>
																	<div class="col-md-6" >	
																		<input type="text" name="con_prenom" id="con_prenom" class="gui-input prenom champ-contact" placeholder="Prénom" value="<?=$action["act_con_prenom"]?>" />	
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-6" >	
																		<input type="text" name="con_tel" id="con_tel" class="gui-input telephone champ-contact" placeholder="Téléphone" value="<?=$action["act_con_tel"]?>" />		
																	</div>
																	<div class="col-md-6" >	
																		<input type="text" name="con_portable" id="con_portable" class="gui-input telephone champ-contact" placeholder="Portable" value="<?=$action["act_con_portable"]?>" />		
																	</div>
																</div>
															</div>
														</div>
														<!-- FIN COL 1 -->
														
														<!-- COL 2 -->
														<div class="col-md-8" >
														
															<!--<div class="row mb10" >
																<div class="col-md-4 text-right pt10" >Produit :</div>
																<div class="col-md-8" >
																	<select name="produit" id="produit" class="select2 produit" required >
																		<option value="0" >Selectionnez un produit ...</option>					
																	</select>
																</div>
															</div>-->
															
															<div class="row" >
																<div class="col-md-6" >
																	<label for="vehicule" >Véhicules :</label>																
																	<select name="vehicule" name="vehicule" class="select2" >
																		<option value="0" >&nbsp;</option>
																<?php	if(!empty($d_vehicules)){
																			foreach($d_vehicules as $veh){
																				if($veh["veh_id"]==$action["act_vehicule"]){
																					echo("<option value='" . $veh["veh_id"] . "' selected >" . $veh["veh_libelle"] . "</option>");
																				}else{
																					echo("<option value='" . $veh["veh_id"] . "' >" . $veh["veh_libelle"] . "</option>");
																				}
																			}
																		} ?>
																	</select>
																</div>
																<div class="col-md-6" >
																	<label for="salle" >Salle :</label>																
																	<select name="salle" name="salle" class="select2" >
																		<option value="0" >&nbsp;</option>
																<?php	if(!empty($d_salles)){
																			foreach($d_salles as $salle){
																				if($salle["sal_id"]==$action["act_salle"]){
																					echo("<option value='" . $salle["sal_id"] . "' selected >" . $salle["sal_nom"] . "</option>");
																				}else{
																					echo("<option value='" . $salle["sal_id"] . "' >" . $salle["sal_nom"] . "</option>");
																				}
																			}
																		} ?>
																	</select>
																</div>
															</div>
															
															<div class="row mt15" >
																<div class="col-md-5" >
																	<label for="" >Déclencher une alerte à J- :</label>
																	<input type="text" name="alerte_jour" class="gui-input" id="alerte_jour" placeholder="" value="<?=$action["act_alerte_jour"]?>" />
																</div>
																<div class="col-md-1 pt25" >jour(s)</div>
																<div class="col-md-6 pt25" >
																	<div class="option-group field">
																		<label class="option option-dark">
																			<input type="checkbox" name="alerte_ok" value="on">
																			<span class="checkbox"></span>Alerte traitée
																		</label>
																	</div>
																</div>
															</div>

															<div class="row mt15" >
															
																<div class="col-md-6 pt25" >
																	<div class="option-group field">
																		<label class="option option-dark">
																			<input type="checkbox" id="dossier_ok" name="dossier_ok" value="on" <?php if($action["act_dossier_ok"]==1) echo("checked"); ?>  >
																			<span class="checkbox"></span>Dossier formateur traité
																		</label>
																	</div>
																</div>																
																<div class="col-md-6" >
																	<label for="dossier_date" >Dossier traité le :</label>
																	<span  class="field prepend-icon">
															 <?php 		if($action["act_dossier_ok"]!=1){ ?>
																			<input type="text" id="dossier_date" name="dossier_date" class="gui-input datepicker" placeholder="" disabled />
															 <?php 		}else{ ?>
																			<input type="text" id="dossier_date" name="dossier_date" class="gui-input datepicker" placeholder="" value="<?php if(!empty($dossier_date)) echo($dossier_date->format("d/m/Y")); ?>" />
															 <?php 		} ?>
																		<span class="field-icon"><i
																			class="fa fa-calendar-o"></i>
																		</span>
																	</span>																	
																</div>
															</div>
															<div class="row mt15" >																
																<div class="col-md-6" >
																	<div class="option-group field">
																		<label class="option option-dark">
																			<input type="checkbox" name="registre" value="on" <?php if($action["act_registre"]==1) echo("checked"); ?> >
																			<span class="checkbox"></span>Registre présent
																		</label>
																	</div>
																</div>
																<div class="col-md-6" >
																	<div class="option-group field">
																		<label class="option option-dark">
																			<input type="checkbox" name="registre_signe" value="on" <?php if($action["act_registre_signe"]==1) echo("checked"); ?> >
																			<span class="checkbox"></span>Registre signé
																		</label>
																	</div>
																</div>																
															</div>
															
															<div class="row mt15" >
																<div class="col-md-6" >
																	<label for="planning_txt" >Texte planning :</label>
																	<input type="text" maxlength="50" name="planning_txt" class="gui-input" value="<?=$action["act_planning_txt"]?>" /> 
																</div>
															</div>
															
															
															<div class="row mt10" >
																<div class="col-md-4" >Commentaire :</div>
															</div>
															<div class="row" >
																<div class="col-md-12 text-center" >
																	<textarea class="gui-textarea" name="commentaire" placeholder="Commentaire sur la formation"><?=$action["act_commentaire"]?></textarea>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="section-divider mb40">
																		<span>Gestion des stagiaires</span>
																	</div>
																</div>
															</div>
															<div class="row mb10" >
																<div class="col-md-6 text-center" >		
																	<div class="radio-custom mb5">
																		<input type="radio" id="sta_formation" class="gest-sta" name="gest_sta" value="1" <?php if($action["act_gest_sta"]==1) echo("checked"); ?> />
																		<label for="sta_formation">Participe à toutes les sessions</label>
																	</div>
																</div>
																<div class="col-md-6 text-center" >		
																	<div class="radio-custom mb5">
																		<input type="radio" id="sta_session" class="gest-sta" name="gest_sta" value="2" <?php if($action["act_gest_sta"]==2) echo("checked"); ?> />
																		<label for="sta_session">Participe à une session</label>
																	</div>
																</div>
															</div>
													<?php	if($action["act_gest_sta"]==1){ ?>
																<div class="row mb10" id="alert_gest_sta" style="display:none;" >
																	<div class="col-md-12 alert alert-warning" >		
																		Attention : tous les stagiaires vont être inscrits sur la première session.																		
																	</div>
																</div>
													<?php	}
															if($action["act_pro_sous_famille"]==14){ ?>
																<div class="section-divider" >
																	<span>Spécifique SST</span>
																</div>
																<div class="row" >
																	<div class="col-md-4 text-right pt10" >Dossier FORPREV :</div>
																	<div class="col-md-8" >
																		<input type="text" maxlength="25" name="forprev_num" class="gui-input" value="<?=$action["act_forprev_num"]?>" />
																	</div>
																</div>
													<?php	}elseif($action["act_pro_sous_famille"]==1){ ?>
																<div class="section-divider" >
																	<span>Spécifique SSIAP</span>
																</div>
																<div class="row" >
																	<div class="col-md-4 text-right pt10" >Dossier SSIAP :</div>
																	<div class="col-md-8" >
																		<input type="text" maxlength="25" name="forprev_num" class="gui-input" value="<?=$action["act_forprev_num"]?>" />
																	</div>
																</div>
													<?php	}
															if($_SESSION["acces"]["acc_droits"][11] OR ($_SESSION["acces"]["acc_droits"][10] AND !$action["act_archive_ok"])){ ?>
																<div class="section-divider" >
																	<span>Archivage</span>
																</div>
																<div class="row" >
																	<div class="col-md-12" >																	
																		<div class="option-group field">
																			<label class="option option-dark">
																				<input type="checkbox" id="archive" name="act_archive" value="on" <?php if($action["act_archive"]==1) echo("checked"); ?> >
																				<span class="checkbox"></span> Archivé
																			</label>
																		</div>																		
																	</div>
																</div>															
														<?php	if(empty($action["act_archive"])){ ?>
																	<div class="row alert alert-warning mt10" id="alert_archive" style="display:none;" >
																		<div class="col-md-12" >	
																			Attention : les inscriptions des clients vont également être archivées.
																		</div>
																	</div>
														<?php	}
															} ?>													
														</div>																										
														<!-- FIN COL 2 -->
													</div>
												</div>
											</div>
										</div>
									</div>
								<div>
							</section>
						</section>
					</div>
					<footer id="content-footer" class="affix">
						<div class="row">
							<div class="col-xs-3 footer-left">
								<a href="action_voir.php?action=<?=$action_id?>&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
									Retour
								</a>
							</div>
							<div class="col-xs-6 footer-middle"></div>
							<div class="col-xs-3 footer-right">
								<button type="submit" class="btn btn-sm btn-success" >
									<i class="fa fa-save" ></i> Enregistrer
								</button>
							</div>
						</div>
					</footer>
				</form>

		<?php	
				include "includes/footer_script.inc.php"; ?>
				<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
				<script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>
				<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
				<script type="text/javascript" src="assets/js/custom.js"></script>				
				<script type="text/javascript">
				
					var gest_sta=parseInt("<?=$action["act_gest_sta"]?>");
					
					var tab_adresse=new Array();
					tab_adresse[0]=new Array(9);
			<?php	foreach($d_adresses as $ad){ ?>
						tab_adresse[<?=$ad["adr_id"]?>]=new Array(9);
						tab_adresse[<?=$ad["adr_id"]?>]["nom"]="<?=$ad["adr_nom"]?>";
						tab_adresse[<?=$ad["adr_id"]?>]["service"]="<?=$ad["adr_service"]?>";
						tab_adresse[<?=$ad["adr_id"]?>]["ad1"]="<?=$ad["adr_ad1"]?>";
						tab_adresse[<?=$ad["adr_id"]?>]["ad2"]="<?=$ad["adr_ad2"]?>";
						tab_adresse[<?=$ad["adr_id"]?>]["ad3"]="<?=$ad["adr_ad3"]?>";
						tab_adresse[<?=$ad["adr_id"]?>]["cp"]="<?=$ad["adr_cp"]?>";
						tab_adresse[<?=$ad["adr_id"]?>]["ville"]="<?=$ad["adr_ville"]?>";
			<?php	} ?>
			
					var tab_contact=new Array(3);
					tab_contact[0]=new Array(3);
			<?php	if(!empty($d_contacts)){
						foreach($d_contacts as $con){ ?>
							tab_contact[<?=$con["con_id"]?>]=new Array(3);
							tab_contact[<?=$con["con_id"]?>]["nom"]="<?=$con["con_nom"]?>";
							tab_contact[<?=$con["con_id"]?>]["prenom"]="<?=$con["con_prenom"]?>";
							tab_contact[<?=$con["con_id"]?>]["tel"]="<?=$con["con_tel"]?>";
							tab_contact[<?=$con["con_id"]?>]["portable"]="<?=$con["con_portable"]?>";					
			<?php		} 
					} ?>
						
					jQuery(document).ready(function (){
						
						$(".adr-ref").change(function(){
							var type_adresse=$('input[name=adr_ref]:checked').val();
							affichage_adresse(type_adresse);
						});
						
						$("#dossier_ok").click(function(){
							if($(this).is(":checked")){
								$("#dossier_date").prop("disabled",false);
								$("#dossier_date").datepicker('setDate', '00/00/0000');
							}else{
								$("#dossier_date").prop("disabled",true);
								
							}
							
																
						});			
						
						$("#adresse").change(function(){
							adresse=$(this).val();
							client=$("#client").val();
							afficher_adresse(adresse);
							if(client>0){
								// si adresse on prend contact associé sinon tous les contacts
								get_contacts(1,client,adresse,0,actualiser_contacts,"","");
							}
						
						});
						
						$("#contact").change(function(){					
							afficher_contact($(this).val());					
						});
						
						if(gest_sta==1){							
							$(".gest-sta").click(function(){
								if($("#sta_formation").is(":checked")){
									$("#alert_gest_sta").hide();	
								}else{
									$("#alert_gest_sta").show();	
								}
							});
						}
						
						if($("#alert_archive").length>0){
							$("#archive").click(function(){							
								if($(this).is(":checked")){								
									$("#alert_archive").show();
								}else{
									$("#alert_archive").hide();
								}
							});
						}
						
						
						// ----- FIN DOC READY -----
					});
					
					function affichage_adresse(type_adresse){
						switch(type_adresse){
							case "1":
								// client
								$(".cont-adr-client").show();
								$(".cont-adresse").show();	
								$(".cont-contact").show();			
								$(".cont-contact-liste").show();								
								break;
							case "2":
								// agence
								$(".cont-adr-client").hide();
								$(".cont-adresse").hide();		
								$(".cont-contact").hide();			
								$(".cont-contact-liste").hide();								
								break;
								
							default:
								// autre
								$(".cont-adr-client").hide();
								$(".cont-adresse").show();	
								$(".cont-contact").show();			
								$(".cont-contact-liste").hide();								
								break;
						}
					}
					
					function actualiser_get_client(json){
						if(json){
							if(json.erreur_txt!=""){
								afficher_message("Erreur","danger",json.erreur_txt);
							}else{	
								
								// on memorie les adresses
								if(json["adr_int"]!=""){
									$.each(json["adr_int"], function(i, ad){
										tab_adresse[ad.id]=new Array(8);
										tab_adresse[ad.id]["nom"]=ad.nom;
										tab_adresse[ad.id]["service"]=ad.service;
										tab_adresse[ad.id]["ad1"]=ad.ad1;
										tab_adresse[ad.id]["ad2"]=ad.ad2;
										tab_adresse[ad.id]["ad3"]=ad.ad3;
										tab_adresse[ad.id]["cp"]=ad.cp;
										tab_adresse[ad.id]["ville"]=ad.ville;
									});
								}
								actualiser_select2(json.adr_int,"#adresse",json.int_defaut);
							}
						}else{
							// PAS DE CLIENT
							actualiser_select2("","#adresse",0);
							$("#adresse").val("").trigger("change");
							actualiser_select2("","#contact",0);
							$("#contact").val("").trigger("change");
							
						}
					}
					
					function afficher_adresse(adresse){
						console.log("afficher_adresse()");
						console.log("adresse :" + adresse);
						console.log("tab_adresse :" + tab_adresse[adresse]);
						
						if(adresse>0){						
							console.log("MAJ");
							//console.log(tab_adresse);
							//console.log(tab_adresse[adresse]["nom"]);
							$("#adr_nom").val(tab_adresse[adresse]["nom"]);
							$("#adr_service").val(tab_adresse[adresse]["service"]);
							$("#adr1").val(tab_adresse[adresse]["ad1"]);
							$("#adr2").val(tab_adresse[adresse]["ad2"]);
							$("#adr3").val(tab_adresse[adresse]["ad3"]);
							$("#adr_cp").val(tab_adresse[adresse]["cp"]);
							$("#adr_ville").val(tab_adresse[adresse]["ville"]);							
						}else{
							$(".champ-adresse").val("");						
						}
					}
				
					// traitement suite à la recuperation des contacts liés à l'adresse 
					function actualiser_contacts(json){					
						if(json){						
							// on memorie les conatcts						
							var con_defaut=0;
							tab_contact=new Array();
							$.each(json, function(i, con){
								tab_contact[con.id]=new Array();
								tab_contact[con.id]["nom"]=con.nom;
								tab_contact[con.id]["prenom"]=con.prenom;
								tab_contact[con.id]["tel"]=con.tel;
								tab_contact[con.id]["portable"]=con.portable;
								tab_contact[con.id]["mail"]=con.mail;
								if(con.defaut==1){
									 con_defaut=con.id;
								}
							});	
							// actualisation du select contact
							actualiser_select2(json,"#contact",con_defaut);
							if(con_defaut==0){
								// securite si 0 pas #contact.change
								$("#contact").val(0).trigger("change");
							}
						}else{
							actualiser_select2("","#contact",0);
							$("#contact").val(0).trigger("change");
						}
					}
				
					function afficher_contact(contact){
						if(contact>0){
							$("#con_nom").val(tab_contact[contact]["nom"]);
							$("#con_prenom").val(tab_contact[contact]["prenom"]);
							$("#con_tel").val(tab_contact[contact]["tel"]);
							$("#con_portable").val(tab_contact[contact]["portable"]);						
						}else{
							$(".champ-contact").val("");							
						}
					}
				</script>
			</body>
		</html>
<?php
	} ?>