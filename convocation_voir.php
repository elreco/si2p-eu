<?php 

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_convocation_data.php');

include('modeles/mod_get_juridique.php');
include('modeles/mod_planning_periode_lib.php');




// VISUALISATION D'UNE CONVOCATIONS

$erreur_txt="";

$action_id=0;
if(!empty($_GET["action"])){
	$action_id=intval($_GET["action"]);
}

if(empty($action_id)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();
}else{

	$stagiaire_id=0;
	if(!empty($_GET["stagiaire"])){
		$stagiaire_id=intval($_GET["stagiaire"]);
	}
	
	$action_client_id=0;
	if(!empty($_GET["action_client"])){
		$action_client_id=intval($_GET["action_client"]);
	}
	
	// personne connecte
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}
	if($acc_societe!=$conn_soc_id){
		$acc_societe=$conn_soc_id;
	}
	
	if(!empty($stagiaire_id)){

		$sql="SELECT ast_action_client FROM Actions_Stagiaires WHERE ast_action=:action_id AND ast_stagiaire=:stagiaire_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam("action_id",$action_id);
		$req->bindParam("stagiaire_id",$stagiaire_id);
		$req->execute();
		$d_action_sta=$req->fetch();
		if(empty($d_action_sta)){
			$erreur_txt="impossible d'afficher la page";
			echo($erreur_txt);
			die();
		}
		
		$data=convocation_data($d_action_sta["ast_action_client"],$stagiaire_id);
	}else{
		$data=convocation_data($action_client_id,0);
	}
	if(empty($data)){
		$erreur_txt="impossible d'afficher la page";
		echo($erreur_txt);
		die();
	}
} ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#container_print{
				width:21cm;
			}
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm">
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">	
		<?php 		if(empty($erreur_txt)){ ?>
		
							<div class="row" >
							
								<!-- zone d'affichage du devis -->
								
								<div class="col-md-8"  >	
								
									<div id="container_print" >
								
										<div id="page_print" >
								<?php		foreach($data["pages"] as $p => $page){ ?>
								
												<div class="page" id="header_<?=$p?>" data-page="<?=$p?>" >
													<header>						
														<table>
															<tr>
																<td class="w-20" >
														<?php		if(!empty($data["juridique"]["logo_1"])){ ?>
																		<img src="../<?=$data["juridique"]["logo_1"]?>" />
														<?php		}else{
																		echo("&nbsp;");
																	} ?>
																</td>
																<td class="w-60" >&nbsp;</td>
																<td class="w-20 text-right" >
														<?php		if(!empty($data["juridique"]["logo_2"])){ ?>
																		<img src="../<?=$data["juridique"]["logo_2"]?>" />
														<?php		}else{
																		echo("&nbsp;");
																	} ?>
																</td>
															</tr>													
														</table>														
													</header>
												</div>
												<div class="ligne-body-<?=$p?>" >
													<table>
														<tr>
															<td class="w-50 tab-label" >												
														<?php	echo("<b>" . $data["juridique"]["nom"] . "</b>");
																if(!empty($data["juridique"]["agence"])){
																	echo("<br/><b>" . $data["juridique"]["agence"] . "</b>");
																}
																if(!empty($data["juridique"]["ad1"])){
																	echo("<br/><b>" . $data["juridique"]["ad1"] . "</b>");
																}
																if(!empty($data["juridique"]["ad2"])){
																	echo("<br/><b>" . $data["juridique"]["ad2"] . "</b>");
																}
																if(!empty($data["juridique"]["ad3"])){
																	echo("<br/><b>" . $data["juridique"]["ad3"] . "</b>");
																}
																echo("<br/><b>" . $data["juridique"]["cp"] . " " . $data["juridique"]["ville"] . "</b>");
																echo("<br/>Tél :" . $data["juridique"]["tel"]);
																if(!empty($data["juridique"]["fax"])){
																	echo("<br/>Fax " . $data["juridique"]["fax"]);
																} ?>
															</td>
															<td class="w-50" style="padding-top:100px;" >
														<?php	
															if(!empty($data["adresse_client"])){
																echo($data["adresse_client"]); 
															}
															echo("<br/><br/>");
															echo("le " . utf8_encode($data["date_doc"])); ?>
															</td>
														</tr>
													</table>
													
													<h1 class="text-center text-strong" >CONVOCATION de <?=$page["sta_prenom"] . " " . $page["sta_nom"]?></h1>
													
													<p>Madame, Monsieur,</p>

													<p>Nous avons le plaisir de vous convier à la session de formation :</p>
													
													<p class="text-center text-strong" ><?=$data["acl_pro_libelle"]?></p>
													<p>
														Cette formation débutera le <?=utf8_encode($page["date_deb"])?> et se terminera le <?=utf8_encode($page["date_fin"])?>, pour une durée de <?=$page["duree"]?> au total. 
														Le planning détaillé de vos journées est décrit ci-dessous.
													</p>
													<table>
														<tr>
															<th class="w-33 text-right" >Planning : </th>
															<td><?=$page["dates"]?></td>
														</tr>
														<tr>
															<th class="w-33 text-right" >Merci d'être présent le : </th>
															<td><?=utf8_encode($page["date_deb"])?> à <?=$page["heure_deb"]?></td>
														</tr>
														<tr>
															<th class="w-33 text-right tab-label" >à l'adresse suivante :</th>
															<td ><?=$data["lieu"]?></td>
														</tr>
														<tr>
															<th class="w-33 text-right" >Horaires : </th>
															<td><?=$page["horaire"]?></td>
														</tr>
														
													</table>
													<p>
															
															<strong>Informations complémentaires : </strong>
															<?=$data["reference_sta"]?>
															
														</p>
													
												</div>
												<div id="foot_<?=$p?>" class="foot" >												
													<table>
														<tr>
															<td class="w-75 va-t" >
																<p>
																	Restant à votre entière disposition, nous vous prions d'agréer, Madame, Monsieur, nos respectueuses salutations. 
																</p>
															</td>
															<td style="padding-bottom:50px;" >
														<?php	if(!empty($data["juridique"]["resp_ident"])){
																	echo($data["juridique"]["resp_ident"]);
																}
																if(!empty($data["juridique"]["resp_fonction"])){
																	echo("<br/>" . $data["juridique"]["resp_fonction"]);
																}
																if(!empty($data["juridique"]["resp_signature"])){ 
																	echo("<br/>"); ?>
																	<img src="../<?=$data["juridique"]["resp_signature"]?>" />
														<?php	}?>
															</td>
														</tr>
													</table>
												</div>
												<div id="footer_<?=$p?>" >
													<footer>
														<table>
															<tr>
																<td class="w-15" >
														<?php		if(!empty($data["juridique"]["qualite_1"])){ ?>
																		<img src="../<?=$data["juridique"]["qualite_1"]?>" class="img-responsive" />
														<?php		}else{
																		echo("&nbsp;");
																	} ?>
																</td>
																<td class="w-70 text-center" ><?=$data["juridique"]["footer"]?></td>
																<td class="w-15 text-right" >
														<?php		if(!empty($data["juridique"]["qualite_2"])){ ?>
																		<img src="../<?=$data["juridique"]["qualite_2"]?>" />
														<?php		}else{
																		echo("&nbsp;");
																	} ?>
																</td>
															</tr>
														</table>
													</footer>
												</div>											
								<?php 			if($p<count($data["pages"])-1){
													echo("<div class='saut-page' ></div>");
												}									
											} ?>

										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->
								
								<div class="col-md-4" >
									<!-- DEBUT ANNEXE -->
									
									<!-- FIN ANNEXE -->
								</div>
								
							</div>
						

		<?php		}else{ ?>
						
						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>
					
		<?php		} ?>
									
				</section>
				
				<!-- End: Content -->
			</section>
			
			
		</div>
		<!-- End: Main -->
		
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >	
			<?php	if(!empty($stagiaire_id)){ ?>
						<a href="action_voir_sta.php?action=<?=$data["action"]?>&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
							Retour
						</a>
			<?php	}else{ ?>
						<a href="action_voir.php?action=<?=$data["action"]?>&onglet=3&societ=<?=$conn_soc_id?>" class="btn btn-default btn-sm" >
							Retour
						</a>
			<?php	} ?>			
				</div>
				<div class="col-xs-6 footer-middle text-center" >
					<button type="button" class="btn btn-sm btn-info ml15" id="print" >
						<i class="fa fa-print"></i> Imprimer
					</button>
				</div>
				<div class="col-xs-3 footer-right" ></div>
			</div>
		</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript" src="assets/js/print.js"></script>
		<script type="text/javascript">
			var l_page=21;
			var h_page=28;
			jQuery(document).ready(function () {				
				
			});
		</script>
	</body>
</html>
