<?php
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
include "modeles/mod_famille.php";

$fonction_id=0;
if(!empty($_GET['id'])){

	$fonction_id=intval($_GET['id']);
    // SI C'EST UNE EDITION
    $req = $Conn->prepare("SELECT * FROM modeles_fonctions WHERE mfo_id = " . $fonction_id);
    $req->execute();
    $fonction = $req->fetch();
}

// RECUP LES PROFILS ET LEURS VALEURS D'AFFECTATION 
$req = $Conn->prepare("SELECT pro_id,pro_libelle,mfp_profil FROM profils LEFT OUTER JOIN Modeles_Fonctions_Profils 
ON (profils.pro_id=Modeles_Fonctions_Profils.mfp_profil AND mfp_fonction=:fonction)");
$req->bindParam(":fonction",$fonction_id);
$req->execute();
$profils = $req->fetchAll();

$menus = array(
	1 => "Commerce",
    2 => "Formations/Planning",
    3 => "RH",
    4 => "Technique",
    5 => "Compta"
);

$directory = "includes/modeles_fonctions/";
$scanned_directory = array_diff(scandir($directory), array('..', '.'));


?>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Paramètres</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <!--   <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">-->

    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<form action="modeles_fonctions_enr.php" method="POST" id="admin-form">
<body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
    <div id="main">
        <?php
        include "includes/header_def.inc.php";
        ?>


        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper" class="">
            
            <section id="content" class="animated fadeIn">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="admin-form theme-primary ">
                            <div class="panel heading-border panel-primary">
                                <div class="panel-body bg-light">
                                    <div class="text-center">

                                        <div class="content-header">
                                            <?php if(!empty($_GET['id'])){ ?>
                                                <h2>Modifier une <b class="text-primary">fonction</b></h2>
                                            <?php }else{ ?>
                                                <h2>Ajouter une <b class="text-primary">fonction</b></h2>
                                            <?php } ?>
                                        </div>

                                        <div class="col-md-10 col-md-offset-1">

                                                <?php if (isset($_GET['id'])): ?>
                                                    <input type="hidden" name="mfo_id" value="<?=$fonction['mfo_id']?>"></input>
                                                <?php endif;?>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label  class="text-left mb5">Nom de la fonction</label>
                                                        <div class="section">
                                                            
                                                            <input type="text" name="mfo_nom" class="gui-input" id="mfo_nom" placeholder="Nom de la fonction" 
                                                            <?php if(!empty($fonction)){ ?>
                                                                value="<?= $fonction['mfo_nom'] ?>"
                                                            <?php } ?>
                                                            >
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="section">
                                                            <label class="text-left mb5">Nom de l'include (A mettre dans "includes/modeles_fonctions/")</label>
                                                            <div class="field select">
                                                                
                                                                <select name="mfo_include" id="mfo_include" required="">
															<?php 	foreach($scanned_directory as $s){ 
																		if(substr($s,-4)==".php"){ ?>
																			<option value="<?= $s ?>"
                                                                    <?php 		if(!empty($fonction) && $fonction['mfo_include'] == $s){ ?>
																					selected
                                                                    <?php 		} ?>
																			>
																			<?=$s?>
																			</option>
																<?php 	}
																	} ?>
                                                                </select>
                                                                <i class="arrow simple"></i>
                                                            </div>
                                                        </div>
                                                    </div>                                      
                                                </div>
												<div class="row">
                                                    <div class="col-md-6">
                                                        <div class="section">
                                                            <label class="text-left mb5">Menu associé :</label>
                                                            <div class="field select">
                                                                
                                                                <select name="mfo_menu" id="mfo_menu" required="">
                                                                <?php foreach($menus as $k=>$m){ ?>
                                                                    <option value="<?= $k ?>"
                                                                    <?php if(!empty($fonction['mfo_menu']) && $fonction['mfo_menu'] == $k){ ?>
                                                                        selected
                                                                    <?php } ?>
                                                                    ><?= $m ?></option>
                                                                <?php } ?>
                                                                </select>
                                                                <i class="arrow simple"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="section-divider mt40 mb25" id="spy5">
                                                    <span>Limité aux profils</span>
                                                </div>
                                                <div class="row">
                                                    <div class="option-group field section">
                                            <?php 		foreach($profils as $p){ ?>
															<div class="col-md-3 text-left">
																<label class="block mt15 option option-primary">
																	<input type="checkbox" name="pro_id_<?=$p['pro_id']?>" value="<?= $p['pro_id'] ?>" 
                                                        <?php 		if(!empty($p['mfp_profil'])){ 
																		echo("checked");
																	} ?> 
																	>
																	<span class="checkbox"></span><?=$p['pro_libelle'] ?>
																</label>
                                                        </div>
                                                    <?php } ?>  
                                                    </div>  
                                                </div>
                                                
                                            </div>
                                        </div>

                                  </div>
                              </div>


                          </div>
                      </div>

                  </div>
              </div>

          </div>
      </section>
      <!-- End: Content -->
  </section>
</div>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left" >
            <a href="modeles_fonctions_liste.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle" ></div>
        <div class="col-xs-3 footer-right" >
            <button type="submit" class="btn btn-success btn-sm">
                <i class='fa fa-save'></i> Enregistrer
            </button>
        </div>
    </div>
</footer>
<?php
    include "includes/footer_script.inc.php"; ?>    

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>

</body>
</form>
</html>
