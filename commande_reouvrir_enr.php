<?php 
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');
include('modeles/mod_add_notification.php');

if(isset($_POST)){ 

	$erreur="";

	$req = $Conn->prepare("SELECT * FROM commandes WHERE com_id = :com_id");
	$req->bindParam(':com_id', $_POST['commande']);
	$req->execute();
	$commande = $req->fetch();
	$commande["com_numero"]= str_replace("BC","DA",$commande["com_numero"]);

	$req = $Conn->prepare("INSERT INTO commandes_histo (chi_commande, chi_type, chi_txt, chi_date, chi_utilisateur) VALUES (:chi_commande, :chi_type, :chi_txt, NOW(), :chi_utilisateur)");
	$req->bindParam(':chi_commande', $_POST['commande']);
	$req->bindParam(':chi_type', $_POST['chi_type']);
	$req->bindParam(':chi_txt', $_POST['chi_txt']);
	$req->bindParam(':chi_utilisateur', $_SESSION['acces']['acc_ref_id']);
	$req->execute();

	$req = $Conn->prepare("DELETE FROM commandes_validations WHERE cva_commande = :cva_commande");
	$req->bindParam(':cva_commande', $_POST['commande']);
	$req->execute();

	$req = $Conn->prepare("UPDATE commandes SET com_etat = 0, com_numero='" . $commande["com_numero"] . "' WHERE com_id = :com_id");
	$req->bindParam(':com_id', $_POST['commande']);
	$req->execute();

	
	// le bc est lié à une action
	if(!empty($commande["com_action"])){

		if(!empty($commande["com_societe"])){

			// le bc peux etre validé sans être sur la société de l'action

			$ConnFct=connexion_fct($commande["com_societe"]);

			$req = $ConnFct->prepare("UPDATE Actions SET 
			act_verrou_admin=1,
			act_verrou_bc=1,
			act_marge_ca=null,
			act_verrou_marge=null
			WHERE act_id=:action;");
			$req->bindParam("action",$commande["com_action"]);
			try{
				$req->execute();
			}Catch (Exception $e){
				$erreur="Echec du calcul de marge.<br/>" . $e->getMessage();
			}

			if(empty($erreur)){

				$req = $ConnFct->prepare("UPDATE Plannings_Dates SET pda_alert=1
				WHERE pda_type=1 AND pda_ref_1=:action;");
				$req->bindParam("action",$commande["com_action"]);
				try{
					$req->execute();
				}Catch (Exception $e){
					$erreur="Blocage administratif. Les dates de formations n'ont pas été actualisées.<br/>" . $e->getMessage();
				}

			}

		
		}else{

			$erreur="Impossible d'identifier l'action liée à la commande. Le blocage administratif n'a pas été actualisé.";

		}

	}

	if (file_exists("documents/commandes/" . $commande['com_numero'] . ".pdf")) {
		unlink("documents/commandes/" . $commande['com_numero'] . ".pdf");
	}

	// notifications
	add_notifications("<i class='fa fa-truck'></i>","Votre bon de commande " . $commande['com_numero'] . " a été réouvert","bg-info","commande_voir.php?commande=" . $_POST['commande'],"","",$commande['com_donneur_ordre'],0,0,0);
	// fin notifications

	if(!empty($erreur)){
		$_SESSION['message'] = array(
			"aff" => "modal",
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur
		);
	}

	
	Header('Location: commande_voir.php?commande=' . $_POST['commande'] . "&succes=3");

}

?>