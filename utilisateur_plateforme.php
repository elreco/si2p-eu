<?php

include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'modeles/mod_platform.php';

$req=$Conn->prepare("SELECT * FROM Utilisateurs
WHERE uti_id = :uti_id;");
$req->bindParam(":uti_id",$_GET['id']);
$req->execute();
$utilisateur=$req->fetch();

$array = [
    'lastname' => $utilisateur['uti_nom'],
    'firstname' => $utilisateur['uti_prenom'],
    'email' => $utilisateur['uti_mail'],
    'role' => 'Utilisateur'
];

if (!empty($utilisateur['uti_tel'])) {
    $array['tel'] = $utilisateur['uti_tel'];
}

$utilisateur_plateforme = plateformCall($array, '/users', 'POST');

if (!empty($utilisateur_plateforme['data'])) {
    $array = [
        'source_id' => 1,
        'tablename' => 'users',
        'tableid' => $utilisateur_plateforme['data']['id'],
        'extname' => 'Utilisateurs',
        'extid' => $utilisateur['uti_id'],
    ];

    plateformCall($array, '/table-sources');

    $_SESSION['message'][] = array(
        "titre" => "Succès",
        "type" => "success",
        "message" => "L'utilisateur a bien été transféré"
    );
} else {

    $_SESSION['message'][] = array(
        "titre" => "Attention",
        "type" => "success",
        "message" => "L'utilisateur a été actualisé"
    );
    unset($array['role']);

    $tableSource = plateformCall($array, '/table-sources/1/utilisateurs/' . $utilisateur['uti_id'], 'GET');

    if (!empty($tableSource['data'])) {
        $utilisateur_plateforme = plateformCall($array, '/users/' . $tableSource['data']['tableid'], 'GET');
        if (!empty($utilisateur_plateforme['data'])) {
            $update = plateformCall($array, '/users/' . $utilisateur_plateforme['data']['id'], 'PATCH');
        }
    }
}
$adresse="utilisateur_voir.php?utilisateur=" . $utilisateur['uti_id'];
header("location : " . $adresse);