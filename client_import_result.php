<?php
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');
include('modeles/mod_parametre.php');
include('modeles/mod_upload.php');

$erreur="";
$erreur_import="";

$rapport_erreur="";

$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
  $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
  $acc_agence=$_SESSION['acces']["acc_agence"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
	$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
}

if($_SESSION['acces']["acc_profil"]!=13){
	echo("Accès refusé!");
	die();
}

// TRAITEMNT DU FORM
$maison_mere=0;
if(!empty($_POST["client"])){
	$maison_mere=intval($_POST["client"]);
}

$nom="";
if(!empty($_POST["nom"])){
	$nom=$_POST["nom"];
}

$code="";
if(!empty($_POST["code"])){
	$code=$_POST["code"];
}else{
	$code=$nom;
}

$fil_intervention_mm=0;
if(!empty($_POST["fil_intervention_mm"])){
	$fil_intervention_mm=1;
}

if(empty($maison_mere) OR empty($nom)){
	echo("Formulaire incomplet!");
	die();
}elseif($acc_societe!=4){
	echo("Vous devez être connecté sur GFC pour accéder à cette fonctionalité");
	die();
}




// SUR LE CLIENT

$sql="SELECT * FROM Clients,Clients_Societes WHERE cli_id=cso_client AND cso_societe=4 AND cso_agence=4 AND cli_groupe AND cli_categorie=2 AND cli_id=:client;";
$req=$Conn->prepare($sql);
$req->bindParam(":client",$maison_mere);
$req->execute();
$d_client=$req->fetch();
if(!empty($d_client)){
	
	// obligatoirement un groupe
	
		if(empty($d_client["cli_filiale_de"])){
			// on rattache à la MM
			$cli_filiale_de=$maison_mere;	
			$cli_niveau=0;
			$cli_fil_de=$maison_mere;
		}else{
			$cli_filiale_de=$d_client["cli_filiale_de"];	
			$cli_niveau=$d_client["cli_niveau"]+1;
			$cli_fil_de=$maison_mere;
		}
	
}else{
	$erreur="L'entité de rattachement n'a pas pu être chargée.";
}

if(empty($erreur)){
	
	
		
	$dossier="temp/";
	if(!file_exists("documents/" . $dossier)){
		mkdir("documents/" . $dossier, 0777, true);
	}
	
	$extension=array("csv");
	
	$taille=2097152999999;
	
	$result_upload=upload("fichier",$dossier,$nom,$taille,$extension,1);
	if($result_upload==0){
		
		$row=0;
			
		$src=$_SERVER["DOCUMENT_ROOT"]. "/documents/temp/" . $nom . ".csv";
		$handle = fopen($src, "r");
		if ($handle!== FALSE){
			
			//********************************
			// 	REQUETE PREPARE
			//********************************
			
			// doublon code client
			$sql_check_code="SELECT cli_id FROM Clients WHERE cli_code=:cli_code;";
			$req_check_code=$Conn->prepare($sql_check_code);
			
		
			// doublon contact
			$sql_check_contact="SELECT con_id FROM Contacts WHERE con_mail=:con_mail;";
			$req_check_contact=$Conn->prepare($sql_check_contact);
			
			//doublon siret
	
			$sql_check_siret="SELECT cli_id FROM Clients INNER JOIN Adresses ON (Clients.cli_id=Adresses.adr_ref_id AND adr_ref=1) WHERE cli_siren=:cli_siren AND adr_siret=:adr_nic;";
			$req_check_siret=$Conn->prepare($sql_check_siret);
			
			
			// INSERT
			
			// client
			$sql_add_cli="INSERT INTO Clients (cli_code,cli_nom ,cli_categorie,cli_sous_categorie,cli_groupe,cli_filiale_de,cli_niveau,cli_fil_de
			,cli_classification,cli_classification_type,cli_classification_categorie 
			,cli_reg_type,cli_reg_formule,cli_reg_fdm,cli_reg_nb_jour ,cli_siren ,cli_ape,cli_opca,cli_opca_type,cli_facture_opca,cli_releve,cli_fac_groupe
			,cli_affacturable,cli_reference
			,cli_date_creation,cli_uti_creation) 
			VALUES (:cli_code,:cli_nom ,:cli_categorie,:cli_sous_categorie,:cli_groupe,:cli_filiale_de,:cli_niveau,:cli_fil_de
			,:cli_classification,:cli_classification_type,:cli_classification_categorie 
			,:cli_reg_type,:cli_reg_formule,:cli_reg_fdm,:cli_reg_nb_jour ,:cli_siren,:cli_ape,:cli_opca,:cli_opca_type,:cli_facture_opca,:cli_releve,:cli_fac_groupe 
			,:cli_affacturable,:cli_reference
			,NOW(),:cli_uti_creation)";
			$req_add_cli=$Conn->prepare($sql_add_cli);
			
			// contact
			
			$sql_add_con="INSERT INTO Contacts (con_ref,con_ref_id,con_fonction_nom,con_titre,con_prenom,con_nom,con_tel,con_portable,con_fax,con_mail,con_compta)
			VALUES (1,:con_ref_id,:con_fonction_nom,:con_titre,:con_prenom,:con_nom,:con_tel,:con_portable,:con_fax,:con_mail,:con_compta);";
			$req_add_con=$Conn->prepare($sql_add_con);
			
			// adresse
			
			$sql_add_adr="INSERT INTO Adresses (adr_ref,adr_ref_id,adr_type,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_defaut,adr_siret,adr_geo)
			VALUES (1,:adr_ref_id,:adr_type,:adr_nom,:adr_service,:adr_ad1,:adr_ad2,:adr_ad3,:adr_cp,:adr_ville,:adr_defaut,:adr_siret,1);";
			$req_add_adr=$Conn->prepare($sql_add_adr);
			
			// lien adresse contact
			
			$sql_con_adr="INSERT INTO adresses_contacts (aco_contact,aco_adresse,aco_defaut) VALUES (:aco_contact,:aco_adresse,:aco_defaut);";
			$req_con_adr=$Conn->prepare($sql_con_adr);
			
			// client societe
			
			$sql_add_cli_soc="INSERT INTO Clients_Societes (cso_client,cso_societe,cso_agence,cso_commercial,cso_utilisateur,cso_archive,cso_rib,cso_rib_change,cso_aff_courrier)
			VALUES (:cso_client,:cso_societe,:cso_agence,:cso_commercial,:cso_utilisateur,:cso_archive,:cso_rib,:cso_rib_change,:cso_aff_courrier);";
			$req_add_cli_soc=$Conn->prepare($sql_add_cli_soc);
			
			// client en base N
			
			$sql_add_cli_n="INSERT INTO Clients (cli_id,cli_agence,cli_code ,cli_nom ,cli_commercial,cli_utilisateur,cli_categorie ,cli_groupe
			,cli_filiale_de,cli_sous_categorie,cli_rib,cli_rib_change,cli_cp,cli_fac_groupe,cli_affacturable)
			VALUES (:cli_id,:cli_agence ,:cli_code,:cli_nom,:cli_commercial,:cli_utilisateur,:cli_categorie,:cli_groupe
			,:cli_filiale_de,:cli_sous_categorie,:cli_rib,:cli_rib_change,:cli_cp,:cli_fac_groupe,:cli_affacturable);";
			$req_add_cli_n=$ConnSoc->prepare($sql_add_cli_n);
			
			
			// UPDATE
			
			// fiche client
			
			$sql_up_cli="UPDATE Clients SET cli_adresse=:cli_adresse,cli_adr_nom=:cli_adr_nom,cli_adr_service=:cli_adr_service
			,cli_adr_ad1=:cli_adr_ad1,cli_adr_ad2=:cli_adr_ad2,cli_adr_ad3 =:cli_adr_ad3,cli_adr_cp=:cli_adr_cp,cli_adr_ville =:cli_adr_ville
			,cli_contact =:cli_contact,cli_con_fct =:cli_con_fct ,cli_con_fct_nom =:cli_con_fct_nom 
			,cli_con_titre =:cli_con_titre,cli_con_nom=:cli_con_nom,cli_con_prenom=:cli_con_prenom,cli_con_tel=:cli_con_tel,cli_con_mail=:cli_con_mail
			WHERE cli_id=:client;";
			$req_up_cli=$Conn->prepare($sql_up_cli);
			
			
			//********************************
			// 	DONNEE POUR IMPORT
			//********************************
			
				// COMMERCIAUX PAR DEFAUT
				
				$d_com_def=array(
					"2-2" => 199,
					"2-10" => 197,
					"3-3" => 200,
					"3-9" => 197,
					"5-5" => 200,
					"5-11" => 197,
					"7-0" => 176,
					"8-0" => 202	
				);
				
				// LES RIB PAR DEFAUT
				
				$d_rib=array();
				
				$sql="SELECT rib_id,rib_societe FROM Rib WHERE rib_defaut ORDER BY rib_id;";
				$req=$Conn->query($sql);
				$d_result_rib=$req->fetchAll();
				if(!empty($d_result_rib)){
					foreach($d_result_rib as $r){
						$d_rib[$r["rib_societe"]]=$r["rib_id"];
					}
					
				}
				
				// LES SOCIETES PAR DEPARTEMENTS
				
				$d_soc_dep=array();
				
				$sql="SELECT sde_departement,sde_societe,sde_agence FROM Societes_Departements WHERE sde_gc ORDER BY sde_departement;";
				$req=$Conn->query($sql);
				$d_result_dep=$req->fetchAll();
				if(!empty($d_result_dep)){
					foreach($d_result_dep as $rd){
						$d_soc_dep[$rd["sde_departement"]]=array(
							"societe" => $rd["sde_societe"],
							"agence" => $rd["sde_agence"]
						);
					}
				}
				
				// LES SOCIETES
				
				$d_soc_agence=array();
				
				$sql="SELECT soc_id FROM Societes WHERE soc_agence ORDER BY soc_id;";
				$req=$Conn->query($sql);
				$d_result_soc=$req->fetchAll();
				if(!empty($d_result_soc)){
					foreach($d_result_soc as $sa){
						$d_soc_agence[$sa["soc_id"]]=true;
					}
				}
				
			
			
			//********************************
			// 	TRAITEMENT DU FICHER
			//********************************
			
			while (($data = fgetcsv($handle, 1000, ";")) !== FALSE){
				$erreur_import="";
				if(!empty($erreur)){					
					break;
				}else{

					$row++;
					
					if($row==1){
						
						$cle=array(
							"CODE" =>"",
							"NOM" =>"",	
							"AGENCE" =>"",										
							"INT_SERVICE" =>"",	
							"INT_AD1" =>"",	
							"INT_AD2" =>"",	
							"INT_AD3" =>"",	
							"INT_CP" =>"",	
							"INT_VILLE" =>"",	
							"FAC_AD_NOM" =>"",	
							"FAC_SERVICE" =>"",	
							"FAC_AD1" =>"",	
							"FAC_AD2" =>"",	
							"FAC_AD3" =>"",	
							"FAC_CP" =>"",
							"FAC_VILLE" =>"",	
							"SIRET" =>"",	
							"CONTACT_FONCTION" =>"",	
							"CONTACT_NOM" =>"",	
							"CONTACT_PRENOM" =>"",
							"CONTACT_TEL" =>"",	
							"CONTACT_FAX" =>"",	
							"CONTACT_MOBILE" =>"",	
							"CONTACT_MAIL" =>"",
							"CONTACT_FONCTION_BIS" =>"",	
							"CONTACT_NOM_BIS" =>"",	
							"CONTACT_PRENOM_BIS" =>"",
							"CONTACT_TEL_BIS" =>"",	
							"CONTACT_FAX_BIS" =>"",	
							"CONTACT_MOBILE_BIS" =>"",	
							"CONTACT_MAIL_BIS" =>"",
							"CODE_INTERNE" => ""
						);
						
						foreach($data as $key => $v){
							if(isset($cle[$v])){
								$cle[$v]=$key;
							}else{
								$erreur="Colonne " . $v . " incompatible";
								break;
							}
						}	
						
					}else{
						
						$int_ville="";
						if($cle["INT_VILLE"]!==""){							
							if(!empty($data[$cle["INT_VILLE"]])){
								$int_ville=utf8_encode($data[$cle["INT_VILLE"]]);
								$int_ville=trim($int_ville);
							}else{
								$erreur_import.="Ligne " . $row . " : ville d'intervention non renseigné<br/>";	
							}
						}
						
						$int_cp="";
						if($cle["INT_CP"]!==""){							
							if(!empty($data[$cle["INT_CP"]])){
								$int_cp=utf8_encode($data[$cle["INT_CP"]]);
								$int_cp=trim($int_cp);
								$int_cp=str_replace('"',"",$int_cp);
								if(strlen($int_cp)==4){
									$int_cp="0" . $int_cp;	
								}; 
							}else{
								$erreur_import.="Ligne " . $row . " : CP d'intervention non renseigné<br/>";	
							}
						}
						
						
						// CHAMP OBLIGATOIRE
						
						$cso_societe=0;
						$cso_agence=0;
						if($cle["AGENCE"]!==""){							
							if(!empty($data[$cle["AGENCE"]])){
								
								switch($data[$cle["AGENCE"]]){
									case "RAA":
										$cso_societe=2;
										$cso_agence=2;
										break;
									case "MED":
										$cso_societe=2;
										$cso_agence=10;
										break;
									case "NN":
										$cso_societe=3;
										$cso_agence=3;
										break;
									case "CO":
										$cso_societe=3;
										$cso_agence=9;
										break;
									case "IDFS":
										$cso_societe=5;
										$cso_agence=5;
										break;
									case "IDFN":
										$cso_societe=5;
										$cso_agence=11;
										break;
									case "SO":
										$cso_societe=7;
										$cso_agence=0;
										break;
									case "GE":
										$cso_societe=8;
										$cso_agence=0;
										break;
									case "GC":
										$cso_societe=4;
										$cso_agence=4;
										break;
								}
							}
						}else{
							
							$cli_dep=substr($int_cp,0,2);
							//$cli_dep=intval($cli_dep);
							
							if(!empty($d_soc_dep[$cli_dep])){
								
								$cso_societe=$d_soc_dep[$cli_dep]["societe"];
								$cso_agence=$d_soc_dep[$cli_dep]["agence"];
							}

						}

						if(empty($cso_societe)){
							$erreur_import.="Ligne " . $row . " : société non identifié<br/>";	
						}elseif(!empty($d_soc_agence[$cso_societe]) AND empty($cso_agence)){
							$erreur_import.="Ligne " . $row . " : l'agence est requise pour les clients affectés à la sociéte " .  $cso_societe . "<br/>";	
							
						}
						
						
						
						// CONTROLE

						if(empty($erreur_import)){
							
							//code
							$cli_code="";
							if($cle["CODE"]!==""){							
								if(!empty($data[$cle["CODE"]])){
									$cli_code=utf8_encode($data[$cle["CODE"]]);
								}
							}
							
							// nom
							$cli_nom="";
							if($cle["NOM"]!==""){							
								if(!empty($data[$cle["NOM"]])){
									$cli_nom=utf8_encode($data[$cle["NOM"]]);
									// pas de code
									// permet d'éviter les doublon de code si plusieurs entité dans la même ville
									if(empty($cli_code)){
										// on le construit 
										$cli_nom_code=str_replace($nom,"",$cli_nom);
										$cli_nom_code=str_replace(" ","",$cli_nom_code);
										$cli_code=$code . substr($int_cp, 0, 2) . $cli_nom_code;
									}
									if(is_bool(strpos($cli_nom,$nom))){
										$cli_nom=$nom . " " . $cli_nom;
									}
								}
							}							
							if(empty($cli_nom)){
								$cli_nom=$nom . " " . $int_ville;
							}
		
							if(empty($cli_code)){
								$cli_code=$code . substr($int_cp, 0, 2) . $int_ville;
							}
							
							$req_check_code->bindParam("cli_code",$cli_code);
							$req_check_code->execute();
							$d_check_code=$req_check_code->fetch();
							if(!empty($d_check_code)){
								$erreur_import.="Ligne " . $row . " : le code " . $cli_code . " existe déjà.<br/>";		
							}
							
	
							// sirent siret
							$cli_siren="";
							$cli_siret="";
							$adr_nic="";
							$cli_affacturable=0;
							$cso_aff_courrier=0;
							
							if($cle["SIRET"]!==""){
								if(!empty($data[$cle["SIRET"]])){
									$cli_siret=$data[$cle["SIRET"]];
									$cli_siret=trim($cli_siret);
									$cli_siret=str_replace("-","",$cli_siret);
									$cli_siret=str_replace(".","",$cli_siret);
									$cli_siret=str_replace(" ","",$cli_siret);
									$cli_siret=str_replace("'","",$cli_siret);
									$cli_siret=str_replace('"',"",$cli_siret);
	
									if(strlen($cli_siret)!=14){
										$erreur_import.="Ligne " . $row . " : Format de siret incorrect.<br/>";		
									}else{
										
										$cli_siren=substr($cli_siret,0,9);										
										$adr_nic=substr($cli_siret,9);	

										$cli_affacturable=1;
										// si le nouveau client est directement en affacturage inutile d'envoyer un courrier à la première facture
										$cso_aff_courrier=1;
										
										$req_check_siret->bindParam(":cli_siren",$cli_siren);
										$req_check_siret->bindParam(":adr_nic",$adr_nic);
										$req_check_siret->execute();
										$d_siret_existe=$req_check_siret->fetchAll();
										if(!empty($d_siret_existe)){
											$erreur_import.="Ligne " . $row . " : le siret " . $cli_siret . "existe déjà dans la base client<br/>";																	
										}
									}
								}
							}
							
							
							$con_mail="";
							if($cle["CONTACT_MAIL"]!==""){
								if(!empty($data[$cle["CONTACT_MAIL"]])){
									if(filter_var($data[$cle["CONTACT_MAIL"]], FILTER_VALIDATE_EMAIL)){
										$con_mail=$data[$cle["CONTACT_MAIL"]];
									}
								}
							}
							
							/*if(!empty($con_mail)){
								
								$req_check_contact->bindParam(":con_mail",$con_mail);
								$req_check_contact->execute();
								$d_contact_existe=$req_check_contact->fetchAll();
								if(!empty($d_contact_existe)){
									$erreur_import.="Ligne " . $row . " : le mail " . $con_mail . "existe déjà dans la base client<br/>";													
								}
							}*/
							
							$con_nom="";
							if($cle["CONTACT_NOM"]!==""){							
								if(!empty($data[$cle["CONTACT_NOM"]])){
									$con_nom=utf8_encode($data[$cle["CONTACT_NOM"]]);
								}
							}
							
							
						
							// donnee clients_soc
							
							if($cso_agence!=4){
								
								$cso_commercial=0;
								if(!empty($d_com_def[$cso_societe . "-" . $cso_agence])){
									$cso_commercial=$d_com_def[$cso_societe . "-" . $cso_agence];
								}else{
									$erreur_import.="Ligne " . $row . " : la structure  " . $cso_societe . "-" . $cso_agence . " n'a pas de commercial par GC.<br/>";		
								}
								
								$cso_rib=0;
								if(!empty($d_rib[$cso_societe ])){
									$cso_rib=$d_rib[$cso_societe];
								}else{
									$erreur_import.="Ligne " . $row . " : la societe  " . $cso_societe . " n'a pas de rib par défaut.<br/>";		
								}
								
							}
						}
						
						// IMPORTATION
						
						if(empty($erreur_import)){
							
							$cli_reference="";
							if($cle["CODE_INTERNE"]!==""){							
								if(!empty($data[$cle["CODE_INTERNE"]])){
									$cli_reference=utf8_encode($data[$cle["CODE_INTERNE"]]);
									$cli_nom=$cli_nom . " - " . $cli_reference;
								}
							}
							
							$cli_code=strtoupper($cli_code);
							$cli_nom=strtoupper($cli_nom);
							
							// creation du client

							$req_add_cli->bindParam(":cli_code",$cli_code);
							$req_add_cli->bindParam(":cli_nom",$cli_nom);
							$req_add_cli->bindParam(":cli_categorie",$d_client["cli_categorie"]);
							$req_add_cli->bindParam(":cli_sous_categorie",$d_client["cli_sous_categorie"]);
							$req_add_cli->bindValue(":cli_groupe",1);
							$req_add_cli->bindParam(":cli_filiale_de",$cli_filiale_de);
							$req_add_cli->bindParam(":cli_niveau",$cli_niveau);									
							$req_add_cli->bindParam(":cli_fil_de",$cli_fil_de);
							$req_add_cli->bindParam(":cli_classification",$d_client["cli_classification"]);
							$req_add_cli->bindParam(":cli_classification_type",$d_client["cli_classification_type"]);
							$req_add_cli->bindParam(":cli_classification_categorie",$d_client["cli_classification_categorie"]);
							$req_add_cli->bindParam(":cli_reg_type",$d_client["cli_reg_type"]);
							$req_add_cli->bindParam(":cli_reg_formule",$d_client["cli_reg_formule"]);
							$req_add_cli->bindParam(":cli_reg_fdm",$d_client["cli_reg_fdm"]);
							$req_add_cli->bindParam(":cli_reg_nb_jour",$d_client["cli_reg_nb_jour"]);
							$req_add_cli->bindParam(":cli_siren",$cli_siren);							
							$req_add_cli->bindParam(":cli_ape",$d_client["cli_ape"]);
							$req_add_cli->bindParam(":cli_opca",$d_client["cli_opca"]);
							$req_add_cli->bindParam(":cli_opca_type",$d_client["cli_opca_type"]);
							$req_add_cli->bindParam(":cli_facture_opca",$d_client["cli_facture_opca"]);
							$req_add_cli->bindParam(":cli_releve",$d_client["cli_releve"]);
							$req_add_cli->bindParam(":cli_fac_groupe",$d_client["cli_fac_groupe"]);
							$req_add_cli->bindParam(":cli_affacturable",$cli_affacturable);
							$req_add_cli->bindParam(":cli_reference",$cli_reference);
							$req_add_cli->bindParam(":cli_uti_creation",$acc_utilisateur);
							try{
								$req_add_cli->execute();
								$client=$Conn->lastInsertId();
							}Catch(Exception $e){
								echo("Creation client : " . $e->getMessage());
								die();
							}
							
							// affectation clients_societes
							
							// le client est un GC dont forcément GFC GC
							
							$req_add_cli_soc->bindParam(":cso_client",$client);
							$req_add_cli_soc->bindValue(":cso_societe",4);
							$req_add_cli_soc->bindValue(":cso_agence",4);
							$req_add_cli_soc->bindParam(":cso_commercial",$d_client["cso_commercial"]);
							$req_add_cli_soc->bindParam(":cso_utilisateur",$d_client["cso_utilisateur"]);
							$req_add_cli_soc->bindValue(":cso_archive",0);
							$req_add_cli_soc->bindParam(":cso_rib",$d_client["cso_rib"]);									
							$req_add_cli_soc->bindValue(":cso_rib_change",0);
							$req_add_cli_soc->bindParam(":cso_aff_courrier",$cso_aff_courrier);
							try{
								$req_add_cli_soc->execute();
							}Catch(Exception $e){
								echo("Add client GC : " . $e->getMessage());
								die();
							}
							
							// ajout de la région qui va utiliser le client
							if($cso_agence!=4){
					
								$req_add_cli_soc->bindParam(":cso_client",$client);
								$req_add_cli_soc->bindParam(":cso_societe",$cso_societe);
								$req_add_cli_soc->bindParam(":cso_agence",$cso_agence);
								$req_add_cli_soc->bindParam(":cso_commercial",$cso_commercial);
								$req_add_cli_soc->bindValue(":cso_utilisateur",0);
								$req_add_cli_soc->bindValue(":cso_archive",0);
								$req_add_cli_soc->bindParam(":cso_rib",$cso_rib);									
								$req_add_cli_soc->bindValue(":cso_rib_change",0);
								$req_add_cli_soc->bindParam(":cso_aff_courrier",$cso_aff_courrier);
								try{
									$req_add_cli_soc->execute();
								}Catch(Exception $e){
									echo("Add client GC : " . $e->getMessage());
									die();
								}
								
							}
							
							// creation du contact
							
							$con_tel="";
							$con_prenom="";
							$con_fonction_nom="";
							if(!empty($con_mail) OR !empty($con_nom)){
															
								if($cle["CONTACT_PRENOM"]!==""){							
									if(!empty($data[$cle["CONTACT_PRENOM"]])){
										$con_prenom=utf8_encode($data[$cle["CONTACT_PRENOM"]]);
									}
								}
								
								if($cle["CONTACT_TEL"]!==""){							
									if(!empty($data[$cle["CONTACT_TEL"]])){
										$con_tel=trim($data[$cle["CONTACT_TEL"]]);
										$con_tel=utf8_encode($con_tel);
										if(substr($con_tel,0,2)=="33"){
											$con_tel="0" . substr($con_tel, 2);
										}elseif(substr($con_tel,0,1)!="0"){
											$con_tel="0" . $con_tel;										
										};
									}
								}	
								
								$con_portable="";
								if($cle["CONTACT_MOBILE"]!==""){							
									if(!empty($data[$cle["CONTACT_MOBILE"]])){
										$con_portable=trim($data[$cle["CONTACT_MOBILE"]]);
										$con_portable=utf8_encode($con_portable);
										if(substr($con_portable,0,2)=="33"){
											$con_portable="0" . substr($con_portable, 2);
										}elseif(substr($con_portable,0,1)!="0"){
											$con_portable="0" . $con_portable;										
										};
									}
								}		
								
								if($cle["CONTACT_FONCTION"]!==""){							
									if(!empty($data[$cle["CONTACT_FONCTION"]])){
										$con_fonction_nom=utf8_encode($data[$cle["CONTACT_FONCTION"]]);
									}
								}
								
								$req_add_con->bindParam(":con_ref_id",$client);
								$req_add_con->bindValue(":con_fonction_nom",$con_fonction_nom);
								$req_add_con->bindValue(":con_titre",0);
								$req_add_con->bindParam(":con_prenom",$con_prenom);
								$req_add_con->bindParam(":con_nom",$con_nom);
								$req_add_con->bindParam(":con_tel",$con_tel);
								$req_add_con->bindParam(":con_portable",$con_portable);									
								$req_add_con->bindValue(":con_fax","");
								$req_add_con->bindParam(":con_mail",$con_mail);
								$req_add_con->bindValue(":con_compta",1);
								try{
									$req_add_con->execute();
									$contact=$Conn->lastInsertId();
								}Catch(Exception $e){
									echo("Creation contact : " . $e->getMessage());
									die();
								}
								
							}
							
							// adresse d'intervention
							
							$adr_int=0;
							
							$int_ad1="";
							if($cle["INT_AD1"]!==""){							
								if(!empty($data[$cle["INT_AD1"]])){
									$int_ad1=utf8_encode($data[$cle["INT_AD1"]]);
									$int_ad1=str_replace('"',"'",$int_ad1);
								}
							}
							
							$int_ad2="";
							if($cle["INT_AD2"]!==""){							
								if(!empty($data[$cle["INT_AD2"]])){
									$int_ad2=utf8_encode($data[$cle["INT_AD2"]]);
									$int_ad2=str_replace('"',"'",$int_ad2);
								}
							}

							$req_add_adr->bindParam(":adr_ref_id",$client);
							$req_add_adr->bindValue(":adr_type",1);
							$req_add_adr->bindParam(":adr_nom",$cli_nom);
							$req_add_adr->bindValue(":adr_service","");
							$req_add_adr->bindParam(":adr_ad1",$int_ad1);
							$req_add_adr->bindParam(":adr_ad2",$int_ad2);
							$req_add_adr->bindValue(":adr_ad3","");									
							$req_add_adr->bindParam(":adr_cp",$int_cp);
							$req_add_adr->bindParam(":adr_ville",$int_ville);
							$req_add_adr->bindValue(":adr_defaut",1);
							$req_add_adr->bindValue(":adr_siret","");							
							try{
								$req_add_adr->execute();
								$adr_int=$Conn->lastInsertId();
							}Catch(Exception $e){
								echo("Creation adresse int : " . $e->getMessage());
								die();
							}
							
							// on lie le contact par defaut et l'adresse d'intervention
							if(!empty($adr_int) AND !empty($contact)){
								
								$req_con_adr->bindParam(":aco_contact",$contact);
								$req_con_adr->bindParam(":aco_adresse",$adr_int);
								$req_con_adr->bindValue(":aco_defaut",1);
								try{
									$req_con_adr->execute();						
								}Catch(Exception $e){
									echo("lien contact adresse int : " . $e->getMessage());
									die();
								}
							}
							
							
							if($fil_intervention_mm==1){
								// il faut dupliquer l'adresse de la filiale 
								// permet de mélanger facturation groupe et factuartion direct dans un meme groupe
								
								$req_add_adr->bindParam(":adr_ref_id",$maison_mere);
								$req_add_adr->bindValue(":adr_type",1);
								$req_add_adr->bindParam(":adr_nom",$cli_nom);
								$req_add_adr->bindValue(":adr_service","");
								$req_add_adr->bindParam(":adr_ad1",$int_ad1);
								$req_add_adr->bindParam(":adr_ad2",$int_ad2);
								$req_add_adr->bindValue(":adr_ad3","");									
								$req_add_adr->bindParam(":adr_cp",$int_cp);
								$req_add_adr->bindParam(":adr_ville",$int_ville);
								$req_add_adr->bindValue(":adr_defaut",0);
								$req_add_adr->bindValue(":adr_siret","");							
								try{
									$req_add_adr->execute();							
								}Catch(Exception $e){
									echo("Creation adresse int MM: " . $e->getMessage());
									die();
								}
								
							}
							
							// adresse de facturation		
							
							$fac_ville="";
							if($cle["FAC_VILLE"]!==""){							
								if(!empty($data[$cle["FAC_VILLE"]])){
									$fac_ville=utf8_encode($data[$cle["FAC_VILLE"]]);
									$fac_ville=trim($fac_ville);
								}
							}
							
							$fac_cp="";
							if($cle["FAC_CP"]!==""){							
								if(!empty($data[$cle["FAC_CP"]])){
									$fac_cp=utf8_encode($data[$cle["FAC_CP"]]);
									$fac_cp=trim($fac_cp);
								}
							}
							
							if(!empty($fac_ville) AND !empty($fac_cp)){
								
								// le fichier contient des adresse de facturation
								
								$fac_ad1="";
								if($cle["FAC_AD1"]!==""){							
									if(!empty($data[$cle["FAC_AD1"]])){
										$fac_ad1=utf8_encode($data[$cle["FAC_AD1"]]);
										$fac_ad1=str_replace('"',"'",$fac_ad1);
									}
								}
								$fac_ad2="";
								if($cle["FAC_AD2"]!==""){							
									if(!empty($data[$cle["FAC_AD2"]])){
										$fac_ad2=utf8_encode($data[$cle["FAC_AD2"]]);
										$fac_ad2=str_replace('"',"'",$fac_ad2);
									}
								}
								
							}else{
								
								$fac_ad1=$int_ad1;
								$fac_ad2=$int_ad2;
								$fac_cp=$int_cp;
								$fac_ville=$int_ville;
								
							}
							
							$adr_fac=0;
							
							if(!empty($fac_cp) AND !empty($fac_ville)){
								
								$req_add_adr->bindParam(":adr_ref_id",$client);
								$req_add_adr->bindValue(":adr_type",2);
								$req_add_adr->bindParam(":adr_nom",$cli_nom);
								$req_add_adr->bindValue(":adr_service","");
								$req_add_adr->bindParam(":adr_ad1",$fac_ad1);
								$req_add_adr->bindParam(":adr_ad2",$fac_ad2);
								$req_add_adr->bindValue(":adr_ad3","");									
								$req_add_adr->bindParam(":adr_cp",$fac_cp);
								$req_add_adr->bindParam(":adr_ville",$fac_ville);
								$req_add_adr->bindValue(":adr_defaut",1);
								$req_add_adr->bindParam(":adr_siret",$adr_nic);							
								try{
									$req_add_adr->execute();
									$adr_fac=$Conn->lastInsertId();
								}Catch(Exception $e){
									echo("Creation adresse fac : " . $e->getMessage());
									die();
								}
							}
							
							
							// on lie le contact par defaut et l'adresse d'intervention
							if(!empty($adr_fac) AND !empty($contact)){
								
								$req_con_adr->bindParam(":aco_contact",$contact);
								$req_con_adr->bindParam(":aco_adresse",$adr_fac);
								$req_con_adr->bindValue(":aco_defaut",1);
								try{
									$req_con_adr->execute();						
								}Catch(Exception $e){
									echo("lien contact adresse fac : " . $e->getMessage());
									die();
								}
							}
							
							// SI SECOND CONTACT
							
							$con_nom_bis="";
							$con_tel_bis="";
							$con_mail_bis="";
							if($cle["CONTACT_NOM_BIS"]!==""){							
								if(!empty($data[$cle["CONTACT_NOM_BIS"]])){
									$con_nom_bis=utf8_encode($data[$cle["CONTACT_NOM_BIS"]]);
								}
							}
							
							if($cle["CONTACT_TEL_BIS"]!==""){							
								if(!empty($data[$cle["CONTACT_TEL_BIS"]])){
									$con_tel_bis=trim($data[$cle["CONTACT_TEL_BIS"]]);
									$con_tel_bis=utf8_encode($con_tel_bis);
									if(substr($con_tel_bis,0,2)=="33"){
										$con_tel_bis="0" . substr($con_tel_bis, 2);
									}elseif(substr($con_tel_bis,0,1)!="0"){
										$con_tel_bis="0" . $con_tel_bis;										
									};
								}
							}
							if($cle["CONTACT_MAIL_BIS"]!==""){
								if(!empty($data[$cle["CONTACT_MAIL_BIS"]])){
									if(filter_var($data[$cle["CONTACT_MAIL_BIS"]], FILTER_VALIDATE_EMAIL)){
										$con_mail_bis=$data[$cle["CONTACT_MAIL_BIS"]];
									}
								}
							}	
							if(!empty($con_nom_bis) AND ( !empty($con_tel_bis) OR !empty($con_mail_bis) ) ){
							
								$con_prenom_bis="";							
								if($cle["CONTACT_PRENOM_BIS"]!==""){							
									if(!empty($data[$cle["CONTACT_PRENOM_BIS"]])){
										$con_prenom_bis=utf8_encode($data[$cle["CONTACT_PRENOM_BIS"]]);
									}
								}
								
								$con_portable_bis="";
								if($cle["CONTACT_MOBILE_BIS"]!==""){							
									if(!empty($data[$cle["CONTACT_MOBILE_BIS"]])){
										$con_portable_bis=trim($data[$cle["CONTACT_MOBILE_BIS"]]);
										$con_portable_bis=utf8_encode($con_portable_bis);
										if(substr($con_portable_bis,0,2)=="33"){
											$con_portable_bis="0" . substr($con_portable_bis, 2);
										}elseif(substr($con_portable_bis,0,1)!="0"){
											$con_portable_bis="0" . $con_portable_bis;										
										};
									}
								}
								
								$req_add_con->bindParam(":con_ref_id",$client);
								$req_add_con->bindValue(":con_fonction_nom","");
								$req_add_con->bindValue(":con_titre",0);
								$req_add_con->bindParam(":con_prenom",$con_prenom_bis);
								$req_add_con->bindParam(":con_nom",$con_nom_bis);
								$req_add_con->bindParam(":con_tel",$con_tel_bis);
								$req_add_con->bindParam(":con_portable",$con_portable_bis);									
								$req_add_con->bindValue(":con_fax","");
								$req_add_con->bindParam(":con_mail",$con_mail_bis);
								$req_add_con->bindValue(":con_compta",0);
								try{
									$req_add_con->execute();
								}Catch(Exception $e){
									echo("Creation contact bis : " . $e->getMessage());
									die();
								}
								
							}
							
							
							// update du client avec les données adresses et facturation
							
							$req_up_cli->bindParam(":cli_adresse",$adr_int);
							$req_up_cli->bindParam(":cli_adr_nom",$cli_nom);
							$req_up_cli->bindValue(":cli_adr_service","");
							$req_up_cli->bindParam(":cli_adr_ad1",$int_ad1);
							$req_up_cli->bindParam(":cli_adr_ad2",$int_ad2);
							$req_up_cli->bindValue(":cli_adr_ad3","");
							$req_up_cli->bindParam(":cli_adr_cp",$int_cp);									
							$req_up_cli->bindParam(":cli_adr_ville",$int_ville);
							$req_up_cli->bindParam(":cli_contact",$contact);
							$req_up_cli->bindValue(":cli_con_fct",0);
							$req_up_cli->bindParam(":cli_con_fct_nom",$con_fonction_nom);		
							$req_up_cli->bindValue(":cli_con_titre",0);
							$req_up_cli->bindParam(":cli_con_nom",$con_nom);
							$req_up_cli->bindParam(":cli_con_prenom",$con_prenom);
							$req_up_cli->bindParam(":cli_con_tel",$con_tel);
							$req_up_cli->bindParam(":cli_con_mail",$con_mail);
							$req_up_cli->bindParam(":client",$client);							
							try{
								$req_up_cli->execute();
							}Catch(Exception $e){
								echo("update client : " . $e->getMessage());
								die();
							}
							
							// ajout du client en base N
							
							// gfc GC
							
							$req_add_cli_n->bindParam(":cli_id",$client);
							$req_add_cli_n->bindValue(":cli_agence",4);
							$req_add_cli_n->bindParam(":cli_code",$cli_code);
							$req_add_cli_n->bindParam(":cli_nom",$cli_nom);
							$req_add_cli_n->bindParam(":cli_commercial",$d_client["cso_commercial"]);									
							$req_add_cli_n->bindParam(":cli_utilisateur",$d_client["cso_utilisateur"]);
							$req_add_cli_n->bindParam(":cli_categorie",$d_client["cli_categorie"]);
							$req_add_cli_n->bindParam(":cli_sous_categorie",$d_client["cli_sous_categorie"]);
							$req_add_cli_n->bindValue(":cli_groupe",1);
							$req_add_cli_n->bindParam(":cli_filiale_de",$cli_filiale_de);								
							$req_add_cli_n->bindParam(":cli_rib",$d_client["cso_rib"]);
							$req_add_cli_n->bindValue(":cli_rib_change",0);
							$req_add_cli_n->bindParam(":cli_cp",$int_cp);
							$req_add_cli_n->bindParam(":cli_fac_groupe",$d_client["cli_fac_groupe"]);		
							$req_add_cli_n->bindParam(":cli_affacturable",$cli_affacturable);								
							try{
								$req_add_cli_n->execute();
							}Catch(Exception $e){
								echo("Add client N GC : " . $e->getMessage());
								die();
							}
							
							if($cso_agence!=4){
								
								$ConnFct=connexion_fct($cso_societe);
								
								$req_add_cli_fct=$ConnFct->prepare($sql_add_cli_n);
								$req_add_cli_fct->bindParam(":cli_id",$client);
								$req_add_cli_fct->bindParam(":cli_agence",$cso_agence);
								$req_add_cli_fct->bindParam(":cli_code",$cli_code);
								$req_add_cli_fct->bindParam(":cli_nom",$cli_nom);
								$req_add_cli_fct->bindParam(":cli_commercial",$cso_commercial);									
								$req_add_cli_fct->bindValue(":cli_utilisateur",0);
								$req_add_cli_fct->bindParam(":cli_categorie",$d_client["cli_categorie"]);
								$req_add_cli_fct->bindParam(":cli_sous_categorie",$d_client["cli_sous_categorie"]);
								$req_add_cli_fct->bindValue(":cli_groupe",1);
								$req_add_cli_fct->bindParam(":cli_filiale_de",$cli_filiale_de);								
								$req_add_cli_fct->bindParam(":cli_rib",$cso_rib);
								$req_add_cli_fct->bindValue(":cli_rib_change",0);
								$req_add_cli_fct->bindParam(":cli_cp",$int_cp);
								$req_add_cli_fct->bindParam(":cli_fac_groupe",$d_client["cli_fac_groupe"]);	
								$req_add_cli_fct->bindParam(":cli_affacturable",$cli_affacturable);										
								try{
									$req_add_cli_fct->execute();
								}Catch(Exception $e){
									echo("Add client N FCT : " . $e->getMessage());
									die();
								}

							}

						}
						
						// fin if(empty($erreur_import);
						
					}
					// FIN IMPORT DU SUSPECT						
				}
			
				
				$rapport_erreur.=$erreur_import;
			}
			// fin boucle ligne
		}
		//fin de traitement fichier
		fclose($handle);
	}else{
		$erreur="Echec de l'enregistrement de l'import!";
	}

}?>
<!DOCTYPE html> 
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8"> 
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	  
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm">
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper" >
				<section id="content" class="" >
	<?php			if(!empty($erreur)){ ?>
						<p class="alert alert-danger" >
							<?=$erreur?>
							</br>Le fichier n'a pas été traité!
						</p>
	<?php			}elseif(!empty($rapport_erreur)){ 	
						if(!empty($rapport_erreur)){ ?>
							<div class="alert alert-danger">
								<h1>Lignes non importées</h1>
								<p><?=$rapport_erreur?></p>
							</div>
	<?php				}
					}else{ ?>
						<p class="alert alert-success" >L'ensemble du fichier a été traité sans erreur!</p>
	<?php			} ?>
				</section>				
			</section>		
		</div>
	

		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<a href="client_import.php" class="btn btn-default btn-sm" >
						<i class="fa fa-arrow-left" ></i> Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle"></div>
				<div class="col-xs-3 footer-right" >
					  <button type="button" id="cancel-btn" class="btn btn-danger btn-sm" style="display:none;">
						<i class='fa fa-times'></i> Annuler
					  </button>
				</div>
			</div>
		</footer>
<?php 	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript">
		</script>
	</body>
</html>
