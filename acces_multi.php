﻿<?php 

session_start(); 

include "includes/connexion.php";

$erreur=1;
if(isset($_SESSION["acces"])){
	if(!empty($_SESSION["acces"]["acc_ref"]) AND !empty($_SESSION["acces"]["acc_ref_id"]) AND ($_SESSION["acces"]["acc_profil"]==13 OR $_SESSION["acces"]["acc_ref_id"]==1 OR $_SESSION["acces"]["acc_ref_id"]==191)){
		$erreur=0;	
	}
}
if($erreur==1){
	header("location : index.php");
	die();
}

	$sql="SELECT uti_id,uti_nom,uti_prenom FROM Utilisateurs WHERE NOT uti_archive ORDER BY uti_nom,uti_prenom;";
	$req=$Conn->query($sql);
	$d_utilisateurs=$req->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" >
	<title>Si2P - Formations incendie, sécurité et prévention</title>
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.min.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/login.css">
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
	<link href='https://fonts.googleapis.com/css?family=Audiowide' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="main">
		<div class="logo">
			<img src="assets/img/login/logo.png" title="Si2P" alt="Si2P" style="margin-bottom:20px;"/>
		</div>
		
		<div class="formulaire">
		
			<h1>Utilisateurs</h1>
			
			<div class="formulaire-body" >
	
				<form method="post" action="acces_multi_enr.php" id="formul" >
					<label>Utilisateur :</label>
					<select class="select select2" name="utilisateur" >
						<option value="" >Selectionner un utilisateur ...</option>
				<?php	if(!empty($d_utilisateurs)){
							foreach($d_utilisateurs as $u){
								if($u["uti_id"]==$_SESSION["acces"]["acc_ref_id"]){
									echo("<option value='" . $u["uti_id"] . "' selected >" . $u["uti_nom"] ." " . $u["uti_prenom"] . "</option>");
								}else{
									echo("<option value='" . $u["uti_id"] . "' >" . $u["uti_nom"] ." " . $u["uti_prenom"] . "</option>");
								}
							}					
						} ?>
					</select>
				
					<input type="submit" value="Me connecter">
					<hr style="margin-top:20px;margin-bottom:20px;">					
				</form>
				
			</div>
		</div>
	</div>
	
	<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
	<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	
	<script src="vendor/plugins/pnotify/pnotify.js"></script>
	
	<script src="assets/js/utility/utility.js"></script>
	<script src="assets/js/main.js"></script>
	<script src="assets/js/orion.js"></script>
	
	<script>
		jQuery(document).ready(function(){	
					
		});
		
	</script>	
</body>
</html>