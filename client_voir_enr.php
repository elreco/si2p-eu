<?php
include "includes/controle_acces.inc.php";

include("includes/connexion.php");
include("connexion_soc.php");
include("modeles/mod_client_2.php");
include("modeles/mod_contact.php");
include("modeles/mod_adresse.php");
include("modeles/mod_parametre.php");
include("modeles/mod_correspondance_client.php");

// modification adresse facturation par défaut

if(isset($_POST['submit1'])){
$client = get_client($_GET['client'], "sus");
if($client['cli_categorie'] == 3){
	$_POST['cli_siret2'] = 0;
	$_POST['cli_siren'] = 0;
}
	if(empty($client['cli_siren']) && isset($_POST['cli_siren'])){
		update_client_siren($_GET['client'], $_POST['cli_siren']);
	}
	update_adresse_client($_GET['adresse'],$_POST['adr_nom'],$_POST['adr_service'],$_POST['adr_ad1'],$_POST['adr_ad2'],$_POST['adr_ad3'],$_POST['adr_cp'],$_POST['adr_ville'],1,$_POST['adr_libelle'], $_POST['cli_geo'] ,$_POST['cli_siret2']);

	Header("Location: client_voir.php?client=" . $_GET['client'] ."&succes=1");

}
// delete correspondance 

if(isset($_GET['submit6'])){
	delete_correspondance($_GET['cor']);
	Header("Location: client_voir.php?client=" . $_GET['client'] ."&search_cor&succes=14");
}
// insertion correspondance

if(isset($_POST['submit2'])){
	// ajout correspondance
	if(!isset($_POST['cor_id'])){


	
		if($_POST['cor_contact'] == "nouveau"){
			if($_POST['con_fonction'] == 'autre'){
				$_POST['con_fonction'] = 0;
			}

			$contact_existe = get_contact($_GET['client'], 1);
			
			if(!empty($contact_existe)){

				$contact = insert_client_contact($_GET['client'],$_GET['categorie'], $_POST['con_fonction'], $_POST['con_fonction_nom'],$_POST['sco_titre'],$_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail'],0,1);

			}else{

				$contact = insert_client_contact($_GET['client'],$_GET['categorie'], $_POST['con_fonction'], $_POST['con_fonction_nom'],$_POST['sco_titre'],$_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail'],1,1);

			}
			$rap = convert_date_sql($_POST['cor_rappeler_le']);

			$date = convert_date_sql($_POST['cor_date']);
			insert_correspondance($_SESSION['acces']['acc_ref_id'], $_POST['cor_commentaire'], $_POST['cor_tel'], $contact, "", $rap, $_GET['client'], $date);

		}elseif($_POST['cor_contact'] == 0){
			$date = convert_date_sql($_POST['cor_date']);
			$rap = convert_date_sql($_POST['cor_rappeler_le']);
			insert_correspondance($_SESSION['acces']['acc_ref_id'], $_POST['cor_commentaire'], $_POST['cor_tel'], 0, $_POST['cor_nom'], $rap, $_GET['client'], $date);
		}else{
			$rap = convert_date_sql($_POST['cor_rappeler_le']);
			$date = convert_date_sql($_POST['cor_date']);

			$req = $ConnSoc->prepare("SELECT * FROM correspondances WHERE cor_client = :sco_suspect AND cor_contact = :sco_contact;");

			$req->bindParam("sco_suspect", $_GET['client']);
			$req->bindParam("sco_contact", $_POST['cor_contact']);
			$req->execute();

			$corr = $req->fetchAll();
			
			if(!empty($corr)){
				foreach($corr as $c){
				
					$req = $ConnSoc->prepare("UPDATE correspondances SET cor_date=:sco_date, cor_rappel = 1 WHERE cor_id = :sco_id;");

					$req->bindParam("sco_date",$date);
					$req->bindParam("sco_id",$c['cor_id']);
					$req->execute();
				}
			}

			insert_correspondance($_SESSION['acces']['acc_ref_id'], $_POST['cor_commentaire'], $_POST['cor_tel'], $_POST['cor_contact'], $_POST['cor_nom'], $rap, $_GET['client'], $date);
			if($_POST['con_fonction'] == 'autre'){
			$_POST['con_fonction'] = 0;
			}
			update_contact_client_2($_POST['cor_contact'], $_POST['con_fonction'], $_POST['con_fonction_nom'], $_POST['sco_titre'], $_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail']);
		}
		

			Header("Location: client_voir.php?client=" . $_GET['client'] ."&search_cor&succes=12");
	// edition correspondance
	}else{
		if($_POST['cor_contact'] == "nouveau"){
			if($_POST['con_fonction'] == 'autre'){
				$_POST['con_fonction'] = 0;
			}
			$contact = insert_client_contact($_GET['client'],$_GET['categorie'], $_POST['con_fonction'], $_POST['con_fonction_nom'],$_POST['sco_titre'],$_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail'],0,1);
			$rap = convert_date_sql($_POST['cor_rappeler_le']);
			$date = convert_date_sql($_POST['cor_date']);
			if(isset($_POST['cor_rappel']) && $_POST['cor_rappel'] == "cor_rappel"){
				$cor_rappel = 1;
			}else{
				$cor_rappel = 0;
			}
			update_correspondance($_POST['cor_id'], $_SESSION['acces']['acc_ref_id'], $_POST['cor_commentaire'], $_POST['cor_tel'], $contact, "", $rap, $_GET['client'],$cor_rappel, $date);

		}elseif($_POST['cor_contact'] == 0){

			$rap = convert_date_sql($_POST['cor_rappeler_le']);
			$date = convert_date_sql($_POST['cor_date']);
			if(isset($_POST['cor_rappel']) && $_POST['cor_rappel'] == "cor_rappel"){
				$cor_rappel = 1;
			}else{
				$cor_rappel = 0;
			}
			update_correspondance($_POST['cor_id'], $_SESSION['acces']['acc_ref_id'], $_POST['cor_commentaire'], $_POST['cor_tel'], 0, $_POST['cor_nom'], $rap, $_GET['client'], $cor_rappel, $date);
		}else{
			$rap = convert_date_sql($_POST['cor_rappeler_le']);
			$date = convert_date_sql($_POST['cor_date']);
			if(isset($_POST['cor_rappel']) && $_POST['cor_rappel'] == "cor_rappel"){
				$cor_rappel = 1;
			}else{
				$cor_rappel = 0;
			}
			update_correspondance($_POST['cor_id'], $_SESSION['acces']['acc_ref_id'], $_POST['cor_commentaire'], $_POST['cor_tel'], $_POST['cor_contact'], $_POST['cor_nom'], $rap, $_GET['client'], $cor_rappel, $date);
			if($_POST['con_fonction'] == 'autre'){
				$_POST['con_fonction'] = 0;
			}
			update_contact_client_2($_POST['cor_contact'], $_POST['con_fonction'], $_POST['con_fonction_nom'], $_POST['sco_titre'], $_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail']);
		}

		Header("Location: client_voir.php?client=" . $_GET['client'] ."&search_cor&succes=13");
	}

}
// update contact
if(isset($_POST['submit3'])){
	if($_POST['sco_fonction'] == "autre"){
		$_POST['sco_fonction'] = 0;
	}
	update_contact_client($_GET['contact'],$_POST['sco_fonction'],$_POST['sco_fonction_nom'],$_POST['sco_titre'],$_POST['sco_nom'],$_POST['sco_prenom'],$_POST['sco_tel'],$_POST['sco_portable'],$_POST['sco_fax'], $_POST['sco_mail'],1);

	Header("Location: client_voir.php?client=" . $_GET['client'] ."&succes=2");
}
// ajout d'adresse
if(isset($_POST['submit4'])){
	$client = get_client($_GET['client'], "sus");
	if($client['cli_categorie'] == 3){
	$_POST['cli_siret2'] = 0;
	$_POST['cli_siren'] = 0;
}
	if(empty($client['cli_siren'])){
			update_client_siren($_GET['client'], $_POST['cli_siren']);
	}
	if(isset($_POST['adr_defaut']) && $_POST['adr_defaut'] == "adr_defaut"){
		$adresses = get_adresses_clients($_GET['client'], $_POST['adr_type']);
		foreach($adresses as $a){
			update_adresse_client_defaut($a['adr_id'],0);
		}

		

		insert_client_adresse($_GET['categorie'],$_GET['client'], $_POST['adr_type'], $_POST['adr_nom'],$_POST['adr_service'],$_POST['adr_ad1'],$_POST['adr_ad2'],$_POST['adr_ad3'],$_POST['adr_cp'],$_POST['adr_ville'],1,$_POST['adr_libelle'] , $_POST['cli_geo'] ,$_POST['cli_siret2']);
	}else{
		insert_client_adresse($_GET['categorie'],$_GET['client'], $_POST['adr_type'], $_POST['adr_nom'],$_POST['adr_service'],$_POST['adr_ad1'],$_POST['adr_ad2'],$_POST['adr_ad3'],$_POST['adr_cp'],$_POST['adr_ville'],0,$_POST['adr_libelle'], $_POST['cli_geo'] ,$_POST['cli_siret2']);
	}
	Header("Location: client_voir.php?client=" . $_GET['client'] ."&search_adresse&succes=3");
}

// ajout contact
if(isset($_POST['submit5'])){
	if($_POST['con_fonction'] == 'autre'){
			$_POST['con_fonction'] =0;
		}
	if(isset($_POST['con_defaut']) && $_POST['con_defaut'] == "con_defaut"){
		$contacts = get_contacts_clients($_GET['client']);
		foreach($contacts as $c){
			update_contact_client_defaut($c['con_id'],0);
		}

		insert_client_contact($_GET['client'],$_GET['categorie'], $_POST['con_fonction'], $_POST['con_fonction_nom'],$_POST['con_titre'],$_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail'],1,1);
	}else{
		insert_client_contact($_GET['client'],$_GET['categorie'], $_POST['con_fonction'], $_POST['con_fonction_nom'],$_POST['con_titre'],$_POST['con_nom'],$_POST['con_prenom'],$_POST['con_tel'],$_POST['con_portable'],$_POST['con_fax'],$_POST['con_mail'],0,1);
	}

	
	Header("Location: client_voir.php?client=" . $_GET['client'] ."&succes=4&search_contact");
}
// editier adresse normale
if(isset($_POST['submit6'])){
	$client = get_client($_GET['client'], "sus");
	if(empty($client['cli_siren'])){
			update_client_siren($_GET['client'], $_POST['cli_siren']);
	}
	if(isset($_POST['adr_defaut']) && $_POST['adr_defaut'] == "adr_defaut"){
		$adresses = get_adresses_clients($_GET['client'], $_POST['adr_type']);
		foreach($adresses as $a){
			update_adresse_client_defaut($a['adr_id'], 0);
		}
		$defaut = 1;
	}else{
		$defaut = 0;
	}
	update_adresse_client($_GET['adresse'],$_POST['adr_nom'],$_POST['adr_service'],$_POST['adr_ad1'],$_POST['adr_ad2'],$_POST['adr_ad3'],$_POST['adr_cp'],$_POST['adr_ville'],$defaut,$_POST['adr_libelle'], $_POST['cli_geo'] ,$_POST['cli_siret2']);

	Header("Location: client_voir.php?client=" . $_GET['client'] ."&succes=1&search_adresse");

}
// update contact
if(isset($_POST['submit7'])){
	if($_POST['sco_fonction'] == "autre"){
		$_POST['sco_fonction'] = 0;
	}
	if(isset($_POST['sco_defaut']) && $_POST['sco_defaut'] == "sco_defaut"){
		$contacts = get_contacts_clients($_GET['client']);
		foreach($contacts as $c){
			update_contact_client_defaut($c['sco_id'],0);
		}
		$defaut = 1;
	}else{
		$defaut = 0;
	}

	update_contact_client($_GET['contact'],$_POST['sco_fonction'],$_POST['sco_fonction_nom'],$_POST['sco_titre'],$_POST['sco_nom'],$_POST['sco_prenom'],$_POST['sco_tel'],$_POST['sco_portable'],$_POST['sco_fax'], $_POST['sco_mail'],$defaut);

	Header("Location: client_voir.php?client=" . $_GET['client'] ."&succes=5&search_contact=1");
}
// supprimer adresse
if(isset($_GET['submit8'])){
	delete_client_adresse($_GET['adresse']);
	Header("Location: client_voir.php?client=" . $_GET['client'] ."&succes=7&search_adresse");
}
// supprimer contact
if(isset($_GET['submit9'])){
	delete_client_contact($_GET['contact']);
	Header("Location: client_voir.php?client=" . $_GET['client'] ."&succes=8&search_contact");
}

// ajouter/editer info
if(isset($_POST['submit10'])){
	if(!isset($_POST['inf_id'])){
		insert_client_info($_POST['inf_type'], $_POST['inf_description'], $_POST['inf_famille'], $_POST['inf_sous_famille'], $_SESSION['acces']['acc_ref_id'], $_GET['client']);
		Header("Location: client_voir.php?client=" . $_GET['client'] ."&succes=9&tab6");
	}else{
		update_client_info($_POST['inf_id'], $_POST['inf_type'], $_POST['inf_description'], $_POST['inf_famille'], $_POST['inf_sous_famille'], $_GET['client']);
		Header("Location: client_voir.php?client=" . $_GET['client'] ."&succes=10&tab6");
	}
	
}
// supprimer info
if(isset($_GET['submit11'])){
	delete_client_info($_GET['info']);
	Header("Location: client_voir.php?client=" . $_GET['client'] ."&succes=11&tab6");
}
