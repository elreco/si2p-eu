<?php

	//EDITION DES PRODUIT D'UN CLIENT

	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");

	//include('modeles/mod_acc_client_edit.php');

	include('modeles/mod_orion_pro_categories.php');
	include('modeles/mod_orion_pro_familles.php');
	include('modeles/mod_get_pro_s_familles.php');
	include('modeles/mod_get_client_produits.php');

	// PARAMETRE

	// $client correspond à l'ID du client consulter // différent de cpr_client qui est l'ID du client qui mémorise la base produit
	$client=0;
	if(!empty($_GET['client'])){
		$client=intval($_GET['client']);
	}
	if($client==0){
		echo("Erreur : client inconnu !");
		die();
	}

	$produit=0;
	if(!empty($_GET['produit'])){
		$produit=intval($_GET['produit']);
	}

	// DONNEE POUR TRAITEMENT

	// personne connecte
	$acc_societe=0;
	if(!empty($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);
	}

	// client
	$cpr_client=$client;

	$req=$Conn->query("SELECT cli_categorie,cli_sous_categorie,cca_gc,cca_libelle,cli_groupe,cli_filiale_de FROM Clients,Clients_Categories WHERE cli_categorie=cca_id AND cli_id=" . $client . ";");
	$d_client=$req->fetch();
	if(!empty($d_client)){
		if($d_client["cli_groupe"] AND $d_client["cli_filiale_de"]>0){
			$cpr_client=$d_client["cli_filiale_de"];
		}
	}else{
		echo("Erreur : client inconnu !");
		die();
	}

	// donnée déja enregistré

	if(empty($produit)){
		// creation
		$donnee=get_client_produits($client,null,0,0,0,0,0,0,0,0,0);
	}else{
		// maj
		$donnee=get_client_produits($client,$produit,0,0,0,0,0,0,0,0,0);
	}

	// classification du produit
	$categorie_produit=orion_produits_categories();
	$famille_produit=orion_produits_familles();
	if(!empty($donnee["famille"])){
		$d_sous_famille=get_pro_s_familles($donnee["famille"]);
	}
	if(!empty($donnee["sous_famille"])){
		$d_sous_sous_famille=get_pro_s_s_familles($donnee["sous_famille"]);
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" >
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" >

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">


	   <!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

	</head>
	<body class="sb-top sb-top-sm ">
	<form method="post" action="client_produit_enr.php" id="form_modal_produit" >
		<div>
			<input type="hidden" name="produit" value="<?=$produit?>" />
			<input type="hidden" name="client" value="<?=$client?>" />
			<input type="hidden" name="cpr_client" value="<?=$cpr_client?>" class="select2-produit-client" />
		</div>
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper" class="" >
					<section id="content" class="animated fadeIn">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">

								<div class="admin-form theme-primary ">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">

											<div class="content-header">
										<?php	if($produit>0){ ?>
													<h2>Edition d'un <b class="text-primary">produit</b></h2>
										<?php	}else{ ?>
													<h2>Nouveau <b class="text-primary">produit</b></h2>
										<?php	} ?>

											</div>
											<div class="col-md-10 col-md-offset-1">

									<?php		if(empty($donnee["client"])){
													// si c'est un spécifique client la maj se fait via produit

													if(!$donnee["ca_verrou_intra"] AND !$donnee["ca_verrou_inter"]){ ?>

														<div class="row" >
															<div class="col-md-6" >
																<select class="select2 select2-produit-cat" name="pro_categorie" id="cpr_pro_categorie"  >
																	<option value="0" >Catégorie...</option>
															<?php	if(!empty($categorie_produit)){
																		foreach($categorie_produit as $fp_k => $fp_v){
																			if($fp_k>0){
																				if($fp_k==$donnee["categorie"]){
																					echo("<option value='" . $fp_k . "' selected >" . $fp_v["pca_libelle"] . "</option>");
																				}else{
																					echo("<option value='" . $fp_k . "' >" . $fp_v["pca_libelle"] . "</option>");
																				}

																			}
																		}
																	} ?>
																</select>
															</div>
														</div>
														<div class="row mt15" id="bloc_famille" <?php if($donnee["categorie"]!=1) echo("style='display:none;'"); ?> >
															<div class="col-md-4" >
																<select class="select2 select2-famille select2-produit-fam" name="pro_famille" id="cpr_pro_famille" >
																	<option value="0" >Famille...</option>
															<?php	if(!empty($famille_produit)){
																		foreach($famille_produit as $fp_k => $fp_v){
																			if($fp_k>0){
																				if($fp_k==$donnee["famille"]){
																					echo("<option value='" . $fp_k . "' selected >" . $fp_v["pfa_libelle"] . "</option>");
																				}else{
																					echo("<option value='" . $fp_k . "' >" . $fp_v["pfa_libelle"] . "</option>");
																				}
																			}
																		}
																	} ?>
																</select>
															</div>
															<div class="col-md-4" >
																<select class="select2 select2-famille-sous-famille select2-produit-sous-fam" id="cpr_pro_s_famille" name="pro_sous_famille"  >
																	<option value="0" selected >Sous-famille...</option>
															<?php	if(!empty($d_sous_famille)){
																		foreach($d_sous_famille as $dsf){
																			if($donnee["sous_famille"]==$dsf["psf_id"]){
																				echo("<option value='" . $dsf["psf_id"] . "' selected >" . $dsf["psf_libelle"] . "</option>");
																			}else{
																				echo("<option value='" . $dsf["psf_id"] . "' >" . $dsf["psf_libelle"] . "</option>");
																			}
																		}
																	} ?>
																</select>
															</div>
															<div class="col-md-4" >
																<select class="select2 select2-famille-sous-sous-famille select2-produit-sous-sous-fam" id="cpr_pro_s_s_famille" name="pro_sous_sous_famille"  >
																	<option value="0" selected >Sous-Sous-famille...</option>
															<?php	if(!empty($d_sous_sous_famille)){
																		foreach($d_sous_sous_famille as $dsf){
																			if($donnee["sous_sous_famille"]==$dsf["pss_id"]){
																				echo("<option value='" . $dsf["pss_id"] . "' selected >" . $dsf["pss_libelle"] . "</option>");
																			}else{
																				echo("<option value='" . $dsf["pss_id"] . "' >" . $dsf["pss_libelle"] . "</option>");
																			}
																		}
																	} ?>
																</select>
															</div>
														</div>
														<div class="row mt15" >
															<div class="col-md-6" >
																<label for="produit" >Produit :</label>
																<select class="select2 select2-produit" name="cpr_produit" id="produit" data-client="<?=$client?>" data-base_client="2" data-produit="<?=$produit?>" >
															<?php	if(!empty($donnee["reference"])){
																		echo("<option value='" . $produit . "' selected >" . $donnee["reference"] . "</option>");
																	}else{
																		echo("<option value='' selected >Produit...</option>");
																	} ?>
																</select>
																<span class="texte_required" id="alerte_produit" >Merci de selectionner un produit!</span>
															</div>
														</div>
											<?php	}else{ ?>
														<div class="row mt15" >
															<div class="col-md-12" >Produit : <strong><?=$donnee["reference"]?></strong></div>
														</div>
											<?php	} ?>
													<div class="row mt15" >
														<div class="col-md-12" >
															<label for="cpr_libelle" >Libellé :</label>
															<input type="text" name="cpr_libelle" id="cpr_libelle" class="gui-input" placeholder="Libellé" value="<?=$donnee["libelle"]?>" />
														</div>
													</div>
													<div class="row mt15">
														<div class="col-md-12">
															<div class="section-divider">
																<span>Tarification</span>
															</div>
														</div>
													</div>
													<div class="row mt15 msg-required" id="alerte_intra_inter"  >
														<div class="col-md-12" >
															Vous devez selectioner au moins une des options ci-dessous :
														</div>
													</div>
													<div class="row mt15" >
														<div class="col-md-6 text-center" >
													<?php	if(!$donnee["ca_verrou_intra"]){ ?>
																<div class="option-group field">
																	<label class="option option-dark">
																<?php	$ini_check="";
																		if(!$donnee["intra"]){
																			$ini_check="disabled";
																		}elseif($donnee["intra_cli"]){
																			$ini_check="checked";
																		} ?>
																		<input type="checkbox" id="cpr_intra" name="cpr_intra" value="1" <?=$ini_check?> />
																		<span class="checkbox"></span>INTRA
																	</label>
																</div>
													<?php	}else{
																if($donnee["intra_cli"]){
																	echo("<i class='fa fa-check-square-o' ></i> INTRA");
																}else{
																	echo("<i class='fa fa-square-o' ></i> INTRA");
																}
															} ?>
														</div>
														<div class="col-md-6 text-center" >
													<?php	if(!$donnee["ca_verrou_inter"]){ ?>
																<div class="option-group field">
																	<label class="option option-dark">
																<?php	$ini_check="";
																		if(!$donnee["inter"]){
																			$ini_check="disabled";
																		}elseif($donnee["inter_cli"]){
																			$ini_check="checked";
																		} ?>
																		<input type="checkbox" id="cpr_inter" name="cpr_inter" value="1" <?=$ini_check?> />
																		<span class="checkbox"></span>INTER
																	</label>
																</div>
													<?php	}else{
																if($donnee["inter_cli"]){
																	echo("<i class='fa fa-check-square-o' ></i> INTER");
																}else{
																	echo("<i class='fa fa-square-o' ></i> INTER");
																}
															} ?>
														</div>
													</div>
													<div class="row mt15" >
												<?php	if(!$donnee["ca_verrou_inter"]){ ?>
															<div class="col-md-6" >
																<label for="cpr_ht_intra" >Prix INTRA :</label>
																<input type="text" name="cpr_ht_intra" id="cpr_ht_intra" class="gui-input" placeholder="Prix INTRA" value="<?=$donnee["ht_intra"]?>" <?php if(!$donnee["intra_cli"]) echo("disabled"); ?> >
																<div id="alert_ht_intra" ></div>
															</div>
												<?php	}else{
															echo("<div class='col-md-6 text-center' >");
																echo("Prix INTRA : <b>" . $donnee["ht_intra"] . " €</b>");
															echo("</div>");
														}
														if(!$donnee["ca_verrou_inter"]){ ?>
															<div class="col-md-6" >
																<label for="cpr_ht_inter" >Prix INTER :</label>
																<input type="text" name="cpr_ht_inter" id="cpr_ht_inter" class="gui-input" placeholder="Prix INTER" value="<?=$donnee["ht_inter"]?>" <?php if(!$donnee["inter_cli"]) echo("disabled"); ?> />
																<div id="alert_ht_inter" ></div>
															</div>
												<?php	}else{
															echo("<div class='col-md-6 text-center' >");
																echo("Prix INTER : <b>" . $donnee["ht_inter"] . " €</b>");
															echo("</div>");
														} ?>
													</div>
										<?php 		if( ($_SESSION["acces"]["acc_droits"][27] AND $donnee["derogation"]<2) OR $_SESSION["acces"]["acc_droits"][31]){ ?>
														<div class="row mt15">
															<div class="col-md-12">
																<div class="section-divider">
																	<span>Dérogation tarifaire</span>
																</div>
															</div>
														</div>
														<div class="row mt15" >
															<div class="col-md-12" >
																<div class="row mt15" >
																	<div class="col-md-4 text-center" >
																		<div class="radio-custom mb5">
																			<input id="derog_niv_0" name="cpr_derogation" type="radio" value="0"<?php if(empty($donnee["derogation"])) echo(" checked"); ?> >
																			<label for="derog_niv_0">Pas de dérogation</label>
																		</div>
																	</div>
																	<div class="col-md-4 text-center" >
																		<div class="radio-custom mb5">
																			<input id="derog_niv_1" name="cpr_derogation" type="radio" value="1"<?php if($donnee["derogation"]==1) echo(" checked"); ?> >
																			<label for="derog_niv_1">Dérogation niv.1</label>
																		</div>
																	</div>
															<?php	if($_SESSION["acces"]["acc_droits"][31]){ ?>
																		<div class="col-md-4 text-center" >
																			<div class="radio-custom mb5">
																				<input id="derog_niv_2" name="cpr_derogation" type="radio" value="2"<?php if($donnee["derogation"]==2) echo(" checked"); if(!$_SESSION["acces"]["acc_droits"][31]) echo(" disabled"); ?> />
																				<label for="derog_niv_2">Dérogation niv.2</label>
																			</div>
																		</div>
															<?php	} ?>
																</div>
															</div>
														</div>
														<div class="row mt25" >
															<div class="col-md-2 text-center" >
																<div class="option-group field">
																	<label class="option option-dark">
																		<input type="checkbox" id="rem_famille" name="cpr_rem_famille" value="cpr_rem_famille"<?php if(!empty($donnee["rem_famille"]) OR empty($donnee["derogation"]) ) echo(" checked"); if(empty($donnee["derogation"])) echo(" disabled"); ?> >
																		<span class="checkbox"></span><b>Prime "Famille"</b>
																	</label>
																</div>
															</div>
															<div class="col-md-4" >
																Cocher cette case pour que le CA généré par ce code produit soit pris en compte dans le calcul des primes "Familles".
															</div>
															<div class="col-md-2 text-center" >
																<div class="option-group field">
																	<label class="option option-dark">
																		<input type="checkbox" id="rem_ca" name="cpr_rem_ca" value="cpr_rem_ca" <?php if(!empty($donnee["rem_ca"]) OR empty($donnee["derogation"]) ) echo(" checked"); if(empty($donnee["derogation"])) echo(" disabled"); ?> >
																		<span class="checkbox"></span><b>Prime "CA"</b>
																	</label>
																</div>
															</div>
															<div class="col-md-4" >
																Cocher cette case pour que le CA généré par ce code produit soit pris en compte dans le calcul des primes "CA".
															</div>
														</div>
										<?php		}elseif(!empty($donnee["derogation"])){ ?>
														<div class="row mt15">
															<div class="col-md-12">
																<div class="section-divider">
																	<span>Dérogation tarifaire</span>
																</div>
															</div>
														</div>
														<div class="row mt15" >
															<div class="col-md-12" >
																<div class="row mt15" >
																	<div class="col-md-4 text-center" >
																		Dérogation tarifaire : <b>Niveau <?=$donnee["derogation"]?></b>
																	</div>
																	<div class="col-md-4 text-center" >
																<?php	if(!empty($donnee["rem_famille"])){
																			echo('<i class="fa fa-check-square-o" ></i> Prime "Famille"');
																		}else{
																			echo('<i class="fa fa-square-o" ></i> Prime "Famille"');
																		} ?>
																	</div>
																	<div class="col-md-4 text-center" >
																<?php	if(!empty($donnee["rem_ca"])){
																			echo('<i class="fa fa-check-square-o" ></i> Prime "CA"');
																		}else{
																			echo('<i class="fa fa-square-o" ></i> Prime "CA"');
																		} ?>
																	</div>
																</div>
															</div>
														</div>
									<?php			}
												} ?>
												<div class="row mt25">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Texte "Devis"</span>
														</div>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-6" >
														<p>Valeur par défaut :</p>
														<div id="devis_def" >
													<?php	if(!empty($donnee["devis_txt_defaut"])){
																echo("<p>" . $donnee["devis_txt_defaut"] . "</p>");
															}else{
																echo("<p class='alert alert-danger' >Pas de valeur par défaut</p>");
															} ?>
														</div>
													</div>
													<div class="col-md-6" >
														<label for="cpr_devis_txt" >Texte pour les devis (ne pas renseigner pour utiliser la valeur par défaut) :</label>
														<div class="section">
															<label class="field prepend-icon">
																<textarea class="summernote-img" id="cpr_devis_txt" name="cpr_devis_txt" placeholder="Texte pour les devis"><?=$donnee["devis_txt"]?></textarea>
															</label>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Autre</span>
														</div>
													</div>
												</div>
												<div class="row mt15" >
													<div class="col-md-6" >
														<label for="cpr_comment_txt" >Commentaire :</label>
														<textarea class="summernote-img" id="cpr_comment_txt" name="cpr_comment_txt" placeholder="Commentaires"><?=$donnee["comment_txt"]?></textarea>
													</div>
													<div class="col-md-6" >

											<?php		if( ($_SESSION["acces"]["acc_droits"][27] AND $donnee["derogation"]<2) OR $_SESSION["acces"]["acc_droits"][31]){ ?>
															<div class="row mt15" >
																<div class="col-md-12 pt25" >
																	<label class="option">
																		<input type="checkbox" name="cpr_nego" id="nego" value="on" <?php if($donnee["nego"]) echo("checked"); if($donnee["derogation"]==0) echo("disabled"); ?> />
																		<span class="checkbox"></span>Renégociation demandée.
																	</label>
																</div>
															</div>
											<?php		}else{ ?>
															<div class="row mt15" >
																<div class="col-md-12 pt25" >
															<?php	if($donnee["nego"]){
																		echo("<i class='fa fa-check-square-o' ></i>");
																	}else{
																		echo("<i class='fa fa-check-o' ></i>");
																	}
																	echo(" Renégociation demandée."); ?>
																</div>
															</div>
											<?php		}
														if($_SESSION["acces"]["acc_droits"][8]){
															if($d_client["cli_categorie"]==2){
																// GC ?>
																<div class="section-divider" >
																	<span><?=$d_client["cca_libelle"]?></span>
																</div>
																<div class="row mt15" >
																	<div class="col-md-12" >
																		<label for="cpr_alerte_jour" >Alerte (jours) :</label>
																		<input class="gui-input" id="cpr_alerte_jour" name="cpr_alerte_jour" placeholder="Alerte" value="<?=$donnee["alerte_jour"]?>" />
																	</div>
																</div>
																<div class="row mt15" >
																	<div class="col-md-12 pt25" >
																		<label class="option">
																			<input type="checkbox" name="cpr_confirmation_gc" id="cpr_confirmation_gc" value="on" <?php if($donnee["confirmation_gc"]) echo("checked"); ?> />
																			<span class="checkbox"></span>Confirmation "service GC"
																		</label>
																	</div>
																</div>
												<?php			if(empty($donnee["client"])){  ?>
																	<div class="row mt15" >
																		<div class="col-md-12 pt25" >
																			<label class="option">
																				<input type="checkbox" name="cpr_valide" id="cpr_valide" value="on" <?php if($donnee["valide"]) echo("checked"); ?> />
																				<span class="checkbox"></span>Tarif validé
																			</label>
																		</div>
																	</div>
												<?php 			}
																if($d_client["cli_sous_categorie"]==3){ ?>
																	<div class="row mt15" >
																		<div class="col-md-12 pt25" >
																			<label class="option">
																				<input type="checkbox" name="cpr_cn" id="cpr_cn" value="on" <?php if($donnee["cn"]) echo("checked"); ?>  />
																				<span class="checkbox"></span>Produit "Contrat National"
																			</label>
																		</div>
																	</div>
												<?php			}
															}elseif($d_client["cli_categorie"]==5){
																// CLIENT NATIONAL ?>
																<div class="section-divider" >
																	<span><?=$d_client["cca_libelle"]?></span>
																</div>
																<div class="row" >
																	<div class="col-md-12 text-center pt25" >
																		<label class="option">
																			<input type="checkbox" name="cpr_valide" id="cpr_valide" value="on" <?php if($donnee["valide"]) echo("checked"); ?>  />
																			<span class="checkbox"></span>Tarif validé
																		</label>
																	</div>
																</div>
												<?php		}
														} ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="client_voir.php?client=<?=$client?>&onglet=13" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" ></div>
					<div class="col-xs-3 footer-right" >
						<button type="button" id="sub_form" class="btn btn-success btn-sm">
							<i class='fa fa-save'></i> Enregistrer
						</button>
					</div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>
		<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/select2/js/i18n/fr.js"></script>
		<script src="/assets/admin-tools/admin-forms/js/jquery.validate.min.js" ></script>
		<script src="/assets/admin-tools/admin-forms/js/jquery.validate.french.js" ></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote.min.js"></script>
		<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
		<script type="text/javascript" >
			var client="<?=$client?>";

			var min_dr_intra=parseFloat("<?=$donnee["min_dr_intra"]?>");
			var min_intra=parseFloat("<?=$donnee["min_intra"]?>");
			<?php if(!empty($donnee["cpr_min_intra"])){ ?>
			var min_intra=parseFloat("<?=$donnee["cpr_min_intra"]?>");
			<?php } ?>

			var ht_intra=parseFloat("<?=$donnee["ht_intra"]?>");

			var min_dr_inter=parseFloat("<?=$donnee["min_dr_inter"]?>");
			var min_inter=parseFloat("<?=$donnee["min_inter"]?>");
			<?php if(!empty($donnee["cpr_min_inter"])){ ?>
			var min_inter=parseFloat("<?=$donnee["cpr_min_inter"]?>");
			<?php } ?>
			var ht_inter=parseFloat("<?=$donnee["ht_inter"]?>");

			var donnee_produit=new Array();
			var derogation=0;
			var min_ref=0;


			jQuery(document).ready(function (){
				$("#cpr_pro_categorie").change(function (){
					if($(this).val()==1){
						$("#bloc_famille").show();
					}else{
						$("#bloc_famille").hide();
					}
				});

				$("#produit").change(function (){
					console.log("#produit");
					produit=$(this).val();
					get_produit(client,produit,0,0,afficher_produit);
				});

				$("#cpr_intra").click(function (){
					if($(this).is(":checked")){
						activer_intra();
					}else{
						desactiver_intra();
					}
				});
				$("#cpr_inter").click(function (){
					if($(this).is(":checked")){
						activer_inter();
					}else{
						desactiver_inter();
					}
				});

				/*$("#cpr_min_intra").change(function(){
					min_intra=$(this).val();
					if($("#cpr_ht_intra").val()<min_intra){
						$("#cpr_ht_intra").val(min_intra);
					}
				});
				$("#cpr_min_inter").change(function(){
					min_inter=$(this).val();
					if($("#cpr_ht_inter").val()<min_inter){
						$("#cpr_ht_inter").val(min_inter);
					}
				});*/

				// Calcul du prix en fonction des dérogations

				$("#cpr_ht_intra").change(function(){
					derogation=$('input[name=cpr_derogation]:checked').val();
					if(derogation==2){
						min_ref=0;
					}else if(derogation==1){
						min_ref=min_dr_intra;
					}else{
						min_ref=min_intra;
					}
					if($(this).val()<min_ref){
						$(this).val(min_ref);
						afficher_txt_user($("#alert_ht_intra"),"Prix minimum de " + min_ref + " €","text-danger",5000);
					}
				});


				$("#cpr_ht_inter").change(function(){
					derogation=$('input[name=cpr_derogation]:checked').val();
					if(derogation==2){
						min_ref=0;
					}else if(derogation==1){
						min_ref=min_dr_inter;
					}else{
						min_ref=min_inter;
					}
					if($(this).val()<min_ref){
						$(this).val(min_ref);
						afficher_txt_user($("#alert_ht_inter"),"Prix minimum de " + min_ref + " €","text-danger",5000);
					}
				});

				// changement de dérogation
				$('input[name=cpr_derogation]').change(function(){
					derogation=$('input[name=cpr_derogation]:checked').val();
					if(derogation==0){
						$("#rem_famille").prop("checked",true);
						$("#rem_famille").prop("disabled",true);
						$("#rem_ca").prop("checked",true);
						$("#rem_ca").prop("disabled",true);
						$("#rem_ca").prop("disabled",true);
						$("#nego").prop("checked",false);
						$("#nego").prop("disabled",true);
					}else{
						$("#rem_famille").prop("disabled",false);
						$("#rem_ca").prop("disabled",false);
						$("#nego").prop("disabled",false);
					}
					if($("#cpr_intra").is(":checked")){
						$("#cpr_ht_intra").trigger("change");
					}
					if($("#cpr_inter").is(":checked")){
						$("#cpr_ht_inter").trigger("change");
					}

				});

				$("#sub_form").click(function(){
					console.log("devis text :" + $("#cpr_devis_txt").code() + "/");
					valide=true;
					if($("#produit").length>0){

						if($("#produit").val()==""){
							valide=false;
							$("#alerte_produit").show();
						}
						if(!$("#cpr_intra").prop("checked") && !$("#cpr_inter").prop("checked")){
							valide=false;
							$("#alerte_intra_inter").show();
						};
					}
					//valide=false;
					if(valide){
						$("#form_modal_produit").submit();
					}
				});
			});

			function activer_intra(){
				$("#cpr_ht_intra").prop("disabled",false);
				$("#cpr_ht_intra").val(ht_intra);
				/*if($("#cpr_min_intra").length>0){
					$("#cpr_min_intra").prop("disabled",false);
					$("#cpr_min_intra").val(min_intra);
				}*/
			}

			function desactiver_intra(){
				$("#cpr_ht_intra").val(0);
				$("#cpr_ht_intra").prop("disabled",true);
				/*if($("#cpr_min_intra").length>0){
					$("#cpr_min_intra").prop("disabled",true);
					$("#cpr_min_intra").val(0);
				}*/
			}

			function activer_inter(){
				$("#cpr_ht_inter").prop("disabled",false);
				$("#cpr_ht_inter").val(ht_inter);
				/*if($("#cpr_min_inter").length>0){
					$("#cpr_min_inter").prop("disabled",false);
					$("#cpr_min_inter").val(min_inter);
				}*/
			}

			function desactiver_inter(){
				$("#cpr_ht_inter").val(0);
				$("#cpr_ht_inter").prop("disabled",true);
				/*if($("#cpr_min_inter").length>0){
					$("#cpr_min_inter").prop("disabled",true);
					$("#cpr_min_inter").val(0);
				}*/
			}

			function afficher_produit(json){

				if(json){

					$("#cpr_libelle").val(json.libelle);

					if($("#derog_niv_0").length>0){
						$("#derog_niv_0").prop("checked",true);
						$("#rem_ca").prop("checked",true);
						$("#rem_famille").prop("checked",true);
						$("#rem_ca").prop("disabled",true);
						$("#rem_famille").prop("disabled",true);
					}

					if(json.intra){
						min_intra=parseFloat(json.min_intra);
						min_dr_intra=parseFloat(json.min_dr_intra);
						ht_intra=parseFloat(json.ht_intra);

						$("#cpr_intra").prop("disabled",false);

						if(json.intra_cli || !json.inter){
							$("#cpr_intra").prop("checked",true);
							activer_intra();
						}else{
							$("#cpr_intra").prop("checked",false);
							desactiver_intra();
						}

					}else{
						min_intra=0;
						min_dr_intra=0;
						ht_intra=0;
						$("#cpr_intra").prop("checked",false);
						$("#cpr_intra").prop("disabled",true);
						desactiver_intra();
					}

					if(json.inter){
						min_dr_inter=parseFloat(json.min_dr_inter);
						min_inter=parseFloat(json.min_inter);
						ht_inter=parseFloat(json.ht_inter);

						$("#cpr_inter").prop("disabled",false);

						if(json.inter_cli || !json.intra){
							$("#cpr_inter").prop("checked",true);
							activer_inter();
						}else{
							$("#cpr_inter").prop("checked",false);
							desactiver_inter();
						}

					}else{
						min_dr_inter=0;
						min_inter=0;
						ht_inter=0;
						$("#cpr_inter").prop("checked",false);
						$("#cpr_inter").prop("disabled",true);
						desactiver_inter();
					}

					if(json.devis_txt_defaut!=""){
						$("#devis_def").html(json.devis_txt_defaut);
					}else{
						$("#devis_def").html("<p class='alert alert-danger' >Pas de valeur par défaut</p>");
					}
					$("#cpr_devis_txt").val(json.devis_txt);

					$("#cpr_comment_txt").val(json.comment_txt);

					if($("#cpr_alerte_jour").length>0){
						$("#cpr_alerte_jour").val(json.alerte_jour);
					}
					if($("#cpr_confirmation_gc").length>0){
						$("#cpr_confirmation_gc").prop("checked",json.confirmation_gc);
					}
					if($("#cpr_cn").length>0){
						$("#cpr_cn").prop("checked",json.cn);
					}
					if($("#cpr_valide").length>0){
						$("#cpr_valide").prop("checked",json.valide);
					}


				/*	if("#cpr_cn").length>0){
						if(json.cn){
							$("#cpr_cn").prop("checked",true);
						}else{
							$("#cpr_cn").prop("checked",false);
						}
					}
					if("#cpr_valide").length>0){
						if(json.valide){
							$("#cpr_valide").prop("checked",true);
						}else{
							$("#cpr_valide").prop("checked",false);
						}
					}*/
				}

			}


		</script>
	</body>
</html>
