<?php 
$tech_li = true;
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';

require "modeles/mod_utilisateur.php";
require "modeles/mod_profil.php";
require "modeles/mod_societe.php";
require "modeles/mod_agence.php";
require "modeles/mod_client.php";
require "modeles/mod_service.php";
require "modeles/mod_document.php";
require "modeles/mod_erreur.php";
require "modeles/mod_produit.php";

$doc = get_document($_GET['id']);
$docs = get_documents2();  
// societes 
$sql="SELECT * FROM Societes WHERE NOT soc_archive ORDER BY soc_id;";
$req=$Conn->query($sql);
$societes=$req->fetchAll();
$profils = get_profil();
$produits = get_produits_code();
if(!empty($_GET['id'])){
    // produits du document
    $sql="SELECT * FROM Produits LEFT JOIN Documents_Produits ON (Documents_produits.dpr_produit = Produits.pro_id) WHERE dpr_document = " . $_GET['id'];
    $req=$Conn->query($sql);
    $produits_selected=$req->fetchAll();

}

	// client selectionné

if(!empty($doc["doc_client"])){
  $req=$Conn->query("SELECT cli_id,cli_code,cli_nom FROM clients WHERE cli_id=" . $doc["doc_client"] . ";");
  $d_client=$req->fetch();
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>SI2P - Orion - Etech</title>
  <meta name="keywords" content=""/>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

  <link href="vendor/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/plugins/select2/css/core.css" rel="stylesheet" type="text/css">

  <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>


  <body class="sb-top sb-top-sm ">
    <!-- Start: Main -->
    <div id="main">
      <?php
      include "includes/header_def.inc.php";
      ?>


      <!-- Start: Content-Wrapper -->
      <section id="content_wrapper" class="" style="margin-bottom:40px;">

        <section id="content" class="animated fadeIn">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="admin-form theme-primary ">
                <div class="panel heading-border panel-primary">
                  <div class="panel-body bg-light">
                    <div class="text-center">

                      <div class="content-header">
                        <h2><?php if(isset($_GET['upload'])): ?>Ajouter<?php else: ?>Editer<?php endif; ?> le document <b class="text-primary"><?= $doc['doc_nom_aff'] ?></b></h2>
                        <h3>Dans: <?= $doc['doc_url']; ?></h3>

                    </div>

                    <div class="col-md-10 col-md-offset-1">
                        <form action="etech_doc_enr.php" method="POST" id="admin-form">
                          <input type="hidden" name="doc_id" value="<?= $_GET['id'] ?>"></input>
                          <?php if(isset($_GET['dos'])): ?>
                            <input type="hidden" name="dos" value="<?= $_GET['dos'] ?>"></input>
                        <?php endif; ?>
                        <?php if(isset($_GET['upload'])): ?>
                            <input type="hidden" name="upload" value="<?= $_GET['upload'] ?>"></input>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-md-12">
                               <div class="section">                              
                                  <select name="doc_url" id="doc_url" class="chosen-select2" required="">
                                     <option value="">Chemin ...</option>
                                     <option value="ETECH/" <?php if($doc['doc_url'] == "ETECH/") echo("selected");?> >ETECH/</option>
                                     <?php 	foreach($docs as $d){ 
                                        if($doc['doc_url'] == $d['doc_url'] . $d['doc_nom_aff'] . "/"){ ?>
                                        <option value="<?=$d['doc_url'] . $d['doc_nom_aff'] . "/"?>" selected ><?= $d['doc_url'] . $d['doc_nom_aff'] . "/" ?></option>
                                        <?php		}else{ ?>
                                        <option value="<?=$d['doc_url'] . $d['doc_nom_aff'] . "/"?>" ><?= $d['doc_url'] . $d['doc_nom_aff'] . "/" ?></option>
                                        <?php		}
                                    } ?>
                                </select>
                                <i class="arrow simple"></i>                              
                            </div>
                        </div>


                        <div class="col-md-12">
                          <div class="section">
                            <div class="field prepend-icon">
                              <input type="text" name="doc_nom_aff" id="doc_nom_aff" class="gui-input" <?php if(isset($_GET['id'])): ?>value="<?= $doc['doc_nom_aff'] ?>"<?php endif; ?> placeholder="Nom d'affichage du fichier" required="">
                              <label for="soc_code" class="field-icon">
                                <i class="fa fa-tag"></i>
                            </label>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6 text-left">
                   <div class="section"> 
                      <label for="doc_type" >Type :</label>
                      <select name="doc_type" class="chosen-select2" id="doc_type">
                         <option value="0">Type ...</option>
                         <?php 	if (isset($_GET['id'])){ ?>
                         <?=get_document_type($doc['doc_type']);?>
                         <?php 	}else{ ?>
                         <?=get_document_type(0);?>
                         <?php 	}?>
                     </select>
                     <i class="arrow simple"></i>                                  
                 </div>
             </div>
             <div class="col-md-6 text-left">
               <div class="section">  
                  <label for="doc_service" >Service :</label>
                  <select name="doc_service" class="chosen-select2" id="doc_service" required>
                     <option value="">Service ...</option>
                     <?php 	if (isset($_GET['id'])): ?>
                        <?=get_service_select($doc['doc_service']);?>
                    <?php 	else: ?>
                        <?=get_service_select(0);?>
                    <?php 	endif;?>
                </select>
                <i class="arrow simple"></i>                           
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-left">														 
           <div class="section" >

              <label for="doc_type" >Client :</label>
              <select name="doc_client" id="doc_client" class="select2 select2-client" data-groupe="1" style="width:100%;" >
                 <option value="0">Client ...</option>
                 <?php	if(isset($d_client)){
                    echo("<option value='" . $d_client["cli_id"] . "' selected >" . $d_client["cli_nom"] . " (" . $d_client["cli_code"] . ")</option>");
                } ?>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
      <div class="section">
        <label class="field prepend-icon">
          <textarea class="gui-textarea" id="doc_mot_cle" name="doc_mot_cle" placeholder="Mots-clés"><?= $doc['doc_mot_cle'] ?></textarea>
          <label for="doc_mot_cle" class="field-icon">
            <i class="fa fa-key"></i>
        </label>
        <span class="input-footer text-left">
            <strong>Note:</strong> Notez les mots-clés pour décrire le fichier.</span>
        </label>
    </div>
</div>
<div class="col-md-6">
    <div class="section">
      <label class="field prepend-icon">
        <textarea class="gui-textarea" id="doc_descriptif" name="doc_descriptif" placeholder="Descriptif"><?= $doc['doc_descriptif'] ?></textarea>
        <label for="doc_descriptif" class="field-icon">
          <i class="fa fa-pencil" aria-hidden="true"></i>
      </label>
      <span class="input-footer text-left">
          <strong>Note:</strong> Ecrivez un paragraphe de quelques lignes pour décrire le fichier.</span>
      </label>
  </div>
</div>
</div>
<div class="row">   
    <div class="col-md-12 mb10">
        <label class="option option-success">
            <input type="checkbox" id="doc_nouveau"  name="doc_nouveau" value="1"
                <?php if(!empty($doc['doc_nouveau'])){ ?>
                    checked
                <?php } ?>
                >
            <span class="checkbox mn"></span> Afficher comme nouveau document
        </label> 
    </div>
</div> 
<div class="row">
    <div class="col-md-12">
      <div class="section-divider mb40">
        <span>Droits par profils</span>
    </div>
</div>
</div>
<div class="row">
    <label class="option option-success">
      <input type="checkbox" id="selectall"  name="mobileos" value="FR">
      <span class="checkbox mn"></span> Sélectionner tous les profils
  </label> 
  <label class="option option-primary">
      <input type="checkbox" id="selectall4"  name="mobileos" value="FR">
      <span class="checkbox mn"></span> Sélectionner tous les profils obligatoires
  </label>
</div>

<div class="row text-left">
 <?php 	foreach($profils as $p){ 
  $doc_pro = get_document_profil( $p['pro_id'], $_GET['id'],0);
  $doc_pro_obligatoire = get_document_profil_obligatoire( $p['pro_id'],$_GET['id'],1); ?>
  <div class="col-md-3 section">
     <div class="option-group field">
        <h2 style="font-size:18px;"><?= $p['pro_libelle'] ?></h2>
        <label class="option option-success" >							
           <input type="checkbox" name="dpr<?= $p['pro_id'] ?>" id="dpr<?= $p['pro_id'] ?>" data-profil="<?=$p['pro_id']?>" <?php if(!empty($doc_pro) OR !empty($doc_pro_obligatoire)): ?>checked<?php endif; ?> class="checkbox1" value="checked">
           <span class="checkbox" data-toggle="tooltip" data-placement="top" title="Accès"></span> Accès
       </label>
       <br><br>
       <label class="option option-primary" >
           <input type="checkbox" class="checkbox4" name="dpr_obligatoire<?= $p['pro_id'] ?>" id="dpr_obligatoire<?= $p['pro_id'] ?>" <?php if(!empty($doc_pro_obligatoire)): ?>checked<?php endif; ?> value="checked">
           <span class="checkbox" data-toggle="tooltip" data-placement="top" title="Consultation obligatoire"></span> Consultation obligatoire
       </label>
   </div>
</div>
<?php 	} ?>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="section-divider mb40">
        <span>Droits par sociétés</span>
    </div>
</div>
</div>
<div class="row">
    <label class="option section" data-toggle="tooltip">
      <input type="checkbox" id="selectall2"  name="mobileos" value="FR">
      <span class="checkbox mn"></span> Sélectionner toutes les sociétés
  </label>
</div>
<div class="row section text-left">

    <?php foreach($societes as $s): ?>
      <?php $doc_soc = get_document_societe( $s['soc_id'],$_GET['id'], 0); ?>
      <div class="col-md-6 section">
        <div class="col-md-12 section">
          <div class="option-group field">

            <label class="option option-info" >

              <input type="checkbox" class="checkbox2" name="dso<?= $s['soc_id'] ?>" id="dso<?= $s['soc_id'] ?>" <?php if(!empty($doc_soc)): ?>checked<?php endif; ?> value="checked">

              <span class="checkbox" data-toggle="tooltip" data-placement="top" title="Accès"></span> 
              <h2 style="color:#3BAFDA;display:inline;font-size:20px;"><?= $s['soc_nom'] ?></h2>
          </label>

      </div>
  </div>
  <?php $agences = get_agence_societe($s['soc_id']); ?>
  <?php foreach($agences as $a): ?>
      <?php $doc_soc_age = get_document_societe( $s['soc_id'],$_GET['id'], $a['age_id']); ?>
      <div class="col-md-10 col-md-offset-1 section">
        <div class="option-group field">
          <label class="option option-success">
            <input type="checkbox" class="checkbox2" name="dso_agence<?= $a['age_id'] ?>" id="dso_agence<?= $a['age_id'] ?>" <?php if(!empty($doc_soc_age)): ?>checked<?php endif; ?> value="checked">
            <span class="checkbox" data-toggle="tooltip" data-placement="top" title="Accès"></span> <h2 style="color:#70CA63;;display:inline;font-size:16px;"><?= $a['age_nom'] ?></h2>
        </label>

    </div>
</div>
<?php endforeach; ?>
</div>
<?php endforeach; ?>

</div>

<div class="row">
    <div class="col-md-12">
      <div class="section-divider mb40">
        <span>Codes produits</span>
    </div>
</div>
</div>

<div class="row">
    <select name="doc_produit[]" multiple class="select2 select2-produit" style="width:100%;">
        <?php foreach($produits_selected as $p){ ?>
        <option value="<?= $p['pro_id'] ?>" selected><?= $p['pro_code_produit'] ?></option>
        <?php }?>

    </select>
</div>
<div class="col-md-12 section" style="margin-top:10px;">
    <div class="option-group field">
      <label class="option">
          <input type="checkbox" name="doc_archive" id="doc_archive" value="doc_archive" <?php if($doc['doc_archive'] == 1): ?> checked <?php endif; ?>>

          <span class="checkbox" data-toggle="tooltip"></span> 
          <h2 style="display:inline">Archivé</h2>

      </label>


  </div>
</div>
</section>
</div>
<?php
if(!empty($_GET["erreur"])){ ?>

<div id="modal-error" class="modal fade" role="dialog" data-show="true" >
  <div class="modal-dialog">      
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title" id="titreModal" >Erreur</h4>
    </div>
    <div class="modal-body">
        <?=get_erreur_txt($_GET["erreur"]);?> 
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
          <i class="fa fa-close" ></i>
          Fermer
      </button>         
  </div>
</div>      
</div>
</div>

<?php 
}; 
?>
<!-- End: Main -->
<footer id="content-footer" class="affix">
    <div class="row">
      <div class="col-xs-3 footer-left" >
        <a <?php if(isset($_GET['dos'])): ?>href="etech.php?dos=<?= $_GET['dos'] ?>"<?php else: ?>href="etech.php"<?php endif; ?>  class="btn btn-default btn-sm">
          <i class="fa fa-long-arrow-left"></i>
          Retour
      </a>
  </div>
  <div class="col-xs-6 footer-middle" ></div>
  <div class="col-xs-3 footer-right" >
    <button type="submit" name="submit2" class="btn btn-success btn-sm">
      <i class='fa fa-save'></i> Enregistrer
  </button>
</div>
</div>
</footer>
</form>
<?php
include "includes/footer_script.inc.php"; ?>	
<!-- validation inputs -->

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script src="assets/js/custom.js"></script>
<style type="text/css">
  .chosen-container:last-of-type .chosen-drop:last-of-type {
    border-bottom: 0;
    border-top: 1px solid #aaa;
    top: auto;
    bottom: 40px;
}
</style>
<script type="text/javascript" >
 jQuery(document).ready(function () {

    $('#loader').hide();
    $(".chosen-select2").select2();
    $(".chosen-select").select2();

								// PROFIL ALL
								$('#selectall').click(function(event) {   
									if(this.checked) {
										// Iterate each checkbox
										$('.checkbox1').each(function() {
											this.checked = true;                        
										});
									}
									if(!this.checked){
										
										$('.checkbox1').each(function() {
											this.checked = false;                        
										});
										
										// obligatoire all
										$("#selectall4").prop("checked",false);
										// obligatoire profil
										$('.checkbox4').each(function() {
											this.checked = false;                        
										});
									}
								});
								
								// PROFIL ALL OBLIGATOIRE
								$('#selectall4').click(function(event) {   
									if(this.checked) {
                                       $('.checkbox4').each(function() {
                                         this.checked = true;                        
                                     });
										// profil accès
										$('.checkbox1').each(function() {
											this.checked = true;                        
										});
										// profil all
										$('#selectall').prop("checked",true);
									}else{
										$('.checkbox4').each(function() {
											this.checked = false;                        
										});                                  
									}
								});


								// PROFIL NO ALL
								$('#selectall2').click(function(event) {   
                                   if(this.checked) {
															// Iterate each checkbox
															$('.checkbox2').each(function() {
                                                                this.checked = true;                        
                                                            });
                                                      }
                                                      if(!this.checked) {
															// Iterate each checkbox
															$('.checkbox2').each(function() {
                                                                this.checked = false;                        
                                                            });
                                                      }
                                                  });

                                $('#selectall3').click(function(event) {   
                                   if(this.checked) {
										// Iterate each checkbox
										$(".chosen-select option").each(function() {
                                          this.setAttribute('selected', true);                       
                                      });
                                   }
                                   if(!this.checked) {
										// Iterate each checkbox
										$(".chosen-select option").each(function() {
                                          this.setAttribute('selected', false);                         
                                      });
                                   }
                               });



                                $(".checkbox1").click(function(){
                                    var profil=$(this).data("profil");
                                    if(!$(this).is(":checked")){
                                       $("#dpr_obligatoire" + profil).prop("checked",false);								
                                   }
                               });


                                <?php foreach($profils as $p): ?>

                                $('#dpr_obligatoire<?= $p['pro_id'] ?>').click(function()
                                  { if($(this).is(':checked')){
                                    $('#dpr<?= $p['pro_id'] ?>').prop('checked', true);     
                                }
                            });


                            <?php endforeach; ?>

                            <?php foreach($societes as $s): ?>

                            $('#dso<?= $s['soc_id'] ?>').click(function()
                              { if($(this).is(':checked')){

                                <?php $agences = get_agence_societe($s['soc_id']); ?>
                                <?php foreach($agences as $a): ?>

                                $('#dso_agence<?= $a['age_id'] ?>').prop('checked', true);
                                if($('#dso_agence<?= $a['age_id'] ?>').is(':checked')){
                                  $('#dso<?= $s['soc_id'] ?>').prop('checked', true);
                              }else{
                                  $('#dso<?= $s['soc_id'] ?>').prop('checked', false);
                              }
                          <?php endforeach; ?>     

                      }
                  });



                        <?php endforeach; ?>

                    });
                </script>
            </body>
            </html>
