<?php
	include "includes/controle_acces.inc.php";
	include "includes/connexion.php";
	include "modeles/mod_parametre.php";

	$mod_menu = 0;
	if(!empty($_GET['menu'])){
		$mod_menu = $_GET['menu'];
	}
	
	// récupérer les modèles de la personne connectée
	$reqmod = $Conn->prepare("SELECT * FROM Modeles WHERE mod_utilisateur = :mod_utilisateur AND mod_menu = :mod_menu");
	$reqmod->bindParam("mod_utilisateur", $_SESSION['acces']['acc_ref_id']);
	$reqmod->bindParam("mod_menu", $mod_menu);
	$reqmod->execute();
	$modele = $reqmod->fetch();
	
	// récupérer les fontctions
	$sql="SELECT * FROM Modeles_fonctions 
	LEFT JOIN modeles_fonctions_profils ON (modeles_fonctions_profils.mfp_fonction = modeles_fonctions.mfo_id) 
	WHERE mfp_profil = :mfp_profil";
	IF($mod_menu>0){
		$sql.="	AND mfo_menu=:menu";
	}
	$sql.=" ORDER BY mfo_nom;";
	$reqfon = $Conn->prepare($sql);
	$reqfon->bindParam("mfp_profil", $_SESSION['acces']['acc_profil']);
	IF($mod_menu>0){
		$reqfon->bindParam("menu",$mod_menu);
	}
	$reqfon->execute();
	$fonctions = $reqfon->fetchAll();

?>
		
<!DOCTYPE html>
<html>
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Modèle de page</title>
	<meta name="keywords" content=""/>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->

	
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

    <link rel="stylesheet" type="text/css" href="vendor/plugins/image-picker/image-picker.css">
	<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />

	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/img/favicon.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

</head>
<style type="text/css">
	.image_picker_selector{
		text-align:center!important;
	}
</style>
<body class="sb-top sb-top-sm ">
	
	<form method="post" action="modele_enr.php" enctype="multipart/form-data" >
		<input type="hidden" name="mod_menu" value="<?= $mod_menu ?>">
		<!-- Start: Main -->
		<div id="main">
		
			<?php
			include "includes/header_def.inc.php";
			?>


			<!-- Start: Content-Wrapper -->
			<section id="content_wrapper" class="">
			
				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-12">
							<div class="admin-form theme-primary ">								
								<div class="panel heading-border panel-primary">
									
									<div class="panel-body bg-light">
										
										<div class="text-center">

											<div class="content-header">
												<?php if(!empty($modele)){ ?>
													<h2>Créer un <b class="text-primary">modèle de page</b></h2>
												<?php }else{ ?>
													<h2>Modifier mon <b class="text-primary">modèle de page</b></h2>
												<?php } ?>
											</div>
										</div>
										
										<div class="row">                      
											<div class="col-md-5 col-md-offset-4  text-center">
												<div class="section text-center">
													<select name="mod_type" id="mod_type" class="text-center">
														<option data-img-src='assets/img/modele1.png' value='1'
														<?php if(!empty($modele) && $modele['mod_type'] == 1){ ?>
															selected
														<?php } ?>
														></option>
														<option data-img-src='assets/img/modele2.png' value='2'
														<?php if(!empty($modele) && $modele['mod_type'] == 2){ ?>
															selected
														<?php } ?>></option>
														<option data-img-src='assets/img/modele3.png' value='3'
														<?php if(!empty($modele) && $modele['mod_type'] == 3){ ?>
															selected
														<?php } ?>></option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<?php for ($i=1; $i < 7; $i++) { ?>
												
											
												<div class="col-md-6 mb10" id="select-fonction-<?= $i ?>">
													<label>Fonction <?= $i ?></label>
													<select name="mod_fct_<?= $i ?>" class="select2">
														<option value="0">Choisir la fonction <?= $i ?></option>
														<?php foreach($fonctions as $f){ ?>
															<option value="<?= $f['mfo_id'] ?>"

																<?php if(!empty($modele) && $modele['mod_fct_' . $i] == $f['mfo_id']){ ?>
																	selected
																<?php } ?>
															>
																<?= $f['mfo_nom'] ?>
															</option>
														<?php } ?>
													</select>
												</div>
											<?php } ?>	
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>					
				</section>
			</section>
		</div>
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left">
					<a href="accueil.php?menu=<?=$mod_menu?>" class="btn btn-default btn-sm">
						<i class="fa fa-long-arrow-left"></i>
						Retour
					</a>
				</div>
				<div class="col-xs-6 footer-middle"></div>
				<div class="col-xs-3 footer-right">
					<button class="btn btn-success btn-sm" >
						 <i class='fa fa-floppy-o'></i> Enregistrer
					</button>
					
				</div>
			</div>
		</footer>
	</form>

<?php
	include "includes/footer_script.inc.php"; ?>	
	<script src="vendor/plugins/select2/js/select2.min.js"></script>
	<script type="text/javascript" src="vendor/plugins/image-picker/image-picker.js"></script>
	<!-- jQuery -->

	<script type="text/javascript">

		jQuery(document).ready(function () {
			
			$("select").imagepicker();

			if($("#mod_type").val() == 1){
				for(i = 1; i < 7; i++) { 
					if(i < 3){
						$("#select-fonction-" + i).show();
					}else{
						$("#select-fonction-" + i).hide();
					}
				}
			}

			if($("#mod_type").val() == 2){
				for(i = 1; i < 7; i++) { 
					if(i < 5){
						$("#select-fonction-" + i).show();
					}else{
						$("#select-fonction-" + i).hide();
					}
				}
			}
			if($("#mod_type").val() == 3){
				for(i = 1; i < 7; i++) { 
					if(i < 7){
						$("#select-fonction-" + i).show();
					}else{
						$("#select-fonction-" + i).hide();
					}
				}
			}

			$("#mod_type").change(function() {
				if($(this).val() == 1){
					for(i = 1; i < 7; i++) { 
						if(i < 3){
							$("#select-fonction-" + i).show();
						}else{
							$("#select-fonction-" + i).hide();
						}
					}
				}

				if($(this).val() == 2){
					for(i = 1; i < 7; i++) { 
						if(i < 5){
							$("#select-fonction-" + i).show();
						}else{
							$("#select-fonction-" + i).hide();
						}
					}
				}
				if($(this).val() == 3){
					for(i = 1; i < 7; i++) { 
						if(i < 7){
							$("#select-fonction-" + i).show();
						}else{
							$("#select-fonction-" + i).hide();
						}
					}
				}
			});

		})		
		</script>
	</body>
</html>

