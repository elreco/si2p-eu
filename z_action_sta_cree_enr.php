 <?php
 
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion.php");
	
	include_once("modeles/mod_add_stagiaire.php");
	
	
	// INSCRIPTION D'UN STAGIAIRE A UNE FORMATION
	
	$erreur_txt="";
	
	$action_id=0;
	if(isset($_POST["action"])){
		if(!empty($_POST["action"])){
			$action_id=intval($_POST["action"]);
		}
	}
	$action_client_id=0;
	if(isset($_POST["sta_action_client"])){
		if(!empty($_POST["sta_action_client"])){
			$action_client_id=intval($_POST["sta_action_client"]);
		}
	}
	
	if(empty($action_id) OR empty($action_client_id)){
		$erreur_txt="Formulaire incomplet!";
	}
	
	/**************************************
			CONTROLE
	*************************************/
	
	if(empty($erreur_txt)){
		
		$session_id=0;
		if(isset($_POST["sta_session"])){
			if(!empty($_POST["sta_session"])){
				$session_id=intval($_POST["sta_session"]);
			}
		}
		
		// SUR LE CLIENT
		$sql="SELECT acl_client FROM Actions_Clients WHERE acl_id=:action_client_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_client_id",$action_client_id);
		$req->execute();
		$d_action_client=$req->fetch();
		if(empty($d_action_client)){
			$erreur_txt="Impossible de charger les données";
		}
		
		
		// info sur l'action
		$sql="SELECT act_gest_sta FROM Actions WHERE act_id=:action_id;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":action_id",$action_id);
		$req->execute();
		$d_action=$req->fetch();
		if(empty($d_action)){
			$erreur_txt="Impossible de charger les données";
		}else{
			if($d_action["act_gest_sta"]==2 AND empty($session_id)){
				$erreur_txt="Impossible de charger la session de formation";
			}
		}
	}
	
	if(empty($erreur_txt)){
		
		$stagiaire_id=0;
		if(isset($_POST["sta_id"])){
			if(!empty($_POST["sta_id"])){
				$stagiaire_id=intval($_POST["sta_id"]);
			}
		}
		
		$sta_nom=$_POST["sta_nom"];
		$sta_prenom=$_POST["sta_prenom"];
		
		if(empty($stagiaire_id) AND (empty($sta_nom) OR empty($sta_prenom) )){
			$erreur_txt="Le nom et le prénom son obligatoire pour créer un nouveau stagiaire!";
		}
		
	}
	
	// FIN DE CONTROLE

	/**************************************
			TRAITEMENT AVANT ENR
	*************************************/
	
	if(empty($erreur_txt)){
		
		$sta_attestation=0;
		if(isset($_POST["sta_attestation"])){
			if(!empty($_POST["sta_attestation"])){
				$sta_attestation=intval($_POST["sta_attestation"]);
			}
		}
		
	}
	
	/**************************************
			ENREGISTREMENT
	*************************************/

	if(empty($erreur_txt)){
		
		// CREATION DU STAGIAIRE
		if(empty($stagiaire_id)){
			
			$stagiaire_id=add_stagiaire($sta_nom,$sta_prenom,$_POST["sta_naissance"],"","","","","","","","","","","",$d_action_client["acl_client"],1);
			if(!is_int($stagiaire_id)){
				$erreur_txt=$stagiaire_id;
				$stagiaire_id=0;
			}
		}
	}
	
	if(empty($erreur_txt) AND $stagiaire_id>0){
		
		// INSCRIPTION DU STAGIAIRE
		
		$sql_get_sta="SELECT ast_stagiaire,ast_action FROM Actions_Stagiaires WHERE ast_stagiaire=:stagiaire_id AND ast_action=:action_id;";
		$req_get_sta=$ConnSoc->prepare($sql_get_sta);
		$req_get_sta->bindParam(":stagiaire_id",$stagiaire_id);
		$req_get_sta->bindParam(":action_id",$action_id);
		$req_get_sta->execute();
		$d_stagiaire=$req_get_sta->fetch();
		if(!empty($d_stagiaire)){
			// LE STAGIIARE EST DEJA INSCRIT
			// -> UPDATE
			$sql_up_sta="UPDATE Actions_Stagiaires SET 
			ast_action_client=:ast_action_client,
			ast_confirme=:ast_confirme,
			ast_attestation=:ast_attestation
			WHERE ast_stagiaire=:ast_stagiaire AND ast_action=:ast_action;";
			$req_up_sta=$ConnSoc->prepare($sql_up_sta);
			$req_up_sta->bindParam(":ast_stagiaire",$stagiaire_id);
			$req_up_sta->bindParam(":ast_action",$action_id);
			$req_up_sta->bindParam(":ast_action_client",$action_client_id);
			$req_up_sta->bindValue(":ast_confirme",1);
			$req_up_sta->bindParam(":ast_attestation",$sta_attestation);
			try{
				$req_up_sta->execute();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
			}
		}else{
			
			// ADD
			
			$sql_add_sta="INSERT INTO Actions_Stagiaires (ast_stagiaire,ast_action,ast_action_client,ast_confirme,ast_attestation) VALUES (:ast_stagiaire,:ast_action,:ast_action_client,:ast_confirme,:ast_attestation);";
			$req_add_sta=$ConnSoc->prepare($sql_add_sta);
			$req_add_sta->bindParam(":ast_stagiaire",$stagiaire_id);
			$req_add_sta->bindParam(":ast_action",$action_id);
			$req_add_sta->bindParam(":ast_action_client",$action_client_id);
			$req_add_sta->bindValue(":ast_confirme",1);
			$req_add_sta->bindParam(":ast_attestation",$sta_attestation);
			try{
				$req_add_sta->execute();
			}Catch(Exception $e){
				$erreur_txt=$e->getMessage();
			}
		}
		
		if(empty($erreur_txt)){
			
			if(empty($session_id)){
				
				$sql="SELECT ase_id Actions_Sessions WHERE ase_action=:action_id;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":ast_action",$action_id);
				$req->execute();
				$d_session=$req->fetchAll();
			
			}else{
				
				$d_session=array();
				$d_session[0]["ase_id"]=$session_id;
			}
			
			// PREP REQUETE
			
			$sql_get_ses="SELECT ass_stagiaire FROM Actions_Stagiaires_Sessions WHERE ass_stagiaire=:stagiaire_id AND ass_session=:session_id;";
			$req_get_ses=$ConnSoc->prepare($sql_get_ses);
			$req_get_ses->bindParam(":stagiaire_id",$stagiaire_id);
		
			$sql_up_ses="UPDATE Actions_Stagiaires_Sessions SET 
			ass_action=:action_id
			WHERE ass_stagiaire=:stagiaire_id AND ass_session=:session_id;";
			$req_up_ses=$ConnSoc->prepare($sql_up_ses);
			$req_up_ses->bindParam(":stagiaire_id",$stagiaire_id);
			$req_up_ses->bindParam(":action_id",$action_id);
			
			$sql_add_ses="INSERT INTO Actions_Stagiaires_Sessions (ass_stagiaire,ass_session,ass_action) VALUES (:stagiaire_id,:session_id,:action_id);";
			$req_add_ses=$ConnSoc->prepare($sql_add_ses);
			$req_add_ses->bindParam(":stagiaire_id",$stagiaire_id);
			$req_add_ses->bindParam(":action_id",$action_id);
			
			
			foreach($d_session as $ses){
				
				$req_get_ses->bindParam(":session_id",$ses["ase_id"]);
				$req_get_ses->execute();
				$existe=$req_get_ses->fetch();
				if(!empty($existe)){
					$req_up_ses->bindParam(":session_id",$ses["ase_id"]);
					$req_up_ses->execute();
				}else{
					$req_add_ses->bindParam(":session_id",$ses["ase_id"]);
					$req_add_ses->execute();
				}
			}
		}
	}
	
	if(!empty($erreur_txt)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_txt
		);
	}
	header("location : action_sta_cree.php?action=" . $action_id . "&action_client=" . $action_client_id . "&session=" . $session_id);

?>