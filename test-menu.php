<?php
$commerce_li = true;
/* variable pour la nav (active) */
include "includes/controle_acces.inc.php";
include_once 'includes/connexion.php';
/* include pour chaque fichier */
?>
<!DOCTYPE html>
<html>
<!-- Mirrored from admindesigns.com/demos/absolute/1.1/layout_horizontal-sm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 09:52:52 GMT -->
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>SI2P - Orion - Script</title>
    <meta name="keywords" content=""/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link rel="stylesheet" type="text/css" href="vendor/plugins/contextMenu/jquery.contextMenu.css">
    <!--
       <link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
       <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
   -->
   <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
   <!-- Favicon -->
   <link rel="shortcut icon" href="assets/img/favicon.png">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<?php
include "includes/header_def.inc.php";
?>
<body class="sb-top sb-top-sm ">
    <section id="content_wrapper">
        <section id="content">
            <p>test</p>
        </section>
    </section>

    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <button class=" btn btn-neutral btn-block" id="context-menu-one">right click me</button>
        </div>
        <div class="col-md-12">
            <button class=" btn btn-neutral btn-block" id="context-menu-two">right click me</button>
        </div>
        <div class="col-md-12">
            <button class=" btn btn-neutral btn-block" id="context-menu-tree">right click me</button>
        </div>
        <div class="col-md-12">
            <button class=" btn btn-neutral btn-block" id="context-menu-four">right click me</button>
        </div>
        <div class="col-md-12">
            <button class=" btn btn-neutral btn-block" id="context-menu-five">right click me</button>
        </div>
        <div class="col-md-12">
            <button class=" btn btn-neutral btn-block" id="context-menu-six">right click me</button>
        </div>
		 
		<div id="context-menu" style="position:absolute;top:100px;left:100px;" >
		
			<ul class="dropdown-menu" style="display:block;" >
				<li class="dropdown-submenu">
					<a tabindex="-1" href="#">Action</a>
					<ul class="dropdown-menu" style="width:300px;" >
						<li class="dropdown-submenu">
							<a tabindex="-1" href="#">Action</a>
							<ul class="dropdown-menu" style="width:300px;" >
								<li style="width:100%;" >
									<div class="row admin-form">
										<div class="section">
											<div class="field prepend-icon">
												<input type="text" name="pca_libelle" class="gui-input input-sm" id="pca_libelle" placeholder="Libellé">
												<label for="pca_libelle" class="field-icon">
													<i class="fa fa-book"></i>
												</label>
											</div>
										</div>
										<div class="section">
											<label class="field select">
												<select id="doc_societe" name="doc_societe" class="empty">
													<option value="0" selected="selected">Société</option>
												</select>
												<i class="arrow double"></i>
											</label>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li style="width:100%;" >
							<div class="row admin-form">
								<div class="section">
									<div class="field prepend-icon">
										<input type="text" name="pca_libelle" class="gui-input input-sm" id="pca_libelle" placeholder="Libellé">
										<label for="pca_libelle" class="field-icon">
											<i class="fa fa-book"></i>
										</label>
									</div>
								</div>
								<div class="section">
									<label class="field select">
										<select id="doc_societe" name="doc_societe" class="empty">
											<option value="0" selected="selected">Société</option>
										</select>
										<i class="arrow double"></i>
									</label>
								</div>
							</div>
						</li>
					</ul>
				</li>
				<li class="dropdown-submenu">
					<a tabindex="-1" href="#">Congés</a>					
				</li>
				<li>
					<a tabindex="-1" href="#">Autres</a>					
				</li>
			</ul>
		</div>
		
		 
  

        
    </div>
        
    </div>
    <div id="html5"  style="display:none">
        <form action="categorie_enr.php" method="POST" id="admin-form">


            <div class="row admin-form">

                <div class="section">
                  <div class="field prepend-icon">
                    <input type="text" name="pca_libelle" class="gui-input input-sm" id="pca_libelle" placeholder="Libellé">
                    <label for="pca_libelle" class="field-icon">
                      <i class="fa fa-book"></i>
                  </label>
              </div>
          </div>
          <div class="section">
              <label class="field select">
                  <select id="doc_societe" name="doc_societe" class="empty">
                    <option value="0" selected="selected">Société</option>
                    

                </select>
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
	
	

</form>
</div>
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-xs-3 footer-left">
            <a href="script.php" class="btn btn-default btn-sm">
                <i class="fa fa-long-arrow-left"></i>
                Retour
            </a>
        </div>
        <div class="col-xs-6 footer-middle"></div>
        <div class="col-xs-3 footer-right">

        </div>
    </div>
</footer>
</form>

<?php
include "includes/footer_script.inc.php"; 
?>	

<script src="vendor/plugins/contextMenu/jquery.contextMenu.min.js"></script>
<script type="text/javascript">

    $(function() {
        $.contextMenu({
            selector: '#context-menu-one', 
            callback: function(key, options) {
                var m = "clicked: " + key;
                window.console && console.log(m) || alert(m); 
            },
            items: {
                "test 1": {name: "Edit", icon: "edit"},
                "test 2": {name: "Cut", icon: "cut"},
               
                
               
                

            }
        });

});
    $(function() {
        $.contextMenu({
            selector: '#context-menu-two', 
            callback: function(key, options) {
                var m = "clicked: " + key;
                window.console && console.log(m) || alert(m); 
            },
            items: {
                "edit": {name: "Edit", icon: "edit"},
                "cut": {name: "Cut", icon: "cut"},
                copy: {name: "Copy", icon: "copy"},
                "fold1": {
                    "name": "Sub group", 
                    "items": $.contextMenu.fromMenu($('#html5')),
                    "fold2": {
                        "name": "Sub group 2", 
                        "items": $.contextMenu.fromMenu($('#html5'))
                    },
                },
                "fold2": {
                "name": "Sub group", 
                "items": {
                    "fold1-key1": {"name": "Foo bar"},
                    "fold2": {
                        "name": "Sub group 2", 
                        "items": $.contextMenu.fromMenu($('#html5'))
                    },
                    "fold1-key3": {"name": "delta"}
                }
            },
                "paste": {name: "Paste", icon: "paste"},
                "delete": {name: "Delete", icon: "delete"},
                "sep1": "---------",
                "quit": {name: "Quit", icon: function(){
                    return 'context-menu-icon context-menu-icon-quit';
                }}
            }
        });

});
       /* $(function(){
            $.contextMenu({
                selector: '#context-menu-two', 
                items: $.contextMenu.fromMenu($('#html5'))
            });
        });


    });
   */ /*/*$(function(){
    /**************************************************
     * Context-Menu with Sub-Menu
     **************************************************/
    /*$.contextMenu({
        selector: '#context-menu-one', 
        callback: function(key, options) {
            var m = "clicked: " + key;
            window.console && console.log(m) || alert(m); 
        },
        items: {
            "edit": {"name": "Edit", "icon": "edit"},
            "cut": {"name": "Cut", "icon": "cut"},
            "sep1": "---------",
            "quit": {"name": "Quit", "icon": "quit"},
            "sep2": "---------",
            "fold1": {
                "name": "Sub group", 
                "items": {
                    "fold1-key1": {"name": "Foo bar"},
                    "fold2": {
                        "name": "Sub group 2", 
                        "items": {
                            "fold2-key1": {"name": "alpha"},
                            "fold2-key2": {"name": "bravo"},
                            "fold2-key3": {"name": "charlie"}
                        }
                    },
                    "fold1-key3": {"name": "delta"}
                }
            },
            "fold1a": {
                "name": "Other group", 
                "items": {
                    "fold1a-key1": {"name": "echo"},
                    "fold1a-key2": {"name": "foxtrot"},
                    "fold1a-key3": {"name": "golf"}
                }
            }
        }
    });
});*/
</script>
</body>
</html>
