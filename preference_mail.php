<?php

// MISE à JOUR PREFERENCE MARKET

include('includes/connexion.php');

$erreur="";
if(!empty($_GET["public"])){
	$public=$_GET["public"];
}else{
	$erreur="Erreur!";
}

if(empty($erreur)){
	
	$sql="SELECT cli_code,cli_nom,con_id,con_nom,con_prenom,con_tel,con_portable,con_fax,con_mail,con_market_all,con_market_no FROM Clients,Contacts WHERE cli_id=con_ref_id AND con_ref=1
	AND con_hash_public=:con_hash_public;";
	$req=$Conn->prepare($sql);
	$req->bindParam("con_hash_public",$public);
	$req->execute();
	$d_client=$req->fetch();
	if(empty($d_client)){
		$erreur="Erreur!";
	}
}
if(empty($erreur)){
	
	// PREFERENCE DE CONTACT
	
	
	$sql="SELECT mac_id,mac_libelle,mac_descriptif,cma_market FROM Marketing_Actions LEFT OUTER JOIN Contacts_Marketing ON (Marketing_Actions.mac_id=Contacts_Marketing.cma_market AND cma_contact=:contact)
	ORDER BY mac_libelle;";
	$req=$Conn->prepare($sql);
	$req->bindParam("contact",$d_client["con_id"]);
	$req->execute();
	$d_market=$req->fetchAll();
	if(empty($d_market)){
		$erreur="Erreur!";
	}

	
}

?>
<!DOCTYPE html>
<html lang="fr">  
	<head>
		<meta charset="utf-8">
		<title>SI2P</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

	
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			.img_rgpd{
				width:600px;
			}
			.logo{
				width:150px;
			}
			.juridique{
				font-size:8pt;
				text-align:justify;
				padding:10px;
			}
		</style>
	</head>
	<body class="sb-l-c">

		<form action="preference_mail_enr.php" method="post" id="form_contact" class="admin-form form-inline form-inline-grid" >
			<div>
				<input type="hidden" name="public" value="<?=$public?>" />			
			</div>
			<div id="main">
				<section id="content_wrapper" class="">
					<section id="content" class="">			
				<?php	if(!empty($erreur)){ ?>
							
							<p class="alert alert-danger" >
								Impossible d'afficher la page !
							</p>
					
				<?php	}else{ ?> 
							
							<div class="row">
								<div class="col-md-8 col-md-offset-2">		
									<img src="https://www.si2p.eu/assets/img/logo_reseau.png" alt="Réseau Si2P" title="Réseau Si2P" class="logo" />
									<div class="admin-form theme-primary ">
										<div class="panel heading-border panel-primary">
									
											<!-- contenu du form -->
											<div class="panel-body bg-light">
											
												
												<div class="content-header">
													<h2>Centre de préférences des <b class="text-primary">mails</b></h2>
												</div>
												
												<div class="row">																		
													<div class="col-md-6">
														<label for="con_nom" >Nom * : </label>
														<input type="text" id="con_nom" name="con_nom" class="gui-input nom" placeholder="Nom" required value="<?=$d_client['con_nom']?>" />
													</div>
													<div class="col-md-6">
														<label for="con_prenom" >Prénom : </label>
														<input type="text" id="con_prenom" name="con_prenom" class="gui-input prenom" placeholder="Prénom" value="<?=$d_client['con_prenom']?>"  />												
													</div>
												</div>												
												<div class="row mt15">
													<div class="col-md-6">
														<label for="con_tel" >Téléphone : </label>
														<label class="field prepend-icon">
															<input type="text" name="con_tel" id="con_tel" class="gui-input telephone" placeholder="Tél" value="<?=$d_client['con_tel']?>" />
															<label for="con_tel" class="field-icon">
																<i class="fa fa-phone"></i>
															</label>
														</label>
													</div>
													<div class="col-md-6">
														<label for="con_portable" >Portable : </label>
														<label class="field prepend-icon">
															<input type="text" name="con_portable" id="con_portable" class="gui-input telephone" placeholder="Portable" value="<?=$d_client['con_portable']?>" />
															<label for="con_portable" class="field-icon">
																<i class="fa fa-mobile"></i>
															</label>
														</label>
													</div>												
												</div>
												<div class="row mt15 mb15">
													<div class="col-md-12">				
														<label for="con_mail" >Mail * : </label>
														<label class="field prepend-icon">
															<input type="email" name="con_mail" id="con_mail" class="gui-input" required placeholder="Mail" value="<?=$d_client['con_mail']?>" />
															<label for="con_mail" class="field-icon">
																<i class="fa fa-envelope-o"></i>
															</label>
														</label>
													</div>
												</div>
												
												<div class="row mt15 mb15">
													<div class="col-md-12">	
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" name="market_all" id="market_all" value="on" <?php if(!empty($d_client['con_market_all'])) echo("checked"); ?>/>
																<span class="checkbox"></span>Je souhaite recevoir toutes les communications du réseau Si2P.
															</label>
														</div>
													</div>
												</div>
												<div class="row mt15 mb15">
													<div class="col-md-12 text-center">														
														OU
													</div>
												</div>
												<div class="row mt15 mb15">
													<div class="col-md-12">														
														Je sélectionne les communications auxquelles je souhaite être abonné(e) :
													</div>
												</div>
									<?php		foreach($d_market as $market){ ?>
													<div class="row mt15 mb15">
														<div class="col-md-12">	
															<div class="option-group field">
																<label class="option option-dark">
																	<input type="checkbox" name="market_<?=$market["mac_id"]?>" value="on" <?php if(!empty($market["cma_market"])) echo("checked") ?> class="choix" />
																	<span class="checkbox"></span><?=$market["mac_libelle"]?>
																</label>
															</div>
														</div>
													</div>
									<?php		} ?>
												<div class="row mt15 mb15">
													<div class="col-md-12 text-center">														
														OU
													</div>
												</div>
												<div class="row mt15 mb15">
													<div class="col-md-12">	
														<div class="option-group field">
															<label class="option option-dark">
																<input type="checkbox" name="market_no" id="market_no" value="on" <?php if(!empty($d_client['con_market_no'])) echo("checked"); ?>/>
																<span class="checkbox"></span>Je souhaite me désinscrire de toutes les communications du réseau Si2P.
															</label>
														</div>
													</div>
												</div>
												<div class="row mt15 mb15" id="info_market_no" style="display:none;" >
													<div class="col-md-12"><b>Nous comprenons votre choix cependant vous pourrez revenir sur votre décision en prenant contact par mail : <a href="mailto:dpo@si2p.fr" >dpo@si2p.fr</a></b></div>													
												</div>
		
											</div>
											<div class="row mt15 mb15">
												<div class="col-md-12 text-center">	
													<button type="submit" class="btn btn-success btn-sm">
														<i class='fa fa-floppy-o'></i> Enregistrer
													</button>
												</div>
											</div>
											<!-- fin de contenu form -->
						

											<div class="juridique" >
												<p>* champ obligatoire.
												<p>
													Les informations recueillies sur ce formulaire sont enregistrées dans un fichier informatisé pour la communication marketing et commerciale sont destinées à l'usage unique du Réseau Si2P.
												</p>
												<p>S’agissant des clients, ces informations sont conservées pendant la durée de la relation contractuelle et pendant 10 ans après la fin de la relation contractuelle.</p>
												<p>S’agissant des prospects, ces informations sont conservées 3 ans à compter de leur collecte ou de notre dernier contact avec vous.</p>
												<p>Conformément à la loi "Informatique et libertés", vous pouvez exercer votre droit d'accès, d'opposition, de modification, de rectification et de suppression des données vous concernant en adressant votre demande à l'adresse <a href="mailto:dpo@si2p.fr" >dpo@si2p.fr</a> ou par courrier : 
												Si2P GFC, Délégué à la Protection des Données, ZA des Hautes Perches, Chemin du Bois, 49610 Saint Melaine Sur Aubance FRANCE. Cette demande devra être accompagnée d'une copie de votre justificatif d'identité.</p>
												
												<p>Notre <a href="https://www.centre-formation-incendie.fr/wp-content/uploads/politique-rgpd.pdf" >politique de protection des données personnelles</a> est disponible à tout moment sur notre site internet <a href="https://www.centre-formation-incendie.fr" >www.centre-formation-incendie.fr</a><p>

												<p>Réseau Si2P © 2018 Tous droits réservés, www.centre-formation-incendie.fr</p>
											</div>

										</div>
									</div>
								</div>
							</div>

			<?php		} ?>
						
						
					</section>
					
				</section>
				
			</div>
		</form>

		<script src="/vendor/jquery/jquery-2.0.0.min.js"></script>
		<script src="/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
		<script src="/assets/js/utility/utility.js"></script>
		<script src="/assets/js/main.js"></script>
		<script type="text/javascript">		
			jQuery(document).ready(function(){
				"use strict";
				// Init Theme Core    
				Core.init();

				$("#market_all").click(function(){
					if($(this).is(":checked")){					
						$("#market_no").prop("checked",false);
						$("#info_market_no").hide();

						$(".choix").prop("checked",false);
			
					}
				});

				$(".choix").click(function(){
					if($(this).is(":checked")){		
						
						$("#market_all").prop("checked",false);	
						$("#market_no").prop("checked",false);
						$("#info_market_no").hide();

					}
				});
				
				$("#market_no").click(function(){
					if($(this).is(":checked")){
						$("#market_all").prop("checked",false);
						$(".choix").prop("checked",false);
						$("#info_market_no").show();
					}else{
						$("#info_market_no").hide();
					}
				});

				
			});
			
		</script>
	</body>
</html>
