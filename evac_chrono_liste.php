<?php
  include "includes/controle_acces.inc.php";
  include('includes/connexion.php');
  // TOUS LES THEMES
  $req=$Conn->query("SELECT * FROM evacuations_chronologies_param ORDER BY ecp_position");
  $d_chronos=$req->fetchAll();
  

?>
<!DOCTYPE html>
<html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Si2P - Paramètres</title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="Si2P">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
    <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
    <link href="assets/admin-tools/admin-forms/css/admin-forms-orion.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
    <link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body class="sb-top sb-top-sm" >

    <!-- Start: Main -->
    <div id="main">
      <?php 
        include "includes/header_def.inc.php";
      ?>  
        
      <!-- Start: Content-Wrapper -->
      <section id="content_wrapper">     
        <!-- Begin: Content -->
              
        <section id="content" class="animated fadeIn">
        
          <div class="row" >
            <div class="col-md-12" >
              <div class="table-responsive" >
                <table class="table table-hover table-striped" >
                  <thead>
                    <tr class="dark" >
                      <th>Position</th>                      
                      <th>Libellé</th>                     
                      <th>&nbsp;</th>
                    </tr>                             
                  </thead>
                  <tbody>
                <?php                       
                    if(!empty($d_chronos)){                    
                      foreach($d_chronos as $value){  ?>
                        <tr>                        
                          <td><?=$value["ecp_position"]?></td>
                          <td><?= $value['ecp_libelle'] ?></td>                                                   
                          <td class="text-center" >
                            <a href="evac_chrono.php?id=<?= $value["ecp_id"] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="Modifier" >
                              <i class="fa fa-pencil"></i>
                            </a>
                          </td>                 
                        </tr> 
                <?php   
                      };
                    }; ?>                                     
                  </tbody>
                </table>
              </div>
            </div>
          </div>
  
        </section>
        <!-- End: Content -->
      </section>
    </div>
    <!-- End: Main -->
    <footer id="content-footer" class="affix" >
      <div class="row">
        <div class="col-xs-3 footer-left" >
          <a href="parametre.php" class="btn btn-default btn-sm" >
            <i class="fa fa-long-arrow-left"></i>
            Retour
          </a>
        </div>
        <div class="col-xs-6">
          
        </div>
        <div class="col-xs-3 footer-right" >
          <a href="evac_chrono.php" class="btn btn-success btn-sm">
            <i class="fa fa-plus" ></i>
            Nouvel élément de chrono
          </a>
        </div>
      </div>
    </footer>
            
  <?php
    include "includes/footer_script.inc.php"; ?>                          

    
    <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
    <script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
    <script type="text/javascript">
   
    </script>
    <!-- END: PAGE SCRIPTS -->
  </body>
</html>
