<?php
$menu_actif = "6-4";
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'includes/connexion_fct.php';


$erreur_txt="";

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
    $acc_agence=$_SESSION['acces']["acc_agence"];    
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
    $acc_societe=$_SESSION['acces']["acc_societe"];  
}
$acc_utilisateur=0;
if($_SESSION['acces']["acc_ref"]==1){
    $acc_utilisateur=$_SESSION['acces']["acc_ref_id"];  
}

// ON LISTE LES SOCIETES DU CONCOURS

$agence=0;
if(!empty($_GET["agence"])){
	$agence=1;
}

$d_societes=array();
$d_agences=array();
$concours_soc=array();

$liste_soc="";
$sql="SELECT soc_id,soc_nom,age_id,age_nom FROM societes LEFT JOIN Agences ON (societes.soc_id=Agences.age_societe)
 WHERE NOT soc_archive AND (soc_intra_inter=1 OR soc_intra_inter=0) AND NOT soc_id=18";
$req=$Conn->query($sql);
$d_r_societes=$req->fetchAll();
if(!empty($d_r_societes)){
	foreach($d_r_societes as $r){
		
		if(empty($d_societes[$r["soc_id"]])){
			$d_societes[$r["soc_id"]]=$r["soc_nom"];
			$liste_soc.=$r["soc_id"] . ",";
		}
		$libelle=$r["soc_nom"];
		
		$age_id=0;
		if($r["soc_id"]==3){
				if(!empty($r["age_id"])){
				$age_id=$r["age_id"];
				$d_agences[$r["age_id"]]=$r["age_nom"];
				$libelle.=" / " . $r["age_nom"];
			}
		}
		
		$cle=$r["soc_id"] . "-" . $age_id;
		
		$concours_soc[$cle]=array(
			"total" => 0,
			"nom" => $libelle,
			"societe" => $r["soc_id"],
			"agence" => $age_id,
			"visite" => 0,
			"devis" => 0,
			"nv_client" => 0,
			"produit" => 0,
			"plannif" => 0,
			"nb_com" => 0
		);
	}
}else{
	$erreur_txt="Aucune société ne correspond aux critères du concours!";
}

$concours_soc["7-0"]["nb_com"]=2;
$concours_soc["3-9"]["nb_com"]=3;
$concours_soc["3-3"]["nb_com"]=3;
$concours_soc["5-0"]["nb_com"]=3;
$concours_soc["2-0"]["nb_com"]=3;
$concours_soc["8-0"]["nb_com"]=1;
$concours_soc["4-0"]["nb_com"]=1;
/*echo("<pre>");
	print_r($concours_soc);
echo("</pre>");
die();*/

// ON MEMORISE LES INFOS DE CHAQUE UTILISATEUR
// meme en mode agence on recherche les utilisateurs pour savoir dans quelle équipe ils jouent

if(empty($erreur_txt)){

	$concours_uti=array();

	$liste_soc=substr($liste_soc,0,-1);

	$sql="SELECT uti_id,uti_nom,uti_prenom,uti_societe,uti_agence FROM Utilisateurs WHERE (uti_societe IN (" . $liste_soc . ") OR uti_id=432) AND NOT uti_archive";
	if(empty($agence)){
		// en indiv seul les com + Patrick Alexandre R et Fabien R jouent. 
		// et Morgan ne joue plus en ind
		$sql.=" AND (uti_profil=3 OR uti_id IN (24,8,11)) AND NOT uti_id=16";
	}
	$sql.=" ORDER BY uti_id;";
	$req=$Conn->query($sql);
	$d_r_uti=$req->fetchAll();
	if(!empty($d_r_uti)){
		foreach($d_r_uti as $k => $r){
			$concours_uti[$r["uti_id"]]=array(
				"total" => 0,
				"nom" => $r["uti_nom"],
				"prenom" => $r["uti_prenom"],
				"societe" => $r["uti_societe"],
				"agence" => $r["uti_agence"],
				"visite" => 0,
				"devis" => 0,
				"nv_client" => 0,
				"plannif" => 0,
				"produit" => 0
				
			);
			if($r["uti_id"]==22){			
				// Jean Maury est GFC mais il joue pour CO #MERCATO
				$concours_uti["22"]["societe"]=3;
				$concours_uti["22"]["agence"]=9;
			}elseif($r["uti_id"]==432){			
				// Patricia L joue pour NN
				$concours_uti["432"]["societe"]=3;
				$concours_uti["432"]["agence"]=3;
			}elseif($r["uti_societe"]!=3){
				$concours_uti[$r["uti_id"]]["agence"]=0;
			}
		}
	}else{
		$erreur_txt="Aucun utilisateur n'est associé au concours!";	
	}
}
/*
echo("<pre>");
	print_r($concours_uti);
echo("</pre>");
die();*/
// LES PRODUITS DU CONCOURS

$sql="SELECT pro_id,pro_pt_0,pro_pt_1,pro_pt_2 FROM Produits WHERE pro_pt_0>0 ORDER BY pro_id;";
$req=$Conn->query($sql);
$d_r_pro=$req->fetchAll();
if(!empty($d_r_pro)){
	foreach($d_r_pro as $p){
		$concours_pro[$p["pro_id"]]=array(
			"0" => $p["pro_pt_0"],
			"1" => $p["pro_pt_1"],
			"2" => $p["pro_pt_2"]
		);
	}
}
		
		
		
if(empty($erreur_txt)){	
	// DONNEE EN BASE 0

	// les correspondances
	
	// correspondance propest uniquement -> OK
	$sql_cor="SELECT cor_id,Clients.cli_id,cor_utilisateur,cor_devis
	,Clients.cli_first_facture
	,Holdings.cli_id
	FROM Clients INNER JOIN Correspondances ON (Clients.cli_id=Correspondances.cor_client)";
	$sql_cor.="INNER JOIN ( 
	SELECT cor_client, MIN(cor_date) AS min_date FROM Correspondances WHERE cor_type=3 GROUP BY cor_client ) AS groupe 
	ON Correspondances.cor_client = groupe.cor_client AND Correspondances.cor_date = groupe.min_date AND Correspondances.cor_type=3";
	$sql_cor.=" LEFT JOIN Clients AS Holdings ON (Clients.cli_filiale_de=Holdings.cli_id)";
	$sql_cor.=" WHERE Correspondances.cor_date>='2019-07-01' AND Correspondances.cor_date<='2019-12-31' AND NOT Clients.cli_categorie=2";
	$sql_cor.=" AND (Clients.cli_first_facture=0 OR ISNULL(Clients.cli_first_facture) OR Clients.cli_first_facture_date>='2019-07-01')"; 
	$sql_cor.=" AND (ISNULL(Holdings.cli_id) OR ISNULL(Holdings.cli_first_facture) OR Holdings.cli_first_facture=0 OR ISNULL(Clients.cli_first_facture) OR Clients.cli_first_facture_date>='2019-07-01')
	AND NOT Clients.cli_hors_concours"; 
	$req_cor=$Conn->query($sql_cor);
	$d_correspondances=$req_cor->fetchAll();
	foreach($d_correspondances as $corresp){
		
		if(!empty($concours_uti[$corresp["cor_utilisateur"]])){
			
			if(empty($agence)){
				// INDIV
				$concours_uti[$corresp["cor_utilisateur"]]["visite"]=$concours_uti[$corresp["cor_utilisateur"]]["visite"]+0.5;
				$concours_uti[$corresp["cor_utilisateur"]]["total"]=$concours_uti[$corresp["cor_utilisateur"]]["total"]+0.5;
				
				if(!empty($corresp["cor_devis"])){
					
					$concours_uti[$corresp["cor_utilisateur"]]["devis"]=$concours_uti[$corresp["cor_utilisateur"]]["devis"]+0.5;
					$concours_uti[$corresp["cor_utilisateur"]]["total"]=$concours_uti[$corresp["cor_utilisateur"]]["total"]+0.5;
					
				}
			}else{
				// AGENCE

				$team=$concours_uti[$corresp["cor_utilisateur"]]["societe"] . "-" . $concours_uti[$corresp["cor_utilisateur"]]["agence"];
				
				$nb_com=$concours_soc[$team]["nb_com"];
				
				$nb_point=0.5/$nb_com;
				
				
				if(!empty($concours_soc[$team])){
					
					$concours_soc[$team]["visite"]=$concours_soc[$team]["visite"]+$nb_point;
					$concours_soc[$team]["total"]=$concours_soc[$team]["total"]+$nb_point;
					
					if(!empty($corresp["cor_devis"])){
						
						$concours_soc[$team]["devis"]=$concours_soc[$team]["devis"]+$nb_point;
						$concours_soc[$team]["total"]=$concours_soc[$team]["total"]+$nb_point;
						
					}
						
				}
				
			}
		}
	}
	
	// les transformation prospect -> client
	
	// facture prospect uniquement OK 
	// la requete retourne les premieres fac quelques soit la société qui l'a généré.
	// mais comme on boucle sur $d_societe -> seule les premières factures des sociétes du concours seront comptabilisé.
	
	$premiere_fac=array();
	
	
	$sql_nv_cli="SELECT Clients.cli_first_facture,Clients.cli_first_facture_soc,Holdings.cli_id FROM Clients LEFT JOIN Clients AS Holdings ON (Clients.cli_filiale_de=Holdings.cli_id) 
	WHERE (Clients.cli_first_facture_date>='2019-07-01' AND Clients.cli_first_facture_date<='2019-12-31') 
	AND (ISNULL(Holdings.cli_id) OR ISNULL(Holdings.cli_first_facture_date) OR (Holdings.cli_first_facture_date>='2019-07-01' AND Holdings.cli_first_facture_date<='2019-12-31' ))
	AND NOT Clients.cli_hors_concours;";
	$req_nv_cli=$Conn->query($sql_nv_cli);
	$d_clients=$req_nv_cli->fetchAll();
	foreach($d_clients as $cli){
		
		if(empty($premiere_fac[$cli["cli_first_facture_soc"]])){
			$premiere_fac[$cli["cli_first_facture_soc"]]=array();
		
		}
		$premiere_fac[$cli["cli_first_facture_soc"]][]=$cli["cli_first_facture"];
		
	}
	
	
	// pour la plannification 2020, il faut identifier les clients jamais facturé (facile avce bool cli_facture ne bse N) 
	// mais nous avons aussi besoin d'identifier les prospects qui sont facturés pour la première fois sur le T1 2020 pour figer la stat
	
	$clients_2020=array();
	
	
	$sql_clients_2020="SELECT Clients.cli_id,Clients.cli_first_facture,Clients.cli_first_facture_soc FROM Clients LEFT JOIN Clients AS Holdings ON (Clients.cli_filiale_de=Holdings.cli_id) 
	WHERE (Clients.cli_first_facture_date>='2020-01-01') 
	AND (ISNULL(Holdings.cli_id) OR ISNULL(Holdings.cli_first_facture_date) OR (Holdings.cli_first_facture_date>='2020-01-01'))
	AND NOT Clients.cli_hors_concours;";
	$req_clients_2020=$Conn->query($sql_clients_2020);
	$clients_2020=$req_clients_2020->fetchAll();
	if(!empty($clients_2020)){
		$tab_src=array_column ($clients_2020,"cli_id");
		$liste_clients_2020=implode($tab_src,",");
	}
	
	//************************************
	// LES DONNEES EN BASE N
	//************************************
	
	foreach($d_societes as $soc => $v){
			
		$ConnFct=connexion_fct($soc);
		
		$sql_sus="SELECT * FROM Suspects_Correspondances INNER JOIN ( 
			SELECT sco_suspect, MIN(sco_date) AS min_date FROM Suspects_Correspondances WHERE sco_type=3 GROUP BY sco_suspect ) AS groupe 
		ON Suspects_Correspondances.sco_suspect = groupe.sco_suspect AND Suspects_Correspondances.sco_date = groupe.min_date AND Suspects_Correspondances.sco_type=3 WHERE sco_date>='2019-07-01' AND sco_date<='2019-12-31'";
		
		//$sql_sus="SELECT * FROM Suspects_Correspondances WHERE sco_type=3";
		$req_sus=$ConnFct->query($sql_sus);
		$d_correspondances=$req_sus->fetchAll();
		foreach($d_correspondances as $corresp){
			if(!empty($concours_uti[$corresp["sco_utilisateur"]])){
				
				if(empty($agence)){
					$concours_uti[$corresp["sco_utilisateur"]]["visite"]=$concours_uti[$corresp["sco_utilisateur"]]["visite"]+0.5;
					$concours_uti[$corresp["sco_utilisateur"]]["total"]=$concours_uti[$corresp["sco_utilisateur"]]["total"]+ 0.5;
				}else{
					
					$team=$concours_uti[$corresp["sco_utilisateur"]]["societe"] . "-" . $concours_uti[$corresp["sco_utilisateur"]]["agence"];
				
					$nb_com=$concours_soc[$team]["nb_com"];			
					$nb_point=0.5/$nb_com;
				
					if(!empty($concours_soc[$team])){
						
						$concours_soc[$team]["visite"]=$concours_soc[$team]["visite"]+$nb_point;
						$concours_soc[$team]["total"]=$concours_soc[$team]["total"]+$nb_point;
						
					}
					
				}
			}
		}
		
		if(!empty($premiere_fac[$soc])){
			
			$liste_fac=implode(",",$premiere_fac[$soc]);
			
			$facture=0;
			
			$sql_fac="SELECT fac_id,fac_agence,com_ref_1,fli_produit,fli_qte,acl_derogation,MONTH(act_date_deb) as mois,acl_pro_inter 
			FROM Factures 
			INNER JOIN Commerciaux ON (Factures.fac_commercial=Commerciaux.com_id)
			INNER JOIN Factures_Lignes ON (Factures.fac_id=Factures_Lignes.fli_facture)
			LEFT JOIN Actions_Clients ON (Factures_Lignes.fli_action_client=Actions_Clients.acl_id AND fli_action_cli_soc=" . $soc . ")
			LEFT JOIN Actions ON (Actions_Clients.acl_action=Actions.act_id)
			WHERE fac_commercial=com_id AND fli_facture=fac_id AND acl_pro_inter=0
			AND com_type=1 AND fac_id IN (" . $liste_fac . ") AND fac_nature=1 AND act_date_deb>'2019-07-01'";
			// securité au cas ou l'action aurait été facturé par anticipation avant le 31/12 alors que l'action à lieu en janvier
			$sql_fac.=" AND act_date_deb<='2019-12-31'";
			
			// on exclut certain commerciaux du concours agence.
			// l'exclusion du concours indiv se fait au niveau de la requete utilisateur
			if($soc==7){
				// EL HOUARI(SO) Caroline
				$sql_fac.=" AND NOT com_id=174";
			}elseif($soc==3){
				// DUTILLY Laurie
				$sql_fac.=" AND NOT com_id=196";
			}elseif($soc==2){
				// CASSUBIE Séverine
				$sql_fac.=" AND NOT com_id=114";
			}
			$sql_fac.= " ORDER BY fac_id;";
			$req_fac=$ConnFct->query($sql_fac);
			$d_factures=$req_fac->fetchAll();
			if(!empty($d_factures)){
				
				foreach($d_factures as $fac){
					
					if($fac["fac_id"]!=$facture){
						
						// nouvelle facture -> on comptabilise les points clients
						
						if(empty($agence)){
							
							// en indiv -> on s'assure que la facture est affecté au commercial -> donc à l'utilisateur
							if(!empty($concours_uti[$fac["com_ref_1"]])){
								$concours_uti[$fac["com_ref_1"]]["nv_client"]=$concours_uti[$fac["com_ref_1"]]["nv_client"]+10;
								$concours_uti[$fac["com_ref_1"]]["total"]=$concours_uti[$fac["com_ref_1"]]["total"]+ 10;
							}
						}else{
							
							if($soc==3){
								$team=$soc . "-" . $fac["fac_agence"];
							}else{
								$team=$soc . "-0";
							}
							
							$nb_com=$concours_soc[$team]["nb_com"];			
							$nb_point=10/$nb_com;
					
							if(!empty($concours_soc[$team])){						
								$concours_soc[$team]["nv_client"]=$concours_soc[$team]["nv_client"]+$nb_point;
								$concours_soc[$team]["total"]=$concours_soc[$team]["total"]+$nb_point;
								
							}
							
						}
						$facture=$fac["fac_id"];
					}
					
					// nouvelle ligne de facture
					
					if(!empty($concours_pro[$fac["fli_produit"]]) AND $fac["acl_pro_inter"]==0){
								
						// le produit fait partie du concours
					
						if(empty($agence)){
							
							// INDIV
							
							if(!empty($concours_uti[$fac["com_ref_1"]])){	
							
								// uti fait partie du concours							
									
								$dero=0;
								if(!empty($fac["acl_derogation"])){
									$dero=$fac["acl_derogation"];
								}
								
							
								$pt=$concours_pro[$fac["fli_produit"]][$dero] * $fac["fli_qte"];								
								if($fac["mois"]==7 OR $fac["mois"]==8 OR $fac["mois"]==12 OR $fac["mois"]==1 OR $fac["mois"]==2){
									$pt=$pt*2;
								}								
								
								$concours_uti[$fac["com_ref_1"]]["produit"]=$concours_uti[$fac["com_ref_1"]]["produit"]+$pt;
								$concours_uti[$fac["com_ref_1"]]["total"]=$concours_uti[$fac["com_ref_1"]]["total"]+$pt;
								
							}
							
						}else{
							
							// AGENCE
							
							if(!empty($concours_soc[$team])){	
							
								// agence fait partie du concours						
									
								$dero=0;
								if(!empty($fac["acl_derogation"])){
									$dero=$fac["acl_derogation"];
								}
								
							
								$pt=$concours_pro[$fac["fli_produit"]][$dero] * $fac["fli_qte"];
								
								if($fac["mois"]==7 OR $fac["mois"]==8 OR $fac["mois"]==12 OR $fac["mois"]==1 OR $fac["mois"]==2){
									$pt=$pt*2;
								}
								
								$nb_com=$concours_soc[$team]["nb_com"];			
								$pt=$pt/$nb_com;
							
								$concours_soc[$team]["produit"]=$concours_soc[$team]["produit"]+$pt;
								$concours_soc[$team]["total"]=$concours_soc[$team]["total"]+$pt;
								
							}
						}
					}
	
				}
			}
		}

		
		// ON AJOUT LES ACTIONS DE PROSPECTS PLANNIFIE SUR LE T1
		
		$sql_fac="SELECT DISTINCT cli_id,com_ref_1,com_agence FROM Actions 
		INNER JOIN Actions_Clients ON (Actions.act_id=Actions_Clients.acl_action)	
		INNER JOIN Clients ON (Clients.cli_id=Actions_Clients.acl_client AND Clients.cli_agence=Actions.act_agence)	
		INNER JOIN Commerciaux ON (Actions_Clients.acl_commercial=Commerciaux.com_id)
		WHERE act_date_deb>='2020-01-01' AND  act_date_deb<='2020-03-31' AND acl_confirme AND acl_confirme_date<='2019-12-31'
		AND com_type=1";
		if(!isset($liste_clients_2020)){
			$sql_fac.=" AND NOT cli_facture";
		}else{
			$sql_fac.=" AND (NOT cli_facture OR cli_id IN (" . $liste_clients_2020 . ") )";
		}
		// on exclut certain commerciaux du concours agence.
		// l'exclusion du concours indiv se fait au niveau de la requete utilisateur
		if($soc==7){
			// EL HOUARI(SO) Caroline
			$sql_fac.=" AND NOT com_id=174";
		}elseif($soc==3){
			// DUTILLY Laurie
			$sql_fac.=" AND NOT com_id=196";
		}elseif($soc==2){
			// CASSUBIE Séverine
			$sql_fac.=" AND NOT com_id=114";
		}
		$sql_fac.= " ORDER BY cli_id;";
		$req_fac=$ConnFct->query($sql_fac);
		$d_actions=$req_fac->fetchAll();
		if(!empty($d_actions)){
				
			foreach($d_actions as $act){
					

				if(empty($agence)){
					
					// en indiv -> on s'assure que la facture est affecté au commercial -> donc à l'utilisateur
					if(!empty($concours_uti[$act["com_ref_1"]])){
						$concours_uti[$act["com_ref_1"]]["plannif"]=$concours_uti[$act["com_ref_1"]]["plannif"]+10;
						$concours_uti[$act["com_ref_1"]]["total"]=$concours_uti[$act["com_ref_1"]]["total"]+ 10;
					}
				}else{
					
					if($soc==3){
						$team=$soc . "-" . $act["com_agence"];
					}else{
						$team=$soc . "-0";
					}
					
					$nb_com=$concours_soc[$team]["nb_com"];			
					$nb_point=10/$nb_com;
			
					if(!empty($concours_soc[$team])){						
						$concours_soc[$team]["plannif"]=$concours_soc[$team]["plannif"]+$nb_point;
						$concours_soc[$team]["total"]=$concours_soc[$team]["total"]+$nb_point;
						
					}
					
				}
			}
		}
	}
	arsort($concours_uti);
	arsort($concours_soc);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion - Marketing client</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="market_suspect_liste.php" id="formulaire" >
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
					
						<div class="content-header">
							<h2>Concours <b class="text-primary">Prospection</b></h2>
						</div>
			<?php		if(empty($agence)){ ?>			
							<div class="row">
								<div class="col-md-10 col-md-offset-1">
									<div class="panel">
										<div class="panel-heading panel-head-sm">Classement individuel</div>
										<div class="panel-body" >
											<table class="table table-striped" >
												<thead>
													<tr>
														<th>Utilisateurs</th>
														<th>Societes</th>
														<th>Agences</th>
														<th class="text-center"  >Points "Visites"</th>
														<th class="text-center"  >Points "Devis"</th>
														<th class="text-center"  >Points "Nouveaux clients"</th>
														<th class="text-center"  >Points "Produits"</th>
														<th class="text-center"  >Points "Plannification T1"</th>
														<th class="text-center" >Total</th>
													</tr>
												</thead>
												<tbody>
									<?php			foreach($concours_uti as $uti_id => $uti){ 
														if($uti["total"]>0){ ?>
															<tr>
																<td><?=$uti["nom"] . " " . $uti["prenom"]?></td>
																<td><?=$d_societes[$uti["societe"]]?></td>
																<td>
										<?php						if(!empty($uti["agence"])){
																		echo($d_agences[$uti["agence"]]);
																	} ?>
																</td>
																<td class="text-right" ><a href="market_concours_visite.php?utilisateur=<?=$uti_id?>" ><?=$uti["visite"]?></a></td>
																<td class="text-right" ><a href="market_concours_visite.php?utilisateur=<?=$uti_id?>" ><?=$uti["devis"]?></a></td>
																<td class="text-right" ><a href="market_concours_client.php?utilisateur=<?=$uti_id?>" ><?=$uti["nv_client"]?></a></td>
																<td class="text-right" >
																	<a href="market_concours_facture.php?utilisateur=<?=$uti_id?>" >
																		<?=$uti["produit"]?>
																	</a>
																</td>
																<td class="text-right" >
																	<a href="market_concours_action.php?utilisateur=<?=$uti_id?>" >
																		<?=$uti["plannif"]?>
																	</a>
																</td>
																<td class="text-right" ><?=$uti["total"]?></td>
															</tr>
									<?php				}
													} ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
			<?php		}else{ ?>		
							<div class="row">
								<div class="col-md-10 col-md-offset-1">
									<div class="panel">
										<div class="panel-heading panel-head-sm">Classement agence</div>
										<div class="panel-body" >
											<table class="table table-striped" >
												<thead>
													<tr>
														<th>Agence</th>												
														<th class="text-center"  >Points "Visites"</th>
														<th class="text-center"  >Points "Devis"</th>
														<th class="text-center"  >Points "Nouveaux clients"</th>
														<th class="text-center"  >Points "Produits"</th>
														<th class="text-center"  >Points "Plannification T1"</th>
														<th class="text-center" >Total</th>
													</tr>
												</thead>
												<tbody>
									<?php			foreach($concours_soc as $team => $soc){ 
														if($soc["total"]>0){ ?>
															<tr>
																<td><?=$soc["nom"]?></td>																
																<td class="text-right" ><a href="market_concours_visite.php?societe=<?=$soc["societe"]?>&agence=<?=$soc["agence"]?>" ><?=number_format($soc["visite"],2,","," ")?></a></td>
																<td class="text-right" ><a href="market_concours_visite.php?societe=<?=$soc["societe"]?>&agence=<?=$soc["agence"]?>" ><?=number_format($soc["devis"],2,","," ")?></a></td>
																<td class="text-right" ><a href="market_concours_client.php?societe=<?=$soc["societe"]?>&agence=<?=$soc["agence"]?>" ><?=number_format($soc["nv_client"],2,","," ")?></a></td>
																<td class="text-right" >
																	<a href="market_concours_facture.php?societe=<?=$soc["societe"]?>&agence=<?=$soc["agence"]?>" >
																		<?=number_format($soc["produit"],2,","," ")?>
																	</a>
																</td>
																<td class="text-right" >
																	<a href="market_concours_action.php?societe=<?=$soc["societe"]?>&agence=<?=$soc["agence"]?>" >
																		<?=number_format($soc["plannif"],2,","," ")?>
																	</a>
																</td>
																<td class="text-right" ><?=number_format($soc["total"],2,","," ")?></td>
															</tr>
									<?php				}
													} ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
			<?php		} ?>
						
					</section>
				</section>
			</div>		
			<!-- End: Main -->
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">

					</div>
					<div class="col-xs-6 footer-middle text-center">
			<?php		if(empty($agence)){ ?>
							<a href="market_concours.php?agence=1" class="btn btn-primary btn-sm">
								<span class="fa fa fa-trophy"></span>
								<span class="hidden-xs">Classement par agence</span>
							</a> 
			<?php		}else{ ?>
							<a href="market_concours.php" class="btn btn-primary btn-sm">
								<span class="fa fa fa-trophy"></span>
								<span class="hidden-xs">Classement individuel</span>
							</a>
			<?php		}?>						
					</div>
					<div class="col-xs-3 footer-right"></div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
				
			});
		</script>
	</body>
</html>
