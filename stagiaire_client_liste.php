<?php  
//////////////// INCLUDES /////////////////////

// SPECIFIQUE CNRS

include "includes/controle_acces_cli.inc.php";
include('includes/connexion.php');
include('includes/connexion_fct.php');
include('modeles/mod_parametre.php'); 
require "modeles/mod_erreur.php";
$_SESSION['retour'] = "stagiaire_client_liste.php";
//stagiaires clients
//sta_client
//sta_cli_code
	//var_dump($_SESSION['acces']['acc_groupe']);

	if(isset($_POST["search"])){

		$sql="SELECT sta_id,sta_nom,sta_prenom, sta_naissance 
		FROM Stagiaires ";
		
		$mil="";
		if(!empty($_POST['sta_nom'])){
			$mil.=" AND sta_nom LIKE '" . $_POST['sta_nom'] . "%'"; 
		};

		if(!empty($_POST['sta_prenom'])){
			$mil.=" AND sta_prenom LIKE '" . $_POST['sta_prenom'] . "%'"; 
		};
		$mil.= " AND sta_client IN(" . $_SESSION['acces']['acc_groupe_liste'] . ")"; 
		if($mil!=""){
			$sql.=" WHERE " . substr($mil, 5, strlen($mil)-5);
		}
		$sql.=" ORDER BY sta_nom,sta_prenom;";
		//echo($sql);
		$_SESSION["sql_sta"]=$sql;
	
	}else{
		$sql=$_SESSION["sql_sta"];
	}
	$req = $Conn->query($sql);
	$stagiaires = $req->fetchAll();
	$_SESSION['stagiaire_liste']=$stagiaires;
		
		
		

	
?>

<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>Si2P - ORION</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="Si2P">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->

<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

<link rel="shortcut icon" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="sb-top sb-top-sm">
    <!-- Start: Main -->
    <div id="main">
<?php	include "includes/header_cli.inc.php";  ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

		
            <!-- Begin: Content -->
            <section id="content" class="animated fadeIn">
			<h1>Liste des stagiaires</h1>
		<?php 	if(isset($_GET['succes']) && $_GET['succes'] == 1){ ?>
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            Le Stagiaire a été créé.
                        </div>
                    </div>
        <?php 	}
				if(isset($_GET['succes']) && $_GET['succes'] == 2){ ?>
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            Le Stagiaire a été modifié.
                        </div>
                    </div>
		<?php 	}
				if(!empty($stagiaires)){ ?>
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="table_id">
							<thead>
								<tr class="dark">
									<th class="no-sort">&nbsp;</th>
									<th>Nom et prénom</th>                                 
									<th>Date de naissance</th>
									<th class="no-sort">Modifier</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($stagiaires as $s){ ?>                      
									<tr data-id="<?= $s['sta_id'] ?>">
										<td>
											<a href="stagiaire_client_voir.php?stagiaire=<?= $s['sta_id'] ?>" class="btn btn-success btn-sm" data-toggle="tooltip" title="Voir toutes les formations suivies" >
												<i class="fa fa-eye"></i>
											</a>
										</td>
										<td><?= $s['sta_nom'] . " " . $s['sta_prenom']?></td>
										<td><?= convert_date_txt($s['sta_naissance']); ?></td>
										
										<td>
											<a href="stagiaire_client.php?stagiaire=<?= $s['sta_id'] ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Modifier les informations du stagiaire" >
												<i class="fa fa-pencil"></i>
											</a>
										</td>
									</tr>

					<?php 		} ?>
							</tbody>
						</table>
					</div>
	<?php 		}else{ ?>
					<div class="col-md-12 text-center" style="padding:0;" >
						<div class="alert alert-warning" style="border-radius:0px;">
						  Aucun stagiaire.
						</div>
					</div>
	<?php 		} ?>
            </section>
        </section>
    </div>
    <!-- End: Main -->
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">
				<a href="stagiaire_client_tri.php" class="btn btn-default btn-sm" role="button">
					<span class="fa fa-search"></span>
					<span class="hidden-xs">Nouvelle recherche</span>
				</a>        
            </div>
            <div class="col-xs-6 footer-middle text-center">
				
			</div>
            <div class="col-xs-3 footer-right">
            
                <a href="stagiaire_client.php" class="btn btn-success btn-sm" role="button">
                    <span class="fa fa-plus"></span>
                    <span class="hidden-xs">Nouveau stagiaire</span>
                </a>
            

            </div>
        </div>
    </footer>
	

<!-- MODAL SUPPRESSION PRODUIT -->
<div id="modal2" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Archiver un stagiaire</h4>
            </div>
            <div class="modal-body">
				Êtes-vous sûr de vouloir archiver ce stagiaire ?
            </div>
            <div class="modal-footer">
                <button type="button" id="delete-btn2" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
            </div>
        </div>

    </div>
</div>
<!-- FIN MODAL SUPPRESSION PRODUIT -->

	
<?php
include "includes/footer_script.inc.php"; ?>  
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
/////////////////////// INTERACTIONS UTILISATEUR /////////////////////////
jQuery(document).ready(function () {
    $(document).on('click', '.btn-delete', function() {
        sta_id = $(this).data("id");
        $("#delete-btn2").data("id", sta_id); 
    }); 
    $(document).on('click', '#delete-btn2', function() {
        sta_id = $(this).data("id");
        del_stagiaire(sta_id);
        $("#modal2").modal('hide');
    }); 
    $('#table_id').DataTable({
        "language": {
        "url": "vendor/plugins/DataTables/media/js/French.json"
        },
        "paging": false,
        "searching": false,
        "info": false,
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false,
        }]
    });
    // fin suppression onclick suppr
});

///////////////////////////// FONCTIONS ////////////////////////////////

function del_stagiaire(sta_id){
    $.ajax({
        type:'POST',
        url: 'ajax/ajax_del_stagiaire.php',                  
        data : 'sta_id=' + sta_id,          
        dataType: 'html',              
        success: function(data)       
        {
            
            $('#table_id > tbody  > tr').each(function() {
                   
                    if($(this).data('id') == sta_id){
                        
                        $(this).remove();
                    }
            });

        }
    });
}
</script>
</body>
</html>
