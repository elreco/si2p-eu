<?php
	include "includes/controle_acces.inc.php";
	include_once("includes/connexion.php");
	include_once("includes/connexion_soc.php");

	include 'modeles/mod_parametre.php';
	include 'modeles/mod_utilisateur.php';
	include 'modeles/mod_acces.php';
	include 'modeles/mod_droit.php';

	// ENREGISTREMENT D'UN NOUVELLE INTERVENANT = LIGNE DE PLANNING

	// PERSONNE CONNECTE
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];	
	}
	$int_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$int_agence=$_SESSION['acces']["acc_agence"];	
	}
	$acc_profil=0;
	if(isset($_SESSION['acces']["acc_profil"])){
		$acc_profil=$_SESSION['acces']["acc_profil"];	
	}

	// TRAITEMENT DES PARAMETRES

	$erreur=0;

	$int_type=0;
	if(isset($_POST["int_type"])){
		$int_type=$_POST["int_type"];
	}

	if(isset($_POST["periode_deb"])){
		if(!empty($_POST["periode_deb"])){
			$periode_deb=$_POST["periode_deb"];
		}else{
			$erreur=0;
		}
	}else{
		$erreur=0;
	}
	if(isset($_POST["periode_fin"])){
		if(!empty($_POST["periode_fin"])){
			$periode_fin=$_POST["periode_fin"];
		}else{
			$erreur=0;
		}
	}else{
		$erreur=0;
	}

	if(!empty($_POST["utilisateur_add"])){
		if(empty($_POST["uti_nom"])){
			$erreur=1;	
		}else{
			$int_label_1=$_POST["uti_nom"];
		}	
		if(empty($_POST["uti_prenom"])){
			$erreur=1;	
		}else{
			$int_label_2=$_POST["uti_prenom"];
		}
	}
		

	if($erreur==0){
		switch ($int_type){
			
			case "1":	
				// FORMATEUR INTERNE
				
				$int_ref_2=0;
				$int_place=0;
				
				if(!empty($_POST["utilisateur"])){
					
						// UTILISATEUR EXISTANT
					
					$int_ref_1=$_POST["utilisateur"];
					
					// on verifie qu'il n'est pas déjà au planning
					$sql="SELECT int_id FROM Intervenants WHERE int_type=1 AND int_ref_1=:int_ref_1;";
					$req=$ConnSoc->prepare($sql);
					$req->bindParam(":int_ref_1",$int_ref_1);
					$req->execute();
					$doublon=$req->fetch();
					if(!empty($doublon)){
						$erreur=970;
					}
					
					if($erreur==0){
					
						$sql="SELECT uti_nom,uti_prenom,uti_agence FROM Utilisateurs WHERE uti_id=:utilisateur;";
						$req=$Conn->prepare($sql);
						$req->bindParam("utilisateur",$_POST["utilisateur"]);
						$req->execute();
						$info_uti=$req->fetch();
						if(!empty($info_uti)){
							$int_agence=$info_uti["uti_agence"];
							$int_label_1=$info_uti["uti_nom"];
							$int_label_2=$info_uti["uti_prenom"];
						}
					}
					
				}elseif(!empty($_POST["utilisateur_add"])){
					
					// AJOUT d'UN UTILISATEUR
					
					// controle pour eviter un doublon
					$sql="SELECT uti_id FROM Utilisateurs WHERE uti_nom LIKE :nom AND uti_prenom LIKE :prenom;";
					$req=$Conn->prepare($sql);
					$req->bindParam("nom",$int_label_1);
					$req->bindParam("prenom",$int_label_2);
					$req->execute();
					$doublon=$req->fetch();
					if(!empty($doublon)){
						$erreur=980;
					};
					
					if($erreur==0){
					
						$uti_titre=0;
						if(!empty($_POST["uti_titre"])){
							$uti_titre=$_POST["uti_titre"];	
						}
						$uti_profil=1;
						
						// le connecte à accès à l'ensemble de la société, il a précisé l'agence de le form de création
						if(isset($_POST["uti_agence"])){
							$int_agence=$_POST["uti_agence"];
						}
						
						$uti_responsable=0;
						$sql="SELECT uti_id FROM Utilisateurs WHERE uti_societe=:societe AND uti_agence=:agence AND uti_profil=5;";
						$req=$Conn->prepare($sql);
						$req->bindParam("societe",$acc_societe);
						$req->bindParam("agence",$int_agence);
						$req->execute();
						$responsable=$req->fetch();
						if(!empty($responsable)){
							$uti_responsable=$responsable["uti_id"];	
						};

						$int_ref_1=insert_utilisateur($uti_titre,$int_label_1,$int_label_2,$_POST["uti_ad1"],$_POST["uti_ad2"],$_POST["uti_ad3"],$_POST["uti_cp"],$_POST["uti_ville"],"","","",$_POST["uti_tel"],$_POST["uti_fax"],$_POST["uti_mobile"],$_POST["uti_mail"],$acc_societe,$int_agence,$uti_profil,0,"Formateur",$uti_responsable);

						// par defaut on affecte l'acces à la société auquel l'utilisateur est rattaché.
					
						insert_acces_societe($int_ref_1,$acc_societe,$int_agence);
					
						// on lui affect les droits par defaut lié à son profil
					
						$droit_par_defaut=get_liste_droit_profil($uti_profil);
						if(!empty($droit_par_defaut)){
							
							foreach($droit_par_defaut as $d){
								
								if(!empty($d["udr_droit"])){
									insert_droit_utilisateur($d["udr_droit"],$int_ref_1,$d["udr_reaffecte"]);
								}
							}
						}
						
						IF($acc_profil!=7 AND $acc_profil!=12){
							
							// CREATION HORS SERVICE RH
							
							$sql="SELECT uti_id FROM utilisateurs WHERE uti_profil IN (7,12) AND NOT uti_archive;";
							$req=$Conn->query($sql);
							$rh=$req->fetchAll();
							if(!empty($rh)){
								
								$not_icone="<i class='fa fa-user' ></i>";
								$not_texte="Le compte ". $int_label_1 . " " . $int_label_2 . " a été crée";
								$not_classe="bg-success";
								$not_url="utilisateur_voir.php?utilisateur=" . $int_ref_1;
								
								$sql="INSERT INTO Notifications (not_utilisateur,not_icone,not_texte,not_vu,not_classe,not_url,not_date)";
								$sql.=" VALUE (:not_utilisateur, :not_icone, :not_texte, false, :not_classe, :not_url, NOW())";
								$req = $Conn->prepare($sql);
								foreach($rh as $r){
									$req ->bindParam(":not_utilisateur",$r["uti_id"]);
									$req ->bindParam(":not_icone",$not_icone);
									$req ->bindParam(":not_texte",$not_texte);
									$req ->bindParam(":not_classe",$not_classe);
									$req ->bindParam(":not_url",$not_url);
									$req->execute();
								}
							}
						}
					}
				}else{
					$erreur=1;
				}
				
				break;
				
			case "2": 
				
				// INTERCO
				
				if(!empty($_POST["societe"])){
					$int_ref_1=$_POST["societe"];
				}else{
					$erreur=0;
				}
				$int_ref_2=0;
				if(!empty($_POST["agence"])){
					$int_ref_2=$_POST["agence"];
				}
				if(!empty($_POST["interco_label"])){
					$int_label_1=$_POST["interco_label"];
				}else{
					$erreur=0;
				}
				
			case "0":
				
				// AUTRES	

				$int_ref_1=0;
				$int_ref_2=0;
				if(!empty($_POST["autre_label"])){
					$int_label_1=$_POST["autre_label"];
				}else{
					$erreur=0;
				}
				$int_label_2="";
				break;
		}
		
		// FIN GESTION Type
		
		// ON AJOUTE L'INTERVENANT
		
		if($erreur==0){
			
			$sql="INSERT INTO Intervenants (int_type,int_ref_1,int_ref_2,int_agence,int_label_1,int_label_2)";
			$sql.=" VALUE (:int_type, :int_ref_1, :int_ref_2, :int_agence, :int_label_1, :int_label_2)";
			$req = $ConnSoc->prepare($sql);
			$req ->bindParam(":int_type",$int_type);
			$req ->bindParam(":int_ref_1",$int_ref_1);
			$req ->bindParam(":int_ref_2",$int_ref_2);
			$req ->bindParam(":int_agence",$int_agence);
			$req ->bindParam(":int_label_1",$int_label_1);
			$req ->bindParam(":int_label_2",$int_label_2);
			$req->execute();
			$int_id=$ConnSoc->lastInsertId();

		}
		
		// ON INSCRIT L'INTERVENANT AU PLANNING
		
		if($erreur==0){
		
			$date_deb = new DateTime(convert_date_sql($periode_deb));
			$date_fin = new DateTime(convert_date_sql($periode_fin));
			WHILE($date_deb<$date_fin){

				$semaine=$date_deb->format("W");
				$annee=$date_deb->format("Y");

				$place=0;
				$sql="SELECT MAX(pin_place) FROM Plannings_Intervenants,Intervenants WHERE int_id=pin_intervenant";
				$sql.=" AND int_type=:int_type AND pin_semaine=:semaine AND pin_annee=:annee;";		
				$req = $ConnSoc->prepare($sql);
				$req ->bindParam(":int_type",$int_type);
				$req ->bindParam(":semaine",$semaine);
				$req ->bindParam(":annee",$annee);
				$req->execute();
				$p=$req->fetch();
				if(!empty($p)){
					$place=$p[0];
				}
				$place++;
				
				$sql="INSERT INTO Plannings_Intervenants (pin_intervenant,pin_semaine,pin_annee,pin_place)";
				$sql.=" VALUE (:pin_intervenant, :pin_semaine, :pin_annee, :pin_place)";
				$req = $ConnSoc->prepare($sql);
				$req ->bindParam(":pin_intervenant",$int_id);
				$req ->bindParam(":pin_semaine",$semaine);
				$req ->bindParam(":pin_annee",$annee);
				$req ->bindParam(":pin_place",$place);
				$req->execute();
				
				$date_deb->add(new DateInterval('P7D'));		
			}
		
		}
	}
	header('Location: planning.php?erreur=' . $erreur);
?>