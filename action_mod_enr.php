 <?php

	// ENREGISTREMENT DES MODIFICATIONS PROPRE A UNE ACTION DE FORMATION

	include "includes/controle_acces.inc.php";
	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");

	include 'modeles/mod_add_notification.php';


	$erreur=0;
	$erreur_text="";
	$warning_text="";

	$action_id=0;
	if(isset($_POST["action"])){
		if(!empty($_POST["action"])){
			$action_id=$_POST["action"];
		}
	}
	if(empty($action_id)){
		$erreur_text="Formulaire incomplet!";
	}

	if(empty($erreur_text)){

		// RECUPERATION DU FORM

		$adr_ref=0;
		if(!empty($_POST["adr_ref"])){
			$adr_ref=intval($_POST["adr_ref"]);
		}
		$adr_ref_id=0;
		if($adr_ref==1){
			if(!empty($_POST["client"])){
				$adr_ref_id=intval($_POST["client"]);
			}
		}
		$adresse=0;
		if(!empty($_POST["adresse"])){
			$adresse=intval($_POST["adresse"]);
		}
		$adr_nom="";
		if(!empty($_POST["adr_nom"])){
			$adr_nom=$_POST["adr_nom"];
		}
		$adr_service="";
		if(!empty($_POST["adr_service"])){
			$adr_service=$_POST["adr_service"];
		}
		$adr1="";
		if(!empty($_POST["adr1"])){
			$adr1=$_POST["adr1"];
		}
		$adr2="";
		if(!empty($_POST["adr2"])){
			$adr2=$_POST["adr2"];
		}
		$adr3="";
		if(!empty($_POST["adr3"])){
			$adr3=$_POST["adr3"];
		}
		$adr_cp="";
		if(!empty($_POST["adr_cp"])){
			$adr_cp=$_POST["adr_cp"];
		}
		$adr_ville="";
		if(!empty($_POST["adr_ville"])){
			$adr_ville=$_POST["adr_ville"];
		}
		$contact=0;
		if(!empty($_POST["contact"])){
			$contact=intval($_POST["contact"]);
		}
		$contact_autre=0;
		if(!empty($_POST["contact_autre"])){
			$contact_autre=1;
		}
		$con_nom="";
		if(!empty($_POST["con_nom"])){
			$con_nom=$_POST["con_nom"];
		}
		$con_prenom="";
		if(!empty($_POST["con_prenom"])){
			$con_prenom=$_POST["con_prenom"];
		}
		$con_tel="";
		if(!empty($_POST["con_tel"])){
			$con_tel=$_POST["con_tel"];
		}
		$con_portable="";
		if(!empty($_POST["con_portable"])){
			$con_portable=$_POST["con_portable"];
		}

		$vehicule=0;
		if(!empty($_POST["vehicule"])){
			$vehicule=intval($_POST["vehicule"]);
		}
		$salle=0;
		if(!empty($_POST["salle"])){
			$salle=intval($_POST["salle"]);
		}

		$alerte_jour=0;
		if(!empty($_POST["alerte_jour"])){
			$alerte_jour=intval($_POST["alerte_jour"]);
		}
		$alerte_ok=0;
		if(!empty($_POST["alerte_ok"])){
			$alerte_ok=1;
		}

		$dossier_ok=0;
		if(!empty($_POST["dossier_ok"])){
			$dossier_ok=1;
		}
		$dossier_date=null;
		if(!empty($_POST["dossier_date"])){
			$dt_dossier = date_create_from_format('d/m/Y',$_POST["dossier_date"]);
			$dossier_date=$dt_dossier->format("Y-m-d");
		}

		$registre=0;
		if(!empty($_POST["registre"])){
			$registre=1;
		}
		$registre_signe=0;
		if(!empty($_POST["registre_signe"])){
			$registre_signe=1;
		}
		$planning_txt="";
		if(!empty($_POST["planning_txt"])){
			$planning_txt=$_POST["planning_txt"];
		}
		$commentaire="";
		if(!empty($_POST["commentaire"])){
			$commentaire=$_POST["commentaire"];
		}

		$forprev_num="";
		if(!empty($_POST["forprev_num"])){
			$forprev_num=$_POST["forprev_num"];
		}

		/*$date_h_def_1=0;
		if(!empty($_POST["date_h_def_1"])){
			$date_h_def_1=$_POST["date_h_def_1"];
		}
		$date_h_def_2=0;
		if(!empty($_POST["date_h_def_2"])){
			$date_h_def_2=$_POST["date_h_def_2"];
		}*/

		$gest_sta=1;
		if(!empty($_POST["gest_sta"])){
			$gest_sta=intval($_POST["gest_sta"]);
		}

		$archive=0;
		$archive_date=null;
		$archive_uti=0;
		$archive_ok=0;
		if(!empty($_POST["act_archive"])){
			$archive=1;
		}

		// FIN DU FORM


		/*****************************************
			TRAITEMENT AVANT ENREGISTREMENT
		*****************************************/

		// personne connecté

		$acc_societe=0;
		if(isset($_SESSION['acces']["acc_societe"])){
			if(!empty($_SESSION['acces']["acc_societe"])){
				$acc_societe=$_SESSION['acces']["acc_societe"];
			}
		}
		$acc_agence=0;
		if(isset($_SESSION['acces']["acc_agence"])){
			if(!empty($_SESSION['acces']["acc_agence"])){
				$acc_agence=$_SESSION['acces']["acc_agence"];
			}
		}
		$acc_utilisateur=0;
		if(!empty($_SESSION['acces']["acc_ref"])){
			if($_SESSION['acces']["acc_ref"]==1){
				$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
			}
		}
		if($conn_soc_id!=$acc_societe){
			$acc_societe=$conn_soc_id;
			$acc_agence=0;
		}


		// SUR L'ACTION

		$req=$ConnSoc->prepare("SELECT act_date_deb,act_agence,act_vehicule,act_salle,act_gest_sta,act_archive,act_archive_date,act_archive_uti,act_archive_ok,
		act_pro_famille,act_pro_sous_famille,act_pro_sous_sous_famille,act_produit,act_pro_type,act_adr_ville
		FROM Actions WHERE act_id=:action");
		$req->bindParam(":action",$action_id);
		$req->execute();
		$d_action=$req->fetch();
		if(empty($d_action)){
			$erreur_text="L'action demandée n'a pas été trouvé.";
		}else{
			if(!$_SESSION["acces"]["acc_droits"][10] OR ($d_action["act_archive_ok"] AND !$_SESSION["acces"]["acc_droits"][11]) OR ($d_action["act_archive"]==1 AND $archive==1) ){
				$archive=$d_action["act_archive"];
				$archive_date=$d_action["act_archive_date"];
				$archive_uti=$d_action["act_archive_uti"];
				$archive_ok=$d_action["act_archive_ok"];
			}elseif($archive==1){
				$archive_date=date("Y-m-d");
				$archive_uti=$acc_utilisateur;
				if($_SESSION["acces"]["acc_droits"][11]){
					$archive_ok=1;
				}else{
					$archive_ok=0;
				}
			}
		}

		// ON RECUPERE LES DATES DE FORMATIONS

		$sql="SELECT pda_date,pda_demi,pda_intervenant FROM Plannings_Dates WHERE pda_type=1 AND pda_ref_1=" . $action_id . ";";
		$req=$ConnSoc->query($sql);
		$d_dates=$req->fetchAll();

		// TENTATIVE DE DESARCHIVAGE
		if(empty($erreur_text)){

			if($archive==0 AND $d_action["act_archive"]==1){

				// on vérifie que les dates sont libres.
				$sql="SELECT pda_id FROM Plannings_Dates WHERE NOT pda_archive=1 AND (NOT pda_type=1 OR (pda_type=1 AND NOT pda_ref_1=" . $action_id  . "))";
				if(!empty($d_dates)){
					$sql.=" AND (";
					foreach($d_dates as $d){
						$sql.=" (pda_date='" . $d["pda_date"] . "' AND pda_demi=" . $d["pda_demi"] . " AND pda_intervenant=" . $d["pda_intervenant"] . ") OR";
					}
					$sql=substr($sql,0,-3);
					$sql.=" )";
					var_dump($sql);
					$req=$ConnSoc->query($sql);
					$d_result=$req->fetchAll();
					if(!empty($d_result)){
						$erreur_text="Certaines dates ne sont plus disponobles";
					}

				}

			}
		}

		// CONTROLE EN FONCTION DES DATES
		if(empty($erreur_text)){

			// on tente d'affecter un véhicule à toutes les dates
			if(!empty($vehicule) AND empty($d_action["act_vehicule"])){

				// ON S'ASSURE QUE LE VEHICULE EST LIBRE

				$sql="SELECT pda_id FROM Plannings_Dates WHERE pda_vehicule=" . $vehicule . " AND (NOT pda_type=1 OR (pda_type=1 AND NOT pda_ref_1=" . $action_id  . "))";
				if(!empty($d_dates)){
					$sql.=" AND (";
					foreach($d_dates as $d){
						$sql.=" (pda_date=" . $d["pda_date"] . " AND pda_demi=" . $d["pda_demi"] . ") OR";
					}
					$sql=substr($sql,0,-3);
					$sql.=" )";
					$req=$ConnSoc->query($sql);
					$d_result=$req->fetchAll();
					if(!empty($d_result)){
						$warning_text.="Le vehicule selectionné n'est pas disponible sur la totalité de la formation.<br/>";
						$vehicule=0;
					}

				}
			}

			// on tente d'affecter une salle à toutes les dates
			if(!empty($salle) AND empty($d_action["act_salle"])){

				// ON S'ASSURE QUE LE SALLE EST LIBRE

				$sql="SELECT pda_id FROM Plannings_Dates WHERE pda_salle=" . $salle . " AND (NOT pda_type=1 OR (pda_type=1 AND NOT pda_ref_1=" . $action_id  . "))";
				if(!empty($d_dates)){
					$sql.=" AND (";
					foreach($d_dates as $d){
						$sql.=" (pda_date=" . $d["pda_date"] . " AND pda_demi=" . $d["pda_demi"] . ") OR";
					}
					$sql=substr($sql,0,-3);
					$sql.=" )";
					$req=$ConnSoc->query($sql);
					$d_result=$req->fetchAll();
					if(!empty($d_result)){
						$warning_text.="La salle selectionnée n'est pas disponible sur la totalité de la formation.<br/>";
						$salle=0;
					}
				}
			}

		}
	}

	if(empty($erreur_text)){



		// date d'alerte
		if(!empty($alerte_jour)){
			$dt_alert = DateTime::createFromFormat('Y-m-d',$d_action["act_date_deb"]);
			$dt_alert->modify('-' . $alerte_jour . ' day');
			$alerte_date=$dt_alert->format('Y-m-d"');
		}

		// adresse

		if($adr_ref==2){
			// agence



			if(!empty($d_action["act_agence"])){

				$sql="SELECT age_nom,age_ad1,age_ad2,age_ad3,age_cp,age_ville,soc_nom FROM Agences,Societes WHERE age_societe=soc_id AND age_id=:acc_agence;";
				$req=$Conn->prepare($sql);
				$req->bindParam(":acc_agence",$d_action["act_agence"]);
				$req->execute();
				$d_agence=$req->fetch();
				if(!empty($d_agence)){
					$adr_nom=$d_agence["soc_nom"] . " " . $d_agence["age_nom"];
					$adr_service="";
					$adr1=$d_agence["age_ad1"];
					$adr2=$d_agence["age_ad2"];
					$adr3=$d_agence["age_ad3"];
					$adr_cp=$d_agence["age_cp"];
					$adr_ville=$d_agence["age_ville"];
				}

			}else{
				$sql="SELECT soc_nom,soc_ad1,soc_ad2,soc_ad3,soc_cp,soc_ville FROM Societes WHERE soc_id=:acc_societe;";
				$req=$Conn->prepare($sql);
				$req->bindParam(":acc_societe",$acc_societe);
				$req->execute();
				$d_societe=$req->fetch();
				if(!empty($d_societe)){
					$adr_nom=$d_societe["soc_nom"];
					$adr_service="";
					$adr1=$d_societe["soc_ad1"];
					$adr2=$d_societe["soc_ad2"];
					$adr3=$d_societe["soc_ad3"];
					$adr_cp=$d_societe["soc_cp"];
					$adr_ville=$d_societe["soc_ville"];
				}
			}
		}
	}

	/******************************
		ENREGISTREMENT
	******************************/

	if(empty($erreur_text)){

		// MAJ DES DONNEES ACTIONS

		$sql="UPDATE Actions SET
		act_adr_ref=:adr_ref,
		act_adr_ref_id=:adr_ref_id,
		act_adresse=:adresse,
		act_adr_nom=:adr_nom,
		act_adr_service=:adr_service,
		act_adr1=:adr1,
		act_adr2=:adr2,
		act_adr3=:adr3,
		act_adr_cp=:adr_cp,
		act_adr_ville=:adr_ville,
		act_contact=:contact,
		act_con_nom=:con_nom,
		act_con_prenom=:con_prenom,
		act_con_tel=:con_tel,
		act_con_portable=:con_portable,
		act_vehicule=:vehicule,
		act_salle=:salle,
		act_alerte_jour=:alerte_jour,
		act_alerte_date=:alerte_date,
		act_alerte_ok=:alerte_ok,
		act_dossier_ok=:dossier_ok,
		act_dossier_date=:dossier_date,
		act_registre=:registre,
		act_registre_signe=:registre_signe,
		act_planning_txt=:planning_txt,
		act_commentaire=:commentaire,
		act_forprev_num=:forprev_num,
		act_gest_sta=:gest_sta,
		act_archive=:archive,
		act_archive_date=:archive_date,
		act_archive_uti=:archive_uti,
		act_archive_ok=:archive_ok
		WHERE act_id=:action;";
		$req=$ConnSoc->prepare($sql);
		$req->bindParam(":adr_ref",$adr_ref);
		$req->bindParam(":adr_ref_id",$adr_ref_id);
		$req->bindParam(":adresse",$adresse);
		$req->bindParam(":adr_nom",$adr_nom);
		$req->bindParam(":adr_service",$adr_service);
		$req->bindParam(":adr1",$adr1);
		$req->bindParam(":adr2",$adr2);
		$req->bindParam(":adr3",$adr3);
		$req->bindParam(":adr_cp",$adr_cp);
		$req->bindParam(":adr_ville",$adr_ville);
		$req->bindParam(":contact",$contact);
		$req->bindParam(":con_nom",$con_nom);
		$req->bindParam(":con_prenom",$con_prenom);
		$req->bindParam(":con_tel",$con_tel);
		$req->bindParam(":con_portable",$con_portable);
		$req->bindParam(":vehicule",$vehicule);
		$req->bindParam(":salle",$salle);
		$req->bindParam(":alerte_jour",$alerte_jour);
		if(isset($alerte_date)){
			$req->bindParam(":alerte_date",$alerte_date);
		}else{
			$req->bindValue(":alerte_date",null);
		}
		$req->bindParam(":alerte_ok",$alerte_ok);

		$req->bindParam(":dossier_ok",$dossier_ok);
		$req->bindParam(":dossier_date",$dossier_date);


		$req->bindParam(":registre",$registre);
		$req->bindParam(":registre_signe",$registre_signe);
		$req->bindParam(":planning_txt",$planning_txt);
		$req->bindParam(":commentaire",$commentaire);
		$req->bindParam(":forprev_num",$forprev_num);
		$req->bindParam(":gest_sta",$gest_sta);
		$req->bindParam(":archive",$archive);
		$req->bindParam(":archive_date",$archive_date);
		$req->bindParam(":archive_uti",$archive_uti);
		$req->bindParam(":archive_ok",$archive_ok);
		$req->bindParam(":action",$action_id);
		try{
			$req->execute();
		}Catch(Exception $e){
			$erreur_text="UPDATE ACTION " . $e->getMessage();
		}
	}

	if(empty($erreur_text)){

		if($d_action["act_adr_ville"]!=$adr_ville AND $d_action["act_pro_type"]==1){

			// changement de lieu d'intervention sur une intra -> actualisation du text survol

			$texte_survol="";

			$sql_pro="SELECT pro_code_produit FROM Produits WHERE pro_id=:produit;";
			$req_pro=$Conn->prepare($sql_pro);
			$req_pro->bindParam(":produit",$d_action["act_produit"]);
			$req_pro->execute();
			$d_produit=$req_pro->fetch();
			if(!empty($d_produit)){

				$texte_survol=$d_produit["pro_code_produit"] . " " . $adr_ville;

			}
		}

		// ON MET A JOUR LE CONTENU DES CASES DU PLANNINGS

		$sql_date="UPDATE Plannings_Dates SET pda_texte = :planning_txt, pda_archive=:archive";
		if(!empty($vehicule)){
			$sql_date.=",pda_vehicule=:vehicule";
		}
		if(!empty($salle)){
			$sql_date.=",pda_salle=:salle";
		}
		if($d_action["act_adr_ville"]!=$adr_ville AND $d_action["act_pro_type"]==1){
			$sql_date.=",pda_survol=:texte_survol";
		}
		$sql_date.=" WHERE pda_type=1 AND pda_ref_1=:action_id";
		$req_date=$ConnSoc->prepare($sql_date);
		$req_date->bindParam(":planning_txt",$planning_txt);
		$req_date->bindParam(":archive",$archive);
		$req_date->bindParam(":action_id",$action_id);
		if(!empty($vehicule)){
			$req_date->bindParam(":vehicule",$vehicule);
		}
		if(!empty($salle)){
			$req_date->bindParam(":salle",$salle);
		}
		if($d_action["act_adr_ville"]!=$adr_ville AND $d_action["act_pro_type"]==1){
			$req_date->bindParam(":texte_survol",$texte_survol);
		}
		try{
			$req_date->execute();
		}Catch(Exception $e){
			$warning_text.="Les dates de formations n'ont pas été actualisées!" . $e->getMessage();
		}

		// MAJ DE L'ARCHIVAGE

		if($archive!=$d_action["act_archive"]){

			if($archive==1){

				// ON ENREGISTRE LES NOTIFICATIONS
				if(!$archive_ok){

					$form_nom="";
					if(!empty($d_action["act_pro_sous_sous_famille"])){
						$req=$Conn->query("SELECT pss_libelle FROM Produits_Sous_Sous_Familles WHERE pss_id=" . $d_action["act_pro_sous_sous_famille"]);
						$d_fam=$req->fetch();
						if(!empty($d_fam)){
							$form_nom=$d_fam["pss_libelle"];
						}
					}elseif(!empty($d_action["act_pro_sous_famille"])){
						$req=$Conn->query("SELECT psf_libelle FROM Produits_Sous_Familles WHERE psf_id=" . $d_action["act_pro_sous_famille"]);
						$d_fam=$req->fetch();
						if(!empty($d_fam)){
							$form_nom=$d_fam["psf_libelle"];
						}
					}elseif(!empty($d_action["act_pro_famille"])){
						$req=$Conn->query("SELECT pfa_libelle FROM Produits_Familles WHERE pfa_id=" . $d_action["act_pro_famille"]);
						$d_fam=$req->fetch();
						if(!empty($d_fam)){
							$form_nom=$d_fam["pfa_libelle"];
						}
					}
					$dt_notif = DateTime::createFromFormat('Y-m-d',$d_action["act_date_deb"]);
					$notif_date=$dt_notif->format('Y-m-d');

					$texte=$_SESSION["acces"]["acc_prenom"] . " " . $_SESSION["acces"]["acc_nom"] . " a archivé la formation " . $form_nom . " du " . $notif_date . " N°" . $action_id;
					$not_url="action_voir.php?action=" . $action_id;
					$sql="SELECT uti_responsable FROM utilisateurs WHERE uti_id = :uti_id";
					$req=$Conn->prepare($sql);
					$req->bindParam(":uti_id",$_SESSION["acces"]["acc_ref_id"]);
					$req->execute();
					$uti_co=$req->fetch();

					$sql="SELECT * FROM utilisateurs WHERE uti_id = :uti_id AND uti_id != 98";
					$req=$Conn->prepare($sql);
					$req->bindParam(":uti_id",$uti_co['uti_responsable']);
					$req->execute();
					$responsable=$req->fetch();

					if(!empty($responsable)){
						$sql="SELECT * FROM utilisateurs_droits WHERE udr_utilisateur = :uti_id AND udr_droit = 11";
						$req=$Conn->prepare($sql);
						$req->bindParam(":uti_id",$responsable['uti_id']);
						$req->execute();
						$responsable_droit=$req->fetch();

						if(!empty($responsable_droit)){
							add_notifications("<i class='fa fa-times' ></i>",$texte,"bg-danger",$not_url,"","",$responsable['uti_id'],$acc_societe,$d_action["act_agence"],1);
						}else{
							$sql="SELECT * FROM utilisateurs WHERE uti_responsable = :uti_id AND uti_id != 98";
							$req=$Conn->prepare($sql);
							$req->bindParam(":uti_id",$responsable['uti_id']);
							$req->execute();
							$responsable_bis=$req->fetch();
							add_notifications("<i class='fa fa-times' ></i>",$texte,"bg-danger",$not_url,"","",$responsable_bis['uti_id'],$acc_societe,$d_action["act_agence"],1);
						}
					}
					//add_notifications("<i class='fa fa-times' ></i>",$not_texte,"bg-danger",$not_url,"",11,"",$acc_societe,$d_action["act_agence"],0);
				}
                // Selection des actions
                $sql="SELECT * FROM Actions_Clients WHERE acl_action=:action_id";
                $req=$ConnSoc->prepare($sql);
                $req->bindParam(":action_id",$action_id);
                $req->execute();
                $actions_clients_exists = $req->fetchAll();
                // Suppression interco
                foreach($actions_clients_exists as $ace){
                    $sql="DELETE FROM Actions_Clients_Intercos WHERE aci_action_client=:aci_action_client;";
                    $req=$ConnSoc->prepare($sql);
                    $req->bindParam(":aci_action_client",$ace['acl_id']);
                    $req->execute();
                }
                // Selection des actions
                $sql="SELECT soc_id FROM Societes WHERE soc_archive = 0";
                $req=$Conn->prepare($sql);
                $req->execute();
                $d_societes = $req->fetchAll();
                // Suppression interco
                foreach($d_societes as $s){
                    $ConnFct=connexion_fct($s['soc_id']);
                    $sql="DELETE FROM Actions_Clients_Intercos WHERE aci_action_interco=:aci_action_client_interco AND aci_action_client_interco_soc = :soc;";
                    $req=$ConnFct->prepare($sql);
                    $req->bindParam(":aci_action_client_interco",$action_id);
                    $req->bindParam(":soc",$_SESSION['acces']['acc_societe']);
                    $req->execute();
                }

				// ON ARCHIVE LES ACTIONS CLIENTS
				$sql_ac="UPDATE Actions_Clients SET acl_archive=:archive, acl_archive_date=:archive_date, acl_archive_uti=:archive_uti, acl_archive_ok=:archive_ok
				WHERE acl_action=:action_id AND NOT acl_archive";
				$req_ac=$ConnSoc->prepare($sql_ac);
				$req_ac->bindParam(":action_id",$action_id);
				$req_ac->bindParam(":archive",$archive);
				$req_ac->bindParam(":archive_date",$archive_date);
				$req_ac->bindParam(":archive_uti",$archive_uti);
				$req_ac->bindParam(":archive_ok",$archive_ok);
				try{
					$req_ac->execute();
				}Catch(Exception $e){
					$warning_text.="Les inscriptions clients n'ont pas été archivées!" . $e->getMessage();
				}


			}else{
				// ON DESARCHIVE
			}

		}

		// MODIFICATION DE LA GESTION STAGIAIRE
		if($d_action["act_gest_sta"]!=$gest_sta){

			if($gest_sta==1){

				// -> on passe en participation complete

				$ast_stagiaire=0;

				$sql="SELECT ast_stagiaire,ase_id
				FROM Actions_Stagiaires INNER JOIN Actions_Clients_Dates ON (Actions_Stagiaires.ast_action_client=Actions_Clients_Dates.acd_action_client)
				INNER JOIN Actions_Sessions ON (Actions_Clients_Dates.acd_date=Actions_Sessions.ase_date)
				WHERE ast_action=:action_id ORDER BY ast_stagiaire;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":action_id",$action_id);
				$req->execute();
				$d_sessions=$req->fetchAll();
				if(!empty($d_sessions)){

					$sql_ses="INSERT INTO Actions_Stagiaires_Sessions (ass_stagiaire,ass_session,ass_action) VALUES (:ass_stagiaire,:ass_session,:action_id);";
					$req_ses=$ConnSoc->prepare($sql_ses);
					$req_ses->bindParam(":action_id",$action_id);

					foreach($d_sessions as $d_ses){
						$req_ses->bindParam(":ass_stagiaire",$d_ses["ast_stagiaire"]);
						$req_ses->bindParam(":ass_session",$d_ses["ase_id"]);
						try{
							$req_ses->execute();
						}Catch(Exception $e){

						}

						if($d_ses["ast_stagiaire"]!=$ast_stagiaire){
							$ast_stagiaire=$d_ses["ast_stagiaire"];

							// changement de durée on supp l'attestation
							if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Attestations/" . $acc_societe . "/attestation_". $action_id . "_" . $ast_stagiaire . ".pdf")){
								unlink($_SERVER["DOCUMENT_ROOT"] . "/Documents/Attestations/" . $acc_societe . "/attestation_". $action_id . "_" . $ast_stagiaire . ".pdf");
							}
						}
					}
				}

			}else{

				// -> on passe en participation session
				// -> on inscrit tous les stagiaires sur la première session du client

				$sql="SELECT ast_stagiaire,ast_action_client FROM Actions_Stagiaires WHERE ast_action=:action_id ORDER BY ast_stagiaire;";
				$req=$ConnSoc->prepare($sql);
				$req->bindParam(":action_id",$action_id);
				$req->execute();
				$d_stagiaires=$req->fetchAll();
				if(!empty($d_stagiaires)){

					$sql_ses="INSERT INTO Actions_Stagiaires_Sessions (ass_stagiaire,ass_session,ass_action, ass_signature) VALUES (:stagiaire_id,:session_id,:action_id, :ass_signature);";
					$req_ses=$ConnSoc->prepare($sql_ses);
					$req_ses->bindParam(":action_id",$action_id);

		            // c'est ici que les signatures sont supprimées ==> pas bon
                    $sql_ass="SELECT * FROM Actions_Stagiaires_Sessions WHERE ass_action=:action_id AND ass_stagiaire=:stagiaire_id;";
					$req_ass=$ConnSoc->prepare($sql_ass);
					$req_ass->bindParam(":action_id",$action_id);

					$sql_del="DELETE FROM Actions_Stagiaires_Sessions WHERE ass_action=:action_id AND ass_stagiaire=:stagiaire_id;";
					$req_del=$ConnSoc->prepare($sql_del);
					$req_del->bindParam(":action_id",$action_id);

					$sql_get="SELECT ase_id FROM Actions_Clients_Dates INNER JOIN Plannings_Dates ON (Actions_Clients_Dates.acd_date=Plannings_Dates.pda_id)
					INNER JOIN Actions_Sessions ON (Actions_Clients_Dates.acd_date=Actions_Sessions.ase_date)
					WHERE acd_action_client=:acd_action_client ORDER BY pda_date,pda_demi,ase_h_deb;";
					$req_get=$ConnSoc->prepare($sql_get);

					foreach($d_stagiaires as $d_sta){

						// pour chaque stagiaire

						// -> on recup la première session

						$session_id=0;

						$req_get->bindParam(":acd_action_client",$d_sta["ast_action_client"]);
						$req_get->execute();
						$d_session=$req_get->fetchAll();
						if(!empty($d_session)){
							$session_id=$d_session[0]["ase_id"];
						}


						if($session_id>0){
                            $req_ass->bindParam(":stagiaire_id",$d_sta["ast_stagiaire"]);
                            $req_ass->execute();
                            $d_ass = $req_ass->fetch();
                            // on delete toutes les sessions
							$req_del->bindParam(":stagiaire_id",$d_sta["ast_stagiaire"]);
							$req_del->execute();

							// on insert juste la première session
							$req_ses->bindParam(":stagiaire_id",$d_sta["ast_stagiaire"]);
							$req_ses->bindParam(":session_id",$session_id);
                            $req_ses->bindParam(":ass_signature",$d_ass['ass_signature']);
							$req_ses->execute();

						}

						// changement de durée on supp l'attestation
						if(file_exists($_SERVER["DOCUMENT_ROOT"] . "/Documents/Attestations/" . $acc_societe . "/attestation_". $action_id . "_" . $d_sta["ast_stagiaire"] . ".pdf")){
							unlink($_SERVER["DOCUMENT_ROOT"] . "/Documents/Attestations/" . $acc_societe . "/attestation_". $action_id . "_" . $d_sta["ast_stagiaire"] . ".pdf");
						}
					}

				}
			}

			// ON RECALCUL NE NOMBRE DE D'INSCRIT PAR DATE

			$sql="SELECT DISTINCT ast_confirme,COUNT(ast_stagiaire),ase_date
			FROM Actions_Stagiaires INNER JOIN Actions_Stagiaires_Sessions ON (Actions_Stagiaires.ast_stagiaire=Actions_Stagiaires_Sessions.ass_stagiaire)
			INNER JOIN Actions_Sessions ON (Actions_Stagiaires_Sessions.ass_session=Actions_Sessions.ase_id)
			WHERE ast_action=:action_id AND ass_action=:action_id
			GROUP BY ast_confirme,ase_date;";
			$req=$ConnSoc->prepare($sql);
			$req->bindParam(":action_id",$action_id);
			$req->execute();
			$d_inscrits=$req->fetchAll();

		}

    }

	if(!empty($erreur_text)){
		$_SESSION['message'][] = array(
			"titre" => "Erreur",
			"type" => "danger",
			"message" => $erreur_text
		);
		if(!empty($action_id)){
			header("location : action_mod.php?action=" . $action_id . "&societ=" . $conn_soc_id);
			die();
		}else{
			header("location : action_tri.php");
			die();
		}
	}else{
		if(!empty($warning_text)){
			$_SESSION['message'][] = array(
				"titre" => "Modification partielle",
				"type" => "warning",
				"message" => $warning_text
			);
		}else{
			$_SESSION['message'][] = array(
				"titre" => "Enregistrement terminé",
				"type" => "success",
				"message" => "L'action a bien été modifiée!"
			);
		}
		header("location : action_voir.php?action=" . $action_id . "&societ=" . $conn_soc_id);
		die();

	}
?>
