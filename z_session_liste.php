<?php  
//////////////// INCLUDES /////////////////////
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php'); 
require "modeles/mod_erreur.php";
//////////////// REQUETES VERS LE SERVEUR /// ///////////////

$_SESSION["retour_formation"]="session_liste.php";

$critere=array(
	"ses_produit" => 0,
	"ses_date_deb" => "",
	"ses_date_fin" => "",
	"ses_lieu" => "",
	"filtre" => 0
);

if(isset($_GET["raz"])){
	
	if(isset($_SESSION["critere_session"])){
		unset($_SESSION["critere_session"]);
	}
	
}elseif(!empty($_POST)){
	
	if(!empty($_POST['ses_produit'])){
		$critere["filtre"]=1;
		$critere["ses_produit"]=intval($_POST['ses_produit']);
	}
	if(!empty($_POST['ses_date_deb'])){
		$critere["filtre"]=1;
		$critere["ses_date_deb"]=$_POST['ses_date_deb'];
	}
	if(!empty($_POST['ses_date_fin'])){
		$critere["filtre"]=1;
		$critere["ses_date_fin"]=$_POST['ses_date_fin'];
	}
	if(!empty($_POST['ses_lieu'])){
		$critere["filtre"]=1;
		$critere["ses_lieu"]=$_POST['ses_lieu'];
	}
	$_SESSION["critere_session"]=$critere;
	
}elseif(isset($_SESSION["critere_session"])){
	
	$critere=$_SESSION["critere_session"];
}

$sql="SELECT * FROM sessions WHERE ses_groupe = " . $_SESSION['acces']['acc_holding'];
if(!empty($critere["ses_date_deb"])){
	$sql.=" AND ses_date>='" . convert_date_sql($critere["ses_date_deb"]) . "'";
};
if(!empty($critere["ses_date_fin"])){
	 $sql.=" AND ses_date<='" . convert_date_sql($critere["ses_date_fin"]) . "'";
};
if(!empty($critere["ses_lieu"])){
	$sql.=" AND ses_lieu LIKE '" . $critere["ses_lieu"] . "%'"; 
};
if(!empty($critere["ses_produit"])){
	$sql.=" AND ses_produit=" . $critere["ses_produit"]; 
};
$sql.=" ORDER BY ses_date DESC,ses_demi DESC;";
$req = $Conn->query($sql);
$sessions = $req->fetchAll();

// PRODUITS DISPONIBLE
$req = $Conn->query("SELECT cpr_id,cpr_libelle FROM Cnrs_Produits ORDER BY cpr_libelle;");
$d_produits=$req->fetchAll();
	

?>

<!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>Si2P - ORION</title>
<meta name="keywords" content=""/>
<meta name="description" content="">
<meta name="author" content="Si2P">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Theme CSS -->

<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

<link rel="shortcut icon" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="sb-top sb-top-sm">
    <div id="main">
<?php	include "includes/header_cli.inc.php"; ?>
        <section id="content_wrapper">
			
			<header id="topbar">
                <div class="text-center">
                    <h4 style="margin:0;">Liste des <small>formations</small></h4>
                </div>
            </header>
            
			<section id="content" class="animated fadeIn">
		<?php 	if(isset($_GET['succes']) && $_GET['succes'] == 1){ ?>
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            La formation a été créée.
                        </div>
                    </div>
		<?php 	}
				if(isset($_GET['succes']) && $_GET['succes'] == 2){ ?>
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            Le formation a été modifiée.
                        </div>
                    </div>
		<?php 	} ?>
				
				<!-- FORMULAIRE RECHERCHE -->
                <form action="session_liste.php" method="POST">
					<div class="row mb20 admin-form">
						<div class="col-md-2">
							<label for="fpr_type">Date du :</label>                      
							<input type="text" name="ses_date_deb" class="gui-input date datepicker" placeholder="Date du" value="<?=$critere["ses_date_deb"]?>" />
						</div>
						<div class="col-md-2">
					   
							<label for="fpr_type">Au :</label>
							
							<input type="text" name="ses_date_fin" class="gui-input date datepicker" placeholder="Au" value="<?=$critere["ses_date_fin"]?>" />
						</div>
						<div class="col-md-2">
							<label for="fpr_type">Produit :</label>
							<select name="ses_produit" class="form-control" >
								<option value="" >Produit...</option>
					<?php		if(!empty($d_produits)){
									foreach($d_produits as $p){
										if($p["cpr_id"]==$critere["ses_produit"]){
											echo("<option value='" . $p["cpr_id"] . "' selected >" . $p["cpr_libelle"] . "</option>");
										}else{
											echo("<option value='" . $p["cpr_id"] . "' >" . $p["cpr_libelle"] . "</option>");
										}
									}
								} ?>
							 </select>
						</div>
						<div class="col-md-3">
							<label for="fpr_type">Lieu :</label>
							<input type="text" name="ses_lieu" class="gui-input" placeholder="Lieu" value="<?=$critere["ses_lieu"]?>" />
						</div>
						<div class="col-sm-1">
							<button type="submit" name="search_produit" class="btn btn-primary mt20" data-toggle="tooltip" title="trier">
								Filtrer
							</button>
						</div>
				<?php 	if($critere["filtre"]==1){ ?>
							<div class="col-sm-2">
								<a href="session_liste.php?raz" class="btn btn-info mt20" data-toggle="tooltip" title="Effacer tous les critères de selection" >
									Remettre à zéro
								</a>
							</div> 
                <?php	} ?>
					</div>
				</form>
				<!-- FIN FORMULAIRE RECHERCHE --> 
				
		<?php 	if(!empty($sessions)){ ?>
			
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="table_id">
							<thead>
								<tr class="dark">
									<th>ID</th>
									<th>Date</th>
									<th>Demi-journée</th>
									<th>Lieu</th>
									<th>Formation</th>
									<th class="text-center" >Feuille de présence</th>
									<th class="text-center" >Attestations</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
					<?php 	foreach($sessions as $s){ ?>                     
								<tr>
									<td>
										<?= $s['ses_id'] ?>
									</td>
									<td>
									   <?= convert_date_fr($s['ses_date']); ?>
									</td>
									<td>
					<?php				if($s['ses_demi']==1){
											echo("Matin");
										}elseif($s['ses_demi']==2){
											echo("Après-midi");
										}elseif($s['ses_demi']==3){
											echo("Toute la journée");
										} ?>
									</td>
									<td>
										<?= $s['ses_lieu']; ?>
									</td>
									 <td>
										<?= $s['ses_formation']; ?>
									</td>
									<td class="text-center" >
							<?php		if(file_exists("documents/sessions_stagiaires/" . $s['ses_id'] . "." . $s['ses_ext'])){
											$download_nom="Présence " . $s['ses_formation'] . " " . convert_date_txt($s['ses_date']); ?>
											<a href="documents/sessions_stagiaires/<?= $s['ses_id'] ?>.<?= $s['ses_ext'] ?>" download="<?=$download_nom?>" data-toggle="tooltip" title="Télécharger la feuille de présence" class="btn btn-success btn-sm">
												<i class="fa fa-file-pdf-o"></i>
											</a>
							<?php		}else{
											echo("<span class='text-danger' >Non disponible</span>");
										} ?>
									</td>
									<td class="text-center" >
							<?php		if(file_exists("documents/attestations/". $s['ses_id'] . "/conso_" . $s['ses_id'] . ".pdf")){
											$download_nom="Attestation groupée " . $s['ses_formation'] . " " . convert_date_txt($s['ses_date']); ?>
											<a href="documents/attestations/<?= $s['ses_id'] ?>/conso_<?= $s['ses_id'] ?>.pdf" download="<?=$download_nom?>" data-toggle="tooltip" title="Télécharger l'attestation groupée" class="btn btn-success btn-sm">
												<i class="fa fa-file-pdf-o"></i>
											</a>
							<?php		}else{
											echo("<span class='text-danger' >Non disponible</span>");
										} ?>
									</td>
									<td>
										<a data-toggle="tooltip" title="Modifier la session" href="session.php?id=<?= $s['ses_id'] ?>" class="btn btn-warning btn-sm">
											<i class="fa fa-pencil"></i>
										</a>
										<a href="session_stagiaire.php?id=<?= $s['ses_id'] ?>" class="btn btn-success btn-sm" data-toggle="tooltip" title="Voir la liste des inscrits">
											<i class="fa fa-eye"></i>
										</a>
										<span data-toggle="tooltip" title="Supprimer la formation et toutes les données associées">
											<a href="#" data-toggle="modal" data-target="#modalsuppr" data-id="<?= $s['ses_id'] ?>" class="btn btn-danger btn-sm delete" >
												<i class="fa fa-times"></i>
											</a>
										</span>
									</td>
								</tr>
					<?php 	} ?>
						</tbody>
					</table>
				</div>
	<?php 	}else{ ?>
				<div class="col-md-12 text-center" style="padding:0;" >
					<div class="alert alert-warning" style="border-radius:0px;">
					  Aucune session.
					</div>
				</div>
	<?php 	} ?>
            </section>
        </section>
    </div>
    <footer id="content-footer" class="affix">
        <div class="row">
            <div class="col-xs-3 footer-left">
              
              
            </div>
            <div class="col-xs-6 footer-middle">&nbsp;</div>
            <div class="col-xs-3 footer-right">
            
                <a href="session.php" class="btn btn-success btn-sm" role="button" data-toggle="tooltip" title="Accéder à l'écran de création d'une formation" >
                    <span class="fa fa-plus"></span>
                    <span class="hidden-xs" >Nouvelle formation</span>
                </a>
            

            </div>
        </div>
    </footer>
	
	<!-- MODAL SUPPRESSION PRODUIT -->
	<div id="modalsuppr" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Supprimer une session</h4>
				</div>

				<div class="modal-body">
					Êtes-vous sûr de vouloir supprimer cette session ?

				</div>

				<div class="modal-footer">
					<a href="" id="delete-btn" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Oui</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
				</div>
			</div>

		</div>
	</div>
<!-- FIN MODAL SUPPRESSION PRODUIT -->
    <style type="text/css">
        #table_id_filter label .form-control{
            width:300px;
        }

    </style>
<?php
include "includes/footer_script.inc.php"; ?>  
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
/////////////////////// INTERACTIONS UTILISATEUR /////////////////////////
jQuery(document).ready(function () {
    $(document).on('click', '.last_session', function() {
        sta_id = $(this).data("id");
        get_sessions(sta_id);
    }); 
    // fin suppression onclick suppr
    $(document).on('click', '.delete', function() {
        $('#delete-btn').attr("href","session_delete.php?id=" + $(this).data('id'));
    }); 
    /*$('#table_id').DataTable({
        "language": {
        "url": "/vendor/plugins/DataTables/media/js/French.json",
        searchPlaceholder: "Rechercher une formation (exemple : 04 Avril 2017)"
        },
        "aaSorting": []

    });*/

    $(".datepicker").datepicker({
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        showButtonPanel: false,
        beforeShow: function(input, inst) {
            var newclass = 'admin-form';
            var themeClass = $(this).parents('.admin-form').attr('class');
            var smartpikr = inst.dpDiv.parent();
            if (!smartpikr.hasClass(themeClass)) {
                inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
            }
        }
    });
});


///////////////////////////// FONCTIONS ////////////////////////////////

function get_sessions(sta_id){
    $.ajax({
        type:'POST',
        url: 'ajax/ajax_sessions_stagiaires.php',                  
        data : 'sta_id=' + sta_id,          
        dataType: 'json',              
        success: function(data)       
        {
            console.log(data);
            $.each(data, function(key, value) {
                $('.body-liste').append($("<li></li>").text(value['ses_date'] + " de " + value['ses_h_deb'] + " à " + value['ses_h_fin']));
            });

        }
    });
}


</script>
</body>
</html>
