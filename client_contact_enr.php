<?php
include "includes/controle_acces.inc.php";
include("includes/connexion.php");
include("includes/connexion_soc.php");

include_once 'modeles/mod_envoi_mail.php';



// MAJ D'UN CONTACT client

$client=0;
if(!empty($_POST["client"])){
	$client=intval($_POST["client"]);	
}
$contact=0;
if(!empty($_POST["contact"])){
	$contact=intval($_POST["contact"]);	
}

if(empty($client)){
	echo("Erreur!");
	die();
}


$erreur_txt="";

if(empty($_POST['con_nom'])){
	
	$erreur_txt="Impossible de mettre à jour le contact";
	
}else{
	
	// TRAITEMENT DU FORM

	$con_titre=0;
	if(!empty($_POST['con_titre'])){
		$con_titre=intval($_POST['con_titre']);
	}

	$con_fonction=0;
	$con_fonction_nom="";
	if(!empty($_POST['con_fonction'])){
		if($_POST['con_fonction']=="autre"){
			$con_fonction_nom=$_POST['con_fonction_nom'];
		}else{
			$con_fonction=intval($_POST['con_fonction']);
		}
	}

	
	/*$con_adresse=0;
	if(!empty($_POST["con_adresse"])){
		$con_adresse=intval($_POST['con_adresse']);	
		
	}*/
	
	$con_compta=0;
	if(!empty($_POST["con_compta"])){
		$con_compta=1;	
	}

	// FIN TRAITEMENT FORM
	
	// CONTROLE
	
	// contact avant modif
	
	/*if($contact>0){
		
		$sql="SELECT con_adresse FROM contacts WHERE con_id=" . $contact . ";";
		$req = $Conn->query($sql);
		$d_contact_old=$req->fetch();
		if(empty($d_contact_old)){
			unset($d_contact_old);
		}
		
	}*/

	
	/*if($con_adresse>0){
		
		if($con_defaut==0){
			
			// si adresse lié n'a pas de contact on force adr_defaut
			$sql="SELECT con_id FROM contacts WHERE con_adresse=" . $con_adresse . " AND con_defaut AND NOT con_id=" . $contact . ";";
			$req = $Conn->query($sql);
			$d_contact_defaut=$req->fetch();
			if(empty($d_contact_defaut)){
				$con_defaut=1;	
			}
		}
		
		// info sur adresse associé au contact
		$sql="SELECT adr_defaut FROM adresses WHERE adr_id=" . $con_adresse . ";";
		$req = $Conn->query($sql);
		$d_adresse=$req->fetch();
		if(empty($d_adresse)){
			unset($d_adresse);
		}	
		
	}*/
	
	
	// FIN CONTROLE
	
	if($contact>0){
		
		// DONNEE AVANT MODIF
		
		$sql="SELECT con_rgpd,con_mail,con_hash_public FROM Contacts WHERE con_id=" . $contact . ";";
		$req = $Conn->query($sql);
		$d_contact_old=$req->fetch();
		if(!empty($d_contact_old["con_hash_public"])){
			$con_hash_public=hash('ripemd160', 'contact-' . $contact);
		}else{
			$con_hash_public=$d_contact_old["con_hash_public"];
		}
		
		// MODIF
		
		$sql="UPDATE contacts SET 
		con_fonction = :con_fonction,
		con_fonction_nom = :con_fonction_nom,
		con_titre = :con_titre,
		con_nom = :con_nom,
		con_prenom = :con_prenom,
		con_tel = :con_tel,
		con_portable = :con_portable,
		con_fax = :con_fax,
		con_mail = :con_mail,
		con_compta = :con_compta,
		con_comment = :con_comment 
		WHERE con_id=" . $contact . " AND con_ref_id=" . $client . ";";
		$req = $Conn->prepare($sql);
		$req->bindParam("con_fonction",$con_fonction);
		$req->bindParam("con_fonction_nom",$con_fonction_nom);
		$req->bindParam("con_titre",$con_titre);
		$req->bindParam("con_nom",$_POST["con_nom"]);
		$req->bindParam("con_prenom",$_POST["con_prenom"]);
		$req->bindParam("con_tel",$_POST["con_tel"]);
		$req->bindParam("con_portable",$_POST["con_portable"]);
		$req->bindParam("con_fax",$_POST["con_fax"]);
		$req->bindParam("con_mail",$_POST["con_mail"]);
		$req->bindParam("con_compta",$con_compta);
		$req->bindParam("con_comment",$_POST["con_comment"]);
		try{
			$req->execute();
		}catch( PDOException $Exception ){
			$erreur_txt=$Exception->getMessage();
			//$erreur_txt.="Erreur, le contact n'a pas été mis à jour!";
		}
		
	}else{
		
		// CREATION
		
		$req = $Conn->prepare("INSERT INTO contacts (
		con_ref, con_ref_id, con_fonction, con_fonction_nom, con_titre, con_nom, con_prenom, con_tel, con_portable, con_fax, con_mail, con_compta
		,con_comment) VALUES (
		1, :con_ref_id, :con_fonction, :con_fonction_nom, :con_titre, :con_nom, :con_prenom, :con_tel, :con_portable, :con_fax, :con_mail, :con_compta
		,:con_comment);");
		$req->bindParam("con_ref_id",$client);
		$req->bindParam("con_fonction",$con_fonction);
		$req->bindParam("con_fonction_nom",$con_fonction_nom);
		$req->bindParam("con_titre",$con_titre);
		$req->bindParam("con_nom",$_POST["con_nom"]);
		$req->bindParam("con_prenom",$_POST["con_prenom"]);
		$req->bindParam("con_tel",$_POST["con_tel"]);
		$req->bindParam("con_portable",$_POST["con_portable"]);
		$req->bindParam("con_fax",$_POST["con_fax"]);
		$req->bindParam("con_mail",$_POST["con_mail"]);
		$req->bindParam("con_compta",$con_compta);
		$req->bindParam("con_comment",$_POST["con_comment"]);
		try {
			$req->execute();
			$contact = $Conn->lastInsertId();
			$con_hash_public=hash('ripemd160', 'contact-' . $contact);			
		}catch( PDOException $Exception ){
			$erreur_txt=$Exception->getMessage();
		}
	}
	
	if(empty($erreur_txt)){
		
		if(!empty($_POST["con_mail"])){
			
			if(!isset($d_contact_old) OR empty($d_contact_old["con_rgpd"]) OR $d_contact_old["con_mail"]!=$_POST["con_mail"]){
				
				// MAIL RGPD
				
				$con_hash_public=hash('sha1', $client. '-' . $contact);

				$adr=array(
					"0" => array(
						"nom" => $_POST["con_prenom"] . " " . $_POST["con_nom"],
						"adresse" => $_POST["con_mail"]
					)
				);
				
				$param=array(
					"sujet" => "RGPD : choisissez les communications que vous souhaitez recevoir",
					"cle_public" => $con_hash_public
				);

				//$envoie_ok=envoi_mail("rgpd",$param,$adr);
				//if(is_bool($envoie_ok)){
					
					//$sql="UPDATE contacts SET con_rgpd = 1,con_hash_public = :con_hash_public WHERE con_id=:contact AND con_ref_id=:client;";
					$sql="UPDATE contacts SET con_hash_public = :con_hash_public WHERE con_id=:contact AND con_ref_id=:client;";
					$req = $Conn->prepare($sql);
					$req->bindParam("con_hash_public",$con_hash_public);
					$req->bindParam("contact",$contact);
					$req->bindParam("client",$client);					
					try{
						$req->execute();
					}catch( PDOException $Exception ){
						$erreur_txt=$Exception->getMessage();
						$erreur_txt.="Erreur 001! Le contact n'a pas été mis à jour. Contacter l'administrateur du site.";
					}
				//}
			}
		}
		
		/*if(isset($d_contact_old)){
			
			if($d_contact_old["con_adresse"]!=$con_adresse AND !empty($d_contact_old["con_adresse"]) AND $d_contact_old["con_defaut"]==1){
				
				// le contact était le contact par défaut de l'ancienne adresse.
				
				$sql="SELECT adr_id FROM adresses WHERE adr_id=" . $d_contact_old["con_adresse"] . " AND adr_defaut;";
				$req = $Conn->query($sql);
				$d_adresse_old=$req->fetch();
				if(!empty($d_adresse_old)){
					
					// l'ancienne adresse est l'adresse par defaut du client
					// il faut mettre à jour la fiche client 
					
					$sql="UPDATE clients SET 
					sus_contact = 0,
					sus_con_fct = 0,
					sus_con_fct_nom = '',
					sus_con_titre = 0,
					sus_con_nom = '',
					sus_con_prenom = '',
					sus_con_tel = '',
					sus_con_mail = ''
					WHERE sus_id=" . $client . ";";
					$Conn->query($sql);
					
				}
			}
		}
		
		if(isset($d_adresse)){
			
			// le contact est lié à une adresse
			
			if($con_defaut==1){
				
				$sql="UPDATE contacts SET con_defaut = 0 WHERE con_adresse=" . $con_adresse . " AND NOT con_id=" . $contact . ";";
				try {
					$req=$Conn->query($sql);
				}catch( PDOException $Exception ){
					$erreur_txt=$Exception->getMessage();
				}
				
				if($d_adresse["adr_defaut"]==1){
					
					// il s'agit du contact par defaut de l'adresse par defaut => maj de la fiche client
					
					$sql="UPDATE clients SET 
					sus_contact = :sus_contact,
					sus_con_fct = :sus_con_fct,
					sus_con_fct_nom = :sus_con_fct_nom,
					sus_con_titre = :sus_con_titre,
					sus_con_nom = :sus_con_nom,
					sus_con_prenom = :sus_con_prenom,
					sus_con_tel = :sus_con_tel,
					sus_con_mail = :sus_con_mail
					WHERE sus_id=" . $client . ";";
					$req=$Conn->prepare($sql);
					$req->bindParam("sus_contact",$contact);
					$req->bindParam("sus_con_fct",$con_fonction);
					$req->bindParam("sus_con_fct_nom",$con_fonction_nom);
					$req->bindParam("sus_con_titre",$con_titre);
					$req->bindParam("sus_con_nom",$_POST["con_nom"]);
					$req->bindParam("sus_con_nom",$_POST["con_nom"]);
					$req->bindParam("sus_con_prenom",$_POST["con_prenom"]);
					$req->bindParam("sus_con_tel",$_POST["con_tel"]);
					$req->bindParam("sus_con_mail",$_POST["con_mail"]);				
					try {
						$req->execute();
					}catch( PDOException $Exception ){
						$erreur_txt=$Exception->getMessage();
					}
					
				}
			}
		}*/
	}
	// FIN GESTION COMPTE
}
if(!empty($_SESSION['retourContact'])){
	 $retour=$_SESSION['retourContact'];
	 if(!empty($erreur_txt)){
		$retour.="&erreur=" . $erreur_txt; 
	 }
}

$_SESSION['message'][] = array(
		"titre" => "Succès",
		"type" => "success",
		"message" => "Le contact du client a été actualisé"
	);
header("location : " . $retour);
