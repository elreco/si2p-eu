<?php
include "includes/controle_acces.inc.php";
// error_reporting( error_reporting() & ~E_NOTICE );
include "includes/connexion.php";
include "includes/connexion_soc.php";
include "modeles/mod_client.php";
include "modeles/mod_contact.php";
include "modeles/mod_adresse.php";
include "modeles/mod_parametre.php";
if(!isset($_POST['cli_reg_nb_jour'])){
    $_POST['cli_reg_nb_jour'] = 0;
}
// si c'est une création
if (!empty($_POST) && !isset($_POST['search'])) {

    if(!empty($_POST['cli_code'])){

        if(!empty($_POST['cli_siret2'])){


        $sql3="SELECT * FROM suspects_adresses WHERE sad_siret = :siret";

        $req3 = $ConnSoc->prepare($sql3);
        $req3->bindParam("siret",$_POST["cli_siret2"]);
        $req3->execute();
        $sirets = $req3->fetch();

        $sql4="SELECT * FROM adresses WHERE adr_siret = :sireti";

        $req4 = $ConnSoc->prepare($sql4);
        $req4->bindParam("sireti",$_POST['cli_siret2']);
        $req4->execute();
        $sirets2 = $req4->fetch();

        if(!empty($sirets2) OR !empty($sirets)){
            Header("Location: client.php?error=3");
            exit();
        }
    }


        if (isset($_POST['cli_facture_opca']) && $_POST['cli_facture_opca'] == "oui") {
            $cli_facture_opca = 1;
        } else {
            $cli_facture_opca = 0;
        }
        if(!isset($_POST['cli_reg_fdm'])){
            $_POST['cli_reg_fdm'] = 0;
        }
        if(!empty($_POST['cli_siret'])){
            $_POST['cli_siret'] = $_POST['cli_siren'] . $_POST['cli_siret'];
        }
        if($_POST['con_fonction'] == "autre"){ 
            $_POST['con_fonction'] = 0;
        }
        if(!isset($_POST['cli_agence'])){
            $_POST['cli_agence'] = 0;
        }
        //groupe
        if($_POST['cli_categorie'] == 2 && isset($_POST['cli_groupe'])){

            $cli_groupe_societe = $_POST['cli_societe_2'];
            

        }else{
            $cli_groupe_societe = $_SESSION['acces']['acc_societe'];
           
        }

        if(isset($_POST['cli_groupe'])){
            $sus_filiale_de_type = $_POST['sus_groupe_client'];
            $cli_groupe = 1;
        }else{
            $cli_groupe = 0;
            $sus_filiale_de_type = 0;
        }

        if(isset($_POST['cli_releve'])){
            $cli_releve = 1;
        }else{
            $cli_releve = 0;
        }

        $_POST['cli_filiale_de'] = 0;

        // création du suspect
        
        $ref_id2 = insert_suspect($_POST['cli_code'], $_POST['cli_nom'], $_POST['cli_agence'], $_POST['cli_commercial'], $_POST['cli_categorie'],$_POST['cli_sous_categorie'], $_POST['cli_stagiaire_type'], $_POST['cli_secteur'], $_POST['cli_sous_secteur'], $_POST['cli_classification'], $_POST['cli_filiale_de'],$cli_groupe_societe, $_POST['cli_reg_type'], $_POST['cli_siren'], $_POST['cli_ape'], $_POST['cli_opca'], $cli_facture_opca, $_POST['cli_reg_formule'], $_POST['cli_reg_nb_jour'], $_POST['cli_reg_fdm'], $_POST['cli_sous_classification'], $_POST['cli_ident_tva'], $_POST['cli_prescripteur'], $cli_groupe, $sus_filiale_de_type, $cli_releve, $_POST['cli_reference']);

        // on actualise le suspect pour mettre à jour le GROUPE

        if(isset($_POST['cli_maison_mere']) && $_POST['cli_maison_mere'] == "oui" && isset($_POST['cli_groupe'])){
            $sus_niveau = 0;
            $req = $ConnSoc->prepare("UPDATE suspects SET sus_filiale_de = 0, sus_niveau = :sus_niveau WHERE sus_id = :sus_id;");
            $req->bindParam(':sus_id', $_GET['client']);
            $req->bindParam(':sus_niveau', $sus_niveau);
            $req->execute();
        }
        if(isset($_POST['cli_maison_mere_non']) && $_POST['cli_maison_mere_non'] == "non" && isset($_POST['cli_groupe']) && $_POST['cli_filiale_de'] != 0){

            $req = $ConnSoc->prepare("SELECT sus_id,sus_niveau,sus_filiale_de FROM suspects WHERE sus_id = :sus_filiale_de");
            $req->bindParam(':sus_filiale_de', $_POST['cli_filiale_de']);
            $req->execute();
            $donnee = $req->fetch();

            $nivo = $donnee['sus_niveau'] + 1;
            $req = $ConnSoc->prepare("UPDATE suspects SET sus_filiale_de = :sus_filiale_de, sus_niveau = :sus_niveau, sus_fil_de = :sus_fil_de WHERE sus_id = :sus_id;");
                $req->bindParam(':sus_id', $_GET['client']);
            $req->bindParam(':sus_filiale_de', $donnee['sus_id']);
            $req->bindParam(':sus_niveau', $nivo);
            $req->bindParam(':sus_fil_de', $donnee['sus_id']);
            $req->execute();

        }

        // fin

        if(!empty($_POST['con_nom'])){
            insert_suspect_contact($ref_id2, $_POST['cli_categorie'], $_POST['con_fonction'], $_POST['con_fonction_nom'], $_POST['con_titre'], $_POST['con_nom'], $_POST['con_prenom'], $_POST['con_tel'], $_POST['con_portable'], $_POST['con_fax'], $_POST['con_mail'], 1, 1);
        }
        if(!empty($_POST['adr_ville1']) && !empty($_POST['adr_cp1'])){
            insert_suspect_adresse($_POST['cli_categorie'], $ref_id2, 1, $_POST['adr_nom1'], $_POST['adr_service1'], $_POST['adr_ad11'], $_POST['adr_ad21'], $_POST['adr_ad31'], $_POST['adr_cp1'], $_POST['adr_ville1'], 1, $_POST['adr_libelle1'], $_POST['cli_geo'], $_POST['cli_siret2']); 
        }

        if(!empty($_POST['adr_ville2']) && !empty($_POST['adr_cp2'])){
            insert_suspect_adresse($_POST['cli_categorie'], $ref_id2, 2, $_POST['adr_nom2'], $_POST['adr_service2'], $_POST['adr_ad12'], $_POST['adr_ad22'], $_POST['adr_ad32'], $_POST['adr_cp2'], $_POST['adr_ville2'], 1, $_POST['adr_libelle2'], $_POST['cli_geo2'], $_POST['cli_siret2']);
        }

        header("Location: suspect_voir.php?client=" . $ref_id2 . "&succes=5");

// si le code n'est pas renseigné
    }elseif(!empty($_POST['cli_nom']) && !empty($_POST['adr_cp1']) && !empty($_POST['adr_ville1'])){

        if(!empty($_POST['cli_siret'])){
         $sql3="SELECT * FROM suspects_adresses WHERE sad_siret = :siret";

        $req3 = $ConnSoc->prepare($sql3);
        $req3->bindParam("siret",$_POST["cli_siret"]);
        $req3->execute();
        $sirets = $req3->fetch();

        $sql4="SELECT * FROM adresses WHERE adr_siret = :sireti";

        $req4 = $ConnSoc->prepare($sql4);
        $req4->bindParam("sireti",$_POST['cli_siret']);
        $req4->execute();
        $sirets2 = $req4->fetch();

        if(!empty($sirets2) OR !empty($sirets)){
            Header("Location: client.php?error=3");
            exit();
        }
    }
        if (isset($_POST['cli_facture_opca']) && $_POST['cli_facture_opca'] == "oui") {
            $cli_facture_opca = 1;
        } else {
            $cli_facture_opca = 0;
        }
        if(!isset($_POST['cli_reg_fdm'])){ 
            $_POST['cli_reg_fdm'] = 0;
        }
        if(!empty($_POST['cli_siret'])){
            $_POST['cli_siret'] = $_POST['cli_siren'] . " " . $_POST['cli_siret'];
        }
        if($_POST['con_fonction'] == "autre"){ 
            $_POST['con_fonction'] = 0;
        }
        if(!isset($_POST['cli_agence'])){
            $_POST['cli_agence'] = 0;
        }
        //groupe
        if($_POST['cli_categorie'] == 2 && isset($_POST['cli_groupe'])){

            $cli_groupe_societe = $_POST['cli_societe_2'];

        }else{
            $cli_groupe_societe = 0;
            
        }

        if(isset($_POST['cli_groupe'])){
            $sus_filiale_de_type = $_POST['sus_groupe_client'];
            $cli_groupe = 1;
        }else{
            $cli_groupe = 0;
            $sus_filiale_de_type = 0;
        }

        if(isset($_POST['cli_releve'])){
            $cli_releve = 1;
        }else{
            $cli_releve = 0;
        }

        $_POST['cli_code'] = substr($_POST['cli_nom'], 0, 6) . substr($_POST['adr_cp1'], 0, 2) . substr($_POST['adr_ville1'], 0, 8);

        $ref_id2 = insert_suspect($_POST['cli_code'], $_POST['cli_nom'], $_POST['cli_agence'], $_POST['cli_commercial'], $_POST['cli_categorie'],$_POST['cli_sous_categorie'], $_POST['cli_stagiaire_type'], $_POST['cli_secteur'], $_POST['cli_sous_secteur'], $_POST['cli_classification'], $_POST['cli_filiale_de'],$cli_groupe_societe, $_POST['cli_reg_type'], $_POST['cli_siren'], $_POST['cli_ape'], $_POST['cli_opca'], $cli_facture_opca, $_POST['cli_reg_formule'], $_POST['cli_reg_nb_jour'], $_POST['cli_reg_fdm'], $_POST['cli_sous_classification'], $_POST['cli_ident_tva'], $_POST['cli_prescripteur'], $cli_groupe, $sus_filiale_de_type, $cli_releve, $_POST['cli_reference']);

        if(isset($_POST['cli_groupe']) && $_POST['cli_filiale_de'] == 0){
            $sus_niveau = 0;
            $req = $ConnSoc->prepare("UPDATE suspects SET sus_filiale_de = :sus_filiale_de, sus_niveau = :sus_niveau WHERE sus_id = :sus_id;");
            $req->bindParam(':sus_id', $ref_id2);
            $req->bindParam(':sus_filiale_de', $_POST['cli_filiale_de']);
            $req->bindParam(':sus_niveau', $sus_niveau);
            $req->execute();

        }elseif(isset($_POST['cli_groupe']) && $_POST['cli_filiale_de'] != 0){
            $req = $ConnSoc->prepare("SELECT sus_id,sus_niveau,sus_filiale_de FROM suspects WHERE sus_id = :sus_filiale_de");
            $req->bindParam(':sus_filiale_de', $_POST['cli_filiale_de']);
            $req->execute();
            $donnee = $req->fetch();

            $nivo = $donnee['sus_niveau'] + 1;
            $req = $ConnSoc->prepare("UPDATE suspects SET sus_filiale_de = :sus_filiale_de, sus_niveau = :sus_niveau, sus_fil_de = :sus_fil_de WHERE sus_id = :sus_id;");
            $req->bindParam(':sus_id', $ref_id2);
            $req->bindParam(':sus_filiale_de', $donnee['sus_filiale_de']);
            $req->bindParam(':sus_niveau', $nivo);
            $req->bindParam(':sus_fil_de', $donnee['sus_id']);
            $req->execute();
        }
        if(!empty($_POST['con_nom'])){
            insert_suspect_contact($ref_id2, $_POST['cli_categorie'], $_POST['con_fonction'], $_POST['con_fonction_nom'], $_POST['con_titre'], $_POST['con_nom'], $_POST['con_prenom'], $_POST['con_tel'], $_POST['con_portable'], $_POST['con_fax'], $_POST['con_mail'], 1, 1);
        }
        if(!empty($_POST['adr_ad11']) && !empty($_POST['adr_ville1']) && !empty($_POST['adr_cp1'])){
            insert_suspect_adresse($_POST['cli_categorie'], $ref_id2, 1, $_POST['adr_nom1'], $_POST['adr_service1'], $_POST['adr_ad11'], $_POST['adr_ad21'], $_POST['adr_ad31'], $_POST['adr_cp1'], $_POST['adr_ville1'], 1, $_POST['adr_libelle1'], $_POST['cli_geo'], $_POST['cli_siret2']); 
        }

        if(!empty($_POST['adr_ad12']) && !empty($_POST['adr_ville2']) && !empty($_POST['adr_cp2'])){
            insert_suspect_adresse($_POST['cli_categorie'], $ref_id2, 2, $_POST['adr_nom2'], $_POST['adr_service2'], $_POST['adr_ad12'], $_POST['adr_ad22'], $_POST['adr_ad32'], $_POST['adr_cp2'], $_POST['adr_ville2'], 1, $_POST['adr_libelle2'], $_POST['cli_geo2'], $_POST['cli_siret2']);
        }

        header("Location: suspect_voir.php?client=" . $ref_id2 . "&succes=5");
    }else{
        header("Location: client.php?error=1");
    }

}
