<?php 

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('includes/connexion_soc.php');
include('modeles/mod_parametre.php');
include('modeles/mod_get_juridique.php');

include('modeles/mod_stagiaire_attestation_data.php');




// VISUALISATION D'UNE CONVENTION

$erreur_txt="";


$action_id=0;
$action_client_id=0;
$stagiaire_id=0;
$stagiaire_list="";

if(!empty($_GET)){ 
	if(!empty($_GET["action"])){
		$action_id=intval($_GET["action"]);
	}
	$action_client_id=0;
	if(!empty($_GET["action_client"])){
		$action_client_id=intval($_GET["action_client"]);
	}
	$stagiaire_id=0;
	if(!empty($_GET["stagiaire"])){
		$stagiaire_id=intval($_GET["stagiaire"]);
	}
	if(!empty($_GET["stagiaire"])){
		$stagiaire_id=intval($_GET["stagiaire"]);
	}
}elseif(!empty($_POST)){ 

	if(!empty($_POST["action"])){
		$action_id=intval($_POST["action"]);
	}
	if(!empty($_POST["stagiaires"])){
		$stagiaire_list=implode(",",$_POST["stagiaires"]);
	}
}


if(empty($action_id) OR (empty($action_client_id) AND empty($stagiaire_id) AND empty($stagiaire_list) ) ){
	$erreur_txt="impossible d'afficher la page";
}else{
	
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=intval($_SESSION['acces']["acc_societe"]);  
	}
	if($acc_societe!=$conn_soc_id){
		$acc_societe=$conn_soc_id;
	}
	
	// MODELE DATA CONCU POUR UN STAGIAIRE
	
	$d_attestations=array();

	$warning_text="";
	
	$sql="SELECT * FROM Actions_Stagiaires WHERE ast_action=:action_id";
	if(!empty($action_client_id)){
		$sql.=" AND ast_action_client=:action_client_id";
	}elseif(!empty($stagiaire_list)){
		$sql.=" AND ast_stagiaire IN (" . $stagiaire_list . ")";
	}else{
		$sql.=" AND ast_stagiaire=:stagiaire_id";
	}
	$req=$ConnSoc->prepare($sql);
	$req->bindParam(":action_id",$action_id);
	if(!empty($action_client_id)){
		$req->bindParam(":action_client_id",$action_client_id);
	}elseif(!empty($stagiaire_list)){
		//$req->bindParam(":stagiaire_list",$stagiaire_list);
	}else{
		$req->bindParam(":stagiaire_id",$stagiaire_id);
	}
	$req->execute();
	$d_stagiaires=$req->fetchAll();
	if(!empty($d_stagiaires)){
		
		foreach($d_stagiaires as $sta){
			$r_attest=stagiaire_attestation_data($sta);
			if(!empty($r_attest)){
				$d_attestations[]=$r_attest;
				if(!empty($r_attest["pages"][0]["alert"])){
					if($r_attest["pages"][0]["alert"]["type"]=="warning"){
						$warning_text.=$r_attest["pages"][0]["alert"]["texte"] . "<br/>";
					}
				}
			}
		}
	}
	if(empty($d_attestations)){
		$erreur_txt="Il n'y a pas de stagiaire!";	
	}
}
 ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title></title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">
		

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#container_print{
				width:21cm;
			}
			
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm">
		<div id="zone_print" ></div>
		<!-- Start: Main -->
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">	
		<?php 		if(empty($erreur_txt)){ ?>
		
							<div class="row" >
							
								<!-- zone d'affichage du devis -->
								
								<div class="col-md-8"  >	
								
									<div id="container_print" >
								
										<div id="page_print" >
										
								<?php		foreach($d_attestations as $p => $data){
												if(!empty($data)){
													foreach($data["pages"] as $page){ ?>
										
														<div class="page" id="header_<?=$p?>" data-page="<?=$p?>" >
															<header> 
																<div class="text-center" >
																	<img src="../<?=$data["juridique"]["logo_1"]?>" />
																</div>
																<div class="text-center" >
																	<h1 class="text-center" >ATTESTATION DE FIN DE FORMATION</h1>
																	<p>Article L. 6353-1 du Code du Travail</p>
																</div>
																		
															</header>
														</div>
														<div id="head_<?=$p?>" >
															<section>
																<p>
																	<?=$data["juridique"]["nom"]?> organisme de formation déclaré sous le n° <?=$data["juridique"]["num_existence"]?>, auprès du préfet de la région <?=$data["juridique"]["region_nom"]?>, atteste que :
																</p>
																<p class="text-center text-strong" ><b><?=$page["sta_nom"] . " " . $page["sta_prenom"]?></b></p>
																<p>a suivi la formation <?=$page["att_titre"]?> dispensée par <?=$data["juridique"]["nom"]?> :</p>
															</section>
															<section>
																<table class="w-100 " >																						
																	<tr>
																		<td class="w-25 text-right p5" ><b>Intitulé</b> :</td>
																		<td class="w-75 text-strong p5" ><?=$page["att_titre"]?></td>
																	</tr>
																	<tr>
																		<td class="w-25 text-right p5" style="vertical-align:top;" ><b>Rappel des objectifs</b> :</td>
																		<td class="w-75 text-strong p5" ><?=trim($page["att_contenu"])?></td>
																	</tr>
														<?php		if(!empty($page["att_planning"])){ ?>
																		<tr>
																			<td class="w-25 text-right p5" ><b>Date(s) de formation</b> :</td>
																			<td class="w-75 text-strong p5" ><?=$page["att_planning"]?></td>
																		</tr>
														<?php		} ?>
																	<tr>
																		<td class="w-25 text-right p5" ><b>Durée</b> :</td>
																		<td class="w-75 text-strong p5" ><?=$page["att_duree"]?></td>
																	</tr>
																</table>
															</section>
															<section>
																<p>a été évalué(e), au regard des objectifs de formation rappelés ci-dessus :</p>
															</section>
														</div>																								
														<div id="ligne_header_<?=$p?>" >	
															<div class="table-div" >														
																<div class="text-center w-70 bt" >Compétences ou connaissances visées</div>
																<div class="text-center w-30 bt" >
																	Résultats à l’issue de la formation
																	<div class="table-div" >
																		<div class="text-center table-div-th w-50" >Acquise</div>
																		<div class="text-center table-div-th w-50" >Reste à acquérir</div>
																	</div>
																</div>
															</div>	
														</div>
																
														
												<?php	if(!empty($page["competences"])){
															foreach($page["competences"] as $comp){ ?>
																<div class="table-div ligne-body-<?=$p?>" >														
																	<div class="w-70 p5" ><?=$comp["comp_txt"]?></div>
													<?php			if($comp["comp_acquise"]){ ?>
																		<div class="w-15 text-center" >
																			<img src="../assets/img/checked.png" />
																		</div>
																		<div class="w-15 text-center" >
																			<img src="../assets/img/unchecked.png" />
																		</div>
													<?php			}else{ ?>
																		<div class="w-15 text-center" >
																			<img src="../assets/img/unchecked.png" />
																		</div>
																		<div class="w-15 text-center" >
																			<img src="../assets/img/checked.png" />
																		</div>
													<?php 			} ?>
																</div>
												<?php		}
														}?>
										
													
														<div id="foot_<?=$p?>" >
															<section style="padding-bottom:50px;" >
																<p>Fait à <?=$data["juridique"]["ville"]?>, le <?=utf8_encode($data["date_doc"])?></p>
																
																<table id="attest_signe" >
																	<tr>
																		<td class="w-60" >&nbsp;</td>
																		<td>
																	<?php	if(!empty($data["juridique"]["resp_ident"])){
																				echo($data["juridique"]["resp_ident"]);
																			}
																			if(!empty($data["juridique"]["resp_fonction"])){
																				echo("<br/>" . $data["juridique"]["resp_fonction"]);
																			}
																			if(!empty($data["juridique"]["resp_signature"])){ 
																				echo("<br/>"); ?>
																				<img src="../<?=$data["juridique"]["resp_signature"]?>" class="img-responsive" />
																	<?php	}?>
																		</td>
																	</tr>
																</table>
															</section>
															
														</div>
														<div id="footer_<?=$p?>"  >
															<footer class="text-center" >
																<?=$data["juridique"]["footer"]?>
															</footer>
														</div>
										<?php		}
													if($p<count($d_attestations)-1){
														echo("<div class='saut-page' ></div>");
													}
												}
											} ?>
										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->
								
								<div class="col-md-4" >
									<!-- DEBUT ANNEXE -->
									
									<!-- FIN ANNEXE -->
								</div>
								
							</div>
						

		<?php		}else{ ?>
						
						<p class="alert alert-danger text-center" >
							<?=$erreur_txt?>
						</p>
					
		<?php		} ?>
									
				</section>
				
				<!-- End: Content -->
			</section>
			
			
		</div>
	
		<!-- End: Main -->
		
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >	
			<?php	if(isset($_SESSION["retourAttest"])){ ?>
						<a href="<?=$_SESSION["retourAttest"]?>" class="btn btn-default btn-sm" >
							Retour
						</a>
			<?php	}else{ ?>
						<a href="action_voir.php?action=<?=$action_id?>&onglet=3" class="btn btn-default btn-sm" >
							Retour
						</a>
			<?php	} ?>
				</div>
				<div class="col-xs-6 footer-middle text-center" >
					<button type="button" class="btn btn-sm btn-info ml15" id="print" >
						<i class="fa fa-print"></i> Imprimer
					</button>
				</div>
				<div class="col-xs-3 footer-right" ></div>
			</div>
		</footer>

<?php	if (!empty($warning_text)) { ?>
			<div id="modal-error" class="modal fade" role="dialog" data-show="true" >
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header bg-warning">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title" id="titreModal" >Avertissement!</h4>
						</div>
						<div class="modal-body"><?=$warning_text?></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
								<i class="fa fa-close" ></i>
								Fermer
							</button>
						</div>
					</div>
				</div>
			</div>
<?php	}
		
		include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript" src="assets/js/print.js"></script>
		<script type="text/javascript">
			var l_page=21;
			var h_page=29;
		</script>

	</body>
</html>
