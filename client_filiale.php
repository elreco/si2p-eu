<?php

include "includes/controle_acces.inc.php";
include_once("includes/connexion_soc.php");
include 'includes/connexion_fct.php';
include_once("includes/connexion.php");

// RECHERCHE DES FILIALES REGIONAL D'UNE MM GFC

// sur la personne connecte
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];	
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];	
}

$client=0;
$filt_societe=0;
$filt_filiale=1;
$filt_code="";
$filt_nom="";
$filt_siren="";
$filt_siret="";

$filtrer=false;

if(!empty($_POST)){
	
	
	
	// FILTRE
	if(!empty($_POST["client"])){
		$client=intval($_POST["client"]);
	}
	
	if(!empty($_POST["filt_code"])){
		$filtrer=true;
		$filt_code=$_POST["filt_code"];
	}
	if(!empty($_POST["filt_nom"])){
		$filtrer=true;
		$filt_nom=$_POST["filt_nom"];
	}
	if(!empty($_POST["filt_siren"])){
		$filtrer=true;
		$filt_siren=$_POST["filt_siren"];
	}
	if(!empty($_POST["filt_siret"])){
		$filtrer=true;
		$filt_siret=$_POST["filt_siret"];
	}
	
	$filt_filiale=0;
	if(!empty($_POST["filt_filiale"])){
		$filtrer=true;
		$filt_filiale=1;	
	}
	
	
	if(!empty($_POST["filt_societe"])){
		$filt_societe=intval($_POST["filt_societe"]);
	}

}else{
	
	if(!empty($_GET["client"])){
		$client=intval($_GET["client"]);
	}
	$filt_filiale=1;
	$filtrer=true;	
	
}

if($client==0){
	
	$erreur=1;
	
}else{

	// SUR LE CLIENT (MAISON MERE)

	$sql="SELECT cli_code,cli_nom,cli_categorie FROM Clients WHERE cli_id=" . $client . ";";
	$req=$ConnSoc->query($sql);
	$d_client=$req->fetch();

	// SUR LA SOCIETE

	$sql="SELECT * FROM Societes WHERE soc_id=" . $acc_societe . ";";
	$req=$Conn->query($sql);
	$d_societe=$req->fetch();

	$erreur=0;
	if(empty($d_client) OR empty($d_societe)){
		$erreur=2;
	}
}

if($erreur==0){
	
	// LA CATEGORIE
	
	$sql="SELECT cca_gc FROM Clients_Categories WHERE cca_id=" . $d_client["cli_categorie"] . ";";
	$req=$Conn->query($sql);
	$d_categorie=$req->fetch();
	if(!empty($d_categorie)){
		// l'ajout de filiale GC doit se faire sur soc_gc
		if($d_categorie["cca_gc"]==1){
			$erreur=2;
		}
	}else{
		$erreur=2;	
	}
}

if($erreur==0){
	
	$groupe_multi_soc=false;
	if($d_categorie["cca_gc"]==1 OR $d_client["cli_categorie"]==7){
		$groupe_multi_soc=true;
	}
	
	if($groupe_multi_soc){
		$sql="SELECT soc_id,soc_nom FROM Societes;";
		$req=$Conn->query($sql);
		$d_societes=$req->fetchAll();
	}
	
	$filiales=array();

	if($filtrer){

		$sql_fil="SELECT cli_id,cli_code,cli_nom,adr_ad1,adr_cp,adr_ville,cli_filiale_de FROM Clients LEFT JOIN Adresses ON (Clients.cli_id=Adresses.adr_ref_id)";
		$sql_fil.=" WHERE cli_categorie=1";
		if(!empty($filt_code)){
			$sql_fil.=" AND cli_code LIKE '%" . $filt_code . "%'";
		}
		if(!empty($filt_nom)){
			$sql_fil.=" AND cli_code LIKE '%" . $filt_nom . "%'";
		}
		if(!empty($filt_siren)){
			$sql_fil.=" AND cli_siren LIKE '%" . $filt_siren . "%'";
		}
		if(!empty($filt_siret)){
			$sql_fil.=" AND adr_siret LIKE '%" . $filt_siret . "%'";
		}
		if($filt_filiale==1){
			$sql_fil.=" AND cli_filiale_de=" . $client;
		}
		if(!$groupe_multi_soc){
			$req=$ConnSoc->query($sql_fil);
			$d_filiales=$req->fetchAll();
			
			foreach($d_filiales as $df){
				$filiales[]=array(
					"client" => $df["cli_id"],
					"societe" => $acc_societe,
					"code" => $df["cli_code"],
					"nom" => $df["cli_nom"],
					"ad1" => $df["adr_ad1"],
					"cp" => $df["adr_cp"],
					"ville" => $df["adr_ville"],
					"filiale_de" => $df["cli_filiale_de"],
					"filiale_de_soc" => $df["cli_filiale_de_soc"]
				);
			}
		}elseif($filt_societe>0){
			/*$ConnFct=connexion_fct($filt_societe);*/
			$req=$ConnFct->query($sql_fil);
			$d_filiales=$req->fetchAll();
			foreach($d_filiales as $df){
				$filiales[]=array(
					"client" => $df["cli_id"],
					"societe" => $filt_societe,
					"code" => $df["cli_code"],
					"nom" => $df["cli_nom"],
					"ad1" => $df["adr_ad1"],
					"cp" => $df["adr_cp"],
					"ville" => $df["adr_ville"],
					"filiale_de" => $df["cli_filiale_de"],
					"filiale_de_soc" => $df["cli_filiale_de_soc"]
				);
			}
		}else{
			foreach($d_societes as $s){
				$ConnFct=connexion_fct($s["soc_id"]);
				$req=$ConnFct->query($sql_fil);
				$region=$req->fetchAll();
				foreach($region as $df){
					$filiales[]=array(
						"client" => $df["cli_id"],
						"societe" => $s["soc_id"],
						"code" => $df["cli_code"],
						"nom" => $df["cli_nom"],
						"ad1" => $df["adr_ad1"],
						"cp" => $df["adr_cp"],
						"ville" => $df["adr_ville"],
						"filiale_de" => $df["cli_filiale_de"],
						"filiale_de_soc" => $df["cli_filiale_de_soc"]
					);
				}
			
			}
		}
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

	</head>

	<body class="sb-top sb-top-sm" >
		<form method="post" action="client_filiale.php" id="formulaire" >
			<div>
				<input type="hidden" name="client" value="<?=$client?>" />
			</div>
			<!-- Start: Main -->
			<div id="main">
	<?php		include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">		

					<section id="content" class="animated fadeIn">
				<?php	if($erreur!=0){
							echo($erreur);
						}else{ ?>
							<div class="row">
								<div class="col-md-10 col-md-offset-1">						
									<div class="admin-form theme-primary ">
										<div class="panel heading-border panel-primary">
											<div class="panel-body bg-light">
												
												<div class="content-header text-center">
													<h2>Gestion des filiales de <b class="text-primary"><?=$d_client["cli_nom"]?></b></h2>
												</div>											
												<div class="col-md-10 col-md-offset-1">		
													<div class="row" >
														<div class="col-md-4">
															<label for="filt_code" >Code :</label>
															<input type="text" name="filt_code" id="filt_code" class="gui-input" placeholder="Code du client" value="<?=$filt_code?>" />												
														</div>	
														<div class="col-md-8">
															<label for="filt_nom" >Nom :</label>
															<input type="text" name="filt_nom" class="gui-input" placeholder="Nom du client" value="<?=$filt_nom?>" />	
														</div>
													</div>
													<div class="row mt10" >
														<div class="col-md-4 mt10">
															<label class="option">
																<input type="checkbox" name="filt_filiale" value="on" <?php if($filt_filiale==1) echo("checked"); ?> >
																<span class="checkbox"></span>Filiales
															</label>
														</div>
														<div class="col-md-8">
											<?php			if($groupe_multi_soc){ ?>			
																<label for="filt_societe" >Société :</label>
																<select name="filt_societe" id="filt_societe" class="select2" >
																	<option value="0" >&nbsp;</option>
														<?php		if(isset($d_societes)){
																		foreach($d_societes as $ds){
																			if($ds["soc_id"]==$filt_societe){
																				echo("<option value='" . $ds["soc_id"] . "' selected >" . $ds["soc_nom"] . "</option>");
																			}else{
																				echo("<option value='" . $ds["soc_id"] . "' >" . $ds["soc_nom"] . "</option>");
																			}
																		}
																	} ?>
																</select>
											<?php			} ?>	
														</div>
													</div>
													<div class="row mt10" >
														<div class="col-md-8">
															<label for="filt_siren" >Siren :</label>
															<input type="text" name="filt_siren" id="filt_siren" class="gui-input siren" placeholder="Siren" value="<?=$filt_siren?>" />												
														</div>	
														<div class="col-md-4">
															<label for="filt_nom" >Siret :</label>
															<input type="text" name="filt_siret" id="filt_siret" class="gui-input siret" placeholder="Siret" value="<?=$filt_siret?>" <?php if(strlen($filt_siren)!=11) echo("disabled") ?> />	
														</div>
													</div>
													<div class="row mt10 " >													
														<div class="col-md-12 text-center">
															<button type="submit" class="btn btn-primary" >
																Rechercher
															</button>
														</div>	
													</div>
												
													
										<?php		//print_r($filiales);									
													if(!empty($filiales)){ 
														asort($filiales); ?>													
														<table class="table" >	
															<thead>
																<tr>
																	<th>
																<!--
																		<label class="option" >
																			<input type="checkbox" id="filiale_all" class="check-all" />
																			<span class="checkbox"></span>
																		</label> -->
																		&nbsp;
																	</th>
																	<th>Code</th>
																	<th>Nom</th>
																	<th>Adresse</th>
																	<th>CP</th>
																	<th>Ville</th>
																</tr>
															</thead>
															<tbody>
														<?php	$nb_filiale=0;
																foreach($filiales as $f){ 
																	$select=0;
																	if($f["filiale_de"]==$client AND $f["filiale_de"]==$acc_societe){
																		$select=1;
																	}  
																	$nb_filiale++; ?>
																	<tr>
																		<td>																									
																			<label class="option" >
																				<input type="checkbox" class="filiale" data-client="<?=$f["client"]?>" data-societe="<?=$f["societe"]?>" <?php if($select==1) echo("checked"); ?> />
																				<span class="checkbox"></span>
																			</label>                        												
																		</td>
																		<td><?=$f["code"]?></td>
																		<td><?=$f["nom"]?></td>
																		<td><?=$f["ad1"]?></td>
																		<td><?=$f["cp"]?></td>
																		<td><?=$f["ville"]?></td>
																	</tr>
														<?php	} ?>
															</tbody>
														</table>
										<?php		} ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>			
			<?php		} ?>			
					</section>
					
					<!-- End: Content -->
				</section>
			</div>
			<footer id="content-footer" class="affix" >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="client_voir.php?client=<?=$client?>&tab10" class="btn btn-default btn-sm" >
							<i class="fa fa-arrow-left"></i>					
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle center" ></div>
					<div class="col-xs-3 footer-right" >
					
					</div>
				</div>
			</footer>
		</form>
<?php	include "includes/footer_script.inc.php"; ?>
		<script src="/vendor/plugins/select2/js/select2.min.js"></script>		
		<script src="/vendor/plugins/mask/jquery.mask.js" ></script>
		<script src="/assets/js/custom.js" ></script>
		<script type="text/javascript" >
			var holding="<?=$client?>";
			$(document).ready( function(){
				
				$(".filiale").click(function(){
					client=$(this).data("client");
					societe=$(this).data("societe");
					checked="";
					if($(this).is(":checked")){
						checked="1";
					}
					set_groupe_filiale(holding,client,societe,checked);
				});	
					

				$("#filt_siren").blur(function(){
					if($("#filt_siren").val().length!=11){
						$("#filt_siret").val("");
						$("#filt_siret").prop("disabled",true);
					}else{
						$("#filt_siret").prop("disabled",false);
					}
				});
			});

			function set_groupe_filiale(holding,client,societe,checked){
				if(holding>0 && client>0 && societe>0 ){
					$.ajax({			
						type:'POST',
						url: 'ajax/ajax_set_groupe_filiale.php',
						data : "holding=" + holding + "&client=" + client + "&societe=" + societe + "&checked=" + checked,
						dataType: 'json',
						success: function(data){
							
							afficher_txt_user($(".footer-middle"),"Données enregistrées","text-success",5000);
						},
						error: function(data) {
							if(data){
								modal_alerte_user(data.responseText);
							}else{
								modal_alerte_user("Erreur : les données n'ont pas été enregistrés");	
							}
						}			
					});
				}	
			}			
		</script>	
	</body>
</html>