<?php

// AFFICHE LA LISTE DES FACTURES
include "includes/controle_acces.inc.php";

include('includes/connexion.php');
include('includes/connexion_soc.php');
include('includes/connexion_fct.php');

include('modeles/mod_parametre.php');

// DONNEE UTILE AU PROGRAMME
/* var_dump($_SESSION['fac_relance_tri']);
die(); */
$acc_agence=0;
if(isset($_SESSION['acces']["acc_agence"])){
	$acc_agence=$_SESSION['acces']["acc_agence"];
}
$acc_societe=0;
if(isset($_SESSION['acces']["acc_societe"])){
	$acc_societe=$_SESSION['acces']["acc_societe"];
}
$acc_utilisateur=0;
if(!empty($_SESSION['acces']["acc_ref"])){
	if($_SESSION['acces']["acc_ref"]==1){
		$acc_utilisateur=intval($_SESSION['acces']["acc_ref_id"]);
	}
}

if(!empty($_GET['annee'])){
	$annee=$_GET['annee'];
}else{
    die("probleme");
}

if(!empty($_GET['semaine'])){
	$semaine=$_GET['semaine'];
}else{
    die("probleme");
}

if(!empty($_GET['utilisateur'])){
	$utilisateur=$_GET['utilisateur'];
}else{
    die("probleme");
}
// selectionner tous les intervenants
$sql="SELECT * FROM Intervenants WHERE int_type = 1 AND int_ref_1 = " . $utilisateur;
$req = $ConnSoc->query($sql);
$d_intervenant=$req->fetch();
if(empty($d_intervenant)){
    die("probleme");
}
// selectionner tous les intervenants
$sql="SELECT uti_prenom, uti_nom FROM Utilisateurs WHERE uti_id = " . $utilisateur;
$req = $Conn->query($sql);
$d_utilisateur=$req->fetch();

$_SESSION['retourRapport'] = "rap_for.php?utilisateur=" . $utilisateur ."&semaine=" . $semaine . "&annee=" . $annee;

// DONNEES POUR LA REQUETE

$sql="SELECT * FROM formations WHERE for_annee = " . $annee . " AND for_semaine=" . $semaine . " AND for_intervenant=" . $d_intervenant['int_id'] . " ORDER BY for_date ASC, for_demi ASC";
$req = $ConnSoc->query($sql);
$d_formations=$req->fetchAll();

$sql="SELECT * FROM formations_utilisations ORDER BY fut_libelle ASC";
$req = $Conn->query($sql);
$d_types=$req->fetchAll();

foreach($d_types as $t){
    $types[$t['fut_id']] = $t['fut_libelle'];
}
/* $sql="SELECT * FROM vehicules_categories";
$req = $Conn->query($sql);
$d_vehicules_categories=$req->fetchAll();

foreach($d_vehicules_categories as $v){
    $v_categories[$v['vca_id']] = $v['vca_libelle'];
} */

$sql="SELECT veh_libelle, veh_id, veh_immat FROM vehicules";
$req = $Conn->query($sql);
$d_vehicules=$req->fetchAll();

foreach($d_vehicules as $v){
    $vehicules[$v['veh_id']]["veh_libelle"] = $v['veh_libelle'];
    $vehicules[$v['veh_id']]["veh_immat"] = $v['veh_immat'];
}

// DONNEE POUR TRAITEMENT

// sur la personne connecte
$sql="SELECT uti_saisie_rapport FROM Utilisateurs WHERE uti_id = " . $acc_utilisateur;
/* $sql="SELECT uti_saisie_rapport FROM Utilisateurs WHERE uti_id = 86"; */
$req = $Conn->query($sql);
$d_uti=$req->fetch();
// DONNEES POUR LA REQUETE
$drt_voir = false;
if($_SESSION['acces']['acc_profil'] != 1){
    $drt_voir = true;
}

$nb_semaines = $d_uti['uti_saisie_rapport'];





$date = new DateTime();

$currentweek = $date->format("W");
if($date->format("w") < 3){
    $currentweek = $currentweek - 1;
}
$year = date("Y");
// ALLER CHERCHER LES TROIS SEMAINES SUIVANTES
$week_number =intval(sprintf("%02d", $currentweek));

for ($i=$currentweek; $i < $currentweek + 3; $i++) {

	if($week_number == 0){
		$week_number = 1;

	}
    if(date("Y") == $year && $date->format("W") == sprintf("%02d", $week_number)){
		if($week_number == 1){
			$year = $year;
		}
        $weeks[$week_number . "-" . $year]["current"] = true;
    }else{

        $weeks[$week_number . "-" . $year]["current"] = false;
    }

    $weeks[$week_number . "-" . $year]['semaine'] = $week_number;
    $weeks[$week_number . "-" . $year]['annee'] = $year;
    $week_number = $week_number + 1;
}



if(!empty($nb_semaines)){
    $week_number  = $currentweek;
    $year = date("Y");
    for ($i=$currentweek; $i > $currentweek - $nb_semaines; $i--) {
		  $i = intval(sprintf("%02d", $i));


		  if($week_number == 0){
  			$week_number = 52;
  		}

        if(date("Y") == $year && $date->format("W") == $week_number){

            $weeks[$week_number . "-" . $year]["current"] = true;
        }else{
			if($week_number == 52){
				$year = $year -1;
			}
            $weeks[$week_number . "-" . $year]["current"] = false;
        }
        $weeks[$week_number . "-" . $year]['semaine'] = $week_number;
        $weeks[$week_number . "-" . $year]['annee'] = $year;

        $week_number = $week_number - 1;

    }
}
$continue = false;
foreach($weeks as $w){
	if(($w['semaine'] == $semaine && $w['annee'] == $annee) OR $drt_voir == true){
		$continue = true;
	}
}
if($continue == false){
	die("impossible d'accéder");
}
if(!empty($_GET['retour_liste'])){
	$_SESSION['retourRapportVoir'] = "rap_for_voir.php?annee=". $annee ."&utilisateur=" . $_GET['utilisateur'] . "&semaine=" . $semaine ."&retour_liste=1";
}else{
	$_SESSION['retourRapportVoir'] = "rap_for_voir.php?annee=". $annee ."&utilisateur=" . $_GET['utilisateur'] . "&semaine=" . $semaine;
}

?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

		<link rel="stylesheet" type="text/css" href="vendor/plugins/DataTables/media/css/dataTables.bootstrap.css">

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm">
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<style>
						@media (max-width: 1200px) {
							#content{
								overflow-x: auto;
							}
						}
					</style>
					<section id="content" class="animated fadeIn">


                        <h1>Rapports hebdomadaires de <?= $d_utilisateur['uti_prenom'] ?> <?= $d_utilisateur['uti_nom'] ?> pour la semaine <?=  $semaine ?></h1>
                        <?php if(!empty($d_formations)){ ?>
                        <div class="row">

                            <div class="col-md-12" style="overflow-x: auto;">

                                    <!-- <div class="table-responsive"> -->
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
													<th>Actions</th>
                                                    <th colspan="2" >Date</th>
                                                    <th>Type</th>
                                                    <th>Clients</th>
													<th>Nb 1/2 jours</th>
                                                    <th>Nb groupes</th>
                                                    <th>Nb h/groupe</th>
                                                    <th>Total h</th>
                                                    <th>Nb stagiaires</th>
                                                    <th>Observations</th>
                                                    <th>Véhicule</th>
                                                    <th>Immatriculation</th>
                                                    <th>Km</th>
                                                    <th>Hébergement</th>

                                                </tr>
                                            </thead>
                                            <tbody id="tableRap">
                                                <?php foreach($d_formations as $f){
													$sql="SELECT DISTINCT cli_nom, acl_id,cli_code,cli_id, acl_nb_sta_rap,fac_nb_stagiaires,acl_action, acl_produit, acl_pro_reference FROM Formations_actions
													LEFT JOIN actions_clients ON actions_clients.acl_id = formations_actions.fac_action_client
													LEFT JOIN Clients ON clients.cli_id = actions_clients.acl_client WHERE fac_formation =" . $f['for_id'] . " AND acl_archive=0";
													$req = $ConnSoc->query($sql);
													$d_formations_actions=$req->fetchAll();
													$retour = array();
													foreach($d_formations_actions as $acl){
														if(!isset($retour[$acl['acl_produit']])){

															$retour[$acl['acl_produit']] = array(
																"pro_id" => $acl['acl_produit'],
																"pro_code_produit" =>$acl['acl_pro_reference'],
																"acl_action" => $acl['acl_action'],
																"clients" => array(

																)
														    );
														}
														if(!empty($acl['fac_nb_stagiaires'])){
															$retour[$acl['acl_produit']]['clients'][] = array(
																"cli_id" => $acl['cli_id'],
																"cli_code" => $acl['cli_code'],
																"acl_id" => $acl['acl_id'],
																"fac_nb_stagiaires" => $acl['fac_nb_stagiaires']
															);
														}else{
															$retour[$acl['acl_produit']]['clients'][] = array(
																"cli_id" => $acl['cli_id'],
																"cli_code" => $acl['cli_code'],
																"acl_id" => $acl['acl_id'],
																"fac_nb_stagiaires" => 0
															);
														}
													}
                                                    ?>
                                                <tr id="tr_rap_<?= $f['for_id'] ?>">
													<td>
														<a href="#" data-toggle="modal" data-target="#modal_suppr_date" data-rap_id="<?= $f['for_id'] ?>" class='btn btn-xs btn-danger delete-rap' data-toggle="tooltip" data-placement="top" title="Supprimer">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                        <a href="rap.php?for_id=<?= $f['for_id'] ?>" class='btn btn-xs btn-warning'data-toggle="tooltip" data-placement="top" title="Modifier">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <!-- <a href="rap_import.php?for_id=<?= $f['for_id'] ?>" class='btn btn-xs btn-info'data-toggle="tooltip" data-placement="top" title="Importer depuis le planning">
                                                            <i class="fa fa-download"></i>
                                                        </a> -->
                                                    </td>
                                                    <td>
                                                        <?= convert_date_txt($f['for_date']) ?>
                                                    </td>
                                                    <td>
                                                        <?php if($f['for_demi'] == 1){ ?>
                                                         Matin
													 <?php }elseif($f['for_demi'] == 1.5){ ?>
                                                            Toute la journée
                                                        <?php }else{ ?>
															Après-midi
														<?php }?>

                                                    </td>
                                                    <td>
														<?= $types[$f['for_utilisation']] ?>
                                                    </td>
                                                    <td>
													<?php
													$nb_stas = 0;
													if(!empty($d_formations_actions)){
														?>
														<table class="table">
															<thead>
																<th>Produit</th>
																<th>Client</th>
																<th>Nb stagiaires</th>
															</thead>
															<tbody>
																<?php

																foreach($retour as $re){
																	foreach($re['clients'] as $k1=>$c){
																	$nb_stas = $nb_stas + $c['fac_nb_stagiaires'];
																	?>
																	<tr>
																		<?php if($k1==0){ ?>
																		<td rowspan="<?= count($re['clients']) ?>"><?= $re['pro_code_produit'] ?></td>
																	<?php } ?>
																		<td><?= $c['cli_code'] ?></td>
																		<td><?= $c['fac_nb_stagiaires'] ?></td>
																	</tr>

																<?php
																	}

																}?>

															</tbody>
														</table>
													<?php } ?>
                                                    </td>
													<td>
														<?= $f['for_nb_demi'] ?>
													</td>
                                                    <td>
                                                          <?= $f['for_nb_groupe'] ?>
                                                    </td>
                                                    <td>
                                                          <?= $f['for_nb_h_groupe'] ?>
                                                    </td>
                                                    <td>
                                                          <?= $f['for_total_h'] ?>
                                                    </td>
                                                    <td>
														<?= $nb_stas ?>
                                                    </td>
                                                    <td>
                                                          <?= nl2br($f['for_observations']) ?>
                                                    </td>
                                                    <td>
														<?php if(!empty($vehicules[$f['for_vehicule_id']])){ ?>
                                                          <?= $vehicules[$f['for_vehicule_id']]['veh_libelle'] ?>
													  <?php }?>
                                                    </td>
                                                    <td>
                                                        <?= $f['for_vehicule_numero'] ?>
                                                    </td>
                                                    <td>
                                                          <?=number_format($f['for_km'], 0, ',', ' ')?>
                                                    </td>
                                                    <td>
                                                            <?php if(!empty($f['for_hebergement'])){ ?>
                                                                <?php if($f['for_hebergement'] == 1){ ?>
                                                                    Hôtel
                                                                <?php }else{ ?>
                                                                    Autres
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <?php if(!empty($f['for_hebergement_texte'])){ ?>
                                                            <br>
                                                            <?= $f['for_hebergement_texte'] ?>
                                                            <?php } ?>
                                                    </td>

                                                </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                    <!-- </div> -->

                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="alert alert-warning">
                            Aucun rapport hebdomadaire.
                        </div>
                        <?php }?>


					</section>
				</section>
			</div>
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left">
                    <?php if(!empty($_SESSION['retour'])){ ?>
                        <a href="rap_for_liste.php?utilisateur=<?= $utilisateur ?>" class="btn btn-default btn-sm">
                    <?php }?>

                            <i class="fa fa-long-arrow-left"></i>
                            Retour
                        </a>
					</div>
					<div class="col-xs-6 footer-middle text-center">
                        <a href="rap_import.php?annee=<?= $annee ?>&intervenant=<?= $d_intervenant['int_id'] ?>&semaine=<?= $semaine ?>" class="btn btn-info btn-sm">
                            Importer les rapports non remplis depuis le planning
                        </a>

                    </div>
					<div class="col-xs-3 footer-right">
                        <a href="rap.php?annee=<?= $annee ?>&intervenant=<?= $d_intervenant['int_id'] ?>&semaine=<?= $semaine ?>" class="btn btn-success btn-sm">
                            Ajouter une formation
                        </a>
                    </div>
				</div>
			</footer>
		</form>
		<!-- MODAL SUPPRESSION DATE -->
		<div id="modal_suppr_date" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Supprimer une date</h4>
					</div>

					<div class="modal-body">
						Êtes-vous sûr de vouloir supprimer ce rapport ?

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-success btn-sm btn-supp-rap-modal" data-rap_id=""> <i class="fa fa-check"></i> Oui</a>
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
					</div>
				</div>

			</div>
		</div>
<?php	include "includes/footer_script.inc.php"; ?>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function (){
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
					 $('.table-responsive').css("overflow","visible");
				}
				$(".btn-supp-rap-modal").click(function(){
				   rap_id=$(this).data("rap_id");
				   supprimer_rap(rap_id);
				});
				$(".delete-rap").click(function(){
				   rap_id=$(this).data("rap_id");
				   $(".btn-supp-rap-modal").data("rap_id", rap_id);
				});
            });
			// supprimer une date
			 function supprimer_rap(rap_id){

				$.ajax({
					type:'GET',
					url: 'ajax/ajax_delete_rap.php',
					data : 'rap_id=' + rap_id,
					dataType: 'html',
					success: function(data){

						$("#tr_rap_" + rap_id).remove();
						$("#modal_suppr_date").modal("hide");

					},
					error: function(data){
						afficher_message("Erreur","danger",data.responseText);
					}
				});
			 }
		</script>
	</body>
</html>
