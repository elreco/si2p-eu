 <?php

	// EDITION DES DEVIS

	include "includes/controle_acces.inc.php";

	include_once("includes/connexion_soc.php");
	include_once("includes/connexion_fct.php");
	include_once("includes/connexion.php");

	include_once("modeles/mod_parametre.php");
	include 'modeles/mod_get_commerciaux.php';
	include 'modeles/mod_get_adresses.php';
	include 'modeles/mod_get_contacts.php';

	/********************************************
		PARAMETRE
	********************************************/
	$erreur_txt="";

	$devis_id=0;
	if(isset($_GET["devis"])){
		if(!empty($_GET["devis"])){
			$devis_id=$_GET["devis"];
		}
	}

	$client_id=0;
	if(isset($_GET["client"])){
		if(!empty($_GET["client"])){
			$client_id=$_GET["client"];
		}
	}

	$corresp_id=0;
	if(isset($_GET["correspondance"])){
		if(!empty($_GET["correspondance"])){
			$corresp_id=$_GET["correspondance"];
		}
	}

	/********************************************
		DONNEE POUR TRAITEMENT
	********************************************/

	// la personne logue

	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];
	}

	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	// ON RECUPERE LES INFOS SI LE DEVIS EXISTE

	$dev_commercial=0;

	$devis_vide=true;
	if($devis_id>0){

		$devis_vide=false;

		$sql="SELECT * FROM Devis WHERE dev_id=" . $devis_id . ";";
		$req=$ConnSoc->query($sql);
		$d_devis=$req->fetch();
		if(!empty($d_devis)){

			$dev_agence=$d_devis["dev_agence"];
			$DTime_dev_date = date_create_from_format('Y-m-d',$d_devis["dev_date"]);

		}else{
			$erreur_txt="Impossible d'afficher la page!";
		}

		if(empty($erreur_txt)){

			// info sur le client
			$adr_type=1;
			$sql="SELECT cli_id,cli_code,cli_nom,cli_groupe,cli_filiale_de,cli_fil_de,cli_niveau,cli_categorie FROM Clients WHERE cli_id=:client;";
			$req=$Conn->prepare($sql);
			$req->bindParam(":client",$d_devis["dev_client"]);
			$req->execute();
			$d_client=$req->fetch();
			if(!empty($d_client)){
				if($d_client["cli_categorie"]==3){
					$adr_type=2;
				}
			}

		}

	}elseif(!empty($client_id)){

		$sql="SELECT cli_nom,cli_code,cli_categorie,cli_reg_formule,cli_reg_type,cli_reg_nb_jour,cli_reg_fdm
		,cso_agence,cso_commercial
		,adr_id,adr_nom,adr_service,adr_ad1,adr_ad2,adr_ad3,adr_cp,adr_ville,adr_geo
		,con_id,con_titre,con_nom,con_prenom,con_tel,con_portable,con_mail
		FROM Clients INNER JOIN Clients_Societes ON (Clients.cli_id=Clients_Societes.cso_client)
		LEFT JOIN Adresses ON (Clients.cli_adresse=Adresses.adr_id)
		LEFT JOIN Contacts ON (Clients.cli_contact=Contacts.con_id)";
		$sql.= " WHERE cli_id=" . $client_id . " AND cso_societe = " . $acc_societe . " AND NOT cso_archive";
		if(!empty($acc_agence)){
			$sql.= " AND cso_agence = " . $acc_agence;
		}
		if(!$_SESSION['acces']["acc_droits"][6]){
			$sql.=" AND cso_utilisateur=" . $acc_utilisateur;
		}
		/*var_dump($sql);
		die();*/
		$req=$Conn->query($sql);
		$data_devis=$req->fetchAll();
		if(!empty($data_devis)){
			if(count($data_devis)==1){

				$devis_vide=false;

				$adr_type=1;
				if($data_devis[0]["cli_categorie"]==3){
					$adr_type=2;
				}

				$d_client=array(
					"cli_id" => $client_id,
					"cli_categorie" => $data_devis[0]["cli_categorie"],
					"cli_nom" => $data_devis[0]["cli_nom"],
					"cli_code" => $data_devis[0]["cli_code"]
				);

				$dev_agence=$data_devis[0]["cso_agence"];
				$d_devis["dev_client"]=$client_id;
				$d_devis["dev_cli_categorie"]=$data_devis[0]["cli_categorie"];

				$d_devis["dev_adresse"]=$data_devis[0]["adr_id"];
				$d_devis["dev_adr_nom"]=$data_devis[0]["adr_nom"];
				$d_devis["dev_adr_service"]=$data_devis[0]["adr_service"];
				$d_devis["dev_adr1"]=$data_devis[0]["adr_ad1"];
				$d_devis["dev_adr2"]=$data_devis[0]["adr_ad2"];
				$d_devis["dev_adr3"]=$data_devis[0]["adr_ad3"];
				$d_devis["dev_adr_cp"]=$data_devis[0]["adr_cp"];
				$d_devis["dev_adr_ville"]=$data_devis[0]["adr_ville"];
				$d_devis["dev_adr_geo"]=$data_devis[0]["adr_geo"];

				$d_devis["dev_contact"]=$data_devis[0]["con_id"];
				$d_devis["dev_con_titre"]=$data_devis[0]["con_titre"];
				$d_devis["dev_con_nom"]=$data_devis[0]["con_nom"];
				$d_devis["dev_con_prenom"]=$data_devis[0]["con_prenom"];
				$d_devis["dev_con_tel"]=$data_devis[0]["con_tel"];
				$d_devis["dev_con_portable"]=$data_devis[0]["con_portable"];
				$d_devis["dev_con_mail"]=$data_devis[0]["con_mail"];

				$d_devis["dev_libelle"]="";
				$d_devis["dev_total_ht"]="";

				$d_devis["dev_commercial"]=$data_devis[0]["cso_commercial"];
				$d_devis["dev_reference"]="";
				$d_devis["dev_esperance"]=30;
				$d_devis["dev_commande"]=false;
				$d_devis["dev_validite"]="3 mois";
				$d_devis["dev_bordereau"]=false;
				$d_devis["dev_reg_formule"]=$data_devis[0]["cli_reg_formule"];
				$d_devis["dev_reg_type"]=$data_devis[0]["cli_reg_type"];
				$d_devis["dev_reg_nb_jour"]=$data_devis[0]["cli_reg_nb_jour"];
				$d_devis["dev_reg_fdm"]=$data_devis[0]["cli_reg_fdm"];

				$d_devis["dev_cli_societe"]=$acc_societe;

				$d_devis["dev_correspondance"]=$corresp_id;

				$DTime_dev_date = new DateTime();

			}
		}
	}


	if(empty($erreur_txt)){

		if(!$devis_vide){

			// DONNEE COMP POUR COMPLETER LE DEVIS

			// adresse

			$adresses=get_adresses(1,$d_devis["dev_client"],$adr_type);

			//contacts (tous indépendanment de l'adresse)
			$contacts=get_contacts(1,$d_devis["dev_client"],null,null);

		}else{

			// PAS DE DONNEE RENSEIGNEE

			$d_client=array(
				"cli_id" => 0,
				"cli_categorie" => 0,
				"cli_nom" => ""
			);

			$dev_agence=0;
			$d_devis["dev_client"]=0;

			$d_devis["dev_adresse"]=0;
			$d_devis["dev_adr_nom"]="";
			$d_devis["dev_adr_service"]="";
			$d_devis["dev_adr1"]="";
			$d_devis["dev_adr2"]="";
			$d_devis["dev_adr3"]="";
			$d_devis["dev_adr_cp"]="";
			$d_devis["dev_adr_ville"]="";

			$d_devis["dev_contact"]=0;
			$d_devis["dev_con_titre"]=0;
			$d_devis["dev_con_nom"]="";
			$d_devis["dev_con_prenom"]="";
			$d_devis["dev_con_tel"]="";
			$d_devis["dev_con_portable"]="";
			$d_devis["dev_con_mail"]="";

			$d_devis["dev_libelle"]="";
			$d_devis["dev_total_ht"]="";

			$d_devis["dev_commercial"]=0;
			$d_devis["dev_reference"]="";
			$d_devis["dev_esperance"]=30;
			$d_devis["dev_commande"]=false;
			$d_devis["dev_validite"]="3 mois";
			$d_devis["dev_bordereau"]=false;
			$d_devis["dev_reg_formule"]=1;
			$d_devis["dev_reg_nb_jour"]=0;
			$d_devis["dev_reg_fdm"]=0;
			$d_devis["dev_cli_categorie"]=1;
			$d_devis["dev_cli_societe"]=0;
			$DTime_dev_date = new DateTime();
			$d_devis["dev_adr_geo"]=1;
			$d_devis["dev_reg_type"]=2;

			$d_devis["dev_correspondance"]=$corresp_id;


		}
	}

	if(empty($erreur_txt)){

		// sur la societe

		if($acc_agence==0){
			$req=$Conn->query("SELECT age_id,age_nom FROM Agences,Societes WHERE age_societe=soc_id AND soc_agence AND soc_id=" . $acc_societe . ";");
			$d_agences=$req->fetchAll();
			if(empty($d_agences)){
				unset($d_agences);
				$dev_agence=0;
			}
		}else{
			$dev_agence=$acc_agence;
		}


		$base_tva=array(
			0 => "&nbsp;",
			1 => "Normale",
			2 => "Réduit alimentaire",
			3 => "Sans TVA",
			4 => "Réduit",
			5 => "Spé"
		);

		// commerciaux accéssible

		$com_source=get_commerciaux($acc_societe,$dev_agence,0);

		// CATEGORIE DE PRODUITS

		$req=$Conn->query("SELECT pca_id,pca_libelle,pca_planning FROM Produits_Categories WHERE NOT pca_compta ORDER BY pca_libelle;");
		$d_pro_categories=$req->fetchAll();

		// LES FAMILLES

		$req=$Conn->query("SELECT pfa_id,pfa_libelle FROM Produits_Familles ORDER BY pfa_libelle;");
		$d_pro_familles=$req->fetchAll();

	} ?>
	<!DOCTYPE html>
	<html lang="fr" >
		<head>
			<meta charset="utf-8">
			<title>SI2P - Orion</title>
			<meta name="keywords" content=""/>
			<meta name="description" content="">
			<meta name="author" content="">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<!-- CSS THEME -->
			<link href="assets/skin/si2p/css/theme.css" rel="stylesheet" type="text/css" >
			<link href="assets/admin-tools/admin-forms/css/admin-forms.css" rel="stylesheet" type="text/css">


			<!-- CSS PLUGINS -->
			<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
			<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />

			<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" />

			<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >

			<!-- CSS Si2P -->
			<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

			<!-- Favicon -->
			<link rel="shortcut icon" href="assets/img/favicon.png">

			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
			<![endif]-->



		</head>
		<body class="sb-top sb-top-sm ">

			<form method="post" action="devis_enr.php" id="form_devis" >
				<div>
					<!--hotfix/devis-duplicate-id
					<input type="hidden" id="id_heracles" name="id_heracles" value="0" />-->
					<input type="hidden" id="devis" name="devis" value="<?=$devis_id?>" />
		<?php		if(!isset($d_agences)){ ?>
						<input type="hidden" name="dev_agence" id="agence" class="get-client-agence" value="<?=$dev_agence?>" />
		<?php		} ?>
					<input type="hidden" id="num_ligne" name="num_ligne" value="" />
					<input type="hidden" name="dev_correspondance" value="<?=$d_devis["dev_correspondance"]?>" />

				</div>
				<div id="main">
		<?php		include "includes/header_def.inc.php"; ?>
					<section id="content_wrapper">

						<section id="content" class="animated">

				<?php		if(!empty($erreur_txt)){
								echo("<p>" . $erreur_txt . "</p>");
							}else{ ?>

								<div class="row" >

									<div class="col-md-12">

										<div class="admin-form theme-primary ">

											<div class="panel heading-border panel-primary">

												<div class="panel-body bg-light">

													<div class="content-header mtn mbn">
												<?php	if($devis_id==0){
															echo("<h2>Nouveau <b class='text-primary' >devis</b></h2>");
														}else{
															echo("<h2>Modification <b class='text-primary' >" . $d_devis["dev_numero"] . "</b></h2>");
														}?>
													</div>

													<!-- AFFICHAGE DEVIS -->

													<div class="row" >

														<!-- COLONNE DE GAUCHE -->
														<div class="col-md-6" >
												<?php		if(isset($d_agences)){
																if(!empty($d_agences)){ ?>
																	<div class="row" >
																		<div class="col-md-2 mt5 text-right" >Agence :</div>
																		<div class="col-md-10" >
																			<div class="select2-sm" >
																				<select name="dev_agence" id="agence" class="select2 select2-com-agence client-n-agence get-client-agence" data-com_archive="0" required >
																					<option value="0" >Sélectionnez une agence</option>
																			<?php	foreach($d_agences as $a){
																						if($a["age_id"]==$dev_agence){
																							echo("<option value='" . $a["age_id"] . "' selected >" . $a["age_nom"] . "</option>");
																						}else{
																							echo("<option value='" . $a["age_id"] . "' >" . $a["age_nom"] . "</option>");
																						}
																					}?>
																				</select>
																			</div>
																			<p class="text-danger" id="text_agence" <?php if(!isset($d_agences) OR !empty($dev_agence)) echo("style='display:none;'"); ?> >
																				Merci de selectionner une agence pour accéder à la liste des clients.
																			</p>
																		</div>
																	</div>


												<?php			}
															} ?>
															<div class="row mt5" >
																<div class="col-md-2 mt5 text-right" >Client :</div>
																<div class="col-md-10" >
																	<div class="select2-sm" >
																		<select id="client" name="dev_client" required class="select2-client-n get-client select2-produit-client" <?php if(isset($d_agences) AND empty($dev_agence)) echo("disabled"); ?> data-archive="0" data-adr_type="1" data-adr_contact="" >
																	<?php	if(!empty($d_client["cli_id"])){ ?>
																				<option value='<?=$d_client["cli_id"]?>' selected='selected' ><?=$d_client["cli_nom"]?> (<?=$d_client["cli_code"]?>)</option>
																	<?php	}else{ ?>
																				<option value='0' selected='selected' >Client ...</option>
																	<?php	} ?>
																		</select>
																	</div>
																</div>
															</div>

															<div class="row mt5" >
																<div class="col-md-12 section-divider" >
																	<span>Adresse</span>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12" >
																	<div class="select2-sm" >
																		<select name="dev_adresse" id="adresse" class="select2" <?php if($d_client["cli_categorie"]==3) echo("disabled"); ?> >
																			<option value="0" >Selectionnez une adresse</option>
																<?php		if(!empty($adresses)){
																				foreach($adresses as $ad){
																					if(!empty($ad["adr_libelle"])){
																						$adr_libelle=$ad["adr_libelle"];
																					}else{
																						$adr_libelle=$ad["adr_nom"];
																					}
																					if($d_devis["dev_adresse"]==$ad["adr_id"]){
																						echo("<option value='" . $ad["adr_id"] . "' selected >" . $adr_libelle . "</option>");
																					}else{
																						echo("<option value='" . $ad["adr_id"] . "' >" . $adr_libelle . "</option>");
																					}
																				}
																			}?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12" >
																	<input type="text" id="adr_nom" name="dev_adr_nom" class="gui-input gui-input-sm nom champ-adresse" placeholder="Nom" value="<?=$d_devis["dev_adr_nom"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12" >
																	<input type="text" id="adr_service" name="dev_adr_service" class="gui-input gui-input-sm champ-adresse" placeholder="Service" value="<?=$d_devis["dev_adr_service"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12" >
																	<input type="text" id="adr1" name="dev_adr1" class="gui-input gui-input-sm champ-adresse" placeholder="Adresse" value="<?=$d_devis["dev_adr1"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12" >
																	<input type="text" id="adr2" name="dev_adr2" class="gui-input gui-input-sm champ-adresse" placeholder="Complément 1" value="<?=$d_devis["dev_adr2"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12" >
																	<input type="text" id="adr3" name="dev_adr3" class="gui-input gui-input-sm champ-adresse" placeholder="Complément 2" value="<?=$d_devis["dev_adr3"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-3" >
																	<input type="text" id="adr_cp" name="dev_adr_cp" class="gui-input gui-input-sm code-postal champ-adresse" placeholder="CP" value="<?=$d_devis["dev_adr_cp"]?>" />
																</div>
																<div class="col-md-9" >
																	<input type="text" id="adr_ville" name="dev_adr_ville" class="gui-input gui-input-sm nom champ-adresse" placeholder="Ville" value="<?=$d_devis["dev_adr_ville"]?>" />
																</div>
															</div>
															<div class="row mt5">
																<div class="col-md-6 text-center">
																	Zone géographique
																	<div class="section">
																		<label for="adr_geo1" class="mt15 option option-info">
																			<input type="radio" id="adr_geo1" class="adr-geo" name="dev_adr_geo" value="1" <?php if($d_devis["dev_adr_geo"]==1) echo("checked"); ?> />
																			<span class="radio"></span> France
																		</label>
																		<label for="adr_geo2" class=" mt15 option option-info">
																			<input type="radio" id="adr_geo2" class="adr-geo" name="dev_adr_geo" value="2" <?php if($d_devis["dev_adr_geo"]==2) echo("checked"); ?> />
																			<span class="radio"></span> UE
																		</label>
																		<label for="adr_geo3" class=" mt15 option option-info">
																			<input type="radio" id="adr_geo3" class="adr-geo" name="dev_adr_geo" value="3" <?php if($d_devis["dev_adr_geo"]==3) echo("checked"); ?> />
																			<span class="radio"></span> Autre
																		</label>
																	</div>
																</div>
																<div class="col-md-6 text-center pt25">

																	<div class="option-group field" id="bloc_adr_add" <?php if(!empty($d_devis["dev_adresse"])) echo("style='display:none;'"); ?> >
																		<label class="option option-dark">
																			<input type="checkbox" name="adr_add" value="1">
																			<span class="checkbox"></span>Enregistrer comme nouvelle adresse
																		</label>
																	</div>
																	<div class="option-group field" id="bloc_adr_maj" <?php if(empty($d_devis["dev_adresse"])) echo("style='display:none;'"); ?> >
																		<label class="option option-dark">
																			<input type="checkbox" name="adr_maj" value="1">
																			<span class="checkbox"></span>Mettre à jour l'adresse
																		</label>
																	</div>

																</div>
															</div>

															<!--CONTACT -->
															<div class="row mt5" >
																<div class="col-md-12 section-divider" >
																	<span>Contact</span>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12" >
																	<div class="select2-sm" >
																		<select name="dev_contact" id="contact" class="select2 actu-contacts aff-contact" >
																			<option value="0" >Selectionnez un contact client</option>
																<?php		if(!empty($contacts)){
																				foreach($contacts as $c){
																					if($d_devis["dev_contact"]==$c["con_id"]){
																						echo("<option value='" . $c["con_id"] . "' selected >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
																					}else{
																						echo("<option value='" . $c["con_id"] . "' >" . $c["con_nom"] . " ". $c["con_prenom"] . "</option>");
																					}
																				}
																			}?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-4" >
																	<div class="select2-sm" >
																		<select name="dev_con_titre" id="con_titre" class="select2" >
																			<option value="0">Civilité...</option>
																			<option value="1" <?php if($d_devis["dev_con_titre"]==1) echo("selected"); ?> >M.</option>
																			<option value="2" <?php if($d_devis["dev_con_titre"]==2) echo("selected"); ?> >Mme</option>
																		</select>
																	</div>
																</div>
																<div class="col-md-4" >
																	<input type="text" id="con_nom" name="dev_con_nom" class="gui-input gui-input-sm nom champ-contact" placeholder="Nom" value="<?=$d_devis["dev_con_nom"]?>" />
																</div>
																<div class="col-md-4" >
																	<input type="text" id="con_prenom" name="dev_con_prenom"  class="gui-input gui-input-sm prenom champ-contact" placeholder="Prénom" value="<?=$d_devis["dev_con_prenom"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-6" >
																	<input type="text" id="con_tel" name="dev_con_tel"  class="gui-input gui-input-sm telephone champ-contact" placeholder="Téléphone" value="<?=$d_devis["dev_con_tel"]?>" />
																</div>
																<div class="col-md-6" >
																	<input type="text" id="con_portable" name="dev_con_portable"  class="gui-input gui-input-sm telephone champ-contact" placeholder="Portable" value="<?=$d_devis["dev_con_portable"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-6" >
																	<input type="text" id="con_mail" name="dev_con_mail"  class="gui-input gui-input-sm champ-contact" placeholder="Mail" value="<?=$d_devis["dev_con_mail"]?>" />
																</div>
																<div class="col-md-6" >
																	<div class="option-group field" id="bloc_con_add" <?php if(!empty($d_devis["dev_contact"])) echo("style='display:none;'"); ?> >
																		<label class="option option-dark">
																			<input type="checkbox" name="con_add" value="1">
																			<span class="checkbox"></span>Enregistrer comme nouveau contact
																		</label>
																	</div>
																	<div class="option-group field" id="bloc_con_maj" <?php if(empty($d_devis["dev_contact"])) echo("style='display:none;'"); ?> >
																		<label class="option option-dark">
																			<input type="checkbox" name="con_maj" value="1" >
																			<span class="checkbox"></span>Mettre à jour le contact
																		</label>
																	</div>
																</div>
															</div>

														</div>
														<!-- FIN COLONNE DE GAUCHE -->

														<!-- COLONNE DE DROITE -->
														<div class="col-md-6" >

															<div class="row mt5" >
																<div class="col-md-4 mt5 text-right" >Commercial :</div>
																<div class="col-md-8" >
																	<div class="select2-sm" >
																		<select class="select2 select2-commercial actu-client-select2-com" id="commercial" name="dev_commercial" required >
																			<option value="" >&nbsp;</option>
																	<?php	if(!empty($com_source)){
																				foreach($com_source as $com){
																					if($com["com_id"]==$d_devis["dev_commercial"]){
																						echo("<option value='" . $com["com_id"] . "' selected >" . $com["com_label_1"] . " " . $com["com_label_2"] . "</option>");
																					}else{
																						echo("<option value='" . $com["com_id"] . "' >" . $com["com_label_1"] . " " . $com["com_label_2"] . "</option>");
																					}
																				};
																			}; ?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-4 mt10 text-right" >Référence :</div>
																<div class="col-md-8" >
																	<input type="text" name="dev_reference" class="gui-input gui-input-sm" placeholder="Référence" value="<?=$d_devis["dev_reference"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-4 mt10 text-right" >Fiabilité :</div>
																<div class="col-md-8" >

																	<div class="select2-sm" >
																		<select class="select2 select2-sm adresse-client" name="dev_esperance" >
																			<option value="" >&nbsp;</option>
																			<option value="0" <?php if($d_devis["dev_esperance"]==0) echo("selected"); ?> >Sans suite</option>
																			<option value="30" <?php if($d_devis["dev_esperance"]==30) echo("selected"); ?> >30%</option>
																			<option value="50" <?php if($d_devis["dev_esperance"]==50) echo("selected"); ?> >50%</option>
																			<option value="90" <?php if($d_devis["dev_esperance"]==90) echo("selected"); ?> >90%</option>
																			<option value="100" <?php if($d_devis["dev_esperance"]==100) echo("selected"); ?> >Commandé</option>
																		</select>
																	</div>

																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-4 mt10 text-right" >Date :</div>
																<div class="col-md-8" >
																	<span class="field prepend-icon prepend-icon-sm">
																		<input type="text" size="10" name="dev_date" id="date_devis" class="gui-input datepicker" placeholder="Selectionner une date" value="<?=$DTime_dev_date->format("d/m/Y")?>" />
																		<label class="field-icon" for="date_devis" ><i class="fa fa-calendar-o"></i></label>
																	</span>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-4 mt10 text-right" >Validité :</div>
																<div class="col-md-8" >
																	<input type="text" id="validite" name="dev_validite" size="25" class="gui-input gui-input-sm" placeholder="" value="<?=$d_devis["dev_validite"]?>" />
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-4 mt10 text-right" >Bordereau de prix :</div>
																<div class="col-md-8 mt5" >
																	<label class="option">
																		<input type="checkbox" name="dev_bordereau" value="on" <?php if($d_devis["dev_bordereau"]) echo("checked"); ?> />
																		<span class="checkbox"></span>
																	</label>
																</div>
															</div>

															<div class="section-divider" >
																<span>Modalité de règlement</span>
															</div>

															<div class="row mt5" >
																<div class="col-md-12">
																	<label for="reg_type" >Paiement :</label>
																	<select name="dev_reg_type" id="reg_type" class="select2" >
															<?php		foreach($base_reglement as $reg_type => $reg_lib){
																			if($reg_type!=0){
																				if($reg_type==$d_devis["dev_reg_type"]){
																					echo("<option value='" . $reg_type . "' selected >" . $reg_lib . "</option>");
																				}else{
																					echo("<option value='" . $reg_type . "' >" . $reg_lib . "</option>");
																				}
																			}
																		} ?>
																	</select>
																</div>
															</div>

															<div class="row mt5" >
																<div class="col-md-12">
															<?php	if($d_devis["dev_reg_formule"]==1){ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_1" class="reg-formule" name="dev_reg_formule" value="1" checked >
																			<span class="radio"></span>

																		</label>
																		Date + <input type="number" max="60" size="2" id="reg_nb_jour_1" name="dev_reg_nb_jour_1" value="<?=$d_devis["dev_reg_nb_jour"]?>" class="champ-reg-formule reg-formule-1" /> jours
															<?php	}else{ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_1" class="reg-formule" name="dev_reg_formule" value="1" />
																			<span class="radio"></span>
																		</label>
																		Date + <input type="number" max="60" size="2" id="reg_nb_jour_1" name="dev_reg_nb_jour_1" value="0" class="champ-reg-formule reg-formule-1" disabled /> jours
															<?php	} ?>
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
																	<label class="option">
																		<input type="radio" id="reg_formule_2" class="reg-formule" name="dev_reg_formule" value="2" <?php if($d_devis["dev_reg_formule"]==2) echo("checked"); ?> >
																		<span class="radio"></span>
																	</label>
																	Date + 45j + fin de mois
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
																	<label class="option">
																		<input type="radio" id="reg_formule_3" class="reg-formule" name="dev_reg_formule" value="3" <?php if($d_devis["dev_reg_formule"]==3) echo("checked"); ?> >
																		<span class="radio"></span>
																	</label>
																	Date + Fin de mois + 45j
																</div>
															</div>
															<div class="row mt5" >
																<div class="col-md-12">
															<?php	if($d_devis["dev_reg_formule"]==4){ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_4" class="reg-formule" name="dev_reg_formule" value="4" checked />
																			<span class="radio"></span>
																		</label>
																		Date + <input type="number" max="30" size="2" id="reg_nb_jour_4" name="dev_reg_nb_jour_4" value="<?=$d_devis["dev_reg_nb_jour"]?>" class="champ-reg-formule reg-formule-4" /> jours
																		+ Fin de mois +  <input type="number" size="2" max="20" id="reg_fdm_4" name="dev_reg_fdm_4" value="<?=$d_devis["dev_reg_fdm"]?>" class="champ-reg-formule reg-formule-4" /> jours
															<?php	}else{ ?>
																		<label class="option">
																			<input type="radio" id="reg_formule_4" class="reg-formule" name="dev_reg_formule" value="4" />
																			<span class="radio"></span>
																		</label>
																		Date + <input type="number" max="30" size="2" id="reg_nb_jour_4" name="dev_reg_nb_jour_4" value="0" disabled class="champ-reg-formule reg-formule-4" /> jours
																		+ Fin de mois +  <input type="number" size="2" max="20" id="reg_fdm_4" name="dev_reg_fdm_4" value="0" disabled class="champ-reg-formule reg-formule-4" /> jours
															<?php	} ?>
																</div>
															</div>

														</div>
														<!-- FIN COLONNE DE GAUCHE -->
													</div>

													<div class="table-responsive mt10" >
														<table class="table" id="table_devis" >
															<thead>
																<tr class="dark" >
																	<th>&nbsp;</th>
																	<th class="text-center" >Référence</th>
																	<th class="text-center" >Désignation</th>
																	<th class="text-center" >TG</th>
																	<th class="text-center" >Prix HT</th>
																	<th class="text-center" >Qté</th>
																	<th class="text-center" >Remise / TG</th>
																	<th class="text-center" >% Remise / TG</th>
																	<th class="text-center" >Montant HT</th>
															<?php	if($_SESSION['acces']["acc_droits"][9]){ ?>
																		<th id="gest_tva" class="text-center" >TVA</th>
															<?php	} ?>
																</tr>
															</thead>
															<tbody>
												<?php			$sql="SELECT * FROM Devis_Lignes WHERE dli_devis=" . $devis_id . " ORDER BY dli_id;";
																$req=$ConnSoc->query($sql);
																$lignes=$req->fetchAll();
																if(!empty($lignes)){
																	$num_ligne=0;
																	foreach($lignes as $l){
																		$num_ligne++;

																		$att_ca_verrou="";
																		$att_btn_derog="";
																		if($l["dli_derogation"]==2 AND !$_SESSION["acces"]["acc_droits"][31]){
																			$att_ca_verrou="readonly";
																			$att_btn_derog="disabled";
																		}elseif($l["dli_derogation"]==1 AND !$_SESSION["acces"]["acc_droits"][31] AND !$_SESSION["acces"]["acc_droits"][27]){
																			$att_ca_verrou="readonly";
																			$att_btn_derog="disabled";
																		}?>
																		<tr id='devis_ligne_<?=$num_ligne?>' data-ligne="<?=$num_ligne?>" class="ligne" >
																			<td>
																				<button type='button' class='btn btn-xs btn-danger supp-ligne' data-ligne="<?=$num_ligne?>" data-toggle="tooltip" title="Supprimer la ligne" >
																					<i class='fa fa-times' ></i>
																				</button>

																				<button type='button' class='btn btn-xs btn-warning mod-ligne' data-ligne="<?=$num_ligne?>" data-toggle="tooltip" title="Changer de produit" >
																					<i class='fa fa-pencil' ></i>
																				</button>

																		<?php	if($_SESSION["acces"]["acc_droits"][27] OR $_SESSION["acces"]["acc_droits"][31]){ ?>
																					<button type='button' class='btn btn-xs btn-info derog-ligne' id="btn_derog_<?=$num_ligne?>" data-ligne="<?=$num_ligne?>" data-toggle="tooltip" title="Dérogation tarifaire" <?=$att_btn_derog?> >
																						<i class='fa fa-euro' ></i>
																					</button>
																		<?php	} ?>
																			</td>
																			<td>

																				<input type='hidden' id='id_<?=$num_ligne?>' name='dli_id_<?=$num_ligne?>' value='<?=$l["dli_id"]?>' />
																				<input type='hidden' id='produit_<?=$num_ligne?>' name='dli_produit_<?=$num_ligne?>' value='<?=$l["dli_produit"]?>' />
																				<input type='hidden' id='type_<?=$num_ligne?>' name='dli_type_<?=$num_ligne?>' value='<?=$l["dli_type"]?>' />
																				<input type='hidden' id='derogation_<?=$num_ligne?>' name='dli_derogation_<?=$num_ligne?>' value='<?=$l["dli_derogation"]?>' />
																				<input type='hidden' id='rem_famille_<?=$num_ligne?>' name='dli_rem_famille_<?=$num_ligne?>' value='<?=$l["dli_rem_famille"]?>' />
																				<input type='hidden' id='rem_ca_<?=$num_ligne?>' name='dli_rem_ca_<?=$num_ligne?>' value='<?=$l["dli_rem_ca"]?>' />
																				<input type='hidden' id='ht_min_<?=$num_ligne?>' name='dli_ht_min_<?=$num_ligne?>' value='<?=$l["dli_ht_min"]?>' />
																				<input type='hidden' id='action_<?=$num_ligne?>' name='dli_action_<?=$num_ligne?>' value='<?=$l["dli_action"]?>' />
																				<input type='hidden' id='action_client_<?=$num_ligne?>' name='dli_action_client_<?=$num_ligne?>' value='<?=$l["dli_action_client"]?>' />
																				<input type='hidden' id='action_cli_soc_<?=$num_ligne?>' name='dli_action_cli_soc_<?=$num_ligne?>' value='<?=$l["dli_action_cli_soc"]?>' />
																				<span id="reference_<?=$num_ligne?>" data-categorie="<?=$l["dli_categorie"]?>" data-famille="<?=$l["dli_famille"]?>" data-sous_famille="<?=$l["dli_sous_famille"]?>" data-sous_sous_famille="<?=$l["dli_sous_sous_famille"]?>" >
																					<?=$l["dli_reference"]?>
																				</span>

																			</td>
																			<td>
																				<input type='text' class="gui-input" id='libelle_<?=$num_ligne?>' name='dli_libelle_<?=$num_ligne?>' value="<?=$l["dli_libelle"]?>" size='50' /><br/>
																				<div class="pull-right" >
																					<button type="button" class="btn btn-xs btn-warning btn-texte" data-num_ligne="<?=$num_ligne?>" data-toggle="tooltip" title="Modifier la désignation" >
																						<i class="fa fa-pencil" ></i>
																					</button>
																				</div>

																				<div id="cont_texte_<?=$num_ligne?>" >
																					<?=html_entity_decode($l["dli_texte"])?>
																				</div>
																				<div id="cont_texte_date_<?=$num_ligne?>" >
																					<?=$l["dli_dates"]?>
																				</div>
																				<div id="cont_texte_sta_<?=$num_ligne?>" >
																					<?=$l["dli_stagiaires_txt"]?>
																				</div>
																				<input type="hidden" id="texte_<?=$num_ligne?>" name="dli_texte_<?=$num_ligne?>" value="<?=$l["dli_texte"]?>" />
																				<input type="hidden" id="texte_date_<?=$num_ligne?>" name="dli_dates_<?=$num_ligne?>" value="<?=$l["dli_dates"]?>" />
																				<input type="hidden" id="texte_sta_<?=$num_ligne?>" name="dli_stagiaires_txt_<?=$num_ligne?>" value="<?=$l["dli_stagiaires_txt"]?>" />
																			</td>
																			<td>
																				<input type='text' class='text-right gui-input' id='tg_<?=$num_ligne?>' name='dli_tg_<?=$num_ligne?>' value='<?=$l["dli_tg"]?>' size='8' readonly />
																			</td>
																			<td>
																				<input type='text'  class='gui-input tarif tarif-ht text-right' id='ht_<?=$num_ligne?>' name='dli_ht_<?=$num_ligne?>' value='<?=$l["dli_ht"]?>' size='8' <?=$att_ca_verrou?> />
																				<div id="bloc_alert_ht_<?=$num_ligne?>" class="text-danger" ></div>
																			</td>
																			<td>
																				<input type='text' class='gui-input tarif tarif-qte text-right' id='qte_<?=$num_ligne?>' name='dli_qte_<?=$num_ligne?>' value='<?=$l["dli_qte"]?>' size='5' />
																			</td>
																			<td>
																				<input type='text' class='gui-input tarif tarif-remise text-right' id='remise_<?=$num_ligne?>' name='dli_remise_<?=$num_ligne?>' value='<?=$l["dli_remise"]?>' size='9' maxlength="9" <?=$att_ca_verrou?> />
																			</td>
																			<td>
																				<input type='text' class='gui-input tarif tarif-remise-taux text-right' id='remise_taux_<?=$num_ligne?>' name='dli_taux_remise_<?=$num_ligne?>' value='<?=$l["dli_taux_remise"]?>' size='5' <?=$att_ca_verrou?> />
																			</td>
																			<td>
																				<input type='text' class='gui-input tarif tarif-montant text-right' id='montant_<?=$num_ligne?>' name='dli_montant_ht_<?=$num_ligne?>' value='<?=$l["dli_montant_ht"]?>' size='9' maxlength="9" <?=$att_ca_verrou?> />
																			</td>
																	<?php	if($_SESSION['acces']["acc_droits"][9]){ ?>
																				<td>
																					<label class="field select">
																						<select id="tva_<?=$num_ligne?>" name="dli_tva_id_<?=$num_ligne?>" >
																				<?php		foreach($base_tva as $k => $v){
																								if($l["dli_tva_id"]==$k){
																									echo("<option value='" . $k . "' selected >" . $v . "</option>");
																								}else{
																									echo("<option value='" . $k . "' >" . $v . "</option>");
																								}
																							} ?>
																						</select>
																						<i class="arrow"></i>
																					</label>
																				</td>
																	<?php	} ?>
																		</tr>
													<?php			}
																} ?>
															</tbody>
															<tfoot>
																<tr>
																	<td colspan="8" >
																		<input type="text" name="dev_libelle" class="gui-input" placeholder="Info complémentaire" value="<?=$d_devis["dev_libelle"]?>" />
																	</td>
																	<td class="text-right" >
																		<input type="text" id="montant_total" class="text-right" readonly size="9" maxlength="9" value="<?=$d_devis["dev_total_ht"]?>" />&nbsp;€
																	</td>
															<?php	if($_SESSION['acces']["acc_droits"][9]){ ?>
																		<td>&nbsp;</td>
															<?php	} ?>
																</tr>
															</tfoot>
														</table>
													</div>

													<!-- FIN AFFICHAGE DEVIS -->

												</div>
											</div>
										</div>
									</div>
								</div>
				<?php		} ?>
						</section>
					</section>
				</div>
				<footer id="content-footer" class="affix">
					<div class="row">
						<div class="col-xs-3 footer-left">
				<?php		if($devis_id>0){ ?>
								<a href="devis_voir.php?devis=<?=$devis_id?>" class="btn btn-sm btn-default" >
									Retour
								</a>
				<?php		}elseif(isset($_SESSION["retourDevis"])){ ?>
								<a href="<?=$_SESSION["retourDevis"]?>" class="btn btn-sm btn-default" >
									Retour
								</a>
				<?php		} ?>
						</div>
						<div class="col-xs-6 footer-middle"></div>
						<div class="col-xs-3 footer-right">
							<button type="button" class="btn btn-sm btn-success mod-ligne" data-ligne="0" >
								<i class="fa fa-plus" ></i> Ajouter produit
							</button>

							<button type="button" id="save_devis" class="btn btn-sm btn-success" >
								<i class="fa fa-save" ></i> Enregistrer
							</button>

						</div>
					</div>
				</footer>
			</form>

			<!--AJOUTE ET MOD DES CODES PRODUITS -->
			<div id="modal_produit" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
							×
							 </button>
							<h4 class="modal-title">Nouveau produit</h4>
						</div>
						<div class="modal-body">
							<div class="admin-form theme-primary ">

								<div class="row" >
									<div class="col-md-2 text-right mt5">Catégorie :</div>
									<div class="col-md-10">
										<select id="produit_categorie" name="pro_categorie" class="select2 select2-produit-cat" >
											<option value="">Catégorie ...</option>
							<?php 			if(!empty($d_pro_categories)){
												foreach($d_pro_categories as $pca){
													echo("<option value='" . $pca["pca_id"] . "' >" . $pca["pca_libelle"] . "</option>");
												}
											} ?>
										</select>
									</div>
								</div>
								<div class="row mt15 bloc-planning text-center" id="bloc_type" >
									<div class="col-md-2">&nbsp;</div>
									<div class="col-md-5" >
										<div class="section" >
											 <label class="option">
												<input type="radio" class="produit-type" name="produit_type" id="produit_intra" value="1" checked />
												<span class="radio"></span>Produit intra
											</label>
										</div>
									</div>

									<div class="col-md-5" >
										<div class="section" >
											 <label class="option">
												<input type="radio" class="produit-type" name="produit_type" id="produit_inter" value="2" />
												<span class="radio"></span>Produit inter
											</label>
										</div>
									</div>
								</div>
								<div class="row mt5 bloc-planning" id="bloc_fam" >
									<div class="col-md-2 text-right mt5">Famille :</div>
									<div class="col-md-10">
										<select id="produit_famille" name="pro_famille" class="select2" >
											<option value="0" >&nbsp;</option>
								<?php		if(!empty($d_pro_familles)){
												foreach($d_pro_familles as $f){
													echo("<option value='" . $f["pfa_id"] . "' >" . $f["pfa_libelle"] . "</option>");
												}
											} ?>
										</select>
									</div>
								</div>
								<div class="row mt5 bloc-planning" id="bloc_s_fam" >
									<div class="col-md-2 text-right mt5">Sous-famille :</div>
									<div class="col-md-10">
										<select id="produit_sous_famille" name="pro_sous_famille" class="select2" >
											<option value="0" >&nbsp;</option>
										</select>
									</div>
								</div>
								<div class="row mt5 bloc-planning" id="bloc_s_s_fam" >
									<div class="col-md-2 text-right mt5">Sous Sous-famille :</div>
									<div class="col-md-10">
										<select id="produit_sous_sous_famille" name="pro_sous_sous_famille" class="select2" >
											<option value="0" >&nbsp;</option>
										</select>
									</div>
								</div>
								<div class="row mt5" id="bloc_produit" >
									<div class="col-md-2 text-right mt10">Produit :</div>
									<div class="col-md-10">
										<select id="produit" name="produit" class="select2" >
											<option value="0" >&nbsp;</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success btn-sm" id="maj_ligne" data-ligne="0" >
								Ajouter
							</button>
							<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
								Fermer
							</button>
						</div>
					</div>
				</div>
			</div>

			<div id="modal_confirmation_user" class="modal fade" role="dialog" >
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title">Confirmer ?</h4>
						</div>
						<div class="modal-body">
							<p>Êtes-vous sûr de vouloir supprimer cette ligne ?</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
								Annuler
							</button>
							<button type="button" class="btn btn-success btn-sm" id="confirmation_user" >
								Confirmer
							</button>
						</div>
					</div>
				</div>
			</div>

			<!-- MAJ DU TEXTE D'UNE LIGNE DE DEVIS -->
			<div id="modal_texte" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
							×
							 </button>
							<h4 class="modal-title">Désignation</h4>
						</div>
						<div class="modal-body">
							<div class="admin-form theme-primary ">
								<div class="row mt5" >
									<div class="col-md-12" >
										<label for="text_texte" >Texte</label>
										<div id="cont_texte" >
											<textarea id="text_texte" ></textarea>
										</div>
									</div>
								</div>
								<div class="row mt5" >
									<div class="col-md-12" >
										<label for="text_texte_date" >Dates</label>
										<div id="cont_texte_date" >
											<textarea id="text_texte_date" ></textarea>
										</div>
									</div>
								</div>
								<div class="row mt5" >
									<div class="col-md-12" >
										<label for="text_texte_sta" >Stagiaires</label>
										<div id="cont_texte_sta" >
											<textarea id="text_texte_sta" ></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success btn-sm" id="sub_texte"  >
								Valider
							</button>
							<button type="button" class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
								Annuler
							</button>
						</div>
					</div>
				</div>
			</div>
			<!-- DEROGATION TARIFAIRE -->
			<div id="modal_derogation" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
							×
							 </button>
							<h4 class="modal-title">Dérogation tarifaire</h4>
						</div>
						<div class="modal-body">
							<div class="admin-form theme-primary ">
								<div class="row mt15" >
									<div class="col-md-12" >
										<div class="row mt15" >
											<div class="col-md-4 text-center" >
												<div class="radio-custom mb5">
													<input id="derog_niv_0" name="derogation" type="radio" value="0" />
													<label for="derog_niv_0">Pas de dérogation</label>
												</div>
											</div>
											<div class="col-md-4 text-center" >
												<div class="radio-custom mb5">
													<input id="derog_niv_1" name="derogation" type="radio" value="1" />
													<label for="derog_niv_1">Dérogation niv.1</label>
												</div>
											</div>
									<?php	if($_SESSION["acces"]["acc_droits"][31]){ ?>
												<div class="col-md-4 text-center" >
													<div class="radio-custom mb5">
														<input id="derog_niv_2" name="derogation" type="radio" value="2" />
														<label for="derog_niv_2">Dérogation niv.2</label>
													</div>
												</div>
									<?php	} ?>
										</div>
									</div>
								</div>
								<div class="row mt25" >
									<div class="col-md-6" >
										<div class="option-group field">
											<label class="option option-dark">
												<input type="checkbox" id="rem_famille" value="on" />
												<span class="checkbox"></span><b>Prime "Famille"</b>
											</label>
										</div>
									</div>
									<div class="col-md-6" >
										<div class="option-group field">
											<label class="option option-dark">
												<input type="checkbox" id="rem_ca" value="on" >
												<span class="checkbox"></span><b>Prime "CA"</b>
											</label>
										</div>
									</div>
								</div>
								<div class="row mt25" >
									<div class="col-md-6" >
										Cocher cette case pour que le CA généré par ce code produit soit pris en compte dans le calcul des primes "Familles".
									</div>
									<div class="col-md-6" >
										Cocher cette case pour que le CA généré par ce code produit soit pris en compte dans le calcul des primes "CA".
									</div>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-success btn-sm" id="sub_derogation" data-ligne="0" >
								Valider
							</button>
							<button type="button" class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
								Annuler
							</button>
						</div>
					</div>
				</div>
			</div>

	<?php	include "includes/footer_script.inc.php"; ?>
			<script type="text/javascript" src="vendor/plugins/select2/js/select2.min.js"></script>
			<!--<script type="text/javascript" src="vendor/plugins/DataTables/media/js/jquery.dataTables.js"></script>
			<script type="text/javascript" src="vendor/plugins/DataTables/extensions/RowReorder/js/dataTables.rowReorder.js"></script>-->
			<script type="text/javascript" src="vendor/plugins/mask/jquery.mask.js"></script>
			<script type="text/javascript" src="assets/js/custom.js"></script>

			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.1/summernote.min.js"></script>
			<script type="text/javascript" src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
			<!--
			hotfix/devis-duplicate-id
			<script type="text/javascript" src="/assets/js/sync_heracles.js"></script>
			-->
			<script type="text/javascript">

				var tab_contact=new Array(5);
				tab_contact[0]=new Array(5);

		<?php	if(isset($contacts)){
					foreach($contacts as $c){ ?>
						tab_contact[<?=$c["con_id"]?>]=new Array(5);
						tab_contact[<?=$c["con_id"]?>]["titre"]="<?=$c["con_titre"]?>";
						tab_contact[<?=$c["con_id"]?>]["nom"]="<?=$c["con_nom"]?>";
						tab_contact[<?=$c["con_id"]?>]["prenom"]="<?=$c["con_prenom"]?>";
						tab_contact[<?=$c["con_id"]?>]["tel"]="<?=$c["con_tel"]?>";
						tab_contact[<?=$c["con_id"]?>]["portable"]="<?=$c["con_portable"]?>";
						tab_contact[<?=$c["con_id"]?>]["mail"]="<?=$c["con_mail"]?>";
		<?php		}
				} ?>

				var tab_adresse=new Array();
				tab_adresse[0]=new Array(7);
		<?php	if(isset($adresses)){
					foreach($adresses as $a){ ?>
						tab_adresse[<?=$a["adr_id"]?>]=new Array(7);
						tab_adresse[<?=$a["adr_id"]?>]["nom"]="<?=$a["adr_nom"]?>";
						tab_adresse[<?=$a["adr_id"]?>]["service"]="<?=$a["adr_service"]?>";
						tab_adresse[<?=$a["adr_id"]?>]["ad1"]="<?=$a["adr_ad1"]?>";
						tab_adresse[<?=$a["adr_id"]?>]["ad2"]="<?=$a["adr_ad2"]?>";
						tab_adresse[<?=$a["adr_id"]?>]["ad3"]="<?=$a["adr_ad3"]?>";
						tab_adresse[<?=$a["adr_id"]?>]["cp"]="<?=$a["adr_cp"]?>";
						tab_adresse[<?=$a["adr_id"]?>]["ville"]="<?=$a["adr_ville"]?>";
						tab_adresse[<?=$a["adr_id"]?>]["geo"]="<?=$a["adr_geo"]?>";
		<?php		}
				} ?>

				var client_categorie=0;

				// val pour ini modal produit
				var ini_pro_fam=0;
				var ini_pro_s_fam=0;
				var ini_pro_s_s_fam=0;
				var ini_pro_id=0;

				// val modal produit suite interaction U (evite de les récupérer des les différentes fonction)
				var mdl_pro_cat=0;
				var mdl_pro_fam=0;
				var mdl_pro_s_fam=0;
				var mdl_pro_s_s_fam=0;
				var mdl_pro_type=0;
				var mdl_pro_id=0;


				var ligne_focus=0;

				var pro_categories=new Array();
				pro_categories[0]=0;
		<?php	if(!empty($d_pro_categories)){
					foreach($d_pro_categories as $pca){ ?>
						pro_categories[<?=$pca["pca_id"]?>]="<?=$pca["pca_planning"]?>";
		<?php		}
				} ?>

				var tab_tva=new Array();
				tab_tva[0]=0;
		<?php	if(!empty($base_tva)){
					foreach($base_tva as $t=>$t_val){ ?>
						tab_tva[<?=$t?>]="<?=$t_val?>";
		<?php		}
				}

				if($_SESSION["acces"]["acc_droits"][31]){ ?>
					var u_derog=2;
		<?php	}elseif($_SESSION["acces"]["acc_droits"][27]){ ?>
					var u_derog=1;
		<?php	}else{ ?>
					var u_derog=0;
		<?php	} ?>



				jQuery(document).ready(function (){

					//*****************************************
					//	INTERACTION DEVIS
					//******************************************/

					$("#agence").change(function(){
						//console.log("agence -> change");
						if($(this).val()>0){
							$("#text_agence").hide();
							$("#client").prop("disabled",false);
						}else{
							$("#text_agence").show();
							$("#client").prop("disabled",true);
						}
						$("#client").val(0).trigger("change");

					});

					// GESTION DES MODALITES DE REGLEMENTS
					$("input[name=dev_reg_formule]").click(function (){
						reg_formule=$("input[name=dev_reg_formule]:checked").val();
						actualiser_formule_reg(reg_formule);
					});

					$(".champ-reg-formule").blur(function(){
						if( ($(this).val()*1)>($(this).prop("max")*1) ){
							$(this).val($(this).prop("max"));
						}
					});

					// chagement d'adresse
					$("#adresse").change(function(){
						adresse=$(this).val();
						client=$("#client").val();
						if(adresse>0){
							$("#adr_nom").val(tab_adresse[adresse]["nom"]);
							$("#adr_service").val(tab_adresse[adresse]["service"]);
							$("#adr1").val(tab_adresse[adresse]["ad1"]);
							$("#adr2").val(tab_adresse[adresse]["ad2"]);
							$("#adr3").val(tab_adresse[adresse]["ad3"]);
							$("#adr_cp").val(tab_adresse[adresse]["cp"]);
							$("#adr_ville").val(tab_adresse[adresse]["ville"]);
							if(tab_adresse[adresse]["geo"]>0){
								$("#adr_geo" + tab_adresse[adresse]["geo"]).prop("checked",true);
							}

							$("#bloc_adr_maj").show();

							$("#adr_add").prop("checked",false);
							$("#bloc_adr_add").hide();

						}else{
							$(".champ-adresse").val("");
							$("#adr_maj").prop("checked",false);
							$("#bloc_adr_maj").hide();

							$("#bloc_adr_add").show();
						}
						if(client>0){
							// si adresse on prend contact associé sinon tous les contacts
							get_contacts(1,client,adresse,0,actualiser_contacts,"","");
						}
					});

					// chagement d'adresse
					$("#contact").change(function(){
						contact=$(this).val();
						if(contact>0){
							$("#con_titre").val(tab_contact[contact]["titre"]).trigger("change");
							$("#con_nom").val(tab_contact[contact]["nom"]);
							$("#con_prenom").val(tab_contact[contact]["prenom"]);
							$("#con_tel").val(tab_contact[contact]["tel"]);
							$("#con_portable").val(tab_contact[contact]["portable"]);
							$("#con_mail").val(tab_contact[contact]["mail"]);

							$("#bloc_con_maj").show();

							$("#con_add").prop("checked",false);
							$("#bloc_con_add").hide();

						}else{
							$(".champ-contact").val("");
							$("#con_titre").val(0).trigger("change");
							$("#con_maj").prop("checked",false);
							$("#bloc_con_maj").hide();
							$("#bloc_con_add").show();
						}
					});


					// ajout ou modif d'une ligne
					$(document).on("click",".mod-ligne",function(){
						//console.log(".mod-ligne click");
						if($("#client").val()>0){
							ini_modal_produit($(this).data("ligne"));
						}else{
							modal_alerte_user("Merci de selectionner un client avant d'ajouter une ligne de devis");
						}
					});

					// suppression d'une ligne
					$(document).on("click",".supp-ligne",function(){
						//console.log(".supp-ligne click");
						modal_confirmation_user("Êtes-vous sûr de vouloir supprimer cette ligne ?",supprimer_ligne,$(this).data("ligne"));
					});

					// gestion des dérogation tarifaire
					$(document).on("click",".derog-ligne",function(){
						ini_modal_derogation($(this).data("ligne"));
					});


					$(document).on("blur",".tarif",function(){
						//console.log(".tarif blur");
						calculer_tarif($(this));
					});

					// maj du texte de la ligne num_ligne
					$(document).on("click",".btn-texte",function(){
						//console.log("click .btn-texte");
						num_ligne=$(this).data("num_ligne");
						ini_modal_texte(num_ligne);
					});

					// ENVOIE DU FORM
					$("#save_devis").click(function(){
						var alerte="";

						num_ligne=0;
						if($("#table_devis tbody tr:last").length==1){
							num_ligne=$("#table_devis tbody tr:last").data("ligne");
						};
						$("#num_ligne").val(num_ligne);

						if($("#client").val()==0 || $("#client").val()==""){
							alerte=alerte + "Vous devez selectionner un client<br/>";
						}
						//alerte=alerte + "commercial : " + $("#commercial").val();
						if($("#commercial").val()==0 || $("#commercial").val()=="" || !$("#commercial").val()){
							alerte=alerte + "Vous devez selectionner un commercial<br/>";
						}
						if($("#adr1").val()=="" && $("#adr2").val()=="" && !$("#adr3").val()){
							alerte=alerte + "Vous devez renseigner au moins une ligne d'adresse.<br/>";
						}
						if($("#adr_cp").val()==""){
							alerte=alerte + "Vous devez renseigner le code postal.<br/>";
						}
						if($("#adr_ville").val()==""){
							alerte=alerte + "Vous devez renseigner la ville.<br/>";
						}
						if($("#con_nom").val()==""){
							alerte=alerte + "Vous devez renseigner le nom du contact.<br/>";
						}
						if(num_ligne==0){
							alerte=alerte + "Votre devis ne contient aucun produit<br/>";
						}else{
							$(".ligne").each(function( index ) {
								ligne_chrono=$( this ).data("ligne");
								ligne_cat=$("#reference_" + ligne_chrono).data("categorie");
								ligne_pro=$("#produit_" + ligne_chrono).val();
								if(ligne_cat==1 && ligne_pro!=1201 && ligne_pro<1208){
									alerte=alerte + "Le code " + $("#reference_" + ligne_chrono).html() + " est archivé. Merci de selectionner un nouveau code.<br/>";
								}
							});

						}
						if(alerte!=""){
							modal_alerte_user(alerte,"");
						}else{
							// hotfix/devis-duplicate-id
							//if($("#devis").val()==0){
							//	devis_sync("<?=$acc_societe?>","#form_devis","#id_heracles");
							//}else{
								$("#form_devis").submit();
							//}
						}
					});



					//*****************************************
					//	INTERACTION MODAL LIGNE
					//******************************************/

					$("#produit_categorie").change(function(){
						$("#produit_sous_sous_famille option[value!='0'][value!='']").remove();
						mdl_pro_cat=$(this).val();
						//console.log("#produit_categorie -> change");
						if(mdl_pro_cat>0){
							if(pro_categories[mdl_pro_cat]==1){
								// produit formation
								$("#produit_famille").val(ini_pro_fam).trigger("change");
								$(".bloc-planning").hide();
								$("#bloc_type").show();
								if($("#produit_intra").is(":checked")){
									mdl_pro_type=1;
								}else{
									mdl_pro_type=2;
								}
								$("#bloc_fam").show();

							}else{
								// produit comm

								$("#produit_famille").val(0).trigger("change");
								$("#produit_intra").prop("checked",true);
								mdl_pro_type=1;
								$(".bloc-planning").hide();
								afficher_produits();
							}
						}else{
							afficher_produits();
						}
					});

					// actualisation famille / sous famille

					$("#produit_famille").change(function(){
						console.log("produit_famille -> change");
						mdl_pro_fam=$(this).val();
						get_sous_familles($(this).val(),afficher_sous_famille,ini_pro_s_fam);

					});

					$("#produit_sous_famille").change(function(){
						console.log("produit_sous_famille -> change");
						mdl_pro_s_fam=$(this).val();
						get_sous_sous_familles($(this).val(),afficher_sous_sous_famille,ini_pro_s_s_fam);
					});

					$("#produit_sous_sous_famille").change(function(){
						mdl_pro_s_s_fam=$(this).val();
						console.log("produit_sous_sous_famille -> change");
						afficher_produits();
					});

					// changement de type
					$(".produit-type").click(function(){
						if($("#produit_intra").is(":checked")){
							mdl_pro_type=1;
						}else{
							mdl_pro_type=2;
						}
						afficher_produits();
					});

					// envoie du modal ajout ou edit produit
					$("#maj_ligne").click(function(){
						if($(this).data("ligne")==0){
							get_produit($("#client").val(),$("#produit").val(),0,"",ajouter_ligne,mdl_pro_type,"");
						}else{
							get_produit($("#client").val(),$("#produit").val(),0,"",modifier_ligne,$(this).data("ligne"),mdl_pro_type);
						}
						$("#modal_produit").modal("hide");
					});


					//*****************************************
					//	INTERACTION MODAL TEXTE
					//******************************************/

					// applique la maj des textes à la ligne concerne
					$("#sub_texte").click(function (){
						//console.log("#sub_texte click");
						if(ligne_focus!=null){
							texte=$("#text_texte").val();
							$("#cont_texte_" + ligne_focus).html(texte);
							$("#texte_" + ligne_focus).val(texte);

							texte_date=$("#text_texte_date").val();
							$("#cont_texte_date_" + ligne_focus).html(texte_date);
							$("#texte_date_" + ligne_focus).val(texte_date);

							texte_sta=$("#text_texte_sta").val();
							$("#cont_texte_sta_" + ligne_focus).html(texte_sta);
							$("#texte_sta_" + ligne_focus).val(texte_sta);
						}
						$("#modal_texte").modal("hide");
						ligne_focus=null;
					});


					//*****************************************
					//	INTERACTION MODAL DEROGATION
					//******************************************/

					//changement du niveau de derogation
					$('input[name=derogation]').change(function(){
						derogation=$('input[name=derogation]:checked').val();
						if(derogation==0){
							$("#rem_famille").prop("checked",true);
							$("#rem_famille").prop("disabled",true);
							$("#rem_ca").prop("checked",true);
							$("#rem_ca").prop("disabled",true);
						}else{
							$("#rem_famille").prop("disabled",false);
							$("#rem_ca").prop("disabled",false);
						}
					});

					// envoie du modal
					$("#sub_derogation").click(function(){
						console.log("#sub_derogation -> click");
						num_ligne=$(this).data("ligne");

						$("#derogation_" + num_ligne).val($('input[name=derogation]:checked').val());

						if($("#rem_famille").is(":checked")){
							$("#rem_famille_" + num_ligne).val(1);
						}else{
							$("#rem_famille_" + num_ligne).val(0);
						}
						if($("#rem_ca").is(":checked")){
							$("#rem_ca_" + num_ligne).val(1);
						}else{
							$("#rem_ca_" + num_ligne).val(0);
						}

						// recup le min selon la derogation

						produit=$("#produit_" + num_ligne).val();

						get_produit($("#client").val(),produit,1,"",appliquer_derogation,num_ligne,"");

						$("#modal_derogation").modal("hide");

					});

				});

				//*****************************************
				//	FONCTION DEVIS
				//	-> declenché par interaction devis
				//******************************************/

				// retour de la requete get_client
				function actualiser_get_client(json,param1,param2){
					if(json){
						if(json.erreur_txt!=""){
							afficher_message("Erreur","danger",json.erreur_txt);
						}else{

							client_categorie=json.categorie;

							// commercial
							$("#commercial").val(json.commercial).trigger("change");

							// actu des modalite de reglement
							$("#reg_formule_" + json.reg_formule).prop("checked",true);
							actualiser_formule_reg(json.reg_formule);
							if($("#reg_nb_jour_" + json.reg_formule).length>0){
								$("#reg_nb_jour_" + json.reg_formule).val(json.reg_nb_jour);
							}
							if($("#reg_fdm_" + json.reg_formule).length>0){
								$("#reg_fdm_" + json.reg_formule).val(json.reg_fdm);
							}
							if($("#reg_type").length>0){
								$("#reg_type").select2("val",json.reg_type);
							}

							// on memorie les adresses
							if(json["adr_int"]!=""){
								$.each(json["adr_int"], function(i, ad){
									tab_adresse[ad.id]=new Array(8);
									tab_adresse[ad.id]["nom"]=ad.nom;
									tab_adresse[ad.id]["service"]=ad.service;
									tab_adresse[ad.id]["ad1"]=ad.ad1;
									tab_adresse[ad.id]["ad2"]=ad.ad2;
									tab_adresse[ad.id]["ad3"]=ad.ad3;
									tab_adresse[ad.id]["cp"]=ad.cp;
									tab_adresse[ad.id]["ville"]=ad.ville;
									tab_adresse[ad.id]["geo"]=ad.geo;
								});
							}

							// on selectionne l'adresse par defaut
							actualiser_select2(json.adr_int,"#adresse",json.int_defaut);

							if(json.categorie==3){
								$("#adresse").prop("disabled",true);
							}else{
								$("#adresse").prop("disabled",false);
							}
						}
					}else{

						// PAS DE CLIENT

						client_categorie=0;

						// reinitialisation adresse
						$("#adresse").prop("disabled",false);
						actualiser_select2("","#adresse",0);
						// actu select ne genere pas un change si val def = 0
						$("#adresse").val(0).trigger("change");

						// reinitialisation contact
						$("#contact").prop("disabled",false);
						actualiser_select2("","#contact",0);
						$("#contact").val(0).trigger("change");


					}

				}

				function actualiser_formule_reg(formule){
					//console.log("actualiser_formule_reg(" + formule + ")");
					$('.champ-reg-formule').each(function(){
						if($(this).hasClass("reg-formule-" + formule)){
							$(this).prop("disabled",false);
						}else{
							$(this).val(0);
							$(this).prop("disabled",true);
						}
					});
				}

				// traitement suite à la recuperation des contacts liés à l'adresse
				function actualiser_contacts(json){
					if(json){
						// on memorie les conatcts
						var con_defaut=0;
						tab_contact=new Array();
						$.each(json, function(i, con){
							tab_contact[con.id]=new Array();
							tab_contact[con.id]["nom"]=con.nom;
							tab_contact[con.id]["prenom"]=con.prenom;
							tab_contact[con.id]["tel"]=con.tel;
							tab_contact[con.id]["portable"]=con.portable;
							tab_contact[con.id]["mail"]=con.mail;
							tab_contact[con.id]["titre"]=con.titre;
							if(con.defaut==1){
								 con_defaut=con.id;
							}
						});
						// actualisation du select contact
						actualiser_select2(json,"#contact",con_defaut);
						if(con_defaut==0){
							// securite si 0 pas #contact.change
							$("#contact").val(0).trigger("change");
						}
					}else{
						actualiser_select2("","#contact",0);
						$("#contact").val(0).trigger("change");
						$("#contact").prop("disabled",true);
					}
					if(client_categorie==3){
						$("#contact").prop("disabled",true);
					}else{
						$("#contact").prop("disabled",false);
					}
				}



				function supprimer_ligne(num_ligne){
					//console.log("supprimer_ligne(" + num_ligne + ")");
					$("#produit_" + num_ligne).val(0);
					$("#montant_" + num_ligne).val(0);
					$("#devis_ligne_" + num_ligne).remove();
					calculer_total();
				}

				function calculer_tarif(elt){

					if(elt.hasClass("tarif-ht")){
						// calcul en partant du pu ht
						id=elt.prop("id");
						num_ligne=id.replace("ht_","");
						ht=elt.val();
						tg=$("#tg_" + num_ligne).val();
						qte=$("#qte_" + num_ligne).val();

						remise=tg-ht;
						remise=parseFloat(Math.round(remise * 100) / 100).toFixed(2);
						if(tg!=0){
							remise_taux=(remise*100)/tg;
						}

						montant=ht*qte;
						montant=parseFloat(Math.round(montant * 100) / 100).toFixed(2);

					}else if(elt.hasClass("tarif-qte")){

						id=elt.prop("id");
						num_ligne=id.replace("qte_","");
						qte=elt.val();
						ht=$("#ht_" + num_ligne).val();

						montant=ht*qte;
						montant=parseFloat(Math.round(montant * 100) / 100).toFixed(2);

						remise=$("#remise_" + num_ligne).val();
						remise_taux=$("#remise_taux_" + num_ligne).val();

					}else if(elt.hasClass("tarif-remise")){
						id=elt.prop("id");
						num_ligne=id.replace("remise_","");
						remise=elt.val();
						tg=$("#tg_" + num_ligne).val();
						qte=$("#qte_" + num_ligne).val();

						ht=tg-remise;
						ht=parseFloat(Math.round(ht * 100) / 100).toFixed(2);

						if(tg!=0){
							remise_taux=(remise*100)/tg;
						}
						montant=ht*qte;
						montant=parseFloat(Math.round(montant * 100) / 100).toFixed(2);

					}else if(elt.hasClass("tarif-remise-taux")){
						// calcul en partant du taux de remise
						id=elt.prop("id");
						num_ligne=id.replace("remise_taux_","");
						tg=$("#tg_" + num_ligne).val();
						qte=$("#qte_" + num_ligne).val();

						remise_taux=elt.val();
						remise=(tg*remise_taux)/100;
						remise=parseFloat(Math.round(remise * 100) / 100).toFixed(2);
						if(tg!=0){
							remise_taux=(remise*100)/tg;
						}
						ht=tg-remise;
						remise=parseFloat(Math.round(remise * 100) / 100).toFixed(2);

						montant=ht*qte;
						montant=parseFloat(Math.round(montant * 100) / 100).toFixed(2);


					}else if(elt.hasClass("tarif-montant")){
						// calcul en partant du montant total
						id=elt.prop("id");
						num_ligne=id.replace("montant_","");
						tg=$("#tg_" + num_ligne).val();
						qte=$("#qte_" + num_ligne).val();
						montant=elt.val();

						if(qte!=0){
							ht=montant/qte;
						}
						ht=parseFloat(Math.round(ht * 100) / 100).toFixed(2);

						remise=tg-ht;
						remise=parseFloat(Math.round(remise * 100) / 100).toFixed(2);

						if(tg!=0){
							remise_taux=(remise*100)/tg;
						}
					}
					min_ht=0;
					if($("#ht_min_" + num_ligne).val()!=""){
						min_ht=$("#ht_min_" + num_ligne).val();
						min_ht=parseFloat(Math.round(min_ht * 100) / 100).toFixed(2);
					};

					console.log("min_ht : " + min_ht);
					console.log("ht : " + ht);

					if(1*min_ht>1*ht){
						$("#bloc_alert_ht_" + num_ligne).html("HT min à " + min_ht);
						$("#bloc_alert_ht_" + num_ligne).show().fadeOut(5000);
						//$("#ht_" + num_ligne).val(min_ht).trigger("blur");
					}

					$("#ht_" + num_ligne).val(ht);
					$("#qte_" + num_ligne).val(qte);
					$("#remise_" + num_ligne).val(remise);
					$("#remise_taux_" + num_ligne).val(remise_taux);
					$("#montant_" + num_ligne).val(montant);
					calculer_total();


				}

				function calculer_total(){
					total=0;
					$(".tarif-montant").each(function(){
						//console.log($(this).val());
						total=total + parseFloat($(this).val());
					});
					total=parseFloat(Math.round(total * 100) / 100).toFixed(2);
					$("#montant_total").val(total);
				}

				//initialisation du modal pour maj du texte d'un ligne
				function ini_modal_texte(num_ligne){

					// textarea designation
					texte=$("#texte_" + num_ligne).val();
					$("#cont_texte").append("");
					html_texte="<textarea id='text_texte' >" + texte + "</textarea>";
					$("#cont_texte").html(html_texte);
					$('#text_texte').summernote({
						lang: 'fr-FR',
						height:250,
						toolbar: [
							// [groupName, [list of button]]
							['style', ['bold', 'italic', 'underline', 'clear']],
							['font', ['strikethrough', 'superscript', 'subscript']],
							['fontsize', ['fontsize']],
							['color', ['color']],
							['para', ['ul', 'ol', 'paragraph']],
							['height', ['height']],
							['picture', ['picture']]
						],
						callbacks: {
							onPaste: function (e) {
								var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

								e.preventDefault();

								// Firefox fix
								setTimeout(function () {
									document.execCommand('insertText', false, bufferText);
								}, 10);
							}
						}
					});
					// textarea date
					texte_date=$("#texte_date_" + num_ligne).val();
					$("#cont_texte_date").append("");
					html_texte_date="<textarea id='text_texte_date' >" + texte_date + "</textarea>";
					$("#cont_texte_date").html(html_texte_date);
					$('#text_texte_date').summernote({
						lang: 'fr-FR',
						height:250,
						toolbar: [
							// [groupName, [list of button]]
							['style', ['bold', 'italic', 'underline', 'clear']],
							['font', ['strikethrough', 'superscript', 'subscript']],
							['fontsize', ['fontsize']],
							['color', ['color']],
							['para', ['ul', 'ol', 'paragraph']],
							['height', ['height']],
							['picture', ['picture']]
						],
						callbacks: {
							onPaste: function (e) {
								var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

								e.preventDefault();

								// Firefox fix
								setTimeout(function () {
									document.execCommand('insertText', false, bufferText);
								}, 10);
							}
						}
					});

					// textarea designation
					texte_sta=$("#texte_sta_" + num_ligne).val();
					$("#cont_texte_sta").append("");
					html_texte_sta="<textarea id='text_texte_sta' >" + texte_sta + "</textarea>";
					$("#cont_texte_sta").html(html_texte_sta);
					$('#text_texte_sta').summernote({
						lang: 'fr-FR',
						height:250,
						toolbar: [
							// [groupName, [list of button]]
							['style', ['bold', 'italic', 'underline', 'clear']],
							['font', ['strikethrough', 'superscript', 'subscript']],
							['fontsize', ['fontsize']],
							['color', ['color']],
							['para', ['ul', 'ol', 'paragraph']],
							['height', ['height']],
							['picture', ['picture']]
						],
						callbacks: {
							onPaste: function (e) {
								var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
								e.preventDefault();
								document.execCommand('insertText', false, bufferText);
							}
						}
					});
					ligne_focus=num_ligne;
					$("#modal_texte").modal("show");
				}

				//*****************************************
				//	FONCTION MODAL PRODUIT
				//	-> declenché par interaction modal ligne
				//******************************************/
				function ini_modal_produit(num_ligne){

					//console.log("ini_modal_produit(" + num_ligne + ")");

					if(num_ligne==0){

						// NOUVELLE LIGNE

						ini_pro_fam=0;
						ini_pro_s_fam=0;
						ini_pro_s_s_famille=0;
						ini_pro_id=0;

						$("#modal_produit .modal-title").html("Nouveau produit");
						$("#produit_categorie").select2("val",0);
						$("#maj_ligne").data("ligne",0);
						$(".bloc-planning").hide();
						$("#bloc_produit").hide();

						$("#produit_categorie").prop("disabled",false);
						$("#produit_famille").prop("disabled",false);
						$("#produit_sous_famille").prop("disabled",false);
						$("#produit_sous_sous_famille").prop("disabled",false);

					}else{


						ini_pro_fam=$("#reference_" + num_ligne).data("famille");
						ini_pro_s_fam=$("#reference_" + num_ligne).data("sous_famille");
						ini_pro_s_s_famille=$("#reference_" + num_ligne).data("sous_sous_famille");
						ini_pro_id=$("#produit_" + num_ligne).val();

						$("#maj_ligne").data("ligne",num_ligne);

						if($("#type_" + num_ligne).val()==1){
							$("#produit_intra").prop("checked",true);
							mdl_pro_type=1;
						}else{
							$("#produit_inter").prop("checked",true);
							mdl_pro_type=2;
						}

						if($("#action_" + num_ligne).val()>0){
							$("#produit_categorie").prop("disabled",true);
							$("#produit_famille").prop("disabled",true);
							$("#produit_sous_famille").prop("disabled",true);
							$("#produit_sous_sous_famille").prop("disabled",true);
						}else{
							$("#produit_categorie").prop("disabled",false);
							$("#produit_famille").prop("disabled",false);
							$("#produit_sous_famille").prop("disabled",false);
							$("#produit_sous_sous_famille").prop("disabled",false);
						};

						$("#produit_categorie").val($("#reference_" + num_ligne).data("categorie")).trigger("change");

					}
					$("#modal_produit").modal("show");
				}

				function afficher_sous_famille(json,selected){
					//console.log("afficher_sous_famille");
					$("#produit_sous_famille option[value!='0'][value!='']").remove();
					if(json!=""){
						// il y a des sous-famille U doit en choisir une
						$("#produit_sous_famille option[value!='0'][value!='']").remove();
						$("#produit_sous_famille").select2({
							data: json,
							closeOnSelect: true
						});
						if(selected!=""){
							$("#produit_sous_famille").val(selected).trigger("change");
						}
						$("#bloc_s_fam").show();
					}else{
						$("#bloc_s_fam").hide();
						afficher_produits();
					}

				}

				function afficher_sous_sous_famille(json,selected){
					//console.log("afficher_sous_sous_famille");
					$("#produit_sous_sous_famille option[value!='0'][value!='']").remove();
					if(json!=""){
						// il y a des sous sous-famille U doit en choisir une
						$("#produit_sous_sous_famille option[value!='0'][value!='']").remove();
						$("#produit_sous_sous_famille").select2({
							data: json,
							closeOnSelect: true
						});
						if(selected!=""){
							$("#produit_sous_sous_famille").val(selected).trigger("change");
						}
						$("#bloc_s_s_fam").show();
					}else{
						$("#bloc_s_s_fam").hide();
						afficher_produits();
					}

				}

				// verifie si on a assez d'info pour déclencher la recherche des codes produits
				function afficher_produits(){
					console.log("afficher_produits");

					affichable=false;

					client=$("#client").select2("val");

					if(client>0 &&mdl_pro_cat>0){
						if(pro_categories[mdl_pro_cat]==1){
							if($("#produit_sous_sous_famille option").length>1){
								if(mdl_pro_s_s_fam>0){
									affichable=true;
								}
							}else if($("#produit_sous_famille option").length>1){
								if(mdl_pro_s_fam>0){
									affichable=true;
								}
							}else if(mdl_pro_fam>0){
								affichable=true;
							}
						}else{

							affichable=true;
							mdl_pro_fam=0;
							mdl_pro_s_fam=0;
							mdl_pro_s_s_fam=0;
						}
					}

					if(affichable){
						// il n'y a pas de sous sous_famille -> le produit est selectionnable
						if(mdl_pro_type==1){
							intra=true;
							inter=false;
						}else{
							intra=false;
							inter=true;
						}
						get_produits(client,mdl_pro_cat,mdl_pro_fam,mdl_pro_s_fam,mdl_pro_s_s_fam,intra,inter,actualiser_produit,ini_pro_id,"");
					}else{
						actualiser_produit("");
					}
				}

				// affiche les produits dans un select2
				function actualiser_produit(json,selected){
					//console.log("actualiser_produit()");

					$("#produit option[value!='0'][value!='']").remove();
					if(json){
						$("#produit").select2({
							data: json,
							closeOnSelect: true
						});
						if(selected!=""){
							$("#produit").val(selected).trigger("change");
						}
						if(pro_categories[mdl_pro_cat]==1){
							/* get_devis_action(client,mdl_pro_fam,mdl_pro_s_fam,mdl_pro_s_s_fam,mdl_pro_type,afficher_actions,"",""); */
						}
						$("#bloc_produit").show();
					}else{
						$("#bloc_produit").hide();
					}
				}

				function afficher_actions(json,param1,param2){
					console.log("afficher_actions");

				}

				function ajouter_ligne(json,produit_type){

					console.log("ajouter_ligne");
					console.log("produit_type : " + produit_type);

					ca_verrou=false;

					if(produit_type==1){

						produit_tg=json.tg_intra;
						produit_ht=json.ht_intra;
						min_ht=json.cpr_min_intra;
						if(json.ca_verrou_intra){
							ca_verrou=true;
						};
					}else{

						produit_tg=json.tg_inter;
						produit_ht=json.ht_inter;
						min_ht=json.cpr_min_inter;
						if(json.ca_verrou_inter){
							ca_verrou=true;
						};
					}

					remise=produit_tg-produit_ht;
					remise_taux=0;
					if(produit_tg!=0){
						remise_taux=(remise*100)/produit_tg;
					}

					console.log("produit_tg : " + produit_tg);
					console.log("produit_ht : " + produit_ht);


					num_ligne=1;
					if($("#table_devis tbody tr:last").length==1){
						num_ligne=$("#table_devis tbody tr:last").data("ligne");
						num_ligne=num_ligne+1;
					};

					data_ligne="data-categorie='" + json.categorie + "' data-famille='" + json.famille + "'";
					data_ligne=data_ligne + " data-sous_famille='" + json.sous_famille + "' data-sous_sous_famille='" + json.sous_sous_famille + "'";

					// derogation sur le nouveau code
					att_btn_derog=""
					if(json.derogation>u_derog){
						att_btn_derog="disabled"
					}

					code="<tr id='devis_ligne_" + num_ligne + "' data-ligne='" + num_ligne + "' class='ligne' >";
						code+="<td>";
							code+="<button type='button' class='btn btn-xs btn-danger supp-ligne mr5' data-ligne='" + num_ligne + "' >";
								code+="<i class='fa fa-times' ></i>";
							code+="</button>";

							code+="<button type='button' class='btn btn-xs btn-warning mod-ligne mr5' data-ligne='" + num_ligne + "' >";
								code+="<i class='fa fa-pencil' ></i>";
							code+="</button>";
					<?php	if($_SESSION["acces"]["acc_droits"][27] OR $_SESSION["acces"]["acc_droits"][31]){ ?>
								code+="<button type='button' class='btn btn-xs btn-info derog-ligne' id='btn_derog_" + num_ligne + "' data-ligne='" + num_ligne + "' data-toggle='tooltip' title='Dérogation tarifaire' " + att_btn_derog + " >";
									code+="<i class='fa fa-euro' ></i>";
								code+="</button>";
					<?php	} ?>

						code+="</td>";
						code+="<td>";
							code+="<span id='reference_" + num_ligne + "' " + data_ligne + " >" + json.reference + "</span>";
							code+="<input type='hidden' id='produit_" + num_ligne + "' name='dli_produit_" + num_ligne + "' value='" + json.id + "' />";
							code+="<input type='hidden' id='type_" + num_ligne + "' name='dli_type_" + num_ligne + "' value='" + produit_type + "' />";

							code+="<input type='hidden' id='derogation_" + num_ligne + "' name='dli_derogation_" + num_ligne + "' value='" + json.derogation + "' />";
							code+="<input type='hidden' id='rem_famille_" + num_ligne + "' name='dli_rem_famille_" + num_ligne + "' value='" + json.rem_famille + "' />";
							code+="<input type='hidden' id='rem_ca_" + num_ligne + "' name='dli_rem_ca_" + num_ligne + "' value='" + json.rem_ca + "' />";
							code+="<input type='hidden' id='ht_min_" + num_ligne + "' name='dli_ht_min_" + num_ligne + "' value='" + min_ht + "' />";

						code+="</td>";
						code+="<td>";

							code+='<input type="text" class="gui-input" id="libelle_' + num_ligne + '" name="dli_libelle_' + num_ligne + '" value="' + json.libelle + '" size="50" /><br/>';

							code+="<div class='pull-right' >";
								code+="<button type='button' class='btn btn-xs btn-warning btn-texte' data-num_ligne='" + num_ligne + "' data-toggle='tooltip' title='Modifier la désignation' >";
									code+="<i class='fa fa-pencil' ></i>";
								code+="</button>";
							code+="</div>";

							code+="<div id='cont_texte_" + num_ligne + "' >";
								code+=json.devis_txt;
							code+="</div>";
							code+="<div id='cont_texte_date_" + num_ligne + "' ></div>";
							code+="<div id='cont_texte_sta_" + num_ligne + "' ></div>";
							code+="<input type='hidden' id='texte_" + num_ligne + "' name='dli_texte_" + num_ligne + "' value='"  + json.devis_txt.replace(/'/g, "`") + "' />";
							code+='<input type="hidden" id="texte_date_' + num_ligne + '" name="dli_dates_' + num_ligne + '" value="" />';
							code+='<input type="hidden" id="texte_sta_' + num_ligne + '" name="dli_stagiaires_txt_' + num_ligne + '" value="" />';
						code+="</td>";
						code+="<td>";
							code+="<input type='hidden' id='min_" + num_ligne + "'  value='" + min_ht + "' />";
							code+="<input type='text' class='gui-input text-right' id='tg_" + num_ligne + "' name='dli_tg_" + num_ligne + "' value='" + produit_tg + "' size='8' readonly />";
						code+="</td>";
						code+="<td>";
							if(ca_verrou){
								code+="<input type='text'  class='gui-input tarif tarif-ht text-right' id='ht_" + num_ligne + "' name='dli_ht_" + num_ligne + "' value='" + produit_ht + "' size='8' readonly />";
							}else{
								code+="<input type='text' class='gui-input tarif tarif-ht text-right' id='ht_" + num_ligne + "' name='dli_ht_" + num_ligne + "' value='" + produit_ht + "' size='8' />";
							}
							code+="<div id='bloc_alert_ht_" + num_ligne + "' class='text-danger' ></div>";
						code+="</td>";
						code+="<td>";
							code+="<input type='text' class='gui-input tarif tarif-qte text-right' id='qte_" + num_ligne + "' name='dli_qte_" + num_ligne + "' value='1' size='5' />";
						code+="</td>";
						if(ca_verrou){
							code+="<td>";
								code+="<input type='text' class='gui-input tarif tarif-remise text-right' id='remise_" + num_ligne + "' name='dli_remise_" + num_ligne + "' value='" + remise + "' size='5' readonly />";
							code+="</td>";
							code+="<td>";
								code+="<input type='text' class='gui-input tarif tarif-remise-taux text-right' id='remise_taux_" + num_ligne + "' name='dli_taux_remise_" + num_ligne + "' value='" + remise_taux + "' size='5' readonly />";
							code+="</td>";
							code+="<td>";
								code+="<input type='text' class='gui-input tarif tarif-montant' id='montant_" + num_ligne + "' name='dli_montant_ht_" + num_ligne + "' value='" + produit_ht + "' size='8' readonly />";
							code+="</td>";
						}else{
							code+="<td>";
								code+="<input type='text' class='gui-input tarif tarif-remise text-right' id='remise_" + num_ligne + "' name='dli_remise_" + num_ligne + "' value='" + remise + "' size='5' />";
							code+="</td>";
							code+="<td>";
								code+="<input type='text' class='gui-input tarif tarif-remise-taux text-right' id='remise_taux_" + num_ligne + "' name='dli_taux_remise_" + num_ligne + "' value='" + remise_taux + "' size='5' />";
							code+="</td>";
							code+="<td>";
								code+="<input type='text' class='gui-input tarif tarif-montant text-right' id='montant_" + num_ligne + "' name='dli_montant_ht_" + num_ligne + "' value='" + produit_ht + "' size='8' />";
							code+="</td>";
						}


				<?php	if($_SESSION['acces']["acc_droits"][9]){ ?>
							code+="<td>";
								code+="<label class='field select' >";
									code+="<select id='tva_" + num_ligne + "' name='dli_tva_id_" + num_ligne + "' >";
										for(bcl=0;bcl<tab_tva.length-1;bcl++){
											if(bcl==json.tva){
												code+="<option value='" + bcl + "' selected >" + tab_tva[bcl] + "</option>";
											}else{
												code+="<option value='" + bcl + "' >" + tab_tva[bcl] + "</option>";
											}
										}
									code+="</select>";
									code+="<i class='arrow' ></i>";
								code+="</label>";
							code+="</td>";
				<?php	} ?>

					code+="</tr>";

					$('#table_devis').append(code);


					// on ajout les interactions
					/*$(".supp-ligne").off();
					$(".supp-ligne").click(function(){
						supprimer_ligne("",supprimer_ligne,$(this).data("ligne"));
					});
					$(".mod-ligne").off();
					$(".mod-ligne").click(function(){
						ini_modal_produit($(this).data("ligne"));
					});
					$(".tarif").off();
					$(".tarif").blur(function(){
						calculer_tarif($(this));
					});*/

				}

				function modifier_ligne(json,ligne,produit_type){

					console.log("modifier_ligne");

					if($("#btn_derog_" + ligne).length>0){
						if(json.derogation<=u_derog){
							$("#btn_derog_" + ligne).prop("disabled",false);
						}else{
							$("#btn_derog_" + ligne).prop("disabled",true);
						}
					}
					$("#produit_" + ligne).val(json.id);
					$("#type_" + ligne).val(produit_type);
					$("#derogation_" + ligne).val(json.derogation);
					$("#rem_famille_" + ligne).val(json.rem_famille);
					$("#rem_ca_" + ligne).val(json.rem_ca);

					if(produit_type==1){
						produit_tg=json.tg_intra;
						produit_ht=json.ht_intra;
						produit_ht_min=json.cpr_min_intra;
					}else{
						produit_tg=json.tg_inter;
						produit_ht=json.ht_inter;
						produit_ht_min=json.cpr_min_inter;
					}
					$("#ht_min_" + ligne).val(produit_ht_min);
					$("#tg_" + ligne).val(produit_tg);
					$("#ht_" + ligne).val(produit_ht);

					$("#reference_" + ligne).html(json.reference);
					$("#reference_" + ligne).data("categorie",json.categorie);
					$("#reference_" + ligne).data("famille",json.famille);
					$("#reference_" + ligne).data("sous_famille",json.sous_famille);
					$("#reference_" + ligne).data("sous_sous_famille",json.sous_sous_famille);

					$("#libelle_" + ligne).val(json.libelle);

					$("#cont_texte_" + ligne).html(json.devis_txt);
					$("#texte_" + ligne).val(json.devis_txt);

					if($("#tva_" + ligne).length>0){
						$("#tva_" + ligne).val(json.tva);
					}

					if(json.derogation>u_derog){
						$("#ht_" + ligne).prop("readonly",true);
						$("#remise_" + ligne).prop("readonly",true);
						$("#remise_taux_" + ligne).prop("readonly",true);
						$("#montant_" + ligne).prop("readonly",true);
					}else{
						$("#ht_" + ligne).prop("readonly",false);
						$("#remise_" + ligne).prop("readonly",false);
						$("#remise_taux_" + ligne).prop("readonly",false);
						$("#montant_" + ligne).prop("readonly",false);
					}
					calculer_tarif($("#ht_" + ligne));
				}

				//*****************************************
				//	FONCTION MODAL DEROGATION
				//******************************************/

				function ini_modal_derogation(num_ligne){

					derogation=$("#derogation_" + num_ligne).val();
					rem_ca=$("#rem_ca_" + num_ligne).val();
					rem_famille=$("#rem_famille_" + num_ligne).val();

					$("#derog_niv_" + derogation).prop("checked",true);

					if(derogation==0){
						$("#rem_famille").prop("checked",true);
						$("#rem_famille").prop("disabled",true);
						$("#rem_ca").prop("checked",true);
						$("#rem_ca").prop("disabled",true);
					}else{
						$("#rem_famille").prop("disabled",false);
						$("#rem_ca").prop("disabled",false);

						if(rem_ca==1){
							$("#rem_ca").prop("checked",true);
						}else{
							$("#rem_ca").prop("checked",false);
						}

						if(rem_famille==1){
							$("#rem_famille").prop("checked",true);
						}else{
							$("#rem_famille").prop("checked",false);
						}
					}
					$("#sub_derogation").data("ligne",num_ligne);
					$("#modal_derogation").modal("show");
				}

				function appliquer_derogation(json,num_ligne){
					console.log("appliquer_derogation");
					if(json&&num_ligne>0){

						produit_type=$("#type_" + num_ligne).val();
						derogation=$("#derogation_" + num_ligne).val();
						if(produit_type=1){
							if(derogation==2){
								ht_min=0;
							}else if(derogation==1){
								ht_min=json.min_dr_intra;
							}else{
								ht_min=json.min_intra;
							}
						}else{
							if(derogation==2){
								ht_min=0;
							}else if(derogation==1){
								ht_min=json.min_dr_inter;
							}else{
								ht_min=json.min_inter;
							}
						}
						$("#ht_min_" + num_ligne).val(ht_min);
						calculer_tarif($("#qte_" + num_ligne));
					}
				}

			</script>
		</body>
	</html>
