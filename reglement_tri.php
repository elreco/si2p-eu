<?php

// SELECTION DES FACTURES

$menu_actif = "5-4";
include "includes/controle_acces.inc.php";

include_once 'includes/connexion.php';
include_once 'includes/connexion_soc.php';

if(isset($_SESSION['cli_tri'])){ 
    unset($_SESSION['cli_tri']);
}
// securité

if($_SESSION['acces']['acc_service'][2]!=1){
	header("location : deconnect.php");
	die();
}

$sql="SELECT ban_id,ban_code FROM Banques WHERE NOT ban_archive ORDER BY ban_code;";
$req=$Conn->query($sql);
$d_banques=$req->fetchAll();

$sql="SELECT rty_id,rty_libelle FROM Reglements_Types WHERE NOT rty_archive ORDER BY rty_libelle;";
$req=$Conn->query($sql);
$d_reg_types=$req->fetchAll();



?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>SI2P - Orion</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="sb-top sb-top-sm ">
		<form method="post" action="reglement_liste.php" id="formulaire" >			
			<div id="main">
<?php			include "includes/header_def.inc.php"; ?>
				<section id="content_wrapper">
					<section id="content" class="animated">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="admin-form theme-primary">
									<div class="panel heading-border panel-primary">
										<div class="panel-body bg-light">											
											<div class="content-header">
												<h2>Recherche de <b class="text-primary title-suscli">règlement</b></h2>
											</div>												
											<div class="col-md-10 col-md-offset-1">		

												<p class="alert alert-info" >
													Le module encaissement n'intègre pas les règlements associés aux factures remises au factor. Pour ces règlements, merci de passer par le module affacturage.
												</p>
															
												<div class="row">
													<div class="col-md-6">
														<div class="section">
															<label for="reg_date_deb" >Règlement entre le</label>
															<span  class="field prepend-icon">
																<input type="text" id="reg_date_deb" name="reg_date_deb" class="gui-input datepicker" placeholder="Règlement entre le" required />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">															
															<label for="reg_date_fin" >et le</label>
															<span  class="field prepend-icon">
																<input type="text" id="reg_date_fin" name="reg_date_fin" class="gui-input datepicker" placeholder="Et le" />
																<span class="field-icon"><i
																	class="fa fa-calendar-o"></i>
																</span>
															</span>
														</div>
													</div>												
												</div>
												<div class="row">
											<?php	if(!empty($d_banques)){ ?>
													
														<div class="col-md-6">
															<div class="section">
																<label for="reg_banque" >Banque :</label>
																<span class="field select">
																	<select id="reg_banque" name="reg_banque" >
																		<option value="" >Banque</option>
												<?php					foreach($d_banques as $banque){ 
																			echo("<option value='" . $banque["ban_id"] . "' >" . $banque["ban_code"] . "</option>");
																		} ?>
																	</select>
																	<i class="arrow simple"></i>
																</span>
															</div>
														</div>
													
										<?php		}
													if(!empty($d_reg_types)){ ?>
													
														<div class="col-md-6">
															<div class="section">
																<label for="reg_type" >Type :</label>
																<span class="field select">
																	<select id="reg_type" name="reg_type" >
																		<option value="" >Type</option>
												<?php					foreach($d_reg_types as $d_type){ 
																			echo("<option value='" . $d_type["rty_id"] . "' >" . $d_type["rty_libelle"] . "</option>");
																		} ?>
																	</select>
																	<i class="arrow simple"></i>
																</span>
															</div>
														</div>
													
										<?php		}  ?>
												</div>
												
											</div>																						
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
			
			<footer id="content-footer" class="affix">
				<div class="row">
					<div class="col-xs-3 footer-left"></div>		
					<div class="col-xs-6 footer-middle text-center" >
				
					</div>					
					<div class="col-xs-3 footer-right">			
						<button type="submit" name="search" class="btn btn-primary btn-sm">
							<i class='fa fa-search'></i> Rechercher
						</button>
					</div>
				</div>
			</footer>
		</form>

<?php	include "includes/footer_script.inc.php"; ?>	
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function (){
				
			});
		</script>
	</body>
</html>
