<?php

include "includes/controle_acces.inc.php";
include_once("includes/connexion_soc.php");
include_once("includes/connexion.php");
include_once("includes/connexion_fct.php");

// CHERCHER LES INTERCOS

$sql="SELECT aci_id FROM Actions_Clients_Intercos
 WHERE aci_action_client = :aci_action_client";
$req=$ConnSoc->prepare($sql);
$req->bindParam("aci_action_client",$_GET['aci_client']);
$req->execute();
$d_actions_intercos = $req->fetchAll();

foreach($d_actions_intercos as $interco){
    // TRANSFERER VERS l'ACTION PRINCIPALE
    ///////////////// VERIFICATIONS //////////////////////

    $sql="SELECT aci_id FROM Actions_Clients_Intercos
     WHERE aci_action_client = :aci_action_client
     AND aci_action_client_interco = :aci_action_client_interco
     AND aci_client=:aci_client
     AND aci_action_client_interco_soc=:aci_action_client_interco_soc";
    $req=$ConnSoc->prepare($sql);
    $req->bindParam("aci_action_client", $_POST['aci_client']);
    $req->bindParam("aci_action_client_interco_soc", $_POST['aci_action_client_interco_soc']);
    $req->bindParam("aci_action_client_interco", $aci_action_client_interco);
    $req->bindParam("aci_client", $_POST['aci_client']);
    $req->execute();
    $d_action_interco_exists = $req->fetch();

    /////////////// FIN VERIFICATIONS ////////////////////
}

$_SESSION['message'][] = array(
    "titre" => "Succès",
    "type" => "success",
    "message" => "Votre interco est terminée"
);

header("location: action_voir.php?action=" . $_POST['action'] . "&action_client=" . $_POST['aci_action_client'] . "&societ=" . $_POST['societ']);
die();
?>
