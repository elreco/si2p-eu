<?php
    include "includes/controle_acces.inc.php";
    include "includes/connexion.php";
    include "modeles/mod_utilisateur.php";
    include "modeles/mod_diplome.php";
    include "modeles/mod_parametre.php";


    $erreur=0;
    
    $ref=2;
    
    $intervenant=0;
    if(isset($_GET["intervenant"])){
        $intervenant=$_GET["intervenant"];
    }else{
        $erreur=1;
    }
    
    $diplome=0;
    if(isset($_GET["diplome"])){
        $diplome=$_GET["diplome"];
    }else{
        $diplome=0;
    }

    $fournisseur=0;
    if(isset($_GET["fournisseur"])){
        $fournisseur=$_GET["fournisseur"];
    }else{
        $fournisseur=0;
    }
    if($erreur>0){
        
        header("Location: /index.php");
        die();
        
    }else{ 
        $sql="SELECT * FROM fournisseurs_intervenants WHERE fin_id = " . $intervenant;
        $req=$Conn->query($sql);
        $fournisseur_intervenant = $req->fetch();

        $intervenant_nom = $fournisseur_intervenant['fin_prenom'] . " " . $fournisseur_intervenant['fin_nom'];

        if($diplome == 0){
            $sql="SELECT dip_id,dip_libelle,idi_diplome FROM diplomes 
            LEFT OUTER JOIN intervenants_diplomes ON (intervenants_diplomes.idi_diplome = diplomes.dip_id AND idi_ref=2 AND idi_ref_id=" . $intervenant . ") 
            WHERE ISNULL(idi_diplome) ORDER BY dip_libelle";
            $req=$Conn->query($sql);
            $diplomes = $req->fetchAll();
        }else{
            $dip=get_diplome($diplome); 
            $info_dip=get_diplome_intervenant($ref,$intervenant,$diplome); 
            $idi_date_fin=convert_date_txt($info_dip["idi_date_fin"]);
        }
        
        
        
        ?>
        
        <!DOCTYPE html>
        <html>
        <head>
            <!-- Meta, title, CSS, favicons, etc. -->
            <meta charset="utf-8">
            <title>SI2P - Fournisseurs</title>
            <meta name="keywords" content=""/>
            <meta name="description" content="">
            <meta name="author" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <!-- Theme CSS -->
            <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
            <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css" >
            <link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
            <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
            <link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
            
            <!-- Favicon -->
            <link rel="shortcut icon" href="assets/img/favicon.png">

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
            <![endif]-->

        </head>
        <body class="sb-top sb-top-sm ">
            
            <form method="post" action="fournisseur_intervenant_diplome_enr.php" enctype="multipart/form-data" >
                <div>
                    <input type="hidden" name="ref" value="<?=$ref?>" />
                    <input type="hidden" name="intervenant" value="<?=$intervenant?>" />
                    <input type="hidden" name="diplome" value="<?=$diplome?>" />
                    <input type="hidden" name="fournisseur" value="<?=$fournisseur?>" />
                </div>
                <!-- Start: Main -->
                <div id="main">
                
                    <?php
                    include "includes/header_def.inc.php";
                    ?>


                    <!-- Start: Content-Wrapper -->
                    <section id="content_wrapper" class="">
                    
                        <section id="content" class="animated fadeIn">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="admin-form theme-primary ">                             
                                        <div class="panel heading-border panel-primary">
                                            
                                            <div class="panel-body bg-light">
                                            
                                                <div class="text-center">

                                                    <div class="content-header">
                                                        <h2><?=$intervenant_nom?></h2>
                                                        <?php if(empty($dip)){ ?>
                                                            <h2>Ajouter un nouveau <b class="text-primary">diplôme</b></h2>
                                                        <?php }else{ ?>
                                                            <h2>Modifier le diplôme <b class="text-primary"><?=$dip["dip_libelle"]?></b></h2>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <?php if(empty($dip)){ ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="section mb15">
                                                            <select id="uti_diplome" class="select2" name="uti_diplome" required>
                                                                <option value="">Sélectionner un diplôme...</option>                    
                                                        <?php   foreach($diplomes as $d){ ?>
                                                                    <option value="<?= $d['dip_id'] ?>"><?= $d['dip_libelle'] ?></option>
                                                        <?php   } ?>                                                        
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php }else{ ?>
                                                    <input type="hidden" name="uti_diplome" value="<?= $dip['dip_id'] ?>">
                                                <?php } ?>
                                                <div class="row">                      
                                                    <div class="col-md-12 mb40">
                                                        <div class="section">
                                                            <div for="idi_date_fin" class="field prepend-icon">
                                                                <input type="text" id="idi_date_fin" name="idi_date_fin" class="gui-input date" 
                                                                <?php if(!empty($idi_date_fin)){ ?>
                                                                    value="<?=$idi_date_fin?>" 
                                                                <?php } ?>
                                                                placeholder="Date de fin de validité du diplôme" required >
                                                                <label class="field-icon"><i class="fa fa-calendar-o"></i></label>
                                                            </div>
                                                        </div>
                                                        <label>Ne pas renseigner si le diplôme n'expire pas</label>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">   
                                                    <div class="col-md-12"> 
                                                        <div class="section">
                                                            <div class="field prepend-icon file">
                                                                <span class="button btn-primary">Choisir</span>
                                                                <input type="file" accept="application/pdf" class="gui-file" name="idi_fichier" id="idi_fichier" onchange="document.getElementById('uploader_fichier').value = this.value;">
                                                                <input type="text" class="gui-input" id="uploader_fichier" placeholder="Diplôme">
                                                                <label class="field-icon">
                                                                    <i class="fa fa-upload"></i>
                                                                </label>
                                                                <span class="input-footer">fichier .pdf uniquement</span>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>                                                  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                  
                        </section>
                    </section>
                </div>
                <footer id="content-footer" class="affix">
                    <div class="row">
                        <div class="col-xs-3 footer-left">
                            <a href="fournisseur_voir.php?fournisseur=<?= $_GET['fournisseur'] ?>&intervenant=<?= $intervenant ?>&tab=5" class="btn btn-default btn-sm">
                                <i class="fa fa-long-arrow-left"></i>
                                Retour
                            </a>
                        </div>
                        <div class="col-xs-6 footer-middle"></div>
                        <div class="col-xs-3 footer-right">
                            <button class="btn btn-success btn-sm" >
                                 <i class='fa fa-floppy-o'></i> Enregistrer
                            </button>
                            
                        </div>
                    </div>
                </footer>
            </form>
        <?php
            include "includes/footer_script.inc.php"; ?>    
            <script src="vendor/plugins/mask/jquery.mask.js"></script>
            <!-- jQuery -->
            <script src="assets/admin-tools/admin-forms/js/datepicker-fr.js"></script>
            <script src="vendor/plugins/select2/js/select2.min.js"></script>
<script src="vendor/plugins/select2/js/i18n/fr.js"></script>

            <script type="text/javascript">

                jQuery(document).ready(function () {

    $('.date').mask('00/00/0000');
                    $("#idi_date_fin").datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>',
                        nextText: '<i class="fa fa-chevron-right"></i>',
                        showButtonPanel: false,
                        beforeShow: function(input, inst) {
                            var newclass = 'admin-form';
                            var themeClass = $(this).parents('.admin-form').attr('class');
                            var smartpikr = inst.dpDiv.parent();
                            if (!smartpikr.hasClass(themeClass)) {
                                inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
                            }
                        }
                    }); 

                })      
                </script>
            </body>
        </html>
<?php   
    } ?>
