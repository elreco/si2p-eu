<?php  
include "includes/controle_acces.inc.php";
include('includes/connexion.php');
include('modeles/mod_parametre.php');
include('modeles/mod_contact.php');

// MISE A JOUR D'UNE FICHE FOURNISSEUR

$fournisseur_id = 0;
if(!empty($_GET["id"])){
    // si les get sont pas remplis
    $fournisseur_id = intval($_GET["id"]);
}
if($fournisseur_id == 0){
	echo("Impossible d'afficher la page");
	die();
}

// Chercher le fournisseur
$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_id = :fou_id");
$req->bindParam(':fou_id', $_GET['id']);
$req->execute();
$fournisseur = $req->fetch();
if(empty($fournisseur)){
	echo("Impossible d'afficher la page");
	die();
}

// groupe
$req = $Conn->prepare("SELECT * FROM fournisseurs WHERE fou_groupe = 1 AND fou_type != 2 AND fou_filiale_de = 0 AND fou_id !=" . $_GET['id'] . " ORDER BY fou_nom");
$req->execute();
$holdings = $req->fetchAll();

// TYPE D'ACHAT
$req = $Conn->prepare("SELECT ffa_id, ffa_libelle, ffa_type,ffj_famille FROM fournisseurs_familles LEFT JOIN fournisseurs_familles_jointure
ON (fournisseurs_familles.ffa_id=fournisseurs_familles_jointure.ffj_famille AND ffj_fournisseur=:fournisseur_id)
ORDER BY ffa_libelle;");
$req->bindParam(":fournisseur_id",$fournisseur_id);
$req->execute();
$fournisseurs_familles = $req->fetchAll();


// Chercher les contacts du fournisseur
$req = $Conn->prepare("SELECT * FROM fournisseurs_contacts WHERE fco_fournisseur = :fou_id AND fco_defaut=1");
$req->bindParam(':fou_id', $_GET['id']);
$req->execute();
$fournisseur_contact = $req->fetch();

// Chercher l'adresse du fournisseur
$req = $Conn->prepare("SELECT * FROM fournisseurs_adresses WHERE fad_fournisseur = :fou_id");
$req->bindParam(':fou_id', $_GET['id']);
$req->execute();
$fournisseur_adresse = $req->fetch();

// Chercher la maison mère du fournisseur
$req = $Conn->prepare("SELECT fou_id, fou_nom, fou_code FROM fournisseurs WHERE fou_id = :fou_id");
$req->bindParam(':fou_id', $fournisseur['fou_filiale_de']);
$req->execute();
$fournisseur_maison_mere = $req->fetch();

// SELECT LE CONTACT
$req = $Conn->prepare("SELECT * FROM fournisseurs_contacts WHERE fco_fournisseur = " . $_GET['id']);
$req->execute();
$fournisseur_contact_actuel = $req->fetchAll();

// SELECT L'INTERVENANT
$req = $Conn->prepare("SELECT * FROM fournisseurs_intervenants WHERE fin_fournisseur = " . $_GET['id'] . " AND fin_archive = 0");
$req->execute();
$fournisseur_intervenant_actuel = $req->fetchAll();

$masquer_categorie = 0;
// SI INTERVENANT > 1, ON NE PEUT PAS CHANGER 
if(count($fournisseur_intervenant_actuel) > 1){
	
	$masquer_categorie = 1;

}
// SI CONTACT > 1, ON NE PEUT PAS CHANGER 
if(count($fournisseur_contact_actuel) > 1){
	$masquer_categorie = 1;
}

// SI NOM ET PRENOM DIFFERENTS
if(empty($fournisseur_intervenant_actuel)){
	
	$masquer_categorie = 1;
}

if(!empty($fournisseur_intervenant_actuel) && !empty($fournisseur_contact_actuel) && ($fournisseur_contact_actuel[0]['fco_nom'] != $fournisseur_intervenant_actuel[0]['fin_nom'] OR $fournisseur_contact_actuel[0]['fco_prenom'] != $fournisseur_intervenant_actuel[0]['fin_prenom'])){

	$masquer_categorie = 1;
}

?>
<!DOCTYPE html>
<html>
<!-- Mirrored from admindesigns.com/demos/absolute/1.1/layout_horizontal-sm.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 09:52:52 GMT -->
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SI2P - Orion - Fournisseurs</title>
	<meta name="keywords" content=""/>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
	<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
<!--
<link rel="stylesheet" type="text/css" href="assets/fonts/stateface/stateface.css">
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
-->
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms-orion.css">
<link href="vendor/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/img/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="sb-top sb-top-sm ">
	<form method="post" action="fournisseur_mod_enr.php" >
		<div>
			<input type="hidden" name="fou_id" value="<?= $_GET['id'] ?>">
		</div>
		<div id="main">
<?php		include "includes/header_def.inc.php"; ?>
			
			<section id="content_wrapper" >

				<section id="content" class="animated fadeIn">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="admin-form theme-primary">
								<div class="panel heading-border panel-primary">
									<div class="panel-body bg-light">
										<div class="text-left">
											<div class="content-header">
												<h2>Modifier le fournisseur <b class='text-primary'><?= $fournisseur['fou_nom'] ?></b></h2>					
											</div>
											<div class="col-md-10 col-md-offset-1">
									<?php 		if(isset($_GET['error']) && $_GET['error'] == 1){ ?>
													<div class="col-md-12">
														<div class="alert alert-danger">
															Le Fournisseur existe déjà dans notre base de données.
														</div>
													</div>
									<?php		} ?>
												<div class="row">
													<div class="col-md-12">
														<div class="section-divider mb40">
															<span>Informations générales</span>
														</div>
													</div>
												</div>
								
												<div class="row">
													<!--<div class="col-md-4">
														<div class="section">
															<label>Code</label>
															<div class="field prepend-icon">
																<input type="text" name="fou_code" id="fou_code" class="gui-input nom" placeholder="Code" required="" 
																<?php if(!empty($_GET['id'])): ?>				
																	value="<?= $fournisseur['fou_code']; ?>"
																<?php endif; ?>
																>
																<label for="fou_code" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</label>
															</div>
														</div>
													</div>-->
													<div class="col-md-6">
														<div class="section">
															<label>Nom</label>
															<div class="field prepend-icon">
																<input type="text" name="fou_nom" id="fou_nom" class="gui-input" placeholder="Nom" required="" <?php if(!empty($_GET['id'])): ?>				
																value="<?= $fournisseur['fou_nom']; ?>"
																<?php endif; ?>>
																<label for="fou_nom" class="field-icon">
																	<i class="fa fa-bookmark"></i>
																</label>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6" id="source2">
														<div class="section">
															<label>Siren</label>
															<div class="field prepend-icon">
																<input type="text" name="fou_siren" id="fou_siren" class="gui-input siren" placeholder="Siren" required

																value="<?= $fournisseur['fou_siren'] ?>"

																>
																<label for="fou_siren" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</label>
															</div>
														</div>
													</div>
													<div class="col-md-6" id="source3">
														<div class="section">
															<label>Siret</label>
															<div class="field prepend-icon">
																<input type="text" name="fou_siret" id="fou_siret" class="gui-input siret" placeholder="Siret" required value="<?= $fournisseur['fou_siret'] ?>">
																<label for="fou_siret" class="field-icon">
																	<i class="fa fa-barcode"></i>
																</label>
															</div>
														</div>
													</div>

												</div>
											
												<div class="row">
													<div class="col-md-6" <?php if(!empty($masquer_categorie)) echo("style='display:none;'"); ?> >
														<div class="section">
															<label>Type</label>
															<span class="select">
																<select id="fou_type" name="fou_type" >
														<?php 		foreach($base_type_fournisseur as $k => $n){
																		if($k > 0){ ?>
																			<option value="<?=$k?>" <?php if($k == $fournisseur['fou_type']) echo("selected"); ?> ><?= $n ?></option>
														<?php			}
																	} ?>
																</select>
																<i class="arrow"></i>
															</span>
														</div>
													</div>
													<div class="col-md-6">
														<div class="section">
															<label>Site Internet</label>
															<div class="field prepend-icon">
																<input type="url" name="fou_site" id="fou_site" class="gui-input" placeholder="Site Internet" value="<?= $fournisseur['fou_site'] ?>">
																<label for="fou_site" class="field-icon">
																	<i class="fa fa-globe"></i>
																</label>
															</div>
														</div>
													</div>
												</div>	

												<div class="row groupe">
													<div class="col-md-6">
														<div class="section">
															
															<label class="option option-dark">
																<input type="checkbox" name="fou_groupe" id="fou_groupe" value="oui" <?php if($fournisseur['fou_groupe'] == 1): ?> checked <?php endif; ?>>
																<span class="checkbox"></span>
																<label for="fou_groupe">Groupe</label>
															</label>
														</div>
													</div>

													<div class="col-md-6 maisonm">
														<div class="section">
															<label for="fou_maison_mere" class=" mt15 option option-dark">
																<input type="radio" id="fou_maison_mere" name="fou_maison_mere" value="oui" <?php if($fournisseur['fou_filiale_de'] == 0): ?>checked<?php endif; ?>>
																<span class="radio"></span> Ce fournisseur est la maison mère
															</label>
															<label for="fou_maison_mere_non" class=" mt15 option option-dark">
																<input type="radio" id="fou_maison_mere_non" name="fou_maison_mere" value="non" <?php if($fournisseur['fou_filiale_de'] != 0): ?> checked<?php endif; ?>>
																<span class="radio"></span> Ce fournisseur est une filiale
															</label>
														</div>

													</div>
												</div>
												
												<div class="row filiale" <?php if($fournisseur['fou_filiale_de'] == 0) echo("style='display:none;'"); ?> >
													<div class="col-md-6 col-md-offset-6">												
														<h3>Choisir la maison mère :</h3>
														<select id="fou_filiale_de"  name="fou_filiale_de" class="select2">
															<option value="0" selected>Maison mère...</option>
												<?php 		if(!empty($holdings)){
																foreach($holdings as $holding){
																	if($holding["fou_id"]==$fournisseur["fou_filiale_de"]){
																		echo("<option value='" . $holding['fou_id'] . "' selected >" . $holding['fou_nom'] . "</option>");
																	}else{
																		echo("<option value='" . $holding['fou_id'] . "' >" . $holding['fou_nom'] . "</option>");
																	}
																}
															} ?>
														</select>
													</div>
												</div>

												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">																	
																<div class="section-divider mb40">
																	<span>Adresse</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<label>Nom</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fad_nom" id="fad_nom" class="gui-input" placeholder="Nom" 
																		<?php if(!empty($fournisseur_adresse)): ?>
																			value="<?= $fournisseur_adresse['fad_nom'] ?>"
																		<?php endif; ?>
																		>
																		<label for="fad_nom" class="field-icon">
																			<i class="fa fa-tag"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<label>Libellé</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fad_ad1" id="fad_ad1" class="gui-input" placeholder="Adresse"
																		<?php if(!empty($fournisseur_adresse)): ?>
																			value="<?= $fournisseur_adresse['fad_ad1'] ?>"
																		<?php endif; ?>
																		>
																		<label for="fad_ad1" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<label>Libellé (Complément 1)</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fad_ad2" class="gui-input" id="fad_ad2" placeholder="Adresse (Complément 1)"
																		<?php if(!empty($fournisseur_adresse)): ?>
																			value="<?= $fournisseur_adresse['fad_ad2'] ?>"
																		<?php endif; ?>>
																		<label for="fad_ad2" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<label>Libellé (Complément 2)</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fad_ad3" id="fad_ad3" class="gui-input" placeholder="Adresse (Complément 2)"
																		<?php if(!empty($fournisseur_adresse)): ?>
																			value="<?= $fournisseur_adresse['fad_ad3'] ?>"
																		<?php endif; ?>>
																		<label for="fad_ad3" class="field-icon">
																			<i class="fa fa-map-marker"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-3">
																<div class="section">
																	<label>Code postal</label>
																	<div class="field prepend-icon">
																		<input type="text" id="fad_cp" name="fad_cp" class="gui-input code-postal" placeholder="Code postal"  
																		<?php if(!empty($fournisseur_adresse)): ?>
																			value="<?= $fournisseur_adresse['fad_cp'] ?>"
																		<?php endif; ?>>
																		<label for="fad_cp" class="field-icon">
																			<i class="fa fa fa-certificate"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-9">
																<div class="section">
																	<label>Ville</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fad_ville" id="fad_ville" class="gui-input nom" placeholder="Ville"
																		<?php if(!empty($fournisseur_adresse)): ?>
																			value="<?= $fournisseur_adresse['fad_ville'] ?>"
																		<?php endif; ?>
																		>
																		<label for="fad_ville" class="field-icon">
																			<i class="fa fa fa-building"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Contact</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<label>Fonction</label>
																	<div class="field select">
																		<select name="fco_fonction" id="fco_fonction" onchange="nomchange(this.value)">
																			<option value="0">Sélectionner une fonction...</option>
																			<?php if (!empty($fournisseur_contact['fco_fonction'])): ?>
																				<?=get_contact_fonction_select($fournisseur_contact['fco_fonction']);?>
																			<?php else: ?>
																				<?=get_contact_fonction_select(0);?>
																			<?php endif;?>
																			<option value="autre" 
																			<?php if(!empty($fournisseur_contact['fco_fonction_nom'])): ?>
																				selected
																			<?php endif; ?>>Autre fonction...</option>
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12 fco_fonction_nom_style"<?php if(!empty($fournisseur_contact['fco_fonction_nom'])): ?> 
																	<?php else: ?> style="display:none;" <?php endif; ?>>
																<div class="section">
																	<label>Nom de la fonction</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fco_fonction_nom" id="fco_fonction_nom" class="gui-input" placeholder="Autre fonction"  
																		<?php if(!empty($fournisseur_contact['fco_fonction_nom'])): ?>
																			value="<?= $fournisseur_contact['fco_fonction_nom'] ?>"
																		<?php endif; ?>
																		>
																		<label for="fco_fonction_nom" class="field-icon">
																			<i class="fa fa-tag"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<label>Civilité</label>
																	<div class="field select">
																		<select name="fco_titre" id="fco_titre" >
																			<option value="0">Civilité...</option>
																			<?php foreach($base_civilite as $b => $c): ?>
																				<?php if($b > 0): ?>
																					<option value="<?= $b ?>"
																						<?php if(!empty($fournisseur_contact['fco_titre']) && $fournisseur_contact['fco_titre'] == $b): ?>
																							selected
																						<?php endif; ?>
																						><?= $c ?></option>
																					<?php endif; ?>
																				<?php endforeach; ?>
																		</select>
																		<i class="arrow simple"></i>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<label>Nom</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fco_nom" id="fco_nom" class="gui-input nom" placeholder="Nom" 
																		<?php if(!empty($fournisseur_contact)): ?>
																			value="<?= $fournisseur_contact['fco_nom'] ?>"
																		<?php endif; ?>>
																		<label for="fco_nom" class="field-icon">
																			<i class="fa fa-user"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label>Prénom</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fco_prenom" id="fco_prenom" class="gui-input prenom" placeholder="Prénom"
																		<?php if(!empty($fournisseur_contact)): ?>
																			value="<?= $fournisseur_contact['fco_prenom'] ?>"
																		<?php endif; ?>
																		>
																		<label for="fco_prenom" class="field-icon">
																			<i class="fa fa-tag"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<label>Téléphone</label>
																	<div class="field prepend-icon">
																		<input name="fco_tel" id="fco_tel" type="tel" class="gui-input telephone" placeholder="Numéro de téléphone"
																		<?php if(!empty($fournisseur_contact)): ?>
																			value="<?= $fournisseur_contact['fco_nom'] ?>"
																		<?php endif; ?>>
																		<label for="fou_tel" class="field-icon">
																			<i class="fa fa fa-phone"></i>
																		</label>
																	</div>
																</div>
															</div>

															<div class="col-md-6">
																<div class="section">
																	<label>Fax</label>
																	<div class="field prepend-icon">
																		<input name="fco_fax" id="fco_fax" type="tel" class="gui-input telephone" placeholder="Numéro de fax"
																		<?php if(!empty($fournisseur_contact)): ?>
																			value="<?= $fournisseur_contact['fco_fax'] ?>"
																		<?php endif; ?>>
																		<label for="fou_fax" class="field-icon">
																			<i class="fa fa-phone-square"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="section">
																	<label>Portable</label>
																	<div class="field prepend-icon">
																		<input name="fco_portable" id="fco_portable" type="tel" class="gui-input telephone" placeholder="Numéro de portable"
																		<?php if(!empty($fournisseur_contact)): ?>
																			value="<?= $fournisseur_contact['fco_portable'] ?>"
																		<?php endif; ?>>
																		<label for="fou_tel" class="field-icon">
																			<i class="fa fa fa-mobile"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<label>Email</label>
																	<div class="field prepend-icon">
																		<input type="email" name="fco_mail" id="fco_mail" class="gui-input" placeholder="Adresse Email"
																		<?php if(!empty($fournisseur_contact)): ?>
																			value="<?= $fournisseur_contact['fco_mail'] ?>"
																		<?php endif; ?>>
																		<label for="fou_mail" class="field-icon">
																			<i class="fa fa-envelope"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Facturation</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<label>Mode de règlement</label>
																	<span class="select">
																		<select id="fou_reg_type" name="fou_reg_type">
																			<option value="0">Mode de règlement...</option>
																	<?php 	foreach($base_reglement as $k => $n){
																				if($k > 0){ ?>
																					<option value="<?=$k?>" <?php if($fournisseur['fou_reg_type'] == $k) echo("selected"); ?> ><?= $n ?></option>
																	<?php 		}
																			} ?>
																		</select>
																		<i class="arrow"></i>
																	</span>
																</div>
															</div>														
															<div class="col-md-12">
																<div class="option-group field section text-left">															
																	<label style="margin-top:0!important;margin-right:0px!important;padding-right:0px;" for="reg1" class=" mt15 option option-dark">
																		<input type="radio" id="reg1" name="fou_reg_formule" value="1" <?php if($fournisseur['fou_reg_formule'] == 1) echo("checked"); ?> />																
																		<span class="radio"></span>
																	</label> 
																	<label style="margin-top:15px!important;"> 
																		Date + 
															<?php		if($fournisseur['fou_reg_formule'] == 1){ ?>
																			<input type="number" max="60" name="fou_reg_nb_jour" class="reg-formule reg-formule-1" style="width:100px;" value="<?= $fournisseur['fou_reg_nb_jour']?>" />
															<?php		}else{ ?>
																			<input type="number" max="60" name="fou_reg_nb_jour" class="reg-formule reg-formule-1" style="width:100px;" value="0" disabled />
															<?php		} ?> 
																		jours
																	</label>
																	<br>
																	
																	<label for="reg2" class=" mt15 option option-dark">
																		<input type="radio" id="reg2" name="fou_reg_formule" value="2" <?php if($fournisseur['fou_reg_formule'] == 2) echo("checked"); ?> />	
																		<span class="radio"></span> Date + 45j + fin de mois
																	</label>
																	<br>
																	<label for="reg3" class=" mt15 option option-dark">
																		<input type="radio" id="reg3" name="fou_reg_formule" value="3" <?php if($fournisseur['fou_reg_formule'] == 3) echo("checked"); ?> />
																		<span class="radio"></span> Date + Fin de mois + 45j
																	</label>
																	<br>
																	
																	<label style="margin-top:0!important;margin-right:0px!important;padding-right:0px;" for="reg4" class=" mt15 option option-dark">
																		<input type="radio" id="reg4" name="fou_reg_formule" value="4" <?php if($fournisseur['fou_reg_formule'] == 4) echo("checked"); ?> />
																		<span class="radio"></span>
																	</label> 
																	<label style="margin-top:15px!important;"> 
															<?php		if($fournisseur['fou_reg_formule'] == 4){ ?>
																			Date + 
																			<input type="number" name="fou_reg_nb_jour" class="reg-formule reg-formule-4" style="width:100px;" value="<?=$fournisseur['fou_reg_nb_jour']?>" max="30" > 
																			jours + Fin de mois +  
																			<input type="number" name="fou_reg_fdm" class="reg-formule reg-formule-4" style="width:100px;" value="<?=$fournisseur['fou_reg_fdm']?>" max="20" > jours
															<?php		}else{ ?>
																			Date + 
																			<input type="number" name="fou_reg_nb_jour" class="reg-formule reg-formule-4" style="width:100px;" value="0" max="30" disabled > 
																			jours + Fin de mois +  
																			<input type="number" name="fou_reg_fdm" class="reg-formule reg-formule-4" style="width:100px;" value="0" max="20" disabled > jours
															<?php		} ?> 		
																	</label>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section">
																	<label>identification TVA</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fou_tva" id="fou_itva" class="gui-input" placeholder="Identification TVA" value="<?= $fournisseur['fou_tva'] ?>">
																		<label for="fou_tva" class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label for="fou_montant_max">Montant maximum d'achat</label>
																<div class="section">
																	<div class="field prepend-icon">
																		<input type="number" name="fou_montant_max" id="fou_montant_max" class="gui-input" placeholder="Montant maximum d'achat" value="<?= $fournisseur['fou_montant_max']; ?>">
																		<label for="fou_montant_max" class="field-icon">
																			<i class="fa fa-euro"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="section">
																	<div class="checkbox-custom checkbox-default mb5">
																		<input type="checkbox"
																		name="fou_depassement_autorise" value="oui" id="fou_depassement_autorise" <?php if($fournisseur['fou_depassement_autorise'] == 1): ?> checked <?php endif; ?>>
																		<label for="fou_depassement_autorise">Dépassement autorisé</label>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-8">
																<div class="section">
																	<label for="fou_iban">IBAN</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fou_iban" id="fou_iban" class="gui-input iban" placeholder="IBAN" value="<?= $fournisseur['fou_iban']; ?>">
																		<label for="fou_iban" class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="section">
																	<label for="fou_iban">BIC</label>
																	<div class="field prepend-icon">
																		<input type="text" name="fou_bic" id="fou_bic" class="gui-input" placeholder="BIC" value="<?= $fournisseur['fou_bic']; ?>">
																		<label for="fou_bic" class="field-icon">
																			<i class="fa fa-barcode"></i>
																		</label>
																	</div>
																</div>
															</div>
															<?php if($_SESSION['acces']['acc_droits'][9]): ?>
																<div class="col-md-12">
																	<div class="section">
																		<label>Code SAGE</label>
																		<div class="field prepend-icon">
																			<input type="text" name="fou_sage" id="fou_sage" class="gui-input" placeholder="Code Sage" value="<?= $fournisseur['fou_sage']; ?>">
																			<label for="fou_sage" class="field-icon">
																				<i class="fa fa-barcode"></i>
																			</label>
																		</div>
																	</div>
																</div>
															<?php endif; ?>

														</div>

													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Familles</span>
																</div>
															</div>
														</div>
														<div class="row text-left">
												<?php		foreach($fournisseurs_familles as $ffa){ ?>
																<div class="col-md-6">
																	<div class="section">
																		<div class="checkbox-custom checkbox-default mb5">
																			<input type="checkbox" class="select-famille" name="ffj_famille[]" value="<?=$ffa['ffa_id']?>" id="checkboxFamille<?=$ffa['ffa_id']?>" <?php if(!empty($ffa['ffj_famille'])) echo("checked"); ?> />
																			<label for="checkboxFamille<?= $ffa['ffa_id'] ?>"><?= $ffa['ffa_libelle']; ?></label>
																		</div>
																	</div>
																</div>
												<?php 		} ?>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="section-divider mb40">
																	<span>Autre</span>
																</div>
															</div>
														</div>
														<div class="row text-left">

															<div class="col-md-3">
																<div class="section">

																	<div class="checkbox-custom checkbox-default mb5">
																		<input type="checkbox"
																		name="fou_blacklist" value="oui" id="fou_blacklist" <?php if($fournisseur['fou_blacklist'] == 1): ?> checked <?php endif; ?>>
																		<label for="fou_blacklist">Blacklisté</label>
																	</div>
																</div>
															</div>
															<div class="col-md-9">
																<div class="section">
																	<label for="fou_blacklist_txt">Raison du blacklist:</label>
																	<textarea class="gui-textarea summernote" id="fou_blacklist_txt" name="fou_blacklist_txt" placeholder="Raison du blacklist"><?php if(isset($fournisseur)): ?><?= $fournisseur['fou_blacklist_txt'] ?><?php endif; ?></textarea>
																</div>
															</div>
														</div>
											<?php		if (empty($fournisseur['fou_interco_soc'])) { ?>
															<div class="row">
																<div class="col-md-6">
																	<div class="section">
																		<div class="checkbox-custom checkbox-default mb5">
																			<input type="checkbox"
																			name="fou_archive" value="oui" id="fou_archive" <?php if($fournisseur['fou_archive'] == 1): ?> checked <?php endif; ?>>
																			<label for="fou_archive">Archivé</label>
																		</div>
																	</div>
																</div>
															</div>
											<?php		}else {
															echo("<p class='alert alert-info' >
																<b>Archivage</b><br/>
																" . $fournisseur['fou_code'] . " est un fournisseur INTERCO.<br/>
																Pour archiver cette fiche, merci de contacter le service informatique.
															");
														} ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>
		<!-- End: Main -->
		<footer id="content-footer" class="affix">
			<div class="row">
				<div class="col-xs-3 footer-left" >
					<?php if(!empty($_GET['id'])): ?>
						<a href="fournisseur_voir.php?fournisseur=<?= $_GET['id']; ?>" class="btn btn-default btn-sm">
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					<?php endif; ?>
				</div>
				<div class="col-xs-6 footer-middle" ></div>
				<div class="col-xs-3 footer-right" >
					<button type="submit" class="btn btn-success btn-sm" id="fou_submit">
						<i class='fa fa-save'></i> Enregistrer
					</button>
				</div>
			</div>
		</footer>
	</form>
<?php include "includes/footer_script.inc.php"; ?>	

<script src="vendor/plugins/mask/jquery.mask.js"></script>
<!-- plugin pour les masques formulaires -->

<script src="vendor/plugins/holder/holder.min.js"></script>
<script src="vendor/plugins/select2/js/select2.min.js"></script>
<script src="vendor/plugins/summernote/summernote.min.js"></script>
<script src="vendor/plugins/summernote/summernote-fr-FR.js"></script>
<!-- pour mettre des images de tests -->
<!-- Theme Javascript -->

<script src="assets/js/custom.js"></script>
<script>
	jQuery(document).ready(function($){
		
			// reg FORMULE
		$( "#reg1" ).click(function(){
			$(".reg-formule-1").prop("disabled", false);
			$(".reg-formule-4").val(0);
			$(".reg-formule-4").prop("disabled", true);
		});
		$( "#reg2" ).click(function(){
			$(".reg-formule-1").val(0);
			$(".reg-formule-1").prop("disabled", true);
			$(".reg-formule-4").val(0);
			$(".reg-formule-4").prop("disabled", true);
		});
		$( "#reg3").click(function(){
			$(".reg-formule-1").val(0);
			$(".reg-formule-1").prop("disabled", true);
			$(".reg-formule-4").val(0);
			$(".reg-formule-4").prop("disabled", true);
		});
		$("#reg4").click(function(){
			$(".reg-formule-1").val(0);
			$(".reg-formule-1").prop("disabled", true);
			$(".reg-formule-4").prop("disabled", false);
		});
		
		$(".reg-formule").blur(function(){
			if($(this).prop("max")>0){			
				if($(this).val()>$(this).prop("max")){
					$(this).val($(this).prop("max"));	
				}
			}
		});
	

		$( "#fou_type" ).change(function() {
			if($(this).val() == 3){
				$('.groupe').hide();
				$(".maisonm").hide();
				$(".filiale").hide();
			}else{
				$('.groupe').show();
				$('#fou_groupe').attr("checked", false);
				$(".maisonm").hide();
				$(".filiale").hide();
			}

		});


		$('#fou_groupe').click(function() {
			if($("#fou_groupe").is(':checked')){

				$(".maisonm").show();
				if($("#fou_maison_mere").is(':checked')){
					$(".filiale").hide();
				}else if($("#fou_maison_mere_non").is(':checked')){

					$(".filiale").show();
				}

			}else{
				$(".maisonm").hide();
				$(".filiale").hide();
			}
		});
		
		$('#fou_maison_mere').click(function() {
			if($("#fou_maison_mere").is(':checked')){
				$(".filiale").hide();
			}else if($("#fou_maison_mere_non").is(':checked')){

				$(".filiale").show();
			}
		});
	
		$('#fou_maison_mere_non').click(function() {
			if($("#fou_maison_mere").is(':checked')){
				$(".filiale").hide();
			}else if($("#fou_maison_mere_non").is(':checked')){

				$(".filiale").show();
			}
		});

		$('.iban').mask('SS00 0000 0000 0000 0000 00', {
			placeholder: '____ ____ ____ ____ ____ __'
		});

		$( "#fou_siret" ).focusout(function() {
			if ( $(this).val().length != 6){
				$(this).val('');
			}

		});
		
		$( "#fou_siren" ).focusout(function() {
			if ( $("#fou_siren").val().length != 11){

				$("#fou_siren").val('');
				$("#fou_siren").val("");
			}else{

				$("#fou_siren").val($("#fou_siren").val());
			}
		});

	});
	// IN DOC READY

	function nomchange(selected){
		if(selected == "autre"){
			$(".fco_fonction_nom_style").show(400);
		}else{
			$(".fco_fonction_nom_style").hide(400);
		}

	}
</script>

</body>
</html>