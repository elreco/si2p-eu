<?php

include "includes/controle_acces.inc.php";
include_once("includes/connexion_soc.php");
include_once("includes/connexion.php");
include_once("includes/connexion_fct.php");

// INSERER Actions_Clients_Intercos
// EXPLODE ACTION CLIENT INTERCO
if(!empty($_POST['aci_action_client_interco'])){
    $explode = explode("-",$_POST['aci_action_client_interco']);
    $aci_action_interco = $explode[0];
    $aci_action_client_interco = $explode[1];
}else{
    die('parametre absent');
}

if(!empty($_POST['aci_client'])){
    // ALLER CHERCHE LE CODE ET NOM DU CLIENT
    $sql="SELECT cli_code, cli_nom FROM Clients WHERE cli_id = :aci_client";
    $req=$Conn->prepare($sql);
    $req->bindParam(":aci_client",$_POST['aci_client']);
    $req->execute();
    $d_client = $req->fetch();
}else{
    die('parametre absent');
}

///////////////////////////////////////////////
//// VERIFICATIONS DATES ET SESSIONS///////////
//////////////////////////////////////////////
// !! VERIF DOUBLON !! //

// $sql="SELECT aci_id FROM Actions_Clients_Intercos
//  WHERE aci_action_client = :aci_action_client
//  AND aci_action_client_interco = :aci_action_client_interco
//  AND aci_client=:aci_client
//  AND aci_action_client_interco_soc=:aci_action_client_interco_soc";
// $req=$ConnSoc->prepare($sql);
// $req->bindParam("aci_action_client", $_POST['aci_action_client']);
// $req->bindParam("aci_action_client_interco_soc", $_POST['aci_action_client_interco_soc']);
// $req->bindParam("aci_action_client_interco", $aci_action_client_interco);
// $req->bindParam("aci_client", $_POST['aci_client']);
// $req->execute();
// $d_action_interco_exists = $req->fetch();

$sql="SELECT aci_id FROM Actions_Clients_Intercos
 WHERE aci_action_client_interco = :aci_action_client_interco
 AND aci_action_client_interco_soc=:aci_action_client_interco_soc";
$req=$ConnSoc->prepare($sql);
$req->bindParam("aci_action_client_interco_soc", $_POST['aci_action_client_interco_soc']);
$req->bindParam("aci_action_client_interco", $aci_action_client_interco);
$req->execute();
$d_action_interco_exists = $req->fetch();

if(!empty($d_action_interco_exists)){
    $_SESSION['message'][] = array(
        "titre" => "Erreur",
        "type" => "danger",
        "message" => "Cette action est déjà liée."
    );

    header("location: action_client_interco.php?action=" . $_POST['action'] . "&action_client=" . $_POST['aci_action_client']);
    die();
}
// !! FIN VERIF DOUBLON !! //
// !!VERIFICATION DES DATES!!

// SELECTIONNER LES DATES SUR L'AUTRE PLANNING POUR VOIR SI CA COLLE
$ConnFct = connexion_fct($_POST['aci_action_client_interco_soc']);
$sql="SELECT * FROM Plannings_Dates
WHERE pda_type = 1 AND pda_ref_1 = :action ORDER BY pda_date ASC, pda_demi ASC";
$req=$ConnFct->prepare($sql);
$req->bindParam(":action",$aci_action_interco);
$req->execute();
$d_plannings_dates_autre = $req->fetchAll();
// SELECTIONNER LES DATES DE CE PLANNING
foreach($d_plannings_dates_autre as $pda){
    $_continue = true;
    if($pda['pda_demi']==1){
        $sql="SELECT * FROM Plannings_Dates
        WHERE pda_demi = 1 AND pda_date = :pda_date AND pda_ref_1 = :action ORDER BY pda_date ASC, pda_demi ASC";
        $req=$ConnSoc->prepare($sql);
        $req->bindValue(":pda_date",$pda['pda_date']);
        $req->bindValue(":action",$_POST['action']);
        $req->execute();
        $d_plannings_dates_check_matin = $req->fetch();

        if(empty($d_plannings_dates_check_matin)){
            $_continue = false;
        }
    }
    if($pda['pda_demi']==2){
        $sql="SELECT * FROM Plannings_Dates
        WHERE pda_demi = 2 AND pda_date = :pda_date AND pda_ref_1 = :action ORDER BY pda_date ASC, pda_demi ASC";
        $req=$ConnSoc->prepare($sql);
        $req->bindValue(":pda_date",$pda['pda_date']);
        $req->bindValue(":action",$_POST['action']);
        $req->execute();
        $d_plannings_dates_check_am = $req->fetch();

        if(empty($d_plannings_dates_check_am)){
            $_continue = false;
        }
    }
    // SI AU MOINS UNE DATE N'EST PAS PRESENTE J'ANNULE

    if(empty($_continue)){

        $_SESSION['message'][] = array(
            "titre" => "Erreur",
            "type" => "danger",
            "message" => "Les dates ne correspondent pas à celles de l'action que vous avez sélectionnée."
        );

        header("location: action_client_interco.php?action=" . $_POST['action'] . "&action_client=" . $_POST['aci_action_client']);
        die();
    }
        echo("<hr>");
}
// !!FIN VERIFICATION DES DATES!!

// !!VERIFICATION DES SESSIONS!!
// $sql="SELECT * FROM Actions_Sessions
// WHERE ase_action = :ase_action";
// $req=$ConnSoc->prepare($sql);
// $req->bindParam(":ase_action",$_POST['action']);
// $req->execute();
// $d_session = $req->fetch();
//
// foreach($d_plannings_dates_autre as $pda){
//     // CHERCHER LES SESSIONS SUR l'AUTRE PLANNING
//     $ConnFct = connexion_fct($_POST['aci_action_client_interco_soc']);
//     $sql="SELECT * FROM Actions_Sessions
//     WHERE ase_date = :ase_date AND ase_action = :ase_action";
//     $req=$ConnFct->prepare($sql);
//     $req->bindParam(":ase_date",$pda['pda_id']);
//     $req->bindParam(":ase_action",$aci_action_interco);
//     $req->execute();
//     $d_sessions_interco = $req->fetchAll();
//
//     foreach($d_sessions_interco as $s){
//
//         $sql="SELECT * FROM Actions_Sessions
//         WHERE ase_action = :ase_action";
//         $req=$ConnSoc->prepare($sql);
//         $req->bindParam(":ase_action",$_POST['action']);
//         $req->execute();
//         $d_sessions_check = $req->fetch();
//         if(empty($d_sessions_check) && !empty($d_session)){
//             // ELLE A CREE DES SESSIONS, JE FAIS LA VERIF
//
//             $_SESSION['message'][] = array(
//                 "titre" => "Erreur",
//                 "type" => "danger",
//                 "message" => "Les sessions ne correspondent pas à celles de l'action que vous avez sélectionnée."
//             );
//
//             header("location: action_client_interco.php?action=" . $_POST['action'] . "&action_client=" . $_POST['aci_action_client']);
//             die();
//         }
//     }
// }

// if(empty($d_session)){
//     // SI ELLE N'A PAS CREE DE SESSION C'EST BON
//     // JE CREE LES SESSIONS
//     foreach($d_plannings_dates_autre as $pda){
//         foreach($d_sessions_interco as $s){
//             if(!empty($plannings_dates_check_matin[$pda['pda_id']])){
//                 $date_id = $plannings_dates_check_matin[$pda['pda_id']];
//             }else{
//                 $date_id = $plannings_dates_check_am[$pda['pda_id']];
//             }
//             $sql="INSERT INTO Actions_Sessions (ase_date, ase_h_deb, ase_h_fin, ase_action, ase_annule) VALUES (:ase_date, :ase_h_deb, :ase_h_fin, :ase_action, :ase_annule)";
//             $req=$ConnSoc->prepare($sql);
//             $req->bindParam(":ase_date",$date_id);
//             $req->bindParam(":ase_h_deb",$s['ase_h_deb']);
//             $req->bindParam(":ase_h_fin",$s['ase_h_fin']);
//             $req->bindParam(":ase_action",$_POST['action']);
//             $req->execute();
//         }
//     }
// }

// !!FIN VERIFICATION DES SESSIONS!!
///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////


// INSERER L'INTERCO
$sql="INSERT INTO actions_clients_intercos
(aci_action_client, aci_client, aci_code, aci_nom, aci_action_client_interco, aci_action_client_interco_soc, aci_action_interco)
VALUES
(:aci_action_client, :aci_client, :aci_code, :aci_nom, :aci_action_client_interco, :aci_action_client_interco_soc, :aci_action_interco)";
$req=$ConnSoc->prepare($sql);
$req->bindParam(":aci_action_client",$_POST['aci_action_client']);
$req->bindParam(":aci_client",$_POST['aci_client']);
$req->bindParam(":aci_code",$d_client['cli_code']);
$req->bindParam(":aci_nom",$d_client['cli_nom']);
$req->bindParam(":aci_action_client_interco",$aci_action_client_interco);
$req->bindParam(":aci_action_client_interco_soc",$_POST['aci_action_client_interco_soc']);
$req->bindParam(":aci_action_interco",$aci_action_interco);
$req->execute();
////

$_SESSION['message'][] = array(
    "titre" => "Succès",
    "type" => "success",
    "message" => "Votre interco est enregistrée"
);

header("location: action_voir.php?action=" . $_POST['action'] . "&action_client=" . $_POST['aci_action_client'] . "&societ=" . $_POST['societ']);
die();
?>
