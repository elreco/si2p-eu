<?php
	include "includes/controle_acces.inc.php";
	include('includes/connexion.php');
	$param_commande = 0;

	if(!empty($_GET['id'])){
	    // si les get sont pas remplis
	    $param_commande = intval($_GET['id']);

	}
	if($param_commande == 0){
	 
		echo("Impossible d'afficher la page");
		die();
	}
	// toutes les familels de fournisseurs pour le select
	$req = $Conn->prepare("SELECT * FROM fournisseurs_familles ORDER BY ffa_id");
	$req->execute();
	$fournisseurs_familles = $req->fetchAll();
	
	// la commande
	$req = $Conn->prepare("SELECT * FROM commandes_grilles WHERE cgr_id = " . $_GET['id']);
	$req->execute();
	$commande = $req->fetch();
	$max = $commande['cgr_montant_max'] + 1;
	$min = $commande['cgr_montant_min'] - 1;
	
	// la tranche juste avant
	$req = $Conn->prepare("SELECT * FROM commandes_grilles WHERE cgr_famille = " . $commande['cgr_famille'] . " AND cgr_montant_max =" . $min);
	$req->execute();

	$t1 = $req->fetch();
	if(!empty($t1)){
		$t1['cgr_montant_max'] = $t1['cgr_montant_max'] + 1;
	}
	
	// la tranche juste après
	$req = $Conn->prepare("SELECT * FROM commandes_grilles WHERE cgr_famille = " . $commande['cgr_famille'] . " AND cgr_montant_min =" . $max);
	$req->execute();
	$t2 = $req->fetch();
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>Si2P - Paramètres</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/select2.min.css" />
    	<link rel="stylesheet" type="text/css" href="vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">

		
		<link rel="shortcut icon" href="assets/skin/si2p_skin/img/favicon.png">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body class="sb-top sb-top-sm" >
		<form method="post" action="validations_bc_enr.php" >

			<?php if(isset($_GET['id'])){ ?>
				<input type="hidden" name="grille" value="<?=$_GET['id']?>" />
			<?php }?>
			<!-- Start: Main -->
			
			<div id="main">
				<?php	
					include "includes/header_def.inc.php";
				?>	
					
				<!-- Start: Content-Wrapper -->
				<section id="content_wrapper">		 
					<!-- Begin: Content -->
								
					<section id="content" class="animated fadeIn">
					
					
						<div class="admin-form theme-primary ">
						
							<div class="panel heading-border panel-primary">
								<div class="panel-body bg-light">
								
									<div class="row" >
									
										<div class="col-md-10 col-md-offset-1" >
								
											<div class="row" >
												<div class="col-md-12" >							
													<div class="text-center">								
														<div class="content-header">
											
															<h2>Mise à jour d'une <b class="text-primary">grille de validation</b> de <?= $commande['cgr_montant_min'] ?>€ à <?= $commande['cgr_montant_max'] ?>€</h2>
												
																
												
															
														</div>
													</div>
												</div>
											</div>
											<div class="alert alert-danger" id="msg" style="display:none;">
												Cette tranche existe déjà, merci d'entrer un autre montant.
											</div>	
											<input type="hidden" name="cgr_id" value="<?= $commande['cgr_id'] ?>">
											<div class="row">
												
			                                    <div class="col-md-4">
			                                    	<h3 style="margin-top:10px;" class="text-right">
			                                    	<?php if(!empty($t1)){ ?>
			                                    	<?= $t1['cgr_montant_max']; ?>
			                                    	<?php } else{ ?>
			                                    	0 
			                                    	<?php } ?>
			                                    	≥
													    
													</h3>
			                                    </div>
			                                    <div class="col-md-4">
			                                   
			                                    
			                                        <div class="section">
			                                            <input type="number" data-toggle="tooltip" title="Montant minimum (€)" required step="0.01" id="cgr_montant_min" name="cgr_montant_min" class="gui-input" placeholder="Montant (€)" value="<?= $commande['cgr_montant_min'] ?>">
			                                        </div>
			                                    </div>
			                                    <div class="col-md-4">
			                                    	<h3 style="margin-top:10px;" class="text-left">
		                                    		
			                                    	< 
			                                    	<?php if(!empty($t2)){ ?>
			                                    		<?= $t2['cgr_montant_min']; ?>
			                                    	<?php } else{ ?>
			                                    	 999999
			                                    	<?php } ?>
													    
													</h3>
			                                    </div>
			                                    
											</div>
											
											<div class="row">
												<div class="col-md-12">
													<div class="section-divider mb40">
														<span>Validants</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3">
													<div class="field select mb10">
			                                            <select name="type_1" id="type_1">
			                                                <option value="1"
			                                                <?php if(!empty($commande['cgr_profil_1'])){ ?>
			                                                	selected
			                                                <?php }?>
			                                                >Profil</option>
			                                                <option value="2" 

			                                                <?php if(empty($commande['cgr_profil_1'])){ ?>
			                                                	selected
			                                                <?php }?>

			                                                >Utilisateur</option>
			                                            </select>
			                                        	<i class="arrow simple"></i>
			                                        </div>
													
			                                        <div class="section">
			                                            <select name="utilisateur_1" id="utilisateur_1" class="select2">
			                                                <option value="0">Validant 1...</option>
			                                            </select>
			                                        </div>
			                                    </div>
			                                    <div class="col-md-3">
			                                    	<div class="field select mb10">
			                                            <select name="type_2" id="type_2">
			                                                <option value="1"
			                                                <?php if(!empty($commande['cgr_profil_2'])){ ?>
			                                                	selected
			                                                <?php }?>
			                                                >Profil</option>
			                                                <option value="2" 

			                                                <?php if(empty($commande['cgr_profil_2'])){ ?>
			                                                	selected
			                                                <?php }?>

			                                                >Utilisateur</option>
			                                            </select>
			                                        	<i class="arrow simple"></i>
			                                        </div>
			                                        <div class="section">
			                                            <select name="utilisateur_2" id="utilisateur_2" class="select2">
			                                                <option value="0" selected="selected">Validant 2...</option>
			                                            </select>
			                                        </div>
			                                    </div>
			                                    <div class="col-md-3">
			                                    	<div class="field select mb10">
			                                            <select name="type_3" id="type_3">
			                                                <option value="1"
			                                                <?php if(!empty($commande['cgr_profil_3'])){ ?>
			                                                	selected
			                                                <?php }?>
			                                                >Profil</option>
			                                                <option value="2" 

			                                                <?php if(empty($commande['cgr_profil_3'])){ ?>
			                                                	selected
			                                                <?php }?>

			                                                >Utilisateur</option>
			                                            </select>
			                                        	<i class="arrow simple"></i>
			                                        </div>	
			                                        <div class="section">
			                                            <select name="utilisateur_3" id="utilisateur_3" class="select2">
			                                                <option value="0" selected="selected">Validant 3...</option>
			                                            </select>
			                                        </div>
			                                    </div>
			                                    <div class="col-md-3">
			                                    	<div class="field select mb10">
			                                            <select name="type_4" id="type_4">
			                                                <option value="1"
			                                                <?php if(!empty($commande['cgr_profil_4'])){ ?>
			                                                	selected
			                                                <?php }?>
			                                                >Profil</option>
			                                                <option value="2" 

			                                                <?php if(empty($commande['cgr_profil_4'])){ ?>
			                                                	selected
			                                                <?php }?>

			                                                >Utilisateur</option>
			                                            </select>
			                                        	<i class="arrow simple"></i>
			                                        </div>	
			                                        <div class="section">
			                                            <select name="utilisateur_4" id="utilisateur_4" class="select2">
			                                                <option value="0" selected="selected">Validant 4...</option>
			                                            </select>
			                                        </div>
			                                    </div>
											</div>

											
											
											
											
									
										</div>
										
									</div>
										
								</div>
							</div>
							
						</div>

					</section>
					<!-- End: Content -->
				</section>
			</div>
			<!-- End: Main -->
			<footer id="content-footer" class="affix"  >
				<div class="row">
					<div class="col-xs-3 footer-left" >
						<a href="validations_bc.php" class="btn btn-default btn-sm" >
							<i class="fa fa-long-arrow-left"></i>
							Retour
						</a>
					</div>
					<div class="col-xs-6 footer-middle" >
					</div>
					<div class="col-xs-3 footer-right" >
						<button type="submit" class="btn btn-success btn-sm" >
							<i class="fa fa-add" ></i>
							Enregistrer
						</button>
					</div>
				</div>
			</footer>
	</form>
									
		
	<?php
		include "includes/footer_script.inc.php"; ?>	                         

		<script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
		<script src="assets/admin-tools/admin-forms/js/jquery.validate.french.js"></script>
		<script src="vendor/plugins/select2/js/select2.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				for(var i = 0; i<5; i++){
					actualiser_liste($("#type_" + i).val(), i);
				}

				
				$("#cgr_montant_min").change(function() {
					<?php if(empty($t1)){ ?>
						if($(this).val() < 0){
							$(this).val(<?= $commande['cgr_montant_min'] ?>);
						}
					<?php }else{ ?>
						if($(this).val() < <?= $t1['cgr_montant_max'] ?>){
							$(this).val(<?= $commande['cgr_montant_min'] ?>);
						}
					<?php } ?>

					<?php if(empty($t2)){ ?>
						if($(this).val() >= 999999){
							$(this).val(<?= $commande['cgr_montant_min'] ?>);
						}
					<?php }else{ ?>
						if($(this).val() >= <?= $t2['cgr_montant_min'] ?>){
							$(this).val(<?= $commande['cgr_montant_min'] ?>);
						}
					<?php } ?>
				});
				$("#type_1").change(function() {
					actualiser_liste($("#type_1").val(), 1);
				});
				$("#type_2").change(function() {
					actualiser_liste($("#type_2").val(), 2);
				});
				$("#type_3").change(function() {
					actualiser_liste($("#type_3").val(), 3);
				});
				$("#type_4").change(function() {
					actualiser_liste($("#type_4").val(), 4);
				});
				
				$('#cgr_montant_min').on('change', function(){
				    if($(this).val() < 0){
				        $(this).val("");
				    }
				    if($(this).val() > $("#cgr_montant_max").val()){
				    	console.log($(this).val());
				    	console.log($("#cgr_montant_max").val());
				    	alert("Votre montant minimum doit être inférieur à votre montant maximum");
				    }
				});
				$('#cgr_montant_max').on('change', function(){
				    if($(this).val() < 0){
				        $(this).val("");
				    }

				    if($(this).val() < $("#cgr_montant_min").val()){
				    	alert("Votre montant maximum doit être supérieur à votre montant minimum");
				    }
				});
			});	

			// fonctions
				function actualiser_liste(type, numero){

					// type == 1 => profil
					// type == 2 => utilisateur

					$.ajax({
						type:'POST',
						url: 'ajax/ajax_utilisateur_profil.php',                  
						data : 'type=' + type,          
						dataType: 'json',              
						success: function(data)       
						{
							$('#utilisateur_' + numero).find("option:gt(0)").remove();
							if(type==1){
								$.each(data, function(key, value) {
				            		$('#utilisateur_' + numero).append($("<option></option>").attr("value",value["pro_id"]).text(value["pro_libelle"]));
				        		});
							}else{
								$.each(data, function(key, value) {
				            		$('#utilisateur_' + numero).append($("<option></option>").attr("value",value["uti_id"]).text(value["uti_prenom"] + " " + value["uti_nom"]));
				        		});
							}
							$("#utilisateur_1").val(<?= $commande['cgr_utilisateur_1'] ?>);
							$("#utilisateur_1").trigger("change");
							$("#utilisateur_2").val(<?= $commande['cgr_utilisateur_2'] ?>);
							$("#utilisateur_2").trigger("change");
							$("#utilisateur_3").val(<?= $commande['cgr_utilisateur_3'] ?>);
							$("#utilisateur_3").trigger("change");
							$("#utilisateur_4").val(<?= $commande['cgr_utilisateur_4'] ?>);
							$("#utilisateur_4").trigger("change");
							
						}
					});
					

				}
			// fin fonctions

		</script>
	</body>
</html>
