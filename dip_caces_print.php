<?php 

setlocale(LC_TIME, "fr");
date_default_timezone_set('Europe/Paris');

include "includes/controle_acces.inc.php";

include('includes/connexion.php');

include('modeles/mod_get_juridique.php');

include('modeles/mod_parametre.php');

include('modeles/mod_caces_data.php');

// VISU D'UN OU PLUSIEUR CACES

$erreur=0;

$dossiers=array();

$dossier_id=0;
$print=0;

if(!empty($_GET["dossier"])){
	$dossiers[]=intval($_GET["dossier"]);
}elseif(!empty($_POST["dossiers"])){
	
	$dossiers=$_POST["dossiers"];
	if(!empty($_POST["print"])){
		$print=intval($_POST["print"]);
	}
}

if(empty($dossiers)){
	$erreur_txt="impossible d'afficher la page";
	echo($erreur_txt);
	die();
	
}else{

	// la personne logue
	$acc_agence=0;
	if(isset($_SESSION['acces']["acc_agence"])){
		$acc_agence=$_SESSION['acces']["acc_agence"];  
	}
	$acc_societe=0;
	if(isset($_SESSION['acces']["acc_societe"])){
		$acc_societe=$_SESSION['acces']["acc_societe"];  
	}

	$acc_utilisateur=0;
	if(isset($_SESSION['acces']["acc_ref"])){
		if($_SESSION['acces']["acc_ref"]==1){
			$acc_utilisateur=$_SESSION['acces']["acc_ref_id"];
		}
	}

	// ON RECUPERE LES DONNES DU DEVIS A AFFICHER
	
	$data=caces_data($dossiers);
	if(empty($data)){
		$erreur_txt="impossible d'afficher la page";
		echo($erreur_txt);
		die();
	}
	
} ?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<title>ORION</title>
		<meta name="keywords" content=""/>
		<meta name="description" content="">
		<meta name="author" content="Si2P">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Theme CSS -->

		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/theme.css">
		<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
		
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/select2.min.css" />
		<link rel="stylesheet" type="text/css" href="/vendor/plugins/select2/css/core.css" />
		<link rel="stylesheet" type="text/css" href="vendor/plugins/summernote/summernote.css" >
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion.css">
		
		<link rel="stylesheet" type="text/css" href="assets/skin/si2p/css/orion_print.css">

		<link rel="shortcut icon" href="assets/img/favicon.png">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css" >
			#container_print{
				width:21cm;
			}
			
			.page{
				height:28cm;
			}
			
			.diplome{
				border:1px solid black;
				height:50%;
			}
			
			.dip-col{
				margin:none;
				padding:none;
				font-size: 0;
				height:100%;
			
				
			}
			.dip-col>div{
				vertical-align:middle;
				display:inline-block;
				font-size: 9pt;
				height:100%;
				padding:5px;
				
			}
			
			
			.dip-col-20{
				width:20%;
			}
			.dip-col-45{
				width:45%;

			}
			.dip-col-50{
				width:50%;
			}
			.dip-col-25{
				width:25%;
				
			}
			.dip-col-70{
				width:70%;			
			}
		
			
			
			.soc_logo{
				max-height:150px;
				max-width:300px;
				
			}
			.soc_logo_2{
				max-height:75px;
				max-width:150px;
			}
			.signature{
				max-width:150px;
			}
			
			.bloc-categorie{
				padding:0px!important;
				margin:0px!important;
				width:77%;
			}
			.bloc-photo{
				padding:0px 0px 0px 2px!important;
				margin:0px!important;
				width:20%;
			}
			
			.vertical-rl{
				width:3%;
				text-align:center;
				writing-mode : vertical-lr;
				
				margin:0px 0px 0px 0px!important;
			
				padding:0px 0px 0px 0px!important;
			}
			
			.cachet{
				font-size:10px;
				border:1px solid black;
				padding:3px;
			}
			.photo{
				width:92%;
				margin;auto;
				border:1px solid black;
				text-align:center;
				height:180px;
			}
			.photo>img{
				max-width:100%;
				max-height:100%;
			}
			.mention{
				margin:5px 0px 0px 0px;
				padding-bottom:2px!important;
				font-size:7pt;
			}
			.aipr{
				margin:5px 0px 0px 0px;
				padding:2px;
				text-align:center;
				border:1px solid black;
			}
			.sans-object{
				background-image:url("assets/img/caces_croix.png");
				background-size: 100% 100%;
				background-position:center center;
			}
	
		</style>
		<style type="text/css" media="print" >
			@page{
				size:A4 portrait;
				margin:5mm;
			}
		</style>
	</head>
	<body class="sb-top sb-top-sm document-affiche">
		<div id="zone_print" ></div>
		<div id="main">
	<?php	include "includes/header_def.inc.php"; ?>
			<section id="content_wrapper">
				<section id="content" class="animated fadeIn">	
		<?php 		if($erreur==0){ ?>
		
							<div class="row" >
							
								<!-- zone d'affichage du devis -->
								
								<div class="col-md-12"  >	
								
									<div id="container_print" >
								
										<div id="page_print" >
										
											<div class="page" >
										
										<?php	$nb_dip=0;
											
												foreach($data as $num => $caces){ 

													$nb_dip++; ?>
										
													<div class="diplome" >
														<div class="dip-col" >
															<div class="dip-col-45" >
																<p>
																	<i><u>Titulaire :</u></i><br/>
																	<?=$caces["sta_identite"]?>
																</p>
																<p>
															<?php	if($caces["qualification"]==16){
																		echo("<i><u>CAUSPR numéro(s) :</u></i><br/>");
																	}else{ 
																		echo("<i><u>CACES® numéro(s) :</u></i><br/>");
																	} ?>
																	
																	<ul>
															<?php		foreach($caces["categories"] as $cat){
																			if(!empty($cat["dcc_numero"])){?>
																				<li><?=$cat["dcc_numero"]?></li>
															<?php			}
																		} ?>
																	</ul>
																</p>
																<p>
																	<i><u>Délivré par :</u></i><br/>
															<?php	echo($caces["societe"]["soc_nom"]);
																	if(!empty($caces["societe"]["soc_ad1"])){
																		echo("<br/>" . $caces["societe"]["soc_ad1"]);
																	}
																	if(!empty($caces["societe"]["soc_ad2"])){
																		echo("<br/>" . $caces["societe"]["soc_ad2"]);
																	}
																	if(!empty($caces["societe"]["soc_ad3"])){
																		echo("<br/>" . $caces["societe"]["soc_ad3"]);
																	}
																	echo("<br/>" . $caces["societe"]["soc_cp"] . " " . $caces["societe"]["soc_ville"]); ?>																	
																</p>
																<p>
																	<i><u>Inscrit dans la base de l'INRS sous le N° :</u></i> <b><?=$caces["societe"]["soc_inrs"]?></b>										
																</p>
														<?php	if($caces["reco"]=="R400"){  	
																	if($caces["qualification"]!=16){ ?>		
																		<p class="cachet" >
																			Pour vérifier la validité de ce(s) CACES® [employeurs] ou pour éditer l'attestation correspondante [titulaire] consulter la base de données CACES® sur le site : http://www.ameli.fr
																		</p>
														<?php		}
																} ?>
															</div>
															<div class="dip-col-5 vertical-rl" >Document recto / verso. Toute copie doit comporter les deux faces.</div>
															<div class="dip-col-50 text-center" >	
														
													<?php		if(!empty(file_exists($_SERVER["DOCUMENT_ROOT"] . "/documents/societes/logos/" . $caces["societe"]["soc_logo_1"]))){ ?>
																	<img src="/documents/societes/logos/<?=$caces["societe"]["soc_logo_1"]?>" class="soc_logo" />
													<?php		} ?>
																<h1><?=$caces["societe"]["soc_nom"]?></h1>
																<p>
													<?php			if(!empty($caces["societe"]["soc_ad1"])){
																		echo("<br/>" . $caces["societe"]["soc_ad1"]);
																	}
																	if(!empty($caces["societe"]["soc_ad2"])){
																		echo("<br/>" . $caces["societe"]["soc_ad2"]);
																	}
																	if(!empty($caces["societe"]["soc_ad3"])){
																		echo("<br/>" . $caces["societe"]["soc_ad3"]);
																	}
																	echo("<br/>" . $caces["societe"]["soc_cp"] . " " . $caces["societe"]["soc_ville"]);
																	
																	if(!empty($caces["societe"]["soc_tel"])){
																		echo("<br/>Tél: " . $caces["societe"]["soc_tel"]);
																	}
																	if(!empty($caces["societe"]["soc_fax"])){
																		echo("<br/>Fax: " . $caces["societe"]["soc_fax"]);
																	}
																	if(!empty($caces["societe"]["soc_mail"])){
																		echo("<br/>" . $caces["societe"]["soc_mail"]);
																	}
																	if(!empty($caces["societe"]["soc_site"])){
																		echo("<br/>" . $caces["societe"]["soc_site"]);
																	} ?>
																</p>
																<h1 class="text-center" >
															<?php	if($caces["qualification"]==16){
																		echo("CAUSPR");
																	}else{ 
																		echo("CACES®");
																	} ?>
																</h1>
													<?php		if(!empty($caces["societe"]["soc_logo_2"]) AND file_exists($_SERVER["DOCUMENT_ROOT"] . "/documents/societes/logos/" . $caces["societe"]["soc_logo_2"])){ ?>
																	<table>
																		<tr>
																			<td class="dip-col-50" >
																				<img src="/documents/societes/logos/<?=$caces["societe"]["soc_logo_2"]?>" class="soc_logo_2" />
																			</td>
																			<td class="dip-col-50" >
																				<img src="assets/img/CNAMTS.png" />
																			</td>
																		</tr>
																	</table>
													<?php			if($caces["qualification"]!=16){ ?>
																		<p>La marque CACES® est protégée par un dépôt à l'INPI sous le numéro 03.3237295</p>																	
													<?php			}
																}else{ ?>	
																	<table>
																		<tr>
																			<td class="dip-col-50" >
													<?php						if($caces["qualification"]!=16){ ?>
																					La marque CACES® est protégée par un dépôt à l'INPI sous le numéro 03.3237295
													<?php						} ?>		
																			</td>
																			<td class="dip-col-50" >
																				<img src="assets/img/CNAMTS.png" />
																			</td>
																		</tr>
																	</table>
													<?php		} ?>
															</div>																												
														</div>
													</div>
											
										<?php		// ON AFFICHE LES VERSO LIE AURECTO CREE PLUS TOT
													
													if($nb_dip==2 OR $num==count($data)-1 ){ 
														echo("</div>");
														echo("<div class='saut-page' ></div>");
														echo("<div class='page' >"); ?>
														
												<?php	$verso=array();
														if($nb_dip==2){
															$verso[]=$data[$num-1];
														}
														$verso[]=$data[$num];
														
														foreach($verso as $caces_verso){

															/*echo("<pre>");
																print_r($caces_verso);
															echo("<pre>");*/ ?>
															
															<div class="diplome" >											
																<h1 class="text-center pn mn" >
												<?php				if($caces["qualification"]==16){
																		echo("CAUSPR " . $caces_verso["reco_code"] . " - " . $caces_verso["reco_nom"]);
																	}else{
																		echo("CACES® " . $caces_verso["reco_code"] . " - " . $caces_verso["reco_nom"]);
																	}	?>
																</h1>																
																<div class="dip-col" >
																	<div class="bloc-photo" >
																		<div class="photo" >
																<?php		if(file_exists("documents/Stagiaires/sta_phid_". $caces_verso["sta_id"] . ".jpg")){ ?>
																				<img src="documents/Stagiaires/sta_phid_<?=$caces_verso["sta_id"]?>.jpg" alt="" id="sta_photo_preview" />
															<?php			} ?>																				
																		</div>
																		
																		<p>
																			<i><u>Titulaire (en toutes lettres):</u></i><br/>
																			<?=$caces_verso["sta_identite"]?>
																		</p>
																		<p>
																			<i><u>Date de naissance :</u></i><br/>
																			<?=$caces_verso["sta_naissance_txt"]?>
																		</p>
																		<p>
																			<i><u>Signataire (en toutes lettres):</u></i><br/>																			
																			<?=$caces_verso["societe"]["soc_resp_identite"]?><br/>
																			<?=$caces_verso["societe"]["soc_resp_fonction"]?><br/>
																	<?php	if(!empty(file_exists($_SERVER["DOCUMENT_ROOT"] . "/documents/Utilisateurs/Signatures/signature_" . $caces_verso["societe"]["soc_responsable"] . ".png"))){ ?>
																				<img src="/documents/Utilisateurs/Signatures/signature_<?=$caces_verso["societe"]["soc_responsable"]?>.png" class="signature" />
																	<?php	}  ?> 
																		</p>
																	</div>
																	<div class="bloc-categorie" >
																	<?php
																		/*echo("<pre>");
																			print_r($caces_verso["categories"]);
																		echo("</pre>"); */	?>
																		<table class="tab-border" style="font-size:10px;" >
																			<tr>
																				<th>Cat.</th>
																		<?php	if($caces["reco"]=="R400"){ 
																					echo("<th>Type</th>");
																				} ?>
																				<th>
																		<?php		if($caces["qualification"]==16){
																						echo("N° du CAUSPR<br/>");
																					}else{
																						echo("N° du CACES®<br/>");
																					} ?>																						
																				</th>
																				<th>
																					Options
																			<?php	if(!empty($caces["reco_mention"])){
																						echo($caces["reco_mention"]);
																					} ?>
																				</th>
																				<th>
																					NOM - Prénom<br/>
																					du testeur
																				</th>
																				<th>
																					Obtention<br/>
																					<b>Échéance</b>
																				</th>
																			</tr>
																	<?php	

																			foreach($caces_verso["categories"] as $cat){ 
																				if(!empty($cat["dcc_numero"])){ ?>
																					<tr>
																						<td><?=$cat["qca_libelle"]?></td>
																				<?php	if($caces["reco"]=="R400"){ 	
																							echo("<td>" . $cat["qca_nom"] . "</td>");
																						} ?>
																						<td><?=$cat["dcc_numero"]?></td>
																						<td class="ptn pbn" style="font-size:9px;"  >
																							
																					<?php	if(!empty($cat["qua_opt_1"])){ 
																								echo($cat["qua_opt_1"] . ": ");
																								if(!empty($cat["dcc_opt_1_valide"])){ 
																									echo("OUI");
																								}else{
																									echo("NON");
																								}
																							} 
																							if(!empty($cat["qua_opt_2"])){ 
																								echo("<br/>" . $cat["qua_opt_2"] . ": ");
																								if(!empty($cat["dcc_opt_2_valide"])){ 
																									echo("OUI");
																								}else{
																									echo("NON");
																								}
																							} 
																							/*if(!empty($cat["qua_opt_3"])){ 
																								echo("<br/>" . $cat["qua_opt_3"] . ": ");
																								if(!empty($cat["dcc_opt_3_valide"])){ 
																									echo("OUI");
																								}else{
																									echo("NON");
																								}
																							} */?>
																						</td>
																						<td><?=$cat["dcc_testeur_identite"]?></td>
																						<td>
																					<?php	if($cat["dcc_date"]<$caces_verso["obtention"]){
																								echo("<b>" . $caces_verso["obtention_fr"] . "</b>");
																							}else{
																								echo("<b>" . $cat["dcc_date_fr"] . "</b>");
																							} ?>																						
																							<br/>																							
																							<b><?=$caces_verso["validite"]?></b>
																						</td>
																					</tr>
																<?php			}else{ ?>
																					<tr>
																						<td><?=$cat["qca_libelle"]?></td>
																				<?php	if($caces["reco"]=="R400"){  		
																							echo("<td>" .  $cat["qca_nom"] . "</td>");
																						} ?> 
																						<td colspan="4" class="sans-object" >
																							<!--<img src="/assets/img/caces_croix.png" alt="sans object" title="sans object" />-->
																						</td>
																					</tr>
																<?php			}
																			} ?>
																		</table>
																<?php	if(!empty($caces["reco_mention_txt"])){
																			echo("<p class='mention' >" . $caces["reco_mention_txt"] . "</p>");
																		}
																		if($caces["reco"]=="R400"){  	
																			if($caces["qualification"]!=16){
																				if(!empty($caces_verso["aipr"])){ ?>
																					<div class="aipr" >Option RESEAUX : Réussite au QCM-IPR "opérateur" le : <b><?=$caces_verso["aipr"]?></b></div>	
																		<?php	}else{ ?>
																					<div class="aipr" >Ce(s) CACES® ne permet(tent) pas la délivrance d'un AIPR</div>																
																		<?php	}
																			}
																		}	?>
																	</div>
																	<div class="vertical-rl" >
																		Document recto / verso. Toute copie doit comporter les deux faces.
																	</div>
															
																</div>
															</div>
																				
										<?php			}
														
														$nb_dip=0;
														if($num!=count($data)-1){
															echo("</div>");
															echo("<div class='saut-page' ></div>");
															echo("<div class='page' >"); 
														}
														
													}
												} ?>
											</div>	
										
										</div>
										<!-- FIN PAGE PRINT -->
									</div>
								</div>
								<!-- fin zone d'affichage du devis -->

							</div>
						

		<?php		}else{ ?>
						
						<p class="alert alert-danger text-center" >
							Impossible d'afficher cette page!
						</p>
					
		<?php		} ?>
									
				</section>
				
				<!-- End: Content -->
			</section>
			
			
		</div>
		<!-- End: Main -->
		
		<footer id="content-footer" class="affix" >
			<div class="row">
				<div class="col-xs-3 footer-left" >	
		<?php		if(isset($_SESSION["retourCaces"])){
						if(!empty($_SESSION["retourCaces"])){ ?>
							<a href="<?=$_SESSION["retourCaces"]?>" class="btn btn-sm btn-default" >
								<i class="fa fa-left-arrow"></i> Retour
							</a>
		<?php			}
					} ?>
					
				</div>
				<div class="col-xs-6 footer-middle text-center" >		
					<button type="button" class="btn btn-sm btn-info ml15" id="print_local" >
						<i class="fa fa-print"></i> Imprimer
					</button>							
				</div>
				<div class="col-xs-3 footer-right" ></div>
			</div>
		</footer>
		
<?php	include "includes/footer_script.inc.php"; ?>	
		<script type="text/javascript">
		
			var l_page=21;
			var h_page=28;

			jQuery(document).ready(function (){								
				var ppc=(parseInt($("#page_print").width())/l_page);
				var hauteur=parseInt((h_page*ppc));
				/*$(".page").each(function( index ){
					$(this).css("height", hauteur + "px");
				});*/
			
				$("#print_local").click(function(){
		
					$("#zone_print").html($("#container_print").html());
					
					$("#main").hide();
					$("#content-footer").hide();					
					$(".btn").hide();
					
					$("#zone_print").show();
					
					window.print();
					
					$("#zone_print").hide();
					
					$("#main").show();
					$("#content-footer").show();
					$(".btn").show();
					
					
				});
				
			});
			
			
		</script>
	</body>
</html>
